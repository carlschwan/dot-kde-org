---
title: "15 years of KDE e.V. - Today"
date:    2012-11-29
authors:
  - "jospoortvliet"
slug:    15-years-kde-ev-today
comments:
  - subject: "20 = 38"
    date: 2012-11-29
    body: ";-)"
    author: "unormal"
  - subject: "Guess on the numbers"
    date: 2012-11-30
    body: "6 = KDE desktop, possibly around 2.4\r\n11 = Eva Brucherseifer\r\n12 = Daniel Molkentin\r\n(and a wild guess, this was a kdepim meeting)\r\n18 = the mouse from the famous german kids show \"Die Sendung mit der Maus\"\r\n19 = a list of candidates for the Vorstand, must have been one of the first\r\n20 + 38 = David Faure\r\n21 = Mr current_president Cornelius Schumacher\r\n\r\n"
    author: "neverendingo"
  - subject: "Some additions"
    date: 2012-11-30
    body: "22 = 26 = Matthias Ettrich\r\n13 = Kalle Dalheimer\r\n\r\n:)"
    author: "Milian Wolff"
  - subject: "Crazy People"
    date: 2012-11-30
    body: "Yes, we KDE guys have always been crazy people. Holy shit, some of the pics really look (intentionally) weird. I still remember how we shot the photo 16+17 to make fun of Ralf while he was sleeping. Totally hilarious."
    author: "Torsten Rahn"
  - subject: "That also solves the question"
    date: 2012-11-30
    body: "That also solves the question of who person 16 is :P"
    author: "neverendingo"
  - subject: "KFM 0.0.1 users represent!"
    date: 2012-11-30
    body: "LOL, I remember using KFM 0.0.1 with Window Maker when it launched.  Good times and good stuff.  I wish KDE all the best going forward."
    author: "Trae McCombs"
  - subject: "Best DE"
    date: 2012-11-30
    body: "keep up the good work guys.Big fan"
    author: "Yusup"
  - subject: "Kandalf"
    date: 2012-12-01
    body: "32 Is Kandalf, KDE's former mascot.\r\nHappy Birthday KDE e.V."
    author: "emilsedgh"
  - subject: "Not 18..."
    date: 2012-12-07
    body: "Doesn't say what is 18, though :D"
    author: "jospoortvliet"
  - subject: "I did before and..."
    date: 2012-12-10
    body: "Jos, you live in germany now, you should know it already :P see http://www.wdrmaus.de/"
    author: "neverendingo"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://ev.kde.org"><img src="http://dot.kde.org/sites/dot.kde.org/files/ev-LogoAt15Shaded4.png" width="200" /></div>
Last Tuesday (November 27, 2012) was the 15th birthday of KDE e.V. (eingetragener Verein; registered association), the legal entity which represents the KDE Community in legal and financial matters. We <a href="http://dot.kde.org/2012/11/27/15-years-kde-ev-early-years">interviewed two of the founding members</a> (Matthias and Matthias) on the why, what and when of KDE e.V. in the beginning and <a href="http://dot.kde.org/2012/11/28/15-years-kde-ev-growing">presented a video interview with emeritus board member Mirko Böhm</a>. Today, we focus on the present with interviews with two current KDE e.V. Board members, Lydia Pintscher and Cornelius Schumacher. On the bottom, we've got a nice puzzle for you. And next week, we'll present an impression of the day to day activities of KDE e.V.

<h2>Lydia Pintscher on being a board member</h2>
In this interview, Lydia points out the importance of the e.V. in explaining to people what KDE does and how it is worth supporting it in this world with increasingly high-walled gardens and vendor lock-in.

<strong>Why did you choose to volunteer for the KDE e.V. Board of Directors?</strong>
I stepped up because it is a role that needs to be filled and is important for KDE as a whole. By doing this I want to free up people's time so they can do great work in other areas of KDE and don't have to spend their time on administration and politics.

<strong>How would you describe the job of KDE e.V. and its Board today?</strong>
The KDE e.V. supports the KDE community in financial, legal and organizational matters. This includes things like raising money for sprints, running our annual conference Akademy, being a steward for the Fiduciary Licensing Agreement (FLA), finding answers for legal questions our contributors have and of course it is also a formal representation to the outside world that can't deal with a fuzzy thing like "the KDE community".

The Board decides on the direction, distributes money, and does a lot of the day-to-day tasks to run a non-profit. Generally speaking, the Board makes sure things happen. Claudia Rauch, KDE e.V.'s Business Manager (and only employee) supports the Board a great deal in these tasks. 

<strong>You have been involved for quite a long time both within the community and the formal organization of KDE e.V. Please tell us a bit about how both have changed over the years?</strong>
Both the community and the e.V. have grown and have become more mature since I joined. We've got a Business Manager now who makes a huge difference. We've got new working groups like the community working group, things like the Code of Conduct, the FLA and the Manifesto and more. All of this makes a huge difference and ensures KDE is here to stay even if individual people might come and go as in any other Free Software project.

<strong>What challenges do you see for the future of KDE e.V.?</strong>
KDE e.V. has to continue to make people understand that what matters in the big picture is what the KDE community does and that this is worth supporting. With the continuing vendor lock-in and closing down in our industry, this is more important than ever. 

<strong>How does it make you feel thinking about what KDE has become?</strong>
Proud. It's amazing to see what KDE has become. It's a project that started not far away from where I have studied for years. I'm delighted about how it touches the lives of people all over the world.

<h2>Cornelius Schumacher - President of KDE e.V.</h2>
Camila Ayres interviewed Cornelius to get the views of the President of KDE e.V. In the process, it comes out that the work on KOrganizer never seems to be finished. And Cornelius gives us a glimpse of 2013 and the future of KDE beyond next year.

<strong>Camila: I’m here with Cornelius Schumacher to talk a bit about KDE e.V. How did you start with KDE?</strong>
<strong>Cornelius:</strong> With KDE, I got involved in 1999 when I was looking for some opportunities to do open source work. Some colleagues of mine worked on KDE or they used KDE at least. I had used Qt before so it was kind of natural for me to look for a project that was using Qt.

I started hacking like crazy for a couple of years and then I got involved in the organization behind KDE, KDE e.V.

<strong>How did you get to the KDE e.V. Board of Directors?</strong>
That was in 2005 in Malaga at Akademy during the General Assembly. It’s one of the activities of Akademy, the annual meeting in person of KDE. There I got elected to the Board. I had attended a couple of General Assemblies before. In Hamburg in 2002, we had something of a revival of KDE e.V. Before that for one or two years, it was really quiet; there wasn’t a lot of activity. We got together all the people that we could to inject some energy into KDE. I think that worked pretty well. It gave us a good restart and added a couple of new Board members. After a couple of years, I saw that I could contribute more to the organization side rather than just coding. I ran for the Board and got elected.

<strong>Tell us a bit more about KDE e.V. and its Board.</strong>
KDE e.V. is the formal organization behind the KDE community. It has three primary tasks. The first one is to represent the community. KDE e.V. is the organization that represents KDE in legal and financial matters. We can hold money, own trademarks and web domains. Something that wouldn’t be possible if there weren’t a formal body. This also includes acting as point of contact and coordination. So one important part of the three tasks is representation. 

The second is support. The main goal of KDE e.V. is to support the community in creating great software. Great KDE software. We do that mostly by organizing Akademies, an annual event, and by supporting people to go to sprints. These are focused events where people can get together for a short time in small groups to write code, inject creativity and momentum into the community. That’s where we can do a lot as KDE e.V. using the money we get from donors and sponsors. This is a really important aspect of the community because we can support people who otherwise wouldn’t be able to contribute to KDE in the same way.

Third area is the governance aspect. Of course KDE is an open source project. We are doing Free Software. We are using normal governance mechanisms which are typical for these kind of projects. The important aspect of our way of governance is that the people who are doing the work decide what is happening and what is done. That’s where KDE e.V. took the conscious decision to not interfere with how these processes work. The people who write the code take the decisions about the code. KDE e.V. does not get involved with control over how development is being done. 

This is an important decision that KDE e.V. made. We support the community, we represent the community, but we don’t control the community. So we don’t get involved in development. About the concrete questions of development, we are only involved in the support part. In this way, we don’t introduce friction in the community by trying to decide things that should be decided by the people who are doing the work.

So that’s what KDE e.V. is about: representation, support and governance. 

<strong>You have been active for quite a long time in KDE. Please talk about how both have changed over the years... the Community and KDE e.V.</strong>
Sure. I can try. I haven’t been working with KDE for a really long time. I wasn’t working on KDE when things started. So I wasn’t one of the initial coders such as people like Matthias Ettrich, Martin Konold, and Kalle Dalheimer. Those guys who were there in the first hours, days or months of the Project. When everything got started. I got involved at the transition from KDE1 to KDE2. When I started I think that my first patch was fixing KOrganizer which was crashing at odd times. Something like that. 

It was an interesting situation when I started, because in this transition, everything was broken. People were just hacking like crazy and putting things back together again. We wouldn’t do it this way any more. Look at KDE. How it is now. Much bigger, much better organized. We have unit tests, we have continuous integration. We have all this stuff that’s needed to run a big project that has a lot of code. 

And of course the growth of the community is showing. So at Akademy, there are more people. I remember at the first big gathering in a small village in the Czech Republic. We had about a hundred people there, and everyone was sitting in 2 big rooms and hacking. There will also a couple of talks, but hacking really was the focus. Nowadays at Akademy, there are more people, and also more interaction and more coordination needed. There is still a feeling that we are one community. But now there are subgroups that work on a specific part of KDE. So it’s not just one group of people where everybody is knowing everybody. But groups of teams that work together to create something bigger. That’s also an interesting development.

For KDE e.V. as an organization, we have achieved the ability to run our business in a professional way. We have supporting members. An office in Berlin. An employee who is helping the community by doing all the administration and organizing. These things would be really hard without somebody who can spend full time on it. We have a professional organization behind KDE to support those things. That reflects the relevance of the project. KDE is one of the biggest open source projects on the planet. We are in a situation where we can actually sustain this organization to support the community to write all the wonderful KDE software we have.

<strong>What challenges do you see in the future of KDE e.V.?</strong>
Challenges for the future? One is that our initial mission was to create a desktop for Linux machines and other Unix machines . This is more or less completed. If you run a modern Linux system, you get a decent desktop. There’s no question about that. People are happy with that. There’s not really a debate about what is missing there. Or that Linux desktops are much less than other systems. And also the desktop as a concept is becoming less relevant, so our initial mission is mostly completed. And maybe that mission is not a thing that is exciting people that much any more. It’s just there, not exciting. Everybody expects it to be what it is. So that’s not the most exciting area of software any more. 

People also have phones, tablets, all kinds of other devices. So that’s an area where a lot of development is going on. If you look at how Android developed, you see an interesting way that Linux became very popular that no one expected a couple of years ago. And certainly not when KDE was founded. So we’re dealing with these challenges. Finding a place there for KDE. There are a lot of things going on. People working on Plasma Active and other stuff. There we see challenges where KDE as a project has to extend its reach and write software that is not only important and focused on the desktop but in new areas as well. 

And for KDE e.V. Our mission is to support those changes, so of course this is a challenge for us as well. In general for KDE e.V., we had some good times from the financial point of view over the last couple of years when Nokia bought Trolltech and jumped in heavily, betting on Qt. There were more resources available than there are now. Things are getting a bit smaller. We have be more careful with our planning and with our expenses so that we actually can sustain the activities we have built up over the last couple of years. That is certainly one of our biggest challenges--to make sure that KDE e.V. is organized enough to be able to support the community the way the community needs.

I see that there’s still a lot of enthusiasm out there. I was really excited about the donation campaign for the Randa Sprints a short while ago. We reached out to people in the KDE community and outside the community, to KDE users asking them to support the sprints and KDE with donations. We got the donations we needed for those sprints. So I’m pretty confident that the community is strong to sustain all those activities we have put in place. But it certainly will take some effort. So from the KDE e.V. point of view, that is the most challenging part of the next few years.

<strong>What are the plans for KDE e.V. for 2013?</strong>
We had a Board meeting in Berlin a few weeks ago. And we talked about the plans for the next year there. Basically we want to sustain the business we have. Akademy is a big goal for 2013. We have a wonderful location in Bilbao where the next Akademy will happen. We expect to have a lot of community people there. 

Another particular goal for 2013 is that we want to get closer to the Qt community and work closer together with that group. Obviously Qt is an important part of KDE. That’s the library that all KDE applications are based on. Now Qt is more independent and also has open governance. KDE has an opportunity to contribute there in a way that wasn’t possible before. We want to remove barriers which were there for historical reasons or perhaps because of a corporate structure. I was at Qt Developer Days which just happened. There we had a lot of interesting conversations about KDE and Qt and how the future could be. I’m seeing a lot of opportunities there. Together KDE & Qt make a strong team. If you look at who’s active in the Qt community, there are a lot of KDE people or people who have some KDE history. There are people working on things where we can work together. So that’s something that KDE e.V. could potentially support. We could get people involved to grow the contributor base. It’s important that we get rid of barriers that prevent us from being the most effective. And then, of course we will run sprints again and are trying to reach the same level as we reached this year. It was a bit tougher to run the sprints from a financial point of view. Things will be different next year so we have to be careful about what we support. We want to do as much as we can. So that’s one of the goals for the next year.

<strong>You have been involved with KDE for quite a long time. How does it make you feel when you think about what KDE has become?</strong>
I’m just fascinated by how this whole thing has worked. I was already fascinated at the beginning because it was such an ambitious project. It seemed pretty much impossible to actually do what we thought about. What really impresses me if I look from today’s perspective is that the people who started KDE had the foresight to create the KDE e.V. to support the project. That really gave KDE stability and also outreach that wouldn’t have been possible otherwise.

I also didn’t imagine that we would reach this level of professional organization. A nice office in the middle of Berlin together with the FSFE. That we would run something like the Desktop Summit two years ago in Berlin together with GNOME. We had hundreds of people working on the Free Desktop in one place. That’s really exciting. 

What always surprises me is how strong the ties of the KDE community are. That’s something which actually hasn’t changed over the years. I’m a bit surprised that it doesn’t really change that much. If you go to a meeting with other KDE people, it still feels like meeting old friends. That’s the same as it was at the beginning. That feeling comes from having a strong community and bonds between KDE people, which is quite impressive. That’s something that makes me happy. 

<strong>Thank you very much for your time. It is awesome to talk with you. It’s great to get the perspective of the past and get some ideas about how the future might be.</strong>

<i>Recorded interview transcribed by Carl Symons</i>

<h2>Join the Game</h2>
Please consider supporting KDE e.V. by becoming a <a href="http://jointhegame.kde.org/">supporting member</a>. We're playing a big game and invite you to join it.

<h2>Quest of the 42</h2>
The first article featured a collage of images from 'the early days' as well as one outlier. We've now slapped some numbers on it - it's up to you to assign names/descriptions/memories/etc to them in the comments!

<div style="position: relative; center; padding: 1ex; margin-left: 1px; width: 700px; border: 0px solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/collage_numbers_0.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/collage_numbers_small.jpg" /></a>
<br />
<table border="1" width="700" >
<colgroup span="4" width="175">
      </colgroup>
<tr>
<td>1 </td><td>2 </td><td>3 </td><td>4 </td>
</tr>
<tr>
<td>5 </td><td>6 - KDE desktop, possibly around 2.4</td><td>7 </td><td>8 </td>
</tr>
<tr>
<td>9 - Ralf Nolden</td><td>10 - Matthias Ettrich</td><td>11 - Eva Brucherseifer</td><td>12 - Daniel Molkentin</td>
</tr>
<tr>
<td>13 - Kalle Dalheimer</td><td>14 - Reggie Stadlbauer</td><td>15 -  Trysil (Norway bugfix sprint)</td><td>16 - Torsten Rahn</td>
</tr>
<tr>
<td>17 - Ralf Nolden</td><td>18 - the mouse from the famous german kids show "Die Sendung mit der Maus"</td><td>19 - list of candidates for the Vorstand, must have been one of the first</td><td>20 - David Faure</td>
</tr>
<tr>
<td>21 - Mr current_president <br />Cornelius Schumacher</td><td>22 - Matthias Ettrich</td><td>23 - Werner Trobin</td><td>24 - Christoph Neerfeld?</td>
</tr>
<tr>
<td>25 </td><td>26 - Matthias Ettrich</td><td>27 - Arnt Gulbrandsen</td><td>28 - Lars Knoll Trolltech Employee #3</td>
</tr>
<tr>
<td>29 - Torben Weis - prolific coder</td><td>30 - Bradley Hughes</td><td>31 - mosfet (Daniel M. Duley)</td><td>32 - Kandalf, KDE's former mascot</td>
</tr>
<tr>
<td>33 - KCalc</td><td>34 </td><td>35 - Ralf Nolden</td><td>36 - Mirko Böhm</td>
</tr>
<tr>
<td>37 - mosfet</td><td>38 - David Faure</td><td>39 - Matthias Hölzer-Klüpfel</td><td>40 - Arnt Gulbrandsen</td>
</tr>
<tr>
<td>41 </td><td>42 </td><td>&nbsp; </td><td>&nbsp; </td>
</tr>
</table>
</div>