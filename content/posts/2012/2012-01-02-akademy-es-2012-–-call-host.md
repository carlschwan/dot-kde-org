---
title: "Akademy-es 2012 \u2013 Call for Host"
date:    2012-01-02
authors:
  - "apol"
slug:    akademy-es-2012-–-call-host
---
KDE España has started planning Akademy-es 2012—the most important KDE-related event in Spain. The conference is an opportunity for Spanish KDE users and developers to meet, share experiences, catch up on KDE news, and plan for the future. Akademy-es includes a range of presentations, workshops, hacking sessions, informal get-togethers and an assembly of KDE España members. KDE members and supporters will be coming from other countries to enjoy famous Spanish hospitality and meet up with KDE friends. The date for Akademy-es depends on what location is chosen and the availability of suitable facilities.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/akademy-es-2011500px.jpeg" /><br />Some Akademy-es 2011 participants</div>

In order to have a successful Akademy-es 2012, KDE España will collaborate with a local group or association. This group manages tasks such as securing a location, finding suitable and economical housing and places to eat, and making sure that the location is suitably equipped with Internet and audiovisual equipment. It also helps coordinate the actual Akademy-es. The chosen location can expect lots of help from KDE and KDE España members. KDE España has the <a href="http://kde-espana.es/akademy-es2012/ubicacion.php"}>Official Call for Locations</a>.

Hosting an event such as Akademy-es is an excellent opportunity to get actively involved with the KDE community. It is also effective for promoting KDE and FLOSS locally. If you and your group have been looking for ways to support KDE and FLOSS, there is no better way.

More information about what's involved with hosting Akademy-es can be found in the <a href="http://www.kde-espana.es/akademy-es2011/bilbao_akademy_es_2010.pdf">successful proposal from Akademy-es 2010 in Bilbao</a>.

If you're interested in collaborating with KDE España to organize Akademy-es 2012, please <a href="mailto:akademy-es-org@kde-espana.es">send us a proposal</a> by the deadline, 15 February 2012. It should include:
<ul>
<li>Contact information</li>
<li>City or town</li>
<li>How Akademy-es 2012 would benefit the local community, KDE or FLOSS group</li>
<li>Conference location, which should have capacity for at least 75 people, and Internet and easy access</li>
<li>Information about the organizing group and any collaborating institutions, including a description of the group and key team members</li>
<li>Affordable housing alternatives, and where they are located relative to Akademy-es 2012</li>
<li>Transportation from other cities and regions (by plane, train, or other options)</li>
<li>Financing and sponsoring possibilities. We prefer to have local funding participation, but this is not a requirement for your proposal to be accepted.</li>
<li>Possible Akademy-es 2012 dates. Usually we celebrate it during the months of April, May and June. We won't reject other proposals. Akademy-es happens over a weekend, between Friday afternoon and Sunday afternoon.</li>
<li>Any other information that you think is important to be considered.</li>
</ul>
Please <a href="mailto:akademy-es-org@kde-espana.es">contact KDE España</a> if you have any questions or want more information.
