---
title: "KDE: Rely on Qt, protect Qt's freedom, contribute to it"
date:    2012-07-09
authors:
  - "jospoortvliet"
slug:    kde-rely-qt-protect-qts-freedom-contribute-it
comments:
  - subject: "Qt Commercial Charts"
    date: 2012-07-09
    body: "How does digia's Qt Commercial Charts fit into all of this?"
    author: ""
  - subject: "Qt 5.0"
    date: 2012-07-09
    body: "\"If the agreement of the KDE Free Qt Foundation is not fullfilled, KDE will receive the last released version of Qt under the BSD license.\"\r\n\r\nI'll rest easy when Qt 5.0 is released. ;)"
    author: ""
  - subject: "Digia currently doesn't own"
    date: 2012-07-09
    body: "Digia currently doesn't own Qt, they only have the right to license the commercial version to others (and build on it). So, the answer would be - right now, they don't.\r\n\r\nOf course, as a partner in the ecosystem (and supporting KDE!) they matter to us. And they (can) play a major role in the future of Qt. But so do KDAB, BasysKom and many other companies.\r\n\r\nTheir Qt Commercial Charts is a separate product (note that KDAB also has a similar product out there). It has little to do with any of this...\r\n\r\nNote that I don't speak for KDE e.V. (at least, not in this comment), these are my personal thoughts on this..."
    author: "jospoortvliet"
  - subject: "CLA"
    date: 2012-07-09
    body: "As long as contributors have to sign a CLA to effectively hand over \"do whatever you want with it\" rights to Nokia, I won't contribute even a translation to a single line.\r\nIf Nokia feels that building closed source versions is a must, all of Qt should be released under e.g. Apache License to everybody."
    author: ""
  - subject: "Technically true"
    date: 2012-07-09
    body: "Qt Commercial Charts is technically a separate component. However, considering that everyone who gets a commercial Qt license from digia automatically gets commercial charts for free, and there is apparently no way to get commercial charts without getting a commercial Qt license from digia, I don't think it is a stretch to say that digia's commercial Qt version offers features unavailable to the open-source version."
    author: ""
  - subject: "Formal Agreements ?"
    date: 2012-07-09
    body: "Formal agreements for businesses are called 'Contracts'. \r\n\r\nContracts are typically include a series of obligations that both parties must meet specified requirements for the specified terms of the agreement. If one or the other party wants to abandon the agreement then they must face some penalty or buy themselves out of the contract. \r\n\r\nThere are fun details besides that (such as agreeing to  abide by the decision of a specified neutral adjudicator in case of disagreement stemming from the contract), but that is what makes a formal agreement with a business.\r\n\r\nSo the KDE folks have a contract with Nokia? \r\n\r\nIf not.. it's not really a formal agreement at all. "
    author: ""
  - subject: "Please read the linked page"
    date: 2012-07-09
    body: "Please read the linked page in the article about <a href=\"http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php\">The KDE Free Qt Foundation</a>"
    author: "sealne"
  - subject: "scan of contract"
    date: 2012-07-09
    body: "http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php#update_2009"
    author: ""
  - subject: "Some details"
    date: 2012-07-09
    body: "The agreement between KDE and Nokia ensures that no part of Qt is moved from the Free version to a commercial-version-only model. It does not apply to new, independently written code. Digia can write new software and give it to their commercial customers, but they can neither un-free existing code nor distribute an altered version of Free Qt code without releasing it under the Free licenses. "
    author: ""
  - subject: "A new trolltech"
    date: 2012-07-10
    body: "We need a new Trolltech. Maybe KDAB, basyskom and the others together found a new company and take over Qt."
    author: ""
  - subject: "Rest easy..."
    date: 2012-07-10
    body: "My understanding is that the current legal advice is that the Alpha and Beta releases do count as releases in terms of the Free Qt agreement. There is also no indication from within Nokia that they wish to block or close the release of 5.0 either, they know that would immediately prompt a fork based on the last version in git.  Instead the do genuinely seem to be giving Qt the time to get 5.0 out the door before making any moves around the Qt team's future."
    author: "odysseus"
  - subject: "Needed for Free Qt"
    date: 2012-07-10
    body: "Please remember that the CLA is also needed for the Free Qt Foundation to be able to release all contributions under the BSD, without the CLA the Free Qt agreement would be impossible.\r\n\r\nOn a personal note, it's a compromise I'm happy to make.  I'd prefer it's not there, but I see it as a fair trade-off for receiving all the work Nokia puts in, not just in code terms but in QA and infrastructure as well.\r\n\r\nJohn."
    author: "odysseus"
  - subject: "Yup. Moreover, there simply"
    date: 2012-07-10
    body: "Yup. Moreover, there simply are quite a few customers who simply wouldn't use Qt if there was no commercial license. I would prefer to have them use it than not - esp if it means forking over money to whoever develops Qt :D"
    author: "jospoortvliet"
  - subject: "My entirely personal take on"
    date: 2012-07-10
    body: "My entirely personal take on this:\r\n\r\nQt comes with an ecosystem of many companies and stakeholders. It's not a closed product a buyer can take any direction they want without loosing the customers and hence their business.\r\n\r\nWith Open Governance, more companies are now involved in Qt development. Whoever takes over Qt from Nokia should keep in mind that yes, there is a serious chance that the other parties in the ecosystem can decide to set up a foundation and take (a fork of) Qt their own way if the new owners don't play nice.\r\n\r\nAnd these owners will found out, if they don't realize that already, that such a fork (supported by a foundation etc, without their involvement) would basically kill their investment entirely.\r\n\r\nBasically, wherever the core Qt people end up will be the direction the business goes, too. And that's not necessarily the company that buys Qt from Nokia, as not all of them actually work at Qt, nor are they unable to switch jobs if they like.\r\n\r\nOf course, as KDE e.V. states in the article, a schism is NOT in the interest of the Qt ecosystem and a less-than-perfect new owner is preferred over a fork. But if the new owner is not willing to listen to their customers and partners, such a thing is surely on the table, either with or without help from the KDE-Free-Qt Foundation. I know that the customers at least prefer a independent place for Qt - a foundation or such. I would suggest to a buyer that setting up a foundation or otherwise making sure the customers have reasons to feel safe with their investments in Qt is a VERY good idea."
    author: "jospoortvliet"
  - subject: "giving and taking"
    date: 2012-07-11
    body: "@Anonymous\r\n\r\nThat seems a silly stance to take.\r\n\r\nFor BSD-licensed projects, a contributor *also* pretty much says \"do whatever you want with it\" - except he says is to *everybody* out there (most of whom won't EVER contribute anything back), wheres in the case of the Qt CLA, a contributor only gives those rights to the organization who is responsible for ~90% of all contributions to the project.\r\n\r\nYou really can't claim that your contribution of the proverbial \"single line\" (or indeed any contribution that you as an individual could reasonably make) would unfairly tilt the \"giving and taking\" balance between Nokia and you to your disadvantage."
    author: ""
  - subject: "Qt Foundation?"
    date: 2012-07-13
    body: "Any news about the Qt Foundation?\r\n\r\nThere was a session about this at Qt Contributor Summit, and Mirko was blogging a bit, but I did not hear any further steps."
    author: ""
  - subject: "Commercial and Open-source the same"
    date: 2012-07-16
    body: "I understand that.  The issue I was pointing to was not that they were violating their agreement, but rather this sentence from the OP:\r\n\r\n\"KDE will work actively to make sure that <strong>the Free Software and commercial versions of Qt remain identical</strong> and continue innovating, by this reducing the incentive to fork.\"\r\n(emphasis added)\r\n\r\nTechnicalities aside, when Digia bundles exclusive, closed-source content with their commercial version, I don't think it is fair to say, in a practical sense, that the commercial and free-software versions are the same anymore.  Now the commercial users automatically get Qt components that are not available to those using the free software version.  That seems to me to be a significant difference between the two versions.  It may still be allowed by the agreement, but I would say the stated goal of keeping the two versions identical is not being met.  \r\n\r\nYou are free to disagree on this, of course, and it is not my decision. But please at least recognize how people could see it this way."
    author: ""
  - subject: "What about the huge patch delta carried by Qt Commercial?"
    date: 2012-07-16
    body: "AFAIK Qt Commercial still carries a large patch delta consisting of bug fixes and such. Won't these mean that the FOSS version of Qt can't  be as rock solid as the commercial one until the entire patch delta is applied to the FOSS version as well?\r\n\r\nAlso, what about Digia's possible plans to add a few commercial-only features to Qt Creator and such?"
    author: ""
  - subject: "Re: Rest easy..."
    date: 2012-08-04
    body: "> before making any moves around the Qt team's future\r\n\r\nhttp://www.smh.com.au/it-pro/business-it/nokia-closes-australian-development-office-20120803-23kc4.html"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE QT.jpg" /></div>

The KDE community is one of the largest and most influential Free Software communities world-wide with thousands of volunteer contributors and countless users. Most of the software written by KDE is based on the Qt toolkit. With the recent strategy changes within Nokia—the largest contributor to Qt, there is uncertainty about the future of Qt that concerns KDE. This is the position of the KDE community regarding the future of Qt: 
<ul><li>KDE will continue to <strong>rely</strong> on Qt <strong>and cooperate</strong> with the Qt copyright owners and contributors.</li>
<li>Using the strong ties between both communities and existing formal agreements, KDE will <strong>protect the freedom</strong> of Qt and KDE where necessary.</li>
<li>Continuing the KDE Frameworks 5 development process that has already begun, KDE will help <strong>improve Qt and contribute</strong> to it.</li></ul>
<!--break-->
<h2>Building on top of Qt</h2>

KDE software is built using Qt, and will continue to be so. Qt is the best UI development toolkit available, and its quality and continuous innovation have helped tremendously in making KDE successful. The KDE community has an interest in Qt continuing to innovate and striving to get better. KDE's interest is mainly with the Free Software version of Qt, which we see centered around the Qt Project under Open Governance. We encourage commercial contributions to Qt, although KDE will remain neutral with the commercial Qt partner ecosystem.

<h2>Protecting Qt and KDE</h2>

With the interdependence between the Qt and KDE communities come risks to the future of the both products. Since Qt is licensed under the GPL and the LGPL, and the Qt Project is under Open Governance, we consider the Free Software version of Qt to be safe for the future. The biggest threat to the future of Qt is fragmentation due to forking. Another risk, a growing difference between the Free Software and commercial versions, has already been anticipated and addressed in existing formal agreements between KDE and Nokia. KDE will work actively to make sure that the Free Software and commercial versions of Qt remain identical and continue innovating, by this reducing the incentive to fork.

Under the terms of the <a href="http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a>, an agreement between Nokia and KDE, Nokia is obliged to continue the development of Qt and to release it as Free Software. This obligation will pass on to another entity should Nokia decide to discontinue its Qt activities and sell its assets. If the agreement of the KDE Free Qt Foundation is not fullfilled, KDE will receive the last released version of Qt under the BSD license. KDE has a firm position that the terms of the KDE Free Qt Foundation are binding.

<h2>Contributing to and extending Qt</h2>

As part of the ongoing development of a future KDE Frameworks 5, many modules of kdelibs are being re-written to be complementary extensions of modules existing in Qt. The technical gap between the two projects will be closing, and users of Qt will often be users of KDE Frameworks as well, and vice versa. Many KDE contributors also work on Qt development. Many of the Qt contributors, maintainers and approvers are also active within KDE. KDE plans to continue and increase its contributions to Qt, and to keep a close relationship between the projects. For this effort to be continuous and stable in the long term, KDE favors further opening up development of Qt and enabling even more collaboration and cooperation between commercial and volunteer Qt contributors.

<h2>Summary</h2>

Qt and KDE share many common interests, and to a large part depend on each other. The KDE community will work to ensure a bright future for Qt and KDE, and extends an open hand to Nokia to collaborate on that.