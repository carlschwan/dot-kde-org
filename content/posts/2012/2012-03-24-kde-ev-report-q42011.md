---
title: "KDE e.V. Report for Q42011"
date:    2012-03-24
authors:
  - "kallecarl"
slug:    kde-ev-report-q42011
comments:
  - subject: "Lilian"
    date: 2012-03-24
    body: "\"What started almost as a joke - recording a video of a new tiny feature\", I have an impression that I hear a reference to Gnome :)"
    author: ""
  - subject: "Wonderful reading"
    date: 2012-03-25
    body: "I always enjoy reading these reports. I always already know some of what is in there from Planet KDE, but it is always great to read such summaries and see what happened in the past quarter. Also, I am very fond of the fact that KDE EV is very transparent to its members and to all interested in KDE. A big thank you to all the contributors to the report. It is impossible not to notice that the reports are getting better every time."
    author: ""
  - subject: "Thanks"
    date: 2012-03-28
    body: "Excellent report, thanks for taking the time to compile and publish. Gives a very nice overview of the important development and community work over the last few months."
    author: ""
---
The report of KDE e.V. for the fourth quarter of 2011 has been published. It gives an overview of the activities KDE e.V. supported in the last 3 months of last year. The report has a message from the President of the Board reflecting on the first 15 years of KDE and looking forward to more opportunities. There are stories about exciting product announcements and the various sprints that produced exceptional results. 

<a href="http://ev.kde.org/reports/ev-quarterly-2011_Q4.pdf">Read the full report (PDF).</a>

As the report mentions several times, the work of KDE e.V. would not be possible without our supporters. If <em>you</em> want to support KDE's work, please <a href="http://jointhegame.kde.org/">Join the Game</a>!