---
title: "Akademy 2012 - papers, registration, travel"
date:    2012-03-07
authors:
  - "kallecarl"
slug:    akademy-2012-papers-registration-travel
---
<div style="float: right; padding: 1ex; margin: 0ex 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/HeaderScreenShot.png" alt="Akademy 2012 - Tallinn" width="300" /></div>

Akademy—the annual world summit of the KDE Community—will take place from 30 June to 6 July in Tallinn, Estonia. 10 years of Akademy; 15 years of KDE!

<a href="http://akademy2012.kde.org/how-to-register">Registration</a> for the conference is open. The <strong><a href="http://akademy2012.kde.org/cfp">Call for Papers</a> closes 15 March 2012—just a few days from now</strong>. More information is available on the <a href="http://akademy2012.kde.org">Akademy website</a>.

We encourage people to submit presentation proposals that demonstrate KDE innovation, accomplishment, partnership and community. This is an opportunity to share your passion and commitment, possibly inspiring people to join your project or launch their own.

We are asking for presentation proposals on the following topics:
<ul>
<li>Maturity; Innovation</li>
<li>The Qt Project, especially topics featuring Qt collaboration</li>
<li>Other partnerships</li>
<li>KDE Frameworks</li>
<li>Beautiful, fast, useful applications</li>
<li>Plasma Workspaces breakthroughs</li>
<li>Expanding KDE's reach through accessibility, promotion, translation and localization</li>
<li>Other topics that are relevant to KDE and the Qt ecosystems</li>
</ul>

<h2>About Akademy</h2>
Akademy is the place to learn about the latest features and share ideas for future releases of KDE's software. The conference will bring together hundreds of KDE contributors, users and industry partners. Participants gain in-depth technical insights, meet and work with KDE and Qt experts, and take part in deciding the future direction of KDE.

If you are planning to attend, please plan your travel soon. Tallinn is a cultural center and a popular summer travel destination. Hotel recommendations and more travel information are available <a href="http://akademy2012.kde.org/hotel-travel">on the Akademy website</a>.

Under some conditions, the KDE e.V. may reimburse travel costs. Read more about reimbursement on the <a href="http://ev.kde.org/rules/reimbursement_policy.php">KDE e.V. website</a>.