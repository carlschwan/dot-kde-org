---
title: "KDevelop 4.3 released with basic C++11"
date:    2012-03-20
authors:
  - "Milian Wolff"
slug:    kdevelop-43-released-basic-c11
comments:
  - subject: "Congratulations"
    date: 2012-03-20
    body: "Wonderful news and great work!"
    author: "Boudewijn Rempt"
  - subject: "KDevelop impressions"
    date: 2012-03-20
    body: "I tried it when I first tried KDE and I didn't understand it, found it too complicated, I was used to \"Create new project\" and \"Run\"... But now I took a little bit of time to get it started and I love it. Very good for big projects and feature rich."
    author: ""
  - subject: "wow dats so so wonderful.."
    date: 2012-03-22
    body: "wow dats so so wonderful.."
    author: ""
  - subject: "Version numbering"
    date: 2012-03-23
    body: "If this newly released stable version is 4.3.0, why is the version I have installed (via openSUSE's KDE Factory Playground repository) numbered 4.3.6 in the about dialog? "
    author: "jadrian"
---
<p>After about nine months of extensive development, the KDevelop team is happy to announce <a href="http://kdevelop.org/kdevelop/kdevelop-430-final-released-basic-c11-support">the immediate availability of KDevelop 4.3</a>. As usual, this feature release comes packed with new features, bug fixes and improved performance.</p>
<!--break-->
<p>Here are some statistics about <a href="http://kdevelop.org/">KDevelop</a> to entice you to upgrade as soon as possible - of course something we recommend everyone should do!</p>

<table style="width:100%">
<tr>
<th>Package</th>
<th><center>Commits since 4.2.3</center></th>
<th>Diffstat</th>
</tr>
<tr>
<th>kdevplatform</th>
<td><center>766</center></td>
<td>615 files changed, 22194 insertions(+), 8377 deletions(-)</td>
</tr>
<tr>
<th>kdevelop</th>
<td><center>542</center></td>
<td>376 files changed, 29167 insertions(+), 15793 deletions(-)</td>
</tr>
<tr>
<th>kdev-php</th>
<td><center>39</center></td>
<td>33 files changed, 565 insertions(+), 381 deletions(-)</td>
</tr>
</table>

<h2>New Features and Other Notable Changes</h2>

<p>This new release has seen lots of work from various contributors. A few noteworthy items are highlighted in the following but this list is far from complete. Try out the new release and see for yourself!</p>

<h3>Basic C++11 Support</h3>

<p>The new C++ standard, which was released last year, is now partially suppported in KDevelop. At least the parser should not trip over new language features such as initializer lists, lambdas, range-based for loops or variadic templates. Similarily, explicitly defaulted or deleted methods, auto, rvalue-references and many more features are supported. Many of the new <code>stdlib</code> classes can be used as well. Keep in mind though that the new C++ standard contains lots of new features, and we are still lacking support for quite a few things. But we will continue to improve this in the next releases, and provide you with a rock-solid C++11 coding environment.</p>

<h3>Editor Restoration</h3>

<p>With KDevelop 4.3 we finally catch up with Kate when it comes to closing and reopening files: folded code regions, bookmarks etc. are now properly restored for the last 20 opened files.</p>

<h3>Enhanced VCS Integration</h3>

<p>Some work went into improving the Version Control System (VCS) integration too, mostly on two fronts—creating a <a href="http://kdevelop.org/screenshots/vcs-changes-view">VCS Changes tool view</a> and improving the Review Mode.</p>

<p>VCS Changes show you what files have changed in your project since the last commit. It is useful to keep track of what you are working on and to decide if there should be a commit.</p>

<p>We improved the Review mode by making it more responsive to the user by updating the view while the user is working on the changes.</p>

<h3>KDE Projects Integration</h3>

<p>The KDE Projects infrastructure was adapted to support <a href="http://projects.kde.org">projects.kde.org</a>. This provides you with a list of all KDE projects together with the ability to retrieve them in order to start contributing to KDE as fast as possible.</p>

<h3>Improved Konsole Integration</h3>

<p>The embedded Konsole in KDevelop has seen some improvements—when you use Bash, it is now possible to control the surrounding KDevelop session, i.e. to open files, create new ones, search through files and more. Type <code>help!</code> to find out what you can do there now.</p>

<h3>Source Formatting</h3>

<p>Integrated source formatting got a bit better—you can now let it override the indentation settings of the embedded editor. Furthermore the "Custom Script Formatter", formerly known to support Gnu Indent, was extended to make it even easier to support custom formatting scripts. One example is the new <code>kdev_format_source.sh</code> script, shipped with KDevelop, which allows fine-grained formatting rules by placing format_sources meta-files into the project's file system. Especially paired with the powerful uncrustify formatter, this enables you to work seamlessly on big, heterogeneously formatted projects.</p>

<h3>Various Bugfixes</h3>

<p>We fixed <a href="https://bugs.kde.org/buglist.cgi?list_id=7184&resolution=FIXED&chfieldto=Now&chfield=resolution&query_format=advanced&chfieldfrom=2011-06-25&chfieldvalue=FIXED&bug_status=RESOLVED&product=kdevelop&product=kdevplatform">over 170 bugs</a> since KDevelop 4.2.3. Among others, SVN 1.7 is now properly supported, various pieces of C++ code got improved, the GDB plugin got better and lots of crashes and other issues were resolved.</p>

<h3>Optimizations</h3>

<p>Besides adding new features and improving stability, this release also comes with some noteworthy performance improvements—opening large projects with tons of files should be considerably faster now. Similarily, Quickopen is now even faster, and searching with it is more fluid when dealing with large projects.</p>

<h3>Forum</h3>

<p>Our new <a href="http://forum.kde.org/viewforum.php?f=218">forum</a> is the place to go if you need KDevelop support. Our <a href="http://kdevelop.org/mailinglists">mailing lists</a> as well as the #kdevelop IRC channel on freenode are also available, and we are happy to take your questions.</p>

<h3>Wrapup</h3>

<p>A big thank you to all the contributors who made this release possible. Many thanks to our users as well, for reporting bugs and making it such a fun and rewarding task to work on KDevelop!</p>