---
title: "KDE Ships December Updates to Plasma Workspaces, Applications and Platform "
date:    2012-12-05
authors:
  - "sebas"
slug:    kde-ships-december-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "<3 your writing style! :-)"
    date: 2012-12-05
    body: "See subject."
    author: "Anonymous"
  - subject: "known bugs list?"
    date: 2012-12-06
    body: "Is there a list of known bugs and regressions?\r\n\r\nThere is a regression since 4.9.3:  Bug 311214 (clicking on an opendocument file opens it as zip-file.)\r\n\r\nIt's already been fixed in source but it needs to be released."
    author: "Pascal d'Hermilly"
  - subject: "Apparently they fixed the Folder Refresh Bug!"
    date: 2012-12-06
    body: "From the list of bugfixes it looks like they have finally tackled the very long-standing bug where Dolphin/Plasma folder widgets would not show updates to files in a directory unless manually refreshed... YAY!  This bug has been reported many times and I'm going to test it out to see if they have finally squashed it for good."
    author: "DonCornelius"
  - subject: "bugzilla or your distro can help you..."
    date: 2012-12-07
    body: "As far as I know there's nothing else than a smart bugzilla query you can try. Meanwhile, you can try to ping your distro team to include the fix in their packages. It seems the openSUSE packages already have it as I can't reproduce this on 4.9.4 here."
    author: "jospoortvliet"
  - subject: "Let's see ;-)"
    date: 2012-12-07
    body: "Aw, that's cool. Hope it works with Nepomuk: I have a 'recent files' folderview on my desktop but after booting it shows that Nepomuk is not running - just a timing issue and a refresh solves it. I'll check it later today :D"
    author: "jospoortvliet"
  - subject: "Perhaps a wiki in future"
    date: 2012-12-07
    body: "Could be nice to have a wiki page for these kind of releases, so we can stand on each others shoulders.\r\nKubuntu has applied the patch as well now."
    author: "Pascal"
  - subject: "High Memory Usage"
    date: 2012-12-08
    body: "In my system (Kubuntu 12.04) \"plasma-desktop\" is taking >90 MB after the update. I would be glad to know the appropriate forum to discuss it."
    author: "Rajendra"
  - subject: "Forums"
    date: 2012-12-08
    body: "http://forum.kde.org/ > KDE Software > Workspace\r\nor \r\nhttp://www.kubuntuforums.net/content.php"
    author: "kallecarl"
---
Today KDE <a href="http://kde.org/announcements/announce-4.9.4.php">released</a> updates for its Workspaces, Applications, and Development Platform.
These updates are the last in a series of monthly stabilization updates to the 4.9 series. 4.9.4 updates bring many bugfixes and translation updates on top of the latest edition in the 4.9 series and are recommended updates for everyone running 4.9.3 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone.
<br /><br />
The list of 71 recorded bugfixes include improvements in the Dolphin file manager and Kontact email and groupware client. KDE's development platform has received a number of updates which affect multiple applications.
The changes are listed on <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.9.4&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">KDE's issue tracker</a>. For a detailed list of changes that went into 4.9.4, you can browse the Subversion and Git logs. 4.9.4 also ships a more complete set of translations for many of the 55+ supported languages.
To  download source code or packages to install go to the <a href="http://www.kde.org/info/4.9.4.php">4.9.4 Info Page</a>. If you would like to find out more about the KDE Workspaces and Applications 4.9, please refer to the <a href="http://www.kde.org/announcements/4.9/">4.9 release notes</a> and its earlier versions.