---
title: "KDE Development \u2013 A Beginner's Guide"
date:    2012-02-05
authors:
  - "rohangarg"
slug:    kde-development-–-beginners-guide
comments:
  - subject: "OK Where is it?"
    date: 2012-09-14
    body: "I need a beginner's guide. I have been searching for a week. How about a link? Or do I have to wait for the dead tree version to ship to me?"
    author: "Leo in NJ"
  - subject: "Read online"
    date: 2012-09-14
    body: "Fourth paragraph from the bottom...\r\n<blockquote>What's more, you can buy the book in print! Yes, you read that right, buy a physical copy of the book in dead tree format. Download the PDF or ePub file or read it online at Flossmanuals.</blockquote>\r\n\r\nClick on the Flossmanuals link."
    author: "kallecarl"
---
During a recent 5 day sprint, four KDE contributors planned and produced a handbook for beginning KDE developers. We had assistance from several generous organizations, worked hard, and learned a lot.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/KDEDevGuide.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEDevGuide350.jpg" /></a><br />Beginning KDE Development (click for larger)</div>

The project started when Google offered KDE an amazing opportunity to write and publish a book in less than a week. One of our young developers developed a proposal and gathered a team. Google accepted the proposal. With that we were off on an adventure and some intense, hard work.

Last October, our team of four enthusiastic KDE contributors gathered at the Google offices in Mountain View, CA to write a guide on how to get started with KDE development. In five days!? Fortunately we had guidance from Adam Hyde from <a href="http://www.flossmanuals.net/">FLOSS Manuals</a>, and inspiration from Gunner of <a href="http://www.aspirationtech.org/"">Aspiration: Better Tools for a Better World</a>.

After planning and organizing for a day, there were 3 days of hard work: generating content, editing, editing and more editing—along with major assistance from Andy Oram of <a href="http://oreilly.com/">O'Reilly Media</a> (thanks Andy!), we finalized the <em>Beginner's Guide to KDE Development</em>. We then recapped, discussed Lessons Learned, and published, both online and printed by <a href="http://www.lulu.com/">Lulu</a>.

The guide is recommended for every new contributor to KDE development. It outlines technical aspects of contributing to KDE and is a valuable first point of contact for new developers. The guide offers insights into KDE from the developer's point of view, and explains how to check out existing code, modify it and submit patches.

Currently the guide only focuses on the coding aspects of KDE. Contributors are welcome (encouraged) to expand the guide to cover other aspects of the KDE Community as well as enhance the existing content in the book. We are currently working on how to release subsequent versions.

The book can be edited on the FLOSS Manuals site by creating an account. The "All Manuals" menu under the "Write" tab has a listing for "KDE-Guide". Edits are performed in real time and reflected on the FLOSS Manuals site as soon as the chapter is saved. 

What's more, you can buy the book in print! Yes, you read that right, buy a <a href="http://www.lulu.com/product/paperback/beginning-kde-development/18849700">physical copy of the book</a> in dead tree format. Download the PDF or ePub file or read it online at <a href="http://flossmanuals.net/kde-guide/">Flossmanuals</a>.



The team found that the entire experience was absolutely wonderful. We now realize how difficult it is to write good documentation. So our hats are off to the hundreds of people contributing to KDE Techbase, Userbase and writing relevant and up-to-date documentation.

Special thanks to Valorie Zimmerman for major help putting together this story and the Development Guide.

Comments are moderated based on <a href="http://kde.org/code-of-conduct/">the KDE Code of Conduct</a>. There will be a delay before they are published.