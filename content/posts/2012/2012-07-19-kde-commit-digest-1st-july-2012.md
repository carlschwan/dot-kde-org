---
title: "KDE Commit-Digest for 1st July 2012"
date:    2012-07-19
authors:
  - "mrybczyn"
slug:    kde-commit-digest-1st-july-2012
---
In <a href="http://commit-digest.org/issues/2012-07-01/">this week's KDE Commit-Digest</a>:

<ul><li>RDF support is back in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li>Compilation manager for Sphinx added in <a href="http://simon-listens.org">Simon</a></li>
<li>Conquirere ported to Nepomuk-Core</li>
<li>Work on group import in <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>Support for network zones added in <a href="http://userbase.kde.org/NetworkManagement">Network Management</a></li>
<li>Work on a new dialout application in KDE Telepathy</li>
<li>General Transit Feed Specification (GTFS) support added to Public Transport</li>
<li>Work on rendering in <a href="http://userbase.kde.org/Gluon">Gluon</a></li>
<li>GSoC work in <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://edu.kde.org/kstars/">KStars</a>, Calligra, <a href="http://edu.kde.org/cantor/">Cantor</a>, <a href="http://edu.kde.org/marble/">Marble</a></li>
<li>Bugs statistics: 336 opened, 332 closed.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-07-01/">Read the rest of the Digest here</a>.
<!--break-->
