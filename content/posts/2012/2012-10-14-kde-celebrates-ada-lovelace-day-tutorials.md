---
title: "KDE celebrates Ada Lovelace Day with tutorials"
date:    2012-10-14
authors:
  - "kallecarl"
slug:    kde-celebrates-ada-lovelace-day-tutorials
comments:
  - subject: "It's funny reading through"
    date: 2012-10-16
    body: "It's funny reading through those sixteen-years-old e-mail threads. I almost wish I could go back in time and tell those guys complaining that we shouldn't be designing yet another file manager and tell them that sixteen years later, in the year 2012, people will still be redesigning file managers. In the future, some groups are actually <em>removing</em> features from their file managers!"
    author: "Chris Kahn"
---
Today, KDE celebrated its 16th birthday. On October 14, 1996, Matthias Ettrich <a href="https://groups.google.com/forum/?fromgroups=#!msg/de.comp.os.linux.misc/SDbiV3Iat_s/zv_D_2ctS8sJ">started KDE</a>. Since then, amazing women have helped make KDE what it is today. Women like Anne-Marie Mahfouf, Eva Brucherseifer, Alexandra Leisse, Celeste Lyn Paul, Anne Wilson, Claire Lotion, Lydia Pintscher, Myriam Schweingruber, Claudia Rauch and many many more. Women have shaped both KDE code and KDE community.

This Tuesday (October 16th) there is an opportunity to attract more women contributors to technology in general and KDE in particular. Tuesday is <a href="http://en.wikipedia.org/wiki/Ada_Lovelace">Ada Lovelace</a> Day; we are using the occasion to invite women to discover KDE. We want to help another generation of women be part of KDE. The recently announced <a href="http://manifesto.kde.org/">KDE Manifesto</a> distinguishes what “being KDE” means. Those values and purpose—along with the wide range of tasks within KDE—offer an unparalleled possibility for anyone to be included in the field of technology.

KDE’s celebration of <a href="http://findingada.com/">Ada Lovelace Day</a> is an event for women and their friends. In other words, it is intended primarily for women and men are welcome.

During the day, we will offer tutorials on different topics to give you a running start. The only prerequisites are an interest in technology and a willingness to learn. The tutorials will include subjects such as:
<ul>
<li>how to help with bug triage to help improve quality</li>
<li>how to create great documents with <ahref="http://kile.sourceforge.net/">Kile</a> and <a href="http://www.latex-project.org/intro.html">LaTeX</a></li>
<li>how to write your first <a href="http://techbase.kde.org/Projects/Plasma/Plasmoids">Plasmoid</a> with <a href="http://doc.qt.digia.com/4.7-snapshot/qdeclarativeintroduction.html">QML</a> (a programming language that is simple to learn, but also useful for developing complex software)</li>
</ul>
The day will close with a session for questions about getting involved in KDE.

Find out more about the event on <a href="http://community.kde.org/AdaLovelaceDay/2012">the KDE community wiki</a>. 

We are looking forward to seeing you at the events and are excited about helping you make your first steps in KDE.