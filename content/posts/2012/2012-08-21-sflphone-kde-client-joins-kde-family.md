---
title: "SFLPhone KDE client joins KDE family"
date:    2012-08-21
authors:
  - "Emmanuel_Lepage"
slug:    sflphone-kde-client-joins-kde-family
comments:
  - subject: "Download?"
    date: 2012-08-21
    body: "Thanks a lot for the KDE client. But I could not find a download link for that on the site. Only GNOME client is available. "
    author: "kamesh"
  - subject: "Packages broken"
    date: 2012-08-22
    body: "I added the ppa listed at the website but the package is broken on Kubuntu 12.04.\r\n\r\nThe following packages have unmet dependencies:\r\n sflphone-client-kde : Depends: kdelibs5 but it is not going to be installed\r\nE: Unable to correct problems, you have held broken packages.\r\n"
    author: "Blackpaw"
  - subject: "NB. I do have the 4.9"
    date: 2012-08-22
    body: "NB. I do have the 4.9 packages installed from the beta ppa - maybe thats part of the problem."
    author: "Blackpaw"
  - subject: "SFLPhone support"
    date: 2012-08-22
    body: "Might get better support at http://sflphone.org/ through the contact link or mailing list. I don't know whether or not the Savoir-Faire Linux people are monitoring the Dot story."
    author: "kallecarl"
  - subject: "Packages ?"
    date: 2012-08-22
    body: "Hi !\r\nVery interesting. Up to now I was using twinkle that seems to not be developed any more and is a bit outdated. Will you provide packages for various distros ?"
    author: "Anonymous"
  - subject: "Confused"
    date: 2012-08-23
    body: "Hi,\r\n\r\nI am always confused with SIP clients. How can these help in talking to a non-Linux user or someone who is familiar and not-willing-to-try a new piece of software? I am basically talking of Skype. Can I use sflphone to connect to skype \"network\"?\r\n\r\nThanks."
    author: "KenP"
  - subject: "Great!"
    date: 2012-08-23
    body: "As soon as I saw this on The Planet, I ditched the Gnome client and installed the KDE one, which looks a lot more functional. \r\n\r\nI noticed on the Downloads page ( http://sflphone.org/download/stable-release ) there is no mention of the KDE client. Can this be fixed?\r\n\r\n\r\n\r\nI'm new to SIP"
    author: "Michael"
  - subject: "Check out sip2skype"
    date: 2012-08-25
    body: "I think there is something called sip2skype. It is a website bridge that allow Skype calls to be converted to SIP calls. I am not sure it work with SFLPhone, some say it does, some say it does not."
    author: "Emmanuel Lepage Vallee"
  - subject: "Here is the download"
    date: 2012-08-25
    body: "Apologies for the delay. Here is the download. It is also on kde-apps.org\r\n\r\nhttp://download.kde.org/stable/sflphone/1.2.0/src/"
    author: "Emmanuel Lepage Vallee"
  - subject: "Please see above"
    date: 2012-08-25
    body: "Comment title \"Here is the download\" has the correct link."
    author: "kallecarl"
  - subject: "it crashes"
    date: 2012-08-27
    body: "It compiles and all. But when I try and invoke the client, it crashes with the following message.\r\n\r\nkamesh@MatriX:~/Downloads/sflphone-kde/build$ sflphone-client-kde \r\nQObject::startTimer: QTimer can only be used with threads started with QThread\r\n\r\nI am on kubuntu 12.04. \r\n\r\nkamesh@MatriX:~$ uname -a\r\nLinux MatriX 3.2.0-29-generic-pae #46-Ubuntu SMP Fri Jul 27 17:25:43 UTC 2012 i686 i686 i386 GNU/Linux\r\n\r\nkamesh@MatriX:~$ qmake -v\r\nQMake version 2.01a\r\nUsing Qt version 4.8.1 in /usr/lib/i386-linux-gnu"
    author: "Kamesh"
  - subject: "Please send error messages"
    date: 2012-08-27
    body: "Hi,\r\n\r\nThere are probably a few errors after that one. This is just a warning. Many applications have it, like Amarok. Are you sure the SFLPhone daemon is running (sflphoned)? If yes, can you pastebin the complete content of the debug output to emmanuel.lepage at savoirfairelinux.com? Also, if there is a \"segmentation fault\" please include the backtrace."
    author: "Emmanuel Lepage"
---
The SFLPhone team and <a href="http://www.savoirfairelinux.com/en/">Savoir-Faire Linux</a>, a Montreal Open Source consulting company, are pleased to announce the availability of <a href="http://sflphone.org/">SFLPhone 1.2.0</a>, the first version since the KDE client was moved to KDE infrastructure. Our team is proud of joining the KDE family as part of Playground, and looking forward to being part of Extragear soon. SFLPhone KDE and SFLPhone Qt have been in development for the better part of a decade, aiming to provide the KDE environment with a professional software phone app. Recently, we have been working hard to bring the application to the status of KDE first class citizen. Thank you to the Oxygen and l10n community members for showing such an interest in our application and helping us improve it.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/sflphone-1.2_8.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/sflphone-1.2_8_wee.png" /></a><br />SFLPhone in action</div> 

As SFLPhone KDE was until now a little known application, and as everybody loves screenshots, here is a little overview of how it looks and works. With SFLPhone, calls can be made using a SIP or IAX account, as used by most enterprise internal phone networks and well known servers such as Asterisk or FreeSwitch. It is also possible to use it over a land line by installing those tools.

<h2>Features</h2>
SFLPhone has all of the basic and many advanced SIP features, including:
<ul>
<li>The ability to manage multiple calls at once</li>
<li>Making conference calls without any other external tools</li>
<li>Both classic transfer and call-to-call transfer</li>
<li>Multiple account configuration</li>
<li>Integration with KDE PIM (Akonadi) Address Book</li>
<li>Support for different devices for ringing and making calls</li>
<li>Various secure calling features</li>
<li>History, voice mail and call recording</li>
<li>Some video support</li>
</ul>

Ease of use has been high on the agenda, with the application supporting various types of drag'n'drop and an interface designed to scale very well from phone-like <a href="http://en.wikipedia.org/wiki/Skeuomorph">skeuomorphism</a> to an advanced multi-column, call-center style look.

<a href="http://dot.kde.org/sites/dot.kde.org/files/sflphone-1.2_2.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/sflphone-1.2_2_wee.png"></a>

We encourage free software users to experiment with the robust <a href="http://www.savoirfairelinux.com/en/community">SFLphone</a> softphone. It's useful for the home and individual use, and can be extended to support high volume enterprise communication needs.