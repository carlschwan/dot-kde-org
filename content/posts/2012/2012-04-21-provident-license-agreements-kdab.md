---
title: "Provident License Agreements for KDAB "
date:    2012-04-21
authors:
  - "kallecarl"
slug:    provident-license-agreements-kdab
comments:
  - subject: "Kalle's code"
    date: 2012-04-24
    body: "There is still code from Kalle in kdelibs (KApplication in particular). I sometimes see that when doing a \"git blame\" on some old code :-)\r\n"
    author: "dfaure"
---
(Matthias) Kalle Dalheimer is the President and Founder of <a href="http://www.kdab.com/">KDAB</a>, and also one of the founding members of the KDE project and <a href="http://ev.kde.org/">KDE e.V.</a> He hasn't personally been very active in KDE lately, but some of the old-timers will remember that he served as President and Treasurer of KDE e.V. for a few years. He also wrote the first C++ class ever used in kdelibs (KConfig) even though it's doubtful that any of the code is still left in today's codebase.

Kalle reports the recent signing of <a href="http://ev.kde.org/rules/fla.php">Fiduciary License Agreements</a> that support code contributions to KDE that ensure the continuity of Free Software.  
<blockquote>
"KDAB is a company that aims to be the world's foremost source for all things <a href="http://en.wikipedia.org/wiki/Qt_(framework)">Qt</a> - consulting, training, and add-on components. We pride ourselves in being "the guys for the hard cases" and are a small team of extremely skilled developers. Many of us have KDE backgrounds, and quite a few are active in various KDE projects today, such as KDE Frameworks 5 and various kdepim modules and applications. KDAB and its partners have been contracted by various customers to supply KDE development services. All of this code has made it back into KDE's codebase, and has helped to make KDE what it is today. In addition, KDAB has sponsored numerous KDE activities throughout the years and is a <a href="http://ev.kde.org/getinvolved/supporting-members.php">Patron of KDE</a>.

"We recently signed Fiduciary License Agreements for all of our about 45 developers. It is important to us that the code we have contributed to KDE remains usable and alive for as long as the KDE project wants to use that code, independent of what becomes of KDAB or its employees. We want to set a good example for other companies that let their employees contribute to KDE during work time, as it is essential for KDE to have a reliable code base that is not tainted by any legal doubts."
</blockquote>

Code is the lifeblood of KDE. But if you are not a software developer, you can contribute in other ways. Consider <a href="http://community.kde.org/Getinvolved">Getting Involved</a> or <a href="http://jointhegame.kde.org/">Joining the Game</a>. 

“We exist temporarily through what we take, but we live forever through what we give.” Douglas M. Lawson