---
title: "Kate/KDevelop October Sprint: What's new in Kate"
date:    2012-11-24
authors:
  - "dhaumann"
slug:    katekdevelop-october-sprint-whats-new-kate
comments:
  - subject: "great, great work"
    date: 2012-11-25
    body: "Ol\u00e0, just installed whole kde-sc from git, helped by gentoo live ebuilds. The improvements are pretty nice, with \"minimap\" and \"projects\" being the ones which make a real difference to me.\r\n\r\nMinimap used as a scrollbar could use some improvement in mouse handling. But it proves to be useful in a fast understanding of where we are working, even more useful in split view.\r\nProjects, it's a great start, and could be the basis for a really powerful project manager in the future. Some seamless integration with git and svn (commit, diff to master, etc) could make it a killer app.\r\n\r\nA regression since 4.8 is that in \"column selection\" mode, the cursor automatically moves to the end of the <strong>current</strong> line, while before it was allowed to stay past the EOL. This make impossible to select multi-line correctly in column mode unless the <strong>last</strong> selected line is the longest of the section (or you don't need to select to the end). Hopefully it's a bug and not a design decision :)\r\n"
    author: "Francesco Riosa"
  - subject: "I HATE the notification system"
    date: 2012-11-25
    body: "When I first saw such a notification in Dolphin (A folder cannot be dropped on itself) I could not believe it!\r\n\r\nBIG BOLD, IN YOUR FACE.\r\nIrritating no, these CAPS? But that's how it is for some users...\r\nNow the computer is continously asking for attention.\r\n\r\nDefinitely NOT how I want my computer to behave, the applications should be out of the way as much as possible, allowing me to focus on my WORK, not satisfying attention-craving applications...\r\n"
    author: "John van Spaandonk"
  - subject: "notifications"
    date: 2012-11-26
    body: "First of all, thanks for your feedback. The idea behind the notification system is to have a unified way to show notifications that require the user's attention: data recovery, lines were wrapped on load and the document is read-only, encoding problems, delayed loading for remote files, ...\r\n\r\nThis is valuable information, and, in fact, we had quite some complaints that the feed-back for e.g. the read-only mode was simply not there. For most of the other cases, there were modal popup windows, which is even more \"in the face\" than the notifications.\r\n\r\nWhat I wonder is whether you at least tried the current behavior, since it's been in git master just for a few weeks.\r\n\r\nBesides that, it would be cool to have constructive feedback: if you know a way to make it better: awesome ;) But then, tell us how. And, even cooler, provide a patch :-)"
    author: "Dominik"
  - subject: "Actually, it's asking for"
    date: 2012-11-27
    body: "Actually, it's asking for less attention.  Most of those messages you see in Dolphin used to be pop-up dialogs that were way worse.  Now they are passive and stay out of your way until you want to deal with them instead of demanding you click on them immediately."
    author: "odysseus"
  - subject: "Notification"
    date: 2012-11-27
    body: "I most certainly like the new notification (limited to the experience I've had with Dolphin).\r\n\r\nIt does get my attention and I've gotten used to it being there. But then again, I have experienced that it did not prevent me from continuing work and therefore too late responding to the error.\r\n\r\nIn critical cases, the view should be locked from further action. But this is difficult because what is a critical error in my case compared to others?\r\n\r\nSo in short, it is really nice, looks good, but does not have the same \"fatal\" consequence as a popup. So if it could stop further interaction it would be perfect (in my opinion)--a button for \"okay, I've seen the message\". Of course, this would not be for information or other non-fatal actions :-)\r\n"
    author: "Jarl E. Gjessing"
  - subject: "Images used in this article"
    date: 2012-12-01
    body: "This is an article about changes in KATE. Why are the images from KWrite?"
    author: "Paul L"
  - subject: "Kate"
    date: 2012-12-04
    body: "Because the changes happened in the Katepart, which is the editor component of not only Kwrite and Kate but also KDevelop, Kile and probably others. \r\n\r\nAlso note that the project plugin is part of Kate. All the rest is built into Katepart thus available for all users of that (you can have the minimap in KDevelop for example)."
    author: "Milian Wolff"
  - subject: "Immensely grateful"
    date: 2012-12-06
    body: "Many thanks for all the time and brains you have spent on doing Kate as good as it is! \r\n\r\nDoing a text editor might not always appear to be the coolest thing in the world of quickly evolving software, but your Kate is silently used by millions! I use Kate every workday at my work at Opera Software, as part of KDevelop, and the text editing works just beautiful! When I have shown colleagues how well it works developing in KDevelop several of them have changed to KDE! :-)"
    author: "Anders Karlsson"
---
After the successful developer sprint in <a href="http://dot.kde.org/2010/03/08/kate-kdevelop-and-okteta-developers-meet-berlin">Berlin in 2010</a>, the Kate and KDevelop teams met for the second time from the 23rd to the 29th of October. This time, the developer sprint was held in <a href="http://en.wikipedia.org/wiki/Vienna">the beautiful city of Vienna</a>. In total, 13 contributors discussed and collaborated on the future of Kate and KDevelop for a whole week.

The developer sprint was organized and partly financed by <a href="http://www.jowenn.net/">Joseph Wenninger</a>, who also showed himself to be an excellent city guide in Vienna. The remaining travel costs were kindly supported by the KDE e.V., which is possible thanks to our <a href="http://ev.kde.org/getinvolved/supporting-members.php">supporting members</a> and the <a href="http://jointhegame.kde.org/">Join The Game campaign</a>. Further, a special thanks goes to <a href="http://www.vivid-planet.com/">Vivid Planet</a> for supporting the joint Kate/KDevelop with a really tasty dinner.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/group_1.jpg" /></a><br />Kate/KDevelop Sprint in Vienna </div>

The developer sprint was very productive, so this sprint report is split into two parts. In this Dot story, information about Kate is presented. KDevelop sprint activities will be provided soon.

<h2> What's new in Kate </h2>
The developers worked hard to make Kate, <a href="http://kate-editor.org">KDE's Advanced Text Editor</a>, an even more pleasant experience. There were about 400 commits in Kate's source code in just this week, ranging from major changes to small details such as bug fixes or fine tuning.

<h3> Notification System </h3>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/replace.png" width="350" /><br />Replace text notification</div>
The editor component Kate Part (also used in KDevelop and Kile) received a <a href="http://kate-editor.org/2012/11/06/passive-notifications-in-kate-part/">new message interface</a>. This message interface allows the applications to show passive, interactive notifications in the editor text view. For instance, this notification system is used to notify the user when <a href="http://kate-editor.org/2012/11/05/loading-remote-files/">loading remote files will take a long time</a>, when the document <a href="http://kate-editor.org/2012/10/25/data-recovery-in-4-10/">content needs to be recovered</a>, or when text is being replaced (see screenshot).

<h3> Minimap as Scrollbar </h3>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/minimap.png" width="350" /><br />Minimap</div>
Kate gained the feature to show a <a href="http://kate-editor.org/2012/11/03/busy-katekdevelop-sprint-results-in-mini-map/">minimap of the document in the scroll bar</a>. Minimap shows text in a miniature view and is useful for fast text navigation in the document (screen shot). While the feature itself is stable, it may be changed and should be considered experimental.

<h3> Project Plugin </h3>
Kate gained a new Project Management plugin. This powerful plugin reads simple .kateproject files in the project root folder. A sidebar shows all the files belonging to the project, either through filters or by querying git or subversion. It is tightly integrated into the Search & Replace plugin, enabling the user to search & replace only in the files belonging to the project. It seamlessly integrates into the new Quick Open feature, accessible either through the menu or the shortcut Ctrl+Alt+o. The Project Management plugin uses ctags in the background to parse all project files. This way, basic yet convenient auto completion support is available in all project files. Get more details in <a href="http://kate-editor.org/2012/11/02/using-the-projects-plugin-in-kate/">this blog post</a>.

<h3> Predefined Color Schemes </h3>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/darkcolors.png" width="350" /><br />Dark Colors predefined color scheme</div>
Another improvement is that Kate now has several predefined color schemes. An example can be seen in the screen shot, and <a href="http://kate-editor.org/2012/11/07/default-color-schemas/">further information is available on the Kate homepage</a>. Colors may change in the future (e.g., to improve contrast).

<h3> Additional Changes </h3>
In addition to these updates, minor changes include a small tooltip while scrolling that shows the <a href="http://kate-editor.org/2012/10/28/show-line-while-scrolling/">current line number</a>. Next, the option to <a href="http://kate-editor.org/2012/10/27/remove-trailing-spaces/">remove trailing spaces on save was improved</a>. Documents in read-only mode now show an additional suffix "[read only]" to make the read-only mode more visible to the user. <a href="http://kate-editor.org/2012/11/06/kate-scripting-updates-zen-like-quick-coding/">The scripting API was improved</a> and contains experimental support for zen-like quick coding also for arbitrary languages.

<h3> Concluding Remarks </h3>
We read almost all of the 850 bug reports and brought the number down to a manageable size of just 60 open bug reports and 300 wishlist items. <a href="http://kate-editor.org/2012/11/09/bug-hunting-please-help-poor-kate/">Assistance with finding and reporting bugs is much appreciated</a>. The buq squashing trend is reflected in the following chart:

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/charts.png" /><br />Bug status</div>

Many thanks to all our supporters who made this productive developer sprint happen. All the functions mentioned above are available in KDE Software Release 4.10. We are looking forward to more dedicated developer sprints in the coming years!