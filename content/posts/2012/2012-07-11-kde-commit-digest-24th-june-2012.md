---
title: "KDE Commit-Digest for 24th June 2012"
date:    2012-07-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-24th-june-2012
---
In <a href="http://commit-digest.org/issues/2012-06-24/">this week's KDE Commit-Digest</a>:

<ul><li>Face detection condition plugin and vision library added to <a href="http://simon-listens.blogspot.fr/">Simon</a></li>
<li>Improved lesson management in <a href="http://edu.kde.org/ktouch/">KTouch</a></li>
<li>ASCII source file access optimization in <a href="http://kst.kde.org/">Kst</a></li>
<!--break-->
<li>Better keyboard navigation in <a href="http://en.opensuse.org/Kickoff">Kickoff</a></li>
<li>New templates and better project management in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>Enable threaded <a href="http://www.python.org/">Python</a> support in <a href="http://kate-editor.org/">Kate's</a> Python plugins</li>
<li>System-wide monitor profile can be overridden by users in <a href="http://www.calligra-suite.org/">Calligra</a></li>
<li>Tabbed view available and cache added in conquirere</li>
<li>In <a href="http://pim.kde.org/akonadi/">Akonadi</a>, support added for Facebook posts and notification collections</li>
<li><a href="http://community.kde.org/Plasma/Plasma_Media_Center">Plasma Media Center</a> has a redesigned home screen</li>
<li>The <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy Nepomuk</a> service ported to Nepomuk2</li>
<li>GSoC work on <a href="http://edu.kde.org/kstars/">KStars</a> and <a href="http://edu.kde.org/cantor/">Cantor</li>
<li>Bugs status: 354 opened and 897 closed.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-06-24/">Read the rest of the Digest here</a>.