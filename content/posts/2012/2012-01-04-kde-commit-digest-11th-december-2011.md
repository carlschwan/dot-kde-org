---
title: "KDE Commit-Digest for 11th December 2011"
date:    2012-01-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-11th-december-2011
---
In <a href="http://commit-digest.org/issues/2011-12-11/">this week's KDE Commit-Digest</a>: 

<ul><li>Better support of wildcards in the directory path field and improved menu key functionality in <a href="http://dolphin.kde.org/">Dolphin</a></li> 
<li>Interface for pictureshape cropping, cache rendering in assistants, numbered bibliography entries, improved DOCX, OOXML support in <a href="http://www.calligra-suite.org/">Calligra</a></li> 
<li>Memorize expanded groups in bookmarks in <a href="http://userbase.kde.org/Skrooge">Skrooge</a></li> 
<li>QWidget-based tooltips in <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy</a></li> 
<li>Sync improvements in <a href="http://userbase.kde.org/Rekonq">rekonq</a></li> 
</ul> 

<a href="http://commit-digest.org/issues/2011-12-11/">Read the rest of the Digest here</a>.