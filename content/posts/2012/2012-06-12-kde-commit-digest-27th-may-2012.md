---
title: "KDE Commit-Digest for 27th May 2012"
date:    2012-06-12
authors:
  - "mrybczyn"
slug:    kde-commit-digest-27th-may-2012
---
In <a href="http://commit-digest.org/issues/2012-05-27/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://okular.org/">Okular</a> saves annotations locally</li>
<li>Delayed search in <a href="http://www.calligra-suite.org/">Calligra</a> allows the users to type the whole word; adding image lists possible; brush rotation is absolute to canvas rotation</li>
<li><a href="http://amarok.kde.org/">Amarok</a> is now capable of transcoding music when copying it to USB Mass Storage devices</li>
<li>Exponential notation supported in <a href="http://kst.kde.org/">Kst</a></li>
<li>Play Previous and Play Next and other updates in miniplayer</li>
<li>Further preparations for drag-and-drop support in the Places Panel in <a href="http://dolphin.kde.org/">Dolphin</a></li>
<li>Refactoring work in <a href="http://www.kdenlive.org/">Kdenlive</a></li>
<li>GSoC work in <a href="http://edu.kde.org/cantor/">Cantor</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-05-27/">Read the rest of the Digest here</a>.