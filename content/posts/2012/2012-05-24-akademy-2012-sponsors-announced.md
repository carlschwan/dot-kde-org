---
title: "Akademy 2012 Sponsors Announced"
date:    2012-05-24
authors:
  - "kallecarl"
slug:    akademy-2012-sponsors-announced
---
Akademy, the KDE community summit, is happening in just a few weeks, from the 30 of June to the 6 of July, in Tallinn, Estonia. The Akademy Organizing Team is pleased to welcome this year's <a href="http://akademy.kde.org/sponsors">Akademy sponsors</a> whose support is critical to the success of the conference.
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/AllLogos.png" /></div>
<h2>Platinum Sponsors</h2>
Akademy 2012 is generously supported by two Platinum sponsors, Blue Systems and Nokia.

<strong>Blue Systems</strong>, based in Germany, invests in Free and Open Source technologies, and sponsors the development of KDE projects and distributions like Kubuntu, Netrunner and Linux MintKDE. Blue Systems is also sponsoring the party Saturday evening.

<strong>Nokia</strong>, a global leader in mobile communication and the main contributor to the Qt Project, has a long history of supporting the KDE community and Akademy.
<h2>Silver Sponsors</h2>
In addition to the Platinum sponsors, the KDE community is happy to welcome the following Silver sponsors:
<ul>
<li><strong>Digia</strong> is the worldwide exclusive commercial licensor of Qt, and shares KDE's commitment to the future of Qt for desktop and embedded development.</li>
<li><strong>Google</strong> is a major user of open source software and supports KDE's software development with the annual Summer of Code and Code-in Contest.</li>
<li><strong>Intel</strong> is a leader in the open source ecosystem, providing a range of solutions that is deep and broad at all levels of the solution stack. Intel is also sponsoring the Welcome Reception on Friday.</li>
</ul>
<h2>Other Supporters</h2>
The Akademy Bronze sponsors are <strong>Collabora</strong>, a consultancy with a long history of open source software development, and <strong>Zabbix</strong> who offers a popular enterprise-class open source monitoring solution.

The event is also supported by <strong>Froglogic</strong> and <strong>Red Flag Software</strong>.

Once again, our media partner for Akademy 2012 is Linux Magazine.

The organizing team thanks our sponsors on behalf of the KDE Community. There are still sponsorship opportunities. If you are interested, please visit <a href="http://akademy.kde.org/sponsoring">Sponsoring Akademy 2012</a> for more information, including sponsor benefits.

<h2>Akademy 2012 Tallin, Estonia</h2>

For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.