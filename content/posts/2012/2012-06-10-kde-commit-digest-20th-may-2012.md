---
title: "KDE Commit-Digest for 20th May 2012"
date:    2012-06-10
authors:
  - "mrybczyn"
slug:    kde-commit-digest-20th-may-2012
---
In <a href="http://commit-digest.org/issues/2012-05-20/">this week's KDE Commit-Digest</a>:

             <ul><li>Basic drag and drop support enabled for the Places Panel and bookmark synchronization in <a href="http://dolphin.kde.org/">Dolphin</a></li>
<li>Multiple improvements for drawing in <a href="http://www.calligra-suite.org/">Calligra</a>; fullscreen mode in <a href="http://www.kexi-project.org/">Kexi</a>; configuration dialog added in <a href="http://www.calligra.org/words/">Words</a></li>
<li>Work on backup/restore in <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>Better lyrics scrolling in <a href="http://amarok.kde.org/">Amarok</a></li>
<li>In QtMobility, Android Bluetooth partial support with socket client and device discovery</li>
<li>Easier access to common tasks in <a href="http://edu.kde.org/marble/">Marble</a>.</li>
</ul>

               <a href="http://commit-digest.org/issues/2012-05-20/">Read the rest of the Digest here</a>.