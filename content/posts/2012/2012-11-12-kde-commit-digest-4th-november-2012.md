---
title: "KDE Commit-Digest for 4th November 2012"
date:    2012-11-12
authors:
  - "mrybczyn"
slug:    kde-commit-digest-4th-november-2012
---
In <a href="http://commit-digest.org/issues/2012-11-04/">this week's KDE Commit-Digest</a>:

<ul>
<li><a href="http://www.kdevelop.org/">KDevelop</a> sees multiple improvements. It is nowpossible to plug custom actions into the 'Open With' from extensions; updates and fixes in patch-review; optimization of icon loading for large projects</li>
<li><a href="http://kate-editor.org/">Kate</a> adds Mini-Map of the document, allows user to override automatic encoding detection</li>
<li><a href="http://www.kdenlive.org/">Kdenlive</a> implements analysis load/save</li>
<li><a href="http://edu.kde.org/ktouch/">KTouch</a> gets user interface changes</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> optimizes contact search queries</li>
<li><a href="http://korganizer.kde.org/">KOrganizer</a> reminders work reliably again for people who do not have KDE running 24/7 (this was broken since the switch to <a href="http://pim.kde.org/akonadi/">Akonadi</a>).</li>
</ul>

<a href="http://commit-digest.org/issues/2012-11-04/">Read the rest of the Digest here</a>.