---
title: "Plasma Active 3 Improves Performance, Brings New Apps "
date:    2012-10-15
authors:
  - "kallecarl"
slug:    plasma-active-3-improves-performance-brings-new-apps
comments:
  - subject: "iPad Support"
    date: 2012-10-15
    body: "Great job! It looks very promising, I hope to see a good partner to deliver hardware with Plasma Active!\r\n\r\nI'd love to have it running on my wife's iPad... iPad hardware is really amazing but software is terrible... I cannot agree to use itunes for everything, I cannot open a file under software x using software y. \r\n\r\n\r\n\r\n"
    author: "Caio Ferreira"
  - subject: "Perspectives"
    date: 2012-10-15
    body: "KDE developers chime in on Plasma Active 3 ...\r\n\r\n<a href=\"http://www.notmart.org/index.php/Software/Active_three\">Marco Martin</a>\r\n<a href=\"http://ivan.fomentgroup.org/blog/2012/10/15/active-3-for-kdes-16th-birthday/\">Ivan \u010cuki\u0107</a>\r\n<a href=\"http://aseigo.blogspot.com/2012/10/plasma-active-three.html\">Aaron Seigo</a>\r\n"
    author: "kallecarl"
  - subject: "That is a nice idea but"
    date: 2012-10-15
    body: "That is a nice idea but highly unlikely to ever happen. The ipad is an extremely locked down device and IF someone ever manages to break it, it would be very much illegal.\r\n\r\nOf course those facts (there is hardware on the market which you can't use how you want because it is cryptographically locked down to always remain under control of the supplier; AND you bought such hardware) are very sad in themselves, imho it should be illegal to lock owners of hardware out of, well, their own hardware. But it is a common practice and Apple is king of the hill in doing that stuff.\r\n\r\nIn short, you bought the wrong device, sorry."
    author: "jospoortvliet"
  - subject: "True innovation from the OSS community"
    date: 2012-10-15
    body: "A new generation of file managers ... I like that. It's similar to the Mac OS, when you click on \"Images\" in the Finder, your stored images come up in a completely different way to what you're used to, without file extensions showing, just with an iPhoto-like layout...\r\n\r\nAnd KDE is the first to port a new Files concept to a mobile device! Congrats KDE, you are real innovators. You should also look into companies who are exploring BYOD (Bring Your Own Device) schemes. KDE is the first to support a complete, high grade office suite for tablets out of the box.\r\n\r\nI just hope Plasma Active sees some real world use! The market is a bit saturated right now... it will be hard for new OSes to be adopted by the masses. I wish you the best though. We need more freedom in the tablet area, and we MUST break away from the app store business model that curbs both developer and consumer freedom!"
    author: "Yannis A"
  - subject: "another supplier?"
    date: 2012-10-15
    body: "If Google can sell it for $199, it should be possible to get ASUS to make one for KDE, or even use the same hardware. Or whoever makes Kindle Fire HD."
    author: "vasu"
  - subject: "Not illegal in all countries"
    date: 2012-10-16
    body: "In some (most?) countries, doing things like installing software on your stuff is not illegal. Apple may not like it and it might void the warranty, but it is not illegal."
    author: "Thomas Arnold"
  - subject: "Plasma Active + Hybrid PCs"
    date: 2012-10-17
    body: "I can't wait to get one of the new Windows 8 x64 Hybrid PCs (Samsung has a very nice one coming) and put most likely Kubuntu with Plasma Active onto it. "
    author: "Corodius"
  - subject: "Web browser ?"
    date: 2012-10-20
    body: "Have there been any improvements to the web browser? A browser is the main application I use on a tablet. In Plasma Active 2, it was a rather painful experience. I am wondering if it has been improved in version 3?"
    author: "Cyrille Berger"
  - subject: "Realistic hardware?"
    date: 2012-10-22
    body: "Is there any chance that Plasma Active will function some day on \"real\" devices instead of some weird machine I have to buy on the net? Most users aren't going to change their smartphones or tablets only to be able to use PA. I can't see a very bright future if an OS which isn't ever going to get the industry's support can't be installed on common hardware. Imagine if users had to buy specific computers to use Linux and KDE, only a small percentage of today's Linux users would be using it.\r\nSo, will we see soon PA working on a Samsung Galaxy S III, an HTC One, a Sony Xperia. In short, will we be able to use Plasma Active the same way we can use \"cooked\" Android ROMs like Cyanogen and others?\r\n\r\nRegards."
    author: "Mart\u00edn Ib\u00e1\u00f1ez"
  - subject: "Goals and perspective"
    date: 2012-10-23
    body: "<blockquote>Imagine if users had to buy specific computers to use Linux and KDE, only a small percentage of today's Linux users would be using it.</blockquote>\r\n\r\nThe environment for tablets is not the same as the one for PCs.\r\n\r\nImagine if the first PCs were as closed as tablets are now. The PC world would have evolved much differently. And it's likely that few people would be running Linux and KDE at all.\r\n\r\nThere are 2--soon to be 3 including MSFT--tablet OSs. They are all more or less closed. Even AndroidLinux is <a href=\"http://www.visionmobile.com/product/open-governance-index/\">closed to a great extent</a>, even though it's open source. Individual contributors do not have a seat at this table. The market is further dominated by concentrations of manufacturers, carriers and content providers.\r\n\r\nPlasma Active (PA) is open all the way down; the project itself and the Mer core operating system are openly developed and governed. Not everyone agrees that Plasma Active&Mer will never see industry adoption. There are already manufacturers supporting Plasma Active and open hardware. Customers who want to own the equipment they purchase. Still other customers who want and are willing to pay for customized solutions not supported by the tablet oligopoly.\r\n\r\nThe Plasma Active Project is not about competing with Apple, Google or MSFT. It's not about creating an OS that will run on closed hardware. Almost certainly there will be people who figure out how to get PA to run on devices such as those mentioned. They may even be involved in the Project.\r\n\r\nIBM introduced the IBM Personal Computer in August 1981. It was ten years and some days later that Linus Torvalds sent a message about his Unix-like OS project that would become Linux. And he was able to do that on an IBM-compatible clone, only possible because the hardware wasn't locked down by IBM.\r\n\r\nNow that the tablet and smartphone markets are dominated by closed hardware and software, how would a project such as Linux arise? \r\n\r\n"
    author: "kallecarl"
  - subject: "Awesome"
    date: 2013-03-16
    body: "I like Plasma Active. Cool features. Thanks"
    author: "Mukul Rana"
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/palogo.png" width="250" /></div>

KDE has <a href="http://kde.org/announcements/plasma-active-three/">released the third stable version</a> of Plasma Active, KDE's device-independent mobile user experience. The <a href="http://plasma-active.org/">Plasma Active</a> user interface is touch-friendly and works well across a range of devices. It is designed for personalized productivity, a complete mobile experience right from the first start-up. 

Its Activities function gives users a natural way to organize and access applications, files and information. Compared to previous versions, Plasma Active Three provides a noticeably better experience with its enhanced and expanded set of apps, improved performance and a new virtual keyboard.

<h2>New Apps</h2>
Files is a new default application in Plasma Active. It is a file manager, but, unlike most others, it isn't based on folders. Instead users can search for documents by file type, creation time and semantic information such as Tags. Files does not use the file system directly; it organizes documents with Nepomuk, Plasma Active's underlying semantic engine. 
 
Okular Active is Plasma Active's new Ebook Reader. Okular Active is built on the technology which also drives the desktop version of the popular Document Viewer, and is optimized for reading documents on a touch device. Okular Active supports a range of file formats, such as PDF, EPub, Open Document, and many others.
 
Through Plasma Active's Adds Ons , thousands of ebooks are available for free, with paid applications and content coming soon. 

<h2>Champions: Kontact and Calligra</h2>
With Calligra Active and Kontact Touch, Plasma Active delivers scalable and proven applications for office and groupware tasks, with a focus on interoperability. Kontact Touch supports many groupware solutions, and brings calendaring, email, contact management among other features. Calligra Active has excellent support for most common office file formats such as OpenDocument and Microsoft's .doc, .docx, .xls and .xlsx.  

<h2>Better Performance</h2>
KDE developers have put a lot of effort into improving the performance of the user experience, its applications and underlying libraries. These improvements bring a snappier and more visually coherent user interface, and make using Plasma Active a more enjoyable experience compared to previous versions.

<h2>Collaboration with Mer</h2>
Mer is the leading openly developed Linux-based OS for mobile devices. It is based on work from the MeeGo Project. With the size of the Core OS reduced to the bare essentials, Mer delivers performance. It is also a fully open development project, and thus supports fundamental Plasma Active design principles—openness, user freedom, collaboration.

<h2>Improved Text Input</h2>
Thanks to a new virtual keyboard based on <a href="https://wiki.maliit.org/Main_Page">Maliit</a>—the input method used on devices such as Nokia's N9 smartphone—Plasma Active Three makes text input easier. The new virtual keyboard is faster and more convenient to use and offers great flexibility for system integrators.

<h2>Roadmap: More Apps, Qt5 and File Synchronization </h2>
In future releases, users can expect support for a wider range of devices, easier synchronization of data across devices, an improved user experience through the use of Qt5 and KDE Frameworks 5, and more applications for popular uses. 

Plasma Active is openly developed, free software. It's built on the <a href="http://merproject.org/">Mer operating system</a> and runs on a number of devices—popular x86-based tablet computers, ARM-based hardware such as the Archos G9 series, and emerging touch-enabled devices as well. There are no artificial technical barriers to adoption and extension. This makes Plasma Active the perfect platform for customizing taken to a whole new level—write your own app or create an entirely new device experience. No barriers, only possibilities.

The KDE team is currently working to bring a Plasma Active-based tablet to the market to demonstrate its capabilities fully and offer users a fresh and open alternative to existing mobile operating systems. Participation in Plasma Active is welcomed, through individual contributions or by joining the <a href="http://dot.kde.org/2012/05/22/announcing-make-play-live-partner-network">Make * Play * Live partner network</a> as an official support or distribution partner.