---
title: "KDE Commit-Digest for 17th June 2012"
date:    2012-07-05
authors:
  - "mrybczyn"
slug:    kde-commit-digest-17th-june-2012
---
In <a href="http://commit-digest.org/issues/2012-06-17/">this week's KDE Commit-Digest</a>:

<ul><li>Multiple new features in <a href="http://www.calligra-suite.org/">Calligra</a>: better support for paragraph merging in Words, hover buttons for quick actions in Stage Slide Sorter, unlimited dragging of objects from outside supported in Stage and Flow, better monitor profile update of canvas, more options for styles in charts, most of the CPU-based <a href="http://opencolorio.org/">OCIO</a> implemented</li>
<li>Improved last commits history dialog in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<!--break-->
<li>Work started on OpenGL rendering in RasterImageView in <a href="http://gwenview.sourceforge.net/">Gwenview</a></li>
<li>Added support for Like, Comment and Application objects in <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>Support for many types of graphs in analitza</li>
<li>Changed the Note tab in the sidebar widget in conquirere</li>
<li>Formatting plugin added to <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Ktp</a></li>
<li>GSoC work on <a href="http://edu.kde.org/cantor/">Cantor</a></li>
</ul>

<a href="http://commit-digest.org/issues/2012-06-17/">Read the rest of the Digest here</a>.