---
title: "Announcing the Make Play Live Partner Network"
date:    2012-05-22
authors:
  - "sebas"
slug:    announcing-make-play-live-partner-network
comments:
  - subject: "Add-ons Partners"
    date: 2012-05-22
    body: "Sebastian, is it safe to say that Add-ons Partners include hardware peripheral makers?"
    author: "texrat"
  - subject: "Peripherals"
    date: 2012-05-22
    body: "We don't have any partners working on hardware peripherals at this time (just haven't gotten to that point in developing the ecosystem and projects around it) ... but if/when we do, then yes, they would slot into the Add-ons Partner category. At least that's the thinking right now. If combining both hardware and software initiatives in the same partner category becomes clumsy, we can always add another one :)"
    author: "aseigo"
  - subject: "More info about Coherent Theory LLC"
    date: 2012-05-23
    body: "Is there any information about that company?"
    author: ""
  - subject: "Coherent Theory LLC"
    date: 2012-05-23
    body: "At the bottom of the makeplaylive.com homepage there is information that MakePlayLive is a trademark of Coherent Theory LLC. MakePlayLive is the public face of the Vivaldi Project.\r\n\r\nIn the activities associated with the Plasma Active project, there has been almost nothing said about Coherent. They are coordinating the various tasks and activities associated with the Vivaldi tablet and encouraging people to take part.\r\n\r\nSearching the web for additional information will lead to http://aseigo.blogspot.com/ where there is plenty of information.\r\n"
    author: "kallecarl"
  - subject: "what do you wish to know?"
    date: 2012-05-23
    body: "i understand curiosity; in return, i hope people can respect the desire for privacy. we really don't think Coherent Theory is a very important or interesting thing. it's a legal entity that lets us take on the financial and legal risks of making things like Vivaldi and a place to coordinate things like this partner network and to pay hackers to work on Free sofware.\r\n\r\nthe company is privately owned by myself and another free software hacker (whom you probably also know by name), and it has so far been funded from our own pockets. (*gulp!*)\r\n\r\nnot very exciting, i know, but that's sort of the point :)\r\n\r\nthe exciting bits are Make\u00b7Play\u00b7Live, a collaborative brand, the products built with Free software and how we are using those efforts to in turn support Free software in a sort of virtuous circle.\r\n\r\ni really do not wish to make a company that is somehow \"important\" in the Free software world. there are enough of those already. i want the Free software world to do amazing things, and so that is where the focus belongs, and where ours is. the company is a legal entity that helps us achieve that, and if we could do all these things without it we would :)\r\n\r\ncheers ..."
    author: "aseigo"
  - subject: "Thanks"
    date: 2012-05-23
    body: "Thank you, that makes things clearer."
    author: ""
  - subject: "We're curious, of course."
    date: 2012-05-24
    body: "We're curious, of course. Your explanation really made things clear. Thanks!\r\n"
    author: ""
---
<p>
In the wake of the announcement of the first ever KDE powered tablet, quite a few interesting things are happening in the background. One of them is the formation of a professional Partner Network for devices such as the <a href="http://makeplaylive.com/">Vivaldi tablet</a>. Let's look at this Partner Network in more detail.</p>
<p>
The Make Play Live Partner Program is designed to build and support a collaborative business and economic network. Members work together to provide comprehensive professional service and product offerings around Plasma Active and devices such as Vivaldi. Professional support options make it easier to convince potential parties, such as users, clients, customers and partners, bringing KDE software to a larger group of users.
</p>
<p>
As not all services needs are the same, a way to identify the right companies is important. The Make Play Live Partner Network therefore defines four different types of partners:
</p>
<ul>
	<li><strong>Add-ons Partner</strong> -- These participants focus on development and support of post-purchase content. You will usually find the work of Add-ons Partners in the Add-ons app provided in Plasma Active, either in the form of apps or content, such as books, movies, music, etc.
	</li>
	<li><strong>Integration Partner</strong> -- An integration partner is the "Goto-Resource" for customization and integration of technologies used in Make Play Live products. Integration partners have experience with technologies such as Plasma Active or the Mer operating system, and are the right partners to get these products running on your device or in your environment.
	</li>
	<li><strong>Enterprise Partner</strong> -- If you need support for deployments in enterprise settings, such as companies, schools, (non-)governmental organizations or elsewhere, talk to any of our enterprise partner about the challenges you face and how they are prepared to solve these for you.
	</li>
	<li><strong>Retail Representative</strong> -- These partners form the retail backbone for Make Play Live products and services by offering sales channels between individual customers and Make Play Live.</strong>
</ul>
<p>
Partners may be represented in one or more segments of the program. All partners work together in a collaborative fashion that allows them to deliver more sophisticated and complete offerings than any individual partner could on their own. The partners also share a commitment to supporting the Free software communities and products behind the technologies, with a dual focus on direct contribution and the long-term sustainability of these communities.
</p>
<p>
John Blanford of VAULTechnology says, "As we do more computing with mobile devices, the long term trend is towards closed platforms where the software, the hardware and the available services are proprietary and locked down.  We are headed for a future of closed platforms. Vivaldi is all about the ecosystem!  Tablet development today is dominated by some of the world's largest companies.  If we hope to play in this arena, to make a real difference in the world, we can only do it as a community.  Alone we are small, but together as partners, we have a chance."
</p>
<p>
Coherent Theory LLC, the company orchestrating the Vivaldi and Make Play Live efforts, is facilitating the creation and development of this Partner Network. Companies and groups that are interesting in participating are encouraged to contact Coherent Theory via email to <a href="mailto:partners@makeplaylive.com">partners@makeplaylive.com</a>.
</p>
<p>
The following members of the Make Play Live Partner Network have already joined:
<ul>
	<li><a href="http://basyskom.com/">basysKom GmbH</a> -- Integration Partner</li>
	<li><a href="http://www.open-slx.com">open-slx GmbH</a> -- Retail Representative (Europe)</li>
	<li><a href="http://www.vaultechnology.com/">VAULTechnology LLC</a> -- Retail Representative (Americas) and Enterprise Partner</li>
	<li><a href="http://www.kogmbh.com/">KO GmbH</a> -- Add-ons and Integration Partner</li>
	<li><a href="http://kolabsys.com/">Kolab Systems AG</a> -- Enterprise Partner</li>
	<li><a href="http://www.xompu.de/">Xompu GmbH</a> -- Enterprise and Add-ons Partner</li>
	<li><a href="http://www.indt.org/">INdT</a> -- Add-ons Partner</li>
	<li><a href="http://www.vgrade.co.uk/">Vision Grade Services</a> -- Integration Partner, with a focus on Mer</li>
	<li>Coherent Theory LLC</li>
</ul>

...and so can your organization.</p>