---
title: "KDE Celebrates 2012 Google Summer of Code Success"
date:    2012-10-03
authors:
  - "Stuart Jarvis"
slug:    kde-celebrates-2012-google-summer-code-success
comments:
  - subject: "Thanks!"
    date: 2012-10-03
    body: "Thanks for the great release. I guess no comments means everyone is pretty much happy :)"
    author: "hmmm"
  - subject: "master?"
    date: 2012-10-03
    body: "And which of the above projects are 'merged into master?'"
    author: "emilsedgh"
  - subject: "Re : 'merged into master'"
    date: 2012-10-03
    body: "My project which is a Nepomuk plugin for Amarok is merged into master. It will be shipped with the next release of Amarok. \r\n\r\nYou could give it a try using Amarok <a href=\"http://quickgit.kde.org/index.php?p=amarok.git&a=summary\">git</a>."
    author: "Phalgun"
  - subject: "Import Tool Revamp in DigiKam"
    date: 2012-10-03
    body: "My project\u2014Import Tool Revamp in digiKam\u2014is also merged into master and will be available in the next release of digiKam (3.0.0)."
    author: "Islam Wazery"
  - subject: "That's some great news!"
    date: 2012-10-04
    body: "That's some great news! \r\nHowever, could it be, indeed, clarified for all the projects, which of them have already been merged to master, and in which stage is the merger of the rest?"
    author: "Jer Jerf"
  - subject: "Smit Mehta: Port KBreakout"
    date: 2012-10-05
    body: "Smit Mehta: Port KBreakout game to QtQuick\r\nViranch Mehta: Port KBreakout game to QtQuick\r\n\r\nTwo students working on exactly the same does not sound right."
    author: "Anonymous"
  - subject: "Duplicate entry fixed"
    date: 2012-10-05
    body: "Smit Mehta: UPnP / DLNA plugin for digiKam (as a kipi-plugin)"
    author: "kallecarl"
---
<!--Style for alternating grey & white backgrounds in table-->
<style type="text/css">
tr.d0 td {background-color: #e6e6e6}
</style>
KDE has again taken part in <a href="http://code.google.com/soc/">Google Summer of Code (GSoC)</a> as its biggest participating organization. Fifty-nine out of mind-boggling sixty projects have been completed successfully. Lots of new things have been learned, lots of code has been written and there's been plenty of fun. GSoC is over but the code has not gone away and the work is not finished. Over the next months, many of the students will continue to be part of KDE, integrate the code for future releases, improve it, maintain it, become more part of KDE. For now, however, pencils are down and we congratulate the students. It was a great summer and we enjoyed having you around!

<h2>Lots of variety</h2>
<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/gsoc-kde-screenshot-5.png" /><br />Improved shapes in Calligra</div>Many different KDE projects have been involved in Google Summer of Code 2012, including core libraries (kdelibs), accessibility, education, games, graphics, multimedia and PIM. Prominent KDE projects that are shipped independently from the KDE combined releases have also taken part, such as Amarok, the music player; Calligra, the office suite; Choqok, the microblogging client; Gluon, the game engine and KDevelop, the IDE.

The projects undertaken by students varied from under-the-hood code tweaking to reimplementing parts of the software using new technologies like QtQuick and QML to improve the user interfaces. Not every success will be noticed by our users immediately. Some of the projects focused on performance optimization; some will only prove fruitful in the future. Of course, to our developers and mentors every project and every effort was dear and precious!

<h2>Lessons learned and friends gained</h2>
The Google Summer of Code provides many valuable lessons, not only for the students—who get to learn from some of the world's leading free software developers—but also for the mentors who learn useful skills in training the next generation of KDE contributors.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/gsoc-kde-screenshot-6_0.png" /><br />Work in KStars</div><b>Rishab Arora</b>, who worked on improving KStars, enjoyed  "the chance to work with amazing people from all over the world" and learned that "just because you cannot see something, doesn't mean it isn't important - clarity and readability are as important as working code". <b>Samikshan Bairagya</b> also worked on KStars and "got to learn the awesome concept of model-view and learned a lot about programming conventions and UI designing". 

For <b>Matěj Laitl</b>, working on Amarok provided the opportunity to "finally work on open-source full-time during the summer without having to take less fun jobs on proprietary software". Fellow Amarok contributor, <b>Phalgun Guduthur</b>, sees the experience as "a platform to start contributing to open source projects that people use", noting that "it's satisfying to be part of something that is used by so many people around the world".

"The fact that you could learn about so many different things so quickly and so easily and get to interact with so many people" was the best thing for <b>Pankaj Bhambhani</b>, working on Facebook integration in Choqok. These thoughts were echoed by <b>Francisco Fernandes</b>, who worked on Krita, "the community interaction and the learning is the most valuable experiences"

There was praise, not only for the quality of mentors, but also the quality of software: " I learned how great Krita actually is," said <b>Shivaraman Aiyer</b>.

<h2>Thank you, great working with you. Don't be strangers!</h2>
Thank you to Google for this opportunity. Congratulations to our students again for their success; we invite you to stay with us. Keep hacking on KDE, keep learning and consider participating again in Google Summer of Code next year, as students or even as mentors, helping others get as much as possible out of the experience!

Let's help more students like <b>Claudio Desideri</b> to say: "I broke my limits".

<h2>More information</h2>
There is more about the <a href="http://code.google.com/soc/">2012 Google Summer of Code</a> at the <a href="http://community.kde.org/GSoC/2012/StatusReports">KDE Community Wiki</a>, including brief descriptions and more information about the projects. 

<h2>2012 Google Summer of Code participants and their projects</h2>

<table>
<tbody>
<tr>
<th style="text-align: center;">Name</th>
<th> &nbsp; </th>
<th style="text-align: center;">Project</th>
</tr>
<tr>
<td>A Janardhan Reddy</td><td> &nbsp; </td><td>Video SlideShow generator for Digikam</td>
</tr>
<tr class="d0">
<td>Abhinav Badola</td><td> &nbsp; </td><td>Video metadata support in Exiv2 library</td>
</tr>
<tr>
<td>Abhishek B S</td><td> &nbsp; </td><td>Improve Formula Shape in Calligra</td>
</tr>
<tr class="d0">
<td>Alessandro Cosentino</td><td> &nbsp; </td><td>Feed aggregator for ownCloud</td>
</tr>
<tr>
<td>Amandeep Singh</td><td> &nbsp; </td><td>Focus tracking in KWin / KMagnifier</td>
</tr>
<tr class="d0">
<td>Anant Kamath</td><td> &nbsp; </td><td>Implementing S.M.A.R.T. and improving ISO file management features in KDE</td>
</tr>
<tr>
<td>Ander Pijoan</td><td> &nbsp; </td><td>OpenStreetMap vector rendering on Marble</td>
</tr>
<tr class="d0">
<td>Antonis Tsiapaliokas</td><td> &nbsp; </td><td>Integrate Kwin with Plasmate SDK</td>
</tr>
<tr>
<td>Arthur Ribeiro</td><td> &nbsp; </td><td>QML Plasmoid for Plasma NetworkManagement</td>
</tr>
<tr class="d0">
<td>Avnee Nathani</td><td> &nbsp; </td><td>Port KDiamond game to Qt Quick</td>
</tr>
<tr>
<td>Bernhard Beschow</td><td> &nbsp; </td><td>An OpenGL mode for Marble</td>
</tr>
<tr class="d0">
<td>Brijesh Patel</td><td> &nbsp; </td><td>Improve saving of charts to OpenDocument in Calligra</td>
</tr>
<tr>
<td>Cezar Mocan</td><td> &nbsp; </td><td>Natural Earth Vector Map in Marble</td>
</tr>
<tr class="d0">
<td>Claudio Desideri</td><td> &nbsp; </td><td>Website design and implementation for GamingFreedom/Gluon</td>
</tr>
<tr>
<td>Cyril Oblikov</td><td> &nbsp; </td><td>Asynchronous errors handling during file transfer</td>
</tr>
<tr class="d0">
<td>Davide Bettio</td><td> &nbsp; </td><td>Port Plasma Calendar and other widgets to QML</td>
</tr>
<tr>
<td>Deepak Mittal</td><td> &nbsp; </td><td>Inter-ownCloud Instance Collaboration</td>
</tr>
<tr class="d0">
<td>Dominic Lyons</td><td> &nbsp; </td><td>Improve integration of Photivo into digiKam</td>
</tr>
<tr>
<td>Eli MacKenzie</td><td> &nbsp; </td><td>Modularized, server-aware IRC protocol handling for Konversation</td>
</tr>
<tr class="d0">
<td>Felix Rohrbach</td><td> &nbsp; </td><td>OCS specific JSON parser in Attica</td>
</tr>
<tr>
<td>Francisco Fernandes</td><td> &nbsp; </td><td>A Sandpainting brush for Krita</td>
</tr>
<tr class="d0">
<td>Ganeshprasad T P</td><td> &nbsp; </td><td>Bullet integration into Gluon</td>
</tr>
<tr>
<td>Giorgos Tsiapaliokas</td><td> &nbsp; </td><td>Make Plasmate ready for release</td>
</tr>
<tr class="d0">
<td>Islam Wazery</td><td> &nbsp; </td><td>Import Tool Revamp in digiKam</td>
</tr>
<tr>
<td>Jigar Raisinghani</td><td> &nbsp; </td><td>Support for pivot tables in Calligra Sheets</td>
</tr>
<tr class="d0">
<td>Lasath Fernando</td><td> &nbsp; </td><td>Message Filtering Plugin System</td>
</tr>
<tr>
<td>Lisa Vitolo</td><td> &nbsp; </td><td>Solid API extension for partitioning and a Dolphin plugin to use it</td>
</tr>
<tr class="d0">
<td>Lucas Lira Gomes</td><td> &nbsp; </td><td>Social Music - Tomahawk features in Amarok</td>
</tr>
<tr>
<td>Luís Gabriel Lima</td><td> &nbsp; </td><td>QMLify Plasma widgets</td>
</tr>
<tr class="d0">
<td>Mahfuzur Rahman (Mamun)</td><td> &nbsp; </td><td>Face Recognition for Digikam</td>
</tr>
<tr>
<td>Mailson D. Lira Menezes</td><td> &nbsp; </td><td>Tile-based rendering in Okular page view</td>
</tr>
<tr class="d0">
<td>Martin Klapetek</td><td> &nbsp; </td><td>Social feed</td>
</tr>
<tr>
<td>Martin Küttler</td><td> &nbsp; </td><td>New interface for Cantor</td>
</tr>
<tr class="d0">
<td>Matěj Laitl</td><td> &nbsp; </td><td>Statistics synchronization for pluggable devices and Last.fm in Amarok</td>
</tr>
<tr>
<td>Maximilian Löffler</td><td> &nbsp; </td><td>Kate Scripting IDE plugin</td>
</tr>
<tr class="d0">
<td>Miha Čančula</td><td> &nbsp; </td><td>Template system for KDevelop</td>
</tr>
<tr>
<td>Miquel Sabaté</td><td> &nbsp; </td><td>Improve KDevelop Ruby Support</td>
</tr>
<tr class="d0">
<td>Nityam Vakil</td><td> &nbsp; </td><td>Full support level 4 of the OpenFormula specification</td>
</tr>
<tr>
<td>Pankaj Bhambhani</td><td> &nbsp; </td><td>Facebook integration for Choqok</td>
</tr>
<tr class="d0">
<td>Paul Mendez</td><td> &nbsp; </td><td>Support for editing shape animations in Calligra Stage</td>
</tr>
<tr>
<td>Percy Camilo Triveño Aucahuasi</td><td> &nbsp; </td><td>Replacement for KmPlot</td>
</tr>
<tr class="d0">
<td>Phalgun Guduthur</td><td> &nbsp; </td><td>Semantic desktop collection in Amarok</td>
</tr>
<tr>
<td>Riccardo Iaconelli</td><td> &nbsp; </td><td>Amarok will shine like new</td>
</tr>
<tr class="d0">
<td>Rishab Arora</td><td> &nbsp; </td><td>Improving Data Storage, Logs and adding DSO catalogs to KStars</td>
</tr>
<tr>
<td>Roney Gomes</td><td> &nbsp; </td><td>Porting games to a modern graphics framework</td>
</tr>
<tr class="d0">
<td>Samikshan Bairagya</td><td> &nbsp; </td><td>Add "What's Interesting..." feature to KStars</td>
</tr>
<tr>
<td>Shivaraman Aiyer</td><td> &nbsp; </td><td>Perspective drawing in Krita</td>
</tr>
<tr class="d0">
<td>Shreya Pandit</td><td> &nbsp; </td><td>Introduction of context action-based layout and UI improvements in Gluon Creator</td>
</tr>
<tr>
<td>Shrikrishna Holla</td><td> &nbsp; </td><td>Adding an Infinite Canvas / Wraparound mode for Krita</td>
</tr>
<tr class="d0">
<td>Sinny Kumari</td><td> &nbsp; </td><td>Advanced features and enhancements for Plasma Media Center</td>
</tr>
<tr>
<td>Smit Mehta</td><td> &nbsp; </td><td>UPnP / DLNA plugin for digiKam (as a kipi-plugin)</td>
</tr>
<tr class="d0">
<td>Smit Patel</td><td> &nbsp; </td><td>Bibliography engine integration and UI to manage citations in Calligra Words</td>
</tr>
<tr>
<td>Vegard Øye</td><td> &nbsp; </td><td>Improve Kate's vi Input Mode</td>
</tr>
<tr class="d0">
<td>Victor Dodon</td><td> &nbsp; </td><td>Porting libkipi and kipi-plugin to KDE-XML GUI</td>
</tr>
<tr>
<td>Vinay S Rao</td><td> &nbsp; </td><td>Implementing saving/loading Gluon Engine's game state</td>
</tr>
<tr class="d0">
<td>Viranch Mehta</td><td> &nbsp; </td><td>Port KBreakout game to QtQuick</td>
</tr>
<tr>
<td>Vladislav Sitalo</td><td> &nbsp; </td><td>Integration of CMU SPHINX into Simon</td>
</tr>
<tr class="d0">
<td>Yash Shah</td><td> &nbsp; </td><td>Using computer vision to improve speech recognition in Simon</td>
</tr>
<tr>
<td>Zhengliang Feng</td><td> &nbsp; </td><td>Integrate Spotify into Amarok</td>
</tr>
</tbody>
</table>