---
title: "KDE Commit-Digest for 8th January 2012"
date:    2012-01-27
authors:
  - "mrybczyn"
slug:    kde-commit-digest-8th-january-2012
---
In <a href="http://commit-digest.org/issues/2012-01-08/">this week's KDE Commit-Digest</a>:

<ul>
<li><a href="http://techbase.kde.org/User:Mgraesslin/Aurorae">Aurorae</a> goes QML, ClientGroup becomes scriptable in KDE base</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> sees work on footnotes, all interactive painting tools are threading-friendly; <a href="http://www.kexi-project.org/">Kexi</a> introduces Simple User Feedback Agent</li>
<li><a href="http://userbase.kde.org/Rekonq">Rekonq</a> adds about:tabs, an easy method to manage rekonq tabs</li>
<li>Support for Twitter photo upload API method in <a href="http://userbase.kde.org/Choqok">ChoqoK</a></li>
<li>Board Editor and random layout addition to <a href="http://games.kde.org/game.php?game=kmahjongg">KMahjongg</a></li>
<li>Circular reference memory leaks fix and other updates in <a href="http://amarok.kde.org/">Amarok</a></li>
<li>Initial port of Calendars resource to libKGoogle 0.3 in akonadi-google</li>
<li><a href="http://userbase.kde.org/Skrooge">Skrooge</a> has "Show" menu in scheduled operations.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-01-08/">Read the rest of the Digest here</a>. The Commit-Digest team is aware of the lack of top level statistics. The issue is being worked on.