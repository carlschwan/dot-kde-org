---
title: "KDE Announces 4.9 Beta1 and Testing Initiative"
date:    2012-06-04
authors:
  - "sebas"
slug:    kde-announces-49-beta1-and-testing-initiative
comments:
  - subject: "The work on kwin shows! =:^)"
    date: 2012-06-04
    body: "While waiting for the beta last week, I read up on the feature plan.  The biggest thing there by far was all the bugs and wishlist items fixed in kwin, including some going back to the kde3 era.  I mean, there's a *LOT* of them!\r\n\r\nSo I was looking forward to trying the beta, to see how it worked out.  Kwin is /noticeably/ faster, effects snappier.  There's also additional improvements to the effects config applet.\r\n\r\nThe work REALLY shows and it's all around quite impressive!  Thanks!\r\n\r\nFWIW I'd also been waiting on the konsole extended area mouse fixes.  I was CCed on the bug and as an mc user I couldn't wait when the fix got in for 4.9.  That's actually one of the big reasons I was so looking forward to this first 4.9 beta, to try it out.  It works as expected, now.  =:^)\r\n\r\nDuncan"
    author: ""
  - subject: "Automatic test cases?"
    date: 2012-06-05
    body: "Wouldn't it make more sense to start writing automatic testcases for the applications? This way regressions could be found faster and easier, since manual testing is tedious and not really much fun. In the java world, unit testing the applications is common practice."
    author: ""
  - subject: "Re: Unit testing"
    date: 2012-06-05
    body: "@Anonymous It's not an either or. We already have many unit tests especially in kdelibs, but they're certainly not covering a 100%. We do encourage developers to work with unit tests, but the coverage could certainly be better.\r\n\r\nThat's not a substitute at all for functional testing done by real human beings."
    author: "sebas"
  - subject: "LiveCD?"
    date: 2012-06-05
    body: "Is there any liveCD to test it? I would like to have a look at this beta but I can't install it on my system since I'm not the root.\r\n\r\nThanks, it looks great."
    author: ""
  - subject: "The Beta Testing Team can answer questions"
    date: 2012-06-05
    body: "Please contact the Team on IRC channel #kde-quality on freenode.net."
    author: "kallecarl"
  - subject: "sure"
    date: 2012-06-05
    body: "of course, and quite a few KDE apps already have that, others are adding unit tests (see kwin post on the planet). But it's work, not glamorous or very interesting - so it needs people doing :D"
    author: ""
  - subject: "There are unstable KDE"
    date: 2012-06-05
    body: "There are unstable KDE packages for openSUSE:\r\nhttp://en.opensuse.org/KDE_repositories#Unstable:SC_aka._KUSC_.28KDE_trunk.29\r\n\r\nyou can use these to build a LiveCD on SUSEStudio.com but I don't know how up-to-date those packages are. Then again, it is not that hard to update them via the web interface on build.opensuse.org - see https://build.opensuse.org/project/show?project=KDE%3AUnstable%3ASC\r\n\r\nYou could use the web interface to try and build more up-to-date packages (and even submit the newer ones back to the project). It would be quite a bit of work but it'll be interesting and useful :D\r\n\r\nhttp://kdeatopensuse.wordpress.com/2012/06/03/adopt-an-opensuse-kde-package-everybody-can-help-packaging/"
    author: ""
  - subject: "Kwin not (yet) bug-free"
    date: 2012-06-05
    body: "Martin, Thomas and all kwin contributors are indeed spending a huge effort in bug-fixing, and deserve warm thanks and admiration for this. \"...all the bugs and wishlist items fixed in kwin\" doesn't mean that every bug has been fixed.\r\n\r\nThis is an incredibly hard target to achieve, given the status of video drivers on free OSes as described by Martin himself some months ago:\r\n<a href=\"http://blog.martin-graesslin.com/blog/2011/02/kwin-and-the-unmanageable-combinations-of-drivers/\">http://blog.martin-graesslin.com/blog/2011/02/kwin-and-the-unmanageable-combinations-of-drivers/</a>\r\n\r\nBut it's true that, in spite of this, the Kwin team is working miracles, and the milestone of a \"bugfree\" kwin is becoming closer and closer:\r\n<a href=\"http://blog.martin-graesslin.com/blog/2012/04/less-than-200-open-kwin-bug-reports/\">http://blog.martin-graesslin.com/blog/2012/04/less-than-200-open-kwin-bug-reports/</a>\r\n\r\nWay to go, folks!"
    author: "Vajsvarana"
  - subject: "Live CD"
    date: 2012-06-05
    body: "I am offering a Live-CD build from SUSEStudio and update it about once a week, if the packages have changed.\r\n\r\nhttp://susestudio.com/a/tAWYe6/kde-plasma-daily\r\n\r\nMartin Gr\u00e4\u00dflin"
    author: ""
  - subject: "Thank you very much"
    date: 2012-06-07
    body: "Thank you very much"
    author: ""
  - subject: "milestone"
    date: 2012-06-09
    body: "Hello!\r\n\r\nI'm a loyal KDE user, although rather disappointed up to now. Tried other stuff like Gnome, OSX, but those lack functionality and flexibility. Loved KDE3, but that lacks modern features.\r\n\r\nSince I compiled 4.9 on gentoo, I couldn't believe it. 4.9 will be a milestone. Even though it's beta, things are noticeably better. KWin runs much quicker. I need some patch to make qtcurve work, but can live without it.\r\n\r\nPlasma runs with less memory, smoother and more stable. Some new Kdepim features I had trouble with, but regular features are faster and more stable (I have a lot of imap/calendar/vcard agents with many records which increases the startup time).\r\n\r\nDolphin is great and usuable. I was able to switch SVN/GIT back on. Had problems with new features, like the difficult \"Control\" button with cog or opening of archives if I directly try to open by command line parameter. I still miss the \"full row selection\" feature that was in kde3 konqueror. Anyway, it's a great progress I haven't seen before with KDE4 at all.\r\n\r\n"
    author: ""
  - subject: "Safe to upgrade from pre-Akonadi KMail?"
    date: 2012-06-10
    body: "There's been no progress reported recently on the bug that causes migration from pre-Akonadi Kmail to go wrong. Has there been any progress with migrating a substantial volume (GBs) of email? Any improvement in 4.9 beta? Has anyone tried high volume KMail migration with this beta?\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=283563\r\n\r\n"
    author: ""
  - subject: "Quick: migration has been"
    date: 2012-06-10
    body: "Quick: migration has been disabled since KMail 4.8. Now KDE will import your mails as a mail resource, and will work with that. You need to set up your IMAP and POP3 accounts separately. But it will work after that and you won't lose mail."
    author: "Alejandro Nova"
  - subject: "KMail migration"
    date: 2012-06-11
    body: "I haven't tried 4.9beta, but the migration to KMail2 was the most troublesome part of upgrading from Kubuntu 10.10 to 12.04 (which is KDE 4.8). I migrated >100K messages (several gigabytes) spanning about a decade of using Kontact.\r\n\r\nIt's been some time now, and so my memory is not entirely fresh, but:\r\n1. I avoided the \"migration tool\". It crashed several times and lost all \"status flags\" (replied, unread, etc). I found it better to manually copy all files and folders to the new location, ~/.local/share/local-mail/. I used akonadiconsole to get everything configured properly.\r\n2. The new nepomuk is nice, but it worked best for me to disable it during the migration. I had to restart it multiple times to get it to finish cataloging all of my email (it kept crashing). Turn it back on once you're done. Once migration was done, it sure is faster to search by message content than it used to be!\r\n3. Import of address book and calendar was much smoother. The most annoying hiccup is that, when composing an email, KMail2 no longer finds all of my address book entries when I start typing the recipient's name. For most of my address book (but not all, and I haven't yet discovered the pattern), when composing an email I have to click the \"Select...\" button for each recipient. That's much slower than typing the first few characters of their name, but I can live with it.\r\n\r\nI didn't lose any emails, and some of the new features are a definite improvement. I'm semi-content with the new system. With more stabilizing/polishing, the future looks good. But expect some pain in the migration (and definitely start from a backup), especially with lots of emails. And follow instructions and reports from people who've succeeded."
    author: ""
  - subject: "KMail migration"
    date: 2012-09-17
    body: "Thanks so much for that, I finally took the plunge (after having to get a new desktop which needed a recent kernel and X) by upgrading to Kubuntu 12.04 and KDE 4.9.1 from the PPA.   I avoided the migration agent, just adding my old Kmail1 folders as a KMail resource which seemed to have worked well (so far).\r\n\r\nThe only major bug I've run into so far is that filters don't seem to work on IMAP inboxes - they work when I apply them manually to folders in the folder but don't get triggered for new emails arriving there.  Painful!"
    author: "Chris Samuel"
---
Today KDE released the first beta for its renewed Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing new and old functionality. Highlights of 4.9 include:

<ul>
    <li>
    Qt Quick in Plasma Workspaces -- Qt Quick is continuing to make its way into the Plasma Workspaces; the Qt Quick Plasma Components, which were introduced with 4.8, mature further. Additional integration of various KDE APIs was added through new modules. More parts of Plasma Desktop have been ported to QML. While preserving their functionality, the Qt Quick replacements of common plasmoids are visually more attractive, easier to enhance and extend and behave better on touchscreens.
    </li>
    <li>The Dolphin file manager has improved its display and sorting and searching based on metadata. Files can now be renamed inline, giving a smoother user experience.
    <li>
    Deeper integration of Activities for files, windows and other resources: Users can now more easily associate files and windows with an Activity, and enjoy a more properly organized workspace. Folderview can now show files related to an Activity, making it easier to organize files into meaningful contexts.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.9_Feature_Plan">4.9 Feature Plan</a>. As with any large number of changes, we need to give 4.9 a good testing in order to maintain and improve the quality and the user experience when they get the update. Therefore, in tandem with this first 4.9 beta, we also announce a testing initiative led by KDE's Testing Team.
<!--break-->
<h3>Thorough and Rigorous Beta Testing</h3>
The KDE Community is committed to improving quality substantially with a new program that starts with the 4.9 releases. The 4.9 beta releases of Plasma Workspaces, KDE Applications, and KDE Platform are the first phase of a testing process that involves volunteers called “Beta Testers”. They will receive training, test the two beta releases and report issues through <a href="http://bugs.kde.org">KDE Bugzilla</a>.

<h4>The Beta Testing Team</h4>
The Beta Testing Team is a group of people who will do a 'Testapalooza' on the beta releases, catching as many bugs as possible. This is organized in a structured process so that developers know clearly what to fix before the final release. Anyone can help, even people without programming skills.

<h4>How It Works</h4>
There are two groups of beta testers:
<ul>
<li>informal testers install the beta from their distribution and use it as they normally would. They focus on familiar applications and functions, and report any bugs they can reproduce reliably.</li>
<li>selected functional testing involves a list of applets, new programs and applications that require full testing. Volunteers will choose from the list and test according to <a href="http://community.kde.org/Getinvolved/Testing/Beta">established beta testing guidelines</a>.</li>
</ul>
Testing is coordinated to make sure that people's testing time and effort are used as effectively as possible. Coordination gets the most value out of the overall testing program.

<h4>Get Involved</h4>
This is an excellent way for anyone to make a difference for KDE. Continuous quality improvement sets KDE apart. Assisting developers by identifying legitimate bugs leverages their time and skills. This means higher quality software, more features, happier developers. The Beta Testing Program is structured so that any KDE user can give back to KDE, regardless of their skill level.

If you want to be part of this quality improvement program, please contact the Team on the IRC channel #kde-quality on freenode.net. The Team Leaders want to know ahead of time who is involved in order to coordinate all of the testing activities. They are also committed to having this project be fun and rewarding.

After checking in, you can install the beta through your distribution package manager. The <a href="http://community.kde.org/Getinvolved/Testing/Beta/InstallingBeta1">KDE Community wiki</a> has instructions. This page will be updated as beta packages for other distributions become available. With the beta installed, you can proceed with testing. Please contact the Team on IRC #kde-quality if you need help getting started.

<h4>Training</h4>
Effective bug reporting is critical to the success of the Program. Bugzilla is a valuable tool for developers and increases their efficiency at closing bugs. Reporting works best when done properly, so the Program includes Bugzilla training. An IRC bugzilla training will be offered in #kde-bugs (freenode) on the weekend of June 9th and 10th. Please visit the #kde-quality IRC channel for information about the Program and the Bugzilla training, or if you have any questions.

Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining in.