---
title: "Krita Sketch - Mobile Artistry"
date:    2012-12-19
authors:
  - "annma"
slug:    krita-sketch-mobile-artistry
comments:
  - subject: "Best sketching and painting application!"
    date: 2012-12-30
    body: "Krita Sketch is even much better than Sketchbook Pro. It supports quite a high resolution of 10000 * 10000 pixels (maximum). Krita Sketch is a revolutionary painting application for artists. If this resolution is enough for you, then there's no reason to buy other applications like ArtRage or PD Howler. Krita Desktop even supports a very high resolution. More than Photoshop does, which is also very good.\r\nThe best part of Krita is the Brush engines and special brushes,not found in any of other painting applications.Just try out this application.You will love it.Brush engine is confususious initially but latter on you realise its best brush engine you can get.I advice Krita developers to provide a detailed user manual for brush engine in Krita.\r\nKrita is best Painting and sketching application as of now!and best thing is it is free!"
    author: "Aman"
  - subject: "Thanks, Aman, for your kind"
    date: 2012-12-31
    body: "Thanks, Aman, for your kind words!"
    author: "jospoortvliet"
---
Imagine drawing anywhere with your tablet. It's now possible! <a href="http://www.kde.org">KDE</a> and <a href="http://www.kogmbh.com/">KO GmbH</a> have released Krita Sketch, the first tablet and ultrabook version of <a href="http://krita.org/">Krita</a>, the award-winning digital painting application. Optimized for touch screens and developed with a QML interface, Krita Sketch provides everything needed to create artwork from beginning to end.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/TimoSketchTest.png" /></a><br />Krita Sketch - image by animtim</div>
Krita Sketch was developed in partnership with Intel for a major marketing campaign. Version 1.0 for Windows 7 and Windows 8 is available for free from <a href="http://www.appup.com/app-details/krita-sketch">Intel AppUp</a>. Krita Sketch is free software and is also available as source code from <a href="https://projects.kde.org/projects/calligra/repository">the Calligra git repository</a>.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kritasketch0.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kritasketch0wee.png" /></a><br />Krita Sketch startup screen</div>

<h2>Features</h2>
Krita Sketch has many of the same advanced features as the famous Krita application for desktop computers:
<ul>
<li>layers</li>
<li>filters</li>
<li>mirrored painting</li>
<li>a powerful brush engine</li>
<li>brush presets</li>
<li>... and much more.</li>
</ul>

Discover your creativity with the intuitive, fluid and powerful interface of Krita Sketch.

<a href="http://www.timotheegiet.com/plux/">Timothee Giet (aka animtim)</a>, a French artist using Free Software working with Krita said, "The brush presets are revolutionary, and the smooth modern interface never interrupts your workflow while painting". Timothee contributed to the interface of Krita Sketch. Professional artists use Krita for its powerful features and smooth workflow. Krita Sketch allows all this power to be available at the tip of your finger on a tablet.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kritasketch-main.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kritasketch-mainwee.png" /></a><br />Working interface - image by animtim</div>
Krita Sketch is a <a href="http://en.wikipedia.org/wiki/QML">QML</a> application that re-uses the existing Krita engine and plugins, so it will be an easy port to <a href="http://plasma-active.org/">Plasma Active</a>, KDE's Plasma Workspace for mobile devices. Qt makes it easy to create the necessary packages for touch screens on Linux.

<a href="http://krita.org/item/125-announcing-the-krita-foundation">Stichting Krita Foundation</a>, the Krita Foundation, has also been announced to support Krita development and promotion. Please support the Krita Foundation with a subscription or a one time donation.

Sketching, painting, drawing and creating comic characters has never been so easy or satisfying. For accomplished artists and anyone else who wants to express themselves creatively!