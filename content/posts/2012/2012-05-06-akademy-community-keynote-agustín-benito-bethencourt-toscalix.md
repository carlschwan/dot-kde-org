---
title: "Akademy Community Keynote: Agust\u00edn Benito Bethencourt (toscalix)"
date:    2012-05-06
authors:
  - "tcanabrava"
slug:    akademy-community-keynote-agustín-benito-bethencourt-toscalix
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/a_benito_penguins.png" /></div>

Agustín Benito Bethencourt (aka "toscalix") recently joined the KDE e.V. Board of Directors. He will be presenting the KDE Community Keynote at Akademy 2012 in Tallinn.
<!--break-->
<strong>Tomaz:</strong> You are active in the KDE Community—the Promo team, KDE e.V. Board of Directors, new ideas, big ideas, helping wherever you can. It seems from your blog posts that you do free software even in your sleep. What you do in Madrid in your free time?

<strong>Agustín:</strong> I like sports in general, especially basketball, which I used to play when I was younger. Two sentences define what I like to do in my free time—"work hard, party hard" and "music is my first love and it will be my last".

I enjoy traveling and living close to the sea, by the beach, especially back home in La Palma, Canary Islands.

<strong>Tomaz:</strong> People often think of bullfighting in Spain, but football is big. What football team would you endorse and put a 'K' on the shirt?

<strong>Agustín:</strong> I'm a big fan of CD Tenerife. I used to go to the stadium regularly when I was living in the Canary Islands. Sadly, we are not doing good at all lately. I also like Real Madrid. But above all, I would like to see the KDE logo on the shirts of the Spanish national football team, especially during this Eurocup Final, which will take place during Akademy, by the way. Being able to celebrate a victory in Tallinn would be very cool.

<strong>Tomaz:</strong> How has your view of free software changed from your early university days to now when you are well known in the Qt/KDE world?

<strong>Agustín:</strong> The biggest change comes with the success of Free Software business models. I've lived the success of Free Software in general, and KDE in particular, from the business side and it has been impressive so far. But the best is yet to come.

This success has helped to turn an ethical/technical movement into a worldwide social and economic one with impacts in many other areas. Our tools, processes, organization models, and philosophy have also influenced other growing movements like Open Data, Open Government, Open Hardware and more.

Engineering processes is another area where Free Software has made a huge impact, probably bigger than we think. New software develoment methodologies are strongly influenced by what we started a few years ago. We are constantly innovating, and seeing our new ideas becoming mainstream.

Free Software communities are probably the most evolved digital based structured ecosystems right now. Many people are beginning to be aware of the innovation that we, as organizations, represent. It wouldn't surprise me that in the near future we will be used as a success story for other social movements to evolve from. Probably we are already.

I would also like to mention that Free Software is helping to change the vision many have in two areas that are critical for our society: copyright/intellectual property and patents as protection tool for innovation.

And obviously, on the technical side, Free Software has changed the software industry. Tools and products we use nowdays were nothing but a dream not so long ago.

<h2>About Akademy</h2>

<strong>Tomaz:</strong> Akademy 2009 in the Canary Islands was my first time in another country serving a greater good. Did you help organize it? How does it feel to organize something so far from home this time? 

<strong>Agustín:</strong> Yes. I was one of the local organizers. My role this year is quite special. I was elected KDE Treasurer just a few weeks ago. Akademy is KDE's major economic effort. My role is to make sure that this edition is a financial success, like previous ones have been. I work in collaboration with the rest of the Board members, our Business Manager and the Akademy Team. We have a good team so everything will be fine.

The KDE e.V. Board has some institutional duties during Akademy, like organizing our annual Assembly and meeting local authorities, sponsors, and Patrons. This year I'm going to live Akademy from a different perspective than before.

<strong>Tomaz:</strong> Akademy 2010 was my second. It was an amazing and terrific time. I hacked, hacked and hacked on things that I usually don't have much time to do because of real life issues like a job. I also made quite a few new friends. What's your opinion about Akademy and the bonding that it provides for the Community?

<strong>Agustín:</strong> Every one of us has different motivations for contributing to KDE, but we share what we can summarize as a KDE Identity. This Identity is a mixture of different values. Some of them need to be built and nourished through personal face to face interactions. Akademy is the perfect place to do it.

KDE is getting bigger every day. Attending Akademy is the most effective way of getting a global picture of what we are doing. 

This picture is needed to understand where are we going from the community perspective, but also for each one of our contributors and followers.

Akademy is a first class networking event. We just turned 15 years old a few months ago. Past and current community members are working in many fields, in many different organizations worldwide, come from different environments, have different cultures. Akademy is a genuine innovation forum in several different areas, not just software. Attending Akademy opens your mind and your eyes. So you go back home with renewed motivation. It is a like a yearly refreshment point. And not just for KDE contributors and users, but for every attendee.

<strong>Tomaz:</strong> What was the best Akademy for you, and why? 

<strong>Agustín:</strong> I can remember every single one of them for different reasons. The first one is always shocking. Mine was in Malaga in 2005. The first Desktop Summit will always be a major milestone in my life for many reasons. Working so closely with so many local, KDE and GNOME enthusiasts was a very rewarding experience. The last Desktop Summit/Akademy in Berlin was also a good one since I attended during my vacation. I was relaxed and could fully enjoy it.

I face the 2012 edition with the impression that I won't forget it either, for very good reasons. 

<h2>About Life as a KDE spokesperson</h2>

<strong>Tomaz:</strong> What are your main contributions to KDE? How do you gather people to help on KDE Projects?

<strong>Agustín:</strong> I would divide my KDE life into three different phases. The first was while I was living in the Canary Islands coordinating the development and deployment of the Education distribution from the Regional Government, based on KDE. The second was working on the organization of the first Desktop Summit. The third will see the light soon. It is called KDE Connect. Meanwhile I've helped here and there, promoting KDE, recruiting new contributors, helping establish KDE Spain and make it successful. 

<strong>Tomaz:</strong> Near the end of the year, there is LatinoWare, the Open Source conference for Latin American countries near Foz do Iguaçu, Brazil. What about coming over to talk about your new KDE activities? Have you ever visited Brazil, Paraguay or Argentina?

<strong>Agustín:</strong> I haven't been in any of those countries. Latinoware? Yes, of course. I only see one problem. I might not come back home again.

<strong>Tomaz:</strong> It's not bad at all then. Agustín, thanks for your time and for sharing your thoughts. See you at Akademy.

<h2>Akademy 2012 Tallin, Estonia</h2>

For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy.kde.org/">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.