---
title: "Randa Fundraising Success!"
date:    2012-09-22
authors:
  - "jospoortvliet"
slug:    randa-fundraising-success
comments:
  - subject: "Do KDE users read the Dot?"
    date: 2012-09-22
    body: "KDE could use a more prominent system to spread the word for such campaigns. My sister and my parents are KDE users but never read KDE web sites. But when the system starts the start-up graphic could fade into a \"important news\" box staying for lets say 10 sec in front of all windows: \"WE NEED YOUR HELP!\" . Some l10n of such messages could also help.\r\n\r\nBye\r\n\r\n  Thorsten"
    author: "schnebeck"
  - subject: "good point"
    date: 2012-09-22
    body: "Yeah, that's something we could do. Question is - how would users feel about that? Is it spam? Evil? Not sure how they react... But I do agree that this is something we could/probably should do some day: just get a message out to our users.\r\n\r\nBut I do feel it's a good idea and at least worth investigating and possibly experimenting with. After all - it's not just about the money here, as I wrote, it is also about building relationships, showing we care, stuff like that. Just having people 'like' KDE on facebook and follow on G+ etc would actually count as a contribution if you ask me.\r\n"
    author: "jospoortvliet"
  - subject: "It's something we discussed"
    date: 2012-09-25
    body: "It's something we discussed at the last KDE PIM sprint, and also at the last Desktop Summit.  The general idea was to have a couple of Plasmoids that could appear on the default desktop (if the distro's allowed of course).  One idea was a 'Join The Game' plasmoid would just be the logo with a link like \"Donate to help make KDE better\" to take you to the JTG website.  Another idea was a \"Meet the Developers\" plasmoid that displayed random developer photos and details to put a human face on KDE and again encourage donations (down side: if the software breaks they know who to hunt down and inflict pain on :-) ).  The enhanced \"About KDE/KApp\" dialog was getting a social element and there was some suggestion to enhance it with a donate link.\r\n\r\nA better idea might be to write a library that all apps call on start-up that checks if there is a fundraiser for that app and then displays a splash screen once only to ask the user if they would like to donate to support the app.  If it was low-profile and infrequent enough it could work without annoying the user. The down-side is that distro's may not like it and disable it, or replace it with their own fundraiser.\r\n\r\nThe wider problem that is FOSS-wide is we don't have a well know and trusted platform for FOSS projects and users to \"meet-up\" and advertise funding opportunities.  Using all the different mainstream funding sites we easily get lost in the crowd.  If there was a single trusted platform that FOSS projects could list their fundraisers on then we could educate users to always use that would be a big step forward.  At the very least, the KDE eV should probably look around and decide on a single 'official' funding platform that we list all official fundraisers on and can direct users to (we'd need an official policy for what can get listed and how the money is managed)."
    author: "odysseus"
  - subject: "So thinking a little more,"
    date: 2012-09-25
    body: "So thinking a little more, the first aim should be a default \"Welcome to KDE\" plasmoid that has links to the home page, User Base, JTG and the social networks.  Distro's could add their own links if they want.  It would take someone a couple of hours to knock together the required HTML or QML, it's nothing fancy required, just a pretty presentation, a few web-links, some translations, and a modification to the default Plasma config."
    author: "odysseus"
  - subject: "welcome screen"
    date: 2012-09-25
    body: "Something like the welcome screen is a good way to speak to the users. But I think we should learn from all the social media stuff: people like to read a message and interact in a direct way. If you have a screen \"welcome users\" and some links and you get this visual impression every day noone clicks on the links but presses the close button. Such a welcome screen has to be active maintained and if this can be handled it has to be translated.\r\nWhen I say active I do not think about a news page style - a welcome screen should be quiet most of the time. But when there is important stuff (a new version, a main upgrade, fundraising action, loss of important community members) then the plasmoid should be activated ... only 10..20 times a year: Do not annoy the users, but keep them curious about KDE. But its important to show the message and not only a link to the message.\r\n\r\nBye\r\n\r\n  Thorsten\r\n"
    author: "schnebeck"
  - subject: "It's difficult as for this"
    date: 2012-09-26
    body: "It's difficult as for this the software has to \"phone home\", which many people (especially Admins) do not like. KDE is a free software community which values the aspects of freedom. Part of the freedom is to also protect the privacy of our users. And just these days we see that many users are upset by the thought that their system will phone home.\r\n\r\nI totally agree that it would be great to have a good welcome applet, but we have to be careful about the phoning home or advertising (asking for donations on your desktop is quite close to putting Amazon ads into the launcher) aspects. Integrating the latest news might be a solution. But the social aspect needs to be the focus. E.g. use the BehindKDE series."
    author: "mgraesslin"
---
As of now, the <a href="http://pledgie.com/campaigns/18045">fund drive for the Randa Sprint</a> has exceeded the goal. That's right, our Community generated over 10,000 € from 287 awesome supporters to make the meeting happen! The Sprint started yesterday evening; the fundraising is staying open for people who want to contribute. Any further donations will go towards <a href="http://sprints.kde.org/">future sprints funding</a>. Follow the action in Randa on <a href="https://twitter.com/i/#!/nightrose/kde-randa-2012-sprint">this twitter stream</a> and <a href="https://plus.google.com/events/cum2bq3ru6g2uf7unj259olpr10">here on Google Plus</a>. And look for <a href="http://planetkde.org">posts on Planet KDE</a> (<a href="http://wm161.net/2012/09/22/greetings-from-randa/">Trever said 'hi' already</a>) about the event! There will be a comprehensive report after the Sprint.
<div style="float: right; padding: 0ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Randa_wee.jpg" /><br />Commence Randa hacking</div>

<h2>Thanks, Thanks, Thanks!</h2>
Thank you to everyone who contributed—either by donating (that's like 289 hugs from all of you to us!) or by spreading the word on the fund raiser. Martin already <a href="http://blog.martin-graesslin.com/blog/2012/09/a-real-update-on-the-progress-of-wayland-in-kwin-and-kde/">wrote a thank you blog about KWin and Wayland</a> and <a href="http://blog.lydiapintscher.de/2012/09/21/check-whats-happening-in-randa/">e.V. Board member Lydia said thanks too</a>. We are all very happy with the result! It is not just about the money—it also sends a message to us that you, our users, care. 

<h3>Why we do this</h3>
Our contributors put in serious effort, more than most people realize. Writing code is fun, yes, there is the scratching of itches but in Free Software it comes with responsibilities, duties (maintainership), chores (bug reports, testing), sometimes less than positive feedback and other things which are not fun. It involves giving lots of free time; it is not uncommon for KDE contributors to put in unpaid vacation days or make less hours at work to be able to contribute. Really, not publishing your code is so much easier.

We do this <strong>for you</strong>. Without users, most of us wouldn't bother. While not all of us are deep believers in the Free and Open philosophy, at the very least we appreciate its benefits, the opportunities for less fortunate folks who can't afford closed software or whose governments spy on them. Children are using Free Software to learn about the world, we hear about small companies growing big which wouldn't have been possible without the Source, and it makes us happy.

All of this says, "We care". And that's why it helps so much if <strong>you</strong> show you care, too! That makes our efforts all the more satisfying. Your money doesn't go to the poor shareholders of fruit companies in dire need of shiny new stuff. It goes to make the world a better place, one line of code at a time!

<h2>What's the Deal with these Sprints?</h2>
As was written in <a href="http://dot.kde.org/2012/09/17/who-randa">the Dot story "Who is Randa For?"</a>, sprints are <a href="http://dot.kde.org/2012/08/13/support-2012-kde-randa-meetings-inspired-and-intense">very important</a> for our community and for you, our users.

<div style="float: right; padding: 0ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/K3M.jpeg" /><br />KDE Multimedia meeting nostalgia</div>

The decision by <a href="http://ev.kde.org">KDE e.V.</a> to spend most of our funds on developer meetings was made for a reason—sprints have proven themselves. Your writer vividly remembers one of the first 'real sprints'—the KDE Multimedia meeting in 2006 in the Netherlands (<a href="http://dot.kde.org/2006/05/27/first-day-kde-4-multimedia-meeting">first day</a>, <a href="http://dot.kde.org/2006/05/28/second-day-kde-4-multimedia-meeting">second day</a>, <a href="http://vizzzion.org/?id=gallery&gcat=KDE4MultimediaMeeting">pics</a>). This meeting was a major factor in bringing the Amarok crowd into KDE. At that time, they were contemplating going Qt only for Amarok 2.0. Instead, we now count many of the Amarok hackers among our friends, see them at KDE events, and receive valuable code contributions for other parts of KDE development!

Another meeting which had a great effect on the future of KDE was the <a href="http://blog.jospoortvliet.com/2009/10/kde-marketing-and-promo-meeting.html">first marketing and promo sprint ever</a> in Stuttgart in 2009. This meeting energized the marketing and promo side of KDE and had a profound <a href="http://troyunrau.ca/node/18">impact on our communication</a>.

Since these 'early days' the number of sprints has increased. As shown <a href="http://community.kde.org/Sprints">on our sprint page</a>, there have been about 20 meetings each year since 2009.

<h2>How You Can Help: Spread the Word!</h2>
Sprints are expensive and the economy is tighter than it has been in past years. We are grateful to the people who supported the Randa fundraising effort. But it's not right to depend on the same few hundred awesome donors (including quite a few who are already active KDE contributors!). Instead, the challenge is to reach the millions of KDE users who didn't even know about this fund raiser! Many of those would be happy to support the free software they use every day—<strong>if they just knew how to help</strong>.

So, the next time we do a fund raiser (and we will!), please support the effort in another very important way: help spread the word about it! You'd be surprised how far you can reach with your network on Facebook, Twitter, Google+ or just by talking to people.

<h2>Join The Game</h2>
There are many <a href="http://community.kde.org/Getinvolved">opportunities to participate</a> in KDE for a variety of skill levels. And you can <a href="http://jointhegame.kde.org/">Join The Game</a> to become a more permanent supporter of KDE and to contribute financially without waiting for the next fund raiser. And any further <a href="http://pledgie.com/campaigns/18045">donations to this fundraiser</a> will go towards <a href="http://sprints.kde.org/">future sprint funding</a>. Last but not least, remember: <em><big>Spreading the word matters!</big></em>