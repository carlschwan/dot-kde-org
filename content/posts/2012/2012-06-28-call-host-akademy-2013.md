---
title: "Call For Host Akademy 2013"
date:    2012-06-28
authors:
  - "nightrose"
slug:    call-host-akademy-2013
---
The KDE Community is looking for a host for Akademy 2013.

Akademy is the annual gathering of the KDE Community, one of the largest in the world of Free and Open Source Software. At Akademy, KDE people gather to exchange ideas for development, plan for the future, and discuss other important issues. It is an extraordinary occasion for creativity, enthusiasm, commitment, close working relationships and innovation.

Hosting Akademy is a rare opportunity. The local hosting team plays a key and highly visible role in producing this event, being actively involved within the KDE community and with the local community as well. The hosting community gains financially with attendees visiting from all over the world. More importantly, there is an opportunity for the local community to benefit from the value of Free and Open Source Software and one of its premier organizations. People from local businesses, educators, students, government officials and technology enthusiasts are encouraged to attend and are warmly welcomed. Hosting Akademy 2013 has the potential to make a major difference for everyone involved. The opportunity is as big as the hosts make it.

<h2>Akademy</h2>
The conference program is usually structured as follows:
<ol>
<li>Friday is for travel, arrival and a pre-registration/get acquainted event.</li>
<li>On Saturday and Sunday, the actual conference takes place. There are 2 or 3 keynotes and 25 to 30 sessions in tracks so that attendees can choose according to their interests.</li>
<li>The remainder of the week is used for intensive coding and workshops for multiple smaller groups of 10 to 50 people.</li>
</ol>
The venue needs to offer a lecture room that fits 400+ people, a smaller lecture room for up to 200 people, and several smaller rooms for workshops.

<h2>Host Requirements</h2>
Akademy requires a location in or near Europe that is easy to reach, preferably close to an international airport. The conference venue itself should be equipped to host attendees comfortably, and provide reliable, high speed Internet access during the conference, workshops and hacking sessions. Ample hotel accommodations and places to eat should be close to the venue or easily reachable.

Organizing an Akademy is a demanding activity and requires a significant investment of time and effort. While there will be considerable assistance from people who have experience producing previous Akademies, the local team should be prepared to spend a lot of time on it. Previous hosting teams have found this to be a rewarding project which is highly recognized and appreciated by the KDE community. The benefits to KDE, the attendees and the Akademy hosts and organizers are immeasurable.

For more detailed information, please see the <a href="http://ev.kde.org/akademy/CallforHosts_2013.pdf">Call for Hosts</a> (pdf). In case of questions or concerns, please contact the Board of KDE e.V. at kde-ev-board@kde.org. Applications can be submitted until 15 September 2012.