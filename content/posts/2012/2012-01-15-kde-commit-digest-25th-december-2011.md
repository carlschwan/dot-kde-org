---
title: "KDE Commit-Digest for 25th December 2011"
date:    2012-01-15
authors:
  - "mrybczyn"
slug:    kde-commit-digest-25th-december-2011
---
In <a href="http://commit-digest.org/issues/2011-12-25/">this week's KDE Commit-Digest</a>:

<ul>
<li>Multiple changes in scripting in <a href="http://userbase.kde.org/KWin">KWin</a>: properties, interactive console to run scripts, import of a KCM</li>
<li>Start of generic clip job framework implementation for <a href="http://userbase.kde.org/Kdenlive">Kdenlive</a></li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, updates of MSOffice 2000/2003 and 2007 format support, multiple fixes</li>
<li>Volume normalization now works between <a href="http://amarok.kde.org/">Amarok</a> and iPods</li>
<li>Contact list in <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">Telepathy</a> moved to a separate class</li>
</ul>

<a href="http://commit-digest.org/issues/2011-12-25/">Read the rest of the Digest here</a>.