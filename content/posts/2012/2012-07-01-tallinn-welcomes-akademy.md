---
title: "Tallinn welcomes Akademy"
date:    2012-07-01
authors:
  - "jospoortvliet"
slug:    tallinn-welcomes-akademy
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;text-align: center"><a href="http://dot.kde.org/sites/dot.kde.org/files/ak2012-agustin.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-agustin-wee2.jpg" /></a><br />Community Keynote<br />Agustín Benito Bethencourt</div>
Yesterday, nearly 300 hackers grabbed their badges at the Estonian IT College in Tallinn and launched Akademy 2012. Mathias Klang's keynote, <a href="http://dot.kde.org/2012/04/22/akademy-keynote-dr-mathias-klang-freedom-expression">Freedom of Expression</a>, got things going quickly with his urgent call to action for those who stand for freedom, which often disappears gradually and in a slow creep.

Some people kicked off the conference on Friday night at the pre-registration party sponsored by Intel or even arrived earlier in the week to explore Tallinn and <a href="http://blogs.kde.org/node/4581">sample Estonia's beers</a>. During the first day of Akademy, there were sessions on release management, Plasma Active, and the future of KDE and Qt technologies.
<!--break-->
<h2>Keynotes and sessions</h2>
Saturday keynotes by Mathias Klang and Agustín Benito Bethencourt have already gotten some publicity on <a href="http://planet.kde.org">planet KDE</a> (a great place to follow Akademy action in near real time). Boudewijn Rempt posted <a href="http://www.valdyas.org/fading/index.cgi/2012/06/30#klang_keynote">quotes and a link to the Klang's slides</a>. There is also a report on <a href="http://blog.jospoortvliet.com/2012/06/keynote-by-agustin-benito-bethencourt.html">Agustín's Community Keynote</a>. Boudewijn also posted <a href="http://www.valdyas.org/fading/index.cgi/2012/06/30#how_open">some thoughts</a> about Mirko Böhm's <a href="https://plus.google.com/108138837678270193032/posts/YqUsX4Ug79d">thought-provoking talk</a> about openness and governance.

<div style="float: right; padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/ak2012-talks2.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-talks2-wee.jpg" /></a></div>

After Mirko and the GSoC/SoK student presentations came a session by our epic hero, Kévin Ottens, explaining the Frameworks 5 efforts, followed by David Faure talking about the current state of this work and where we need to go.

Jeroen van Meeuwen <a href="http://blog.jospoortvliet.com/2012/06/akademy-talk-about-release-management.html">talked about KDE's release management</a>. He criticized KDE's handling of bugzilla and proposed ways of improving things. And he's not just talking. Jeroen has taken up KDE's bugzilla maintainership. He plans to initiate cleanups and will work with the separate KDE teams to adapt bugzilla to support their work flows better. Marta Rybczyńska explained how developers can make the job of the Commit Digest Team easier and get their commits featured in the Digest.

After a walk to lunch in a forest setting and a tasty feeding time, Aaron Seigo painted the possibilities of A New Hope for Open Devices: the awesome Vivaldi tablet and what's to come. He was followed by Sebastian Kügler talking about the Device Spectrum vision of the Plasma team. Eva Brucherseifer spoke about the Contour User Experience, Plasma Active, and the future of mobile devices. Thomas Pfeiffer rounded this track out with a presentation about Plasma Active Human Interface Guidelines. A concurrent track featured Thiago Macieira, Quim Gil and another Thiago (Lacerda) together with Dakar Fernandes Pinheiro all updating the audience on the state of the Qt project and specifics on Qt 5. Michael Hasselman spoke about <a href="https://plus.google.com/108138837678270193032/posts/Se5yk6fPzZ4">Wayland and input method architecture</a>. In addition, Patrick Spendrin and Patrick von Reth presented Packaging KDE on Windows. 

<div style="float: right; padding: 1ex; margin: 1ex;text-align: center"><a href="http://dot.kde.org/sites/dot.kde.org/files/ak2012-party1.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-party1-wee.jpg" /></a><br />
<a href="http://dot.kde.org/sites/dot.kde.org/files/ak2012-karaoke.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-karaoke-wee.jpg" /></a></div>

<h2>Party time</h2>
During the first day people got their share of hacking and socializing, the social part of Akademy really took off on Saturday night. Blue Systems sponsored a party at Rock Cafe. It was definitely a party place. But there was room for chatting and getting further acquainted. A local band provided rock music from Estonia, which led naturally into the KDE karaoke tradition. At the end of the party, everyone was on stage to sing the final song "Imagine".

<h2>Directions</h2>
As expected, Akademy Tallinn 2012 is a fun and productive event. There's a focus on looking ahead, on the future, on the human side of the project. This generates energy and enthusiasm. Today, looking at the program, there are discussions about how to actually get there. When the BoF sessions start, the real work will begin.

<em>Photos by Matthias Welwarsky (CC BY-SA)</em>