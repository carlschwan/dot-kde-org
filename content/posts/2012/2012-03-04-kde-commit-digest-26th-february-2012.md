---
title: "KDE Commit-Digest for 26th February 2012"
date:    2012-03-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-26th-february-2012
---
In <a href="http://commit-digest.org/issues/2012-02-26/">this week's KDE Commit-Digest</a>:

<ul><li>In <a href="http://www.calligra-suite.org/">Calligra</a>: Kexi's welcome status bar fetches GUI updates from the server if available; WPS file support; updates and fixes in the text undo/redo framework</li>
<li>Improved synchronization view in <a href="http://userbase.kde.org/Lokalize">Lokalize</a></li>
<li>Improvements in C++ and C++11 in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li>More configurable tab and window title in <a href="http://konsole.kde.org/">Konsole</a></li>
<li>Stepped script execution implemented in <a href="http://userbase.kde.org/Rocs">Rocs</a></li>
<li><a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a> has improved support of cvs import</li>
<li><a href="http://www.last.fm/help/faq?category=Scrobbling">last.fm scrobbling</a> support in <a href="http://developer.kde.org/~wheeler/juk.html">JuK</a></li>
<li>PrintManager QML plasmoid is available</li>
<li>FFT-based (fast Fourier transform) correlation has been implemented in <a href="http://userbase.kde.org/Kdenlive">Kdenlive</a></li>
<li>Debugger added to TimetableMate in <a href="http://publictransport.horizon-host.com/">Public Transport</a></li>
<li>The KgDifficulty class from project Tagaro has been imported to kdegames</li>
<li>Speed up maildir listing and syncing in <a href="http://pim.kde.org/">KDE-PIM</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-02-26/">Read the rest of the Digest here</a>.