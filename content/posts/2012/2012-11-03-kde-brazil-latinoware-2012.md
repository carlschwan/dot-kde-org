---
title: "KDE Brazil at Latinoware 2012"
date:    2012-11-03
authors:
  - "araceletorres"
slug:    kde-brazil-latinoware-2012
---
Just as another year is passing by and summer is on its way, KDE Brazil gathered and fled south, just like migratory birds. Dragons are like birds, you know? Konqi the KDE Dragon is a little bigger than a bird, but he still has wings. We all had a common destination in this trip, the beautiful <a href="http://en.wikipedia.org/wiki/Itaipu_Dam">Itaipu</a>, the biggest water-powered power plant in the Americas. (It used to be the biggest in the world, but a bigger one was built in Asia last year.)

Why go to Itaipu? A power plant controlled by two countries? And what does that have to do with dragons? Well ... we simply couldn't miss the 2012 LatinoWare conference.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEBrKonqi.jpg" /><br />Konqi at LatinoWare</div>

The conference had over four thousand attendees, and KDE was represented by the cute KDE Brazil team—xiu. The rest of the KDE- LatAm people, we missed you. =(  We had talks, courses and a pretty booth like in previous years when we attended the LatinoWare conference.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/LatinoWareTeam.jpg" /><br />KDE Brazil team</div>

<h2>KDE Presentations</h2>
Our group presented four talks:

<strong>* Contributing to KDE without writing code - by Aracele Torres</strong>
&nbsp; This talk was interesting not only to novices but to people that are used to Free Software since there's still a bit of mystification on how to help KDE. Everybody can help, but what are the barriers?

<strong>* Multiplatform Education Software - by Filipe Saraiva</strong>
&nbsp; Introducing a lot of good apps, apps that are changing the world as we know it. Culture is not just about books but about knowledge aggregation. If we make knowledge free (like in freedom), culture will be free as well, education will be available to more people and technology can help spread it.

<strong>* You, University and Science Applications - by Filipe Saraiva</strong>
&nbsp; Very handy applications that can boost the desire to learn, search and research. We need to know the basics, and we are also eager for more knowledge. One thing that will never stop revealing itself is science. Let's research the world, let's research outside our boundaries, let's use that knowledge to build great KDE software too :D

<strong>* Introduction to QML - by Lamarque Souza</strong>
&nbsp; A short course about QML application development

<strong>* Developing QML widgets for Plasma - by Sandro Andrade</strong>
&nbsp; QML is the new child in town and can be run on a lot of devices, it is easy to learn, memory-smart and allows for pretty sexy code to be created. Sandro Andrade demystified the use and creation of Plasmoids using Plasmate and also presented several Plasmate tricks.

<h2>LatinoWare Future</h2>
KDE Brazil has consolidated its presence. Next year we will celebrate 5 years of KDE Brazil presence at LatinoWare with something special. LatinoWare is an important event because it brings together people of all Latin America countries and strengthens ties between KDE Latin American communities. It is also a place where we can meet many beginners in the free software world. People who are willing to learn and get to know about the topic. In short, it is a good place to promote the KDE project. ;-)