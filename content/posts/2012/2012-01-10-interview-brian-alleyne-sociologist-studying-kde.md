---
title: "Interview with Brian Alleyne, Sociologist Studying KDE"
date:    2012-01-10
authors:
  - "oriol"
slug:    interview-brian-alleyne-sociologist-studying-kde
comments:
  - subject: "Article text?"
    date: 2012-01-10
    body: "Is the Sociology article available to us, the innocent research subjects?"
    author: "Bille"
  - subject: "article here ..."
    date: 2012-01-10
    body: "see here for full text of paper ... \r\n\r\nhttp://goldsmiths.academia.edu/BrianAlleyne/Papers/1110573/Challenging_Code_A_sociological_reading_of_the_KDE_Free_Software_project\r\n"
    author: ""
  - subject: "See:\nhttp://goldsmiths.academ"
    date: 2012-01-10
    body: "See:  \r\nhttp://goldsmiths.academia.edu/BrianAlleyne/Papers/1110573/Challenging_Code_A_sociological_reading_of_the_KDE_Free_Software_project\r\n"
    author: ""
  - subject: "\\o/"
    date: 2012-01-11
    body: "@Brian:\r\n\r\nGood to have another British KDE enthusiast!\r\n\r\nBe sure to keep us updated with any future relevant papers. "
    author: ""
  - subject: "As a PHD Student you learn how to do this..."
    date: 2012-01-11
    body: "http://eprints.gold.ac.uk/4362/1/Challenging_Code_-_A_Study_of_the_KDE_Free_Software_Project.pdf"
    author: ""
  - subject: "Wonderful interview, and"
    date: 2012-01-11
    body: "Wonderful interview, and awesome to see involvement from you to bridge the various worlds together. Looking forward to more insights/results that come out of something like this! :D"
    author: ""
  - subject: "Not a democracy"
    date: 2012-01-11
    body: "KDE is a technocracy/meritocracy. A developer can push the project into a new direction simply by working on something. If a certain feature is being implemented is only based on decisions by developer and not users.\r\n\r\nIn a democracy every user would have the equal say about the direction of the project and that's clearly not the case."
    author: ""
  - subject: "Democratic is not the same as democracy"
    date: 2012-01-11
    body: "Saying a community is democratic does not mean that everything is determined by a vote. Rather, it means that each member has equal opportunities: anybody can become an important contributor and affect the direction of the project.\r\n\r\nCases where a meritocracy might not be democratic could be when a newcomer couldn't become a contributor because of nonexistent documentation, inaccessible code or dismissive attitudes of main developers. Might be the case in some KDE projects but many seem to feel that KDE is democratic in general."
    author: "Tuukka"
  - subject: "Depends on who is considered part of the democracy"
    date: 2012-01-11
    body: "Nope, I do not think that KDE is no democracy. Just because you're using American products you would never be permitted to vote in the US. And you wouldn't say the US is no democracy (although it's ... peculiar at times to Europeans ;) ).\r\n\r\nInside KDE, there is a lot of spontaneous democracy: Everyone is allowed to speak up, contribute his/her opinion on a topic and very often they are also heard - independently of their origin, their capabilities in programming and so on. You just have to be somewhat engaged, which is a small price. KDE's openness to anyone and anyone's opinion makes it a democracy - maybe not strictly speaking in its structures, but in the way it's structures are filled with life. Thats probably the difference between a sociological and a political/institutional approach to defining democracy."
    author: ""
  - subject: "Democracy and meritocracy"
    date: 2012-01-11
    body: "My sense, as an observer, is that KDE is democratic in general. In the paper that follows the one mentioned in the interview, I am looking at Jono Bacon's defence of the Ubuntu 'meritocracy', and the whole debate around governance in Ubuntu. I always suspected that the role of 'Self Appointed Benevolent Dictator for Life' was going to irritate many in a largely voluntaristic community. A lot to think about here ..."
    author: ""
  - subject: "I will certainly do!"
    date: 2012-01-11
    body: "I will certainly do!  "
    author: ""
  - subject: "Thanks! That article is just"
    date: 2012-01-11
    body: "Thanks! That article is just the beginning, I have a lot more work in progress and planned. Watch this space :)"
    author: "brian-alleyne"
  - subject: "My sense is also that KDE is"
    date: 2012-01-11
    body: "My sense is also that KDE is also a democracy: even if a direct one. In my next paper I am looking at a number of debates, including one around democracy and meritocracy in FLOSS projects. I always suspected that the role of 'Self Appointed Benevolent Dictator for Life', while cute, funny and ironic, was going to irritate many people, especially when the balance between benevolence and dictatorship swings toward the latter."
    author: "brian-alleyne"
  - subject: "London"
    date: 2012-01-11
    body: "If you're ever in need of a London-based victim, err research subject, drop me a line at jlayt@kde.org."
    author: "odysseus"
  - subject: "A single developer can _try_"
    date: 2012-01-11
    body: "A single developer can _try_ push the project in a single direction, but without the agreement(or at least the indifference) of the wider dev community they won't get very far.  So KDE is a democracy of developers, not of users, which operates on the principle of consensus.\r\n"
    author: "odysseus"
  - subject: "Akademy 2012"
    date: 2012-01-11
    body: "Great interview! Brian, you should come to Akademy this year :)"
    author: ""
  - subject: "KDE is whatever-cracy, FLOSS is libertarian"
    date: 2012-01-11
    body: "KDE can be whatever, but whole FLOSS world is more libertarian or anarchists than democratic or meritocratic or something else.\r\n\r\nIt doesn\u2019t matter if KDE is democratic or what. You can skip from KDE to e.g. Gnome and you are not obliged to leave your old PC with all data inside."
    author: ""
  - subject: "Word up!"
    date: 2012-01-12
    body: "I would like to second the comments above, namely:\r\n\r\nTue, 2012/01/10 - 11:34pm \u2014 Anonymous\r\nWonderful interview, and awesome to see involvement from you to bridge the various worlds together. Looking forward to more insights/results that come out of something like this! :D\r\n- and to it, also add: \r\n\r\nWed, 2012/01/11 - 1:09pm \u2014 odysseus\r\nScore: 0\r\nLondon\r\n\r\nIf you're ever in need of a London-based victim, err research subject, drop me a line at jlayt@kde.org.\r\n\r\nThen say, \r\n\"If you are in need of a research subject - (from the other side) then drop me a line at\", cumminsl10290@hotmail.com\r\n\r\nGreat work dude!\r\nContinued and increased success in all that you undertake."
    author: ""
  - subject: "I hope to!"
    date: 2012-01-12
    body: "I hope to!"
    author: "brian-alleyne"
  - subject: "meeting in London"
    date: 2012-01-12
    body: "Soon, I hope to meet and interview KDE people in London."
    author: "brian-alleyne"
  - subject: "=D"
    date: 2012-01-13
    body: "Awesome... KDE, as always, rocks! "
    author: ""
  - subject: "Freenode channel"
    date: 2012-06-11
    body: "There's a ##sociology channel on irc.freenode.net if anyone wants to lurk and see if it can develop."
    author: ""
---
A few months ago, the British journal <i>Sociology</i> published an article titled <a href="http://soc.sagepub.com/content/45/3/496.abstract">"Challenging Code: A Sociological Reading of the KDE Free Software Project"</a>. Eager to find out what a 'sociological reading' of KDE entails, Dot editor Oriol Mirosa rushed to contact the article's author, sociologist Brian Alleyne, who graciously and patiently agreed to be the subject of an interview. Read on to learn more about Brian, sociology, and the significance of KDE for the social sciences:

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/BrianAlleyne-sml.jpg"/><br/>Sociologist Brian Alleyne</div><b>Oriol:</b> Hi Brian. Can you start by telling us a little bit about yourself?

<b>Brian:</b> I live and work in London. I grew up in Trinidad in the Caribbean. I come from a family with a long history of migration, so I like to think of my 'routes' rather than 'roots'. I am currently a Senior Lecturer in Sociology at Goldsmiths, University of London. I teach social theory, research methods, and global development issues to undergraduates and postgraduates. From January 2012 I will teach a new short course on Free Software cultures and practice, aimed at non-technical postgraduate students. My research is currently in two connected areas; first, on narratives,  i.e. studying and using stories in order to better understand the social world; and second, on various aspects of information society, but especially Free, Libre and Open Source Software – FLOSS.

<b>Oriol:</b> What do sociologists do?

<b>Brian:</b> Sociologists are interested in understanding human life in the collective sense. A key question for us is: what are the structures and processes that enable and constrain social life? We take any and all aspects of social life as potential material for investigation, from language and culture to technology and politics. We use a wide of range of methods to generate data, from close observation, to interviews, to complex statistical analysis. We treat the everyday as if it were strange, and we seek to make the seemingly strange more accessible. People who do not understand or like what we do often accuse us of writing long articles on trivial matters. Well, they miss the point: the everyday social world as we experience it is far from trivial, it is actually much more than its surface appearance. Think of the web of social relations and culture (apart from the technology in the strict sense) that is implied in sending  out text messages to arrange to meet friends for drinks.

<b>Oriol:</b> How did you become interested in free software and KDE?

<b>Brian:</b> We sociologists never give short answers (we are like <a href="http://en.wikipedia.org/wiki/Ent">Tolkien's Ents</a>). So by way of background to my encounter with KDE, I was a geek before becoming a sociologist and I proudly remain a geek. I first got involved in the personal computing scene in 1982, when I got my first machine (and yes, I did  have an Amiga, back in the mid 1980s). I learned to program in BASIC, Turbo Pascal, modula-2, and even dabbled a little in C. I first encountered Unix around 1986 and worked with it for a year or so: my job involved tailoring an Informix database for an insurance broker, and then I had to modify an accounting package that ran on a version of Unix - Xenix I think it was called; there was no IDE, I had to work on the code in vi. (I have painful memories of working with RPG II on IBM minicomputers: the less said about that the better). After working as a programmer and computer tutor for a few years, I went to university in 1988 to study Sociology. I've been in university ever since.

When I look over my personal and research journals of the past 10 years, I find that there are hundreds of entries on my experience with Linux and KDE. I first encountered Linux itself in 1999 in the form of Caldera Linux, on which I seem to remember there was an early version of KDE. I then moved on to Mandrake and SUSE in the early 2000s, on both of which I found KDE to be the desktop environment that was to my liking. I did my PhD at the University of Cambridge, where people referred to the great rival – Oxford – as 'the other place'; by way of analogy, I never took to the 'other desktop environment' :) I have been using Linux for about half of my computing since 2004, and have always used KDE software. So, yes I am a KDE user. As a way of tackling the learning curve, I used KDE software on Mandrake and SUSE almost full-time for a couple of years to do my regular work as an academic: writing lectures and papers, preparing graphics, emailing and web browsing/archiving. It was enlightening and invigorating (except when I was fiddling with LILO and battling with printer drivers). I studied the Qt toolkit for a bit and planned to learn PyQT and PyKDE to get back into coding, but have not found  the time. I am currently thinking through what role I can play on becoming an official KDE member.

<b>Oriol:</b> As a sociologist, how did you become interested in KDE as a research topic?

<b>Brian:</b> It was always a dream of mine to combine the two main intellectual interests of my life: sociology and computing. KDE software as an open technology allowed me to look under the hood, but the main reason I chose it as a research topic was because of the excellent online community and resources. The project web site is a superb resource for a researcher. I also learned a great deal about the human side of KDE from the <a href="http://www.behindkde.org/">'People behind KDE'</a> series. The biographical sketches were a gold mine for someone like myself, because ever since my PhD research, I have used <a href="http://srmo.sagepub.com/view/the-a-z-of-social-research/n6.xml">biographical methods</a> (i.e. the in-depth study of an individual's life in order to understand particular social processes) in my work. 

As a sociologist I am very interested in the idea of community, in how people come to understand themselves as belonging to some community and how they determine who is in and who is out. I have worked on questions of community for some years now. I was struck by how often the term community came up in FLOSS discussions, and in KDE more specifically. Even as I was using KDE software on a regular basis, my sociological half was wondering: What is the KDE community? Who is in it? What are the kinds of activities that are characteristic of FLOSS communities? Are there social structures as sociologists understand these, e.g. class? Is there some distinctive culture involved, e.g. use of language? What kind of knowledge is produced and valued in the KDE community? How does one learn to operate in the community? How is the community governed?

<b>Oriol:</b> You just published a paper about KDE titled "Challenging Code: A Sociological Reading of the KDE Free Software Project." What is the main argument you are making in that paper?

<b>Brian:</b> Well, that paper is the first in a series. My aims were to bring FLOSS into mainstream (British) sociology through discussion of a project, and I also wanted to consider the KDE project from sociological theories of activism and social movements. My basic argument is that the social significance of KDE, as FLOSS more generally, is in its making a critical intervention into the economic and social relations of software production and use. KDE, as a radically democratic and open project, in rejecting the norms of proprietary software, is not just about the politics of software, but is in fact an example of a broader potential shift in society and economy towards openness and sharing. It is far too soon to know how this will all turn out, but I want sociologists to pay more attention to FLOSS because it is a pivotal space in our world and is likely to increase in significance.

The overall framework I adopted in the paper was a theoretical perspective on social movements, which sees them as a 'cognitive praxis': a way of generating and applying knowledge in the pursuit of a particular political project (political here refers broadly to any activity in which there are different, opposed ideas and objectives, for which some kind of conflict resolution is required). I think that this framework was well suited to analyze KDE as a project.

There is lot of fantastic material written on FLOSS (some of which I discuss  in my paper) but even if you're not a hacker, you at least need to be a geek to follow a lot of it, e.g. Raymond's The Art of Unix Programming assumes a fair bit of technical knowledge. I wanted to discuss a project that non-technical students of social science could grasp. But that project had to be a big part of the FLOSS world. KDE fit the bill, it ticked all of my boxes, it was a project and software suite that I had been using for several years. I try to act as a kind of translator between the world of free and open source software as represented through KDE, and that of sociologists.

<b>Oriol:</b> How did you do your research on KDE?

<b>Brian:</b> My work is what we call ethnographic, a term borrowed from anthropology; an ethnography - as the Greek roots imply - is a written account of a social and cultural milieu, or community. To produce an ethnography, a researcher immerses him/herself in the world they wish to study for an extended period of time. By using KDE software for my own work over several years, I became immersed as a user. Then I followed the many mailing lists and forums, and more recently, various blogs and podcasts. I collected interviews that had been done with key figures. My main face-to face encounter with the project was at <a href="http://akademy2007.kde.org/">Akademy 2007</a> in Glasgow, Scotland. There I had many informal talks with people. Because of my interest in narrative text, I have so far focused on the material published on the Web. In the future I hope to do more interview and observation work.

<b>Oriol:</b> How has the KDE community responded to your interest in researching it?

<b>Brian:</b> The people I have discussed  my work with have been very helpful, if not always sure what sociologists do!

<b>Oriol:</b> What are your future plans for your research? Do they include KDE?

<b>Brian:</b> I have just completed a paper on hacking, in which I take an overview of the various forms: clandestine, FLOSS hacking, hacktivism, and hardware hacking. I am just finishing up another paper,  on 'Ideas of community in FLOSS worlds', in which I apply sociological theories of community to the many articulations of community in FLOSS; as empirical material I examine the recent forking of Mandriva to Mageia as a case of community conflict, and I look in-depth at Jono Bacon's book, The Art of Community (I take Bacon to task for his dismissive attitude toward social theory!). Beyond that I hope to do more work on KDE through interviews and participation.

<b>Oriol:</b> In the course of your research and in your experience as a user, have you found out anything about KDE that could be useful for the KDE community to consider?

<b>Brian:</b> I think the documentation needs to be much better. Having said that, this is one of the areas where I hope to contribute in the future. 

<b>Oriol:</b> What do you like best about KDE software? What are your favorite applications?

<b>Brian:</b> I like the ease of customization of just about everything. I like the visual style overall and  the consistency of visual style across applications. Most exciting to me is the promise of <a href="http://dot.kde.org/2011/09/21/nepomuk-stability-and-performance">Nepomuk</a>: I think <a href="http://trueg.wordpress.com/">Sebastian Trueg</a> has done wonderful work. I look forward to the full realization of  the semantic desktop, which will be a big benefit for the kind of work that I do, as for many academics in the social sciences and humanities.

My favorite KDE desktop application is <a href="http://amarok.kde.org">Amarok</a>. Overall, I really like the way <a href="http://kde.org/workspaces/plasmadesktop/">Plasma</a> has developed. I am keeping an eye on <a href="http://owncloud.org/">ownCloud</a> as well.

<b>Oriol:</b> Do you have any final thoughts that you would like to share with the readers of the Dot?

<b>Brian:</b> I am truly excited to be carrying out research on as rich and vibrant a project as KDE. 

<b>Oriol:</b> Thank you very much for your time and insights!