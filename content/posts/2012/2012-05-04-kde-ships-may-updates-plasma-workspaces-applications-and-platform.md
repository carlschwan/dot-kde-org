---
title: "KDE Ships May Updates to Plasma Workspaces, Applications and Platform"
date:    2012-05-04
authors:
  - "sebas"
slug:    kde-ships-may-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "kick out D-BUS and go back to DCOP"
    date: 2012-05-04
    body: "Long time ago, the war between OpenLook and OSF-Motif was over and the winner became MS-Windows, I installed CDE on my UNIX workstation. CDE seemed to be a merger of mostly OSF-Motif and some nice OpenLook apps. Along with some extensions, nasty ones.\r\n\r\nThat was a new RPC-like server that acts as a bus between the X11 applications. I did not really figure out its real intention, but it left a sticky desktop, sometimes the desktop froze or locked up. That was a new behavior to X11 users. X11 was not fast on those old computers, but before this there had not been screen lockups typical of MS-Windows.\r\n\r\nMS-Windows became the standard OS and there was no choice. A group of Linux enthusiasts created a new desktop-gui called KDE.\r\n\r\nKDE became a wonderful alternative to MS-Windows. There were no lockups. The system ran fluently and was quite robust, even if an app got stuck on your desktop.\r\n\r\nWith KDE 4.7 or 4.8, somebody made some mistakes like the ones in CDE and introduced D-Bus. And hello ... the desktop lockup is back again.\r\n\r\nToday, there is a beautiful GUI that is not as usable for me as it once was, because it's slower and unstable. I am using 4.8.2, and it seems to me that KDE lost one of its impressive advantages over MS-Windows\u2014its impressive GUI with fluently non-blocking execution. \r\n\r\nA long time KDE Fan (since Version 1.x) is going to\r\nsay goodbye to KDE.\r\n\r\n\r\n\r\n\r\n\r\n\r\n"
    author: ""
  - subject: "Well, if you have a bad"
    date: 2012-05-04
    body: "Well, if you have a bad experience with KDE and deem it not worth to stick with it or help fixing whatever bug you run into, it is fine with me, if you move on to another DE or even operating system. However, I would like to point out that you really don't know what you are talking about. Are you sure you know what DCOP and D-BUS are? D-Bus was NOT introduced \"with KDE 4.7 or 4.8\", nor was it ever used by CDE. D-Bus is used in KDE since about four (!) years. Surely, you must have some issues with your computer, but rest assured that they have nothing to do with the move from DCOP to D-Bus."
    author: ""
  - subject: "Hello KDE"
    date: 2012-05-04
    body: "\"A long time KDE Fan (since Version 1.x) is going to\r\nsay goodbye to KDE.\"\r\n\r\nI guess you can't win them all for the time being, can you? Best of luck with Windows.\r\n\r\nOn another note, KDE 4.8.x is being welcomed by the many who are returning back after trying the alternatives. You will too.\r\n\r\n\r\n"
    author: ""
  - subject: "D-Bus blocking"
    date: 2012-05-04
    body: "If you get blocking with D-Bus then it's pretty much the application's fault for using synchronous D-Bus calls which are not recommended. Using them is the same as calling into any external function that doesn't return immediately - in the absence of threading your application will become unresponsive. FYI I think there was a bug fixed recently in kded and/or apper where a synchronous D-Bus call was being used, resulting in the interface blocking.\r\n\r\nPlease don't make out that this is a fundamental problem with D-Bus, because that's just not true."
    author: "bluelightning"
  - subject: "you  missed something"
    date: 2012-05-04
    body: "dbus was introduced in KDE 4.0, not 4.7... So your theory doesn't hold water. If KDE 4.7 or 4.8 is slower for you than 4.6, something else has changed."
    author: ""
  - subject: "Okay, I still have the problem"
    date: 2012-05-04
    body: "It seems that I was wrong with my assertion that D-BUS was introduced in 4.7 or 4.8. I noticed it beginning with 4.7 & Fedora 16 shipped with KDE.\r\n\r\nAnyway, I know the difference between DCOP and D-BUS. DCOP is sent by X11 wire. D-BUS has its own server so another socket connection has to be established.\r\n\r\nSo this is exactly the same technique that was introduced by CDE, which had a dselect or dsocket server. A CDE Client held a connection to the X Server and also to the other one.\r\n\r\nNow KDE has exactly the same visible misbehavior that CDE had. The new service sometimes is starving with the viewable result that the X11 connection cannot be served.\r\n\r\nFor the application, it's  easier to deal with one connection and to reuse the X11 transport. D-BUS was a step backwards.\r\n\r\n"
    author: ""
  - subject: "DCOP is dead."
    date: 2012-05-05
    body: "DCOP is dead. It hasn't been used in KDE since 4.0 four years ago. KDE has switched to DBUS, DCOP isn't coming back. Which means that you have used it for three or four years without any problem.\r\n\r\nD-Bus is a widely accepted standard of the Linux desktops. Many projects like Networkmanager or telepathy use it, so it's a fixed part of most Linux desktops. Even if you drop KDE, you won't find a desktop that won't use DBUS.\r\n\r\nKDE no longer works only on X. DCOP couldn't support it.\r\n\r\nYour problem are very likely caused by some other problem. There are many users with systems that use D-Bus and they don't complain.\r\n\r\nHave you tried any forums?"
    author: ""
  - subject: "zeromq"
    date: 2012-05-06
    body: "I have another idea, dump dbus and dcop and just use zeromq (IPC,INPROC)"
    author: ""
  - subject: "I did not complain about it,"
    date: 2012-05-22
    body: "I did not complain about it, yet. But D-Bus calls are slow if the system load is high, making my desktop sluggish. \r\n\r\nSo I can confirm this report to some degree.\r\n\r\nWith high system load and heavy IO, a D-Bus call to just show a window can take several seconds to prompt a reaction. That\u2019s when I turn to top instead of alt-F2 to get system status."
    author: "ArneBab"
  - subject: "PS: for me, 4.7 is not slower"
    date: 2012-05-22
    body: "PS: for me, 4.7 is not slower than 4.\u22646."
    author: "ArneBab"
---
Today KDE <a href="http://kde.org/announcements/announce-4.8.3.php">released</a> updates for its Workspaces, Applications, and Development Platform. These updates are the third in a series of monthly stabilization updates to the 4.8 series. 4.8.3 updates bring many bugfixes and translation updates on top of the latest edition in the 4.8 series and are recommended updates for everyone running 4.8.2 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
<!--break-->
