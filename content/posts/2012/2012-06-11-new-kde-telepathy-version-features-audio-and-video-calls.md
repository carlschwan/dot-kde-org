---
title: "New KDE Telepathy version features audio and video calls"
date:    2012-06-11
authors:
  - "mck182"
slug:    new-kde-telepathy-version-features-audio-and-video-calls
comments:
  - subject: "Awesome"
    date: 2012-06-11
    body: "Nice to see the progress. Hope it soon will be a full Kopete replacement and more. It appears like there is not much missing. "
    author: "mark"
  - subject: "Great !"
    date: 2012-06-11
    body: "thats great,I wish to see all the KDE tightly integrated into plasma to get a solid desktop and consistancy."
    author: ""
  - subject: "Is there gpg encryption"
    date: 2012-06-11
    body: "Is there gpg encryption support for XMPP again as it had been in Kopete until they completely replaced it by OTR encryption?\r\nActually, this is one of the reasons I prefer Psi at the moment."
    author: ""
  - subject: "Wishlist."
    date: 2012-06-11
    body: "1. Ivan Cukic, where are your rotten Telepathy patches for Lancelot? Now it's time for them, but it seems Lancelot for KDE 4.9 won't compile with them. Can you tweak and finally release a Telepathy-KDE ready Lancelot?\r\n\r\n2. Nepomuk integration. It's in the backburner, but how much we will have to wait for it? ;)"
    author: "Alejandro Nova"
  - subject: "Ready?"
    date: 2012-06-11
    body: "Looks ready for public use as far as I'm concerned. If it does all the things that google chat does, then it's good enough.\r\nfor most casual users.\r\n\r\nOr is there something I didn't notice?"
    author: ""
  - subject: "Great!"
    date: 2012-06-11
    body: "Wow you are heading towards the right position. It is very integrated and easy to use and the new plasmoid for quick chat is very handy. It is somehow rough for now but still very usable. \r\nSome rough edges include the fact that the quick chat plasmoid doesn't focus input so you need one more click and that when you have new message using enter opens it and can't be used otherwise. \r\nIs there a way to have sound notifications along with quick chat plasmoid?\r\nStill KDE's IM is progressing quickly in one of the best ones around. Thanks for this release."
    author: ""
  - subject: "Quality"
    date: 2012-06-12
    body: "What's the quality of audio/video compared to skype?"
    author: ""
  - subject: "why so many plasmoids?"
    date: 2012-06-12
    body: "there is a \"quick chat\",  a \"contact list\", an \"IM contact\" and an \"IM presence\" plasmoid. \r\n\r\nAre there plans to make one flexible plasmoid out of them all? Currently when you move the contact list into a panel it's not usable, the quick chat is invisible until needed and the status opens the the contact list window.\r\n\r\nA possible combined \"telepathy plasmoid\" could look like the \"presence plasmoid\" when moved into a panel, unfold into the \"contact list plasmoid\" and display the \"quickchat\" when needed.\r\n\r\nWouldn't that be more convenient than having 4 single purpose plasmoids?"
    author: ""
  - subject: "Because we have a plan."
    date: 2012-06-12
    body: "> Wouldn't that be more convenient than having 4 single purpose plasmoids?\r\n\r\nYes. But if you think about it, they are all still separate components. And Plasma actually enables us to /very easily/ couple those together. So for now, we're developing them separately and making sure they are all the top quality on their own level. Once we're there, it's really easy to put it all together :)"
    author: "mck182"
  - subject: "Sound notifications"
    date: 2012-06-12
    body: "Thanks for your comment!\r\n\r\nTo have sound notifications, open up System Settings, go to Application and System Notifications and there you can find Instant Messaging in the \"Event source\" and there you can find \"Incoming message\" :)"
    author: "mck182"
  - subject: "Sound notifications"
    date: 2012-06-13
    body: "Sound notifications are working just fine without the chat plasmoid. When using chat plasmoid though they are disabled and can't be enabled. I asked in #kde-telepathy and it was said that this aspect was not taken into account and I was prompted to open a bug report which I did.\r\nThanks anyway and keep up the great job."
    author: ""
  - subject: "Depends..."
    date: 2012-06-13
    body: "Given that it's xmpp only at the moment, the (current) answer to your question probably is largely \"it depends on who your xmpp provider is\". When SIP (which is more P2P, like Skype) is more supported, I would guess the bottle necks will move more towards your bandwidth and client software (ie telepathy).\r\n\r\nI am not an expert on either xmpp / sip :)"
    author: ""
  - subject: "Idea:"
    date: 2012-06-16
    body: "Why don't you guys gather and make a fundraising effort to backup open-sourced cross-platform (at least linux and android) voip solution? There is such a mess out there. It would be good to have an alternative to skype. Especially when skype is going to be adware."
    author: ""
  - subject: "I'd prefer if they were kept separate"
    date: 2012-07-13
    body: "I agree with the component-based approach and I think you should stick with it because it makes it easy to change out components, make changes to one without affecting the others and just keep things clean on a coding/design level. \r\n\r\nThat said a container plasmoid that brings everything together would not hurt.\r\n\r\n"
    author: ""
  - subject: "quick chat"
    date: 2012-07-15
    body: "I've just updated to 0.4, added a quick-chat plasmoid, and it worked properly, display a message notification and receiving/sending messages. \r\n\r\nBut then I clicked on the baloon icon to see what it is for, and the conversation was moved to a normal window. The problem is that I can't get the plasmoid to work again now. When I get messages, I receive the same old notifications and I'm able to chat only via window. I tried reinstalling the component, but quick-chat plasmoid seems to be dead.\r\n\r\nWhat can I do?\r\n\r\nThanks"
    author: ""
  - subject: "contact list"
    date: 2012-07-15
    body: "Also, it would be nice to have a filter box in the contact list plasmoid, as well as some options like sorting contacts. Finally, it could open the conversation with the quick chat plasmoid instead of with the normal window if it is present at the workspace, or at least offer the option."
    author: ""
  - subject: "trouble shooting works best in forums"
    date: 2012-07-15
    body: "\"What can I do?\"\r\n\r\nGo to forum.kde.org.\r\n\r\nSpecifically the KDE Telepathy section\r\nhttp://forum.kde.org/viewforum.php?f=230\r\n\r\nThat works better than this news channel for dealing with problems. It's more useful for other users too."
    author: "kallecarl"
  - subject: "Re: Idea"
    date: 2012-07-18
    body: "This is a strong idea. With MS onboard, Skype is doomed, and the huge uptake of Android means a vast swathe of users who are undoubtedly eager to jump to a stable, ad-free client. I vote we go for it."
    author: ""
  - subject: "xxx"
    date: 2012-09-11
    body: "No support in Lancelot launcher "
    author: "xxx"
  - subject: "Could you possibly help me running it?"
    date: 2012-09-14
    body: "I have openSUSE 12.2 with the unstable whatever telepathy repository which installs 0.5, not the git stuff.\r\n\r\nI've added \"a bunch\" of ktp and telepathy packages (I don't get why they don't pull all of the necessary stuff to behave correctly as dependencies) and I can run the contact list both as plasmoid and as \"application\" but when I click some name I get Handler no longer available for org.freedesktop.Telepathy.Error\r\n\r\nI have no idea what I may be missing and there's no meta package. I haven't installed the file transfer, vnc and ssh stuff but the rest should be there (and it would had made sense if text-ui brought in what it needed or something, I suppose.)\r\n\r\nFeel free to tell me a zypper install line for all the packages I should have or something such.\r\n\r\nAnd mail me =P"
    author: "Johan Kr\u00fcger Haglert"
  - subject: "+1"
    date: 2013-10-24
    body: "+1"
    author: "qqq"
---
After four months full of work, the KDE Telepathy team is back with another release. Version 0.4 of the Instant Messaging suite for KDE Workspaces now supports making audio and video calls right from the desktop. It also adds the ability to browse chat logs. We focused strongly on stability and performance, so most of the improvements are "under the hood". In addition, work was started to have a Kopete log importer in the next release. 
<div><img src="http://dot.kde.org/sites/dot.kde.org/files/Telepathycall_uiCropt544px.png" /></a><br /><big><strong>User Interface</strong></big></div>

<h2>Plasma Integration Tech Preview</h2>
Groundwork was done for further Plasma integration, so tech previews of new Plasma components can be tested:
<ul>
<li>a Chat Plasmoid allows you to communicate without switching windows. It sits in your panel and pops up with new messages.</li>
<li>a Contact List Plasmoid shows the presence of your buddies and lets you start a chat with them.</li>
<li>the KRunner interface makes it easy to search for contacts and start chats.</li>
</ul>

Even though these Plasma components are alpha, they are integrated with the release and installed by default. We encourage you to try them out. Please report bugs, ideas and suggestions at <a href="https://bugs.kde.org/enter_bug.cgi?product=telepathy&format=guided">the KDE Telepathy Bugzilla</a> or our <a href="http://forum.kde.org/viewforum.php?f=230">new forum</a>, where we continuously watch and provide support.

<div><a href="http://dot.kde.org/sites/dot.kde.org/files/Telepathydesktop_4_2.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/Telepathydesktop_4_2544px.png" /></a><br /><big><strong>KDE Telepathy Plasmoids</strong></big> (click for larger)</div>

<h2>Opportunity to Contribute</h2>
Our wiki pages are being revised to create a central place for basic support and guidance for common tasks. If you are a KDE Telepathy user and you'd like to help the project in ways other than coding, this is your chance. Visit the #kde-telepathy IRC channel on Freenode.net or <a href="mailto:kde-telepathy@kde.org">email our mailing list</a>. We are always very happy to welcome any contributors, so if you feel like joining a KDE project, we'll get you started.

<h2>Getting KDE Telepathy</h2>
You can grab the sources for KDE Telepathy 0.4 from <a href="http://download.kde.org/download.php?url=unstable/kde-telepathy/0.4.0/src/">KDE's FTP network</a>.

Thanks for all the continued support!