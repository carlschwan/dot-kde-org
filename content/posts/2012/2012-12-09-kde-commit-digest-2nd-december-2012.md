---
title: "KDE Commit-Digest for 2nd December 2012"
date:    2012-12-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-2nd-december-2012
---
<a href="http://commit-digest.org/issues/2012-12-02/">This week's KDE Commit-Digest</a> brings three stories: recent KDE-PIM activities, changes in Strigi and Google Code-In. The commit list includes:

<ul><li><a href="http://kate-editor.org/">Kate</a> adds "Keep highlighting" next to the close button</li>
<li><a href="http://kdevelop.org/">KDevplatform</a> optimizes document loading</li>
<li><a href="http://community.kde.org/KDE_PIM">KDEPIM</a> updates holidays for several countries</li>
<li>libnm-qt implements GSM settings</li>
<li>Muon-backends gets extensive work, including being split in two--libmuonapt and libmuon (no QApt dependency)</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a>'s Experiment PaintOp now uses highly optimized Displacement option.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-12-02/">Read the rest of the Digest here</a>.