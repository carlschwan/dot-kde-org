---
title: "Akademy 2013 in Bilbao"
date:    2012-10-18
authors:
  - "sealne"
slug:    akademy-2013-bilbao
---
<strong>Bilbao will host <a href="http://akademy2013.kde.org">Akademy 2013</a> from the 13th to 19th of July next year.</strong>

<h2>Kaixo KDE!</h2>
In Basque (the primary language of Bilbao), the letter K is used to change verb case and also to make plurals of some words. For the hard C sound, Basque uses the letter K. 

Lots of Basque words include K: Kaixo (hello), teKnologiaK (technologies), Kalitatea (quality), BerriKuntza (innovation), asKatasuna (freedom), Kultura (culture), Kidetasuna (fellowship), Komunitatea (community), esKuluze (generous), etorKizuna (future), eusKera (Basque language). The letter K is at home in Euskal Herria (Basque Country). 

Obviously, Bilbao will be like home to KDE too. Kaixo KDE, Hello KDE. Welcome to <a href="http://en.wikipedia.org/wiki/Bilbao">Bilbao</a> and <a href="http://en.wikipedia.org/wiki/Basque_Country_(greater_region)">Basque Country</a>.

<h2>Akademy 2013 Bilbao ‒ an interview with the local coordinator</h2>

Following a successful conference in Tallinn, Estonia earlier this year, Akademy will move to Basque Country in the north of Spain. To introduce Bilbao, here is an interview with with Dani Gutiérrez Porset, coordinator of the local team for Akademy 2013:

<strong>Dani, please introduce yourself. Who are you, what do you do when you're not organizing Akademy, and how are you involved in KDE?</strong>
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;text-align: center"><img src="/sites/dot.kde.org/files/Dani_Gutiérrez_Porset.jpg" /><br /> Dani Gutiérrez Porset</div>
<em>I'm a part-time teacher of IT engineering in the <a href="http://www.ingeniaritza-bilbao.ehu.es/p224-home/es">Public University of the Basque Country</a>, and  I also do some freelance work using FLOSS technologies. I try to spread Free Software professionally and also as a volunteer. Two years ago I worked at the Technical Office for the support of Free Software in the Basque Country Government. Now I'm a member of <a href="http://fsfe.org/">FSFE</a> and of <a href="http://itsas.ehu.es/">Itsas</a>, the group that promotes FLOSS in our University. I'm vocal in <a href="http://kde-espana.es/">KDE-España</a>; my main contributions have been to organize <a href="http://dot.kde.org/2010/03/09/akademy-es-2010">Akademy-es in 2010</a> and a sprint for KDE Edu, both here in Bilbao. It's only natural that I would work on the big KDE event—Akademy.</em>

<strong>How did you get the idea to organize Akademy 2013? What is your motivation?</strong>
<em>This is a question of principles in favor of freedom for knowledge and technologies. Some people contribute coding, others designing, translating, documenting. I believe that this great IT work should be universally and freely accessible for everyone. And I see the strength of so many of its communities around the globe. So, for me it's important to support and make visible all this good work that is being done for today's and coming generations. Let's try to make the world see how interesting these digital public commons are, these truly human resources. Let's tell everyone about KDE and its great <a href="http://manifesto.kde.org/">Manifesto</a>. I want this event to be a small but sincerely warm tribute for all the people from so many places that silently are making KDE and all free software day by day, night by night. All of you are very welcome, and we hope you enjoy your time here in Bilbao.</em>

<strong>Can you tell us a bit about Bilbao, the Akademy venues? What can attendees expect from the host city?</strong>
<em>Bilbao is the economic capital of the Basque Country, set in a valley surrounded by mountains, famous for its food, its friendly people and its relaxed atmosphere. It's just 30 min to some of the beaches on the Cantabrian coast. This is a small city compared to others, but there are a lot of places to visit here and in the surrounding area. It is our intention to go along with our KDE friends to sample some of these sites and our culture.</em>

<em>Akademy will start in <a href="http://www.alhondigabilbao.com/">La Alhóndiga</a>, a modern building that hosts many cultural events and has restaurants, a gym, swimming pools and cinema. On Monday we'll move to the <a href="http://www.ingeniaritza-bilbao.ehu.es/p224-home/es">Engineering School of Bilbao</a> to have lots of room for all the workshops, BoFs and hacking. In the city, it is easy to get around by public transport, bike or just walking.</em>

<div style="padding: 1ex; margin: 1ex; border: 1px solid grey;text-align: center"><img src="/sites/dot.kde.org/files/BILBAO.png" /><br />Bilbao <em>by Andrea Bocchino</em></div>

<strong>How is the local Free Software scene in Bilbao and the Basque region? And are there any local Free Software groups that will support the conference?</strong>
<em>For more than ten years, the Basque Region has been involved in different activities related to Free Software, some of them cultural and sociopolitical, some of them in the business sphere. One of the first main events was in 2001, when the <a href="http://www.sindominio.net/hmleioa01/">Spanish hackmeeting<a/> took part in Leioa, and hackers from different parts of the country met for a weekend. Ah, the years of the console... but desktops were growing up and improving more and more. Those guys from the hacklab grew up too, and now a lot of them work in IT companies or education, and collaborate with important FLOSS projects like Debian, Ubuntu or Asterisk.</em>

<em>Nowadays there are a variety of groups that continue the work started in 2001. Two of them, <a href="http://itsas.ehu.es/">Itsas</a> and <a href="http://www.e-ghost.deusto.es/">e-ghost</a> from the main universities will be organizing Akademy 2013 together with KDE e.V. Another is <a href="http://librezale.org/">Librezale</a>, which translates many Free Software projects into the  Basque language. And also there are more small non-profit associations and a lot of people thinking that FLOSS is a very good way to construct a different and better technological future.</em>

<em>It's also important to mention here that in Euskadi (the Basque name for the area) there is an association (<a href="http://www.esle.eu/">ESLE</a>) of 36 companies that base their business models on Free software,  ranging from software development to implementation and service  delivery. They consist of more than 500 people, with a combined income of approximately €25 million. This summer the local government of the Basque Country signed an agreement formally adopting a strategy of software openness and reuse of applications. Let's see how far it reaches.</em>
<em>It's difficult to mention all the names of the many people and organizations that have contributed to adopting FLOSS in this land. They may not be taking part in the big and well-known developments, but they are trying to spread the magic and ethical values of this innovative way to construct software.</em>

<em>So, with different events organized in the past couple of years (Moodle moots, FLOSS summer courses, meetings for the universities, Akademy-es, and a KDE Edu sprint), it was time to take on an important and international event like Akademy. This will be the longest international IT event ever done here. We are looking forward to celebrating it with all of you, KDE folks. But not only for yet-KDEer-believers, let's open the event so more and more people can know, evaluate and adopt these cool free software technologies. Let's go and prepare for a fabulous technical and social meeting.</em>

<h2>About Akademy</h2>
<div style="float: right; padding: 1ex; margin: 1ex;text-align: center"><img src="/sites/dot.kde.org/files/akademy_A.png" /></div>
For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

Next year will be the 11th edition of Akademy, when once again a few hundred Free Software enthusiasts will gather for 2 days of talks and 5 days of  workshops and coding sessions.

For more information, please contact <a href="http://akademy2013.kde.org/contact">The Akademy Team</a>.