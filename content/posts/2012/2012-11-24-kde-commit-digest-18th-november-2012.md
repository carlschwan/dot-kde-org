---
title: "KDE Commit-Digest for 18th November 2012"
date:    2012-11-24
authors:
  - "mrybczyn"
slug:    kde-commit-digest-18th-november-2012
comments:
  - subject: "kate search and replace behavior replaced"
    date: 2012-11-25
    body: "Sad day indeed. :(\r\n\r\n[The commit digest story tells the rest.]"
    author: "Jaka"
  - subject: "A big thank you!"
    date: 2012-11-25
    body: "Just wanted to say thanks to everybody involved to push out the Commit Digest. I'm really happy to see that the digest is up to date again and that it contains stories."
    author: "mgraesslin"
  - subject: "I wrote \"this is a sad day\","
    date: 2012-11-25
    body: "<em>I wrote \"this is a sad day\", since I personally liked the previous behavior a lot. But there seemed to be quite some users who never figured out that Escape was needed to remove these highlightings :)</em>\r\n\r\nWell, appearently, that doesn\u2019t work in VI mode. I guess it doesn\u2019t help."
    author: "SciK"
  - subject: "A Shame"
    date: 2012-11-25
    body: "This is a shame really.  I'm not real keen on the time-out mode.  Since linux is about choice, it seems to me that the logical thing would have been to have the two different behaviors as separate options.  Though I'm sure this would have had an impact upon the coding and testing required, but it would have provided the users the ability to choose the behavior that best suited them.  "
    author: "P. McGee"
  - subject: "stop when search bar is closed instead"
    date: 2012-11-25
    body: "...or stop highlighting when the search bar is closed. You lose a tiny bit of screen space while you want your search results to be shown, but that seems like a small price to pay for the \"highlight all\" feature.\r\n\r\nI also don't understand how the text \"there are 42 matches\" can replace the highlighting \"there a match here, here, and here\". It's not the same type of information, and doesn't serve the same usecases."
    author: "moltonel"
---
<a href="http://commit-digest.org/issues/2012-11-18/">This week's KDE Commit-Digest</a> starts with three short stories: Kate search and replace changes; Homerun; and Linux Color Management Hackfest 2012. Of course, it also gives the overview of development activity:

<ul><li><a href="http://userbase.kde.org/KWin">KWin</a> supports color correction</li>
<li>New features in <a href="http://simon-listens.blogspot.com/">Simon</a>: scenario configuration and auto-switching recognition backend based on the model type</li>
<li>Comic plasmoid ported to QML</li>
<li>Work on threading in <a href="http://skrooge.org/">Skrooge</a></li>
<li><a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a> allows the use of tags</li>
<li><a href="http://amarok.kde.org/">Amarok</a> Google Summer of Code project for <a href="http://strohel.blogspot.com/search/label/gsoc">statistics synchronization</a> merged with many features and improvements</li>
<li><a href="http://userbase.kde.org/Plasma/Public_Transport">PublicTransport</a> improves GTFS (General Transit Feed Specification) feed import performance</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> improves document and music search</li>
</ul>

<a href="http://commit-digest.org/issues/2012-11-18/">Read the rest of the Digest here</a>.