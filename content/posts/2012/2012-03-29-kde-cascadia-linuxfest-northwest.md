---
title: "KDE Cascadia & LinuxFest Northwest"
date:    2012-03-29
authors:
  - "kallecarl"
slug:    kde-cascadia-linuxfest-northwest
comments:
  - subject: "Great"
    date: 2012-03-30
    body: "This conference sounds like it will be fun. Hopefully the location will promote attendance from folks on both sides of the border. If I remember correctly, Amtrak is also an option to get to Bellingham from Seattle or Vancouver."
    author: "vladislavb"
  - subject: "Yup, yup, looking good. I'm"
    date: 2012-03-31
    body: "Yup, yup, looking good. I'm going to attend LFNW and greatly look forward to that :D"
    author: "jospoortvliet"
  - subject: "Amtrak Cascades"
    date: 2012-03-31
    body: "Beautiful trip right along the water of Puget Sound for most of the trip, whether from the north or the south. Bellingham ain't bad either. 60 miles from the seashore to skiing. \r\n\r\nA lot of people make the journey from Seattle. Quite a few from Portland, one of the hotspots of Free and Open Source. Vancouver has a thriving FOSS community as well.\r\n"
    author: "kallecarl"
  - subject: "KDE Cascadia/ Linuxfest Northwest"
    date: 2012-04-04
    body: "I'm going to KDE Cascadia/ Linuxfest Northwest.  I'm really looking forward to it.  \r\nInstead of just focusing on individual components of KDE, applications, & more technical aspects, I'm hoping there will be some mention at KDE Cascadia about how KDE is currently the best desktop environment. As shown by Tech Radar & the readers of LinuxQuestions.org: \"http://www.techradar.com/news/software/operating-systems/what-s-the-best-linux-desktop-environment--1045280\"      \r\n\r\n\"http://dot.kde.org/2012/03/05/kde-best-desktop-environment-year.\" "
    author: "elcaset"
---
KDE Cascadia will be held in partnership with <a href="http://linuxfestnorthwest.org/">LinuxFest Northwest</a> (LFNW) in <a href="http://www.bellingham.org/">Bellingham, Washington</a> on April 28 and 29, 2012. This is a pilot of regional KDE gatherings in conjunction with established grassroots FOSS conferences. The goals are to reduce the effort and expense associated with a single annual stand-alone meeting, and to increase user and developer involvement in KDE. The LFNW Organizers warmly welcome the KDE Community.
<!--break-->
<h2>Cascadia</h2>
<a href="http://en.wikipedia.org/wiki/Pacific_Northwest">Cascadia</a> is the <a href="http://www.flickr.com/search/?q=cascade%20mountains">scenic Cascade Mountain Range</a> region of the NW North American continent that includes British Columbia in Canada, and northern California, Oregon and Washington in the U.S. LFNW draws from this geographical area. It is home to Vancouver, BC (about 2 million people), Seattle (3 million), and Portland (2 million).

With a regional focus, KDE is reaching out to users; bringing KDE closer to home for them and making it easier for people to connect with other KDE supporters and potential contributors. There are approximately 20 other community-driven Free and Open Source events throughout the U.S. and Canada where regional KDE gatherings can be held if KDE Cascadia shows promise.

<h2>LinuxFest Northwest</h2>
LinuxFest Northwest (LFNW) is the original community-driven FOSSFest in North America. Held annually at <a href="http://www.btc.ctc.edu/">Bellingham Technical College</a> (BTC), 2012 will be the 13th LFNW. It is a grassroots event with appeal for the local community, technical users, regional FOSS enthusiasts, SysAdmins and developers, as well as internationally known presenters. It attracts between 1000 and 1200 people. The “no stress” Fest includes a wide variety of about 100 presentations, a low key expo. Registration is optional. LFNW runs two days—Saturday and Sunday. There is no admission charge to Fest events; a reasonably-priced lunch is served by the Culinary Arts Department of the College.

LFNW includes a party (beer, soft drinks and appetizers) Saturday evening, which will be held at the <a href="http://www.sparkmuseum.org//">Spark Museum of Electrical Invention</a> in downtown Bellingham. Other activities will be available in the downtown area that evening. KDE attendees will feel right at home with other LFNW folks in Bellingham, sharing commitments to FOSS, community-based software, collaboration and beer.

Travel may be an obstacle for people coming from a distance. Allegiant Airlines flies directly into Bellingham from major cities on the US West Coast. Both the main hotel and the conference venue are close to the airport. Seattle and Vancouver BC are major airline transportation hubs. Seattle is generally cheaper and is about 100 miles south of Bellingham. Vancouver is about 50 miles north, across the busy Canada/U.S. border.

Several visible KDE projects will be presented, including <a href="http://krita.org/component/content/article/10-news/109-krita-at-linuxfest-northwest">Krita</a>, <a href="https://owncloud.com/">ownCloud</a> (2 presentations), and Plasma Active and the open Vivaldi tablet (demo and panel of core developers). A KDE Birds of a Feather session is planned, as is a KDE social event Friday evening before the Fest. Based on the typical audience of LFNW, KDE Cascadia is intended to appeal primarily to KDE users and developers, existing and potential. In this way, KDE Cascadia will benefit from the audience and promotion associated with LFNW.

<h2>If you want to attend</h2>
Please visit the LinuxFest Northwest website and create an account. Once you have an account, complete your profile. Please indicate KDE in the “interests” area. Registering gets you a snazzy name badge and entry to the free Party Saturday evening. The website also has travel, meals and lodging information. The Hampton Inn is the center of activity away from the Fest itself.
 
Email kdelfnw2012@gmail.com, let us know that you’ll be here, and where you’re traveling from.