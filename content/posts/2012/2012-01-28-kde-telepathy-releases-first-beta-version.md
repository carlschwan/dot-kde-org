---
title: "KDE Telepathy releases first beta version"
date:    2012-01-28
authors:
  - "mck182"
slug:    kde-telepathy-releases-first-beta-version
comments:
  - subject: "Ubuntu packages "
    date: 2012-02-01
    body: "any volunteer to provide ubuntu packages?\r\n\r\nthanks\r\n\r\n"
    author: "somekool"
  - subject: "packages"
    date: 2012-02-03
    body: "I too am looking forward to updated deb packages for Kubuntu"
    author: "mrdr"
  - subject: "Replacing Kopete?"
    date: 2012-02-08
    body: "Is this going to replace Kopete as the standard messaging client in KDE SC someday?"
    author: "Yaro"
  - subject: "Re"
    date: 2012-03-06
    body: "Depends on distro. For example, Kubuntu 12.04 LTS plans to use it as default only in case that it is stable."
    author: ""
---
The KDE Telepathy team has made the first beta release of the new KDE Instant Messaging suite. This 0.3 release coincides with the release of <a href="http://www.kde.org/announcements/4.8/">KDE Workspaces, Applications and Platform 4.8</a>.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/KDETelepathy.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDETelepathy300.png" /></a><br />KDE Telepathy (click for larger)</div>

In the 3 months since KDE Telepathy 0.2, members of the team have <a href="http://bit.ly/AyOjJX">closed over 100 bugs</a>. With the added stability and polish, the project has been moved to KDE Extragear starting new life as an accepted KDE application.

Along with bug fixes, 0.3 brings the following new features:
<ul>
<li>A completely rewritten and simplified Plasmoid for controlling online status
<li>Support for <a href="http://blogs.fsfe.org/drdanz/?p=632">MSN-XMPP<a/></li>
<li>Redesigned dialog for adding accounts to show favorite IM networks first</li>
<li>Multiple file transfers with drag'n'drop support</li>
<li>Ability to resume file transfers</li>
<li>The contact list is now separate, fully self-contained, easily embeddable widget.</li>
<li>Greatly improved contact tooltips in contact list</li>
<li>Contact filtering in send file utility</li>
<li>Support for <a href="http://www.ircbeginner.com/ircinfo/ircc-commands.html"> /me</a> in chats</li>
</ul>
More information about the project can be found at the <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">KDE Telepathy homepage</a>.

Source code is available for <a href="http://download.kde.org/download.php?url=unstable/kde-telepathy/0.3.0/src/">download</a>; some distributions may also have packages.