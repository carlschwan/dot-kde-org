---
title: "LaKademy \u2014 First KDE Event for All of Latin America"
date:    2012-04-23
authors:
  - "araceletorres"
slug:    lakademy-—-first-kde-event-all-latin-america
comments:
  - subject: "Viva La Kademy"
    date: 2012-04-23
    body: "Wrong language, but still... Also: Good initiative!"
    author: ""
  - subject: "Mexico"
    date: 2012-04-23
    body: "Not completely sure if mexico is in the logo or not... more resolution needed, add smaller gears. hehehe :)"
    author: "gamaral"
  - subject: "Thank you, and..."
    date: 2012-04-24
    body: "Many thanks to the full KDE community of developers! You all are doing a wonderful job, and please know your hard work is very much appreciated.\r\n\r\nAlso want to ask that as you are all working on the \"Plasma Network Management\" feature - which is wonderful BTW! - maybe add a keep alive / auto-Reconnect selection? Especially need in DSL/PPPoE(service) as our ISP here at our university housing disconnects after 2days/48hrs.\r\n\r\nBill Greenwood\r\n\r\n"
    author: ""
  - subject: "Y Mexico?"
    date: 2012-04-25
    body: "Mmm..\r\nBueno, yo se que Mexico es cuna de GNOME. Peeeroo.. no me gusta que en su logotipo no hayan incluido a Mexico =(\r\n\r\nTambien habemos KDE\u2019eros por aca.\r\n\r\nYO soy autor de un theme de Plasma de KDE: Perla Negra.\r\n(que nunca termine pero pues\u2026 lo intente)\r\n\r\nSaludos!\r\n"
    author: ""
  - subject: "Mexico is part of the logo"
    date: 2012-04-26
    body: "Mexico is the dark yellow shape at the top of the logo. Pieza amarillo m\u00e1s alto.\r\n\r\n\r\n\r\n"
    author: "kallecarl"
  - subject: "\u00bfamarillo oscuro?"
    date: 2012-05-01
    body: "\u00bfamarillo oscuro? pens\u00e9 que era anaranjado.\r\n"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/logo-camisa-01_wee.jpg" /></div>
Latin America is a big place with many opportunities for KDE; major deployments of KDE software are proof. Over the years, groups of KDE developers have emerged in Argentina, Brazil, Chile, Colombia, Peru, and probably other places. These groups work together to make a better KDE. As we know, meeting and talking in person is important to strengthen the bonds in a community. So we decided to organize a Latin American meeting of KDE contributors following the lead of the first Akademy-br in 2009. Like Akademy-br, the first <a href="http://br.kde.org/LAkademy">LaKademy</a> will be similar to a sprint for developers and one for people interested in promoting KDE in this part of the world.

The first LaKademy is taking place in the city of Porto Alegre, Brazil from April 27th to May 1st, 2012. People were invited from Brazil, Argentina, Peru, and Colombia. 12 known KDE contributors have confirmed their presence, along with 6 newcomers.  Current KDE contributors recommended newcomers based on their abilities and their potential to become long-time KDE contributors.
<!--break-->
During the five days of the event, we will work on the following activities:
<ul>
<li>Artwork</li>
<li>KDEedu: Cantor and Rocs</li>
<li>Plasma Active</li>
<li>Plasma Network Management</li>
<li>Promotion</li>
<li>Translations</li>
<li>KDE-games</li>
<li>And more!</li>
</ul>

We expect this meeting to provide more maturity to Latin American groups and help pave the way for new contributors. It will be a good opportunity for us to share experiences and learn from each other. We plan to have fun and make new friends, because that’s very important too.

Thanks to KDE e.V and Claudia Rauch for supporting the event.