---
title: "KDE Commit-Digest for 9th September 2012"
date:    2012-10-28
authors:
  - "mrybczyn"
slug:    kde-commit-digest-9th-september-2012
---
In <a href="http://commit-digest.org/issues/2012-09-09/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://konsole.kde.org/">Konsole</a> reimplements KDE3's print screen functionality</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> adds animation tool docker for editing shape animations</li>
<li>KDE-Workspace splits SceneOpenGL into a concrete SceneOpenGL1 and SceneOpenGL2</li>
<li>In <a href="http://edu.kde.org/kstars/">KStars</a>, Star Hopper now takes the field of view from the current visible FOV symbol</li>
<li><a href="http://okular.org/">Okular</a> optionally shows a complete file path in window title</li>
<li>Updated internal libraw in libkdcraw supports new cameras</li>
</ul>

<a href="http://commit-digest.org/issues/2012-09-09/">Read the rest of the Digest here</a>.