---
title: "KDE Response to the UK Open Standards Consultation"
date:    2012-06-04
authors:
  - "mirko"
slug:    kde-response-uk-open-standards-consultation
comments:
  - subject: "Great! Thank you for stepping"
    date: 2012-06-06
    body: "Great! Thank you for stepping up!\r\n\r\n(I also wrote to them, but one user likely has far less weight than an official note from a huge community)"
    author: ""
---

KDE e.V. has responded to the <a href="http://consultation.cabinetoffice.gov.uk/openstandards/">United Kingdom Open Standards Consultation</a> on behalf of KDE, one of the largest and most influential Free Software communities world-wide with thousands of volunteer contributors and countless users. Open Standards align with the goals of the KDE Community, especially with regards to digital freedom and contributions to the common good, while patent mania and malicious licensing terms threaten KDE and other Free Software projects. 

The <a href="http://dot.kde.org/sites/dot.kde.org/files/UKStandardsKDEResponse.pdf">KDE e.V. response</a> (pdf) is intended to persuade the Open Standards Consultation to create a level playing field between community-driven and commercial software solutions. A truly open standard would increase competition and provide substantial benefits to the government and its citizens.

The core issues raised by the Consultation are:
<ul>
<li>identifying the key components of an open standard</li>
<li>determining the circumstances and specifics for considering mandated open standards.</li>
</ul>

The key to KDE's response is that we consider a standard open if it allows and encourages implementations according to the <a href="http://fsfe.org/about/basics/freesoftware.en.html">Four Freedoms of Free Software</a>, as defined by the Free Software Foundation. The UK Government is encouraged to use these when mandating standards.