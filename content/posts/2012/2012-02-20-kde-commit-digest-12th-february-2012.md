---
title: "KDE Commit-Digest for 12th February 2012"
date:    2012-02-20
authors:
  - "mrybczyn"
slug:    kde-commit-digest-12th-february-2012
---
In <a href="http://commit-digest.org/issues/2012-02-12/">this week's KDE Commit-Digest</a>:

<ul><li>Important improvements in simon. The first working speech recognition implementation based on stock <a href="http://julius.sourceforge.jp/en_index.php">Julius</a> (continuous speech recognition system) Text To Speech (TTS) to RSS and email</li>
<li><a href="http://konsole.kde.org/">Konsole</a> gains a Clone Tab action, DBus objects for each Konsole window</li>
<li>Work on annotation view in <a href="http://kst.kde.org/">Kst</a>: items can now move with data</li>
<li><a href="http://www.digikam.org/">Digikam</a> can now be compiled with <a href="http://www.littlecms.com/faq.html">lcms2</a> (Little CMS, a color management library)</li>
<li><a href="http://www.freedesktop.org/wiki/Software/LightDM">LightDM</a> adds support for guest session</li>
<li>In <a href="http://extragear.kde.org/apps/kipi/">KIPI</a>, JPEG lossless plugin now uses actionthreadbase class based on ThreadWeaver to use multiple CPUs</li>
<li>Change in <a href="http://amarok.kde.org/">Amarok</a>'s mass storage management: does not show unmounted USB devices and makes it clear when a device is not activated.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-02-12/">Read the rest of the Digest here</a>.

<small><em>Comments are moderated based on <a href="http://kde.org/code-of-conduct/">the KDE Code of Conduct</a>. There will be a delay before they are published.</em></small>