---
title: "KDE Announces 4.9 RC1"
date:    2012-06-27
authors:
  - "kallecarl"
slug:    kde-announces-49-rc1
---

Today KDE released the <a href="http://kde.org/announcements/announce-4.9-rc1.php">first Release Candidate</a> for the 4.9 versions of Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is still on fixing bugs and further polishing new and old functionality. Highlights of 4.9 include:

<ul>
    <li>
    Qt Quick in Plasma Workspaces -- Qt Quick is continuing to make its way into the Plasma Workspaces; the Qt Quick Plasma Components, which were introduced with 4.8, mature further. Additional integration of various KDE APIs was added through new modules. More parts of Plasma Desktop have been ported to QML. While preserving their functionality, the Qt Quick replacements of common plasmoids are visually more attractive, easier to enhance and extend and behave better on touchscreens.
    </li>
    <li>The Dolphin file manager has improved its display and sorting and searching based on metadata. Files can now be renamed inline, giving a smoother user experience.
    <li>
    Deeper integration of Activities for files, windows and other resources: Users can now more easily associate files and windows with an Activity, and enjoy a more properly organized workspace. Folderview can now show files related to an Activity, making it easier to organize files into meaningful contexts.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.9_Feature_Plan">4.9 Feature Plan</a>. As with any large number of changes, we are still giving 4.9 a good testing in order to ensure the highest level of quality and an outstanding user experience when they get the update.

<h4>The Testing Team</h4>
As with the two beta releases, the testing continues for Release Candidate 1.
 
The KDE Testing Team is continuously growing and collaborates directly with KDE developers. 91 more bugs were fixed since the beta2 release and about 40 regressions were identified and mostly fixed as well. Under the hood, there is more work going on in collaboration with the KDE sysadmins to facilitate automatic testing for all KDE projects, continuous integration is already used by several core components of KDE.
 
To join the KDE Testing Team please come to the #kde-quality channel on irc.freenode.net or join the mailing list kde-testing@kde.org.

Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining in.