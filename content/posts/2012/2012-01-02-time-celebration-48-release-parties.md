---
title: "Time for celebration - 4.8 Release Parties"
date:    2012-01-02
authors:
  - "Arushi Garg"
slug:    time-celebration-48-release-parties
comments:
  - subject: "Thank you"
    date: 2012-01-02
    body: "Thank you to all that work on KDE. I just switched from regular Ubuntu to Kubuntu and I am AMAZED at how well it works and what all it will do. The more I use it, the more I like it. I am an IT consultant and in my opinion, KDE is the best!\r\n\r\nThank you!\r\nGary"
    author: ""
  - subject: "Seconded."
    date: 2012-01-05
    body: "Seconded."
    author: ""
  - subject: "I concur"
    date: 2012-01-07
    body: "thirded"
    author: ""
---
Developers, testers, bug chasers and many more people are putting in a major effort on KDE's next major release. It will soon be time to appreciate the result, sit back and relax! The occasion calls for commemoration of KDE’s recent accomplishments.

A <a href="http://community.kde.org/Promo/Events/Release_Parties/4.8">wiki page for the 4.8 parties</a> has been created. If you want, please organize a party. But first add your name to the list and start getting the word out.
<!--break-->
What is the party all about?

<ul>
<li>The KDE Community will meet all around the world for a fun time to celebrate the release.</li>
<li>It's not just a release party. It's a time for the Community to rejoice together.</li>
<li>Anyone can organize a party. Just add it to the wiki and invite people to sign up.</li>
<li>What happens at the party is up to you. Your event could be anything from meeting for drinks to an all-out event with talks, demos and an Upgrade Fest. Be creative!</li>
<li>Everyone is welcome—contributors, soon-to-be contributors, users, and anyone else who just wants to celebrate.</li>
<li>Release parties are a KDE tradition. Check out <a href="http://dot.kde.org/2010/03/21/kde-partying-around-world-new-release"> events from 2 years ago</a> for ideas.</li>
</ul>

The only thing left to do is get started, so go ahead. Get organized with other people. Reserve a table at a local restaurant. Make the occasion memorable. Take a lot of pictures.
 
Please put any questions in comments.