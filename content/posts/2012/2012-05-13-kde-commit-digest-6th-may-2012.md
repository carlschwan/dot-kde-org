---
title: "KDE Commit-Digest for 6th May 2012"
date:    2012-05-13
authors:
  - "mrybczyn"
slug:    kde-commit-digest-6th-may-2012
---
In <a href="http://commit-digest.org/issues/2012-05-06/">this week's KDE Commit-Digest</a>, Frederik Gladhorn writes about the current state of development in KDE-Accessibility. 

As usual, there is also a change list that includes:

<ul><li><a href="http://edu.kde.org/marble/">Marble</a> gets the first version of FlightGear position provider</li>
<li><a href="http://kphotoalbum.org/">KPhotoAlbum</a> saves the video length in a database; search for the length is also possible</li>
<li>KWebkitParts gets an option that allows the user to disable the internal handling of flash and java applets</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> extends the notes class; there are some new properties</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> allows dragging of selected text. It works with drag and drop too.</li>
<li><a href="http://amarok.kde.org/en">Amarok</a> can write album covers to iPods and USB Mass Storage devices; changes in cover art reading and writing</li>
<li><a href="http://solid.kde.org/">Solid</a> adds proper support for volumes, storage disks and storage access, which was not working on Windows</li>
<li>Soprano uses non-Qt local socket communication on Unix systems.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-05-06/">Read the rest of the Digest here</a>.