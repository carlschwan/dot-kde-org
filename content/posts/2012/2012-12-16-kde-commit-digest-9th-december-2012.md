---
title: "KDE Commit-Digest for 9th December 2012"
date:    2012-12-16
authors:
  - "mrybczyn"
slug:    kde-commit-digest-9th-december-2012
---
In <a href="http://commit-digest.org/issues/2012-12-09/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.kdevelop.org/">KDevelop</a> adds "Switch To Buddy" plugin</li>
<li><a href="http://www.digikam.org/">Digikam</a>'s batch queue manager makes better usage of multiple CPU cores</li>
<li>KIO listDir uses time-based entry batching</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> better supports the mobi format</li>
<li><a href="http://www.kde.org/applications/system/kdepartitionmanager/">PartitionManager</a> performs resizing of FAT and HPFS filesystems with parted 3.1 or above</li>
<li>KMix gains better keyboard control</li>
<li>Work on the new KScreen Daemon.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-12-09/">Read the rest of the Digest here</a>.