---
title: "Call for Host for Akademy 2013 Still Open"
date:    2012-08-27
authors:
  - "sealne"
slug:    call-host-akademy-2013-still-open
---
The KDE Community are <a href="http://dot.kde.org/2012/06/28/call-host-akademy-2013">still looking</a> for proposals to Host Akademy in 2013.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2012-group-reallywee.jpg" /><br />Akademy 2012</div>
The hosting proposal needs a strong team of local volunteers who have an eye for detail that is able to organize and host our annual community summit.

Akademy is a great event to host as it is the gathering of the KDE community, one of the largest and most significant Free Software communities. Hosting Akademy for a week will create marvelous results and give visibility to you as a host and many others way beyond the internal KDE activities. It will also make an impact on your local Free Software community.

The Call for Hosts is open for another 3 weeks till the 15th of September. Full details of the requirements are in the <a href="http://ev.kde.org/akademy/CallforHosts_2013.pdf">Call for Hosts</a>. If you have any questions or concerns, please contact the KDE e.V. Board at <a href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>. 
