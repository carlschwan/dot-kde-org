---
title: "KDE Commit-Digest for 30th September 2012"
date:    2012-11-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-30th-september-2012
---
In <a href="http://commit-digest.org/issues/2012-09-30/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://simon-listens.blogspot.fr/">Simon</a> uses new synchronization protocol</li>
<li>Kmix implements new communication infrastructure</li>
<li>Top control bar for visual graph editor in <a href="http://userbase.kde.org/Rocs">Rocs</a> allows changing of documents, data structures, adding/removing and accessing the properties dialogs</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> introduces better VCard support</li>
<li>Phonon-gstreamer adds audio channel selection</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> improves slide switching</li>
<li><a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">KDE Telepathy</a> sets spell checking language per contact instead of per tab.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-09-30/">Read the rest of the Digest here</a>.