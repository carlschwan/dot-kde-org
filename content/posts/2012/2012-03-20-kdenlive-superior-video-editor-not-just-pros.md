---
title: "Kdenlive: Superior Video Editor ... not just for pros"
date:    2012-03-20
authors:
  - "sethkenlon"
slug:    kdenlive-superior-video-editor-not-just-pros
comments:
  - subject: "Keep going...please."
    date: 2012-03-21
    body: "Just read this\r\nhttp://jderose.blogspot.com/2012/03/open-letter-to-kdenlive-community.html Had the same thought yesterday when this news appeared.\r\n\r\nThe team has already exceeded their fundraising goal, but that's no reason to stop. Kdenlive is an open source star. The developers have already demonstrated what they can do. Please help keep the money rolling in.\r\n\r\nAbsolutely yes to \"Kdenlive's quality benefits all Free and Open Source Software.\"\r\n\r\nThanks Kdenlive team!\r\n"
    author: ""
  - subject: "I've read lots of good things"
    date: 2012-03-28
    body: "I've read lots of good things about KDEnlive, and I regularly recommend it, but for me it's never worked as I expected. Whether trying to make a simple slideshow of still images, or stitching a few clips together into a single video, I've always had to switch to something else in order to get the job done.\r\n\r\nMaybe I'm too much of a video noob, maybe I had bad luck with the versions that I tried (always from repo, I know sometimes bugs linger a long time in old packages), or maybe KDEnlive needs to improve usability and community support (I have sought but never found answers in the IRC channel). I just don't know. Pitivi has always been more usable for me.\r\n\r\nGreat to see the project gain popularity, I'm just sharing my experience as an average KDE power-user."
    author: ""
  - subject: "Kdenlive is the best i've used so far"
    date: 2012-03-31
    body: "Kdenlive is amazing. I used it to edit my 100% foss animation \"Clocked In\", which won an award for best animated film at the diy film festival and played in many other festivals. (http://clockedin.ahloe.com)\r\n\r\nIf it had a unified keyframing system for all effects and settings, it would soar.\r\n\r\nSadly, kdenlive rendering and other things are screwed up in ubuntu 12.04 (still in beta). I think there are some major bugs in MLT or ffmpeg in the 12.04 repos, because other applications are being weird too. \r\n\r\nSadly I'm not the right person to know if it's the program, packaging, or other distro stuff. I use medibuntu libav packages, but the problems happen anyway. Hopefully when the sunab PPA is up for Ubuntu 12.04 it will be fixed. \r\n\r\nPeople shouldn't really have to use a PPA though, but that's the state of Ubuntu packaging/distribution. It's stubborn, and it thinks it can do a better job maintaining and caring for the software experience than the program's developers, which doesn't work too well.\r\n"
    author: "ikeahloe"
  - subject: "Nice edits! i approve mostly!"
    date: 2012-03-31
    body: "Nice edits! I approve mostly! I was half awake when typing that comment.\r\n\r\nUbuntu 12.04 is definitely still in beta, but I doubt the issue will be resolved before release knowing Ubuntu.\r\n\r\nThe only thing I'd make clear is my comment towards packaging and distribution extends beyond Ubuntu. I'm talking about the default packaging and distribution method for most distros. Some cool projects are out there like \"appimages\", but those projects  are narrow-minded (don't care about 64-bit, don't want to include kde programs)but more importantly need help from desktops/file managers for integration. I think KDE could pioneer the application bundle integration system because nepomuk is so amazing. Nepomuk could see those files as an application class, and search and launch them as such. But this is overall getting wayyy off topic I apologize."
    author: ""
  - subject: "Kdenlive How To"
    date: 2012-04-03
    body: "There's a pretty good overview of everything you could ever need to know about Kdenlive here:\r\n\r\nhttp://opensource.com/life/11/11/introduction-kdenlive\r\n\r\nIt's in six parts, and covers everything from basic edits to advanced techniques and tricks, to audio, to the final render.\r\n\r\nBeing the author of the series, I'm biased. But I believe it could be helpful. In my experience, Kdenlive is one of the best editors out there, period.  I'm not saying it couldn't use some additional features and/or polish (what app can't?). But nowadays I shoot video just as an excuse to edit in Kdenlive!"
    author: ""
  - subject: "KDEnlive"
    date: 2012-08-27
    body: "KDEnlive is an excellent tool for video editing. I donated to this project's fundraising page earlier this year because I use KDEnlive and wanted to show my appreciation. I am glad I did.  "
    author: "marytee"
---
<div style="float: right; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/Kdenlivelogo.png" /></div>

Video editing has evolved from a niche market in the computer world to something that computers are simply expected to do. It's a tall order to be everything to everyone. But if any video editing software comes close to that mark, it's Kdenlive—a KDE Applications star. With a strong commitment and a plan for making major improvements, the <a href="http://kdenlive.org/users/ttill/kdenlive-fundraising-campaign">Kdenlive team is raising money</a>. Please help out if you can.
<!--break-->
Kdenlive is KDE's industrial-strength non-linear video editing application. Like the rest of KDE software, Kdenlive is feature-rich, well-integrated with the rest of the user environment. It's flexible, intuitive and a pleasure to use, even if only for YouTube shorts. Our company uses Kdenlive for professional film production and I've written about it <a href="http://opensource.com/life/11/11/introduction-kdenlive"> for professional editors</a>.

What many people don't realize is that within the filmmaking community, the number of video editing platforms has been sharply reduced. And some of the most game-changing editing applications have drastically changed how video editors have to use those editing applications. Alienated video editing pros are desperately seeking alternatives. Kdenlive is poised to fill the void, and even advance by leaps and bounds.

One thing that makes Kdenlive unique and attractive for many editors is its ability to ingest media without arbitrary restrictions on how the media is brought onto the computer. There's no requirement for a Firewire port or direct media import from an SD card, as there are in many popular proprietary programs. Kdenlive uses a powerful multimedia backend that puts control into the editor's hands where it belongs.

The user interface of Kdenlive is familiar, using a video editing paradigm that has worked for decades. It is an interface that most professional editors see no reason to abandon. Interestingly, this UI not only works for professionals. It's intuitive and works just as well for new and amateur editors who may not need advanced features. It works fine for simple basic edits on personal video projects. Whether or not the user requires advanced features like proxy clips and chroma keys, Kdenlive is an easy entry point to making movies.

Kdenlive sometimes surprises its users with innovative thinking. Well before other popular video applications featured it, Kdenlive used precise color curves (rather than the more traditional color wheels) for its color correction workflow. Those who know color have consistently been amazed by the power provided by this new take on an old process. 

In an effort to add even more features and polish to their fine product, the Kdenlive team has started a modest <a href="http://www.indiegogo.com/kdenlive-re">fund-raising campaign</a>. They have a deadline of April 30<sup>th</sup> and a goal of $4000. With this funding, the Kdenlive team will be able to do full-time development for two months. 

Whether you are a professional video editor looking for the perfect platform for your creativity, or a home user wanting to cut video for fun, Kdenlive is an important and exciting project. Even if you're not interested in video editing, Kdenlive's quality benefits all Free and Open Source Software. 

Donate now, and be a part of Kdenlive's success!