---
title: "KDE Commit-Digest for 22nd January 2012"
date:    2012-02-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-22nd-january-2012
---
In <a href="http://commit-digest.org/issues/2012-01-22/">this week's KDE Commit-Digest</a>:

<ul>
<li>Work on lambda expressions and handling of the override token in c++11, C# import, new features in list generator for <a href="http://www.python.org/">Python</a> in <a href="http://www.kdevelop.org/">KDevelop</a></li>
<li><a href="http://solid.kde.org/">Solid</a> APIs are now used for DPMS (VESA Display Power Management Signaling) service jobs in kde-workspace</li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, basic GUI for the character styles in the style manager, first version of the Lindenmayer brush, new OpenFormula functions: CELL, SHEET and SHEETS; introduction of Page Layout tool</li>
<li>Unified album artist handling in <a href="http://amarok.kde.org/">Amarok</a>; albums with same name but different album artists are now correctly separated in USB mass storage, iPod and various online service collections</li>
<li>Support extended mouse coordinates in <a href="http://konsole.kde.org/">Konsole</a></li>
<li>Closer integration of <a href="http://ksudoku.sourceforge.net/">KSudoku</a> and the new generator/solver.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-01-22/">Read the rest of the Digest here</a>.