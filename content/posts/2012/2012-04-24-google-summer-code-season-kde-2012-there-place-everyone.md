---
title: "Google Summer of Code & Season of KDE 2012 - there is place for everyone!"
date:    2012-04-24
authors:
  - "camilasan"
slug:    google-summer-code-season-kde-2012-there-place-everyone
comments:
  - subject: "query about sok"
    date: 2012-04-25
    body: "From where we can get list of mentors for SOK.?"
    author: ""
  - subject: "Result"
    date: 2012-04-25
    body: "when result for selected student will be declared?"
    author: ""
  - subject: "Probably on melange, see"
    date: 2012-04-26
    body: "Probably on melange, see <a href=\"http://www.google-melange.com/\">here</a>."
    author: "jospoortvliet"
  - subject: "You mean the FINAL results?"
    date: 2012-04-26
    body: "You mean the FINAL results? That'll be over half a year from now :D\r\n\r\nOr the final list of projects? I'd suggest to look on <a href=\"http://www.google-melange.com/\">Google Melange</a>, I guess it's there. And on <a href=\"http://planet.kde.org\">planet KDE</a> many projects are announcing themselves at the moment."
    author: "jospoortvliet"
  - subject: "When is the announcement of selected students? "
    date: 2012-04-26
    body: "I think the question is about the announcement of SoK accepted students."
    author: ""
  - subject: "They will be announced a few"
    date: 2012-04-27
    body: "They will be announced a few days after we close applications on the 7th."
    author: "nightrose"
  - subject: "List of accepted students"
    date: 2012-05-15
    body: "Has the list of students selected to SOK been announced? By which date can we expect a mail? Will we get a mail even if we've been rejected? \r\n\r\nDo even young students pursuing a bachelor's degree have a chance?"
    author: ""
  - subject: "We will send to everyone"
    date: 2012-05-15
    body: "Hey :)\r\n\r\nYes we will send emails to everyone, no matter the outcome. We are currently trying to match up everyone with a mentor. Since there are quite a few students this is taking a bit of time.\r\n\r\n\r\nCheers\r\nLydia"
    author: "nightrose"
  - subject: "SoK acceptance information"
    date: 2012-05-15
    body: "Accepted students haven't been announced yet. The KDE student programs team is working right now with prospective mentors to find mentors for as many students as possible. This is a quite manual and time consuming task. Please be patient. We will send emails to every student, accepted or not, in a few days.\r\n\r\nYoung students pursuing a bachelor's degree surely have a chance ;)"
    author: "teo"
  - subject: "Date of acceptance information"
    date: 2012-05-19
    body: "Can we have an approximate date as to when we will get the results of who all have been selected?\r\nWill it be this week or even later?"
    author: ""
  - subject: "Any requirement of presubmission?"
    date: 2012-05-21
    body: "Is there any requirement of presubmission of any projects? Or will all the students who have opted for KDE SoK get the project?\r\n\r\nAre there any selection criteria?\r\n"
    author: ""
  - subject: "why so much delay ?"
    date: 2012-05-26
    body: "Any idea when result is going to be declared ?"
    author: ""
  - subject: "Re: Date of acceptance information"
    date: 2012-05-27
    body: "I promise I will *try* to get the emails out this week, but it is not up to me since I'm still waiting for some mentors to get back to me about their candidate students."
    author: "teo"
---
<div style="float: right; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/banner-gsoc2012_small.jpg"></div>

Google has published the list of <a href="http://www.google-melange.com/gsoc/org/google/gsoc2012/kde">60 student proposals that have been accepted</a> for Google Summer of  Code 2012 for KDE. It means that 60 students will be able to work full-time on changing the world this summer! A big thank you to Google for making this possible.
<!--break-->
<h2>More than 200 proposals, 60 students accepted</h2>

Big thanks also to every student who dedicated their time thinking up and writing awesome proposals: we received more than 200 proposals! This year Google allocated 60 student slots to KDE. During the past few weeks, the GSoC admins and mentors of KDE had to make some really tough choices. The selection process was hard and competitive, and we had to say, "No" to quite a few brilliant proposals. If your proposal was accepted, we think you are among the very best. Congratulations! You will be spending your summer hacking with KDE. We welcome you with open arms and wish you luck and fun!

<h2>What's next for the GSOC students?</h2>
It is important to pay attention to the instructions that Google sends by e-mail and be around on the IRC channel #kde-soc on Freenode. Don't forget to always check the <a href="http://www.google-melange.com/gsoc/events/google/gsoc2012">timeline</a>.

Now it is time to get to know your mentor, read the documentation around your project and get up to speed to begin working on your project.

<h2>Not selected?</h2>
Unfortunately, we could not accept all proposals. Hard thinking went into the selection: how realistic is the proposal, how will KDE benefit, how much will the student learn, how much time does the mentor have, how much time will the student have and more. Sometimes, there were multiple proposals for a mentor, and choices had to be made on things like timezone or other practicalities. Sometimes we want someone new to the community, sometimes we need somebody more familiar. We have only so many mentors, so many projects and so many slots!

All in all, the limits we face mean that not all good proposals can be accepted. But, for motivated students with a good proposal and a willingness to learn and get involved, we have an alternative: <strong>Season of KDE</strong>!

Season of KDE (SoK) is similar to Google Summer of Code: while the student doesn’t get paid, he or she does get a mentor and gets to hack on KDE for the summer and beyond. And - you'll get yourself a very cool t-shirt and certificate!

The SoK timeline is up to you and your mentor. We advise you to stay as close as possible to the Google Summer of Code timeline. The only hard constraint is the application deadline: you must apply through the application form before <b>May 7, 2012 at 19:00 UTC</b> in order to be eligible for participation. We give preference to those who have applied for Google Summer of Code and to students, but we will gladly consider applications from anyone else. Please read the <a href="http://teom.wordpress.com/2012/04/23/announcing-season-of-kde-2012/">detailed instructions on how to apply</a>.

Season of KDE is managed by the same team of KDE admins and mentors that take care of Google Summer of Code and Google Code-in, with the same level of quality and care. However, it is not related to Google in any way beyond that.

For further questions feel free to join our IRC channel #kde-soc on Freenode.net or email the admin team at kde-soc-mentor-owner@kde.org.

In the past four years, we had in order 1, 4, 8 and 20 successful Season of KDE students. Our goal for this year is at least 30. We want to see you helping to increase these numbers! Be part of KDE!

<h2>Selected or not...</h2>
In the end, GSOC or SOK student - or hacking all year around, <i>we're all KDE</i>. You all make a difference. So Hack On!