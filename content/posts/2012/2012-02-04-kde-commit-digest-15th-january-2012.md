---
title: "KDE Commit-Digest for 15th January 2012"
date:    2012-02-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-15th-january-2012
comments:
  - subject: "background"
    date: 2012-02-04
    body: "Can someone provide some background on the GTL giant lock?\r\nWhat is it? and why was it necessary?"
    author: "Fyzix"
  - subject: "GTL is a language used to"
    date: 2012-02-06
    body: "GTL is a language used to implement image filters and such (http://www.opengtl.org/). Apparently it is not thread safe and they needed to block multiple filters from executing at the same time. I think (hope) that this just serializes filters, but that each individual filter can still use multiple cores ?"
    author: "moltonel"
---
In <a href="http://commit-digest.org/issues/2012-01-15/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://userbase.kde.org/NetworkManagement">Network Management</a> now uses QtNetworkManager to activate connections</li>
<li>Non-QML TabBox removed in <a href="http://kde.org/workspaces/">kde-workspace</a></li>
<li>Fix of high cpu usage in <a href="http://userbase.kde.org/KWin">Kwin</a> fade effect</li>
<li>Widget text color change possible in <a href="http://uml.sourceforge.net/">Umbrello</a></li>
<li>Initial prototype of support for view profiles in <a href="http://utils.kde.org/projects/okteta/">Okteta</a></li>
<li>Threading support, global search bar and tree of folders for bookmarks in <a href="http://edu.kde.org/marble/">Marble</a></li>
<li>Initial implementation of Google Reader API in <a href="http://pim.kde.org/akonadi/">Akonadi</a></li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> introduces a GTL (Graphics Transformation Language) giant lock to avoid threading issues; tripleclick implementation.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-01-15/">Read the rest of the Digest here</a>.