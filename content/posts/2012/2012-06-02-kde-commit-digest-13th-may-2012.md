---
title: "KDE Commit-Digest for 13th May 2012"
date:    2012-06-02
authors:
  - "mrybczyn"
slug:    kde-commit-digest-13th-may-2012
---
In <a href="http://commit-digest.org/issues/2012-05-13/">this week's KDE Commit-Digest</a>:

<ul><li>On startup, <a href="http://kphotoalbum.org/">KPhotoAlbum</a> can search for videos without thumbnails</li>
<li><a href="http://dolphin.kde.org/">Dolphin</a> allows remembering view properties for the search mode</li>
<li>Screen edge bindings for scripts and scripted effects implemented in KDE Workspace</li>
<li>New assert methods and Get Hot New Stuff support are available for KWin scripts</li>
<li>Abstract calendar and printing of the highlighted part of an email in <a href="http://pim.kde.org/">KDE-PIM</a></li>
<li>In <a href="http://www.calligra-suite.org/">Calligra</a>, author information gets configurable on a settings level, floating message class added</li>
<li>GSoC work in cantor and bibliography database in Calligra</li>
<li>Bugfixes in <a href="http://amarok.kde.org/">Amarok</a>, KDE-PIM and the KDE base packages.</li>
<li>Statistics are back...and they are impressive.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-05-13/">Read the rest of the Digest here</a>.