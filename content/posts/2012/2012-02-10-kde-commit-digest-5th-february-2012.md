---
title: "KDE Commit-Digest for 5th February 2012"
date:    2012-02-10
authors:
  - "mrybczyn"
slug:    kde-commit-digest-5th-february-2012
comments:
  - subject: "kdelibs & Qt5, nice"
    date: 2012-02-11
    body: "\"Partial port of kdelibs to Qt5\" - very nice... Anything you want in one awesome Framework."
    author: ""
  - subject: "KDE Download page"
    date: 2012-02-20
    body: "No idea if any of the KDE admins is ready this, but http://kde.org/download/ still shows KDE 4.7."
    author: ""
---
In <a href="http://commit-digest.org/issues/2012-02-05/">this week's KDE Commit-Digest</a>:

<ul>
<li>Partial port of kdelibs to Qt5</li>
<li>RSS feed handling in <a href="http://www.kde.org/workspaces/">Plasma</a></li>
<li>Fix drag and drop issues with non-local URLs</li>
<li><a href="http://edu.kde.org/kstars/">KStars</a> <a href="http://en.wikipedia.org/wiki/FITS">FITS</a> (Flexible Image Transport System) improved to work well in Ekos</li>
<li>Support for embedded album cover reading and writing, more natural drag-and-drop behaviour in collection browser in <a href="http://amarok.kde.org/">Amarok</a></li>
<li>Maintenance action in Tools menu of <a href="http://www.digikam.org/">Digikam</a> to maintain collections etc. while scanning for new data</li>
<li>Refactoring in KisNodeModel, improved support for Excel and MSOffice in <a href="http://www.calligra-suite.org/">Calligra</a>; start of <a href="http://krita.org/">Krita</a> optimized for touch.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-02-05/">Read the rest of the Digest here</a>.

<small><em>Comments are moderated based on <a href="http://kde.org/code-of-conduct/">the KDE Code of Conduct</a>. There will be a delay before they are published.</em></small>