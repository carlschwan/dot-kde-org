---
title: "KDE Commit-Digest for 5th August 2012"
date:    2012-10-10
authors:
  - "mrybczyn"
slug:    kde-commit-digest-5th-august-2012
---
KDE Commit Digest is getting back up to speed!

In <a href="http://commit-digest.org/issues/2012-08-05/">this week's KDE Commit-Digest</a>:

<ul><li>Work started on declarative canvas for <a href="http://krita.org/">Krita</a></li>
<li>Work on Sphinx support and face detection in <a href="http://simon-listens.blogspot.fr/">Simon</a></li>
<li>Seamless login from LightDM-KDE to KSplash
<li>Improved <a href="http://userbase.kde.org/KWin">KWin</a> decoration engine when switching themes</li>
<li>Aurorae Previewer added to Plasmate</li>
<li>Support for config values in QML decorations in KDE-workspace</li>
<li>Season of KDE (SoK) work in <a href="http://edu.kde.org/kstars/">KStars</a></li>
<li>Work on the import tool in <a href="http://www.digikam.org/">digiKam</a></li>
<li>New application: NepomukCleaner</li>
<li>Bugfixes in <a href="http://gwenview.sourceforge.net/">Gwenview</a>, kwebkitpart, kmix and others</li>
</ul>

<a href="http://commit-digest.org/issues/2012-08-05/">Read the rest of the Digest here</a>.