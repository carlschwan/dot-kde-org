---
title: "KDE e.V. Quarterly Report for Q1 2012"
date:    2012-06-26
authors:
  - "kallecarl"
slug:    kde-ev-quarterly-report-q1-2012
---
The KDE e.V. report for the first quarter of 2012 has been published. It gives an overview of the activities KDE e.V. supported during that time, including various sprints, conferences and projects. It also has a feature article by Stuart Jarvis about the ALERT Project, an initiative to improve bug resolution processes in Open Source development teams.

<a href="http://ev.kde.org/reports/ev-quarterly-2012_Q1.pdf">Read the full report today.</a>

The work of KDE e.V. would not be possible without our financial supporters. If you want to support the work of KDE but can't contribute directly, why not <a href="http://jointhegame.kde.org/">Join the Game</a>?