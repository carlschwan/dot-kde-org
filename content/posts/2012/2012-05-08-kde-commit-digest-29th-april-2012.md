---
title: "KDE Commit-Digest for 29th April 2012"
date:    2012-05-08
authors:
  - "mrybczyn"
slug:    kde-commit-digest-29th-april-2012
comments:
  - subject: "rekonq"
    date: 2012-05-08
    body: "omigosh~  the web browser has been my last great hold out of non KDE/Qt apps in daily use.  I think that is no more..."
    author: ""
  - subject: "Re: Dolphin view can be synchronized with terminal location"
    date: 2012-05-10
    body: "Any elaboration? No mention in commit messages. How exactly does the feature work? Thank you for the commit digest. More elaboration would be welcome."
    author: ""
  - subject: "More information in Commit Digest"
    date: 2012-05-10
    body: "The Commit Digest stories on the Dot offer highlights from the Commit Digest, and a link (well, 2 links usually) to the actual Commit Digest.\r\n\r\nIn this case...\r\n- the Commit Digest link on the Dot opens the Commit Digest page, which can be searched for \"terminal\".\r\n- that search turns up more information on this feature. The fix will be implemented in 4.9.0. Frank Reininghaus made the commit and thanked Jekyll Wu.\r\n- there is also a link to Bug 156732 where there is considerable information about the feature and what it took to get it implemented.\r\n\r\nWhat further elaboration do you suggest?"
    author: "kallecarl"
  - subject: "Nice improvements to Dolphin"
    date: 2012-05-10
    body: "Great to see some improvements in Dolphin, but shall we see any year a browsing history? Will we be able to click the back arrow, hold and see a menu showing the folders we have visited, and choose the one 6 steps back instead needing to click the back arrow 6 times? I know that this is a vanguard feature, only very advanced file managers have it, such as the file manager that my first computer had--Windows 95. What an advanced piece of software, eh?\r\nXD\r\n\r\nExcuse the sarcasm, but shouldn't basic features that everybody needs every day be priority?\r\n\r\nRegards"
    author: ""
  - subject: "Have you made a feature request?"
    date: 2012-05-11
    body: "I use Dolphin a lot. It has never occurred to me to have a file browsing history such as you describe. Besides, clicking step-wise back through the history makes a lot of sense. \r\n\r\nI question your assertion that everybody needs this every day. You apparently need it, but you're not \"everybody\".  If you made a feature request, it is possible that other people might see some value and support your request.\r\n\r\nDisparaging applications is not the best way to get what you want. \r\n"
    author: "kallecarl"
---
In <a href="http://commit-digest.org/issues/2012-04-29/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://dolphin.kde.org/">Dolphin</a> view can be synchronized with terminal location</li>
<li><a href="http://konsole.kde.org/">Konsole</a> has two new profile options for copy & paste mouse behavior</li>
<li>More search options in Dolphin with <a href="http://nepomuk.kde.org/">Nepomuk</a></li>
<li>Google Sync Bookmarks implemented in <a href="http://userbase.kde.org/Rekonq">rekonq</a></li>
<li>The order of units is now ensured in the whole <a href="http://www.calligra-suite.org/">Calligra</a> UI</li>
<li>The star rating may be hidden in <a href="http://kphotoalbum.org/">KPhotoAlbum</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2012-04-29/">Read the rest of the Digest here</a>.
