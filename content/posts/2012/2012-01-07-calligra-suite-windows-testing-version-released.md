---
title: "Calligra Suite for Windows - Testing Version Released"
date:    2012-01-07
authors:
  - "Bugsbane"
slug:    calligra-suite-windows-testing-version-released
comments:
  - subject: "kudos!"
    date: 2012-01-07
    body: "Very nice, kudos to all involved.  And, an msi-based installer as a bonus.  Can't wait to try it out."
    author: "rdieter"
  - subject: "Very cool"
    date: 2012-01-07
    body: "I hope that someday this project will not be worse than for the convenience of Microsoft Windows Office and even better ..."
    author: "Hronom"
  - subject: "Long-term windows plan"
    date: 2012-01-08
    body: "I would hope the long-term Windows plan includes an Update system that auto-loads small patches, rather than asking users to download a 150MB software suite for a point update.\r\n\r\nThis is one of the major reasons I don't use OpenOffice anymore."
    author: ""
  - subject: "Keep up the good work!"
    date: 2012-01-08
    body: "I haven't tried out calligra for a couple of years now but damn it has improved a lot! Great work guys."
    author: ""
  - subject: "Calligra is lightweight?"
    date: 2012-01-09
    body: "In which universe is a >150MB installer file lightweight?\r\nThat's pretty much exactly as big as the installer of OpenOffice for Windows INCLUDING(!) the Java Runtime!"
    author: ""
  - subject: "The installer includes also"
    date: 2012-01-09
    body: "The installer includes also Krita, which brings at lot of stuff with it. So separate installers are available, it should be much smaller. The installer also includes Qt and KDE stuff.\r\n\r\nI assume that without Krita it could get a size similar to the Amarok installer, which is at about 88 MB. Maybe with KDE Frameworks 5 could become smaller.\r\n\r\nKeep in mind that it's the first testing installer, so it is probably a lot of room for improvement."
    author: ""
  - subject: "And OO's Windows installer"
    date: 2012-01-09
    body: "And OO's Windows installer includes Java. So that should roughly level the field."
    author: ""
  - subject: "Re: Calligra is lightweight?"
    date: 2012-01-10
    body: "I guess \"lightweight\" refers to the amount of resources it consumes, not size of bundle.\r\n\r\n--\r\nAndreas"
    author: ""
  - subject: "since Calligra and kde depens"
    date: 2012-01-10
    body: "since Calligra and kde depens on QT what about getting Calligra-windows version into kde-windows installer. Then We dont have to install dups of the same libraries. Just food for tought"
    author: ""
  - subject: "I do see it the other way"
    date: 2012-01-11
    body: "I do see it the other way round... I'd like to install Calligra or for example only Krita without the whole KDE stuff. And I am pretty sure a lot of windows users think quite the same =)"
    author: ""
  - subject: "Unlikely"
    date: 2012-01-11
    body: "Yes, lot of people say that. But it's extremely unlikely to happen because when you look at it objectively, the \"KDE stuff\" is just a bunch of libraries we use to provide functionality we don't want to write ourselves. Just like the thirty or forty other libraries we package. We have stripped down the bundled Qt and KDE stuff to the minimum we use; right now, the largest part of the download is actually Krita and its resources."
    author: "Boudewijn Rempt"
  - subject: "Nice"
    date: 2012-01-11
    body: "I've been waiting for this, finally I can use my favorite applications in Windows as well without having to build them myself. :)\r\nHave to thank you for all the hard work and honestly I expect you to keep working hard even after the final 2.4 is out.\r\n\r\nNow for funs sake: Investigate why this file linked in the comments for the video won't open and add some error messages explaining the issue. http://www.youtube.com/watch?v=875AbZcWoCk it's stuck at 5% without any warnings for me.\r\nAnyways, glad to see this reaching a larger userbase soon!"
    author: ""
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/windows_artsymptom.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/windows_artsymptom300.jpg" /></a><br />Krita (click to enlarge)</div>

The <a href="http://calligra-suite.org/">Calligra</a> and <a href="http://windows.kde.org/">KDE on Windows</a> teams are proud to announce the first testing version of the Calligra Suite Windows installer. Yes, Windows users, you can now use and help test the Calligra from the comfort of your own operating system with a single installer.
<!--break-->
Calligra is the lightweight, free and open source office and creativity suite for spreadsheets, word processing, presentations, project management, flowcharts, digital painting and vector images. It uses the Open Document Format (ODF) to share files natively with other office suites while providing basic read support for Microsoft Office files (including .doc, .docx, .xls, .xlsx, .ppt, & .pptx).

Inge Wallin, marketing coordinator for Calligra, says: “We have long wanted to spread our user base to incorporate Windows users as well. The Calligra project does not release binaries, so we are happy that KO gmbH has made the effort to adapt and build Calligra for Windows. We hope that many users will try this package and report back any bugs they find.”

It should be noted that this is highly experimental, early beta software. <strong>Do not use it for critical production work, just yet</strong>. Remember, many Calligra developers use Linux exclusively, meaning we're relying on Windows users to let us know where we can improve things! Expect bugs. Expect crashes. Expect to report both to bugs.kde.org and help us make the best office suite on any operating system, period.

Some features may also be missing due to libraries that have yet to be ported to Windows (for example, some of Krita's high bit depth color modes).

Even in this early, unstable state though, we're already seeing real work being done by brave Calligra testers. Examples include this very announcement and the beautiful artwork of <a href="http://artsymptom.com/">Giovanny Arce</a> using <a href="http://krita.org/">Krita</a> on Windows and featured in the screenshot above.

Major thanks must also go out to <a href="http://kogmbh.com/">KO GmbH</a> (for creating and hosting the builds), <a href="http://nlnet.nl/">NLnet</a> (for funding assistance), Stuart MD who actually did all the work, and the KDE-Win team. It was a major effort, made easier by the surprising number of features that worked flawlessly "out of the box" due to the excellent QT and KDE Platform libraries Calligra is based on.

<strong>You can <a href="http://www.kogmbh.com/download.html">download your unstable, testing copy</a> now.</strong>

...and let the fun begin!

(Note: This installer includes the entire Calligra Suite. Single application installers for some apps are also on their way.)

About KO GmbH:
KO GmbH is an expert consulting and development company with services around office document applications in general, the Open Document Format in particular and conversions between office document formats. It is incorporated in Germany but employs developers all over Europe and the United States.

About NLnet:
The foundation "Stichting NLnet" stimulates network research and development in the domain of Internet technology. The articles of association for the NLnet foundation state: "to promote the exchange of electronic information and all that is related or beneficial to that purpose".