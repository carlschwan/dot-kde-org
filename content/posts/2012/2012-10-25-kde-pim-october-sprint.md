---
title: "KDE PIM October Sprint"
date:    2012-10-25
authors:
  - "kevkrammer"
slug:    kde-pim-october-sprint
comments:
  - subject: "As much as I love KMail I"
    date: 2012-10-25
    body: "As much as I love KMail I sometimes really miss the times when I used Opera email client and its dynamic filters. I never had to create mailing list filters, Opera did that for me based on message headers, I hope to see such intelligence in KDE PIM one day."
    author: "Max Blank"
  - subject: "Technically possible now"
    date: 2012-10-26
    body: "This is technically possible now with Nepomuk, as long as Nepomuk integration works painlessly. Please file a wish item for it at bugs.kde.org (severity = wishlist). Just mentioning the idea on the Dot will not make anything happen."
    author: "andras"
  - subject: "Here's a nice summary of some"
    date: 2012-10-26
    body: "Here's a nice summary of some of the fixes done at the sprint: http://community.kde.org/KDE_PIM/Meetings/PIM_October_2012_meeting#Meeting_Notes"
    author: "Eike Hein"
  - subject: "still can't spool directly to Inbox"
    date: 2012-11-01
    body: "How about moving mbox mail from /var/spool/mail to Inbox automatically? Is there any news about <a href=\"https://bugs.kde.org/show_bug.cgi?id=286793\">bug 286793</a>?\r\n\r\nThis feature used to be available in KMail. It should work like POP and IMAP."
    author: "Dieter Jurzitza"
  - subject: "There is no such thing as "
    date: 2012-11-06
    body: "There is no such thing as \"work like POP and IMAP\" because those two types of server access are so different.\r\n\r\nWith POP3 messages are fetched from the server, stored locally and then usually deleted from the server.\r\nWith IMAP messages stay on the server and are only fetched when needed (being accessed or being cached).\r\n\r\nThe mbox feature currently works like IMAP, the wish item report would like it to behave like POP3.\r\n"
    author: "kevkrammer"
---
While not as traditional as the annual KDE PIM meeting in Osnabrück near the beginning of each year, a second meeting around October/November is starting to become a tradition of its own. Similar to last year and in contrast to the Osnabrück meetings—which usually focuses on discussing ideas and planning—this year's October sprint again concentrated on improvements to existing features.

Supported by <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://www.kdab.com">KDAB</a>, a group of enthusiastic contributors gathered on Friday, October 12th in KDAB's Berlin offices and worked hard for the duration of the weekend to improve KDE PIM applications and their underlying infrastructure.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/pim-sprint-2012-10-group-photo2000B.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/pim-sprint-2012-10-group-photo565.jpg" /></a><br />left to right standing: Andreas Hartmetz, Sune Vuorela, Christian Mollekopf, John Layt, Kevin Krammer, Andras Mantia, Mark Gaiser, Milian Wolff, Volker Krause <br />sitting: Stephen Kelly, Tobias König, Martin Klapetek, Alex Fiestas</div>

<h2>Emails, lots of them</h2>
This guy at the sprint—let's call him Bob for privacy—not only has tons of email, he also likes having many folders with several hundred thousands of messages! Bob expects KMail and all involved background components to be able to deal with his situation. He actually expects them to work really well. And rightfully so!

Naturally a challenge like that could not go unanswered for long. So soon after Bob's arrival, several people started to look into different areas that would potentially be affected and then began investigating and implementing various improvements.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/pim-spring-2012systemdiagram300.JPG" /></a><br />Thinking big</div>

For example, Milian Wolff—famous for his work on KDevelop—took on the task of improving the runtime memory consumption of KMail itself, i.e. the actual end user application. Most developers confronted with that task would run, hide and cry themselves to sleep. However, Milian is KDAB's master of memory profiling and author of the <a href="http://milianw.de/blog/massif-visualizer-ready-for-testers-and-feedback">Massif Visualizer</a>. There was a distinct aura of defiance in the air.

On the other end of the processing chain, where emails are fetched from various backends, several people worked hard on speed, parallelism and timely progress reporting. One team, consisting of Alex Fiestas of Solid and BlueDevil fame, and Volker Krause, a.k.a. The Guru, worked on a new way to speed up the initial synchronization with IMAP servers. They started to refer to this as "The Spanish Sync", which is neither a reference to the Spanish Inquisition, nor any hint whatsoever on Bob's identity.

Somewhere along the processing chain, Andras Mantia, the guy who brought you Quanta+, fought various email filtering issues. All by himself. With one hand tied to his back: Victoriously!

Sources close to various email filtering issues report that there are discussions within the group to retire from the active fighting scene and move onto consulting. We shall see!

<h2>Teaching, Learning and Research</h2>
An important aspect of any KDE sprint, independent of the topic of interest or any strong focus area, is spreading knowledge!

Several people new to KDE PIM took the opportunity to learn about the inner workings of KDE PIM technologies, from design considerations (during an introduction to KDE PIM components) to implementation hints and best practices. Naturally, those who currently have this knowledge welcome these sessions as a means to reflect on things, gather input from new points of view and identify areas with missing or outdated documentation.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/pim-spring-2012smallgroup300.JPG" /></a><br />Sprinting hard</div>

Within such an environment, thriving on the free flow of information, where people with expert knowledge in lots of areas are readily available, new use cases being brainstormed, prototypes being shown off and world domination plans being refined, some minds are tempted to explore uncharted territory.

Kevin Krammer—famous bridge builder and chocolate supplier—indulged in a project which should, once it is completed, allow fluent JavaScript developers access to the KDE PIM infrastructure similar to what their C++ knowledgeable counterparts currently enjoy.

Being a quite persuasive chap, Kevin managed to divert another developer, Tobias König (of Address Book fame) from The True Path<sup>TM</sup> and into the abyss of unbounded hacking. It's rumored that this project is all about UI. And QML. And QtWidgets. Something not quite entirely different, but better.

More tangible results were achieved by Mark Gaiser, continuing his work on QML-based user interfaces for calendaring, and Martin Klapetek—known for his outstanding work on KDE Telepathy—who continued to improve on the Social Feed integration he started with this year as Google Summer of Code project.

<h2>Socializing, videos and dogs</h2>
Yes, you read that correctly. No videos involving cats. Sorry!

KDE sprints always provide good opportunities to socialize, put faces to email addresses or IRC nicks, chat about life, the universe and everything during breakfast, lunch and dinner. Deep undercover informers reported that some people were even seen drinking beer while lounging on the couches in KDAB's office. Rest assured that the proper authorities are on to them!

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/popcorn.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/popcorn_wee.jpg" /></a><br />Also new but mostly there to distract: Popcorn</div>

During the sprint Alex and Martin, who are part of a Behind the Scenes of KDE effort called <a href="http://www.youtube.com/user/kdeteatime">KDE TeaTime</a> sat down with Kevin Krammer and Volker Krause, and talked about KDAB, KDE PIM, Akonadi and other things. Watch the <a href="http://www.youtube.com/watch?v=i8X4G9yXhLA&feature=plcp">video</a>.

The Pimsters<sup>TM</sup> and, of course, Bob, greatly enjoyed visits from other members of the wider KDE Community: Plasma guru Chani Armitage, KDE Women activist Camila Ayres, openSUSE community dude and KDE Promo legend Jos Poortvliet and, really needing no elaborate introduction, Jos and Camila's dog Popcorn, currently being trained to herd developers.