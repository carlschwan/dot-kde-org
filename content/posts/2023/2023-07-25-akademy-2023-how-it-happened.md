---
title: "Akademy 2023 - How it Happened"
date:    2023-07-25
authors:
  - "Paul Brown"
slug:    akademy-2023-how-it-happened
---
<figure>
<img src="/sites/dot.kde.org/files/question.jpg" alt="Audience member asks a question at Akademy 2023" />
</figure>

<i><a href="https://akademy.kde.org/">Akademy</a> is KDE's annual event where the community comes together in one place to meet in person, share ideas and hack on common projects.</i>

<i>This year's Akademy was held in Thessaloniki, Greece and started on July 15th and ran until July 21st. This year 150 people attended Akademy in person, and 220 tuned in online to attend chats and BoFs over video conference.</i>

<i>The first weekend of Akademy, as is tradition, was dedicated to talks, panels and fireside chats. The sessions, which were streamed live to the whole world, covered a wide variety of KDE-related topics, ranging from the hot topic of the road to Plasma 6, to how to hack off-the-shelf solar panels, and many things in between.</i>

<h2>Day 1 - Saturday 15 July</h2>

<h4>09:45 Opening</h4>

Aleix Pol, president of KDE, opened the event and thanked all attendees and sponsors for making the event possible.

<h4>10:00 Empowering Open-Source Space Technologies</h4>

The first keynote of Akademy 2023 was given by Eleftherios Kosmas from the <a href="https://libre.space/">Libre Space Foundation</a>. Eleftherios explained how the LSF is making inroads in space exploration and technology through the use of a wide range of open source technologies. He talked about how free software and hardware are carving out a niche in the ultra-competitive and cut-throat space industry, despite the fact that, as he reminded the audience several times, "space is hard".

<h4>11:15 KDE Goals</h4>

In the traditional <a href="https://community.kde.org/Goals">Goals</a> time slot, Nate Graham, leader of the <i>Automate and Systematize Internal Processes</i> goal, explained how they intend to preserve the knowledge from one generation of KDE contributors to the next; Carl Schwan listed the ways the <i>Accessibility</i> goal allows more people to use KDE software in more ways than one; and Joseph De Veaugh-Geiss highlighted the milestones of the <i>KDE Eco</i> goal.

<i>At this point, the conference split into two tracks.</i>

<h4>12:30 Measuring Energy Consumption</h4>

In room 1, and related to the above, Volker Krause took us on a tour of how we, the end users, can start contributing to greener computing by measuring consumption at home. Volker walked us through the available software and then told us about devices, some expensive, but some surprisingly cheap and effective, that everyone could use.

<h4>12:30 A Tale of Kleopatra</h4>

In room 2, Ingo Klöcker gave us a glimpse of how, over the course of a year, it was possible to push KDE's cryptography key manager <a href="https://apps.kde.org/kleopatra/">Kleopatra</a> from barely accessible to accessible with minor restrictions, often thanks to, sometimes in spite of Qt.

<h4>14:30 KDE e.V. Board Report</h4>

After lunch, the <a href="https://ev.kde.org/corporate/board/">KDE e.V. board</a> presented their annual report to the attendees. The board members have done a lot of work to support the KDE community over the past year. In this session, board members Adriaan de Groot, Aleix Pol Gonzalez, Lydia Pintscher, and Nate Graham (Eike Hein couldn't make it) told the audience about the things the e.V. has done, the work of the organization, and future endeavors.

Topics covered included contractors (we have more of them now), events (including external events), sprints, fundraising and sponsors, highlights of the past year, and future plans.

<h4>14:30 Over a million reasons why Snaps are important</h4>

In room 2, we heard from Scarlett Moore that there have been more than one million downloads of <a href="https://snapcraft.io/">Snaps</a> since the project began in 2015. This means that Snap is a packaging system to be reckoned with. Scarlett told us how she got into the workflow of building a huge amount of Snaps and how she managed to keep them updated using KDE's invent.kde.org platform and Ubuntu's launchpad.

<h4>15:15 KDE e.V. Working Group reports</h4>

Back in room 1, we attended a panel hosted by Lydia Pintscher on <a href="https://ev.kde.org/workinggroups/">KDE's working groups</a>. Neofytos Kolokotronis talked about the Community WG and how its main mission is to act as a mediator, ensuring smooth communication between community members and defusing conflicts. David Edmundson told us about the Finance WG and how we had deliberately overspent 2022 to reduce the accumulated funds in KDE's coffers, and the ways the extra expenditure is being used to help strengthen the community. Carl Schwan explained how the Fundraising WG has had several recent successes, but continues to look for new opportunities. David Redondo of the KDE Free Qt Foundation, responsible for protecting the free version of Qt, told us about progress in the relationship between KDE and the Qt Group. Finally, Bhushan Shah, representing the Sysadmin WG, talked about updates to KDE's hardware infrastructure.

<h4>15:15 Flatpak and KDE</h4>

At the same time, in room 2, Albert Astals talked about another way to distribute software, this time using <a href="https://www.flatpak.org/">Flatpaks</a>. In his talk, Albert discussed what Flatpak is, why it's interesting for KDE, and talked about the different ways developers can build and distribute their software using Flatpak, Flathub, KDE's CI and binary factory infrastructures, and so on.

<h4>16:25 KF6 - Are we there yet?</h4>

Later, in room 1, Alexander Lohnau, Nicolas Fella and Volker Krause talked about the current state of <a href="https://develop.kde.org/products/frameworks/">KDE's frameworks and toolkits</a>, and the progress and challenges of migrating to Qt 6.

<h4>16:25 Documentation goals and techniques for KDE and open source</h4>
In room 2, Thiago Sueto discussed what it is to be a technical writer, what he does, and what documentation goals and technologies are used to achieve them, focussing on KDE in particular, but also on open source in general.

<h4>17:05 Plasma 6 is coming</h4>

Marco Martin and Niccolò Venerandi then took the stage in room 1 and showed us many of the new visual improvements we should expect to see in Plasma 6 (Niccolò) and the underlying technical parts, changing components and APIs (Marco).

<figure>
<img src="/sites/dot.kde.org/files/Niccolo.jpg" alt="Niccolò Venerandi talks about Plasma 6 at Akademy 2023" />
</figure>

<h4>17:05 UIs in Rust with Slint</h4>

At the same time in room 2, Tobias Hunger introduced us to <a href="https://slint.dev/">Slint</a>, a UI framework written in Rust with bindings to Rust, C++, and Javascript. Slint scales from microcontrollers with no OS and only a few KiB of RAM to desktop UIs supported by graphics accelerators. The presentation showed what slint is and how to build a small UI with it.

<h4>17:55 - KRunner: Past, Present, and Future</h4>

In room 1, Alexander Lohnau talked about <a href="https://userbase.kde.org/Plasma/Krunner">KRunner</a> and how he started developing for KDE three years ago, thanks in part to KDE's search-run-and-do-a-lot-more utility. In his talk, Alexander covered the changes needed to migrate KRunner to Plasma 6, and explored how to port and improve existing runners.

<h4>17:55 - KDE Embedded - Where are we?</h4>

Meanwhile, in room 2, Andreas Cord-Landwehr talked about the tools KDE has for easily creating embedded devices and how they work; which devices are most interesting at the moment; the concept of an immutable image; and the next topics and directions KDE community members should pursue in the embedded area.

<h2>Day 2 - Sunday 16 July</h2>

<h4>10:00 Kdenlive - what can we learn after 20 years of development?</h4>

Eugen Mohr, Jean-Baptiste Mardelle and Massimo Stella from the <a href="https://kdenlive.org/">Kdenlive</a> team took us down memory lane, from the very beginning to the present day. They told us about how the team came together, the hurdles they had to overcome, the current situation and the plans for the future of KDE's popular video editing software.

<h4>10:50 Make it talk: Adding speech to your application.</h4>

Jeremy Whiting believes that speech is an underrepresented but perfectly valid way to communicate with users. Jeremy proved his point with examples of applications that, if speech-enabled, would help people who are visually impaired (or just looking at something else), and went on to explain <a href="https://doc.qt.io/qt-6/qtexttospeech.html">Qt technologies</a> that could be used to integrate speech into KDE applications.

<h4>11:00 The Community Working Group - Keeping a Healthy Community</h4>

In this talk, Andy Betts presented the work of the <a href="https://ev.kde.org/workinggroups/cwg/">Community Working Group</a>, how it resolves conflicts between community members, and advice on how to get along within the KDE community.

<i>At this point, the conference split into two tracks.</i>

<h4>11:40 Internal Communication At KDE: Infrastructure For A Large And Diverse Community</h4>

In room 1, Joseph De Veaugh-Geiss took the stage and walked us through some of the problems that a community as large as ours has when it comes to communicating with each other and the outside world.

<h4>11:40 - An OSS Tool for Comprehending Huge Codebases</h4>

In room 2, Tarcisio Fischer talked about an OSS tool being developed by CodeThink to help developers understand large codebases. Although the tool is still under heavy development, he showed and explained the tool using KF5, Kate, Konsole as case studies.

<h4>12:25 The Evolution of KDE's App Ecosystem and Deployment Strategy</h4>

Back in room 1, Aleix Pol explored how the Linux application ecosystem has changed and the implications for KDE.

<h4>12:25 Matrix and ActivityPub for everything</h4>

At the same time, in room 2, Alexey Rusakov, Carl Schwan, and Tobias Fella talked about the <a href="https://matrix.org/">Matrix IM</a> platform and <a href="https://activitypub.rocks/">ActivityPub</a> standards and how they can be used outside of their primary purpose. They gave a broad overview of the functionality provided by Matrix and how it fits into the ActivityPub protocols, which are primarily intended for real-time messaging and social networking applications.

<figure>
<img src="/sites/dot.kde.org/files/1000-709-max.jpg" alt="Akademy 2023 group photo" />
</figure>

<i>During the lunch break, all attendees got together in the main conference hall, and later in the University's lobby for the all-important group photo.</i>

<h4>14:30 Selenium GUI Testing</h4>

After lunch, in room 1, Harald Sitter told us about and demonstrated <a href="https://eco.kde.org/categories/selenium/">Selenium</a>, a technology for testing GUIs of KDE software. Selenium can be used for regular testing, testing for accessibility problems, and also for energy efficiency.

<h4>14:30 Remote Desktop for KWin Wayland</h4>

In room 2, Arjen Hiemstra showed-cased how he was working to fix the lack of networking support in Wayland.

In X11, running graphical applications remotely was a core feature. Wayland does not implement this feature natively, but remote desktop control remains an important use case for a number of users.

Arjen showed how it is possible to use KDE's <a href="https://userbase.kde.org/KWin">KWin</a> compositor, combined with a new library called KRdp, to control any KWin Wayland session over a network.

<h4>15:15 Testing the latest KDE software</h4>

Later, in room 1, Timothée Ravier joined us virtually and discussed what, when and how non-developers can test the latest KDE software, including Plasma Desktop and Apps. He also gave a demo using Flatpak.

<h4>15:15 Entering a Wayland-only World</h4>

Meanwhile, in room 2, Neal Gompa of the Fedora Project analyzed how KDE is doing with its move to Wayland and what problems still need to be solved.

<h4>16:20 KDE Wayland Fireside Chat</h4>

Right after that, Room 2 was handed over to Aleix Pol Gonzalez, David Edmundson, David Redondo, Vlad Zahorodnii, and Xaver Hugl for a fireside chat with the attendees about <a href="https://wayland.freedesktop.org/">Wayland</a>...

<figure>
<img src="/sites/dot.kde.org/files/IMG_3867.jpeg" alt="Wayland Fireside at Akademy 2023" />
</figure>


<h4>16:20 Kyber: a new cross-platform high-quality remote control software</h4>

... And a round of 10-minute Fast Track talks began in room 1, with Jean-Baptiste Kempf of VLC fame kicking things off with an explanation and demo of his new project, Kyber.

Kyber attempts to provide a high quality, 0 latency video feed, with bi-directional input forwarding, providing a cross-platform application and SDK to control any type of machine, regardless of hardware and operating system.

<h4>16:30 Fun with Charts: Green Energy in System Monitor</h4>

The second lightning talk was given by Kai Uwe Broulik, who explained how he managed <a href="https://github.com/kbroulik/qalphacloud/tree/master">to get data from off-the-shelf solar panels</a> (that originally used proprietary software) and visualize it in Plasma's system monitor, alongside things like CPU and network load.

<h4>16:40 What has qmllint ever done for us?</h4>

Then Fabian Kosmale explained the usefulness of <a href="https://doc.qt.io/qt-6/qtquick-tool-qmllint.html">qmllint</a>, a command-line utility originally designed to check that QML files are syntactically valid. But it has evolved to cover more checks, has been integrated into CI pipelines, and has a plugin API on the way.

<h4>16:50 Wait, are first-run wizards cool again?</h4>

Finally, Nate Graham introduced the new Plasma Wizard and explained why it was necessary in this day and age.

<i>Both tracks rejoined for the final leg of the conference section of Akademy.</i>

<h4>17:05 Sponsors lightning talks</h4>

Akademy would not be possible without the support of our sponsors. Aleix Pol welcomed to the stage representatives from the <a href="https://www.qt.io/">Qt Group</a>, <a href="https://www.codethink.co.uk/">Codethink</a>, <a href="https://www.kdab.com/">KDAB</a>, <a href="https://canonical.com/">Canonical</a>, <a href="https://www.collabora.com/">Collabora</a>, <a href="https://www.opensuse.org/">openSUSE</a>, <a href="https://haute-couture.enioka.com">enioka Haute Couture</a> and <a href="https://extenly.com/">Externly</a>. The representatives, many of whom are also KDE members, explained what they do and received a round of applause from the audience.

Other sponsors who were not present at the event, but who were also applauded by the attendees, were <a href="https://kde.slimbook.es/">Slimbook</a>, <a href="https://www.tuxedocomputers.com/">TUXEDO</a>, and <a href="https://pine64.org/">PINE64</a>.

The media partner for the event was <a href="https://bit.ly/Linux-Update">Linux Magazine</a>.

<h4>17:50 Akademy Awards</h4>

The Akademy Awards recognize outstanding contributions to KDE. Aniqa Khokhar and Harald Sitter, last year's winners, presented this year's winners:

<ul>
    <li>In the category of Best Application, the winner was <a href="https://apps.kde.org/heaptrack/">Heaptrack</a>, maintained by Milian Wolff and the rest of the development team.</li>
    <li>The award for Best Non-Application Contribution went to Hannah Von Reth for her work on KDE's *Craft* system.</li>
    <li>The Jury Award went to Johnny Jazeix for his work organizing and supporting *Season of KDE* and *Google Summer of Code*.</li>
</ul>

Finally, the special award went to the local team representing the <a href="https://www.uom.gr">University of Macedonia</a>, co-organizer of the event. The KDE community cannot thank the University and the organizing crew enough for the work they put into making Akademy 2023 a success.

<figure>
<img src="/sites/dot.kde.org/files/award_0.jpg" alt="The local team receives the award for their hard work making Akademy possible." />
</figure>


<h4>18:10 Closing</h4>

Aleix Pol closed the conference part of the event and the five days of BoFs, hackathons and trainings began.
<!--break-->
