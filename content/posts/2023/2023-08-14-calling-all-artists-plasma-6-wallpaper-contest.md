---
title: "Calling All Artists! - The Plasma 6 Wallpaper Contest"
date:    2023-08-14
authors:
  - "Paul Brown"
slug:    calling-all-artists-plasma-6-wallpaper-contest
---
<figure>
<img src="/sites/dot.kde.org/files/wallpapercontest_1500_0.jpg" alt="Wallpaper Contest announcement poster" />
</figure>

Plasma 6 release day is getting closer... and we still have no wallpaper to use! But we're going to change that with your help and this contest! We are calling on all artists to submit their original wallpaper designs and compete for the chance to win a brand-new laptop (see below). The contest will be open for three months, starting now!

<h2>The Contest</h2>

The winning entry will be used as the default wallpaper for KDE Plasma 6.0. <a href="https://kde.org/plasma-desktop">Learn more about Plasma</a>.

<h4>Theme</h4>

Wallpaper entries should somehow suggest the following themes:

<ul>
  <li><b>Trustworthy</b> (i.e. reliable, stable, dependable)</li>
 <li><b>"A reflection of me"</b> (i.e. personalized, familiar, comfortable, "fits like a glove")</li>
 <li><b>Personal growth</b> (i.e. independence, level mindedness, critical thinking)</li>
</ul>

<i>You should not feel constrained by the themes. Quite the contrary: The goal is for them to inspire your design and maybe help evoke these feelings/thoughts in the viewer, but you should take the abstract nature of the ideas as a way to freely express your creativity in your design.</i>

<h4>Style</h4>

<ul>
 <li>KDE Plasma 6 is a whole new Plasma! Be bold with your design, and don't feel like you have to adhere to the visual style of Plasma 5 wallpapers. A unique style is fine, even more so if it can start a design trend for the following Plasma 6 versions!</li>
 <li>Lean on the abstract. Representational and literal elements should be used sparingly.</li>
 <li>We'd recommend <i>avoiding</i> text elements in your wallpaper, such as "Plasma" or even "6". However, this is not a strict rule: if it works with your style, then it works!</li>
 <li>Similarly, we would recommend <i>avoiding</i> logos overlaid on top of the wallpaper; if you do decide to include a logo, it should integrate with the design and not be used for quick identification.</li>
</ul>

<figure>
<img src="/sites/dot.kde.org/files/wallpapers_1500.png" alt="Some examples from past Plasma releases." />
<br /><figcaption>Some examples from past Plasma releases.</figcaption>
</figure>

<h4>Rules</h4>

<ul>
 <li>The wallpaper must be original, created specifically for this contest, and released under the CC-BY-SA-4.0 license. Therefore no submissions using AI art will be accepted.</li>
 <li>The minimum required size for wallpapers is 3840x2160, though 5120x2880 is preferred. Vertical wallpapers should be at least 1080x2280.</li>
 <li>You are allowed to submit up to 3 wallpapers. Entries can be made in public at <a href="https://discuss.kde.org/c/community/wallpaper-competition/26">in this Discuss category</a>, but you can also submit them in private to niccolo.venerandi@kde.org</li>
 <li>Each submission should include the following: </li>
 <ul>
  <li>The wallpaper</li>
  <li>The name of the wallpaper</li>
  <li>A dark mode version of the wallpaper (not mandatory, but recommended)</li>
  <li>A vertical version of the wallpaper (not mandatory, but recommended)</li>
 </ul>
 <li>If asked, you should be able to provide the source files used to create the wallpaper in a non-proprietary format, like an Inkscape-compatible SVG, .blend, .kra, .xcf, etc.</li>
 <li>Any submission containing racist, sexist, demeaning, or any other inappropriate content will be removed and disqualified immediately.</li>
 <li>The organizers will be accepting submissions up to <b>11:59 pm of November 14, 2023</b>.</li>
</ul>

<h4>Selection process</h4>

Judges for the competition will be selected from the KDE Visual Design Group and other esteemed community members. Wallpapers will be judged based on artistic merit, originality, and adherence to the design themes mentioned earlier. At the end of the submission period, six finalists will be selected for a second round.

Artists who make it to this stage will receive a small prize (e.g. a KDE t-shirt!) and actionable feedback from the judges. The artists will be able to upload different variations addressing the feedback. This stage will take between one and three weeks. At the end of it, a winner will be selected and announced. The winner will get a <a href="https://frame.work/">Framework Laptop 13</a>!

While there will only be one winning selection, other submissions may be included as optional wallpapers in Plasma 6, or used in future releases. The judges' decision is final.

<h2>The Prize</h2>
<figure>
<img src="/sites/dot.kde.org/files/framework_1500.jpg" alt="Framework 13'' computer" />
</figure>

The winner of the contest will receive a Framework Laptop 13 DIY Edition, featuring a 13th Gen Intel® Core™ i5-1340P. This laptop has a modular design and can easily be wholly disassembled and rebuilt, allowing you to replace any part or upgrade key components anytime.

The Framework Laptop 13 also features an Expansion Card system that lets you choose exactly the ports you want and where you want them; there are four available slots and a great variety of cards: USB-C, USB-A, HDMI, DisplayPort, MicroSD, Ethernet, Audio, ultra-fast storage, and more.

Of course, compatibility was tested with the most popular Linux distributions and Framework offers many step-by-step setup guides for them.

<h3><a href="https://discuss.kde.org/t/wallpaper-competition-announcement/3773">Join the competition here!</a></h3>
<!--break-->
