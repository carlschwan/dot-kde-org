---
title: "g10 Code Becomes a KDE Patron"
date:    2023-04-25
authors:
  - "Paul Brown"
slug:    g10-code-becomes-kde-patron
---
<img class="img-fluid" src="/sites/dot.kde.org/files/g10_logo.png" alt="GNUPG.com logo." width="100%" /> 

<b><a href="https://gnupg.com/">g10 Code GmbH</a> joins the ranks of KDE patrons!</b> g10 Code provides custom development, enhancements, and audits of cryptographic software -- in particular for the popular GnuPG encryption and digital signature tools.

<figure style="float: right; padding: 1ex 1ex 1ex 3ex;"><img src="/sites/dot.kde.org/files/Werner_Koch_Portrait_01.2015-2.jpg" width="200" /><br /><figcaption>Werner Koch, CEO g10 Code</figcaption></figure>

"The KDE Community supports us in providing professionally-designed, accessible desktop software to our users in many different languages," states CEO Werner Koch. "While we consider KDE's KMail mail client to have the best GnuPG integration, our main businesses case comes from Windows users in professional settings using our <a href="https://gnupg.com/gnupg-desktop.html">GnuPG VS-Desktop</a> product, which is approved by Germany, the EU, and NATO for use with restricted documents. This allowed us to change from <a href="https://gnupg.org/blog/20220102-a-new-future-for-gnupg.html">donation-based development</a>. Our free-of-charge distribution <a href="https://www.gpg4win.org">Gpg4win</a> has hundreds of thousands of downloads per month and is used by NGOs, journalists, and most "Tor-based" transactions. This is all only possible because we provide a KDE-based user interface to GnuPG with KDE's Kleopatra app."

Says KDE e.V. President Aleix Pol Gonzalez: "KDE has a well-established reputation for prioritizing privacy and security. For end-users, implementing effective security measures is important but also challenging. I'm looking forward to working further with g10 towards building great cryptographic solutions that are easy to adopt in organisations of all sizes as well as on our individual systems."

g10 Code joins KDE e.V.’s other Patrons: Blue Systems, Canonical, Google, Kubuntu Focus, Slimbook, SUSE, The Qt Company, and TUXEDO Computers to continue to support Free Open Source Software and KDE development through KDE e.V.
<!--break-->