---
title: "Season of KDE 2023: Mentees and Projects"
date:    2023-01-23
authors:
  - "unknow"
slug:    season-kde-2023-mentees-and-projects
---
By Benson Muite

<img class="img-fluid" src="/sites/dot.kde.org/files/Kde_dragons_transparent_1500.png" alt="Image of dragons in different colors representing the diverse and inclusive KDE community" width="100%" />

This year Season of KDE has several projects focusing on the accessibility and sustainability <a href="https://kde.org/goals/">goals</a>. There are three projects focused on accessibility, three on sustainability and three additional projects in other areas.

The sustainability projects had fifteen excellent applicants for just three projects, so selecting mentees was challenging. The time mentee applicants invested in applying is much appreciated, and any applicants who have not been selected are encouraged to continue contributing to KDE and open source. It is possible to make smaller contributions to KDE projects that allow possible mentors to see your work and then mentor you informally.

The projects and their mentees that were officially selected for SoK are listed below.

<h3>Sustainability Projects</h3>

These projects are related to the <a href="https://eco.kde.org/#be4foss">Blauer Engel (Blue Angel) for FOSS project</a>, a certification for environmentally friendly open source software. The <a href="https://www.blauer-engel.de/en">Blauer Engel project</a> is part of the <a href="https://globalecolabelling.net/">Global Ecolabelling Network</a>, a non-profit organization that certifies eco-friendliness of products within a particular class, and thus allows users to make informed choices.
<ul>
  <li><a href="https://community.kde.org/SoK/Ideas/2023#Sustainability_1:_Preparation_of_KDE_apps_for_Blue_Angel_eco-certification">Preparation of KDE apps for Blue Angel eco-certification</a></li>
  <ul>
    <li>Mentor: <i>Joseph P. De Veaugh-Geiss</i></li>
    <li>Mentee: <b>Rudraksh Karpe</b></li>
  </ul>
  <li><a href="https://community.kde.org/SoK/Ideas/2023#Sustainability_2:_Improve_.22KDE_Eco_Test.22_emulation_tool">Improve KDE Eco Test emulation tool</a></li>
  <ul>
    <li>Mentors: <i>Karanjot Singh</i>, <i>Emmanuel Charruau</i>, and <i>Joseph P. De Veaugh-Geiss</i></li>
    <li>Mentee: <b>Mohammed Ibrahim</b></li>
  </ul>
  <li><a href="https://community.kde.org/SoK/Ideas/2023#Sustainability_3:_Measurement_of_energy_consumption_with_Selenium">Measurement of energy consumption with Selenium</a></li>
  <ul>
    <li>Mentors: <i>Emmanuel Charruau</i> and <i>Harald Sitter</i></li>
  <li>Mentee: <b>Nitin Tejuja</b></li>
  </ul>
</ul>
<h3>Accessibility Projects</h3>

These projects aim to allow eveybody to use KDE software and KDE websites by improving their accessibility.

<ul>
  <li>Rishi Kumar will be working on the accessibility and UI testing of   <a href="https://apps.kde.org/fr/tokodon/">Tokodon</a>. This is based on the<a href="https://apachelog.wordpress.com/2022/12/14/selenium-at-spi-gui-testing">AT-SPI/Selenium bridge</a> created by Harald Sitter.</li>
  <ul>
    <li>Mentor: <i>Carl Schwan</i></li>
    <li>Mentee: <b>Rishi Kumar</b></li>
  </ul>
  <li><a href="https://community.kde.org/SoK/Ideas/2023#Accessibility:_Work_on_improving_the_accessibility_of_KDE.27s_websites_.28slot_filled.2C_do_not_submit_a_proposal_for_it.29">Improving the accessibility of KDE’s websites</a></li>
  <ul>
    <li>Mentor: <i>Paul Brown</i> and <i>Aniqa Khokhar</i></li>
    <li>Mentee: <b>Victoria Chen</b></li>
  </ul>
  <li><a href="https://community.kde.org/SoK/Ideas/2023#Accessibility:_Plasma_Accessibility_Widget_.28Slot_filled.29">Plasma accessibility widget</a> </li>
  <ul>
    <li>Mentor: <i>Fushan Wen</i></li>
  <li>Mentee: <b>Brent Mackey</b></li>
  </ul>
</ul>

<h3>Additional Projects</h3>

<ul>
  <li><a href="https://community.kde.org/SoK/Ideas/2023#Systematization_1:_Automate_Flatpak_checks_in_GitLab_Invent_CI">Automate Flatpak checks in GitLab Invent CI</a></li>
  <ul>
    <li>Mentors: <i>Timothée Ravier</i> and <i>Aleix Pol</i></li>
    <li>Mentee: <b>Neelaksh Singh</b></li>
  </ul>
  <li>Brannon Aw will be working on improving the annotation tools in <a href="https://apps.kde.org/spectacle/">Spectacle (KDE's screenshot app)</a>. They will be adding features such as cropping, an eraser tool, and so on. <a href="https://community.kde.org/SoK/Ideas/2023#Spectacle:_Improving_the_annotation_tools_.28Slot_filled.29">Spectacle: Improving the annotation tools</a>.</li>
  <ul>
    <li>Mentor: <i>Bharadwaj Raju</i></li>
    <li>Mentee: <b>Brannon Aw</b></li>
  </ul>
  <li><a href="https://community.kde.org/SoK/Ideas/2023#Plasma:_Better_holiday_support_in_the_digital_clock_widget_.28Slot_filled.29">Plasma: Better holiday support in the digital clock widget</a>.</li>
  <ul>
    <li>Mentor: <i>Fushan Wen</i></li>
    <li>Mentee: <b>Ruoqing He</b></li>
  </ul>
  <li>Théophile Gilgien will be working improving <a href="https://apps.kde.org/audiotube/) (a YouTube music client based on Kirigami">Audiotube</a>. He will be working on multiple small features like KRunner integration, ability to play favorite songs as a playlist and more! </li>
  <ul>
    <li>Mentor: <i>Carl Schwan, Devin Lin & Jonah Brüchert</i></li>
    <li>Mentee: <b>Théophile Gilgien</b></li>
  </ul>
  <li>Arpit Jain wil be working on the new epub reader, <a href="https://apps.kde.org/arianna/">Arianna</a>, which is based on epub.js and QtWebEngine. His knowledge of web-based technologies will be helpful to achieve his goals of syncing the application color scheme to the webengine content, adding settings and adding a table of content. </li>
  <ul>
    <li>Mentor: <i>Carl Schwan</i></li>
    <li>Mentee: <b>Arpit Jain</b></li>
  </ul>
</ul>

<figure style="float: right; padding: 1ex 1ex 1ex 3ex;"><img src="https://community.kde.org/images.community/thumb/7/79/Mascot_konqi-app-dev.png/261px-Mascot_konqi-app-dev.png" alt="Image of Konqi, the KDE mascot using a computer with multiple screens" width="200" /></figure>
<h3>Good Luck and have Fun!</h3>

KDE thanks the mentors and mentees for improving the KDE ecosystem and wishes them a good experience. The KDE community looks forward to learning about the mentees progress through their blog posts.
<!--break-->
