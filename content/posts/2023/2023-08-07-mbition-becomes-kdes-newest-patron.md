---
title: "MBition Becomes KDE's Newest Patron"
date:    2023-08-07
authors:
  - "Paul Brown"
slug:    mbition-becomes-kdes-newest-patron
---
<figure>
<img src="/sites/dot.kde.org/files/head.jpg" alt="MBition's logo overlaid over a futuristic looking car dashboard" />
</figure>

<b>MBition becomes a KDE patron</b> and supports the work of the KDE community with its generous sponsorship.

<a href="https://mbition.io/)">MBition</a> designs and implements the infotainment system for future generations of Mercedes-Benz cars and relies on KDE's technology and know-how for its products.

"<i>After multiple years of collaboration across domains, we feel that becoming a patron of KDE e.V is the next step in deepening our partnership and furthering our open-source strategy</i>“ says Marcus Mennemeier, Chief of Technology at MBition.

"<i>We are delighted to welcome MBition as a Patron,</i>" says Lydia Pintscher, Vice President of KDE e.V. "<i>MBition has been contributing to KDE software and the stack we build on it for some time now. This is a great step to bring us even closer together and support the KDE community, and further demonstrates the robustness and hardware readiness of KDE's software products.</i>"

MBition joins KDE e.V.'s other patrons: Blue Systems, Canonical, g10 Code, Google, Kubuntu Focus, Slimbook, SUSE, The Qt Company and TUXEDO Computers, who support free open source software and KDE development through KDE e.V.
<!--break-->