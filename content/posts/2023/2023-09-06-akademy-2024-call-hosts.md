---
title: "Akademy 2024 Call for Hosts"
date:    2023-09-06
authors:
  - "aniqakhokhar"
slug:    akademy-2024-call-hosts
---
<figure>
<img src="/sites/dot.kde.org/files/F3egnymXkAAXzgn.jpg" alt="The KDE Community at an outdoor event at Akademy 2023" />
</figure>

One of the biggest things you can do for KDE (that does not involve coding) is helping us organize <a href="https://akademy.kde.org/">Akademy</a>. 

Now is your chance to become KDE champions and help make Akademy 2024 happen! We are looking to host Akademy 2024 during the months of June, July, August, and September. Download the <a href="https://ev.kde.org/akademy/CallforHosts_2024.pdf" target="_blank">Call for Hosts</a> guide, and submit a proposal to host Akademy in your city to akademy-proposals@kde.org by October 1, 2023.

Do not hesitate to send us your questions and concerns! We are here to help you organize a successful event and you can reach out at any time for advice, guidance, or any assistance you may need. We will support you and help you make Akademy 2024 an event to remember.
<!--break-->