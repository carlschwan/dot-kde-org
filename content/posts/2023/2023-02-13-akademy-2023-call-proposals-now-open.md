---
title: "Akademy 2023 Call for Proposals is Now Open!"
date:    2023-02-13
authors:
  - "aniqakhokhar"
slug:    akademy-2023-call-proposals-now-open
---
<img src="/sites/dot.kde.org/files/photo_2023-01-27_16-44-04.jpg" alt="" width="1280" height="426" />

<a href="https://akademy.kde.org/2023/">Akademy</a> 2023 will be a hybrid event held in <a href="https://www.visitgreece.gr/mainland/macedonia/thessaloniki/">Thessaloniki</a>, Greece, and online from Saturday the 15th to Friday the 21st of July. <b><a href="https://akademy.kde.org/2023/cfp/">The Call for Participation</a></b> is open! <a href="https://conf.kde.org/event/5/abstracts/">Send us</a> your talk ideas and abstracts.

<h3>Why talk at #Akademy2023?</h3>

Akademy attracts artists, designers, developers, translators, users, writers, companies, public institutions and many other KDE friends and contributors. We celebrate the achievements and help determine the direction for the next year. We all meet together to discuss and plan the future of the Community and the technology we build. You will meet people that are receptive to your ideas and can help you with their skills and experience. These sessions offer the opportunity for gaining support, and making your plans for your project become a reality.

<h3>How to get started</h3>

Do not worry about details or a slides right now. Just think of an idea and submit some basic details about your talk. You can edit your abstract after the initial submission. All topics relevant to the KDE Community are welcome. Here are a few ideas to get you started on your proposal:

<ul>
<li>Helping new people and organizations discover KDE</li>
<li>Working towards <a href="https://kde.org/goals/">KDE's goals</a></li>
<li>Granting people more digital freedom and autonomy through KDE technologies</li>
<li>Developing new technologies</li>
<li>Guiding new, intermediate and expert users to encourage participation</li>
<li>Anything else that might interest the audience.</li>
</ul>

These are just some ideas to get the ball rolling. However, you can submit a proposal on any topic as long as you can make it relevant to KDE.

For more ideas for talks, check out the videos from previous years: <a href="https://tube.kockatoo.org/w/p/rNYtZ5yFrMrUEgNfz3AnLQ">2022</a>, <a href="https://tube.kockatoo.org/w/p/smnM1hdvHG9bx5rcXTsrX4">2021</a>, <a href="https://www.youtube.com/watch?v=_172B5er4P4&list=PLsHpGlwPdtMrNmuCWAdTWJ05TYB_rQXYI">2020</a>, <a href="https://www.youtube.com/watch?v=Nxem6yTRC4M&list=PLsHpGlwPdtMrEkjUUzwlIA6fJBsZxDWY3">2019</a>, <a href="https://www.youtube.com/watch?v=_yyOc6-x2yI&list=PLsHpGlwPdtMraXbFHhkFx7-QHpEl9dOsL">2018</a>, <a href="https://www.youtube.com/watch?v=Uzemu8xbsSo&list=PLsHpGlwPdtMojYjH8sHRKSvyskPA4xtk6">2017</a>, <a href="https://www.youtube.com/watch?v=-Ek368vPBz0&list=PLsHpGlwPdtMogitRYzwPz4dWTVsZRGwts">2016</a>, and <a href="https://www.youtube.com/watch?v=BFyl9lPV6rI&list=PLsHpGlwPdtMoYr6716veZi0OJteaU9ud3">2015</a>.

For more details and information, visit our <a href="https://akademy.kde.org/2023/cfp/">Call for Participation page</a>.
<!--break-->