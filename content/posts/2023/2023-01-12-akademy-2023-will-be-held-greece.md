---
title: "Akademy 2023 will be held in Greece"
date:    2023-01-12
authors:
  - "aniqakhokhar"
slug:    akademy-2023-will-be-held-greece
---
<strong>Akademy 2023 will be held at the <a href="https://www.uom.gr">University of Macedonia (UoM)</a> in <a href="https://www.visitgreece.gr/mainland/macedonia/thessaloniki/">Thessaloniki</a>, Greece, from Saturday the 15th to Friday the 21st of July.</strong>

Akademy 2023 will be a hybrid event, combining on-site and remote sessions, and will include talks, workshops, Birds of a Feather (BoF) meetups, training and coding sessions. The conference is expected to draw hundreds of attendees from the global KDE community to discuss and plan the future of the community and its technologies. Many participants from the broad Free and Open Source software community, local organizations and software companies will also attend. The call for papers will open soon, and the registrations shortly after. We will soon update Akademy's website, in the meanwhile follow us on <a href="https://twitter.com/akademy">Twitter</a> and <a href="https://floss.social/@akademy">Mastodon</a> to keep up to date with Akademy’s news.

<h3>About Thessaloniki</h3>

<img class="img-fluid" src="/sites/dot.kde.org/files/anastasius-8DkDA67JAIs-unsplash_cropped_0.jpg" alt="The White Tower, perhaps Thessaloniki's most famous landmark." width="100%" /> 

Thessaloniki was founded in 315 BC and has a population of over 800,000. It is Greece's second major economic, industrial, commercial and transportation hub. It is renowned for its rich history, festivals, and events, and is considered to be Greece's cultural capital.

<h3>About the University of Macedonia (UoM)</h3>

The University of Macedonia is a modern state university that provides education in a wide range of disciplinary fields. UoM’s Department of Applied Informatics makes software, especially free software, the main focus of a large part of the university's studies and research. The university is located only a short walk from the centre of the city, close to cafés, restaurants, and other historical and cultural spots.

<h3>About Akademy</h3>

<img class="img-fluid" src="/sites/dot.kde.org/files/000_Group_photo_1500_cropped.jpg" alt="Attendees to Akademy 2022 in Barcelona." width="100%" />

For most of the year, <a href="https://kde.org">KDE</a>, one of the largest free and open software communities in the world, works online communicating over email, instant messaging, video-conferencing, forums and mailing lists. <a href="https://akademy.kde.org/">Akademy</a> provides all KDE contributors with the opportunity to meet in person to foster social bonds, work on concrete technology issues, discuss new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE community also welcomes companies building on KDE technology to Akademy, as well as those that are looking for opportunities.
<!--break-->
