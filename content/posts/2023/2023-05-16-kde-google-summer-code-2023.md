---
title: "KDE & Google Summer of Code 2023"
date:    2023-05-16
authors:
  - "unknow"
slug:    kde-google-summer-code-2023
---
By Benson Muite

<figure>
<img src="/sites/dot.kde.org/files/promo.png" alt="Google Summer of Code logo with KDE dragons Konqi and Katie" />
</figure>

<b>This year KDE will mentor 9 projects in the Google Summer of Code (GSoC).</b> GSoC is a program in which contributors new to open source spend between 175 and 350 hours working on an open source project.

Meet the projects and contributors for 2023:

<h3 id="projects">Projects</h3>

    <h4 id="kalendar"><a href="https://apps.kde.org/kalendar/">Kalendar</a></h4>

        <figure>
            <img src="/sites/dot.kde.org/files/kalendar.png" alt="Screenshot of Kalendar application showing event creation dialog box" style="width 150px; float: right; padding: 1ex; margin: 1ex;" />
        </figure>

        <ul>
            <li><strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/5r2LyurQ">Improving Mail Integration in Kalendar</a></strong> - <a href="https://invent.kde.org/aakarshmj">Aakarsh MJ</a> will work on the mail integration in Kalendar. This will make it possible to use Kalendar as a full featured email client integrated with the already existing calendar and address book features.</li>
            <li><strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/5NO7f2Yn">Implement calendar availability</a><strong> - <a href="https://invent.kde.org/infiniteverma">Anant Verma</a> will work on calendar availability in Kalendar. This will allow you to specify your working hours where you are available and can be invited to meetings and events.</li>
            <li><strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/va42BXAO">Calendar Invitations</a></strong> - <a href="https://invent.kde.org/vkpotterhead">Vansh K</a> will work on adding support for calendar invitations to Kalendar, allowing you to send invitations to events and to also handle incoming invitations.</li>
        </ul>

    <h4 id="digikam"><a href="https://www.digikam.org/">digiKam</a></h4>

       <div style='text-align: center'>
        <figure style="padding: 1ex; margin: 1ex;">
           <img src="/sites/dot.kde.org/files/digikam.jpg" width="800" alt="Screenshot of digiKam application showing image preview" />
        </figure>
     </div>

        <ul>
            <li><strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/RfX21ZZQ">Add Automatic Tags Assignment Tools and Improve Face Recognition Engine for digiKam</a></strong> - <a href="https://invent.kde.org/quochungtran">TRAN Quoc Hung</a> will develop a deep learning model that will be able to recognize various categories of objects, scenes, and events in digital photos, generate corresponding keywords that can be stored in digiKam’s database and assign them to photos automatically.</li>
            <li><strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/cpdotYqG">Improve Items Properties Management</a></strong> <a href="https://invent.kde.org/kumarutkarsh946">Utkarsh Kumar</a> will use machine-learning to improve the property labeling process for images in digiKam. At present, users are facing a host of difficulties when they try to transfer properties such as color, tags, and labels to numerous pictures. This is resulting in an extremely laborious and monotonous task of copying them repeatedly. This project will introduce a more seamless and efficient approach, which will enable users to execute these actions with a single click and drag of the mouse, thus significantly enhancing the user experience.</li>
        </ul>

    <h4 id="krita"><a href="http://krita.org/">Krita</a></h4>

        <figure>
            <img src="/sites/dot.kde.org/files/krita2.jpg" alt="Screenshot of Krita application showing a menu with bundle creation" style="width 150px; float: right; padding: 1ex; margin: 1ex;"/>
        </figure>

        <strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/SB6pWpuy">Improving the Bundle Creator</a></strong> - <a href="https://invent.kde.org/srirupa">Sriruppa Datta</a> will be working on improving and expanding the bundle creator in Krita. Bundles are packages of resources, like brushes or gradients that Krita users can add or swap out.

        <figure style="float: right; padding: 1ex 1ex 1ex 3ex;"><img src="/sites/dot.kde.org/files/KDE-eco.png" width="200" /></figure>

    <h4 id="kde-eco"><a href="https://eco.kde.org/">KDE Eco</a></h4>
        <strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/IFkJPy65">Measuring Energy Consumption using Remote Lab</a></strong> - <a href="https://invent.kde.org/drquark">Karanjot</a> will improve remote access to the KDE Eco energy measurement lab by:

        <ol type="1">
            <li>automating the energy measurement process, including providing a summary of the results</li>
            <li>setting up backend CI/CD integration</li>
            <li>setting up a frontend upload portal</li>
        </ol>

    <h4 id="tokodon"><a href="https://apps.kde.org/tokodon/">Tokodon</a></h4>
   <div style='text-align: center'>
      <figure style="padding: 1ex; margin: 1ex;">
         <img src="/sites/dot.kde.org/files/tokodon.png" width="800" alt="Screenshot of Tokodon application" />
      </figure>
   </div>

        <strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/sGepI44u">Adding moderation tool in Tokodon project under KDE</a></strong> - <a href="https://invent.kde.org/keys">Rishi Kumar</a> will work on implementing the admin APIs in Tokodon. This will make Tokodon suitable as a Mastodon client for instance moderators and admins.

    <h4 id="okular"><a href="https://okular.kde.org/">Okular</a></h4>
      <div style='text-align: center'>
        <figure style="padding: 1ex; margin: 1ex;">
           <img src="/sites/dot.kde.org/files/okular-main_wo_logos_0.png" width="800" alt="Screenshot of Okular application showing an open pdf file " />
        </figure>
     </div>

        <strong><a href="https://summerofcode.withgoogle.com/programs/2023/projects/O0ndeDYr">Improve Okular For Android</a></strong> - <a href="https://invent.kde.org/shivodayt">Shivodit</a> will work on improving Okular for Android, bringing in the much needed font rendering improvement when fonts are not embedded in the PDF file among other things.  The work will primarily be in the <a href="https://gitlab.freedesktop.org/poppler/poppler">Poppler Freedesktop repository</a>.

<h3 id="next-steps">Next Steps</h3>

        Over the next few weeks, candidates will be learning more about the KDE community, after which they will start the coding phase of their projects. Contributors will report on their progress on <a href="https://planet.kde.org/">KDE's Planet</a>.

        We look forward to welcoming our new contributors and making their experience with KDE pleasant and fruitful.

<!--break-->