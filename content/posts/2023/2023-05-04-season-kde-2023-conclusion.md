---
title: "Season of KDE 2023: Conclusion"
date:    2023-05-04
authors:
  - "Paul Brown"
slug:    season-kde-2023-conclusion
---
By Johnny Jazeix and Joseph P. De Veaugh-Geiss

For this year's edition of Season of KDE, 8 participants successfully completed their projects. Several of the projects push forward the work to achieve <a href="https://community.kde.org/Goals#Current_goals">KDE's three goals</a>, namely:

<ul>
    <li>KDE For All: Boosting Accessibility</li>
    <li>Sustainable Software</li>
    <li>Automate and Systematize Internal Processes</li>
</ul>

Congratulations to everyone involved and excellent work, SoK23 mentees!

<a href="https://community.kde.org/SoK/2023/StatusReport/Mohamed_Ibrahim">Mohamed Ibrahim</a> took on the task of improving the <a href="https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest">KdeEcoTest</a> emulation tool. The idea behind KdeEcoTest is to provide a simple-to-use scripting tool for building Standard Usage Scenario scripts and then running them when measuring the energy consumption of software. Mohamed first focused on improving the documentation to install and run the tool, then made several improvements to add functionalities to the tool.

<p align="center">
   <video width="750" controls="1">
      <source src="https://eco.kde.org/blog/videos/blog_ex1_fast.mp4">
            Your browser does not support the video tag.
   </video>
 </p>
<br/>

<a href="https://community.kde.org/SoK/2023/StatusReport/Nitin_Tejuja">Nitin Tejuja</a> also worked on scripting for energy consumption measurements, but with another approach using the WebDriver for Appium <a href="https://invent.kde.org/sdk/selenium-webdriver-at-spi">selenium-webdriver-at-spi</a>. The advantage of this approach is that the Accessibility framework is also used so contributors will be adding "good" accessibility names -- multiple gains with one addition! Nitin created a script to test the consumption of the KDE educational suite <a href="https://apps.kde.org/gcompris/">GCompris</a>.

<p align="center">
   <video width="750" controls="1">
      <source src="https://invent.kde.org/websites/eco-kde-org/-/raw/master/content/blog/videos/kde-eco-test-gcompris-selenium-script.webm">
            Your browser does not support the video tag.
   </video>
 </p>
<br/>

<a href="https://community.kde.org/SoK/2023/StatusReport/Rudraksh_Karpe">Rudraksh Karpe</a> furthered work on preparing KDE applications for Blue Angel eco-certification. At the moment only <a href="https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/">Okular has this certification</a>, but Rudraksh continued work on the scripts for GCompris and Kate using the KdeEcoTest tool. Rudraksh also developed the <a href="https://invent.kde.org/teams/eco/feep/-/tree/master/tools/SUS-Log-Formatter">"SUS Log Formatter"</a> tool to provide an overview of the actions taken from a Standard Usage Scenario log file.


<p align="center">
   <video width="750" controls="1">
      <source src="https://invent.kde.org/websites/eco-kde-org/-/raw/master/content/blog/videos/KDE-Log-Formatter-Demo.webm">
            Your browser does not support the video tag.
   </video>
 </p>
<br/>

<a href="https://community.kde.org/SoK/2023/StatusReport/Rishi_Kumar">Rishi Kumar</a> worked on improving the accessibility of the Mastodon client <a href="https://apps.kde.org/tokodon/">Tokodon</a> also using the WebDriver for Appium <a href="https://invent.kde.org/sdk/selenium-webdriver-at-spi">`selenium-webdriver-at-spi`</a>. Rishi added multiple tests using the Accessibility framework for various functionalities such as search and offline use and improved the accessibility of Tokodon's GUI.

<figure class="float-md-center w-100 p-2" style="max-width: 80%">
  <img class="img-fluid" src="https://k3yss.github.io/images/post_pics/timelineTest.gif" />
</figure>

<a href="https://community.kde.org/SoK/2023/StatusReport/Theophile_Gilgien">Theophile Gilgien</a> worked on improvements to <a href="https://apps.kde.org/audiotube/">AudioTube</a>. AudioTube is a client for YouTube, and Theophile added multiple features such as removing songs from the history, adding a volume slider in maximized player, making the back-end for search history more efficient, and much more.

<figure class="float-md-center w-100 p-2" style="max-width: 80%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/Audiotubechanges1.png" />
</figure>

<figure class="float-md-center w-100 p-2" style="max-width: 80%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/Audiotubechanges2.jpg" />
</figure>

<a href="https://community.kde.org/SoK/2023/StatusReport/Neelaksh_Singh">Neelaksh Singh</a> setup Flatpak builds in the Continuous Integration workflow for KDE applications. Neelaksh built on the foundation laid in last year's SoK by continuing automatization for the packaging of multiple apps during Nightly builds. <a href="https://invent.kde.org/packaging/flatpak-kde-applications">More info here</a>.

<a href="https://community.kde.org/SoK/2023/StatusReport/Brannon_Aw">Brannon Aw</a> improved the annotation tools in KDE's <a href="[https://apps.kde.org/spectacle/">Spectacle</a>. Brannon simplified the way for the eraser tool and clearing annotations, which was a tedious task before.

<figure class="float-md-center w-100 p-2" style="max-width: 80%">
  <img class="img-fluid" src="https://community.kde.org/images.community/3/3d/SoK_2023_-_Spectacle_eraser_demo.gif" />
</figure>

<figure class="float-md-center w-100 p-2" style="max-width: 80%">
  <img class="img-fluid" src="https://community.kde.org/images.community/8/85/SoK_2023_-_Spectacle_clear_annotations_in_rectangular_mode.gif" />
</figure>

<a href="https://community.kde.org/SoK/2023/StatusReport/Ruoqing_He">Ruoqing He</a> improved holiday support in the digital clock widget in <a href="https://kde.org/plasma-desktop/">Plasma</a>. Ruoqing added a sublabel used to display holiday events for better support.

<figure class="float-md-center w-100 p-2" style="max-width: 80%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/ruoqing.jpg" />
</figure>
<!--break-->