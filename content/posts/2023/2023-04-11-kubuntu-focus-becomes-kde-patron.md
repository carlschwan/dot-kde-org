---
title: "Kubuntu Focus Becomes KDE Patron"
date:    2023-04-11
authors:
  - "Paul Brown"
slug:    kubuntu-focus-becomes-kde-patron
---
<img class="img-fluid" src="/sites/dot.kde.org/files/2021-12-05-kfocus-logo-dark.png" alt="Kubuntu Focus logo on a white background." width="100%" /> 

<strong><a href="https://kfocus.org">Kubuntu Focus</a> is now generously supporting KDE as its newest <a href="https://ev.kde.org/supporting-members/">patron</a>!</strong>

Kubuntu Focus offers the best out-of-the-box experience for professional Linux users. All Kubuntu Focus systems come with the beautiful and intuitive Plasma desktop from KDE on top of industry-standard Ubuntu LTS. The hardware is designed to save time and hassle, thanks to its device optimizations, curated apps, Focus Tools, system-specific HOWTOs, and excellent Linux support.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
  <img class="img-fluid" src="/sites/dot.kde.org/files/2023-02-01_kfocus-family-wide_1500.jpg" />
   <figcaption align="center">Kubuntu Focus family of products: from left to right, the XE GEN 2, M2 GEN 5, and the NX GEN 2.</a></figcaption>
</figure>

“Our team has been active with the KDE community for years by contributing rigorous testing, reporting, and bug fixes,” stated Dana Roth, CEO of Kubuntu Focus. “We believe even deeper collaboration will benefit not only our customers but also the entire community, and we are especially interested in contributing solutions that enable professionals to replace their proprietary desktops with Linux and KDE's software.”

"Having hardware partners is crucial for KDE as they provide the means for our users to experience our products." said Aleix Pol Gonzalez, KDE e.V. President. "Extending our collaboration with providers is a step in the right direction towards solutions that truly help our society by putting products in people's hands and acting on their feedback. It's noteworthy that Kubuntu Focus is based in the United States of America, an area not covered by our current KDE Patrons who are focused on hardware. I'm looking forward to learning how we can improve our products to better serve the region."
<!--break-->