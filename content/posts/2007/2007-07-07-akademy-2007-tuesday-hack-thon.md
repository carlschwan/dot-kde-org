---
title: "aKademy 2007: Tuesday Hack-a-thon"
date:    2007-07-07
authors:
  - "jpoortvliet"
slug:    akademy-2007-tuesday-hack-thon
comments:
  - subject: "Ugly Amarok is dead, Long live cute Amarok!"
    date: 2007-07-07
    body: "It is obvious that more artsy work will go into the UI, but what a beauty already!\n\nIt is more or less clear now which buttons relate to global scope and which to specific widget. This is a very welcoming face. Thank you. I will not be ashamed to show it to people anymore.\n\nI am not really comfortable with the (the useless FOR ME) information view hugging the center stage, but I bet lots of other will love this. Can't wait the new Amarok..."
    author: "suslik"
  - subject: "Re: Ugly Amarok is dead, Long live cute Amarok!"
    date: 2007-07-07
    body: "Same here,\n\nAlso, I wonder how much we'll be able to customize the layout and playlist. I'd love to be able to control what information goes where for the songs in the playlist."
    author: "Soap"
  - subject: "Re: Ugly Amarok is dead, Long live cute Amarok!"
    date: 2007-07-07
    body: "You will be able to customize it considerably more than the current solution allows you to - while the three-pane layout is staying, you <i>can</i> if you really want to hide the context browser (by simply dragging the context browser/playlist splitter all the way to the left) and the same for the sidebar browsers (simply by doing as now, clicking the browser button).\n\nAs for the deciding what info goes where in the playlist - check Ian's blog entry on the new playlist layout :) Don't worry, it will work, he showed some of it off at the lightning-talk session yesterday and it was really quite nifty :) (yes, even if the idea is mine and stuff, Ian is doing the code and making niftiness :) )"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Ugly Amarok is dead, Long live cute Amarok!"
    date: 2007-07-07
    body: "I think people in the beginning weren't welcoming new view because Kontext ;) view looked like afterthought. To compose playlist Collection or Playlists view were opened and that was all. With context always visible its usefullness should become obvious. The only problem *I* see that drag'n'drop from collection/playlists will be awkward - dragging through the whole window?\nPersonally I'd prefer panels on both sides of playlist - like in Digikam. I am only afraid it is too late for that :("
    author: "m."
  - subject: "Re: Ugly Amarok is dead, Long live cute Amarok!"
    date: 2007-07-07
    body: "Fortunately we already have a solution for the drag-and-drop problem: The Popup-Dropper.\n\nAt this point I meant to show you a mockup, but I can't find the relevant blog entry or mailing list post ;)\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: Ugly Amarok is dead, Long live cute Amarok!"
    date: 2007-07-07
    body: "I'm very sceptical about the context view taking up that much space.\nI really can't see any use for it, unless your whole collection is complete with good quality covers - in that case, it may look stunning.\nOtherwise, it will just look ugly and make it harder to build your playlists. Of course, this \"popup dropper\", that Mark Kretschmann is talking about, sound interesting, but I think a collapsible, movable context view would have been even better.\nAnyway, we'll have to wait and see :-)"
    author: "G\u00f6ran Jartin"
  - subject: "Re: Ugly Amarok is dead, Long live cute Amarok!"
    date: 2007-07-07
    body: "it IS collapsible, but I don't think it'll be movable..."
    author: "jospoortvliet"
  - subject: "Re: Ugly Amarok is dead, Long live cute Amarok!"
    date: 2007-07-09
    body: "OK! Collapsible is the most important thing."
    author: "G\u00f6ran Jartin"
  - subject: "haha"
    date: 2007-07-07
    body: "Haha, I love it! the list is full of Anime soundtracks :P did you at least have the Ergo Proxy soundtrack somewhere in there."
    author: "madpuppy"
  - subject: "Re: haha"
    date: 2007-07-07
    body: "The collection scanner is currently a bit picky, so I just limit Amarok 2 to my anime folder. :)"
    author: "Ian Monroe"
  - subject: "Re: haha"
    date: 2007-07-08
    body: "Woot! An anime loving KDE hacker! :thumbs up:\n\n(Sorry for the OT :P)"
    author: "Jucato"
  - subject: "Context browser"
    date: 2007-07-08
    body: "> while the three-pane layout is staying, you can if you really want to hide the context browser (by simply dragging the context browser/playlist splitter all the way to the left)\n\n\nIt would be very good if that will cause a button appeared in the left panel for amarok1.4-like context browser. Or not cause (configurable in settings)."
    author: "CHX"
  - subject: "Amarok's Tabs"
    date: 2007-07-09
    body: "Well, I still do not like Amarok's (or Konqueror's, or ANY application's) vertical tabs, but at least it is better that they are sufficiently wide with pretty clear text on it!\n\nStill no fan of vertical tabs, but, I'll take any improvement I can get :)\n\nThat screenshot is looking good.\n\nAnd I do not know if I speak for myself now, but I like the control buttons at the bottom, since when I click on the taskbar to un-minimize Amarok, it is only a short distance to go there. On the top, it's a much longer movement. Then again, I usually use the shortcut key brilliancy :) And maybe you can re-arrange the panels yourself? Not such a big deal, but I still thought I'd mention this.\n\n"
    author: "Darkelve"
  - subject: "Re: Amarok's Tabs"
    date: 2007-07-14
    body: "Actually, while reading this I came with an idea. How about moving the tabs in konqueror to the side? While browsing the web on current hi-res screens you usually have a lot of horizontal space, but more vertical space would always be useful. For that reason, a vertical tab bar on the side would be a marvelous feature!"
    author: "zonk"
---
<a href="http://akademy2007.kde.org/">aKademy 2007</a> is continuing. Tuesday featured the Education Day and many smaller <a href="http://en.wikipedia.org/wiki/BoF">BoF</a> sessions. In the meantime, hackers worked around-the-clock on various projects, both in the 'quiet' area and in the discussion area. Read on for the aKademy 2007 Tuesday Hack-a-thon report.


<!--break-->
<h3>BoF sessions</h3>

<p>Several BoF sessions were run on Tuesday, for example about <a href="http://akademy2007.kde.org/codingmarathon/talks/semantic_desktop.php">Nepomuk</a>, and the Google Summer of Code.</p>

<a href="http://static.kdenews.org/dannya/akademy_bof.jpg"><img src="http://static.kdenews.org/dannya/akademy_bof_small.jpg" alt="A BoF session in progress" title="A BoF session in progress"></a>

<br />

<h4>Amarok</h4>

<p>The Amarok hackers held more meetings, and this time they went into hacking mode as well. A lot of code has been written, and those who are fortunate enough to have a KDE 4 checkout can see for themselves, as this work has been committed to the repositories. I asked the developers to provide me with some information and a screenshot, and they did: according to Wendy, they spend a lot of time on the new interface. For example, they want to <a href="http://www.monroe.nu/archives/99-Solution-for-organizing-the-new-playlist.html">improve the playlist</a>, where songs expand when you click on them, so you see more information. Nikolaj wrote <a href="http://amarok.kde.org/blog/archives/440-The-Grand-Plan.html">a very interesting blog</a> about their work, which is well worth reading.</p>

<a href="http://static.kdenews.org/dannya/akademy_amarok.jpg"><img src="http://static.kdenews.org/dannya/akademy_amarok_small.jpg" alt="The Amarok team" title="The Amarok team"></a>
<br />
<a href="http://static.kdenews.org/dannya/akademy_amarok-playlist.png"><img src="http://static.kdenews.org/dannya/akademy_amarok-playlist_small.png" alt="A Amarok 2 playlist concept" title="A Amarok 2 playlist concept"></a>

<br />

<h4>Hacking around, talking to each other...</h4>

<p>Finally, various work was done all over the KDE 4 codebase. The collective commit rate went up to 420 commits during the day.</p>

<p>Krita had their first meeting on Sunday, discussing architecture and tackling some serious issues. They postphoned several points to tuesday, and they got together to sketch out the details. I had a short talk with Boudewijn, who told me they have been mostly working on various structural issues. He pointed out that they were very happy with these face-to-face meetings, as several of the topics discussed had already been fruitlessly been talked through on email and IRC, without any result - yet face to face, they were able to come to solutions quickly.</p>

