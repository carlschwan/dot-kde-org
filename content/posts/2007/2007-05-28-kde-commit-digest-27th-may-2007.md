---
title: "KDE Commit-Digest for 27th May 2007"
date:    2007-05-28
authors:
  - "dallen"
slug:    kde-commit-digest-27th-may-2007
comments:
  - subject: "A clock..."
    date: 2007-05-28
    body: "OK, you can call me a Troll, give me minus karma points, say that I'm a idiot, but... just a clock?? What's next, desktop eyes? :)\n\nDon't take me wrong, plasmoids are nice and beatifull, but I was expecting changed in the core interface of KDE. Ff you look at the plasma page it says that plasma is about rethinking the desktop paradigm, and I belive that is waht people are expecting from it."
    author: "Iuri Fiedoruk"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "oh, come on. they're DEVELOPING the damn thing. i don\u00e4t care what you say or what your intentions are, i think you still are acting like a troll."
    author: "Viktor"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "As I said, yes, you can call me troll, I know I'm kind of trolling.\nI just tought, and see that that guilt for that is plasma homepage(!) [that's the troll part, I admit, I took what was written there as true], that the work could be on UI bases for KDE, not plasmoids. Plasmoids can be done throught Karamba also, so I TOUGHT it was low priority for Plasma.\n\nObviously I was wrong :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Think of it like this: Sometimes before you can take a step forward, you must take a step backward."
    author: "Sutoka"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Well, you've totally misinterpreted the nature of plasma then.  Plasma is a replacement for kdesktop and kicker.  It is designed to replace the DESKTOP metaphor, not the whole UI.  KWin is still there.  The applications will behave in much the same manner you'd expect applications to behave.  What will change is the taskbar, the tray, the clock, desktop drawing, etc.  and it's being designed in such a way to make it trivial to extend, and add bling to these components...\n\nOS X is famous because of it's dock.  It doesn't really change the way the applications themselves work from OS 9, but it changes the general feel of the system.  Think of plasma in the same way.\n\nAdditionally, there are new widget drawing styles, and window decorations in the works to go with the Oxygen icon artwork that will give KDE 4 applications a fresh visual appeal - this however is in no way part of the plasma interface.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Like it said in the video this was the first plasmoid made.  Since Plasma is still under heavy development, and KDE 4.0's release is looming closer and closer, Plasma's priorities are to provide equivalent functionality that Kicker/KDesktop provided for the KDE 3 series (AFAIK).  Remember that KDE 4.0s release doesn't mean that KDE 4 is done, just that it has only begun."
    author: "Sutoka"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Yep, I agree, absolutely!\nActually I'm saying since long ago that Plasma (as defined on it's page) will not be part of KDE 4.0. \n\nI just surprised that work started on plasmoids and not on KDE UI.\nAaron Seigo even showed some mockups very nice of icons with text and other things, and I belived that would be first in Plasma.\n\nBUT, maybe the clock is just a first step or a test, and not a indicative of things to come (x-eyes, climate applet, cpu temperature..). In this case I will be happy if you guys call me a clown, a troll or whatever :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "> BUT, maybe the clock is just a first step or a test, and not a indicative of things to come (x-eyes, climate applet, cpu\n> temperature..). In this case I will be happy if you guys call me a clown, a troll or whatever :)\n\nThat's EXACTLY what all the other replies AND the article in the digest said. The Clock is the first Plasmoid and there will be more to come.\n\n> Aaron Seigo even showed some mockups very nice of icons with text and other things\n\nIf you're talking about the mouse-over icon selection menu thing, then that was discussed for Dolphin, not Plasma desktop.\n\nIt makes no sense that you're complaining about us not working on \"KDE UI\" and then you tell us to not work on the desktop/panel etc."
    author: "Matt Williams"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Plasmoids are not just fancy widgets. IIRC kde4's kmenu and panel replacements are also plasmoids. ie why plasmoids are important.\nCheck this out,\nhttp://techbase.kde.org/Projects/Plasma/Menu"
    author: "kryptik"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Well, troll if you must, but KDE is still months away from releasing.  For the longest time, after kdesktop was killed, we didn't even have a background rendering.\n\nOpen source is not done behind the scenes, so since it is public, you get to see some of the early development screenshots.  Sometimes this is a mixed blessing as you users expect something that is fully polished when the first screenshot is released.  The alternative is to get no news at all, and in fact, with trolling, and user comments such as this one, the developers and writers (such as myself) become less inclined to show off early work.  It's kind of sad actually.\n\nSo please, when you look at the screenshots and the videos, please look at it as a proof of concept. This is the very first visible evidence of work being done on plasma, which needs to be treated as such. There is real code powering those objects, and the groundwork has been laid for pretty much every other kind of applet you can think of, including the existing superkaramba applets. While a clock is not that exciting, it shows that much of the groundwork now exists and functions.\n\nAdditionally, kicker will shortly be put to rest, and will be reimplemented as a plasma applet.  This means that while you are used to kicker being a separate application, it will now be as easily configurable, and extendable as any desktop applet ever was.  The problem is, that at the moment, people are working on the backends - the stuff you don't see.  For example, the panel replacement would need a number of data sources programmed - it needs stuff to feed to the taskbar, the tray; it'll need a new menu system and the menu editor will probably need to be reworked.\n\nSometimes you make our jobs totally not worth it."
    author: "Troy Unrau"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Please don't become disheartened and withhold news / screenshots etc!  For every user who doesn't get it there are lots, like me, who are waiting with baited breath for the next week's commit-digest.  Sad, I know, but the stuff that's going into kde4 has got me really excited.  I reckon even for many of the ungraceful comments, it's the case that the people that make them are really psyched about the work you guys are doing."
    author: "Adrian Baugh"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "OK, I'll call you a troll, but this comment isn't meant to bash you; please don't take anything personally.\n\nFirst we complain that Plasma hasn't made any progress. When Plasma finally is mentioned in the digests, we complain that it's \"just\" a clock - where's the revolution?\n\nBasically I think people (including me) has very high expectations of KDE4 and Plasma, seeing \"just\" a clock is disappointing. But please think of it this way: it's still under heavy development, the most work isn't even visible to the eye. The devs are giving you a chance to follow the development and give constructive criticism - if you know you sound like a troll in your post, can't you at least try to sound nicer.\n\nAt least I appreciate when people are showing screenshots and videos. Even if I don't feel \"WOW\" I can see what's going on and point out what I like/dislike and give suggestions. And believe it or not, but the devs are listening. Love you guys. :)\n\nOT: This time it was actually a \"wow\". Too bad I get errors when I run cmakekde when compiling kdelibs from SVN (Kubuntu, following the guide on techbase). I want to try to drag and resize the clock myself."
    author: "Lans"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Sure, no offense :)\n\nI really think Plasma is doing OK, and that just a clock is OK if you plan in the long road.\nMy main complain with Plasma is actually their home-page.. man, I wish they never had publiced that! There are a lot of promisses there that can't be filled in the short term, and it generated too much antecipation for something that, let's take real, probally will be really functional around KDE 4.3.\nTaking that aside, I don't even bother keeping the award winning and cute KDE3 interface (kicker, kdesktop, konqueror, etc), I like it a lot and my computer isn't very fast for fancy graphics (just a old Duron 1.6)."
    author: "Iuri Fiedoruk"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "> There are a lot of promisses there that can't be filled in the short term\n\nyes, i suppose the best thing to do is not have any goals. the next best thing to not having any goals is not to tell others about them so that nobody can help you work towards them.\n\nyou, sir, are brilliant.\n\n> probally will be really functional around KDE 4.3\n\nand you base that prediction on what?\n\n> I like it a lot and my computer isn't very fast for fancy graphics\n\nit'll work just fine on your system."
    author: "Aaron J. Seigo"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: ">> There are a lot of promisses there that can't be filled in the short term\n\n>yes, i suppose the best thing to do is not have any goals. the next best thing to not having any goals is not to tell others about them so that nobody can help you work towards them.\n\n>you, sir, are brilliant.\n\nAlthough I generally agree with you, you totally missed the point here. Having goals is good. Publishing goals is good. But it really depends on how you publish them. I won't argue with you about when all those things will be ready, noone could tell better than you. But from what I've heard it is pretty obvious they won't all be ready for KDE 4.0 and that is what should have been stated clearly on the Plasma page since the very first day of its existence. Telling afterwards (yes, I do follow your blog) that KDE 4 is not KDE 4.0 seems like a lame excuse. I'm not saying it is, actually I'm pretty sure that this was clear to you from the very beginning. But for most people it was not and it's only natural they are a little disappointed now - despite all the cool things there already are, they just expected more. When Apple announces features for OS X 10.5, you expect them for 10.5.0, not for 10.5.x .\n\nAll in all a communication problem and you as a developer should really not have to worry about this. Although I can understand this is driving you nuts sometimes, just try to ignore it. You know what caused it and you can avoid it the next time. There's no point in wasting time answering such posts.\n\nBtw thank you for all the great work you did on KDE and your interesting blog posts!"
    author: "wassi"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "Oh, and to add some really useful feedback: Please make it possible to delete Plasma widgets from the screen just by dragging them into the waste bin - would be really intuitive (not sure if OS X has it)"
    author: "wassi"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "\"Please make it possible to delete Plasma widgets from the screen just by dragging them into the waste bin - would be really intuitive\"\n+1 :)"
    author: "Dolphin-fanatic :)"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "and +1 again !"
    author: "Cissou"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "easy, keep them working. a clock is actualy GREAT, because it means that the basics work. it's like a hello world that works, it means that you can do other great stuff.\n\njust wait and see and dont complain"
    author: "Beat Wolf"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "Where exactly have I been complaining ?\n\nAnd I know what this clock really means (framework-wise) since I know programming a little too. But it really does not matter what you show off, as long as there's not everything included that was promised on the Plasma page, people will always complain - so there's absolutely no point in listening to them. That was the essence of my post..."
    author: "wassi"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "kde4 isn't out yet, so no one can tell if plasma will meet the expectations that plasma.kde.org is posing. \nAnd looking at that website, i am confident that plasma will meet most of them.\nJust don't expect to see hundreds of plasmoids on the release day of kde 4.0"
    author: "whatever noticed"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "Try the kdesvn-build script instead.  I used it last night for the first time on my ubuntu fiesty box and it worked without any problems."
    author: "Chuck Shaw"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "There's something like that? Damn, I think you just ruined my sleep tonight. ;)\nI'll try that, thanks!"
    author: "Lans"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "> but... just a clock??\n\nthe idea of plasma is to provide new ways to build things for the desktop and new ways to arrange your desktop. that is the core idea.\n\nto create that we need a few example widgets and a few dataengines. then we exercise the framework with those and get the underlying structure in place so as to support all kinds of user visible goodness.\n\ni know that from a user's perspective a clock isn't exciting. i'm not trying to excite you with it, so there you go, we're all even. what i'm trying to do, along with the rest of the plasma team, is make something that can produce things that will excite you. please understand that plasma is not a marketing exercise, but a technology project which means that technical goals and processes are what we aim for. exciting you will happen at appropriate points in development, but i'm not going to try and excite you with useless hacks and half-designed rubbish.\n\nand yes, we have lots of plans as to what we will produce, but we have to get there first.\n\n> but I was expecting changed in the core interface of KDE. \n\nif you could understand the technical underpinnings of what we're working on and had the ability to see big picture items, maybe you'd begin to see how that's where we're going. i'm not renigging on what i've said in the past, though i'm getting the idea that people have a really hard time understanding what \"getting there\" looks like.\n\ninterestingly, people find the path there most uninteresting. which is ok, most good ideas are, in retrospect, not only obvious but built up from small logical steps that seem in the process to be straightforward but that end up somewhere completely unexpected. so, like a good joke, wait for the punchline. don't interupt half way through and say \"it's not funny yet.\"\n\n> Ff you look at the plasma page it says that plasma is about rethinking the\n> desktop paradigm, and I belive that is waht people are expecting from it.\n\ntell you what, leave the thinking to me and just sit back and enjoy the ride. it'll be a lot less frustrating for both of us. people are trying to figure out what we're doing and evidently failing spectacularly at it. that's ok, but let's roll with it and instead of getting everyone flumoxed, let's just agree to let the thinking happen on this side of the fence. in return, you'll get some nice free software. deal?\n\ni'm sorry if that sounds harsh, but i'm tired of dealing with this.\n\nyou know, i remember in elementary and high school teachers would always pair me up for group projects with people who couldn't keep up. so i'd end up doing all the work, so as not to endanger my grade, and just tell my partner(s) to look busy but stay out of the way. i guess some things never change."
    author: "Aaron J. Seigo"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "\"i know that from a user's perspective a clock isn't exciting. \"\nwell, annma posted a kde4 screenshot on the planet recently, and it damn well seemed exciting to me. ;)\n...later that day I discovered that I can't access kde svn from behind this evil proxy. maybe after class today I'll have to hack around that. I wanna see kde4 again!"
    author: "Chani"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "> so, like a good joke, wait for the punchline. don't interupt half way\n> through and say \"it's not funny yet.\"\n\nhehe thats a nice one :-)"
    author: "b"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "Agreed. For anyone who is not a programmer, it's impossible to appreciate the degree of triumph a compiler-writer feels in getting \"hello, world\" to compile and execute. If that were something I had just done, the next thing I'd do is call in my next-door officemate and show it off. \"Months of work for that???\" would be the standard reaction from anyone who doesn't know programming.\n\nEven for people who do program, it's pretty hard to appreciate just what's going on without familiarity with the technical details. KDE4 is clearly going to make it far easier to produce beautiful, functional applets & applications, but the closer you are to the details the easier it is to see the progress that's being made and how it's going to impact things.\n\nSo, for every 100 users who make unhelpful, naive comments (and remember they're watching in part because they are excited about what's going on), I hope you do attract the attention of one person who actually helps with some of the heavy lifting. If that happens, then I hope you'll view the publicity as a success."
    author: "Ted"
  - subject: "Re: A clock..."
    date: 2007-05-29
    body: "I actually was looking for things less visual.\n\nIn my (humble) opinion, after reading the plasma page, I tought it was going to be like a UI plan, to be followed by applications, think of it was a set of guidelines and tools for developers to use on their GUI.\nIn my mind, things like a integrated find (a le firefox) would be part of Plasma, but maybe I just had misread the Plasma homepage text.\n\nThat said, yes, a clock looks nice, but I kind was expecting \"Development of KDE4 has just begun, and it is during these major release cycles that we have the opportunity to retool and rethink our applications and environment at the fundamental level.\" :)"
    author: "Iuri Fiedoruk"
  - subject: "yep you did misread"
    date: 2007-05-29
    body: "Yea you did misread. Plasma is the kicker/kdesktop replacement. It is going to do more then the current kicker/kdesktop (scriptable, desktop widgets), but its not something that will be used from within other apps."
    author: "Ian Monroe"
  - subject: "Re: yep you did misread"
    date: 2007-05-30
    body: "well it still will have DBus interface, etc., so it still can be used from within other apps, but for notifications, etc."
    author: "Jordan K"
  - subject: "Re: A clock..."
    date: 2007-05-28
    body: "My desktop paradigm needs a clock :)"
    author: "Cyrille Berger"
  - subject: "No! Basket mustn't die!"
    date: 2007-05-28
    body: "Neither am I a developer nor do I know how to program. I just use KDE and it's applications and find them to be so polished that I recently convinced several people to make the switch from Windows to KDE. One of the programs we really like is Basket. I was so glad to see this great piece of work S\u00e9bastien Lao\u00fbt created and just hope that some developer steps up and fills the gap. Please read the Commit-Digest to find out about all the great features that were planned for Basket 2.0! Hopefully someone takes heart and continues the development of this great application."
    author: "kde-user"
  - subject: "Re: No! Basket mustn't die!"
    date: 2007-05-28
    body: "I agree - I find this program very useful, and use it regularly when planning my Road to KDE 4 article topics.  I can only dream of what could be done with this program with a qgraphicsview...\n\nIf only I was better at coding, I'd take up this program..."
    author: "Troy Unrau"
  - subject: "Re: No! Basket mustn't die!"
    date: 2007-05-28
    body: "Troy, \n\nWhile I find Basket very useful, your work on \"The road to KDE4\" is at least as valuable to the community, and I for one would hate to see you stop publishing those.\n\nOn the other hand, the best way (in my humble opinion) to become stronger at coding is to jump in at the deep end and learn as you go! :-)\n\n- Nikolaj"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: No! Basket mustn't die!"
    date: 2007-05-28
    body: "Thanks, I appreciate the kind words :)\n\nI have taught myself fairly well to code with a number of technologies, from databases to complex numerical analysis, however I have never been good at the UI stuff.  I'll leave that to the people that can work those things out, and I'll stick to my role within KDE :)"
    author: "Troy Unrau"
  - subject: "Re: No! Basket mustn't die!"
    date: 2007-05-28
    body: "Rather than QGraphicsView, I would use the Flake library of KOffice. I did discuss it with basket's author, and it would really fit, as throught the shapes, it would give access to texts, images, vectors, etc... with editing possibilities for each."
    author: "Cyrille Berger"
  - subject: "What bothers me at Basket..."
    date: 2007-05-28
    body: "Why must Basket coded in C++? With Kross and the kde-bindings to good scripting-languages (like PyKDE) it would be no problem to create a useful and more dynamic application. Also, it would be possible to outsource the development to the community, by using an extendable framework, like firefox did it. And in the end, it would be less work.  So, why C++?\n"
    author: "Chaoswind"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "The problem with you're argument is that you're suggesting that firefox is written in a scripting language -- it most definitely is not.  It is written in a real language, compiled to machine code, and the extensions are scripted.  This is pretty much the way everything in KDE works as well.  See Amarok for a good example of using scripting languages for extensions within KDE.\n\nBasket is coded in C++ because it's the core application."
    author: "Troy Unrau"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "> The problem with you're argument is that you're suggesting \n> that firefox is written in a scripting language \n\nWell, no. Im just say, that a application can gain advantage from outsourcing code to plugins."
    author: "Chaoswind"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "> The problem with you're argument is that you're suggesting that\n> firefox is written in a scripting language\n\nThe platform that power Firefox (XULRunner, Gecko, GTK+/Win32/...) are written in C and C++, but Firefox, the interface, which also includes a lot of logic, are written in XUL and JavaScript. Its perfectly reasonable for a program like Basket to be written in a high-level language. (I am not necessarly advocating that, just saying; I prefer C++ myself and I bet that's the case of Basket's author.)"
    author: "fast penguin"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-29
    body: "Afaik the UI in KDE is also, like firefox', mostly xml-like stuff. I'm not a developer, so I'm not sure about this, but I don't think it's that different...\n\nAnyway, the idea from Cyrille sounds very interesting - Basket and flakes..."
    author: "superstoned"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-29
    body: "no, there is a big difference. qt ui-files are xml, but they are mostly used to generate c++ code (though qt can also load them at runtime, that's not needed for most applications).\n\nalso, using only xml or a scripting language (like javascript with xul/firefox) is still not the same. xml (when used for what is intended ;)) will only increase the startup time - with a slow scripting language (or rather a slow interpreter) your whole gui may become unresponsive. "
    author: "ac"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "So that people can use it without bogging their computers down... I need my desktop to be FAST, and I do NOT allow any program written in Java, much less any scripting language like Python or Ruby. They're fine for simple scripts, and as scripting languages to extend applications, but if someone makes a full useful app in Python, I will start looking for a C/C++ coded alternative. No apps written in scripting languages on my (ancient) PC, thank you. BTW, that's exactly what I hate about firefox. It's big, slow and bloated. I wish someone coded some efficient, open-source, multiplatform alternative browser (yeah, I know I'm getting greedy :)), so I could get rid of that piece of bloat from my desktop."
    author: "zonk"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "> They're fine for simple scripts, and as scripting languages to extend \n> applications, but if someone makes a full useful app in Python, I will start \n> looking for a C/C++ coded alternative.\n\nThats your personal problem. Python is more than fast enough for desktop-applications like basket. I use dozens of self-written PyKDE-Apps, without any performance-problems. Ok, of course they are slower than Native-Qt, but that doesn't count in real live.\n\n> BTW, that's exactly what I hate about firefox. It's big, slow and bloated.\n\nFirefox is'nt really slow, it has only a horribly thread-management, and a stupid  Default-Configuration. But, yes, there are some bottlenecks, because of to much used Layers in the design."
    author: "Chaoswind"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "Well, if you're lucky, Konqueror for KDE 4 could be packaged up in such a fashion... it runs native on OS X and Windows now... the downside is that there are a number of libs that would have to be shipped with it, such as Qt, kdelibs, etc. which could drive up the download size.  On the flipside, this libs are shared between a number of other newly portable KDE applications, such as Amarok - so that downside could be easily spun to be an upside...\n\nOf course, we'd prefer to pull people onto the more free platforms and away from OS X and Windows... then they can have KDE in all of it's beauty."
    author: "Troy Unrau"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "I'm always intrigued by this apparent trade-off that computer languages must either be \"expressive and rapid to develop, but slow on execution\" (like Python or Ruby), or \"slow and hard to develop, but fast on execution\" (like C/C++). Come on, you can have *both*!\n\nWhy don't you guys try some of the functional languages out there, like Ocaml, Clean, or Haskell?  You get the best of both worlds: highly expressive languages which make development a breeze, and at the same time their runtime performance is comparable to C/C++ (check the computer language shootout for numbers).\n\nI know there was a project to add Qt/KDE bindings to Ocaml, but it seems dead.  It's a pity, because I am sure that KDE could benefit greatly in supporting the functional paradigm...\n"
    author: "dario"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "I know it works with KDE after a fashion - it's used in parts of Kalzium for KDE 4..., Ocaml, I mean."
    author: "Troy Unrau"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "I agree with you!\n\nPeople say that Python or such languages are \"fast enough\". This is true to a certain point. When Basket implements cross-baskets searches or other advanced methods, you need resources! A good developer should not think about the application he is going to have in three months, but about the application he is aiming at!\n\nSomebody says you can implement the slowest parts in C or C++ and plug the code into Python. Who is going to do that? You had better be motivated, especially if your objects are complex!\n\nPython is very good and I use it everyday. But I can get much further in C++, even if it is harder at the beginning."
    author: "Hobbes"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "There is a wrong assomption that a an application written in python/ruby will b e entirely written in python/ruby, the database backend (hence the search engine) or a mp3 decoder or an image processing algorithm should be _written_ in a language which is translated to assembly (that can be python with gcc ;) even if I didn't try), but then the UI just need to do the glue. A lot of things are easier for the programmer, memory management, comprehensive errors, etc... And in the end, it doesn't result in a slow program, but it definitively results in something that was easier to debug."
    author: "Cyrille Berger"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-28
    body: "I wrote that a whole application *should not* be written entirely in Python. I *never* assumed \"an application written in python/ruby will be entirely written in python/ruby\".\n\nIn practice, there is a big risk (and very often an outcome): *a few* slow parts remain in Python although they should be implemented in assembly, C, C++ or whatever fast. I do not know how to state that more clearly."
    author: "Hobbes"
  - subject: "Re: What bothers me at Basket..."
    date: 2007-05-30
    body: "Amen brother!!!"
    author: "anon"
  - subject: "Re: No! Basket mustn't die!"
    date: 2007-05-28
    body: "I just hope someone will take up this project ...BasKet really is one of the coolest and most useful things around.\n\nIt's a pity though that it's only mentioned at the bottom of Commit-Digest."
    author: "Matija \"hook\" &#352;uklje"
  - subject: "It's alive !"
    date: 2007-05-28
    body: "I'm really happy that plasma is finally coming into the commit-digest issues and more than that with a really nice video to blend into users expectations. It's a really early stage but I'm glad things are moving and as I can see all in a good direction.\nThanks KDE."
    author: "Rares Sfirlogea"
  - subject: "Re: It's alive !"
    date: 2007-05-28
    body: "Yes, it's always good to see such progress. It's also really nice to have people giving us encouragement like this rather than just complaining.\nSo, thank you Rares :)"
    author: "Matt Williams"
  - subject: "Re: It's alive !"
    date: 2007-05-28
    body: "It's quite logical most people will only see the clock not realising that this means the whole system had to be written in order to make the clock show and work.\n\nI appreciate very much what Plasma is trying to achieve ...I just hope it doesn't end up looking like a spawn of Vista+MacOSX - KDE (and Linux) has its own way logic behind the interface (virtual desktops, middle-click paste, shading windows, etc. etc.) and this should not be forgotten. I sure hope Plasma will in due time implement what Slicker (http://slicker.org) tried to do.\n\nDon't get me wrong - I like what I see so far and I think you guys are doing great. I'm merely expressing my hopes now instead of being an ass about it when Plasma \"1.0\" comes out.\n\nKeep up the good work ...and keep it good ;)"
    author: "Matija \"hook\" Suklje"
  - subject: "Re: It's alive !"
    date: 2007-05-28
    body: "> I just hope it doesn't end up looking like a spawn of Vista+MacOSX \n\nseeing as i think vista's desktop ui is a complete joke and MacOS X is a severe under-performer, i'd be disappointed with this as well. obviously we'll be learning from the things they do well (e.g. i think expose ideas are very interesting) as well as what they do wrong, but also providing a lot of both traditional kde stuff (you mentioned a few) as well as some new things."
    author: "Aaron J. Seigo"
  - subject: "Re: It's alive !"
    date: 2007-05-29
    body: "That's what I wanted to hear ;)\n\nYou just made my day (sic!).\n\nWe're right behind you &#8212; just don't take the bitching kind as the majority of KDE users ...there majority is probably the wise bunch who just sits back and doesn't say anything before they see the end product."
    author: "Matija \"hook\" Suklje"
  - subject: "Re: It's alive !"
    date: 2007-05-28
    body: "Why do I have to think of Robot Chicken when I read your post title... ;)"
    author: "Evil Eddie"
  - subject: "Re: It's alive !"
    date: 2007-05-28
    body: "thank you for the positivity. much appreciated. and there's so much more to come from our side =)"
    author: "Aaron J. Seigo"
  - subject: "Data sources"
    date: 2007-05-28
    body: "Plasma developers: you are doing a great job, so ignore the trolls! (Don't let them get to you, that's how they get their kicks).  After watching the clock plasmoid video, all I can say is \"wow!\"...\n\nBtw, if I understood some of the basic ideas behind Plasma, am I correct to assume that if you open 3 different clocks, they would all be feeding out of the same data source, one which is able to provide a \"current time\" feed?\n\nAnother question: are there plans to add support in Kross to a strong, static type functional language such as Ocaml or Haskell?  Javascript does have a number of neat functional features (such as first-order functions and closures), but I am not very fond of its weak-type system.\n"
    author: "dario"
  - subject: "Re: Data sources"
    date: 2007-05-28
    body: "Obviously, what I meant by \"first-order functions\" is actually \"higher-order functions\" and functions as \"first-class\" citizens in the language.  That's one silly portmanteau...\n"
    author: "dario"
  - subject: "Re: Data sources"
    date: 2007-05-28
    body: "While I'm sure this can be achieved with Kross, I'm under the impression that they are trying to keep the plasmoids agnostic of the system.  Which means that languages such as Python, Ruby and Javascript can be run using only an interpreter, C++ and so forth cannot.  Which means that the plasmoids that will be made available to automatic download through the NewStuff interface will only be the ones that are system agnostic.\n\nAs far as I understand it, Ocaml and Haskell do not fit this criteria, but I would happily be corrected."
    author: "Troy Unrau"
  - subject: "Re: Data sources"
    date: 2007-05-28
    body: "Ocaml can be run under an interpreter (\"ocaml\"), actually.\n\nI would be strongly interested in a projet to bring a good KDE and Qt support into OCaml, too. If some of you has more concrete ideas, i'd even try to help, but my lack of C++ knowledge is a real pain when it comes to KDE integration.\n\nThe different way i see are :\n- scripting support with Kross\n- kde binding using SWIG\n- \"silent\" integration in some applications (ocaml code in the backend, like in kalzium or semantik)\n\nIf you're a Haskell fan, you could perhaps have a look at the Haskell-on-JVM thing : QtJambi may be the easier bridge here."
    author: "bluestorm"
  - subject: "Re: Data sources"
    date: 2007-05-28
    body: "re different way, maybe add to the list;\n- kde binding using Smoke\n\nSmoke provides everything needed already and it should be imho easier to get bindings done using Smoke rather then SWIG. But in any case (so, whatever you like to use here, Smoke or Kross or ...) I would suggest to ask/talk about it before at the kde-bindings mailinglist.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "In the meantime I came across the Qyoto/Kimono project, which aims to bring Qt/KDE bindings to C# and other .NET languages: http://cougarpc.net/qyoto/\n\nWith this in mind, there might be already a nearly-working-though-quite-convoluted way of integrating Ocaml with KDE:\n\n- The language F# (which Microsoft pretty much copied straight from Ocaml) is part of .NET.\n- The Qyoto/Kimono guys bring the Qt/KDE bindings for .NET.\n- The Mono guys bring .NET for Linux.\n\nHmm... could this work?  (Though using Smoke is still a better long-term solution).\n"
    author: "dario"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "\"Though using Smoke is still a better long-term solution\"\n\nWell in fact Qyoto uses Smoke, and the code is based on the QtRuby code which in turn was based on the PerlQt code. We are quite close to a first release of Qyoto, and I've started working on the code generation for the Kimono KDE classes and that is looking good. We (Arno Rehn and myself) hope to have Kimono working by the time we give a talk at this years akademy.\n\nDoing a complete binding for Qt/KDE is a heroic amount of work even using Smoke, and so I think that using F# is the best way to do KDE programming in a functional style. Another alternative is to try Scala with the QtJambi bindings and the JVM (and Nice?)."
    author: "Richard Dale"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "You are correct: Ocaml can indeed be run under an interpreter of sorts.  It is in fact one of the more flexible languages around, since you can do the following with the same programme:\n\na) Compile to native code, nearly as fast as C/C++.\nb) Compile to the bytecode of a VM, so the same binary can run in a very large number of platforms at fairly good speed, though not as fast as a).\nc) Put the shebang #! at the beginning of the programme, and execute it as a script.  Not as fast as a) or b), but excellent for scripting.\n\nAnyway, me too, I am interested in bringing Ocaml goodness to KDE.  Does anyone know if there are already any projects out there aiming for this?\n"
    author: "dario"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "In Kalzium we use OCaml (with libfacile) to solve a couple of eqautions. We do that because OCaml is _not_ needed on the users computers but only on comptiletime. So for the majority of us that means: My distributor needs to install it when he creates my deb/rpm-file."
    author: "Carsten Niehaus"
  - subject: "Re: Data sources"
    date: 2007-05-28
    body: "Work on support for the Java language has started (see http://websvn.kde.org/trunk/playground/bindings/krossjava ). Thanks goes to google for sponsoring the development within there summer of code project and to all those KDE-developers out there for there fantastic work on KDE4."
    author: "Sebastian Sauer"
  - subject: "Re: Data sources"
    date: 2007-05-28
    body: "> they would all be feeding out of the same data source\n\nfrom the same DataEngine, which would be publishing one DataSource per timezone requested. so if 2 of the clocks were showing GMT and one was showing, say, CEST then the DataEngine would be publishing two DataSources, one for GMT and one for CEST, and two of the clocks would be getting their data from the former and the other from the latter. only one DataEngine would need to be in operation for this.\n\nthis is, in fact, how it already works =)\n\n> Javascript does have a number of neat functional features \n\nmy goal is not to provide for language fetishism (don't worry, i suffer from it too ;), but to lower the barrier to entry. ocaml doesn't exactly help there =) that said, plasmoids are going to tend to be fairly simple things. individually, they aren't going to be full fledged applications. so languages like js are a fine fit."
    author: "Aaron J. Seigo"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "> so languages like js are a fine fit\n\nDon't get me wrong with the Ocaml vs. Javascript thing:  I agree 100% with you that Javascript is indeed a very nice match for plasmoids.  In fact, I have come to enjoy the language, after having an initial bad impression with it (that most programmers seem to share).\n\nFor me, there's only one thing sorely missing from Javascript: a strong type system.  I have yet to find a good example that shows dynamic typing in a positive light.  By this I mean an example where dynamic typing outweights its enormous disadvantages, and where the same thing could not be achieved in a type-safe manner via higher-order functions and/or parametric polymorphism \u00e0 la Ocaml.\n\nThankfully, it seems (optional) type annotations will make their way into Javascript 2.0.  Not nearly as sophisticated as types in functional languages, but a step in the right direction...\n\n(And yes, I also agree that Ocaml is hardly the best choice for beginners.  Though it's immensely rewarding for those willing to learn it!).\n"
    author: "dario"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "> I have yet to find a good example that shows dynamic typing \n> in a positive light.\n\nit's awesome for quick stuff, but surely sucks as you start to do almost anything resembling serious work. i haven't done much javascript, but with other dynamic typed languages i've had the pleasure of working with i've run into some real wtf's =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "> and where the same thing could not be achieved in a type-safe manner via higher-order functions\n\nCorrect me if I'm wrong, but that seems like a stupid definition. You can code absolutely everything in Assembler too, so why Ocaml? Although I have to confess I do not know this language, I guess the reason is just: Elegance leading to easier maintaining. Which is quite a good reason IMHO.\n\nIt might still be true you are right about weak typing, but your definiton for proving the contrary just sucks..."
    author: "wassi"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "I don't think he's making a Turing tar pit argument. The point is that languages like OCaml frequently match the elegance that people typically claim for dynamically typed languages (like Javascript and Python), while retaining a static type system that's actually stronger than, say, C++.\n\nSo, the argument is: Why forgo the static safety checks by using Javascript (which, admittedly, can frequently be more concise than C++) when you can have both type safety *and* elegance with OCaml?\n\nHave your cake and eat it, too."
    author: "dolio"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "> Correct me if I'm wrong, but that seems like a stupid definition.\n\nYou are wrong.  Correction follows:\n\nDon't confuse the universality of a Turing machine with the suitability of different languages for different tasks.  Obviously you can implement any algorithm in whatever language you prefer, but that does not mean all languages are equally suitable for all tasks!\n\nReal carefully my original statement: \"I have yet to find a good example that shows dynamic typing in a positive light. By this I mean an example where dynamic typing outweights its enormous disadvantages, and where the same thing could not be achieved in a type-safe manner via higher-order functions and/or parametric polymorphism \u00e0 la Ocaml\".\n\nGot it now?\n\n"
    author: "dario"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: ">> For me, there's only one thing sorely missing from Javascript: a strong type system. I have yet to find a good example that shows dynamic typing in a positive light. By this I mean an example where dynamic typing outweights its enormous disadvantages, and where the same thing could not be achieved in a type-safe manner via higher-order functions and/or parametric polymorphism \u00e0 la Ocaml.<<\n\nI agree with you that the problem with Javascript is that it is weakly-typed. However, there are other dynamically-typed languages which don't suffer from that.  Python is strongly-typed and type-safe and dynamically-typed.  It's functions are first-class citizens, it's easy to write/use higher-order functions, and the type system is designed for parametric polymorphism through the use of duck-typing, instead of being designed to use a nominative type system like many other languages.\n\nSee: http://en.wikipedia.org/wiki/Template:Type_system_cross_reference_list\n"
    author: "dani3l"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "> However, there are other dynamically-typed languages which don't suffer\n> from that. Python is strongly-typed and type-safe and dynamically-typed.\n\ndef foo(bar):\n....if bar:\n........return (1 + 2)\n....else:\n........return (1 + \"screwed up\")\n\nPython accept this, and of course won't say anything unless you try foo(False).\nWhat does \"type-safe\" mean to you ?"
    author: "bluestorm"
  - subject: "Re: Data sources"
    date: 2008-01-21
    body: "He's talking about strong vs weak typing and you're talking about dynamic vs static typing. I for one am tired of this confusion, go look it up."
    author: "anonymous"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: ">my goal is not to provide for language fetishism (don't worry, i suffer from it too ;), but to lower the barrier to entry.\n\nI hope that means that I will be able to write a cool plasmoid in Ruby when KDE4 is out, that's what I'm learning it for since some time =) It's quite nice to learn, even for someone who doesn't know much about programming otherwise - I hope that works out.\n\nWhen one is able to write/enhance Plasmoids and/or KDE programs with one of these so-called scripting languages, I'm sure that will bring some more people in who aren't coders but who are motivated enough to learn one of the easier languages like Ruby or Python because that gives them a very low entry barrier to participate."
    author: "Anonymous"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "there's a ruby kross .. uhm.. whatever they call it (backend?) so yeah, that should be possible, along with python"
    author: "Aaron J. Seigo"
  - subject: "Re: Data sources"
    date: 2007-05-29
    body: "What about using Interview in qt4 instead of building a new MVC framework?\n\nI know that the amount of data used by each plasmoid may not justify using interview but it could be worthwhile to investigate it.\n\nJust my $0.02\nBen"
    author: "Ben"
  - subject: "Webkit?"
    date: 2007-05-28
    body: "I don't understand the news. Does this mean that webkit is (optionally) available within KDE and for example can be used in konqueror?\n\nThanks!"
    author: "LB"
  - subject: "Re: Webkit?"
    date: 2007-05-28
    body: "AFAIK, it is not yet possible but it's being worked on."
    author: "apol"
  - subject: "Re: Webkit?"
    date: 2007-05-28
    body: "exactly; the goal is for it to be available as a full kpart but it's still a work in progress. go tronical, zack, george and friends!"
    author: "Aaron J. Seigo"
  - subject: "Re: Webkit?"
    date: 2007-05-28
    body: "I a kpart something konqueror can use when browsing the web like a rendering engine?"
    author: "LB"
  - subject: "Re: Webkit?"
    date: 2007-05-28
    body: "a kpart is something that any kde app can use to display data of a certain mimetype. many applications use khtml in this way, including kmail, akregator, amarok, kcontrol ..... so once the kpartization of webcore is done it can be used in all sorts of places.\n\nthere is some khtml specific in many of these apps that really needs to be abstracted out to an html rendering interface that all such parts can then provide, making the use of html rendering more generic.\n\nand of course, webkit still has to integrate with a number of kde infrastructure components (cookiejars, wallets, etc)...\n\nso much work left, but at least it is going... =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Webkit?"
    date: 2007-05-29
    body: "does Webkit supports WYSIWYG?\nthats really missing in KDE (And Konqueror)"
    author: "Emil Sedgh"
  - subject: "Re: Webkit?"
    date: 2007-05-29
    body: "> does Webkit supports WYSIWYG?\n\ni don't know if the qt port of it is there yet, but safari certainly does."
    author: "Aaron J. Seigo"
  - subject: "Re: Webkit?"
    date: 2007-05-29
    body: "Cool, thanks a lot for your time and explanation. It's really appreciated.\n\nAlso thanks a gazillion for you work on Plasma. Please don't get demoted from some of the comments. Just realize that some people have no insight in the framework and don't know that a clock is a proof of concept of the underlying framework and not the ultimate goal. I for one think the clock is extremely cool and when I realize that this is a proof of concept I really can't imagine what other great things will come.\n\nThanks!"
    author: "LB"
  - subject: "Re: Webkit?"
    date: 2007-05-29
    body: "OK, I'm a little confused now, so please bear with me :-)\n\nI've seen various debates in the past over Webkit vs KHTML vs a merge of the two...  Is the intent here to have the slightly hacky (I've heard) but highly functional Webkit engine as an alternative to KHTML, for those who can't wait for features to filter though?"
    author: "Mark Williamson"
  - subject: "Re: Webkit?"
    date: 2007-05-29
    body: "as far as i've heard the goal is to eventually have WebKit (KJS and KHTML developed by apple) as the replacement for KHTML. fixes can than be made to WebKit benefitting both OSX and KDE communities.\n\nfirst it will be available as option. logically.\n\nbut please forgive me if i'm wrong here."
    author: "cies breijs"
  - subject: "Re: Webkit?"
    date: 2007-05-29
    body: "Point is, some want what you write, others want to keep khtml and not merge with webkit, to stay in control.\n\nSo we'll have both available for some time, until one will clearly be the one used and/or developed most. Darwinism the FOSS way ;-)"
    author: "superstoned"
  - subject: "Re: Webkit?"
    date: 2007-05-29
    body: "FOSS=Darwinism :P\n\nyou know ...\n\"Open source is the ultimate expression of Darwinism, and evolution takes many forms, but one thing is for certain:  There is no such thing as immortality for Linux, FreeBSD, or anything else for that matter.\"\nMatthew Dillon\n\n( from http://lists.freebsd.org/pipermail/freebsd-current/2003-July/006939.html )\n"
    author: "Dolphin-fanatic :)"
  - subject: "Re: Webkit?"
    date: 2007-05-30
    body: "At first sight I thought it was an \"Heroes\" quote :D"
    author: "Davide Ferrari"
  - subject: "Re: Webkit?"
    date: 2007-05-30
    body: "Why do you call Webkit \"slightly hacky\"? FUD?"
    author: "fast penguin"
  - subject: "thanks opensuse for your build system"
    date: 2007-05-28
    body: "i was able to see the first work done in plasma, in real, without even a direct internet connexion to my home, so thanks suse for your build service, and by he way for the first time, i got the pleasure to show a real new way of doing thing superior to my windows fanatic friend.\n\nso thanks guy, you are making the future"
    author: "djouallah mimoune"
  - subject: "Plasma"
    date: 2007-05-28
    body: "Wow, that plasma demo is really nice.   I'm very impressed by the architecture that asiego has talked about for Plasma, with the Data Engines and all that.   It seems like a very slick and flexible way of doing things.   With Kross too, it looks like a very bright future for Plasma.\n\nI'm amazed at how fast things are going for Plasma now that things are coming together."
    author: "Tom Corbin"
  - subject: "Kwin requirements?"
    date: 2007-05-29
    body: "Reading through the Kwin composite hardware requirements [1] I wonder what the problem with Intel cards is supposed to be: the Intel drivers are most likely the best integrated and most advanced developed available. The free ATI drivers do not integrate with RandR 1.2 atm, for example.\n\nAlso, they do support everything to run AIGLX - and Beryl and such alikes.\nSo why is kwin-composite not working? What is different in kwin composite compared to Beryl or Compiz?\n\nBtw., about Plasma: great to see the first visible results, I love it! I'm really looking forward to the engines and sources you developers will come up with!\n\nliquidat\n\n[1] http://websvn.kde.org/trunk/KDE/kdebase/workspace/kwin/COMPOSITE_HOWTO?view=markup"
    author: "liquidat"
  - subject: "Re: Kwin requirements?"
    date: 2007-05-29
    body: "KWin requires (for GL compositing) AIGLX, XGL or the proprietary nvidia driver.  If these are not available, but XRender is, then certain effects will still work.  If none of these are available, it'll be pretty much the same KWin you are used to from KDE 3.x...\n\nI haven't gotten GL Compositing running on my system, but the XRender mode seems to work alright for me so far...  I think it's just an X setting I need to change to make the GL stuff work.\n\nKWin article in a few days - maybe tomorrow or wednesday... trying to put a little space between the digest an my article for maximum exposure of both :)\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Kwin requirements?"
    date: 2007-05-29
    body: "GL for Kwin works fine here, tell me if you need screenshots or other things."
    author: "superstoned"
  - subject: "Re: Kwin requirements?"
    date: 2007-05-29
    body: "My question was more focussing on the question why the Intel drivers are marked as \"no idea\" and why there is a commentary implying thy would not work - because they should work for AIGLX in contrast to ATI's fglrx drivers.\n\nSo first, was Intel hardware tested at all? And second, if tested and failed, why did it fail?"
    author: "liquidat"
  - subject: "Re: Kwin requirements?"
    date: 2007-05-29
    body: "I think that the reason is very simple: the author doesn't own an intel based card, so he cannot comment for sure. Anyway I suppose it should work just like compiz and beryl do."
    author: "Davide Ferrari"
  - subject: "Re: Kwin requirements?"
    date: 2007-05-29
    body: "From reading the link, and seeing the \"no idea\" comment for Intel cards I think the explenation is simple. My guess would be that the developer writing it simpply does not have acces to a Intel card to test on, hence he has no idea if it works and how to set it up correctly."
    author: "Morty"
  - subject: "Re: Kwin requirements?"
    date: 2007-05-29
    body: "Something that be of interest was an article [1] I read yesterday. It did a comparison of the various graf cards in common usuage WRT beryl, compiz etc. For me, it is something to look at when I decide I need another computer.\n\nne...\n\n[1] http://www.phoronix.com/scan.php?page=article&item=730&num=1"
    author: "ne..."
  - subject: "Make KDE4's desktop a sandbox/playground!"
    date: 2007-05-29
    body: "Make KDE4's desktop a sandbox/playground with plasmoids being the \"toys\"!\nI commented this on the Plasmoid video on youtube:\n\nIt would be cool if you could resize and rotate the plasmoids interactively.\nWhat if, say, when you hover your mouse on top of a plasmoid for a few seconds, some buttons, \"hallos\", would appear around it. One would allow you to resize the plasmoid when you dragged it, another would allow you to rotate it. \n\nYeah, I'm hinting for a playground and sandbox aproach to the desktop in KDE4!\n[Perhaps that's even your original intention] \n \nThis is nothing new, Squeak's Morphs have that ability.\nIt would also allow you guys to test users reaction to this kind of interface, where the things you can Do to an object, are Around that object.\n\nThere are a few projects which implement this type of interface. \nSophie book (www.sophieproject.org) doesn't have a conventional formatting bar. Instead, you select a text/image/video/etc., and after some milliseconds, apropriate buttons for it appear on it's border, say for resizing an image, or for text, a formatting button expands into a miniwindow for configuration, to choose a different size and text font.\n\nThe other interface system [like this] appears in Squeak's Morphic system. \nHere's a short intro on how it looks and works:\n\nhttp://static.squeak.org/tutorials/morphic-tutorial-1.html "
    author: "hoboprimate"
  - subject: "Re: Make KDE4's desktop a sandbox/playground!"
    date: 2007-05-30
    body: "I had the same squeak-style-assosiation while reading your first sentence.\n\nactually as squeaker its quite logical to treat plasmaoids as a kind of \"squeak-morph\" using \"halos\". [1]\n\nBeside the Squeak [2] halo-paradigm in 2D - I suggest the authors of plasmoids to have look at the 3-D paradigm in Croquet [3].\n\nMaybe you'll find some nice options for plasmaoids here - especially when think of handling such a plasmaoid in a kind of 3D (or maybe more a 2.5-D) on your desktop (see the SuperKaramba screenshot on the digest-article)...\n\n[1] http://squeakland.org/school/drive_a_car/html/Drivecar12.html\n[2] http://www.squeak.org/\n[3] http://www.opencroquet.org"
    author: "doesn't matter"
  - subject: "Kicking some asses"
    date: 2007-05-29
    body: "What I like most about this, is that mr. Seigo (and probably others?) is actually *designing* software rather than just starting to write, make things look cool as quickly as possible (with bad design and code) and finally ending up with something that doesn't look as cool as it could have been and isn't extensible at all, therefore making it a drain to keep it up to date and to add new features.\n\nMany open source authors work that way. Personally I'm really happy to see that's finally about to change for a big and important project such as KDE.\n\nPlease don't listen to the whiners with their shortsighted visions. These very same people will kiss your feet on their bare knees once they see what the outcome of all this great work will be.\n\nI know I will."
    author: "Someone"
  - subject: "Re: Kicking some asses"
    date: 2007-05-29
    body: "No (assuming you mean by designing more than giving it some thoughts before coding)\nYou can only design a program if you know all ins and outs in front, like with an administration system. People trying to design other programs likely end up with a bloaded, unnecessary complex, constraining application.\nAbove all, its OSS written by volunteers, making something work and adding features and refactor(*) is fun. Designing is boring.\n(*) this is the part you missed\n\nIf you look at project like plasma/phonon etc, they basically do refactor common practice in KDE3, which is a good thing. However, being core libraries, their is certainly a risk of adding constrains on future development, simply because nobody knows how things are developing in front. Combined with all the cool stuff of Qt4, which every developer want to use of course, we should all be very alert that we not constrain ourself for KDE4.x (x > 0). This is the big risk of designing, looks cool on paper sucks in reality."
    author: "koos"
  - subject: "Applets and Desklets"
    date: 2007-05-29
    body: "I think that we should be reminded that plasmoids will take the place of applets and desktop widgets in KDE 4.  From my understanding, when plasma is finished, you will be able to drag your digital clock off of a panel and onto your desktop and it will become a clock widget, or you can drag your battery status desktop widget onto your panel and it will become a small battery meter beside your taskbar, and vice versa.\n\nThis seamlessness and consistency in and of itself will revolutionize our desktops.  This feature will make KDE make so much sense and be so easy to use.  This is why a clock plasmoid is so important, but is also why I am not too pleased about the porting of superkaramba to KDE 4.\n\nUnification is a beautiful thing.  If superkaramba continues to thrive in the KDE 4, users who love their plasmoids and want to find more will discover that some of the desktop widgets available on the web won't work unless they install a new package called \"superkaramba\", but that the new desklets which they install won't be integrated with their desktop as well as the plasmoids, causing confusion and destroying the purpose of plasma.\n\nI'd much rather see some good guides created which explain how to convert superkaramba themes into plasmoids with full functionality."
    author: "batonac"
  - subject: "Re: Applets and Desklets"
    date: 2007-05-29
    body: "\"From my understanding, when plasma is finished, you will be able to drag your digital clock off of a panel and onto your desktop and it will become a clock widget, or you can drag your battery status desktop widget onto your panel and it will become a small battery meter beside your taskbar, and vice versa.\"\nIs this correct? \nIf yes then WOW! :D I like it :)"
    author: "Dolphin-fanatic :)"
  - subject: "Re: Applets and Desklets"
    date: 2007-05-29
    body: "Yes.\nAs I know there will be no real difference between Panel and Desktop area, when Raptor Menu is Completed, you could have it even on your Desktop."
    author: "Emil Sedgh"
  - subject: "Re: Applets and Desklets"
    date: 2007-05-29
    body: "Agreed on all of the above (including the superkaramba concerns); I've been giddy since I first heard of Plasma. And heck, KDE4 gives me so much else to be giddy about, too! Who needs a daily newspaper when you can have the KDE svn commit log? :)\n\nOne thing in particular that I'm really enthusiastic about are extenders; they seem like such a clean way to offer a wealth of information to users (see http://plasma.kde.org/cms/1069). If the implementation ends up looking as clean as the mock-up on that page, I'm going to be one very, very happy chappy!\n\nI have exams coming up soon, but once those are over, you can be sure that I'll be hacking away at plasmoids; what more fun way could there be to start on KDE development? :P"
    author: "Jeff Parsons"
  - subject: "The adress for the Clocks in Plasma video"
    date: 2007-05-29
    body: "  \nClocks in Plasma (KDE Commit-Digest Issue 60)\nhttp://www.youtube.com/watch?v=Ti2GTO0KBqE&eurl="
    author: "hoboprimate"
  - subject: "Konstruct doesn't work?"
    date: 2007-05-29
    body: "Seems like konstruct is broken. \nI've downloaded it, unpacked and cd into konstruct/meta/everything and did a make all. Eventually it'll try to find pkg-info on download.kde.org, which isn't there. If I get that manually it'll continue for a bit more and then die on some other package which isn't there either (first it's libxslt, then libxml2).\n\nHas anyone else tried building from konstruct lately?\n\nAlso, is there a konstruct of KDE4? It would be sweet if I could build and run that without messing up my current desktop. Then I could really get into filing bugs or helping out in some other way.\n\nOscar"
    author: "Oscar"
  - subject: "Plasma and animation"
    date: 2007-05-29
    body: "I understand that plasma will really be interesting when animation and detaching/reattaching is added as core components. I hope the animation system is modelled after flash's since it is very usable and powerful.\n\nIf  plasma has the equivelent to movie clips (embeddable SVG?) and the ability to morph/animate the movie clips along paths and expose all of the APIs through kross, it would probably be enough to make plasma a Killer Feature. I'm guessing that plasmoid developers could build their own animation systems like people adopted flash's scene graph and added their own animation APIs during flashMX.\n\nI'm guessing that each plasmoid will need a library and heirarchical scene graph of instances+paths to achive this goal. Having a designer for plasmoids would be nice too (but not 100% necessary).\n\nGreat progress so far!!\nBen\n"
    author: "Ben"
  - subject: "why not use latest Qt / why not kill SuperKaramba?"
    date: 2007-05-29
    body: "Quoting from the story on Plasma:\n\n\"and speaking of widgets, work on widgets and geometry management of them is progressing as well. much of our work is likely to be superceded by work currently happening at Trolltech for either Qt 4.4 or 4.5, but we need basic support for layouts and widgets for KDE 4.0 (which uses Qt 4.3).\"\n\nSo it would be favorable to use the most recent Qt release which is 4.4./4.5, but because KDE 4.0 uses Qt 4.3 that's not happening? If the functionality is already included in Qt itself, then KDE is duplicating work, and wasting time, no?\n\nI don't know exactly how KDE development works, but my guess is that KDE settled on Qt 4.3 for KDE 4.0 because they can't keep upgrading the Qt version because then they would keep upgrading, confusing and slowing down development? And as soon as development of the next minor release starts, KDE 4.1, the latest Qt version will be used?\n\n\n\nConcerning SuperKaramba, I thought that SuperKaramba would be ditched, and be merged into Plasma? In the story I read that SuperKaramba will continue to exist because they don't want to loose all the work done on SuperKaramba applets so they need backwards compatibility. And they want to use it to fill the period of time when applets for Plasma aren't plentiful yet.\nBut I wonder, shouldn't it be relatively easy to port a SuperKaramba applet to Plasma? Is the advantage really worth the possible disadvantages, such as developing time being spent on SuperKaramba and not Plasma, duplication of work, and dividing the community between Plasma and SuperKaramba, when the goal is in the end that everyone uses Plasma and not SuperKaramba? I mean, what's the rationale behind this?"
    author: "Alexander van Loon"
  - subject: "Re: why not use latest Qt / why not kill SuperKaramba?"
    date: 2007-05-29
    body: "The latest Qt release is Qt 4.3 Release Candidate, final release is scheduled for early June 2007.\n\nhttp://trolltech.com/company/newsroom/announcements/press.2007-05-08.6670937954"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: why not use latest Qt / why not kill SuperKaramba?"
    date: 2007-05-29
    body: "I Think that nobody will not create Karamba when KDE 4.0 is release, just when KDE 4.0 Comes, there will not many Plasmoids, so exisiting Karambas could help.there will be no Divide.\nIts also a way to keep Karamba's alive.lots of job is done on Karambas, think about Aero/IO, LiquidWeather and ..."
    author: "Emil Sedgh"
  - subject: "Webkit vs khtml, konqueror vs dolphin"
    date: 2007-05-29
    body: "Eh am i seeing here the debian syndrome ?, where absolute democracy can block some hard decision to be made. But this is free software; you can't oblige someone to code as you like if he works for free \nI hope I am wrong \n"
    author: "djouallah mimoune"
  - subject: "Progress of Solid"
    date: 2007-05-30
    body: "The Solid homepage (http://solid.kde.org/cms/1002) says that big parts of it are completed. But how about the necessary backends, esp. for Microsoft\u00ae Windows\u00ae (as my family members have to use it for some programs and devices)? Is there work going on it, or is Windows\u00ae support even scheduled for 10/23/2007?\n\nThanks in advance.\n\n\n\nAnnotation: Microsoft\u00ae and Windows\u00ae are registered trademarks of the Microsoft\u00ae Corporation."
    author: "Guest"
  - subject: "Re: Progress of Solid"
    date: 2007-05-30
    body: "Hi\nI Think its the KDE-Windows groups TODO.You should ask them on Mailing list if you are really intrested.\n"
    author: "Emil Sedgh"
  - subject: "Gnustep"
    date: 2007-05-30
    body: "A question: when it webkit becomes a kpart will it then also be possible to use all the other Mac OSX functions? Does KDE interoperate with GNUstep?"
    author: "Ben"
  - subject: "Re: Gnustep"
    date: 2007-06-01
    body: "WebKit is a HTML renderer (it started off as a fork from KHTML). It is platform independent; it works for GTK+ (used in some Nokia devices in fact), and it seems Qt hooks already exist too (but they need to be extended to take full power of KDElibs). It means nothing with regard to MacOSX toolkit compatibilities."
    author: "fast penguin"
---
In <a href="http://commit-digest.org/issues/2007-05-27/">this week's KDE Commit-Digest</a>: Continued work in <a href="http://plasma.kde.org/">Plasma</a>, particularly in the clock visualisations. <a href="http://edu.kde.org/kalzium/">Kalzium</a> uses the GetHotNewStuff framework to download new molecules for its 3d viewer, plus speed optimisations for the rendering of these molecules. The start of fullscreen support in the <a href="http://gwenview.sourceforge.net/">Gwenview</a> image viewer. Work begins on a <a href="http://webkit.opendarwin.org/">WebKit</a>-based KPart. A KControl module is created to allow for easy manipulation of KWin "Composite" settings. Continued work on the OpenChange <a href="http://pim.kde.org/akonadi/">Akonadi</a> resource which enables interoperability with Exchange servers. Statistics plugin for a graphical representation of connection speeds in <a href="http://ktorrent.org/">KTorrent</a>. Improved handling of HDR imagery in <a href="http://www.koffice.org/krita/">Krita</a>. Branch created for the integration of <a href="http://solid.kde.org/">Solid</a>-based connection management and notification in <a href="http://www.konqueror.org/">Konqueror</a>.

<!--break-->
