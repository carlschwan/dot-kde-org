---
title: "Munich's KDE Desktops Usability Certified"
date:    2007-05-22
authors:
  - "oschmidt"
slug:    munichs-kde-desktops-usability-certified
comments:
  - subject: "Finally a credible T\u00dcV-certificate!"
    date: 2007-05-22
    body: "However lately the T\u00dcV is also \"certifying\" really weird things, like price-stability of some electronics store and the safety of \"Sofort\u00fcberweisung\" (instant money transfer) where you have to enter your bank-credentials (including PIN and TAN!) at an external website... these things really make T\u00dcV less credible to me.\n\nAnyway, congratulations, KDE!"
    author: "Scepticist"
  - subject: "Re: Finally a credible T\u00dcV-certificate!"
    date: 2007-05-22
    body: "\"these things really make T\u00dcV less credible to me.\"\n\nWhy? T\u00dcV looks quite comprehensive in testing everything, which gives them a degree of experience few organisations have.\n\nI trust german cars you know..."
    author: "NabLa"
  - subject: "Re: Finally a credible T\u00dcV-certificate!"
    date: 2007-05-22
    body: "Well, for one T stands \"Technischer\" and testing price-stability does not have anything to do with that IMHO. Secondly, Instant-Money-Transfer urges people to enter their bank-credentials at websites other than their banks', something the banks have been fighting against ever since phishing started. It's like paying with a blank cheque and I think most people will be even less capable of detecting phishing here."
    author: "Scepticist"
  - subject: "Re: Finally a credible T\u00dcV-certificate!"
    date: 2007-05-22
    body: "I agree. T\u00dcV has a good reputation from the past, but today it seems like that they will certify anything for money that it meets the selfset criteria. But still it is a nice to have, because many people still think a lot about it. And anyway: KDE is a great Desktop, anyone who has ever tried it, does not need a certificate to know that. :-)"
    author: "Mark"
  - subject: "Re: Finally a credible T\u00dcV-certificate!"
    date: 2007-05-24
    body: "\"instant money transfer) where you have to enter your bank-credentials (including PIN and TAN!) at an external website\"\nWell, that should be easy to certify. With a big fat 'reject' sticker. Seriously. People have just about got the idea that they should never enter their PIN into a web site or random telephone call, and then the banks turn round and destroy that ? Phishers paradise. Jeez.\n\nBut I digress - what great news for KDE !"
    author: "Tom Chiverton"
  - subject: "OK, but how can someone see LiMux at work?"
    date: 2007-05-22
    body: "It's not a public project, one can't download it, nor buy it on a cheap CD, nor get the sources.\n\nOpen source is open source... but not that open, huh?"
    author: "B\u00e9ranger"
  - subject: "Re: OK, but how can someone see LiMux at work?"
    date: 2007-05-22
    body: "Munich will make a public release (under the GPL) later this year."
    author: "Olaf Schmidt"
  - subject: "Re: OK, but how can someone see LiMux at work?"
    date: 2007-05-22
    body: "Is their KDE standard, or do they have patched it ?"
    author: "Charles de Miramon"
  - subject: "Re: OK, but how can someone see LiMux at work?"
    date: 2007-05-22
    body: "The GPL does not require them to publish their version to the general public. Only those that actually use the system (that is employees of the city of Munich) have a right to request sources."
    author: "CAPSLOCK2000"
  - subject: "Re: OK, but how can someone see LiMux at work?"
    date: 2007-05-22
    body: "I'm not sure employees have the right to sources either; after all, it's their employer that uses the software.\n\nIf they were using Microsoft Office or AutoCad at work, they would also not automatically have the right to use that at home. (Although Microsoft has a special provision for that in their license)."
    author: "Didi"
  - subject: "Re: OK, but how can someone see LiMux at work?"
    date: 2007-05-22
    body: "Indeed, afaik Munich doesn't distribute it to their employees so they don't HAVE to disclose the source. If they smart, they will, of course, but they don't have to."
    author: "superstoned"
  - subject: "ISO 9241"
    date: 2007-05-22
    body: "Does ISO 9241 make sense for usability testing?"
    author: "Ben"
  - subject: "Re: ISO 9241"
    date: 2007-05-23
    body: "http://en.wikipedia.org/wiki/ISO_9241\n\nPart 11: Guidance on usability"
    author: "whatever noticed"
---
Munich's KDE distribution <a href="http://www.muenchen.de/Rathaus/dir/limux/english/147197/index.html">LiMux</a> has been certified to meet the international usability standard ISO 9241. The use of KDE 3 as an "effective, efficient and satisfactory" working environment is named as a decisive factor for passing the certification. Read on for more details.

<!--break-->
<p>This is the first time that a Free Software desktop has been officially attested for usability by the <a href="http://www.tuvit.de/XS/T1.HeadNews/c.070100/EID.426/SX/">German certification association TÜV</a>. After an extensive two-year process of iterative testing, TÜV IT confirms that "Open Source has reached a very high level of professionalism" and concludes that KDE's Open Source nature made it very easy for Munich to ensure that the software is especially efficient and user friendly for their office workers.</p>

<p>LiMuX has been customized for the needs of Munich's city administration, which serves and governs 1,300,000 citizens. Further optimizations will be made in the future as part of product aftercare by using feedback from the growing number of users, the computer news website <a href="http://www.heise.de/newsticker/meldung/89781">Heise reports</a> (German). Until mid-2009, 11,000 desktop computers will migrate to KDE. Aim of the customization is not only a "comfortable feeling" for Munich's office workers, but also health protection and a reduction of retraining efforts.</p>

<p>TÜV is well-known in Germany for auditing the security of vehicles, machines, products and workspaces. For example, car owners are legally required to have their vehicles tested by TÜV every two years.</p>

<p>Mayor Christine Strobl agrees with TÜV's assessment from first-hand  experience, saying that the KDE migration was personally far easier than expected, <a href="http://www.linux-magazin.de/meldung/12836">a German Linux magazine reports</a>.</p>

<p>The ISO 9241 standard <a href="http://www.userfocus.co.uk/resources/iso9241/index.html">consists of 17 parts</a>. It can be summarized as a detailed list of criteria for evaluating the usability of computer software for a given purpose and group of users. Professional usability tests always address a well-defined use case. KDE aims to make it easy to optimise software deployments for the intended use cases by providing <a href="http://techbase.kde.org/SysAdmin/Kiosk/Introduction">advanced customization tools</a>.</p>
