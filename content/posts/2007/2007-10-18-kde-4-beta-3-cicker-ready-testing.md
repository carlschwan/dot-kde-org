---
title: "KDE 4 Beta 3 \"Cicker\" Ready for Testing"
date:    2007-10-18
authors:
  - "jospoortvliet"
slug:    kde-4-beta-3-cicker-ready-testing
comments:
  - subject: "do not release kde on current schedule"
    date: 2007-10-18
    body: "I think the kde4 should be released when is complete, not on a specific date(christmas) without major components completed. The KOffice schedule is a more realistic one"
    author: "zvonsully"
  - subject: "ok! many apps- just ports"
    date: 2007-10-18
    body: "current state is alfa, not beta. Maybe some core components are beta, but a Desktop Enviroment should be COMPLETE!!"
    author: "gogu"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "By whose standards of \"complete\" should it be released.  KDE 3 is still being updated on a constant basis, so by the strictest definition, it is not \"complete.\"  By your way of thinking, we would still be waiting for KDE 3.0.  I'll take my KDE 4.0 in whatever shape it comes.  Sure, I'm putting some faith in the efforts of others, but since they ask nothing in return, I'm not gonna bitch if there are rough edges to be worked out."
    author: "Louis"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "Did you actually read the announcement and try it out? I've been using the current version quite extensively, while there surely are some bugs, it is really getting usable. Most applications work really well and look excellent.\n\nI wonder what your findings are based on, it doesn't match my own experience."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "apps and components like kopete, kprint, koffice, translations are alfa or very simple ports from kde3. Also nasty bugs(some very old) are still present. My opinion is that kde 4.1 will be what kde 4.0 should have been"
    author: "zvonsully"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "You can use KDE3 apps in KDE4 (except kdeprint).  And simple ports are fine, since all those apps were great in KDE3, so a port to KDE4 does not diminish their greatness (improvements will come in due time)."
    author: "Leo S"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "KDEPrint from KDE3 should work fine for all KDE3 apps that run on top of KDE4. After all, KDEPrint(3) is part of kdelibs3, and if a KDE3 app is installed then mostly also the kdelibs3 needs to be installed.\n\nIn addition, kprinter used as a standalone utility should still work in the same way as it does now. You should be able that you \"Export to...\" or \"Save as...\" PDF/PostScript file, and still use kprinter from KDE3 to print it (unless kprinter from KDE3 suffers from serious bugs). \n\nThe limitation will be: If you print _directly_ from a KDE4 application, you can't use kprinter[3]. Instead, a new dialog, ${foobar_kprinter4} will appear. We are told this will have less functionality in KDE 4.0, but be back to comparable functionality in KDE 4.1.\n\nAbout kprinter{3} as a standalone app, see also http://www.kdedevelopers.org/node/3051 .\n\n"
    author: "Kurt Pfeifle"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "* You know that some \"very old nasty bugs\" are still present.\n\n* You know that some components are incomplete (by your definition) or \"very simple ports\".\n\nWell, okay.  All that's as it may be, and of course you're free to be highly unsatisfied if you really want to.  Nobody's going to force you to switch to KDE 4.0 - ever, let alone as soon as it's released.\n\nBut... What is it that you're contributing (code? artwork? translations? docs?) that will make KDE 4.0 better than you're evidently afraid it will be?\n\nIf the answer's \"nothing, really\", then either put up (contribute) or shut up. Or better, give thanks that so many skilled people are letting you (and me, I admit!) enjoy their hard work.\n\nThat's not to say that critical feedback isn't allowed, just that it's rather rude to say \"you're only halfway to where I want you to go!\" when you're not lifting a finger to help push the boat along.\n"
    author: "Yawn"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "He's contributing his thoughts, which are very valuable things.  Please don't diminish someone's participation just because you don't like the message."
    author: "Lee"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "He should've thought a bit more then as to not to waste our time. His opinion is clear, and wrong. \"release early and release often\". If you don't get Free Software, don't bother wasting our time voicing stupid opinions that don't make sense.\n\nFigure out how FOSS works, THEN you can contribute your thougts. If you're to lazy to do that, just shut up and let us do our job.\n\nNot nice? No, but it's true."
    author: "jospoortvliet"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "I don't particularly agree with this \"release early, release often\" idea, at least I don't think it should be followed at all costs.\n\nIf KDE 4.0 is a pile o' disappointment (and I sincerely hope that it will be much, much better than the so-called \"betas\" I've seen so far), it's a HUGE negative PR hit to the KDE project, and then you can try to fix that for a bunch of releases thereafter.\n\nI think it's not surprising at all that people would like to see a KDE 4 we all can be proud of -- the developers, the third party developers, the translators, the docs people, and last but not least the users, who've been waiting for the new and awesome KDE all this time."
    author: "Henrik Pauli"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "Big news:\n\na) Release early and often is absolutely a must with Free Software. There is no way this could work differently. A lot of people that are part of the KDE community won't join the effort until after release.\n\nb) All the big open source projects that have become successful, did this. One exception was early BSDs, they did not become successful, because they had these guys that didn't publish things before it was really good, which often never happened.\n\nc) We don't need to pretend that KDE development works any different from how it actually works. Quite a few things got redone for KDE4, they are not going to be in all cases ready to surpass KDE3 right now. But they are more ready to do so in the future.\n\nd) The whole point of releasing KDE 4.0.0 is the ability to release an KDE 4.1.0 that has seen a much wider review and participation. This is an absolutely needed and cruical step of the release process. Without public feedback and participation, KDE cannot work. The release of KDE 3.0 was not good in large parts, but it was an absolute necessity to get to where KDE 3.5 is today.\n\ne) Everybody has the absolute right to be very proud of KDE 4.0.0, because it is the best system that we were able to create up to now. The \"best\" there is often on the design level, on the code cleanups, and the added scope of the framework, and not so much in the polish. So what, that's how it works. If you can't be proud of the KDE 4 evolution and ambition now, you won't be able to contribute to what KDE 4.5 will be like.\n\nf) And that negative PR hit you fear is a strawman. Having people have a realistic view of KDE 4.0.0 is not a problem. The problem is people like you who appear to scare developers about releasing their Free Software, just because it doesn't (already) meet unrealistic expectations.\n\nAll you do, is waisting the time of people who are advancing the process. And you contribute to the false perception that KDE should only be allowed to make releases conforming to a standard that it never set itself.\n\ng) I personally am waiting for the stable release to start some work on Koffice2. I need a stable and supported platform on the API level to do that, I do not need it to be all revolutionary and perfect in its GUI. But I may help with that, starting from then.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "I'm not taking sides here because I didn't try any of the Betas. \n\nHowever, release early and often doesn't mean you should tag it alpha, or beta, or  RC, or final. It's just about releasing. If you all don't agree with the guy use sensible arguments. "
    author: "Bxs"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-21
    body: "> A lot of people that are part of the KDE community won't join the effort until after release.\n\nSo basically you plan to trick them into thinking it's a final release to get them to help you?  Do you realise what that will do to the trust and confidence people have in the KDE project?\n\n> All the big open source projects that have become successful, did this.\n\nHow many big open source projects that have become successful drop features and compromise quality to meet a release date?  It's practically the *opposite* of open-source development.  KDE is the exception, not the rule.\n\n\"Release early, release often\" is not about slapping a final release version number on it.  It's basically saying \"get your code out there as early as possible, no matter how incomplete, so that people can begin debugging and sending patches\".  That doesn't mean you should label that buggy code as a final release and it doesn't mean you should trick people into believing that it's finished so that they'll give it a go.\n\n> Having people have a realistic view of KDE 4.0.0 is not a problem.\n\nLook at the negative response on this website, which is full of technically-oriented KDE supporters.  There's even people *grateful* that basic features like the panel are working in the third beta!  You think the rest of the world is likely to be this forgiving?\n"
    author: "Jim"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-21
    body: "> So basically you plan to trick them into thinking it's a final \n> release to get them to help you?\n\nThat's a rather negative and wrong way to look at things. The fact of the matter is, there are never enough people testing beta versions of software. Beta testers are rare, and in open betas many of them who do test do not necessarily have the inclination to test thoroughly enough or make useful reports anyway. This is a problem faced my many developers in the free/open source world including the Linux kernel developers - often, after a period of testing, you just have to get something out there or you won't have the useful feedback that you need.\n\nThe beta releases have bugs. Yes, everyone knows that, and that is the purpose of testing. Those bugs are being fixed, and beta 3 is just that - beta 3 - it is not the release candidate. I sincerely doubt 4.0 will be let out the door with any true showstoppers.\n\n> That doesn't mean you should label that buggy code as a final \n> release and it doesn't mean you should trick people into \n> believing that it's finished so that they'll give it a go.\n\nNobody's trying to \"trick\" anybody. Honestly you and others on this board keep trying to put forward the idea that somehow 4.0 is going to be a bug-filled and unusable release, without having even seen it. The KDE developers are smart people, and they are not going to let that happen.\n\nThe level of uninformed and totally unproductive complaining on this forum is getting ridiculous. Please understand, you are not achieving anything other than demotivating the people who are putting the work in. Just have some faith, and some patience, and all will be sorted out."
    author: "Paul Eggleton"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-21
    body: "> That's a rather negative and wrong way to look at things.\n\nAnd when an end-user installs KDE 4.0 and finds it to be full of frustrating bugs, you don't think they'll look at it in that way?\n\n> Beta testers are rare, and in open betas many of them who do test do not necessarily have the inclination to test thoroughly enough or make useful reports anyway.\n\nPrevious KDE major versions managed to produce reasonably stable betas.  This is not the insurmountable task you describe it as.\n\n> often, after a period of testing, you just have to get something out there or you won't have the useful feedback that you need.\n\nAnd that should be decided by what day it is not matter how stable the code is?  You really think that?\n\n> The beta releases have bugs. Yes, everyone knows that, and that is the purpose of testing.\n\nThere is a world of difference between having some bugs but basically being operational, and having very little work right at all.\n\nI've used every beta since the 1.0 betas.  The quality of these is *far* worse than the betas for previous major version.  Those betas may have had bugs, but they weren't like this.\n\n> I sincerely doubt 4.0 will be let out the door with any true showstoppers.\n\nHow is this going to happen?  Because when you start with a much more unstable beta, you need to do a lot more work to fix it up.  Are you saying that KDE developers have gotten significantly better at fixing bugs than they were during previous betas?  Or are you saying  that they weren't trying hard enough during previous betas?\n\n> The level of uninformed and totally unproductive complaining on this forum is getting ridiculous. Please understand, you are not achieving anything other than demotivating the people who are putting the work in. Just have some faith, and some patience, and all will be sorted out.\n\nAll I see is the following:\n\n* This is nowhere near working properly.  The code wasn't ready for beta.  Can't you put off the final release?  It's not going to be stable in time.\n* Be quiet!\n* Troll!\n* It's a beta!\n* We need the developers to think happy thoughts!\n* That's what open-source is all about!\n* \"i get enough crap from applet and engine writers about shifting targets that i really don't care\"\n* \"to kvetch about that isn't very interesting\"\n* \"i'm not particularly taken by the heartstrings people are plucking here\"\n\nIf any other open-source project had responses to betas like this, they'd at least take the \"kvetching\" seriously enough to re-examine their release schedule rather than insulting people.\n"
    author: "Jim"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-22
    body: "> And when an end-user installs KDE 4.0 and finds it to be full \n> of frustrating bugs, you don't think they'll look at it in that way?\n\nYou don't know this is going to happen. Right now you are just spreading FUD on this issue.\n\n> How is this going to happen? Because when you start with a much more \n> unstable beta, you need to do a lot more work to fix it up. Are you \n> saying that KDE developers have gotten significantly better at fixing \n> bugs than they were during previous betas? Or are you saying that they\n> weren't trying hard enough during previous betas?\n\nAre you really looking for answers, or are you just trying to be inflammatory? It sounds a lot more like the latter to me. Anyway, in your statement which you repeat from your previous posts, you have not accounted for the fact that there are more people working on KDE now than ever before. Which satisfies your requirement for \"more work\".\n\n>... It's not going to be stable in time.\n\nYou keep implying this, but you have no way of knowing for sure that it is true."
    author: "Paul Eggleton"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-22
    body: "> Are you really looking for answers, or are you just trying to be inflammatory?\n\nI'm really looking for answers, and nobody is answering, just dodging the questions.  Don't call me a troll.  That's really insulting.  Why do you think it's impossible to be anything other than a brown-nose or a troll?\n\n> Anyway, in your statement which you repeat from your previous posts, you have not accounted for the fact that...\n\nHey, somebody is finally attempting to answer me.  Did it ever occur to you that I'm repeating it from previous posts not because I'm a troll but because nobody has attempted to answer this question until now?\n\n> you have not accounted for the fact that there are more people working on KDE now than ever before.\n\nYou're right, I haven't accounted for that.  How many more people are working on KDE?  In what capacity?  Are they application developers or library developers?  Are you aware that adding developers to a project can actually be counter-productive?  Have you read, e.g. The Mythical Man-Month, Peopleware or Death March?\n\n\n> >... It's not going to be stable in time.\n\n> You keep implying this, but you have no way of knowing for sure that it is true.\n\nAnd you have no way of knowing for sure that it is false.  But you seem awfully keen on telling me to shut up and \"have faith\" in the developers, which seems like an odd thing to do when they show every indication of caring more about a release date than stability.\n\nWhat do you think of this?\n\n> If any other open-source project had responses to betas like this, they'd at least take the \"kvetching\" seriously enough to re-examine their release schedule rather than insulting people.\n\nDo you agree?  Do you think re-examining the release schedule is unreasonable?  From the way you talk, it sounds like you think everybody should just shut up and just *hope* that 4.0 works okay.  That's not like any open-source project I'm familiar with.\n"
    author: "Jim"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-22
    body: "> I'm really looking for answers, and nobody is answering, just dodging the \n> questions. Don't call me a troll. That's really insulting. Why do you think \n> it's impossible to be anything other than a brown-nose or a troll?\n\nNo, I don't think it's impossible, and I did not call you a troll, I said you were being inflammatory. Which you were. Not necessarily because of what you said but the way you said it.\n\n> How many more people are working on KDE? In what capacity? Are they \n> application developers or library developers? \n\nNo idea. If you are interested I am sure you could make a study of it.\n\n> Are you aware that adding developers to a project can actually be \n> counter-productive? \n\nYes I am, and you are right, there is a point where too many people can be detrimental. I'm not convinced that KDE has quite reached the size where that has occurred, but it is definitely something to keep in mind when a project gets larger.\n\n> Do you agree? Do you think re-examining the release schedule is unreasonable?\n\nIt's not my decision to make. If it is the right decision closer to release when we know more about the state of 4.0 final, the KDE release team will make it. Regardless, complaining on this forum is not really the right way to go about effecting change in KDE release policy.\n\n> That's not like any open-source project I'm familiar with.\n\nI am sure that the reception of many open source projects to comments such as the ones on this forum lately from relative outsiders would be somewhat less than enthusiastic."
    author: "Paul Eggleton"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-23
    body: "> > Why do you think it's impossible to be anything other than a brown-nose or a troll?\n\n> No, I don't think it's impossible, and I did not call you a troll, I said you were being inflammatory.\n\nYou said:\n\n> Are you really looking for answers, or are you just trying to be inflammatory?\n\nIf I weren't really looking for answers and just trying to be inflammatory, I would be trolling.  You *were* calling me a troll, whether you meant to or not.\n\nThere are some people here, and I feel you are included, who react to any criticism that doesn't include glowing praise to offset it as if it were a troll.  That certainly gives the impression that you are dividing the world up into two categories, praisers and trolls, with no room for people who have real questions but no desire to praise.\n\n> Not necessarily because of what you said but the way you said it.\n\nCriticism without ego-stroking is not unreasonable.\n\n> > How many more people are working on KDE? In what capacity? Are they application developers or library developers?\n\n> No idea. If you are interested I am sure you could make a study of it.\n\nYou can't say that KDE is going to fix the bugs quicker because there are more developers, and when I ask how many more, admit that you haven't got a clue.  How do you know that they are going to fix the bugs quicker then?\n\n> I am sure that the reception of many open source projects to comments such as the ones on this forum lately from relative outsiders would be somewhat less than enthusiastic.\n\nHow many open source projects would let it get to this stage though?  Take a look at what's happened: the first beta was released that was utterly broken for many people.  The people pointing out that a beta should be at least barely operational were ridiculed and called trolls.  The second beta was released, same thing.  The third beta was released, same thing.\n\nThe response to this third beta did not come out of nowhere.  It is a direct result of previous actions by the KDE project that no other open-source project would take.\n"
    author: "Jim"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "> \"release early and release often\"\n\n\"Release early and release often\" does *not* mean that you should rush to label any old crap as a final release.\n\nWant to make lots of releases?  Go ahead!  Have lots of alphas and betas.  But don't drop features, ignore bugs and call the resulting mess 4.0 because you want to hit a deadline.\n\nThat's not what \"release early and release often\" is all about, and if you think it is, then it is *you* that doesn't get Free Software.  Free Software does not revolve around intentionally keeping everything in a permanent beta state.\n"
    author: "Jim"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "Hello,\n\neven Free Software needs a project planning and regular flow of releases. In these cases whatever is not ready is being pushed to the next release and only a few things are allowed to be absolute showstoppers.\n\nThese guys work for fun. Making them stuck in Beta with no users getting to see what was done so far, means no feedback, means no incentive to complete something on time. And why would developers of Phonon have to wait for the other guys that had enough time? Why would waiting any longer suddenly make lots of developers switch their occupation to doing what wasn't done so far?\n\nIn practice that would mean, nobody would write any kind of documentation, and nobody would do translation already. Application developers wouldn't yet start to think about porting their own programs. And you would loose these guys who are going to provide fixes, but not during development, because it's not what they currently use.\n\nAin't it funny how your last sentence is the only one that made sense? Keeping everything in a permanent and artificial beta state would indeed hold everything back.\n\nAnd final release? It's going to be an initial release. Since when did a major architectural restructuring a software to be immediately surpassing the previous result of many years of maintance? How is waiting without release even going to allow this to EVER happen?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "> even Free Software needs a project planning and regular flow of releases.\n\nI am not arguing against project planning in general.  I am arguing against *this particular* project plan.\n\n> In these cases whatever is not ready is being pushed to the next release\n\nBut it isn't a case of things that aren't ready being pushed back.  You *can't* push back kdelibs, khtml, etc.  If they aren't ready, then they make the whole of KDE unstable and you can't just leave them out.  And when it's a component you *can* leave out, it reduces functionality compared with what was there before.\n\n> These guys work for fun.\n\nSome do, some don't.  I believe the core contributors and people who set the release schedule are paid to work on KDE.\n\n> Making them stuck in Beta with no users getting to see what was done so far, means no feedback\n\nDoesn't this thread disprove that claim?  There's already been a lot of users seeing the betas and there's already been a lot of feedback.  You don't need a buggy final release for feedback.\n\n> And why would developers of Phonon have to wait for the other guys that had enough time?\n\nThey don't.  Development doesn't have to happen in lockstep.  Just create a new branch for 4.1 development and the modules that have been finished can push ahead without waiting for the modules that aren't stable.\n\n> Why would waiting any longer suddenly make lots of developers switch their occupation to doing what wasn't done so far?\n\nI don't understand what you mean by this.\n\n> In practice that would mean, nobody would write any kind of documentation, and nobody would do translation already. Application developers wouldn't yet start to think about porting their own programs.\n\nWhy do you think this?\n\nI think you have misunderstood me somewhat.  I'm arguing that KDE isn't beta quality yet, and that the release schedule shouldn't prematurely slap a 4.0 version on it.  That doesn't change the code or speed of development in any way.  Calling something 4.0 a month later doesn't slow down development by a month, it just means that what is called \"4.0\" has had a month longer to mature.\n\n> And final release? It's going to be an initial release. Since when did a major architectural restructuring a software to be immediately surpassing the previous result of many years of maintance? How is waiting without release even going to allow this to EVER happen?\n\nCan you try rephrasing that?  I don't understand you.\n"
    author: "Jim"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "\"Making them stuck in Beta with no users getting to see what was done so far, means no feedback\"\n\nThis is not true. Betas can be (and are) provided to users. Just look at Mplayer. The reason to tag something stable shouldn't have anything to do with wanting more people to install it, that is just decieving the user. \n\nAnd again, I am NOT taking sides here. I didn't try the Betas so they may well be quite good as far as I know. "
    author: "Bxs"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "113 BUG REPORTS. thats my main contribution ;)"
    author: "zvonsully"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "Thank you.  While it sometimes takes us developers (I say that if I have done any kde work in the last few years...) are while to get to bug reports, we do appreciate the effort.  Good bug reports make our job much easier.  Contrary to popular belief, they also make it more fun."
    author: "Henry Miller"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "That, my friend, is a load of bollocks. \n\n\"when you're not lifting a finger to help push the boat along.\n\nGreat. This is the correct attitude. Because the world has two types of people: artists that could contribute to KDE, or programmers that could contribute to KDE.\n\nThere is no such a thing as people that hasn't got the skills to do any of them. No? \n\nI'm tired of this childish attitude already. \n\nIf there is no criticism, you sleep on a bed of complacency, ffs, and never go forward. \n\nGrow up."
    author: "NabLa"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-22
    body: "> There is no such a thing as people that hasn't got the skills to do any of them. No? \n\nArtwork or coding? Perhaps there are, but this is a false dichotomy.\n\nThere is plenty of work to be done in a large project like KDE. Some useful activities are, for example:\n\nCoding\nArtwork\nTranslation\nAlpha and Beta testing\nFiling good bug reports\nBuilding precompiled packages\nRunning and administering project webpages\nHelping spread KDE/other Free Software projects through LUGs and word of mouth\n\nMost people can do some of this. "
    author: "KOffice fan"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-22
    body: "\"Most people can do some of this.\"\n\nMaybe you should also accept that there are people out there who like to use KDE and share their positive _and_ negative impressions for the common goal of a superior desktop, but who can't do any of this.\n\nMaybe you heard that all this takes time and maybe people have other priorities - work, job, family, real life - despite favoring KDE and taking an active part in discussions on the desktop (so called \"users\")?\n\nSeriously, we all can debate on release schedules and so on, but the worst possible arguments concerning a desktop environment aimed at the and user is \"you don't code, so you are not entitled to have an opinion.\""
    author: "Peter Thompson"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-22
    body: ">  Seriously, we all can debate on release schedules and so on, but the worst \n> possible arguments concerning a desktop environment aimed at the and user is \n> \"you don't code, so you are not entitled to have an opinion.\"\n\nYet this is not what anyone is saying. I was merely pointing out that there are things other than coding that one can do. And you still managed to completely miss it, as witnessed by your last sentence.\n\nAn open source project is not a free giveaway anybody is entitled to. It is a vibrant community, first and foremost, and every member of this community is a volunteer. There are many members of the community: programmers, translators, artists,  documentation writers, webmasters, beta testers, bug reporters, people providing extra material (educational files, etc), modders, and, yes, even regular users who take part in the community by discussing their experience in a respectful way and offer constructive suggestions. But they all form a community which produces a software platform in a cooperative and organic manner. And the barrier to entry (writing documentation, or proofreading documentation or beta-testing, for example) is ridiculously low.\n\nI haven't seen anyone here saying users shouldn't provide constructive criticism. But there is a difference between constructive criticism and \"Look, I don't wanna do shit, I just want you to write me a software for me and it should be like this this and this, cuz right now it's shit.\"\n\nIn short, nobody owes you anything.\n\n>  Maybe you should also accept that there are people out there who like to use KDE \n> and share their positive _and_ negative impressions for the common goal of a superior\n> desktop, but who can't do any of this.\n\nI fail to see how one can have the time to bitch for hours on a weblog in the pursuit of the common goal of a superior desktop, but cannot submit a bug report.\n\nAnyone who can share their positive and negative impressions can also submit a bug report or beta-test or proofread documentation.\n\nOf course, everybody's (respectful) input is welcome, but the length at which people go to avoid contributing and justify trolling internet forums is quite amazing."
    author: "KOffice fan"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-22
    body: "Funnily, the way you try to refute my point only proves it the more ;). But you probably didn't even notice it.\n\nAnyway, as you are so involved in the KDE development, you might have noticed from your regular visits to the KDE developpers blogs that there are some contributors who voice the same concerns as they are voiced by the stupid, thankless, lazy non-coding users here, so maybe...\n\nConcerning myself, I simply stopped trying out the KDE betas, its good enough to have some of the new apps running with good old KDE3. As many people already observed, quite some apps are already usable and simply better than their KDE3 equivalents (konsole)."
    author: "Peter Thompson"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-22
    body: "> Funnily, the way you try to refute my point \n> only proves it the more ;). But you probably \n> didn't even notice it.\n\nNo, I didn't. Care to explain?\n\nPerhaps our thoughts aren't all that different. I'm just saying two things:\n\n- Comments and suggestions are welcome, but this is a volunteer project and nobody owes the \"user\" anything. Nobody is saying that only coders are allowed to comment, just that the coders are not paid to suffer insults from abusive people on the internet.\n\n- It is not THAT difficult to contribute to KDE as some people make it out to be. \n\nThe two are unrelated."
    author: "KOffice fan"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "> apps and components like kopete, kprint, koffice\n\nYes and guess what, KOffice is released as Alpha ! And won't be release as final before at least six more months. As for kopete, its developers are still not sure wether it will make it for 4.0. Other application, like KDevelop or Quanta have decided to skip 4.0. But that shouldn't be much of a problem, as work has been made to ensure KDE3 applications to run fine in a KDE4 environement."
    author: "Cyrille Berger"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "If you think so, step up and help. There's a lot you can do even outside coding, such as translations, documentation...\n\nFolks, can't you just stop this mindless bashing? Do you realize it only has negative effects on the developers?"
    author: "Luca Beltrame"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "Repeat after me: \"Release early, release often\".\n\n\nThat's the Free Software way. If we would be microsoft, KDE 4.1 would be our first release. But we're not. We need to get this out there, so users can report bugs and request features and developers can start tinkering with it.\n\nSimple as that."
    author: "jospoortvliet"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "No, if we were Microsoft, we'd release KDE 4 beta 2 about five years from now. ;)"
    author: "Amy Rose"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "You are full of it if you think for a second that the amount of work done on Vista RTM isn't better and more stable than KDE4 beta 2. "
    author: "Anon"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "Vista actually is worse, they demanded from their BUYING users that they Beta-Test their application framework.\n\nHow can you compare this to KDE, which is \"just\" a desktop environment?"
    author: "she"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "Vista is a piece of crap, which is why companies are not moving to it, it is why computer manufacturers are still selling XP, because the demand to move to Vista was weak.  And it is why linux has picked up a lot of business customers.\n\nKDE is at beta 3, with a hell of a lot of work being put in by developers, and Novell working damn hard on it as well, so all the Ubuntu Novell Haters will get their nice new shiny KDE.  I tend to think people judging KDE4 on the beta's should shut up, and realise it is a beta, and wait for the final release."
    author: "Richard"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-21
    body: "> I tend to think people judging KDE4 on the beta's should shut up, and realise it is a beta, and wait for the final release.\n\nI've used KDE since the 1.0 betas.  None of the betas of previous versions have been anywhere near this bad.\n\nThe problem is not that this is beta quality, the problem is that this *isn't* beta quality.\n"
    author: "Jim"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "Release early, release often isn't about alpha quality software. \n\nGnome is releasing early, releasing often. Six months schedule, but every feature included has to be stable or else it gets cut and will be included in the next release. Or for the X+2 release. Or X+3. But it has to be stable."
    author: "Anon"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-19
    body: "Really? Then that's probably one of the main reasons Gnome is so far behind KDE.\n\nOne thing I love about KDE is that it is a great group of really bright people who are not afraid of experimenting with new ideas. If something breaks for a while as a result, that's OK, because the end result is worth it."
    author: "hmmm"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "Erm. Did you ever use KDE 3.0? KDE 3.0 *exactly* looked like KDE 2 although under the hood quite some things had changed (not only Qt 2 -> 3).\n\nMany people complained that time KDE 3.0 isn't complete and ready and that KDE 2.2.2 is much more better... But KDE 3.0 was absolutely necessary for the great success of KDE 3.x\n\nI could go back in time further. KDE 2.0 was a big change from KDE 1. A *huge* number of KDE 1 apps was not ported when KDE 2.0 came out. KDE 2.0 had some very rough edges and KDE 1 was still in use for quite some time.\n\nSo look at the old big KDE transitions how they went out. There is nothing unusal with KDE 4.0 release scedule compared to them.\n\nNone forces you to use KDE 4 inmediatly. KDE 3.5.8 is a great release."
    author: "Arnomane"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "Yes, kde 3.5.8 is great, but still konqueror can't use latest netscape-flash or swfdec because XEmbed is missing. So people will start to use firefox and move all their bookmarks there. Guess what? They'll not come back easily,\nif firefox doesn't broke itself."
    author: "nae"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-18
    body: "Can't speak for swfdec, but the latest version of Macromedia Flashplayer works great in Konqueror here (much better than the older versions used to work)."
    author: "Sutoka"
  - subject: "Re: ok! many apps- just ports"
    date: 2007-10-20
    body: "> There is nothing unusal with KDE 4.0 release scedule compared to them.\nWhat he said :)."
    author: "riddle"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "I (like everybody else) don't know what the final product will be, but my personal opinion is that at least very basic things who are currently missing should be in place when they release. Do File -> Open in KDE 3.5.x and 4.0 SVN (beta 3). The file dialog box in KDE 3 can do just about everything (even preview video files, open files on remote SSH locations using fish://, etc).\n\nI tried File -> Open in KDE4's kate and that was enough to determine that KDE4 isn't even close to complete, nor does it fit the description \"beta3\" (alfa3 perhaps). Simple affect-every-application things like file dialog boxes really need to be complete or these things will make ALL the KDE4 software unattractive."
    author: "xiando"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "I'm really looking forward to the new beta release!\n\nBut I think, concerning the naming of \"beta releases\", there should be one big \"lessons learnt\" from the KDE4.0 release cycle:\n\nNowadays, if a program is tagged \"beta\", it is expected that basic functionality is in place und an end user can install and use the software, albeit with some missing functionality and errors (for which he can file bug reports). Everything lesser should simply be labelled \"alpha\", in order not to raise unfulfilled expectations. \n\nIf I read the comments here, KDE4 seems nearing this kind of beta-status with the beta3 release, and that's good. For all prior versions, I didn't even think of filing bug reports - because so much was broken, I wouldn't know where to start. (Yes, I know, KDE has defined \"beta\" by certain internal milestones, API freezes and so on, but this criteria is of no value to the average non-developper end-user, at whom betas are targetted!)\n\nBut apart from that - thanks for all the work!"
    author: "Peter Thompson"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "> the average non-developper end-user, at whom betas are targetted\n\nA beta is not targetted at the end-user. KDE uses the classical definition of Beta, and that means \"functions - OK, stability - TODO\". What you mean with beta is the Google definition: \"The software is finished, but there might be one bug remaining.\""
    author: "Stefan"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "Functions - OK?\n\nBeta 1 and 2 weren't anywhere close to that, and I still can't open a damn thing with Konqueror in beta 3.  That doesn't sound like \"functions: okay\" to me, sorry.\n\n\"functions: bleeding, stability: bleeding\" is alpha quality, no matter how you look at it.\n\nThankfully from what I've seen Beta 3 has been incomparably better than the previous \"betas\", so yeah."
    author: "Henrik Pauli"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "I  think the BIG problem with KDE project is :\nWhat is KDE ?\n\nIt's a set of libraries (kdelib)\nIt's a window manager (kwin)\nIt's a desktop ( plasma, with menu, taskbar, pannel etc...)\nIt's a set of applications ( konqueror, kontact, koffice, games, edu, etc... )\nAnd more.\n\nAnd the problem is that from one part to another, there are HUGE difference in stability : see for instance, openSuse wich includes some KDE4 soft in there oficial realeese. WTF ? It's only beta and is included in stable realeese ? Yes, because it's, in my opinion, ready for it. But other parts are not even existing yet ( quanta, many apps on KDE-apps, wich even if not part of KDE, are important in a kde environement etc... ) and other in the middle, alpha or beta quality.\n\nSo, when you release all of this, what do you base your name on ? the libs? they are tagged realised already. The desktop, or widget theme, it's alpha (or was until the beta3)\n\nAnd last : If like me, you use a KDE4 build of your distro, it can add or fix some bugs. IE, mandriva and open suse will work and crash on uterly different things and soft, when they both have a KDE4 beta 2 ...\n\nI dare say that taking the goods in both distrib, and forgeting plasma, konqueror and oxygen ( wich are show stopper in the official agenda ), beta2 is exactly a beta.\n\n"
    author: "kollum"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "It's the applications developers job to port the applications to the new frameworks, not the framework providers, said in an eerie, sort of abstract kind of way.\nSo go whine to the Quanta developers about porting to KDE 4."
    author: "martin"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "I think he makes a good point, just perhaps not really well phrased.\n\nIf we look at the libs, it's certainly pretty stable, beta-quality, almost-ready stuff.  It seems that KDE developers measure the readiness of KDE 4.0 in this, hence it getting tagged as beta 3 now.  KDE4-the-framework is in a decent state.\n\nHowever, users, and beta testers of software will probably measure things completely differently, for example by how mature KDE4-the-desktop is or how mature KDE4-the-collection-of-user-software is.  Both of these are severely bleeding so far -- the desktop admittedly so (see a comment somewhere here by Jos), the software... hmm.  I guess we are exposed to this aspect of it much more and therefore spend our time complaining about the fact that pretty much nothing works beyond being able to open the whatever program, or perhaps play a round of Konquest or KMahjongg..."
    author: "Henrik Pauli"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "I personaly don't care if Quanta is not ported to \"4\" when kde 4 is released. I can go on with the 3.5 build ( if only I used quanta ). The same for Kopete, wich I use quite often.\n\nThe point I try to take to sun light is that diferent KDE components are in different states. So what would you base your release greek letter on ?\n\nThe thing is, KDE will be released with some missing bits and routh edges, but if you don't whant them, either :\n- whait for a later release, wich will be the same as waiting for the first released to be delayed.\n- use KDE 4 with some KDE 3 apps (oh, and even GNOME (yes, this other damn fu**ing desktop) ) if you are not satisfied with their KDE4 counterpart.\n\nand about this :\n> ... or perhaps play a round of Konquest or KMahjongg...\nYou forgot Kpat, wich is in my opinion the most polished KDE 4 thing today ^^\nAlso like Kalzium in its current state, and Kstars in kde4 is _for me_ more stable than in kde3.\n\nOh, and a last thing : you may whant to change oxygen to a less buggy widget theme, it will fix (as of beta _2_ ) a lot of bugs. (because some widgets arent drawn, you think \"where is this button? oh, such a basic feature isn't available !\" But changing the widget theme, buttons magicaly appear and everything sudenly sucks less )"
    author: "kollum"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-19
    body: "You seem to have been living under a rock for some time.\n\nKDE 4.0 is (afaik) so that the different technologies in KDE can start maturing (decibel, phonon, etc.), and most KDE developers suggest that if you want a (polished) desktop with, you wait for 4.1.\n\nAnd the betas aren't \"decided\" by how stable they are, but more how long it has been since feature freeze, etc."
    author: "martin"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "Ummm, actually the file dialog IS finished.  Everything in kdelibs is, bar some bug polishing, the removal of KDEPrint, and the special cases of kate and khtml.  That's why we're tagging the final version of kdelibs next week.\n\nNow, if there's features that you want that are no longer there, that's another matter.  Previews are still there (you turned them on in settings yet?), so's the edit bar too if you look for it (not immediately obvious I'll grant you).  So what are you missing that leads you to conclude it's not ready yet?\n\nJohn."
    author: "Odysseus"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "Yes, right-clicking to get to the traditional lineedit is not obvious, perhaps this behaviour could be set to default in kconfig somewhere?  \nAlso I note that it is difficult to revert, and that trying to type '/' then return to view files in the root folder does not work.  And are the verbose 'Home Folder'  etc  prefixes to the paths *really* necessary?\n\nDo we just file these as bugs in bugs.kde.org ?"
    author: "ptolys"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-19
    body: "> right-clicking to get to the traditional lineedit is not obvious\n\nmouse over the breadrumb, move to the far right, click. there's even a visual indicator. right click is unnecessary.\n\n> and are the verbose 'Home Folder' etc prefixes to the paths *really*\n> necessary?\n\nno in the line edit mode, no.. that shoudl be fixed indeed.\n\n> Do we just file these as bugs in bugs.kde.org ?\n\nyes, or provide patches which are even better =)"
    author: "Aaron J. Seigo"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-19
    body: "> that shoudl be fixed indeed.\n\ni just committed a fix for this as revision #726807.\n\nand now, time to take nap. my body thinks it is still in munich =)"
    author: "Aaron J. Seigo"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-19
    body: "> yes, or provide patches which are even better =)\n\nI'll have a look on the weekend to see how easy that is as I'd love to contribute in some way. Problem is I'm a physicist and I think/write fortran and python so any c++ patch might be a bit of a mongrel!\n\nIn case anyone else fancies a crack at it, there is also a small problem where long filenames are not always abbreviated (...'ed) in the dialog."
    author: "ptolys"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-20
    body: "\"Yes, right-clicking to get to the traditional lineedit is not obvious, perhaps this behaviour could be set to default in kconfig somewhere?\"\n\nYes please, let us have a way to configure line edit to be the default behavior.  It's so much quicker to type the location of a file then click through the filesystem.  I still don't see what's wrong with using an 'up' button instead of the road block to typing in a path that is the breadcrumb, but I'm cool with it as long as I can configure it away."
    author: "MamiyaOtaru"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-20
    body: "What I built yesterday seems OK regarding File -> Open in Kate, at least it is up to Beta.  Apparently not showing preview is default but it can be enabled.\n\nOTOH, File -> Open in KWrite resulted in a crash.  This would seem to be Alpha.\n\nThere are other issues!  The Quick Access Navigation Panel had \"media:/\" as a default.  This is still a useless 'feature'.  Is this to make it look more like Windows?  Well I have a lot of HD partitions and I have no idea which one to look on.  It appears to me that what is needed is: \"removableMedia:/\" so that we can have access to those devices without having to search through all of the HD partitions.\n\nHowever, I do agree that what has been released as Beta 3 is really the final Alpha.  I say this because the desktop doesn't work yet.  I can't find a menu.  Is there supposed to be one?"
    author: "James Richard Tyrer"
  - subject: "How a little math can make your day happier"
    date: 2007-10-18
    body: "To those that are dissatisfied with the state of KDE 4.0, simply substract 0.1 from the version number: In this way, the 4.0.x series becomes beta releases, 4.1 the first (real) release and the schedule is pushed back 6 months like so many requests. \nSeriously. What the current release is labeled as is quite unimportant. What matters is the insane amount of work that goes into KDE4, and that, not the labeling, is what will make KDE4 great. \nPersonally i think KDE 4.0 will be much more complete (relatively) than KDE 3.0 was."
    author: "th"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "The \"Release when it is ready\" plan does not work well. Look at projects like emacs or the hurd, they release once every blue moon. When you have a target date, along with freeze schedules, then efforts can be coordinated to release the code. Even if the project is late, the target date serves a purpose by helping focus efforts on stabilization.\n\nIf there is no target date, then what happens is that people keep adding features, and every time the code is getting stable, someone else comes along and adds an unstable feature.\n\nOne way to get around that, is by going modular, where every component has its own release schedule, but that has problems as well. Especially with a big project like switching QT version, that becomes difficult.\n\nAnother way, is the way the kernel is done: have tons of branches, each with some particular feature or bug fix. The branches as they become ready get merged. This way mainstream will only contain stuff that has been tested to some degree. While this could be done in the future, this is difficult when you are changing the platform like they are doing for kde4.\n"
    author: "Paul"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-18
    body: "\"Look at projects like emacs or the hurd, they release once every blue moon.\"\n\nYeah, that was a real good example, of nothing. Hurd has really never had a proper  release, nothing other than development releases. And it's been in development for what, 15 years or something. It does not really go anywhere. \n\nAnd Emacs, judging by version numbers it has had lots of, and frequent releases in the past(newer cared about it, so i haven't followed its history either so I may be mistaken). Current stable release is 22.1 or something. The fact that nothing much has been happening it the last years, does have more to do with the maturity of the codebase and the lack of new fetures. Than the release early and often paradigm. Besides from the user trends seen, it's steadily losing ground compared to other editors. So it may even fade into obscurity, where it imho belongs."
    author: "Morty"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-10-19
    body: "Best post in this thread.\n\nHe obviously has worked on codebases, unlike the armchair quarterbacks round here, bitching."
    author: "Joe"
  - subject: "Please do release kde on current schedule!"
    date: 2007-10-18
    body: "KDE 4.0 is a stepping stone to a replacement for 3.5.8 but it is not a replacement.\n\nSome people seem to have a fundamental problem with not upgrading when there is a higher number release! Early adopters of 4.x will take some pain but those early adopters are needed to take the 4.x forward. The release date serves to focus development on a specific goal and is needed. \n\nGetting the 4.0 out the door without some favourite apps or functionality is still the quickest way to get them done eventually rather than wait for the whole desktop experience to be complete so that everyone can upgrade from 3.5.8 at the same time.\n\n"
    author: "Matt"
  - subject: "Please release kde on current schedule"
    date: 2007-10-20
    body: "I just updated KDE 4 beta 3 from Opensuse 10.3 (3.94.1) and it is working great!!\n\nMajor things are working without much issues: dolphin, konsole, systemsettings. Even the panel started to show up 8-).\n\nI already watched movies, heard MP3's, and prepared a presentation with NO crash!!\n\nThe KDE 4 team is doing a great job!!"
    author: "Volker"
  - subject: "Re: do not release kde on current schedule"
    date: 2007-11-08
    body: "\"End User\" testing to me means:\n* All components either upgraded or compatible (installing won't kill my KNewsTicker or KPager or whatever) with 3.5\n* If something new replaces something old, there is an upgrade process that does not require user interaction\n* There are binary packages that upgrade existing KDE binary installs, and preferably a YUM repository for FC5 and greater at least.\n\n"
    author: "Otis Wildflower"
  - subject: "Slashdot, Digg, Phoronix, Linux.com ..."
    date: 2007-10-18
    body: "We need to submit this story into the tech media outlets. There are many cool screenshots here to get people excited about KDE4 :)\n\nhttp://www.kde.org/announcements/announce-4.0-beta3.php"
    author: "Scott"
  - subject: "Re: Slashdot, Digg, Phoronix, Linux.com ..."
    date: 2007-10-18
    body: "You can Digg it here:\nhttp://digg.com/linux_unix/KDE_4_0_Beta_3_Released\nhttp://digg.com/linux_unix/KDE_4_Beta_3_Ready_for_Testing"
    author: "Boris"
  - subject: "kubuntu package"
    date: 2007-10-18
    body: "kubuntu says it has packages but I can't see some packages such as dolphin and konqueror. Anyone know what's up?"
    author: "Patcito"
  - subject: "Re: kubuntu package"
    date: 2007-10-18
    body: "The kubuntu packages are fairly poor.  The desktop will not even start with kubuntu's packages. You're better off following techbases instructions to compile them from source... its actually not that hard."
    author: "Level 1"
  - subject: "Re: kubuntu package"
    date: 2007-10-18
    body: "Konqueror seems to be in kde4base, dolphin I don't know because I can't stand it and didn't look for it yet."
    author: "Henrik Pauli"
  - subject: "Apps on the taskbar?"
    date: 2007-10-18
    body: "That may be the first time I've seen apps properly showing on the taskbar in KDE 4.  It may seem small, but the biggest complaint I've heard from beta testers so far is the lack of functionality in the taskbar/panels."
    author: "T. J. Brumfield"
  - subject: "Re: Apps on the taskbar?"
    date: 2007-10-18
    body: "Yes, since it is ok to have many bugs, problems and missing features. But if you cannot switch windows or if it is just to complicated, than you cannot test over a longer time ;) At least this was my problem.\n\nAfter all, Beta 1 and 2 was not for beta testers, I think/hope they won't choose that name for Alphas/TPs again for the next release cycle ;) This one is the first version worth a test for me, the announcement also contains informations for Bug reporters.\n\nGreetings\nFelix\n"
    author: "Felix"
  - subject: "Re: Apps on the taskbar?"
    date: 2007-10-18
    body: "I really appreciate the great work of the KDE folks. But that doesn't stop me from being concerned about the current state of development and how it is \"marketed\". At least Beta 1+2 were far from Beta quality (as some others already said) - which is understandable, since a LOT of new cool stuff is being developed, and that takes its time. When the first Beta came out, I was really eager to get my hands on it, start testing and contribute to the project by reporting bugs, but I got frustrated within a few minutes since basically nothing useful worked. Beta 2 was a bit better, but still not really testable. Now (given that I don't have a flat rate) I'm reluctant to even download Beta 3.\nDon't get me wrong, this is NOT bashing. I am really grateful for all the awesome work, and I'm a big fan of the K. Consider this a petition to the developers not to stick to the release plan at all cost, but maybe rather release a few more Betas that are actually testable, so that things really actually DO get tested by \"black box\" users before the final release."
    author: "chris"
  - subject: "Re: Apps on the taskbar?"
    date: 2007-10-18
    body: "Point is, it simply depends on what you consider beta quality or not. The libraries where rather complete in the previous betas, and most applications as well. The basic workspace, plasma, on the other hand, wasn't. But I've always stressed that point in the release announcements.\n\n\nReally, it's mostly plasma which makes you (and all others here) complain about the bad quality of KDE 4.0 betaX."
    author: "jospoortvliet"
  - subject: "Re: Apps on the taskbar?"
    date: 2007-10-18
    body: "What I consider Beta quality is everything that can be tested in a non-critical production environment. That is, on my home PC as my primary DE for private purposes. And if the user interface doesn't work, that criteria isn't met. In other words, it's very frustrating. And I think this is valid criticism. \nAnyway, thanks for the information, that really cheers me up. I'm quite confident that 4.0 will be the greatest KDE release ever."
    author: "chris"
  - subject: "Re: Apps on the taskbar?"
    date: 2007-10-18
    body: "\"I'm quite confident that 4.0 will be the greatest KDE release ever.\"\n\nActually, quite the opposite: it will likely be one of the worst.  I'd wait for 4.1 or 4.2 before it gets to the same level as KDE3.5.x."
    author: "Anon"
  - subject: "Re: Apps on the taskbar?"
    date: 2007-10-18
    body: "But thats only because KDE 3.5 is such a solid release. You apparently are not familiar with the early KDE 3 releases, I think KDE 4.0 will beat those. :)"
    author: "Ian Monroe"
  - subject: "Re: Apps on the taskbar?"
    date: 2007-10-22
    body: "hi\n\ni just want to say that im using svn from 4 days ago... (beta 3 with a bit more progress) and is excellent... the quality is incredible, is so stable that is very usable already... im using it as my main DE right now, im also thinking in removing KDE 3.5 already, i actually don't need it anymore... sure, is not feature complete yet but KDE 4 is already usable for me and i can't wait for the final release :)\n\nkeep up the great work KDE team"
    author: "diego"
  - subject: "Dolphin Toolbar"
    date: 2007-10-18
    body: "I know this has been brought up time and time again, yet I haven't heard much of a developer response.\n\nIn literally every KDE 4 screenshot I've ever seen, there are these huge toolbars, most prevalent in Dolphin screenshots.  The icons are fairly large, have text labels, and perhaps worst of all, they have tons of dead/empty/white space around them.\n\nI know this is a subjective matter of taste and all, but I know there are plenty of people who like to maximize the space for the actual content in their application, and minimize the screen real estate that the interface takes up.\n\nI'm curious why the interface takes up so much space, and if there is a way within Oxygen to configure this, or will we just have to wait for others to develop other styles all together?"
    author: "T. J. Brumfield"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-18
    body: "Right click on those icons, then switch off the text (or show only the text, whatever you prefer), and choose the icon size to your liking.\n\nI find the buttons with icons+text default for most apps really useful, it goes together with a general cleanup of toolbars. They're also much easier to hit with the mouse or touchpad."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-18
    body: "Having icons+text is not Oxygen related. It is the same for all styles\n\nBut in Oxygen we will try to make the distance from icon to text smaller. No promisses though. I don't know yet if it's even possible in a friendly way.\n"
    author: "Casper Boemann"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-18
    body: "Hmm, rather then (just) minimizing the distance, why not make the font for the labels 1 or 2px smaller? Would achieve the same effect to make the buttons easier to hit, but would look better and wast _a little_ less space."
    author: "Martin Stubenschrott"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-18
    body: "\"But in Oxygen we will try to make the distance from icon to text smaller. No promisses though. I don't know yet if it's even possible in a friendly way.\"\n\nThat's very surprising - could you give a brief technical reason for this, if you have time? I'm intrigued - it seems like such a simple change, and I wonder what the hurdles could be.  Is it some inflexibility in KStyle/ QStyle etc ... ?"
    author: "Anon"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-11-03
    body: "Maybe this is related to the fact, that fonts usually contain a lot of characters, which go vertically beyond the top (or bottom) of the \"normal\" alphabet characters (A-Za-z) and text widgets usually have to size so big that all caracters of the font COULD be displayed, even when actually not needed.\n\nI had a similar problem with some line based report generator with which it was not possible to minimize the space between some text and a barcode below.\n\nMy solution was to edit the font with some font editor software, remove a lot of caracters, move or reseize some others and edit the global font characteristics values, such that no character got below or above an \"A\".\n"
    author: "Harald Henkel"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-18
    body: "The default behaviour of toolbars is large icons + text.  This is to make the functionality in toolbars more apparent, and discourage app developers from putting 30 icons in the toolbar (which makes the whole interface look cluttered).  \nThis is a global KDE option and you can easily switch back to the small, icon only view that was the default in KDE3."
    author: "Leo S"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-18
    body: "But will there still be so much empty space around each icon?"
    author: "T. J. Brumfield"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-18
    body: "Judging by quickly comparing it to the amount of space around the icons in KDE3 with it set to text under the icons, there doesn't seem to be really that much of a change in the amount of space used for that setting.\n\nI presonally prefer text alongside the icons. It still discourages the flooding of the toolbar with icons, but saves more area in the application for actual usage, and doesn't leave as much dead space to the right of the list of toolbar buttons (though as a default for everyone, it may be better to go with under from a usability perspective)."
    author: "Sutoka"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-19
    body: "I completely agree with the comments about the dead space in KDE.  I've been complaining about this since about the start of KDE3.  When I'm working on a windows machine at work, first thing I do is reduce the width of the scrollbars, reduce the size of the min, max buttons / bar.  Yet, these features are almost impossible to do.  KDE 3.5.8, and I still cannot reduce the amount of space wasted by a scrollbar I never use (god gave us the mouse wheel for a reason).\n\nIts not just that, but the space wasted everywhere - between buttons, round tabs, between toolbar entries etc.\n\nI always want the most out of my desktop real estate, and KDE just doesn't deliver.  Another example, the other day I was writing a little ruby script at work on notepad++.  Went home and wanted to continue working on it in Kate, but had forgot to commit it.  Remoted desktop into work, copied the text and pasted it into Kate.  On desktop that was exactly the same size, 1280x1024, notepad++ wasn't maximised, kate was, yet the lines of code were almost all wrapping on kate, but weren't anywhere near on n++.\n\nSorry to sound like I'm ranting, I'm really not, I just cannot believe so few people see this as a massive issue when it comes to producing a working env.  I love KDE to bits, I just cannot use it for anything more than a hobby when its so detrimental to my productivity."
    author: "Greg Loscombe"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-19
    body: "The scrollbars width depends on the widget styles. Polyester allows you to change the scrollbar width. The title bar height again depends on the window decorations used. Since window decorations & styles are just KDE modules implemented in C++, different widget styles & window decorations have different levels of customization available.\n\nRegarding the space, that should depends on the widget styles also. But even if I use the default Plastik and customized my toolbars to remove line separators, there's almost no redundant space left, IMO.\n\nI would also like to take the chance to rants about Windows then.\nSeveral missing features regarding windows management that absolutely killed my productivity in Windows are\n\n1. Missing windows setting such as shading, always on top and the ability to change the behavior under actions such as double clicking, mouse wheels, Alt + drags.\n\n2. The ability to assign window shortcuts. For example, I like to set Win+K to be my konqueror window, Win+E for my emacs windows so that I can focus them anywhere/anytime, even if they are minimized or on a different desktop.\n\n3. The ability to set window size, geometry, decorations based on the applications or on the windows name. For example, I like to open konsole to be only of size 600x480. I want my dolphin windows to be maximized vertically upon start since I usually display files in a listing not as icons, and many other uses.\n\nSo as a test case, try doing this in Windows. Copy 10 segments of text from a maximized webbrowser to a non-maximized editor. You will have to fiddle around with clicking taskbars to get focus since your if you want to focus your webbrowser, you have to click it, which then de-focus your editor, and since your editor cannot be always on top, to get back to your editor, you have to click the taskbar or cycle through the window task list with alt-tabs.\n\nBottom lines ? I just can't believe, even when I'm high, stoned, and having the greatest sex of my life, that so few people see Windows as having a massive detrimental effects to their productivity. "
    author: "Yosh!!"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-19
    body: "You don't have nVidia card and driver, have you?\nThey give you shading and auto focus and such.\nActually, WinXp is not about WinXP, it's about WinXP\nmodders, tweakers and hackers."
    author: "reihal"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-21
    body: "Your absolutely right. And its not only wasted (work)space, because of big \nplump (or neat - it doesn't matter here) icons and widgets, that makes me \nfeel a chance was missed. The chance to become (stay) a (the) professional desktop. \nAttention turned to eyecandy, Dolphin instead of Konqi as the default FM, \nicon-views instead of tree-views etc. The latter is a good example. \nThere is - known for ages, think of genealogical trees - nothing what \ncompares to tree-views when it comes to map hierarchical systems. \nAnd thats what our current file systems are. But then it was argued, \nsome would have difficulties with treeviews: \nA. Seigo: \"... but if we can avoid a treeview that would be great as \nmost people and trees don't get along ...\" - So here is a decision,\nmade not by the accurate solution but caused by the customization to \nan (imaginary) average user. But you cannot map a hierarchical system \nwith a flat (icon)view regardless of 'breadcrumb-navigation' ... You can \nnot abstract from the essential.\nAnd what was wrong with the approach, to provide a user with the \nessential features but let him also choose some quite usefull simplification \nor nice eyecandy. That is what i would think of as a professional approach.\nThe reverse - even if its really nice - it's not.\n\nI would not care if only the defaults would change (even though i think it's\nthe wrong turn), but here priorities had changed. And the doubtless great efforts\nthat were made to those eyecandy and simplifications should have gone more into \nthe great and outstanding features KDE has had - Konqueror to name it. But also\nKate or Quanta, or KDE-wordTrans - not only because of my bad English ...\nRight, there was work done too - but i'm talking about priorities and tendencies.\nAnd if those features and applications are as good as they could be now, then \na community would arise, building superkaramba or the like, to make their workplace nicer ...\n\nIMHO\n \n"
    author: "stein"
  - subject: "Re: Dolphin Toolbar"
    date: 2007-10-21
    body: "a) Dolphin has a tree view; and b) there has not been some huge shift in focus from functionality to \"eye-candy\" - both are worked on, but Qt4 makes eye-candy so easy to create that taking advantage of it is practically effortless.  "
    author: "Anon"
  - subject: "Composite Screenshot"
    date: 2007-10-18
    body: "Looking at the composite screenshot ( http://www.kde.org/announcements/announce_4.0-beta3/kwin-composite.png ) it appears that the Konsole window has the background of the console area alpha-blended, but not the text? If so, that's definitely going to be a nice feature, though adding a slight amount of blur to whats underneath might make it easier to read the text when theres text under it (though I'm pretty sure I first heard of that method on the blog of the guy working on KWin-Composite, so probably don't need to tell him what he told me ;). Yet another feature I'm going to be excited for, a Composite Manager that doesn't forget about the whole Window Management thing! Though hopefully by the time I get around to upgrading to 4.0 there will be better r300 support for Composite & AIGLX.\n\n"
    author: "Sutoka"
  - subject: "Re: Composite Screenshot"
    date: 2007-10-18
    body: "Oh my! I nearly forgot the most important part of my post: Thank you to all the KDE contributors for your underappreciated work! The KDE Community has really created quite an amazing project."
    author: "Sutoka"
  - subject: "Re: Composite Screenshot"
    date: 2007-10-19
    body: "KWin does indeed already have the ability to blur what's below a semi-translucent ability -- the effect simply wasn't enabled in the screenshot. :)"
    author: "Eike Hein"
  - subject: "Oh come on!"
    date: 2007-10-18
    body: "I can't believe how many of you are bashing the developers.\n\nThey are making an amazing job, it's indeed true that some applications are just ports, so what? Don't you use them every damn day in KDE 3.5.8?\n\nI can't understand all that people, I mean, they basely port everything to Qt 4, then they added new feature, wrote a new technologies like Phonon, Plasma and Solid, Added Composite support to Kwin, created a new theme, and it's not just gnome theme or domino engine, it's actually a lot of work, have you ever seen all the work on the icons?\n\nAnd because they couldn't add features to 2 or 3 applications and didn't have time to completely port 2, you're fucking in every single commit digest.\n\nKDE folks, just ignore them, you are (and always had) making an amazing job with KDE 4, cheers to all of you."
    author: "Luis"
  - subject: "Re: Oh come on!"
    date: 2007-10-18
    body: "Well said.\nToo many people act as spoiled children here. It's not that much a question of making some remarks than the way these remarks are made.  "
    author: "Richard Van Den Boom"
  - subject: "Re: Oh come on!"
    date: 2007-10-18
    body: "You summed up my feelings quite well.\nDo the people expect completely new apps each major release?\n\nI know that when I read the release announcement on kde.org the changes were very exciting (especially in Plasma <3). Keep up the good work, guys. Don't let the n00bs burn you out. ;)"
    author: "Jonathan"
  - subject: "Re: Oh come on!"
    date: 2007-10-18
    body: "One of the most stupid remarks. Where are people bashing the developpers? On the contrary, most people here seem to be looking forward to the new and shiny KDE 4.\n\nThere are, however, some people, including me, who are a bit dissatisfied with the fact that things don't quite feel \"beta\", rather \"alpha\". This is not bashing, this is criticism. Maybe even constructive criticism. \n\nIf you don't like that, you should forbid all kinds of user feedback which are not enthusiastic. Maybe even bugzilla, where per definition only \"negative feedback\" is posted?\n\nCountering such postings with \"if you don't code, don't complain\" or disqualifying them as \"trolls\" disqualifys yourself. In fact, I remember that the KDE developers, unlike the Gnome developpers, tend to react receptive to user feedback, even if it's not only positive, and integrate it in their plans.\n\nBut in order to improve a product, you must be free to point out shortcomings."
    author: "Peter Thompson"
  - subject: "Re: Oh come on!"
    date: 2007-10-18
    body: "There's a big difference between feedback and bashing.\nI didn't, in any way, tried to said that everyone in dot make this, but there quite some that does it, and If you can't see them, well my friend, you're maybe one of them ;)"
    author: "Luis"
  - subject: "Re: Oh come on!"
    date: 2007-10-18
    body: "No, feedback is : \"you have made that particular choice, when I use this particular app. I think it's not the good one because of this, and this, and this\". Or \"I tried this in this app, it crashed repeatedly\".\nBut what I see roughly is \"nothing works, you must not release anything because nothing is ready, you should have done this instead of that, you lost a lot of time because of this, KDE4 just cannot be released that way\", etc.\n\nIt's NOT constructive feedback, it's nowhere close to be useful, it's just completely depressing and negative at a moment when the developpers need support and positive feelings to get it out in time in the best possible shape.\nMany of them have worked for years on this and when you work on such a large project, you need to have it out at some point to get some new impulse. Otherwise, if it gets postponed all the time, you just get worn out and leave. If the devs were listening to the constant moaning we see here, KDE4 would never get out because there would always be something to fix/change/adjust.\n\nExactly what do you expect by saying \"this is alpha, not beta\" or things like that? What do you think you're contributing? It's not being a genius to imagine that KDE devs do not want the final release to be unstable or completely useless, so it's easy to consider that they will use the remaining months to iron out everything as much as possible. So again, what is the point, the \"constructive behaviour\", in complaining that something is more \"alpha\" than \"beta\"?\n\nBTW, I believe that not providing a single line of code or a single second of work to a project should force anyone to use a certain amount of humility when asking for features or making a critic. And my opinion is that most of the critics I see here completely lack this humble tone I would expect. There is the excuse, of course, of being fond of KDE and wanting it the best possible, but I think the devs want it too, so noone should be afraid."
    author: "Richard Van Den Boom"
  - subject: "Humility??"
    date: 2007-10-19
    body: "> No, feedback is : \"you have made that particular choice, when I use this particular app. I think it's not the good one because of this, and this, and this\". Or \"I tried this in this app, it crashed repeatedly\".\n\nNO.  feedback _IS_ \"i've used this suite of applications and find things to be unstable\"    \n\nif you see enough of those, a clue bulb might go off and the next step would be to filter the \"nothing works\" out from more relevant feedback.\n\nI believe most people are smart enough to filter trollz from a general feeling of \"we find thing to be possibly unstable\".\n\nwhat you are looking for and will never get, is \"in line 46 of file kde.h, there is a macro referencing a null ptr\", etc.  NOT going to happen.\n\n> I believe that not providing a single line of code or a single second of work to a project should force anyone to use a certain amount of humility\n\nhumility?!!?\n\nwithout users of various degrees of technical enthusiasm,  KDE SVN should be a closed portal reserved for those that code in cluster jerks.\n\nReal developers don't ask for humility."
    author: "dave null"
  - subject: "Re: Humility??"
    date: 2007-10-19
    body: "well, if it's any help to this discussion: i've found a couple of useful comments here but most of them have been completely unhelpful and very frustrating. yes, i'd rather do without that."
    author: "Aaron J. Seigo"
  - subject: "Re: Humility??"
    date: 2007-10-19
    body: "Well, sorry if you take the comments personally. I really didn't seen any _personal_ criticism and in fact I'm very glad that you KDE developpers keep on doing such a great work (even donated - a few - euro now and then as my only contribution). I'd like to say that, I know, it's hard and often unrewarding work, and you're doing a great job at it.\n\nBut without taking it personally, I think it should be taken into account by the devs that there is some dissatisfaction with the current release policy.\n\nI see it for myself: In former KDE betas, starting from pre-1 versions, I could use them for my daily usage at home. A few bugs, but basic desktop working. The KDE4 betas, however, in most cases where missing absolutely fundamental features like starting apps and switching apps. \n\nThis drove me off from further beta testing, and this cannot be what you release beta versions for. "
    author: "Anon"
  - subject: "Re: Humility??"
    date: 2007-10-24
    body: "Aaron didn't say he took it personal, he said most comments weren't very useful. He's simply right about that. 90% of the complaints where based on the non-functional plasma - and that's only 1% of KDE. Most other components are already very very usable."
    author: "jospoortvliet"
  - subject: "Re: Humility??"
    date: 2007-10-19
    body: ">Real developers don't ask for humility.\n\nWhat a peremptory statement. And how much did you contribute to KDE to speak as a \"real developer\"? How much did you contribute to **any** open source project to make such a statement?\nAnd more importantly, how well do you know all the KDE devs to make any claim on how they take the different broadly general critics done here?\nDid you ever consider that when you have spent maybe 20-30 hours a week to work, sometimes besides a full time job, on a project, you may not be very pleased or energized by \"it's unstable\" comment? Especially when this statement is not coming as well with some positive or encouraging feedback beside it?\n\nWell, my opinion is that KDE devs are real developers who probably want to have some kind of appreciation for all the time they spend freely developing these tools. And to get again and again and again things like \"this is unstable, period\" is just unrewarding, depressing and counter-productive.\n\nAs for your view of feedback and respect, you should go back using software you paid for, your attitude suits the relationship with software companies very well.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Humility?? "
    date: 2007-10-19
    body: "Richard,\n\nYou seem to be taking so much personally while making blanket statements about others when at the most, you can only speak for yourself.\n\nYou should take a few hours away from this charged discussion and hopefully come to the conclusion that one can only hope for respect while not asking for such things as humility from others -- it is so very wrong to even think this.\n\nperhaps the connotation of your words was not what you meant it to be?"
    author: "dave null"
  - subject: "Re: Oh come on!"
    date: 2007-10-18
    body: "> I can't believe how many of you are bashing the developers.\n\nI don't see anybody bashing the developers.  I only see people bashing *the release schedule*.  The developers are doing a great job.  If the same code was released as alphas and the developers weren't being rushed, everybody would be fine with the way things are going.  But whoever has decided the release schedule and that these alpha-quality builds should be called betas is screwing things up for everybody.\n"
    author: "Jim"
  - subject: "Re: Oh come on!"
    date: 2007-10-18
    body: "Well, I guess the release schedule is a concerted agreement by most of the devs, that the dates were more or less set in stone some months ago, even a year, and thus that what's in the alpha/beta is what the devs managed to get in for the day of tagging.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Oh come on!"
    date: 2007-10-18
    body: "> the release schedule is a concerted agreement by most of the devs\n\nDo you have any idea just how many developers contribute to KDE?  \"Most\" would be hundreds of people.  I don't think the release schedule is \"a concerted agreement by most of the devs\".\n\n> the dates were more or less set in stone some months ago\n\nThey weren't \"set in stone\".  There is nothing forcing the KDE project to conform to that schedule.  If the KDE project releases things according to that schedule, it is by *choice*.  They could also choose to change the schedule or ignore it entirely if they felt it necessary.  From the opinions I see here and elsewhere and the quality of the betas so far, I think a lot of people feel it is necessary.\n\n> and thus that what's in the alpha/beta is\n\nSo if the release schedule is responsible for producing the worst betas in the history of the KDE project (IMHO, been using it since the betas of 1.0 and have never seen a beta utterly fail to operate, let alone three betas in a row), perhaps that indicates that the release schedule is a bad idea.\n"
    author: "Jim"
  - subject: "Re: Oh come on!"
    date: 2007-10-19
    body: "I'm not familiar with all the process that led to this schedule, but I can imagine that if most of the devs had found that the schedule was completely insane, it would have been changed. If it stayed that way, I suppose that most devs actually were happy with it or at least considered it to be an achievable target. It has actually been postponed, remember?\nI've been involved in some projects before and there's nothing more depressing than having the feeling that everything is always postponed and that nobody will ever use your work. I understand that at some point, people want to see their work out and have others using it. This is probably even stronger in open source. So having a schedule is not a bad thing, and post-ponning it again and again can actually be bad. Better release in the best possible shape then prepare a second release with fixes.\nAs for the betas, it doesn't matter if they're the worst in the history of KDE, what's important is the final result, whether you like it or not. When you make a movie, it doesn't matter if the first cut is slow and too long : the final cut is what counts. But you need the first cut to find your way to the final one.  "
    author: "Richard Van Den Boom"
  - subject: "Re: Oh come on!"
    date: 2007-10-21
    body: "> I've been involved in some projects before and there's nothing more depressing than having the feeling that everything is always postponed and that nobody will ever use your work.\n\nThat is not an argument for releasing something that is not ready.  You know what's worse than something of yours not being released?  Something of yours being released and then finding out that it's full of bugs.\n\nWhat this *is* an argument for is an earlier feature-freeze.\n\n> having a schedule is not a bad thing\n\nI didn't say that schedules were bad.  I said *this* schedule was bad.  I agree that having a schedule is a good thing.  But that doesn't mean any particular schedule is good just because it's a schedule.\n\n> As for the betas, it doesn't matter if they're the worst in the history of KDE, what's important is the final result\n\nOf course.  But where is this stability going to come from?  Stability doesn't magically come about from calling something the final release.\n\nUnless you think they weren't trying very hard with previous releases, they are only going to do as well at fixing bugs as they have done with previous releases.  So if the betas are far buggier, then either it will take far longer to get the final release out, or the final release will be far buggier than previous releases.\n"
    author: "Jim"
  - subject: "Konqueror's background"
    date: 2007-10-18
    body: "Wonderful! Hope that we have beautiful and stable KDE 4 in January.\n\nI have one request though. Is there any chance that the background picture of Konqueror's start page could be changed? (see http://kde.org/announcements/announce_4.0-beta3/konqueror.png) \nI know it's a mater of taste, but I think that colour and design could be much better. ;)\n\nRegards"
    author: "Prom"
  - subject: "Re: Konqueror's background"
    date: 2007-10-18
    body: "I would say, do your best and make a suggestion...\n\nIf you have a good one that a lot of people like I could be taken...\n\nAnd those developers have a little more time, pollishing KDE 4.0..."
    author: "boemer"
  - subject: "Re: Konqueror's background"
    date: 2007-10-18
    body: "First off, this background is exactly the same as in KDE3, except for the new Oxygen icons. Yes, they indeed don't fit well with this old background... and some of them are missing, displayed as \"?\" which looks bad of course...\nSomebody attempted a redesign:\nhttp://lists.kde.org/?t=118987262500006&r=1&w=2\nIf you would go ahead and propose other designs, that would be terrific."
    author: "logixoul"
  - subject: "Re: Konqueror's background"
    date: 2007-10-19
    body: "note that design proposals should come in the form of working html, as that is what is used for that particular screen."
    author: "Aaron J. Seigo"
  - subject: "Great job!"
    date: 2007-10-18
    body: "Thank you KDE Team."
    author: "Dawid Ci&#281;&#380;arkiewicz"
  - subject: "Oh please give them a break!"
    date: 2007-10-18
    body: "Can't all those \"experts\" please stop posting the same bollox time after time again?\nI'm so tired reading posts about wrong release plans, betas being alphas, icons being too large etc. all over the dot.\n\nCongrats to all the Devs!\n\n________\n\"The usability people need to usabil harder.\"\n\n\n"
    author: "another expert"
  - subject: "beta 3 background "
    date: 2007-10-18
    body: "where can i get that beautiful flower background or in what package is he included . thx :-)"
    author: "pete"
  - subject: "Re: beta 3 background "
    date: 2007-10-18
    body: "It's one of the thousands of submissions to the Oxygen wallpaper contest. Once the winners are chosen, all the submissions are going to be uploaded somewhere (the archive is huge but somebody already offered to host it).\nOn that note: great choice for a wallpaper! Signifies how KDE4 is a flower slowly unfolding and growing :)"
    author: "logixoul"
  - subject: "Re: beta 3 background "
    date: 2007-10-28
    body: "Breathtaking, my wife loves it a lot. Really nice art work :)"
    author: "Bobby"
  - subject: "Impressive Stuff"
    date: 2007-10-18
    body: "I guess the developers might get the wrong impression from all the bashing posts.\n\nPlease note that most people only post to complaint or give their suggestions, everyone else just thinks \"wow!\".\n\nSo, instead of just thinking it I'll write:\n\nwow! \nthese guys know where their towel is.\nand thanks for all the free (and great) stuff."
    author: "-"
  - subject: "Re: Impressive Stuff"
    date: 2007-10-18
    body: "\"Please note that most people only post to complaint or give their suggestions, everyone else just thinks \"wow!\". \"\n\nwhat \ncan I say now, wow! :D"
    author: "Dolphin-fanatic :)"
  - subject: "Splendid!"
    date: 2007-10-18
    body: "Great work guys! Keep it up and forget about the trolls!"
    author: "Paul"
  - subject: "Re: Splendid!"
    date: 2007-10-19
    body: "Critisism isn't always trolling."
    author: "Dennie"
  - subject: "Alpha 3?"
    date: 2007-10-18
    body: "Goto the Kubuntu forums and take a look at everyone who has tried KDE4, it has never worked, not one release.\n\nThis could very well be the fault of those that compile for the repos, but its where many are getting their bad experience from.\n\nFrom my experience in Kubuntu with KDE4, its not even Alpha, let alone Beta.  _nothing_ except the clock works.  No apps, no taskbar, no working desktop, nothing.  And its been this way with every release, for everyone I know using Kubuntu.  And since 'Beta' 2, the desktop wont even load anymore.\n\nI sincerely hope that there is a major flaw with the Kubuntu packages, and that this isnt purely the fault of KDE itself."
    author: "Leto Atreides"
  - subject: "Re: Alpha 3?"
    date: 2007-10-18
    body: "I'm going to try Beta 3 this weekend, and report my findings.\nIn any case, please spare comments like \"it has never worked\". Detailed reports, without \"oh it's alpha and not beta\"-style remarks would be much better. "
    author: "Luca Beltrame"
  - subject: "Re: Alpha 3?"
    date: 2007-10-18
    body: "There is not much to detail.\n\nBeta 2 and previous (on Feisty Fawn) boot to the desktop, with 'error bubbles' over the taskbar and _nothing_ (except the clock) works.\n\nBeta 2/3 (on Gutsy Gibson) wont even boot to the desktop.\n\nNot much to give a detailed report on.  Its pretty much the same for everyone (as far as I can tell) on the Kubuntu forums.  A simple browsing there would give you any info you wanted.\n\nAgain, I think, and hope, that its the fault of those who build the Kubuntu packages, and that KDE4 really isnt that messed up.  I love KDE, and want nothing more then to use the latest and greatest."
    author: "Leto Atreides"
  - subject: "Re: Alpha 3?"
    date: 2007-10-18
    body: "Kubuntu KDE 4.0 beta 2 packages were broken and incomplete (some modules were still beta 1). For beta 3 you need install also kde4base-dev, in yesterday announcement this information was missing."
    author: "Poborskiii"
  - subject: "Re: Alpha 3?"
    date: 2007-10-18
    body: "'dev' packages only contain files needed to manually compile applications against the libs in question.\n\nThere is no reason that kde4 binary packages would require this.\n\nThe comment on the package even says:\n\n\"This package contains all the headers and other related files needed to compile KDE 4 applications, or develop applications which use the core KDE 4 base applications.\n\nThis KDE 4 package is for development only, it offers no binary compatibility guarantee and will no help users.\""
    author: "Leto Atreides"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "\"There is no reason that kde4 binary packages would require this.\"\n\nThe packaging is a little broken, there's your reason.\n"
    author: "Reply"
  - subject: "Re: Alpha 3?"
    date: 2007-10-22
    body: "this might be kubuntu fault because i compiled latest svn from 4 days ago on slackware and everything works and is so stable, just don't expect it to be feature complete at this point"
    author: "diego"
  - subject: "Re: Alpha 3?"
    date: 2007-10-18
    body: "I've been testing from SVN every so often, and I find that \"what works\" on any given day is immensely variable.  Case in point - I can't interact with the desktop in any way at the moment, but the taskbar works fine.  A while ago, it was the reverse.  So all the pieces are there and working, but Plasma is in too much of a state of flux (and Aaron is, unfortunately, required to perform so many other non-Plasma related tasks) for it to be predictable which pieces are working on any given day ;) When it is closer to feature-completeness, the devs will hopefully be able to take the time to make it Work (since all the pieces seem to be in mostly goof working order, this could well take much less time than you'd expect.)\n\nIn the meantime, I highly recommend building every few days from SVN (not on Mondays, though).  Instructions are here:\n\nhttp://techbase.kde.org/Getting_Started/Build/KDE4\n\n"
    author: "Anon"
  - subject: "Re: Alpha 3?"
    date: 2007-10-18
    body: "I tried both Kubuntu's and Opensuse's packages for KDE4 and it seems that OpenSuse is a step forward. Kubuntu's packages seem pretty broken to me. On the other hand, with OpenSuse's packs I was able to try out plasma and its plasmoids almost perfectly, dolphin, konsole, marble and some other apps worked well too, even amarok 2.0 I was able to try out. Of course, sometimes some packages are broken and things \"stop\" working, but it is just a matter of packages that are being updated. As a suggestion, I think the best way is to compile from SVN weekly (on Tuesday) if you want a real state of the working in progress.  "
    author: "Somebody"
  - subject: "Re: Alpha 3?"
    date: 2007-10-18
    body: "I switched from Gentoo/Sabayon because I didnt want to compile major packages for 24 hours.  Not going to do so because the Kubuntu guys broke KDE4."
    author: "Leto Atreides"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "> _nothing_ except the clock works\n\nevidently neither you nor the kubuntu packagers had/have an understanding of how the code is being managed for the desktop then.\n\nfeatures are developed in branches and then merged in. trunk/ has and will remain a place to aggregate changes and work on the core lib in ways that aren't overly disruptive.\n\ni get enough crap from applet and engine writers about shifting targets that i really don't care if you only get a clock in betaX.\n\nbtw, in beta3 there was a lot more than that available. sounds like the kubuntu packages are simply a bit stuffed."
    author: "Aaron J. Seigo"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "When I say only the clock I mean _only_ the clock.  Task bar doesnt work, no start/K button, no applications will run, only the widget in the top left corner will activate, and since it only has a clock applet, that makes it the only thing that runs.  Period.\n\nIt has nothing to do with my understanding of how code is being managed.\n\nAs I have freely admitted, my experience is based solely on precompiled packages in Kubuntu."
    author: "Leto Atreides"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "repeating what you said doesn't make it more accurate, it just repeats it. ;)\n\nlet me try and be a bit more specific now that i've had a couple hours of rest and therefore have a bit more energy (i just flew for 11 hours + aiport time + taxi rides today ... and adjusting to a -8hr tz change. meh.)\n\nthe reason that only the clock worked for you is that they apparently only packaged the clock. the rest of the goodies were elsewhere, though hardly hidden. they are being moved into kdebase and extragear/plasma as they become ready.\n\nto kvetch about that isn't very interesting because, well, that's what you get if you use packages from beta2 or earlier that only included kdebase. even in beta3 the menu isn't fully integrated yet; it's being finished up in playground/ first.\n\nthere are good reasons for doing it like this (my sanity and the ability for people to freely experiment among them) and i'm not particularly taken by the heartstrings people are plucking here. there are lots of things to test and bump around with in these betas. stop fixating on plasma for the moment; you'll get to play with more of its features as more releases come. in the meantime, we're working in a way that allows us to move forward as quickly as possible even if it doesn't give you instant gratification right this second.\n\nthere is exactly one release that counts for plasma, and that'll be 4.0, though the rc's leading up to it will be important as well. there is also exactly one canonical place to gauge the \"workingness\" (hm. neat word.) of things right now and that's svn.\n\nand of course if the kubuntu packages are screwed up, then there's even less that you or i can do about things. =)\n\ni expect kde4 to be an interesting journey for packagers as it is rather different in several key ways from previous releases. it's a net improvement, but it means changing some things that packagers have probably been doing for *years* now. they'll get it figured out with time though..."
    author: "Aaron J. Seigo"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "I just built beta3 from source and the menu and window list are indeed not working, so it's not Kubuntu's fault.\n\nIf it's working on openSUSE it's likely it's not actually beta3 but a more recent snapshot."
    author: "Reply"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "I am not talking about lack of Plasma widgets (I didnt really expect it to come with more than a clock), I am talking about the core KDE functionality not working.  (Desktop, Taskbar, Startmenu).  I dont know how much of that is related to plasma in KD4 4, but none if it should be considered optional, or extra.\n\nI am not complaining, I am simply stating my results and the results many others get as well.\n\nIf this is simply the result of bad packaging, then thats what it is and the majority of us wont know until they are properly packaged."
    author: "Leto Atreides"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "\"(Desktop, Taskbar, Startmenu)\"\n\nare all part of plasma, are developed in separate branches, will be ready for 4.0 (because they are marked as showstoppers) and are surely not considered to be optional."
    author: "infopipe"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "(Desktop, Taskbar, Startmenu) are all plasma applets."
    author: "Duncan Idaho"
  - subject: "Re: Alpha 3?"
    date: 2007-10-19
    body: "Most reports I've read list the desktop/panels as having largely been broken for some time, because Plasma seems to be the system in the most flux right now.  However, the apps work fine.\n\nI think you are confusing a lack of an ability to see your apps on a taskbar with the ability to run them.\n\nAnd now that the panels/taskbar/Kickoff all work, you can now see and start your apps easier."
    author: "T. J. Brumfield"
  - subject: "Re: Same for OpenSuse"
    date: 2007-10-20
    body: "I have tried beta1 and beta2. Both do not show a useable desktop. The only app that works is kicker3 (from KDE 3.5). Sad but true Kicker3 is very useful to stop KDE4.\nI will try Beta3 for OpenSuse when it is available.\n\nBye Joe  "
    author: "Joe"
  - subject: "Re: Same for OpenSuse"
    date: 2007-10-21
    body: "It's available now for openSUSE. Currently you have to search a it for working packages, I was astonished to find the latest KDE packages without unfulflilled dependancies in the \"Education:\" repository?!?\n\nUnfortunately, at least for me, I still haven't something like a working taskbar. First step therefore still is to start a konsole and type \"kicker\" ..."
    author: "Peter Thompson"
  - subject: "Re: Same for OpenSuse"
    date: 2007-10-23
    body: "No i have tried Beta3 for OpenSuse. This time Kde4 does not start at all. I only get a message box (\"kdeinit could not be started\")."
    author: "Joe"
  - subject: "\"Cicker\" made my day :-)"
    date: 2007-10-18
    body: "One again you created a very qt code name. \"Cicker\" just rocks.\n\na) Kicker is gone thus some last tribut to Kicker.\nb) Cicker spelled German with a Z perfectly matches some of the people complaining about KDE 4. A \"Zicke\" in German is a drama queen/bitch. Sometimes you jokingly refer to a male Zicke as \"Zicker\". ;-)\n\nCheers."
    author: "Arnomane"
  - subject: "Re: \"Cicker\" made my day :-)"
    date: 2007-10-18
    body: "I like that one (Zicker), it's really good :-)\nI would like to say a very BIG thanks to the KDE team for it's dedication and hard work over the years and for creating one of the most beautiful, flexible and functional desktops on this planet.\nKDE is a master piece no matter what all those critics might say and KDE4 will be the crowning.\nI am testing it on openSuse 10.3 and yes it's a beta but I can start a lot of applications and they are working quite well. I just can't wait to see the final release.\n\nRespect and keep up the good work guys.\n\n"
    author: "Bobby"
  - subject: "kde 3.94 doesn\u00b4t start on Kubuntu gutsy"
    date: 2007-10-18
    body: "Yesterday i installed kde 4 beta 3 under my kubuntu 7.10 after restarting the X and change the session, kde start but can not complete the full cicle, on the loading kde restart and back into login. Any one knows what should be happening ??"
    author: "Marcos Frederico Mollica"
  - subject: "Re: kde 3.94 doesn\u00b4t start on Kubuntu gutsy"
    date: 2007-10-18
    body: "You need install kde4base-dev. In yesterday Kubuntu.org announcement this information was missing, but now is announcement complete."
    author: "Poborskiii"
  - subject: "Re: kde 3.94 doesn\u00b4t start on Kubuntu gutsy"
    date: 2007-10-18
    body: "Yes : You are using beta software in a beta distribution.\nThat simple."
    author: "kollum"
  - subject: "Re: kde 3.94 doesn\u00b4t start on Kubuntu gutsy"
    date: 2007-10-18
    body: "err, gutsy is rc1 now.\nJust my 2 cts !"
    author: "Aldoo"
  - subject: "Re: kde 3.94 doesn\u00b4t start on Kubuntu gutsy"
    date: 2007-10-18
    body: "Oops, it has actually been released today !"
    author: "Aldoo"
  - subject: "LiveCD?"
    date: 2007-10-18
    body: "Is there a LiveCD for beta 3 yet? At least at http://home.kde.org/~binner/kde-four-live/ I can not find one yet. Would be nice.\n\nAnyway - great work KDE-team!"
    author: "Markus"
  - subject: "Re: LiveCD?"
    date: 2007-10-21
    body: "It's available now."
    author: "binner"
  - subject: "Re: LiveCD?"
    date: 2007-10-21
    body: "Thank you! Could someone please post the easy way how to try this on a running system? I vaguely remember being able to do so with qemu, bit I don't remember the exact command."
    author: "ac"
  - subject: "Re: LiveCD?"
    date: 2007-10-22
    body: "qemu -m 512 -cdrom foobar.iso -boot d"
    author: "Anonymous"
  - subject: "it sucks..."
    date: 2007-10-18
    body: "...ass"
    author: "bummer"
  - subject: "Re: it sucks..."
    date: 2007-10-18
    body: "-like you!"
    author: "you're a bummer"
  - subject: "Re: it sucks..."
    date: 2007-10-18
    body: "Woah, really? Does it do the laundry too?"
    author: "Hans"
  - subject: "Re: it sucks..."
    date: 2007-10-21
    body: "LOL"
    author: "A KDE Advocate"
  - subject: "1 FPM"
    date: 2007-10-18
    body: "Well, I installed the Beta 3 packages from the KDE4 openSUSE build service repository on my old T30 Thinkpad with a Radeon 9250 (?) on it and the first thing I noticed when trying to log into the desktop is that each drawing operation takes literally minutes. I have to wait about 5 minutes until the desktop is drawn. Trying to interact with the GUI is not possible at all since it reacts (visually) only after a couple of minutes and even then only in slow motion. The system is still responsive though when it comes to mouse movement and switching consoles so it doesn't look like it's a CPU bottleneck.\n\nThat is something that actually never happened before so I would like to ask if this is a bug with the packages or some new default in KDE4 (like composite support) which brings my system to a halt?"
    author: "Erunno"
  - subject: "Re: 1 FPM"
    date: 2007-10-19
    body: "NEI. (not enough information)\n\nwhat GUI are you trying to interact with. what is your hardware set up (particularly video card and driver), etc.. you're being way too vague to usefully comment on your issue."
    author: "Aaron J. Seigo"
  - subject: "Re: 1 FPM"
    date: 2007-10-19
    body: "My guess would be your card barely supports composite, and it's enabled, and KWin (which I think has Composite enabled by default currently) is going incredibly slow because mesa is probably doing all the rendering on the CPU instead of the GPU. (note, this is merely a guess based on my own experience with my Radeon card and opengl applications)"
    author: "Sutoka"
  - subject: "Re: 1 FPM"
    date: 2007-10-19
    body: "I get the exactly same thing when I enable composited kwin in 3.5.x on my Radeon 9600. Just disable Compositing in your xorg.conf, and all will be fine :). "
    author: "shiny"
  - subject: "Re: 1 FPM"
    date: 2007-10-19
    body: "I have the same problem on SuSE 10.3. I had disabled the composit but it is not better...\nI have the same Graka radeon 9250 that uses the xorg radeon driver with aiglx... \nkernelmodule is radeonthat loads the R200...\n\nIn kde3.5.7 composit with compiz works very good, ther no problems...\n\nattached .xsession-errors"
    author: "andreas"
  - subject: "Re: 1 FPM"
    date: 2007-10-20
    body: "It is definitely related to composite.  On my machine with XGL enabled, I get the behaviour you describe...even though compiz works fine.  On my machine with XGL disabled, the KDE4 beta3 works.  By the way, I'm using Opensuse also, and I've had this problem with all prevous betas that I have tried."
    author: "Henry S."
  - subject: "Great achievement! But performance problems..."
    date: 2007-10-19
    body: "I would first like the developers for all their efforts. This beta is actually very usable, and it's all starting to look really nice. And personally, I think oxygen is breathtaking. I have no doubt KDE4 will be really great.\n\nStill, one thing I noticed while trying the beta on kubuntu is that the interface feels less snappy then 3.5. After a click on a menu it sometimes takes about a second until it opens. Also, dragging plasma applets around cost a lot of cpu, and the applet moves not so smooth (it takess about a second before the applets appears at the new position). I have a not so state of the art but still decent athlon xp 2500+ with 512 MB ram. Is this performance something that will probably be fixed while polishing KDE 4, or does this new version really require some up to date hardware?"
    author: "pinda"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "KDE4 will be clearly slower than KDE3, especially the resizing.That's unfortunately the cost we have to pay for the double buffering (painting everything first on a pixmap) but at least with Qt 4.4 we will see the advantage. Beside that, have you tried to disable the effects of kwin or a Qt version bigger or smaller than 4.3.2. The latest Qt release seems to be buggy which makes it three times slower as 4.3.1 :(\n"
    author: "anon"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "> KDE4 will be clearly slower than KDE3\n\ni love blanket statements. ;)\n\nplease be careful with these kinds of sentences. there are plenty of things that are faster in kde4. there will certainly be regressions, but we're not pulling a vista here."
    author: "Aaron J. Seigo"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "Ok, sorry, it was really to general. It applies only to Qt4's double buffered way of widget painting what will make KDE4 slower. Qt4's painting itself compared to Qt3 is actually faster. Many people doesn't know about the double buffering and except KDE4 to get faster, like many comments show. I'm afraid they will think KDE is badly coded, like I thought about the fresh gtk2 at that time.\n"
    author: "anon"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "> I'm afraid they will think KDE is badly coded\n\nIn that case don't make general statements that KDE 4 will be slower. ;-) You'll be creating a perception or myth that's hard to fix later.\n\nStuff like double buffering can be disabled per widget, but it's not desirable as it's one of the things needed to fix flicking widgets. In the Qt3 book, double buffering was actually pointed out as a way to improve performance in some cases too. :-p\n\nStep 1 will be to get a workable desktop out. Step 2 will be fine tuning, apply more usability idea's and optimizing the thing. So don't worry yet!"
    author: "Diederik van der Boor"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-20
    body: "\"Stuff like double buffering can be disabled per widget, but it's not desirable as it's one of the things needed to fix flicking widgets.\"\n\nWouldn't using Qt 4.4 do this without the double buffering?  (google 'qt aliens')"
    author: "MamiyaOtaru"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "\"It applies only to Qt4's double buffered way of widget painting what will make KDE4 slower\"\n\nAnd, worst of all, *much* more memory-hungry - buffering an entire window takes a lot of RAM (on a modestly-sized 1200x900 32-bit colour screen, a maximised Window will take up 4.5MB - effectively doubling the memory footprint of, say, a KWrite instance!) God knows how this will work with Compositing, which takes yet *another* pixmap of each window.  It's a real shame, as KDE has always been accused of bloat even while its memory footprint is basically the same as GNOME's, but now the accusation is justified.  What's even worse is that the Trolltech devs have been proclaiming Qt4 as much more memory efficient than Qt3, when it's clearly nothing of the sort!"
    author: "Anon"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "Did you do any *real* memory tests, or did you just make that up? Please show us real numbers, using good memory management tools! (which isn't `top` or `free`)."
    author: "Diederik van der Boor"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "I'm using QT 4.3.2.\nWill QT 4.4 be faster? (I assume 4.4 will be used in KDE 4.1?)\n\nI was wondering if it isn't possible to disable the double buffering in QT for people using lower end hardware."
    author: "pinda"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "Based on what the Trolls have said in their blogs, KDE 4.4 should improve in at least some ways (like resizing windows) greatly, because they won't be using 'native' widgets or windows (can't remember which it was called). Since the Trolls seem to care greatly about performance, and KDE is probably the largest user of Qt so they'll probably be able to find LOTS of bottlenecks after watching how KDE 4.0 performs."
    author: "Sutoka"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "which applets are taking a while to drag? all of them? if so, something is heinously broken there, and not something we're seeing on the devel side.\n\nas for your processor and memory, that should be fine. what video card do you have and what driver are you using?\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "I tested it with today's svn checkout and saw that mouse enter / leave and press events are very memory (temp. ca. 50 MB usage) and cpu intensive which are resulting that the applets don't move for some seconds. The digital clock is the only applet where enter and leave events doesn't show this behavior. If an applet gets painted between two screens in a xinerama layout it will get stuck for some time, but they will work on both screens. Beside this issues it looks really good :)\n"
    author: "anon"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "Thanks you for the answers. I didn't know about the double buffering. Interesting to know. And yes, I did notice a higher memory consumption then 3.5.\n\nI tested it with the photo frame (which was displaying a slideshow, nice feature), and the analog clock. They both had that problem, but the photo frame had clearly a larger lag then the analog clock.\n\nI have a Geforce 2 (64 MB) and am using the proprietary nvidia legacy drivers (1.0.7185). You think my old video card is the bottleneck? In that case I think I'll buy a new card when KDE4 arrives. If you think this is caused by a bug, please say so, and I will enter a bug report."
    author: "pinda"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "Good to know this.\n\nMy duron 1.6Ghz with 512 RAM is showing it's age and KDE isn't helping (even more that my girlfriend uses another KDE session).\nYou know, KDE is very fat in 3.X for me, and I was hoping for the less CPU/RAM promisses qt4 brought, but looks like KDE4 won't like for the less resources usages expectations.\n\nI love KDE, but each day I'm looking more and more for gnome because it runs much faster (as a DE, I hate the apps itselfs)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "The latest version of KDE4 is also really slow on my system:\n\nPentium D805@ 3,9GHz\nLeadtek Nvidia 8800GTS 640MB\n2GB DDR2 667MHz\n\nit takes at least 5 minutes to start kde itself, plasma reacts after 1min when you click on something, the apps also start really slow.\n\nI've got KDE4 from the opensuse 10.3 KDE4 repo (software.opensuse.org/download/KDE:/KDE4, checkout from the 19.October)\n\nThe latest nvidia driver is installed and running. I've deleted the whole .kde4 folder from my home to start KDE4 with completely new settings (since last update). I've tried this before and after removing the compositing option. It doesn't change anything. It is still unbelievable slow. This slowdon is just since the version from the 19. October (the last one was from the 17th and it was fast)\n\nTell me if you need any files and logs and where to get them\n\nI hope I can help you!\n\nWith friendly greetings\n\nBernhard"
    author: "LordBernhard"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "I've just found out that the slowdown is definately caused by kwin. I've crashed kwin by activating the 3D effects and now KDE4 is as fast as ever"
    author: "LordBernhard"
  - subject: "Re: Great achievement! But performance problems..."
    date: 2007-10-19
    body: "it's caused by some options in the xorg.conf (maybe AddARBGLXVisuals or AllowGLXWithComposite):\n\nhere the two xorg.confs i've tried:\n\n1 (slow): http://mustermaxi.googlepages.com/xorg.conf.saxsave\n2 (normal): http://mustermaxi.googlepages.com/xorg.conf"
    author: "LordBernhard"
  - subject: "Solution for ATI \"radeon\" driver users"
    date: 2007-10-20
    body: "I experienced performance problems, too. If you are using an ATI card in conjunction with the \"radeon\" driver, please enable EXA-Acceleration in /etx/X11/xorg.conf! Example:\n\n========================================\n\nSection \"Device\"\n  BoardName    \"Radeon X600 Pro\"\n  Driver       \"radeon\"\n  Identifier   \"Device[0]\"\n  Option       \"AccelMethod\" \"EXA\"\nEndSection"
    author: "Stephanw"
  - subject: "Plasma on KDesktop Locked Screen (Screensaver)"
    date: 2007-10-19
    body: "I'd like plasma to be put in the kdesktop locked screen-- not by default, but as an option.  If its something that's possible, I'd personally take the time to implement it and distribute as a patch.  Here's my ideas:\n\n1. When screen is locked, a plasma window would appear allowing you access to some plasmids.\n\n2. There would be a kconfig module to activate it and add applets.  The kconfig module would embed a simulation of the locked screen with all the plasmids in it, like amarok's plasma window.\n\n3. The kconfig module would allow you to add and remove applets, but the locked screen would not.\n\n4. While most plasmids are benign, like the clock, the user would be advised not to include potentially compromising plasmids like the file browser.\n\n5. The user would be warned that plasma as a screen locker represents a security risk.  I'll do some field testing to see how safe it is, but for now its best we assume that its not very safe.\n\nI want to do this because there are a lot of cool things that can be done with this. For example, gnome* recently added a feature that allows people to leave eachother notes when the screen is locked.  A simple clock would be nice to have, and having a media control applet without logging in is infinitely useful.  I'm sure there are many uses of this, Security concerns notwithstanding \n\n*No, this isn't a \"gnome has it, why doesn't kde?\" I genuinely think this is a good idea."
    author: "Level 1"
  - subject: "Re: Plasma on KDesktop Locked Screen (Screensaver)"
    date: 2007-10-19
    body: "That would be a really nice thing to have!"
    author: "Jasper Boot"
  - subject: "Re: Plasma on KDesktop Locked Screen (Screensaver)"
    date: 2007-10-20
    body: "Ditto.  Sounds great to be able to leave a memo note in a plasmoid on a locked screen for when the user returns or to be able to glance at the clock, etc."
    author: "The Vicar"
  - subject: "Re: Plasma on KDesktop Locked Screen (Screensaver)"
    date: 2007-10-19
    body: "cool idea; and should be pretty easy to do given that krunner already links against libplasma.\n\ni don't really like the idea of a control panel to manage it though. it would probably be enough to have a \"Add Widgets\" button or a version of the toolbox in the corner that would prompt for the password before letting the person change things. much more natural.\n\nwe'd first want a non-dialog version of the applet browser ... which in turn means getting itemviews on the canvas. those are being worked on in Qt, but aren't available quite yet."
    author: "Aaron J. Seigo"
  - subject: "Re: Plasma on KDesktop Locked Screen (Screensaver)"
    date: 2007-10-19
    body: "Well, I'm new to kde development, but my idea seems much simplier from a development standpoint.  To me, your idea is a little awkward, but that's just my opinion.  Anyway, I'm glad to hear to a developer is interested in my idea!  I read your blog, btw.  I'm compiling the kde development environment now, so hopefully I can get started on some of my ideas soon!"
    author: "Level 1"
  - subject: "Re: Plasma on KDesktop Locked Screen (Screensaver)"
    date: 2007-10-21
    body: "Amarok already has a QGV itemview. :)"
    author: "Ian Monroe"
  - subject: "How things are developping..."
    date: 2007-10-19
    body: "...From a user point of view. The apps are great. They are, really. The new games are wicked cool. The new kate (those days that it works) is (will be) excellent. Konsole has a split-screen mode, this, alone represents a huge amount of coolness.\n\n   Dolphin is really cool. I mean, even with the debug, it is _fast_. And the categories are one of the coolest thing ever seen in a file manager (the coolest is still the ioslaves).\n\n   And kwin. A _real_ WM, that works really well as a WM with all the eyecandy of beryl (well, nearly, but that is in fact way less important than the basics of a WM). Yay shadows!\n\n   There are, however two problems. The first is really not KDE's fault, and it is integration with the distros. Because of such things as launching dbus, the default .directory and stuff, running alongside KDE3 is not immediately straightforward. This is unfortunate.\n\n   The other problem is that for most people, KDE _is_ the desktop and panel. not the apps, not the API. And, well, though plasma is growing in coolness and power at an incredible pace, it is not yet usable. I have no doubt it will be for the final release (go Aaron!), but, well, it takes time.\n\n   And this is unfortunate, because it hides all the working goodness to be found in this beta (first impressions, etc.)\n\n   Did I mention the Oxygen style looks good (though tabs need more contrast)?"
    author: "hmmm"
  - subject: "Re: How things are developping..."
    date: 2007-10-19
    body: "Fully agree here. :-)\n\nThe apps area really brilliant. Dolphin, Gwenview, Oxygen icons, the svg games, konsole, cleaned up toolbars. They all make me really happy.\n\n\n> And this is unfortunate, because it hides all the working goodness to be found in this beta (first impressions, etc.)\n\nGrin, I think the KDE teams are quite aware of this :-)\n\nhttp://akademy.kde.org/conference/talks/46.php\nhttp://home.kde.org/~akademy07/videos/1-10-Webkit_and_KDE---also_Beautiful_Features.ogg"
    author: "Diederik van der Boor"
  - subject: "Kaffeine?"
    date: 2007-10-19
    body: "Does anybody know if kaffeine will be part of 4.0? I haven't heard anything about kaffeine being ported. Or do I have to wait until 4.1? Kaffeine is still my favourite video player."
    author: "pinda"
  - subject: "Re: Kaffeine?"
    date: 2007-10-19
    body: "Kaffeine has never been part of KDE per se, and ther are nothing suggesting it will be for 4.0 either. It's a 3rd party application released separatly of the KDE releases. If it get ported to KDE 4, it will be released sometime after KDE 4.0. Depending on it's developers."
    author: "Morty"
  - subject: "Re: Kaffeine?"
    date: 2007-10-19
    body: "Thanks for the answer, I wasn't aware Kaffeine isn't part of KDE.\nDo you also know if phonon is ready to be used by video applications, so Kaffeine can start using this?"
    author: "pinda"
  - subject: "Re: Kaffeine?"
    date: 2007-10-19
    body: "It should be, Codeine(or Video Player as it's renamed to) are supposedly already ported to Phonon. And according to this http://planet-soc.com/node/514, it was helped along by looking at the Kaffeine4 code :-) "
    author: "Morty"
  - subject: "Re: Kaffeine?"
    date: 2007-10-21
    body: "Kaffeine is the application driving the development of Phonon's video support. So yea, expect it to be out."
    author: "Ian Monroe"
  - subject: "Re: Kaffeine?"
    date: 2007-10-23
    body: "FWIW, I tried SVN of KPlayer a couple of weeks ago, and it is already in pretty good shape as a KDE 4 app. All the usual stuff is there, except there is of course still a couple of bugs here and there.\n\nI personally always preferred KPlayer over Kaffeine, so that is great news to me!"
    author: "aha"
  - subject: "stupid people"
    date: 2007-10-19
    body: "stupid people download beta, and even alpha software and complain it isn't working.  Duh, if you want software that is more reliable why are you trying beta's.  Don't go complaining that it doesn't work, that it is slow, buggy, etc, because that is what beta software is like.  \n"
    author: "Richard"
  - subject: "Re: stupid people"
    date: 2007-10-19
    body: "Beta tests by definition are designed to search for bugs, so beta software is often buggy.  However, beta software in theory has already passed alpha testing, and is also largely feature complete.\n\nI think the problem is that KDE is a large project consisting of a desktop, window manager, apps, and even fairly low-level hardware support.\n\nThe core libraries, and many apps seem to be fairly feature complete for the 4.0 roadmap.  The desktop itself is not.\n\nIs KDE on the whole in poor condition?  No.\nIs the entire KDE 4.0 branch in beta quality?  No, because the desktop isn't.\n\nI think the solution is to be clear in terms.  If you require a stable desktop, then KDE 4 testing may not be ready just yet, though it seems close."
    author: "T. J. Brumfield"
  - subject: "\"Goal and mandate\""
    date: 2007-10-19
    body: "I hate to go here again, but I've never really got answer, only flames, so let me dare ask again.\n\nPlasma's website claimed the desktop hasn't really changed in design and concept in decades.  It promised a revolution, and on kde-look.org I saw a variety of mockups and concepts on how to shake up the desktop paradigm and do some radical things to hopefully increase productivity.\n\nI know Plasma is barely more than a fetus at this point, and it doesn't even fully replicate all the features of the old desktop.  The only new features that I know of are SVG scaling (admittedly a huge plus), built in composite effects (replicating what you could get from Compiz), and built in widgets (what you could get from Karamba).\n\nThe built in composite is an advantage in that it is built into KWin, and you don't have to lose the power of KWin to use another window manager.  Admittedly that is nice, but I can't imagine that the new and improved KWin can do everything that Compiz can at this point either.\n\nI've never liked widgets, so I don't care a whole lot.  I hear there is all kinds of scripting support and it is very easy to make useful widgets.  Great, except they are still widgets.\n\nWhat I really want is to shakeup the desktop paradigm and increase productivity.  I can't imagine widgets are going to be that solution. If you told me that there was already a roadmap and a design plan for how to rethink the desktop, and it is going to take another two years to code it, then so be it.\n\nHowever, I don't think such a design even exists right now.  Shouldn't such a design predate coding on Plasma?  How do you know what system you need if you don't know what you need it to do?\n\nWhere do we move forward with Plasma?  What KDE4 mockups should be emulated or implemented?  How can we rethink the desktop and do something truly innovative?\n\nThe Plasma vision states:\n\n\"It is time that the desktop caught up with modern computing practices and once again made our lives easier and more interesting when sitting in front of the computer. Just like those icons did for people back in 1984.\n\nDevelopment of KDE4 has just begun, and it is during these major release cycles that we have the opportunity to retool and rethink our applications and environment at the fundamental level. The fact that the current desktop concepts have lasted this long is a testament to their effectivity, and we should not simply abandon all sense of the familiar and the useful. Yet we can not stay where we are either.\n\nThis, then, is the goal and mandate of Plasma: to take the desktop as we know it and make it relevant again. Breathtaking beauty, workflow driven design and fresh ideas are key ingredients and this web site is your portal onto its birth.\"\n\nThose aren't my words.  Can anyone say that Plasma today is meeting the GOAL AND MANDATE set forth by the Plasma team?  I'm not demanding immediate results.  I'd just like to see a plan."
    author: "T. J. Brumfield"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-20
    body: "Well stated.  That is exactly my questions as well. Like Brumfield, I don't expect anything radical in 4.0 but how exactly is Plasma going to redefine the desktop in future releases such as 4.1, 4.2 and so on? Are design plans available?"
    author: "The Vicar"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-20
    body: "wait, simply wait.\nIt's not all visible yet (some parts will never be visible anyway, as it's mostly stuff \"under the hood\"). Plasma is not going to throw away every bit of known \"usage habits\" we have with our desktops atm. It'll add some nice yet mostly unknown features (you'll be surprised ;-), but what counts more is imo a shift in terms of flexibility and an API which for the first time gets close to putting together a working environment like using \"LEGO bricks\". It's so damn straightforward, you'd be stunned. Once you have taken a look what's inside Plasma your mind starts to juggle with all the different combinations and possibilities to put the different pieces together (in the long run this might eventually really change our way of using Computers as Plasma is like paving the road, the actual walking down that road has still to be left to the users)\n"
    author: "Thomas"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-20
    body: "Again, I said if you told me it would take two years, I'll happily wait.\n\nHowever I don't believe there is even a plan or design.\n\nIt was the stated goal and mandate of the team to redesign how we use the desktop.  Several suggestions were made, and KDE 4 has been in planning for a long time, but I don't believe a proper plan/roadmap for Plasma was ever developed.  What we ended up getting is a new implementation largely of existing concepts.\n\nIf I'm wrong, then please educate me.  How will scriptable widgets really completely alter how I use my computer?  I've tried varying forms of widgets since the old Win95 app called Corkboard, and they've never really helped productivity.  In the end, they just waste resources and clutter my desktop."
    author: "T. J. Brumfield"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-21
    body: "I'll try it this way: \nThe current solutions are reaching the boundaries due to the underlying design of nowadays desktops.\n\nPlasma is not only about, it actually _is_ a new desgin, stretching the boundaries of \"doable\" vs. \"not doable\"/\"not doable without ugly hacks\" beyond the limits we have atm.\n\nTake e.g. the new LED-light-bulbs. At first they resemble nowadays usage of light. But in the long run, this new technique will open the way to completely new and exciting possibilities which have simply not been doable before (due to the limitations of the underlying 100 year-old design of light-bulbs)\n\nWell, you can try not to get excited about it (be it Plasma or LEDs) and yawn with ostentation every time you meet somebody who is excited... but that's your choice."
    author: "Thomas"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-20
    body: "You say that you hate widgets; sure, I don't like putting lots of Superkaramba widgets on my desktop either, but this is not only what Plasma is about.\n\nTaskbars, system trays, pagers, clocks... all those things will become what you call widgets with Plasma. You'll be able to put them on the desktop like Karamba widgets, as expected, but also in panels etc. thus replacing Kicker.\n\nYes, Plasma is not only about the standard \"desktop\" where you currently place icons and karamba widget.\n\nIf you like Mac OS X' \"Dashboard\", you'll be able to do the same in KDE4 (note: it doesn't have to mean \"KDE 4.0). You heard right - you can show the desktop over the windows.\n\nYou also asked about which mockups will become reality. For example, this mockup (http://kde-look.org/content/show.php/Tasks+Info+in+Less+Windows+%28mockup%29?content=33673) is already, as far as I know, already implanted; not we just need a nice plasmoid version.\n\nI have faith in the Plasma developers (and the rest of the KDE team, of course) and follows the development in SVN and on the dot/planet. I've written everything here as a regular user, so no guarantee that everything is correct.\n\nAbout 'KDE4 being a revolution', I suggest you to read Aaron's post here: http://aseigo.blogspot.com/2007/08/on-success-of-kde4.html"
    author: "Hans"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-20
    body: "So the revolution of how we use the desktop is to replace a system tray with a new system tray, a clock with a new clock, etc?\n\nExcept the old system worked, and the new system doesn't yet.\n\nHow does any of this revolutionize how we use the desktop?  Can anyone even point to the plan and design for how this revolution will take place?"
    author: "T. J. Brumfield"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-20
    body: "Right now, as others said, just wait. I've already said it to you: your observations are without \"bad\" intentions but your insistence may just have a bad effect on the developers *right now*, since a lot of other people are bashing them for no reason."
    author: "Luca Beltrame"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-21
    body: "> So the revolution of how we use the desktop is to replace a system tray with a new system tray, a clock with a new clock, etc?\n \nNo, compare the situations. Old style desktop elements, like the desktop (background, icons), panel, and probably widgets are all separate things.\n\nPlasma replaces them all together, which means you can e.g. choose where to put a certain data monitor and move it later on.\n\nSay you have a battery monitor on your panel, but then you are working on something important and you are close to running out of power, you might want to have a more detailed display in an \"always-on-top\" style window.\n\nIn an separated programs situation, you would have to check if the is a stand-alone battery monitoring application and basically run it additionally to the panel applet.\n\nIn a Plasma situation you can just drag the applet from the panel itself becomes more detailed.\n\nIt's mainly a matter of not needing separate pieces of software for basically the same task just because at one time you give it more attention and at another time you just put it aside."
    author: "Kevin Krammer"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-21
    body: "so for example, would it be possible to move (or link) my email folders, contacts, files and notes regarding one of my tasks together in to one \"plasma directory\" on panel?\n"
    author: "mdl"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-22
    body: "Well, since we will have the possibility of \"linking\" data items together based on semantic relations, I imagine that visualising a dynamic set of matching items based on such relations should be doable as well."
    author: "Kevin Krammer"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-22
    body: "Don't confuse the Semantic Desktop project with Plasma.  They were different teams with different goals, and now you are crediting Plasma with the work done by the Nepomuk team.\n\nAgain, I never demanded anything of KDE, because you can't demand anything from a free, volunteer-driven project.  However, again, it was in the words of the Plasma team that they were delivering a revolution to change we use the desktop.\n\nAll I'm asking, is if years later they have even started to design and plan such a thing."
    author: "T. J. Brumfield"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-22
    body: "> They were different teams with different goals, and now you are crediting Plasma with the work done by the Nepomuk team.\n\nNo.\n\nI am answering a question regarding the possibility of having related items accessible through one panel item.\n\nSince building of relations is something Nepomuk is good at, any solution for displaying it on panel will most likely use it.\n\nJust because a solution means incorporating functionality from more than one parts of the KDE framework does not invalidate the achievement of any of the pieces."
    author: "Kevin Krammer"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-23
    body: "I'm getting a bit worried if I hear that marketing speech about \"revolution on the desktop\". Probably this is because I'm getting too old for radical new concepts, maybe ;) ?\n\nBut basically, the desktop paradigm for using a computer, be it Mac, Windows, Unix-Desktops and so on, is nowadays common knowledge. Menus, windows, startmenu, taskbar, trash, this aspects are always there. \n\nEven Apple, usually heralded as the benchmark concerning usablity, has never revolutionized this concept since the first Mac, only evolved it. To be honest, this often concerns more eye candy (which isn't, as fas as I'm concerned, bad!).\n\nBut to say \"revolution on the desktop\" is probably pure marketing speech (which basically isn't bad, too, as it is necessary nowadays, but you also have to live up to the expectations you generate).\n\nAs far as I see it, \"Plasma\" isn't really a radically new concept of using a computer, but a more flexible, more appealing implementation of the well-known \"desktop\"-concept (that is, if it works some day). \n\nAs far as I'm concerned, that's a realistic and valuable goal. I don't know if I'd like a completely revolutionary new desktop concept replacing the one I, and millions of other users, are used to."
    author: "Peter Thompson"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-23
    body: "well said.\n\nPlasma is not reinventing the \"desktop\"-wheel. With Plasma as the basis the plasmoids will \"mimic\" the common components like Taskbar, Menus, Trash....\n\nThe advantage is that with the Plasma concept you can do much more than just \"mimic\" the old components, yet still nobody knows where exactly this may lead us.\n\nFor me, the \"revolutionionary\" part is in the Plasma libraries making the future \"evolution\" of the desktop a lot easier."
    author: "Thomas"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-23
    body: "Save for the fact that the Plasma team in their words promised just such a revolution.\n\nThey said the revolution would be on par with the advent of icons 20 years ago.\n\nAnd you can say that no one innovates, and that it is impossible to change the desktop these days, but there were several nifty KDE 4 mock-ups that looked promising."
    author: "T. J. Brumfield"
  - subject: "Re: \"Goal and mandate\""
    date: 2007-10-23
    body: "\"Save for the fact that the Plasma team in their words promised just such a revolution.\n\nThey said the revolution would be on par with the advent of icons 20 years ago.\"\n\n*Marketing speech*, as Peter said.  \n\n\"And you can say that no one innovates, and that it is impossible to change the desktop these days, but there were several nifty KDE 4 mock-ups that looked promising.\"\n\nIf you're really desperate to hear something good about Plasma: Plasma should make implementing those mockups far, far easier than in KDE3."
    author: "Anon"
  - subject: "Kamion & Galmuri"
    date: 2007-10-20
    body: "I know both apps are being made for KDE 4. Is there any real difference between them? From what I heard, they both do backup?"
    author: "Jason Adams"
  - subject: "emacs and vi"
    date: 2007-10-22
    body: "I know both apps are being made for GNU/Linux.  Is there any real difference between them?  From what I heard, they both do text editing? ;-)"
    author: "Adrian Baugh"
  - subject: "Where does one ask for help?"
    date: 2007-10-20
    body: "I'm trying to compile qt-copy from SVN but I have some compile troubles, where do I look for to solve them? (I have googled for them already without much success."
    author: "Luca Beltrame"
  - subject: "Re: Where does one ask for help?"
    date: 2007-10-20
    body: "Try #kde4-devel on irc.freenode.net :)"
    author: "Anon"
  - subject: "hmm..."
    date: 2007-10-20
    body: "Just installed the third beta on my Kubuntu Gutsy system. It is barely usable.\n\nThere is no taskbar or I could not figure out, how to find it (but it is on the screenshots so there must be one). \n\nOxygen style looks nice, but tabs in the background are not outlined (there is just text) and drawing is sometimes buggy too). apps are crashing frequently, icons are missing almost everywhere (I installed everything). Plasma crashes frequently as well (but that had to be expected, so I won't complain).\n\nThere's nothing wrong with all of that. I still love it and I see the \"big picture\" behind it, but even if I swore to myself not to get disappointed like some others, frustration crawls in. Not because of KDE4.0 being \"bad\" but because of being buggy AND called \"beta3\". I remember using KDE2.0 beta and KDE3.0 beta, they were \"rough\" but usable. KDE4.0 b3 really should have been called \"Alpha 3\". \n\n(And yes: I will file bug reports and I will try to make KDE4.0 better within my (albeit limited) possibilies (donating money and filing bugs is the only thing I can do at the moment). And no: I don't want to discourage any of the developers. I'm just concerned about my beloved desktop environment... ;-) )."
    author: "kubuntu-user"
  - subject: "Re: hmm..."
    date: 2007-10-21
    body: "I second this, but yesterday I decided to build the beast myself from source (using instructions on techbase) and i found it to be much more stable than pre-packaged stuff.\n\nAnyway, go KDE!"
    author: "A KDE Advocate"
  - subject: "OpenSUSE 10.3"
    date: 2007-10-20
    body: "Does anyone have step-by-step instruction how to try this on OpenSUSE 10.3, preferably without borking an insisting installation? Does KDE4 then just show up as an additional session type? Is there any risk of messing with an existing KDE3 setup, like .kde configuration files?"
    author: "ac"
  - subject: "Re: OpenSUSE 10.3"
    date: 2007-10-20
    body: "See this: http://en.opensuse.org/KDE4"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: OpenSUSE 10.3"
    date: 2007-10-28
    body: "The easiest way is to use the openSuse one-click intallation http://software.opensuse.org/search. Install the kde4-meta-base package and then  start a KDE4 Session at login. It's better to use a test user. You can create a test user using YaST. You can also add additional KDE4 packages using the YaST software installation tool.\nHave a lot of fun!"
    author: "Bobby"
  - subject: "Beta 3 working... but..."
    date: 2007-10-21
    body: "With the openSUSE packages labelled 3.94.1 (NOT 3.94.0) I finally got some basic desktop environment working. Looks definitely better than previous betas, the first beta usable for playing around with KDE (a working panel and a task bar and so on...).\n\nWhat I really can't get used to is this plasmoid explorer stuff in the upper right corner (formerly upper left corner). Is there already consensus that this will be standard configuration? Or is this just a beta-speciality? \n\nI consider it very annoying. It gets in the way, and basically it presses functionality on you never needed in everyday desktop usage. Plasma is a great thing, but basically it powers the desktop and isn't an app that is configured all the time. The average user will not explore plasmoids all day (probably he shouldn't even be bothered with this technology, if he doesn't want to).\n\nApart from that, I'm kind of glad that finally there is a KDE4 to play around with. Great!"
    author: "Peter Thompson"
  - subject: "No Compositing w/ nVidia 6200, debian sid/exp'l"
    date: 2007-10-22
    body: "Hi there!\nI've been trying hard to make kwin use Its full compositing powers, but to no avail. Been trying svn, and lately the debian experimental packages on top of a sid install. The hardware is an nVidia 6200 (running nvidia 100.14.19), and the xorg.conf works just fine on a parallel sid install with Compiz fusion.\nHowever, in kwin4, when opening the Desktop Effects tool (you knowaddimean, right-klicking on the title bar->Advanced->bottommost entry), it states \"Compositing is not supported on your system\".\n\nAny ideas what to look at first? Xorg.0.log doesn't yield anything of interest."
    author: "rufusD"
  - subject: "Re: No Compositing w/ nVidia 6200, debian sid/exp'l"
    date: 2007-10-23
    body: "Have you enabled compositing in xorg.conf, Compiz-Fusion works AFAIR with XGL and without compositing (not for me, but IIRC for some others), too^^"
    author: "Lukas Appelhans"
  - subject: "Re: No Compositing w/ nVidia 6200, debian sid/exp'l"
    date: 2007-10-23
    body: "Have you enabled compositing in xorg.conf, Compiz-Fusion works AFAIR with XGL and without compositing (not for me, but IIRC for some others), too^^"
    author: "Lukas Appelhans"
  - subject: "Re: No Compositing w/ nVidia 6200, debian sid/exp'"
    date: 2007-10-24
    body: "I never really understood this XGL/AIGLX/bla stuff, but AFAIK it's got to do with the type of GPU (not the type of compositing window manager) you're using (I may be wrong though). Anyways, the xorg.conf I'm using (the compiz fusion one) has compositing enabled (and this clearly shows in Xorg.0.log)"
    author: "rufusD"
  - subject: "Re: No Compositing w/ nVidia 6200, debian sid/exp'"
    date: 2007-10-28
    body: "I didn't have much luck with that either. I tried to get compositing working but it crashed the desktop. I don't know why because I have Compiz Fusion working fine with my nVidia card and the composite extension in the xorg file is enabled. Could it be that I have to switch from XGL to Xorg in order to get it working? While writing I was just thinking that that might be the problem.\nIn spite of that KDE4 seems to be shaping up very nicely. I have noticed that a few useful widgets have been added including an application launcher and the photo albut is beautiful. \nStartup is quite slow but when the desktop is up and running things seem to be relatively fast and the most application are working very stable.\n\nI just can't wait to see the Desktop of the year 2007 finished."
    author: "Bobby"
  - subject: "Please, get rid of Qt3!"
    date: 2007-10-22
    body: "Right now there's no way of building KDE4 without Qt3Support libraries and headers. I cannot wait till the day when Qt3Support will not be required."
    author: "Artem S. Tashkinov"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-22
    body: "\"Please, get rid of Qt3!\"\n\nWhat the hell do you think the developers are in the process of doing? Jesus, reading dot.kde.org does my head in nowadays.  "
    author: "Anon"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-22
    body: "I remember compiling KDE3 alpha 1 and it didn't require any piece of Qt2.\n\nSo your accusations are not legit."
    author: "Artem S. Tashkinov"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-22
    body: "The qt3support library is not a part of Qt3; it is a part of Qt4."
    author: "Boudewijn Rempt"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-22
    body: "\"So your accusations are not legit.\"\n\n*sigh* - the developers are working on making sure that all apps use Qt4 only, and don't require Qt3Support - keywords being \"working on\" as in *they haven't finished the task yet*.\n\nSo my accusations are perfectly legitimate, thankyou."
    author: "Anon"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-23
    body: "Sorry, but applications like Kile 2.0 are built on Qt3 and that application is a gem. There are many applications that haven't been ported to Qt4, let alone KDE 4.0.\n\nI'd expect them to be porting when KDE 4.1/Qt4.4 is rolling out, but only after KDE 4.1 feature freeze has been announced. At least, that's what I would do if I was developing Kile."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-23
    body: "I dont think he means applications you install after the fact, he means the base KDE4.\n\nWhich, if this were a final release, I could understand his irritation, but its unfounded in a beta because obviously they are still in the proccess of porting the apps over.  And as far and I am concerned, this is a very low priority compared to the rest of the system.  Chances are you will be using some Qt3 apps for many years to come, not every app will be instantly ported, so this isnt much of an issue, as long as its on the todo list, I am fine with that."
    author: "Leto Atreides"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-23
    body: "Actually, while I don't think it's a huge deal, this is the kind of thing that should NOT be in a Beta. The porting is alpha stage. By the time you reach Beta state everything should be pretty much ported and feature complete, the focus being on polishing and bug fixing during this stage. "
    author: "Bxs"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-23
    body: "Actally the whole issue are just nonsens, there are no Qt 3.\n\nThere is qt3support, but that as stated above, is a part of Qt4. It's a support library to help application developers port from Qt 3 to Qt 4. Making the porting of applications like Kile simpler. If some applications use some qt3support functionality, it does not really matter one way or another. It's what that library is for, and as a part of Qt 4 it's stable and supported by TT.\n\nThe KDE libraries does not use this, as they are already ported to Qt 4. In fact the KDE4 libraries are feature complete and frozen, so there are really no good reason too not start porting 3rd party applications to KDE4. \n\n"
    author: "Morty"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-24
    body: "The point is that additional libraries take addition space on your HDD, in your RAM and make your applications start slower.\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-28
    body: "the Qt3support library isn't huge, and the apps which still use it will hopefully be ported once 4.1 comes out. If it's really a huge deal to you, why don't you help porting?"
    author: "jospoortvliet"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-24
    body: "> he means the base KDE4.\n\nRight you are."
    author: "Artem S. Tashkinov"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-24
    body: "grep kde4-libs-beta3 for those who claim that Qt3 (support) is dropped:\n\n(kde3support library is not counted)\n\nkhtml/misc/loader.h:#include <Qt3Support/Q3PtrDict>\nkhtml/misc/loader.h:#include <Qt3Support/Q3Dict>\nkhtml/domtreeview.h:#include <Qt3Support/Q3ListView>\nkhtml/html/html_formimpl.h:#include <Qt3Support/Q3PtrList>\nkhtml/xml/dom_docimpl.h:#include <Qt3Support/Q3PtrList>\nkhtml/xml/dom_docimpl.h:#include <Qt3Support/Q3IntDict>\nkhtml/xml/dom_docimpl.h:#include <Qt3Support/Q3Dict>\nkhtml/xml/xml_tokenizer.h:#include <Qt3Support/Q3PtrList>\nkhtml/css/css_base.h:#include <Qt3Support/Q3PtrList>\nkhtml/css/cssstyleselector.h:#include <Qt3Support/Q3PtrList>\nkhtml/css/cssstyleselector.h:#include <Qt3Support/Q3MemArray>\nkhtml/css/css_stylesheetimpl.h:#include <Qt3Support/Q3PtrList>\nkhtml/css/css_valueimpl.h:#include <Qt3Support/Q3IntDict>\nkioslave/http/kcookiejar/kcookiejar.h:#include <Qt3Support/Q3PtrList>\nsecurity/crypto/crypto.h:#include <Qt3Support/Q3CheckListItem>\nsecurity/crypto/crypto.h:#include <Qt3Support/Q3PtrList>\nsecurity/kcert/kcertpart.h:#include <Qt3Support/Q3ListView>\nkdeprint/tests/helpwindow.h:#include <Qt3Support/Q3TextBrowser>\nkdeprint/management/kmlistview.h:#include <Qt3Support/Q3ListView>\nkfile/kfiletreebranch.h:#include <Qt3Support/Q3ListView>\nkfile/k3mimetyperesolver.h:#include <Qt3Support/Q3ScrollView>\nkfile/k3filetreeviewitem.h:#include <Qt3Support/Q3PtrList>\nkate/plugins/autobookmarker/autobookmarker.h:#include <Qt3Support/Q3PtrList>\n\nAnd let me clarify that: I'm not blaming anyone, I'm merely asking."
    author: "Artem S. Tashkinov"
  - subject: "Re: Please, get rid of Qt3!"
    date: 2007-10-24
    body: "Qt3Support is not the same thing as Qt3; it's 2.9 mb little library that makes next to no difference in the big scheme of things. Sure, it'll be neater if it weren't used anymore. But judging from the above list, the public API is clean as clean."
    author: "Boudewijn Rempt"
  - subject: "Complexity.."
    date: 2007-10-22
    body: "Is it me or is Dolphin and some other apps heading to look the same way like KDE 3.5, in terms of complexity?\nI certainly hope they're not headed that way :P"
    author: "Ahmad Yasser"
  - subject: "GOOD JOB"
    date: 2007-10-23
    body: "this is well deserved and much needed: YOU GUYS ROCK, thank you for another beta release! :-D I'm getting really excited about the upcoming release!!!!"
    author: "Gian Luca Bellini"
---
The KDE Community is happy to release <a href="http://www.kde.org/announcements/announce-4.0-beta3.php">the third beta for KDE 4.0</a>. This beta, aimed at further polishing of the KDE codebase, also marks the freeze of the KDE Development Platform. We are joined in this release by the <a href="http://www.koffice.org/">KOffice project</a> which releases its <a href="http://www.koffice.org/announcements/announce-2.0alpha4.php">4th alpha release</a>, bringing many improvements in OpenDocument support, a KChart Flake shape and much more to those willing to test. Read on for more.







<!--break-->
<p>
Since the last beta, most of KDE has been frozen for new features, instead receiving the necessary polish and bugfixing. The components which were exempt from this freeze saw significant improvements as planned, and Aaron Seigo notes, "It is amazing to see the Plasma community growing. The pace of development is amazing, and we're getting really close to having all the features we want for KDE 4.0 available. After that, we have a solid foundation for implementing new and exciting user interface concepts for the Free Desktop".
</p>
<p>
KDE 4 is the next generation of the popular KDE Desktop Environment which seeks to fulfil the need for a powerful yet easy to use desktop for both personal and enterprise computing. The aim of the KDE project for the 4.0 release is to put the foundations in place for future innovations on the Free Desktop. The many newly introduced technologies incorporated in the KDE libraries will make it easier for developers to add rich functionality to their applications, combining and connecting different components in any way they want.
</p>



