---
title: "Josh Berkus: KDE Aids The PostgreSQL Team"
date:    2007-05-01
authors:
  - "aseigo"
slug:    josh-berkus-kde-aids-postgresql-team
comments:
  - subject: "This is nice"
    date: 2007-04-30
    body: "Its wonderful to see one team appreciative about the tools offered by another one. It promoted the feeling of openness and cooperation. Also, the points that Josh has noted are very well valid, and I can ratify that no other mail client offers the multilingual support/pgp support that kmail is capable of. "
    author: "Sarath"
  - subject: "Re: This is nice"
    date: 2007-05-01
    body: "This reminds me of another particularly interesting symbiosis between two unrelated projects - MoinMoin and Mercurial.\n\nMoinMoin uses Mercurial for development, and Mercurial uses MoinMoin for the home page/wiki. I find that funny for some reason :)\n"
    author: "Ricardo Correia"
  - subject: "Re: This is nice"
    date: 2007-05-01
    body: "Like David Roundy (darcs) uses Ion3 window manager and Tuomo Valkonen (Ion3) manages the code with darcs? And further Ion3 web pages are managed with Ikiwiki by Joey Hess and Joey uses Ion3..."
    author: "Frank"
  - subject: "Re: This is nice"
    date: 2007-05-01
    body: "indeed, kmail is so good, it's hard to imagine it's development isn't going as well as it should (eg lack of contributors)."
    author: "superstoned"
  - subject: "Hooray for mutual assistance"
    date: 2007-05-01
    body: "I like Postgres for their approach that emphasizes correctness and reliability, and it always warms my heart to hear of open source \"celebrities\" using KDE.\nPeople like Linus Torvalds, Josh Berkus, Paul Vixie, and many others. If they like KDE, we can generalize this to a large part of the enthusiast and power user audience.\nIt also feels great to make thinking and communication tools that help the best people solving hard problems."
    author: "Andreas"
  - subject: "Re: Hooray for mutual assistance"
    date: 2007-05-01
    body: "good point, interviewing them might be an idea..."
    author: "superstoned"
  - subject: "Re: Hooray for mutual assistance"
    date: 2007-05-02
    body: "SECONDED. This is an awesome idea!\nAsking prominent figures of the free software world what they like about KDE would be great both as feedback on what we're doing right and as a way to increase our visibility. I love this idea!"
    author: "S."
  - subject: "Kmail and Encodings"
    date: 2007-05-01
    body: "It's funny, actually, just a few days ago at work I received translated work for the first time, which kmail seamlessly saved and kate seamlessly opened, everything was great until I threw it up on the apache server that defaults to a utf8 encoding, thus causing the little characters to show up all wonky.  Doing the same set of operations on a windows box didn't cause any problems...\n\nIt was only the next day that I discovered windows was making decisions for me, reencoding to utf8, and that it was KDE that was truly honoring the encoding.  Now that I know the power inherent in these tools, I have full confidence in my ability to handle any further translated items."
    author: "Sean Kellogg"
  - subject: "Re: Kmail and Encodings"
    date: 2007-05-01
    body: "you should tell kate to use utf8 as default"
    author: "whatever noticed"
  - subject: "Re: Kmail and Encodings"
    date: 2007-05-03
    body: "I prefer for files not in my favoured encoding (UTF-8 or ASCII which is a subset of UTF-8 anyhow) to be automagically converted to UTF-8 just to avoid problems like the one you mention.  Although, Microsoft programs have a bad tendency for screwing up encoding; you can usually tell when someone uses Word to spellcheck or to write their post for online because their quotes are encoded improperly (or just nonexistent), reserved characters (usually in UTF-8) are used for some other purpose (like the aforementioned quotes issue), etc.\n\nBeing configurable in this regard is always good."
    author: "Matt"
  - subject: "Re: Kmail and Encodings"
    date: 2007-05-03
    body: "with your windows situation you are still somewhat lucky. Czech windows (and other localizations too I guess) use their own obscure encoding without even bothering to indicate it in the files they create. So there is no way to recognize these files but to open them yourself and discern whether they have crippled characters or not. The paradox is that no other windows localizations will render your subtitles, txt files etc. correctly either. They must have been using this encoding (windows-1250) since some 9x version and it is still used in Vista."
    author: "omichalek"
  - subject: "kexi"
    date: 2007-05-02
    body: "i was expecting something on kexi and pgsql actually...\nbut well kmail and kate is also nice ;)"
    author: "cies breijs"
  - subject: "Thanks for a \"different\" kind of article."
    date: 2007-05-02
    body: "Enjoyed reading it. Makes me want to check out Kmail sometime. Can it interface with Gmail and other pop3 accounts, with broad options like Thunderbird(leaving messages on server for X days, delete from server when moved from Inbox, etc.)?"
    author: "Darkelve"
  - subject: "Re: Thanks for a \"different\" kind of article."
    date: 2007-05-03
    body: "Yes - I currently use KMail to access my Gmail account, as well as my IMAP work account. It also has those options you ask about, as well as a number available that Thunderbird doesn't. I particularly like the ability to set up different identities for sending from my different email aliases, even though they all arrive into the same email account (whereas I have to set up \"fake\" accounts in Thunderbird that I don't use).\n\nLike Konqueror, Kmail is a simple to use package that can be really powerful - if you need it. Two examples of the excellent programs coming out of the KDE labs."
    author: "AndyC"
  - subject: "Vim, anyone?"
    date: 2007-05-05
    body: "\"Kate allows me to open multiple copies of the press release materials simultaneously in different editor buffers while handling them all perfectly, even when they are each in a different encoding. EMACS was painful in this regard.\"\n\nWell, it seems that someone didn't try out Vim ;). It's just one of those many, many things that Vim does right, and Emacs doesn't. :P\n\nBy the way, why do people compare things to that crappy Emacs, when there are better alternatives out there?"
    author: "troll :P"
---
During <a href="http://fisl.softwarelivre.org/">FISL 8.0</a> I caught up with <a href="http://www.postgresql.org">PostgreSQL</a> contributor Josh Berkus who was there to present on PostgreSQL and meet up with the local PostgreSQL community. Josh is a member of the PostgreSQL core team and works at Sun Microsystems as part of their open source database team. Over lunch, Josh shared how KDE plays an important role in the release coordination process which Josh oversees.




<!--break-->
<div style="float: right; margin: 6px;"><img src="http://static.kdenews.org/jr/josh-berkus-fisl2007.png" width="250" height="332"><br><div style="margin: 0px; padding: 0px; text-align: center;">Josh Berkus, PostgreSQL</div></div> 

<p>Josh explained, "We create a press kit for every PostgreSQL release that is sent out to 600-700 people by email in up to 11 different languages. The translation is done by our translation community which is made up of teams of 1-4 people per language."</p>

<p>How does KDE factor into this process? Josh continued, "Two of my essential tools in this process are KMail and Kate. The reason being that the way some of the translators handle translations is that I put the materials in CVS and some of the translators translate the press release and the web version of the press kit and check it directly into CVS. But not all translators are comfortable with CVS and instead they email their work in to me."</p>

<p>He went on to explain how KMail's excellent internationalization support allows him receive mail with attatchements in multiple languages and encodings without altering or otherwise destroying the contents. Josh noted that he had tried other popular and common mail tools and didn't have nearly the same level of success.</p>

<p>Once he has the documents saved to disk, Josh continued, "Kate allows me to open multiple copies of the press release materials simultaneously in different editor buffers while handling them all perfectly, even when they are each in a different encoding. EMACS was painful in this regard."</p>

<p>According to Josh, having to use other programs such as Open Office or Thunderbird would result in him and the PostgreSQL release team losing significant amounts of valuable time and effort, slowing down their release process. Of course, Kate and KMail are just two of the tools that help Josh and his teammates with a smooth PostgreSQL release process. As Josh noted, "Quality open source tools are key to PostgreSQL development, and KDE is among those tools."</p>

<p>Free and Open Source projects aiding the development and support of each other in this way is a marvelous example of the rich ecosystem that has arisen. Due to the efforts and commitment of those involved in projects such as PostgreSQL and KDE, Open Source projects are able to enjoy a more productive development process resulting in more frequent and impressive releases. The KDE project is happy to be able to participate both as a contributor and benefactor in this technology community.</p>


