---
title: "KDE 4.0 Alpha 2 Released"
date:    2007-07-05
authors:
  - "sk\u00fcgler"
slug:    kde-40-alpha-2-released
comments:
  - subject: "Live-CD"
    date: 2007-07-04
    body: "KDE Four Live: http://home.kde.org/~binner/kde-four-live/"
    author: "binner"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "The SUSE build service is really nice but isn't a size of 667M a little too much for just previewing KDE?\n\nThanks\n"
    author: "backtick"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "Too much? That's the space needed for all KDE modules, KOffice and Amarok. You can use Kiwi to create a much smaller CD with kdebase only of course. :-)"
    author: "binner"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "If you use the build service and not the CD then simply install as many packages as you want?"
    author: "Anonymous"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "I've posted a metalink for the live cd at http://www.metalinker.org/samples.html#kde\n\nI tried it out metalink downloads in the new KGet about a week ago, but it didn't work yet. they contain mirrors & checksums."
    author: "Ant Bryan"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "Hello,\n\nWhen I launch this live-CD on my computer, here is the message I get immediately after the linux kernel is loaded :\n\nLoading KIWI CD Boot-system...\nFailed to detect CD drive !\nrebootException reboot in 60 sec...\n\nI don't understand why it doesn't work, because I have a kubuntu distro installed on this computer, and the hardware is very well recognized.\n \nIf someone could give me an advice...\n\nThanks,\n\nArnaud741\n"
    author: "arnaud741"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "Have you verified the MD5SUM before burning? \n\nYou can also test the LiveCD in qemu ('zypper install qemu' first on openSUSE) by running: qemu -cdrom KDE-Four-Live.i686-0.2.iso -boot d -m 256  (or -m 512 if you can give it that much memory). "
    author: "apokryphos"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "Thanks for that. I'd been too lazy to look into running it in a vm - hadn't realised it was that simple."
    author: "Simon"
  - subject: "Re: Live-CD"
    date: 2007-07-06
    body: "Thank you for this suggestion : it works very well in qemu !\nMaybe it doesn't work otherwise because my DVD drive is a S-ATA one ?\n"
    author: "arnaud741"
  - subject: "Re: Live-CD"
    date: 2007-08-05
    body: "Using a USB CD-ROM drive, I get the same error message.\n(A temporary solution was to use a IDE CD drive, which not everybody has...)"
    author: "probono"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "Is there a torrent for this somewhere???"
    author: "Jonathan"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "Judging from the file dates, and the dates on this announcement, this is an OLDER CD, not alpha2."
    author: "Lee"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "Judging from the date of the files and the date at which KDE Alpha2 packages were tagged and created (which is the important date unlike the date of the announcement), this definitively likely to be the alpha2 CD."
    author: "Cyrille Berger"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "\"KDE-Four-Live.i686-0.2.iso 29-Jun-2007 04:49  667M   KDE 4.0 Alpha 2\"\nis this the official alpha 2? or just after the \"alpha 2\" tag begun?\n"
    author: "Coward"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "oops, already answered... nvm then, downloading."
    author: "Coward"
  - subject: "Re: Live-CD"
    date: 2007-07-05
    body: "The Live-CD is very nice and shows KDE4 progress. Thanks !\n\nI could not get a French keyboard it seems that the xorg keyboard definitions are missing."
    author: "Charles de Miramon"
  - subject: "isn't too early for an alpha2 !!!"
    date: 2007-07-04
    body: "ok it 's an aplha release, but at lease i was expecting a working kicker ( or whatever it will be), the system tray is totaly broken, how i am supposed to test amarok.\n\nand where is the icons !!!!!!!!\n\n"
    author: "djouallah mimoune"
  - subject: "Re: isn't too early for an alpha2 !!!"
    date: 2007-07-04
    body: "Kicker will be replaced by a Plasma plasmoid app (at least this is the plan).\nBut Plasma is kind of late in the schedule for KDE4, but the good news it that it seems like it's libraries don't need much changes and work is focusing on plasmoids. This means that freeze won't be that much of a problem for Plasma and development will continue to be seen in newer versions od KDE4."
    author: "Iuri"
  - subject: "Re: isn't too early for an alpha2 !!!"
    date: 2007-07-04
    body: "don't bother, everytime djouallah posts something on the dot, it's for bitching about KDE:\n\nhttp://www.googlesyndicatedsearch.com/u/dot?as_q=&as_epq=%22Also+by+djouallah+mimoune%22&as_oq=&as_eq=&num=100"
    author: "Patcito"
  - subject: "Re: Icons irrelevant"
    date: 2007-07-06
    body: "You are an user ! go away - this release is not for you"
    author: "dfsdfsdf"
  - subject: "Wrong link"
    date: 2007-07-04
    body: "Hi, the link to PlanetKDE is wrong (you have two double-colons there)"
    author: "Dorin"
  - subject: "Re: Wrong link"
    date: 2007-07-05
    body: "Fixed, thanks."
    author: "Navindra Umanee"
  - subject: "Impressive!"
    date: 2007-07-05
    body: "I'm just playing around with alpha 2 in a VM and I'm really impressed! The first alpha was nearly unusable slow for me and crashed all the time. Alpha 2 is much, much better.\n\nThe programs show really nice improvements compared to KDE3.x, Oxygen looks just beautiful and I really like the little animations in Dolphin (zooming of icon size preview, fading of status messages) or Konsole (themes sliding in).\n\nAnd everything including Plasma is very fast and responsive - in a virtual machine without any 3D acceleration and without compositing.\n\nGreat work!"
    author: "Ascay"
  - subject: "Systemsettings"
    date: 2007-07-05
    body: "I wonder what the reasons are behind replacing kcontrol with Systemsettings.\nWhile it doesn't matter for me I'm pretty sure that others had some thoughts about it.\n\nIs Systemsettings easier to maintain? Or does it fit better to HIGs? Or does it offer more options? And if yes, which?"
    author: "liquidat"
  - subject: "Re: Systemsettings"
    date: 2007-07-05
    body: "There was a long thread on KDE-core-Devel Mailing list about this...\nIt looks like that People like it better than KControl."
    author: "Emil Sedgh"
  - subject: "Re: Systemsettings"
    date: 2007-07-05
    body: "Although a probably bigger motivation is that KControl is unmaintained and broken for KDE 4, while System Settings is actively being worked on has started to be ported to KDE 4.\n\nOf course, some people liked it better, and some didn't. :)"
    author: "Jucato"
  - subject: "Re: Systemsettings"
    date: 2007-07-05
    body: "Thanks for the info - the discussion can be seen here:\nhttp://lists.kde.org/?l=kde-core-devel&m=118185759614117&w=2"
    author: "liquidat"
  - subject: "Re: Systemsettings"
    date: 2007-07-05
    body: "Systemsettings - this is the one that Kubuntu currently uses, is that correct? It took a bit of getting used to after years of KControl for me but I can see that it is probably better for someone coming to it new. Also, there are the other reasons as discussed on the mailig list"
    author: "Simon"
  - subject: "Re: Systemsettings"
    date: 2007-07-05
    body: "Ah, just read your blog post Liquidat, which answers my question"
    author: "Simon"
  - subject: "Re: Systemsettings"
    date: 2007-07-05
    body: "Someone over at the ubuntuforums.org raised some good points where System Settings is inferior to KControl  Since I generally agree I'll just paste them here:\n\n1) KControl displays widgets much better in any screen size. System settings responds to a smaller screen size by always adding scrollbars, instead of resizing the widgets. (see Screenshot #1 style.png). This even happens on my relatively large 1280x800 display, so it certainly isn't good on even smaller displays.(see screenshot #2 size.png). \n \n 2) KControl has a much more intuitive way of managing oversized modules. It places the module within a scroll bar, but places the Apply/Cancel buttons outside, ensuring that they are always available. This is much smarter than system settings, which scrolls everything including the buttons(see screenshot #3 search.png).\n \n 3) While System Settings has a decent search function, which tells you which categories the results are in, the Kcontrol one is a lot better. It allows you to load several modules to see which result you want, while keeping the search results active. While system settings can do this, you need to go back and forth to achieve the same result.\n \n"
    author: "Erunno"
  - subject: "Re: Systemsettings"
    date: 2007-07-06
    body: "Yep, and systemsettings isn't controllable by keyboard (you need the mouse).\n\nBut I think (hope) that now it is in the KDE svn those issues will be solved because usually KDE software is of very high quality also in regard to keyboard control (unlike unfortunately the software added to kubuntu which is not in default KDE)."
    author: "ac6"
  - subject: "Re: Systemsettings"
    date: 2007-07-09
    body: "Actually, the keyboard issue is already solved, and work is going on on the size-thingy.\n\nThe search interface might improve as well, though the way Kcontrol vs systemsettings are makes it impossible (I think) to really do it the Kcontrol way."
    author: "jospoortvliet"
  - subject: "Re: Systemsettings"
    date: 2007-07-31
    body: "I didn't code any of this or greatly follow it's creation, but my interpretation is that the System Settings changes were motivated by a very rare situation. That being: usability is not the primary concern of a computer settings editor, discoverability is.\n\nHigh usability apps are usually somewhat difficult to learn (look at any professional tool and how many shortcuts it has that aren't obvious, most of the Adobe suite comes to mind). The general settings for a desktop are very rarely edited, so having it not done in the exact minimum time-frame is not a large overall penalty.\n\nIn real use, a settings editor is only used periodically and often when it is being used it is being used by a first-time user, or at least by a user who has never done what he is looking to do before. You usually only have to touch a specific setting once or twice before it's right, not every few days.\n\nBecause of this, a 'well designed' settings editor might be a little cumbersome to an experienced user, because the improvements to discoverability slow them down. I think that's the main reason that, when the interface is improved overall, this particular tool can take what many see as a back-step.\n\nSo, keep in mind not only how much the changes effect you, and also how much the changes will affect inexperienced users, as they're the ones most affected by the interface of this module."
    author: "jame"
  - subject: "Fedora Packages"
    date: 2007-07-05
    body: "As with previous alphas, the core components (kdelibs4, kdepimlibs and kdebase4) are packaged for FC6 and F7 in the kde-redhat unstable repository. Those packages are designed to be safe to use in a KDE 3 environment: they install to /opt/kde4 and save settings in ~/.kde4.\n\nWe (Than Ngo, Rex Dieter and me) are working on getting you a more complete KDE 4 experience for Fedora 8, see:\nhttp://fedoraproject.org/wiki/Releases/FeatureKDE4"
    author: "Kevin Kofler"
  - subject: "Re: Fedora Packages"
    date: 2007-07-05
    body: "PS: The main goal of the FC6 and F7 packages is to allow developers to port applications to KDE 4 (or write new ones). Running a full KDE 4 desktop with all components upgraded to the KDE 4 versions is the goal for Fedora 8."
    author: "Kevin Kofler"
  - subject: "Re: Fedora Packages"
    date: 2007-07-05
    body: "On which rhythm are these updated? Weekly? Or only after the official releases of Alpha/Beta snapshots?"
    author: "liquidat"
  - subject: "Re: Fedora Packages"
    date: 2007-07-05
    body: "Currently at each alpha/beta."
    author: "Kevin Kofler"
  - subject: "So..."
    date: 2007-07-05
    body: "So... are you Celtic or Rangers KDE devs?"
    author: "me"
  - subject: "Re: So..."
    date: 2007-07-05
    body: "For those who don't get it, this post is sectarianism.  I'd suggest deleting it, and maybe banning the poster."
    author: "Lee"
  - subject: "Re: So..."
    date: 2007-07-05
    body: "I think its actually humour. "
    author: "blah"
  - subject: "Re: So..."
    date: 2007-07-05
    body: "Indeed - there is some division in Glasgow (and other places still) along sectarian lines, although never anywhere near as bad as used to be in Northern Ireland. Rangers v Celtic (football clubs) corresponds - or used to - to Protestant v Catholic.\n\nI don't doubt that the original post is intended as a lighthearted joke and I think for most people in Scotland/UK it would be seen as such."
    author: "Simon"
  - subject: "Re: So..."
    date: 2007-07-07
    body: "LOL :D"
    author: "Anon Ymous"
  - subject: "kiosk in kde4"
    date: 2007-07-05
    body: "does anyone know the status of kiosk in kde4? has it been ported yet?  is there a development team for that project?\n\n- Thanks!"
    author: "robert"
  - subject: "Re: kiosk in kde4"
    date: 2007-07-05
    body: "The status is that it hasn't been ported, and that, unfortunately, no one is working on it currently, that was an issue raised at a talk about deploying KDE on installation with more than 1000 computers, so maybe someone will stand up and take the job."
    author: "Cyrille Berger"
  - subject: "Re: kiosk in kde4"
    date: 2007-07-05
    body: "Actually I think that kiosktool is not ported yet, the KIOSK framework is since it is a part of the config framework."
    author: "Kevin Krammer"
  - subject: "Codename?"
    date: 2007-07-05
    body: "Has codenames been abandoned?\n\nExample: First Development Snapshot of KDE4: \"Krash\"\n\nNot that it matters to me :)\n\n"
    author: "Joergen Ramskov"
  - subject: "Re: Codename?"
    date: 2007-07-05
    body: "From http://www.kde.org/announcements/announce-4.0-alpha2.php:\n> KDE Project Ships Second Alpha Release for Leading Free Software Desktop,\n> Codename \"Cernighan\""
    author: "Kevin Kofler"
  - subject: "Impressive!"
    date: 2007-07-05
    body: "I just tried out the KDE Four Live CD with alpha2 on my machine and i have to say that i am impressed by the progress that has been made. I am sure KDE4 will just rock :)\n\nOne remark about the LiveCD: It would be very nice if X starts in 24bit mode instead of 16bit. I had to do some editing to get KDE started in 24bit, and it looks much better then :)"
    author: "Jab"
  - subject: "Getting started with Plasmoids"
    date: 2007-07-05
    body: "Having downloaded the LiveCD, I would like to try my hand at making Plasmoids. Are there any tutorials available? Documentation? Where is the clock plasmoid stored?"
    author: "Peter"
  - subject: "Re: Getting started with Plasmoids"
    date: 2007-07-06
    body: "When the fallout from Akademy has cleared, come on over to #plasma on irc.freenode.net.  It's a bit hectic and disorganised at the moment, but a few people have successfully managed to make their own.  Good luck!"
    author: "Anon"
  - subject: "Re: Getting started with Plasmoids"
    date: 2007-07-06
    body: "The API is quite straightforward and it can be found from kdebase/workspace/libs/plasma (applet.h and dataengine.h iirc). The clock applet is located in kdebase/workspace/plasma/applets/clock or something and engines directory for the engine.\n\nAbout the tutorial, I think Bas Grolleman is working on to get multi-part tutorial to the techbase, but at least there's nothing currently."
    author: "Teemu Rytilahti"
  - subject: "DPI problem in the LiveCD"
    date: 2007-07-06
    body: "The DPI is way too high. I have a widescreen laptop and the screen is crammed.\n\nSetting the DPI (after a long fight with every piece of the GUI) to 96 fixed the problem. I found that it was originally undefined. But the problem now is that I have to log out and then log back in to restart the desktop which was useless because the settings are forgotten."
    author: "Capslock"
  - subject: "Re: DPI problem in the LiveCD"
    date: 2007-07-06
    body: "DPI isn't a setting you change. It's a physical parameter: your resolution (in pixels) divided by your screen size (in inches). Change the resolution, the DPI changes too. The end result is that a 10pt font looks the same visually at 640x480, 800x600, 1600x1200 or any other resolution."
    author: "Thiago Macieira"
  - subject: "Wow!"
    date: 2007-07-06
    body: "Hi peeps,\n\nJust wanted to say, I made a VMWare Player image out of the KDE 4 alpha 2 LiveCD (available on demand :)) and I am VERY impressed with what the KDE developpers are on to. It's still alpha and not very stable, but the general feel of it is very, very smooth and polished. So thank you all for the excellent work. :)"
    author: "S."
  - subject: "Re: Wow!"
    date: 2007-07-06
    body: "Could you post some screenshots?\nYou could even use http://bayimg.com/ for (free) hosting.\n\nThanks."
    author: "Coward"
  - subject: "Screenshots here:"
    date: 2007-07-07
    body: "http://ubuntuforums.org/showthread.php?t=494339"
    author: "Luis"
  - subject: "Booting with hardware-accelerated kvm fails"
    date: 2007-07-07
    body: "on my intel CPU, with this error:\n\nhttp://kvm.qumranet.com/kvmwiki/FAQ#head-34a6ebff198dc73f61f9c0fb269cfb8b70e97018\n\nAs I understand this is related to the bootloader on the CD.\nWould it be possible to use or modify this bootloader in a way that we could\nuse fas hardware-accelerated kvm for running the live CD?\n\nOr... instead of a LIVECD, offer an image of a preinstalled OS with KDE ontop :-) ?\n"
    author: "yves"
---
The KDE Community is happy to announce the immediate availability of the second alpha 
release of the K Desktop Environment. This release comes straight out of Glasgow, 
the largest city in Scotland where <a href="http://akademy.kde.org">aKademy</a> is currently taking place. 
Hundreds of KDE hackers are working like crazy to hunt down bugs, complete features 
for KDE 4.0 and sit together developing and finishing new and exciting applications 
for the new major version of the leading Free Desktop.







<!--break-->
<p />
The most exciting new development is currently going on in 
<a href="http://plasma.kde.org">Plasma</a>, KDE 4's new shell for the desktop. 
Plasma provides krunner, an application to directly launch programs and start other 
tasks. Plasmoids are applets that display information such as the time, information about hardware 
devices and also provide access to online resources, for example 
showing RSS feeds, images or providing dictionary lookup.
<p />
System Settings, the replacement for kcontrol is an improved user interface hosting various modules for 
configuring the desktop and other aspects of the system is another addition worth mentioning.
<p />
The upcoming weeks will be spent on further stabilizing and streamlining the underlying 
KDE libraries to provide a stable interface for programmers for the whole KDE4 lifecycle. 
Furthermore, the focus will shift to finishing the applications that are shipped with 
the base desktop and polishing their user interfaces.
<p />
The end of July will see a full freeze of the kdelibs module, manifested in the first 
beta release of KDE 4.0 with the final release currently planned for the end of October this year.
<p />
For the bravehearts who want to try KDE 4.0 Alpha2, please refer to 
<a href="http://www.kde.org/info/3.91.php">the alpha 2 info page</a> 
to find ways to have a peak at the current status yourself. As the pace of development 
would outdate screenshots very quickly, the best way to find out about progress is to
refer to sites such as <a href="http://dot.kde.org">The Dot</a> or 
<a href="http://planetkde.org">Planet KDE</a>, the collection of KDE developers' weblogs.



