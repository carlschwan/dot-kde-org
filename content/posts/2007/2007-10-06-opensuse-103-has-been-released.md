---
title: "openSUSE 10.3 Has Been Released"
date:    2007-10-06
authors:
  - "sPiN"
slug:    opensuse-103-has-been-released
comments:
  - subject: "Great job, looks like an all around great release"
    date: 2007-10-05
    body: "I might have to try this out, I'm inbetween computers at the moment and this looks like a great release to try out."
    author: "Skeith"
  - subject: "One of the best linux distros ever!"
    date: 2007-10-05
    body: "Great release guys! Beautiful KDE, a lot better than Kubuntu's. Very cool integration of the community-repositories as well and tons of packages. \n\nI installed 10.3 with RC1 and I'm impressed...congratulations on this superb release!"
    author: "fish"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-06
    body: "I agree. I'm not a fan of Novell and I dislike their \"MS's puppy\"-like behaviour, but openSUSE 10.3's KDE is awsome."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-06
    body: "I second this. I switched to Kubuntu over from SUSE after their Microsofish deal. And even before that, I also was simply pissed off when they switched focus to GNOME. But it's nice to see they are doing some great work and producing one of the most polished KDE distros around. "
    author: "A KDE Advocate"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "Read http://en.opensuse.org/FAQ:Novell-MS please.\n\nopenSUSE did not switch to GNOME, it does not have a default desktop, the user have to choose it during the installation. SLED has GNOME by default but KDE is also supported. AFAIK Novell hires more KDE developers than anyone else and openSUSE project manager Stephan Kulow is a core developer of KDE so they still pay a lot attention to KDE."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "From the first link you posted:\n\n\"Not using openSUSE, a project/distribution which is merely sponsored by Novell because of this deal (whatever your feelings) is absurd. Novell has also created or is among the top sponsors of projects such as the Linux Kernel, GCC, OpenOffice.org, KDE, GNOME, Tomboy, F-Spot, Banshee, Beagle, (K)NetworkManager, Kickoff, Evolution, XEN, Xgl, and Compiz etc. Are you refusing to use any of those as well? Since they all have substantial amounts of Novell code.\"\n\nRather disingenuous, in my view. It is not absurd to choose not to use a distribution _controlled_ by Novell and a direct base for their enterprise projects if you dislike the Novell-MS deal. The relationship of the kernel, gcc, OOo etc to Novell is quite different to the relationship of openSUSE to Novell."
    author: "Simon"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "\"Rather disingenuous, in my view.\"\n\nI totally agree.\nBut this is the price we all pay when companies start to play a major role in developing."
    author: "she"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-08
    body: "I don't think it follows that involvement of companies is a bad thing. Of course companies do what is in their best financial interests while (hopefully) remaining legal. Whether the Novell-MS deal will be good for Novell in the long term remains to be seen (and is arguable - what is the benchmark against which any growth should be measured? Red Hat?). On the legal side, hopefully GPLv3 limits this to a mere blip."
    author: "Simon"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "> The relationship of the kernel, gcc, OOo etc to Novell is quite different to the relationship of openSUSE to Novell.\n\nThis still doesn't hit the head of the point for why people make up this sort of nonsense excuse to not use it. It's not about \"fighting for a cause\" or for general self-righteousness in most cases, it's just about going off negative baseless headlines and fighting for an imaginary cause which isn't under threat. And this to just try to give out the perception that they care about software freedom (when 98% of people, I can say from experience, just don't have a clue about the deal). \n\nYou're suggesting that some people draw the line when the project is \"controlled\" by Novell. Oh, really? Well, openSUSE is a _community_ project _sponsored_ by Novell (read openSUSE.org)\n\nIf openSUSE is \"controlled\" because it's the primary sponsor then let's not rule out other projects here. KDE's biggest contributors are Trolltech and SUSE/Novell. Last time I saw ALSA was almost completely being maintained by Novell/SUSE, and yet that's another community project I don't see people stop using. KDE has other sponsors? It sure does and that's wonderful. So does openSUSE (AMD; again, see openSUSE.org). \n\nopenSUSE is the base for SUSE Linux Enterprise. Guess what? So is _all_ standard Linux software. \n\nDon't use openSUSE because you don't like the way it handles things, or for some technical reason: that's absolutely fine. But saying that you don't use it because you dislike some deal Novell made is pure nonsense."
    author: "apokryphos"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-08
    body: "From the openSUSE guiding principles (read openSUSE.org ;-) :\n\n\"The board of maintainers consists of five community members including two people who are not employed by Novell. The board is headed by a chairperson  with veto power over any decision. The chairperson is appointed by Novell and  will typically be a Novell employee.\"\n\nThe situation is quite different between the projects I highlighted and openSUSE. Novell does not have a power of veto over the kernel, OOo or KDE. You could argue the point with respect to Mono and perhaps some of the others that originate with Novell.\n\nSure people should make their own decisions and I'm not calling for a boycott of openSUSE or Novell, merely saying that it is contradictory to choose to avoid openSUSE while using KDE/Linux kernel etc. Note I hadn't expressed an opinion on whether people should boycott Novell and openSUSE. (In fact I was an openSUSE user for almost a year after the deal. My main motivation for switching from openSUSE 10.2 was bad package management; my motivation for moving away from openSUSE rather than seeing if things were better in 10.3 was partly driven by the Novell-MS deal and the fact that I no longer saw myself as part of the openSUSE community, no longer wanted to file bugs etc)."
    author: "Simon"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-08
    body: "Of course, I meant \" it is _not_ contradictory to choose to avoid openSUSE while using KDE/Linux kernel etc\"\n\nHonest ;-)"
    author: "Simon"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-08
    body: "\"The situation is quite different between the projects I highlighted and openSUSE. Novell does not have a power of veto over the kernel, OOo or KDE. You could argue the point with respect to Mono and perhaps some of the others that originate with Novell.\"\n\nIt's not the exact same setup -- I'm not sure anyone is trying to say that. They are similar in that they're projects where Novell/SUSE has a substantial contribution. \n\nIf we're talking merely of control, however, then: the point of the response to that question is that there's probably _no-one_ 'boycotting' openSUSE while still boycotting Compiz, Xgl, ALSA, Mono, KNetworkManager etc. which all have similar setups of 'structure' (if you like). So, presuming that people _aren't_ boycotting those, their position is quite evidently _not_ consistent, so it's just as meaningless to not use openSUSE on the same grounds."
    author: "apokryphos"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-08
    body: "\"Read http://en.opensuse.org/FAQ:Novell-MS please.\"\n\nYeah right, like no one can ever see how \"healthy\" to Free Software is exploiting Microsoft patent FUD as a \"selling point\" for a distribution.\n\n\n\"openSUSE did not switch to GNOME\"\n\nI didn't say it \"switched to GNOME\". I said switched FOCUS to GNOME, which is different, considering SUSE's history. \n\n\n\n\"Novell hires more KDE developers than anyone else\"\n\nTrue and that's something to be very grateful for, but it shouldn't be a basis for changing positions on Novell/MS patent deal. (And I know about the interoperability part, but that's not what I'm talking about)\n\n\n"
    author: "A KDE Advocate"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-08
    body: "> I didn't say it \"switched to GNOME\". I said switched FOCUS to GNOME, which is different, considering SUSE's history. \n\nWhich is still false. openSUSE doesn't have a default, but even so there's definitely a bigger focus on KDE in openSUSE than GNOME. The GNOME developers spend more time on the enterprise products.\n\n> but it shouldn't be a basis for changing positions on Novell/MS patent deal\n\nIt's mentioned to highlight (among other things) that Novell is the greatest contributor (as a company) to the Linux desktop in the world. You would be incredibly surprised about the amount of people who don't know this. "
    author: "apokryphos"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-08
    body: "\"Novell is the greatest contributor (as a company) to the Linux desktop in the world\"\n\n\nNo single company or individual is the so-called \"greatest contributor\" to the collective Free Software efforts on desktop. It's rather disappointing that anyone would buy into such marketing crap from any GNU/Linux vendor (unless you _are_in their marketing dept., that would be a different story) And it's very disrespectful to the whole Free Software community from those who wrote the simplest command-line utilities, windowing toolkits to the thousands of developers who invest their time and effort writing applications atop them. "
    author: "A KDE Advocate"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-06
    body: "i'm also not a fan of the deal: the deal attacks the freedom ideals behind the GPL -- which is good as it can only make the free software movement stronger.\n\ncurrently i have mixed feeling over the deal as it helps novell making money which it then can invest in openSUSE.\n\ni switched to kubuntu not because of the deal, but because of the 10 series where too buggy and slow (installing a 100k package took me 15 minutes at some point), and needed a lot of additional configuration to make them usable.\n\nfrom the announcement (manly this http://www.kdedevelopers.org/node/3015) i got curious enough to give openSUSE an other try.\n\nthanks for the release!\n"
    author: "cies breijs"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "I dont think that SuSE ever was a great distribution for advanced users.\n\nThere just were too many headaches you get...\n\nfor the click-guys though, SuSE is very fine."
    author: "she"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "> I dont think that SuSE ever was a great distribution for advanced users.\n\nUnfortunately thousands of \"advanced users\" disagree with you. \n\nAnyway, he bifurcation you suggest is just simply not existent; not all \"power-users\" want to configure _everything_. I might know how to configure a few million things on my system but I certainly don't _want_ to. I want an operating system that will run quickly and in general stay out of my way. And when I do have to fix things, I want there to be easy ways to do so (YaST). \n\nSo while you spend your few hours editing your xorg.conf I think I'll just go to YaST -> monitor and have everything done perfectly for me in a matter of seconds."
    author: "apokryphos"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-08
    body: "> I dont think that SuSE ever was a great distribution for advanced users.\n[...]\n> for the click-guys though, SuSE is very fine.\n\nFor \"click-guys\" like Linus TORVALDS, maybe? He used to have SuSE on his private computer (and RH on his computer at work when he was at Transmeta). That was way before The Deal, though, and I don't know what happened then. I for one still use openSUSE, not *because* of Novell, but *despite*."
    author: "Melchior FRANZ"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-10
    body: "I believe he's running Fedora mostly these days, judging from the bug reports he's filing in RH Bugzilla. ;-)"
    author: "Kevin Kofler"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-06
    body: "openSUSE is nice and all, but it takes KDE and turns it into a glorified Windows clone.  Besides, Kubuntu 7.10 is even better although it hasn't been released yet.  At this time, however, I could probably say that openSUSE has a better KDE desktop by default at the moment.  Just wait a couple weeks, though... ;)"
    author: "Matt"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-06
    body: "Kubuntu has been about the worst KDE offering since its inception - \"Ubuntu is GNOME at its best; Kubuntu is KDE at its worst\" as one wag so succinctly put it.\n\nWhy would the next two weeks bring such a dramatic reversal?"
    author: "Anon"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-06
    body: "I am curious to know why people like it too. I tried a couple of KUbuntu releases early on and a number of aspects about it really annoyed me.  Lately, though, I have noticed it has been receiving much more praise on various forums and has come out of Ubuntu's shadow.  Even some apparently die-hard KDE people are saying it is great.  What has changed?\n\n\nPersonally, my major frustrations with Ubuntu/Kubuntu are that it seems to have worse hardware support than my distro of choice.  (On paper it should be better as it has more drivers but my specific set-up has never liked it.)  Also, the fact it is limited to a single CD really limits the number of packages.  For people like me, with extremely slow internet access, downloading anything but the smallest additional software from the repositories isn't a realistic option. I wish they could supply a second \"applications\" disk based upon, say, the most popular downloads from the repositories and supply that via ShipIt. Obviously, that would raise costs significantly. Otherwise, I am content to stick to the distro I am currently using (which I can grab off magazine covers when it is released.)"
    author: "The Vicar"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "I totally agree.  I am at a loss to explain why it is so popular. KDE is virtually crippled and although it is easy for an expert to undo the changes to Konqueror, it is not a good advert to newcomers.  I don't believe in simplifing software by taking functionality away. That is the GNOME approach.  Make it more usable, yes, but not by limiting features.\n\nA small whinge is the dreadful default theme.  I hear numerous compaints about Ubuntu's brown human theme but the \"blue on blue\" KDE theme is not just ugly but really painful to my eyes.  Of course it can be changed easily but it really shows a lack of thought going into the product if it is literally painful for someone to use.  \n\nHardware support is definitely lacking, even with non-free drivers. I have had more luck with Fedora.  More things \"just work\" in that distro.\n\nYes, a second disk of the best applications would be great  That way we could receive a full KDE and best-of-breed applications.  Linux has a good chance of penetrating the third world but we must recongise not everyone in those places can simply download more programs from the repositories.  If Canonical can give away three copies of a disk to a single person if requested, surely one installation disk and one disk of bonus applications is not too much to ask for?  As Linux application software and libraries grow in size, they will find the one CD approach a more and more rigid limit."
    author: "Sputnik50"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-06
    body: "Nonsense. It's like Windows because it has a larger menu than the default? That's silly. I'd like to hear your reasons for thinking that it is any sort of 'Windows clone', these type of statements are always so peculiar."
    author: "apokryphos"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "I dont think it looks beautiful at all.\n\nIn fact, I think it looks ugly.\n"
    author: "she"
  - subject: "Re: One of the best linux distros ever!"
    date: 2007-10-07
    body: "Aaah, well, taste is hard to discuss..."
    author: "jospoortvliet"
  - subject: "Re: One of the best linux distros ever!"
    date: 2008-05-20
    body: "I am curently running 10.3 and am looking very forward to 11. i don't care hwo novel sleeps with, as long as they keep making suse better. i tryed ubuntu and kubuntu and the bugs far out numbered those in suse, i like that it has great config tools and feels more complete than most linuxes, and maybe this novel cuddleing with ms can be a good thing, like getting things in linux that would not be there otherwise, do to some wierd pattent thing, im not saying its good but, i have watched linux do more dammage to itsself, with infighting and indignant atitudes toward new users asking qwestions, than novel could ever do to linux by sleeping with ms, \n\nsorry for bad typing not a stong point for me "
    author: "david"
  - subject: "Laptop'n fun!"
    date: 2007-10-05
    body: "Sounds like a nice release. The improved boot times will be quite nice for my laptop, since 10.2 had (or seemed to ;) quite a long boot time (fortunately suspend-to-ram works flawlessly with the OSS r300 drivers :).\n\nHopefully the good people at SUSE/Novell will keep up the great work!"
    author: "Sutoka"
  - subject: "Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "No Thx."
    author: "Laerte"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "I couldn't agree more, GStreamer is really bad and not just for Amarok but for video as well. Xine really does just work and work well so what was ever wrong with it (in Amarok's case). I understand that GStreamer is much more functional in that you can encode as well as decode and pipe effects but until it can play back video with out some green down one side or without making all videos look like they were encoded really badly (when they look like the original in Xine) or keep video and audio in sync (yes I am on version 10) you can keep it. If they needed a new framework that had more unnecessary features (from Amarok's perspective) then they could use NMM, it can at least play an MP3 stream flawlessly.\n\nAnyway some one is going to tell me to report said bugs but since I don't use it and don't intend to I have too little interest in it to bother."
    author: "Dave Taylor"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "It just proves how wise was the KDE team and community to go build the multi-backend phonon instead of locking themselves to one party or another that not all users will be happy with :)"
    author: "A KDE Advocate"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "Indeed! Imagine that some folks were flaming the KDE devs for not chaining themselves to the GStreamer thing! The argument was that GStreamer was *the* future (because we say so) and that anyone not jumping on board was a splitter. Of course GStreamer may yet turn out great, but at least the aRts story will not be repeated for KDE."
    author: "Martin"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-07
    body: "I installed OpenSUSE 10.3 couple of days ago, and the music sounds a little different... at first I thought \"oh no, it sounds bad-der\" , but after a while I could get used to it. But what does happen more is audio 'skipping' once in a while when the system is under load (for example when I was browsing the net, burning a CD-RW disc and playing music at the same time).\n\nI do not know if with Xine it would be better, but it's a pity that if I decide to use Xine I cannot use mp3 playback. I understand the reason why they did this, I just hate being 'locked into' a technology like that."
    author: "Darkelve"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-07
    body: "Packman repository has xine with mp3 support.\n"
    author: "ME"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-07
    body: "OK, thanks! :)\n\nI was afraid that package would have disappeared."
    author: "Darkelve"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-08
    body: "Actually, it is now even easier to install because you can easily add repos with the \"Community Repositories\" module, or just use the \"1 Click Install\" from the following website:\n\nhttp://software.opensuse.org/search\n\n"
    author: "Henry S."
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-08
    body: "Or even better http://software.opensuse.org/codecs"
    author: "Anonymous"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "XINE engine comes too; the reason the yauap/gstreamer engine is default is because it can provide MP3 Support. I prefer XINE too, but a GStreamer engine with MP3 support is obviously better than a XINE one without it.\n\nInstalling Amarok with full xine support has also never been easier, you can now use 1-click-install for ALL codecs in openSUSE. See http://opensuse-community.org/Multimedia"
    author: "apokryphos"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "Mmmm, why wouldn't xine provide MP3 support? I use xine here on my Slack and it plays MP3 alright.\nI suppose that's a licence issue but even there, why would they allowed Gstreamer to play them and not xine?\nProbably something I'm not aware of, I guess..."
    author: "Richard Van Den Boom"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "Fluendo provides a licensed mp3 codec with gstreamer."
    author: "Boudewijn Rempt"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "Trolltech is backing GStreamer for Phonon (KDE 4). Xine will just ship with KDE 4.0, but will most likely be replaced by GStreamer. Read http://vir.homelinux.org/blog/index.php?/archives/45-Phonon-Trolltech.html for further details.\n\nI guess you should take the chance with openSUSE 10.3 and make yourself comfortable with GStreamer, because it's here to stay."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: ">> Read http://vir.homelinux.org/blog/index.php?/archives/45-Phonon-Trolltech.html for further details.\n\nSays nothing about Trolls backing GStreamer... Its all about Phonon.\n\nI have nothing against GS, Xine engine for Amarok is just better.\n\n\nPeace."
    author: "Laerte"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "There's more information here:\nhttp://labs.trolltech.com/page/Projects/DevDays/DevDays2007\n\nBut both Xine and GStreamer have their strengths and weaknesses."
    author: "Tim Beaulen"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-06
    body: "It's definitely here to stay - that's for sure, at almost 9 years old it's probably the oldest of the major multimedia projects but it isn't making much headway on the other projects (Mplayer = 7, Xine = 7, VLC = 6). I would like it to be good because it could be a definitive solution for all platforms but at this rate the smart money is on Phonon, so in another 4-5 years when GStreamer is out of beta then I can just switch over the backend through the venerable d-package, but by then may be some RPM based package manager could be comparable to d-package (in other news hell froze over and pigs grow wings). Apologies about the rant I'm cynical after Manchester united beat my home team 4-0 and I had to sit through that in the west stand!"
    author: "Dave Taylor"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-07
    body: "> so in another 4-5 years when GStreamer is out of beta\n\n\nLOL"
    author: "jospoortvliet"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-07
    body: "Unstable API\nPre 1.0 release\nDubious playback quality (concensus around here)\nNo scheduled 1.0 release\nHolds promise\n\nName the release version that took it out of beta."
    author: "Dave Taylor"
  - subject: "Re: Amarok With GStreamer Engine by Default?"
    date: 2007-10-07
    body: "I think jos is agreeing with you..."
    author: "Odysseus"
  - subject: "KDEPIM enterprise"
    date: 2007-10-06
    body: "What exactly is enterprise version of KDEPIM? Where can I find more information about the differences between enterprise and normal versions? Thanks."
    author: "Pietro"
  - subject: "Re: KDEPIM enterprise"
    date: 2007-10-06
    body: "From what I can gather it is simply a lot of tweaks that the Opensuse KDE team have done to KDEPIM, so it is as far as I know only avaible on opensuse.  If you go to the official release notice of 10.3 at news.opensuse.org, you should be able to learn a little more about it"
    author: "Richard"
  - subject: "Re: KDEPIM enterprise"
    date: 2007-10-06
    body: "It's included in Kubuntu Gutsy Gibbon as well."
    author: "Jannick Kuhr"
  - subject: "Re: KDEPIM enterprise"
    date: 2007-10-08
    body: "And in the upcoming Fedora 8 too, and most likely soon as an update to Fedora 7 and possibly 6 too."
    author: "Kevin Kofler"
  - subject: "Re: KDEPIM enterprise"
    date: 2007-10-09
    body: "It will land in Fedora 7 updates-testing with the next push."
    author: "Kevin Kofler"
  - subject: "Re: KDEPIM enterprise"
    date: 2007-10-06
    body: "More correct would be \"the companies of the Kolab Konsortium\"."
    author: "Anonymous"
  - subject: "Re: KDEPIM enterprise"
    date: 2007-10-07
    body: "no, \"the Kompanies of the Kolab Konsortium\".\n\n:D"
    author: "jospoortvliet"
  - subject: "Re: KDEPIM enterprise"
    date: 2007-10-08
    body: "The enterprise version actually works. ;-)\n\nThe regular kdepim 3.5.7 has annoying bugs like http://bugs.kde.org/show_bug.cgi?id=126182 which aren't there in the kdepim-enterprise branch. We've pushed a build from the enterprise to Rawhide (the development version of Fedora, currently heading towards Fedora 8) for that reason and are probably going to push it as an update to our stable releases too."
    author: "Kevin Kofler"
  - subject: "Will Yast be ported to Kubuntu?"
    date: 2007-10-06
    body: "Certainly opensuse is a great distro written by extremely talented people, but I'm very attached to my kubuntu, since it was my first foray into linux. Still, there are certainly areas that opensuse excels in, where other distros should sit up and take note.\n\nOne is definitely the slick artwork, which is extremely pleasing on the eye.\nAnother is that fantastic Swiss knife known as YaST, which offers nice neat options for configuring various parts of the system. I know work was started by ubuntu devs to port it, but when will there be a chance of this appearing in Kubuntu? System settings is nice, but it would be great to have this as an options as well. I know even Oracle is porting YaST for their Red Hat rip-off, which underlines its broad appeal."
    author: "Jimmy Stretton"
  - subject: "Re: Will Yast be ported to Kubuntu?"
    date: 2007-10-06
    body: "System Settings is just an alternative to KControl and not a replacement for Yast (although there's some slight overlapping of functionality). Since Yast has been published under the GPL some time ago nobody is stopping other distributors from integrating it into their products (other than NIH syndrome). That said, I would like to see a tighter integration between KControl and Yast in the future (KDE4?) versions of openSUSE. The unified look of the control center, application browser and Yast in the GNOME version is very appealing and makes the desktop look much more coherent."
    author: "Erunno"
  - subject: "Re: Will Yast be ported to Kubuntu?"
    date: 2007-10-06
    body: "Indeed this is what I mean - unify System Settings into YaST and make it available on other KDE-based distros. This program is too good to be on Suse-based systems alone :)"
    author: "Jimmy Stretton"
  - subject: "Re: Will Yast be ported to Kubuntu?"
    date: 2007-10-06
    body: "While that would be fantastic, porting seems to be very time-consuming; some developers tried porting yast to debian a while back but gave up due to lack of resources :("
    author: "Pat Barnes"
  - subject: "Re: Will Yast be ported to Kubuntu?"
    date: 2007-10-08
    body: "\"Since Yast has been published under the GPL some time ago nobody is stopping other distributors from integrating it into their products (other than NIH syndrome).\" \n\nUh, there's no NIH syndrome at all. A lot of people simply don't like YaST from a technical point of view (aka, \"this file was generated by YaST, do not edit\").\n"
    author: "Reply"
  - subject: "Re: Will Yast be ported to Kubuntu?"
    date: 2007-10-15
    body: "And that is a technical reason to stay away from something that good?\n\nThen work with the Yast team and explain what you would prefere.\n\nOne thing I would like is a list over changed files after a yast module has been executed. We'll se if that comes along sometime :-)\n\ncu...."
    author: "birger"
  - subject: "Re: Will Yast be ported to Kubuntu?"
    date: 2007-10-06
    body: "There have been already efforts to port Yast to Debian and more recently Ubuntu, but they never go too far due to lack of resources as you need a bunch of people that understand the various parts of *both* systems very well.\n\nYaST has really two sides; the framework --a interpreter for its own programming language (YCP) and a set of libraries that range from editing files to the interface ones which hook to Qt, ncurses and now GTK too--, and then you have the actual tools (done in YCP -- some in the Perl bindings) that implement an interface and apply user's actions.\n\nMost efforts start by porting the framework, which is the fun part, but then get stuck as they port the several tools, because there are some significant differences between the distros, as well as a lot of small ones which are just as a hassle to work out. And as Suse has a bunch of guys working on it, and you want to merge their fixes and improvements, its hard to keep up, and it also means you don't want to work on top of an old effort.\nUnless you have an actual commitment from Canonical or whatever, I don't see this happening."
    author: "Brush"
  - subject: "Just wondering..."
    date: 2007-10-06
    body: "If there are so many Red Hat clones floating around, why aren't there any known clones of SUSE? Don't they provide the same access to source code? Or some other reason?"
    author: "Parminder Ramesh"
  - subject: "Re: Just wondering..."
    date: 2007-10-06
    body: "In my country, a popular [1] distribution used by some enterprises, schools and government offices was based on Suse as a few years ago. Actually, I believe they keep using a lot of Suse packages to save work and they make a point to say the kernel is from them to market it as Suse compatible.\n\n[1] its actually pretty unpopular among geeks because of its closed nature and shady government backing."
    author: "Brush"
  - subject: "It would be interesting..."
    date: 2007-10-06
    body: "to find out the number of people who downloaded the whole DVD, vs. the number of those who downloaded a gnome only or KDE only CD. I don't think we'll see a surprise here, but hopefully will mean _something_ to novel. Hint Hint!\n"
    author: "Zak"
  - subject: "Re: It would be interesting..."
    date: 2007-10-07
    body: "The project will publish more statistics than in the past, the two-day trend seems to be that the single install CDs slightly decrease the \"have both desktops installed\" fraction - for the benefit of KDE."
    author: "binner"
  - subject: "kontact.."
    date: 2007-10-07
    body: "I'm not entirely sure if I'm just paranoid or what, but at least 10.2:s KDE is steadily slipping in quality. There were times (8.0->10.0) when Kontact practically never crashed and Evolution never worked - now, however, it seems that times are changing. I get Kontact crashes 3-5 times a day whereas Evolution might actually work for a day (a small miracle in itself). Kudos for Kontact not loosing anything on crash though :)"
    author: "jmk"
  - subject: "Re: kontact.."
    date: 2007-10-07
    body: "been running 10.3 since Beta 1 and haven't had it crash once..."
    author: "Odysseus"
  - subject: "Re: kontact.."
    date: 2007-10-09
    body: "Evolution has got to be the best email program around. Never managed to get it to crash! And I'm running KDE!!  if you like crashes you can always turn to MS...."
    author: "jupieter"
  - subject: "Re: kontact.."
    date: 2007-10-08
    body: "My Kontact hasn't crashed in over a year, you really should debug that issue instead of just living with it. There are places to file bug reports, you know :-) "
    author: "apokryphos"
  - subject: "Re: kontact.."
    date: 2007-10-08
    body: "\nKontact has never crashed for me on Opensuse. Then again, I never use many of the features in Kontact, like the Calender and Feeds...so it may have something to do with those.\n\nIt is true and funny about Evolution though.  Aside from the crashes, that thing is a tad bit slow.  Nice UI though."
    author: "Henry S."
  - subject: "Re: kontact.."
    date: 2007-10-08
    body: "I use kontact on a daily basis since SuSE 9.3. The groupware behind it is an Openexchange 5 server. I update KDE rather frequently.   \n\nI think that in general the kontact quality has improved with time. In my opinion it is possible to use it in a productive and professional environment today. In our environment there are whole weeks without any crash of kontact.    \n\nNevertheless: \nCrashes still occur from time to time. In my case these crashes are most often related to the /lib64/libthread_db_xxx.so libraries (and the KDE developers know that). The crash frequency seems to rise when TLS encryption is used to communicate with our Cyrus IMAP server. Other regions where crashes sometimes occur are the calendar and task management and resulting synchronization processes with the OX 5 server.    \n\nFurthermore I got the impression that the stability varies from KDE subrelease to subrelease, i.e. some versions under KDE 3.5.6 appeared to be more stable than some subversions of 3.5.7. So sometimes it pays to make an upgrade sometimes not. I regard this as normal - give the developers a chance!    \n\nSo my recommendation is: Try a recent subversion of KDE 3.5.7 (the 3.5.7-64.1 rpms from SUSE were quite OK) but test the stability of all production relevant components (as kontact) thoroughly before you perform the upgrades in a productive environment. \nAnd - if possible under your local security aspects - think about it twice!) - deactivate TLS or SSL when connecting to your imap or groupware server in an internal network. \n\n"
    author: "moenchmeyer"
  - subject: "Re: kontact.."
    date: 2007-10-09
    body: "I'd suggest using a checkout of the kdepim-enterprise branch rather than the regular kdepim 3.5 branch if you're going to check out something from SVN anyway. At least if you care about IMAP in KMail in any way. POP3 works fine in 3.5.7, but IMAP is badly broken and at least one of the issues (filtering completely messed up) is still open (so I don't think it's fixed in 3.5.8 SVN), the enterprise branch doesn't have the issue.\n\nSee:\nhttp://bugs.kde.org/show_bug.cgi?id=126182\nhttps://bugzilla.redhat.com/show_bug.cgi?id=244930"
    author: "Kevin Kofler"
  - subject: "Amarok in 64 version?"
    date: 2007-10-09
    body: "Has anybody used Amarok in 64 bits version? It tells me every time that it has no MP3 support, even when the fluendo lib is in /usr/lib64/gstreamer-0.10 and it crash when I exit the application or when I try to play an OGG song...\n\nUsing in-board sound card with an Asus motherboard."
    author: "Klander"
  - subject: "Re: Amarok in 64 version?"
    date: 2007-10-11
    body: "I run it. I just use xine as engine. take a look at the opensuse wiki, in the section multimedia, they have a \"one click installer\" for all codecs."
    author: "Mariano Guezuraga"
  - subject: "RESPECT!"
    date: 2007-10-15
    body: "I seriously respect you guys for what you stand for and what you are doing for the linux community! Brilliant release! Thank you for all the work and effort you have put into producing this fine distro.\n\nwernher"
    author: "itchy8me"
---
openSUSE 10.3 <a href="http://news.opensuse.org/?p=400">has been released</a> with a new single KDE installation CD option. It has an excellent delivery of KDE with the latest stable KDE 3.5.7 and the <a href="http://news.opensuse.org/?p=219">first parts of KDE 4</a>: games, KRDC and KRFB. KDEPIM has also <a href="http://news.opensuse.org/?p=341">been upgraded</a> to the <em>enterprise</em> branch, providing a few new features and countless fixes.


<!--break-->
<div align="center"><a href="http://news.opensuse.org/wp-content/uploads/2007/09/kde-desktop.png"><img border="0" src="http://static.kdenews.org/jr/suse-10.3.png" /></a></div>
<br />
A full KDE 4 preview is available on the DVD or from the online repositories. Other notable changes include Amarok with <a href="http://news.opensuse.org/?p=325">MP3 support</a> <em>out-of-the-box</em>, a new <a href="http://news.opensuse.org/?p=133">1-click-install</a> technology, OpenOffice.org 2.3, and <a href="http://opensuse.org/Screenshots/openSUSE_10.3">beautiful green artwork</a>. Furthermore, a typical default boot time to a KDM screen <a href="http://news.opensuse.org/?p=104">has been decreased</a> from a painful 50+ seconds in openSUSE 10.2, to a super-fast <a href="http://news.opensuse.org/wp-content/uploads/2007/10/opensuse-103-desktop.png">24 seconds</a> in openSUSE 10.3. 

