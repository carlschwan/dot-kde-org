---
title: "FOSDEM 2008: Devroom Talks Wanted"
date:    2007-12-23
authors:
  - "bcoppens"
slug:    fosdem-2008-devroom-talks-wanted
comments:
  - subject: "come on!"
    date: 2007-12-25
    body: "Now come on ppl, last year we had a great FOSDEM, let's out-do ourselves this year! Everyone who can be there should put him/herself on the wiki! Really, helping is fun ;-)"
    author: "jospoortvliet"
  - subject: "Re: come on!"
    date: 2007-12-25
    body: "I already did! :-)"
    author: "Nikolaj Hald Nielsen"
  - subject: "x2go (server based computing - kde based)"
    date: 2008-02-04
    body: "Hello,\n\nmaybe I'm too late for the FOSDEM 2008, but if there is still a presentation slot, I would like to offer a presentation about x2go (x2go.berlios.de). x2go is a complete free (GPL - no dual licence) low bandwith capable thin client solution with boot images and desktop clients (QT4, command line,...). It's not a KDE \"core\" application, but maybe it's interesting for the audience.\n\nGreetings,\n\nhmg"
    author: "H.M. Graesing"
---
As always, KDE will have a presence at <a href="http://www.fosdem.org/2008/">next year's FOSDEM</a> in Belgium on 23-24 February 2008. FOSDEM is a European meeting of free software developers, to listen to a plethora of interesting talks about anything related to free software.
We are looking for people to give a talk in the KDE or cross-desktop devroom.





<!--break-->
<p>On Sunday, we will be sharing a room with the developers from Gnome. This means that just like last year, we are also interested in talks that transcend free desktops generally. You can have a look at last year's <a href="http://archive.fosdem.org/2007/schedule/tracks/kde">KDE</a> and <a href="http://archive.fosdem.org/2007/schedule/tracks/crossdesktop">Cross-Desktop</a> talks to have an idea about what kind of talk we might find interesting to do in the devrooms.</p>

<p>For more information including accommodation, see  the <a href="http://wiki.kde.org/tiki-index.php?page=FOSDEM2008">KDE wiki page</a>.  If you want to give a talk, please add yourself to this page or contact Bart Coppens.</p>




