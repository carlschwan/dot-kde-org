---
title: "Akademy-es 2007 in Zaragoza, Spain"
date:    2007-11-08
authors:
  - "acid"
slug:    akademy-es-2007-zaragoza-spain
comments:
  - subject: "ok?"
    date: 2007-11-09
    body: "So they \"fixed\" the aKademy spelling to Akademy, but now they decided to make it plural with a \"weird\" -es suffix?"
    author: "KDE User"
  - subject: "Re: ok?"
    date: 2007-11-09
    body: "Perhaps the es is supposed to symbolize espa\u00f1ol?"
    author: "Anon"
  - subject: "Re: ok?"
    date: 2007-11-09
    body: "exactly. 'es' is the two letter language code =)"
    author: "Aaron J. Seigo"
  - subject: "Re: ok?"
    date: 2007-11-11
    body: "Please guys, I'm trying to troll here."
    author: "KDE User"
  - subject: "Re: ok?"
    date: 2007-11-12
    body: "Oh! ok... 'es' is the two letter language code bitch!"
    author: "ZDF"
  - subject: "Re: ok?"
    date: 2007-11-09
    body: "ESpa\u00f1a y ol\u00e9"
    author: "Patxi"
  - subject: "Re: ok?"
    date: 2007-11-12
    body: "Por que no te callas?    ;-)"
    author: "Markus"
  - subject: "Re: ok?"
    date: 2007-11-14
    body: "j0j0j0"
    author: "j0j0j0"
  - subject: "North American aKademy someday?"
    date: 2007-11-12
    body: "Will this ever happen?"
    author: "T. J. Brumfield"
  - subject: "Re: North American aKademy someday?"
    date: 2007-11-12
    body: "Maybe. Meanwhile you have a nice excuse to come and get to know Europe ;)"
    author: "Super Mario"
  - subject: "Re: North American aKademy someday?"
    date: 2007-11-13
    body: "We hope that something like this will take off.\n\nWe do realise that our presence in N-A is suboptimal, at best and we'd like to nurture the KDE community on that side of the pond as well. This is, by the way also one of the reasons to hold a release event in Mountain View, including a hacking / BoF session. Maybe we can pull of something similar in coming years as well. It would definitely be cool."
    author: "Sebastian K\u00fcgler"
---
Our conference in Spain, <a href="http://www.ereslibre.es/akademy-es/">Akademy-es</a>, will be <a href="http://www.ereslibre.es/akademy-es/index.php?option=com_content&amp;task=view&amp;id=27&amp;Itemid=40">held in Zaragoza this year</a> on the 17th and 18th November.  We have <a href="http://www.ereslibre.es/akademy-es/index.php?option=com_content&amp;task=view&amp;id=31&amp;Itemid=44">a very interesting schedule</a>  with talks about CMake, KOffice, KDE programming, KDE 4 among other interesting topics. Of course, entry to the talks is free, only limited by physical space. We are planning to have a dinner on saturday night, if you are interested in attending mail sent us a mail to cena-akademy2007@ereslibre.es.



<!--break-->
