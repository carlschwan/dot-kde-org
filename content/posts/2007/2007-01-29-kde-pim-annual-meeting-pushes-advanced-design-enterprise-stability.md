---
title: "KDE PIM Annual Meeting Pushes Advanced Design, Enterprise Stability"
date:    2007-01-29
authors:
  - "wstephenson"
slug:    kde-pim-annual-meeting-pushes-advanced-design-enterprise-stability
comments:
  - subject: "gmail"
    date: 2007-01-29
    body: "As more and more people are switching to gmail, google calendar et al, it would be great to have full support of those services in KDE PIM. Such as using gmail pop3 (already possible), syncing with google calendar (no  idea if it's possible) and maybe the interaction with google map in kcontacts to get a picture of where the contact live and maybe how to get to his home from my address :-)"
    author: "Patcito"
  - subject: "Re: gmail"
    date: 2007-01-29
    body: "If implemented, the synching will probably be done via opensync. IIRC, opensync has a google calendar plugin.\n\nSlowly, things start fitting together. Thanks kdepim-guys!"
    author: "ben"
  - subject: "Re: gmail"
    date: 2007-01-29
    body: "\" interaction with google map in kcontacts to get a picture of where the contact live and maybe how to get to his home from my address\"\n\nLooks like someone is already working on it:\nhttp://frinring.wordpress.com/2006/08/03/web-services/\n\nOpen in Google Maps is even already available, even if it does not work for all addresses:\nhttp://www.kde-apps.org/content/show.php?content=42120"
    author: "F3"
  - subject: "Re: gmail"
    date: 2007-01-29
    body: "kool, thanx for the info"
    author: "Patcito"
---
On Friday 14 January 2007, members of the KDE PIM developer group came together for the fifth year in a row in Osnabrück, Germany to review
the state of the project. Important topics including Akonadi, KDE PIM
maintenance and enterprise usage. A record number of attendees were welcomed
into the Intevation office and made at home by Bernhard Reiter, Jan-Oliver Wagner and the rest of the team.
















<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex">
<a href="http://static.kdenews.org/danimo/os5_group.jpg">
<img src="http://static.kdenews.org/danimo/os5_group_small.jpg" border="0" width="300" height="215" /></a><br />
<small>The meeting participants (from left to right):<br />
Martin Konold, Tom Albers, Simon Hausmann, <br />
Bernhard Reiter, Cornelius Schumacher, Volker <br /> 
Krause, Till Adam, Ingo Klöcker, Robert Zwerus, <br />
David Jarvie, Frank Osterfeld, Thorsten Stärk, Will<br />
Stephenson, Tobias Koenig, Adriaan de Groot</small>
<a href="http://static.kdenews.org/danimo/os5_group.jpg">
</a>
</div>
<p>The emphasis of this meeting lay in driving the development of <a href="http://pim.kde.org/akonadi/">Akonadi</a>
forward.  With much of the storage functions implemented, the team's 
attention moved to accessing PIM data and the design of the libakonadi 
clients which will connect the cache to the outside world.  A streamlined 
offline caching design and the issues posed by large volumes of enterprise 
PIM data were also discussed.</p>

<p>KDE PIM is the Linux client for the <a href="http://www.kolab.org/">Kolab groupware solution</a>, and as such has 
many enterprise users who are supported by the companies making up the <a href="http://www.kolab-konsortium.de/en/index.html">Kolab 
Konsortium</a>.  Due to their stability requirements, most commercial Kolab users 
are still using the KDE 3.3 codebase.  A new enterprise branch of the KDE 3.5 
PIM module is being created which will be used as an ultra-stable base for 
Kolab users, and so that Kolab-derived improvements can return to the KDE 3.5 
branch.</p>

<p>Another topic under discussion was the maintenance strategy for KDE PIM.  PIM 
software is a critical component for most KDE users, both as individuals and 
organisations, and as KDE grows in importance it is important that its 
quality is maintained.  Distributions' representatives, developers and 
companies using KDE PIM will in future use a dedicated list to coordinate PIM 
maintenance work, to ensure that important bugs are solved once and that all 
the participants are aware when a fix is available.</p>

<p>During the long weekend there was plenty of other activity taking place.  For 
example, KDE PIM software has a tradition of placing demands on the KDE 
platform that lead to improvements benefiting everyone, and this year's 
meeting was no exception: Akonadi uses a heavily multithreaded design to give 
fast access to your PIM data and this requires the ability to make and 
receive D-Bus calls using several threads and a single D-Bus connection.  
Improvements to the <a href="http://doc.trolltech.com/4.2/intro-to-dbus.html">QtDBus</a> bindings' multithreading behaviour were also discussed, which are now being implemented.</p>

<p>The KDE 4 progress of the individual applications was reviewed, so that 
user interface improvements can take place while Akonadi is still under 
development.  A dedicated session to learn about Interview, the Qt 4 model-view framework, took place on Sunday morning.  In addition, Till Adam and Martin Konold spent time analysing the safety properties of disconnected IMAP, while Adriaan de Groot and Cornelius Schumacher planned to rationalise the KDE PIM group of websites under the <a href="http://kontact.kde.org">Kontact</a> banner.</p>

<p>While several of the participants were returning to Osnabrück, some new faces 
were present.  Tom Albers made the hop across the border to present <a href="http://www.mailody.net">Mailody</a>,
his new mail application, which was received enthusiastically, and long-term 
PIM contributors were able to share some of their experience with Tom.  Frank Osterfeld, the <a href="http://akregator.kde.org">Akregator</a> author, made his first appearance, and we welcomed Robert Zwerus, a student at the Universiteit Twente who hopes to work on Akonadi as part of his studies.  One goal of Akonadi is to make it easier for new authors like Tom to write PIM applications, by making the features and knowledge developed by the project over the years available via libakonadi.  The team hopes that KDE 4 will see a flourishing development of radical uses for PIM data.</p>

<p>The KDE PIM team would like to thank the KDE e.V. for supporting the KDE PIM 
meeting at Osnabrück with the help of its supporting members and <a href="http://www.trolltech.com">Trolltech</a>.  Our gratitude goes also to <a href="http://www.intevation.de">Intevation</a> for kindly hosting us again, and <a href="http://www.klaralvdalens-datakonsult.se">KDAB</a> and the <a href="http://www.kolab-konsortium.de/en/index.html">Kolab Konsortium</a> for a fine Mongolian meal.</p>  

<p>The meeting will be followed up in April with a smaller Akonadi hacking meeting at the KDAB offices in Berlin.</p>















