---
title: "People Behind KDE: Cyrille Berger"
date:    2007-02-02
authors:
  - "dallen"
slug:    people-behind-kde-cyrille-berger
comments:
  - subject: "Krita"
    date: 2007-02-02
    body: "I rather like Krita. I'm anything but a graphic artist, but sometimes I have needs more complicated then what showfoto can do. All my recent experiences with Krita have been very straightforward. It just does what you expect."
    author: "Ian Monroe"
  - subject: "Football"
    date: 2007-02-02
    body: "\nBy football, does he mean soccer?  Or Super Bowl Football?"
    author: "Henry S."
  - subject: "Re: Football"
    date: 2007-02-02
    body: "France was the world best footbal team! european footbal"
    author: "zvonsully"
  - subject: "Re: Football"
    date: 2007-02-02
    body: "The thing we non-english call football, you know, the sport that is actually played using the feet and a ball instead of picking up an object that doesn't even a resemble a ball and running with it while holding it in your hands! ;-)"
    author: "Quintesse"
  - subject: "Re: Football"
    date: 2007-02-02
    body: "You mean non-american :-)"
    author: "tobami"
  - subject: "Re: Football"
    date: 2007-02-02
    body: "No, someone not American or Australian (Socceroos anyone?) ;)  Probably not Canadian either.."
    author: "MamiyaOtaru"
  - subject: "Re: Football"
    date: 2007-02-02
    body: "Some people think the name came from the fact that it is played on foot, rather than on a horse.\n\nStill, American football is a silly sport. Although not quite as silly as baseball and cricket!"
    author: "Tim"
  - subject: "Blog++"
    date: 2007-02-02
    body: "I met Cyrille 2 days ago and was very pleased to do so. Not being artistic, I use Krita for simple tasks and it actually manages to do things I want! It's thrilling to see Krita growing into a mature and stable application that I can now proudly show to friends and kids. And thanks Cyrille for your lively blog which I enjoy reading!"
    author: "annma"
  - subject: "offtopic question  what is the state of kplato"
    date: 2007-02-02
    body: "hello\n\ni know this is not the right place but the wiki of kplato is nearly empty and mailing list is too technical for me.\n\nso just one question what is the state of kplato and when can we have a version for windows even alpha.\n\nfriendly\n\nPS; pardon Cyrille mais vu votre nom j'ai cru que tu es une fille, d'ailleurs je vous ai parler une fois a IRC et j'\u00e9tais \u00e9tonn\u00e9e de voir \" une fille\" si gentille."
    author: "morphado"
  - subject: "Re: offtopic question  what is the state of kplato"
    date: 2007-02-04
    body: "KPlato is going quite well: lots of commits these days."
    author: "Boudewijn Rempt"
  - subject: "Re: offtopic question  what is the state of kplato"
    date: 2007-02-04
    body: "thanks Rempt what's about the merge with taskjuggler "
    author: "morphado"
  - subject: "Re: offtopic question  what is the state of kplato"
    date: 2007-02-04
    body: "I don't think any such thing is ongoing."
    author: "Boudewijn Rempt"
  - subject: "menubar in screenshot?"
    date: 2007-02-04
    body: "Always interesting to hear about the devs, many thanks to you all btw.\n\nHowever, I am really interested to hear how he managed to get the window menubar to show in the panel at the top of the screen in the desktop screenshot. I have a similarly gnome layout at the moment, main panel at the top (on autuhide) and an external taskber at the bottom. I know you can set to have the menubar of all applications show detached at the top of the screen like macOS but can't figure out how to merge it with the main panel as cyril seems to have done.\n\ndoes anyone know?"
    author: "bailout"
  - subject: "Re: menubar in screenshot?"
    date: 2007-02-04
    body: "I've got a similar setup to Cyrille. My main panel is at the bottom and autohides, and I've added the clock, pager, systray and kmenu to the Menubar panel. As far as I know, the menubar panel cannot do autohiding."
    author: "Boudewijn Rempt"
  - subject: "Re: menubar in screenshot?"
    date: 2007-02-05
    body: "On the latest KDE you just right click on the menu bar, and then select add new applet. The only thing I miss with that setup is that only the menu from KDE applications appears on the top panel, the menu from pure-qt apps and gtk apps stay on top of the window. A freedesktop standard for menus would be so coool."
    author: "Cyrille Berger"
  - subject: "Well put"
    date: 2007-02-05
    body: "\"I think KDE is lacking a good strategy for recruitment of contributors, especially non-coders. Most of the people outside of the project think that the only way to contribute is to be a developer. But there are plenty of other tasks that need to be done and for which KDE lacks manpower. I am thinking about manuals, or translations (except for mainstream languages, the level of translation is really low, especially for documentation), and even testing, it would be really nice to have a bunch of testers before the release, who would check that there are no big regressions within KDE. There are graphics too, even if we now have an artist team, I am sure we can do with more of them.\""
    author: "Bert"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we travel to France to meet a developer who likes polishing, someone who loves to create images only to filter them out -- tonight's star of People Behind KDE is <a href="http://behindkde.org/people/cyrille/">Cyrille Berger</a>.

<!--break-->
