---
title: "Akonadi Hacking Meeting"
date:    2007-04-25
authors:
  - "tk\u00f6nig"
slug:    akonadi-hacking-meeting
comments:
  - subject: "Sommer?"
    date: 2007-04-26
    body: "\"Google Sommer of Code\" - those damn Germans! ;)"
    author: "zonk"
  - subject: "Re: Sommer?"
    date: 2007-04-26
    body: "Fixed :)"
    author: "Daniel Molkentin"
  - subject: "MySQL"
    date: 2007-04-26
    body: "Hi, I'm not very much at all up to date on what's going on with Akonadi, but... MySQL? Will KDE 4 require a database? (Sorry if this is a stupid question!!!)"
    author: "n"
  - subject: "Re: MySQL"
    date: 2007-04-26
    body: "My semi-informed reply would be: no, it will not require a database. It will just be possible to use one if you want to. I'm sure there will be support for other storage back ends as well. Still, I am not a dev in KDE-PIM, so I might be wrong."
    author: "Andre"
  - subject: "Re: MySQL"
    date: 2007-04-26
    body: "Akonadi uses the database to store metadata and usually relies on external providers/agents/ressources for the actual storage. In the mail case, for example, your mail would be in a maildir, and only a cache of things like the headers or a summary along with transient info like tags, colors, etc. would end up in the db, for easy and fast indexing, searching, etc. It can cache the payload data itself as well, though, to avoid unnessecary re-downloads, or to make offline work possible, in which case the data does end up in the database. Currently it can work with MySQL embedded and a MySQL server, so if you don't want to have a full on server running, you can simply use the single-file engine, which works much like, for example, sqlite. The database in this case is completely transparent to the user. It's merely an implementation detail, and if you happen to be running MySQL anyhow, for Amarok, KPhotoAlbum, or what have you, it can use that too. Does that clarify it a bit?"
    author: "Till Adam"
  - subject: "Re: MySQL"
    date: 2007-04-26
    body: "> Akonadi uses the database to store metadata and usually relies\n> on external providers/agents/ressources for the actual storage.\n\nHm, that sounds a lot like it could make use of Nepomuk's KMetaData.\nNo idea if that satisfies Akonadi's performance criteria, though."
    author: "Jakob Petsovits"
  - subject: "Re: MySQL"
    date: 2007-04-27
    body: "There's a demo feeder for Nepomuk, but we also store information that Nepomuk is not interested in. Sure, we'll use it."
    author: "Till Adam"
  - subject: "Re: MySQL"
    date: 2007-04-26
    body: "And out of curiosoty, why when caching the E-mail payload will the entire E-mail go into the database, instead of another maildir? "
    author: "Ben"
  - subject: "Re: MySQL"
    date: 2007-04-27
    body: "The idea is to keep the resources (the bits that know how to deal with a storage format, or groupware server) as simple as possible, which includes them not having to re-implement cache mechanisms over and over again, as was needed with the old kresources framework. So conceptually all caching is done inside Akonadi. We might special case well-known formats such as email to use a different on-disk store, for example maildir, for performance and robustness reasons, but in the general case the database acts as the cache. Much of the data is small (contacts, events, chat logs, etc.) and can thus be stored very efficiently in the db itself."
    author: "Till Adam"
  - subject: "Re: MySQL"
    date: 2007-04-26
    body: "I wonder if using Strigi is an option for them? Strigi in turn supports several databases... It might make their life easier?!?"
    author: "superstoned"
  - subject: "Re: MySQL"
    date: 2007-04-26
    body: "We use strigi for indexing, yes."
    author: "Till Adam"
  - subject: "A Wiki in Akonadi"
    date: 2007-04-26
    body: "Would it be possible to have a wiki in Akonadi ? \n\nFor example, my work group uses MoinMoin/dokuwiki/Mediawiki. \n\nAkonadi on my laptop would keep a copy of this wiki. I could work on the local copy and synchronize the two versions when needed.\n\n "
    author: "cmiramon"
---
Last weekend was not only the time for the KMail Hacking Days but
also for the second <a href="http://pim.kde.org/akonadi/">Akonadi</a>·
Hacking Meeting in Berlin, Germany.  7 KDE-PIM developers came together for 2 days at the <a
href="http://www.kdab.com">KDAB</a> offices in Berlin's Kreuzberg district and
continued to improve Akonadi, the personal information data storage for KDE 4.  Meeting the other developers in real life and discussing issues face to face
always helps to find new solutions and implement crucial features in a short
period of time.





<!--break-->
<p>
The first big step was to make the setup of Akonadi as easy as possible.
Now you just have to start the 'akonadi_control' application and all other
components of Akonadi, including the external MySQL process, are started
automatically so you can begin to read and write data from/to the storage
right away.
</p>
<p>
To be able to test the functionality effectively the application
'akonadiconsole' (which is supposed to be a test application only) was extended
to support displaying type specific information. Only a few lines of code later
it was able to show contacts stored in Akonadi
</p>
<a href="http://static.kdenews.org/jr/dot_akonadi_model.png">
  <img src="http://static.kdenews.org/jr/dot_akonadi_model_small.png">
</a>
<p>
Under the hood the code of Akonadi was cleaned up and the API of libakonadi
improved for easier extension in the future.
</p>
<p>
Also the roadmap of the project was refined and the next steps to take clarified,
so on the server side the API of the resources (that are plugins which
communicate between the Akonadi storage and external sources
e.g. groupware servers) will be simplified to allow 3rd party developers to
create new resources by just reimplementing 3 virtual methods.·
</p>
<p>
On the client side the next steps will include writing <a href="http://doc.trolltech.com/4.3/model-view-programming.html">models</a>
for all kind of PIM data (e.g. mails, contacts, events, todos) which
fetch their data from Akonadi. This task is covered by a Google Summer of Code
<a href="http://code.google.com/soc/kde/appinfo.html?csaid=FEC28979B7F91E04">project</a>.
Another step will be to write components like editors and viewers
which work standalone with the Akonadi storage and provide features like
live-update on changes and conflict resolution, new developers for these tasks are always welcome :)
</p>
<p>
In the picture below you can see the participants of this meeting:
</p>
<a href="http://static.kdenews.org/jr/dot_akonadi_group.jpg">
  <img src="http://static.kdenews.org/jr/dot_akonadi_group_small.jpg">
</a>
<p>
From left to right:
<ul>
<li>Ingo Kloecker: KMail hacking</li>
<li>Cornelius Schumacher: KOrganizer hacking</li>
<li>Till Adam: Akonadi storage</li>
<li>Robert Zwerus: Akonadi database storage</li>
<li>Tobias Koenig: Akonadi storage and libraries</li>
<li>Frank Osterfeld: Akregator and feeds model hacking</li>
<li>Volker Krause: Akonadi poster boy :)</li>
</ul>
</p>
<p>
All in all it was a great meeting at a great location with great people.
</p>
<p>
If you are interested in helping the Akonadi team with development, documentation writing or
artistic work, feel free to contacts us via <a href="mailto:kde-pim@kde.org">mail</a> or join
the IRC channel #kontact on irc.kde.org.
</p>




