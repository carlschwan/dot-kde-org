---
title: "KDE Commit-Digest for 1st April 2007"
date:    2007-04-02
authors:
  - "dallen"
slug:    kde-commit-digest-1st-april-2007
comments:
  - subject: "=)"
    date: 2007-04-02
    body: "Hey Danny,\nThanks for this 52th edition of the commit-digest! Good to see the commitment of writing a new edition each week!\n\nThanks and congrats!"
    author: "Niels van Mourik"
  - subject: "Re: =)"
    date: 2007-04-02
    body: "I copy that, great commitment, great editions.\nIt has become rutine for me to read the commit-digest with the first cop of coffee on monday mornings.\nThanks Danny!"
    author: "Snis"
  - subject: "Re: =)"
    date: 2007-04-02
    body: "So do I !!!!"
    author: "Marc"
  - subject: "Re: =)"
    date: 2007-04-02
    body: "Another on here ;-)"
    author: "superstoned"
  - subject: "Re: =)"
    date: 2007-04-02
    body: "++"
    author: "Joergen Ramskov"
  - subject: "Re: =)"
    date: 2007-04-02
    body: "+++++\n\nI'd like to echo the thanks given as well. Commit Digest always makes for an interesting read :-) "
    author: "David"
  - subject: "Re: =)"
    date: 2007-04-02
    body: "bhappy birthday commit digest.\nAnd thanks danny for the weekly fix :)"
    author: "kollum"
  - subject: "QSR"
    date: 2007-04-02
    body: "Yippie, search&replace.\n\nBTW, does anyone know if in KDE it's possible to\ndo a site-wide Search&Replace (over FTP), including\nregular expressions?\n\nDreamweaver can do it (I don't know if it handles RegEx though)\nand some software for Window too. But somehow I never found this\noption in a Linux/KDE program?"
    author: "Darkelve"
  - subject: "Re: QSR"
    date: 2007-04-02
    body: "There is KFileReplace, in the network development package. It is not yet the ideal app IMHO, since it does not show you the context of your replacements. The interface is also a tad unintuitive, but unless they specifically crippled it there should be network transparency (i.e. FTP support)."
    author: "Martin"
  - subject: "Re: QSR"
    date: 2007-04-02
    body: "Okay thanks.\n\nYou would think that, Linux would be THE system to have a killer app for that. Or am I missing something here?"
    author: "Darkelve"
  - subject: "Re: QSR"
    date: 2007-04-03
    body: "I agree. Hardcore hacker would tell you to s/something/sed, awk etc./ but that's not always what you want. You would expect this in Kate for instance; it has search across multiple files, and it is an editor, so why not search/replace? I'm sure you went looking there for this functionality at some point, as have I.\n\nWhat is this new QSR, anyway?"
    author: "Martin"
  - subject: "Re: QSR"
    date: 2007-04-04
    body: "If you mount your FTP into your filesystem you could use the command line rename utility, on debian etch that renames according to a perl regexp \n\nAlthough it doesn't have a recursive switch for some reason. "
    author: "Ben"
  - subject: "compiler complains"
    date: 2007-04-02
    body: "i got trouble with xrandr after updating xorg (suse 10.2). qt-copy compiles fine, but does not find xrandr and therefore kdebase fails. there seems to be a missing typedef for \"Connection\" but i cannot find it anywhere. is this suse only again or is something wrong after three days of breaking things?"
    author: "AJ"
  - subject: "Re: compiler complains"
    date: 2007-04-04
    body: "you should ask that at http://kde-forum.org\n"
    author: "whatever noticed"
  - subject: "ghns & sonnet?"
    date: 2007-04-02
    body: "Could GHNS & sonnet be combined so that you could download new dictionaries, or synchronize the words you added back to the dictionary-server? Then, if multiple people upload the same new words, they could automatically be added to the standard-dictionary for that language?"
    author: "ben"
  - subject: "Re: ghns & sonnet?"
    date: 2007-04-02
    body: "sounds potentially good to me.\n\nBut what I dislike most is that currently (at least in KDE 3.4.2, don't know if has been fixed already) you can't even add a new word to the dictionary from right click menu in the app. you're using, which is a bad thing.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: ghns & sonnet?"
    date: 2007-04-02
    body: "While downloading is a good idea, I don't think that uploading new additions is feasible. It will make for very poor quality of those dictionaries, and cause lots of reviewing, forward- and backwardporting work, and probably lots of different dictionary forks.\n\nI'd welcome some way to upload my personal dictionary additions to some central place so they're available on different machines, though. But that's probably nothing to do with GHNS at all.\n\n"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: ghns & sonnet?"
    date: 2007-04-03
    body: "Well, thats why I think a word should only be comitted after multiple people uploaded it. You could even combine this with further logic. For example, google needs to find more than 10.000 hits for the word before it can be added.\n\nThat would make THE most up-to-date wordlist. I don't know why there would be much porting at all, but I can imagine that GHNS is not the right framework. Maybe rather a database server with an xml interface that sonnet would connect to? Is there a free online wordlist with such features already available?"
    author: "ben"
  - subject: "Re: ghns & sonnet?"
    date: 2007-04-03
    body: "Hi,\n\nI went looking for a dictionary format, and I think I encountered an XML format that was called... I think, XDF? Unfortunately, there were no front-end applications for it to use in Linux, except a Java-based application of which I forgot the name...\n\nI was looking for a multi-language translation dictionary though, not a standard dictionary. Can't say I found one though."
    author: "Darkelve"
  - subject: "Re: ghns & sonnet?"
    date: 2007-04-03
    body: "A submission is a submission, that can be included or not."
    author: "pod"
  - subject: "Re: ghns & sonnet?"
    date: 2007-04-05
    body: "They aren't exactly great quality at the moment. aspell doesn't think 'practice' is a word, and it thinks coincide can be spelt coinside."
    author: "Tim"
  - subject: "Re: ghns & sonnet?"
    date: 2007-04-06
    body: "My aspell does know it, at least when I set aspell to use en_US. Using en_GB, it suggests practise.\n\nThe problem I tried to outline earlier though is the following:\n\n- You want one place where such a dictionary is maintained, that's a real problem to be solved\n- Twenty people submitting a wrong word doesn't make it correct\n\nYou face the same 'challenges' with translations, submissions need to be reviewed and merged, this will not go away if you allow adding words to a dictionary. (Done right, it *could* make it easier, I would agree with that.)"
    author: "Sebastian K\u00fcgler"
  - subject: "teg"
    date: 2007-04-04
    body: "is KsirK compatible with teg?"
    author: "Todd"
  - subject: "Dolphin and menu top"
    date: 2007-04-04
    body: "First of all I would like to tell you that I tested Dolphin and I am absolutely amazed by that tool, it makes operating your system very convenient. I am looking forward to KDE4.\n\nA personally prefer to keep the menu bar of focused programs at the top of the screen. This setting works pretty well with all KDE applications but unfortunately it does not work with GTK applications. Is there any way to change this, e.g. via Freedesktop standardisation? \n\nThese days you don't really feel much a difference between KDE or GTK applications  despite that GTK applications usually install half of the Gnome desktop but the lack of support for the upper top menu bar, that is really annoying."
    author: "pod"
  - subject: "Re: Dolphin and menu top"
    date: 2007-04-05
    body: ">> This setting works pretty well with all KDE applications but unfortunately it does not work with GTK applications. Is there any way to change this, e.g. via Freedesktop standardisation?\n\nThat, and the application name instead of \"File\" or whatever it is (File -> Exit ...huh?) would probably make me use OS X-like menu bar."
    author: "Lans"
  - subject: "KsirK, KDE, GNOME and the GGZ"
    date: 2007-04-05
    body: "I wish the KDE and GNOME Game guys would work closer together with each other and with the GNU Gaming Zone. There are a lot of games that are available for each DE that support GGZ, it would be really cool to enable a friendly gaming rivalry between the DEs.\n\nIf KsirK supported the T.E.G. server at the GGZ that would add a pretty good game to the current list that is unfortunately a bit boring. \n"
    author: "Imos Anon"
---
In <a href="http://commit-digest.org/issues/2007-04-01/">this week's KDE Commit-Digest</a>: The beginnings of a KControl module for <a href="http://decibel.kde.org/">Decibel</a> configuration make an appearance. Developments in the Subversion plugin for <a href="http://www.kdevelop.org/">KDevelop</a>. More optimisations in the KJS JavaScript interpreter. Further progress in the KBattleship rewrite. New country maps in <a href="http://edu.kde.org/kgeography/">KGeography</a>. KRfb, a desktop sharing utility, starts to be ported to KDE 4. A new GStreamer backend for <a href="http://phonon.kde.org/">Phonon</a>, and QSR, a search-and-replace utility, are imported into KDE SVN.
<!--break-->
