---
title: "KDE Commit-Digest for 25th November 2007"
date:    2007-11-28
authors:
  - "dallen"
slug:    kde-commit-digest-25th-november-2007
comments:
  - subject: "KDE4 really nice"
    date: 2007-11-28
    body: "we wait for the big day :)"
    author: "zvonsully"
  - subject: "Happy Birthday KDE e.V.!"
    date: 2007-11-28
    body: "(27th November) is the day on which the KDE e.V. was founded, ten years ago."
    author: "zvonsully"
  - subject: "Video previews"
    date: 2007-11-28
    body: "> fix the layout of the preview widget. Have fun watching videos \n> in the file dialog :-)\n\nNeat, it works very nicely.  Thanks for that Matthias :)"
    author: "Robert Knight"
  - subject: "Re: Video previews"
    date: 2007-11-28
    body: "Can also this video preview be enabled in the Dolphin's preview panel?"
    author: "Josep"
  - subject: "Nothing That Made Me Go Wow -- Good!"
    date: 2007-11-28
    body: "I think this is the first commit digest in a long time that contained no item that made me go 'wow!'.  I suppose that's a sign that we're nearing release time, which is a very good thing."
    author: "Inge Wallin"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-11-28
    body: "I'll try harder for next week? ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-11-28
    body: "You're the man Danny.  Thanks for all the work you put into the digest."
    author: "Leo S"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-11-28
    body: "The summary didn't mention any one huge feature that jumped out at me, but the \"Contents\" section lists that 9 of the categories had new features.  And the Digest only represents a portion of all commits.  Honestly, I'm shocked that so many \"new\" features are being included this late in the game, well after feature freeze."
    author: "T. J. Brumfield"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-11-28
    body: "RTFA! You should take a closer look.\n\nFeatures in Development Tools -> playground and KDevelop\nFeatures in Educational -> playground\nFeatures in Games -> playground\nFeatures in Graphics -> extragear and work branch\nFeatures in Office -> koffice\nFeatures in Multimedia -> extragear and work branch\nFeatures in Utilities -> playground and work branch\n\nSo only 2 of the categories had new features after the feature freeze. And those very either small features (almost bugfixes like hiding the menubar) or plasma and kopete.\n\nStop spreading FUD!"
    author: "cloose"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-11-28
    body: "Er.. that wasn't FUD."
    author: "bsander"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-11-30
    body: "well, the person probably didn't do it on purpose, but I agree with cloose it DOES classify as fear/uncertainty/doubt spreading, eg FUD..."
    author: "jospoortvliet"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-12-01
    body: "How is it spreading fear to say that developers are still working on new features?\n\nOne person posted initially in a negative light about the lack of \"wow\" features, and I was trying to spin it in a positive light, and I was attacked for it."
    author: "T. J. Brumfield"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-12-01
    body: "It's the way you say it, implying that people break the feature freeze to add new features. This is clearly not the case, but you crate uncertainty that the freeze is not respected and that the release will contain new and unstable code added up until the last minute.\n"
    author: "Morty"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-12-01
    body: "> It's the way you say it\n\nOr maybe people just feel offended too easily. Textual conversation differs a lot to face2face communication. It's a pity people get too fast and too much heated about nothing, as if they were toxiomanics you took away their drugs."
    author: "Carlo"
  - subject: "Re: Nothing That Made Me Go Wow -- Good!"
    date: 2007-12-01
    body: "Seriously. This isn't FUD. It's some guy making himself look like an idiot by posting without reading the article. Crying FUD at every little tidbit of misinformation is hardly needed. Just correct the guy and tell him to read the article next time."
    author: "john ringo"
  - subject: "Enough of plasma applets"
    date: 2007-11-28
    body: "Is it really neccessary to have feature stories about every silly plasma applet that gets added?  Its fairly clear what plasma is going to be in 4.0, I don't think that stories are all that important.  It would be nice to see a focus each week on a different module that will be released in 4.0 as we get closer to the release, maybe written by the release coordinator for that module, with the focus being on what to expect in kde4.0, what not to expect in kde4.0, and what might be coming later."
    author: "Dan"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "I agree with you. Too much info about Plasma, and no info about another components. \n\nAnyway, I enjoy reading this. Kudos to Danny and all other people involved."
    author: "Jorge S. de Lis"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "Until last week, I hadn't covered Plasma for quite a while. Having a small section on Plasma this week, I hardly think that's overkill (especially when news on Plasma seems to be pretty much always \"popular\").\n\nRegarding module overviews before KDE 4.0 is released, I plan to do that, and the next weeks should see these sections.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "I like to see what is going on plasma, but I have to agre that it seems that right now the only thing going on is about applets. I would like to know what else is going on like the panel and the other menus(Lancelot, Raptor). I also rember something about the  little icons on the 4 corners of an icons so that I could play it or do more actions (anyone knows what happened to that?)I think they were called quick actions or \u00a8plasma Icons\u00a8. I think that fromt he beginning plasma was supposed to be about more that applets so it would be nice to know if there is anything else going on with that."
    author: "tikal26"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "You are right, of course, but maybe we should not expect every idea implemented and detail finished and polished for 4.0. I'll just settle for a usable and fairly stable 4.0.0. Everything else is a bonus IMHO."
    author: "Andr\u00e9"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "yeah, there are hover buttons now, have been for over a week iirc. oh, and the systray should be fixed now, really this time (assuming the patch has been committed). :) the panel's been improving, and some other layout bugs appear to be gone (yay! I can see all of the fortune applet now!)\n\nI think applets are just easier to report on, and/or came at more convenient times for the commit digest."
    author: "Chani"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "Your memories are fadding :) This week : weather applet. Last week : Aaron about plasma containement. The week before : ah right nothing. The week before before : the vision of raptor. Then before again : raptor. And then a month before finding again some stuff about plasma. (all at the top of the page)."
    author: "Cyrille Berger"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "I wouldn't worry too much, Danny - a while back, every commit digest was filled with \"Where's all the plasma news?!?\" rants - you can't please all of the people all of the time ;)\n\nBang up job, and lots of little \"featurettes\" this week - much appreciated!"
    author: "Anon"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "I find it quite interesting and helpful. Being working on plasma myself, the information of what's going on in my neighbours codebase is really helpful, especially since most of the stuff is quite new, and I might want to peak into, for example Shawn's code how he did something.\n\nSo the value of the commit-digest is not only for enthusiasts to 'read up on cool stuff', but it's also very valuable as an \"internal communication tool\" for us developers.\n\nOf course, you can choose to just ignore the parts you've read enough about already ... :-)\n"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "i couldn't believe the weather applet video.. I heard so much about it, then there's nothing to it... WHO CARES? it's not done... it grabs some content off of a web page or some database, seriously?! This is front and centre? haha come on! There are more important things than some silly little half baked weather applet."
    author: "mike"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-28
    body: "Somewhere between the idea and the finished product there lies what you are seeing now. Without this \"silly half baked weather applet\" there will never be a \"serious fully baked weather applet\".\n\nIn short, fuck off.\n\nThank you"
    author: "MrGrim"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-29
    body: "No need to be rude.\nPutting front and center the subject of a trivial unfinished project is quite silly.  "
    author: "Dan"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-29
    body: "Then you must find the very idea of commit-digest silly..."
    author: "Ian Monroe"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-29
    body: "go take a look at the code for the weather data engine, then come back here and tell me it's trivial or unfinished.\n\nthe applet isn't the news, it's the ability to grab the data. and in case it isn't wildly apparent, here's why it's cool:\n\nit abstracts away the sourcing of the data. so it can grab data from the internet, from a local weather station .. whatever there is an ion for. we're up to 4 this week, 6 by next week.\n\nthe data itself is in turn abstracted so that you can show this weather data via any QObject with the dataUpdated slot.\n\nnow, it is just weather info. but when it comes to weather, this is pretty comprehensive and pretty much the pinacle of what one could want. pluggable back ends, completely API neutral front end.\n\nit even does matching on what you type to find the locations ... again, with any backend you choose.\n\nnow, that may not make you all hot and bothered. but if it doesn't, you're probably not the audience for a weekly on the details of software development =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-29
    body: "So in order to appreciate software development I should get all warm inside every time someone overengineers a solution? Because thats clearly what this is."
    author: "Dan"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-29
    body: "I'm not much into these applets, and doubt I will use them, but I like reading about them.  \n\nI think the problem is, that with KDE4, there is so much within it to cover, and so little space to cover it all.  It's impossible to cover what everyone wants to read about, so you can just cover little bits that excite those who are doing these articles.  \n\nI'm not interested in some of the stuff that is written, so I just bypass it, can't you do that, is it really necessary to start a thread that you dont want to read any more about it, can you not just skip reading about it? "
    author: "Richard"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-29
    body: "*sigh*\n\nok, first, it's not over-engineered. it's really bare essentials to deliver the end goal (which relates to a particular sort of user experience, not the engine itself).\n\nsecond, you don't have to get all warm inside. but that wasn't the point, wast it? no, the mike guy above was rather unkind about something he evidently didn't grasp overly well. my reply wasn't to make you feel all warm and fuzzy, it was to help mike and others like him to better understand what is actually interesting about an apparently uninteresting thing.\n\nand yeah, it's a bit frustrating to work on something, show what's there thus far and have someone who only knows how to measure a particular sort of end result and lacks a real understanding of what's going on come along and go, \"pfft. big deal.\""
    author: "Aaron J. Seigo"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-30
    body: "Being a programmer myself, I know exactly what you mean. I believe this \"issue\" shows one of the most significant differences between developers and users. Users just don't give two damns about the guts as long as the app works. Developers are able to appreciate the effort that went into the design, but it's sort of pointless to try to explain that to users.\n\nPersonally, I'd just say that there's more than meets the eye and stop right there. :)"
    author: "clayman"
  - subject: "Re: Enough of plasma applets"
    date: 2007-11-30
    body: "\"Being a programmer myself, I know exactly what you mean. I believe this \"issue\" shows one of the most significant differences between developers and users. Users just don't give two damns about the guts as long as the app works.\"\n\nIndeed. This isn't just about fancy design. It's about getting the guts and infrastructure right, so in the future there is very little work for developers to do when they want to come up with something new and cool. All the users then say \"Wow, how did you do that, and why hasn't X got that feature?\""
    author: "Segedunum"
  - subject: "Re: Enough of plasma applets"
    date: 2007-12-01
    body: "No no no, you seem to be misunderstanding.\n\nThis isn't some sort of revolutionary feature that will cause the desktop to leap ahead miles and miles, putting those pesky gnomies, let alone redmond, out of buisness forever.\n\nThis.\n\nIs.\n\nThe.\n\nWeather.\n\nA great extensible design doesn't mean a thing when the subject matter is so trivial."
    author: "Dan"
  - subject: "Re: Enough of plasma applets"
    date: 2007-12-02
    body: "This news items clearly goes over your head, despite the many thoughtful explanations in response which have shown more excercise in patience and goodwill in communication that I doubt you can ever muster, so why must you continue to display your ignorance with each successive post?"
    author: "Solbod"
  - subject: "Re: Enough of plasma applets"
    date: 2007-12-02
    body: "Thanks for the well thought out, informative, and interesting reply."
    author: "Dan"
  - subject: "Re: Enough of plasma applets"
    date: 2007-12-07
    body: "Your very welcome."
    author: "Solbod"
  - subject: "Re: Enough of plasma applets"
    date: 2007-12-01
    body: "oh boy, what a temper.\n\nthe post was questioning why the unfinished weather applet was front and centre. it doesnt do much so why is it highlighted? surely there are more important things (like many of the things listed much further below)\n\ni'll be thinking of the importance of the weather applet the next time i click the bookmark i have for my local weather.\n\nfor those interested, I'll be uploading a screencast of myself clicking bookmarks i have for the few cities that are important to me. I will show the viewers at home how they too can search for the weather.\n\nso lo-tech programmers are probably cringing everywhere.\n\noh well."
    author: "mike"
  - subject: "KDE4daily dosn't work here"
    date: 2007-11-28
    body: "I use a del inspiron 6400 with dual core and 2048 RAM.\nSee the screenshot for the error message."
    author: "Patcito"
  - subject: "Re: KDE4daily dosn't work here"
    date: 2007-11-28
    body: "This doesn't appear to be a KDE issue, but just the core Linux distro underneath.  Grub, or whatever boot loader is using uuid identifiers to point to the root partition.  When grub comes up, you should be able to press \"e\" to edit the boot menu, and edit the root entry to say /dev/sda1 /dev/sda2 /dev/sda3, whatever.  Try it a few times, and you may be able to find the correct device."
    author: "T. J. Brumfield"
  - subject: "Re: KDE4daily dosn't work here"
    date: 2007-11-28
    body: "If you are using (K)Ubuntu 7.10 (Gutsy), then please read this:\n\nhttp://ubuntuforums.org/showpost.php?p=3660695&postcount=10"
    author: "SSJ"
  - subject: "Non-Compositing Support"
    date: 2007-11-28
    body: "I have an ati r500 card in my laptop, and I can't use fglrx, its worthless. Composite will run, but is nearly unusable; also, composite does not have shadows for me even though the option is checked. I simply cannot get shadows.  Oxygen is not very usuable if there is no compositing support; everything blends into eachother and it just isn't very pretty.  There are no shadows where there need to be shadows.\n\nPlease kde devs, don't force me to use kde3! provide at least basic compatability with non-compositing systems!\n\nhttp://www.alweb.dk/gallery/engelsk/oxygen_menu"
    author: "Level 1"
  - subject: "Re: Non-Compositing Support"
    date: 2007-11-28
    body: "it's being worked on."
    author: "Aaron J. Seigo"
  - subject: "Re: Non-Compositing Support"
    date: 2007-11-28
    body: "Another thing is that kwin compositing is a __lot__ slower than compiz for example. I have a geforce 6600gt with the nvidia drivers, and kwin with compositing turned on is simply unusably slow.\n\nWithout compositing the plasma panel gets black borders, which just looks alarming to me. So I end up using compiz instead of kwin when I run kde4 sessions.\n\nAnyone know why it is so much slower? Is anyone working on it?"
    author: "loffe"
  - subject: "Re: Non-Compositing Support"
    date: 2007-11-28
    body: "here feels slow when the system is under big job, like compiling from svn ;)\n"
    author: "Rudolhp"
  - subject: "Re: Non-Compositing Support"
    date: 2007-11-28
    body: "\"Anyone know why it is so much slower?\" \"\n\nVastly less man-hours poured into it than Compiz Fusion, and much less end-user testing to date.\n\n\"Is anyone working on it?\"\n\nTwo people are working on it when they get a chance.  More are needed."
    author: "Anon"
  - subject: "Re: Non-Compositing Support"
    date: 2007-11-28
    body: "I think it's an Nvidia issue...\nIn compiz, you can disable detect refresh rate but i think kwin do it by default..."
    author: "Cedric"
  - subject: "Re: Non-Compositing Support"
    date: 2007-11-29
    body: "I have also Geforce 6600gt with Nvidia drivers. When I use KDE 4 on one monitor 1920x1024 everything is little slower than KDE 4. It's annoying but usable. But when I enable Xinerama and add another 1920x1024 monitor everything is really deadly slow. Not only shadows and windows. But dropdowns are the most annoying. When I click on a dropbox when choosing weather for example, for a second dropbox shows and then disappears. And it is impossible to choose anything from it. \nI am willing to test if anything fixes this.\n\nnick mabu on freenode.\n\nIMHO Plasma looks very nice now, but it is not usable, at least for me."
    author: "MaBu"
  - subject: "Re: Non-Compositing Support"
    date: 2007-11-28
    body: "You can change the colours just as you could in KDE3. I don't use compositing either, so when I tried out KDE4, I used a different colour scheme, and it worked well.\n\nSystem Settings -> Appearance & Themes -> Colours (or something like that)."
    author: "Soap"
  - subject: " \t\n\nATI Catalyst\u0099 7.11 Proprietary Linux x86"
    date: 2007-11-29
    body: "http://ati.amd.com/support/drivers/linux/previous/linux-r-cat711.html\n\nRelease Date: Nov 21, 2007\nFile Size: 45.4MB"
    author: "Marc Driftmeyer"
  - subject: "Re:  \t\n\nATI Catalyst\u0099 7.11 Proprietary Linux x86"
    date: 2007-11-29
    body: "I don't want to use fglrx because it doesn't support suspend to ram.  STR support is probably the single most important thing to me."
    author: "Level 1"
  - subject: "Re:  \tATI Catalyst&#8482; 7.11 Proprietary Linux x86"
    date: 2007-11-29
    body: "I'm using the 7.11 fglrx driver AND STR on my notebook.\n\nI've an Asus Z9252Va (based on A6VA), with an AMD X700.\n\nThe only thing you need to do is force suspend (but that's because my machine isn't recognized).\n\n\"s2ram -f\" is the command that works for me - flawlessly!!!\n\nMy distribution: openSUSE 10.3\n\n(and Kwin4 compositing also works (although moving windows while transparent is a bit slow)"
    author: "Bernhard"
  - subject: "kwin4 effects"
    date: 2007-11-28
    body: "what about kwin effects ???\n\ni dont see a update about the effects availables or the hotkey to use it."
    author: "Rudolhp"
  - subject: "Re: kwin4 effects"
    date: 2007-11-28
    body: "What exactly are you expecting?  They work (for me at least, hopefully the remaining issues for other people will be solved at some point)."
    author: "Leo S"
  - subject: "Re: kwin4 effects"
    date: 2007-11-28
    body: "what are the current thing that kwin can do ??\n\ncan do zoom like compiz ??\n\nthings like these."
    author: "rudolph"
  - subject: "Re: kwin4 effects"
    date: 2007-11-28
    body: "Yes you can zoom (although I haven't figured out how to set the shortcut for zoom to something like Meta+Mousewheel).  This may be a more general problem, since no keyboard shortcuts work in my install of RC1 (debian experimental).\n\n"
    author: "Leo S"
  - subject: "ktorrent"
    date: 2007-11-28
    body: "I know we can just use KDE 3 apps in KDE 4, but is there any news of a KDE 4 port of ktorrent?  It is my favorite torrent app on any platform, and basic torrent support in kget is nice, but I'd rather still use ktorrent, especially for the built in search support."
    author: "T. J. Brumfield"
  - subject: "Re: ktorrent"
    date: 2007-11-28
    body: "I agree. Ktorrent is THE best bittorrent client, bar none. "
    author: "Asket"
  - subject: "Re: ktorrent"
    date: 2007-11-29
    body: "i'll third that motion =)"
    author: "Aaron J. Seigo"
  - subject: "Re: ktorrent"
    date: 2007-11-28
    body: "A KDE4 port of ktorrent exists a long time."
    author: "Anonymous"
  - subject: "Re: ktorrent"
    date: 2007-11-28
    body: "It is nearly feature complete, I still need to finish the new queue manager GUI, and after that the only thing missing is the RSS plugin and some other minor stuff.\n\nThere will probably be an alpha or beta release in a couple of weeks."
    author: "Joris Guisson"
  - subject: "Re: ktorrent"
    date: 2007-11-28
    body: "Outstanding!  Thanks!"
    author: "T. J. Brumfield"
  - subject: "Re: ktorrent"
    date: 2007-11-28
    body: "Thank you!!"
    author: "Jadrian"
  - subject: "Re: ktorrent"
    date: 2007-11-29
    body: "besides users requests to kget handle bittorrent protocol. we also want to fully handle the metalink files. that blends http, ftp and bittorrents sources to speed up file downloads. and checksums verifications\n\nso if you want to use a 100% bittorrent apps ktorrent is your call :)\n\n"
    author: "Manolito"
  - subject: "Goya into Qt"
    date: 2007-11-28
    body: "I am very interested in Goya. To me, it really looks like a patch to the Qt framework, that I think really belongs there. Not to say that it should not be worked on, on the contrary, but I really think this should have been taken up by Trolltech. It would not suprise me if they integrated the functionality in a later release, as they seem to be doing now in other areas as well (phonon, Network Access vs. KIO, Qt Concurrent vs. ThreadWeaver, QtWebkit...)."
    author: "Andr\u00e9"
  - subject: "speed improvement"
    date: 2007-11-28
    body: "Can we expect any improvement to the speed of KDE 4 when it is released.  It seems very sluggish as a release candidate \n"
    author: "Richard"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "KDE3.0 was hardly a speed demon, and the KDE3 series got progressively faster and more memory-efficient as time went on (fitting in nicely with the Coding  Mantra - \"Make it work.  Make it work well.  Make it work fast\").\n\nI see absolutely no reason why KDE4 shouldn't behave in the same way."
    author: "Anon"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "What exactly feels sluggish? Starting applications works fine here, painting is quick, the UI blocks considerably less than in Qt4. And that's with a debug build ...\n\nPlease, if you plan to make such a general statement, try to analyse the problem a bit more in details, and ideally file a bugreport on bugzilla.kde.org. Most developers don't browse the Dot when they're looking for issues that need to be fixed."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "I think the big problem is the computer.  When KDE 3.0 came out I had a 450Mhz p2.  And gawd was it slow.  Once I got a 1.4ghz athlon it was much faster.  KDE is really just doing the same thing Apple and Microsoft have been doing for years.  Using that new CPU :)\n\nIf you want speed you dont want to run modern software.  As long as developers strive for eyecandy, and you give them the latest and greatest hardware this cycle continues.  \n\nLastly remember this is open source.  No-one* pays people to do the boring stuff like make sure its usable on an older system.  So just get a faster state of the art machine and it will be fine.  \n\n* Where no-one is a small number of companies that service very specific customers for performance.  "
    author: "kde user"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "I haven't noticed any slower performance in KDE4.  If anything, it feels faster because there is no flicker and apps start very quickly.  This is on a fast machine though so it's hard to tell.  Once KDE4 is released I'll put it on my EeePC and let you know how it runs on a 630Mhz celeron with 512 Mb of ram :)"
    author: "Leo S"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "That's simply not true with regard to KDE.\n\nI have a Duron 700 Mhz PC (with 256 MB RAM) for years and I started running KDE 1 on it, later KDE 2 and then KDE 3. Although some components have been replaced (CD-ROM/burner, hard disk and graphics card) the whole computer has still the same hardware performance and I can definitely say that since KDE 2.0 KDE got faster with *every* new release.\n\nAs well the Linux 2.6 kernel did significantly improve the performance on my hardware in contrast 2.2 and 2.4 kernels.\n\nFor example I was not able to burn a CD, listen to music and surfing the web at the same time in 2002, cause music skipped, CD burning had buffer underruns, and the mouse pointer was jumping (each of the tasks alone was no problem).\n\nNow in 2007 this is kind of workload runs without any problems and many KDE applications have greately improved their responsiveness, startupt time and task processing on this system during the whole KDE 3 cycle. I haven't testet KDE 4 there yet (just on a virtual machine at my laptop) but I am very confident that I will be able to use it without problems on it (maybe modulo some graphical effects).\n"
    author: "Arnomane"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "This isn't true. My laptop is not a big thing, it's just a P4 whit 512 MB in RAM, and it runs smoothly, and I don't feel any speed difference, it's just buggier XD."
    author: "Luis"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "That is wrong. Each computer artificialy made obsolete build up the pile of hazard e-waste. Each new computer use more than a ton of materials to be manufactured. Aditionaly, not everyone has the money to buy an new computer every year, especialy if the old one can be perfectly funcional.\n\nI hope that I can make KDE4 run on an 32mb system like is possible with KDE 3.5. At least, I want to see KDE4 runing very well (not cripled like it is needed for 32mb on KDE 3.5) on an 96mb system.\n\nlink: http://translate.google.com/translate?u=http%3A%2F%2Fwww.guiadohardware.net%2Ftutoriais%2Fusando-kde-micros-32-mb-ram%2F&langpair=pt%7Cen&hl=pt-BR&ie=UTF-8 (funny, but \"workable\", google translation of an brazilian article)\n\nWith Qt4 promissing speed-ups and lower memory foot-print, and the previous kde record, I hope that KDE4 becomes light, not an MS Vista."
    author: "Manabu"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "\"With Qt4 promissing speed-ups and lower memory foot-print, and the previous kde record, I hope that KDE4 becomes light, not an MS Vista.\"\n\nUntil TrollTech give a way of globally switching off double-buffering - which on a 1600x1200 24bit screen can add almost 6MB *per maximised window* - apps built on Qt4 will likely take up more  than their Qt3 counterparts, on balance.  Apart from the double-buffering of all widgets, though, Qt4 does indeed seem more memory efficient, but KDE4 will likely take a while to optimise.  \n\nSomeone posted a quick and dirty memory usage \"analysis\" in a previous commit digest - it's currently using more than KDE3, but nowhere near Vista levels (and I'll wager that it never will, either). "
    author: "Anon"
  - subject: "Re: speed improvement"
    date: 2007-11-29
    body: "To be honest, I find it very unlikely you can run KDE 3.5.x properly on 32 mb ram (I had trouble with 192, so now I consider 256 as minimum). KDE 4 is focussing on 'usable on 256 mb', afaik, and anything below (often > 8 yo hardware) might be usable - but you'd have to turn of some stuff for that."
    author: "jospoortvliet"
  - subject: "Re: speed improvement"
    date: 2007-11-30
    body: "Well, if you see my link, you will see what compromisses were made to reach at 22mb memory usage. It will certanly not run like it would run in an 512mb sytem, but it should be suficient for some basic tasks.\n\nAnd for your information, I think that last year in Brazil, most brand new computers sold had less than 256mb of usable memory. Good part of that were computers with 128mb of memory with 32mb shared for onboard video, wich means 96mb  of usable memory. Today most new computers being sold have 256mb or even 512mb of memory (-32mb of onboard video). You will not find nothing with more than 1GB of memory in the main market places. So change your \">8yo hardware\" to \"currently being sold hardware\".\n\nI'm not asking KDE to focus use in 32mb computers, it was only an extreme exemple, but to the many people who would like to use it in their new computers, with less than 256mb of memory, or in their old but still good computers, that don't need to be obsoleted and turn in e-waste."
    author: "Manabu"
  - subject: "Re: speed improvement"
    date: 2007-12-01
    body: "> I hope that I can make KDE4 run on an 32mb system\n\nThat's a joke, right!?"
    author: "Carlo"
  - subject: "Re: speed improvement"
    date: 2007-11-29
    body: "The whole thing just seems sluggish.  \n\nOne of the worst was kopete, which I gather will improve when released, but typing on that, the letters were a good ten seconds behind my typing.  \n\nApplications were slow loading.  Basically the whole thing just seemed much slower than  3.5, to the point I took it off.  Time wise it just wasn't worth the time I was wasting waiting for things to open.  Opening and closing windows was slower than in 3.5.  I just figured this was something to do with it being a rc and would improve with the 11 dec release.\n\nThe other thing was. that little annoying thing in the top right hand corner, looks kind of like a system tray, showing some of the things I had opened.  Real pain of a thing because it covered the close and minimize buttons to all the windows.  I couldn't find a way to move that or make it not appear on top of the windows.\n\nBut yeah, everything to me was sluggish, even amarok.  My computer is only about 3 years old, running a gb of ram.  And I really hope I don't have to follow the microsoft way of life, that to run new releases you need a new computer.  I was running the opensuse version, so don't know if they have done anything different to it to make it like that."
    author: "Richard"
  - subject: "Re: speed improvement"
    date: 2007-11-29
    body: "> Applications were slow loading\n\nthen something was very wrong with the installation you ended up with. apps are noticeably faster starting up on kde4 on the same hardware on my machines here. and they aren't exactly new. 3-4 years old.\n\n> looks kind of like a system tray\n\nit *was* the system tray ;) it works now in svn. enjoy."
    author: "Aaron J. Seigo"
  - subject: "Re: speed improvement"
    date: 2007-11-29
    body: "Nice job Aaron. KDE4 is shaping up so fast now that one will certainly miss a lot of the action if one doesn't hake a daily update. The system tray was an obstacle like one person pointed out, I am happy that it's gone and the ugle taskbar! KDE4 is getting more beautiful, functional and usable with every update. i was even able to use Amarok 2 yesterday and I am sure to get another pleasant surprise today :)\nBig thanks to the whole KDE team."
    author: "Bobby"
  - subject: "Re: speed improvement"
    date: 2007-11-29
    body: "thanks Aaron. \n\n"
    author: "Richard"
  - subject: "Re: speed improvement"
    date: 2007-11-29
    body: "np; for the system tray thing though you need to thank jstubbs, chani and seli. all i did was sit around and offer some suggestions on how to get what needed done (that was seli's input) in the plasma code, but jason did the actual coding work =) Chani also worked on the system tray stuff to get it up to that point before jason made the final fixes."
    author: "Aaron J. Seigo"
  - subject: "Re: speed improvement"
    date: 2007-12-01
    body: "> the UI blocks considerably less \n\nWhat an improvement... _Any_ ui blocking isn't acceptable nowadays."
    author: "Carlo"
  - subject: "Re: speed improvement"
    date: 2007-11-28
    body: "Sluggish performance could happen from different reasons:\n- kwin-composite is slow with repaints\n- one application is locking stuff up (knotify4 for example).\n- your widget theme causes problems.\n- it's a debug build.\n\nSee if you can narrow the problem, you can help developers that way!\n\nWith only a few weeks left, the focus is making stuff work first. After that KDe developers can look at nice enhancements or performance improvements."
    author: "Diederik van der Boor"
  - subject: "Kopete connection status"
    date: 2007-11-28
    body: "Would I be right in assuming a solid based replacement is lined up, or already implemented to replace the now removed plugin?"
    author: "Ben"
  - subject: "Re: Kopete connection status"
    date: 2007-11-29
    body: "Yes, the plumbing in Kopete to use the KDE wide networkstatus service is already there and I'm working on NetworkManager 0.6 and 0.7 backends for this at the moment."
    author: "Will Stephenson"
  - subject: "Digikam"
    date: 2007-11-28
    body: "> We use now a dedicaced Multithreaded Thumbnail Loader instance from digiKam core for all kipi-plugins we need thumb. We never use default KDE thumbnails loader with all kipi-plugins running under digiKam.\n\nLovely, wouldn't it be cool to have this digikam thumbnail loader be default in KDE?\n\nAnother thing, I wonder how the digikam developers think about usability... for me, Digikam is horrible. Some features and things are great, but I would love a cleaner interface (eg get rid of the vertical sidebars, they make the app unusable). I'd say target an UI more like F-Spot or Picassa.. Better to steal properly and have something good than to develop something horrible yourself, right?\n\nDon't get me wrong, I love much of what Digikam can do and the work being done on it, but I simply don't use it because the UI scares me away. And I think I can be considered a poweruser so it really must be (and is) bad.\n\nSo I wonder - do you guys need help from usability experts? If there are none with time to help, I would suggest just copying stuff from other UI's like F-spot..."
    author: "jospoortvliet"
  - subject: "Re: Digikam"
    date: 2007-11-29
    body: "Jos, I'm going to disagree with you on that point.  I do tons of photo management and manipulation.  I've tried F-spot and don't like it.  I also don't care much for Picasa.  They both seem targeted at the casual snapshooter.  I can get a lot more done in less time with Digikam.  Now, I'd agree that Digikam would benefit from some usability and UI tweaking.  I just don't think that copying f-spot is the answer. (not that I claim to know the answer :-)"
    author: "Louis"
  - subject: "Re: Digikam"
    date: 2007-11-29
    body: "+1, on Linux, Digikam is the only app I use.  I can't really stand Picasa, F-Spot, or (shudder) The Gimp."
    author: "T. J. Brumfield"
  - subject: "Re: Digikam"
    date: 2007-11-29
    body: "I actually find Digikam very refreshing to use\n\nI had a look at Picasa on another computer the other day, to me the whole things seemed like software that was rather dumbed down."
    author: "Richard"
  - subject: "Re: Digikam"
    date: 2007-11-29
    body: "Digikam interface is not good, F-Spot interface is even worst (that widget in a line for navigate photos per time is useless), Picasa interface has it's problems, but it's, without doubt, the better of the 3.\n\nFeature wise, the battle is between Digikam and Picasa."
    author: "Luis"
  - subject: "Re: Digikam"
    date: 2007-11-29
    body: "IMHO Digikam interface is far better than F-Spot. For instance, showing the exif information in a separate window rather than in the sidebar is inconvenient for me. \nMoreover F-Spot lacks many of Digikam features : preview of the effect of filters, kipi plugins, color management, ... it seems to me that Digikam display unblured pictures quicker. F-Spot is also lacking of a management of the quality of pictures (number of stars + filtering), even if it can be done using tags.\n\nBut Digikam could still steal some ideas. For instance having brightness, contrast, color, exposition, alltogether is nice. \n\n  "
    author: "Julien Narboux"
  - subject: "Re: Digikam"
    date: 2007-11-29
    body: "In reply to your mail on the digikam users mailing list I already\ngave a datailed account on the development process for digikam, see\nhttp://mail.kde.org/pipermail/digikam-users/2007-November/004439.html\nfor details.\nSo for a constructive approach to improving digikam I strongly \nsuggest to make use of the possibilities listed there.\nIn particular I would like to emphasize \nconcrete proposals for modifications via entries in the bug tracker,\nand even more importantly: code contributions.\n\n"
    author: "A. Baecker"
  - subject: "Re: Digikam"
    date: 2007-11-29
    body: "IMO digiKam already passed Picasa and F-Spot level in its development. In discussions on mail lists, BKO entries, when talking about digiKam we are comparing with Adobe Bridge or FotoStation - industry heavyweights. It is no longer simple photo management app. And its level demands different interface than simpler tools.\n\nIf you want comparison in hardware compare controls of Nikon Coolpix camera and those of D300. Sure, in few places some refinements could be made but don't dumb down interface.\n\nEg. what replacement for vertical sidebars? I heard this complaint several times but no sensible proposition. And this type interface is well known for KDE users (Konqueror, Amarok)."
    author: "m."
  - subject: "Re: Digikam"
    date: 2007-11-30
    body: "> We use now a dedicaced Multithreaded Thumbnail Loader instance from digiKam >core for all kipi-plugins we need thumb. We never use default KDE thumbnails >loader with all kipi-plugins running under digiKam.\n \n> Lovely, wouldn't it be cool to have this digikam thumbnail loader be default >in KDE?\n\nNot agree... digiKam is an extragear application. It become outside KDE core. Who will maintain this code. Not me, i don't work on KDE core. I have no more free time for that.\n\nAlso, this way will add another big dependency to KDE core and will increase the complexity to maintain digiKam. So definitivly no way...\n \n>Another thing, I wonder how the digikam developers think about usability... >for me, Digikam is horrible. \n\nAnd for me this comment sound like a troll and a garbage. Continue to hurt developpers like this, and you will never see any progress in opensource world! So your request is definitivly rejected from me... This is a waste of time..\n\n>Some features and things are great, but I would >love a cleaner interface (eg >get rid of the vertical sidebars, they make the >app unusable). I'd say target >an UI more like F-Spot or Picassa.. Better to >steal properly and have >something good than to develop something horrible >yourself, right?\n\nThe digiKam interface have been discuted with users. There re a lots of threads bout this subject in bugzilla. Do you know ???\n\n>Don't get me wrong, I love much of what Digikam can do and the work being done >on it, but I simply don't use it because the UI scares me away. And I think I >can be considered a poweruser so it really must be (and is) bad.\n \n>So I wonder - do you guys need help from usability experts? If there are none >with time to help, I would suggest just copying stuff from other UI's like >F-spot...\n\nWell, you is free to use F-spot. Also, forget KDE and return to Gnome env. It certainly better...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Digikam"
    date: 2007-11-30
    body: "Your English doesn't make any sense. "
    author: "Holyshit"
  - subject: "Re: Digikam"
    date: 2007-11-30
    body: "I can understand it just fine.\n\nNot everyone has English as their first language, you know."
    author: "Paul Eggleton"
  - subject: "Re: Digikam"
    date: 2007-12-01
    body: "Guess you should be happy that the default language for communication here *is* English.  I wonder how well you'd be understood if this discussion were in [insert anyone else's native language]?"
    author: "mactalla"
  - subject: "promoting the digest itself"
    date: 2007-11-28
    body: "a quick note, i hope somebody still notices this :)\ni might be dumb or ignorant (or both), but until last week i was reading only these introductions to digests - because i thought that's all there is. really, i never noticed that first link which... surprise, takes you to the actual digest !\n\ni just thought it's a quick summary on commit and development stats.\n\nmaybe there is some way to promote actual digest link more, in case there's somebody other as silly as me ;)"
    author: "richlv"
  - subject: "Re: promoting the digest itself"
    date: 2007-11-28
    body: "It also took me a while to realize there is a link there. "
    author: "x25"
  - subject: "Re: promoting the digest itself"
    date: 2007-11-28
    body: "How about a \"read more...\" at the end of the summary?"
    author: "Robin"
  - subject: "Re: promoting the digest itself"
    date: 2007-11-29
    body: "I'd use a different color for \"important\" links as these many links make it a little bit confusing.\nShouldn't be much work to change the CSS-File and to use the new class.\n\nCheers."
    author: "mat69"
  - subject: "Re: promoting the digest itself"
    date: 2007-11-29
    body: "ha, I was the same.  \n\nI think it needs a link at the end of it strongly showing the link to it."
    author: "Richard"
  - subject: "nice?! depends ..."
    date: 2007-11-29
    body: "Hmm what's nice?\nSeeing 4.0 approaching fast is very nice.\nI just (*really*) realised that we are close to December, meaning 4.0 gets closer each day.\n\nAnd all these nice news \"steal\" time which I should spend on studying - especially now. So in that apsect you suck guys! :p\n\nCheers!"
    author: "mat69"
  - subject: "It's just a demo of the progress"
    date: 2007-11-29
    body: "While I find it wildly amusing to see some of the comments about the weather applet/engine, the point of the demo was to show the progress not the final result. To those who think this is 'overengineered' you don't understand the goals.\n\nThere has already been interest from some other KDE developers to reuse the API to display weather in other applications such as Kontact (we'll see if this can be done). \n\nIf you didn't read what Aaron posted you wouldn't understand why this has been designed this way.\n\nNow that I'm done discussing this issue, I can get back to coding, thanks :-)"
    author: "Shawn Starr"
  - subject: "Decibel+Kopete"
    date: 2007-11-29
    body: "Does someone knows of the currents status of the integration of Decibel and Kopete?\nAs Decibel is based on Tapioca/Telepahty, it should allow Kopete to make voice and video calls for gtalk and msn, what would be a very, very welcome feature to KDE4.X."
    author: "Iuri Fiedoruk"
  - subject: "Re: Decibel+Kopete"
    date: 2007-11-29
    body: "\"In planning\".  Something for 4.1."
    author: "Bille"
  - subject: "Question about Graphics Hardware / kwin+composite"
    date: 2007-11-30
    body: "Hi you guys,\n\ni'm not the hardware expert, so please don't mind my OT question:\n\ni'm about to upgrade my older office-pC one last time in anticipation of KDE4, at the moment i'm at AthlonXP-M 2800, 1GB DDR and a Geforce4MX...\n\nso i have to decide: will a cheap Geforce 6200 AGP suffice for kwin + full composite effects, or should i choose a geforce 6600GT?\n\nI hope some of you can give advice :)"
    author: "Marc O."
  - subject: "Re: Question about Graphics Hardware / kwin+composite"
    date: 2007-11-30
    body: "The 6600GT is just a good card in general.  That was my last card before I bought my current system, and I can't recommend it enough."
    author: "T. J. Brumfield"
---
In <a href="http://commit-digest.org/issues/2007-11-25/">this week's KDE Commit-Digest</a>: A Trash applet, various general improvements, and support for sharing configuration layouts in <a href="http://plasma.kde.org/">Plasma</a>. "Undo close tab" feature in <a href="http://www.konqueror.org/">Konqueror</a>. Development continues towards <a href="http://amarok.kde.org/">Amarok</a> 2.0, with services becoming plugins and support for the Amapche music server. Continued progress in <a href="http://www.kdevelop.org/">KDevelop</a> and <a href="http://edu.kde.org/keduca/">KEduca</a>. More work on album display and improved thumbnails (with RAW format support) in <a href="http://www.digikam.org/">Digikam</a>. A BitTorrent plugin for <a href="http://kget.sourceforge.net/">KGet</a>, based on the recently created libktorrent. Directory monitoring-and-update support in <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>. Work returns to Okteta, a hex editing utility. "Connection Status" plugin removed from <a href="http://kopete.kde.org/">Kopete</a>. Kile begins to be ported to KDE 4, whilst work begins on KGPG2. Goya, a GUI widget framework, is imported into playground.

<!--break-->
