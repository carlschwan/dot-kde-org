---
title: "Trolltech Becomes the First Corporate Patron of KDE"
date:    2007-02-28
authors:
  - "sk\u00fcgler"
slug:    trolltech-becomes-first-corporate-patron-kde
comments:
  - subject: "Submitted to Digg"
    date: 2007-02-28
    body: "I've submitted this story to digg at http://digg.com/linux_unix/Trolltech_Becomes_the_First_Corporate_Patron_of_KDE"
    author: "Alex Lowe"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "Why?"
    author: "foo"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "To let people know."
    author: "Alex Lowe"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "The proper question is \"why should we care?\". This is a site for promoting KDE, not for promoting third party websites."
    author: "Anonymous Coward"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "And that's what he is doing (promoting KDE)."
    author: "Narishma"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "He's doing both :)"
    author: "whatever noticed"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "And asking others to help me promote KDE."
    author: "Alex Lowe"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "Indeed. Some seem to not like your digging efforts, but even though I don't use digg myself, more visibility for KDE never hurts. Please continue!"
    author: "superstoned"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "People can submit stories to Digg as they please, but why do they have to announce here that they've done so? I find those posts very annoying."
    author: "ac"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "Much more annoying are the 9 responds to the original 'Digg this' post.\nMy first and last respond in a 'Digg this' thread. ;)"
    author: "Bernd"
  - subject: "Re: Submitted to Digg"
    date: 2007-02-28
    body: "Stop spamming. It gives bad karma. God hates spammers. Allah does. "
    author: "Debian User"
  - subject: "Re: Submitted to Digg"
    date: 2007-03-01
    body: "He's not spamming IMHO. He's making it easier for people to promote KDE. I wouldn't submit the story to digg, but whenever somebody posts a link to a kde digg article, I follow the link and digg the story. A lot of KDE related articles end up on the digg frontpage this way, thus giving KDE more exposure.\n"
    author: "Anony Mouse III"
  - subject: "No to spam campaign"
    date: 2007-03-01
    body: "Say no!"
    author: "Youssef"
  - subject: "KDE a part of Trolltech success"
    date: 2007-02-28
    body: "I have always felt that the KDE is also responsible for Trolltech QT's success. I came to know about this great toolkit through the use of KDE. There should be many people like me. Well from my point of view, Trolltech made the right move by becoming the Patrons of KDE. =)"
    author: "Swaroop"
  - subject: "Re: KDE a part of Trolltech success"
    date: 2007-02-28
    body: "We shoulkd also be gratefull. THANK YOU Trolltech !"
    author: "KubuntuUserExMandrake"
  - subject: "Re: KDE a part of Trolltech success"
    date: 2007-02-28
    body: "Exactly. I think it really works both ways. Trolltech is benefitting from KDE, but KDE is also really benefitting from having such a sound foundation as Qt proves to be.\n"
    author: "Andr\u00e9"
  - subject: "Re: KDE a part of Trolltech success"
    date: 2007-02-28
    body: "And more importantly, the community of users are benefiting the most. I don't think there is anyone who could deny that."
    author: "Abe"
  - subject: "Re: KDE a part of Trolltech success"
    date: 2007-02-28
    body: "Yes, isn't Free Software great? Making money while releasing your software under the GPL... The Free Software Foundation designed the GPL to allow this Dual licensing as one of the 'good' ways to make money with Free Software - we should be gratefull to them and specifically Richard Stallmann for his great Vision on Freedom. I don't agree with all he says or does, but if he hadn't come up with this great idea, we would be very limited in our choice of software..."
    author: "superstoned"
  - subject: "Re: KDE a part of Trolltech success"
    date: 2007-03-03
    body: "Or we'd be very stoned."
    author: "Anonymous"
  - subject: "Typo"
    date: 2007-02-28
    body: "\"Knut Irvin\" -> \"Knut Yrvin\"\n\nOtherwise he'll be very disappointed. ;-)"
    author: "Anonymous Custard"
  - subject: "Re: Typo"
    date: 2007-02-28
    body: "Thanks. :)"
    author: "Navindra Umanee"
  - subject: "First or second Patron"
    date: 2007-02-28
    body: "Please check the first sentence."
    author: "Michael Daum"
  - subject: "Re: First or second Patron"
    date: 2007-02-28
    body: "i think they are second patron, after mark shuttleworth, but the first corporate patron to enter this program."
    author: "Aaron J. Seigo"
  - subject: "Sweet souvenir "
    date: 2007-02-28
    body: "Sad to see, that companies are not hurrying up to catch this opportunity, i don\u0092t know why, but when i read this article it recall me when  some big names formed the gnome foundation,  I remember the courageous response of KDE, we are not afraid.\n\nOh my God it seems for me like just yesterday.  We user, we love our KDE, whatever companies adopted or not.\n\nFriendly  \n"
    author: "djouallah mimoune"
  - subject: "Good PR about time recognition"
    date: 2007-02-28
    body: "about time"
    author: "ace"
  - subject: "Thank you Trolltech"
    date: 2007-02-28
    body: "KDE owes QT everything. The rapid and massive development of QT is such a huge asset. Every release got only better _and_ feature richer _and_ faster _and_ more portable _and_ more free.\n\nI personally have worked towards using QT (over GTK or wxWindows) in our company not only, but also based on the grounds that it supports KDE.\n\nSponsoring/Employing people like Aaron or Zack to work on KDE stuff, is such a huge and valueable contribution on top of what QT already is.\n\nAnd other than that, I point to Trolltech when talking about how a successful Free Software company can be. The win of Trolltech is growing fast as well as their investment into QT.\n\nBest regards,\nKay\n\nPS: Shame on those Digg spammers for preventing any meaningful discussion here."
    author: "Debian User"
  - subject: "Thank you KDE"
    date: 2007-03-01
    body: "QT owes KDE everything. The rapid and massive development of KDE is such a huge asset. Every release got only better _and_ feature richer _and_ faster _and_ more portable _and_ more free.\n \nI personally have worked towards using KDE (over GNOME) in our company not only, but also based on the grounds that it uses QT.\n \nSponsoring/Employing people like Aaron or Zack to work on QT stuff, is such a huge and valueable contribution on top of what KDE already is.\n \nAnd other than that, I point to all the KDE guys when talking about how a successful Free Software project can be. The win of the KDE community is growing fast as well as their investment into QT.\n \nBest regards,\nKray"
    author: "Kray"
  - subject: "Re: Thank you KDE"
    date: 2007-03-01
    body: "The sponsoring part obviously is not happening. But all other true as well. QT and KDE have a nice symbiosis (spelling, Konqueror likes it though?!).\n"
    author: "Debian User"
  - subject: "Unfortunately, Gtk has better license"
    date: 2007-03-01
    body: "The single biggest problem with Qt is, that it is not LGPL like Gtk.\nEverybody, even Windows devs, would probably use Qt (instead of the sucking MFC or the .NET overkill) if the commercial Qt license would be replaced by some more liberal one like LGPL of FreeBSD.\n\nThis single problem distracts big players from the Qt/KDE. Vice versa - the only advantage of Gtk is its liberal license, which is enough for most commercial companies to choose as their primary toolkit.\n\nI hope Trolltech will free the Qt som day and make money on something else, than the overprised Qt commercial."
    author: "ZACK"
  - subject: "Re: Unfortunately, Gtk has better license"
    date: 2007-03-01
    body: "I second that.\n\nBesides a company  which uses Qt has to buy a license for every developer. And we're talking about huge sums of money."
    author: "Artem S. Tashkinov"
  - subject: "Re: Unfortunately, Gtk has better license"
    date: 2007-03-01
    body: "Qt is by no means expensive.  Also small businesses can qualify to get a 65% discount on the initial price of Qt.  After the first year (which includes the initial 'purchase' of Qt) the most expensive Maintenance & Support license costs $2050, with the lowest costing only $550.  If you have many developers you can also get volume discounts.  So is $2050 a year for a developer REALLY that expensive?\n\nThe pricing page is at: http://www.trolltech.com/products/qt/licenses/pricing\n\nAgain, how can people make the claim that $2050/dev/year isn't a good deal for a company that will be making money off the product?  It really is a benefit to everyone, since then the company gets support (can companies get paid support for GTK?), as well as helping fund the continued improvement of Qt, which will in turn benefit them."
    author: "Sutoka"
  - subject: "Re: Unfortunately, Gtk has better license"
    date: 2007-03-03
    body: "This is astroturf. Yes, for some organizations $2050/yr/developer is REALLY _that_ expensive. I can only afford to use qt (for KDE's power) for GPL projects. For us the cost is just too much."
    author: "jgilbert"
  - subject: "Re: Unfortunately, Gtk has better license"
    date: 2007-03-06
    body: "I've released shareware that's pulled in just a few hundred dollars.  I've also programmed niche applications (educational apps for blind students) that made under a thousand dollars.  Things are different for me now, but there was a time in my professional life when even the price of Turbo C and/or Power C was a heavy hit.\n\nThat doesn't mean that it's not well priced.  It means that it is priced beyond the means of the hobby programmer who wants to release a shareware app that they wrote out of love.  There is a fairly large world between professional coders working for companies with development budgets and GPL developers."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Unfortunately, Gtk has better license"
    date: 2007-03-01
    body: "But I am sure you understand why Trolltech really has no point of letting those use their toolkit for free, that don't want to open their source too.\n\nRest assured that no \"big player\" is distracted by QT from the fact that it costs money if they want to remain closed source. Imagine how much cheaper Linux/QT is if you compare it to something that costs royalties per unit.\n\nOur small to mid size company bases all on QT and finds it very cheap. We pay more for things like RHEL licenses, Oracle, etc, a lot, so QT is basically free, once you bought it for one project, you can use it in any project.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Unfortunately, Gtk has better license"
    date: 2007-03-19
    body: "I am not sure - but I think licensing issues are one of the reasons why the JDS (Java Desktop System) was build at top of GNOME..\n\n"
    author: "Piero"
  - subject: "Re: Unfortunately, Gtk has better license"
    date: 2007-03-01
    body: "Indeed, everyone would use it. But you have to understand that Trolltech would not make money and fund Qt development if no one paid for it.\n\nSo, if you pay for it, you're contributing to improving it, as well as indirectly contributing to KDE and a couple other Free Software projects. The way I see it, it's a win-win situation.\n\nIn any event, a company is entitled to choose. If they choose Gtk, so be it. They can choose it because it integrates better with the desktop they chose. They can choose it because it integrates better with their language of choice (C or even C#). They can even choose it because they don't want to depend on Trolltech. There are many reasons why Gtk may be good for some people.\n\nThey just shouldn't do it on pricing concerns, because we all know that the math doesn't add up. If they intend on making money, I hope they intend on earning more than $10k per year. Otherwise, the project is doomed from the start.\n\nBTW, FreeBSD doesn't use LGPL: they use the BSD license. Next, there are big players using Qt, so saying \"this single problem distracts big players from Qt\" is over-generalisation."
    author: "Thiago Macieira"
  - subject: "One area of a major overhaul"
    date: 2007-03-01
    body: "That would be great if Trolltech could improve startup times of C++ applications.\n\nRight now Linux just sucks in comparison to Windows.\n\nA cold start of WinWord 2003 takes no more than 1,5 seconds on my PC while for KOffice Word this means at least 7-10 seconds."
    author: "Artem S. Tashkinov"
  - subject: "Re: One area of a major overhaul"
    date: 2007-03-01
    body: "I believe a \"cold start\" of Word 2003 on Windows isn't really a cold start at all..."
    author: "Kray"
  - subject: "Re: One area of a major overhaul"
    date: 2007-03-01
    body: "I believe a large part of the problem with startup times of C++ applications is the fault of the linker and GCC (in regards to linking).  The hidden visibility (and inlines) compile option in GCC really helps nicely with startup times (I use it with KDE/Qt on my Gentoo box, the amount of linking that the linker reportidly had to do dropped dramatically), as well as various other tools (like prelink/bdirect flag), though I believe theres far more the linker/compiler can still do to help.\n\nI agree with Kray and don't believe the 'cold start' times of office and many other Microsoft apps (ESPECIALLY internet explorer) are truly cold starts.  Also I think Qt4 based apps generally have a shorter start up time on average as compared to Qt3 versions (I don't have any links or tests to prove it though)."
    author: "Sutoka"
  - subject: "Re: One area of a major overhaul"
    date: 2007-03-01
    body: "If you have a recent enough binutils (as in, not yet released at the time of this writing) and gcc 4.1 or better, you can turn the -reduce-relocations in the Qt configure script.\n\nThat drastically reduces the load time of Qt and applications linking to it. The reason that you need a recent enough binutils is because it requires a feature that was added per request from Trolltech developers.\n\nSo, Trolltech is contributing to improving the load times of C++ programs."
    author: "Thiago Macieira"
  - subject: "Re: One area of a major overhaul"
    date: 2007-03-01
    body: "> If you have a recent enough binutils (as in, not yet released at the time of this writing) and gcc 4.1 or better, you can turn the -reduce-relocations in the Qt configure script.\n\nI have googled a lot but I still cannot understand where -reduce-relocations comes from. Is it an option of new gcc/ld/or what?"
    author: "Artem S. Tashkinov"
  - subject: "Re: One area of a major overhaul"
    date: 2007-03-30
    body: "Should it be the features described in these threads?:\n\nhttp://gcc.gnu.org/ml/gcc/2007-01/msg00305.html\nhttp://gcc.gnu.org/ml/gcc/2007-01/msg00342.html\nhttp://gcc.gnu.org/ml/gcc/2007-01/msg00363.html\n"
    author: "yg"
  - subject: "Re: One area of a major overhaul"
    date: 2007-03-03
    body: "How about this optimization? Any distros using it ? :-)\n\nhttp://lists.kde.org/?l=kde-optimize&m=115867874021537&w=3\n\nFrom TFA:\n\nsince about a week, the GNU binutils have gained support for reducing \nintra-shared lib relocation overhead. Via a special flag, you can ask the \nlinker to resolve relocations any uninteresting or only a certain set of \nsymbols always internally. This is a significant gain for C++, where you \nnormally don't make use of LD_PRELOAD's that overwrite C++ mangled symbols. \n\nI've tested this against libqt3-3.3.6, and it reduces the symbol relocations \nby about 60%, reducing library footprint by about 8% and improving relocation \nstartup time by far more than 50%. \n\nOn a fully internally linked KDE 3.x system, I measured a relocation speedup \nof something between 20-40%. I've not done login time benchmarking. \n"
    author: "yves"
  - subject: "Re: One area of a major overhaul"
    date: 2007-03-06
    body: "obvious question: why dont all distro's use this... ?"
    author: "ptrV"
  - subject: "Re: One area of a major overhaul"
    date: 2008-05-31
    body: "Any more information about this feature?"
    author: "a.c."
---
<a href="http://www.trolltech.com">Trolltech</a>, the Norwegian company behind 
the Qt toolkit has become the second Patron of
KDE. Trolltech itself should not need an introduction, since they
have worked together with the KDE project since its inception ten years ago. Knut Yrvin,
the community manager for Trolltech points out that "<em>KDE does an excellent job of making
UNIX-based desktops easy to use. Trolltech gains from feedback, bug reports and
the spread of Qt through the success of KDE</em>".




<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right;">
    <img src="http://static.kdenews.org/jr/trolltech.png" width="264" height="46"/>
</div>

<p>Being a <a href="http://ev.kde.org/supporting-members.php">Patron of KDE</a> 
is an ideal way to both support the KDE project and become a more
active member of the KDE community. After the inaugural 
<a href="http://dot.kde.org/1160932072/">membership of Mark Shuttleworth</a>, 
Trolltech is the first corporate Patron of KDE.</p>

<p><em>"Being a Patron of KDE should help KDE when organising developer gatherings and 
organising the voluntary effort done by thousands of developers"</em>, Yrvin
continues. Trolltech's involvement with KDE is widely-acknowledged, the company
providing the Qt toolkit, a high performance, cross-platform developer framework
which is distributed as Free Software under the terms of the GPL license.</p>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left;">
    <img src="http://static.kdenews.org/jr/patron_small.png" width="91" height="24"/>
</div>

<p>Patrons of KDE may display the "Patron of KDE" logo on their website and any
other material for as long as they are a Patron of KDE and will be listed on the
KDE e.V. website, if they so wish. This is the highest level of membership
available within KDE e.V., and will allow KDE e.V. to continue its work
supporting and maintaining the structures of development.</p>

<p>Of course, aside from financial matters, sponsors of KDE are a vital part of the
vibrant community outreach and relations scheme - feedback from all our
supporters helps to shape our shared goals and future development.</p>

<p>KDE e.V. wishes to thank all its current supporters, and would like to invite
all interested parties to help us continue to serve the KDE community.</p>



