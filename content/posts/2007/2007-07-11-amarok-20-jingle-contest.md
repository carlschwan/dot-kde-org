---
title: "Amarok 2.0 Jingle Contest"
date:    2007-07-11
authors:
  - "lpintscher"
slug:    amarok-20-jingle-contest
comments:
  - subject: "a jingle competition"
    date: 2007-07-11
    body: "because really, splash screens aren't irritating enough"
    author: "..."
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "Right :) We're also looking into animated splash screens.\n\n\nThrow your love around\nLove me, love me\nTake it into town\nHAPPY, HAPPY!\n"
    author: "Mark Kretschmann"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "Right. And that's one of the reasons I don't even have Amarok installed. There's plenty of great KDE players that just do their job and not try really hard to annoy you."
    author: "hehe"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "Well, I just wonder why thousands maybe million others use it... maybe they are just all silly?! *shrug*"
    author: "apachelogger"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "Because they have different tastes? You know, \"disagreeing\" doesn't equal \"you're wrong and stupid\"..."
    author: "Niels"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "Because it's extremely difficult to turn the splash screen off...\n\nIt's amazing what people will complain about."
    author: "Joergen Ramskov"
  - subject: "Re: a jingle competition"
    date: 2007-07-12
    body: "It's also amazing that people think their application needs a splashscreen. \n\n\"Hey, how about making it start up instantaneously?! Then we wouldn't need a freaking splashscreen. Naaah, let's just look for a new jingle.\"\n\nWhat the hell..."
    author: "santa clown"
  - subject: "Re: a jingle competition"
    date: 2007-07-14
    body: "The splash screen can be turned off, the jingle from what I hear will only play during initial startup and can be turned off. If you're put off by such tiny additions to an app, you're really failing to consider the bigger things it has to offer."
    author: "Koshu"
  - subject: "Re: a jingle competition"
    date: 2007-07-15
    body: "I agree with your assessment concerning the splash screen and the jingle, but I have yet to find a setting to turn of that nasty animation in the playlist where the selection indicator would change color all the time..."
    author: "Andre"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "I'm not sure I notice Amarok's splash screen at all. It starts up automatically when I log in, along with Kontact and Kopete. It's not like it uses a whole lot of resources when you aren't using it. Or maybe it does, but I hardly notice it with my fairly moderate 1 GB of RAM."
    author: "Spank Me"
  - subject: "Re: a jingle competition"
    date: 2007-07-12
    body: "Because clearly one should dismiss the greatest music player on any platform because of a splash screen than can easily be turned off."
    author: "T. J. Brumfield"
  - subject: "Re: a jingle competition"
    date: 2007-07-14
    body: "Yes, because one minor annoyance that lasts momentarily and can be switched off is enough reason to not use an entire application. Oh well, your loss :P"
    author: "Koshu"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "The first option on the first tab of amarok's configuration dialog is a checkbox for \"show splash screen on startup.\""
    author: "happy amarok user"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "Yes, but whiners don't actually look for those things :-P"
    author: "Shadowfiend"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "Be glad Amarok is a KDE program.  If it were a GNOME program, you wouldn't even be able to turn off the splash screen let alone be part of the process for making the splash screen."
    author: "Matt"
  - subject: "Re: a jingle competition"
    date: 2007-07-11
    body: "You should be happy, because since I updated my Mandriva Spring, the splash is all I see of Amarok before it crashes :-)"
    author: "G\u00f6ran Jartin"
  - subject: "Re: a jingle competition"
    date: 2007-07-14
    body: "Now if you're lucky, you will also be able to hear a jingle!"
    author: "zonk"
  - subject: "Re: a jingle competition"
    date: 2007-07-17
    body: "I have Mandriva 2007 Spring and Amarok works great!.\n\nAs it starts along with KDE, will I hear kde jingle and amarok jingle toghether? :p\n\nIf it keeps being shuch a good audio player, I don't care if they put a movie when it starts."
    author: "Diego Bello"
  - subject: "Do people with complaints..."
    date: 2007-07-11
    body: "...just sit around refreshing the dot waiting to find a new article to complain in? Almost every article these days, the first post is almost always some inane complaint. \n\nIts tiring me out and I'm not even a KDE dev (yet... I'm working on changing that) I feel sorry for the people who put in all this hard work and seem to get nothing but stupid complaints (come on, really you don't use an application because of a splash screen? that can be turned off!)\n\nI can't wait to hear how some of these jingles turn out. I assume this is the track that will be default loaded the first time the Amarok2 is run?"
    author: "Borker"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-11
    body: "Yes. Right now it's a intro spoken by Matthias Ettrich and we want something new for Amarok 2."
    author: "Lydia Pintscher"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-11
    body: "ahh, now you mention it I do remember the original one :) Maybe someone can do a Matthias remix."
    author: "Borker"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-11
    body: "I'm Matthias Ettrich, and I invite you to check-acheck-acheckitout!  *some beatboxing here*"
    author: "Leo S"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-11
    body: "Say no to beatbox, say no to funk and trance, hail Richard Wagner.\n\nWhy not a Die Walk\u00fcre remix?\n\nHojotoho! Hojotoho!\nHeiaha! Heiaha!\nMatthias Ettrich\nHojotoho! Heiaha!\nK3B K3B\nDrum sieh, wie den Sturm du bestehst:\nich Lustige lass' dich im Stich!\nHojotoho! Hojotoho!\nHeiaha! Heiaha!\nHeiahaha!"
    author: ":-)"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-14
    body: "Actually, you could make a techno remix of that! xDDDDD"
    author: "zonk"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-12
    body: "Have you heard the techno remix of RMS singing the GNU song?  It's probably best that we <i>don't</i> have a Matthias remix for 'rok 2.0.... ;-)"
    author: "Adrian Baugh"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-12
    body: "I suggest the halleluia chorus from Handel's messiah, just with the words \"Amarok 2\" instead of the last halleluiah.\n\nHa-lleluia, Ha-llelia! Halleluia, Halleluia, Ama-rok 2!!!!!!"
    author: "Jordan K"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-12
    body: "+1"
    author: "nick"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-14
    body: "Or yet better - Lordi! \"Hard Rock Hallelujah\"!\n\nAaa! Maaa! Rok Hallelujah!! xDDDD"
    author: "zonk"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-12
    body: "This shouldn't be an absolute.\n\nComplaining about trivial things (such as things you have the option to turn off) seem pretty contrary to me.\n\nHowever, in the digests, I think people should have the right to express positive and negative sentiments towards changes that are occurring.  I believe constructive criticism can be made vastly more tactfully than it has been, but that doesn't mean that we should only heap positive praise on the project.\n\nI love KDE.  I try to convert everyone I can.  I am extremely grateful that I receive this product free, and that it keeps getting better.\n\nHowever, unless users voice what they are looking for, developers don't necessarily know."
    author: "T. J. Brumfield"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-12
    body: "I have nothing against negative opinions about things, its just the recent influx of comments that are basically a one comment to the effect of '{whatever} sucks'. Critical assessment is an absolutely essential part of a community process, but it should be discussed and supported by useful arguments, not just spewed out and walked away from. \n\nA good example are some of the comments on the recent blog post about the start of a cursor theme for oxygen http://blog.ruphy.org/?p=16 If you look at Troy's comment, it is critical of an aspect of the theme but he supports his comment with a reasoned argument."
    author: "Borker"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-12
    body: "I am with you there. Sometimes, it concerns me that so many clueless people put so much time into bitching and whining and so little time into actually getting informed. Putting up this kind of FUD here puts off developers, it doesn't improve things.\n\nOn the other hand, I think it's quite cool we allow comments on our primary news outlet :>"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Do people with complaints..."
    date: 2007-07-12
    body: "\nI don't really find that it is a symptom of the dot, but of the internet as a whole.  It is really difficult to find any thread on the internet without anger, elitism, and other disrespectful speech.  As you move to more hard-core forums, the disrespectful speech reaches massive proportions.  I find it odd...you would think geeks could have intelligent conversations without reverting to calling each other \"fanboys\".\n\nBut I have to say...compared to other geek forums, I am actually more amazed at the amount of love and kudos that KDE *does* receive on the dot. I guess that is the bright side."
    author: "Henry S."
  - subject: "concerning amarok"
    date: 2007-07-12
    body: "Hi, talking about Amarok : did anyone else observe that amarok tries to send information with kmail without user (a.k.a. me) agreement ??\nI noticed that because Amarok was trying to open kmail... but kmail is not installed!"
    author: "David"
  - subject: "Re: concerning amarok"
    date: 2007-07-12
    body: "Amarok only tries to open kmail when it crashes, so you can send an stacktrace to the devs which may help tracing the origin of the crash. At least, AFAIK."
    author: "Fede"
  - subject: "Re: concerning amarok"
    date: 2007-07-12
    body: "Yes, and the user has to press the send-button by himself and can add additional comments to the mail. The backtrace itself is completely visible, so nothing is sent without asking the user before."
    author: "Sven Krohlas"
  - subject: "Re: concerning amarok"
    date: 2007-07-14
    body: "I would like to get that kmail crash thing as optional to get turned off. I dont like that when amarok crash (when or if) it starts opening kmail what takes a while for some reason. I like to send those but not always......."
    author: "Fri13"
  - subject: "Re: concerning amarok"
    date: 2007-07-16
    body: "Dive in the code and change it for yourself. It's such a trivial thing it's a waste of time and user interface space to turn THAT into an option... How often does Amarok crash anyway, and you'll get OR the KDE crash dialog OR the kmail one - which only takes slightly longer to start than the normal crash handler."
    author: "jospoortvliet"
  - subject: "Re: concerning amarok"
    date: 2007-07-12
    body: "It just creates a new message, with the backtrace, and a description for the user saying what the purpose is. The user is free to send it, or not. "
    author: "someone"
  - subject: "Re: concerning amarok"
    date: 2007-07-13
    body: "And well, I sent it when my Amarok 1.3 kept krashing, and it's fixed in 1.4 :)"
    author: "kollum"
  - subject: "I have already mixed mine"
    date: 2007-07-16
    body: "Developers Developers Developers Developers..."
    author: "zncdr"
  - subject: "cool"
    date: 2007-07-17
    body: "will have to have a go at that jingle comp!\n\nas for Amarok - it's cool - I love it - splash/jingle/none - doesn't matter!! (but maybe even nicer if you got a choice of jingles/splashes for those that don't like the one you're going to choose!!!)\n\nthe only problem I have with it is the visualisations - tend to make Amarok very unstable - but I'm only using on-board video - so maybe that's it!\n\nKeep up the good work\ndil23\n:)"
    author: "dil23"
  - subject: "Amarock 2.0 JINGLE"
    date: 2008-04-07
    body: "Ama-roks!!!"
    author: "brandon stroud"
---
The <a href="http://amarok.kde.org/">Amarok</a> team needs your help. Amarok are <a href="http://amarok.kde.org/en/node/238">looking for a new, shiny and fresh jingle</a> to play at first start of Amarok 2.0 and are holding a contest to find one. <a href="http://magnatune.com/">Magnatune</a> and <a href="http://ccmixter.org/">ccMixter</a> have generously offered their help to get this going. The Amarok project will award the winner with fame, glory and a load of cool prizes, including $100 US dollars in cash and cool Amarok swag. The 2 runners up take away some swag as well. Read on for the rules of the contest.





<!--break-->
<p>So take some songs from the Magnatune pool, get mixing and submit your jingle at <a href="http://ccmixter.org/media/thread/1095">ccMixter.org</a></p>

<p><i>Judgment will be done by a totally-biased panel of Amarok people!</i></p>

<h4>A few boring words on licensing...</h4>

<p>As this jingle will be distributed along with Amarok 2.0, by non-commercial as well as commercial distributions, the jingle needs to be released under the <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons by-sa license</a>.</p>

<p>Magnatune has allowed Amarok jingles using their samples to be released under this license. However if you use samples from any other source, they  need to be under the correct licence or Amarok cannot use it.</p>

<p>Good luck in your efforts to help us make Amarok 2.0 rok the experience of music lovers everywhere!</p>




