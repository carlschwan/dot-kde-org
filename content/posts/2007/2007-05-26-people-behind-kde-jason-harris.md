---
title: "People Behind KDE: Jason Harris"
date:    2007-05-26
authors:
  - "dallen"
slug:    people-behind-kde-jason-harris
comments:
  - subject: "KDE America :)"
    date: 2007-05-26
    body: "So Jason, it seems you and I agree on this 'needing a north american event' thing.  Perhaps if dannya keeps interview from this side of the pond for a few weeks, we'll discover that there are in fact more than a dozen of us over here :) \n\nI nominate Wade to organize the event... Minnesota is pretty central :P"
    author: "Troy Unrau"
  - subject: "Re: KDE America :)"
    date: 2007-05-26
    body: "Agreed I'd love one a bit closer to home. I'm in Iowa myself, can't get much more central than that."
    author: "Stephen"
  - subject: "Re: KDE America :)"
    date: 2007-05-26
    body: "If you want to see more than corn, stick with MN ;)"
    author: "Narg"
  - subject: "Re: KDE America :)"
    date: 2007-05-26
    body: "naturally the subject comes up as soon as I leave the continent... since I (normally) live in vancouver, all the KDE events in europe are a bit far away. being on a student budget again, even going somewhere in america might be a bit far... but I won't be home for a couple of years, so I guess it doesn't matter to me for now."
    author: "Chani"
  - subject: "Re: KDE America :)"
    date: 2007-05-26
    body: "I think it would be good to have an American KDE event for the American users... The developers can get to Europe with financial help from the KDE EV, but users obviously not."
    author: "superstoned"
  - subject: "Re: KDE America :)"
    date: 2007-05-26
    body: "Only one thing for it Chani, you'll have to organise a KDE Asia event..."
    author: "Bille"
  - subject: "Re: KDE America :)"
    date: 2007-05-26
    body: "Well, I have been volunteering for ages to organize an US KDE event. I think I didn't do so bad last year with the aKademy so a local event would even be easier. ;-)\n\nThere is a kde-usa-event mailinglist BTW.\n\nFor those of you who do not know it, Waldo and I are located in Portland, OR. Eric   from Quanta is in Hillsboro, OR and I know Charles lives in the Bay Area, Theobroma lives also in the Pacific NorthWest so there might be more than you think. ;-)\n"
    author: "Tink"
  - subject: "Re: KDE America :)"
    date: 2007-05-26
    body: "I live in Quebec, Canada. Of course aseigo live in Calgary. I know that Matt Rogers live in Texas. They are many KDE developers in North America :)"
    author: "Micha\u00ebl Larouche"
  - subject: "Re: KDE America :)"
    date: 2007-05-26
    body: "Since this is the best idea I've heard all day, and I presume it's a low traffic list, I'll bite :)\n\nCheers"
    author: "Troy Unrau"
  - subject: "nx is *not* a kde application"
    date: 2007-05-26
    body: "it's nice to see jason listing nx amongst his favorite applications. \n\nhowever, nx is __not__ a kde application. in fact, nx has been snubbed by kde developers very much so. for whatever reason..."
    author: "nxfanboy"
  - subject: "Re: nx is *not* a kde application"
    date: 2007-05-26
    body: "I think that neither of  the last four ( emacs, the Gimp, Hugin, NX ) are KDE apps :)"
    author: "kollum"
  - subject: "Re: nx is *not* a kde application"
    date: 2007-05-26
    body: "The question was about my favorite apps, not necessarily favorite KDE apps :)\n\nI'm not sure what you mean when you say nx has been snubbed by kde developers..."
    author: "LMCBoy"
  - subject: "Re: nx is *not* a kde application"
    date: 2007-05-26
    body: "> The question was about my favorite apps, not necessarily favorite KDE apps :)\n\nWell, at least in the written interview the title says \"Fav. KDE applications:\".  Maybe just a misunderstanding ... \n\n\n> I'm not sure what you mean when you say nx has been snubbed by kde developers...\n\nNeither am I.  Especially since George Wright did his Google SoC 2006 project on NX: http://blog.gwright.org.uk/articles/2006/5\n\n\n"
    author: "cm"
  - subject: "Re: nx is *not* a kde application"
    date: 2007-05-26
    body: "> Well, at least in the written interview the title says \"Fav. KDE applications:\". Maybe just a misunderstanding ... \n\nAh, yes.  In the interview email template, it just said \"favorite applications\".  oops :)\n"
    author: "LMCBoy"
  - subject: "Re: nx is *not* a kde application"
    date: 2007-05-27
    body: "Sorry, this is my fault ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "we need 3d space"
    date: 2007-05-26
    body: "we need more 3d visuals like space lets work on bringing 3d desktops in order thanks from shreveport la."
    author: "tony"
  - subject: "Thank you!"
    date: 2007-05-29
    body: "Just a note of thanks, particularly for KStars.  I enjoy using it with my son, as we have recently started attending a local astronomy group.  Only a couple of weeks ago, it helped me spot Mercury for the first time - just above the moon at the time!  Thank you, and keep up the good work!  And enjoy married life!"
    author: "David Joyce"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we remain in North America, down to the deserts of Arizona to meet an astronomer who uses his work expertise to bring the galaxy to our desktops - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/harris/">Jason Harris</a>.
<!--break-->
