---
title: "aKademy 2007: The Second Day"
date:    2007-07-05
authors:
  - "jpoortvliet"
slug:    akademy-2007-second-day
comments:
  - subject: "Really"
    date: 2007-07-05
    body: "great article! Thanks."
    author: "Ljubomir"
  - subject: "Re: Really"
    date: 2007-07-07
    body: "My pleasure. I like writing the articles, but it's also good to know they're appreciated. After all, it is a lot of work, most ppl don't seem to realize that. TNX to danny as well, he does the final editing and putting it on the dot (another of those things which take more time than you would expect)."
    author: "jospoortvliet"
  - subject: "Colorblindness"
    date: 2007-07-05
    body: "Guess it would be possible have something like a daltonization compositing plugin, to help our friends with less colorful environment to get better contrast. Any one with that kind of skill?"
    author: "}('-'){"
  - subject: "Re: Colorblindness"
    date: 2007-07-13
    body: "It would be very easy to manipulate colors as they are drawn, using fragment shaders. Though someone should first come up with an idea WHAT manipulation exactly should be done? Personally, I'm clueless :)."
    author: "zonk"
  - subject: "eyecandy.jpg"
    date: 2007-07-05
    body: "The link to the eyecandy.jpg image at the end of the \"Josef Spillner, GHNS2 and Games\" section seems to be broken.\n\nI guess it's not supposed to link here... :-)\nftp://ftp.kde.org/pub/kde/unstable/icons/eyecandy.jpg\n"
    author: "DaveC"
  - subject: "Re: eyecandy.jpg"
    date: 2007-07-11
    body: "yay, good catch :D\nkde 4 returns to roots !"
    author: "Richlv"
  - subject: "Videos"
    date: 2007-07-06
    body: "The \"sheets and videos\" page is great. You really get a good overview of the talks that take place, the people giving the presentations and their subjects, and videos and sometimes even PDFs!.\n\nThere is only a problem. The videos are big, as in HUGE. I have broadband, but most talks weight in at over 100MB! How did you code them? 0 compression? They don't have to be ultra DivX compressed to 2MB videos, but come on, 150MB is way too big.\n\nSo please if that could be addressed Akademy coverage would be nearly perfect."
    author: "tobami"
  - subject: "Re: Videos"
    date: 2007-07-06
    body: "Ah! I downloaded first the Sonnet talk, being only 26MB, and it was... well, a bit underwhelming would be too nice. While Zack is introduced in substitution of Jacob, he actually doesn't even get to begin with the talk before the video ends!. You just see how he setups the overhead projector and his laptop.\nI think someone ought to remove or substitute that video.\n"
    author: "tobami"
  - subject: "Re: Videos"
    date: 2007-07-08
    body: "Also, 1-18-Unlisted.ogg is actually Stefan Teleman's Solaris talk."
    author: "Kevin Kofler"
---
<a href="http://akademy2007.kde.org/">aKademy 2007</a> continues! Sunday, the second day of the conference, brought more talks covering a wide diversity of topics. Read on for the Sunday aKademy 2007 Report.

<!--break-->
<p>Sunday was very busy and interesting, and we regret that we were not able to attend and cover all talks. Yet, we reported some of the most interesting. Luckily, you will be able to find <a href="http://akademy2007.kde.org/conference/programme.php">sheets and videos</a> of the talks on the <a href="http://akademy2007.kde.org/">aKademy 2007</a> website.

<h3>Lubos Lunak about Compositing in KWin</h3>

<p>Lubos starts by explaining that his talk will comprise two parts: first he will cover the 'boring' part, then continue talking about the 'other' part. Starting with the boring stuff, he explains how compositing works and what it is useful for.</p>

<a href="http://static.kdenews.org/dannya/akademy_lubos.jpg"><img src="http://static.kdenews.org/dannya/akademy_lubos_small.jpg" alt="Konqi composited over other windows" title="Konqi composited over other windows"></a>

<h4>The 'Boring' Part</h4>
<p>Normally, windows are asked by X to paint only the parts which are actually visible on-screen. Parts of windows which are obscured by other windows or outside the visible screen area are not painted. In compositing mode, all windows are always fully painted to an off-screen bitmap, and then painted on the screen. The composition manager can do something with the windows before they are painted, like add shadows, transparancy, rotate them, let them explode or draw them multiple times. Also, windows which are not painted for some time can still be moved around, without turning into a white blob - the window contents are still in the off-screen buffer. Windows also don't need to repaint all the time when you move them. Of course, there are disadvantages - each window needs to actually be in the off-screen buffer, which uses video memory. And the drawing process can be slower as well, even if all effects are turned off.</p>

<h4>The 'Other' Part</h4>
<p>So, how to use all this? Technologies like XGL and AIGLX are used to do the compositing. From Lubos' point of view, they are equivalent - just with different bugs. Lubos started the kwin-composite project in a seperate branch, as he expected to de-stabilize KWin greatly. Things turned out to be a lot better, and in hindsight, he wouldn't have needed the seperate branch. The compositing stuff doesn't effect the 'normal' mode of KWin in any significant way.</p>

<p>KDE 3.x already had compositing, based on the (not so powerful) XRENDER extension of X.org. With the composite, XGL and AIGLX technologies, real compositing is now possible, and several experiments are going on in this area - think of Compiz and Beryl. They mostly focus on fancy effects, and Lubos decided it was better to add compositing to a mature window manager than window manager to a compositor - hence the work on KWin. Lubos then goes into some rather technical details, and then points out the current status. There is a mostly-stable API for the effects, which makes it pretty easy to write plugins - you often don't even need to know OpenGL or XRENDER stuff. He shows us a zoom-in example created from 50 lines of code. KWin itself is mostly stable, and there are some cool plugins, but there is also a lot of work to do on the actual look-and-feel. Lubos wants to focus on the usable, rather than primarily 'cool', and he needs some help writing useful plugins. So if anybody with graphic design skills is willing to give him a hand, Lubos would be appreciative!</p>

<h3>Josef Spillner, GHNS2 and Games</h3>

<p>Josef held a talk about collaborative data sharing and briefly touched on games.</p>

<p>Currently, KDE has the <a href="http://ghns.freedesktop.org/">GetHotNewStuff</a> framework, which is capable of uploading and downloading user data. Most applications only use it for downloading, though. The framework is missing several needed features, so for KDE 4, work has started on a GHNS2. This is being done in close co-operation with FreeDesktop.org, though unfortunately GNOME isn't particularly active in this specific area of the platform.</p>

<p>Currently, GHNS2 consists of over 3500 lines of code on the server side, offering all the necessary infrastructure for handling uploading and downloading, metadata (like ratings), and even has a versioning system. It is not yet complete, as there are a lot of additional functionality requests from application developers, but the latest SVN version has already incorporated most ideas, and the GHNS2-hackers are looking for more input and comments. Feedback from the <a href="http://edu.kde.org/">KDE-Edu</a> and <a href="http://games.kde.org/">KDE-games</a> developers has already proven very valuable, and a lot of work has been done on account handling, OpenID integration and security. There are also initial proposals for desktop-wide single-sign-on, so we'll see a lot of great things in these areas throughout the KDE 4 lifecycle.</p>

<p>Of course, on the client-side, work has been done to support all new server-side things, as well as creating an easy-to-use API and a flexible configuration system. Josef explains that the needs of applications can differ enormously, thus such configuration elements are important, as is the simple API.</p>

<p>Currently integrated and (mostly) working are uploading and downloading, with some dialogs and buttons already available to use in the interfaces of applications. The backend stuff is mostly ready, but there is still a lot of work to do in the GUI area. Josef also tells us there will be close Plasma integration, and some Plasma-related work has already been developed.</p>

<p>He continues to talk about the specifics of the architecture and the API. As said before, the API has been carefully designed to be as easy as possible, and almost all basic tasks can be done in just one line of code, yet the system allows for very fine-grained control for those who need it.</p>

<p>Another new feature is the sharing of data between applications, made possible by the seperation of GHNS2 and application configuration. Josef continues to show us screenshots of several predefined interfaces, including cool 'tag clouds', and explains how they developed a a 'daemon' mode. This allows for web services and Strigi integration.</p>

<p>According to Josef, the community around GHNS2 is very active and innovation is happening at an impressive pace. By closely collaborating with the applications which need GHNS, great ideas are turned into code quickly. He calls for even more ideas, so everybody who can potentially make use of the GHNS2 framework are urged to get into contact!</p>

<p>Josef uses the last few minutes of his talk to tell us about the GGZ system being built by the KDE Games community. Their idea is to allow casual online gaming: simple, good-looking and addictive games, easy to play online against other people. To consolidate the protocols and file formats, they are again working with FreeDesktop.org to ensure GNOME apps will be able to interoperate and play with the KDE apps. Encouraging multiple implementations will ensure the API's are sane, and allow for a lot of progress in a relatively short time. The foundations are laid, the big work left to do is on the GUI: creating widgets for the new features, and integrating them into the applications.</p>

<img eyecandy.jpg> Eyecandy for KDE...

<h3>Towards a Semantic KDE</h3>
<p>Tudor Groza, a researcher from <a href="http://www.deri.ie/">the Digital Enterprise Research Institute</a>, explained the technical and social aspects of the Semantic Desktop efforts at the Nepomuk project. Starting with the history of semantic research (which started in 1945!), he explained that technical limitations ensured that the social desktop has not yet arrived. But now most basic of the technology is in place, like Wiki's, social networks and the World Wide Web, so it's time to start integrating these things.</p>

<p>Tudor went into the architectural details of the semantic desktop, focusing on the RDF technology and explaining what ontologies are. Ontologies are shared conceptualisations, defining concepts, relations and properties. RDF is the method of expressing them, and it is actually very simple. A RDF statement consists of triplets, each having a subject, a predicate and an object. For example, you can have:
<br>
<br>Subject: John
<br>Predicate: Age
<br>Object: "40"
<br>Or:
<br>Subject: John 
<br>Predicate: Likes 
<br>Object: Mary</p>

<p>The difficult thing, and the reason for more research, is sharing this knowledge, a social semantic web. Getting from personal information management through distributed information management to a real social network is a huge step. We do have the seperate technologies for this, like p2p applications, social networking sites, natural language processing tools - but we need to combine them. We now have a Semantic Desktop, but in time, it will evolve into a Social Semantic Desktop.</p>

<p>This has the potential to accelerate collaboration, people can maintain shared views in a global network, but also create sub-communities. It will further the sharing of knowledge and data, which is crucial in this era of information explosion, but also a growing gap between the light of knowledge and the darkness of misinterpretation.</p>

<p>The Nepomuk project, funded with 11.5 million euro by the European Union, consists of 16 different organizations. These range from big, commercial players like IBM, through to universities and smaller players like Mandriva - and of course, the KDE project. Their goals are defining, standardizing and implementing metadata creation and sharing.</p>

<h4>Nepomuk-KDE</h4>

<p>Tudor showed a few mockups and screenshots of what should be there and what is, and then delved further into what they are building for KDE. The foundation is there now, after the creation of a shared metadata storage with standardized ontologies. Work has begun on basic integration in some KDE applications and getting the Nepomuk technologies into the KDE core libraries. Then tools to analyze and search metadata have to be written, and finally work will begin on linking data from several sources and sharing the metadata.<p>

<h4>Conclusion</h4>

<p>The Nepomuk developers enjoy working with the KDE community - according to Tudor, we are a very open group of people. Yet, introducing new technology goes slowly, and people can best be convinced to use it if it solves current problems, rather than just providing something which is part of some distant vision.</p>

<p>On Thursday, a BoF session will be held, talking about the usage and implementation of Nepomuk in KDE applications, and several hackers expressed interest in attending.</p>

<h3>Other talks</h3>

<p>There where many other talks, ranging from KDE platforms talks by Holger Schröder (Windows), Benjamin Reed (Mac OSX) and Stefan Teleman (Solaris), talks about several distributions like Gentoo, Fedora and Kubuntu and a lot community talks. You can find <a href="http://akademy2007.kde.org/conference/programme.php">sheets and videos</a> of the talks on the <a href="http://akademy2007.kde.org/">aKademy 2007</a> website. Don't forget to check it out!</p>

<h3>aKademy Awards</h3>

<p>At 6 pm, the aKademy Awards ceremony started, presented by 2 of <a href="http://dot.kde.org/1159194107/">last years winners</a>, Laurent Montel and Boudewijn Rempt.</p>

<p>Like Saturday, it was a productive and interesting day, and the hackers are looking forward to the rest of aKademy. The network started to work around 2 pm, during the <a href="http://dot.kde.org/1183385741/">keynote by Jim Zemlin</a>. Immediately, many became connected to cheers and applause.</p>
