---
title: "KDE Commit-Digest for 4th November 2007"
date:    2007-11-07
authors:
  - "dallen"
slug:    kde-commit-digest-4th-november-2007
comments:
  - subject: "Here we go again."
    date: 2007-11-07
    body: ">>changing slab thickness to 0.45 as 30 people confirmed on nuno's blog :)\n\nThere go those Oxygen devs doing things the \"closed\" way again! ;-)"
    author: "Louis"
  - subject: "Re: Here we go again."
    date: 2007-11-07
    body: "hmmm, perhaps you should write a half-assed blog entry on this subject and then pretend it's journalism and publish it in an online magazine. You'd be doing community a solid one!"
    author: "Borker"
  - subject: "Re: Here we go again."
    date: 2007-11-07
    body: "I'd like to point out that the original post appears to be a joke, but the reply is taking it seriously"
    author: "Vladislav"
  - subject: "Re: Here we go again."
    date: 2007-11-07
    body: "I'd like to point out that the original post appears to be a joke, but the reply is taking it seriously"
    author: "bluestorm"
  - subject: "Re: Here we go again."
    date: 2007-11-07
    body: "i like the humour on the dot."
    author: "anonymous coward"
  - subject: "Re: Here we go again."
    date: 2007-11-07
    body: "hehehhe we are pretty closed arent we :)"
    author: "nuno pinheiro"
  - subject: "Re: Here we go again."
    date: 2007-11-07
    body: "Psh, artists... ;p"
    author: "Matt"
  - subject: "Raptor"
    date: 2007-11-07
    body: "Really like the ideas presented here, though I like the simple kmenu from kubuntu, I dislike most other efforts in creating a menu, this one seems to look good, and work great, wel ok, only time will tell, but it certainly is promising.\nGood luck chaps and keep up the good work."
    author: "Terracotta"
  - subject: "Just to provide my personal approach"
    date: 2007-11-07
    body: "I'm not too sure about the approach of showing the most used applications in front (it's not specific to Raptor, of course, my remark even applies to Windows). Personnaly, I create short cuts icons in my taskbar and I would otherwise use the menu to get apps I don't use so often. \nI would appreciate more a simple way of draging an app from the menu to the taskbar in order to create a shortcut."
    author: "Richard Van Den Boom"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-07
    body: "I totally agree, but from the way I see it, they are organising the programs in catgories as well, so that you do can choose from categories. It's a bit bizar, but it's at least a bit better done than the suse stuff."
    author: "Terracotta"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-07
    body: "i got the opportunity to use windows XP for a while (job), and i liked the fact that frequently used apps show up in the startmenu for a while: the type of work changed, and i started to need other apps, and the start menu nicely followed. At the time i started to think a shortcut would be handy, it was already there for me to use.\n\non my kubuntu laptop, i still create launch buttons in kicker manually (and at least one of them is for an app i never use anymore)\n"
    author: "ik"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-07
    body: "You have \"most used\" or \"recently used\" applications in the K menu, at least in openSUSE."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-08
    body: "Minor point of semantics to avoid confusion.\n\nIn openSUSE (like KDE 4) they use Kickoff, which has \"favorite\" applications.  I'm not sure if you manually select the favorites, of if it picks them from which you use the most.  The traditional KMenu does not do this that I know of."
    author: "T. J. Brumfield"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-08
    body: "\nIt did on Opensuse.  It even had a built in search.  (this was way before Kickoff)"
    author: "Henry S."
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-08
    body: "With the traditional KMenu, right click on the panel, go to Configure Panel > Menus, under \"Quick Start Menu Items\" select \"show applications most frequently used\" and select a number of apps to show."
    author: "Yuriy Kozlov"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-08
    body: "They are manually chosen favourites.  There's a default set of the usual suspects, then you can right click in the app browser or search results to add to favourites."
    author: "Will Stephenson"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-08
    body: "I use the traditional K menu on openSUSE. (Right click, Change to KDE style menu.)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-13
    body: "One problem with these auto-updating favorites is that you never know the exact localization of the aplication you are launching. You need to search to see in what position the aplication is on favorites before start to move the mouse pointer to clic there. And there is the possibilty that this aplication is not in favorites anymore, what will make you loose even more time.\n\nIn the case of shortcut icons in the taskbar, and the fixed kmenu, you aways know where the aplication is on the menu, and already have some degree of muscular memory to reach it quickly. From time to time it need some reorganization, but that is minor compared to time saved with fixed models. I would be more happy seeing on-menu drag-and-drop suport for editing than all those new concepts of menus... that may be beauty, and good for who don't have good control over the mouse, but are slower."
    author: "Manabu"
  - subject: "Re: Just to provide my personal approach"
    date: 2007-11-07
    body: "You can create panel icons in KDE 3.5 by dragging from the (traditional) K menu to the panel (if it is not locked)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Raptor"
    date: 2007-11-07
    body: "Raptor really reminds me of the Gnome menu in opensuse, which was the reason I don't use gnome, it is far quicker to have a normal menu, click to what you want than to type in and search for it.  Nice work but I'll continue using the suse menu in kde4, it's far superior than this ancient idea of searching, and far less time consuming.  Time is money, and this is just wasting time."
    author: "Richard"
  - subject: "Re: Raptor"
    date: 2007-11-08
    body: "The SUSE menu is also largely predicated on searching, and all the usability experts swear by it, even though I find it the absolute slowest menu interface I've ever used.  To browse to an app normally in SUSE involves a click on the menu, a click on applications, a click on Games, a click on Card Games, and then possibly scrolling to the app, and then clicking on the app.\n\nWhenever I mention how slow this is, I am told I should use the \"Search\" feature, which is also slow.  So I just got in the habit of launching everything with Alt+F2.\n\nRaptor doesn't look completely predicated on searching, though everyone is including search these days."
    author: "T. J. Brumfield"
  - subject: "Re: Raptor"
    date: 2007-11-09
    body: "Searching is not a bad idea and it's sometimes faster that the fastest clicker. This is one of the main reasons why I personally love the Suse Menu. It's also better arranged than the traditional Menu and if I use an application more often then all I need to do is to add it to favorites, with two clicks I am good. You just cant beat that with Alt+F2 no mattter how fast you can type so I don't see the Suse Menu as slow as you described. The only thing that is missing is eye-candy, it's not an attractive Menu but it's very usable.\nI can't say much about Raptor because I haven't used it yet, all that I can say is that it's prettier than the Suse Menu. I would have to use both in order to make a comparison."
    author: "Bobby"
  - subject: "Re: Raptor"
    date: 2007-11-08
    body: "Hello all\n\nI'd like to point out that raptor is not only about search: it also offers the \"traditional\" categories and features a categorisation by means of TOM, so categorising it with TASKS rather than with application groups.\n\nIt will be possible to configure it working with TOM (task oriented menu), Categories (traditional) or any other plugin as default."
    author: "dracorx"
  - subject: "Re: Raptor - Kickoff"
    date: 2007-11-12
    body: "\nKick off already implemented all these things that Raptor is aiming to implement.\n\n\nCould\u00b4n\u00b4t raptor dudes just join kickoof"
    author: "Ivan"
  - subject: "Re: Raptor - Kickoff"
    date: 2007-11-12
    body: "Can Kickoff be placed anywhere?\n\nWill Kickoff do smart searching that places more relevant options more prominently?  Will Kickoff allow for click-less, quick navigation?"
    author: "T. J. Brumfield"
  - subject: "Re: Raptor - Kickoff"
    date: 2007-11-13
    body: "Huuummmm...\n\nLooks like just a little improvements that might be easy to implement in Kickoff. \n\nIt already provides search, that could be improved with integration to Nepomuk...\n\nIt already provides clickless navigation, that could be extended...\n\nI'm not a Kickoff developer, just a user... It already passed the Grandma test for me (I upgraded to last Mandriva version two months ago, switched to kickoff in my mother's, my wife and my sister (diferent ages and homes) computers without warning them, they just started to use it and when I asked about the menu they said \"It changed, no? Really better...\")... \n\nSo I think raptor concept has good ideas, but can't see how this could not be implemented in kickoff.\n\n"
    author: "Ivan"
  - subject: "Raptor"
    date: 2007-11-07
    body: "Yeah go Raptor!\n\nRaptor is the best."
    author: "fooishbar"
  - subject: "Re: Raptor"
    date: 2007-11-07
    body: "I'm not much convinced about that, the best menu is the one\nallowing users start everything with less clicks,\nand actually raptor instead:\n-seems to require more clicks for mouse-only input\nor\n-requires both mouse and keyboard input (or keyboard + arrows)\n\nI hope I'm wrong."
    author: "mark"
  - subject: "Re: Raptor"
    date: 2007-11-07
    body: "Not with less clicks, but faster and less mind stressing.\nwe could do a only over/action menu that woud require zero clicks, but i gess that would be a pain... "
    author: "nuno pinheiro"
  - subject: "Re: Raptor"
    date: 2007-11-07
    body: "I personally think the best menu is different for each user.\n\n-The Run Dialog fits me best, because it's a quick hotkey, and a few characters to run any app. My left hand is usually oon the keyboard anyway. \n-KMenu is tolerable for me, because I can quickly browse it for apps that I don't use often, or forget the name of.\n-Kickoff (when I used it) got in the way with too many mouse-overs, and too much depth to the hiearchy where all previous context is lost. It just doesn't work for me.\n\nI have high hopes for raptor being a better solution for me. I don't mind having to click once to go to productivity apps. A group for games might be nice too, though.\n"
    author: "Soap"
  - subject: "Video in Solid/Kopete"
    date: 2007-11-07
    body: "> Solid gets support for Video(4Linux) devices.\n> Kopete uses Solid for network detection and support of audio/video devices.\n\nWay to go! That would make it much easier to access webcam devices in KDE apps! I never understood why Kopete had to ship with it's own code for V4L support in the first place. Especially if all chat clients need to duplicate that code"
    author: "Diederik van der Boor"
  - subject: "Re: Video in Solid/Kopete"
    date: 2007-11-08
    body: "It still ships a lot of v4l code for actually talking to the devices.  This will go into Phonon at a later date.  \n\nThe new code uses Solid (and its hal-backend) for detecting webcams as they are hotplugged and removed.  "
    author: "Will Stephenson"
  - subject: "raptor image"
    date: 2007-11-07
    body: "raptor image link is broken\nhttp://commit-digest.org/issues/2007-11-28/files/raptor1.png\n\nthe big image is here\n\nhttp://commit-digest.org/issues/2007-11-04/files/raptor1.png"
    author: "elveo"
  - subject: "Re: raptor image"
    date: 2007-11-07
    body: "heeaa beter one here http://www.nuno-icons.com/images/estilo/raptor/main menu.png :)"
    author: "nuno pinheiro"
  - subject: "Re: raptor image"
    date: 2007-11-07
    body: "I can't wait to try both this and Lancelot out.  They both look incredible.\n\nIs there any chance we'll eventually see a theme that more closely resembles some of your early mock-ups and art?  Is there a chance you could design a widget and window-decoration theme to more closely match the Plasma theme and glass borders?\n\nI think your art and icons look fantastic, and Raptor is a breath of fresh air!  Keep up the incredible work!"
    author: "T. J. Brumfield"
  - subject: "solid v4l support"
    date: 2007-11-07
    body: "solid supports video for linux now?! whoa! :)"
    author: "mxttie"
  - subject: "M\u00e9nage \u00e0 trois menus"
    date: 2007-11-07
    body: "I'm actually very positive on the eventual outcome of the current K-menu dilemma -- if you can call it that.   And I don't mean that purely on the perspective that a \"Darwinian survival of the fittest\" will lead us to the adoption of the strongest, most popular choice as the default KDE menu.  Sure, that is also likely, but even more interestingly, a lot of cross-pollination is bound to happen between all these competing ideas.  Therefore, rather than a single \"winner\", I am confident the KDE community will adopt the love child of all these interesting projects!\n\n(Nevertheless, I am also sure we are bound to hear in this thread from the customary legion of complaints -- many bordering on trolling territory.  How foolish they will eventually feel...)\n"
    author: "dario"
  - subject: "Re: M\u00e9nage \u00e0 trois menus"
    date: 2007-11-07
    body: "As long as there is choice, I'm happy."
    author: "T. J. Brumfield"
  - subject: "Re: M\u00e9nage \u00e0 trois menus"
    date: 2007-11-07
    body: "Open Source is all about choice.  KDE 3 has multiple \"third party\" start menus, and there sure will be many for KDE 4, too.\n\nAnd seeing how fast new plasmoids appear, they might even be developed within the 4.0 lifecycle.\n"
    author: "christoph"
  - subject: "Re: M\u00e9nage \u00e0 trois menus"
    date: 2007-11-08
    body: "At first it seemed that we might only have Kickoff at 4.0 and that worried me."
    author: "T. J. Brumfield"
  - subject: "Re: M\u00e9nage \u00e0 trois menus"
    date: 2007-11-08
    body: "Raptor in KDE 4.x\n\nHi all\n\nThe raptor team is damn too small. Therefore it's probably not ready for kde 4.0, I'm afraid. So it is more possible to come with 4.1 or so. Or even better - if you want to join our team so it is sooner ready, contact us on panel-devel, where siraj is reading ;-)"
    author: "dracorx"
  - subject: "Kdebase won't compile"
    date: 2007-11-07
    body: "It's about 7 days that i can't compile kdebase.. What's happening? I hope to see kde 4 compiled from svn soon! "
    author: "Marco"
  - subject: "Re: Kdebase won't compile"
    date: 2007-11-07
    body: "\"What's happening?\"\n\nWell, it's hard to say if you don't tell us what the error is, or what you have re-installed ;) For the record, I compiled it fine yesterday."
    author: "Anon"
  - subject: "Re: Kdebase won't compile"
    date: 2007-11-07
    body: "Come over to <a href=\"irc://irc.freenode.net/kde4-devel\">the #kde4-devel channel on freenode</a>, we'll help out. I have been compiling kde4 from svn once every 3 days, no problem ever."
    author: "Ho\u00e0ng &#272;&#7913;c Hi&#7871;u"
  - subject: "move window: clicking on an empty part and drag."
    date: 2007-11-07
    body: ">Fredrik H\u00f6glund committed changes in /trunk/KDE/kdebase/workspace/krunner:\n>Make it possible to move the krunner window by clicking on an empty part of it >and dragging the mouse.\n>Diffs: 1, 2\n\nIt would be so cool to have this feature also for normal windows!"
    author: "Frank"
  - subject: "Re: move window: clicking on an empty part and drag."
    date: 2007-11-07
    body: "That already exists in X11 in some form. Hold the Alt button and you can move the window by hitting any part of it. Very convenient."
    author: "Diederik van der Boor"
  - subject: "Re: move window: clicking on an empty part and dra"
    date: 2007-11-07
    body: "Yeah, I know.\nOf course I means without holding [Alt]. IIRC Firefox on Windows had such a behavior."
    author: "Frank"
  - subject: "Re: move window: clicking on an empty part and dra"
    date: 2007-11-07
    body: "Some Qt themes support this, IIRC. (But I can't remember Firefox doing this on Windows)"
    author: "LiquidFire"
  - subject: "Re: move window: clicking on an empty part and dra"
    date: 2007-11-07
    body: "> Of course I mean [clicking on an empty part] without holding Alt.\n\nSounds like a really bad idea. It means applications have random hotspots which cause different behavior. Users don't trust computer systems which are unpredictable. Making random spots is just doing that. They won't get it.\n\nThis is one of the things I even have to say *it confuses people* _a lot_. And before you get me wrong, I typically dislike the GNOME approach of usability.\n\nHeck, have you ever seen some stubled faces from a user who \"lost all windows at the desktop\"? They don't know what they did, just that they lost all windows. It was caused by an accidental flip of the scrollwheel at an empty desktop area. I have some \"oh so convenient\" settings enabled for \"switch desktops using the scrollwheel at an empty desktop area\" and \"hide windows from other desktops in the taskbar\". That combination was good enough to confuse smart people.\n\nIt opens your eyes to see intelligent people stumble at my desktop because of this unpredictable behavior. Random moving windows is one of those too. ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: move window: clicking on an empty part and dra"
    date: 2007-11-09
    body: "with compositing and other new tech, you can make these things more intuitive: the scrolling desktops behavior, for example, could use an animation (does in Kwin if compositing is on) so the user sees what is going on."
    author: "jospoortvliet"
  - subject: "Re: move window: clicking on an empty part and dra"
    date: 2007-11-08
    body: "My mouse don't have an ALT button.\nA lot of other buttons, though."
    author: "reihal"
  - subject: "Re: move window: clicking on an empty part and dra"
    date: 2007-11-09
    body: "So why not to bind some of those extra buttons to move window?"
    author: "Mikael Kujanp\u00e4\u00e4"
  - subject: "Re: move window: clicking on an empty part and dra"
    date: 2007-11-07
    body: "I think this is a job for Qt library, not KDE."
    author: "And"
  - subject: "Re: move window: clicking on an empty part and drag."
    date: 2007-11-07
    body: "I got it started\n-> http://lists.kde.org/?l=kde-core-devel&m=119445642928312&w=2"
    author: "logixoul"
  - subject: "Re: move window: clicking on an empty part and dra"
    date: 2007-11-07
    body: "Wow, thats great. :D"
    author: "Frank"
  - subject: "Oxygen"
    date: 2007-11-07
    body: "Forgive me for reposting basically the same comment from a week ago, but I posted the comment so late, I'm not sure if anyone saw it, and I certainly wouldn't mind seeing a dev's response/take on the issue.\n\nThere was an Oxygen website early, and thusly I assumed there was going to be a unified artistic vision from the early stages.\n\nThe glass border around the plasmoids looks great, but in turn, the window decorations around the actual Kwin windows look out of place. (I'd love to see a widget style that better complements the glass plasma theme!) The oxygen theme for widgets has lacked contrast, as has been reported repeatedly, but the splash of color that is thrown in is green, which is an odd departure from the traditional KDE blue. Then you get an odd black Krunner look which just stands out, and doesn't seem to mesh with anything else.\n\nThe other complaint I've often heard (and mirrored myself) is the white space, or empty space seen all over the place in Oxygen. Oddly, some areas of the KDE 4 desktop seem nice and tight, and aspect's of Nuno's mockups seem nice and tight (though there is plenty of white space used in the mockups in other areas as well). This is also inconsistent.\n\nIndividually some of these components look nice, but shouldn't the widgets, plasma borders, window borders, dialogs, and icons all have had a consistent design scheme?\n\nBugfixes and showstoppers take priority, but I seriously hope 4.1 features a more unified visual theme across KDE, and I'd love it to closely match Nuno's mock-ups.\n\nRight now, I'm not just waiting for KDE 4.x to be perhaps a little more stable, I'm also waiting for new themes to arrive before I use it.  I know it is a minor contrivance compared to the rich features, but I really enjoy the look of my desktop right now.  I also enjoy having usable space, and right now the Oxygen look overall is pretty good in some areas, but it looks poorly thrown together on the whole, and the waste of space in some areas is enough to keep me from using it."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "Please make detailed screenshots, showing exactly one issue at a time, and a detailed writeup why this is a problem/ugly/whatever.  Merely stating that there is \"empty space seen all over the place in Oxygen\" is not very useful, because no-one knows what you're talking about.\n\nSmall, detailed problem reports with screenshots will get you informed comments at least, if you post it in the appropriate irc channel or mailing list."
    author: "Leo S"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "I'm heading out the door to work, but will upload some screenshots a little later."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "http://enderandrew.com/kde4/\n\nI made these hastily in Paint on a Windows box.\n\nhttp://enderandrew.com/kde4/consistent.png - This is a newer screenshot and it shows Oxygen looking more grey and less white, so it blends better with some of the black you see in the KDE4 desktop, and the Plasma glass frames.  However, I'd like the two window decorations (the Plasma one and the KWin one) to look consistent with each other, as opposed to two very different frames.\n\nhttp://enderandrew.com/kde4/wasted%20space.png - This should give you an idea of some wasted space.\n\nhttp://enderandrew.com/kde4/minimal.png - Quick copy/paste hack in Paint that took 156 pixels down to something like 61 pixels in the top portion of the window, and showed how you could easily squeeze more icons in even less space.  This may be a bit extreme, however my current Firefox window in Windows and KDE both run around 80 pixels for the window decoration, the menubar, and the toolbar all together, and I'm not going for extreme minimalist there either.  I have a moderately sized window decoration.  This 80 pixel size looks much nicer than the 150 pixel size in my opinion.  At the very least, perhaps we can compromise and meet in the middle.\n\nIn all fairness, some of the size (a few pixels) is having text labels under the toolbar icons which can be turned on or off.  But much of it is still empty space.\n\nFor all the talk of \"cleaning\" up the design in KDE4, I find this space contrary to the design goal.  The most important part of my work space is my work.  I want to see my data/content on the screen, and less of the interface."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "I checked out the pictures and though I can see your point I'm not sure I agree. Putting everything closer seems like a logical thing to do, but that way everything looks much more \"stressful\". I don't like feeling stressed only because I want to find a file :) After all using the mouse wheel, which is pretty much standard, instead of the scrollbar makes going through lots of files an easy job ..."
    author: "Nick L."
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "I think this is the wrong approach from a usability stand point.  Why should I spend extra time to scroll due to dead space?\n\nVista was designed to look pretty, and usability studies are showing that users going to XP to Vista are less productive.\n\nI think a good desktop can look nice without sacrificing usability.\n\nMy \"minimal\" example was admittedly perhaps a little extreme, but so is the 150+ pixel version.\n\nI'd like to see some experimentation with tightening it up some."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-09
    body: "KDE is all about configurability, right?  So why not have a global \"tightness\" setting, which with a single control alters various whitespace-related settings: the amount of padding around the menubars, iconbars etc., as well as the thickness of window borders and the spacing between icons in konqueror, dolphin etc.\n\nMaybe it's difficult to do, I'm not familiar enough with the code to say, but even if it had to wait till 4.1 (\"ready for everyone\") it would be a good way to keep everyone happy.\n\nI'm not so sure about having a totally consistent theme between plasma and kwin - although they might seem a bit too different at the moment some kind of contrast between the themes is good, as there is a considerable conceptual difference between plasma desktop \"windows\" and real kwin windows.  Personally I kind of prefer the plasma approach at the moment, and would like to see oxygen's window decorations move a bit in that direction, but still leave a bit of a difference."
    author: "Adrian Baugh"
  - subject: "Re: Oxygen"
    date: 2007-11-10
    body: "I see your point. One of the reasons I like Kubuntu over Ubuntu is because Gnome widgets are often much larger. Menus are larger, especially.\n\nOn a 1024x768 screen you want to squeeze more data on the screen.\n\nBut what about the larger screens ppl have nowadays... Small widgets make everything look cluttered and full.\n\nThe point of the above is that the new theme, Oxygen, is simply more suited for modern, larger screens. Time moves on, default (!) themes have to change. If you have a small screen, you should use Plastique, the Qt style which is very plastik-like. Most ppl have a large screen now, so the default favors them.\n\nSorry..."
    author: "jospoortvliet"
  - subject: "Re: Oxygen"
    date: 2007-11-10
    body: "Will KDE 4.0 even ship with other themes?\n\nAnd again, we're talking about going from 80 pixels, to 150 pixels, and almost double the top of every window.  That is pretty excessive."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-17
    body: "yes, KDE 4.0 ships with at least 5 themes, including a plastik-like one (Plastique)."
    author: "jospoortvliet"
  - subject: "Re: Oxygen"
    date: 2007-11-10
    body: "Hey T.J. Look what ol' winnie can do.\nTight as a xxxxxxx xxxx.\n"
    author: "reihal"
  - subject: "Re: Oxygen"
    date: 2007-11-10
    body: "How do you make that shot?"
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-10
    body: "With Snippy.\nIt's just Winamp on top of Firefox with Crystal lite theme.\n"
    author: "reihal"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "For consistent...  I'm not sure what's better here.  Plasmoids are not the same thing as windows, and I'm not sure that they should look the same.  Keeping them distinct has some value (and just the glass border would look odd with the grey window contents.  Switching to black for all windows isn't really an option as a default).\n\nWasted space..  I agree that it could be a bit more compact (actually I think the current version is already more compact than your screenshot).  Sure, you can crowd icons together to fit more in less space, but if that's the goal, you might as well set the toolbar to icons only mode, and make the icons small.  This will give you maximum space (just like in KDE3).  I find a good compromise is to set the icons to text beside icons.  This reduces vertical space and still makes the icons easy to hit and understand.  Like so:\nhttp://leo.spalteholz.ca/dolphin.png\n\nThis is kde4 dolphin running under gnome (long story).\n\n"
    author: "Leo S"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "I almost always use a \"detailed\" view as well.  Have the icon the left, like in that pic, and then nice detail info on what the file is.\n\nAs for the consistency, I didn't say the glass border on the plasmoids such be the window decoration, but rather that the vision of the desktop should be consistent.  They should match, and seem like they go together.\n\nIn using compiz-fusion, I've seen window decorations in Emerald where you have a clear glass border with no visible buttons, and I don't think it works well for normal windows.\n\nI just think the overall artistic design from plasmoid borders, Kwin, widgets, icons, menu, etc. should be consistent in the design.  I assumed that the Oxygen team felt the same way.\n\nLooking at the Krunner dialog, I see something that looks nice, but does it match anything else?"
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "Yeah I agree with you for the most part on look.  I can't put my finger on it exactly, but the oxygen style is just not that appealing to me..  I liked the mockups, and I liked the early versions of the style, but lately something is just not feeling right.  But I can't think of the reason specifically, so I wouldn't know where to start fixing.  Although the window buttons are an obvious one to fix.  They don't extend to the screen corners in maximized windows, making them extremely hard to hit."
    author: "Leo S"
  - subject: "Re: Oxygen"
    date: 2007-11-10
    body: "I always use detailed.\nI think maybe if people compile kde from scratch, the \nsettings should be to detailed-view (because, someone who compiles\nfrom scratch, normally knows more than the \"average\" user \nand thus is a lot more likely to get as much info as possible,\ni.e. all details)"
    author: "she"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "I totally agree - especially regarding the gaps between file-icons in dolphin. It's unbelievable how many space is wasted there - 70% of the current distance should be more than enough to guarantee a nice and \"distinctive\" look of every single icon."
    author: "stevie"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "As long as the spacing between the icons is configurable, everything's OK. (But this doesn't have to happen in KDE 4.0.0)"
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "I agree."
    author: "Soap"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "AFAIK, it is already configurable."
    author: "Hans"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "That would be very good news. Do you have a hint where i can find this option?"
    author: "stevie"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "> That would be very good news. Do you have a\n> hint where i can find this option?\n\nSettings -> Configure Dolphin... -> View Modes -> Icons:\n- use \"Grid Spacing\" for the space between the items\n- use \"Number of lines\" to specify the number of textlines which should be reserved for the item name\n\nI just reduced the default setting of Dolphin from 2 lines to 1 line to decrease the vertical gap, but I'll discuss a proper default setting for KDE 4.0 with the Oxygen team to be sure...\n\nBTW: I don't think Dolphin uses extremely much whitespace, e. g. please have a look at http://www.habibbijan.com/images/screenshots/osx_thumbnails.jpg for the spacing inside Finder. \n\nCheers,\nPeter"
    author: "Peter Penz"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "The vertical spacing in that shot seem decent, but the horizontal spacing seems perhaps a bit much.  In the KDE 4 screen shot, the vertical spacing was double the icon size, which is excessive.\n\nRegardless decisions shouldn't be made based on what others do, but rather on what is the best design/best solution.  Sometimes that means doing the same thing as everyone else, and sometimes it means deviating.\n\n"
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "Thanks a lot for your effort!"
    author: "stevie"
  - subject: "Re: Oxygen"
    date: 2007-11-13
    body: ">As long as the spacing between the icons is configurable, everything's OK.\n>(But this doesn't have to happen in KDE 4.0.0)\n\nEver heard about \"sane defauts\"? \"First impression is the most important\"? I don't think that the dead space shown in those screenshots are sane, and not every one will think of waste time adjusting the space beween icons and so on.\n\nConfigurability is important, but is not suficient.\n\nAs for people saying that monitors sizes are growing, etc, I say to wait until they grow suficiently to change de *defaut* theme acordling. The theme don't need to be the same throught the whole 4.x life-cicle, or I'm wrong? Currently, most people in the world run in 1024x768 or lower resolutions."
    author: "Manabu"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "Your screenshot looks way too crowded. I don't like it at all."
    author: "Michael"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "Quoting myself:\n\n\"Quick copy/paste hack in Paint that took 156 pixels down to something like 61 pixels in the top portion of the window, and showed how you could easily squeeze more icons in even less space. This may be a bit extreme, however my current Firefox window in Windows and KDE both run around 80 pixels for the window decoration, the menubar, and the toolbar all together, and I'm not going for extreme minimalist there either. I have a moderately sized window decoration. This 80 pixel size looks much nicer than the 150 pixel size in my opinion. At the very least, perhaps we can compromise and meet in the middle.\"\n\nI made it very quickly, eliminating most of the space just to see what would happen.  I admit it was a hasty/ugly hack, and may be a bit extreme.  I suggested we might compromise, and the 80 pixels or so that I see in my current Linux desktop and my Win XP desktop look about right.  60 pixels may be too tight, but at the same time, 150+ pixels is too much wasted space.\n\nAgain, I said there is probably a good way to meet in the middle."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "i think icon view is only preferable for folders with images (large thumbnail icons), and for other folders detailed view is a good choice (+filter bar like the one found in amarok's Collection tab)"
    author: "nick_s"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "Just a comment on minimal.png - you've moved the menu (the last item \"Help\" is visible) up to be in the same horizontal line as the window decorations ( the max,min,close buttons ).\nAs far as I know, that's not able to be done, as the window frame and window decorations are handled by the window manager - the application provides the window content, including the menubar ( that's my understanding, anyway )\n\nStill - that's an interesting idea if it could be achieved - there always seems to be a large unused horizontal space to the right of the menu bar. Problem then is, where do you put the window title, since the menubar and window decorations are merged?\n\nIts maybe that the window theme and the widget theme are now so closely aligned ( same colour ) that it gives the appearance that there's much more space used at the window top, because you don't get the distinction between the window title/frame and the window content anymore?"
    author: "bonybrown"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "That is how I'd like to see it, almost a reverse of what Mac/Apple does.  On a Mac, the file/edit/whatever menu is always at the top of the screen, and you don't have that menu bar in your window.  It frees up some real estate not to draw a menu bar for each window.\n\nIn IE7/Vista/Office 2007, the design is to hide the menu bar to make room, but this drives me nuts.  I use the menu bar all the time, and I want it visible.\n\nIf I was going to consolidate to make room, I'd eliminate the \"title\" of the window, and move the menu bar to the same line as the window decoration buttons, as I did in that mock-up.  It would no doubt involve some code in KWin to make happen, but it isn't impossible.  In your panel/task bar/whatever, you can see your collection of open windows, and you have your titles there.  When the window is open and in the fore-front, you can see what is going on in that window.\n\nWith even niftier Alt+Tab tricks we're getting from Compiz (and hopefully KWin/Plasma in the future) it is much easier to see screen shots of the windows, and the \"title\" of the window becomes less and less relevant.\n\nI think it would be a bold design move to clean up the window, make more screen real estate, etc."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "KDE aloud users to select a mac menubar style.\n\nIt sucks, yes, it's a very bad usability concept from my point of view, and I shouldn't ever be default, but it's there if you want it.\n\nWindows title are themselves not useful, but the space they use are indeed useful for move windows, as a visual feedback of where the window starts and end, and as very easy to find point to locate windows. It have its reason.\n\nPS: Cheers to KDE 4 team."
    author: "Luis"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "heeeee your minimal it totaly undoable, you have the menu on the title area of the window.\nOxygen wont be able to fix all apps that is mostly the developer in charge of each aplcation job."
    author: "nuno pinheiro"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: ">  The oxygen theme for widgets has lacked contrast, \n> as has been reported repeatedly\n\nYes, that is still a very real problem.  The style is actually usable in its current incarnation (depending on the quality and contrast of your display), but still not comfortable to work with in some situations because controls (particularly smaller ones such as radio buttons and check boxes) have to be hunted for.\n\n> The other complaint I've often heard (and mirrored myself) \n> is the white space, or empty space seen all over the place in Oxygen\n\nThat I think falls down to personal preference.  I prefer applications to have a healthy amount of unused space.  As far as I can tell, dialogs and windows don't take up any more space in the Oxygen style, but because many of the excess lines, dots and other clutter has been removed there appears to be more space.\n\n"
    author: "Robert Knight"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "As for contrast, give is a little more gradient effects.  I love the custom gradients in Domino.  I can't say enough good things for Domino in KDE 3.\n\nAs for empty space, the screenshots showed there is more unused space.  80 pixels is what I'm seeing on average in a KDE 3 or Windows desktop, and Oxygen/KDE 4 is almost double that."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: "> I certainly wouldn't mind seeing a dev's response/take on the issue\n\nas semi-involved, I'll bite ;)\n\n> I assumed there was going to be a unified artistic vision from the early stages.\n\nyep, the problem with that is, artistic vision isn't visible in WIP development snapshots ;)\n\n> the window decorations around the actual Kwin windows look out of place.\n\nthat's largely because the rounded borders aren't antialiased, which we haven't found a solution to. Not relying on COMPOSITE would be a bonus...\nother than that, the unfinished whitish colorscheme detracts from the \"solid piece of metal\" window look we're going for (as visible in nuno's mockups).\n\n> I'd love to see a widget style that better complements the glass plasma theme!\n\nmaybe, maybe ;) but it wouldn't be default. The design concept behind the black plasma theme is that all plasmoids lie on the same disorganized flat plane. unlike a widgetstyle/windeco.\n\n> The oxygen theme for widgets has lacked contrast\n\nonce we've chosen a nice non-white default colorscheme this will be history.\n\n> the splash of color that is thrown in is green, which is an odd departure from the traditional KDE blue\n\nthe splash of color in KDE 4.0 will depend on the dominant color in the (still undecided...) default wallpaper. maybe green, or blue, or khaki. we don't know.\n\n> Then you get an odd black Krunner look which just stands out\n\nit's a middle ground between the flat plasmoids and the solid windows.\nI say wait until you see it in action ;)"
    author: "logixoul"
  - subject: "Re: Oxygen"
    date: 2007-11-07
    body: ">> the splash of color that is thrown in is green, which is an odd departure from the traditional KDE blue\n\n>the splash of color in KDE 4.0 will depend on the dominant color in the (still undecided...) default wallpaper. maybe green, or blue, or khaki. we don't know.\n\nHopefully it'll be easy for users to change the colour ;)"
    author: "Soap"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "> that's largely because the rounded borders aren't antialiased, which we haven't found a solution to.\n\nDoes this mean KWin still won't handle argb visuals? Why doesn't Lubos use them? Probably every other window manager is using them now :(. To hell with all effects, I want argb visuals!\n"
    author: "anon"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "Thanks for your response.\n\nI'm not using KDE 4 yet.  I was all excited to go running with beta releases (I love beta software, and don't mind somewhat unstable software, but the current Oxygen theme is part of the reason I haven't made the jump yet) but I haven't seen any real KDE 4 screenshots that have used Nuno's mockups for tabs.\n\nThe screenshots I've seen seem to have very simple tabs, where the active and inactive tabs lack contrast.  There seems to be a very simple tab divider, and a pretty plain look.\n\nNuno's mockups also seem to have a very nice white to gray gradient that does give a somewhat steel look.  I'm not a fan of the Apple OS X Steel look that is so popular.  It is too textured, and too fake looking in my eyes.\n\nIs there any chance we might get tabs that look something like this:\n\nhttp://www.nuno-icons.com/images/estilo/version%203%20tabs%20and%20version%2021%20scrolloptional.png\n\nOr option 1 here:\n\nhttp://www.nuno-icons.com/images/estilo/new%20style%20tabs.png\n\nGreat contrast, original look, etc.  Can't say these concepts steal too much from Mac or Windows.  I think it would be a very fresh KDE-original design.\n\nI also really like how Nuno mixed in the traditional \"Crystal\" look in the progress bars and scroll bars in his mock-ups.  Vista has similiar looking progress bars, and they are pretty sharp.  If the windows and plasma apps are somewhat flat in their appearance, his green \"crystal\" accents provide a very sexy sense of depth and color that really spice up an otherwise clean desktop.\n\nIt seems this is perhaps the biggest discrepancy I'm seeing between the mock-ups and actual screen shots, this bit of green crystal flair here and there.\n\nKDE 3 might have overused that blue crystal look everywhere, and KDE 4/Oxygen looks a little too sterile.  Maybe meet in the middle?  (Again like those great mock-ups!)"
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "> Then you get an odd black Krunner look which just stands out, and doesn't\n> seem to mesh with anything else.\n\nthat's because the krunner UI has not be completed. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "You mean you guys haven't finished the complete refactoring of KDE yet?  Shouldn't that take like two weeks, tops?  :P\n\nThanks not only for the work, but the way the team responds back to the community, has the developer blogs, etc.  It is really nice to know the community has a voice, and a means to directly interact with the developers.\n\nYou just don't have that with Macs or with Windows.  I love the concept of open source software, and the community it breeds.  I wish I had the coding skills to help out.\n\nI know there has been a bunch of dissent about how unstable Plasma is at this point, but after reading your blog on how excited you are about the possibilities of Plasma, I'm certainly trying to be more open-minded about the whole thing.\n\nI think rewrites are a very good thing.  I think from time to time, you need to rethink your whole design, and start anew.  I'm still curious if Plasma will ever deliver the revolution in desktop usage that was hyped, but either way, after all the bumps, I am beginning to think it will be worth all the hassle.\n\nSo as soon as a 4.1 stable comes out, when are you guys going to map out the next major refactoring?  I think you start anew with a blank project slate and just rewrite KDE 5 completely from scratch ditching all the work that went into the 4 rewrites.  We'll expect 5.0 out in late 2008."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "You seemed so serious right up until that last paragraph..."
    author: "Louis"
  - subject: "Re: Oxygen"
    date: 2007-11-08
    body: "That's what you call a twist ending in English, right? Either way it made me smile; I needed that, thanks. :)"
    author: "Hans"
  - subject: "Re: Oxygen"
    date: 2007-11-11
    body: "> The other complaint I've often heard (and mirrored myself)\n> is the white space, or empty space seen all over the place in Oxygen\n\nI fully agree. Way too much space wasted, just for (debatable) aesthetic reasons, but severely harming basic usability.\n\nDear designers, please: \"form follows function\", not vice versa!"
    author: "Victor T."
  - subject: "Re: Oxygen"
    date: 2007-11-12
    body: "I'm not sure many developers (or really anyone) read the digests this late.  I'd echo that sentiment in a few days when the new digest comes out, and people are more likely to see it."
    author: "T. J. Brumfield"
  - subject: "Connections with krunner"
    date: 2007-11-07
    body: "I like very much the ideas behind Raptor, especially the intelligent way of offering the applications you mostly used.\n\nI see some connections with krunner, which is useful to launch \"everything\", but also applications as Raptor does.\n\nCould krunner and Raptor share the same \"usage database\", or could them be more integrated?\nI like very much also the graphic part of Raptor (of course, I will like also the Krunner one).\n\nTo make the story short: I think that in my case I will use both Raptor and Krunner (the latter for example as a quick calculator), but I'd like very much to have a unique launcher with \"mixed functions\" between these two.\nI'm asking too much? I agree... :)"
    author: "Augu"
  - subject: "Re: Connections with krunner"
    date: 2007-11-08
    body: "The \"connection\" is called runners. \"these plugins, known as runners, are based on a class in libplasma called Plasma::AbastractRunner. this means that any app that links against libplasma can get the same functionality. neat.\"\n\nMore information: http://aseigo.blogspot.com/2007/10/runnersimprove.html"
    author: "Hans"
  - subject: "Thank You"
    date: 2007-11-07
    body: "A big thank you for the commit digest.\n\nIve been waiting the whole sunday + monday + tuesday for this. im really glad you managed to finish this even though you have other work to do which slightly delayed this. \n\nthank you very much!"
    author: "TeeZee"
  - subject: "Re: Thank You"
    date: 2007-11-07
    body: "Yeah, thanks - i've been waiting a long time since the last messages of thanks regarding the Digest. I know it's an appreciated publication, but it's still nice to see messages like this from time to time!\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Thank You"
    date: 2007-11-07
    body: "Your commit digests rock!  Thank you Danny.  The most informative OSS publication out there.  There is lots of work done by people that don't blog much, so your digest is really the only way to keep up with KDE development for users."
    author: "Leo S"
  - subject: "Re: Thank You"
    date: 2007-11-08
    body: "I frantically refreshed over and over again.  I really look forward to this every week."
    author: "T. J. Brumfield"
  - subject: "Re: Thank You"
    date: 2007-11-08
    body: "Same for me, each monday I frantically check the dot to see the commit digest appear :)\n\nThanks a lot Danny"
    author: "Marc"
  - subject: "Re: Thank You"
    date: 2007-11-08
    body: "I, too, checked over and over like a mad man.  This is like the third week that it's come out on Tuesday.  Not complaining, but if you told me that was to become the more likely release day, maybe I could get my OCD under control. :-)\n\nThanks."
    author: "Louis"
  - subject: "Re: Thank You"
    date: 2007-11-08
    body: "I rarely post a comment to the Dot, but I want to add my voice to the 'thanks' in this thread.  Every week I keep refreshing the direct link to the upcoming digest so I can get it earlier than waiting for the Dot.  And sometimes it shows up temporarily before you've compiled all the summaries at the beginning.  I use that as a chance to read through the commits while you're finishing up the summaries and then I go back to read that part.\n\nSo, again, sorry for not saying thanks much, but it is eagerly awaited every week.  You do a fabulous job!"
    author: "mactalla"
  - subject: "Re: Thank You"
    date: 2007-11-08
    body: "I enjoy the digests."
    author: "harold"
  - subject: "Re: Thank You"
    date: 2007-11-08
    body: "Man, the Commit-disgust/The Road to KDE/KDE 4 Pillars (unfortunately the latter two seem to be now dead) are some of the most unique and very pleasing-to-read semi-regular publications in Free Software! Thanks heaps! "
    author: "A KDE advocate"
  - subject: "Re: Thank You"
    date: 2007-11-08
    body: "** grrr I meant \"digest\".. Damn KDE's 3.X awesome spell checker  ;)"
    author: "A KDE advocate"
  - subject: "Re: Thank You"
    date: 2007-11-07
    body: "Yeah, the commit digests rock."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Thank You"
    date: 2007-11-08
    body: "thirded."
    author: "Aaron J. Seigo"
  - subject: "Re: Thank You"
    date: 2007-11-07
    body: "Thanks Thanks Thanks...hunderds of thanks for you Danny..."
    author: "Emil Sedgh"
  - subject: "KDE-PIM"
    date: 2007-11-08
    body: "Is there any website where I can follow progress within KDE-PIM incl. syncing?\n\nbest wishes \nMM"
    author: "MM"
  - subject: "Re: KDE-PIM"
    date: 2007-11-09
    body: "you can follow the commits (using http://commitfilter.kde.org/ of course =) as well as the devel mailing lists. but most projects don't have enough people hanging about how follow and understand development progress well enough to keep websites up to date with development progress."
    author: "Aaron Seigo"
  - subject: "Google Android + WebKit"
    date: 2007-11-14
    body: "Hello, I saw nobody of KDE talking about so I wanted to mention it somewhere:\nGoogle's open source phone system Android is using the KHTML based WebKit. Congratulations for one of the most viral piece of KDE software!\nhttp://webkit.org/blog/"
    author: "ac"
---
In <a href="http://commit-digest.org/issues/2007-11-04/">this week's KDE Commit-Digest</a>: Krushing day concludes with focused bug fixing for the KDE 4.0 release. Work on various "runners" in <a href="http://plasma.kde.org/">Plasma</a>, with general work on applets and the addition of binary and fuzzy clocks. Consraints support in the <a href="http://stepcore.sourceforge.net/">Step</a> physics simulation package. Work on icons across KDE Games applications. Support for the <a href="http://www.scalix.com/community/">Scalix</a> groupware server in <a href="http://pim.kde.org/">KDE-PIM</a>. Entry editing improvements in <a href="http://korganizer.kde.org/">KOrganizer</a>. Improved Blu-Ray format support in <a href="http://k3b.org/">K3b</a>. <a href="http://solid.kde.org/">Solid</a> gets support for Video(4Linux) devices. <a href="http://kopete.kde.org/">Kopete</a> uses Solid for network detection and support of audio/video devices. Various progress across <a href="http://koffice.org/">KOffice</a>.

<!--break-->
