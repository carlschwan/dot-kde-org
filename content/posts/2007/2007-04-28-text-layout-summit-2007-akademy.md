---
title: "Text Layout Summit 2007 at aKademy"
date:    2007-04-28
authors:
  - "jriddell"
slug:    text-layout-summit-2007-akademy
comments:
  - subject: "MUSIC "
    date: 2007-05-06
    body: "I LIKE MUSIC POP , I HOPE I CAN TO BE CLEAVER IN PLAYING MUSIC , I WANT TO OWN LOCK TAB GUEITAR\n\nTHANKS\n"
    author: "agust_0321"
---
The <a href="http://akademy2007.kde.org">aKademy</a> team is pleased to announce that we will be hosting the <a href="http://www.freedesktop.org/wiki/TextLayout2007">Text Layout Summit 2007</a> during our week in Glasgow.  This is the second Text Layout Summit following the success of <a href="http://live.gnome.org/Boston2006/TextLayout/">the event at Gnome's Boston Summit</a> last year.  Experts from the free software world's top <a href="http://www.freedesktop.org/wiki/TextLayout">text rendering</a> apps and libraries are expected including Qt, Pango and the cross platform effort of HarfBuzz.  As previously, <a href="http://www.kde.org.uk/akademy/">register</a> before the end of Monday if you want us to book your accommodation.




<!--break-->
