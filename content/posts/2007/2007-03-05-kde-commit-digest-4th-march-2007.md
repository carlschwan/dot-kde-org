---
title: "KDE Commit-Digest for 4th March 2007"
date:    2007-03-05
authors:
  - "dallen"
slug:    kde-commit-digest-4th-march-2007
comments:
  - subject: "We need more Screenshots ;)"
    date: 2007-03-05
    body: "We need more Screenshots ;)"
    author: "MaX"
  - subject: "Re: We need more Screenshots ;)"
    date: 2007-03-05
    body: "yes, I Really agree on this!"
    author: "Emil Sedgh"
  - subject: "Re: We need more Screenshots ;)"
    date: 2007-03-05
    body: "No. This is work enough allready, besides this is not a \"What's new in KDE 4\" article. This is a service to the developers and the community at large that keeps everybody up to date with the broad look of development in the KDE world. \nSurely this is already a lot of work for Danny.\n\nContinue the fine job Danny."
    author: "pascal"
  - subject: "Re: We need more Screenshots ;)"
    date: 2007-03-06
    body: "If you want screenshots, do them yourself and send them to Danny.\n\nDerek"
    author: "D Kite"
  - subject: "Re: We need more Screenshots ;)"
    date: 2007-03-06
    body: "there really isn't much to see: http://chani.ccdevnet.org/sshots/cmake-is-shiny.png"
    author: "Chani"
  - subject: "Dolphin"
    date: 2007-03-05
    body: "I recently switched to Dolphin (for kde3) for my file-managing needs. And I noticed a few things : \n\n - It is fast. starts faster than konq, and this is very pleasant, the views are fast, too, it feels a bit like the kfm of old. \n - It is quite usable in general, the previews on the side are not distracting, for example.\n - I regret that I cannot have the toolbar and address bar on a single line\n - I re-added the \"up\" button to the toolbar, I find it extremely useful. But perhaps it is just me :)\n - I have not found how to make \"preview\" the default view -- in fact, I find the existence of preview and icons to be slightly redundant, but I understand \"preview\" might cause problems on remote filesystems\n - I found myself regretting I could not navigate the filesystem using only the breadcrumb widget (only a single level of subdirectories is shown)\n\nAll in all, I am quite happy that Dolphin goes in and I am sure any imperfections will have been ironed out before kde4. a huge THANK YOU to the devs."
    author: "hmmm"
  - subject: "Re: Dolphin"
    date: 2007-03-05
    body: "Hope this doesn't start another Konqueror vs Dolphin flame war. Because we all know which one is the better. ;)\n\nJoke beside, you can set the defaul view in the settings. I don't have Dolphin (nor KDE) available on this computer, but I think it should be in the \"general\" settings.\nI didn't really get what you mean with \"navigate the filesystem using only the breadcrumb widget\", but please take a look here: http://static.kdenews.org/dannya/vol10_4x_dolphin_breadcrumb.png\n(Please note that the screenshot shows a svn version)"
    author: "Lans"
  - subject: "Re: Dolphin"
    date: 2007-03-05
    body: "No, I don't want to start a flamewar... Really, I like both, and likely, I'll keep using both.\n\nFor the navigation, what I mean is that from the menu appearing on the screenshot, you cannot go to the sub-sub-directories (I might not be very clear, there...)\n\nfor the default view, you are right, I had missed it. Silly me :)"
    author: "hmmm"
  - subject: "Re: Dolphin"
    date: 2007-03-05
    body: "Sorry, it wasn't my intention to accuse you for being a flamewar starter; I just said that there was this possibility. If you got offended I apologize.\n\n>> For the navigation, what I mean is that from the menu appearing on the screenshot, you cannot go to the sub-sub-directories\n\nHm, think I understand. The menu will show submenus on hover?\n(For example, http://rafb.net/p/CYVOO687.html - well, shomething like this?)"
    author: "Lans"
  - subject: "Re: Dolphin"
    date: 2007-03-05
    body: "Yes, exactly. The problem of course is what to do when you have _lots_ of subdirs...\n\nWith this method, navigation becomes much faster."
    author: "hmmm"
  - subject: "Re: Dolphin bugtracker / feature request ?"
    date: 2007-03-06
    body: "I was looking for a bug tracker and a feature request system for dolphin, but I couldn't find anything on the dolphin website. The KDE bugtracker doesn't have an entry for dolphin. Does anybody know where to place bug reports/feature requests?"
    author: "veru"
  - subject: "Re: Dolphin bugtracker / feature request ?"
    date: 2007-03-06
    body: "there is a dolphin product on bugs.kde.org now. please try and submit bugs for kde4, though, as that's where development is focussed."
    author: "Aaron Seigo"
  - subject: "Video support in Amarok?"
    date: 2007-03-05
    body: "Where has the 90% rule been with this feature? Is it still asleep on this early monday morning?\n\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: Video support in Amarok?"
    date: 2007-03-05
    body: "See AWN #7 for details. We're talking about *basic* video support for music videos and podcasts."
    author: "Mark Kretschmann"
  - subject: "Re: Video support in Amarok?"
    date: 2007-03-05
    body: "Essentially it is like what i've suggested before: When a video stream exists in whatever file you are playing, that video stream will replace the visualisation. Because, arguably, the video stream can be considered visualisation of the sounds coming out of your speakers/headphones :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "KSplashX"
    date: 2007-03-05
    body: "At first, the statement that KSplashX doesn't even support text seems crazy, but if you think about it, Lubos is right.  Those status messages, like \"initializing peripherals\" and such are completely useless.  I've been reading them for so long that they somehow convinced me they are giving me important information.  In fact, they don't mean anything, and as Lubos said, are not even accurate.  All we need is support for a few steps so people can implement a progress bar.  Honestly, I don't even need that, I just turn the splash screen off entirely anyway.\n\nThere might be some negative feedback from all those people that want super flashy splash screens.  I don't see the point of making startup slower by displaying fancy animated transitions, but some people like that.  I think KSplashX should replace the old KSplashML, and if people want programmable splash screens that can be a separate tool.\n\n\n\n"
    author: "Leo S"
  - subject: "Re: KSplashX"
    date: 2007-03-05
    body: "Well, this seems the outcome of the discussion on the mailinglist, as far as I can see, so... :D"
    author: "superstoned"
  - subject: "Re: KSplashX"
    date: 2007-03-05
    body: "openSUSE has had this for ages, and I'll bet good money that we've got far nicer splash screens than the vast majority of other ones out there. They're wonderful.\n\nI completely agree with Lunak on this -- if a splash screen is slowing down the actual startup it is completely failing in its function. "
    author: "apokryphos"
  - subject: "Wait, wait, WAIT"
    date: 2007-03-05
    body: "So. Not only does (much of) KDE already run on Windows, but there also is an installer? How come I didn't know? And how usabe is this?"
    author: "ac"
  - subject: "Re: Wait, wait, WAIT"
    date: 2007-03-05
    body: "USEABLE, I meant USEABLE of course ;)"
    author: "ac"
  - subject: "Re: Wait, wait, WAIT"
    date: 2007-03-05
    body: "I think you may have meant usable."
    author: "Mark Kretschmann"
  - subject: "Re: Wait, wait, WAIT"
    date: 2007-03-05
    body: "Anyway, he probably didn't know because he didn't look good enough ;-)"
    author: "superstoned"
  - subject: "Re: Wait, wait, WAIT"
    date: 2007-03-05
    body: "But... where's that perl dependency coming from?\n\nI'd assumed that was just for the Makefile.am.in->Makefile.am conversion, but I guess that might not apply in KDE4 :o)\n"
    author: "mart"
  - subject: "Re: Wait, wait, WAIT"
    date: 2007-03-05
    body: "No, kde4 uses the cmake build system, wich does not need perl anymore. \nThe perl dependencies come from several perl scripts located in kdelibs. \n\nkdelibs/kate/data/generate-php.pl\nkdelibs/kate/tests/highlight.pl\nkdelibs/kdecore/kconfig_compiler/checkkcfg.pl\nkdelibs/kdeprint/cups/cupsdconf2/cupsdcomment.pl\nkdelibs/kio/proxytype.pl\nkdelibs/kio/useragent.pl"
    author: "ralf"
  - subject: "Re: Wait, wait, WAIT"
    date: 2007-03-05
    body: "grep perl  `kde-config --expandvars --install exe`/*"
    author: "Carlo"
  - subject: "KDE & Windows"
    date: 2007-03-05
    body: "I am impressed and very excited about KDE applications running natively under Windows. The screenshots with kioslave, KWrite and even Konqueror make the port somehow real.\n\nI hope many developers (and users) from the Windows world will help KDE! This should be mutually advantageous. I hope the KDE community is large and strong enough to handle that. As it is written: \"Others are listening to the kde-windows mailing list and doing support for people trying to enter KDE development, which is very important.\" Sure, this is very important!\n\nThank you for all!"
    author: "Hobbes"
  - subject: "Re: KDE & Windows"
    date: 2007-03-06
    body: "In Windows essential KDE bugs as buggy keyboard behaviour won't make a good impression.\nBut I think they don't occur with Windows version of QT.\n"
    author: "abc"
  - subject: "Re: KDE & Windows"
    date: 2007-03-13
    body: "what buggy keyboard behavior are you referring to?"
    author: "David M. Besonen"
  - subject: "Re: KDE & Windows"
    date: 2007-03-14
    body: "i don't know what the original poster meant, but first thing that came to my mind was http://bugs.kde.org/show_bug.cgi?id=137288 ;)"
    author: "richlv"
  - subject: "Nex oxygen icons"
    date: 2007-03-05
    body: "Am I the only one that thinks that some of the new icons are a step backwards?\n\nExamples:\nhttp://commit-digest.org/issues/2007-03-04/moreinfo/638984/#visual\nhttp://commit-digest.org/issues/2007-03-04/moreinfo/638215/#visual\n\nAm I the only thinking that the new ones are gross?"
    author: "NabLa"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "> Am I the only thinking that the new ones are gross?\n\nPlease try to be constructive.  What useful information can an Oxygen icon developer infer from your post?  Could you be more specific about what it is that you do not like.    \n\nThe term 'gross' is also disrespectful, the artwork is mostly ( or perhaps entirely, I am not certain ) put together by people volunteering work in their spare time.  Nuno and David in particular have been very busy.\n"
    author: "Robert Knight"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "I'm sorry if that sounded rude, Robert, it wasn't my intention and I apologise.\n\nIn a masterpiece of summarisation, what I actually wanted to say was:\n\n  * They look very old fashioned. Soo 90's. Gnome-like. That's why I said \"gross\".\n  * The older icons seem easier on my eyes. For example, the old \"home\" button is more schematic and less detailed. I'm no graphic artist, but it occurs to me that the old button would look better at smaller sizes.\n  * I thought the oxygen theme was going to be kinda blue-ish, aren't those icons breaking that look'n'feel?"
    author: "NabLa"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "I agree. \"Old\" icons are more appetizing to eyes."
    author: "Estevam"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "One more thing: less blue pls! Blue icons/themes/etc. are so Windows-like..."
    author: "Estevam"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "You need to come up with a better reason to not use blue than 'Windows-like'.  I personally like blue and don't care if it was Windows-like, OS X-like, or even Gnome-like."
    author: "Sutoka"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-06
    body: "I have four or five different program icons on my toolbar (set to \"small\" size iirc). they all look like strange little blue blobs to me. let's see... they're actually konq, konversation, kontact, knetworkmanager, gvim, and the k menu. most of them are quite pretty, but at that size I can barely tell them apart and can only identify them from memory. at least knetworkmanager is dark blue, so it stands out a bit... \nthese days I don't even click them - I just hit alt-space and start typing the name of what I want.\n\nand I have a vague memory of seeing a possible new kopete icon that was also a small blue blob. please, make program icons that don't all turn into little blue blobs at small sizes! :)"
    author: "Chani"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-08
    body: "Yes, icons should be in particular a functional piece of graphic design rather than beautiful small pictures!! Icons should be simple, clear minimal symbols, in they graphics principles similar to logos, they should be recognized instantly, the eye should resolve them without any pursuit. That tiny stupid details destruct the power of clear significance. Really, read some graphic design textbooks or ask a professional designers (but please, not again some self acclaimed jerks) this is very important thing, not a playground for personal \"creativity\"."
    author: "functionalist"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "\"Please try to be constructive. What useful information can an Oxygen icon developer infer from your post? Could you be more specific about what it is that you do not like. \"\n\nDon't worry. Oxygen is a group of people, who for reasons of copyright control and artistic egoism innately hate almost any suggestion that goes their way. So, mudslinging or constructive criticism - makes no difference whatsoever."
    author: "Daniel"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "You are joking right? can only be, cmon."
    author: "pinheiro"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-06
    body: "I've been hearing what Daniel claimed everynow and then for a while now, though I haven't heard any reasoning to back it up (I think the closest I heard was someone saying that their changes was rejected).\n\nDaniel, if what you say _IS_ based in reality, could you back it up with any evidence to point out that Oxygen is 'closed' (to outsiders) as you state it is?"
    author: "Sutoka"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "I'm no artist (at all!) but here's my stab at it:\n\nA while back, there was a revision of the buttons (which I can no longer find) where the icon in the centre was more grey than white, making the buttons look fairly metallic in a \"gun-metal\"-ly sort of way.  There was something incredibly pleasant about clicking them - they looked like solid, metal studs and when you clicked on them you half-expected a satisfying \"clunk!\" sound to result from it!  This was my favourite iteration, by far.  The next had a much lighter centre icon which looks odd to me - it's \"too bright\", somehow.\n\nThe current ones, though, I do not really like - the arrows are very plain, and their blandness makes them look out of place amongst the other, more vivid and beautiful ones (check e.g. kfind's icon for a truly gorgeous one).  Also, I consider the Back, Forward, Refresh, Home, Up etc buttons to be part of a group - the \"navigate\" group - and so expect them all to be similar to each other, which is certainly not the case here - the Home house does not fit in with the arrows at all.\n\nAnyway, just my 0.2 pennies ;)"
    author: "Anon"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "Yes! They removed the round borders.\n\nRemember, when you want to take a leap forward, sometimes you have to take some steps backwards. OK, that was only something I made up, my point is:\nOxygen has still months to mature; I think it's good that they experiment with things (for example the dot in the icons you linked to), and even if it sometimes turns out to be a \"failure\", it could still be successful.\n\nI think that the icons will be polished as times goes by."
    author: "Lans"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "I also think these icons are a big step backwards. I thought the same when our two icon gods replaced some older icons and announced it in the blogs (\"we have a fight\"). I liked the older versions a lot more."
    author: "me"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "In my experience judging icons based on their 128x128 size is just foolish. This isn't how they are going to appear and size really does matter.\n\nSo I'll just limit myself and say... whats wrong with a happy blue color?"
    author: "Ian Monroe"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "I like the icons without the circular borders.  Putting the symbols into a visual \"button\" is redundant and takes up extra space.  Simple images are more easily recognizeable."
    author: "Louis"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "I prefer the freestanding black arrows to the ones inside the blue circle.  They're effectively bigger, and seem sharp."
    author: "Bastanteroma"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "i also think that those new icons he linked to doesn't look better. if they want to remove the circular icon look, (and i also don'T like round icons that much) they can draw other icons, but with smooth corners and not that many colours in it. \nand choose colours which are heavy and clean to the eye and not so low and less-contrasting. for example, choose some plastic look, they look very good.\n\nsorry, for my bad english, i hope you can understand me ;)"
    author: "litb"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "I agree. Don't like the new home icon either. What I would _really_ like to see (perhaps for KDE-5) is a voting icon theme called \"Democracy\" ;-)\nThat means:\n1) Upload the CrystalSVG or even Oxygen icon theme to a server.\n2) Anyone can now submit new proposals for specific icons and people vote on their preference. \n3) Anyone can change their votes anytime just like in the bug system\n4) Of course, some groups should be created i.e. left/right/up/down so they can only be voted and changed as a whole\n5) A download function lets you download the current theme with all icons with the most votes at the moment.\n\nI wonder what that would yield..."
    author: "Michael"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: ":) "
    author: "pinheiro"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-05
    body: "Actually I like them both."
    author: "Darkelve"
  - subject: "Re: Nex oxygen icons"
    date: 2007-03-06
    body: "> Am I the only one that thinks that some of the new icons are a step backwards?\n\nNo because you can't judge a complete icon set from just two icons. You'll have to see the icons in the right context (e.g. see the full toolbar in konqueror) first. Perhaps that single icon looks like a step backwards, but likely the whole desktop looks cleaner and a step forwards."
    author: "Diederik van der Boor"
  - subject: "thanks Ralf Habacker "
    date: 2007-03-05
    body: "you have make my day man,so now i can downolad kde in my work\n\nthanks really"
    author: "djouallah mimoune"
  - subject: "Credit to Inkscape?"
    date: 2007-03-06
    body: "Why the oxygen desinger are not giving the credit inkscape deserve?\n\nA simple label in the website like \"made using inkscape\" would do."
    author: "Carl"
  - subject: "Re: Credit to Inkscape?"
    date: 2007-03-06
    body: "How often do you see \"made using gcc\" on package info?"
    author: "Dan"
  - subject: "Re: Credit to Inkscape?"
    date: 2007-03-06
    body: "Even if credit was needed, are you sure all icons were done with Inkscape? It appears that most were done with Illustrator. And does it really matter, as long as the SVG is available and fully editable?"
    author: "anon"
---
In <a href="http://commit-digest.org/issues/2007-03-04/">this week's KDE Commit-Digest</a>: KSplashX, a potential replacement for the KSplashML engine is imported into KDE SVN. Continued progress in the <a href="http://solid.kde.org/">Solid</a> and <a href="http://www.gnome.org/projects/NetworkManager/">NetworkManager</a> integration. More refinement, including better keyboard shortcuts, in <a href="http://konsole.kde.org/">Konsole</a>. New keyboard layouts in <a href="http://edu.kde.org/ktouch/">KTouch</a>. Icon and undo support in Step, the educational physics simulation package. KBounce becomes the latest game to move to a scalable interface and graphics. More work in KSquares, Konquest, KSpaceDuel and KReversi. <a href="http://ksudoku.sourceforge.net/">KSudoku</a> starts to be ported to KDE 4. Initial support for input fields and other form elements in <a href="http://okular.org/">okular</a>. A co-ordinate grid and Wikipedia entries for cities comes to Marble. Further improvements, including the addition of Tree View functionality to the <a href="http://enzosworld.gmxhome.de/">Dolphin file manager</a>. New features in <a href="http://www.mailody.net/">Mailody</a>. Initial video support in <a href="http://amarok.kde.org/">Amarok</a>, with heavy interface redevelopment underway for version 2.0. Last stages of an interface overhaul for the next version of <a href="http://ktorrent.org/">KTorrent</a> are completed. Proxy support added to the <a href="http://commit-digest.org/issues/2007-01-14/#1">KDE Windows installation utility</a>. <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> is moved from playground to the kdesupport module.
<!--break-->
