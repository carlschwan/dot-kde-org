---
title: "KDE e.V. Quarterly Report 2007Q2 Now Available"
date:    2007-09-22
authors:
  - "dallen"
slug:    kde-ev-quarterly-report-2007q2-now-available
comments:
  - subject: "gotta love it"
    date: 2007-09-22
    body: "First time I see a quarterly report starting with '.init()', cound't be geekier than that :-D"
    author: "Patcito"
  - subject: "Thank you Danny"
    date: 2007-09-22
    body: "Thank you Danny for the effort you put into creating this report, it is much appreciated."
    author: "Chris Howells"
  - subject: "The president"
    date: 2007-09-22
    body: "Did you forgot to update the president at the last page or did Eva secretly kicked Aaron of the throne? ;)\n"
    author: "Michael"
  - subject: "Re: The president"
    date: 2007-09-22
    body: "Eva was president for the larger part of the reporting period. Aaron will be in the next Report."
    author: "Sebastian K\u00fcgler"
  - subject: "Interest"
    date: 2007-09-24
    body: "Nice, thanks for the informative report KDE e.V.\n\nOne thing I didn't see in the financial section was income due to interest.  Is the balance of KDE's account not generating any or was it just not listed?"
    author: "Leo S"
  - subject: ".pdf not web friendly"
    date: 2007-09-24
    body: "Can get this published in a web friendly format please.  .pdf works baddly on the web.  (It is great for printing, and I don't argue that it shouldn't be published in .pdf for those who want to print it)  My monitor is not large enough that I can fit a full printed page on my screen (at least not with font sizes that my eyes can read), and the 2-column format used makes scrolling to read it difficult.\n\nPlease, don't use pdf for anything intended to be read on electronic devices.  "
    author: "Hank Miller"
  - subject: "Re: .pdf not web friendly"
    date: 2007-09-25
    body: "What a nonsence. PDF is very well suited for this. It preserves layout, for starters, which is important for documents like this. If you prefer to read it of paper, just make a printout."
    author: "Andr\u00e9"
  - subject: "Re: .pdf not web friendly"
    date: 2007-09-29
    body: "What kind of format would your prefer, then? HTML? And what effect would that have exactly?\n\nThe only advantage I could think of would be that you can define the font size manually on your computer and that it tries to still keep the basic layout with some strange magic."
    author: "liquidat"
---
The <a href="http://ev.kde.org/reports/ev-quarterly-2007Q2.pdf">KDE e.V. Quarterly Report</a> is now available for Q2 2007, covering April, May and June 2007. Topics covered include the three scheduled developer meetings of the quarter (for Akonadi, ODF, and Oxygen) and reports from the Marketing Working Group, Human Computer Interaction Working Group, and System Administration Team. All long term KDE contributors are welcome to join the <a href="http://ev.kde.org/getinvolved/members.php">KDE e.V</a>.


<!--break-->
