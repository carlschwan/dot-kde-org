---
title: "The Road to KDE 4: Job Progress Reimagined"
date:    2007-01-24
authors:
  - "tunrau"
slug:    road-kde-4-job-progress-reimagined
comments:
  - subject: "Now we just need the plasmoid"
    date: 2007-01-23
    body: "ok so this is great I begin looking foward tot he day when things like this would start poping up. I guess all that we need now is the bar plasmoid and some polish to make it great."
    author: "tikal26"
  - subject: "Re: Now we just need the plasmoid"
    date: 2007-01-23
    body: "\"...blicly available in SVN, and _anyone_ can reproduce my results...\"\n\nI try but I don't manage.\nSeems to fail during the build of kdelibs.\nBut, the day befor yesterday, Qt didn't compile, know it does, so....\n\n:)\n\nNice article, and not so ugly sreen shot compared to mockups.\nPre alpha software may be not so smart, because it cause a \"I whant one\" thing in you, but if theire were ugly things, I would try to compile too :) just for fun."
    author: "kollum"
  - subject: "Re: Now we just need the plasmoid"
    date: 2007-01-24
    body: "Don't try to build kdelibs during a monday. That's the day when incompatible changes are introduced.  And remember that monday takes 48 hours, since it covers all of the globe in 24 different time zones."
    author: "Inge Wallin"
  - subject: "Re: Now we just need the plasmoid"
    date: 2007-01-24
    body: ">Don't try to build kdelibs during a monday.\nOr: Don't try to build a monday's checkout of kdelibs. :-)\n\n-> svn co -r { [perhaps saturday's date] } URL \nis your friend."
    author: "Andreas Leuner"
  - subject: "Re: Now we just need the plasmoid"
    date: 2007-01-24
    body: "This time of the year, the globe has 25 timezones, not 24. New Zealand is actually now in GMT+13 (more than half a day ahead of GMT) because they are in Daylight Savings Time.\n\nTrivia of the year..."
    author: "Thiago Macieira"
  - subject: "Re: Now we just need the plasmoid"
    date: 2007-01-24
    body: "> This time of the year, the globe has 25 timezones, not 24. \n> New Zealand is actually now in GMT+13 (more than half a day ahead of GMT)\n> because they are in Daylight Savings Time.\n\nThey are actually in summer time but the GMT+13 is correct, yes ;)\nQuite near where the EU goes one back to summer time, they go to DST and the difference suddenly jumps from 12 to 14 hours.\n\nok, straying too far off topic ;)"
    author: "Thomas Zander"
  - subject: "Re: Now we just need the plasmoid"
    date: 2007-01-24
    body: "... not 14 but 10 hours.\n\n(Geetings to my sister in New Zealand from Germany ;-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Now we just need the plasmoid"
    date: 2007-01-25
    body: "10 in one direction, 14 in the other (the world is round after all) :D"
    author: "MamiyaOtaru"
  - subject: "Digg this"
    date: 2007-01-23
    body: "Help spread the news about this great feature by digging here:\nhttp://digg.com/software/The_Road_to_KDE_4_Job_Progress_Reimagined_Screenshots"
    author: "Tsiolkovsky"
  - subject: "Original Idea"
    date: 2007-01-23
    body: "I saw this idea by Amaury Chamayou on the old kde-artists forum.\n\nDo someone know where all these beautiful mockups are gone ?"
    author: "Marc"
  - subject: "Re: Original Idea"
    date: 2007-01-24
    body: "You can find some of the stuff from the old kde-artists forum here: http://e-gandalf.net/wiki/index.php/KDE4_Features"
    author: "Christian L."
  - subject: "Re: Original Idea"
    date: 2007-01-24
    body: "Yep, I came up with the idea, and Amaury mocked it up.  It was very disappointing to have our hard work disappear, along with lots of other peoples' :/  Nice to see it back, in some form! :)\n\nIt's a pity my other idea, about having audio volumes on a per-application-type basis, rather than on a hardware basis (so, volumes for background music, games, im notifications, etc.), didn't survive (or did it??).  I was much more interested in seeing that in KDE4 ;)"
    author: "Lee"
  - subject: "Re: Original Idea"
    date: 2007-01-24
    body: "That said... the other mockup post referenced above has a great new idea, of having konqueror show file progress IN the file browser.  As long as that's not the only place it's available (since I don't use konqi as a file browser all that often), it's really nice."
    author: "Lee"
  - subject: "Re: Original Idea"
    date: 2007-01-24
    body: "\"It's a pity my other idea, about having audio volumes on a per-application-type basis [...] didn't survive (or did it??).\"\n\nPhonon will probably be able to support this, it has categories for different types of audio, so it should not be difficult to set the volume according to that category.\n\nsee http://phonon.kde.org/cms/1030"
    author: "Christoph"
  - subject: "Re: Original Idea"
    date: 2007-01-24
    body: "it'll support it, already does... i see now several categories in kmix from KDE 4 ;-)"
    author: "superstoned"
  - subject: "Tanks for this series"
    date: 2007-01-24
    body: "I really like this KDE4 series, it's brilliant.\nTanks"
    author: "stepan"
  - subject: "Re: Tanks for this series"
    date: 2007-01-24
    body: "Thanks for keeping everyone informed Troy :)\n(Event though as a compulsive kde/trunk builder, I have seen much of it before)"
    author: "Robert Knight"
  - subject: "Re: Tanks for this series"
    date: 2007-01-24
    body: "Yeah it's really nice, and it just shows that KDE 4 is not just a hype, but that a lot of the stuff promised is going to be there."
    author: "terracotta"
  - subject: "which is to be noted"
    date: 2007-01-24
    body: "as an incredible achivement of flexible libraries and forward-looking development. Wow, KDE does rule *soo* much.."
    author: "eMPe"
  - subject: "Re: Tanks for this series"
    date: 2007-01-24
    body: "++\n\nThese articles have been amazing, can't wait for the next one :)"
    author: "Joergen Ramskov"
  - subject: "IF we had more developers"
    date: 2007-01-24
    body: "KDE 4 would already have been released !"
    author: "chri"
  - subject: "Re: IF we had more developers"
    date: 2007-01-24
    body: "Yes, and if we had even more developers, it would have been released in the middle of the last year.\n\nBut the question is: If we had even even more more developers, is it possible to release kde before the first release and feature plan was made ?"
    author: "Felix"
  - subject: "Re: IF we had more developers"
    date: 2007-01-24
    body: "of course!!! we would have had KDE 4 in 1995... :D"
    author: "superstoned"
  - subject: "Re: IF we had more developers"
    date: 2007-01-24
    body: "http://en.wikipedia.org/wiki/The_Mythical_Man-Month"
    author: "Birdy"
  - subject: "Re: IF we had more developers"
    date: 2007-01-24
    body: "Actually some or all of that isn't applicible to Open Source Development."
    author: "x"
  - subject: "Re: IF we had more developers"
    date: 2007-01-24
    body: "Could you extrapolate a bit on this?\n( I am actually interested. )"
    author: "reihal"
  - subject: "Thanks"
    date: 2007-01-24
    body: "Thanks a lot! This series definitely has me psyched!"
    author: "Daniel"
  - subject: "please no vertical running text"
    date: 2007-01-24
    body: "vertial running text is a design flaw in many case; allthough we can do it quite easily with oure strong qt backend i argue against all its use (in horzontal languages).\n\nthere are about 3 words we can easily read vertical:\n\nB\nA\nR\n\n,\n\nH\nO\nT\nE\nL\n\nand\n\nS\nE\nX\n\n\nthe others take significantly more time to recognise.\n\n(ok i understand that my examples are not exactly fit for use in this arguement :-/ sorry, i hope you get my point though...)"
    author: "cies"
  - subject: "Re: please no vertical running text"
    date: 2007-01-24
    body: "Hehe :)"
    author: "Mark Kretschmann"
  - subject: "Re: please no vertical running text"
    date: 2007-01-26
    body: "While funny, he's absolutely right. In some apps you use more often, you simply memorize in which region you have to click to change the sidebar view. But actually having to read the labels on these vertical tabs, sucks. It would be much better to spend a bit more horizontal space and use well chosen pictograms, instead labels. Problem is the (non-)accessibility of pictograms."
    author: "Carlo"
  - subject: "Re: please no vertical running text"
    date: 2007-01-24
    body: "It's not the same kind of vertical text. That text is rotated, which is easier to read than vertical text, IMO."
    author: "winter"
  - subject: "Re: please no vertical running text"
    date: 2007-01-25
    body: "indeed, that's what i meant by:\n\n> > i understand that my examples are not exactly fit for use in this argument\n\nstill the other kind of vertical that is used in the mock-ups is not quickfast readable/recognizable. people even tend to turn their head for these kinds of tasks: really!\n\n"
    author: "cies"
  - subject: "Re: please no vertical running text"
    date: 2007-01-25
    body: "I think entries are meant to be primarily recognised by their icon. In that case tilting your head to read the text seems fairly acceptable as it shouldn't be happening too often."
    author: "Cerulean"
  - subject: "Re: please no vertical running text"
    date: 2007-01-26
    body: "In fact, I did turn my head! I see the space saving, but it is not read at  glance."
    author: "Henk"
  - subject: "Queued tasks"
    date: 2007-01-24
    body: "In many programs there is the concept of queued tasks, (downloading usually) and in others, it would be really useful to have this feature.  The one that I run into time and time again is copying a file on the local hard drive.  \n\nSuppose I need to copy two large files to different locations on the same physical hard drive.  They each have different start and destination locations.  In konqueror (and any file manager) I can copy one, and it starts copying at a decent speed.  Then I copy the next one and both slow down by an order of magnitude because now the disk is thrashing back and forth doing two transfers.  If each transfer on its own would have taken 2 min, I would now be waiting probably 30 min for both of them to complete.\n\nWhat we really need is a way to queue file transfer operations for media that has issues with two operations at the same time.  HTTP downloads can already do this, if you start more downloads than the server will allow from your IP, then some of them will not start until the others finish.  \n\nI guess implementing this is more in the scope of konqueror, and not this job manager."
    author: "Leo S"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "This is not true.  Modern Linux and UNIXes have advanced disk scheduling algorithms based on \"elevators\" (think of an elevator sweeping up and down a building) that avoid the thrashing and in practice two or three files copied simultaneously have almost no impact in total disk throughput.  They also minimize the effects of disk fragmentation (though not so much)."
    author: "Rudd-O"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "Well it sure is true on my system.  Maybe I don't have this disk scheduling enabled or something.  \nBut just try it, if you copy two files on the same disk at the same time, they don't slow down more than 50% each?   I find that hard to believe, as it completely contradicts my experience on every system I've ever seen.\n\nCan anyone back this up?"
    author: "Leo S"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "I can back you up - it's an annoyance.  Performance drops through the floor when copying multiple large files simultaneously.\n\nIt would be great if this job progress manager allowed one to pause a job until another one had finished.\n\nMaybe:\n\nRMB->file copy job->pause this job until this job has finished->another file copy job\n\nThis would have other benefits - you could start downloading a large .tar.gz, ask ark to extract it and then pause the task until the download had finished."
    author: "lanroth"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "Reminds me of the time I ran Nautilus (GNOME file-manager for you KDE guys :) on a multi-processor system, on a CDROM mounted directory.\n\nNow, someone was smart and made Nautilus launch thumbnail generators in parallel, one for each CPU, and that is a good thing.  Worked great on hard disk or network folders.  However, on the CDROM I thought the system was going to crash, it got so laggy.\n\nRead a bit of file here, seek, seek, seeeeeeeeeeek, read a bit of file over here, seeek, seeeeeeek, seeeeeeeek, repeat.\n\n:)"
    author: "Zan Lynx"
  - subject: "Re: Queued tasks"
    date: 2007-01-25
    body: "In my experience it's often the opposite of what you state (1 transfers at X MB/s, 2 at 1.5 X MB/s which often confuses me, though at some point adding more does cause it to go slower), though I think that a configuration option (probably would have to be in Konqueror, with the options based on the kio-slave) to say the number of concurrent transfers before queuing up new ones (with an option to allow 'infinite')"
    author: "Corbin"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "you are wrong. although the elevator algorithm is rather good, you will still loose a lot of performance when copying two sequential files. reason being that for a single sequential file copy there is no seek time at all and the seek time is significantly larger than the time it takes to read a whole cylinder (ie. multiple sectors).\n\nthis problem becomes more significant as hdd size increases."
    author: "anonymous"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "with a modern (>2.6.18) linux kernel and using the (default) CFQ scheduler, the performance difference will be very small (eg a few % max). so, not to worry about."
    author: "superstoned"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "Can you expand on this? I don't really know how CFQ works, but, the \"Fair\" in its name makes me think that it would not be the best choice for this scenario. Basically, we have two large tasks, with a very high switching cost between the two. To optimize performance, we should minimize the number of switches, that is serving each task for a very long time interval before switching to the other (the best being serving one task until it is completely finished and then going to the other). This does not sound \"Fair\", to me."
    author: "giu73"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "but it is what CFQ does, both ensuring task interactivity AND increasing throughput. compared to the previous kernel scheduler, cfq uses longer timeslices, but has some logic to ensure reads don't slow down, and there are maximum waits for apps who want to read and write data."
    author: "superstoned"
  - subject: "Re: Queued tasks"
    date: 2007-01-24
    body: "If your system becomes really slow with copying files, verify whether you have DMA enabled. The Linux kernel does a decent job of scheduelling IO.\n\nIn fact, once I had to copy a lot of files in Windows and back in Linux. Windows Explorer and IE froze while copying, and the disk made a lot of noice. Linux was very quiet, and only showed short disk activity after 5 files (~30MB). Copying the files took 3 times longer in Windows because it used the disk very inefficiently."
    author: "Diederik van der Boor"
  - subject: "Re: Queued tasks"
    date: 2007-09-08
    body: "Yet another example of inefficient M$ products :)"
    author: "riddle"
  - subject: "The taskbar!"
    date: 2007-01-24
    body: "It'd be great if the taskbar were updated to take advantage of this so that each application could display at least one progress bar there, and maybe a mouseover would show you all of the progress bars for that application, as well as let you select the the one to be always displayed.\n\nWhile I'm asking, it'd be nice to see progress bars in the tab bar for every page that is loading :) "
    author: "Luke"
  - subject: "Re: The taskbar!"
    date: 2007-09-21
    body: "Agreed. That would make perfect sense. Something like this?\nhttps://addons.mozilla.org/en-US/firefox/images/preview/1122/3\n"
    author: "riddle"
  - subject: "Thank you all guys"
    date: 2007-01-24
    body: "Hi,\n\nI just want to thank you all your feedback. I am really glad to see people in general like what is going on at KDE trunk, and for KDE 4 series in general.\n\n* The plasmoid is the idea. When plasmoids specification are available, I'd like to write some kind of plasmoid for this, or maybe move what I have right now there, we will see. But I think you can bet for a plasmoid about jobs.\n\n* The queuing. Well, it can be implemented in the kuiserver, and I really think it would make sense here. I think it is really possible, and that I'm going to improve it to let you have an option for \"Queue transfer jobs as they arrive\", with the possibility to \"Resume\" the one that you want if you want to have some jobs on parallel.\n\nI really appreciate all your opinions and ideas.\n\nThank you all again,\nRafael Fern\u00e1ndez L\u00f3pez."
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Re: Thank you all guys"
    date: 2007-01-24
    body: "Hi Rafael,\n\nFirst of all, THANKS. You're doing an awesome job of an already nice idea and I'm highly looking forward to seeing your work make my life with the KDE desktop (even) easier. :)\n\nSpeaking of optionally queueing tasks: shouldn't that be based on the *ressource* being used, where ressource would be 'disk I/O' or 'network I/O' and such? Sometimes it makes sense to download as much as half a dozen things at the same time, while moving no more than one local file at once. Well, hope I'm not talking out of my ass anyway...\n\nThanks again!"
    author: "A.C."
  - subject: "Re: Thank you all guys"
    date: 2007-01-24
    body: "Rafael, the job you've done is great, but don't you think there's way too much space used for each task? Imagine 10 tasks at a time and you'd have a huge scrolling bar."
    author: "Carlos"
  - subject: "Re: Thank you all guys"
    date: 2007-01-24
    body: "The application you see is an initial prototype useful more to prove the backend work is functional.  When the plasmoid data engine is done, the applets should be able to format this information however they see fit."
    author: "Troy Unrau"
  - subject: "Re: Thank you all guys"
    date: 2007-01-24
    body: "Thank you for your work Rafael !\nAbout I/O job queuing, you may find some ideas here : \nhttp://sourceforge.net/projects/supercopier/\nI DO know this is a Windows application, but it has interesting features : transfer speed control, transfer resuming (and pausing) and the possibility to change queue order.\nThis kind of feature already exists in many http or ftp client. This is really something I miss for simple disk to disk transfer :(.\n\nGood luck !"
    author: "Shamaz"
  - subject: "Re: Thank you all guys"
    date: 2007-01-24
    body: "Nice to see KDE is doing this via D-Bus, so even Gnome programs con benefit in the future. \nI've made two mockups for Gnome but perhaps you are even interested in it:\nThis one shows the progress directly in the Icons:\nhttp://hagemaenner.de/stuff/gnome/ActiveIcons.png\nAnd this one in the Taskbar:\nhttp://hagemaenner.de/stuff/gnome/gTask.png"
    author: "Max"
  - subject: "Re: Thank you all guys"
    date: 2007-01-24
    body: "KDE is now doing almost everything over D-Bus.\n\nRemember that since last May, D-Bus is the official IPC/RPC mechanism for KDE 4.\n\nOne thing I want to do in the near future (whenever I find the time) is to port KIO's own protocol to D-Bus. This would expose the ioslaves to D-Bus, if necessary. That's one of the two remaining non-D-Bus IPC mechanisms that I can remember right now (the other one being the communication between klauncher and kdeinit, but we won't change that).\n\nNow, I'd like to point out that this is not an unified VFS mechanism that many people are clamoring for. But it may be a start, if it proves useful, and it may pave the way to a real implementation."
    author: "Thiago Macieira"
  - subject: "Re: Thank you all guys"
    date: 2007-01-24
    body: "It could be done by adding an option \"Start when [other_job] is finished\", where [other_job] is a dropdown with all active jobs, for example. Jobs that let you pause them can have this feature enabled then.\n\nJust an idea, anyway. Thanks for the great article and this cool new feature which will reduce clutter significantly!"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Thank you all guys"
    date: 2007-01-24
    body: "Or by selecting multiple entries in the list, then Rightclick->\"Queue Us\" or something like that.\nAnyway I think this is a really cool feature !"
    author: "Jens Rutschmann"
  - subject: "Konqueror"
    date: 2007-01-24
    body: "Will we also see the lower part of the screenshot implemented, where Konqueror implements its own observer to show download progress in-place?"
    author: "Martin"
  - subject: "Re: Konqueror"
    date: 2007-01-24
    body: "will depend on konqi, i guess... won't be very hard with the whole infrastructure in place, tough."
    author: "superstoned"
  - subject: "This is great!"
    date: 2007-01-24
    body: "This is good news - I like this idea very much. It answers that question why do I have so many progress dialogs floating all over my desktop.  I often need to know how a print job is progressing, how a file transfer is going. Having it all in one place will be so much clearer for me and new users. Ever seen a newbie look for info on a print job on any OS let alone KDE?\n\nWill it be easy to make hooks for CLI apps? I guess with Dbus it could be. I would love a hook to rsync so I can see how a my home brewed backup is going! "
    author: "Kevin Colyer"
  - subject: "Re: This is great!"
    date: 2007-01-24
    body: "If rsync -- or any tool -- outputs a percentage, it should be quite simple to write a bash script reads from a pipe, creates an entry in the uiserver and updates it. An example tool I can think of right now is cmake & KDE's own build.\n\nHow cool would it be to see the KDE build percentage progress in the uiserver? :-)\n\nIf you need to implement actions like \"pause\", \"cancel\", etc., you will find that bash+qdbus won't suffice. Scripting languages like QtScript, Ruby or Python will come in handy then."
    author: "Thiago Macieira"
  - subject: "ideas"
    date: 2007-01-24
    body: "It would be great if you could pause a job, reboot and continue it later (session management\n\nThis might not be the uiserver's responsibility, but when copying a lot of files, is it possible to FIRST ask for confirmations for all to-be-copied files (do you really want to overwrite etc.) and THEN start copying? That way, I can leave the computer and be sure its done when I come back!\n\nThe copying-two-files-at-the-same-time-becomes-too-slow-problem is even worse when you're reading them from a cd/dvd!"
    author: "me"
  - subject: "Re: ideas"
    date: 2007-01-24
    body: "Great Idee, one of the things that most irritate me, having a directory moved under windows and for every thumb.db, it starts to ask again, if I'm sure... I know it is windows... But watching this progress for KDE 4 makes me feel better every time...."
    author: "boemer"
  - subject: "Conflict resolution dialog"
    date: 2007-01-28
    body: "\"is it possible to FIRST ask for confirmations for all to-be-copied files (do you really want to overwrite etc.) and THEN start copying?\"\n\nBetter yet, it would be great if the system popped up a conflict resolution window where you could check which files you want to overwrite and which you don't want to overwrite.  Then it would be very simple to select all, and perhaps uncheck a couple."
    author: "Boris"
  - subject: "Re: Conflict resolution dialog"
    date: 2007-02-02
    body: "I really like that idea. Perhaps a two-pane view\n\nfile1.jpg     2/1/07  102K   <->   file1.jpg     12/27/06  99K\ndocument.ods  2/1/07  73K    <->   document.ods  12/29/06  99K\n\nwhere if you hover over the filerow you get a thumbnail comparison of those documents. With option to view details (which opens up in appropriate app).\n\n  thumbnail \\/\n __________________________\n|    _______     _______   |\n|   |       |   |       |  |\n|   | PIC 1 |   | PIC 2 |  |\n|   |_______|   |_______|  |\n|   View det      View det |\n|__________________________| \n\nReally ugly above I know but?"
    author: "Anon_Poster"
  - subject: "KDE4 looks incredible"
    date: 2007-01-24
    body: "I cannot wait for the beta/RC and release. It is so wonderful, it is going to be so biiiiig improvement to desktop experience.\n\nI wish all the people to use KDE desktop!"
    author: "Stromek"
  - subject: "Re: KDE4 looks incredible"
    date: 2007-01-25
    body: "My wish is a peacefull world where everybody can do what he/she wants to (accept harming somebody AND something else).\n\nWhy should all people use KDE? There is no meaning full reasing for such a wish. Wasn't that something good about all the Linux/BSD/Unix/KDE/GNOME/Fluxbox/... thingy? Use what best fits to you."
    author: "wish"
  - subject: "Re: KDE4 looks incredible"
    date: 2007-01-25
    body: "Yes, freedom of choice is one of the great things about open source.  But I see his/her wish that everyone use KDE as an expression of excitement about the upcoming KDE4 (and KDE in general).  There is nothing wrong with that.  I didn't take it as trying to mandate that everyone use KDE."
    author: "cirehawk"
  - subject: "Programmer queston"
    date: 2007-01-24
    body: "When I see examples like today at PlanetKDE (http://www.ereslibre.es/?p=11), I get a bit annoyed to see such generic name as \"Observer\" being used for this. How about a name like JobObserver or KJob::Observer?"
    author: "Diederik van der Boor"
  - subject: "Re: Programmer queston"
    date: 2007-01-24
    body: "It's KIO::Observer right now.\n\nThe name may change if it moves out of libkio into libkdecore."
    author: "Thiago Macieira"
  - subject: "Re: Programmer queston"
    date: 2007-01-24
    body: "Thanks!"
    author: "Diederik van der Boor"
  - subject: "This is very good"
    date: 2007-01-24
    body: "I like this, it's not a new concept, you can see this in  beos tracker and mac os finder, but limited to file transfers, now a good point would be interoperability, like you said there are already some talks with gnome folks, but I think that a further step would be some kind of freedesktop job queue with common API that can be implemented in every DE, so if I run kde and want to run gftp for example I could see gftp's jobs run in kde tasks."
    author: "ra1n"
  - subject: "Re: This is very good"
    date: 2007-01-24
    body: "read the article: it'll be d-bus based, and they are already working on standardizing the d-bus interface."
    author: "superstoned"
  - subject: "Re: This is very good"
    date: 2007-01-25
    body: "well it's not too obvious from the article, there are talks with gnome folks and the use of d-bus, but nothing it's decided yet, especially on the other side of the desktop ;-)"
    author: "ra1n"
  - subject: "\"Cancel\" will finally mean something"
    date: 2007-01-24
    body: "It seems this is a great use of multi threading - being able to cancel a long operation.  Most Qt3 and KDE3 apps, even if they have a cancel button your are at the mercy of the event loop before if and when it decides to accept the cancel callback.\n\nI think this will go along way in making KDE 4 more user friendly. May I suggest that it be designed to not only work with DBus for interprocess communications but internal to the app itself. The goal being to make it simpler for programmers to do multi threading for simple thins like a \"cancel\" operation\n\nWay to go Rafael"
    author: "oldschool"
  - subject: "Re: \"Cancel\" will finally mean something"
    date: 2007-01-24
    body: "I hope to make it possible to use D-Bus to do that, so the integration is seamless.\n\nNo promises though."
    author: "Thiago Macieira"
  - subject: "does it integrate with virtual desktops?"
    date: 2007-01-24
    body: "How is this supposed to work with multiple desktops? Is it going to be like the taskbar: \"Show progress information from all desktops\", \"Group similar jobs\", etc.?"
    author: "testerus"
  - subject: "The looks of the mockup are great."
    date: 2007-01-24
    body: "Is that an existing look or something planned or just blue sky?"
    author: "Tom Corbin"
  - subject: "Re: The looks of the mockup are great."
    date: 2007-01-24
    body: "Mostly just a mockup - kiras has some ideas of how he would like KDE 4 to evolve, which is good as some of the ideas are sound.  Other ones are less likely to be implemented from his mockups.  You see the little configure button on the window titlebar - well, his idea would be that when you click that button, the window flips over using OpenGL and displays the options on the backside.  Not likely to happen for a variety of technical and usability reasons, but! the mockup sure looks sweet :)\n\nKonq at the moment looks pretty much like KDE 3's konq, except with improved HTML/Javascript rendering :)"
    author: "Troy Unrau"
  - subject: "This is great!!"
    date: 2007-01-24
    body: "I loved the mockup screenshot when I first saw it on KDE-look.org. I am totally excited to see that this awesome screenshot transforming into actual code! It's also bolstered my confidence that Konqueror will include improvements from some of the KDE4 mockups on KDE-look.org.\n\nThis progress applet will be so cool! A huge THANKS to all the KDE4 developers out there!"
    author: "kwilliam"
  - subject: "mockup"
    date: 2007-01-24
    body: "\nhttp://www.kde-look.org/content/show.php?content=36385\n\nhttp://www.kde-look.org/content/show.php?content=28476"
    author: "vicko"
  - subject: "Job Progress Applet"
    date: 2007-01-24
    body: "Um, I'd like to point out that some apps, like KTorrent may have multiple tasks waiting to finish... Are we going to have drop-down lists of subtasks for each app? and display an 'overall progress' bar at the top of the list when it's collapsed? Just an idea for making the app useful for one more potential user."
    author: "Alex"
  - subject: "Re: Job Progress Applet"
    date: 2007-09-21
    body: "That should be fairly easy to implement...\n\nYou're thinking like this, right?\n--------------------------------\n-Konqueror\n  file1 copy: [=========       ]\n  file2 copy: [=============   ]\n+K4B\n+KTorrent\n--------------------------------\n(Ugly, I know)\n"
    author: "riddle"
  - subject: "Re: Job Progress Applet"
    date: 2007-09-21
    body: "Oops, I just checked out the latest beta. It already supports that.\n\nSmile ;)\n"
    author: "riddle"
  - subject: "I think..."
    date: 2007-01-25
    body: "that the main panel buttons should reflect this. Whereas the windows taskbar buttons flash blue when the window wants attention, we could turn those buttons into miniature progress bars that would scroll their text."
    author: "David"
  - subject: "What about YOU?"
    date: 2007-01-25
    body: "Hey user, ever thought abiut your role in this story? Your are not a programmer and you have no idea what you can do?\nYou can contribute in many ways by donations and thinks like that. Help the developers to cover their affords and give them something since they have spended a lot for you. They  need money for meetings, textbooks, travel cost, ... . KDE is a community project ... and you are the community! Here is how you can support KDE:\nhttp://www.kde.org/support/\n\nBut what do I see here?\nhttp://www.kde.org/support/donations.php\nWhat do I have to read? Last donation on Nov 28, 2006? Oh dear, isn'that poor? Remember, some little helps if a lot of people help a little! ;)"
    author: "great"
  - subject: "Really nice thought"
    date: 2007-01-25
    body: "I'm working daily with kde / kate to work off my development tasks. Though, it sounds very interesting, but the possibility to suppress those other windows from even opening would be worth a thought.\nI'm thinking on developing on a machine with kate, code being on another machine (compilefarm / testing), connecting by fish. By opening thos files, a hundret and three dialogs open which i don't really want to see.\nI'm aware that this is not what this interface is supposed to do, but still worth the tought.\n\nThe folding idea on subtasks sounds interesting, like \"program groups\",  as application you see on the right side on the screenshot (processing x tasks and on a closer look the list).\n\nBesides all those other projects i work on i could even imagine on getting my hands on this, since it could be a important feature for daily work, to see status of downloads copies transfers cd buring or ripping collected, without openeing one after the other application just to take a short look.\n\nKind regards,\nGeorg"
    author: "Georg Grabler"
  - subject: "new GUI"
    date: 2007-01-27
    body: "All of those improvements are good but I think it will be too hard to release on the current system. When I look at the mockup I understand that something looking like this is unreal without HTML+CSS-like technology. I think the better GUI improvement of KDE that can be is an ability to use XML+XSLT+CSS for rendering windows."
    author: "Anton"
  - subject: "Re: new GUI"
    date: 2007-01-27
    body: "why do you think limiting the rendering to html would be a good idea?\n\nkde styles are written in c++. they are programmed. you can do almost everything you want in a kde/qt style right now, while they are still very fast.\n\nusing html based rendering for styles would only make kde realy slow. just look at firefox and its memory usage."
    author: "asd"
  - subject: "Re: new GUI"
    date: 2007-02-10
    body: "Why limiting? And why html? I meant just the stylesheet model but not the html self.\n\nYes, you can do everything you want in a kde/qt style. But why then\na lot of kde applications looks so ugly and awkward? And why all the kde mockups are so differ from their realizations? Because C++ programmers in most cases are unable to make good UI and good designers cant use C++. I'm saying this as a web-developer and a former C++ programmer.\n\nFirefox is not so slow as efficient with all of its extensions written by many enthusiasts. And firefox styles are also programmed ;)"
    author: "Anton"
  - subject: "KDE4 development"
    date: 2007-01-27
    body: "\"If you'd like to get involved in KDE 4 development, adding support for the new KJobs progress monitoring is a fairly easy entry point to KDE programming. It takes only a few lines of code to adapt an application to display progress, and a few more lines to make the action buttons useful.\"\n\nHow can I get involved?"
    author: "Pebete"
  - subject: "Re: KDE4 development"
    date: 2007-01-27
    body: "Contact ereslibre on irc.kde.org... he may be able to help you out with an example application or somesuch."
    author: "Troy Unrau"
  - subject: "Re: KDE4 development"
    date: 2007-01-27
    body: "Thank you."
    author: "Pebete"
  - subject: "Notifications too?"
    date: 2007-01-28
    body: "Its great that we will finally get a unified and tidy way of displaying and keeping task progress together and I'm sure this framework will make lots of developer's lives easier.\n\nI currently use Metamonitor (http://metamonitor.sourceforge.net) for getting notifications of certain events through system logs (such as plugging in USB devices and my backup scripts running, etc). Would it be possible to combine this functionality with this progress manager?\n\nThey're both concepts that require a dialog to be shown for a certain period of time, usually near the system tray area for neatness and consistency and may be stacked up with several other dialogs. I also think it would be useful for it to show a recent history of notifications too as you can sometimes miss what these notifications say because of the popup auto-close (which is normally a good feature IMHO). Applications/services such as Kopete and KBluetooth could then use the same framework for their notifications."
    author: "Colin"
  - subject: "Re: Notifications too?"
    date: 2007-01-28
    body: "Hi,\n\nFirst of all. This app is really great. I find it very useful.\n\nWell, all the available methods you can call are on kdelibs/kio/kio/observer.h.\n\nI don't recommend you to use them right now, because they can suffer strong modifications from monday to monday. Anyway you can take a look there and see what you find :)\n\nBye,\nRafael Fern\u00e1ndez L\u00f3pez."
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "One suggestion"
    date: 2007-02-01
    body: "That's great idea.\nBut please add an option \"Shutdown computer after all jobs are finished\" so user could go sleep, not waiting for printing/downloading/file copying being finished :)\nCan't wait for KDE 4 release."
    author: "Maciej Cencora"
  - subject: "I Know this doesn't have anything to do with this "
    date: 2007-02-02
    body: "But what about doing something about the taskbar hell????\n\nI Believe Steve Jobs got it right with Just showing icons for launchers, and then when you launch them they get an arrow to show they're active, and if you click it it shows the windows. Of course there is expose for kde, and its great, but I think kde guys should try to think something to  improve the taskbar hunting hell...\nvirtual desktops helps... sometimes... sometimes its more dificult to look for each  desktop looking for the window you want.\n\nMy biggest friend now is expose for kde because the taskbar is so slow to use.\n\nLet's learn from the masters! KOOLDOCK for everyone, or something even better? Some brainstorming in this area is needed!!!!"
    author: "dan"
  - subject: "Re: I Know this doesn't have anything to do with this "
    date: 2007-02-07
    body: "Feel free to make some mockups :-)"
    author: "JohnFlux"
  - subject: "Window"
    date: 2007-06-09
    body: "It would be nice if this could be implemented in a way that looks a bit more like the mockup, with a clean, borderless menu. That would be perfect.  "
    author: "Vladsinger"
  - subject: "Re: Window"
    date: 2007-06-10
    body: "It's look will be plasma's job. Just wait for it, I'm sure it will come"
    author: "Jonathan"
  - subject: "Job inerface for simply playing soundfiles?"
    date: 2007-08-25
    body: "This job progress interface can also be used to simply play a soundfile from the file manager, without opening a bloated media player with playlists and such, right? \n\nAs far as I understand, the interface even supports buttons, such as pause and seek (for example the \"Pause\" and \"Cancel\" buttons in the dialog for the download of \"install-x86-universal-2005.1.iso\" in the screenshot)."
    author: "Erik"
  - subject: "Please conserve screen space"
    date: 2007-09-19
    body: "Looking sweet, and something I have wanted for a long time. However, the mockups, while looking good, do waste a lot of screen space and could be smaller.\n\nAlso, without knowing anything about the underlying API's, it would be nice if it was possible to hook into it and listen to the status information. For instance, it would allow such things as adding LED bars on the computer case as status bars. Not only practical, but cooler than a great white shark with shades."
    author: "Anders Troberg"
---
Have you ever had your taskbar filled with 10 applications all 
doing something that involved waiting for a task to finish?  
Document Printing Progress, a K3b CD burning dialogue, Audio Encoding 
via KAudioCreator, File Transfers in Konqueror, Kopete, KTorrent, 
checking email in KMail... The new Jobs support in KDE 4 will unify 
the display of progress for these tasks, making it easy to see and manage what is happening on your system. Read on for details.






<!--break-->
<p>Picture it as a cross between the Firefox download manager and 
the KDE printer queue, except that there is no real restriction on 
what type of jobs can be monitored. The way it works is that each 
KDE 4 app that has a progress dialog adds a flag for 
something called an Observer. Then, a separate application can observe any running Jobs, displaying progress and even adding 
certain actions (like "Cancel Download") which can be submitted back 
to the application that actually has the progress dialog. So the 
applications like K3b, which already have very good progress 
reporting, will not lose their existing dialogs, but rather additionally permit this new applet to observe its progress so that all the progress bars can be pulled into a convenient place.</p>

<p>What started as a <a 
href="http://www.kde-look.org/content/show.php?content=33673"> 
mocked up KDE 4 Improvement</a> via <a 
href="http://www.kde-look.org">KDE-Look.org</a> has turned into a 
full-fledged KDE 4 integration project, thanks to <a 
href="http://ereslibre.livejournal.com/">Rafael Fernandez Lopez</a>.  
And there's been a lot of progress to the point where applications 
are already being adapted to the new infrastructure. Last 
Tuesday's "Binary Incompatible Changes" day saw much of the changes 
officially committed to the KDE 4 repository.</p>

<p>Below is the original mock up, done by KDE user and KDE-look.org 
contributor <a 
href="http://www.kde-look.org/usermanager/search.php?username=kiras"> 
kiras</a>, used with permission.  Click to see the full-sized 
mockup.</p>
<p align="center"><a 
href="http://static.kdenews.org/dannya/vol4_mockup.jpg"><img 
src="http://static.kdenews.org/dannya/vol4_mockup_small.jpg" width="640" height="512"
alt="mockup from kde-look.org by kiras, with permission" /></a></p>

<p>Please keep in mind that the above is a mockup, and does not 
necessarily reflect the ultimate look-and-feel goals of KDE 4, 
Plasma or Konqueror.</p>

<p>Currently, it is being prototyped as a standard system tray applet 
(similar to the printer queue in KDE 3.5.5) which would allow 
interoperability with GNOME's tray implementation as well. However, at this point only KDE applications can be observed, so monitoring download 
progress from Firefox for instance, is not currently supported. That is not to 
say it cannot be made to happen in the future since progress is observed using the standard D-Bus interprocess communication architecture. There are intentions to collaborate with the GNOME project's <a 
href="http://tw.apinc.org/weblog/2006/08/22">Mathusalem</a> team, a 
project of similar scope as this one.</p>

<p>Here is a screenshot of the current appearance of the monitoring 
application as it would appear when clicking on the tray applet. As 
you can see, it's already looking very useful.</p>

<p align="center">
<img src="http://static.kdenews.org/dannya/vol4_devel_uiserver.png" width="667" height="542" alt="uiserver screenshot, KDE 4" />
</p>

<p>As you can see, the Kopete buttons are mostly just placeholders 
at the moment, and only exist for testing purposes. However, when 
you click on one of those buttons, it actually sends a signal back to Kopete, 
and Kopete itself pops up that smaller dialog you see.</p>

<p>The Konqueror download progress bars you see being monitored represent 
an actual file download in progress. They continue even after Konqueror 
is closed. Useful action buttons like "Abort Download" are in the 
works.</p>

<p>If you'd like to get involved in KDE 4 development, adding 
support for the new KJobs progress monitoring is a fairly easy entry 
point to KDE programming.  It takes only a few lines of code to 
adapt an application to display progress, and a few more lines to 
make the action buttons useful.</p>

<p>This new progress monitoring technology will be able to be 
integrated into Konqueror (like in the mockup), desktop applets, and 
anything else that uses D-Bus. I can even imagine a small web-app 
that lets you monitor progress remotely...</p>

<p>Rafael's goal after the initial implementation is completed is to 
add persistence, such that when a job is complete, it would 
optionally stay listed until closed by the user.  He is also looking 
for feedback on this tool and its implementation for future improvements.</p>

<p>Look forward to more feature articles showcasing more great 
technologies for KDE 4.</p>

<blockquote>
A quick note on methodology: I make sure to use the KDE defaults for all of my screenshots, even if it's ugly, since then you can get a better sense of progress as KDE 4 evolves and grows from week to week. As a rule, all of the 
features I've demonstrated so far are publicly available in SVN, and 
anyone can reproduce my results. In today's article, I had to 
uncomment a single line of code to enable this in-development 
feature, which is an exception to my normal rules. Additionally, 
the Kopete progress support is not yet in the official KDE SVN repository, but Rafael uses it to test features.</blockquote>



