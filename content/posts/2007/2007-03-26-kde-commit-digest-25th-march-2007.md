---
title: "KDE Commit-Digest for 25th March 2007"
date:    2007-03-26
authors:
  - "dallen"
slug:    kde-commit-digest-25th-march-2007
comments:
  - subject: "Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "I should draw your attention to the relevant commit log:\n\n\"The window-burning-down effect (gee), except that there's no actual flame, since I'm lame at graphics. Should be easy to add anyway.\"\n\nIn other words - the basic changes in the core are there, help from people who like coding pretty 3D effects would be appreciated."
    author: "Robert Knight"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "Yes.  Right now the core is powerful enough that we can manipulate windows in all sorts of ways; we have windows that twist, windows that explode (shaders!), windows drawn multiple times to make them look all fuzzy, etc.\n\nBut there aren't many of us working on it, so anyone wanting to pitch in is more than welcome!  kwin@kde.org"
    author: "Philip Falkner"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "How much can you take from beryl/compiz?\n\nI admit I know next to nothing about this sort of thing, but there seems to be some NIH going on. The effects are all done in X AFAIK, and they already have aquamarine, which allows them to use kwin decorations. To me, it seems like taking beryl and integrating it with KDE with a qt configuration dialog and using aquamarine by default would do the trick. \n\nAgain, i dont understand this very much, but is this a case like khtml, where alternatives exist but the code you are working on will benefit non-kwin parts of KDE 4?"
    author: "D"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "It's simple I think. KWin is a full fetured and mature windowmanager, while Beryl/Compiz despite all their glitz are not. Last I tried it did not even have vertical maximize, like MMB does on KWin. And how well do they follow the ICCCM standards?\n\nAnd when you start a new windowmanager rather than join one of the myriad existing efforts, it sounds more like NIH. Add some more functionality to your already existing one is not.\n"
    author: "Morty"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "It does have vertical maximize now. Even per default on the right distro. Boot the newest Knoppix.\n\n"
    author: "directlink"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "Still, they have a lot of work to do. The most features are there, sure, but they have to clean up the pathetic mess that are their configuration tools/dialog, and they have to find and fix all the corner cases in window management - pretty much years off work.\n\nAlso, if I understand the kwin dev's, they don't have a proper plugin system, but just expose their internals to the plugins - making backwards compatibility or sharing plugins with other windowmanagers very hard (impossible)."
    author: "superstoned"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "Beryl still has some focus issues with their \"focus stealing prevention\" thing.  It doesn't work with kxdocker and yakuake to name a couple, and it just doesn't work nearly as well as kwin's focus stealing prevention."
    author: "Matt"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "You should read the following post:\n\nhttp://www.kdedevelopers.org/node/2172\n\nMy personal opinion, is that just using compiz/beryl because of the cool effects, is a mistake. KWin does an amazing job with window placement, and window focus stealing prevention. When I used compiz, it took me only a day or two to get bored and start missing KWin."
    author: "suy"
  - subject: "Re: Before anyone asks for screenshots of the Flam"
    date: 2007-03-26
    body: "Is there any use at all for this juvenile 3D noise?\nAll I want is an extra quick change of desktop, does Beryl/Compiz give this?\n(I haven't tried it yet)"
    author: "reihal"
  - subject: "Re: Before anyone asks for screenshots of the Flam"
    date: 2007-03-26
    body: "There isn't, of course. That doesn't stop the kids begging for eye candy, though."
    author: "Carlo"
  - subject: "Re: Before anyone asks for screenshots of the Flam"
    date: 2007-03-26
    body: "Actually a lot of the things that the GL compositors allow will be useful for increasing usability as well as making things more visually pleasing (not talking about eye candy).  Real time previews of the windows in the panel and task switcher is useful for seeing what window 'Konqueror <14>' is.  Also switching desktops/restoring windows won't have to slowly (as in noticeable) repaint themselves (right now I just moved this Konqueror window around on my desktop and if I kept moving the window slowly it would take about 5 seconds for Konqueror and Amarok to repaint themselves!).  GL compositing aren't just about wobbly windows (which can increase usability if used correctly!) and spinning cubes (which helps people understand and visualize the concept of virtual desktops), those are just the sexy features that attracts new developers and testers.\n\nSo again, the 'juvenile 3D noise' is what actually attracts the developers and testers (if compiz simply did what you wanted most likely beryl would have never been started, and there would be little interest in compiz/Xgl/AIGLX)."
    author: "Sutoka"
  - subject: "Re: Before anyone asks for screenshots of the Flam"
    date: 2007-03-27
    body: "\"Real time previews of the windows in the panel and task switcher is useful for seeing what window 'Konqueror <14>' is.\"\n\nNot really.  Previews are small enough that most app windows all look like white backgrounds with vague dashes of black text.  Some konqueror windows will be at a site with a different colored background, so that helps.  Multiple windows at one site won't, of course.\n\nThe whole preview thing turned out to be less than useful for me with Beryl.  Windows with the same toolkit/style often end up looking too much alike from a distance.  Differentiating by icons still wins IMHO."
    author: "MamiyaOtaru"
  - subject: "Re: Before anyone asks for screenshots of the Flam"
    date: 2007-03-27
    body: "(I have to study this subject)\n\nSo if KWin used the \"GL compositors\" it would speed things up if you have an 128MB graphics card without the \"juvenile 3D noise\"?"
    author: "reihal"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "> I admit I know next to nothing about this sort of thing, but there seems to\n> be some NIH going on.\n\nHi,\n\nThis is a valid concern, but in this case, I think, unjustified.\n\nFirst of all, KWin does a HELL of a lot more than Beryl -- other posters in this thread have touched on it a little. It would theorically be possible to patch up Beryl until it could do all that KWin can, but that would probably require more KDE integration than the Beryl guys would allow.\n\nSecondly, Lubos Lunak, KWin developper extraordinaire, did look into making KWin compatible with Beryl plugins. As it turns out, however, the implementation of Beryl's plugins might... possibly not be as modular as one might want, and they go poke into the depths of Beryl too much to make compatibility with KWin an option.\n\nSo at this point, it boils down to either using Beryl (and abandoning all that makes KWin nifty), which you can already do, mind; or prettifying KWin up to Beryl-ish standards.\n\nHowever... I wonder to what extent it would be possible to split the functionality -- window /management/ and window /effects/ -- between Beryl and KWin. Lubos, in case you're reading this -- hi! -- would it be possible to have the effects part of Beryl stripped down to a DBus server, that would be able to process such requests as 'do THIS_OPERATION on THAT_LIST_OF_WINDOW_IDS'? That way, KWin could still manage WHAT is being done to windows, which it is awesome at, while Beryl would see about HOW to do it.\n\nIs that a pipe dream?"
    author: "S."
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "I believe that is how Compiz and Beryl already work.  Each has a window manager (compiz and beryl respectively), and each has a window decorator (e.g. gnome-window-decorator and kde-window-decorator for compiz; emerald, aquamarine, and some other gnome one for beryl).\n\nAlthough, that is the opposite of what you would want because the window decorators they use are most likely very dependent on their respective window managers in the first place."
    author: "Matt"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "Thats not exactly what he was asking about.  The window decorator ONLY decorates the windows (draws the border).  It doesn't actually do any managing.  And before compiz the development was happening in GLcompmgr (I believe that was the name), which was independent of the window manager (mostly what S. was asking for), but apparently the developers agreed that it wasn't practical (too slow) to have them separate programs that communicated via IPC."
    author: "Sutoka"
  - subject: "Re: Before anyone asks for screenshots of the Flam"
    date: 2007-03-26
    body: "There probably is a little bit of NIH going on, but if you look at how tiny each effect is, you'll see that the duplication of effort is minimal; e.g. the explosion effect \n\nhttp://websvn.kde.org/branches/work/kwin_composite/effects/explosioneffect.cpp?revision=646315&view=markup\n\nis a puny < 200 lines of code, a lot of which is whitespace, comments, and boiler-plate.\n\nPersonally, I think Lubos has made a good decision here."
    author: "Anon"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "Would it be possible to wrapper Beryl plugins to do the magic? It seems to me that this would seriously save on duplication of effort in the long run.\n\nGiven the fact that no options showed up the Beryl panel when the plugins were not reinstalled upon an upgrade I did, it seems that most of the gruntwork is done within the plugins themselves."
    author: "James Smith"
  - subject: "Re: Before anyone asks for screenshots of the Flame..."
    date: 2007-03-26
    body: "If I understand the kwin dev's properly, they don't have a proper plugin system, but just expose their internals to the plugins - making backwards compatibility or sharing plugins with other windowmanagers very hard (impossible)."
    author: "superstoned"
  - subject: "SVG icons"
    date: 2007-03-26
    body: "May be this is a dumb question but why are there 7 icons for a mouse?, arent all of them _Scalable_ Vector Graphics?!!"
    author: "Carlos Licea"
  - subject: "Re: SVG icons"
    date: 2007-03-26
    body: "Well, There is no such thing as a dumb question.\n\nIcons are created in different sizes, like 16x16, 32x32 etc. Therefore they commit each icon 7 times =).\n\nAnd yes, SVG is used, at least, that's where they create there icons in. But they compile them into png's so it wouldn't bloat the user's performance.\n\nTho Aaron came up with an interesting idea to use SVG images using caching: http://aseigo.blogspot.com/2007/03/efficient-scallable-icons.html"
    author: "Niels van Mourik"
  - subject: "Re: SVG icons"
    date: 2007-03-26
    body: "So if SVG is the \"source\" of the icons, shouldn't they be committed once, then png(s) built at compilation ?"
    author: "JSob"
  - subject: "Re: SVG icons"
    date: 2007-03-26
    body: "Many of these icons have to be build using inkscape, afaik that's the problem. There aren't many svg render engines having all the features these artists use..."
    author: "superstoned"
  - subject: "Re: SVG icons"
    date: 2007-03-26
    body: "Small (i.e. 16x16, 24x24 - even 48x48, sometimes) icons often need to be re-touched by hand, as automatic scaling cannot be relied upon to maintain the details, colours etc that the artists deem important."
    author: "Anon"
  - subject: "Re: SVG icons"
    date: 2007-03-26
    body: "For other toolkits or programs that want an icon. Note the freedesktop.org naming scheme for icons now being finalized in kde. "
    author: "Ryan"
  - subject: "Re: SVG icons"
    date: 2007-03-26
    body: "\"Note the freedesktop.org naming scheme for icons now being finalized in kde.\"\n\nOnly for KDElibs i fear... :("
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: SVG icons"
    date: 2007-03-26
    body: "> Only for KDElibs i fear... :(\n\nOnly for Oxygen - at the moment - I'd say.\nIIRC there are no Oxygen icons outside KDELibs."
    author: "Pino Toscano"
  - subject: "Re: SVG icons"
    date: 2007-03-26
    body: "Some was done (kopete, games), but not included yet (names...). And this is something i wanted to see with you ;)\n\n\n\n"
    author: "Johann Ollivier Lapeyre"
  - subject: "Strange folder icons..."
    date: 2007-03-26
    body: "Sorry, but the new folder icons looks awful IMHO. The shape is quite strange, they don't even appear like a folder anymore. I didn't like the previous version, but they was better than the new ones.\n\nWhy not to improve over Crystal icons or try something like Crystal Clear?\n\n"
    author: "kde.fan.from.brasil"
  - subject: "Re: Strange folder icons..."
    date: 2007-03-26
    body: "OR they're already improved, or dolphin doesn't have the new icons, but either way, it looks good in current svn ;-)"
    author: "superstoned"
  - subject: "Re: Strange folder icons..."
    date: 2007-03-26
    body: "Don't like them to much myself either. The old one's where better. Wonder how a Dolphin screen full of them look on a 800x600 screen. Also, what do they look like in a (big) treeview?"
    author: "cossidhon"
  - subject: "Re: Strange folder icons..."
    date: 2007-03-27
    body: "Allow me to disagree in this matter of taste. The new ones are nicer and more consistent."
    author: "Debian User"
  - subject: "so what about a vote?"
    date: 2007-03-27
    body: "mmh, as we see alot of re-iconification here, which seems to take place without much consent (anyone who likes to just commits a 'better' icon replacing the current one) why not save all icon submissions and bring them up for a vote on the Oxygen page? Also individual icon rating and comments would probably be handsome to give people the chance to criticizise certain aspects of the icons... what do you Oxygen developers think?\nThat said I still miss contrast around the edges of most of the icons, and I'm not yet satisfied with the folder icon, too. I still like the attached one best :======P"
    author: "eMPee"
  - subject: "Re: so what about a vote?"
    date: 2007-03-27
    body: "I think it has been a discussion before about this: put the icons on a wiki, and let people comment.\n\nI think the devs said that it was ok, as long as someone is willing to do it. I could help if I find some time. However, I'm not sure about the devs' approval; better to hear with them."
    author: "Lans"
  - subject: "Re: so what about a vote?"
    date: 2007-03-27
    body: "Why not on KDE-look?"
    author: "Cossidhon"
  - subject: "KWin effects & video playback"
    date: 2007-03-26
    body: "Do the KWin effects mess up the video playback\nlike they do on Beryl (and, I think, Compiz too)?"
    author: "Darkelve"
  - subject: "Re: KWin effects & video playback"
    date: 2007-03-26
    body: "fud, beryl does not mess up video playback"
    author: "Anounymous Bastard"
  - subject: "Re: KWin effects & video playback"
    date: 2007-03-26
    body: "It isn't FUD.  You do get messed up video playback with some drivers/driver versions (and Xgl versions if you're using Xgl) while running a GL compositor.  It isn't the fault of Beryl/Compiz/any other, but because of problems with the drivers/Xgl (I don't think AIGLX every had an problems directly because of it).\n\nFglrx is probably the worst offender here (the driver's quality is HORRIBLE!  I dare say the quality is non-existant!).  Fortunately I believe the OSS 'ati' driver is in the process of adding support for the newer ATI cards (like my X300, which is a piece of junk, I think my old Radeon 7500 was faster!).\n\nPlease stop throwing around the claim that something is 'FUD' whenever you don't like it.  All you're doing is making people not give a rats ass when groups actually DO spread FUD (don't forget the story of the boy who cried wolf, people seem to forget that way too often...)."
    author: "Sutoka"
  - subject: "Re: KWin effects & video playback"
    date: 2007-03-27
    body: "Wow, that was rather blunt.\n\nOn the forum I visit daily, there are a lot\nof reports of video playback getting weird\nafter installing some kind of 3D manager.\n\nI was asking because I would like to know whether\nKWin has/does not have - that problem.\n\nBut if it gets fixed for Beryl/Compiz/whatever ,\nwell, all the better."
    author: "Darkelve"
  - subject: "Re: KWin effects & video playback"
    date: 2007-03-27
    body: "\"But if it gets fixed for Beryl/Compiz/whatever ,\nwell, all the better\"\n\nOr the ATI drivers, since that seems to be the real problem."
    author: "Darkelve"
  - subject: "Re: KWin effects & video playback"
    date: 2007-04-26
    body: "Why not cube?"
    author: "Fabio Locati"
  - subject: "Re: KWin effects & video playback"
    date: 2007-06-16
    body: "oh anon - keep your ill-informed opinions to yourself if you don't have anything relevant to contribute. This is *NOT* FUD - I can confirm that I get black screens when trying to playback ANY video and Flash when beryl is in use. As soon as I swap my window manager back to metacity everything's A-OK. Sorry to add to the bluntness here but  I am sick and tired of people who have legitimate problems and who ask nicely getting their quesitons derailed by ppl like you. \n\nLikely a driver issue, but Beryl is most definately a catalyst for the issue recurring. "
    author: "jaemo"
  - subject: "Re: KWin effects & video playback"
    date: 2007-03-26
    body: "KWin would mess up video playback as much as Beryl and Compiz (they all use the same thing).  Most likely your problem is your video card's drivers aren't very good (the fglrx drivers are horrid (the Xv extension doesn't work when theres a GL window active, which obvious would cause problems with the GL compositors where the screen is composited with GL), can't test the 'ati' OSS driver because my video card is too new).  The 'nvidia' driver is really good (playing video works great), but the OSS 'nv' driver is still too primitive (not sure how the latest development effort is going though), I believe the OSS intel drivers are supposed to be pretty good at handling all that though (but I seriously doubt they perform as well as the 'nvidia' driver simply because the hardware is far better).  I don't believe anything any of the compositors do require anything more than a half-way decent amount of 3D acceleration so most likely once the various drivers mature (I'm looking at you fglrx!) they should have no problem handling xglx/aiglx/nvidia's aiglx + whatever compositor you want to run.\n\nIf you change the video output driver in the application you're using to something other than Xv (maybe opengl) it /should/ work then."
    author: "Sutoka"
  - subject: "thank you danny :)"
    date: 2007-03-26
    body: "So KBoard is not dead :)\nAwesome !\nIt's great to see some work on knewstuff2 too. I hope it will be widely used in kde 4.\nI also hope amarok will be able to use nepomuk, it would be an excellent showcase for this technology (and it would surely simplify the amarok code)."
    author: "shamaz"
  - subject: "There are times when you know KMail will crash"
    date: 2007-03-27
    body: "You select a mail in one of your IMAP folders, then the preview turns up blank. You try to select another one.. Crash...\n\nAnd then there are the times when it just crashes.. :)"
    author: "Anders"
  - subject: "Re: There are times when you know KMail will crash"
    date: 2007-03-27
    body: "I have an IMAP mail store that isn't even that large, about 70MB and kmail simply cannot cope with it or handle mail in a time-efficient manner.\n\nBesides the crashes, it is jut unbearably slow to move from one IMAP folder to the next one or to do a mail-check operation. \n\nI have been forced to use evolution, because it simply has much more mature IMAP support now (although it sucked too a while back). I don't know whether taking a look at evolution's IMAP implementation could be of help to kmail developers, but  it would make my life as a kde user to have a working imap client in kde. Kmail is find for pop, but I haven't use pop in years."
    author: "Gonzalo"
  - subject: "Re: There are times when you know KMail will crash"
    date: 2007-03-27
    body: "Mailody is doing IMAP quite nice. And it's \"IMAP-core-functionality\" is moving to a library (for KDE4). Maybe KMail will switch from using the KIO-slave to this library. Which surely would help, as the KIO-slave is no optimal way of communication in this case.\n"
    author: "Birdy"
  - subject: "Why KWin Composite?"
    date: 2007-03-27
    body: "Why rework those being well developed, if hardware acceleration is required for KWin comp. to run smoothly, compiz is OK after all. \nA more attractive theme just like the mock-up picture is more important. ;)"
    author: "rockmen1"
  - subject: "Re: Why KWin Composite?"
    date: 2007-03-27
    body: "The point in kwin_composite branch is to have a sane fallback mechanism for those folks that do not have hardware acceleration available, which still present all of the power and sanity you've come to expect from kwin.  For those that have the appropriate drivers/cards, you can now have effects of a similar style to beryl/etc. without sacrificing the other parts of kwin.\n\nOf course, there's a nice theme in the works as well by the artists :)"
    author: "Troy Unrau"
---
In <a href="http://commit-digest.org/issues/2007-03-25/">this week's KDE Commit-Digest</a>: Final stages of Freedesktop.org naming compliance performed on the <a href="http://oxygen-icons.org/">Oxygen</a> mimetype icons. IMAP-related crashes targeted in <a href="http://kontact.kde.org/kmail/">KMail</a>. Many KFilePlugins ported to use <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>. Better zoom and preview size interaction in <a href="http://www.digikam.org/">Digikam</a>. Optimisations in KSysGuard. Better integration with <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>-KDE interface elements in <a href="http://enzosworld.gmxhome.de/">Dolphin</a>, NEPOMUK-KDE components move to the kdereview module. Further <a href="http://kst.kde.org/">Kst</a> documentation work. Continued interface experiments and code progress towards <a href="http://amarok.kde.org/">Amarok</a> 2.0. "Flame" window destruction effect in the KWin Composite branch. KBackup is renamed "Galmuri" due to naming conflicts.
<!--break-->
