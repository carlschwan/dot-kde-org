---
title: "KDE Participating in Google Summer of Code 2007"
date:    2007-03-15
authors:
  - "sruiz"
slug:    kde-participating-google-summer-code-2007
comments:
  - subject: "Hurry up, if you want to participate !"
    date: 2007-03-15
    body: "Better late than never. I was wondering why there was not an article about this on the dot.\nThe student application deadline is in 9 days (March 24) :S\n\nI hope this GSoC will bring great things :)\n\nI'd like to add something that's not in this article : THANK YOU Mr GOOGLE ! :)\n\n"
    author: "shamaz"
  - subject: "Re: Hurry up, if you want to participate !"
    date: 2007-03-15
    body: "The list of groups that were accepted as mentoring organsations was only released last night. It could hardly be announced before we were accepted!"
    author: "Richard Moore"
  - subject: "Re: Hurry up, if you want to participate !"
    date: 2007-03-15
    body: "Yes I know this :)\nI just thought it would have been ok to say that KDE *might* be accepted for GSoC, and so students *might* begin to prepare something... Or just a message telling that KDE want to participate. Other organisations did this (for example : http://haiku-os.org/news/2007-03-03/google_summer_of_code_2007_drive) \n\nAnyway, I understand. I'm just worried by the short deadline :s\n\n"
    author: "shamaz"
  - subject: "Re: Hurry up, if you want to participate !"
    date: 2007-03-15
    body: "Well, I'd already heard from one student about my proposal before we got accepted, so at least some people had already found the page on the wiki. I think the limitations on the number of people involved means the deadline has to be fairly short."
    author: "Richard Moore"
  - subject: "Re: Hurry up, if you want to participate !"
    date: 2007-03-15
    body: "http://digg.com/programming/KDE_participating_in_Google_Summer_of_Code_2007"
    author: "elo"
  - subject: "Re: Hurry up, if you want to participate !"
    date: 2007-03-16
    body: "There was no article on the dot, but I have been sending out news to KDE mailing lists and IRC channels, including the main development mailing list (kde-devel). So, anyone who is already involved a bit with KDE has a great chance to have come across those announcements.\n\nYou can't expect that we announce every single little thing on the dot."
    author: "Thiago Macieira"
  - subject: "An idea for the Summer of Code"
    date: 2007-03-15
    body: "My humble suggestion is to have a project to make KDE and its whole environment a pleasure to look at especially in the font front by default.\n\nI find this Kdevelop screenshot very beautiful and always try to achieve this on KDE. Fonts are small, clear, sharp and crisp. I wonder whether such a screenshot is possible without MS fonts. If it is, then my request is to have steps involved to achieve this done away with. That's why I emphasize \"default\" in this submission.\n\nHave a look. http://www.kdevelop.org/graphics/screenshots/3.0/subclassing2.png"
    author: "Cb.."
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-15
    body: "Wow. I thought you're mad for making such a dumb proposal (sorry :o). Then I looked at the screenshot, and I have to agree with you.\n\nI've always been a little annoyed at the screen space KDEs widgets waste by using big fonts, divisor lines and margins. For me, this seems like a great step towards reducing this \"mess\".\n\nWhich font is this?"
    author: "ben"
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-15
    body: "The sans-serif font is Tahoma and the monospace font is Courier New (both of them MS fonts).\nIs really not that hard to setup your desktop to look like this: first install freetype library (http://www.freetype.org/) with bytecode interpreter enabled, then install the needed MS fonts and, finally, set them as your fonts on KDE fonts control module disabling antializing (you may also need to set the \"force fonts dpi\" to 96dpi).\nUnfortunately, I don't know of any free fonts with the same quality as Tahoma and Courier New, nor is it possible for most distros to have freetype's bytecode interpreter enabled by default because of a patent problem..."
    author: "Sergio Pistone"
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-18
    body: "Bytecode interpreter is not neccessary here (to be honest I got fonts totally screwed with enabled bytecode interpreter on my Gentoo), nor anti-aliasing is. It's just a matter of Tahoma 8 with Plastik Qt theme with 96dpi\n\nand it looks pretty pretty..."
    author: "Maciej Mrozowski"
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-15
    body: "Go to http://code.google.com/soc/student_step1.html, login and post your proposal."
    author: "Thiago Macieira"
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-15
    body: "Ya nuts? :D MS fonts look SOOO ugly compared to the DejaVu fonts family. And if you like 'em smaller, then go to the options and reduce the size... It's that simple."
    author: "zonk"
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-15
    body: "My Thoughts Exactly. Made my head hurt. Nice that we have proper fonts :)"
    author: "Petteri"
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-16
    body: "well, imho MS' fonts are pretty good (love verdana), but I prefer my fonts anti-aliassed. so I don't like the screenshot as well..."
    author: "superstoned"
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-18
    body: "I do like the MS fonts, but the fonts in that screenie are way too small.  I like leaning back away from my monitor and still being able to read it.  That would be a terrible default."
    author: "MamiyaOtaru"
  - subject: "Re: An idea for the Summer of Code"
    date: 2007-03-19
    body: "OMG, you actually like that? One of the things that first drew me towards using Linux as a desktop is the fact that it actually uses such nice looking fonts. Also, with Linux all GUI applications always allow for any size font (unlike Windows apps which half of the time break when you significantly change the font size). But I think this is the first I've heard of someone actually liking little jaggy fonts. Ohh well at least Linux gives us all a choice."
    author: "KRowe"
  - subject: "All KDE apps under the same banner?"
    date: 2007-03-15
    body: "Viewing the number of projects given out previous years it seems to me like it would be smarter not to have alot of projects under the KDE banner but have them seperately, looking at how many projects gaim got i highly doubt that would had gotten that many if they were a part of the gnome project.\n\nI heard that atleast amarok will try to differenciate itself from kde in this application."
    author: "Marius"
  - subject: "Re: All KDE apps under the same banner?"
    date: 2007-03-15
    body: "Amarok's application as a separate mentoring organisation has been turned down."
    author: "..."
  - subject: "Re: All KDE apps under the same banner?"
    date: 2007-03-15
    body: ":'(\nDid KOffice made a separate proposition too ?\n\nSeems like it worked for gnome : they got several separate mentoring organization (beagle, gaim, gnome, AbiSource).\n\ncompletely off-topic : apparently wine has been turned down too.\n"
    author: "shamaz"
  - subject: "Re: All KDE apps under the same banner?"
    date: 2007-03-15
    body: "Ok, I've just read the replies of Thiago and Boudewijn.\nForget about this comment :D"
    author: "shamaz"
  - subject: "Re: All KDE apps under the same banner?"
    date: 2007-03-15
    body: "That's Google's decision, not ours. In the end, my personal opinion is that we benefit more from grouping together: KDE was the largest organisation (along with ASF and PSF, IIRC) last year. And instead of a \"hard limit\" imposed by Google on how many applications to accept for each sub-project, we instead have a \"soft limit\" that we can allocate depending on mentor availability and on the quality of student proposals.\n\nAnyways, this question reappears every year. I am sure we will wonder the same thing next year."
    author: "Thiago Macieira"
  - subject: "Re: All KDE apps under the same banner?"
    date: 2007-03-15
    body: "We thought about going it alone with KOffice this year, and while we had a definite \"no\" after last year, conditions seems more favorable in the running up to gsoc2007 -- but then we sort of decided not to. In the first place, KOffice as a project doesn't really have the resources to run on our own. And in the second place, we had a really good gsoc2006 under kde's umbrella anyway. And the koffice irc channel is buzzing a wasp on steroids with people discussing projects anyway :-)."
    author: "Boudewijn Rempt"
  - subject: "How about a library for all of office?"
    date: 2007-03-16
    body: "It seems that the world could use one library that handles all import/export into MS office, that also include MS's Project Manager and Visio. Once that is developed, then it can be used by KDE, GNOME, OpenOffice, Coreal, etc. More importantly, 1 library means much easier time updating. Then when Office changes up thing, it will be MS's fault since they seem to break all other apps. "
    author: "a.c."
  - subject: "Re: How about a library for all of office?"
    date: 2007-03-16
    body: "You forgot one tiny detail...\n\nThat library would import from MS Office formats into what?\n\nEach application has its own internal tree and representation of the data. Import and export filters are directly tied to that representation.\n\nUnless you're suggesting a conversion to ODF formats.\n\nAnyways, make a proposal and we'll see if our mentors find that interesting. Make the same proposal to OOo, since they are participating too."
    author: "Thiago Macieira"
  - subject: "Also other projects"
    date: 2007-03-18
    body: "http://www.abisource.com/twiki/bin/view/Abiword/SummerOfCode2007\n\n--- idea of sonnet integration..."
    author: "Fiasma"
---
KDE is happy to be participating in the Google <a href="http://code.google.com/soc/">Summer of Code</a> program for the third consecutive year! As usual, we are looking for mentors and students to take us singing through the summer. Whether you have been part of the program in previous years or not, we need your help and fantastic ideas!




<!--break-->
<p><b>If you're a developer:</b>
Make sure that your project ideas are listed on the <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Ideas">ideas</a> page.</p>
<p>Consider applying as a mentor and guiding a student and help KDE budding programmers break into the development world.</p>
<p><b>If you're a student:</b>
Get started on your ideas! If you missed out last year, maybe you would like to revamp your idea for KDE4. Remember, the early bird gets the worm!</p>
<p>
Post your braindump to the ideas page, and apply as a student through the <a href="http://groups.google.com/group/google-summer-of-code-announce/web/guide-to-the-gsoc-web-app-for-student-applicants">Google interface</a></p>

<p>Anybody who has an idea, but either does not qualify for participation (eg, not a student), or does not want to participate should still post the idea on the KDE wiki page for other students to think about!</p>
<p>For more information, read on:
<ul>
<li><a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Participation">How to participate with KDE</a></li>
<li><a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Ideas">KDE SOC ideas page</a></li>
<li><a href="http://code.google.com/soc/">Google Summer of Code page</a></li>
</ul></p>


