---
title: "People Behind KDE: Inge Wallin"
date:    2007-02-15
authors:
  - "dallen"
slug:    people-behind-kde-inge-wallin
comments:
  - subject: "So..."
    date: 2007-02-15
    body: "...when will People Behind KDE interview Danny? :o)"
    author: "Anonymous"
  - subject: "Re: So..."
    date: 2007-02-15
    body: "Talking to oneself isn't generally considered a sane behaviour :-)"
    author: "Thiago Macieira"
  - subject: "Re: So..."
    date: 2007-02-15
    body: "lol ;)\n\nthat is the funniest sentence i read in the dot, of course after the Celeste Paul interview"
    author: "djouallah mimoune"
  - subject: "make Shodan in October this year ?"
    date: 2007-02-15
    body: " eh sorry what is Shodan ?"
    author: "djouallah mimoune"
  - subject: "Re: make Shodan in October this year ?"
    date: 2007-02-15
    body: "Means you get the black belt: http://en.wikipedia.org/wiki/Shodan\nPretty cool to hear, since it's a big step. :)\n\nShodan literally means \"first degree\", because by then you should have the \"complete base\". At that moment you've practiced for a minimum of for 6 years, likely longer. After that your Aikido keep evolving. You'll learn to implement the techniques in different forms (i.e. using lot's of space and avoid all friction, or applying pressure to a partner, etc..)"
    author: "Diederik van der Boor"
  - subject: "Re: make Shodan in October this year ?"
    date: 2007-02-15
    body: "And of course you forgot to mention that Shodan used to be a character in a DOS computer game called SystemShock.\n\nhttp://en.wikipedia.org/wiki/SHODAN\n\nPerhaps you noticed the domainname in my e-mail. Shodan is also a free Dutch hosting service but is going to be shut down anywhere soon when people don't request anymore accounts soon. Oh, and I did not name it Shodan myself, some student did :-)"
    author: "Niels van Mourik"
  - subject: "Re: make Shodan in October this year ?"
    date: 2007-02-15
    body: "I think you should look a bit closer at this picture of Inge:\n\n>  http://behindkde.org/people/ingwa/images/playing.jpg\n\n:-p\n\n\nOr off couse, we could request signatures for free ads :)\n\n--\nDiederik van der Boor\nSoftware developer and Open Source contributor\nWorking on KMess, a MSN Messenger client for KDE"
    author: "Diederik van der Boor"
  - subject: "Good job Inge!"
    date: 2007-02-15
    body: "I haven't been able to log into IRC for some time now, and one of the biggest bummers as a result is not being able to chat with Inge.  I got spoiled during the  KOffice 1.5 and 1.6 release times.\n\nAnd now I need my Inge fix.  Is this healthy?  At least more than interviewing yourself I suppose."
    author: "Wade Olson"
  - subject: "Re: Good job Inge!"
    date: 2007-02-16
    body: "Here you go.\n\nI'm not sure we should use the dot comments as our chat board, though."
    author: "Inge Wallin"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we travel north to Sweden to meet a developer and promotion guru who is charting a course toward success, someone who should not be crossed in a dojo - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/ingwa/">Inge Wallin</a>.
<!--break-->
