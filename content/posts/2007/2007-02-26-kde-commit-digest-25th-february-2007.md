---
title: "KDE Commit-Digest for 25th February 2007"
date:    2007-02-26
authors:
  - "dallen"
slug:    kde-commit-digest-25th-february-2007
comments:
  - subject: "Music"
    date: 2007-02-25
    body: "The music for the tv comercials is nice. I believe it can be used somewhere if not  for this purpose."
    author: "Music for tv comercials"
  - subject: "Re: Music"
    date: 2007-02-26
    body: "the \"tv comercial\" thing was like a goal for this music not a real target\nI hope we can get a tv comercial, but the musick is prety good to be used in all sorts of way in kde, there will be more versions of it so we can make full usage of the theme."
    author: "pinheiro"
  - subject: "TechBase"
    date: 2007-02-25
    body: ">> All hail techbase.kde.org\n\nWord. It is very nice-looking (although it would be even better if it filled the whole browser width. Hah, can't stop complaining about that one, sorry) and contains useful information. \n\nThank you for your hard work everyone!"
    author: "Lans"
  - subject: "Re: TechBase"
    date: 2007-02-25
    body: "Oh, come on.  We all know backstage.kde.org was the better name. :)\n\n* ducks and runs *"
    author: "mart"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "backstage++\nSuch is life..."
    author: "Cerulean"
  - subject: "Re: TechBase"
    date: 2007-02-25
    body: "Speaking of techbase... why? There was already wiki.kde.org \nWhat does techbase give that was not possible there? \nWill techbase become wiki.kde.org? \nWill wiki.koffice.org migrate back to wiki.kde.org? \n\nAlso it's awesome to hear that phonon is taking off. Way to go everyone!"
    author: "anonymous"
  - subject: "Re: TechBase"
    date: 2007-02-25
    body: "> What does techbase give that was not possible there?\nMediaWiki.\nAnd no, wiki.kde.org will not change from TikiWiki as the admin likes that better. In fact he develops it."
    author: "logixoul"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "Tikiwiki is the only reason I stay away from wiki.kde.org as much as I can. It's really a shame that one of the most popular open source project has a wiki that is, to be kind, a mess.\nWhy can't we have a decent one just because a self-proclaimed admin don't allow to explore other options? How about a poll here on the dot? [/rant]"
    author: "AC"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "+1"
    author: "MK"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "Oh dear god do I ever agree with you.  I can't count how many times I've double clicked on a word in that wiki to select it and that stupid thing thinks I want to edit the page, and then warns me with a popup that the page is being edited by anonymous.  Probably some other poor sod that accidentally clicked the page.  "
    author: "Leo S"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "So true!  I couldn't understand what happened the first time I did that. "
    author: "JohnFlux"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "You can disable that: Preferences \u00bb Use double-click to edit pages."
    author: "logixoul"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "+1. Not only I hate it as a user, but when I tried to install it as an admin, I got scared how messy it is. MediaWiki is a lot more comfortable, and better of all, has a decent design."
    author: "Anonymous Coward"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "well, it's not THAT bad, but I prefer mediawiki as well, and twiki (twiki.org) is very good as well - maybe even better than mediawiki."
    author: "superstoned"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "> Tikiwiki is the only reason I stay away from wiki.kde.org as much as I can.\nSame for me.\nSee also http://wiki.kde.org/tiki-view_forum_thread.php?comments_parentId=418&forumId=2 .\nI've yet to respond on that one...\n\n> It's really a shame that one of the most popular open source project has a wiki that is, to be kind, a mess.\nHey, it's only the user wiki, developers have a cool one... but yeah.\n\n> Why can't we have a decent one just because a self-proclaimed admin don't allow to explore other options?\n1. Are you offering your services as a committed admin of a MediaWikified wiki.kde.org? If no, you argument doesn't hold.\n2. He says migrating the user database and complete history to MediaWiki is entirely too timeconsuming and that he'd rather improve our TikiWiki installation than go through the producedure. He's open to suggestions in channel #kde-wiki but he doesn't seem to have much time.\n\n> How about a poll here on the dot?\nAgain, a poll is useless as there's no one to admin a KDE user MediaWiki."
    author: "logixoul"
  - subject: "not that difficult"
    date: 2007-02-26
    body: "> Again, a poll is useless as there's no one to admin a KDE user MediaWiki.\n\nstarting one should be easy:   http://www.wikia.com/wiki/Wikia\n\nif it grows I am sure we will find someone who will host it in the kde domain."
    author: "MK"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "> Speaking of techbase... why? There was already wiki.kde.org\n> What does techbase give that was not possible there? \n\nThe contents of developer.kde.org will be moved there. Using a Wiki makes it easier to maintain. KDE Techbase is KDE's answer to MSDN and Apple's Developer Connection sites."
    author: "Diederik van der Boor"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "> There was already wiki.kde.org\n\ntechbase is a(n eventual) replacement for developer.kde.org, kde.org/areas/sysadmin and other related web pages. techbase is a wiki, but it is not analogous to wiki.kde.org.\n\ntechbase is specifically, stricly and only for technical information of interest to developers (community and ISV), sysadmins and system integrators. it is there to drive kde development internally as well as provide resources external parties.\n\nquality, organization and coverage are all high priorities. wiki.kde.org is a bit more of a free-for-all when it comes to these sorts of things. while community involvement in techbase is encouraged and supported (that's why it's a wiki, for instance) the techbase team actually does maintain the content and structure."
    author: "Aaron J. Seigo"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "> although it would be even better if it filled the whole browser width.\n\nTrue."
    author: "Carlo"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "> although it would be even better if it filled the whole browser width.\n\nFalse.\n\nNo seriously. I agree on pages with a fixed height. This is line a stamp, really annoying. A maximum width on the other hand is actually good design. The paragraph length remains consistent, the number of words at one line can be optimized for best readability. \n\nEveryone who've used Latex probably knows about this. There are in fact scientific studies how large a page margin and line width should be. Open any academic book and you'll notice the large margin space, and number of words at one line. Sites with a good width are easier to read _if and only if_ the width is in \"em\" so it scales with the font size."
    author: "Diederik van der Boor"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "> The paragraph length remains consistent, the number of words at one \n> line can be optimized for best readability. \n\nUnfortunately, unlike TeX, this is impossible with HTML with different DPIs and such for each screen. Second, HTML is suppost to be dynamic and personal. I like more words on a line with slightly more linespacing. Which is impossible with that layout.\n\nFortunately, if you create an account, you can alter your default style on techbase to be the normal media wiki style.  Makes things readable for me again :)"
    author: "Thomas Zander"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "Perhaps, although I think most people think Latex has much too large a default margin. Anyway this page at least should have full width:\n\nhttp://websvn.kde.org/trunk/"
    author: "Tim"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: ">although I think most people think Latex has much too large a default margin.\n\nCount me on the 'most people'.\n"
    author: "renoX"
  - subject: "Re: TechBase"
    date: 2007-02-26
    body: "heh. \"no, really, my brain works differently than the rest of the species\" ;)\n\nas for websvn, i totally agree. that application needs a non-fixed design. text content (e.g. articles and tutorials) benefit from it, but not web apps like websvn or lxr. hopefully we get that addressed at some point in the near-ish future."
    author: "Aaron J. Seigo"
  - subject: "1900x1400"
    date: 2007-02-26
    body: "The site looks bad on high res 16x9 screens like all DELL Laptops"
    author: "funnyfanny"
  - subject: "Sweet"
    date: 2007-02-26
    body: "The oxygen icons are really coming along nicely, I love the new left icon Nuno Pinheiro made (http://websvn.kde.org/*checkout*/trunk/playground/artwork/Oxygen/Pinheiro/left.svg?revision=635719), I hope all the media player icons look like this.\n\nThe work in KGet looks awesome as well and I am really excited about finally having a network manager that works for every distro on KDE.\n\nOn a side note, is there any screenshots of the new KDE development snapshot? Just so I can get more of an idea about where KDE is headed."
    author: "Devon"
  - subject: "Re: Sweet"
    date: 2007-02-26
    body: "Yes and no.  Any screenshots will still show the old KDE 3.x styles, icons, themes, etc. and mostly the same applications.  Kicker, for instance, looks the same but only half works (it's going to die as plasma ramps up) and the desktop doesn't even get drawn at this point.\n\nSo not much to see visually, except that the libraries and applications are steadily improving. For example, khtml now has full (or nearly) CSS 3 support :)\n\nWhen plasma comes online, and the new windecos, widget styles and icons all go online, you will get to see the visual differences.  When kwin_composite comes online, we'll probably need videos though, as the animations are very difficult to capture in images :)"
    author: "Troy Unrau"
  - subject: "Re: Sweet"
    date: 2007-02-26
    body: "there are *some* and incomplete and unfinished etc etc screenshots in my kde 4 presentation which are available at http://www.kde.org/kdeslides/ which will be mentioned in the next daily fosdem report."
    author: "superstoned"
  - subject: "Re: Sweet"
    date: 2007-02-26
    body: "Maybe you know this already, or maybe I got it wrong, but I think that on page 21 of the presentation there's a small spelling mistake:\n\nit says \"SVG beauty en Usability\". I'm guessing that \"en\" has to become \"and\"?\n\nRegards,\n\n\nDarkelve"
    author: "Darkelve"
  - subject: "Re: Sweet"
    date: 2007-02-27
    body: "I translated it from dutch, guess I did miss some things ;-)\n\naaah well...."
    author: "superstoned"
  - subject: "it's cool BUT"
    date: 2007-02-26
    body: "...well, it crashed KPDF (including konqui) when leaving presentation mode, reproducably :===============O\nadditionally, the resolution and graphics are a bit screwed up!?\nStill, it sparks desires for world domination previously hidden deep under the surface.. RAIIIISE KDE RAISE UP!"
    author: "eMPe"
  - subject: "Re: it's cool BUT"
    date: 2007-02-27
    body: "Well, sorry about the crash ;-)\n\nThe PDF is created by OO.o, which seems to screw up resolution no matter what I try. The screenshots are indeed supposed to look a lot better ;-)"
    author: "superstoned"
  - subject: "Re: Sweet"
    date: 2007-02-26
    body: "At first, I disliked Oxygen icons *a lot*. It was too much plain and boring. But recently they where under some cosmetic changes and I'm really starting to think it will rock big time, and plus, will have a nice uniform looking for most of it's icons. \n\nSometimes it's good to aknowledge you are wrong :)"
    author: "Iuri Fiedoruk"
  - subject: "Small error in the summary"
    date: 2007-02-26
    body: "> \"with a move to the kdesvn module.\"\n\nI think kdesdk was meant."
    author: "Cerulean"
  - subject: "Re: Small error in the summary"
    date: 2007-02-26
    body: "Heh\n/me must remember to change the dot blurb when changing all other incorrect instances...\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "I'd really LOVE to have the underlined letter in every menu or button hidden by defalt and shown only when one presses the alt key, just like in Windows. It really gives a \"cleaner\" feeling.\nIs it possible to do it without having to patch Qt?"
    author: "Anonymous"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "I'd really LOVE to have the underlined letter in every menu\nor button not hidden by defalt, just like in Windows 3.1/95/NT.\nIt really gives a more pleasant experience of deciding which\nshortcut to press with only a glance.\n"
    author: "me"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "...and this is why the spirit of KDE is usually to allow these things to be customised whenever practicable. :)"
    author: "Jeff Parsons"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "Oh, great, another obscure configuration option to clutter the control center. :)"
    author: "bkudria"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "The more the merryer. Please add both options to KDE4\n\n"
    author: "Ben"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "I'd like the underline to appear when I press <ctrl><alt> 1 + f\nCan I have that configuration option too please?"
    author: "pete"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "Alt is needed anyway for activating the accelerators, so that key is not chosen randomly.\nRidiculing this part of the proposal is not very constructive IMO."
    author: "onety-three"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-03-01
    body: "True, key is not chosen randomly. But for me, working 'faster'\nis more important than 'having a _more_ clean look/feeling'.\n\nFirst, an additional underlined letter is not excessively\ndistracting.\n\nSecondly, I find it easier to perform:\n   look (eye) + alt (finger) + key (finger)\n\nbecause fingers are used together in a quick successive way,\nrather than:\n   alt (finger) + look (eye) + key (finger)\n\nwhere I need to mentally \"switch\" from finger to eye, and to\nfinger again.\n"
    author: "Re: Here's the most silly request for KDE4 :-)"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "(Score:5, Funny)"
    author: "Steve"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "Maybe we could introduce a configuration option to say if these configuration options are available? ;-)"
    author: "Andr\u00e9"
  - subject: "yeah, rite.."
    date: 2007-02-28
    body: "uber meta configuration setting options are da way to go, definite."
    author: "eMPee"
  - subject: "Re: Here's the most silly request for KDE4 :-)"
    date: 2007-02-26
    body: "I'd love that as well. Is there a feature request to vote for in the bugtracker? Couldn't find any."
    author: "onety-three"
  - subject: "At last Plasma"
    date: 2007-02-26
    body: "At last, great to see some news about plasma. Few months back there was a roadmap page in plasma's website. Where is it go now? At least it is not visible in the menu. Would like to see more coverage about plasma project. Anyway great going  KDE team =)"
    author: "Swaroop"
  - subject: "Re: At last Plasma"
    date: 2007-02-26
    body: "You and everyone else :)\n\nPlasma will get more coverage when things are ready to be covered.  Mostly libraries stuff happening anyway at this point...\n\nThe problem with plasma is that it's hard to make the full switchover from kicker to plasma before plasma starts to approach kicker's level of functionality.  And without that switchover in place, less people within KDE are exposed to the plasma code (as they don't get urges to implement things)... so once it hits a certain point and can realistically take over for kicker, then you'll see development accelerate quite rapidly.\n\n(also note that much of plasma's design is centred around making it easy to extend by third parties... so the good extensions would not necessarily be around for 4.0 yet, but will probably start to pop up after 4.0 and people have had a chance to do new things with it.  But coding this infrastructure is a lot of work and is taking up much of the plasma design time.)"
    author: "Troy Unrau"
  - subject: "KGet improvements"
    date: 2007-02-26
    body: "Congrats and thanks to the KGet developers. It's nice to see some screenshots and hear about the new features (mmm metalink, thanks guys). From the plans, this will be THE download manager.\n\nI can't wait to see KDE 4. The Oxygen icons look really nice, and all the feature/improvements sound great."
    author: "Ant Bryan"
  - subject: "performance"
    date: 2007-02-26
    body: "So do you think KDE 4 will turn out to be usable on my 550MHz box, like KDE 3.5 is? Will it be faster? or slower with all the fancy transparent SVG stuff...???\n\nOn a different note, the new KGet sounds awesome!"
    author: "personmandude"
  - subject: "Re: performance"
    date: 2007-02-26
    body: "Performance and speed is an ongoing topic and while I am not sure if the 4.0.0-release will be actualy faster, there stays a lot of time+potential to get it even significant faster then 3.x was/is during it's lifetime just like 3.x.x improved it's performance during the time.\nFrom someone who runs KDE 3.5.5 on his 500MHz+128MB RAM Compaq Armada and it works fine as long as I don't start OOo (but KOffice is there anyway to replace it ;)"
    author: "Sebastian Sauer"
  - subject: "Re: performance"
    date: 2007-02-26
    body: "And I thought I was pushing it with 800MHz+378MB ram. \n\nWhat distro do you guys use? I'm on debian testing."
    author: "Ben"
  - subject: "Re: performance"
    date: 2007-02-27
    body: "Slackware all the way brotha\nI'm using slackware 10.0 with a big mix of newer software, but I'm thinking of upgrading to 11.0 soon. It's compiled with GCC 3.4 as opposed to GCC 3.3, so it should be a wee bit faster....\n\nWell yeah, at the beginning of the KDE 4 journey there was a lot of hype about how performance was going to be improved, etc...\nI'm hoping that it actually wil :) :)"
    author: "personmandude"
  - subject: "Re: performance"
    date: 2007-02-27
    body: "Well, in some area's, performance will be improved. I guess memory usage can be lower if you disable certain things like strigi's indexing daemon. In any case, you'll have more features for your memory, as Qt4 binaries use approx 20% less memory compared to the same Qt3 executable. And there are more opportunities to use hardware acceleration in KDE/Qt4, so painting might be faster, even on slower hardware without 3D hardware (even 2D could be used better ;-))"
    author: "superstoned"
  - subject: "Codeine in KDE SVN? Cool!"
    date: 2007-02-26
    body: "I really like this video player : starts fast, simple and sensible UI. Just perfect as it is. With Kaffeine for DVB and Amarok for music, this media suite provides everything I need.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-26
    body: "Indeed, Codeine is awesome :)"
    author: "Mark Kretschmann"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-26
    body: "I agree, the more choice the better. Although I really only use KPlayer mostly due to its stability and its use of MPlayer as the backend, the more cool programs KDE has the better. Hope they all eventually move to KDE 4!"
    author: "Marcus Peterson"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-26
    body: "Glad you like it. :)\n\nIts been renamed to Video Player. I think its a simple name to go with a simple player. But its ungoogleable, so I'm open to other suggestions. ;)"
    author: "Ian Monroe"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-26
    body: "FWIW, Codeine was a name that I liked a lot.\nThanks for your great work :)"
    author: "logixoul"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-26
    body: "Codeine was nice, funny pun of Kaffeine?\n\nToo much drugs? What about Vitamine, then? ;)"
    author: "Slubs"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-26
    body: "I also liked the old name.  Just curious, why the change?  Worried about drug-related backlash?"
    author: "Louis"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-26
    body: "\"Video Player\" ain't no name!\n\nBut what's wrong with Codeine Video Player if you want to make it descriptive?"
    author: "Arend jr."
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-27
    body: "How about Adrenaline?  That has a feel of excitement that movies commonly invoke.  Of course, I don't know how difficult that is for non-English speakers.  I still like Codeine better."
    author: "Louis"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-27
    body: "...or you could call it KracK KoKaine  ;-)"
    author: "Louis"
  - subject: "Re: Codeine in KDE SVN? Cool!"
    date: 2007-02-27
    body: "what about Kinine?"
    author: "superstoned"
  - subject: "KDE Nepomuck"
    date: 2007-02-26
    body: "this will be so cool , anyone else read the pdf ?"
    author: "funnyfanny"
  - subject: "Re: KDE Nepomuck"
    date: 2007-02-27
    body: "Even saw the presentation, on FOSDEM ;-)\n\nYes, it's exciting and amazing stuff indeed... Really cool they choose KDE as testbed/first implementation!"
    author: "superstoned"
  - subject: "Re: KDE Nepomuck"
    date: 2007-02-27
    body: "I read it, the desktop search implictions are wonderfull. The shareing meta-data over the web seems to me like a waste of time. That said I don't think its a bad idea, I'm just not intrested in it.\n\nJust so long as its easy to tell KDE to not share metadata over the web I'm happy. Happier if thats the default, or a Kpersonaliser question, that defualts to no. Personal info should never be shared by you're computer without express permission.\n\nOne thing I didn't understand though is how is the metadata stored? is there some big database and you check files against it. or is the metadata stored inside every file. \n"
    author: "Ben"
  - subject: "Re: KDE Nepomuck"
    date: 2007-02-28
    body: "I think it's wonderful to have the option of sharing metadata over the internet. One of my biggest pet peeves with every operating system is that I can't sync information between computers. If I add something on a todo list at work, or add a contact to my address book, or any of a hundred other things I do all the time, I want that same thing to appear on all my computers. I've got two computers at home and one at work, and I don't want to put the same information in each one of them. It's time consuming and shouldn't have to be done. Kudos to the kde team for making this a reality. Of course, defaulting to 'no' also seems like a good idea security-wise. It's just upsetting to not be able to do it at all."
    author: "reldruh"
  - subject: "Re: KDE Nepomuck"
    date: 2007-02-28
    body: "Is a todo list, or address book, actually part of Nepomuck? I've read the specs but only have a shakey idea what their up to.\n\n\n\n"
    author: "Ben"
  - subject: "Question about NEPOMUK"
    date: 2007-02-27
    body: "I like the implications NEPOMUK has for desktop search. And I'm looking forward to that in KDE4. However I don't have any use for shareing meta-data across the internet, would it be possible to have KDE4 without that part of NEPOMUK installed? "
    author: "Ben"
  - subject: "Re: Question about NEPOMUK"
    date: 2007-02-27
    body: "Well, it'll probably be in the KDE libraries, but I'm pretty sure you can turn it off ;-)\n\nThey still have a lot to do, especially in the privacy area."
    author: "superstoned"
  - subject: "Re: Question about NEPOMUK"
    date: 2007-02-27
    body: "Yeah, this is KDE. I shouldn't worry, I'll be able to turn the middleware part off and just use desktop search."
    author: "Ben"
  - subject: "Re: Question about NEPOMUK"
    date: 2007-02-27
    body: "Does this have something to do with the Tenor thing?"
    author: "personmandude"
---
In <a href="http://commit-digest.org/issues/2007-02-25/">this week's KDE Commit-Digest</a>: <a href="http://solid.kde.org/">Solid</a> gets support for <a href="http://www.gnome.org/projects/NetworkManager/">NetworkManager</a>. Support for changing the font colour of the taskbar. File format import work in <a href="http://edu.kde.org/kvoctrain/">KVocTrain</a>. More KDE 4 porting takes place in <a href="http://ktorrent.org/">KTorrent</a>. <a href="http://noatun.kde.org/">Noatun</a> now uses <a href="http://phonon.kde.org/">Phonon</a> as its only backend. Work is begun on refactoring the user interface of <a href="http://amarok.kde.org/">Amarok</a> 2.0. The <a href="http://www.methylblue.com/codeine/">Codeine</a> video player is imported into KDE SVN and ported to <a href="http://www.cmake.org/">CMake</a>, Phonon and KDE 4. Progress in the 'krunner' element of <a href="http://plasma.kde.org/">Plasma</a>. KAlgebra is imported into KDE SVN into the playground/edu module. Search improvements in <a href="http://kate-editor.org/">Kate</a>, with a move to the kdesdk module.

<!--break-->
