---
title: "Art Tablets for Krita"
date:    2007-11-16
authors:
  - "brempt"
slug:    art-tablets-krita
comments:
  - subject: "Great!"
    date: 2007-11-16
    body: "Can't wait to replace Gimp with Krita."
    author: "Jasper"
  - subject: "Re: Great!"
    date: 2007-11-16
    body: "The only thing missing for me to switch is GIF support unfortunately. I hate those stupid GIFs but simply must edit them at work. I'm using Kubuntu and I heard that I have to recompile Krita and QT and what not to get this. Really too much hassle for me, so I stick to the GIMP for the time being which today doesnt really have such a bad interface as it had before, but those stupid multi-windows cluttering my desktop with other application below them, coming up as soon as you clicking a pixel too low when resizing an image window is driving me nuts. And we all know they have no incentive to change that, so I assume GIMP will have that in 30 years if it exists that long. So, I can't wait to replace GIMP as well."
    author: "Michael"
  - subject: "Re: Great!"
    date: 2007-11-16
    body: "that sounds like something you should complain to your distro about. afaik the gif patents have all expired by now, so I don't know of any good reason to exclude it."
    author: "Chani"
  - subject: "Re: Great!"
    date: 2007-11-16
    body: "Gif import/export support (well, the basics -- no animated gifs and Krita doesn't really do palette-based images) is in the graphicsmagick-based import/export filter plugin. Some distributions don't include that by default, other distributions (like Fedora) tend not to install any file filters at all."
    author: "Boudewijn Rempt"
  - subject: "Re: Great!"
    date: 2007-11-18
    body: "If distributions prevent you from using it, then it is time to abolish these distributions. I can understand all \"concerns\" but I stand by my harsh statement and I mean it in full.\n\nStop using distributions that act this way! (And to be honest, I think we as a community should be the central part, and we should stop focusing on distributions. Too often they claim to make life easier but they do not...)"
    author: "she"
  - subject: "Re: Great!"
    date: 2007-11-18
    body: "Feel free to do the research to find out what exactly is wrong; post a bugreport with the distro and if that fails then tell the KOffice developers so they can take such steps.\n\nBut, really, the bug should be identified first and we should try to fix it with the distro if at all possible."
    author: "Thomas Zander"
  - subject: "Re: Great!"
    date: 2007-11-19
    body: ">> And we all know they have no incentive to change that,\n>> so I assume GIMP will have that in 30 years if it exists that long.\n\nhttp://gimp-brainstorm.blogspot.com"
    author: "Blah"
  - subject: "Re: Great!"
    date: 2007-11-21
    body: "> coming up as soon as you clicking a pixel too low when resizing \n> an image window is driving me nuts\n\ni never have that problem as I don't resize windows that way :) try alt+rightclick, it's very handy!"
    author: "mxttie"
  - subject: "Re: Great!"
    date: 2007-11-16
    body: "I wouldn't go this far to say I'll replace gimp with krita. I'm pretty much used to gimp right now, especially with some nice plugins. But I can hardly wait for krita to use it for things gimp can't, like 16bit channels for now. The art brushes, especially with a tablet, are my single most anticipated feature for krita. I consider gimp a nice PS replacement, but the opensource community needs a \"Painter\" replacement as well, imho.\n\nOn you go krita. This is going to be great :-)"
    author: "soriac"
  - subject: "Re: Great!"
    date: 2007-11-18
    body: "Gimp is sadly not really a PS replacement. One main reason is that it is using a weird, \"pure\" MDI model.\n\nMy biggest personal concern is that script-fu is ugly (they should instead use a pure human-readable domain specific language, that is easier and more straight forward). And I would like more scripted support in gimp too, so that i can easily add my own actions that I manually do often (or record them)\n\nAgain MDI gets in the way here (but I must admit, scripting in photoshop is something i have neither ever heard of ...)"
    author: "she"
  - subject: "Re: Great!"
    date: 2007-11-18
    body: "and what is your opinion about Krita?  That *is* what this thread is about afterall.\n\nps. gimp uses pure SDI, not MDI."
    author: "Thomas Zander"
  - subject: "Re: Great!"
    date: 2007-11-19
    body: "GIMP has what I would describe as CSDI, Controlled Single Document\ninterface.  The toolbox acts as a control window but each document has a\nsignle window of its own.  (Photoshop on the Mac would also be CSDI but\nthe menubar acts at the control window.  I've seen other programs\nwhich have a seperate menubar as the control window, including\na version of Mathematica and various IDE.\n\nInkscape has what I would call straight SDI."
    author: "Max"
  - subject: "Re: Great!"
    date: 2007-11-19
    body: "> My biggest personal concern is that script-fu is ugly.\n\nYou can even write plugins for Gimp in Perl, Python, C, or C#. (C++ should work too.)"
    author: "Max"
  - subject: "Re: Great!"
    date: 2007-11-20
    body: "and Krita supports plugins written in Python, Ruby, JavaScript and C++ btw :)\nhttp://techbase.kde.org/Development/Tutorials/Krita_Scripting\n"
    author: "Sebastian Sauer"
  - subject: "Re: Great!"
    date: 2007-11-20
    body: "Yes, I know. But that wasn't the question."
    author: "Max"
  - subject: "Re: Great!"
    date: 2007-11-21
    body: "It is not the question what Krita supports. You should ask what Kross is supporting. I expect the number of scripting languages to grow (*gives PHP a hug*)."
    author: "Stefan"
  - subject: "sponsoring"
    date: 2007-11-16
    body: "if we didn't get a discount, then don't mention the manufacturers name in the news article."
    author: "anonymous"
  - subject: "Re: sponsoring"
    date: 2007-12-10
    body: "They don't. The manufacturer is Wacom. \"Intuos\" is the name of the product line... and besides, Wacom is the market leader because of the quality of their products and they're also a nice Linux-friendly company."
    author: "Stephan Sokolow"
  - subject: "Great news"
    date: 2007-11-16
    body: "This is wonderful news. After haven just spend about 1.000 Euros on Photoshop CS3 and Lightroom I really hope that Krita one day will be so powerful that Photoshop is no longer needed.\nI really like the Krita user interface. For me this is so much better than Gimp 2.4.\nAnd Krita runs on Linux too.\n\nTogether with Blue Marine (http://bluemarine.tidalwave.it) I think\nKrita can be a real competition for PS CS3 and Lightroom\n\n\nMarkus"
    author: "Markus"
  - subject: "Re: Great news"
    date: 2007-11-16
    body: "Thanks for the link to Blue Marine. I hadn't heart about it before. I'll test it now."
    author: "tobias"
  - subject: "Expensive tablet"
    date: 2007-11-16
    body: "If you or the University could not afford this, how can you expect any user to afford it?\nBuy a cheap Wacom instead."
    author: "reihal"
  - subject: "Re: Expensive tablet"
    date: 2007-11-16
    body: "The cheap Wacom tablets don't have the features these tablets have -- rotation, rate and tilt. There are lots of fun things that can only be done when you have those features. There are plenty of artists who have Intuos tablets, so we really want to support those features in Krita."
    author: "Boudewijn Rempt"
  - subject: "Re: Expensive tablet"
    date: 2007-11-16
    body: "Boud, I *so* admire you for being able to reply like that. You own."
    author: "mornfall"
  - subject: "Re: Expensive tablet"
    date: 2007-11-16
    body: "Because the tablets are expensive, it means that there shouldn't be support for them in Krita? What kind of logic is that? Why should authors sponsor the development also with money, is the free time they spend on it not enough? Maybe, following that line of argumentation, also buy you a tablet? Etc... The people who have donated have done so out of their free will, it is not like the authors would threaten you in any way. But that's because some people can appreciate good work, which doesn't seem to be among your properties. Still, you could refrain from posting such ill-mannered replies. Thank you."
    author: "mornfall"
  - subject: "Re: Expensive tablet"
    date: 2007-11-19
    body: "Ok, goodbye."
    author: "reihal"
  - subject: "Re: Expensive tablet"
    date: 2007-11-18
    body: "Wow, someone is jealous :o)"
    author: "whatever noticed"
  - subject: "Re: Expensive tablet"
    date: 2007-11-19
    body: "No, envious."
    author: "reihal"
  - subject: "Passive notebook tablets?"
    date: 2007-11-16
    body: "(Preface: What is described in the article is awesome work, and I'm just asking about a related topic on the side, not diminishing the cool stuff up above).\n\nMy wife and I are looking at the HP Tx1000z convertable tablets for more travel friendly laptops.  They have passive digitizers (poke with your finger or back of a pen).  I was wondering how well they work with Krita... I know that on the Palm, software has to take into account the fuzzy, inexact response of the passive digitizer or you can't draw a straight line.\n\nWe're not really intending to use them for art (we're pretty much just planning to use them to read her scientific papers and my eBooks in tablet mode), but it would be neat if we non-artists could use Krita for casual sketching on low end hardware.  Just curious how well it works, ye who have done the research into drawing in KDE."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Passive notebook tablets?"
    date: 2007-11-16
    body: "I'm not sure about the HP's -- but I've got a Thinkpad X61 tablet with a serial wacom digitizer, and that's great. It's got only 256 levels of pressure sensitivity, no tilt, no rate and no rotation, but it's a great thing to sketch with. And kubuntu feisty with Krita supported it out of the box. (Though Gutsy didn't...)"
    author: "Boudewijn Rempt"
  - subject: "Re: Passive notebook tablets?"
    date: 2007-11-17
    body: "\"And kubuntu feisty with Krita supported it out of the box. (Though Gutsy didn't...)\"\nTry to uncomment(deleting the \"#\" at the beginning of the line) the following lines in your /etc/X11/xorg.conf:\n#       InputDevice     \"stylus\"        \"SendCoreEvents\"\n#       InputDevice     \"cursor\"        \"SendCoreEvents\"\n#       InputDevice     \"eraser\"        \"SendCoreEvents\"\n"
    author: "."
  - subject: "Re: Passive notebook tablets?"
    date: 2007-11-19
    body: "\"And kubuntu feisty with Krita supported it out of the box. (Though Gutsy didn't...)\"\n\nYou can't use your tablet with XGL."
    author: "Max"
  - subject: "KDE e.V. rocks!"
    date: 2007-11-17
    body: "I knew I had forgotten something in my write-up... The KDE e.V. played the role of cashier here. The board kindly agreed to accept the money through paypal, and then helped us with paying for the tablets and getting them to their final locations. Thanks to Cornelius, Eva, Aaron and all the other members of the e.V. board for helping us out with logistics and organisation!"
    author: "Boudewijn Rempt"
  - subject: "Great?"
    date: 2007-11-18
    body: "It appears that I will have to wait a while longer to replace The GIMP *AND* XFig with something.  Whoever it was that tried to integrate XFig and The GIMP didn't seem to understand why people used both applications and the project was basically a failure.  What is needed is to do the drawing on the same \"canvas\".\n\nAnd, Krita seems to be going the wrong way to provide what I, and many other users, need.  There is nothing wrong with a computer simulation of ink and paint on paper and it appears that Krita will succeed at that and when all the bugs are fixed it will certainly be better at it than The GIMP.\n\nHowever, there are other computer users that need other things.  The computer has made it possible for people to write music and hear it performed even though they can't play a note on any instrument.  The computer could also make it possible for those that can't draw a single line to do artwork.  But, this is not going to be accomplished by using a tablet to simulate paint on paper, it is going to be accomplished by fine digital control like XFig offers.  Actually, you need to use both XFig and The GIMP to accomplish this with OSS and this is a bit awkward.\n\nSo, like many others who can't draw without a computer, I await an application that will do this -- an application which integrates vector and pixel drawing in a much more convenient way than having to use the two applications."
    author: "JRT"
  - subject: "Re: Great?"
    date: 2007-11-18
    body: "I guess you have not actually tried krita then, as it *has* a lot of vector capabilities in version 2 already using the Flake vector tools.  Agreed, its hard to use at this time as the flake integration in Krita is still quite broken but you can see how your way of working will actually be supported already."
    author: "Thomas Zander"
  - subject: "Re: Great?"
    date: 2007-11-18
    body: "May I ask the converse, have you use XFig?"
    author: "JRT"
  - subject: "Re: Great?"
    date: 2007-11-19
    body: "You can't compare XFig to Krita. Krita main target is being a painting app.\n\nWhich feature do you miss exactly? I doubt that the XFig UI or CAD-like capabilities will be added to Krita."
    author: "AC"
  - subject: "Re: Great?"
    date: 2007-11-20
    body: "As I said, the fine digital control of what is drawn."
    author: "JRT"
  - subject: "Re: Great?"
    date: 2007-11-18
    body: "I used a tablet and my small amount of art skills to draw a pretty decent flower with the current version of Krita, since I was tracing from a photo. Don't underestimate yourself. ;)"
    author: "Ian Monroe"
  - subject: "Re: Great?"
    date: 2007-11-19
    body: "1. Krita is not an replacement for Gimp. Krita is(will become) a drawing program and Gimp an image manipulating program. \n\n2. If you want to replace XFig you should take a look at Inkscape.\n\n3. If there are other computer users that need other things they should use an other program.\n\nIt doesn't make any sense to have a program that fits the need of all users, because that will end in a bad user interface. A clear product vision like Krita it has is very important for every product. Not only software programs. \nKrita already has some stuff that doesn't fit in its product vision. Perhaps they to change the UI to get it cleaner.\n"
    author: "Max"
  - subject: "Re: Great?"
    date: 2007-11-19
    body: ">2. If you want to replace XFig you should take a look at Inkscape.\n\nThat of course was a typo, I guess you mean Karbon, this beeing KOffice and all. And as pointed out in this tread, when the flake intergration in Krita gets up to par this gives the ability to use it inside Krita as well. \n"
    author: "Morty"
  - subject: "Re: Great?"
    date: 2007-11-20
    body: "Integration of Karbon and Krita is a possible solution.  I was never clear on why they were separate applications."
    author: "JRT"
  - subject: "Re: Great?"
    date: 2007-11-20
    body: "As I see it it's more like making a composite pictures, with bitmap and vector graphics layers, where you seemingless switch between applications when working on the different layers. Giving you the benefit of using the most optimal tool for the different tasks.\n\nBitmap and vector graphics are inherently different. The requirement, functionality and implementation of the tools are different, and more important  the way they are used and the workflow are also different. Thus having separate applications makes sense, and they can be specialized and optimized for the different tasks."
    author: "Morty"
  - subject: "Re: Great?"
    date: 2007-11-19
    body: "Krita is planning on being somewhat of a image editor/drawing hybrid, personally I believe its a lot better at image manipulating then Gimp already."
    author: "Ian Monroe"
  - subject: "Re: Great?"
    date: 2007-11-20
    body: "XFig is a drawing program, and it would be nice to see the drawing features in Krita.  The question is how you draw.  I perfer digital control of drawing while most programs only do freehand -- InkScape has limited digital control.  Using the combination of the two apps, you draw something in XFig, save it as EPS and then import the EPS into The GIMP which results in it being anti-alias rendered as a pixel image.  I would like to see this in either a single app or a toolbased integrated suite.  This really isn't a case of what you do, but rather how you do it."
    author: "JRT"
---
<a href="http://dot.kde.org/1180352311/">The Krita donation drive</a> has succeeded beyond the expectations of the Krita developers. Donations from all over the world made it possible to buy two Intuos graphics tablets and two art pens for the Krita developers to test their software with.  The Krita developers are very grateful to the community for making this possible. The Intuos tablets and art pens make it possible to develop brushes and tools that make use of advanced features such as tilt and rotation for Krita 2.0.


<!--break-->
<p>One tablet is currently with Boudewijn Rempt, who is working on a Chinese paintbrush simulation.</p>

<p><a href="http://static.kdenews.org/jr/krita-tablet-intuos.jpg"><img src="http://static.kdenews.org/jr/krita-tablet-intuos-wee.jpg" width="300" height="225" /></a></p>

<p>The other tablet is in Sardinia, with Emanuele Tamponi, who is working on the improved second-generation version of his <a href="http://commit-digest.org/issues/2007-08-12/">natural colour mixing model and painterly framework</a> with Roberto Baccoli, a Researcher at the University of Cagliari.</a>

<p><a href="http://static.kdenews.org/jr/krita-tablet-emanuele.jpg"><img src="http://static.kdenews.org/jr/krita-tablet-emanuele-wee.jpg" width="300" height="225" /></a></p>

<p>The art pens were not in stock at the shop and will arrive later, but now full tilt for Krita!</p>



