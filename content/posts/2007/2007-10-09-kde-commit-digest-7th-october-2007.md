---
title: "KDE Commit-Digest for 7th October 2007"
date:    2007-10-09
authors:
  - "dallen"
slug:    kde-commit-digest-7th-october-2007
comments:
  - subject: "Qt 4.4"
    date: 2007-10-09
    body: "It seems that Qt 4.4 will be much more memory efficient because of the \"invasion by aliens\" (http://labs.trolltech.com/blogs/2007/08/09/qt-invaded-by-aliens-the-end-of-all-flicker/). Besides it probably will be release before the KDE 4.0 final release.\nSo, does KDE4 developers intend to use it, since it would be nice to see KDE4 using less memory?\n\nThanx for the good work. It really rocks.\n\nBr\u00e1ulio"
    author: "Br\u00e1ulio Oliveira"
  - subject: "Re: Qt 4.4"
    date: 2007-10-09
    body: "> it probably will be release before the KDE 4.0 final release\n\nnot likely. KDE 4.0 is set for december and Qt 4.4 is set for Q1 of next year.\n\nthat doesn't mean that you couldn't compile it with qt 4.4 when it comes out and get those improvements for free."
    author: "Aaron J. Seigo"
  - subject: "Re: Qt 4.4"
    date: 2007-10-09
    body: "So this means that many main distributions, most of which just have had a release (suse, mandriva) or are going to do one (Ubuntu, Fedora) will probably use Qt 4.4..."
    author: "jospoortvliet"
  - subject: "Re: Qt 4.4"
    date: 2007-10-09
    body: "As far as Fedora is concerned, even if Qt 4.4 misses Fedora 9, it will almost certainly be pushed out as an update. It's backwards source and binary compatible, so why wouldn't we push the update?"
    author: "Kevin Kofler"
  - subject: "Re: Qt 4.4"
    date: 2007-10-09
    body: "PS: It will most likely end up in all releases supported at that time, even. For example, Qt 4.3 was pushed all the way down to Fedora 5."
    author: "Kevin Kofler"
  - subject: "KDE in LinuxFB?"
    date: 2007-10-09
    body: "Does anyone know? will you really be able to run KDE 4 in the Linux frame buffer without X11 running at all??  I saw a video on youtube to that effect.\n\n"
    author: "BATONAC"
  - subject: "Re: KDE in LinuxFB?"
    date: 2007-10-09
    body: "there was some early work on this; essentially kde4 compiles with Qt/E and apps do run but there are a number of remaining Qt/E-specific feature/bugs in kde that need continued work.\n"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE in LinuxFB?"
    date: 2007-10-09
    body: "Is this project alive? "
    author: "reihal"
  - subject: "Speed"
    date: 2007-10-09
    body: "I've only run KDE 4 off LiveCDs so far, and running off a disc is not a great way to gauge performance, but from what I've heard thus far, KDE 4 is a little slower and slightly less responsive than 3.5.7\n\nI know 3.5.7 is a very well polished release, and KDE 4 is a beta, but I thought QT 4 was going to bring speed and memory improvements to the table.\n\nAre the speed issues related to added features (such as Plasma), the lack of polish in such a new refactoring of the code, or perhaps these are \"debug\" builds, and not nearly as optimized as they can be?\n\nI'm curious how the KDE 4.0 release builds should compare to 3.5.7 (or 3.5.8 by that point) in terms of responsiveness?"
    author: "T. J. Brumfield"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "this is impossible to answer this without actual numbers, so as to know what is being measured, how it is measured and the actual differences. it's just too vague a question as stated.\n\npersonally, i find it faster in many regards though the immense amounts of debug output does slow some things down right now."
    author: "Aaron J. Seigo"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "I've heard nothing but bad things about the state of Qt4's resource consumption compared to that of Qt3 (at least from programmers not affiliated with Trolltech/ KDE), and this includes people writing release builds of software on Windows.  Resizing of windows/ re-layouting of widgets is one thing that seems to be particularly slower.  All KDE4 apps I've tried use more memory that their KDE3 counterparts (measured as RES minus SHR to counteract the fact that the code is swollen with debug code), *and* they seem to use more X-server resources too, leading to significantly higher net usage (presumably due to caching of pixmaps for double-buffering; I don't know).  \n\nThe \"KDE4 will use much less memory than KDE3!\" spiel was mostly given before KDE4 was even running properly, so I'd take it with a pinch of salt.\n\nThings like alpha-blending and other fancy effects are very fast, though :)"
    author: "Anon"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "Get latest skype or any other Qt4 app and judge by yourself, \ndon't believe to liars."
    author: "nae"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "... For the records, I am not affiliated with either Trolltech or KDE, and I have only good things to say about Qt4 (with perhaps the single exception of non-windowless child widgets, which is already fixed in trunk). Really, really badass fast and lightweight.\n\nWhich doesn't mean much about KDE 4.0 itself, however, seeing as lots of new (and as such not as well tested) technology has been created for it, I'll gladly grant you that.\n\nI'm not too worried, though. KDE 3 was already so crazy light you could run it fine on a computer two generations old, so even if the first few revisions of KDE 4 aren't as optimized yet, there's still a lot of leeway until it actually becomes bad. Time will tell."
    author: "Balinares"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "To me all Qt4 apps I use (mathematical programs, Skype, etc., non-KDE stuff) are as fast as other Qt3/KDE 3.x apps I have. So I can't follow your judgement.\n\nBut the best would be to post real benchmarks, maybe you've found a nasty bug which should better be fixed?\n\nIn any way, regarding speed improvements/problems of Qt4 there was already a highly interesting thread going on at the dot some years back:\nhttp://dot.kde.org/1135084395/1135109572/\nIt is worth a read and sheds some light why in some situations Qt4 can be \"slower\" than former Qt3 applications (it's X' fault, not Qt's fault :) )."
    author: "liquidat"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "And thats why we want KDE 4 running on Linux frame buffer."
    author: "reihal"
  - subject: "Re: Framebuffer"
    date: 2007-10-09
    body: "Qt/KDE running on the linux framebuffer sounds like no accelerated graphics like on the x server, no (x-server) nvidia/ati drivers, no games.. - is that correct or does it work another way? Care to explain?"
    author: "Anon"
  - subject: "Re: Framebuffer"
    date: 2007-10-10
    body: "I can't of course, just wishful thinking.\nDreams of small and fast, like eXPerience33's TinyXP.\nOr MicroXP with the Evil desktop.\nThats where the competition is, not with Vista or OSX.\n"
    author: "reihal"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "I've had a discussion about this with Zack (and others) and their point was: when you do exactly the same with Qt4 compared to Qt3, generally speaking you can expect it to be (much) faster.\n\nYet, nobody does the same with Qt4 - see Oxygen, which is much heavier yet compared to plastik. See all the animations in Qt4 (have you tried KDE 4 yet? it's chockful of subtle animations, like when drag'n'dropping dockers, selecting stuff, etc). Those will undoubtedly suck CPU, no matter how much more efficient Qt4 is. And another thing is of course the fact KDE has just been ported to Qt 4, apps don\\t use it very efficient yet. I'm pretty confident there still is a lot low hanging fruit in the optimization area. KDE 4.1 might really turn out to be a big thing: actually taking advantage of all new architectural stuff, inclusion of the things which wheren't ready for 4.0, AND optimizations and a better Qt ;-)\n\nSo, KDE 4, and esp 4.0, will use more resources than KDE 3. Yes, it sucks. But you GET more as well, and there are probably ways to lower the resource usage - choosing Plastik as theme, turning off effects (no idea if you can turn off the Qt effects, btw). "
    author: "jospoortvliet"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "Could one of the devs please say how easy it is to cut back on animations and other eye candy if you wish to go lightweight?"
    author: "Ben"
  - subject: "Re: Speed"
    date: 2007-10-11
    body: "> yet? it's chockful of subtle animations, like when drag'n'dropping dockers, selecting stuff, etc).\n\nUhgh - hopefully it's possible to disable this crap in KDE 4.0. There isn't a lot I do hate more than superfluous animations for the sake of animation (buy a game console for this purpose, please!), eating cpu cycles, causing the cpu frequency to raise unnecessarily."
    author: "Carlo"
  - subject: "Re: Speed"
    date: 2007-10-12
    body: "Well the animations aren't there just for the sake of animations.  Animations are very helpful to better illustrate changes in the user interface, and guide the user between transitions.  Read up on visual momentum, it's very interesting.  http://www.google.de/search?q=visual+momentum&ie=UTF-8&oe=UTF-8\n\nThat said there is definitely a need for an \"off\" switch as you said."
    author: "Leo S"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "Ok, same \"Anon\" here, and the comments sound very promising to me, so I have high hopes for the future :)\n\nCan someone from TT weigh in on how the double-buffering works, though? I note that I can drag a window over another window in Qt4 and, as opposed to Qt3, I no longer get the terrible \"sheering\" and \"trails\" left behind.  I'm not sure how this could be accomplished short of caching the entirety of each window so that, as another window moves over it, the bits just \"re-exposed\" can be *instantly* \"redrawn\" (I'm ignoring X hardware compositing as I don't think that's the mechanism used, as the same smooth effect can be seen in a Xephyr session), but surely having every single window cached in this way would gobble up a large amount of X-server memory, increasing the total memory used by running Qt4 apps, even when the apps themselves take less memory? Especially when a window is maximised on a high-resolution screen.  Or is there some other TT-magic employed here? :)"
    author: "Anon"
  - subject: "Re: Speed"
    date: 2007-10-15
    body: "A full screen 1280x1024 window is 3.75MB (5.00MB if you do 32-bit alignment / use an alpha channel). That isn't really that much in this day and age.\n\nCompix/AIGLX basically do the same thing for every window, except I believe they use video RAM instead. Windows draw to off-screen buffers in video RAM, and those buffers get composited to the screen every frame via the 3D pipeline (which isn't really taxing at all for today's video cards)."
    author: "Hector Martin"
  - subject: "Re: Speed"
    date: 2007-10-15
    body: "\"A full screen 1280x1024 window is 3.75MB (5.00MB if you do 32-bit alignment / use an alpha channel). That isn't really that much in this day and age.\"\n\nBut it would be more than in Qt3 which (presumably) doesn't use this method.  Hence my concern about whether Qt4 really is more lightweight than Qt3 - kwrite would end up gobbling up *twice as much RAM* using Qt4 than Qt3!"
    author: "Anon"
  - subject: "Re: Speed"
    date: 2007-10-09
    body: "Well I am a Qt4 developer working on Windows, not affiliated with KDE or Trolltech, and I have no complaints about the performance.   At the moment, it does seem a little slow on Linux compared to Qt3 though.  Hard to judge though, since I always run debug builds of my stuff."
    author: "Leo S"
  - subject: "Raptor and Screenshots"
    date: 2007-10-09
    body: "The Raptor link is pointing to a wallpaper currently.\n\nIs there anyplace I can see what Raptor is going to look like, or how it might function?\n\nAlso, in varying screenshots I've seen thus far, it seems the Plasma widgets have this beautiful, simple glass border/frame.  Yet the normal window decorations in KWin have been fairly typical/traditional.\n\nI'm curious if there was any consideration for the Oxygen theme to have window decorations closely related to those beautiful borders on the Plasma widgets.  Wouldn't it make the entire desktop more consistent (not to mention breathtaking)?"
    author: "T. J. Brumfield"
  - subject: "Re: Raptor and Screenshots"
    date: 2007-10-09
    body: "http://pinheiro-kde.blogspot.com/2007/10/raptor-join-fun.html\n\nSome mockups were posted, I do hope those mockups come to life. Looks minimal yet provides everything you need."
    author: "Skeithy"
  - subject: "Re: Raptor and Screenshots"
    date: 2007-10-09
    body: "I love it.  Looks great!"
    author: "T. J. Brumfield"
  - subject: "Kickoff"
    date: 2007-10-09
    body: "Man, I am just full of comments and questions today.  I apologize.\n\nI don't want to start a flame-fest on Kickoff, as we have had in the past.  As far as I'm concerned, I'm a proponent of choice.  I enjoy that today we can use KMenu, KBFX or Kickoff and I assume we will have options in the future.\n\nI know Kickoff is supposed to be well-researched, and the description in the digest keeps mentioning how it is supposedly designed to allow users to navigate quicker.\n\nPersonally I find Kickoff to be visually pleasing, but I find it slows me down considerably.  The \"favorites\" pane (akin to pinned entries in the Windows menu) might provide quick access to a small number of apps, but if there are 4 apps or so you use that frequent, I usually pin an icon to a panel, or use a dock with some shortcuts.  People might also just put icons on their desktop.\n\nNavigating to Kickoff, and then your favorites seems like an extra step to me.  And if I want to navigate the full menu, I click through layers of the menu, which transitions a little slow for my taste.\n\nThe traditional KMenu isn't pretty, but it is quite fast.\n\nI'm hoping that we might be able to configure/alter the speed of Kickoff.  Perhaps there exists a method today that I'm not aware of?"
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "> The traditional KMenu isn't pretty, but it is quite fast.\n\nfor those of us who have gotten used to it and are good with such lists, it can be fast for certain types of functions. however, it's hardly optimal for most people and many tasks. this is what makes things like kickoff a net win.\n\nthat said, *what* exactly do you find slower (exact use cases, so i'm not thinking you're saying A but you're really saying B) and can you measure the speed differences so we have some numbers?\n\nthis is also pretty much completely the wrong place to discuss these things; panel-devel@kde.org would probably be a lot more sane."
    author: "Aaron J. Seigo"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "Let's say I want to play a game.\n\nWhen using Kickoff (which I switched away from and haven't used in a while) I believe, it opened with a favorites tab, and I had to switch to my programs tab, which involves a click, and a pause.\n\nThen I click and pause on the Games menu.\n\nThen I click and pause on a subcategory, like Card Games.\n\nThen I click on the program.\n\nPersonally, I don't think I'm the target user for the app.  Most of the time, I just hit Alt+F2 and type the app name.  Kickoff was the app that frankly drove me away from using menus in general.  However, I'm excited to try out Raptor in the future."
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "What I'd *really* like to see is something like the traditional Kmenu but editable in place, like, hold a meta key and drag and drop entries with the LMB and RMB opens a small dialog to edit, copy, new and remove an entry etc.\n\nThe main problem with the traditonal heirarchical menu is the godawful default layouts offered by most distros. Once the menus are customised and streamlined for the particular end user they are very convenient and fast to use. So some equally easy and fast method of modifying Kmenu options makes the most sense to me... and some way for newly installed packages to respect the USERS grouping, ie; I use separate Audio and Video menu folders but new A/V apps insist on being dumped in some stupid Multimedia folder. I guess that's a matter for XDG standards but the easier it is for this end user to customize these things the happier he'll be."
    author: "Mark Constable"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "AMEN!"
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "I second this."
    author: "Dado"
  - subject: "Re: Kickoff"
    date: 2007-10-10
    body: "Like in Windows? In the start menu in Windows (at least the XP I have to use frequently because of my printer) you can drag items around. However, it is designed awfully, with dialog boxes when you move items which are shown to all users.\n\nThat said, I am not using a KMenu or Kickoff any more. I have removed the button from my panel. Therefore, only Alt-F2 or F12 for Yakuake can be used to invoke commands or start programs. That is the perfect protection against Windows users. ;-)"
    author: "Stefan"
  - subject: "Re: Kickoff"
    date: 2007-10-13
    body: "> Like in Windows?\n\nReally! I use XP occasionally and had no idea one could move the Start menu items around from within the Start menu itself. I'm impressed and now interested to reboot to see how good, or badly, it's implemented.\n\n(To anyone) If in my wildest dreams I could learn how to hack on KDE4, where would I best start to try and implement (or reimplement from scratch) a self-editing Kmenu? Is there a Qt4 list/view widget that would be a good example to start from (gentle, total novice here)?"
    author: "Mark Constable"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "Question: Why not simply use the search feature in kickoff? You can type in the name of the game or the executable. Then press Enter. It's a built-in Run Command + Search..."
    author: "Jucato"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "If I don't know the name of the executable, then I need to search for it.  But quickly I learn the names of all the programs I use."
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "Typing the name of the application is enough, you don't have to necessarily know the name of the executable."
    author: "Erunno"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "\"Typing the name of the application\" is impossible, if you do not know it either.\n\nHowever, you could just type \"game\", and all applications which are somehow associated with \"game\" will show up."
    author: "Kurt Pfeifle"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "do you realy think this is faster than just clicking on \"games\" for most users!?"
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "In the google-age, yeah, I definitely think that writing \"games\" is quickier than visually scan a menu with 20+ submenus."
    author: "Vide"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "Except for people with disabilities like missing fingers, and people like me, who hate to type when I don't have to.  I shouldn't have to type when opening programs.  I customize the kmenu with categories where all my programs are only one layer deep.  I'll put my program-opening speed up against anybody that types program names into kickoff.  Unless they have three hands..."
    author: "Louis"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "Ha.  With a good system (unfortunately I haven't found one yet on Linux..  Launchy is the best for Windows), I can launch an application before you can even display the start menu.  Alt-Space-F-Enter.  Takes a fraction of a second and launches Firefox, or Alt-Space-Sp-Enter launching Speedcrunch.  There is no way you can be faster than that kind of interaction with a mouse based approach.\n\nAlso, the disability card is a misnomer.  If you look at alternative input devices, most of them emulate a keyboard at some point, and providing efficient keyboard based navigation is crucial for people with many types of disabilities. "
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: ">> unfortunately I haven't found one yet on Linux\n\nAlt+F2 in KDE or maybe Katapult?\n\n>> I can launch an application before you can even display the start menu.\n\nWhen I know the name of the applications, I often use Katapult/Alt+F2 to start it. But sometimes I just want to see what I've installed, and maybe find a new game; in these cases, I prefer the old KMenu to Kickoff's navigation system. (Note: I haven't used the latter one very much, thos could be because I'm simply used to the old menu system.)"
    author: "Hans"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "I'll take on either!  Katapult is cool, but I find that the search finds too many similarly-spelled commands.  Especially in KDE where the habit of naming everything with a \"K\" hasn't been completely shaken yet."
    author: "Louis"
  - subject: "Re: Kickoff"
    date: 2007-10-10
    body: "True. Opening \"Konqueror\", for example, was a pain in the \"old\" Katapult. \n\nRyan Bitanga has started a Katapult fork named \"Katapult Fast Track\", you can find it here: http://kde-apps.org/content/show.php/Katapult-Fast+Track?content=60941\n\nThere are some great new features, such as\n* Support for multiple results (Press up or down keys to see more results)\n* Adaptive search\n\nHowever, there are some issues; random crashes etc. Also, be sure to delete your old katapultrc after upgrade."
    author: "Hans"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: "Cool, thanks for the link.  Hope they stabilize it.  This might be what I'm looking for.  Of course, if krunner turns out as I initially proposed it, we won't need katapult anymore."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: ">> Alt+F2 in KDE or maybe Katapult?\n\nSorry,  neither of those are nearly as good as launchy.  The run dialog doesn't know about application names, and katapult doesn't learn, and doesn't provide alternate options, and can't run apps not in the k menu easily.\n\nI'm not against a regular menu, but for the rare time when I want to browse my apps, kickoff is just fine.  I don't see myself being much slower with it.  It avoids the annoyance of nested popup menus, which are a pain with innacurate trackpads."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "Maybe not faster in a one-application contest.  But now lets open a bunch of randomly-selected applications in succession.  That means you have to memorize a bunch of multi-key combinations (more than 60 on my system).  Yuck.  If you do have all those memorized, I humbly bow to your superiority :-)"
    author: "Louis"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: "Nah, no need to remeber anything not needed, just ALT+F2 and type the executable name -> enter and you're done. Or maybe it's just me that finds easier to remember exec's name? Whatever :)\nAnd yes, it's definitely quickier than using mouse + menu."
    author: "anon"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: "No problem.  Here's my thought process:  \n\nDesire: I want to start Openoffice Writer\n\nWith a good application launcher:\nAlt+Space+w+enter (or maybe wri if the system hasn't learned yet)\n\nThere is a direct map from my thought (I want Writer) to my actions (start to type writer)\n\nWith a menu.  Move mouse to menu icon, click, scan categories to find the category that Writer is probably in (Office perhaps?), click the entry, scan entries in the submenu for my application, click the app.  This is very indirect (moving a pointer on a 2d plane to target a small rectangle) compared to typing what you want."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: "thats funny. what do you think your doing while typing? hitting the right small \"rectangles\" in the right order, perhaps? if you think using the keyboard is aktually easier then the mouse than i don't know what to say... why are you using a gui at all!?\n\nits simple: most computer users, at least here in germany, don't type if they don't need to. because of that, they actually arn't able to use a keyboard productive. thats a direct consequence of propagating the use of a _g_ui.\n\nmost users here in germany use a menu or even more the desktop to start applications. they use the favorites to get on google.de and they reach for \"Bearbeiten/R\u00fcckg\u00e4ngig\" instead of hitting ctrl+z. oh, and they use windows, but thats a bit offtopic  ;).\n\n\n\nso, providing all sorts of shortcuts for us keyboard users is all nice, but don't leave the normal gui user out in the cold!"
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: ">  hitting the right small \"rectangles\" in the right order, perhaps?\n\nYes, but that requires no conscious thought.  Unless you are a first time typist, you don't tend to think about \"hitting small rectangles\", your fingers know where the letters are due to muscle memory..  With varying pointer acceleration and mouse sensitivity, it is almost impossible to achieve the same with a mouse.  Try to launch a common app blindfolded with the mouse, versus typing that apps name, also blindfolded.\n\n>> why are you using a gui at all!?\n\nNothing about a GUI ties it to a mouse.  GUI's have many benefits over the CLI, completely independent of the input device.\n\n>> most users here in germany use a menu or even more the desktop to start applications. they use the favorites to get on google.de and they reach for \"Bearbeiten/R\u00fcckg\u00e4ngig\" instead of hitting ctrl+z. oh, and they use windows, but thats a bit offtopic ;).\n\nYes, they have been trained this way, because keyboard shortcuts have been traditionally hard to memorize.  Nothing about Ctrl-Z suggests Undo, so no one bothers to remember it (aside from us geeks).  Context sensitive search is very different, in that you never have to remember anything, you just start typing what you want.  Of course, it doesn't replace the menus, it merely is a more efficient way of accessing items in them.\n\n\n\n"
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-12
    body: "using the mouse doesn't _need_ any conscious processing, too. ever saw a lan party?\n\nusing the mouse properly needs just as much training as using the keyboard. you only need different training.\n\nbeing able to do stuff blindfolded doesn't show anything about the difficulty of the task.\nthe human brain is able to handle all kinds of \"difficult\" tasks without us thinking about it at all. and one of the most powerfull areas is the one responsible for our vision. gamers (just like helicopter pilots, or car drivers) show that our brain is indeed able to handle a mouse without any kind of conscious thought. you don't have to think \"mouse 100px up\", just like you don't have to position your fingers \"yourself\" while typing.\n\nwhat about trying to type your whole next post blindfolded? just to see how much easier it is to handle a keyboard, really :P."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-12
    body: "LOL, lan party you say, so you're clearly talking about some quake deathmatch... well, this has _nothing_ to do with launching apps on a desktop. Nothing."
    author: "Vide"
  - subject: "Re: Kickoff"
    date: 2007-10-15
    body: "sure it has. just like shooting something in quake doesn't requires any conscious thought, clicking on the desktop doesn't need any either.\n\nhow much conscious thought is required depends only on your level of training. someone new to the keyboard will take ages, and many consciouse toughts, scanning the all keys again and again, to type even simple words."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-12
    body: "> most users here in germany use a menu or even more the desktop to start applications\n\ndo not say it that loud or you will get beaten by some usability study that your \"armchair perspective\" is completely wrong ... although from my experience, I would agree that most of the \"lame\" users that I know prefer icons on desktop ;-)"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: "\"With a menu. Move mouse to menu icon, click, scan categories to find the category that Writer is probably in (Office perhaps?), click the entry, scan entries in the submenu for my application, click the app.\"\n\nYou assume here that I've never used my menu before.  As I said already, I have organized my menu very tightly, and know _exactly_ where everything is, and can hit most of them with muscle-memory.  To be fair, if you're going to assume that the keyboard user has memorized all the executable names, then you also have to assume that the mouse user knows his menu intimately.\n\nEither way, I'm glad that KDE lets us have both."
    author: "Louis"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: ">> and can hit most of them with muscle-memory.\n\nTry it blindfolded then.  It is just about impossible.  If you can't do it blindfolded, then it isn't muscle memory.\n\n>> To be fair, if you're going to assume that the keyboard user has memorized all the executable names\n\nNo one needs to memorize anything.  You just need to know what you want to start.  I assume you know in your mind that you want to start \"Kopete\" before you actually perform the actions necessary to start this application.  At the very least you will know something like \"messenger\".  With a good text based launcher (Alt-F2 is a run dialog, not a launcher, it is not the same), you can start to type any of the words you already know.  There is no need to ever memorize any keyboard commands."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: "You missed my point.  Point was that I know my menu; I don't have to browse it as you suggested.  I can't think of any use case where I would be computing blindfolded, so I don't open programs that way either.  I can get to any program on my menu, however, using peripheral vision, without taking my eyes off the app I'm currently using.\n\nFurther, some of the programs in my menu I pass arguments to so they open just how I like.  That seems a bit more difficult to do with a launcher, unless you start creating symlinks and stuff."
    author: "Louis"
  - subject: "Re: Kickoff"
    date: 2007-10-12
    body: ">> You missed my point. Point was that I know my menu; I don't have to browse it as you suggested.\n\nNo, I understand your point.  But no matter how well you know your menu, you still need the visual feedback system to start apps.  Now you may be very fast at finding your apps, but there is still some feedback necessary there, and a lot of memory (you are basically memorizing the exact position of all the apps you use).  If apps are added or removed from the menu, and the position of your apps changes, you revert back to scanning.  This never happens with keyboard based launching.  New apps do not influence the launching of current ones, and there is nothing to memorize.\n\n>>  I can't think of any use case where I would be computing blindfolded, so I don't open programs that way either.\n\nOf course not.  I said that to illustrate that the task of selecting application entries with a mouse in a traditional hierarchical menu requires visual feedback and is not pure muscle memory as you suggested.  \n\n>> I can get to any program on my menu, however, using peripheral vision, without taking my eyes off the app I'm currently using.\n\nHeh, if you had an eye tracker you would see that is not the case.  Your eyes will have saccadic eye movements towards your menu as you bring it up, nothing you can do about it.  This is also a funny use case.  Why are you so intent on not looking at your menu?  Since you are starting a new app, you are breaking your focal context anyway, so there is no point in trying to maintain it.\n\n>> Further, some of the programs in my menu I pass arguments to so they open just how I like. That seems a bit more difficult to do with a launcher, unless you start creating symlinks and stuff.\n\nThe launcher has absolutely no effect on something like arguments.  That's defined in the .desktop files, which will be used by any menu or launcher in the same way.  How you find and select those .desktop files is irrelevant."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-12
    body: ">>no matter how well you know your menu, you still need the visual feedback system to start apps...\n\nI need visual feedback when using Katapult, also.  All the stuff that starts with K requires me to keep typing until if finally get the right app.  K-o-n might be Kontact, Konqueror, or Konsole.\n\n>>This never happens with keyboard based launching. New apps do not influence the launching of current ones\n\nNo?  If I installed a program call Konquest, I would have to type even more to get Konqueror.\n\n>>Of course not. I said that to illustrate that the task of selecting application entries with a mouse in a traditional hierarchical menu requires visual feedback and is not pure muscle memory as you suggested.\n\nJust to satisfy my own curiosity, I tried the blindfolded thing.  Out of the top 20 programs that I use, I was able to hit 14 of them!  I surpassed my own expectations.  I should point out that I use a mousepad with a very slight \"grid\" indention and my thumb can \"feel\" distances.  It also helps that the Kmenu is infinite with regards to the screen corner.  I didn't practice prior to testing myself; I just have a good sense of where my stuff is.  Sweet!\n\n>>you are breaking your focal context anyway, so there is no point in trying to maintain it.\n\nI actually do have use cases here.  I frequently start Amarok while surfing.  It starts minimized and begins playing without breaking my focus.  I also frequently start Ktorrent to resume a download after I had turned it off for a while.  It, too, starts minimized.\n\n>>The launcher has absolutely no effect on something like arguments.\n\nThe launcher can't distinguish between the different ways that I like to start some apps.  Here an example:  I sometimes start Frozen Bubble in colorblind mode and sometimes in regular mode, depending on if it's me or my wife that's playing.  Katapult doesn't distinguish, but my menu entries do.\n\nAnd lastly - I can open my programs with my right hand while chugging a cold beer in my left!  How's that for productivity!!! :-)\n\nAll that said, I still use both Alt+F2 and Katapult on occasion, and find them to be great tools."
    author: "Louis"
  - subject: "Re: Kickoff"
    date: 2007-10-12
    body: ">> No? If I installed a program call Konquest, I would have to type even more to get Konqueror.\n\nYes, this is because Katapult is unfortunately not a good text-based launcher.  It doesn't learn.  With something like Launchy, k would select konqueror if that's the app (starting with k) that you used most often.  KDE does make this harder because of all the K names.  \n\n>> I just have a good sense of where my stuff is. Sweet!\n\nHehe.  Well that's cool. I didn't think it could be done, but I guess there are exceptions.\n\n>> Katapult doesn't distinguish, but my menu entries do.\n\nIf you have two menu entries for the different modes of Frozen bubble (with different names I suppose, else how could you distinguish them in the menu?), then a launcher will pick them up as separate as well.\n\nGood point on the one handed casual use though, I do that as well.  Of course, I have completely removed the kmenu icon from my desktop, so starting apps with the mouse is a bit difficult now :)"
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-12
    body: ">>I have completely removed the kmenu icon from my desktop...\n\nAwesome.  You have made your computer idiot-proof in a new sense of the phrase :-)  You're the man!\n\nBTW, thanks for the lively and diplomatic discussion.  It's been fun."
    author: "Louis"
  - subject: "Re: Kickoff"
    date: 2007-10-10
    body: "Katapult works in a similar way: http://katapult.kde.org/screenshots"
    author: "Maarten ter Huurne"
  - subject: "Re: Kickoff"
    date: 2007-10-10
    body: "\"I can launch an application before you can even display the start menu. Alt-Space-F-Enter. Takes a fraction of a second and launches Firefox, ... .\"\n\nI like using the keyboard, also. This is one of the reasons I like Linux in general better than Windows (and KDE in particular). With KControl, I can assign a key combination to launch any program I wish. For me, Win-F opens Firefox, Win-T opens Thunderbird, etc. This actually seems slightly faster than Alt-Space-F-Enter, etc.\n\nSince these options were available in KControl, I was able to use them without installing anything extra."
    author: "Nathan"
  - subject: "Re: Kickoff"
    date: 2007-10-11
    body: ">> For me, Win-F opens Firefox, Win-T opens Thunderbird, etc. This actually seems slightly faster than Alt-Space-F-Enter, etc.\n\nAbsolutely.  I also have these shortcuts (funny that the Windows key is actually more useful in Linux).  However I don't think most people go to the trouble of defining shortcuts for every app they use, so having a system that works with every app is very handy.  I tend to make a specific shortcut for apps I use every few minutes (browser mostly) and use a text based launcher for the rest, where the extra keystroke isn't going to make much difference.\n\nKrunner is going to bring this kind of interaction to the masses (well, as many masses as are using KDE+linux :)"
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "Please, don't be so freak. What about if I just want to see which programs are installed? should I search for all of them by writting words? Please."
    author: "I\u00f1aki Baz"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "Yes, and for that use case it is fine to take some time to browse the list - you don't really need speed for that."
    author: "Paul Eggleton"
  - subject: "Re: Kickoff"
    date: 2007-10-10
    body: "... so it is fine to slow down the things necessary to browse the list\n\nand while at it, why do not slow down the speed at which the browser displays the page, 'cause people do not read at greater speeds than say 1 line per second for sure, etc. etc.\n\n- great logic :-)"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "I agree that what i would like most is a quick a dirty hack to get kmenu from kde3 into kde4. Should not be standard, but i'm sure many people would like it, and not those shiny animated menus. I'm simply too used to the the normal menu, and i get the application i want very fast. Perhaps a new menu could save me a few miliseconds...but.. is it realy worth it? i simply don't use the menu often enough"
    author: "Beat Wolf"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "What I like about Kickoff is the search field. In a hierarchical menu, it can take quite some time for me to find an application that I don't use often, especially since the location seems to be different across distros and change over time.\n\nIf it would be possible in Kickoff to have the Applications tab as the first tab (instead of Favorites) and skip/speed up the enter/leave menu transition, would that solve your complaint? Or is hover vs click navigation essential?"
    author: "Maarten ter Huurne"
  - subject: "Re: Kickoff"
    date: 2007-10-09
    body: "I think there should be configurable options to start on the favorites or apps, as options are always good things.  I'd also like to see the transition from one layer of the menu to the next a bit faster, perhaps via hovering with a minimal delay.\n\nHowever, at the same time, I don't believe I'm the target user for Kickoff since other people seem to like it just fine."
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "To put it simple: If \"Kickoff\" is what is the default start menu in openSUSE 10.3 and if KDE 4 won't allow me to use the traditional style menu - then KDE 4 will never see the light on any of my computers.\nThe traditional style provides me with a much better overview of what is available to me. \nI don't need the most used apps as default panel (forcing me to switch to a different panel every time) - those apps get icons in my control bar.\nI don't like the slow horizontal scrolling of the sub-menus which always make me wonder where I've got lost and which force me to click back my trail on those arrow buttons on the left instead of directly seeing what other options I have on the top level.\nYou may put as much effort in these menus as you like, but just let the users choose."
    author: "Anonymous Coward"
  - subject: "KPrinter"
    date: 2007-10-09
    body: "Jason Harris / KStars: \"I am using QPrinter and QPrintDialog instead of KPrinter, because from what I've read, KPrinter will probably not be ready for KDE-4.0.\"\n\nIn the KDE 4 release schedule it says under Printing (current state: showstopper): \"All current users of libkdeprint (ie: KPrinter) need porting to QPrinter/QPrintDialog/KPrintPreview.\"\n\nSo does this mean...?\n* KDE 4.0: Ditch KPrinter. Port all programs from KPrinter to QPrinter.\n* KDE 4.1: Resurrect KPrinter. Port all programs from QPrinter to KPrinter.\n\nThat's how I would reconciliate the two statements. But that sounds a bit strange, of course. Or is KPrinter going away for good? What is the story?"
    author: "Martin"
  - subject: "Re: KPrinter"
    date: 2007-10-09
    body: "It's a said story. A story about how KDE managed to kill one of its former showpiece applications through carelessness, lack of maintenance, bit-rotting and abundant negligence.\n\nI would never have thought that Gnome would ever have better printing support than KDE. But soon it will (and not because Gnome has catched up)."
    author: "interested observer"
  - subject: "Re: KPrinter"
    date: 2007-10-09
    body: "That's an interesting way of spinning the events.\n\nWhat really happened is that the person who wrote the code barely documented anything, making him the only person with a realistic chance of maintaining it.  Since he no longer has time to do so, and since printing is inherently a complex field that few people know anything about, it had to be ditched - people tried to get it to work, but were defeated by the daunting 50k+ lines of code with very little comments.  There's no way they could have got it in shape for 4.0 and if they had left it in, they would have had to support this mess through the entirety of the KDE4 cycle.  These people are overworked enough as it is, without having to take on this additional burden.\n\nSaying that \"KDE killed it\" through \"abundant negligence\" and \"carelessness\" is nonsensical - they don't have the funds to hire people to work on printing, and they can't *force* volunteers to work on it, so what could they have done?"
    author: "Anon"
  - subject: "Re: KPrinter"
    date: 2007-10-09
    body: "Well actually \"50k+ lines of code with very little comments\" is negligence and carelessness. And blaming \"KDE\" seems appropriate as the development process didn't work. Stricter guidlines and policy enforcement for documentation of core components of KDE would be a good thing. \n\nNot really trying to play a blame game here. Of course people are overworked and most fon't even get paid to do anything, we all know and understand that. This arguments can be used to 'justify' any lower quality standards, be it missing features, bugs, or whatever. Instead it seems much better to just look at what happened, try to learn from it, and find ways to minimize the chances of it happening again. "
    author: "Bxs"
  - subject: "Re: KPrinter"
    date: 2007-10-09
    body: "> Well actually \"50k+ lines of code with very little comments\" is negligence and carelessness\n\nor a sign that the task is rather complex or would you name any project that has more then 50k loc \"negligence and carelessness\" ?\n"
    author: "Sebastian Sauer"
  - subject: "Re: KPrinter"
    date: 2007-10-09
    body: "No. Just those with very little comments. Got it?"
    author: "Bxs"
  - subject: "Re: KPrinter"
    date: 2007-10-10
    body: "Send your patch to any one of the kde developers. I'm sure they would appreciate it.\n\nDerek"
    author: "D Kite"
  - subject: "Re: KPrinter"
    date: 2007-10-10
    body: "Maybe you've read that answer a number of times and from the context you deduced that it's something you're supposed to say whenever someone is or seems to be \"complaining\" about something. If you actually pay attention to the meaning of those words and to the content of my post though, you'll eventually realize that in this particular case talking about a \"patch\" is nonsense. I wish you better luck with your next use of this standard answer though. Cheers."
    author: "Bxs"
  - subject: "Re: KPrinter"
    date: 2007-10-10
    body: "Actually I'm very serious.\n\nComplaints in a contribution community are corrosive. \n\nWe have a large number of people who work long hours without pay to make KDE. They are well aware of the shortcomings of the code. That's why they work long hours on it.\n\nI would rather act in a way that gives them an idea that I appreciate what they do. Corrosive complaints actually cause people to not want to give to those who seem only to complain.\n\nSo again, where is your patch? I'm sure it would be appreciated.\n\nDerek"
    author: "D Kite"
  - subject: "Re: KPrinter"
    date: 2007-10-10
    body: "I know you're serious. You're just not making any freaking sense. "
    author: "Bxs"
  - subject: "Re: KPrinter"
    date: 2007-10-10
    body: "Okay, in freaking simple words: you see a problem. Go fix it."
    author: "Boudewijn Rempt"
  - subject: "Re: KPrinter"
    date: 2007-10-10
    body: "First of all let me say that I really respect your work and enjoy reading your posts. Honestly that is the only reason why I even bother to reply.\n\nThe whole point of my first post was to say, there was a problem and it shouldn't be ignored. Any developer knows that 50k lines of code without proper documentation is insane. Also suggested that maybe enforcing stricter guidelines regarding documentation of core components might be a good idea. \n\nNot being allowed to acknowledge problems unless you're able and willing to fix them makes no sense. And Kite's talk about submiting patches just reads like a post in the wrong thread, totally out of place.\n\nKeep up the good work!"
    author: "Bxs"
  - subject: "Re: KPrinter"
    date: 2007-10-11
    body: ">> Keep up the good work!\n\nHint: mixing your encouragement with unfounded whining makes the whole thing ring hollow."
    author: "Leo S"
  - subject: "Re: KPrinter"
    date: 2007-10-11
    body: "The problem is with KDE allowing 50k LOC with minimal documentation in an important part of th DE.  I can't think of a patch to send in to correct that policy shortcoming.  "
    author: "MamiyaOtaru"
  - subject: "Re: KPrinter"
    date: 2007-10-09
    body: "QPrinter has caught up quite a bit with Qt4, from a maintenance and support point of view, it makes total sense to base KDE's printing more closely on the one of Qt4. With those changes, we don't have to choose wether to implement everything ourselves. We can now do cherrypicking from the Qt's printing system. That also helps a lot with platform compatibility of course.\n\nEven if KDE4's printing support will not be as excellent as KDE3's, I'm sure this will get sorted out over time."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: KPrinter"
    date: 2007-10-11
    body: "So, this means that we will not have the \"multiple pages per sheet\" option in kde4?\n\nMan will i miss it."
    author: "Mitch"
  - subject: "Re: KPrinter"
    date: 2007-10-09
    body: "It means that for KDE4.x we ditch KPrinter completely since QPrinter does everything KDE3's KPrinter (class) did, and even more plus its actually maintained.\n\nThe obvious advantage is that there don't have to be two parties both doing approximately the same printing development, KDE can just depend on the stuff that Qt does and get all the new features themselves; something that didn't happen with the old system we had in KDE3."
    author: "Thomas Zander"
  - subject: "Yakuake"
    date: 2007-10-09
    body: "Is there any work on a Yakuake port to KDE 4?\n\nI thought I had read somewhere that real transparency was going to be included int he KDE 4 version, but I haven't seen any development on it that I'm aware of.  Again, this isn't something vital, but I do love Yakuake."
    author: "T. J. Brumfield"
  - subject: "Re: Yakuake"
    date: 2007-10-09
    body: "> Is there any work on a Yakuake port to KDE 4?\n\nIt's not been ported yet, but the plan is certainly to have it available in KDE4 Extragear in time for the KDE 4.0 release. The Yakuake codebase is fairly small, and a full port shouldn't take more than weekend. COMPOSITE support is still on the roadmap."
    author: "Eike Hein"
  - subject: "Re: Yakuake"
    date: 2007-10-09
    body: "Sounds fantastic!  Thanks for the update.\n\nAnd color me ignorant, but doesn't konsole support composite/transparency now?  Doesn't yakuake depend on a konsole kpart?"
    author: "T. J. Brumfield"
  - subject: "Re: Yakuake"
    date: 2007-10-09
    body: "Yes, but the standalone Konsole application doesn't actually use the KPart, and Konsole and the KPart each implement background painting individually. The KDE 3 Konsole application does have a hackish implementation of COMPOSITE support (that requires a Qt patch to work without side-effects), but the KPart does not: the provisions to adjust the painting depending on whether or not an ARGB32 visual is used are deactivated."
    author: "Eike Hein"
  - subject: "Re: Yakuake"
    date: 2007-10-10
    body: "Fair enough.  However I'm assuming the code to the painting can be easily copied/ported from konsole to the newly ported yakuake when that is done, right?"
    author: "T. J. Brumfield"
  - subject: "Re: Yakuake"
    date: 2007-10-10
    body: "No, you didn't catch that right. The culprit with regard to COMPOSITE support is the Konsole KPart, not Yakuake, and fixing that requires changing the Konsole KPart, which will hopefully materialize for KDE 4. "
    author: "Eike Hein"
  - subject: "Re: Yakuake"
    date: 2007-10-10
    body: "Sorry.  When you said they each handle painting separately, I thought that meant painting was outside the KPart."
    author: "T. J. Brumfield"
  - subject: "Re: Yakuake"
    date: 2007-10-10
    body: "Konsole doesn't use the KPart."
    author: "martin"
  - subject: "Re: Yakuake"
    date: 2007-11-26
    body: "I'm not sure if it is purely because of also having QT3 installed and running the KDE4 rc in paralell to KDE3.5, but my yakuake is working in KDE4 rc1 with compositing on. Albeit using QT3 styles and all, and having one or two graphical glitches ( such as the tab bar and no transparency ), at least you know that even in the case that an official port isn't done anytime soon, you can still use ( i mean using a wm without yakuake for me has become very tedious, and i was reall stoked when i saw it work :P ).\nIf i wasn't too busy with work atm, i might have attempted a port, but alas, it is teh life.\n:DG"
    author: "David Goemans"
  - subject: "Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "Lubo&#353; Lu&#328;\u00e1k: \"This gets rid of the Klipper icon in the topleft corner ... and since I don't see any system tray anywhere, it kind of gets rid of Klipper completely. And I suppose it doesn't help much that Klipper itself seems broken, so no history keeping or Ctrl+Alt+V. Oh well.\"\n\nI'm getting nervous about KDE4. Not just 4.0, but KDE4. Why did porting to Qt4 and adding some neat technologies break just about everything, all over the place? :-(\n\nBut everything will be fine, will it not? :-)"
    author: "Martin"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "It will be... what it will be :)"
    author: "manyoso"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "KDE 4 is a complete refactoring, and I think that is a VERY good thing, even if it means the initial quality will be lower than the 3.5.8 release.  When you break such things, you get to examine how to rewrite it from a new perspective."
    author: "T. J. Brumfield"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "I agree\n\nits important to get new ideas into it, when breaking with old paradigms\nyou can break with legacy that is not as good as it *should be*"
    author: "she"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-14
    body: "I totally disagree -- but perhaps when I see KDE-4.2.1 I will change my mind.\n\nI think that we are making the \"Vista mistake\".\n\nI would suggest that we port 3.5 to Qt-4 but there are bugs to be fixed first."
    author: "James Richard Tyrer"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "it didn't break just about everything all over the place. it did break a number of things, particular things with lack luster maintenance which have coasted by over the last years when we didn't budge much underneath the sheets (from 2.x through to 3.x). many of those things have been fixed; some are being fixed; others are yet to be fixed. this particular case looks like regressions in klipper that should be completely fixable ... when someone arrives to fix it. it's a question of 'when' not 'if'.\n\nas for no system tray, there is one partially done it just isn't in kdebase/ atm.\n\nit is fun when one answers issue A (\"4.0 will be...\") people just move on to issue A+1 (\"ok, what about the rest of kde4?\") without looking back or, for that matter, forward ... this semiblind wandering from concern to concern makes for an unrewarding game, doesn't it?"
    author: "Aaron J. Seigo"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "Relax. I couldn't help the somewhat sarcastic tone of the commit message, since the combination of systray not working, Klippers shortcut not working and Klipper's clipboard tracking not working really in practice makes Klipper not to be there, but that will be fixed. Nothing is perfect and things break from time to time, especially if bigger changes are made. That's reality.\n"
    author: "Lubo&#353; Lu&#328;\u00e1k"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "Drat. Why did Dot change characters in my name to entities while previewing :( ?\n"
    author: "Lubo&#353; Lu&#328;\u00e1k"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "I'm sure it's just having fun with your name ;-)"
    author: "jospoortvliet"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "I take it you weren't around for the KDE 2.0 and KDE 3.0 releases?  Yes, they were very bugy....but they were ground breaking for Linux and maybe computing in general (in some areas).  Those releases were much more stable at their 2.1 and 3.1 versions.  The feature set also grew dramatically since the \"Dot Oh\" releases.  \n\nIf you prefer your software in *great* condition, I think anyone would recommend you wait till KDE 4.1."
    author: "Henry S."
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-09
    body: "...or use both.  Some distros are set up now to let you choose a KDE3 session or a KDE4 session.  That's what I plan to do.  I'll continue to use KDE3 for production, while testing the limits of KDE4 whenever possible."
    author: "Louis"
  - subject: "Re: Everything will be fine in the end, no?"
    date: 2007-10-10
    body: "That's what I'm doing now. I'm using 3.5.7 and 4.0-svn from SuSE 10.3 repos. Unfortunately, \"/usr/bin\" (KDE 4) is before \"/opt/kde3/bin\" in my $PATH. Therefore, I have a little sed script which modifies my PATH variable to put /opt/kde3/bin before /usr/bin. Combines with a little bash script at /usr/local/bin/4, I invoke Konqueror 3 with \"konqueror\" and Konqueror 4 with \"4 konqueror\"."
    author: "Stefan"
  - subject: "Klipper and Kprinter"
    date: 2007-10-09
    body: "I think these two programs are two gems in KDE's crown... and I really hope they could appear in the 4.0 timeframe, at least Klipper which should fit almost perfectly as a Plasma applet: if in a panel, just an icon as the current klipper applet, with a menu appearing when clicking on it, just as now, And if on the desktop, then a little window with all the clipper contents shown."
    author: "Vide"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-09
    body: "Yeah, the state of both Klipper and Kprinter freaked me out a bit also, but:\n- klipper ain't that bad (see Lubos' comment above)\n- Kprinter isn't what most think it is. The config tool can probably be ported, and there now is a patch on Qt which allows for custom print dialogs or at leas extensions of it. And as far as I can tell from the (rather huge) discussion, it seems to me we won't loose that much functionality - Qprinter isn't that bad in Qt4. Besides, it looks like TT is also comitted to improving it further - so in the future, we might be able to rely on TT for our printing infrastructure."
    author: "jospoortvliet"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-09
    body: "Is printing really important anymore? Maybe that infamous paperless office have finally arrived?\nI bought a new Canon printer with built-in scanner not long ago, I have used the scanner part but the printer hardly at all.\n I also have a hefty HP printer I don't use much."
    author: "reihal"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-09
    body: "In commercial/ workplace/ educational settings, yes.  For home use, I also no longer use a printer."
    author: "Anon"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-09
    body: "Yes, it is. Here where I work there are 7 printers, and I'm sure there are corps where this number is exponentially higher. And for example it was a huge merit of kdeprint/cups if with our newest Kyocera laser printer, that supports double-side printing, all I have to do to print a 90+ pages manual in double-side mode was to simply select a radio button in the \"printer preferences\" dialog in KPDF.\n\nI hope this level of \"just-works\" won't disappear with KDE 4.0"
    author: "Vide"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-10
    body: "Can someone please think of the trees!\nIt's a shame that the design of the e-book readers are so screwed up,\nbut it's just a matter of time until they get it right.\n"
    author: "reihal"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-10
    body: "How much energy and resources do you think *those* take to make? Trees are a renewable resource, many of the components in an ebook reader are not. \n\nStill: I want one! :-)\n"
    author: "Andre"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-11
    body: "How much energy and resources do you think it takes to make paper\nand transport it? And plant new trees? \n\nHere is some new stuff:  http://www.gizmag.com/go/8152/\n\nPersonally I just want B/W with perfect font rendering, kerning, ligatures\nand such."
    author: "reihal"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-11
    body: "I forgot: How much energy and resources does it take to make and transport fertilizer\n to compensate for the trees you take out of the soil producing cycle?\nA lot, is my guess.\n\nNo, get on with the e-readers. Think thin iTouch in various sizes from credit card to A5."
    author: "reihal"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-12
    body: "There are still documents that, at least where I live, you have to have in printed form to be legally valid. Anyway, talking about myself, I only print manuals and only when it's a new technology I have to learn from scratch, cause reading it online makes me sick. I do care about trees, I can assure you, and I'm not one of those printing a 10 pages utterly quoted Outlook mail because there are 10 lines I need to read in a meeting :)"
    author: "Vide"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-09
    body: "This is the most made mistake on the subject; all the commits talk about KPrinter, which is a class that is a KDE version of QPrinter.\nThen there is an application that is basically a print dialog with lots of neat features that is called kprinter.\n\nThe first will go away, people just use QPrinter and actually get more features like that.\nThe application that end users see will not go away, or even need a whole lot of work. You will still be able to do all those neat things in KDE4 like you are used to."
    author: "Thomas Zander"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-10
    body: "Ah, so what you're saying is that there are 2 parts to KPrinter, a front end and a back end.  The front end is the print dialogue that a user sees when they select \"Print\" from a document, and this part will remain.  The difference is that instead of using the back end of KPrinter, it will use QPrinter as a back end.\n\nIf this is the case, it shouldn't be too much of an issue."
    author: "cirehawk"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-10
    body: "Yes, that is what I was saying. :)\nThanks for the clearer language."
    author: "Thomas Zander"
  - subject: "Re: Klipper and Kprinter"
    date: 2007-10-10
    body: "Ok, thanks for the clarification. One more question: has the \"printers manager\" UI got a revamp? I think it was one of the most cluttered UI in all KDE 3.x softwares..."
    author: "Vide"
  - subject: "Kickoff vs raptor"
    date: 2007-10-09
    body: "Is it just me, or is Kickoff and Raptor concurrent work? What will become the default for KDE 4? I mean, with two months left, I sure hope that kicker on the screenshot isn't the final design..."
    author: "Dennie"
  - subject: "Re: Kickoff vs raptor"
    date: 2007-10-09
    body: "\"Is it just me, or is Kickoff and Raptor concurrent work?\"\n\nThat is correct.\n\n\"What will become the default for KDE 4?\"\n\nUnknown at this time, but Kickoff looks like it has the best chance of being the default for 4.0.\n\n\"I mean, with two months left, I sure hope that kicker on the screenshot isn't the final design...\"\n\nIt is not the final design: check out Nuno's mockups here:\n\nhttp://www.nuno-icons.com/images/estilo/\n\nespecially e.g. http://www.nuno-icons.com/images/estilo/image2274.png.  Whether the \"final\" result will look exactly like these mockups is still unknown, but I'm betting it at least won't look like it does at present, with the purple bar etc, although it is possible that this current purple bar will still look as it does for 4.0, depending on the amount of time the oxygen and plasma guys have to spend on aesthetics.  But in the medium-to-long term, it will hopefully look more like Nuno's mockups."
    author: "Anon"
  - subject: "Re: Kickoff vs raptor"
    date: 2007-10-10
    body: "So, what actually happened is this: The release date is set to mid-december, but it is almost certain that plasma/desktop and the oxygen team won't be able to deliver as mocked up. Sorry, maybe it's me, but it sounds to me that the release-date has to be rescheduled to march or something. "
    author: "Dennie"
  - subject: "Re: Kickoff vs raptor"
    date: 2007-10-10
    body: "Why should a 4.0 release be delayed just because its aesthetics don't match the mockup?"
    author: "Anon"
  - subject: "Re: Kickoff vs raptor"
    date: 2007-10-09
    body: "I'm not sure if there is any Raptor code at all yet, so I'm guessing Kickoff will be the default in 4.0"
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff vs raptor"
    date: 2007-10-09
    body: "Raptor has been in development for many months but, last time I looked, it did not have the features, structure or functionality of those mockups, although it is recognisable as an early step towards them."
    author: "Anon"
  - subject: "Re: Kickoff vs raptor"
    date: 2007-10-09
    body: "Actually, it look almost like that, but do nothing at all and slowdown plasma. But it is a start, it \"exist\", it just don't work yet. I am not (yet) a developer, but  i dont think it will be hard to take some code from lancelot and add it to raptor to get the textbox and apps definition working. For the menu, all it need is an editable structure and some polishing. no?"
    author: "Emmanuel Lepage Vallee"
  - subject: "Re: Kickoff vs raptor"
    date: 2007-10-09
    body: "Sounds good.  I'm eager to try it."
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff vs raptor"
    date: 2007-10-10
    body: "2 hours before deadline, Mart while port TastyMenu to Qt4/KDE4, and it will become the default, just because it's superior in every use case (looking through, just clicking through, lightning fast searching speed, favourite pinning, etc.)\n\nSo say we all,\nmartin\n\n(http://www.notmart.org/tastymenu/ and no, you can adjust the size, and is it like you actually look at anything else when you have the menu open?)"
    author: "martin"
  - subject: "\"Seam Carving\" for Digikam"
    date: 2007-10-09
    body: "A few commit-digest back there was a post about a content aware image sizing algorithm which deletes unimportant parts of an image and resize it without to distort it.\n\nhttp://www.faculty.idc.ac.il/arik/imret.pdf (~20MB)\nhttp://www.youtube.com/watch?v=c-SSu3tJ3ns\n\nNow there's a GPL library which implemented that algorithm. \n\nhttp://seam-carver.sourceforge.net/\n\nWould be nice to see it in Digikam sometime in future."
    author: "hias"
  - subject: "Re: \"Seam Carving\" for Digikam"
    date: 2007-10-10
    body: "I would love to see this in Digikam in the 4.1 release cycle."
    author: "T. J. Brumfield"
  - subject: "Re: \"Seam Carving\" for Digikam"
    date: 2007-10-10
    body: "Has coordinator of digiKam project, i don't know 4.1 release cycle (:=))). Are you talking about a digiKam release for KDE4.1 ?\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: \"Seam Carving\" for Digikam"
    date: 2007-10-10
    body: "Yes, that is what I meant."
    author: "T. J. Brumfield"
  - subject: "Re: \"Seam Carving\" for Digikam"
    date: 2007-10-10
    body: "I have take a look into source code of this shared lib. \n\nImplementation is horible to read, but it's possible to backport algorithm into digiKam core (i won't to add another one shared lib depency, especially for that)\n\nBut it's currently the priority. digiKam KDE4 port (0.10.0 release) must be finalized before. \n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: \"Seam Carving\" for Digikam"
    date: 2007-10-10
    body: "Made a wish in KDE bug traking system. Just that idea did'nt get lost\n"
    author: "Dumas"
  - subject: "Re: \"Seam Carving\" for Digikam"
    date: 2007-10-10
    body: "it's already there (bug 149485). just wanted to point to the lib, because I thought it would make it easier to implement this feature "
    author: "hias"
  - subject: "Desktop Toolbox"
    date: 2007-10-09
    body: "I hope the Desktop Toolbox will be changed until the release. It is hardly discoverable for newbies and is also uncomfortable. (E. g. I always move the mouse pointer out of it accidentally and then it disappears.)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Desktop Toolbox"
    date: 2007-10-10
    body: "AFAIK it will be replaced by the Plasma Applet Browser: http://www.youtube.com/watch?v=4QDZkB3PoAk"
    author: "cloose"
  - subject: "Re: Desktop Toolbox"
    date: 2007-10-10
    body: "More reading :D\nhttp://ivan.fomentgroup.org/blog/2007/09/29/applet-browser-in-libplasma/"
    author: "Hans"
  - subject: "Possible Klipper feature"
    date: 2007-10-09
    body: "One of my favorite features in kde is the web shortcuts feature.  I always find myself hitting alt-f2 and using wp: and gg: to quickly look something up.\n\nAnd in the past, I've associated a 6 digit regex with klipper to pull up bug reports on various bug systems.\n\nI wonder if a combination of the two is possible somehow though, maybe by holding down a ctl-shift combination of some sort I could click on any bit of text on my display and do a wp:, gg: or bug url equivalent?\n\nMaybe this is already possible with kipper..."
    author: "Jesse Barnes"
  - subject: "Extenders"
    date: 2007-10-10
    body: "What about extenders on plasmoids? Will they be ready for KDE 4.0?"
    author: "Aleite"
  - subject: "kopete"
    date: 2007-10-11
    body: "sigh, another release and still nothing of interest being done to kopete.  Might have to change to gaim soon, kopete is getting a bit old and tired, and all they are doing is porting this to kde4, no new release.  Come on.  communicating between the different msn yahoo network doesn't work on kopete, gaim just seems to offer more than kopete now."
    author: "Richard"
  - subject: "Re: kopete"
    date: 2007-10-12
    body: "You have probably seen this, but just in case...\nhttp://liquidat.wordpress.com/2007/10/11/kopete-roadmap-rediscovered/"
    author: "Leo S"
  - subject: "Colour conversion system in Krita"
    date: 2007-10-14
    body: "Colour conversion system becomes fully operational in Krita????\n\nIt is hard to know what to say.  It is obvious that a lot of hard work was put into this, but the plain fact is that in 1.6.3 it doesn't work correctly.  At least it doesn't do what I expect -- it appears to work backwards.\n\nPlease see:\n\nhttp://lists.kde.org/?l=koffice-devel&m=119234141218707&w=2\n\nwhich includes links to screen shots.\n\nIn this context perhaps the term 'colour conversion' is a misnomer.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Colour conversion system in Krita"
    date: 2007-10-15
    body: "James, please make a noise like a hoop and roll away. You don't know what you're talking about and your spamming the developer mailing list only wastes our time."
    author: "Boudewijn Rempt"
  - subject: "Re: Colour conversion system in Krita"
    date: 2007-10-15
    body: "Boudewijn, I think that James \"performances\" are well-known to every KDE passionate user, but nonetheless it's useful repeat it again and again because he really likes to generate noise on a couple of topics he (thinks he) knows (Krita colors convertion, KOffice/Qt font rendering etc)."
    author: "Vide"
  - subject: "Re: Colour conversion system in Krita"
    date: 2007-10-16
    body: "It should be noted that I am 100% correct about the Qt PS driver issue.  It should also be noted that I was not the first person to raise the issue.  WYSIWYG does not work correctly in KWord 3.x.  We all hope that with Qt 4 that this can be fixed.\n\nBR insists that he knows that RGB and CMYK color models are not (mathematically) equivalent and the users should use CMYK data files for printing (photographic printers [like at the one-hour places] actually are RGB).  In this instance he doesn't appear to know what he is talking about.  But, I have not been rude and insulting to him.  I have not posted a blog on the web for the sole purpose of insulting him.  Perhaps he doesn't know the difference between remarks about an application and personal insults.  This is an important distinction we all need to make.  People that can't make this distinction have too much of their ego invested in their code.\n\nYes, I made an error using Krita and the incorrect file which I posed is the result of that error.  However, there does seem to be a bug in the way it converts to a smaller gamut using Perceptual Intent.  At least, I did it over and the result is still not what I consider to be correct [file at the same place].\n\nSo, is this how we should do things.  Insult people that have trouble using an app or that actually find bugs and tell them to go away.  If he will treat me with respect, I will attempt to return the favor.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Colour conversion system in Krita"
    date: 2007-10-17
    body: "Well, I didn't take your advice.  I kept at it till I found the cause of the problem.\n\nAs often is the case, it wasn't what it first appeared to be.\n\nhttp://bugs.kde.org/show_bug.cgi?id=150890\n\nThis bug resulted in my having a file which was as I described, the color management was backwards.  This happened not because of a problem with the color management but the result of not properly loading and saving the file.  There is no easy way for a user to tell since the screen image looks the same in Krita  \n\nCyrille Berger is working on it.\n\nI still have some questions about saturation which is very important in photographic printing since saturation is always lost due not only to imperfect dyes (as in inkjet printing) but also due to imperfect color sensitization."
    author: "James Richard Tyrer"
  - subject: "Re: Colour conversion system in Krita"
    date: 2007-10-16
    body: "After considerably more work on this, I find that it works perfectly when converting from a small gamut to a large gamut -- which is not what I am interested in.  However when converting to a smaller color space using Perceptural Intent, it clips the data.\n\nThis reference (NOT ME):\n\nhttp://www.cambridgeincolour.com/tutorials/color-space-conversion.htm\n\nsays that this is wrong."
    author: "James Richard Tyrer"
---
In <a href="http://commit-digest.org/issues/2007-10-07/">this week's KDE Commit-Digest</a>: Image support in <a href="http://edu.kde.org/parley/">Parley</a>, and support for formulas in the note feature of the <a href="http://stepcore.sourceforge.net/">Step</a> physics simulation package. <a href="http://edu.kde.org/blinken/">blinKen</a> changes capitalisation to Blinken for the KDE 4.0 release. Theme work across kdegames, with better collision detection in Kolf. More XMP integration work in <a href="http://www.digikam.org/">Digikam</a>. Work on KConfig merged back into trunk/. Colour conversion system becomes fully operational in <a href="http://www.koffice.org/krita/">Krita</a>. Continued work on the port of the <a href="http://en.opensuse.org/Kickoff">Kickoff</a> menu to KDE 4, initial work on a centred-button menu in <a href="http://www.kde-look.org/content/show.php/Raptor+?content=56391&amp;PHPSESSID=cae">Raptor</a>. KIOFuse, the KIOSlave filesystem bridge, starts to be ported to KDE 4. An uncertain future for the Klipper applet in KDE 4.0, compared to its KDE 3.x form.

<!--break-->
