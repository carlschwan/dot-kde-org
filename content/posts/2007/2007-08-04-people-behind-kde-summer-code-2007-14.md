---
title: "People Behind KDE: Summer of Code 2007 (1/4)"
date:    2007-08-04
authors:
  - "dallen"
slug:    people-behind-kde-summer-code-2007-14
comments:
  - subject: "Photo manager"
    date: 2007-08-04
    body: "<BLOCKQUOTE>I think we need a good photo manager, with features like posting to blogs, mailing, reworking the pics, tagging... apologies if it already exists and I never encountered it.</BLOCKQUOTE>\n\nDigikam is not good enough for this job?"
    author: "Med"
  - subject: "Very good!"
    date: 2007-08-04
    body: "I really liked reading this 4-sided interview.. I also visited the personal/blog pages of the four students. It's very good to see talented guys working this passionately on their own stuff!\nReally really nice article!!\n"
    author: "a reader"
  - subject: "i understand you my brother ;-)"
    date: 2007-08-04
    body: " \n\"the UPC (Universitat Polit\u00e8cnica de Catalunya) and it is somewhere in Barcelona where computer, telecomunications and civil engineering students apply for their course. That means 0 girls around :P. Anyway, it is a great place to study and meet interesting people.\" \n\nyeah, when i was a student ( in civil engineering) we had 500 boys for only 10 girls. i never recovered from this injustice ;-)\n\ngood works guys"
    author: "djouallah mimoune"
  - subject: "finereader"
    date: 2007-08-04
    body: "omg! just googled 'ocr linux russian' and found out that support for russian is to be added soon for google-sponsored tesseract-ocr\nhttp://groups.google.com/group/tesseract-ocr/browse_thread/thread/f2edef50c405bb4d/0821d2ab2a026f6e?lnk=gst&q=russian&rnum=3#0821d2ab2a026f6e"
    author: "shaforostoff"
---
The <a href="http://behindkde.org/">People Behind KDE</a> series takes a temporary break, as we talk to students who are working on KDE as part of the <a href="http://code.google.com/soc/kde/about.html">Google Summer of Code 2007</a> - in the first of four interview articles, <a href="http://behindkde.org/people/soc2007-one/">meet Aleix Pol Gonzàlez, Piyush Verma, Mike Arthur and Nick Shaforostoff</a>!
<!--break-->
