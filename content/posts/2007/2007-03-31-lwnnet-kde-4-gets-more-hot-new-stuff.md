---
title: "LWN.net: KDE 4 Gets More Hot New Stuff"
date:    2007-03-31
authors:
  - "jriddell"
slug:    lwnnet-kde-4-gets-more-hot-new-stuff
comments:
  - subject: "Khtml blacklist"
    date: 2007-03-31
    body: "Can we imagine konqueror using KNS to keep some blacklist up to date? (like with the \"AdBlock Plus\" firefox extension)?"
    author: "Vincent"
  - subject: "Re: Khtml blacklist"
    date: 2007-03-31
    body: "yes, but as you might have read from the article, it won't update it automatically.\n\n>Although KNS 2 is not capable of automatically downloading \n>updated content, it will indicate to the user that the \n>update is available."
    author: "superstoned"
  - subject: "Re: Khtml blacklist"
    date: 2007-03-31
    body: "auto update on items would be cool to :|"
    author: "chrissel"
  - subject: "Re: Khtml blacklist"
    date: 2007-03-31
    body: "Yes, but they could do this:\n\n\"Content providers can offer automatic synchronization by listing references to files on a network rather than the files themselves. When the application attempts to access this quasi-installed content, it will have to fetch it from the server using KIO rather than a local directory. In this way, the content will always appear up to date to the user.\""
    author: "Anonymous"
  - subject: "Re: Khtml blacklist"
    date: 2007-04-01
    body: "that sounds nasty for offline users."
    author: "Chani"
  - subject: "Re: Khtml blacklist"
    date: 2007-04-01
    body: "sounds like a no issue for khtml blacklist"
    author: "ac"
  - subject: "Re: Khtml blacklist"
    date: 2007-04-02
    body: "oh yeah.. that, uh, makes sense. :)"
    author: "Chani"
  - subject: "Re: Khtml blacklist"
    date: 2007-04-03
    body: "still, how often would it check??? I wouldn't want too much traffic..."
    author: "superstoned"
  - subject: "Re: Khtml blacklist"
    date: 2007-03-31
    body: "If it's aware of the new stuff why can't it automatically download it?"
    author: "Ben"
  - subject: "Re: Khtml blacklist"
    date: 2007-03-31
    body: "It's probably a deliberate design decision..."
    author: "Rob"
  - subject: "Lockdown"
    date: 2007-03-31
    body: "Is there any way for a sysadmin to lock down Get Hot New Stuff?"
    author: "Ben"
  - subject: "Re: Lockdown"
    date: 2007-04-11
    body: "Good question!  Maybe there's a download directory or config file you could write-protect, but this should have security designed-in, I agree."
    author: "Jel"
  - subject: "Get Hot New Config"
    date: 2007-03-31
    body: "What *would* be cool is if you could publish the entire set of your KDE program configurations to a server (minus personal things like email/im addresses), and let people download and try them out. I'd love to browse people's *personal* configurations online. (Does that make me sound like a voyeur?).\n\nKDE has so many settings that can be tweaked, and I enjoy seeing how other people have their systems configured when I go to events like aKademy. "
    author: "alsuren"
  - subject: "Re: Get Hot New Config"
    date: 2007-04-02
    body: "I think the idea is great! \n\nMaybe a Hot New stuff for the systemsettings. \n\nKDE can be widely customized, and I think most people (me included) never get real indepth knowledge about the things they can do. \n\nI think the publishing my KDE config/layout/... could give KDE a really big boost. \n\n\"KDE: Your Desktop how You want it... with ease\" \n\nIf these settings include a diff from the default settings, they could have an even greater impact. \n\nBut if they need specific programs, plugins and similar to work, publishing your config that way could require deep integration into the distribution you use. \n\nIt might have to install plugins and programs (or prompt the user), and it might have to remember which ones it installed, so they can be deinstalled when you switch to another preconfig (or the user can be prompted to deinstall them). "
    author: "Arne Babenhauserheide"
  - subject: "Re: Get Hot New Config - Me Too!"
    date: 2007-04-05
    body: "Me too!\n\nI would love such a utility (even though I know it's an impossible dream thanks to things like screen resolution differences, distribution differences, etc.)  \n\n\"Click here to use Linus's desktop configuration.\""
    author: "Jeorb"
  - subject: "Name"
    date: 2007-04-01
    body: "I wish they would change the name. \"Hot new stuff\" sounds like something a 6 year old came up with, not a professional desktop component. How about \"Aftermarket content\" or something similar."
    author: "John"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "Could you possibly come up with a duller name?"
    author: "me"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "What is wrong with self documenting names?"
    author: "reihal"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "\"Aftermarket content\" self documenting? I wouldn't know what it means if it weren't mentioned in the hot new stuff thread :-)\nI like get hot new stuff, it is straight forward and funny."
    author: "infopipe"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "How about \"Self-Leverageing community content\"?\n\nThere is nothing wrong with \"self documenting\" but sometimes a name is just painfully souless, that should be avoided. "
    author: "Ben"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "How about \"Kackle\"?"
    author: "reihal"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "I couldn't agree more!  Every time I read \"Hot new stuff\" I literally cringe.  It's not that the name itself is bad, it's just horrible inconsistent.  When I look at the Kmenu and see the handy descriptions of applications I don't see \"new fangled text editor\" for Kate or \"where you're spam is held\" for Kmail.  KDE has always used easy to understand, no nonsense language.  \"Hot new stuff,\" on the other hand, is totally culturally dependent (I wonder how it translates in other languages) and fails to mesh seamlessly with the rest of KDE's consistent use of language."
    author: "Sean Kellogg"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "My personal ground for objection is the use of English. KDE naming should be language-neutral."
    author: "Andy"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "The only time the user actually sees \"Get Hot New Stuff\" is after they've opened the GHNS window in an application.  Like in Amarok the button that opens the GHNS window is called \"Get More Scripts\" and \"Download Styles\".  People aren't going to be manually running the GHNS independent of any other application from the KMenu, so they'll only see the name after they're already into it.\n\nI'm no translation expert but I'd figure it wouldn't be too hard to translate and localize the name into other languages (I'm sure most languages have equivalents for 'hot' or 'neat' or 'cool' or 'awesome' as well as 'stuff').\n\nAlso GHNS isn't a description, its a name that doesn't make people think 'named by a computer'."
    author: "Sutoka"
  - subject: "Re: Name"
    date: 2007-04-02
    body: "I like \"Get Hot New Stuff\". it makes sense, and it somehow feels like it's encouraging me to install the stuff. :) it's a nice friendly name."
    author: "Chani"
  - subject: "Re: Name"
    date: 2007-04-02
    body: "Except that for non-english speaker, they don't understand it and it's complicated."
    author: "Richard Van Den Boom"
  - subject: "Re: Name"
    date: 2007-04-02
    body: "Wouldn't they be using a translated, localized version then?\nIt seems a bit like a non-issue to me, really. "
    author: "Andr\u00e9"
  - subject: "Re: Name"
    date: 2007-04-02
    body: "You should not have to translate the name of an app. Amarok is the same in every language, so does KDE. An app or a service should have a short name. It does not really matter if it's understood or not (nobody has issue with \"word\" or \"itunes\"), as it is a name for most people. But Get Hot New Stuff is not a name and is thus more complicated for people who do not speak english. Besides, it sounds like a bad commercial. :-)\nH\u00e9 here's a name ! Kommercial! :-D\n(OK, just joking....) "
    author: "Richard Van Den Boom"
  - subject: "Re: Name"
    date: 2007-04-02
    body: "Get Hot New Stuff isn't an application, it is a technology applications like Amarok can use, and the only real user visible time you would see the name is in the title bar for the GHNS window after you explicitly opened it from the application.  Consider it closer to the file selection dialog than an application."
    author: "Corbin"
  - subject: "Re: Name"
    date: 2007-04-06
    body: "\"Get hot new stuff\" is as good a name as \"Open file.\" As far as anyone should be concerned, they are both features and should have easy-to-understand names. Though it could be a bit less childish...maybe \"Download add-on's\" or such..."
    author: "Michael H."
  - subject: "Re: Name"
    date: 2007-04-01
    body: "Full ACK! Though I usually like innovative names like Amarok - this is really a bit too much. It's just too geeky to be taken seriously.\n"
    author: "Michael"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "With the exception that a lot of the names aren't pronounceable...\n\nAmarok?  Let see:\n\nAm-are-o-k\naim-are-ok\naim-air-ok\nam-air-oak\n\n???\n\nWho knows how to pronounce that.\n\nShall I start in on Kivio?\n\nkee-v-ee-oh\nkiv-ee-oh\nkie-vi(short i)-oh\n???\n\nI love the apps in and of themselves, but geesh...\n\n\"Hot New Stuff\" certainly isn't the best name, but it's very easy to pronounce and you have a better idea what it is for just at a glance.\n\n"
    author: "Xanadu"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "But that's not a problem of the Name, but a problem of the english language: It simply has not enough pronounciation rules.\nFor me, as an austrian, it is rather simple:\nAmarok is spelled: A - ma  (the \"a\" and \"ma\" like in \"mamma\" - not like in \"a\" or \"apple\")  - rok (like \"rock\")"
    author: "PolitikerNEU"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "This is good Linux tradition: You know how to spell \"Linux\", especially the \"i\" and the \"u\"? :p Even Linus Torvalds had to make it clear how to spell it right (hint: do never spell it \"English\", German spelling is quite next to his spelling).\n\nFor me (as a German) Amarok is as easy as Linux, and I never had any problems with Kivio and the infamous K of KDE is in many places inspired from German (which does have more K's than English, thanks to the orthography reform of 1901, previous to that it used as many \"C\"s as English).\n\nBut on the other hand in German you often have troubles with \"strange\" English names. For example our lovely text editor \"Kate\". It is a English girl name but in German you spell it different and it even has a different meaning although it is not used very much in today's German (a \"Kate\" is a lowly farmhouse where animals and humans are living under one roof).\n\nMy personal objection out of these language troubles are:\na) Do not use English names. They often create the biggest confusion cause they are often intended to mean everything and nothing at the same time (thanks to the \"international\" character).\nb) If you want something exotic look at some small languages for cute words and create a nice story around your name. That way everybody has to learn about it first. No way to let people think \"Ah I know everything I don't need to read the explanation\".\nb) If you want something which inmediatly can be understood, use Esperanto words. I myself was very much surprised how familar Esperanto words are in a broad range of languages and how nice it sounds and how easy it is to invent new words with a useful meaning (I got inspired by the \"Serchilo\" project and did name my own \"Pakanto\").\n"
    author: "Arnomane"
  - subject: "Re: Name in Esperanto - URLs"
    date: 2007-04-02
    body: "I'll second using words from Esperanto. \n\nAnd since not everyone knows Esperanto, here's an URL to get basic infos: \n- Basic infos: http://en.lernu.net/helpo/kiel_komenci/index.php\n\nIf you search for a name, these two could come in handy. \n- Dictionary online: http://reta-vortaro.de/revo/\n- qVortaro, a dictionary in Qt: http://qvortaro.berlios.de/\n\nAlso interesting are: \n- Wikipedia in Esperanto: http://eo.wikipedia.org/wiki/&#264;efpa&#285;o\n- Travelling with Esperanto: http://www.tejo.org/eo/ps_lingv_en\n\nAnd for all the Germans in here: \n- http://esperanto.de/"
    author: "Arne Babenhauserheide"
  - subject: "Re: Name"
    date: 2007-04-01
    body: "\"Full ACK! Though I usually like innovative names like Amarok - this is really a bit too much. It's just too geeky to be taken seriously.\"\n\nTake a quick look around the Internet, and you'll find a lot of websites with a respectable amount of traffic that get it from offering hot, free or cool new spywar^Wstuff to a market that is decidedly non-geeky. It's a snappy name that's understandable to anyone who speaks english, and I'm sure the name can be changed for other localities.\n\nNow if they'd have given it a recursive acronym like \"GHS Hot Stuff\" or similiar, that would be geeky."
    author: "Marc"
  - subject: "Re: Name"
    date: 2007-04-02
    body: "\"Now if they'd have given it a recursive acronym like \"GHS Hot Stuff\" or similiar, that would be geeky.\"\n\nNot to be a nitpicker here.. but...\n\n\"GHS Hot Stuff\" is not a recursive acronym :)\n\n\"GHS\" alone would be one...\n\nYou don't say:\n\"GNU's Not Unix\"\nBecause that's the initial unroll of the recursive \"GNU\"..\nAnd not the acronym itself :)\n\nbut you do say:\n\"GNU\" which is the recursive acronym for \"GNU's Not Unix\"\n\nAnyway..\n\nA recursive acronym for it could be:\nKuck - Kuck user content kitchen\nehehe...\n\nA better non playful (and way more serious) name might be:\nKAGUC- KDE Applications Get User Contributed Content\n\nuuhwee!!"
    author: "Larpon"
  - subject: "Re: Name"
    date: 2007-04-02
    body: "Well, I don't have anything against the name myself, but maybe people would be more comfortable with just \"KNewStuff\"?"
    author: "Darkelve"
  - subject: "Re: Name"
    date: 2007-04-06
    body: "I wish people would stop trying to get market speech everywhere. Fuck, you even needed to put in the word \"market\" in your proposal. It doesn't make the product more professional, it just makes it seem like an advertisement. What's not to cringe about then?\n\nLook, KDE isn't produced by or for marketers. It's free. Why should it try to emulate the business world?"
    author: "Gj\u00f8k"
  - subject: "Hot New Stuff"
    date: 2007-04-01
    body: "I thought Hot New Stuff was a stroke mag.\n\nMan, am I disappointed!"
    author: "T. J, Brumfield"
  - subject: "Re: Hot New Stuff"
    date: 2007-04-01
    body: "I concur. :) \n"
    author: "Brrrr"
  - subject: "package management"
    date: 2007-04-01
    body: "At some point this will have to incorporate full-blown package management features, namely tracking and resolving dependencies and allowing for manual and automatic updates of installed packages. Dependencies actually come in three forms: 1) the version of the host program (e.g. Amarok) 2) other installed packages from the GHNS system and 3) dependencies on the host system (e.g. some python library). Admittedly that makes the problem a bit tricky, but IMHO that is not a good reason to force the end user to solve it.\n\nToday the developer of a GHNS package, and the user, have to work around the fact that the system does not handle dependencies internally. One way this is done today (by developers) is by making monolithic bundles of parts that would actually be more logical to package separately (e.g the Lyriki-Lyrics family of scripts for Amarok). Still, there is quite a bit of guesswork required on the part of the user and there is no guarantee that an installed package will work.\n\nWhat plans are there to add such capabilities to the system? Could that make it already into the second generation system? My hunch is that rather than going for a home-grown partial solution, one might just as well rip the logic from a proven code base such as Debian's apt?\n\nThanks for a cool KDE feature!"
    author: "Martin"
  - subject: "Re: package management"
    date: 2007-04-01
    body: "Point 2 is probably the trickiest. But, yes, I'd love to see all 3 of those dependency checks included.\n"
    author: "Thomas Zander"
  - subject: "Re: package management"
    date: 2007-04-01
    body: "Well, you know, it's likely simply not going to be everything possible. If it's not self-contained, it won't fly, for starters."
    author: "Debian User"
  - subject: "Jonatan Riddle Vetoes KDE 4 April Fool's Prank "
    date: 2007-04-01
    body: "Why did you remove it? Huh? Weird..."
    author: "fish"
  - subject: "Why not something like KSTUFF etc..."
    date: 2007-04-03
    body: "Why not something like KSTUFF\nor KSOFT (like KDE-SOFTWARE) or\neven KAPT or KCSTUFF like KdeCoolStuff/KdeCoolSoft maby\nyou could say it KCS K-KDE C-Cool S-SOFT or S-STUFF\n\nAnything other than \"More Hot New Stuff\" \nJust an idea...\n\n"
    author: "KDE Power User"
  - subject: "Hot new stuff"
    date: 2007-04-03
    body: "For some applications it would be great if there was \"Boring Old Stuff\" packaged with the software. think of Ktouch. I makes a horrible impression to first time users when there are not example lessons for the software shipped with the program."
    author: "pod"
  - subject: "excellent"
    date: 2007-04-05
    body: "KDE 4 is becoming a complete platform for modern application developers. Very impressive."
    author: "Till Eulenspiegel"
  - subject: "incredible possibilities!"
    date: 2007-04-07
    body: "Great work! \nman, this offers really cool possibilities!\nImagine, how important this feature could become in KOffice for the end-user: he can obtain content right at the time when he needs it! Not only Kword-templates as mentioned (one of the biggest shortcoming in office-suits I know is the lack of good templates eg for busines-letters, etc.), but also cliparts, even fonts, spreadsheets solving certain issues etc. and maybe also whole KDE-desktop-themes!\nBut the whole thing should work upstream as well! It should be easy for users to share the contend they created. Maybe Karbon14 could have a menue-button labelled 'donate to Open-Clipart' which would result in an automatic upload of the present image and availibility over Kgetnewhotstuff. (I think low-quality-content could easily be eliminated using the rating-system)\nIn the same way, every application capable of creating distributable content could provide a simple way to share documents with one click (or wizzard guided...). 'share current desktop-theme' in KDE-settings, 'share current template' in kWord, and so on.\nThis would result in a huge collaborating comunity!\n "
    author: "Heiko Braun"
---
This week's LWN looks at <a href="http://lwn.net/Articles/227855">Get New Hot Stuff in KDE 4</a>.  Improvements currently being made by lead developer 
Josef Spillner include new options for uninstalling content, content synchronisation, the ability to rate content directly from the application interface, a dramatically faster interface and more.  Get Hot New Stuff is now a <a href="http://ghns.freedesktop.org">specification on freedesktop.org</a> and used throughout KDE in apps like Amarok and KOrganizer.


<!--break-->
