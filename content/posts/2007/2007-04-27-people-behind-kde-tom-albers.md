---
title: "People Behind KDE: Tom Albers"
date:    2007-04-27
authors:
  - "dallen"
slug:    people-behind-kde-tom-albers
comments:
  - subject: "Ever heard of friendly competition?"
    date: 2007-04-27
    body: "\"Currently I'm developing Mailody...I can't tell what things I have in mind, because there is competition with other mail clients, some of which can implement things much faster than we can\"\n\nwhat a lamer... "
    author: "Anonymous Coward"
  - subject: "Re: Ever heard of friendly competition?"
    date: 2007-04-27
    body: "Note that smiley afterwards.\n"
    author: "Christopher Schwan"
  - subject: "Re: Ever heard of friendly competition?"
    date: 2007-04-27
    body: "What a stupid post."
    author: "Borker"
  - subject: "Re: Ever heard of friendly competition?"
    date: 2007-04-27
    body: "... get a grip.. the one who's lame here is.... you\ndear, he was jokin' and you're bitching"
    author: "Thomas"
  - subject: "KMail"
    date: 2007-04-27
    body: "And the reason this effort is not better spent improving KMail is what exactly?\n\nI just hope this one is not from the Dolphin series: \"Improve usability until it's totally useless.\""
    author: "hmmm"
  - subject: "Re: KMail"
    date: 2007-04-27
    body: "Dolphin is the best that ever happened. It was like englightment.\n\nSometimes refactoring shows what really matters. It is like a dirty snowball and then someone comes and restores real beauty"
    author: "andre"
  - subject: "Re: KMail"
    date: 2007-04-27
    body: "This comparison it's a non sense. Mailody only implents IMAP, on the other hand Dolphn does not ony implement, say, file:// but every kio slave for files already supported by Konqueror."
    author: "Vide"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "For the record: the KMail and other KDEPIM developers applaud Tom's efforts with Mailody. It's important to rethink our applications occasionally and try out new approaches. Mailody uses many of our libraries, as Tom mentioned, which makes them better. Our aim is to share as much code as possible, but Mailody is much more focused than KMail and has very different constraints.\n\nTom has done a huge amount of work on and for KMail in the past. He was at one point the only person dealing with incoming bug reports and anyone who's ever seen KMail's bug database knows what a gargantuan task that is. Free Software should be fun. Tom has certainly earned the right to do whatever is fun for him having done more than his share of no-fun work for years. The fact that the KDEPIM community benefits from his fun is a pleasant side effect.\n\n(And I'm not saying that because Tom flattered me in his interview. I enjoyed our chat too, Tom, there aren't many people in the world who appreciate the subtleties of sorting emails into threads properly. ;) "
    author: "Till Adam"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "This is absolutely wonderful. But only as long as this experiment is not shoved down our throats as the new default mail program."
    author: "hmmm"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "what the hell is your problem!? such unhelpful posts like this may be exactly the point why certain people don't give a damn about what you think...."
    author: "ac"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "You know how to motivate people! Nice Work. I'll go back to my 'i don't do interviews'-mode as I've done the last years. You know it actually costs time to do this? "
    author: "Tom Albers"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "Ignore them, Tom - anyone who whinges about a default that can be changed in 10 seconds is not worth listening to.\n\nMany, many people appreciate your efforts, but the naysayers are always the most vocal."
    author: "Anon"
  - subject: "FWIW"
    date: 2007-04-29
    body: "Thank you. While I like Kmail, I agree that multiple apps actually help. It is refreshing to see new approaches and ideas coming.\n\nBut I will point out that I only use imap, so I may switch at some time.\n\nThanx again for the effort and the interview."
    author: "a.c."
  - subject: "Re: KMail"
    date: 2007-05-07
    body: "To outnumber the notorious idiots: Tom, Great Work :)\nJust don't care about these few comments, I think your work is really appreciated, and don't stop giving interviews because of that \n"
    author: "Frank"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "I bet if they just call it 'kmail', you would cheer about the new and improved functionality of the application."
    author: "whatever noticed"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "The reason is that kmail's design is broken, especially when you consider the way it uses threads (or rather, the way it doesn't use them). \nTake a look at this bug: http://bugs.kde.org/show_bug.cgi?id=41514"
    author: "Anonymous coward"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "Using threads to solve this kind of problems is utterly moronic. All you need to do is run things like spamassassin and gpg as external processes using KProcess, and that will send their output back to KMail as it becomes available, so there will be no freezing and no delay when the external program is done. Oh, and no threads. Threads are for idiots who can't code properly."
    author: "yaac"
  - subject: "Re: KMail"
    date: 2007-04-28
    body: "sure, reimplementing stuff the kernel does quite good (like scheduling parallel operations) is what the pros do....."
    author: "ac"
  - subject: "digiKam team has disapear ?"
    date: 2007-04-27
    body: "I don't understand exactly these words: \"After the digiKam development group fell apart...\"\n\nSound like Tom has works on digikam project in the past and he have leave the team. digikam is one of the most important application under Linux and this project is very active. There is a team inside this project...\n\n  "
    author: "Roberto Avilus"
  - subject: "Re: digiKam team has disapear ?"
    date: 2007-04-27
    body: "The team which was working on digikam at that moment fell apart, since then a new team has formed, with some people of the old team."
    author: "Tom Albers"
  - subject: "Re: digiKam team has disapear ?"
    date: 2007-04-29
    body: "Take a look at the digkam.org contact page, section \"Former Member\" and you see what Tom is talking about."
    author: "jokele"
  - subject: "Re: digiKam team has disapear ?"
    date: 2007-05-02
    body: "Well I was part of the digiKam team throughout the time when Tom and Joern were in the team. When Renchi left us, he was aptly replaced by Gilles immediately, I didn't have the notion of a 'team falling apart' at all. When Tom and Joern left even less so. digiKam was going strong at all times thanks to the incredible Gilles Caulier who doesn't get enough praise and mention at KDE.dot.NEWS. He is always in the top-five of the commit statistics.\n\nAnyways, digiKam merits more buzz and talk on KDE.dot. digiKam has been voted be the Linux magazine the 3rd most important application after FF and Amarok."
    author: "Gerhard"
  - subject: "Re: digiKam team has disapear ?"
    date: 2007-05-02
    body: "I can not disagree more. But this not the time nor the place to do so."
    author: "Tom Albers"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we travel to The Netherlands to talk to another developer of the KDE-PIM realm. Saving both your hands and your email frustrations - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/toma/">Tom Albers</a>.
<!--break-->
