---
title: "The Road to KDE 4: SVG Rendering in Applications"
date:    2007-01-02
authors:
  - "tunrau"
slug:    road-kde-4-svg-rendering-applications
comments:
  - subject: "Looks like the important Apps are ported first"
    date: 2007-01-02
    body: "<just fun>\nWhy browse and tag your Files when you can play a nice round mahjong :)\nIts already usable by secretaries now :) And they will be more productive !\n\nLets add more games :) kate/konqueror/okular/plasma , who needs this :>>\n</just fun>\n\nseriously : The ksysguard and mahjong look awsome , alt-f2 and atomic need much more work/love :)\n"
    author: "chri"
  - subject: "Re: Looks like the important Apps are ported first"
    date: 2007-01-02
    body: "Well, krunner (the new run dialog) is only a few weeks old... it works well enough, and we'll probably see it mature in the next few weeks (as it's really a very simple app anyway). \n\nAs for KAtomic, it'll get some love too... the SVG graphics are just preliminary at this point anyway.  Notice how there's still shadows under the arrows there... it's because the arrows are just stolen toolbar icons so far.\n\nThe beauty of SVG though is that it can be very easily changed, themed, etc. without having to do much work (IE: no recompilations to fix a pixel)."
    author: "Troy Unrau"
  - subject: "Re: Looks like the important Apps are ported first"
    date: 2007-01-02
    body: "yes, krunner is about 4-5 hours of work altogether so far including me sitting down and thinking about what i wanted it to do.. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Looks like the important Apps are ported first"
    date: 2007-01-03
    body: "i really liked the music when atomic came out on the Amiga. \nCan we perhaps somehow *ask* the copyright holders if we can have the music ?!?!?\n\nthat would be soo great"
    author: "chri"
  - subject: "Re: Looks like the important Apps are ported first"
    date: 2007-01-03
    body: "scratch your itch :-)"
    author: "pascal"
  - subject: "Re: Looks like the important Apps are ported first"
    date: 2007-02-07
    body: "I'm sure the KDE game devs would be very grateful if you would. Half the problem is getting in contact with the people authorized to make such a decision."
    author: "Stephan Sokolow"
  - subject: "Video Shots ?"
    date: 2007-01-02
    body: "as kde gets more and more animations - What about Video-Shots , instead of Screenshots? Would be a nice addition for KSnapshot !"
    author: "chri"
  - subject: "Re: Video Shots ?"
    date: 2007-01-03
    body: "Now, that's an _excellent_ idea. Talk about innovation!"
    author: "KubuntuUser"
  - subject: "Re: Video Shots ?"
    date: 2007-01-04
    body: "Try wink"
    author: "gerd"
  - subject: "Mahjong"
    date: 2007-01-02
    body: "Oh wow, the new KMahJongg looks almost as good as the Gnome version....\nI am excited about KDE 4 though!"
    author: "Caesar Tjalbo"
  - subject: "Re: Mahjong"
    date: 2007-01-03
    body: "From the looks of it, they took the svgs from the gnome version. Im sure it will be improved further later on. I hope they make the two compatible though, so the two apps can continue to borrow tile sets from each other."
    author: "drizek"
  - subject: "Re: Mahjong"
    date: 2007-01-03
    body: "Actually the SVG tilesets used by Kmahjongg (and shared with KShisen) in KDE4 are original, contributed by Raquel Ravanini and Robert Buchholz. Due to our needs (angle switching) we needed to compose tile backgrounds and faces at runtime and have all elements separated in the SVG file, so these were drawn specifically to match what our games needed."
    author: "Mauricio Piacentini"
  - subject: "Re: Mahjong"
    date: 2007-01-03
    body: "I admit that the current GNOME Mahjongg looks better than the current KDE 3.5.5 Mahjongg. However, the KDE 4 Mahjonng with the SVG graphics makes the GNOME version look cartoonish. Take some screen clips, and compare the three side by side.\n\nAfter seeing some of this stuff, I now believe that KDE 4 is going to be stunningly beautiful."
    author: "gfranken"
  - subject: "Re: Mahjong"
    date: 2007-01-03
    body: "nice thing is, the KDE version can change the orientation of the tiles with the g and h keys (if i'm not mistaken), and do that incredibly fast."
    author: "superstoned"
  - subject: "Looks good, but..."
    date: 2007-01-02
    body: "...the question is: Does it render quickly enough, not to flicker, when the system is under load and you're resizing the window (without keeping a (in \"advance\" filled) cache, eating a large amount of memory)? Or do the minimal GPU requirements match Vista? \n\nHopefully there's at least a compatibility mode for people who don't want exchange a fast working system for 'bells and whistles' or feel forced to buy new hardware (or to choose another desktop)...\n\n\nThe minicli above looks much better."
    author: "Carlo"
  - subject: "Re: Looks good, but..."
    date: 2007-01-03
    body: "SVG's can take a while to rerender when you resize the window.\n\nI was thinking for ksysguard to just resize the cached image, with a, say, 500ms timer to rerender the svg once the user stops resizing.  But I didn't think it was worth the effort.  If people do care, then I could perhaps do that.\n(At the moment it rerenders as you resize.  This means it feels jerky as you resize.)"
    author: "John Tapsell"
  - subject: "Re: Looks good, but..."
    date: 2007-01-03
    body: "That (at X, Y, Z size prerendered) bitmaps of the SVG's would be scaled down (at best on GPU level) was the premise I had in mind. Just that you quickly run into the problem of large cache sizes, which are a PITA. \n\nHopefully everyone is keeping this in mind (rather a lot of small elements that get reused and are already cached, than each application \"its\" fat svg costume; prerendering and caching, incl. storing the cache on disk: Nothing is more annoying than having slow starting applications, because of svg elements being rendered - large bitmap loading might be slower though; aggregating multiple svg's in one file avoids seek times,...)\n\nScalable graphics are nice at development time, but at runtime with users noticing artifacts and slowdowns in milliseconds, there's no way to go without very smart bitmap caching.\n\n\n>(At the moment it rerenders as you resize. This means it feels jerky as you resize.)\n\nAt the moment it doesn't matter. A 4.0 release with such a \"feature\" would be catastrophic."
    author: "Carlo"
  - subject: "Re: Looks good, but..."
    date: 2007-01-03
    body: "Remains the question, why Xorg doesn't care about application provided vector graphics - especially when you think about remote desktops. But that's way off topic, I suppose. :)"
    author: "Carlo"
  - subject: "Re: Looks good, but..."
    date: 2007-01-03
    body: "xorg just lacks good SVG rendering acceleration. we'll have to wait a (long) while for that... it would solve the speed problem, tough.\n\nbtw here it's definitely not jerky, but outright slow. it takes approx a sec to resize the window. but imho that's better than being jerky, tough, it takes some time but doesn't flicker or something."
    author: "superstoned"
  - subject: "Re: Looks good, but..."
    date: 2007-01-03
    body: "> btw here it's definitely not jerky, but outright slow. it takes approx a sec to resize the window. but imho that's better than being jerky, tough, it takes some time but doesn't flicker or something.\n\nWell, even if Qt 4 does draw off-screen, avoiding flickering, and isn't as retard...err...90's with regards to threading as Qt 3.x - any user interface \"stalling\" is nowadays outright unacceptable, would be a very bad user experience and bad PR for KDE 4. Such \"little\" things count more, than a long feature list."
    author: "Carlo"
  - subject: "Re: Looks good, but..."
    date: 2007-01-03
    body: "> SVG's can take a while to rerender when you resize the window.\n\nThat would totally s**k.\n\nI have a similar problem with Beryl, I'm back to outline resizing because Beryl is too slow with resizes.\n\nWould it be possible to precompile the SVG into something more efficient? This could be done on the fly when a SVG is changed.\n\nMicrosoft seams to be something something like that with their XAML format (see http://en.wikipedia.org/wiki/Extensible_Application_Markup_Language). They compile it into BAML before it's used as resource. Like SVG, their XAML files can be created by a designer and used 1-on-1 by developers as application interface."
    author: "Diederik"
  - subject: "Re: Looks good, but..."
    date: 2007-01-03
    body: "Parsing the XML is not the problem with resizing.  I presume that Qt is sensible enough to only do that when the SVG is first loaded (although processing large quantities of XML is expensive to do and can adversely affect startup times which is presumably why Microsoft compiles XAML into another binary format).\n\nThe expensive part when resizing (I think) is drawing the complex polygons which were described by the SVG file and then filling them with all manner of gradients and transparencies.\n\nQt 4.3 includes some hefty improvements for rendering complex polygons, including making better use of OpenGL to cut down some of the work considerably.  Zack Rusin's blog provides more details.  \n"
    author: "Robert Knight"
  - subject: "Re: Looks good, but..."
    date: 2007-01-04
    body: "Qt reads in the xml and makes a kind of dom tree. \n"
    author: "John Tapsell"
  - subject: "Re: Looks good, but..."
    date: 2007-01-04
    body: "A strategy I used in scaling images like that before was using a bitmap of, say, max 200 pixels wide and scaling that up to give instantaneous feedback while scaling. A background thread would then render the svg in its full resolution and when its done it will be shown on screen.\nThis gives direct results and smooth rescaling.  And while things move the human eye can't discern details anyway so if it takes a maximum of 200msec before the final svg is finally rendered you would totally NOT notice. If it took half a second you'd just see some updating, but you'd have to look really good to see what it was.\n\nLets see if we can get that into kdelibs somehow ;)\n\n"
    author: "Thomas Zander"
  - subject: "Re: Looks good, but..."
    date: 2007-01-03
    body: "You want to know, but talk like you already know the answer (of course the answer is negative).\nAlso, next time someone complains about \"bells and whistles\", they should be sent straight back to the command line. Or install Windows 95 if they get that running.\nSorry, technical Darwinism is cruel."
    author: "Phase II"
  - subject: "Re: Looks good, but..."
    date: 2007-01-04
    body: "Are you serious?"
    author: "Kirk"
  - subject: "krunner and options"
    date: 2007-01-02
    body: "I'm not convinced krunner really needs any options. The current run command dialog has:\n(i) run as a different user;\n(ii) run as root.\n(iii) run in terminal\n(iv) run with different priority\n(v) run with realtime scheduling\n\n(i) is practically never done, and (ii) should never have to be. Any app that requires root privs should launch with them. (iii) obviously doesn't need to be there; if you want it to run in the terminal, then run it in one. (iv) and (v) are [rarely-used] power-user features and I don't think they should be there."
    author: "apokryphos"
  - subject: "\"should\""
    date: 2007-01-02
    body: "You use the word \"should\" three times. Developers: Please don't remove useful functionality based on how things \"should\" be!\n\nShould not the \"Cancel\" button be removed? If the user does not intend to run a program, she should not press Alt-F2 in the first place.\n\nThat said, you do have a point that these options are probably rarely used, but that is IMHO already taken care of by hiding them under the \"Options\" button."
    author: "crc"
  - subject: "\"should\" additional options go away? NO!!"
    date: 2007-01-03
    body: "I'm using each of the enumerated options no more than 4-5 times a year. I'm using [alt]+[f2] about 20 times a day *without* any options. That means, 99.7% of occurrences when ${me} is using [alt]+[f2] I'm happy with the most simplistic \"dialog\" you can ever imagine (I don't even notice the \"Options >>\", \"Run\" and \"Cancel\" buttons then -- I just hit [enter]... )\n\nDo I therefore want the listed features go away? No, most certainly NO!, 'cause I still need and *use* one of them once a month or so....\n\nWould I strongly oppose any move to scrap the \"Options >>\" button (or an obvious way to invoke a more feature-rich krunner) altogether? Hell, YES!, you can bet on that for sure.\n\nThose new SVG capabilities do open a whole new universe of implementing lots of eyecandy as well as new usability features. (Though I expect no-one, developers or artists, to be able to find the correct balance in his first approach playing with his new SVG tools... be prepared to see also quite some creations that you do only like a week or a month and then be saturated...) I think this is a premier opportunity for KDE to draw a whole new layer of artists into its ranks, who will be excited to see what huge play- and creation-ground these things do offer to them.\n"
    author: "confused reader of news"
  - subject: "Re: \"should\" additional options go away? NO!!"
    date: 2007-01-03
    body: "Indeed, as we are KDE and not GNOME\n\nI like power user options to be accessible, not to be completely hidden from the (not so powerful) user.\n"
    author: "otherAC"
  - subject: "Re: \"should\" additional options go away? NO!!"
    date: 2007-01-03
    body: "Forgot to mention that I *did* notice the (very unobtrusive) \"Options\" link (not a button) in the screenshot of the new krunner above, and I'm happy to see it is still there.\n\nI just did want to let a voice be heared that opposes these \"fascist GUI purgers\" (Linus Torvalds' expression) who do want to dictate users what they ought to have and what they ought to forego.\n\nOf course, I'm not opposed to cleanse \"cluttered\" and \"bloated\" UI elements from the default views -- but please let me continue to get easy access to all of them should I so desire."
    author: "confused reader of news"
  - subject: "Re: \"should\""
    date: 2007-01-03
    body: "That's a bit of a ridiculous argument. \"Should\" is simply an imperative, and even if you don't think or know it, it's used ALL the time. Should all our window decorations be red? No, they shouldn't. \n\nDon't get me wrong, I absolutely adore the configurability and extensibility of KDE, and I in no way want that to change. And re-organising things is certainly not that. And none of what you've said addresses the point.\n\nYou should *never* have to use the \"run as root\" option, and in fact that's even dangerous to new users. If the \"run as new user\" is actually being used then we should find a way to incorporate that, but there's no reason for why it should be in the very basic \"run application\" dialogue."
    author: "apokryphos"
  - subject: "Re: \"should\""
    date: 2007-01-03
    body: "\"You should *never* have to use the \"run as root\" option, and in fact that's even dangerous to new users. If the \"run as new user\" is actually being used then we should find a way to incorporate that, but there's no reason for why it should be in the very basic \"run application\" dialogue.\"\n\n\nNo. You simply don't see a reason to use it, while I can imagine a lot of occasions in which I want to run application X sometimes as root, sometimes not, and *I* want to decide it (not the application). \n\nWho are you to decide what I should have and don't have to use?\n"
    author: "devicerandom"
  - subject: "Re: \"should\""
    date: 2007-01-03
    body: "+1\n\nI hate \"facist GUI purgers\" too.\nI hate people that dictate how I should use my computer and what I should do with it. I want to decide myself!\nI hate GUIs that requires a registry editor to access all features because someone decided for me. (Gnome / Windows)\n\npurging GUIs is as stupid as swapping Ok/Cancel buttons to differenciate from windows forgetting in the meantime that some users may required to run both windows and gnome and thus leading to many mistakes.\n\nThis to illustrate that when the programmer decides for the end-user he can never think to the whole impact of his decision and thus, this always lead to reduced ergonomy.\n\nEmacs succeeded because it was flexible and feature rich. Think about that when removing features.\n\n"
    author: "Olivier."
  - subject: "Re: \"should\""
    date: 2007-01-04
    body: "-1\n\nBy all means, no one dictates how you should use Linux. That's freedom for you ;-). If you want to decide for *yourself* then write your own software. \n\n\"I hate GUIs that requires a registry editor...\"\nLots of KDE functionality is hidden in rc files. Are you going to hate KDE for that too?\n\n\"as stupid as swapping Ok/Cancel\"\nI think you'll find that swapping the OK/Cancel buttons wasn't done to be different from Windows, so stop with the outlandish, uninformed claims.\n\nAs for \"when the programmer decides for the end-user\" -- the programmer is always deciding for the end user when they write their program. Simple as. You have to draw the line somewhere.\nI'd argue that the original request of removing those arcane options is warranted. If you want a more powerful way of executing applications you're most probably a power user that has a shell open and can do that for yourself. If not, a replacement could be optional. \nWith that said, I think the new options is unobtrusive enough to not matter. I definitely agree that the current dialog needs some TLC, and the new one looks sweet."
    author: "Cerulean"
  - subject: "Re: \"should\""
    date: 2007-01-04
    body: ">If you want a more powerful way of executing applications you're most probably a power user \n\n\"GUI fascists\" see users black and white , i. e. either completely stupid and scared or so advanced as to work with registry/command line. But you excluded! a whole lot of users who are not at all afraid about extra 30 options in configuration dialogs, but either cannot edit registries/cmdline or have no time to find out what/where/how to do it.\n\nAs for swapping the OK/Cancel buttons - nobody thought that you saved little mouse  movement, but overloaded my user brain with thinking much more about what button to press, as users you know have habits, and you broke them. It seems to me that some stupid manager/designer decided this, and other people don't see, that king is naked..\n"
    author: "Sergey"
  - subject: "Re: \"should\""
    date: 2007-01-04
    body: "well, it is really a lot easier to press ok in GNOME, when one gets acustomed to it. \nThe problem is, if you need to ask, defaylt should be Cancel, not OK :)"
    author: "pilpilon"
  - subject: "Re: \"should\""
    date: 2007-01-05
    body: "people may switch back to Windows long before the time will come,\nthey would become accustomed to reversed buttons.\nI think Gnome is optimized only for corporate users, home users are ignored."
    author: "Sergey"
  - subject: "Re: \"should\""
    date: 2007-01-05
    body: "It is hilarious how commenters accuse people of \"GUI fascists\" while at the same time making incredibly harsh generalisations themselves.  \n\nThere are tradeoffs.  Sometimes less is more and sometimes less is just less.  Gnome doesn't always get things right but no one ever claimed KDE was perfect.  \nGnome is aiming for a different audience, and they make it much harder to get lost or to shoot yourself in the foot but the downside is things are far less configurable at first.  KDE on the other hand makes many things configurable, probably more than most users need but with time it should become clearer which options can be streamlined or better presented at the right time and place.  Both \n\nGnome and KDE will continue to improve and bounce in and out from the happy medium.  \n\nI wont go into the button order thing but it isn't an arbitrary decision and Apple did it long before Gnome.  QT 4.2 makes it even easier for developers to integrate applications with whichever button order users prefer, hopefully GTK will also improve the work they have done so far to allow alternative button ordering.  "
    author: "Lessismoreisless"
  - subject: "Re: \"should\""
    date: 2007-01-07
    body: "I think the best way to address mentioned tradeoffs is to make in applications menu two choices \"Options\" and \"Advanced Options\"(which would have \"set all to default\"  button). Clueless users won't go to \"Advanced options\" . Those who shooted themselves in the foot will just press \"set all default\" in \"advanced options\".\n\n"
    author: "Sergey"
  - subject: "Re: \"should\""
    date: 2007-01-05
    body: "\"Emacs succeeded because it was flexible and feature rich.\"\n\nYes and no.  Read about Emacs\nhttp://www.jwz.org/doc/lemacs.html\n\nEmacs was forked and replaced by the more user friendly X-Emacs which succeeded because it was more user friendly and gave more people what they wanted (although you could chose to interpret that user friendliness as \"more features\" if you wanted).  \n\n:P\n\nDont underestimate the power of broader appeal and I think the parent post was pointing at that with better defaults and asking the right questions in the right places for less options would be needed.  If the user was prompted for root passwords when needed then wouldn't it be possible to combine both run as root and run as other user?  \n\n"
    author: "Lessismoreisless"
  - subject: "Re: \"should\""
    date: 2007-01-04
    body: "All very nice and vague until you back up an actual use-case. Which X application do you need to run as root; and, do you know of a user that doesn't know about kdesu who runs these applications as root?\n\nIf there's a case, then tell me; I'd genuinely like to know. I actually started off just wondering if we really do need them. See my first post; I only said that I'm not yet convinced. I think the argument raised here that running an application as a different user (with the example of kmail) is a valid use-case, and perhaps makes the option quite worthy. \n\n> Who are you to decide what I should have and don't have to use?\n\nI'm not a KDE developer but, believe it or not, users can still have opinions on how an application should function. There's nothing controversial in this; see bugs.kde.org -- they're all cases of users proposing that an application doesn't function in the appropriate way. Do you think that's unreasonable?"
    author: "apokryphos"
  - subject: "Re: \"should\""
    date: 2007-01-03
    body: "There are cases where the application does not have the ability to prompt the user for a root password.\nIs that the fault of the program? Should it be fixed? Is it really the user's fault that the programmers didn't implement this?\nAnswer: No, no and no.\n\nJust because a user want to run a program as root doesn't mean the program always should run in root. There for its not a good idea for the program to automatically prompt the user.\nLets say I wanted to edit a text file. I don't want to open the file using kate or kwrite, as with a different user as it could take longer to open and if they are not configured it could create a backup file which I don't want. I want to use a console based text editor, and the file I need to edit isn't owned by me.\nHow do I do this?\nwithout the options button I'm forced to start konsole, run su, run nano /path/to/file\nwith the options button? I type in: nano /path/to/file\ncheck the run as different user\ncheck run in terminal window\n\nHow often does one have to do this, perhaps not too often. But that doesn't mean they shouldn't have that ability. having that ability doesn't clutter up anything nor does it make it run slower or not do a good job at what it does."
    author: "Stephen"
  - subject: "Re: \"should\""
    date: 2007-01-04
    body: "> There are cases where the application does not have the ability to prompt the user for a root password.\n> Is that the fault of the program? Should it be fixed? Is it really the user's fault that the programmers didn't implement this?\n\nVery wrong, in fact. It's the distribution's fault if it ships with an application that requires root privs and yet still doesn't open with kdesu or gksu when being launched.\n\n> Lets say I wanted to edit a text file. I don't want to open the file using kate or kwrite, as with a different user as it could take longer to open and if they are not configured it could create a backup file which I don't want. I want to use a console based text editor, and the file I need to edit isn't owned by me.\n> How do I do this?\n> without the options button I'm forced to start konsole, run su, run nano /path/to/file\n\nAgain, wrong. Right-click -> edit as root is a very valid option. See my other post for why I think this is the method that a new user would choose."
    author: "apokryphos"
  - subject: "Re: \"should\""
    date: 2007-01-04
    body: "But in that case, the RMB gets duplicated entries for each app able to edit that kind of file. That causes uglyness. "
    author: "Roberto Alsina"
  - subject: "Re: \"should\""
    date: 2007-01-04
    body: "Exactly, good point.\n\nAnd not to mention you have to open Konqueror, navigate to /path/to/ and then RMB on the file. Which is rather different to the whole concept of using alt+f2 in the first place.\n"
    author: "Morty"
  - subject: "Re: \"should\""
    date: 2007-01-29
    body: "So you check if the user can write to the file.\nIf they can, you only need 'edit'\nIf they can't, you only need 'edit as root'\n\n(and you could then name just them both 'edit')"
    author: "david"
  - subject: "Re: \"should\""
    date: 2007-01-04
    body: ">Very wrong, in fact. It's the distribution's fault if it ships with an \n>application that requires root privs and yet still doesn't open with kdesu or \n>gksu when being launched.\n\nNo, it's you thats wrong. This is not a thing for the distribution or developers to decide, it's for the user to decide if root privs is needed when running the application. You use it for applications you sometimes run as root, but mostly don't. The deleopers or distributions have no way to decide if the user want's it with kdsu or not. \n\nIn a menu they can do it, like with kfm(Konqi in filamnager mode) where you have one entry for normal and one for starting it as root. But this does not fit in the concept and use of alt+F2. The whole point is to *not* use a menu and *not* right clik on files etc.  \n\n"
    author: "Morty"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: ">>I'm not convinced krunner really needs any options.\n\nThen stay away from the [options] button in krunner :o)\n\n(i) i use that to start the e-mail application of my SO\n(ii) i use that one to start an application that i otherwise could use as user as well, but want to run it as root for now, or for non-kde-applications that need to be run as root but do not provide options to become root\n(iii) nice for people who need to run something in a terminal but don't know how to start one (as linux becomes more popular, more and more users have no idea what a terminal, commandline, shell, etc is..)\n(iv) and (v): why hide power user options? The whole thing is hidden for the average user anyway."
    author: "otherAC"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: "(i) granted, but this doesn't suggest that the application couldn't be perhaps put elsewhere. I'm not sure where yet myself. A strong point against my argument, sure. But to address a few of the others:\n\n(ii) please give me an example of the said application. \n(iii) really doesn't work at all. Partly because I've never seen someone struggle to find a terminal, but also of course because it would be plain silly to get a new user to familiarise themselves with using the run dialogue to always start up a terminal. If the terminal's hidden (which it isn't at all), then we should make it more visible and accessible, not create a duplication of effort around when there's no clear need at all."
    author: "apokryphos"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: ">(ii) please give me an example of the said application. \n\nThe most obvious, KWrite. Very handy if you wan't to quikly edit a configuration file. The use cases for this functionality are extreamly numourus. \n\nI'll even give you one for (ii) and (iii) at the same time, it's a nice way to start Minicom (if you run Suse and don't change the default access rights).\n"
    author: "Morty"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: "With regard to kwrite, I don't see why you shouldn't be able to just right-click -> edit as root, which is what a normal user would do. They *wouldn't* alt+f2 -> kwrite /etc/some/file and select \"run as different user\". The users that know about using paths that confidently (and know about editing system-wide config files) know about kdesu."
    author: "apokryphos"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: "The option is for real normal users, not your \"normal\". The way you talk it seems like you actually mean simple or novice users. Normal users are(or very soon gets to) a level above novice, but far from expert user. This option is for those users, they will have a easy way to run as root without knowing about kdesu. Which is rather more obscure and harder to learn, while cliking the options button is not. Your \"normal\" would not know about running as root or alt+F2 anyway.  \n\nIn normal use this option is not needed, that's the whole reaseon it's hidden too. That's usability btw, it's only there when you need it. It does not degrade speed or create confusion. But *when* you need the functionality it's easily accesable, even for non-expert users(For instance a ordinary user having run his home PC on Linux a year or two, but not a sysadmin wizzard).\n\nAnd personaly I find using the option both easier and more logicall than: alt+F2 kdsu kwrite /etc/somefile\n\n"
    author: "Morty"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: ">>With regard to kwrite, I don't see why you shouldn't be able to just right-click -> edit as root,\n\nIf I right click, i don't see an option 'edit as root'\n\nAnd if i want to create a new file as root, that option would not help at all to gain root privileges.\n\nFurthermore, every action on a computer system can be done in several different ways. (like with pasting text, you can use the middele mouse button, the context menu of the right mouse button, the past icon in the toolbar or the menu option [edit -> paste]), so why should we want to strip a (hidden) option from krunner, just because there are other ways to perform the desired action??\n\n"
    author: "otherAC"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: "2.) Run as root? kwrite /etc/apt/sources.list or \n/etc/init.d/whatever restart\n\nThe point is, my boy, you are telling ME how I am supposed to run my shop? Fart on that. You have no clue how most people  use computers, so don't say \"should\"."
    author: "Joe"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: "Er, yeah, you really run that in alt+f2? Good luck, since it won't give you *any* output. Great use that is. If it errors out, you won't know, if it succeeds, you again won't know. I'm quite sure you run your /etc/init.d/ scripts from the terminal.\n\n> You have no clue how most people use computers, so don't say \"should\".\n\nEvidently. I \"have no clue\" that alt+f2 gives you absolutely no output, unless you select the terminal option as well (which I already gave an argument for not having)."
    author: "apokryphos"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: "Check the \"run in console\" option in Alt+F2"
    author: "Roberto Alsina"
  - subject: "Re: Yeah, scrap it all!"
    date: 2007-01-03
    body: "I like how you can talk stuff away. \"Practically never\", \"should never have to be\", \"obviously doesn't need to be\" or simply \"don't think they should be\"... I'm totally hooked. Next time I'm sitting in front of some longer TODO I try to apply your relieving attitude."
    author: "Phase II"
  - subject: "Options are good!"
    date: 2007-01-03
    body: "The ability to run applications as a different user is really convenient for me! I have a personal (or \"home\") account and a professional (or \"work\") account on my laptop. I am not going to mix them! And, sometimes, I need to run a graphical application from the other world... For instance KMail, Korganizer or Kaddressbook, for obvious reasons. With Krunner, I save time.\n\nThanks for the application!"
    author: "Hobbes"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: "Actually I agree with you. Only problem is that KDE is all about choices and the extra options are not even really visible unless you look for them. But maybe you are right, maybe nobody are using this functionality.\n\nOn another note: The built-in calculater in krunner ROCKS! I simply love it. It's so fast and handy when I need it."
    author: "pascal"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: "Well, If I had refreshed the page I would have seen that lots of people are using it. And it seems like a cool feature to be able to open kmail as another user quickly."
    author: "pascal"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: "I agree, this should be a \"tip of the day\", till now my girlfriend and I used start new desktop, but for a quick email check, this will be very helpfull, thanks"
    author: "ac"
  - subject: "Re: krunner and options"
    date: 2007-01-05
    body: "being able to switch to another user account more quickly would help too though..."
    author: "Lessismoreisless"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: "(i) Is very useful if you have more than one non-root account and don't like to switch desktop sessions\n(ii) Maybe programs should ask for a root password, but kde cannot know if all apps really do this. The option doesn't hurt,\n(iii) Very useful for me, as it is better as starting an extra console. Useful for debugging.\n(iv) Also useful for people don't understanding how to use nice/renice\n(v) Some users might need this, I not, but it doesn't hurt.\n\nMost of the time, having more options is better, at least for me. I really like desktops/applications where options are hidden for normal users how cannot understand and use them, but are easily available for me. I don't see a problem in having an \"Options\" or \"Advanced\" button. It gives users the choice !\n\nFelix"
    author: "Felix"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: "I agree with a lot of your points that perhaps these options don't hurt. It is true that in krunner they are less obtrusive. \n\nNo harm in questioning if each one is needed though, too."
    author: "apokryphos"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: "True, but I guess that as long as the features are not in the way, and they are usefull for *someone*, it would be a good idea to keep the feature."
    author: "Andre"
  - subject: "Re: krunner and options"
    date: 2007-01-03
    body: "Excuse me, but I use (i) all the time.  One of the best things about it is that it seems to handle all the details of the X connection (i.e., authorization).  The only problem I have with it is that tab is disabled from selecting the options box after you use the drop down box that pops up when you start typing.\n\nPersonally, I think you could use reading some of the criticism of over simplification.\n\n- While it is true that users only uses about 10% of the features of an app, the 10% differs enough for each user that their union covers pretty everything present.  Taking the intersection of all the feature sets used by all users and remove everything else just winds up leaving everyone disappointed.\n\n- Being a power user is forever; being a novice user is just a passing phase on the road to being a power user.  Killing the features that those that actually use your apps more than just in a passing fancy use is not cool at all.\n\n- Usability is not about killing features.  Usability is about organizing features.\n\nIn short, please stay away from my apps!  : )"
    author: "Tyson"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: "Please please ... the dialogue for KRunner is good as it is. \n\nThe remarks that one \"should never\" run or have to run application xyz as root mean precisely nothing to me. I have a few applications that need root access. And as long as Krunner asks for the root password the responsability is where it belongs ... with whoever gave out the root password. In my case that would be me. I don't think that developers should interfere in this matter.\n\nAnd another thing. I notice that the example application shows signs of gratuitous styling of the worst kind. Whereas the look and feel of KRunner for KDE 3 is neat and functional (as if it was designed for me to get something done without fuss instead of as a programmer's show-off), the picture of the same application under KDE4 has this totally superfluous picture of a gear, much wasted space, obtrusive colouring, an awkwardly small business area, and a stealth button for options. Promise me one thing please ... if you are going to implement this to show off your theming powess, do provide a theme that brings back the KDE3 look and feel ... and make it default so that I never have to look at such styling monstrosities again please. I mean it. I thank and respect you as a developer, but I _really_ don't want you around as a stylist.\n\nThe KDE3 look currently resembles that of Windows, and that is a good thing. Because ... for better or worse, that plain functional WIndows look has been tested and vetted by a lot of ergonomics experts. And what they have done is to remove clutter and to make / keep the look and feel functional. Besides it reminds people of Windows and makes them feel a bit more at ease with Linux. So on behalf of the my users (and those I'm trying to convince to become Linux users) no fancy funky theming please ... and keep the look-and-feel as Windows-like as you can. \n\nThey've gotten used to it, and they *hate* it when developers change the layout of basic applications just to indulge in some random restyling. And I have to say that I sympathise.\n\nSo please ... make it scalable if you must, make it themable if you want, but please please don't touch it's looks, and keep all the options in. "
    author: "Golodh"
  - subject: "Re: krunner and options"
    date: 2007-01-04
    body: ">> the picture of the same application under KDE4 has this totally superfluous picture of a gear, much wasted space, obtrusive colouring, an awkwardly small business area, and a stealth button for options.\n\nas stated before, the backgrounds used in the screenshots are for development purposes only, just to show what's possible with svg.\nThey will be replaced with something more sensible "
    author: "otherAC"
  - subject: "Re: krunner and options"
    date: 2007-01-05
    body: "These are indeed seldom-used features. This is exactly why they are hidden until you press the Options button. If you find too many options annoying or confusing, you can just try not to press Options. I personally like those options and use at least three of them from time to time, and while I can do any of them by opening a terminal emulator, I find it more convenient to start apps or run commands that way. Maybe krunner doesn't *need* options, but why shouldn't it have them anyway? We don't actaully *need* to be able to change the desktop background.\n\nThis is part of why I prefer KDE to Gnome. I refer you to the stuff Linus said about Gnome's print dialog. Basically, potentially useful options were removed, to avoid causing confusion. I couldn't see why they didn't just do exactly what krunner does: hide the advanced options till you ask for them."
    author: "Ben Morris"
  - subject: "Thanks!"
    date: 2007-01-02
    body: "That was a neat little article - looking forward to the next!\n\nHow much will actually be rendered with SVG? yeah, I know that is probably unknown, but what is the goal then? :)"
    author: "Joergen Ramskov"
  - subject: "Re: Thanks!"
    date: 2007-01-03
    body: "As much as possible ;)\n\nMore seriously, i'd like to have (and i plan to do) all kdegame/kdeedu in SVG with an oxygen style, and several other things (finish ksysguard, krunner, umbrello...). But it will be hard to make them all for kde4.0 i fear, and there are still missing maintainer for some games."
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: Thanks!"
    date: 2007-01-04
    body: "Thanks for the answer. I know that KDE 4.0 is just the start, just look at how KDE 3 has evolved over time :) I think the same will happen with KDE 4.0 - it will be an amazingly good base to build from!\n\n"
    author: "Joergen Ramskov"
  - subject: "Excited"
    date: 2007-01-03
    body: "Of these four, I think KMahjongg has improved the most and looks most mature.\n\nKsysguard looks nice, but I really dislike the background. \nKAtomic needs, as somebody already said, some work (and love). I think it is the black blocks (walls) that look a little bit.. too much?\nAnd Krunner, this one was new to me. Looks pretty cool, kind of reminds me of Katapult (which is great). I wonder if it works the similar way?\n\nWell, to sum everything up, it looks promising. Yes, there are many things to be improved, and I look forward to see the result."
    author: "Lans"
  - subject: "Re: Excited"
    date: 2007-01-03
    body: ">> Ksysguard looks nice, but I really dislike the background. \n\nAs stated in replies at other dot articles: the background are for testing purposes only, the final background has not been made/released yet.\n\n>> And Krunner, this one was new to me. Looks pretty cool, kind of reminds me of Katapult (which is great). I wonder if it works the similar way?\n\nNever seen krunner?\nTry pressing [Alt][F2] in KDE (works since kde 2 or earlier)to start it."
    author: "otherAC"
  - subject: "Re: Excited"
    date: 2007-01-03
    body: ">> Never seen krunner?\n\nAh, I meant I haven't seen the new Krunner.Or whatever it is called now."
    author: "Lans"
  - subject: "Re: Excited"
    date: 2007-01-03
    body: "there is no new krunner"
    author: "otherAC"
  - subject: "Re: Excited"
    date: 2007-01-03
    body: "Yes there is...\n\nThe old run dialog is part of kdesktop, but with plasma in the works, it needed to go the way of the dodo.  So krunner, a new application will take over the run dialog (and, I think, screen locking once it's implemented).  It is 100% new.\n\nEverything else in the article is a port of KDE's existing apps to use the new architecture."
    author: "Troy Unrau"
  - subject: "Re: Excited"
    date: 2007-01-03
    body: "True, but the screenshot in this article is just krunner with a different skin, not a successor with different skills."
    author: "otherAC"
  - subject: "Re: Excited"
    date: 2007-01-03
    body: "what is everyone smoking in this thread? =)\n\nthere is no krunner in kde3 (or 2 for that matter).\nthe screenshot is of a completely new application that shares zero lines of code with the old implementation right now (though that will change in a bit as i'll be bringing over some of the options implementations for command execution).\nthe new skills of this successor include being able to have multiple consumers of the entered text each of which can offer possible matches (think: search). it will also include such things as access to servicemenus (of konqi's action context menu fame). and it already does things kdesktop fails to do such as restart reliably in the case of a crash =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Excited"
    date: 2007-01-03
    body: "Will there be \"plugins\" that add support to or example Calculator (this one is currently already in alt+F2), add songs to Amarok, chat with someone in Kopete etc.? Kind of like the current Katapult. :)"
    author: "Lans"
  - subject: "Re: Excited"
    date: 2007-01-03
    body: "Guess that's what he meant with the \"consumers\"."
    author: "Michael"
  - subject: "KMahjongg"
    date: 2007-01-03
    body: "Yeah, those SVGs look hella nice, but to fair you should compare them with the \"Traditional\" tileset in KMahjongg (with the \"Wood\" background, naturally). If I had my druthers, I would have made it the default years ago. It was hard enough getting the damn thing accepted in the first place, though. :-)\n\nI guess it's time to start working on an SVG version now... :-P"
    author: "The guy who made Traditional"
  - subject: "Re: KMahjongg"
    date: 2007-01-03
    body: "If you think the default tileset is not so good, there's already a \"classic\" SVG tileset done. You can see it here : \nhttp://maupiacentini.blogspot.com/2006/12/shisen-sho.html\nIt looks more like your \"Traditional\" one. And I think you can quite easily to edit this \"classic\" tileset to make it more \"Traditional\"-like. Good luck :)"
    author: "Shamaz"
  - subject: "Re: KMahjongg"
    date: 2007-01-03
    body: "There are some similarities but it looks pretty different from \"Traditional.\" I'll have to take a closer look at those tiles. ;-)\n\nAnd yes, \"Default\" is ugly, ugly, ugly. That's the whole reason I went to the trouble to create \"Traditional\" and have it included in the base package--I thought KMahjongg deserved to have a little eye candy. ;-)"
    author: "The guy who made Traditional"
  - subject: "Re: KMahjongg"
    date: 2007-01-03
    body: "Waw!!! Never really looked there...\nAnd you're right of course: Traditional tileset with Wood background (and Tile Shadow) already make KMahjongg look a LOT better. Why was this never made the default setting? Looks like a no brainer to me!! Thanks for your contribution."
    author: "UglyMike"
  - subject: "Re: KMahjongg"
    date: 2007-01-03
    body: "....too bad Shisen-Sho doesn't offer the same customizations."
    author: "UglyMike"
  - subject: "Re: KMahjongg"
    date: 2007-01-04
    body: "Well, actually it does now, in the KDE4 trunk. It shares the tileset rendering code and art with KMahjongg, so you will be able to use the same SVG tiles and backgrounds.\n"
    author: "Mauricio Piacentini"
  - subject: "Looks exciting"
    date: 2007-01-03
    body: "But what about the performance of especially ksysguard (e.g. when the system is already swapping and one needs to kill a process)?\n\nHowever, it looks really great.\n"
    author: "MM"
  - subject: "Re: Looks exciting"
    date: 2007-01-03
    body: "you can start the version of ksysguard without the graphs, with the commmand 'kpm' or the keystroke ctrl esc"
    author: "otherAC"
  - subject: "Re: Looks exciting"
    date: 2007-01-03
    body: "Thx"
    author: "MM"
  - subject: "Re: Looks exciting"
    date: 2007-01-03
    body: "When you are looking the process list (which is what shows first when you launch ksysguard), then the hidden graphs require (practically) no cpu etc (they log a bit of data but that's it).  In particular, the svg's etc are rendered until viewed.\n\nI have spent a large amount of effort to try to ensure that ksysguard is usuable even when the machine is going crazy.  It's not perfect and there's a long way to go, but it's not so bad as it is."
    author: "JohnFlux"
  - subject: "Re: Looks exciting"
    date: 2007-01-03
    body: "Thanks for your reply. But I am not so much concerned about the graphics themselves, but about loading, linking and initializing of the appropriate libraries used to display them. Are they loaded dynamically only when they are really needed? \n\nHowever, good work.\n"
    author: "MM"
  - subject: "Re: Looks exciting"
    date: 2007-01-03
    body: "Yeah, it means it has to link against QSvg etc..  but all those libraries should be already loaded etc so I don't konw if it's a big deal"
    author: "John Tapsell"
  - subject: "Re: Looks exciting"
    date: 2007-01-03
    body: "I'm not sure how linking really works on Linux. Is it correct to say that one special dynamic library is loaded once for all processes on that machine (or at least for that user on that machine) (prefetching etc.)?\n\nIf so, IMHO it may depend on whether the libraries are swapped out (to disk) or not, and the more is going on on a machine, it becomes more likely that they have to be loaded from swap space to RAM...\n\nIf it is statically linked it seems clear to me that it has to be loaded again.\n\nMaybe there is a chance to load the libs dynamically when they are necessary/used only, so that for startup and display of the process list, the libs aren't loaded (and maybe in a further step to not load them at all, e.g. when the machine is already slow, and to use \"traditional\" display (without svg) instead)?\n"
    author: "MM"
  - subject: "Re: Looks exciting"
    date: 2007-01-03
    body: "I also hope that this will be possible, but personally, I prefer a console (not konsole ;)) in such a case, e.g. xterm or Ctrl+Alt+F1"
    author: "Felix"
  - subject: "Re: Looks exciting"
    date: 2007-01-03
    body: "Yeah, the problem with Ctrl+Alt+F1 is that one has to login first. Konsole is okay too if it is already running, IMHO. "
    author: "MM"
  - subject: "Really like the use of SVG!"
    date: 2007-01-03
    body: "Really like the use of SVG!\n\nAbout the new System Guard, the graphs now have titles in\nthe graph itself as well as above it. Also the 'Load Average' title\nis missing its first letter. I know this is probably a minor issue\nbut I see this kind of annoyance regularly in graphical user interfaces,\nKDE is no exception.\n\nWhat would also be nice for the System Guard IMHO would be an x-axis\nscale, so you get an impression of how long the history is you are\nlooking at."
    author: "BJ"
  - subject: "Re: Really like the use of SVG!"
    date: 2007-01-05
    body: "Rough edges happen in alpha stuff... They probably haven't even looked for that sort of issue yet. However, the time-axis idea would be very useful."
    author: "Ben Morris"
  - subject: "Thanks !"
    date: 2007-01-03
    body: "Thanks for this great summery ! I really missed easy to get informations about the progress of kde4.\n\nGreetings\nFelix"
    author: "Felix"
  - subject: "Not impressed"
    date: 2007-01-03
    body: "I prefer the original KMahjongg screenshots. Also, is KDE 4 going to look as ugly as KDE 3.5.5? I'd like something much much better, as good looking as Windows Vista is possible."
    author: "Charles"
  - subject: "Re: Not impressed"
    date: 2007-01-03
    body: "kde 3.5.5 is already much better than vista."
    author: "redeeman"
  - subject: "Re: Not impressed"
    date: 2007-01-04
    body: "Well, no.\nVista looks a lot more elegant and polished than a stock KDE install.\nFunctionality and speed-wise, I'd say they're on par, not \"much better\".\n\nI really hope that KDE 4.0 would be an \"all about the defaults\"-release, since the current ones, well, suck."
    author: "AC"
  - subject: "Re: Not impressed"
    date: 2007-01-04
    body: "Si it is only me who really dislike the look of Vista? Not because it is Window or Microsoft; well, I've never liked MS' designs, but I think Vista just looks \"too much\". Bleh.\n\nA little bit off topic. But I agree that the default look should (and I think will) be improved."
    author: "Lans"
  - subject: "Re: Not impressed"
    date: 2007-01-04
    body: "XP was already too much with the luna interface, Vista made things only worse..\n"
    author: "otherAC"
  - subject: "Re: Not impressed"
    date: 2007-01-04
    body: "Yes, Luna was horrible, I agree on that. But Vista's Glass interface looks good and works well, you have to try it. I'm running it since a month, and it actually doesn't seem a typical Microsoft product. It's very fast, not even a single crash so far, and a lot more polished. And it's trying to \"modernize\" the classic WIMP paradigm, just look at the new Office. I think that we can learn - and borrow - a lot from our main competitor's work."
    author: "AC"
  - subject: "Re: Not impressed"
    date: 2007-01-11
    body: "If you have ever tried PCLinuxOS you will see how KDE 3.5.5. can look 10x better than XP and at least as good as Vista. It is extremely good looking distro.\n"
    author: "John"
  - subject: "Re: Not impressed"
    date: 2007-01-04
    body: "> Functionality and speed-wise, I'd say they're on par, not \"much better\".\n\nFunny, I thought that you needed to buy a new super duper machine just to run Vista, while KDE runs fine on a 4 year old machine. I can only imagine what that must doe for 'speed'.\n\nThe functionality is part is personal; you can configure your KDE much more, for one. You get a much much better command-line application and many more apps in general. For a price you can't beat."
    author: "Thomas Zander"
  - subject: "Re: Not impressed"
    date: 2007-01-05
    body: "The good thing is this, yeah. KDE can give an even better look with very few resources, ie <100m RAM vs 512.\n\nThe \"bad\" thing, is that the default look of KDE sucks. Oversized icons and bars, very common colors and stuff. It makes me wonder why don't the devs go take a look at kde-look.org and get some inspiration... maybe they can include in there 5-10 extra themes.\n\nI'm kind'a sick of distros shipping with only mild tuning of the KDE default, thus making it seem so much \"less\" than it actually is. And it's all simple things.. proper icon sets and sizes / bar sizes / nice colors etc. These can make the difference between \"ok I can work with this\" and \"wow, what's that?\"\n\nPersonally I like black so I've singled out one simple / elegant from kde look\n\nhttp://www.kde-look.org/content/pre1/48437-1.jpg\n\nbut which has a relative harmony in icon sizes / bars / colors ...\n\n..and this one which is was while trying to go a bit vista on mine.. The default suse (10.2) start button sucks anyway - aesthetically. It was just like a rectangle with a not-so-good-look so I used another theme from kde-look... Anyway that's my attempt:\n\nhttp://img515.imageshack.us/my.php?image=sss53oz8.png\n\nSo let's hope default look and embedded themes get significantly improved. It makes a hell of a difference with not much work - just a few choices on fonts, icons, wallpapers, bar sizes etc and voila...\n"
    author: "A.S."
  - subject: "Re: Not impressed"
    date: 2007-01-05
    body: "Lets just say that tastes differ. I'm afraid a significant set of people would walk away in disgust if KDE looked like either of these images by default.\nI know my first reaction was one where I'm happy I didn't hold a drink in my hands while walking on an expensive carpet."
    author: "AC"
  - subject: "Re: Not impressed"
    date: 2007-01-03
    body: "KDE4 will look quite different than 3.x in many ways.  However, things like menus and toolbars and such look much like KDE3 at this point because I made the screenshots using the default themes.  \n\nAt this point though, using the default theme allows app development and porting without having to worry about the possibility of an app crashing because of an untested theme.  I have a few articles scheduled for later that will show off just how different KDE 4 can look, but at this point I'm still focusing on the apps and features."
    author: "Troy Unrau"
  - subject: "Re: Not impressed"
    date: 2007-01-04
    body: ">>as good looking as Windows Vista\n\no_O"
    author: "otherAC"
  - subject: "Will be possible to copy-paste the SVG?"
    date: 2007-01-04
    body: "Hi, the new ksysguard looks really great. Will be possible to copy the current graphic and paste it as SVG on a text editor, or an SVG editor as Inkscape? That will be a nice feature (and I guess similar feature would be also nice in other programs)."
    author: "dis"
  - subject: "Re: Will be possible to copy-paste the SVG?"
    date: 2007-01-04
    body: "That's interesting. Kind of like how text is copied and pasted now. I think the biggest problem would be how exactly you would select individual SVG images. Text doesn't 'overlap' in a window, SVG images can."
    author: "Cerulean"
  - subject: "Re: Will be possible to copy-paste the SVG?"
    date: 2007-01-04
    body: "I suppose this could be possible, but why would you want to?  Do you mean copy the graph with the lines, axis and everything as an svg?"
    author: "John Tapsell"
  - subject: "what theme are you using?"
    date: 2007-01-04
    body: "those are very pretty screenshots, I've compiled from svn a couple of weeks ago and it doesn't look like that. what did you add?"
    author: "joe"
  - subject: "Re: what theme are you using?"
    date: 2007-01-04
    body: "i'd say nothing, development is going fast... it's getting at the stage they can start working on the interfaces."
    author: "superstoned"
  - subject: "Re: what theme are you using?"
    date: 2007-01-04
    body: "I didn't use anything unusual... In fact, the only things I built to get that all up an running were dbus, qt-copy, kdelibs, kdepimlibs, kdebase and kdegames... all in a fresh account on my system that's isolated from KDE 3.5.5... and I purposefully wiped out .kde before loading to ensure that it was using the KDE defaults for all options."
    author: "Troy Unrau"
  - subject: "redundancy in ksysguard"
    date: 2007-01-04
    body: "What's with the redundancy in ksysguard?  Specifically the bar showing current CPU usage.  One can simply look at the far right of the graph to see that.  What's the point in showing it twice?\n\nSee pic if this isn't clear enough:\nhttp://img81.imageshack.us/my.php?image=redundantwz1.png\n\nI'd ask for an option to disable that, but for heaven's sake it should just go.  It grates.  A better way to do it if you simply must have the current value highlighted somehow would be something like this:\nhttp://img205.imageshack.us/my.php?image=lessredundantut2.png"
    author: "MamiyaOtaru"
  - subject: "Re: redundancy in ksysguard"
    date: 2007-01-04
    body: "I dunno, personally I prefer the original screenshot. The second is misleading because it looks as if the load has been steady at the current load for some seconds.\n\nSeeingt he current load in a separate bar is a nice feature imo as it can be difficult to appreciate current load from the graph when it is rising or falling rapidly."
    author: "Simon"
  - subject: "Re: redundancy in ksysguard"
    date: 2007-01-04
    body: "what about placing the 'currrent load bar' on the right of the graph and making it vertical. this way you would be able to compare both more easily and it might even look less out of place"
    author: "papillion"
  - subject: "Re: redundancy in ksysguard"
    date: 2007-01-04
    body: "Yes, that's the way Windows does and it makes a lot of sense, and it does look less \"busy\".\n\nhttp://www.tomshw.it/guides/software/20060304/images/taskcariconormale.JPG\n\nBesides, having duplicated captions (one in the window and one on the graphs) is really a no-go.\n\nI'm not sure about the numbers shown in the status bar either. They contains so much information that to be read you are forced to enlarge the window. How about rewording it? Instead of \"Memory: 349.9 MiB used, 1,001.8 MiB total\" just show \"Memory: 350/1,002 MiB used\" or even better \"Memory: 35% used\" and move the actual numbers to another tab."
    author: "AC"
  - subject: "How can I code and contribute ?"
    date: 2007-01-04
    body: "Can some one tell me where is the repository (svn/cvs) for KDE System Guard (KDE 4 dev series ) ... Have QT/C++/SVG skills will like to contribute ..."
    author: "Ankur Gupta"
  - subject: "Re: How can I code and contribute ?"
    date: 2007-01-04
    body: "To compile KDE4 read this tutorial:\n\nhttp://developer.kde.org/build/trunk.html\n\nKSysGuard is here:\n\nhttp://websvn.kde.org/trunk/KDE/kdebase/workspace/ksysguard/\n\nHTH"
    author: "Carsten Niehaus"
  - subject: "Re: How can I code and contribute ?"
    date: 2007-01-04
    body: "it's just in svn trunk. if you compile and install KDE4, you'll get the new system guard."
    author: "superstoned"
  - subject: "Re: How can I code and contribute ?"
    date: 2007-01-04
    body: "You can email me :  johnflux at gmail dot com  and we can discuss ideas.  Actually the biggest thing I want added at the moment is a simple hard disk monitor to check the hard disks are okay.\n\nJohn"
    author: "John Tapsell"
  - subject: "Re: How can I code and contribute ?"
    date: 2007-01-13
    body: "wow.. this idea sounds really nice... please develope this for us ^^"
    author: "LordBernhard"
  - subject: "Beryl/Compiz compatability?"
    date: 2007-01-04
    body: "Just wonder about any Beryl/Compiz compatability?"
    author: "Dr No"
  - subject: "Re: Beryl/Compiz compatability?"
    date: 2007-01-04
    body: "AFAIK kwin wil get xgl capabilities.\n"
    author: "otherAC"
  - subject: "Re: Beryl/Compiz compatability?"
    date: 2007-01-13
    body: "what about aiglx then?"
    author: "LordBernhard"
  - subject: "Re: Beryl/Compiz compatability?"
    date: 2007-01-05
    body: "You can run Beryl fine with KDE right now. Yes, it involves not using kwin, but there is now a way to use standard kwin themes (I don't know if the window menu has a KDE rather than GTK look yet). The only thing that doesn't really work for me now is kicker's pager."
    author: "Ben Morris"
  - subject: "Lol"
    date: 2007-01-04
    body: "Gnome Rulz\n\nthis enviroment sucks"
    author: "Guille"
  - subject: "Re: Lol"
    date: 2007-01-04
    body: "Oh man - use your gnome and tay away from future!\n\nI love the upcoming - KDE Ruled allways..."
    author: "Mr.Gosh"
  - subject: "Re: Lol"
    date: 2007-01-04
    body: "Oh man - use your gnome and stay away from future!\n\nI love the upcoming - KDE Ruled allways..."
    author: "Mr.Gosh"
  - subject: "Re: Lol"
    date: 2007-01-04
    body: "Wow, you must be very jealous :)\n\nBut don't worry, you can always switch to kde4 when it's ready, without having to worry about loosing data etc.\nHeck, you can even use some of your old applications onder KDE, and with the promise of Portland even with KDE functionality..\n\nSo even for you, the future will look bright!!"
    author: "otherAC"
  - subject: "Revolutionary?"
    date: 2007-01-04
    body: "I thought KDE 4 was going to bring a revolutionary new approach to office computing.\n\nBut this just looks like the same windows-like desktop with neater graphics.\n\nAnyway, is KOffice for Windows coming out anytime soon??"
    author: "Axl"
  - subject: "Re: Revolutionary?"
    date: 2007-01-04
    body: "No promises; but probably later this year."
    author: "Thomas Zander"
  - subject: "Re: Revolutionary?"
    date: 2007-01-04
    body: "Focus as been on porting libs etc., so the current \"KDE4\" looks much like KDE 3.x."
    author: "Lans"
  - subject: "svg ktuberling love please!"
    date: 2007-01-06
    body: "I just don't see how KDE4 can possibly be released without shame if someone doesn't upgrade ktuberling to SVG...\n\nThat and some sort of 3d semi-transparent treatment for kteatimer... and we'd have something very special in a desktop environment.\n\n"
    author: "Tim Middleton"
  - subject: "Re: svg ktuberling love please!"
    date: 2007-01-06
    body: "I guess kteatimer needs to be made a Plasmaoid(sp) and use all the niftyness you get from Plasma :-) "
    author: "Morty"
  - subject: "Re: svg ktuberling love please!"
    date: 2007-01-07
    body: "http://wiki.kde.org/tiki-index.php?page=KDE+Games+SVG+status says it's in progress."
    author: "a thing"
  - subject: "KDE's new look"
    date: 2007-01-06
    body: "I'm all for KDE taking these steps, but I honestly think that, save for the \"Run\" design, every single one of these screenshots is simply hideous. Mentally challenged toddlers could scribble more attractive usage graphs with crayons. Those fonts still look like ass. It's like the KDE folks are desperately shouting down a wind tunnel at Vista and Mac OS X. What are they screaming? \"Wait for me! I can play too! Look at me!\". Vista and Mac OS X simply cling their wine glasses together and laugh."
    author: "anonymous"
  - subject: "Re: KDE's new look"
    date: 2007-01-06
    body: "Remember that this is all just work in progress. You can expect a lot of things to change as we go."
    author: "Ephracis"
  - subject: "KDE 3.5.5 SVG Deco"
    date: 2007-01-06
    body: "http://www.kde-look.org/content/show.php?content=49665\nKDE 3.5.5 Windeco that loads frame and buttons from SVG files.\nBased on Plastik it should be really easy to port to v4 or, at least, the functionality.\nIs this even needed ? I haven't dug into Qt/KDE 4 src yet, waiting until its included with my distro, Qt 4 is, so KDE 4 shouldn't be too far behind. Life expectancy of this dist is supposed to be 2.5 years.\n\n#B^]"
    author: "bitwit"
  - subject: "Great"
    date: 2007-01-07
    body: "I think the SVG rendering is excellent. If all the icons are moved to SVG, then the icons keep their sharpness with increased size. The same with any other graphical item on the desktop. Resizing without pixelating just makes the desktop so much more appealing. I can already see the benefit of this with SVG icons - and this is with the previous version of KDE. One icon image, not multiples for different sizes. And it scales perfectly without pixelating - and is usually smaller."
    author: "TK"
  - subject: "Re: Great"
    date: 2007-01-13
    body: "is it planned vor kde4 that all icons are completely scaleable?"
    author: "LordBernhard"
  - subject: "Amazing!!!!!"
    date: 2007-01-09
    body: "I am so completely blown away!!! This has got to be the most unbelievable and quite simply the uttermost anticlimactic news article I have read involving KDE4 to date. Unfortunately there have been more than a few. :("
    author: "Wowser"
  - subject: "Awesome Looks"
    date: 2007-02-12
    body: "With what im seeing KDE4 promises to be very exciting. The looks are just awesome.\nKeep up the great work guys. "
    author: "Adekunle Lawal"
  - subject: "KDE Needs To Be Faster, Faster, Faster!"
    date: 2007-01-12
    body: "Right now I see that the efforts of KDE developers are again making a mistake by concentrating efforts in the Eye Candy Factor. But there are issues much more important: One is improvement of handling and distributing memory resources. KDE is getting fatter and fatter like a vulgar pig named VISTA. Should I remind that Linux is notable for its (so far) ability to run in old, discontinued hardware? The competitor Microsoft Vista is already entangled in its own trap, being able to run only in top, expensive new hardware.\n\nC'mon KDE guys, Don't follow the steps of Microsoft as your model to imitate or emulate. Linux and of course KDE are not Windows! Please keep doing the right thing. Follow your common sense. I know you have. You should realize that KDE, as a faster GUI will have much more acceptance. If you have to choose between beauty and functionality.... I'm sure u're understanding now.\n\nMaxei DeVrai\nSLED 10-KDE 3.5 running in SOYO SY-k7VTA-B, cpu 1.1GHz, 896 MB SDRAM pc133, Radeon and 3D enabled!\n"
    author: "Maxei DeVraie"
  - subject: "Re: KDE Needs To Be Faster, Faster, Faster!"
    date: 2007-01-13
    body: "Yes, I have to agree.  KDE has become so slow that I am going to have to upgrade my 400 MHz K6 III.  When I open too many windows and tabs in Konqueror, it just seems to get lost at the point that I have 1/2 GByte of swap.  I have 7/16 GByte of memory and also have a Radeon (a 9200 for which X11 supports 3D acceleration).\n\nThen there is the question of the visually impaired.  Is there going to be some simple way to turn off all of this eye candy -- eye candy which is clearly going to make the GUI much less clear for the visually impaired.\n\nIAC, with KSysGuard, I think that it would be a greater improvement to make it fully functional than to add totally unneeded eye candy such as backgrounds and anti-aliased lines.  Which lines?  A bar graph isn't a line -- there is no line to anti-alias -- and I don't think that you can (or need to) anti-alias horizontal and vertical lines."
    author: "James Richard Tyrer"
  - subject: "Re: KDE Needs To Be Faster, Faster, Faster!"
    date: 2007-01-26
    body: "KDE4 is really faster than 3.x series because of QT4 improvements."
    author: "Emil Sedgh"
  - subject: "URL, nothing more."
    date: 2007-03-27
    body: "Read this and find a cushion for your liar's chair. KDE is becoming slimmer with QT4, not fatter.\n\nhttp://ktown.kde.org/~seli/memory/desktop_benchmark.html"
    author: "James Smith"
  - subject: "Re: URL, nothing more."
    date: 2007-03-27
    body: "That link would be great if it had anything to do with KDE4's memory usage.  At all.\n\n\"KDE itself was KDE 3.5.2 with my performance patches\"\n\nWhy did you even bother to post it?\n\n"
    author: "Anon"
  - subject: "Suggestion concerning Ksysguard"
    date: 2007-02-07
    body: "The KDE System Guard background needs to be lighter. Also loose the line or whatever there, it's useless and makes the graph less readable.\n\nI'm generally against using graphics everywhere, they should be used if it makes the application in question easier to use, but not for prettiness sake.\n\nAlso, try to make KDE4 not Vista-slow. Just tested Vista today and it was eating around 20% CPU idle on a 1200$ laptop.\n\nVector graphics are pretty and powerful but eat a lot of cpu, at least before dedicated vector GPUs appear, and thus should be used sparingly.\n\nAlso, when using SVG, remember to optimize! There should be a function in every respectable vector graphics package to do that. Better yet, design effective(as in few paths and few points) graphics from the ground up.\n\nSorry to sound so critical, it's just that I run KDE and want it to be as good as possible.\n\nBut, good work and carry on!\n\n\nps. If there is a particular area that needs improving graphics-wise, I'm a fairly experienced graphic designer trying to learn SVG"
    author: "J"
  - subject: "Increased use of memory with SVG?"
    date: 2007-02-13
    body: "Correct me if I am wrong, but will not using SVG mean increased use of memory?"
    author: "Daniel Aleksandersen"
  - subject: "Amazing"
    date: 2007-03-05
    body: "It is really amazing this KDE 4 ! I am impressed with new look ! It is great!\nThanks KDE team!"
    author: "Ali"
---
Since KDE 4 development is in full swing with plans for a KDE 4.0 
release sometime later this year, I thought I'd put together a 
weekly piece entitled <em>The Road to KDE 4.</em> The idea is to 
have a short overview of one or two of the features that show 
progress in KDE 4. For my first issue, the goal is to show off some of the great SVG work that has taken place so far. Read on for the details...






<!--break-->
<p>Since many features are covered in personal blogs via <a href="http://planetkde.org/">the KDE Planet</a>, I'll try to cover those that receive less public coverage, or need more public coverage.</p>

<p>The first thing I'd like to point out is that KDE 4 builds, installs, and runs well enough that I can test many of the ported KDE 3.x applications, and most of them are pretty stable. The real joys come when you look more closely at those improvements that are afforded by the changes in the base technologies. Today, I'll talk about one of the eyecandy features provided by Qt 4: <em><acronym title="Scalable Vector Graphics">SVG</acronym> rendering in applications.</em></p>

<p>There are many other KDE applications reaping the benefits of SVG drawing to make them more pleasing, and more scalable. Check out some of these posts:<ol><li><a 
href="http://tsdgeos.blogspot.com/2006/12/svg-meets-blinken.html">Albert Astals Cid renovates blinKen</a></li><li><a href="http://wiki.kde.org/tiki-index.php?page=KDE+Games+SVG+status">KDE 4 games SVG status wiki</a></li></ol></p>

<p>Today I'm going to focus on a handful of apps, providing before and after screenshots to compare the KDE 4 development version (pre-alpha stuff) to the existing KDE 3.5.5 equivalents.</p>

<br><p>To begin, I'll look at the KDE System Guard, a useful utility packed into KDE that you can pull up as 'ksysguard'. It does all sorts of neat things like display charts of memory and CPU usage, and a process table (also accessible via the Ctrl-Esc keyboard shortcut).</p>

<p>Here's how it looked in KDE 3.5.5: </p><p align="center"><img src="http://static.kdenews.org/dannya/vol1_355_ksysguard.png" alt="ksysguard in kde 3.5.5" /></p>

<p>And now, in the KDE 4 development series (the lines are antialiased, the graphs are translucent and the backgrounds are SVG): </p><p align="center"><img src="http://static.kdenews.org/dannya/vol1_devel_ksysguard.png" alt="ksysguard in kde4 devel" /></p>

<p>As you can see, it is visually much improved from its current and very functional form.</p>

<br><p>Next we'll look at some of the diversions shipped in the kdegames package. KAtomic is a puzzle game. It's fun, semi-educational, and could definitely have used an image overhaul. Here it is in it's spartan KDE 3.5.5 glory: </p><p align="center"><img 
src="http://static.kdenews.org/dannya/vol1_355_katomic.png" alt="katomic in 
kde 3.5.5" /></p>

<p>And now, with much improved <a href="http://oxygen-icons.org/">oxygen</a>-style graphics in the development series:</p><p align="center"><img 
src="http://static.kdenews.org/dannya/vol1_devel_katomic.png" 
alt="katomic in kde4 devel" /></p>

<br><p>KMahjongg ships in the kdegames package, and is a staple of puzzle gaming. Here it is from KDE 3.5.5 looking like a game that escaped from the <a 
href="http://en.wikipedia.org/wiki/Best_of_Windows_Entertainment_Pack"> 
Best of Windows Entertainment Pack</a>: </p><p align="center"><img 
src="http://static.kdenews.org/dannya/vol1_355_kmahjongg.png" 
alt="kmahjongg in kde 3.5.5" /></p>

<p>And now, with a much-improved SVG-powered tileset in the 
development series: </p><p align="center"><img 
src="http://static.kdenews.org/dannya/vol1_devel_kmahjongg.png" 
alt="kmahjongg in kde4 devel" /></p>

<br><p>And last but not least, is one of the more frequently used parts of KDE: the "Run Command" dialog (Alt-F2). Previously this: </p><p align="center"><img 
src="http://static.kdenews.org/dannya/vol1_355_run.png" 
alt="kmahjongg in kde 3.5.5" /></p>

<p>Now, thanks to desktop interface guru Aaron Seigo, it's a SVG themable, really slick element of the <a href="http://plasma.kde.org/">Plasma</a> desktop. Still a work in progress, but you'll get the idea from this screenshot.</p><p align="center"><img 
src="http://static.kdenews.org/dannya/vol1_devel_run.png" alt="krunner in kde4 devel" /></p>

<p>Until next time folks, when I reveal yet another KDE 4 feature under development. Cheers.</p>
