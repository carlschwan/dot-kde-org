---
title: "Akademy 2008 to be Held in Belgium"
date:    2007-09-29
authors:
  - "bcerneels"
slug:    akademy-2008-be-held-belgium
comments:
  - subject: "Great choice!"
    date: 2007-09-28
    body: "Mmmmmmmm, waffles...."
    author: "Odysseus"
  - subject: "Re: Great choice!"
    date: 2007-09-28
    body: "No, no, no! It's\nMmmmmmmmmmmmmmmh beeeeeer!\nBelgium is well known for its many varieties, and quality, of beer.\nI bet that there is a waffle-flavored beer, though. Cheers!"
    author: "Andreas"
  - subject: "Re: Great choice!"
    date: 2007-09-28
    body: "I am sorry, please no overpriced Belgium beer. Kriek, Oval,... sorry, no. Nothing for a beer gourmet. Leuven - Louvaine, the problem is that there are actually two universities or cities... Don't forget to make an appointment with your MEPs for next year Akademy, Brussels is close."
    author: "Menne"
  - subject: "Re: Great choice!"
    date: 2007-09-28
    body: "But their beer is sooo yummy. "
    author: "Ian Monroe"
  - subject: "Re: Great choice!"
    date: 2007-09-29
    body: "Avoid Kriek but sample Westmalle, Duval, Chimay, Grisette, St Feuillen, Karmelite... the list goes on. \n\nJust remember Belgian beers are strong 5% - 12% (e.g. 1 beer ~ 1/2 bottle of wine) Try not to pay more than 3 euros!\n\nBut you must sample some!"
    author: "Kevin Colyer"
  - subject: "Re: Great choice!"
    date: 2007-09-30
    body: "Grimbergen rules! B-)"
    author: "anonymous"
  - subject: "Re: Great choice!"
    date: 2007-10-09
    body: "A \"kriek\"/\"oude kriek\" from e.g. Boon, Hanssen, Cantillon or Drie Fonteinen is actually quite good.\n\nAnd Belgian beers aren't necessarily strong, e.g. Leroy's \"Prima\" & \"Ridder\" are only 2.x% or 3.x%, but taste _a lot_ better than most well-known Belgian (& foreign) pilsner beers (Jupiler, Stella, Maes, Heineken & other crap)."
    author: "JanC"
  - subject: "Re: Great choice!"
    date: 2007-09-30
    body: "if you look around , you'll find REAL \"kriek\" , and not the\ncommercial stuff (BV). At least, we have a choice \nchoosing our beer's and software (love kde & geuze), \nyou just need to look a little bit further.\n\nWe just have the best beer ;-)\n\nhttp://beeradvocate.com/top_beers?c_id=BE\n\n"
    author: "Geuze"
  - subject: "Re: Great choice!"
    date: 2007-09-30
    body: "\"I am sorry, please no overpriced Belgium beer.\"\n\nIt's much cheaper if you buy it in Belgium than outside Belgium..."
    author: "anonymous"
  - subject: "Re: Great choice!"
    date: 2007-10-01
    body: "*confused*\n\nThey make loads of great beers in Belgium, how can you say no to them?! Here in Denmark they don't cost more than other imported or special beers.\n\nWestvleteren 12...yum!"
    author: "Joergen Ramskov"
  - subject: "Re: Great choice!"
    date: 2007-09-28
    body: "Mmmmm chocolate!"
    author: "Eimai"
  - subject: "Re: Great choice!"
    date: 2007-09-29
    body: "Ummmmmm delicious girls :-P"
    author: "Bad Boy"
  - subject: "Re: Great choice!"
    date: 2007-09-30
    body: "...do you want (Belgian) fries with that? :-)"
    author: "gandy"
  - subject: "Coding marathon"
    date: 2007-09-28
    body: "I always like how the coding marathon overlaps the General Assembly meeting.  When you see all the laptops open during the discussion, you know it's just not the people in the labs cranking away."
    author: "Wade"
  - subject: "Re: Coding marathon"
    date: 2007-09-29
    body: "yes, and you further have the BoF sessions, very inspiring as well..."
    author: "jospoortvliet"
  - subject: "Let's see"
    date: 2007-09-29
    body: "Let's hope that Flanders will still be part of Belgium next year... I really doubt it... as I'm belgian myself. You should have chosen Brussels..."
    author: "Cypher"
  - subject: "Re: Let's see"
    date: 2007-09-29
    body: "Could you explain it, please. ?:-("
    author: "Bad Boy"
  - subject: "Re: Let's see"
    date: 2007-09-29
    body: "If it will come to that, we'll still call it Akademy in Belgium then. Anyone is invited even if we have to provide visa invitations to the Wallon and Bruxellois :)\n\nBut I'm sure Belgium will still exist by August 2008, no worries.\n"
    author: "Stecchino"
  - subject: "Re: Let's see"
    date: 2007-09-29
    body: "I'm surprised you're so sure.  That said, I hope you're right.  It does seem ridiculous how the Walone and Flemmish politicians can't get a long.  But then Belgium is the home of Surr\u00e9alisme. [ A Brit living in Brussels.]"
    author: "Paul Mayer"
  - subject: "Re: Let's see"
    date: 2007-09-29
    body: "Let's not get silly, the borders will all remain open if something happens as the language borders have already been set in law and in practice. There may come a small riot in Brussel because of youthfull heat (located in flanders but now mostly Frenchspeaking as many shopowners refused to speak Dutch, but people working there live beyond city borders in suburbs both in French and Dutch speaking Belgium) however with so many open borders nearby people feel very European and the splitstate government is of no consequence other then itself being the umbrella or the EU being the umbrella for moving some infrastructure money around."
    author: "Dennis"
  - subject: "Re: Let's see"
    date: 2007-09-30
    body: "Well, the truth is that French is kindof supported by foreign nations and Dutch and German get discriminated. "
    author: "Menne"
  - subject: "Re: Let's see (yeeeeeeeeeeeeeha!)"
    date: 2007-10-01
    body: "Sure i'd prefered Brussels too but just cuz i'm living there :).\nAnyway, Belgium or Belgium/2, i don't care, if it's resolved, great! if it splits, that's the way but we will finally look elsewhere and move on.\n\ncheers and welcome.\n\nBTW: Beers and Fries are really better in Brussels or near the coast then, in Mechelen you'll be there just for the conference. :)"
    author: "Omar"
  - subject: "Re: Let's see (yeeeeeeeeeeeeeha!)"
    date: 2007-10-10
    body: "Actually, Mechelen has \"Gouden Carolus Classic\" which is quite good (especially at the brewery's associated restaurant where almost all meals are cooked with beer added ;) )."
    author: "JanC"
  - subject: "branding an event name"
    date: 2007-09-29
    body: "<QUOTE>You will be pleased to know that this year we have corrected the capitalisation of the name \"Akademy\".</QUOTE>\n\nno, i'm not pleased.\n\ncongratulations, you're just about to destroy a well established brandname, and start to drown it amongst the other \"akademy\"s that are out on the internet. and you are inviting now even more comments about the wrong spelling.\n\nso why didn't you also correct the _*spelling*_ of the name?? after all, my english dictionary tells me it should be \"academy\" (maybe \"Academy\" if used as a title).\n\nthe 'aKademy' spelling has a \"branding\" effect. \"Akademy\" takes that away.\n\nit's a stupid decision. for once, KDE will loose all the easy to recognize search results in Google and elsewhere (where the weird capitalization makes the result to become recognizable amongst the irrelevant ones). second, it proofs KDE can't be and behave consistent over time. third,... oh, well, i better stop ranting....\n\nafter all, the most important thing is the belgian organizers get their work done. if they get a great and big aKademy 2008 going, it will be quickly forgiven that they do not know how to give their event an easy to recognize branding.  :-)\n\nso first of all: thanks for organizing the event!"
    author: "interested observer"
  - subject: "Re: branding an event name"
    date: 2007-09-29
    body: "Nah, it's far more professional, and looks better."
    author: "Ljubomir"
  - subject: "Re: branding an event name"
    date: 2007-09-29
    body: "Ha! The logo says \"AKademy\"."
    author: "reihal"
  - subject: "Re: branding an event name"
    date: 2007-09-29
    body: "Hey! We are not supposed to be professionals! ;)"
    author: "Luciano"
  - subject: "Re: branding an event name"
    date: 2007-09-29
    body: "I assume this was a vote taken by the KDE e.V.  They must have had their reasons for it."
    author: "KDE User"
  - subject: "Re: branding an event name"
    date: 2007-09-29
    body: "Yea, its just like how everyone stopped using amaroK when it was renamed Amarok.\n\n...\n\n:)"
    author: "Ian Monroe"
  - subject: "excuse me?"
    date: 2007-09-29
    body: "Who what when and where was the name changed?  I thought I was on every KDE list known to man and I have not seen how or when this decision was made.  Please clarify."
    author: "Anonymous"
  - subject: "Re: excuse me?"
    date: 2007-09-29
    body: "The KDE eV mailing list is not public. The KDE eV is there to handle organizational and \"business\" matters, well, about anything which needs a legal entity. It doesn't have much power apart from that, which is a good thing I think. Many members of the core KDE community are members of the eV.\nSee http://en.wikipedia.org/wiki/Eingetragener_Verein for an explanation of the eV acronym."
    author: "Andreas"
  - subject: "Re: excuse me?"
    date: 2007-09-29
    body: "Thank you for that link. i've been wondering what e.V stood for for a while now. "
    author: "Soap"
  - subject: "Re: excuse me?"
    date: 2007-09-30
    body: "The KDE e.V. has not been involved in this decision.\n\nIt was a decision by the KDE News team and by the aKademy organisers."
    author: "Olaf Schmidt"
  - subject: "Re: excuse me?"
    date: 2007-09-30
    body: "Well, I can say that it wasn't a decision by the KDE News team as you put it -- at least, I didn't see anything about it on the dot list so there was no official decision involved as far as I know.  \n\nThis is something for the e.V. to decide IMHO."
    author: "Navindra Umanee"
  - subject: "Welcome to my country :)"
    date: 2007-09-30
    body: "Great, maybe I'll visit if that's allowed (I'm not a coder) :)\n\nSome tips: \n\n- Belgian beer is not overpriced. And you are in luck, Leuven is a student hangout and there are lots of places where beer (even specialty beer like Trappist) is pretty cheap; at least it was last time I was there. Typical student city so always something to do (besided hacking :p)\n\n- Do not accidentally take the train to Louvain-La-Neuve... happens to the best *cough*\n\n- True, the girls are delicious, but they're all MINE... eh, nevermind that :s\n\n- Since it's university, you should have a pretty fast network connection. ;)\n\n- Don't forget to bring/buy an umbrella (yes, even though it is summer... sometimes we get unlucky)\n\nAnd have fun.\n\n\n\n"
    author: "Darkelve"
  - subject: "Re: Welcome to my country :)"
    date: 2007-09-30
    body: "Everyone is invited, especially non-programmers, we have enough of those hanging around already..... Just kidding, there are never enough developers.\n\nLike I said in my blog post: every user or just interested observer is invited, even during the coding marathon there are interesting things to do for non coders, like BoF sessions. (http://commonideas.blogspot.com/2007/09/akademy-in-belgium.html)\n\nAnd a small correction: aKademy will take place in Sint-Katelijne-Waver, near Mechelen, where girls are pretty and the beer is affordable :) Not Leuven."
    author: "Bart Cerneels"
  - subject: "Re: Welcome to my country :)"
    date: 2007-10-12
    body: "Thanks for the tips on Leuven as student city and the trains to Louvain-La-Neuve, but...\n\nThe Akademy is not taking place in Leuven at all, it's taking place in Sint-Katelijne-Waver (near Mechelen), in the De Nayer Institute. That's clearly stated in the second paragraph. So we better all take a train to Mechelen.\n\nI do understand the confusion though, as the first paragraph refers to this institute as \"an associated campus of the University of Leuven\".\n\nGeert"
    author: "Geert Janssens"
  - subject: "forgot to say:"
    date: 2007-09-30
    body: "\"Typical student city so always something to do (besided hacking :p)\"\n\nExcept in the summer, when most students are home/on vacation"
    author: "Darkelve"
---
The annual KDE World Summit, <a href="http://akademy2008.kde.org">Akademy, has found a home for 2008</a> in the heart of Europe, Belgium. The event is the most important conference for the contributors of the KDE project and will be held from Saturday August 9th to Saturday 16th at the <a href="http://www.denayer.wenk.be/index_eng.htm">De Nayer Institute</a>, an associated campus of the University of Leuven. There are three sub-events: a contributors conference, the KDE e.V. annual general assembly and a week long hacking session. Akademy offers a great opportunity to the community to discuss all issues face-to-face. We also look forward to the chance to mingle with all KDE enthusiasts who want to drop by.

<!--break-->
<style type="text/css">
table.akademy2006 {
    border-collapse: collapse;
    font-size: smaller;
}

table.akademy2006 th {
    padding: 0.4em;
    background: '#CCCCCC';
    border: #535353 1px solid;
}

table.akademy2006 td {
    padding: 0.4em;
    border: #535353 1px solid;
}
td.track_community {
    background: #84EE42;
}
td.track_kde4 {
    background: #ADD8E6;
}
td.track_keynote {
    background: #F7AE66;
}

</style>

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 396px">
<img src="http://static.kdenews.org/jr/akademy-2008-launch.png" width="396" height="262" />
</div>

<p>The conference will be organised by a local team assembled by Bart Cerneels and Wendy Van Craen, whose proposal to host Akademy at Sint-Katelijne-Waver's De Nayer Institute was chosen by the KDE e.V. membership. Sint-Katelijne-Waver is located near the city of Mechelen, and is close to both Brussels and Antwerp.</p>

<p>The Institute is itself a user of KDE. Patrick Pelgrims, coordinator of the Bachelor in Electronics/ICT curriculum said "<em>We will be happy to receive the KDE community at our campus. They are the creators of the amazing software platform we use to educate many young people in the world of Electronics, Computer Engineering and Embedded Linux</em>."</p>

<p>KDE e.V. would like to thank those who submitted the other proposals for hosting Akademy 2008 and we hope they will have a chance to host another KDE meeting soon.</p>

<p>The preliminary schedule for Akademy 2008 is as follows: </p>

<p align="center">
<table summary="This table contains the Akademy 2008 schedule" style="border-collapse: collapse;"  class="akademy2006">
      <tr><th id="days">Days</th>
      <th id="activity" colspan="4">Activity</th>
    </tr>
    <tr><td headers="days">Friday 8 August 2008</td>
      <td rowspan="1" colspan="2">
        Arrivals
      </td>
    </tr>
    <tr><td headers="days">Saturday 9</td>
      <td rowspan="2" colspan="2" class="track_keynote">
        KDE Contributors Conference
      </td>
    </tr>
    <tr><td headers="days">Sunday 10</td></tr>
    <tr><td headers="days">Monday 11</td>
      <td colspan="1" class="track_kde4">
        KDE e.V. General Assembly
      </td>
      <td rowspan="6" class="track_community">
        Coding Marathon
      </td>
    </tr>
    <tr><td headers="days">Tuesday 12</td>
      <td rowspan="5" class="track_community">
        BoF Sessions, Tutorials, Workshops
      </td>
    </tr>
    <tr><td headers="days">Wednesday 13</td>
    </tr>
    <tr><td headers="days">Thursday 14</td>
    </tr>
    <tr><td headers="days">Friday 15</td>
    </tr>
    <tr><td headers="days">Saturday 16</td>
    </tr>
</table>
</p>


<p>You will be pleased to know that this year we have corrected the capitalisation of the name "Akademy".</p>

<p>A formal call for papers will be put up in due time, but you may wish to start thinking of talk subjects to write up for the contributors conference part of Akademy. For the hacking and BoF sessions, try to figure out who you need some quality face-to-face time with and convince them to come too!</p>

<p>We are looking out for people to help with organising Akademy, if you live in the local area or if you have good business relations and conference hosting skills please do get in touch with akademy-team@kde.org.</p>










































