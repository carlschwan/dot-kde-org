---
title: "KDE Multimedia Meeting Video Interviews"
date:    2007-08-23
authors:
  - "sruiz"
slug:    kde-multimedia-meeting-video-interviews
comments:
  - subject: "Desktop Linux Survey"
    date: 2007-08-23
    body: "Seems like ever year theres a post regarding it.. any comments/opinions to the \"2007 Desktop Linux Survey results\" ? \n\nhttp://www.desktoplinux.com/news/NS8454912761.html"
    author: "Todd"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "I was actually logging the total number of votes and percentages of GNOME vs KDE every 5 minutes over a long period, and the logs seem kind of ... well ... odd, to me.\n\nDoes anyone know if there was any checking for non-unique IPs in this survey?\n\nThe general trend was for KDE's share to increase and GNOME's share to decrease but then the number of votes/ 5 minutes would spike suddenly (by an order of magnitude) and these votes seemed to be almost *all* for GNOME.  These spikes happen twice (actually bringing the survey server down at one point) and also oddly, the shares of OpenSuSE, Firefox and Evolution all increased hugely during them (I don't have these in my logs, though), which is doubly odd as KDE is dramatically preferred by OpenSUSE users.  After the spikes, the trend of increasing KDE and decreasing GNOME shares resumes again, but the flow of votes is at a trickle by this stage, so there's no hope of catching up.\n\nIf anyone knows of a site that allows anonymous uploading/ sharing of text files, I'd be happy to share the logs.  This probably sounds like tinfoil, but the huge spikes of OpenSUSE+GNOME+Firefox+Evolution votes are fairly dubious.\n\nIt would be nice if, in future polls, Captchas were used to make the results more trustworthy."
    author: "Anon"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "I think the results are fairly logical given the lead Ubuntu has on other distributions at the moment. Kubuntu is simply a minor distribution by comparison.\n\nPersonally, I think KDE is doing very well to hold the minority it has. Clearly, if you discount Ubuntu (for which most new users are not presented with a facility to install KDE, or even know what it is), KDE is doing very well."
    author: "Alistair John Strachan"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "\"Does anyone know if there was any checking for non-unique IPs in this survey?\"\n\nNope."
    author: "Boudewijn Rempt"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "Nope as in \"no one knows\" or \"there isn't\"? :-)\n\nThe results look a little suspect to experiences I've seen in other fields. If 25% of the total number of votes have indeed been cast in only one night, it looks like someone wanted to screw up the results. On the other hand, it's easy to interpolate in that case (but probably no one will). It doesn't do the value of the survey any good (but then online surveys are always subject to this kind of fanboy manipulation, which is why they should be taken with a grain of salt in any case).\n\n\"There are lies, damn lies and statistics\" as some smart individual once noted. A nice, but unrelated site that deals with what's hidden behind numbers is freakonomics.com, by the way."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "There isn't -- I was a little surprised by one of the spikes myself and tried to enter a second vote, just to check whether they checked. Since I succeeded, I assume that at least at one point in time there were no checks at all."
    author: "Boudewijn Rempt"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "as far as I could see, they did at least SOME check, as I couldn't vote for both myself and my ex (I use konqi, she uses firefox, so I thought I should mention that in the survey) from home..."
    author: "jospoortvliet"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "Between Thursday and Friday the total number of voters increased\nby about 10000 voters, all or most of which were OpenSUSE+GNOME.\n\nI wonder why desktoplinux.com doesn't bother to also publish a graph\nof their traffic sources...\n\n"
    author: "j.v."
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "I think it would be extremely interesting to see these statistics.\n\nWe must realize that this survey is going to be touted as proof that\nGnome has now \"overtaken\" KDE by both ignorant people and people with\nan agenda.  It would be very valuable if we can set things straight\nand perhaps also get a comment from DesktopLinux.com.  \n\nRight now, there is an \"analysis\" stating exactly what I say here \nabove, and that irks me since I think that it's false.  Naturally,\nit's also detrimental to us if people think that we're being \nabandonded.\n\nIf you want to publish your figures, then I am willing to do a writeup\ncomplete with nice charts that make the issue as clear as I can."
    author: "Inge Wallin"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "\"If anyone knows of a site that allows anonymous uploading/ sharing of text files, I'd be happy to share the logs. This probably sounds like tinfoil, but the huge spikes of OpenSUSE+GNOME+Firefox+Evolution votes are fairly dubious.\"\nYeah. Please share your logs. Wouldn't something like http://pastebin.com or Rapidshare not suffice?\n\nOf course even if outright manipulation did not take place, the survey results would be almost worthless nevertheless. As a minimum measure to estimate the real figures one would have to find out how inclined the specific user groups are to take part in this survey.\n\nEven if reliable figures can be obtained, there is still the question how relevant the results are. For example, it was only asked \"which web browsers do you frequently use\", it was not asked how often, and more important, how long the browser is really used. It is possible for instance that non-frequent users just go with the defaults, whereas frequent users deliberately choose another browser.\n\nAlso, usage preferences may not be mistaken as a measure of the quality of the programs. This I think is the most important point for KDE developers and users. Consider this: The question for web browser preferences is actually skewed *against* high-quality desktop environments, since in a well-integrated desktop environment like KDE, you do not need the browser so often, since you can just use http-urls in the applications themselfs (KPDF for example)."
    author: "EP"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "You can upload your file at www.rapidshare.com\n\n<I>This probably sounds like tinfoil, but the huge spikes of OpenSUSE+GNOME+Firefox+Evolution votes are fairly dubious.</I>\n\nWhy? Sounds like Nat Friedman pushed his favorites...Just kidding ;-)"
    author: "bedalius"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "> Does anyone know if there was any checking for non-unique IPs in this survey?\n\nYes, we checked and blocked non-unique IPs from non-corporate IP ranges and we tracked corporate use IP ranges. This prevented Joe Home Linux Fan from voting over and over again. We also made sure that JoesLinux.com couldn't stuff the ballot box for JoesLinux.\n\nThe data made it pretty clear that the GNOME bump came from Ubuntu users. \n\nFWIW, as a dyed in the wool KDE user, I think they don't know what they're missing.\n\nSteven"
    author: "sjvn"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "> The data made it pretty clear that the GNOME bump came from Ubuntu users. \n \nErm, how is this supposed to work mathematically? During the 10000 vote \"peaks\" in question openSUSE votes had to increase up to 40% for the whole duration of the peaks to get 20% for the total vote. During that time the amount of Ubuntu users must have _decreased_ actually. So I can hardly see how you can attribute the change of the poll to the Ubuntu users.\n\n> FWIW, as a dyed in the wool KDE user, \n> I think they don't know what they're missing.\n\nWe know that you like KDE. You know it's neither meant as a rant, nor a personal attack :-) "
    author: "Konqi"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-24
    body: "Thanks for bringing this up. You're not the only one who has caught mysterious shifts in the vote results.  Some days ago, triggered by another comment and having the results from the day before in mind, I checked about two days and three days after vote start and already saw a noticably shift in a certain vote constellation.\nsee here http://dot.kde.org/1187268289/1187269736/1187272356/1187273173/1187274305/1187288798/1187331858/1187343466/1187344416/1187353058/1187362815/\n\nAfter about 2 days the results were about settled (a number people from most continents/timezones having voted so that results weren't easy to skew anymore) so it could be expected that only total vote counts would go up after that.\nBut still, one vote constellation (which was: openSUSE, Gnome, Firefox, Evolution and \"None -- I don't run Windows apps on Linux\") would rise noticably after these about 18.000 votes. After the final 38.500 votes the results shifted like this:\nVote option | percentage after about 18.000 votes (after about two days after start; sorry, only rough numbers) | percentage after about 27.000 votes (half a day later) | final percentage after 38.500 votes\nopenSUSE ~9,5 ->16,5 -> 21\nGnome ~37,5 -> 42,5 -> 45\nFirefox ~54/55 -> 58,5 -> 60\nEvolution 14 -> 21,5 -> 32\nNone -- I don't run Windows apps on Linux ~22 -> 30 -> 39\n\nTo summarize, it was as if almost nobody voted for any other option anymore; massive shifts, even percentages which more than doubled!\n\nEven for persons who are not familiar with distributions of votes and normal shifts which may occur, it's not hard to see that this is very, very, very unlikely. Or to summarize, the poll has been manipulated.\n\nThat no suspicious votes have been seen by desktoplinux.com doesn't mean there haven't been any.\n\n(Perhaps someone can calculate the total number of votes which exclusively went to that combination. As the other vote options didn't change drastically, mostly only went down, I didn't track these.)\nSadly this poll is clearly manipulated and cannot be taken as a reliable source for the percentage a distro or application has on Linux. As I already wrote in the first comment, I also wouldn't trust these kinds of votes if it hadn't  deliberately been manipulated. \n\nP.S.: I don't really care for the manipulator (aka the one with the bot net?), but, well, I don't think he did any good for the options he pushed. His work is obvious, so is he really helping this certain distro, desktop, application or attitude he voted for? Perhaps publicity, but as it is so plain and simple obvious, it's quite certain that people will point at it, which reverses to bad publicity. The most certain thing one can say is: no reliable numbers this year."
    author: "Phase II"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-24
    body: "Honestly, your numbers are poor and mean nothing."
    author: "Div"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-24
    body: "Since you seem to have better insight into this than the rest of us: please share with us why you think these numbers are poor and mean nothing. Better yet, please explain the observed shifts yourself. "
    author: "Andr\u00e9"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-27
    body: "I've asked some ppl here (I work in a research institute) about how to analyze the results with statistical software, and see if it's significantly wrong. So I told them about the results (25.000 votes in 3 days, so on average a little under 350/hour. Then Friday night, from 3 to 4 o'clock - 3000 votes, 2700 Gnome+Suse+F etc, 300 random around KDE. How do I analyze this, see if it's likely to be random or not.\n\nThey said you don't need to analyze that, a child can see such numbers aren't normal."
    author: "jospoortvliet"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-29
    body: "Agreed. Any brainless 2-year-old would know these #s are unreal.\n\nUnfortunately, no-one else would bother to check out and will gloat about how GNOME has overtaken KDE. The fanboy who screwed the results will get what he wanted."
    author: "theriddle"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-24
    body: "So lets analyze the numbers.\n\nopenSuse:\n 18000 * 9,5%  = 1710\n 27000 * 16.5% = 4455\n 38500 * 21%   = 8085\n\nDifferences:\n 18000 early votes:  9.5% openSUSE\n 9000 middle votes: 30.5% openSUSE\n 9500 late votes:   38.2% openSUSE\n\nGNOME:\n 18000 * 37.5 = 6750\n 27000 * 42.5 = 11475\n 38500 * 45   = 17325\n\nDifferences:\n 18000 early votes: 37.5% GNOME\n 9000 middle votes: 52.5% GNOME\n 9500 late votes:   61.6% GNOME\n\nStrange, but no smoking gun. You need time stamps to prove the \"10000\" strange votes."
    author: "Carewolf"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-24
    body: "The News page at opensuse.org ( http://news.opensuse.org/ ) has this to say about the survey:\n\n### . What\u0092s striking is that popularity of openSUSE almost \n### . doubled form (sic!) roughly 10% in 2006 to close to\n### . 20% in 2007 survey.\n\nOh yeah, really striking...\n\nI'm an openSUSE user myself, and I like the distro quite a lot, as well as their fine developers (I don't like their upper/top management types though).\n\nBut with these DesktopLinux figures, something smells fishy....\n\nAnd also compare them to the figures that Novell's own recently published user survey showed: KDE there was by far the most popular desktop environment still, dispite the lots of \"anti-KDE\" bruhuahua that seemed to originate from their management level for a certain period....\n"
    author: "scratching my head"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "How about OFF TOPIC?"
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "Sure, Off topic, but if Todd didn't bring this up would the 'dot? Seems any not-so good news about KDE they'll ignore."
    author: "Some Guy"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "There is no \"they\" here -- but people you can approach yourself. The stories don't get written automatically. So, if you see something, you can do a writeup and submit it. Of course, you'd have to leave your comfortable anonymity."
    author: "Boudewijn Rempt"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-24
    body: "Funny all these people commenting about it, nothing has been submitted or posted. As if the survey results didn't exist. I'd be surprised it hasn't been submitted already (after a few days now)...the question now would be, the moderators are blatantly ignoring it?\n\nI believe the moderators are taking a stance, if its not good news, its no news."
    author: "Some Guy"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "yeah, and not a little bit. Though it IS interesting..."
    author: "jospoortvliet"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "I do believe Ubuntu must have given Gnome a boost. Still find it hard to believe in those percentages though. In any case, does it really matter? \n\nQt is miles ahead of Gtk and evolving much faster. There's no new major release of neither Gtk nor Gnome coming out any time soon. The 4th series of KDE is right around the corner, from then on, the distance between both desktops will grow bigger and bigger. I'm looking forward to compare both desktops, by the time KDE reaches 4.2."
    author: "random user n. 1309"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "Keeping it simple:\nI belive in the results.\nAll major distros are now using Gnome.\nMost people keep the default without thinking (see windows).\nProbaly the \"keep it simply stupid\" moto is working for users.\n\nCongratulations do Gnome, looks like they are doing something right, but this dosen't mean KDE are doing wrong, as gnome wasn't doing wrong when KDE was first.\n\nLet's not start a flame war saying people stole the results, let this for the american voting system :)\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "\"keep it simply stupid\"\n\nInteresting turn of phrase."
    author: "Soap"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "Definitely."
    author: "Axl"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-23
    body: "Its a joke.\nThe real saying is \"keep it simple\". Even Einstein said this (\"as simple as possible but not any simpler!\".\n\nThe stupid part was added by programmers who thought their programs should be \"stupid\" and thus easy as well. But in the end this is a question of managing complexity - simple is just easier to manage than complex.\n\nI find it hilarious that this guy copies the \"keep it STUPID\" as well - i can only laugh about this ;-)"
    author: "she"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-24
    body: "The original saying was like that :\n\"Keep it simple, stupid.\"\nNOT \"keep it simple and stupid\". \nIt doesn't mean the program should be stupid easy. It's telling the programmer that he's the stupid one, responsible for the bloat of his software.\n\nThe KISS saying has nothing to do with easiness. Slackware is a KISS distribution, not because of easiness (it's turning off newbies) but because its structure is simple. No configuration wizards, no package dependencies management, it's technically simple, but requires a sharp mind that knows the basics of the mechanics behind it to get it to work. The KISS philosophy thinks there's only so much you can do while building a software before it gets to the point of bloat no return. Keep it simple before it collapses."
    author: "anon"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-25
    body: "I did a little analysis on \"web popularity\" of both KDE and Gnome, just as an indicator to take:\nhttp://www.ubuntista.com/2007/08/24/the-truth-about-gnome-vs-kde/\n\nIt comes out that KDE is way more popular in Google, link popularity, etc.\n\n"
    author: "ubuntista"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-25
    body: "Oh my, you really can\u0092t accept it! Check the previous 2-3 yearly surveys, GNOME share keeps growing and KDE keeps going down. Check http://www.google.com/trends?q=GNOME%2C+KDE&ctab=0&geo=all&date=all&sort=0 as well (and I don\u0092t think that so many people are looking for garden gnomes)!"
    author: "Francesco"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-26
    body: "Yeeess, certainly ... and both desktops are losing users since 2004 (at least according to the google trend chart).\n\nMmh, I do not trust these results too much.\n\nOh, and by the way: KDE is waaaay more often in the news than gnome ;-)"
    author: "MK"
  - subject: "Re: Desktop Linux Survey"
    date: 2007-08-27
    body: "yeah, I've seen these google trend numbers before. They're weird, as they say we where more popular in 2004 than now, while we didn't have half as much press work going on by then. Everyone in the community must have noticed we got our act together in the last 3 or 4 years - yet according to google, we lost. That's just wrong..."
    author: "jospoortvliet"
  - subject: "so what (desktoplinux survey)"
    date: 2007-08-23
    body: "I think the quality of KDE is unsurpassed and that's what counts for me. KDE4 will be a major milestone for not only for the free desktops out there.\nKOffice is really shaping up to be a very nice piece of software. Everything looks very promising atm. I do believe, that a lot of Gnome, Vista or OSX-users can't resist to try KDE4 when it's out.\nRegarding Gnome, a lot of Gnome user will be afflicted by doubts, if the Gnome path leads to what they'd like to use, or if an overhauled KDE in it's 4th incarnation is more what the future looks like.\n"
    author: "Thomas"
  - subject: "Re: so what (desktoplinux survey)"
    date: 2007-08-23
    body: "Well personally I'm disappointed that it has a category for something like running Windows apps on Linux, but not for a (ahem) music player or instant messenger. "
    author: "Ian Monroe"
  - subject: "Re: so what (desktoplinux survey)"
    date: 2007-08-23
    body: "Furthermore, from what I've seen from most gnome users I know, many if not most of the apps they use aren't even gnome specific, so changing desktop should be quite easy. Their main apps are Firefox, Thunderbird, OpenOffice and the like. Most use Amarok and some use K3b. Coders use Emacs and Vi. If the desktop environment KDE provides becomes more attractive to them, my guess is that they'd change in no time. \n\n"
    author: "Bxs"
  - subject: "Re: so what (desktoplinux survey)"
    date: 2007-08-24
    body: "Oh, you mean that GNOME users, unlike KDE ones, aren't locked in to their desktop environment?\n\n(I know, just kidding... ;)"
    author: "Dima"
  - subject: "Re: so what (desktoplinux survey)"
    date: 2007-08-24
    body: "I'll reply anyway :P\nIt's not about not being locked to, it's about not taking advantage of. \n\n\n"
    author: "Bxs"
  - subject: "Re: so what (desktoplinux survey)"
    date: 2007-08-27
    body: "LOL that was an aKademy hint wasn't it ;-)"
    author: "jospoortvliet"
  - subject: "Re: so what (desktoplinux survey)"
    date: 2007-08-23
    body: "A stable and easily downloadable KOffice 2 for all platforms might actually help KDE 4 becoming a hit too."
    author: "Axl"
  - subject: "Re: so what (desktoplinux survey)"
    date: 2007-08-24
    body: "+1\n\nI know many people who first get in touch with OSS because of OpenOffice (and Firefox, of course, but I dislike that program because of its memory consumption).\n\nLet's konquer their desktop slowly ;-) KOffice comes with Qt, KDElibs, some other libs and daemons, surely they'll also want to get Amarok 2, and so on and so on."
    author: "Stefan"
  - subject: "Speaking about Amarok... (Magnatune)"
    date: 2007-08-24
    body: "Speaking about Amarok, is there any cool new stuff going on with Magnatune integration? For example auto-fetching of lyrics when buying song/albums (if this isn't done already that is)?"
    author: "Darkelve"
  - subject: "there are more important issues to vote for..."
    date: 2007-08-24
    body: "http://www.noooxml.org/petition"
    author: "semsem"
---
You might remember, that a little over a year ago <a href="http://www.kde.nl/">kde.nl</a> graciously hosted the <a href="http://www.kde.nl/agenda/2006-05-k3m/index-en.php">KDE multimedia meeting</a> (or k3m for short). Whilst we were there, hacking away, the folks from <a href="http://www.source21.nl/">Source21</a> joined us to do some interviews for their open source software vidcast.  If you take some time to watch the video, you&#8217;ll hear from <a href="http://www.kdedevelopers.org/blog/1192">Martijn Klingens</a> (KDE marketing, KDE.nl), <a href="http://vir.homelinux.org/blog/">Matthias Kretz</a> (Phonon) and <a href="http://www.sebruiz.net/">myself</a> (Amarok) speaking about our respective areas of expertise. Don&#8217;t be put off by Martijn speaking in Dutch, Matthias and I both interview in English and subtitles are available for the Dutch pieces, put the subtitles in the same directory as the video for Kaffeine to pick them up.  <a href="http://www.source21.nl/2007/08/05/kde-multimedia-event/">Get the video</a> (<a href="http://kubuntu.org/~jriddell/kde-multimedia-event/">mirror</a>).








<!--break-->
