---
title: "Amarok 2.0 Interview: Jeff Mitchell"
date:    2007-06-14
authors:
  - "aemerson"
slug:    amarok-20-interview-jeff-mitchell
comments:
  - subject: "Oboe Support"
    date: 2007-06-14
    body: "I'm very excited to see oboe-support coming. Keep up, good work!"
    author: "Anonymous Coward"
  - subject: "Re: Oboe Support"
    date: 2007-06-15
    body: "Surely woodwinds were supported before?  I can't believe the developers would have limited instrument support to brass and strings when playing music; that's like trying to censor sax and violins on TV.  It's just not going to work."
    author: "Anonymous Coward II"
  - subject: "Re: Oboe Support"
    date: 2007-06-15
    body: "Hey, don't leave out the percussion section. Without the beat there won't be any music..."
    author: "ne..."
  - subject: "Re: Oboe Support"
    date: 2007-06-15
    body: "Oh, the developers are already very accustomed to hammering on things, so that's no problem."
    author: "Anonymous Coward II"
  - subject: "look n feel"
    date: 2007-06-14
    body: "Gooood wooooork!!!!, hope next Amarok release have nice GUI and look n feel, so not only have good functionality, but also the nice GUI design."
    author: "Teddy"
  - subject: "Sound enhancer plugins"
    date: 2007-06-14
    body: "I tend to plug Amarok whenever I can, but some complains about not having the ability to enhance the sound, or at least not easily. Especially mentioned as an example, is WinAmp's Enhancer plugin.\n\nSupporting this would probably pull over the last few that still haven't seen the light ;) "
    author: "larsivi"
  - subject: "Re: Sound enhancer plugins"
    date: 2007-06-14
    body: "I'm not sure this would be simple with phonon :s\nMaybe I'm wrong."
    author: "shamaz"
  - subject: "Re: Sound enhancer plugins"
    date: 2007-06-14
    body: "If not Phonon itself, then at least one of the backends connected to it should provide this (gstreamer, xine etc.)"
    author: "Paul Eggleton"
  - subject: "Re: Sound enhancer plugins"
    date: 2007-06-15
    body: "Then it \"just\" needs to be integrated seemlessly in amarok. As user I don#t really want to have to know, that I'm using Phonon. "
    author: "Arne Babenhauserheide"
  - subject: "Re: Sound enhancer plugins"
    date: 2008-03-23
    body: "So. What to do, if i want to have some dsp effects in Amarok? :)"
    author: "vaulter"
  - subject: "Don't cut queueing!"
    date: 2007-06-14
    body: "Guys, please DO NOT CUT queueing.  I use it all the time!  Besides, all my friends I've introduced to Amarok have loved it and one of the reasons that they love it is that they can queue lots of tracks without having to alter their playlist!"
    author: "Rudd-O"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "I'm with you! One of the things I sorely miss on other players is the ability to queue tracks when I'm in shuffle.\nOn the other hand, they said that maybe queuing won't be needed in 2.0? Hm.."
    author: "Hagai"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "I've got to second this. Maybe they will have some neat alternative, but I tend to have a fairly large playlist, playing randomly, and it's nice to be able to pull a single song out to be played next without having to change either the list itself, or the playing method."
    author: "Alec Munro"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "I'll third it!! I always play one large playlist in shuffle and then sometimes queue the songs I like to listen to next. \n\n\"So Amarok 2.0's Playlist is being designed to better show information about the current and upcoming tracks while discouraging huge long playlists.\"\n\nPlease don't discourage long playlists (I don't care if it slows down my PC) and don't cut queueing. "
    author: "WPosche"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-15
    body: "Fourth here. Ditto for long playlists and queues, I don't think I'll be using Amarok 2 without them."
    author: "Fede"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-15
    body: "5th\nLong playlists make it perfect to choose your songs. Please don't disencourage it :("
    author: "Mootjes"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-17
    body: "6th\n\nI think I've got to use another media player if you remove this absolutely necessary function :-(\n\nOr please let the \"old\" playlist remain in Amarok so the user can choose what he wants to use ..."
    author: "Robert Bill"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-18
    body: "for you and other posters who said they are using long playlists :)\nthere are several reasons why using a short playlist in amarok is better than a huge one where you just lump all your tracks.\n\ni know that it is kinda hard to break that habit when coming from other, less functional ;) players. i did that myself.\n\nso, please, don't dismiss my suggestion without trying it for a while.\nhttp://amarok.kde.org/wiki/Dynamic_Playlist_Walkthrough\ntry to follow this. don't use a playlist larger than 30 tracks for some time. play around with smart and dynamic playlists. i hope that in a week you will feel no need to use queuing anymore.\nmake sure you use all the neat features of the collection browser to find and dragndrop tracks you want explicitly, outside a dynamic or smart playlist.\n\nand if you feel that the article lacks in some way, please, tell about that (or just add the missing information right away :) )"
    author: "richlv"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "What he said is that queuing will be cut, and mainly because it won't be necessary anymore. Let's see what's their solution for it..."
    author: "NabLa"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "Sorry if you understood that queuing is going to be cut from the playlist functionality - this is simply not the case. I believe Jeff meant \"adding to the playlist\" not \"queuing to the playlist\""
    author: "Seb Ruiz"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "What do you mean by adding to playlist? Seems wierd not to be able to add to the playlist..."
    author: "Jan Braunisch"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "There was an extra bit of information that Amara Emerson asked me that unfortunately didn't make it into the interview (since it wasn't one of his questions).  He mentioned that he hoped queueing wouldn't be cut since he liked it quite a bit, and I said that it was being looked at but was by no means a certainty. I'm not the one working on the new Playlist design so I was passing along early musings that I'd heard.\n\nI wouldn't worry too much about it.  We're aware that this is a much-loved feature in Amarok, and if it was cut I think it'd only be because we'd found a better way to handle it.\n"
    author: "Jeff Mitchell"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "Cool!! thanks a lot for such well thought out concept! :-)"
    author: "Diederik van der Boor"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "phew ... I almost had a heart attack :). Thanks for the elucidation."
    author: "Rares Sfirlogea"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-16
    body: "You people have created a great application, so I guess us users owe you some trust. But I do find it a little worrying when you say that Amarok was never meant to work with large playlists, but rather with a suggestion based system.\n\nSome users may find queuing counter-intuitive, and that's fine. They can currently set Amarok to not shuffle and move the playlist entries up and down as they wish. But many users, me included, want our albums in order. Just because I want to hear song #6 after song #2 on a certain album, does not mean that I want it to appear that way in the playlist. So I use the Queue, which is perfect.\n\nI've tried smart playlists and dynamic playlists, and while I sometimes use them a lot, they are not always better than having a large playlist. Simple reason. Creating a smart playlist is like creating a email filter or whatever. It takes some work. To create a playlist you just need to search the collection and drag&drop whatever you want to listen to for the moment. To create a smart playlist for ALL the music on my hdd would be really painful since it is so much, and since lots of it are not consistently and properly tagged.\n\nIn fact, I think that if possible, you should try to design Amarok so that it COULD support large playlists. Perhaps an option to tone down the suggestion-making.\n\nMaybe someone could do an interview with the people that work on designing the playlist, so we could get some more info there?\n\nAgain, thanks for a good article and a great application. Thanks."
    author: "mirshafie"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "\"Guys, please DO NOT CUT queueing. I use it all the time!\"\n\nYeah, what do they mean I won't be able to queue my songs anymore? The queue manager is (was?) one of the best things about Amarok..."
    author: "Darkelve"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-14
    body: "I also love the queueing in Amarok."
    author: "harold"
  - subject: "Re: Don't cut queueing!"
    date: 2007-06-21
    body: "I too say please don't cut queueing... I have my cd collection turned into mp3s and when I load up a few albums... I tend to queue up about 6 or so of songs i want to hear right away... then listen to the rest of the album in its intended order from the musicians and producers.\n\nI always seem to queue up certain songs on a Friday afternoon for example to get psyched for the weekend...\n\nAnyway my $0.02 i know but important enough for me to reply and support queuing support in Amarok 2.x"
    author: "MJT"
  - subject: "Re: Don't cut queueing!"
    date: 2007-08-20
    body: "Yes indeed, do NOT cut queuing. It's THE most used option I use in Amarok, it's what separates Amarok from all others in my book. \nI sometimes -have- to use Windows and I've searched for a music player which allows queuing like I've grown accustomed to but haven't found one much to my chagrin. I love it when I can wander through my rather large playlist which holds my most valued music for a longer period of time. \nBut depending on the mood I'm in, I want to pick a number of songs -only from this playlist mind you, NOT from the even larger number of songs I've collected in total- and queue them for the 'spur of the moment'. And I'd hate it if this option wasn't available anymore.\nWhat I would like even more, is when you would re-enable the shuffle possibility in said queue manager. Earlier Amarok versions would allow this queue to be shuffled once again, _after_ queueing a number of songs. I loved that! I mourn the loss of that possibility every time a new release is issued and find it's not re-instated. Even contemplated going back a number of releases to regain that.\nI know it sounds silly, but I loved it. Please, please, if you could get that option back, I'd be forever in your debt."
    author: "Jo-Anne"
  - subject: "Discourage long play lists?"
    date: 2007-06-14
    body: "> ..only to discover that they've queued up 6,000 tracks. (..) So Amarok 2.0's Playlist is being designed to better show\n> information about the current and upcoming tracks while discouraging huge long playlists.\n\nI'm not sure about this idea. It sounds good to try other usability concepts, but I hope it won't become an excuse to ignore a particular use-case. That's playing the whole list. I have to admit, nowadays I generally let Amarok build a nice short play list because long lists have lot's of crap. For example a random one, or a specific genre (that browser just rocks, also to cut the number of genre-misspellings).\n\nBut one thing strikes me: before the user discovers these abilities, they'll try to dump their whole list in Amarok, and let it play it. If they can't do that, will they like Amarok as music player..? Will they still try to look further in the program to learn about short-lists?\n\nEither these usability enhancements potentially make such short-lists usage more obvious, or there will be complaints about \"not having a full overview (=full list)\" :-/. I love to hear more about the dev teams thoughts here!"
    author: "Diederik van der Boor"
  - subject: "Re: Discourage long play lists?"
    date: 2007-06-14
    body: "I really hope they don't make it harder for those of us who want to have their whole list available and to remodel it fast.\nI for one pretty much keep All my music in the playlist,and shuffle trough it , and when I remember a song, quicly search for it and play it.\nAlso,sometimes i just want 2 artists , and use OR in the search field, and sometimes I'd like to remove one of them,or to change with another, thing I can do very easy with the large playlist and search feature.\n\nI'd really not want to spend a lot of time just hitting next, or playing with my playlists just for 1-2 songs or artists. Time is of great value for all of us, so please keep that in mind when doing the changes you are going to do.\n\nThank you for the great player that Amarok is so far !"
    author: "George"
  - subject: "Amarok 2.0 enhances large playlist performance"
    date: 2007-06-14
    body: "From a UI prospective, we are certainly optimizing the playlist for use as a dynamic playlist or playing a few albums at a time. Essentially, the playlist is not a collection browser. And as Jeff mentioned, dynamic playlist's functionality should be extended so you could play your whole collection. \n\nBut if you must pretend your using XMMS and add your whole collection to your playlist, Amarok 2.0 will actually preform a lot better doing so. I'm the developer of the new playlist, and one thing I wanted to do from the beginning was ensure that large playlists does not cause the lags and large memory use that it does now. (Well, or at least, unreasonably large memory, obviously its going to use more memory.) The Qt 4 Interview system makes it possible."
    author: "Ian Monroe"
  - subject: "Re: Amarok 2.0 enhances large playlist performance"
    date: 2007-06-14
    body: "> I'm the developer of the new playlist\n\nThen please confirm that cueing will not be removed!\nI start a playlist of 50 or 100 random songs almost every night, and every so often one song will remind me of a few others. Sometimes, I end up with a cue that's longer than the playlist ;-)\nBut, of course, if there is an even better system than cue, I wouldn't complain."
    author: "G\u00f6ran Jartin"
  - subject: "Re: Amarok 2.0 enhances large playlist performance"
    date: 2007-06-14
    body: "Thanks! :-) You guys seam to have a good grip on thinking it out :-)"
    author: "Diederik van der Boor"
  - subject: "Re: Amarok 2.0 enhances large playlist performance"
    date: 2007-06-14
    body: "Then what I'd like to know is this: how will the new playlist idea in Amarok work with full albums?  Some of us (well, a lot of us) still listen to whole albums, and a subset of them (including myself) listen only to full albums most of the time (if not all the time), so I was wondering what's being done to promote the listening of full albums in this dynamic playlist fashion?"
    author: "Matt"
  - subject: "full album in dynamic playlists"
    date: 2007-06-15
    body: "Currently dynamic playlists work on just single tracks at a time really. It is a plan to have the ability to add random albums at a time."
    author: "Ian Monroe"
  - subject: "Re: Discourage long play lists?"
    date: 2007-06-14
    body: "'discourage' sure doesn't sound like 'make it impossible' so I guess it'll work fine ;-)"
    author: "superstoned"
  - subject: "Re: Discourage long play lists?"
    date: 2007-06-19
    body: "Please, don't let the long playlist get out of Amarok!!! Amarok rocks! I have a LOOOONG playlist and don't believe is a good idea to look for a song in my Harddrive each time..."
    author: "Antonio Aguillon"
  - subject: "Re: Discourage long play lists?"
    date: 2007-06-19
    body: "But warning. I would like to use a long playlist but need to be optimized the response timing. I have a collection of 7.000 songs, or so... "
    author: "Antonio Aguillon"
  - subject: "large playlists"
    date: 2007-06-14
    body: "I use Amarok all the time an really love it. \n\nI'm very grateful for Dynamic Collection support, but It is true: \"Amarok's never been designed for extremely large playlists\". Whenever I connect an external usb disk with a collection of > 20.000 mp3 files, I would like to be able to easily play random songs (and once in a while skip one). The most intuitive way is to load 'all collection' as a playlist, which as you say isn't really smooth on a modest laptop :). Is there a better way to play random songs from a collection this size? I'd rather not make a selection myself based on artist/album/... but would like to let the collection surprise me."
    author: "peter"
  - subject: "Re: large playlists"
    date: 2007-06-14
    body: "How about creating a Dynamic Playlist with the All Collection Smart Playlist as a source?"
    author: "palli"
  - subject: "Re: large playlists"
    date: 2007-06-14
    body: "Or use the default \"Random Mix\" Dynamic Playlist...pretty intuitive.  :-)"
    author: "Jeff Mitchell"
  - subject: "ReplayGain"
    date: 2007-06-14
    body: "Are there any plans for native ReplayGain support? Or a way to use a ReplayGain script AND volume control?\nhttp://bugs.kde.org/show_bug.cgi?id=81661"
    author: "Sepp"
  - subject: "Re: ReplayGain"
    date: 2007-06-14
    body: "Maybe in Phonon?"
    author: "Martin"
  - subject: "Long playlists"
    date: 2007-06-14
    body: "Amarok not being designed for large playlists is pure nonsense! For example we have the filter function which is great for huge playlists but not necessary for small ones. The queue function is another example, if you have a small playlist you can easily drag the track in the order you want, but that gets very inconvenient if you have a large one.\n\nWithout a large playlist, I don't understand how I could listen to all the music I like (which is about 2000 tracks) without actively changing the playlist all the time. That would become a PITA after a while. I'ts much easier to drag the music you like to the playlist once and for all and then just turning on random... And if I once in a while want to listen to an album I can just enqueue it and rely on amarok starting to choose tracks randomly after the album is finished.\n\nYou should work on improving every reasonably normal way of using amarok, not on discouraging those ways you don't like."
    author: "Jan Braunisch"
  - subject: "Re: Long playlists"
    date: 2007-06-14
    body: "2000 tracks may be fine.  But we see complaints from people who tried to load up 4000, 6000, 8000 tracks and watch Amarok grind to a halt.\n\nThe reason is that we store playlist undo/redo information...which right now is stored as XML files, which take a while to write out and read.  On top of this we have the XML file for the actual playlist itself, so that when you close Amarok it can pop back up with the same playlist that you had.  We have some patches that speed this up quite a bit in the as-yet-unreleased 1.4.6, but it's still a bottleneck.\n\nIn 2.0 much of this is planned to be stored in the database which will make it much faster.  But you also have to remember that the Context View is planned to be center stage in Amarok 2.0.  Which means that you have a much smaller area in which to display the Playlist, so you need to use that area better than having a ton of horizontal columns all of which are cut off at one character.  Amarok's always been about \"Rediscovering Your Music\" and having the Context View be front and center fits that theme.\n\nAs for the PITA you mentioned...why not pop all of those tracks onto an actual playlist so you only have to queue those up once?  Hopefully in 2.0 Dynamic playlists will be able to read tracks off normal playlists as well as Smart playlists, which I think would solve your issue.\n"
    author: "Jeff Mitchell"
  - subject: "Re: Long playlists"
    date: 2007-06-15
    body: "\"In 2.0 much of this is planned to be stored in the database which will make it much faster. But you also have to remember that the Context View is planned to be center stage in Amarok 2.0. Which means that you have a much smaller area in which to display the Playlist, so you need to use that area better than having a ton of horizontal columns all of which are cut off at one character.\"\n\nThat really depends on screen size. With the current trend of bigger, higher def screens (22\", 24\", 30\", at 1600x1050, 1920x1200, 2560x1600, with panoramic aspect ratio, etc), you should really consider that in 2.0's lifecycle many people will end up having enough space to have a playlist as wide as now or even wider. I for one own a 30\" 2560x1600 widescreen monitor, and I'd like the playlist to allow me to continue using it the way I'm used to in current Amarok releases."
    author: "Fede"
  - subject: "Re: Long playlists"
    date: 2007-06-15
    body: "It really isn't designed for large playlists. This is just a fact. It gobbles memory and starts lagging."
    author: "Ian Monroe"
  - subject: "Re: Long playlists"
    date: 2007-06-15
    body: "True.  But shouldn't it be possible to do a bit of redesign so that a bare minimum of information is held in memory for everything except the current (and maybe next and previous) tracks so that even monster playlists don't actually cause memory hogging?  I hear what you're saying about holding undo/redo information in XML files, but if that's causing such problems with sloth then maybe there's a better way of doing things.  (Surely, improving performance with huge playlists would also make the programme slightly more efficient for normal sized ones, so everyone would be a winner.)\n\nI do love amarok, and this is my only gripe: the developers seem to have a bit of a blind spot with regards to use cases that don't match their own preferences.\n\nIn any case I'm glad to hear that 2.0 will be faster even for those of us who like our huge playlists!  Keep up the good work - gripes aside Amarok is still by far the best player out there."
    author: "Adrian Baugh"
  - subject: "Hierarchical playlists"
    date: 2007-06-14
    body: "I love the hierarchical playlists KPlayer has. I wish Amarok2 will have something like that.\n\nKPlayer lets me see the entire hierarchy or any part of it on the right side of the library, and will only show the informational fields that the expanded folders have.\n\nAnother thing I love about it is you can queue files or entire directories at once, and it will keep directories as folders on the playlist. I think every player should work that way."
    author: "hmmm"
  - subject: "playlist feature"
    date: 2007-06-14
    body: "I have a collection of 15000 songs, with different playlists according to my mood. The biggest one has 6500 songs. I would like to continue to use this usage pattern, including queuing."
    author: "bas"
  - subject: "Re: playlist feature"
    date: 2007-06-15
    body: "'discourage' sure doesn't sound like 'make it impossible' so I guess it'll work fine ;-)\n\nThey're trying to allow other ways of playing your music without the need for such large playlists, not to make your life miserable."
    author: "superstoned"
  - subject: "Dynamic load of storage pools"
    date: 2007-06-14
    body: "> Before Dynamic Collection, you'd have to rescan these other storage pools every time \n> you reconnected; now, Amarok simply knows that that filesystem isn't mounted, and \n> keeps the information for when it is.\n\nI currently have to store the collection.db to prevent it to be zeroed when my external USB drive is off. If the function you described exists, I couldn't find it...."
    author: "Jos\u00e9"
  - subject: "A lot of this seems like a step backwards..."
    date: 2007-06-14
    body: "ack.  I can't say I like the direction I see Amarok 2.0 taking.  I'm going to be a bit harsh, and I apologize for that, but I keep reading these articles and getting increasingly frustrated with what I'm seeing.\n\n> As a result, we see complaints from some users that it slows to a crawl,\n> only to discover that they've queued up 6,000 tracks.\n...\n> So Amarok 2.0's Playlist is being designed to better show information about\n> the current and upcoming tracks while discouraging huge long playlists.\n\nThis should tell you something.  People want long playlists.  Rather than changing the interface to make them *even more* painful in an attempt to \"discourage\" users from doing that, why not fix the actual problem?\n\n> In other Playlist news, I believe queueing tracks will be cut.\n\nPlease don't do this.  I use queueing all the time.  There are some things (e.g. in dynamic playlists) you just can't do without it.\n\n> Although queueing tracks is nice, some people find it counterintuitive ...\n\nThen those people don't have to use it.  Please don't cut a feature just because it's \"too hard\" for Grandma to use.  There are much better ways of dealing with that -- improve the feature itself to make it more understandable, or move it out of the way so non-power-users don't see it unless they're looking for it.\n\n> Other big changes include the Context View. No longer just a browser, it's\n> planned to take front-and-center stage ...\n\nMost of the time, I don't care about the context view.  I don't *want* it on center stage.  I want my collection and my playlist on center stage.  I want the context browser to be in a sidebar, where I can ignore it until I need it.\n\n(Yes, I realize others will have different preferences.  My point is more that there should be an option, otherwise people like me will get really annoyed. ;) )\n\n> ... we're going to support many Collections of arbitrary types -- Internet\n> storage services like MP3Tunes, portable music players, SQL collections (of\n> local files) -- you'll be able to queue up and play music from all or any of\n> these seamlessesly.\n\nThis sounds really nifty.  I can't wait to see how it'll work."
    author: "Des"
  - subject: "Re: A lot of this seems like a step backwards..."
    date: 2007-06-14
    body: "I don't know if you were reading the last comments but your questions were answered. The long playlists are optimized for better performance ... the queueing is being kept or being cut only if a better solution comes up ( I doubt that you could come with such a solution but I'm open to new ideas ). From the articles I read before I understood that the interface was being redesigned but still with keeping in mind that some users would want to change it's look to be like the old one ... so I guess there is an option to hide the context view."
    author: "Rares Sfirlogea"
  - subject: "Re: A lot of this seems like a step backwards..."
    date: 2007-06-16
    body: "Excellent.  If everything you say is the case, then I think 2.0 will be just fine for me."
    author: "Des"
  - subject: "Re: A lot of this seems like a step backwards..."
    date: 2007-06-14
    body: "i don't think an application should be build around such idiotic usage patterns. it makes absolutly no sense to load thousands of tracks into the main playlist when you have fully working dynamic playlist - like amarok 2 should have.\n\nloading stuff in a listview you won't look at anyway just wasts your resources."
    author: "ac"
  - subject: "Re: A lot of this seems like a step backwards..."
    date: 2007-06-15
    body: "Sure it makes sense to have a long list available: I often look for a song of which I don't remember the title or the artist, sometimes just knowing it was somewhere in the first part of the list. Now please tell me how I could find the track without having the option to display a full list. The only other way is to browse the entire collection browser tree, checking every track in every album from every artist, clicking each time to see all tracks of one of the albums. That's just a PITA, and it is dozens of times slower than just browsing over the list.\n"
    author: "Selene"
  - subject: "Re: A lot of this seems like a step backwards..."
    date: 2007-06-16
    body: "First off, I object to your characterization of my usage pattern as \"idiotic\".  You have no idea why I do things the way I do, and for you to make that kind of judgement is, well, pretty rude.\n\nIt's quite possible I have a good reason you just haven't thought of.\n\nNow then...\n\n> loading stuff in a listview you won't look at anyway just wasts your resources.\n\nuhm.  Why would I load it into the listview if I wasn't planning on looking at it?\n\nSometimes, a huge list of songs is exactly what I want.  Perhaps I want to see my entire collection in a list/table format so I can inspect and edit tags.  Perhaps I have playlists full of specific songs I've picked for listening at work.  I don't want a dynamic playlist in this case, because I already know which songs I want, and I don't want Amarok making that decision for me.  Maybe I want some tracks in a genre, but not others.\n\nI commonly build large playlists and then work through them over the course of a few days.  Dynamic doesn't cut it for that, either, because I want to see what's ahead, reorder tracks, etc.\n\nThose are just a few use cases I've thought of off the top of my head.  I'm sure there are more.\n\nYou can make dynamic as powerful as you want, but it's never going to substitute for just picking the tracks I'm interested in.  The playlist needs to scale to support that."
    author: "Des"
  - subject: "Re: A lot of this seems like a step backwards..."
    date: 2007-06-16
    body: "The usecases that you've described, they seem not playlist-oriented, but rather collection-oriented. The fact that now playlist is better place to do it is rather coincidence ( well, I use playlist for that now a lot, too).\nIf all of this would be possible from collection perspective ( label support, greatly improved tag editor),  I think it would be better  then it is now."
    author: "pilpilon"
  - subject: "Old View?"
    date: 2007-06-14
    body: "Personally, I am looking forward to Amarok 2, but for those who don't think they will like it perhaps you could have a \"Classic View\" or some other way to customize it so it resembles the current style."
    author: "Timothy Carr"
  - subject: "Please don't cut xmms-visualisation support"
    date: 2007-06-14
    body: "I've heard somewhere about xmms-visualisation support to be cut, if this is true - please reconsider. True, i don't use the visualisation plugins on a day-per-day basis, but sure often enough to miss them if they wouldn't be there at all... \n\nBy the way, it would be great if you could include once more \"iris 3D\", it's still very cool and configurable, and with libvisual 0.4 it wasn't supported anymore. One of the reasons i still have to use xmms from time to time. Please please please... the vis stuff isn't the crucial part about a Player, but sometimes it's fun to have it."
    author: "Phil"
  - subject: "Re: Please don't cut xmms-visualisation support"
    date: 2007-06-15
    body: "libvisual has a nicely working xmms integration layer, and since that will be depended upon for handling visualisations (as it is already) you will be able to run your ancient xmms visualisations just fine through Amarok :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Youtube intergration?"
    date: 2007-06-15
    body: "I like what I'm hearing about AmaroK 2.0!!! Can't wait!\n\nI just had an idea pop into my head: what if in the contex menu there was an option to look for the video on Youtube? (of course amarok would then stop the track playing and searh for the vid)"
    author: "KoRnholio8"
  - subject: "sound devices"
    date: 2007-06-15
    body: "Any chance of some slightly saner sound-device support? There are two bugs here:\n\n1)If you need to use /dev/dsp1  rather than /dev/dsp (in my case, the former is the big hi-fi; the latter is the LCD monitor's speakers), there's no easy way to do it. Amarok only allows you to choose /dev/dsp, /dev/sound/dsp, or (something else I don't recall). My solution: create a symlink:  /dev/sound/dsp -> /dev/dsp1.\n\n2)When something goes wrong in Amarok (eg the sound device breaking), it very unhelpfully reverts to the defaults. This makes twice the work: first I have to fix the problem, then I have to un-break amarok, and force it to go back to the setting I wanted.\n\n3)When I click the Window-close button, I *really* mean \"Exit the program\". If Amarok wants to go to the system tray, it may do so using the *minimise* button.\n\n"
    author: "Richard Neill"
  - subject: "Re: sound devices"
    date: 2007-06-15
    body: "What you're asking for in 1) and 2) should be addressed very nicely in Amarok2 thanks to Phonon.  See http://dot.kde.org/1170773239/ for some more information about Phonon."
    author: "mactalla"
  - subject: "Re: sound devices"
    date: 2007-06-15
    body: "Yea 1 and 2 are really xine issues or ones specific to our xine engine.\n\n3, just turn off the systray."
    author: "Ian Monroe"
  - subject: "Re: sound devices"
    date: 2007-06-15
    body: "Another solution configuring another sound device is editing the configuration manually. In the file ~/.kde/share/apps/amarok/xine-config I found two lines which I changed to (do edit only when Amarok is NOT running!):\naudio.device.oss_device_number:1\naudio.device.oss_mixer_number:1\nand this makes OSS to use /dev/dsp1 instead of /dev/dsp.\n\nYou can also use ALSA instead of OSS as output plugin. First change and apply this in the configuration and then go down into the details and set mono and stereo to hw:1,0 (may depend on what you find in /proc/asound/cards and /proc/asound/devices).\nYou will now find the lines\naudio.device.alsa_default_device:hw:1,0\naudio.device.alsa_front_device:hw:1,0\nin ~/.kde/share/apps/amarok/xine-config but manual editing is not needed.\n\nThis also shows that a lot of inside information is needed (I hope I do understand the hierarchy rightly that Amarok uses Xine (or some other) and that Xine uses OSS or ALSA (or some other). This may be a bit to much for the avarage end-user.\n\nNow after all this configuring I still agree with you that a simple change of output device should be facilitated. After all when I want to use a different printer I don't have to reconfigure CUPS, I just choose one of a list of availlables.\n\nI don't agree with  Ian Monroe that this to be done in Xine. It is in an Amarok configuration file and Amarok calls Xine with these parameters. Kaffeine and others have their own Xine parameters. There does not seem to be a central Xine configuration file. Maybe there should be. I had to invent this configuring for four or five players (and couldn't solve all of them)."
    author: "Henk"
  - subject: "Re: sound devices"
    date: 2007-06-17
    body: "> There does not seem to be a central Xine configuration file. Maybe there\n> should be.\n\nThere will be with Phonon-xine. :-)"
    author: "Kevin Kofler"
  - subject: "Re: sound devices"
    date: 2007-06-18
    body: "Nice! Even if only KDE applications will use it (and not e.g. MPlayer and Realplayer) this will be an improvement. :-)\n\nWhen you read my earlier post you will not be surprised that I hope it will be possible to change the sound device to be used in an user friendly way. One moment I want to listen to music in my office from my local PC speakers. An hour later I want to listen with my wife to internet radio in the living room via my Xitel HiFi-Link (and it's long cable to my home stereo). That change should be possible in not more than say two clicks."
    author: "Henk"
  - subject: "Re: sound devices"
    date: 2007-06-18
    body: "MPlayer and RealPlayer don't even use Xine, so of course they can't share a Xine configuration.\n\nI believe there are plans to use Phonon (rather than xine-lib directly) in future versions of Kaffeine, which should handle the need for a Phonon-using video player.\n\nAs for codecs, xine-lib (and thus Phonon-xine) should be able to load essentially everything MPlayer can, which also includes RealPlayer's proprietary codecs (though AFAIK using them this way is not exactly compliant with their EULA)."
    author: "Kevin Kofler"
  - subject: "Re: sound devices"
    date: 2007-06-19
    body: "Thanks for all the explanations. I try to grep all of this.\n\nWhat I am missing in this case is knowledge. Until now I could not find an overview of how all these parts fit together. Who uses what. What is used by whom. What is are alternatives. And what do all the parts do.\n\nWhen anybody can refer me to a source of information I would be grateful."
    author: "Henk"
  - subject: "A reason for long playlists"
    date: 2007-06-15
    body: "Hi, \n\nMy reason for having many tracks in the main playlist isn't that I need all of them in there, but that management of the non-smart playlists isn't that nice and I want to be able to reorganice my playlists. \n\nFor example I'd love to see the option to show a playlist in the main window, so i can work on it. Or to have them in a format, where I can better work with them, especially with long ones. \n\nAnd the option to generate a dynamic playlist for a static playlist on the fly... \n\nAnd besides: I queue up tracks quite often, for example when chaning the dynamic playlist: \n\n\"I want to listen to political songs, but before that I want to hear those three songs from my filk-collection\" is then simply: \"Queue up the three songs and switch the dynamic playlist.\" \n\nBut most important: Thank you for Amarok! It is great! "
    author: "Arne Babenhauserheide"
  - subject: "A few clarifications"
    date: 2007-06-15
    body: ">> if I'm correct it, along with the new meta-information/collection infrastructure, are already supporting more than just stores - Jamendo (free music posted by authors) and MP3Tunes' Oboe (online music storage locker) already both have basic support. And adding new and arbitrary information or music sources should continue to get easier as it matures. <<\n\n\nJamendo support is coming along very well (It is the service I have been using a s a testbed for the new service framework). Claiming that we have basic support for Oboe locker might be stretching it a little at this point, but it is definately one of the things I will be looking into, right after the Magnatune store has been brought back to life in the new framework.... :-)\n\nAlso, some more interesting (and often requested) features might make its way into the Magnatune store, see http://amarok.kde.org/blog/archives/411-Meeting-up-with-Magnatune.html . All I need is some more free time!!! :-)\n\n\n"
    author: "Nikolaj Hald Nielsen"
  - subject: "Headless Collection Scanner"
    date: 2007-06-15
    body: "This is Unix and a headless/command line collection scanner with\nthe ability to feed the database is needed.\n\nMany people use fileservers and the server itself is the natural\nmachine to run the collection scanner. Nowadays you have to launch X11 \nin order to be able to do that.\n\nA headless collection scanner would take away a lot of pain.\n\nOtherwise, a very nice player.\n\nChristian"
    author: "Christian"
  - subject: "Re: Headless Collection Scanner"
    date: 2007-06-15
    body: "The collection scanner is already a separate program called \"amarokcollectionscanner\" which can be run directly from the command line"
    author: "Nikolaj Hald Nielsen"
  - subject: "New GUI suggestion"
    date: 2007-06-15
    body: "From what I'm reading, I have the impression Amarok developers are currently trying to optimise the GUI in an innovative way. Personally, I'm not entirely keen on the disencouragement of long playlists, or the abolishment of the queue-function (ok, apparently it wasn't meant this way). But I'd like to suggest a new GUI-approach.\n\nIt is the combination of 2 main frames:\n- One frame including the entire database (or a part of it) of your music (left or up)\n- a 2nd frame which is the current playlist (right or down)\n\nYou can click the 'random' button on the first frame to listen to random tracks from your entire database, then in the 2nd frame an entry 'random - collection' appears\n\nOr you can select songs from your 1st frame, and add them to the playlist(2nd frame).\n\nWhat do you guys think?"
    author: "Mootjes"
  - subject: "Playlists"
    date: 2007-06-15
    body: "I think the big problem with palylists is that we are mixing two different things:\n\n1. the last x, the currently playing and the next y songs\n2. a potentially large list of songs that can easily be modified and stored\n\nI think the two need a different UI. It should then be possible to use 2. as a source for 1., manual and/or automatic (random/sequence)."
    author: "michael"
  - subject: "But I *Liked* queuing tracks!"
    date: 2007-06-15
    body: "I'm not quite sure if I understood Jeff correctly but I think one of my favorite features is going to disappear.  I usually run the library on random favoring least recently played, but sometimes when one of favorites comes up, I want to hear a couple more tracks.  So I queue up 2 or 3 by that artist and then let it go back to random.  I'm hoping I can still do that. \n\ndthacker (who will scrobble his 10,000th play to last.fm this month.  Thanks Amarok devs!)"
    author: "dthacker"
  - subject: "Re: But I *Liked* queuing tracks!"
    date: 2007-06-15
    body: "Earlier in the comments, Jeff said that it was not final, and it they did cut it, it would only be because they found a better solution."
    author: "Amara Emerson"
  - subject: "a lot of people prefer a Qt4-only version"
    date: 2007-06-17
    body: "because they want to install another Filemanager if bash, MC, Thunar, Rox.. is enough in a Window Manager only system with GTK and Qt installed?\n\nWe prefer even Gnome-less programs. Why should we install another VFS (KIO, Gnome-VFS) if we have Fuse?\n\nPlease remove KDE-libs from Amarok 2.0. Hopefully, K3b goes the same way. Even better: structure the code to compile a non-KDE system and everyone will be happy! A lot of people doesn't need KDE integration.\n\nThanks a lot!"
    author: "anonymous"
  - subject: "Please add soundtouch integration"
    date: 2007-06-17
    body: "I've suggested to integrate soundtouch functions (pitch-independent song stretch) into Amarok but this has been refused because of it becoming possible only with Phonon. Now in Amarok 2 there should be Phonon so I would wish to have soundtouch integration."
    author: "Robert Bill"
  - subject: "less context, more collection please"
    date: 2007-06-17
    body: "<personal opinion>\nactually i never really understood the meaning of the contextbrowser. I think amarok is a music player and browser. I'm not interested in what amarok thinks are my favourite tracks or how many times i have played them. I'm not saying such features shouldn't be there, but i just cant understand why they should be prioritzed over playing and organizing my music, which again is what i think amarok is for. \n\nI would love for the collection to take center stage more. For example using the new klistview. \n</personal opinion>"
    author: "viktor"
  - subject: "Features I'd love"
    date: 2007-06-18
    body: "Hi\n\nJust some features I would love to see integrated:\n\n1 A lot of my mp3 files are boosted: ie on the viz the lower freqs go over the roof but other freqs barely get up the floor. This is a problem for those who use lo-end speakers. Img as example:\nhttp://img2.freeimagehosting.net/image.php?7676096e3a.png\nI would love a default feature to identify the songs and correct the gain.\n\n\n2 An equilizer that sets itself to the genre of the song WITH ability to set presets to genres ( such as setting RnB->full bass+treble and pop->party.\n\n3 The append to playlist should position to play the song Immediately After the current item.\n\n4 a components list whether features (musicbrainz-mp3, songalizer, ipodlib) are installed.\n\n5 An export feature for amarok information like rating and such. Right now i always cross my fingers before pasting back .amarok during a new install\n\n6 Ability to rate /problability-favor(in random mode) /set INITIAL score by looking up the songs in last.fm. Useful for those who have discographies of unfamiliar artists (like me :-) ).\n\n7 Cached album art/ lyrics should be copied to their folder/ file by default.\n\n8 A kicker applet with ability to set ratings, scroll, album-art and vizulizations. Something on the lines of kirocker:\nhttp://www.kde-apps.org/content/show.php/Kirocker+Music+Display?content=52869\nHey windows media player has one already :-)\n\n9 Fade in/out on pause. allow this to be set fade time separately  from start/stop fade. Also There is the annoyace that plays the fade out of the prevois song even though I JUST Started.\n\n10 Themable mouseover tooltips. Yellow is a bore. And dont get it to display ALL the tags on the window. And it has a top-heavy look not acceptable for somthing  that jumps out of a panel ;-) \n\n11. \"score-84%   star-4 stars\". Can we have graphical representation for the same in OSD and tooltip? \n\n12 Sync info between 2 amaroks between 2 computers or even 2 OSes on the same computer from an online source and ability to check if other computers have acknowledged the the change. (AAAAAAHHHH-MUST-STOP-IMAGINING-UP-A-KDELIVE!-WEBSITE-phew!!!).\n\n13 Since last.fm requires 15 songs from differnt artistes it would be nice for amarok to set the playlists itself. Im not sure if this is violates last.fm TOS\n\nSo this is my lucky 13 ;-) im not sure if some of them are being implemanted yet but i felt i have to post them. Thanks\n\n"
    author: "Jesvin"
  - subject: "dynamic collection?"
    date: 2007-06-19
    body: "I don't understand point about dynamic collection. About 70% of my collection (as defined in settings > collection) is on auto mounted NFS drives all over my network. If starting amarok and even one of those nodes is down, amarok takes half an hour to load, and any action (or attempt of action) after painfull startup freezes amarok or even crashes it. \n\nBTW there are very similar simptoms if my machine, that holds the actual SQL collection data, is down. Can't there be some kind of exception, let it stop trying after a N seconds/minutes (in both cases), warn the user and simply continue to run, and run *stable*.\n\n\nThis is my first writing here, I just want to say thank you. I've been using this great player from the start, and with all it's quirks and gotchas it's stil by far the best music player around no matter of what OS or platform we're talking about."
    author: "rednow"
---
In the lead-up to KDE 4, <a href="http://amarok.kde.org/">Amarok</a> will be undergoing a number of large changes both under the hood, and cosmetically with the user interface. I managed to interview a developer, Jeff Mitchell, to talk about the things changing in Amarok from the 1.4 stable branch to version 2.0, including the playlist redesign, the context view and the new web services framework. Read on for the interview.

<!--break-->
<p><b>Amara Emerson:</b> Amarok is the flagship audio player for KDE and Linux. What in your opinion sets it apart from other similar players?</p>

<p><b>Jeff Mitchell: </b>I think there are a few things that set us apart.  One was the concept of functional "browsers" which provided one of the early unique characteristics of the program, and still sets it apart in many ways today.  Rather than try to define an all-in-one interface that could handle more and more and more features, they were separated into logical browsers that excelled at providing specific functionality: playlist access, music context, collection browsing, etc.  Because you could easily replace an entire part of the UI with a totally new portion that provided new functionality, it kept things neat and organized.  Plus, your music was always viewable, since the playlist was always shown.</p>

<p>Another thing that sets us apart is innovation.  We've looked at what's bugged us about other players, and have pioneered some concepts that in some cases have been imitated and in other cases are still unique.  To name a few, Last.fm radio was a first on Amarok.  We'd supported MP3 streams for a long time, and we'd supported Scrobbling songs for a long time, so when Last.fm was born it was a natural fit.  Dynamic Collection is (so far as I know) still Amarok exclusive, and it's a godsend for mobile users that have music on a laptop that is sometimes connected to other storage pools and sometimes not.  Before Dynamic Collection, you'd have to rescan these other storage pools every time you reconnected; now, Amarok simply knows that that filesystem isn't mounted, and keeps the information for when it is.  File tracking was also a first on Amarok (although it's been imitated in other players), and it's integrated heavily into many parts of Amarok, and it works well.</p>

<p>Finally, we have a real focus on our users and our community, and we're very responsive to them.  Every idea brought forth by our users to enhance the program is considered, and most of them are implemented if we agree that they're good ideas and they wouldn't involve massive, destructive code changes.  We try to balance this responsiveness with feature creap/option bloat so that our application stays accessible to newcomers but becomes more and more powerful for longtime fans and music lovers.</p>

<p><b>Amara Emerson: </b>There are lots of changes in moving from Qt3 to Qt4, what do you think requires the most effort and time; implementing new features/new UI or porting existing Qt3 code?</p>

<p><b>Jeff Mitchell: </b>I'd say overall implementing new features is more of a time sink.  Qt has some backwards-compatibility classes that work as a stopgap until code is ported, so getting Amarok running on Qt4 didn't take too long initially.  Many of the remaining unported parts have not been touched because those subsystems are being replaced by new ones.  So new features and components is really where our time is going, and that's good, because we have big ideas and big plans.</p>

<p><b>Amara Emerson: </b>How much do scripting languages like Ruby play a role in Amarok stable? Will they take a smaller or larger role in 2.0?</p>

<p><b>Jeff Mitchell: </b>In Amarok stable, scripting languages were used for a few functions.  The first was for plugins.  We don't allow third-party binary plugins, but we expose a huge number of functions via DCOP, which is what plugin scripts use in Amarok stable to perform functions; the Amarok Script Manager would start these running and interact with them when necessary.</p>

<p>The other main function was for proxy behavior.  For various services there does not exist a good way to get data we need; Last.fm for instance had a Ruby script acting as a proxy to handle the web interfacing, which would then pass data to the engine to play.  I know that due to changes in Last.fm's API this is no longer necessary in Amarok 2.0.  DAAP is also another service that Amarok supports that uses some Ruby scripting to handle interfacing with other clients, as scripting languages make speaking HTTP quite a bit easier than coding it all up in C++.</p>

<p>For Amarok 2.0, plugin scripts will still use scripting languages, although they'll now be using DBus instead of DCOP.  I'm not entirely sure in what ways scripting languages will end up being used for other purposes.</p>

<p><b>Amara Emerson: </b>Briefly, what major changes/features are going to be made in 2.0?</p>

<p><b>Jeff Mitchell: </b>2.0 is going to see a major change in the Playlist.  Amarok's never been designed for extremely large playlists; the idea was always to browse and explore your music through suggestions or through dynamic playlists, but at the same time the playlist was a big listview that showed many tracks at once and lent itself quite well to long playlists.  As a result, we see complaints from some users that it slows to a crawl, only to discover that they've queued up 6,000 tracks.  At the same time, you had a limited amount of horizontal space to put all the relevant information in.  So Amarok 2.0's Playlist is being designed to better show information about the current and upcoming tracks while discouraging huge long playlists.</p>

<p>Smart playlists work quite well right now, but Dynamic playlists, which use Smart playlists as sources, only allow randomization of tracks; these will probably be revamped to better fit the model of the new Playlist.  In other Playlist news, I believe queueing tracks will be cut.  Although queueing tracks is nice, some people find it counterintuitive to have tracks on the Playlist play in order, except for those that are queued, which will first play in order...we think the new Playlist design won't require the use of a separate queue anyways.</p>

<p>Other big changes include the Context View.  No longer just a browser, it's planned to take front-and-center stage and contain widgets instead of rendered HTML, which ended up being severely limiting in terms of what we could show and maintainability.  Our web services are getting a major kick -- we're desinging an entire framework to make it easy to add arbitrary web services later, be they music stores, lyrics, guitar tabs, concert information, etc.  There is major work being done on the collection system.  The meta information that is passed around to various parts of Amarok is being streamlined and abstracted; as a result, we're going to support many Collections of arbitrary types -- Internet storage services like MP3Tunes, portable music players, SQL collections (of local files) -- you'll be able to queue up and play music from all or any of these seamlessesly.</p>

<p>Finally, portable device handling is being handled by Solid.  We've been doing work behind the scenes with library developers and HAL developers to ensure that when a device is plugged in (perhaps with the necessary device library installed), Amarok can detect it and just work with it.  I can't think of a device that this won't end up just working for -- on Linux, at least.</p>

<p>Oh, did I mention native Mac and Windows ports?</p>

<p><b>Amara Emerson: </b>Amarok 1.x is dependent on kdelibs. Will this dependency still be there for 2?</p>

<p><b>Jeff Mitchell: </b>Yes, we still depend on kdelibs.  We thought about going Qt-only, mainly for a possible Windows and Mac port, but now that kdelibs is actively being ported to those platforms we didn't see much benefit in losing the features and consistency that the KDE libraries often provide for us.  And kdelibs, while large in terms of disk space, is pretty well self-contained; it's not *that* much to ask for those that generally prefer Gnome or another environment.</p>

<p><b>Amara Emerson: </b>Well you touched on the framework for web services earlier. I guess the framework allows you to add new stores like Magnatune, can you expand on that?</p>

<p><b>Jeff Mitchell: </b>Since we added Magnatune support, we've had a number of various independent online music retailers approach us wanting to provide similar support.  One of our developers and a SoC (Summer of Code) student are actually looking at trying to formalize a standard API for online music stores, to ensure that all can be supported equally.  These stores are good for us, as if tracks are purchased through Amarok we get a small cut of the money, all of which goes back into our project to pay for various costs (and not into any developer's pocket).  Even if such a common API does not happen, the web services framework is becoming quite full-featured, and if I'm correct it, along with the new meta-information/collection infrastructure, are already supporting more than just stores - Jamendo (free music posted by authors) and MP3Tunes' Oboe (online music storage locker) already both have basic support.  And adding new and arbitrary information or music sources should continue to get easier as it matures.  Other information we're currently looking at providing, (besides of course lyrics and Wikipedia), are guitar tablature and concert information.</p>
