---
title: "Oxygen Wallpaper Contest"
date:    2007-08-06
authors:
  - "sk\u00fcgler"
slug:    oxygen-wallpaper-contest
comments:
  - subject: "Public?"
    date: 2007-08-05
    body: "Someone wanted all wallpapers to be posted publicly, is that a bad idea?"
    author: "Martin"
  - subject: "Re: Public?"
    date: 2007-08-06
    body: "I think the people that made the photos should consider putting them on kde-look.  If they don't want to, then why should the contest put them online for them?"
    author: "Thomas Zander"
  - subject: "A suggestion concerning image resolutions"
    date: 2007-08-05
    body: "This is a very nice initiative; just by browsing through the catalogue at http://www.kde-look.org/ one can already imagine the beautiful entries that surely will be entered into this competition.\n\nI do have one suggestion, however: it would be helpful if they were a bit more precise about resolutions and/or aspect ratios.  As an example, they mention \"at least 1900x1200\".  Now, many laptops nowadays ship with *exactly* 1920x1200 widescreens (a 1.6 aspect ratio; note also 1920 <> 1900). Why not use that as the reference value?\n\nAnother good reference value is 1600x1200, since it can be scaled down without AR distortion to the common screen sizes of 1152x864, 1024x768, or even 800x600 (they all have the same 1.333 aspect ratio). And for the sake of completeness, it might be a good idea to also include the 1280x1024 resolution (AR of 1.25).\n\nIn short, by targeting just those three reference resolutions (1920x1200, 1600x1200, and 1280x1024) you could cover the overwhelming majority of desktops and laptops out there!"
    author: "dario"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "But scaling down is a well solved problem to programs like Krita or well Plasma, so there is no need to ask the artist to do it himself, normally."
    author: "Debian User"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "Yup, which is why the OP suggested \"targeting just those three reference resolutions (1920x1200, 1600x1200, and 1280x1024)\".\n\nOf course, one could argue that since people now have displays with 2560x1600, that resolution should replace 1920x1200, and that 2048x1536 should replace 1600x1200."
    author: "Vipul Delwadia"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "And I have three monitors! :)\n\nReally 1920x1200 sounds like a sane default. And of course they'll be able to work with the winning contestant."
    author: "Ian Monroe"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "I think people agree with you.  But note that the contest is *not* 1920 (as you suggest), but an oddball 1900.  Are there *any* displays that use 1900?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "But you can scale the smaller resolutions fine from the bigger one even if the factors are different and uneven in X and Y dimensions.\n\n"
    author: "Debian User"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "That would depend on the viewer and the image. I guess for lots of people it's okay, judging from the people who still buy 16x9 TV sets, even if no station has adopted it in years around here... Personally, I can see clearly when some photo is being stretched horizontally, and I don't like it. For abstract art it's less of a problem."
    author: "fast penguin"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "Well, I think for TV the aspect ration makes sort-of sense. In fact, here (in .nl) RTL started broadcasting in wide-screen just this week. :-)"
    author: "Andr\u00e9"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-07
    body: "And the German public broadcasting stations are airing their news in wide-screen format since some months."
    author: "Stefan"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "I personally have never seen the use of widescreen monitors on computers; I find them to be a way to sell me less screen for the same price (wide screens have a smaller surface than normal screens with the same diagonal). I accept that there are people stuck with such a screen, but to bombard it to a standard for wall papers? I hope not.\n\nI was planning to participate, but if the resolution has to be *that* high, I can't. The resolution of my digital cam is just 1600x1200..."
    author: "Andre"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "I guess the reason for a resolution of \"at least 1900x1200\" is that having a high resolution / high quality source available will ensure that you can continue to work with a high quality image later on. Often enough the artists discover that they would need to either update/improve the wallpaper or use it for a different purpose in a different context. It really sucks if you only then discover that your resolution is just slightly too small for what you'd like to do with it.\n"
    author: "Torsten Rahn"
  - subject: "Re: A suggestion concerning image resolutions"
    date: 2007-08-06
    body: "Or, instead of all this resolution junk, we can make SVG wallpapers ;)\n\nOf course, that doesn't really work for photos, but photos tend to be too busy (for my tastes at least) anyway.\n\npoly-p man"
    author: "Poly-poly man"
  - subject: "closing date?"
    date: 2007-08-05
    body: "it would be nice if you would announce dates for the deadline and for the announcement of the winner.\na bad example is currently http://contest.qtcentre.org/ which has no announcement two months after their deadline.\n\n"
    author: "mirest"
  - subject: "Re: closing date?"
    date: 2007-08-06
    body: "i agree"
    author: "she"
  - subject: "Re: closing date? (RTFA)"
    date: 2007-08-06
    body: "The closing date is posted in the linked to blog;\n\"the rest is in http://pd.ruphy.org/  were you can upload and get all the info you need commits close in 20/8 00:00 GMT.\""
    author: "Thomas Zander"
  - subject: "Icon groups"
    date: 2007-08-06
    body: "I use to have lots of icons directly on my desktop, because I like to have them available only one simple click away. The problem I have with lots of great-looking photographic wallpapers is that they contain too many colors and are therefore to noisy to be used under icons. So I stick to simple images with large parts of similar color and rather dark colors. For a long time I've been dreaming of the following feature: Click down on the desktop. Draw open a rectangular region and a box appears. You can give the box a title like \"Utilities\" or \"Development\" and modify its alpha-channel opacity so that the background shines through. Now you can put icons in this box and they are all automatically aligned within this box. You can move the box around and all the icons instantaneously with it. You might even be able to roll-up a box you don't need at the moment. A little bit like the program groups concept of Windows 3.0 if anyone can rememember this but taken to the 21st century. With plasma and true transparency perhaps this dream perhaps isn't too far away anymore."
    author: "Michael"
  - subject: "Re: Icon groups"
    date: 2007-08-06
    body: "Damn! That is a really good idea! :-)"
    author: "E"
  - subject: "Re: Icon groups"
    date: 2007-08-06
    body: "Plasma already have something like that."
    author: "Luis"
  - subject: "Re: Icon groups"
    date: 2007-08-08
    body: "I think it was definitely talked about, but I don't see it in the current Plasma from SVN."
    author: "Robert Knight"
  - subject: "Leak??"
    date: 2007-08-16
    body: "http://pinheiro-kde.blogspot.com/2007/08/i-double-dare-you.html\n\nHas a leak in the picture directory's, you can get the wallpapers but also pictures of KDE4? aimed looks, dunno if that is the meaning. Won't post the direct link to it but its possible and already posted on some sites."
    author: "awaka"
  - subject: "Leak??"
    date: 2007-08-16
    body: "http://pinheiro-kde.blogspot.com/2007/08/i-double-dare-you.html\n\nHas a leak in the picture directory's, you can get the wallpapers but also pictures of KDE4? aimed looks, dunno if that is the meaning. Won't post the direct link to it but its possible and already posted on some sites."
    author: "awaka"
  - subject: "Best Wallpapers I've ever seen"
    date: 2007-08-19
    body: "Try matching these babies. For resolution and crispness. As well as color.\nhttp://www.socksoff.co.uk/walls07.html"
    author: "Bob Kovacs"
  - subject: "Results?"
    date: 2007-08-27
    body: "Any new about the contributions and the result of the contest?"
    author: "Janus"
---
The <a href="http://www.oxygen-icons.org">Oxygen</a> team has announced a 
<a href="http://pd.ruphy.org">wallpaper contest</a>. Send in your wallpaper 
and it might become part of the default set of wallpapers for KDE 4.0. The 
Oxygen team is solliciting contributions from the community. The jury is 
nobody less than <em>Icon GodFather</em> David Vignoni, <em>The Mad 
Scientist</em> Nuno Pinheiro, <em>The Loud American</em> Ken Wimer and <em>Oxygen Swiss Army Knife</em> Riccardo Iaconelli.





<!--break-->
<img src="http://static.kdenews.org/jr/oxygen-logo.png" align="right" width="180" height="178" />

<p>Nuno Pinheiro, Oxygen's style theme and cursor theme art director and core 
icon designer writes on his 
<a href="http://pinheiro-kde.blogspot.com/2007/08/i-double-dare-you.html">weblog</a></p>

<blockquote>
Yes this is a challenge, you know those extra nice pictures you took 
last time you went on a photo conquest that looks so sweet as your 
wallpaper? yes?
Well then please share them with the entire world in the great 
KDE 4.0 Wallpaper Contest.</blockquote>

<p>Riccardo Iaconnelli, window decoration and style developer and cursor designer 
notes on his <a href="http://blog.ruphy.org/?p=19">weblog</a></p>

<blockquote>We welcome any type of image, 3D art, vectors, photos, and pixmaps.
Note though that because this contest is for wallpapers, we do need big images. 
All the wallpapers must be released under a free license.
</blockquote>



