---
title: "KOffice Developers Meet with KDE Core People for ODF Infrastructure"
date:    2007-04-28
authors:
  - "iwallin"
slug:    koffice-developers-meet-kde-core-people-odf-infrastructure
comments:
  - subject: "just put all stuff in libraries"
    date: 2007-04-28
    body: "niice, btw. cant wait to try the alpha of kde4 that is coming out on thusday !!\n\n#it looks like a trend currently - perhaps if the apps were already designt that the buisness logic is in a seperate dll, we already could have all this stuff.\n\n"
    author: "srettttt"
  - subject: "Re: just put all stuff in libraries"
    date: 2007-04-28
    body: "Um.  On my system, /usr/bin/kword is 8 kilobytes.\nThat's less than the size of this webpage (which is 12KB right now).\n\nStill think the 'business logic' isn't in dynamically linked libraries?"
    author: "mart"
  - subject: "Re: just put all stuff in libraries"
    date: 2007-04-28
    body: "You are looking at the wrong binary.  The KWord-specific code will be found in the library pointed at by /usr/lib/libkwordprivate.so.  In my case, this is about 2.2MB \n\nI don't think the original comment was thought through properly.  All applications provide a certain amount of unique functionality and that code is not shared with the rest of the system.  However, where the authors can identify functionality that is common to multiple applications, that code can be placed in a shared library.  This is what already happens with the KOffice, KDE, Qt libraries and others.\n\nTo see a full rundown, run \"ldd /usr/lib/libkwordprivate.so\"\n\nA quick glance at the output shows that shared functionality includes, amongst other things, and in addition to the Qt/KDE foundations:\n\n- User interface elements\n- Text handling\n- Mail merge\n- Common 'ODF office application' functionality\n- Spell checking\n- Image handling (JPG, PNG etc.)\n- Printing\n- Import and export of various formats\n\nKOffice 2 goes somewhat further through the \"flake\" library, which provides various \"shapes\" ( text containers , Krita images , spreadsheets , videos , vector graphics ) and associated tools to manipulate them in dynamically loadable plugins which can then by used by, I think, all KOffice applications.  "
    author: "Robert Knight"
  - subject: "Re: just put all stuff in libraries"
    date: 2007-04-29
    body: "Uh..  I was looking at the right binary - I think was just being somewhat more pedantic about the comment that the functionality wasn't in dynamically loaded libraries.  libkwordprivate is still a dynamically loaded library - but as the name suggests, the API isn't public.\n\nI think my point was that moving things into dlls (.so files, then) is required, but not sufficient.  It's also necessary for that API to be public.  Hence the meeting to discuss it.\n\nOh, look.  Never mind.  I knew what I meant. :o)\n\n(FWIW, I'm still vaguely trying to follow KOffice development, although I haven't actually done anything for months.)"
    author: "mart"
  - subject: "Re: just put all stuff in libraries"
    date: 2007-04-29
    body: "> Um. On my system, /usr/bin/kword is 8 kilobytes.\n> That's less than the size of this webpage (which is 12KB right now).\n> \n> Still think the 'business logic' isn't in dynamically linked libraries?\n\nYes:\n\n$ objdump -x `which kword` | grep NEEDED\n  NEEDED      libkdeinit_kword.so\n  NEEDED      libc.so.6\n\nIt's a trick to make KWord startup fast.\n\nIf things are in libraries, it doesn't automatically mean those libraries can be shared between applications."
    author: "Diederik van der Boor"
  - subject: "eh what about an xml standard for kplato ? "
    date: 2007-04-29
    body: "eh sorry if my question sounds stupid, but is there any planned standard xml file format for exchanging between planning project software( kplato, gnome planner, taskjuggler)\n\nand by the way, please didn't you find a better name then Koffice, specially with the fact that(hopefully) the next version will be cross-platform. \n\nfriendly "
    author: "djouallah mimoune"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-04-29
    body: "A common and open file format for project planning applications is something that has been under discussion for years, but the task is big enough and the pool of developers working on such applications small enough that there have not yet appeared any concrete and implementable results.\n\nAs for the name: KOffice is getting quite a bit of name recognition. It would be a bit of a waste to lose that through a rename."
    author: "Boudewijn Rempt"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-04-29
    body: "unfortunaltely, taskjuggler is mainly the work of one person \"chris schlaeger\" and kplato is eh dag anderson. it's a pity\n\nand even worst primavera the market leader has no plan to support linux, and wine can't handle it ? really regarding project management software, linux really suck.\n\n"
    author: "djouallah mimoune"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-04-29
    body: "What does being cross-platform have to do with the name KOffice?"
    author: "anonymous"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-04-29
    body: "Because the logic behind the naming scheme of puting a K before any application who use KDElibs is a nosens for me as a user, because i see applications more important then kde then gnome then even linux, because we need to be more open to other  kind of users, who perhaps see this K every where as just stupid, because gnome vs kde war is over. And because we care more about software features then the underlaying technology and toolkit.\n\n\n\n"
    author: "djouallah mimoune"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-04-29
    body: "I personaly like Koffice as a name, because it make me feel like coffe (sss) which is, as far as I know, something at the basement of office work :)\n\nOn the other hand, I don't think that knames are so bad.\nsome of them are just good (kontact, konqueror, kaffeine, kooka, krita ...)\nsome K are not so great, as in akregator, but don't feel bat either.\nand for the kmail, kplato ... I like them because such weird names make me think \"ok, this is an app for KDE, it may be well integrated into my desktop, fast loading, and good looking\". For me, it is important, and I was quite disapointed when I tried to install Kino, and apt asked for hundreds of Mbyte of gtk depedencies.\n\nfinaly, take a look at other vendors. Apple as it's \"i-suite\" whit ipod, itunes, iphoto ..., and I don't see people saying that's stupid.\nMicrosoft as it's MS-things and Windows-things, and nobody complaign ( OK, they often skip the ms or window part and will say open this doc in office, or read this video in media player. but you could say, open this planing file in the kde planig tool, this document in the word processor, and forget the K at the begining of the name )\n\nDevs, if you can't figure out a good name, go for a KWhatMyAppDo, even if not good sounding, it's good understanding."
    author: "kollum"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-04-30
    body: "At last, some support for the old naming scheme !\n\nThe name of a piece of software has a few purposes : tell what the software does, where it comes from, and sound nice / catchy. I wont discuss that last point, as it is strictly subjective, but here are a few examples :\n\nkmail, kontact, konsole, koffice, kdevelop, digikam, kwrite... ? Great.\n\nAmarok, Konqueror, Okular... Harder but you can still get it right.\n\nKopete, K3b, Kross, Kate... Well at least you know it's using the KDE framework.\n\nAkonadi, Avahi, Strigi, Dolphin, Plasma... Yeah, right.\n\nPhonon, Decibel... My first guess would be a VoIP app (\"phone\"), and a sound framework.\n\n\nIt's hard to find a good name. But the current trend (not only in KDE) to use cool names that convey no information is annoying at best, and quite a mistake IMHO. The \"KWhatMyAppDo\" names are really not that bad."
    author: "Moltonel"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-05-20
    body: "yeah! I *still* can't remember which flashy name is for which part of kde4... decibel? I keep thinking it's something to do with sound, not... messaging or whatever. plasma? I can't remember what that does at all right now.\nthe k-names may not be thought up by some fancy marketing team, but they can be *understood*. when I'm trying to find an app for some new thing (like, say, voip) I *like* being able to skim through a list of packages and single out the ones with a K as being the first choices to test. sure, some of them seem silly... but in a way, that just makes them more rememberable."
    author: "Chani"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-04-30
    body: "' finaly, take a look at other vendors. Apple as it's \"i-suite\" whit ipod, itunes, iphoto ..., and I don't see people saying that's stupid.'\n\nspeechless to your remark, definitifly, it is not commun here to have auto critics\n"
    author: "djouallah mimoune"
  - subject: "Re: eh what about an xml standard for kplato ? "
    date: 2007-05-01
    body: "What I was saying is that, not as you do, I don't dislike Knames, that they are useful, and to prove it right, I gave an example from another trend.\n\nI _can_ understand some people don't like strange sounding names.\nBut I do like them, _personaly_.\n\nSo, which is beter or worse ? Depends on your feeling. We will never agree on that, but I respect your view.\n\nIf you whant apps to get cool names, fork the KDE project and rename things as you whant ;-) or you could just take renaming contest ( for instance, I'm proud being one of the Mailody name fathers (as name can have two fathers and no mother) I think it's a good name :)\n\nBut don't forget, it may be bad to change the name of a well established software. User not knowing where to go, feeling laid off, when A just became B (and much cooler :)"
    author: "kollum"
  - subject: "what about chinese format?"
    date: 2007-04-30
    body: "since odf is native format of koffice, does that make it difficult to add support for the emerging chinese document format, as support would add a lot of new users to kde. \n"
    author: "mark cox"
  - subject: "Re: what about chinese format?"
    date: 2007-04-30
    body: "It should be pretty easy --  with a couple of provisos: \n\n* We'd better wait to see if Sun's attempt to merge odf and the Chinese format go anywhere.\n* There are already a few Chinese office suites that seem fairly complete\n* Volunteers interested in this file format need to start working on it. Because, basically everyone on the KOffice team is already spending every ounce of effort they have to spare on KOffice."
    author: "Boudewijn Rempt"
  - subject: "Re: what about chinese format?"
    date: 2007-04-30
    body: "Just to make clear; KOffice already supports almost all alive languages and writing systems. Notable exceptions are top-to-bottom writing systems, but the font-layout meeting in aKademy later this year is aimed at that, and KOffice will automatically benefit from that.\n\nSo, we don't need support for this new standard in order to support the Chinese language or Chinese users. KOffice already works fine for Chinese (and Japanse and lots of other) users.\n\nCheers!"
    author: "Thomas Zander"
  - subject: "Re: what about chinese format?"
    date: 2007-04-30
    body: "And just after I had booked my flight, I discovered that the font layout meeting was scheduled right the day after I leave aKademy again :-(."
    author: "Boudewijn Rempt"
  - subject: "Re: what about chinese format?"
    date: 2007-05-01
    body: "Sounds like you turned into a backpacker :)"
    author: "Sebastian Sauer"
---
<a href="http://koffice.org/">KOffice</a>, the KDE office suite, has always stood behind the <a href="http://www.oasis-open.org/committees/office/">OpenDocument Format</a> (ODF) as an industry standard. Now with KOffice 2.0 around the corner, with OpenOffice.org quickly becoming a new leader, and with Microsoft to release its own so-called "open" format, ODF and the interoperability that it promises is more important than ever. The KOffice developers will <a href="http://www.koffice.org/sprints/odf1/">meet in Berlin during the weekend of May 12th-13th</a> to do as much ODF-centered development as possible. Read more to find out what this can mean for KDE at large.




<!--break-->
<p>First a little background.  In May 2006 the OASIS Open Document Format for Office Applications (the OpenDocument Format) was voted an ISO standard for office documents. KOffice is one of the initiators of the format, and has its own representative on the OASIS technical committee: David Faure. Since KOffice 1.5, OpenDocument has been the default file format, making it the first office suite in the world to implement the format.</p>

<p>During the last year the KOffice developers have been working on the next generation of KOffice, based on Qt and kdelibs version 4. This will bring new qualities to KOffice, like improved text layout, better embedding of new data types, and also support for new platforms like Windows and MacOS X.</p>

<p>With a new platform (qt4, kdelibs4) comes new possibilities. The KOffice architecture has changed much since the 1.x days and therefore much of the old code has to be reimplemented. The KOffice developers will gather at the KDAB offices in Berlin during the weekend May 12th-13th to design the new ODF infrastructure and also implement as much of it as possible during 2 frantic days.</p>

<p>That is not all, however. Many people in KDE recognize the importance of ODF as a standard that ensures interoperability. The meeting will also include the participation of Aaron Seigo, who will represent kdelibs and other parts of KDE. The idea is to create a library like kdepimlibs (perhaps to be named kdedoclibs) that will be usable by all KDE applications to create and view ODF files.</p>

<p>KOffice will always be a prime ODF editor suite, but there is no reason to believe that other applications won't need to create or view simple ODF documents. Tobias König of <a href="http://okular.org/">okular</a> will also be present at the meeting to present his view on what is needed to implement effective ODF support in okular. Other potential uses are the creation of tables or diagrams from <a href="http://edu.kde.org/">kde-edu</a> applications, invoices from Kraft (see last Akademy presentations), and so on. There is no end to the potential possibilities.</p>

<p>This OpenDocument library will not be ready for KDE 4.0, but a preliminary goal is to finish the design and also have the first version of it ready for KOffice 2.1 and KDE 4.1. We will produce reports on the progress during and after the meeting so that the KDE community is kept well informed about the progress. We believe that this initiative will help make KDE the premier desktop for business and school use and also produce a strong platform for years of exciting future development.</p>




