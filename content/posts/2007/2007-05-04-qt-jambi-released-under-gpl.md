---
title: "Qt Jambi Released Under GPL"
date:    2007-05-04
authors:
  - "gsletta"
slug:    qt-jambi-released-under-gpl
comments:
  - subject: "TrollTech"
    date: 2007-05-03
    body: "I don't use Java, and I pretty much hate it, but I know not everyone feels the same way.\n\nThank you TrollTech!"
    author: "Benjamin Long"
  - subject: "Re: TrollTech"
    date: 2007-05-03
    body: "This is pretty sweet.\n\nAt school we learn Java, so this allowes me to start learning Qt before i get to learn C++\n\nThis will give alot of students the chance to make KDE programs."
    author: "acemo"
  - subject: "Re: TrollTech"
    date: 2007-05-03
    body: "And with Java being open sourced as well, this will really turn it into the best choice for developing for KDE using a higher level language. \n\nAlthough I still prefer the performance of C++, I feel much more comfortable with Java now than with the legal uncertainty surrounding Mono and .NET.\n\nPerformance of Java has also improved dramatically with 1.6."
    author: "Leo S"
  - subject: "Re: TrollTech"
    date: 2007-05-03
    body: "Java is a safe language. C++ is an artistic language. Generally you can say what can go wrong will go wrong.\n\nIt is a pity that it does not work yet with gcj."
    author: "andre"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "What's missing in GCJ for this to work? Is it just Generics support? In that case, it should work with the GCJ in the soon-to-be-released Fedora 7."
    author: "Kevin Kofler"
  - subject: "Re: TrollTech"
    date: 2007-05-06
    body: "Java is a SLOW language. And it is safe (as in \"the programmer is so stupid, that he has to be put in a room with rubber handles\"). This \"safety\" is what i HATE in Pascal (though I admit, it's not as bad as Java in this regard).\n\nAs of C++, it's (bleah!) OO. An \"artistic\" language - that would be C. BTW, all the power of C++ comes from the fact, that when you need speed / low mem usage / code clarity / whatever , you can simply cast off all this ugly OO bloat, and write in a clean, procedural, C-ish manner. Java makes it practically impossible."
    author: "zonk"
  - subject: "Re: TrollTech"
    date: 2007-08-23
    body: "\"Java is a SLOW language.\"\n\nHere you're just showing that you don't know what you're talking about. If you look around at language benchmarks you'll find that Java's speed is quite high and actually does rival C++. Java's main failing is that its GC scheme and reliance on heap allocation hogs memory. In large programs this can result in a slowdown.\n\nResearch has shown massive productivity increases using \"safe\" languages because they do so much to improve debugging and maintenance. You can continue to use languages from decades past if you feel like it though, it's just that you will render yourself irrelevant.\n\nC++ has to be the worst language ever invented in terms of code clarity, the fact that you mention this as one of its strengths further suggests your state of confusion.\n\n\"write in a clean, procedural, C-ish manner\"\n\nYou are certainly welcome to do this in Java as well, there is nothing preventing it. Java provides you with the same basic primitives as C (except structs), just make all of your methods static and treat classes as namespace groupings rather than data types to be instantiated. Few people are dumb enough to do this, however."
    author: "Nate"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "Java is probably a better language than C++.  One of the reasons is that Java code must be true object oriented while this appears optional with C++.\n\nIt's use has been limited by the fact that Sun never released a compiler that produced binary code and the VCISC processors never showed up either.  The GCJ compiler can greatly improve the performance of Java by compiling it to native binary code.  Therefore, I am quite disapointed to learn that the Jambi library doesn't work with GCJ.  We can only hope that GNU will fix this.\n\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "Saying that object orientation is the One True Programming Style is wrong for the same reason that it was wrong for procedural programming back in the day.  On the other hand there is less of Java to learn than there is of C++.  But then you don't need to learn all of C++ to use it..."
    author: "Michael Pyne"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "> But then you don't need to learn all of C++ to use it...\n\n...and we all love insecure software. :|"
    author: "Carlo"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "<p>Java is a terrible language. Let's pick a few issues at random: Java throws away part of the types during compilation. Java doesn't support metaprogramming. Java doesn't support operator overloading, so e.g. == doesn't mean what it should mean, but rather pointer equals. Java doesn't support smart pointers. Java doesn't support RAII or any other resource management scheme, making it hugely vulnerable to leaks.\n\n<p>Do I have to go on? :) My list is very long. Not that C++ is perfect ---  the pletora of implicit conversion and the lack of typeof operator comes to mind, but for what I loosely call the statically typed languages, I have found none better.\n\n<p>Just my opinion. Java is a nice introductionary language for school and such. Like Pascal was.\n"
    author: "Esben Mose Hansen"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "In Java == means exactly what it should mean, i.e. are two object the *same* object? .equals compares two objects for equality and is class-specific. I don't see any problem with this."
    author: "Flavio"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "It's just a matter of the situation. If you compare two strings for instance, you don't want to know if they are the exactly same object, but if they just are equal. If you care about string references at all, an intelligent memory management should instead hold just one string-object instance for the same string and split it up if someone modifies his local reference to the string, so == would actually mean equal by design.\n\nThe same appears to many other different classes. \n\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "actually, the object class ( mother of all classes) has the \"equals\" method. You can define object equality by simply overriding this method. It actually has BETTER overriding of an \"operator\" ; its just that the implementation is in a more OO way.\n\nJava is a strongly typed language ( or at least it used to be until IBM mucked with it in 1.5, introducing the stupid notion of annotations). This way there is less likelihood of corrupting the thread heap with pointers. Exceptions are either those that are thrown by programmer or are well defined runtime exceptions ( I haven't seen any uncaught exception thrown as just 'Exception' or 'Throwable').\n\n\nC/C++, OTOH, we all know, can just do a 'segmentation fault' that explains, well, nothing."
    author: "v m"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "> It's just a matter of the situation. If you compare two strings for instance, \n> you don't want to know if they are the exactly same object, but if they just \n> are equal. If you care about string references at all, an intelligent memory \n> management should instead hold just one string-object instance for the same\n> string and split it up if someone modifies his local reference to the string, \n> so == would actually mean equal by design.\n\nhmmmmm. nice description of how it is done in java....."
    author: "marc"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "Really? I see a lot of bugs stemming from \"cheese\" == my_cheese_string being false sometimes, but not always. Most other languages use === of object-id comparison, while == test equality, and I find this a far superior approach.  Note also that .equals() is asymmetric... (a.equals(b) == b.equals(a)) is false in some cases. Another behaviour that is a frequent source of bugs.\n\nBut I could go on for days about the shortages of Java, and just get everyone down. So I won't. Suffice it to say that Java is only a nice language if you like your language to be DAMN LIMITING! (sorry Linus!) ;)\n"
    author: "Esben Mose Hansen"
  - subject: "Re: TrollTech"
    date: 2007-05-10
    body: "Let's see what the JDK API documentation has to say about Object.equals():\n\n\"It is symmetric: for any non-null reference values x and y, x.equals(y) should return true if and only if y.equals(x) returns true.\"\n\nSo, the default implementation of the equals() method is symmetric. You are of course free to override it in your own classes by an assymetric one, but that's a design choice of yours, not a weakness of Java (there are a few classes in the JDK that override equals() by an assymetrical operation, but this is clearly documented in each case AFAIK).\n\nWhat about the specific case of String ? Again, from the API Documentation for String.equals():\n\n\"Compares this string to the specified object. The result is true if and only if the argument is not null and is a String object that represents the same sequence of characters as this object.\"\n\nFinally, there's the \"==\" operator. That one basically tests if two objects are identical. Note that this is quite different from two Strings having the same content !\n\nWhy is \"==\" in most cases have the same behavior as \"equals()\" for Strings ? Because of internal optimizations.\n\nSo there are no \"Java weakness\" in this case - there's simply a misunderstanding on what the operator and methods mean. Or in other words: before bashing Java (or any other language/platform), learn to use it first :)"
    author: "Kamina"
  - subject: "Re: TrollTech"
    date: 2007-05-05
    body: "I have to say I like D's approach to this: the '==' operator for equality, the 'is' operator for identity."
    author: "Jeff Parsons"
  - subject: "Re: TrollTech"
    date: 2007-05-05
    body: "I have to say I like Python's approach to this: the '==' operator for equality, the 'is' operator for identity. ;)"
    author: "panzi"
  - subject: "Re: TrollTech"
    date: 2007-05-05
    body: "...and as PyD (http://www.dsource.org/projects/pyd) gets better, the two make such great friends! :P"
    author: "Jeff Parsons"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "g++ actually supports typeof. To bad KDE insists on also supporting crappy proprietary compilers."
    author: "Kevin Kofler"
  - subject: "Re: TrollTech"
    date: 2007-05-05
    body: "You see, the most important to support is standards and the C++ standard only has __typeof__.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: TrollTech"
    date: 2007-05-06
    body: "Uh, the C++ standard doesn't have __typeof__ either, it's a reserved identifier (double-underscore), the compiler can do anything it wants with that, g++ happens to actually do something useful with it."
    author: "Kevin Kofler"
  - subject: "Re: TrollTech"
    date: 2007-05-06
    body: "I totally agree. GNU CC work on most (if not all) platforms, so there is no real need in supporting other compilers. Not only that it WORKS on all major platforms, it is also the default compiler on most of them (GNU/*, OS X, *BSD)."
    author: "Filip Brcic"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "Java works pretty well. It does not support some features and thats fine. A good language is a restrictive one."
    author: "Karsten"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: ">Java doesn't support smart pointers\nBecause it does not need it?\n\n>making it hugely vulnerable to leaks\nYou are saying that C++ is less vulnerable to leaks than Java???\n\n>lack of typeof operator \nit has the 'instanceof'\n\n>Java throws away part of the types during compilation\nIt is statically typed and you have all the type in run time, except for generics. You can even list all the types, methods and attributes of a object in runtime.\n\n>Java doesn't support operator overloading\nMany people think this is a feature :)\n\n"
    author: "Bruno"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "> One of the reasons is that Java code must be true object oriented while this > appears optional with C++.\n\nJust because you must put functions into classes in Java? Use one class, make all functions static, and there goes your object-orientation :)"
    author: "Frank"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: ">Java code must be true object oriented\n\nI'm not trying to be agressive, but you could not be more wrong. Object Oriented *Syntax* does not forces a TRUE object oriented model. Creating a good OO model takes much, much more than using the OO syntax some language offers. \n\nIt's like saying I know how to move the chess pieces therefore I'm a good chess player."
    author: "Bruno"
  - subject: "Re: TrollTech"
    date: 2007-05-06
    body: "With C++, there are cases where you have the choice of using STL and producing true OO code, or writing specific code which must be maintained (i.e. modified if the relevant parts of the program are changed).  With Java, you do not have this choice and the equivalent of STL is built in.\n\nSo, I said that writing true OO code in C++ is optional but you must do it in Java.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: TrollTech"
    date: 2007-05-06
    body: "There is nothing object-oriented in STL, nor true nor otherwise. \nNever ever try class MyVector : public std::vector<int>{}; :)"
    author: "pilpilon"
  - subject: "Re: TrollTech"
    date: 2007-05-06
    body: "You are still so wrong. OO is about design, not about languages."
    author: "Bruno"
  - subject: "Re: TrollTech"
    date: 2007-05-05
    body: "\"The GCJ compiler can greatly improve the performance of Java by compiling it to native binary code.\"\n\nI think JDK6's HotSpot outperforms native GCJ code in most cases, sometimes dramatically. I haven't heard any reports of it being faster. People use it because native binaries are more convenient in many cases, and that GCJ's runtime library, unlike Sun's JRE so far, has been freely distributable. If it's performance you want, you ironically have to go for bytecode :-)"
    author: "Haakon Nilsen"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: "> I pretty much hate it\nI love it!"
    author: "anonymous"
  - subject: "Re: TrollTech"
    date: 2007-05-04
    body: ">I don't use Java, and I pretty much hate\n\nThere are other language which run on top of the JVM, which are able to call Java API so they should be able to use Qt Jambi..\nI'm especially thinking about Scala which is a very nice language.\n"
    author: "renoX"
  - subject: "Thanks! "
    date: 2007-05-03
    body: "Great. I hope I'll be able to check it out soon! \n\nMany thanks to the good trolls! "
    author: "Arne Babenhauserheide"
  - subject: "More Apps"
    date: 2007-05-04
    body: "It will be more apps for KDE and it speaks for itself...\n\nDeveloppers have choise : C++ python or Java, all languages are opensource"
    author: "Gogast"
  - subject: "Re: More Apps"
    date: 2007-05-05
    body: "Though I will be avoiding Java and Python apps like hell. I have a PC that is slow enough, even without killing it with Java..."
    author: "zonk"
  - subject: "one more reason to leave C++ behind "
    date: 2007-05-04
    body: "cool. also I would prefer Swing for Java only applications, I think\nthis might be good for KDE.\n\nnow people can write KDE/QT apps in many cool languages including Java, Ruby and\nPython. no need to use C++, except for performance or low level stuff.\n\nthat all should help people developing much more application in much less time."
    author: "Markus"
  - subject: "Re: one more reason to leave C++ behind "
    date: 2007-05-04
    body: "How do you port from Swing to QT?"
    author: "Karsten"
  - subject: "Re: one more reason to leave C++ behind "
    date: 2007-05-05
    body: "Right now there is only a limited migration path between swing and Qt Jambi. On Windows and X11 it is possible to run both the Swing event loop and the Qt Jambi event loop simultaneously and you can have toplevel widgets from both frameworks open and \"alive\" at the same time (I won't go into details on modallity or focus/activation issues, but...), so it is to some degree possible to port your Swing GUI to Qt Jambi one toplevel at a time and keep the underlying application compiling and running while doing so.\n\nIdeally we would be able to host Swing widgets inside a Qt Jambi widget and vice versa, but we haven't begun the work on this yet. It is however something we intend to look into in the near future."
    author: "Gunnar Sletta"
  - subject: "Re: one more reason to leave C++ behind "
    date: 2007-05-07
    body: "evidence consistently points to java being more performant that c++ with a significantly lower cost for development\n\nhttp://www.idiom.com/~zilla/Computer/javaCbenchmark.html\nhttp://www.javaworld.com/javaworld/jw-02-1998/jw-02-jperf.html\nhttp://www-128.ibm.com/developerworks/java/library/j-jtp09275.html"
    author: "Michael"
  - subject: "Integration with Java"
    date: 2007-05-04
    body: "I am a Java developper, I am interested in Qt and QtJambi. But in my opinion QtJambi is poorly integrated with Java, it's only a \"binding\" between Java and C++ API. They should have taken advantage of Java specific things and make their API closer to what's done in other toolkits like SWT and Swing. Some examples:\n- Event handling: \"connect\" method (not type safe) vs anonymous classes (observer/observable design pattern)\n- I18N: \"tr\" vs resource bundles \n- Useless APIs because already in Standard Java: SQL, Thread, Network, IO, XML... "
    author: "Garcia"
  - subject: "Re: Integration with Java"
    date: 2007-05-04
    body: "Maybe not so useless to take advantage of the standard plattform solutions.\n\n\n"
    author: "Karsten"
  - subject: "Re: Integration with Java"
    date: 2007-05-04
    body: "What you are saying appears more to be a case of having some overlap, rather than poor integration. Whether this is an advantage or not depends on which angle you're coming from. Anyone already familiar with Qt will probably be very pleased with being able to use all his familiar \"useless APIs\", while someone already familiar with the standard Java API would have to relearn a lot.\n\nPersonally, I think the standard Java API is horribly over-engineered and cumbersome to use, so I'd pick the Qt API anytime.\n\nOT: From a user's point of view what I'm still missing is Qt theme integration for Swing/SWT applications..."
    author: "Arend jr."
  - subject: "Re: Integration with Java"
    date: 2007-05-04
    body: "Event-handling-wise, I've always found the Qt way to be far better than the extremely annoying observer design pattern used in Swing. It's really a matter of personal preference, but for me that's one of the things I least like about the Swing API: the fact that I need Listener classes for everything, and therefore end up creating classes that break up my cohesion in a very annoyed way. Let's not even get into what a nasty idea anonymous classes are for code reuse and such. It would be much better if they made connect type safe, though.\n\nThere are advantages to the useless APIs; namely, they're cohesive. Most of the Java APIs, in my opinion, are fairly bad, whereas most of the Qt APIs, again in my opinion, are very very nice, well thought-out, and polished. So then you have a choice.\n\nAs for tr vs resource bundles, they're a different approach. Qt Translator provides a very nice environment for doing translations based on the tr function, though."
    author: "Anonymous"
  - subject: "Re: Integration with Java"
    date: 2007-05-04
    body: "Garcia, thank you for this feedback. I am a developer on Qt Jambi and we are very interested hearing about concerns such as this. I will try to address your concerns one by one with some clarifications as well as my personal opinions about this:\n\nQt Jambi is an alternative to Swing and SWT, and does not attempt to adapt the specific patterns chosen by these toolkits, but rather bring the API designed for Qt into the Java world. Personally, I find the Qt/Qt Jambi approach superior to approaches in either Swing or SWT. That being said, Qt Jambi does in fact \"take advantage of Java specific things\" to a higher degree than either of these toolkits, if we're discussing Java as in the language, and not the framework that is shipping with it. One example of this is proper Java enums rather of integer/String constants, which make autocompletion more useful and code more typesafe. A lot of work has in general gone into tailoring Qt Jambi to be more than \"only a binding between Java and C++\" but an actual first class Java citizen. \n\nIt is true, as you say, that the connect statement is not statically type safe, but \n1. This approach is the same in Qt for C++, and has been proven to work well by almost fifteen years of Qt versions. Emit statements are, as in Qt, completely type safe at compile time.\n2. The initialization code (where the calls to connect are typically located) is always in the code path, which means runtime exceptions in this code will be discovered on first program run. Thus, in practice, the mechanism has equivalent type safety to statically checked mechanisms.\n3. Signals and slots handles a superset of what the listener pattern handles, and it is easily doable to implement listeners on top of signals and slots if you feel more comfortable to work with this familiar pattern. \n\nFinally, there is a lot of functionality in Qt Jambi which is available in standard Java, as you say, but: the API design and structure of Qt Jambi components is quite unique and hopefully more intuitive, and I think you will find a lot of functionality in these parts of Qt Jambi which is not available in the standard class library, such as a resource engine which is integrated with the rest of the I/O subsystem, instead of just with selected parts of the I/O API.\n\nSo the bottom line, I guess, is that Qt Jambi is different from what is available already, and many people (myself included =)) find it more pleasant and intuitive to work with than the available APIs. In addition, there is a lot of functionality in Qt Jambi which is not available in current toolkits.    \n\n(Ps. Just a little clarification: Qt Jambi uses the standard Java thread API and does not attempt to replace this with anything.)    "
    author: "Eskil Blomfeldt"
  - subject: "Re: Integration with Java"
    date: 2007-05-05
    body: "Thanks for this explanation"
    author: "stephan"
  - subject: "how to download??"
    date: 2007-05-04
    body: "can i download the beta or do i have to work for a company?? because thats what i get asked for, when i want to download qt jambi."
    author: "chris"
  - subject: "Re: how to download??"
    date: 2007-05-04
    body: "You can download it. \n\nI just said the truth: \nCompany: -\n\nDownload works fine, and they ask you, if you want the GPL versin, or the Preview-Licensed Version. "
    author: "Arne Babenhauserheide"
  - subject: "GREAT"
    date: 2007-05-04
    body: "I was not expecting this to happen so fast :)\nThank you Trolltech !!!!\n\nOne can use several language on top of the JVM (the same way you can use c#, vb#, j# or something else with .net)\nI really hope it will be possible to use jruby, rhino (javascript) and groovy with Qt Jambi ! (I did not mention Jython since it is in a bad shape :s)\n\nThis would be an alternative to korandum4 and kjsembed :)\n\nAbout the problems with gcj, I'm sure they will be solved quickly :). And I'd like to say that there is a great gcc SOC project to integrate OpenJDK's javac on gcj (made by a gnu classpath developper).\n\ngo java ! :)\n\n\n\n"
    author: "shamaz"
  - subject: "To memory hungry..."
    date: 2007-05-04
    body: "I tried the demo and eaths 106 mb of ram.\n\nNo thx.\n"
    author: "aam"
  - subject: "Re: To memory hungry..."
    date: 2007-05-05
    body: "I was thinking that this sounds way too much so I investigated a little bit. On Windows the webstart demo ends up on 85Mb on my machine using JDK1.6. Again, this is far more than expected so I tried running the demo launcher from the command line. From the command line it takes about 33.5Mb when by the time it comes up and  that is not so bad. \n\nSo why is the webstart demo so much more expensive... Well, its partly because Java Webstart is implemented using Swing and is rather memory hungry on its own ;-) I checked the memory meter just before accepting the \"do you trust Trolltech Developer enough to run this unsafe application\". At this point Qt Jambi is not even loaded and webstart was eating 30 Mb.\n\nTo sum up, the number you see for memory use when running the webstart demo is not really a fair estimate of how Qt Jambi actually behaves."
    author: "Gunnar Sletta"
  - subject: "Re: To memory hungry..."
    date: 2007-05-05
    body: "Java doesn't work like you'd expect anyway. It has a heap that's independent of the OS and it'll continue to consume memory until this heap limit is met. Then the GC will kick in.\n\nIt's not like explicit memory management and shouldn't be compared to it."
    author: "Alistair John Strachan"
  - subject: "Re: To memory hungry..."
    date: 2007-05-11
    body: "Which means that you have the joy of having to choose the 'correct' heap size..\n\nHopefully now that the JVM has been GPL'd, its GC and Linux's memory manager will be modified to dialogue with each other."
    author: "renoX"
  - subject: "Re: To memory hungry..."
    date: 2007-05-05
    body: "That's one of the reasons I stay away from Java apps..."
    author: "zonk"
---
Yesterday Trolltech released the <a href="http://www.trolltech.com/developer/downloads/qt/qtjambi-beta">second beta of Qt Jambi</a>, the Qt API for Java. With this release we also released the source code including the Generator under GPL, opening the option for making KDE libs accessible to Java.  Though it does not work together with gcj, it does work together with the open source Harmony Virtual machine and runtime. For more information look at <a href="http://labs.trolltech.com/blogs/2007/04/26/qt-jambi-beta-2-released/">my release blog</a> or the official <a href="http://www.trolltech.com/company/newsroom/announcements/press.2007-04-20.7347260521">press release</a>.


<!--break-->
