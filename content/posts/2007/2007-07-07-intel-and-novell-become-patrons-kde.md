---
title: "Intel and Novell Become Patrons of KDE"
date:    2007-07-07
authors:
  - "fgiannaros"
slug:    intel-and-novell-become-patrons-kde
comments:
  - subject: "dependency"
    date: 2007-07-07
    body: "good to see that you get more money, but please stay independent and dont be afraid to critisize your sponsors when it becomes obvious that freedom is left behind on the path to profit.\n\n"
    author: "Hannes Hauswedell"
  - subject: "Re: dependency"
    date: 2007-07-07
    body: "Promised.\n\nI think having more corporate sponsors is good because it shows exactly this independence (which is also a core strength of the KDE community). Not being dependant on one or the other vendor is extremely important to us, and to the way we work.\n\nThe reason why Intel and Novell are Patrons of KDE is, assumably, not that they want to influence the KDE community, but that they think that having a functioning community is important for those companies as well, and that being a Patron of KDE is a good way to help the community being effective and develop good and Free software."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: dependency"
    date: 2007-07-08
    body: "I agree with you're novel statement, I can see KDE4 being highly popular with corporate users (The Semantic Desktop) \n\nI'm not too sure what intel thinks its getting from this, perhaps its genine charity. Either way good on both of you :)"
    author: "Ben"
  - subject: "Re: dependency"
    date: 2007-07-08
    body: "Intel Classmate PC will use KDE as default linux environment,\n(look the screenshot from aKademy Education Day)\nso the better is KDE the better will look their project...."
    author: "Mat"
  - subject: "Re: dependency"
    date: 2007-07-07
    body: "I'm so pleased that once and for all it knocks on the head the idea that Novell are anti-KDE.  Hopefully it will end the Kremlinology of each new release of openSUSE (and antecedents) regarding the batting order of desktops for installation. "
    author: "Gerry"
  - subject: "Re: dependency"
    date: 2007-07-07
    body: "> Hopefully it will end the Kremlinology of each new release of openSUSE (and antecedents) regarding the batting order of desktops for installation.\n\nI'm not really sure what this sentence does want to say :-). Don't expect changes to the desktop selection dialog during installation of the \"fat edition\" (6 CDs/DVD/boxed version/FTP). But openSUSE 10.3 will introduce additional new one CD installation media which contain only one single desktop, making you choose your desktop before download, so it will be comparable to Kubuntu and Fedora's KDE spin."
    author: "binner"
  - subject: "Go Home Novell"
    date: 2007-07-07
    body: "Does doing good compensate for the bad? No, I don't think so. Giving money to KDE is nothing when you stick with Microsoft to kill ODF.\n\nMany thanks to the Suse founders, but what a pity you sold out. I guess those guys should get KDE awards one day, they did the most to push KDE back then.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Go Home Novell"
    date: 2007-07-07
    body: "Oh, and great that Intel joined. This is a pure Intel machine that I bought because of the great Intel support for everything from CPU, chipset to graphics to WLAN.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Go Home Novell"
    date: 2007-07-07
    body: "yeak, promoting OOXML instead of ODF is <censored>"
    author: "Dolphin-fanatic :)"
  - subject: "Re: Go Home Novell"
    date: 2007-07-07
    body: "It's fortunate, then, that no-one in the OSS world has done such a thing. "
    author: "apokryphos"
  - subject: "Re: Go Home Novell"
    date: 2007-07-07
    body: "Great Intel support for WLAN...\nSure."
    author: "Maurizio Monge"
  - subject: "Re: Go Home Novell"
    date: 2007-07-09
    body: "Find me another 802.11g product with wpa2 whose driver is in the kernel.  I used to use prism54, but the last of those with hardmac was long ago and didn't do wpa2.\n\nSure, newer Intel wlan models had some issues with binary userspace programs but my 2200bg (with pci to mini pci adapter) works great in my desktop with nothing but the stock kernel.  Besides, it looks like the binary userspace program thing will soon be cleared up ( http://www.heise.de/english/newsticker/news/85146 ).  I really don't see the problem here."
    author: "MamiyaOtaru"
  - subject: "Re: Go Home Novell"
    date: 2007-07-11
    body: "Intel being the only one supporting wpa2 does not make their driver work.\nThat's simple.\n\nWhen i use wireless (latest development driver, previous do not even work) i get disconnected after 15 minutes, after which i have to reboot (rmmod/modprobe is not enough).\nThat's simple."
    author: "Maurizio Monge"
  - subject: "Re: Go Home Novell"
    date: 2007-07-07
    body: "Don't mistake Novell to be a singular entity.  Like any company, they're made of many people, and many separate divisions.  Becoming a patron of KDE doesn't 'compensate' for the Microsoft deal (it is entirely possibly they noticed a little loop-hole in the wording of the contract they plan to exploit eventually, like what a lot of people thought the GPLv3 may cause, or maybe the people responsible for the deal are just flat out insane, no way to really know at this point).\n\nBecoming a Patron of KDE shows that at least powerful elements in Novell are still serious about supporting Free Software, as well as showing their continued support for the Desktop Environment that is clearly superior to all the others. ;)\n\nI say Novell should be congratulated for making this excellent step.  Everyone makes stupid mistakes (except KDE devs of course ;), lets hope thats all it was and just try to move on.\n\nIntel is the one I'm still surprised about, it seems like just yesterday they were the evil closed off empire while their main competitor was the one friendly to the OSS community, though it seems in recent times that it has pretty much reveresed entirely. Intel has been doing quite a lot of great things for the community such as open sourcing their graphics drivers (don't forget hiring people to work on Xorg!) and contributing their powertop tool."
    author: "Sutoka"
  - subject: "Not a surprise"
    date: 2007-07-09
    body: "I'm not surprised that Intel is on the list.  I remember a time when everyone was cheering on Microsoft to topple the big evil IBM.   Today IBM is the good guys.  Even while most open source people can't stand Microsoft today, once in a while they do something good and important to open source (generally in the legal areas).\n\nCompanies change all the time.   \n\n"
    author: "Henry Miller"
  - subject: "Re: Go Home Novell"
    date: 2007-07-07
    body: "First take a look at some FAQs <http://opensuse.org/FAQ:Novell-MS>. Let's summarise:\n\n* doing good -- yes, they persistently do good. They're one of the biggest contributors to OSS software ever.\n\n* They're one of the biggest and most dedicated backers of ODF, and they're very commited to it. Apart from Sun, Novell have the most developers working on OOo, right?\n\n* \"Giving money to KDE\"? Erm, SUSE employ more KDE developers than any other Linux distribution. Didn't you read the story?\n\nJust because a deal involves money it's not to say that anyone \"sold out\". These are just again typical emotive statements that don't have any grounding. There might be bad things about the deal, but you certainly haven't even touched upon them."
    author: "Francis Giannaros (apokryphos)"
  - subject: "Re: Go Home Novell"
    date: 2007-07-09
    body: "\"One the biggest contributors\", hardly. That would be GNU, Sun or Redhat by any form of counting. Do the math, then tell me about what Novell did. \n\nThere are 14 people from Novell working on OOo it seems (see http://www.novell.com/products/desktop/features/ooo.html for what they do, mostly integration work and eye candy is listed), that's right. \n\nThat figure has largely increased since the MS deal, and the OOXML translator will be done (I have yet to come across a single document in that format).\n\nAnd giving money, yes, that what it means to be a sponsor. Employing developers is not needed and won't make you a platinium sponsor alone. But have you forgotten that Novell wanted to drop KDE developers entirely, but had to reverse the decision due to customer requests (customers Suse had earned, developers that Suse has won).\n\nIt's not that their MS deal involves money. It involves the statement that Linux is not Free anymore, but that you need to pay Microsoft for the \"right\" to use unspecified patents that Linux and KDE may violate or not. That attacks the very fundamental freedom aspect. \n\nAnd it's probably an idea that Novell and MS really both like, that not everybody should be allowed to enter the market. But to us it should be unacceptable.\n\nBut that part is so obvious and well discussed, do I have to mention it?\n\nMy intention is to point out, that Novell may be out to limit damages on its reputation, but the Suse you are talking of, it is no more.\n\nYours, Kay\n\n "
    author: "Debian User"
  - subject: "Re: Go Home Novell"
    date: 2007-07-10
    body: "> \"One the biggest contributors\", hardly. That would be GNU, Sun or Redhat by any form of counting. Do the math, then tell me about what Novell did.\n\nI have no idea why you exclude Novell of the group above considering what they've done. Where exactly shall I begin? They employ more developers to work on each respective desktop environment (KDE and GNOME) than anyone else (including the above). They employ developers to work directly on and improve the kernel, GCC, alsa, OOo (as you've mentioned as well) etc. And this is just talking about recent contributions, which says nothing of i.e. how they were by far the biggest contributors to porting linux to x86_64 etc etc. You get the idea. \n\n> There are 14 people from Novell working on OOo it seems (see http://www.novell.com/products/desktop/features/ooo.html for what they do, mostly integration work and eye candy is listed), that's right.\n\nIncredibly curious how quickly you dismiss all of that. There is SO much more than what you just degenerated your commentary to. You've got the page; take a read of it again. But what is this? You're insulting the OOo developers who contribute substantial amounts of code to it? \n\n> And giving money, yes, that what it means to be a sponsor. Employing developers is not needed and won't make you a platinium sponsor alone.\n\nExactly; and Novell do _both_. I find it incredibly striking that you're not giving Novell credit for this. \n\n> It's not that their MS deal involves money. It involves the statement that Linux is not Free anymore, but that you need to pay Microsoft for the \"right\" to use unspecified patents that Linux and KDE may violate or not.\n\nNo, that's merely what you have read into it (helped along very nicely, of course, by Ballmer's fud which i.e. Perens and other people who think they have an \"official spokesperson role\" have apparently bought into). \n\nNovell/SUSE could barely have made it any clearer that at no point in any of the deal did they admit to any infringement of patents. Read the original contract that was signed; it says it very clearly. Read Novell's comments on the deal -- they've continuously stressed this point. Also read http://opensuse.org/FAQ:Novell-MS\n\n> My intention is to point out, that Novell may be out to limit damages on its reputation, but the Suse you are talking of, it is no more.\n\nYou couldn't be more wrong. SUSE is thriving. It has _gained_ Linux engineers overall, and Novell/SUSE's contribution to KDE couldn't be stronger. I'm incredibly interested in hearing about the Linux distribution that you think does more for KDE than SUSE. I'm all ears. \n\nThe \"limiting damages\" point is baseless considering Novell's contribution to free software in the past."
    author: "apokryphos"
  - subject: "Re: Go Home Novell"
    date: 2007-07-08
    body: "- Original Status\nMicrosoft supports openXML\nNovell supports ODF\n\n- Microsoft and Novell start to create a translator between the two formats\nNovell is now supporting OpenXML? Well, so Microsoft now is supporting ODF, isn't?\n"
    author: "Me"
  - subject: "Intel C++ compiler"
    date: 2007-07-07
    body: "Does this mean that it is politically correct to compile KDE with Intels C++ compiler?\nCan it be done, have anyone tried?\n\nhttp://www.intel.com/cd/software/products/asmo-na/eng/compilers/277618.htm"
    author: "reihal"
  - subject: "A sad state of affairs"
    date: 2007-07-07
    body: "Let me express my point of view.\n\nI don't really care about \"Their exceptional financial commitment to the KDE e.V. helps the project with community events, infrastructure and developer meetings\" because for more than ten years we can successfully and effectively communicate over the web.\n\nWhat KDE really needs is developers. IMO, Intel and Novell should have hired ten full time active developers and that would greatly improve the pace of development and overall quality of the project.\n\nRight now (judging from KDE commit digests) KDE has only _ten_ active developers. It makes me feel disappointed. Consider more than three hundred developers who worked just on Vista user interface.\n\nDon't get me wrong, but I see KDE project slowly dying. The main cause of that is the strict conditions of Qt library license. Most ISV want to cut their expenses when there's a talk about porting their software from Windows to Linux. So far, we see that most distros and ISV have chosen LGPL'ed GTK library. I know only two (!) commercial software pieces based on Qt: Opera and Parallels Workstation. Other software has been built over GTK/Motif/wxWidgets/etc."
    author: "Artem S. Tashkinov"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "Skype for Linux is Qt, and so is Google Earth.  And, apparently, Adobe and Autodesk use it too.\n\nwx is probably the best choice for those coming from Windows with an already done piece of software they want to port to other systems, as wx is the \"most native\" there.  Gtk looks very out of place on Windows, so unless someone comes from a non-KDE linux background, I doubt they would decide on it.  Motif?  Don't be ridiculous, you must be talking about software made a decade ago.\n\nBut I think this all has nothing to do with \"KDE dying\".  We have many people working on KDE &| Qt software... most of those don't meddle with the affairs of the KDE core though, but hey.  In my case that's simply because I prefer not to mess around with C++ when possible (and when much more modern languages are available)."
    author: "Henrik Pauli"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: ">> wx is probably the best choice for those coming from Windows with an already \n>> done piece of software they want to port to other systems, as wx is \n>> the \"most native\" there.</blockquote>\n\nInterstingly enough, a lot of companies like to make a separate interface for each platform.  Like Skype uses Qt on Linux, but the Windows interface is made in Delphi, and the Mac interface with Cocoa.  Instead of saving a bunch of work by making one Qt interface, they reinvented the wheel 3 times.  \n\nSimilarly with Opera, they only use Qt on Linux (although they have their own UI toolkit that does most of the drawing on the major platforms (what is it about Norwegians and making toolkits? :)).\n\n"
    author: "Leo S"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-09
    body: "> Interstingly enough, a lot of companies like to make a separate interface for each platform\n\nThis is usually the result of considering too late to also support other platforms.\n\nWhen a company wants to create a multiplatform product by design, they have a real incentive to share as much code as possible (or the other way around to have as few pieces of platform specific code as possible).\n\nBut often companies decide at a latter point to actually support other platforms, which mean they already have considerably large amounts of platform specific code.\n\nIn the long run the first model is obviously superior, since the company at most needs one or two experts per platform and can have the majority of development work spread across all their developers.\n\nCompanies which are aware of this sometimes choose multiplatform technology like Qt even if they are now just aiming at a single platform product, just in case there is enough demand on different platforms later on.\n"
    author: "Kevin Krammer"
  - subject: "Re: A sad^Wexcellent state of affairs"
    date: 2007-07-07
    body: "> Don't get me wrong, but I see KDE project slowly dying.\n\nYou should've checked better on the aKademy news. KDE is alive and kicking like it has never been before, and meeting each other physically is a cornerstone for good collaboration. And there were a lot of Novell who work on KDE here as well. \n\nOw well, I'll not further shoot down your points, but I'd recommend getting informed first, and then posting doom scenarios which are far from the truth."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: A sad^Wexcellent state of affairs"
    date: 2007-07-07
    body: "OK, I have no choice then but to backpedal ;-)\n\nI'm glad you proved me wrong."
    author: "Artem S. Tashkinov"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "no no, are many commercial software pieces based on QT: Skype, VirtualBox, Google Earth (the last version i don't know), Last.fm, etc.\nthe GTK always were free, QT version for Windows Not but now yes, you must wait a little time QT and KDE are going to be the best option to make free software, the QT library and the QT tools like Kdevelop are the best!.\n\nCongratulation to the KDE team, Intel and Novell (break \"the diabolic pact\" with microsoft :) )\n\nGrettings\ninsulae"
    author: "insulae"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "Birdie, stop gasifying the pools, please. Here is the list of vendors who use the QT widgets in their commercial products,\nhttp://trolltech.com/customers/directory\n\nGosh, just mentioning Wolfram's Mathematica 6.0 would be sufficient.\n\nBesides, that is especially ridiculous to call KDE a 'dying project' right before the 4.0 release. 4.2, kiso.\n"
    author: "Foxy"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "but the fact that the 4.0 release cycle took so long is a point of concern. You really cannot wait so long for a new release."
    author: "bert"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "We were waiting for Mathematica 6.0 for two years. So what? Now we get a qualitatively better product. Was there a need for hurry? I highly doubt it. KDE 4.0 will be released when it's ready, and it will be beautiful. For now, KDE 3.5.x is not that bad.;)"
    author: "Foxy"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "KDE 3.5.x is already miles better than anything else available, on Linux or elsewhere, so there is no big hurry :)"
    author: "Leo S"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "Don't forget that 3.5 releases have been going on until very recently, in fact a version just came out not that long ago.  If that is indeed the last KDE 3 release that means we'll only have to wait about 3 months for KDE 4.0 (assuming the release date doesn't slip).  If 6 months is considered a 'fast' release cycle, why is about 3 months between the last 3.5 release and 4.0 considered too long?\n\nIt will definitely be worth the wait as well!"
    author: "Sutoka"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-08
    body: "Might I first suggest here that being overly pessimistic or cheerleading are probably both bad ideas.  What we need is realism.\n\nI read an article in Communications last year about why Open Software projects fail (e.g. die) which would tend to indicate that they have a tendency to die.  Realism would be recognizing that this is always a possibility and taking proactive measures to prevent it.  This is much better than saying how great things are when there are always things that can be improved.\n\nYes, the roadmap for KDE-4 was a bit drastic.  This has resulted in a long development cycle and I don't really expect that it will be suitable to replace KDE-3 until 4.1.1 is released.  It would have been better to simultaniously continue with 3.6, but we have two issues there: (1) we don't seem to have enough people, (2) we have an attitude problem (I was told that continuing to work on KDE-3 was counter-productive [that means that it is harmful] -- well, I can hardly get KDE-4 to run; much too unstable to work on).\n\nAnd so it goes.  KDE is not becoming the best thing since sliced bread and it isn't dying anytime soon.  Still, we need to continue to work to improve the way that we do things to prevent any decline because software can often be \"Red Queen\" (you have to run as fast as you can to stay in the same place -- to avoid falling behind).  That seems to be the way it is with bugs and we really need to find a way to have successive releases more and more stable as we progress into the future, and this doesn't seem to be what is happening.  This is where I see a slight danger of the project slowly dying."
    author: "James Richard Tyrer"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-08
    body: ">I read an article in Communications last year about why Open Software projects fail (e.g. die) which would tend to indicate that they have a tendency to die.\n\nI can't comment on this artical as I haven't read it, but from my, adminatdly anacdoctal experience, what happens is around 80% of open soruce products never get anywhere (because 80% of anything fails, or sucks). Those that get big tend to survive for a long time.\n\n>It would have been better to simultaniously continue with 3.6, but we have two issues there\n\nAny work done on 3.6 would be mostly ignored if KDE 4.0 is out. Developers would be relitively uninterested since they cant use all the cool new libs created for 4.0, and as soon as 4.1.1 is out it will all that effort would be forgotten. However I think they do plan bugfix releases for 3.5 for a bit longer past KDE4 \n\n"
    author: "Ben"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-08
    body: "<i>\"from my, adminatdly anacdoctal experience, what happens is around 80% of open soruce products never get anywhere\"</i>\n\nAnd how many *commercial* products do never get anywhere??\n\nWhat? You don't know??\n\nI'll tell ya': it's nearly 90%.\n\nOf course you never hear about them. \n\nBecause their  design, inception, coding is done behind closed doors. \n\nLots of money being poured in. \n\nBut then, somewhere on their way to a shipping-worth product the management discovers that the market does not need it, that the software has too heavy design flaws, that the competition was faster and dominates everything already, $some-other-reason...\n\nIf you think that in the commercial world of software development the majority of closed source products you need to inhabit a different world from this one next time you live. Maybe there you find the Wonderland you imagine...."
    author: "scratching my head"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "A nice link, indeed, BUT how many of these titles are available for Linux?\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-08
    body: "If you mean the above link to Trolltech's \"list of customers\" - that's not a list of software titles which could be \"available for Linux\".  It's not even a list of companies that (exclusively) develop software for external sale to the public.  (Which seems like an odd metric for the \"success\" or death of an open software project, but you kind of implied earlier that that's what you're interested in using.)  \n\nIt's a list of companies that use the Qt toolkit, which brief capsule summaries of how those companies are using that toolkit.  As was plainly stated in the post that provided the link in the first place: \"the list of vendors who use the QT widgets in their commercial products\".\n\n"
    author: "AC"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "> Right now (judging from KDE commit digests) KDE has only _ten_ active developers.\n\nYou do understand the concept of a \"top ten\" list, don't you?\n\nIf you look at the summary you'll see something like\n\n\"Commits:    2508 by 243 developers...\"\n\nAlmost 250 active contributors in the respective week.\nAnd all of them improve the KDE experience, either by writing code, fixing bugs, adding translations, improving documentation, beautifying artwork, etc\n"
    author: "Kevin Krammer"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "I'm so sorry for making such bold and lame statements.\n\nI now clearly see that KDE is doing very well.\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "Thank you for acknowledging KDE's positive trajectory.  I think it's great to see that rather than expressing negative opinions because you wanted to rile people up, you were expressing negative opinions because you yourself were honestly concerned, and that those concerns were themselves addressable.  "
    author: "Dan"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "Actually, the thought that there were only ten people working on all of KDE made me chuckle. \n\nThey're good, but not _that_ good.\n\n"
    author: "cm"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-07
    body: "And let's not forget the contributors without own svn account.  For example, not all members of the translation teams use svn directly but send their work by email.  Thus they won't show up among as committers. \n\n"
    author: "cm"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-09
    body: "And of course they haven't started translating yet, so it's only developers you see right now."
    author: "jospoortvliet"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-09
    body: "> Don't get me wrong, but I see KDE project slowly dying.\n\nWith google's linux desktop supporting GTK, and now Openmoko also supporting GTK its not hard to think otherwise. GTK's LGPL license starts to show its advantages for companies."
    author: "Stu Warsnap"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-09
    body: "\"With google's linux desktop supporting GTK\"\n\nWhat \"linux desktop\" is this?\n\n\"and now Openmoko also supporting GTK\"\n\nOpenmoko is just one of a long line of niche Linux devices that have supported GTK.  Why should this one suddenly topple KDE?\n\n\"its not hard to think otherwise\"\n\nWhen a project averages around 9000 commits *per month*[1], and has so for several years, and is *still* consistently voted the best desktop[2] (by a huge margin) years after the major distros have adopted it's competitor as default, *and* which is about to make the initial release of a series that is likely to step it up another notch and which is now natively cross-platform, it's pretty hard to be worried.\n\nStop spreading FUD, please.\n\n[1] http://www.linuxquestions.org/questions/showthread.php?t=514945\n[2] http://lists.kde.org/?l=kde-commits&r=1&w=2"
    author: "Anon"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-09
    body: "The citations [1] and [2] are transposed, of course :)\n\nOh, and:\n\n\"GTK's LGPL license starts to show its advantages for companies.\"\n\nThe LGPL has been showing its \"advantages\" for years, and yet somehow, inexplicably!, companies are still buying Qt licenses.  Probably because they consider a natively cross-platform, top-quality and incredibly well documented and easy-to-use toolkit to be worth the tiny fraction of a developer's salary that constitutes a Qt license.  \n\nAnd anyway, since when do we care about what companies that intend to keep their apps closed do? They don't affect KDE one way or another."
    author: "Anon"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-13
    body: "\"What \"linux desktop\" is this?\"\n\nI'm guessing it refers to the recently-released Google Desktop Search for Linux, which uses GTK libraries: http://desktop.google.com/linux/"
    author: "LiquidFire"
  - subject: "myth # 1 : companies are porting sw to linux"
    date: 2007-07-10
    body: "The only s/w that gets \"ported\" in any significant number is J2EE apps - these are x-platform anyway. The others are server-side apps. It is so common to see commercial cos port only the server side of a client-server app. Remaining are some apps, usually insignificant in use or scope, simple enough to be ported or a lousy config app, such as ATI's control centre (after a lot of pressure from FSF/stallman/RH/gnome).\n\nCos are not porting sophisticated commercial desktop apps to linux, definitely not using GTK because it is a huge pain to do so and not worth the effort. Very little s/w in fact is ported to even OS X, much less linux.\n\ngnome pushers never get tired of lies and FUD, even though they should know from experience that it doesn't help anyone."
    author: "v m"
  - subject: "truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "FSF pretty much dictates RH and novell's agenda. FSF has adopted gnome because gnome also uses GTK ( nothing bloats a moron's bean more than stoking his ego). \n\nRH and novell have a lot of people working on gnome - without which gnome wouldn't be where it is today. Sun also has people working on gnome. And so does canonical. Shuttleworth may have thrown crumbs at KDE but he hires people to mostly work on gnome ( as far as desktops go). Check out his job ads.\n\n Novell has retained some of the KDE developers it inherited from SUSE but officially, it tries to be just like RH ( supporting only ext2/3 and gnome). The 'kernel developers' hired by novell and RH are in MA, under the \"guidance\" of the egomaniac moron stallman. Interesting how novell moved its systems development operations to MA after acquiring SUSE. All these facts can be verified. Also check out job postings by RH and Novell.\n\nThis is not gonna change. FSF sets the official linux agenda. And it is gnome. So US cos are gonna push gnome. They will hire gnome developers. It is unfair to rule out KDE because of this. KDE has far more volunteer developers and they are impressive. I live in US and will use KDE as long as I use linux ( I will avoid gnome for the same reason I avoid m$).\n\nHaving said this, I think KDE needs to improve in some matters -\n\nGet rid of too many  duplicate apps. That used to be a gnome legacy and even they are avoiding that nowadays.\n\nDo not make immature apps part of 'official' packages. The apps included must be complete and professional. For e.g., in games, loose kolf, kasteroids and other garbage.\n\nDon't let the crash handler handle everything. The app should handle an event or exception whenever possible. I have seen too many apps fall thru to the crash handler when even a dependency is unresolved. This is a lazy and unprofessional way of doing it.\n\nThis might be a packaging issue ( at the distro level). If KDE apps use non kde apps ( or even other kde apps sometimes), don't make the dependency too strong. If the app/lib it is dependent upon is missing, detect that and inform the user. This makes packaging easier and usability is also improved.\n\nEncourage distros that favour KDE because most of them are struggling. ubuntu does NOT need your help ( and canonical can manage on their own). Help distros such as mandriva, pclinuxos, mepis, knoppix etc. Encourage these distros. Lining up like sheep behind fedora, ubuntu or the next flavour of gnome-based distro is playing right into their hands."
    author: "v m"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "stallman an egomaniac moron?\nI thought he was a bona fide homosexual communist.\nPeculiar that american business executives should want him as a bed partner.\nBut I guess the US is a peculiar place."
    author: "reihal"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-11-06
    body: "so your picking on GTK huh...maybe its cuz gtk is lgpl whereas qt which kde uses isn't ?\n\nOne is MORE FREE than the other so lets not obfuscate the issue shall we , and while your at it keep up the good kde work by showing what utter homophobe's and illiterates you are :)\n\nI bet you dont eat shell fish either  ;)\n\n"
    author: "codeofjustice"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "> FSF pretty much dictates RH and novell's agenda.\n\nAh, so it was the FSF initiating the deal with Microsoft? ;-)\n\n> Novell has retained some of the KDE developers it inherited from SUSE\n\nActually SUSE employs the same amount of developers to work on KDE than before the acquisition by Novell, and three of five current developers were hired after.\n\n> it tries to be just like RH ( supporting only ext2/3 and gnome )\n\nYou're mistaken, SUSE does support KDE and ReiserFS 3.\n\n> The 'kernel developers' hired by novell and RH are in MA\n\nMassachusetts?\n\n> Interesting how novell moved its systems development operations to MA after acquiring SUSE.\n\nStrange that Novell just sent out a press release last week about expanding the Nuremberg location.\n\n> All these facts can be verified. \n\nVerified and found to be false."
    author: "binner"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "> Strange that Novell just sent out a press release last week about expanding the Nuremberg location.\n\nPlease give a link. Their job search at http://www.novell.com/job_search/servlet/eJobSearch doesn't even list Nuremberg as one of their locations."
    author: "doh"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "Maybe it was a Germany only press release: http://www.verivox.de/News/ArticleDetails.asp?aid=55749&pm=1\n\n> Their job search [..] doesn't even list Nuremberg as one of their locations.\n\nYou have to look at the EMEA page: http://www.novell.com/offices/emea/jobs/"
    author: "binner"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "Get over novell and m$. ALL commercial companies have business agreements. IBM has been a m$ pimp for decades ( and still are) and so is dell. And both these cos are current favourites among (clueless) linux geeks today.\n\nDo not confuse opensuse with novell/SLED. In SLED, they carefully removed kmail, konqueror and other key KDE apps from the KDE distro. In fact it is RH that offers ( a largely labotimized and buggy version of) KDE; novell has made press releases quite some time ago that they will officially support only gnome. So, even though you can hunt for KDE and install it on their \"enterprise\" desktop/server, guess what businesses do when they see the word \"support\"? Not to mention, novell is trying to integrate all the s/w that is important to businesses with gnome.\n\nShow me proof that they recently hired (or trying) KDE developers.It should be easy - just post links to jobs at novell that are pertinent. The same goes for their German location. Most of the (important)jobs when I looked, listed MA . Not even Utah, much less Germany.\n\nXimian plays a big role in Novell's overall strategy. And they are the originators of gnome. de icaza hates KDE with a passion. Sure, novell cannot touch opensuse because it is a community effort but how long do you think they will keep employing people who are not important for their corporate strategy ? "
    author: "v m"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "And BTW, FUD works. People in my org  have almost no experience with linux. Even these people have swallowed the RH agenda somehow. They frown on me when I install KDE instead of gnome. I didn't realize they were even aware of KDE or gnome ( because they don't use linux) ! The only reason they don't like KDE is because of FUD.\n\nPeople in the KDE community may have become complacent about gnome ( because there is less animosity on mailing lists) and gnome people believe that they already won the desktop 'battle' ( hence less animosity). Truth of the matter is that linux is still rarely used on the desktop in businesses (at least in the US) and so it doesn't matter yet. As for 3rd world countries, they just replace linux with pirated windows versions. I wouldn't be surprised if OLPC laptop gets hacked to run windows."
    author: "v m"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "> And BTW, FUD works.\n\nThat means you will continue to FUD here? ;-("
    author: "binner"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-13
    body: ">Truth of the matter is that linux is still rarely used on the desktop in >businesses (at least in the US) \n\nWorldwide I do not think the above is true. In Brazil for example the absolute majority of Linux desktops (government, companies or individuals) is KDE-based."
    author: "Mauricio Piacentini"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-10
    body: "> In SLED, they carefully removed kmail, konqueror and other key KDE apps\n\nYou're wrong again: SLED does contain KMail, Konqueror and other key KDE apps.\n\n> they will officially support only gnome.\n\nThat's wrong or obsolete: KDE is supported, on all products including SLED.\n\n> Show me proof that they recently hired (or trying) KDE developers.\n\nWhat's the point showing that at the moment no new developers are hired?"
    author: "binner"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-13
    body: "> You're mistaken, SUSE does support KDE and ReiserFS 3.\n\nFact N1: Novell has officially abandoned ReiserFS support after Hans Reiser was detained after being accused of killing his spouse.\n\nFact N2: Only recently Novell confessed they would continue KDE support. The truth is that Novell gave up on KDE after acquiring Suse.\n\nFact N3: Novell decided to continue supporting KDE after being pressed by their _corporate_ users.\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: truth about RH, novell, ubuntu, gnome, fsf"
    date: 2007-07-13
    body: "Your \"facts\" are wrong: Novell supports ReiserFS 3 as said, the \"announcement\" to not include ReiserFS 4 as long it's not in the upstream Linux kernel was done IIRC one week before Hans Reiser being accused.\n\nAbout KDE, Novell \"confessed\" its support for KDE in 2005 (nothing I would call \"recently\") only days after some rumor article saying that it would be chopped. And that article was posted two years after Novell acquired SuSE so your association with \"after acquiring\" is far-fetched.\n\nIt's always funny when some outsider thinks that he knows more than some company insider ;-)..."
    author: "binner"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-10
    body: "Dude, we had 250 developers at Akademy, and are averaging somewhere on the order of 350 commits per day.  I'll have more on this soon once I have concrete numbers to back it up, but it seems like the opposite is true - KDE is innovating."
    author: "Troy Unrau"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-10
    body: "\"Dude, we had 250 developers at Akademy, and are averaging somewhere on the order of 350 commits per day.\"\n\nAt the risk of causing an unpleasant flamewar, this statistic should be contrasted with GNOME+Gtk, which combined averages around 4500 or so commits a month, compared to KDE's 9000+ - i.e. if we count raw commits, GNOME+Gtk is *half as active* as KDE, and due to GNOME's policy of having many mini-releases in between its major releases, hundreds of these are merely updated translations for apps, or tagging releases, updating changelogs etc - i.e. commits where not a single line of code is changed.\n\nKDE's 9k+ commits a month, of course, does not count improvements to Qt, which also seems to be very actively developed.  Does anyone know if Trolltech release stats such as the number of commits per month ... ?\n\nIf I was going to be worried about the state of one of the desktops, it wouldn't be KDE :)"
    author: "Anon"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-10
    body: ">> If I was going to be worried about the state of one of the desktops, it wouldn't be KDE \n\nWell, we all know how prevalent windows is on the desktop compared to linux and it has nothing to do with technical superiority, stability, number of devs etc. Gnome is in a similar situation w.r.t. KDE as 'doze has w.r.t. linux. All the 'major' companies such as RH, novell and Sun are backing gnome. Of course, since that hasn't led to any real adoption, it doesn't seem to matter. Besides, none of these cos really make any great desktop s/w sales to corporations that we know of.\n\nThis should not be misinterpreted as all software cos backing gnome - in fact most of them only back windows. It is more of a political thing; how much it will matter in the future remains to be seen."
    author: "vm"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-12
    body: "No, Trolltech doesn't release such stats. But if you look at Lars Knoll's presentation at aKademy, you'll see that they have a team of 40+ people working on Qt fulltime.\n\nAlso note that Qt developers work in a rather flexible environment and may be working on \"research\" projects. Those commits may not count towards the Qt development, but they mean work is being done."
    author: "Thiago Macieira"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-12
    body: "I see - thank you, Thiago :)"
    author: "Anon"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-13
    body: "> compared to KDE's 9000+ - i.e. if we count raw commits, GNOME+Gtk is *half as active* as KDE\n\nWhere can I find such statistics?"
    author: "Artem S. Tashkinov"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-13
    body: "Oops, sorry: \n\nhttp://cia.vc/\n\nkeeps the tally for the last month.  KDE's full history of commits can be found at\n\nhttp://lists.kde.org/?l=kde-commits&r=1&w=2\n\nI don't know where GNOME keeps theirs, but I do know that (according to cia.vc), they've had < 5000 commits for at least two months running (when I started checking these things)."
    author: "Anon"
  - subject: "Re: A sad state of affairs"
    date: 2007-07-13
    body: "\"I don't know where GNOME keeps theirs\"\n\nAha - http://mail.gnome.org/archives/svn-commits-list/.\n\nIt doesn't seem to contain valid data before Jan '07.\n\nIt looks like cia.vc undercounts both KDE and GNOME, as GNOME made 5283 commits according to this list (but 4838 according to cia.vc; a shortfall of 445) and KDE made 10867 according to the kde-commits mailing list, whereas cia.vc reported just 9864 (a shortfall of 1003). "
    author: "Anon"
  - subject: "Novell"
    date: 2007-07-07
    body: "Interesting to see Novell there.  I hope they'll behave :)"
    author: "Henrik Pauli"
  - subject: "This is no bashing."
    date: 2007-07-08
    body: "Sorry for my limited english.\n\nIt is an excellent idea to get KDE running on Windows (my significant other, a Windows user, *adores* the Konqueror) and Novell will support you to get things done, but I am paranoid because of the Novell-Microsoft-Deal.\n\nWill the Novell-Microsoft-Deal affect the developement of KDE?\n\nHow will Microsofts refusal, to acknowledge the GPLv3, affect KDE?\n\n\n\n\n\n\n"
    author: "fear"
  - subject: "Re: This is no bashing."
    date: 2007-07-08
    body: "> and Novell will support you to get things done\n\nWhy do you say that?\n\n> I am paranoid because of the Novell-Microsoft-Deal.\n\nWhich part are you paranoid about? Take a look at http://opensuse.org/FAQ:Novell-MS\n\n> Will the Novell-Microsoft-Deal affect the developement of KDE?\n\nThe deal was made quite some time ago. Even with the GPLv3 Novell is not prohibited of any of its Linux offerings. It probably only had a positive effect on KDE, as Novell is doing even better because of the deal (more linux engineers, more cash, more sled coupons sold), which in turn is also good for KDE as Novell is a big contributor to kde."
    author: "apokryphos"
  - subject: "Re: This is no bashing."
    date: 2007-07-08
    body: ">Why do you say that?\nBecause I think that the patronat is good for both, Novell as well KDE. \n\n>Which part are you paranoid about?\nI just thought that Microsoft tries to \"eat\" its \"Partners\". But I might be wrong and Microsoft is a friendly company.\n\nThank you for explaning the things und giving useful links. "
    author: "fear"
  - subject: "Re: This is no bashing."
    date: 2007-07-09
    body: "> But I might be wrong and Microsoft is a friendly company.\n\nLOOOOOOOOL! That's a good one."
    author: "Kevin Kofler"
  - subject: "Re: This is no bashing."
    date: 2007-07-09
    body: "It sure is ;-)"
    author: "jospoortvliet"
  - subject: "Re: This is no bashing."
    date: 2007-07-10
    body: "> I just thought that Microsoft tries to \"eat\" its \"Partners\". But I might be wrong and Microsoft is a friendly company.\n\nEven so, the position that \"Microsoft are pure evil and are patent geniuses\" is misguided. Their history also implies the contrary. Take a look at http://en.opensuse.org/FAQ:Novell-MS#Microsoft_are_Patent_geniuses_and_can_never_be_trusted\n\nFor Novell specifically so far the deal has been incredibly good: more money to invest, more SLE coupons sold, more Linux engineers employed.  "
    author: "apokryphos"
  - subject: "Re: This is no bashing."
    date: 2007-07-10
    body: "In my eyes the one and only goal of Microsoft is to make more money. And yes, I think that they are patent geniuses. In Novell I see some kind of idealism. I do not trust Microsoft, but I am willing to learn because things can change. "
    author: "fear"
  - subject: "linux pclinuxos2007"
    date: 2007-07-14
    body: "i do love this pclinu to the ones that worked on this good job to all worked on it and thanks "
    author: "jeagle1255"
---
<a href="http://www.intel.com/">Intel</a> and <a href="http://www.novell.com/">Novell</a> have both become corporate <a href="http://ev.kde.org/supporting-members.php">Patrons of KDE</a>. Their exceptional financial commitment to the KDE e.V. helps the project with community events, infrastructure and developer meetings. Read on for more information. 







<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left;">
    <img src="http://static.kdenews.org/jr/patron_small.png" width="91" height="24"/>
</div>
<p>For decades, Intel Corporation has developed technology enabling the computer and Internet revolution that has changed the world. Intel has been a strong supporter of KDE and other free software projects in the past, and it continues this effort by becoming a Patron of KDE. </p>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left;">
    <img src="http://static.kdenews.org/jr/patron_small.png" width="91" height="24"/>
</div>
<p>Novell is a global leader in open, enterprise class software and services; it is also the company behind the <a href="http://www.opensuse.org/">openSUSE</a> and <a href="http://novell.com/linux">SUSE Linux Enterprise</a> products. Novell has persistently been among the strongest supporters of KDE by employing many KDE developers and continuously sponsoring events. It now reaffirms its commitment financially by being the first corporate Linux distributor to become a Patron of KDE. 
</p>




