---
title: "KDE Commit-Digest for 28th January 2007"
date:    2007-01-29
authors:
  - "dallen"
slug:    kde-commit-digest-28th-january-2007
comments:
  - subject: "Kwin effects"
    date: 2007-01-29
    body: "> Effects can now request windows to be subdivided into multiple quads.\n\nis that expose like?\n\n\n> Adding WavyWindows effect which makes all windows wavy. Meant to demonstrate possibilities of vertex transforming and for cool screenshots ;-)\n\nkool!\n\nI guess it's a matter of a couple of weeks before we get the whole cube thing :) I 'd love to get the skydome though like in beryl. \n\nAnyway, thanx A LOT for all the hard work. "
    author: "Patcito"
  - subject: "Re: Kwin effects"
    date: 2007-01-29
    body: ">> Effects can now request windows to be subdivided into multiple quads.\n\n> is that expose like?\n\nNo. It means that an effect plugin can request that the rectangle of an individual window can be subdivided internally, adding new vertices within, which can then be controlled (i.e. moved) by the effect, for example to give a window the appearance of wobbling.\n\nNote that kwin_composite already has an Expos\u00e9-like effect, however."
    author: "Eike Hein"
  - subject: "Re: Kwin effects"
    date: 2007-01-29
    body: "It can be seen in the (very) last video from http://www.kdedevelopers.org/node/2651 . Expose-like effect is the first one.\n"
    author: "Lubos Lunak"
  - subject: "And the weekly Thank you! goes to:"
    date: 2007-01-29
    body: "Danny :)"
    author: "F3"
  - subject: "Diff link broken for new files"
    date: 2007-01-29
    body: "I mentioned this before but didn't see an acknowledgement.  If you click the _Diff_ link for a new file, ViewCVS.py generates an error.\n\nE.g. for \"script to help porting in the kinstance-redesign branch\", _Diff_ is a link to \nhttp://websvn.kde.org/trunk/KDE/kdesdk/scripts/qt4/convert-kinstance.pl?r1=625898&r2=625899\n\nBut this new file didn't exist in r1=625898, so you get a 400 Bad Request error,\n\"Invalid path(s) or revision(s) passed to diff\"\n\nIt would be nice if the digest detected that a file is new, for example, provided _View new file_ instead of _Diff_.\n\nMany, many thanks for the commit-digest!"
    author: "S Page"
  - subject: "Re: Diff link broken for new files"
    date: 2007-01-29
    body: "I'll fix it during the code cleanup and enhancements I plan to do over the next few days.\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "kcmfontinst - thanks!"
    date: 2007-01-29
    body: "The face lift kcmfontinst got is awesome!\nI've been hating that interface from day one. Thank you Craig."
    author: "logixoul"
  - subject: "Re: kcmfontinst - thanks!"
    date: 2007-01-29
    body: "Hmm... Well something's wrong on your system. The 'A' column should not be visible in that mode - this column is used to indicate if a font is enabled or not. The installer now has 2 modes - basic installation (as shown in your screenshot), and font management (which will allow fonts to be enabled/disabled, and the creation of font groups).\n\nAlso, its now fontinst (not kcmfontinst)."
    author: "Craig"
  - subject: "okular speed?"
    date: 2007-01-29
    body: "It seems like okular is taking on a similar amount of plugin views that konqueror had with its kparts (for pdf, html, images, etc.), if not more.  Although I like konqueror's features a lot, it was always a little too slow for actually browsing and managing files with, and I suspect part of it was due to all that kparts overhead.  Dolphin is much faster, at least.\n\nThis raises a few questions, for me:\n\nIs okular intended to be a refactoring of konqueror so that konqueror will be faster, or is konqueror still going to be a viewer too?\n\nAlso, how fast will okular be, if I just want to view a PDF, or a PNG file?  Do all the extra formats (or even just the generic format layer) add much overhead?  Will it be as fast as KPDF, for example?\n\nHow generic is the interface for all these things?  If okular can view PDFs and scientific image formats and eBooks, won't some of the toolbar buttons/menu entries/etc be duplicated, unnecessary, or confusingly labelled?\n\nFinally... what's wrong with just using libraries to keep most of the common code in, and having different front ends for different viewers, like a PDF viewer, an eBook viewer, etc.?"
    author: "Lee"
  - subject: "Re: okular speed?"
    date: 2007-01-29
    body: "> Although I like konqueror's features a lot, it was always a little too slow > for actually browsing and managing files with, and I suspect part of it was > due to all that kparts overhead\n\nWhen you say \"faster\", are you referring primarily to startup, speed of changing from one folder to another, speed of loading folder contents, speed of file operations ( renaming, moving etc. )?"
    author: "Robert Knight"
  - subject: "Re: okular speed?"
    date: 2007-01-29
    body: "none of the above has anything to do with the kparts, right? konqueror's speed with filebrowsing had nothing to do with kparts or the fact it could view eg pictures or wordfiles. konqueror wouldn't be a nanosecond faster if it didn't have the ability to view wordfiles or be a webbrowser, sorry."
    author: "superstoned"
  - subject: "Re: okular speed?"
    date: 2007-01-31
    body: "Well, dolphin is much faster, and the primary difference is that it doesn't support kparts etc.  I don't know for a fact that Konqi is slow because of kparts, and I'm not saying that.  I'm asking if the move towards okular is an attempt to fix konqi's speed, or if the speed issue has even been looked at in okular (ie, if it'll repeat the same mistake, without even realising it until it's too late)."
    author: "Lee"
  - subject: "Re: okular speed?"
    date: 2007-01-31
    body: "Okular is a document viewer, and has nothing to do with Konqueror. So there is no move, and it will not have any effect on Konqueror. Other than Okular providing Konqueror with kparts for viewing different kinds of documents, like pdf."
    author: "Morty"
  - subject: "Re: okular speed?"
    date: 2007-01-31
    body: "I'm referring to what feels like \"overhead\" -- things like actually loading the app, loading/displaying widgets on toolbars, etc.  Not things like moving files, or listing dirs, as they're not TOO bad, and I suspect any issues there would be due to lower-level stuff like the filesystem or KIO."
    author: "Lee"
  - subject: "magic solution"
    date: 2007-02-03
    body: "*get more RAM*"
    author: "eMPe"
  - subject: "Re: magic solution"
    date: 2007-02-15
    body: "I have more ram, but I'm using it for other things, thanks."
    author: "Lee"
  - subject: "Re: okular speed?"
    date: 2007-02-03
    body: "I'm sorry to say you have absolutly no clue what you're talking about. However, fear not the slowdown of kde, it will not be."
    author: "eMPe"
  - subject: "Re: okular speed?"
    date: 2007-02-15
    body: "Please control your emotions.  I'm just asking a question."
    author: "Lee"
  - subject: "About Gwenview"
    date: 2007-01-29
    body: "I'm very happy to see the move of Gwenview to kdegraphics :).\nWith kde4, I see several applications changing their names. Many are removing the \"K\" in the name. I have no problem with a \"K\" in the name. But the \"G\" of Gwenview... makes me think of Gnome :\\. So IMHO, in this particular case, a renaming might be needed.  \nAnyway, thank you Aurelien for continuing the maintainership of Gwenview."
    author: "Shamaz"
  - subject: "Re: About Gwenview"
    date: 2007-01-29
    body: "nah, gwenview is pretty well known, and if it gets its gui update, it'll be a perfect KDE app. what's wrong with a G in the name, then?"
    author: "superstoned"
  - subject: "Re: About Gwenview"
    date: 2007-02-02
    body: "The problem is the \"view\" because it is English but KDE has to safeguard langauge neutrality. Gwen seems to be a personal name."
    author: "gerd"
  - subject: "ktorrent and all p2p"
    date: 2007-01-29
    body: "I really wish that the p2p's would split apart the client from the server. It would be nice to place the server on a different machine that is on the network. In fact, with the proper arch, a server could handle multiple protocols. \n\nThen the client part could be moved into a KIO along with the recent background progress meter that was shown."
    author: "a.c."
  - subject: "Re: ktorrent and all p2p"
    date: 2007-01-29
    body: "Well mldonkey does this.. It's has a KMldonkey front end too."
    author: "Diederik van der Boor"
  - subject: "Re: ktorrent and all p2p"
    date: 2007-01-29
    body: "And the same for giFt, and the KDE frontend is named Apollon.\n\nAll that said, the mess that is mldonkey is a particular good example of why this is not a good idea. \n\nI'd say keep the applications simple and stable, whitout the unnecessary complexity and overhead of a server client architecture. Centralized progress information can be done via the new KJobs progress monitoring regardless."
    author: "Morty"
  - subject: "Re: ktorrent and all p2p"
    date: 2007-01-30
    body: "Great thought MLDonkey is, and KDE-ish thought it can be with KMLDonkey, its bittorrent support is not very nice at all right now. It's slower than normal clients, starting torrents is a little odd, and it doesn't seem able to look for a sensible port (i.e. if you are running Democracy or something and the specified port isn't available, it just doesn't start and you have to check the logs to see why). AFAIK, bittorrent support is still being improved though.\n\nHowever, daemon and client isn't very kde-ish. The best way to split ktorrent would probably be to work on minimising memory usage of GUI-related stuff while ktorrent is in the tray."
    author: "Ben Morris"
  - subject: "Re: ktorrent and all p2p"
    date: 2007-01-30
    body: "Museek has it as well.\n\nI let the core run on my server and connect to it with the GUI on my laptop.\n\nGenius!  ;)"
    author: "fish"
  - subject: "Welcome back ..."
    date: 2007-01-29
    body: "... Thomas L\u00fcbking :-)\n\nany screenshots/mockups for the underconstructing KDE4's new shiny skin ?"
    author: "Mohasr"
  - subject: "Nice to see the progress week after week."
    date: 2007-01-30
    body: "Thanks Danny for the dommit digest.\nAnd thans to the KDE devs who make the content of this digest :)\n\nNice to see the Sonnet langage auto detection works, and to have Gwenview continued in KDE4."
    author: "kollum"
---
In <a href="http://commit-digest.org/issues/2007-01-28/">this week's KDE Commit-Digest</a>: KGoldRunner begins the transition to a scalable graphics interface. <a href="http://okular.org/">Okular</a> gains support for DjVu metadata, and investigates the use of threaded text extraction in order to prevent interface freezes. Continued improvement in the font KControl configuration module. More 3D and contemporary effects in the kwin_composite branch. Multiple, discriminatory language spellchecking develops in Sonnet. Improved support for BMP and ZIP files in <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>. Import of user documentation for <a href="http://www.mailody.net/">Mailody</a>. Optimisations in the Dolphin filemanager. An important stage in the replacement of KDesktop elements with KRunner is completed. <a href="http://ktorrent.org/">KTorrent</a> makes exploratory moves towards a KDE 4 port. KSirc, an IRC client, is removed from KDE SVN.

<!--break-->
