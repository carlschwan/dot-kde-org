---
title: "An Interview with KDE-Edu Developers"
date:    2007-03-29
authors:
  - "gventuri"
slug:    interview-kde-edu-developers
comments:
  - subject: "Carsten Niehaus - teaching in a Gymnasium?"
    date: 2007-03-29
    body: "Given that he's teaching chemistry and biology, I don't think he's really teaching in a gym.  He's teaching in what would be called a \"Grammar School.\"  This is the best type of secondary school in Germany.\n\nCarsten hat auf Englisch gesagt, dass er in eine Turnhalle lehrt... :-)\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Carsten Niehaus - teaching in a Gymnasium?"
    date: 2007-03-29
    body: "Wrong, I wrote \"Gymnasium\", not \"gymnasium\". I simply used the german word because there is no good translation for Gymnasium. Same for Abitur. I won't translate it to A-Levels or \"higher leaving examination\". I simply call it what it is: Abitur."
    author: "Carsten Niehaus"
  - subject: "Re: Carsten Niehaus - teaching in a Gymnasium?"
    date: 2007-03-29
    body: "> I wrote \"Gymnasium\", not \"gymnasium\".\n\nYeah..  you wrote \"Biology and Chemistry\" instead of \"biology and chemistry\" too, so I thought you'd made the same mistake.\n\nI really had a mental picture of you giving classes in a sports hall, as that is primary meaning of the word in the English language.  Gesagt ist nicht verstanden....  nicht unbedingt!  I wanted to point out what you really meant, lest others believe you don't teach in a real school.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Carsten Niehaus - teaching in a Gymnasium?"
    date: 2007-03-30
    body: "Ok, that was indeed a mistake :)"
    author: "Carsten Niehaus"
  - subject: "Re: Carsten Niehaus - teaching in a Gymnasium?"
    date: 2007-03-29
    body: "From the OED:\n> 1. A place or building for practice of or instruction in athletic exercises;\n> a gymnastic school.\n\nBut also:\n\n> 2. a. gen. A high school, college, or academy (obs.); b. spec. in Germany\n> and other Continental countries, a school of the highest grade designed to\n> prepare students for the universities. Now often pronounced as a Ger. word.\n\nPS: I love my Konqueror web shortcuts! Having the OED only, what, five keystrokes away is just too useful sometimes. :)"
    author: "reading the dot at work, and nitpicking to boot"
  - subject: "Re: Carsten Niehaus - teaching in a Gymnasium?"
    date: 2007-03-29
    body: "Well I guess we all Germans were tortured with things like:\n* \"don't call a Gymnasium Gymnasium\" (Carsten already gave the answer, why this is wrong, although a lot of English teachers did tell us so).\n* Don't use \"handy\" but \"mobile\" or \"cell phone\" but American people still understand me when I use it...\n\n...but I never figured out why we poor Germans with our funny self created english words weren't warned about the \"beamer\" word (\"beamer\" is in German a \"projector\") cause according to a friend of mine a \"beamer\" is in the UK well a slang word for a BMW (does this car beam people from A to B?!) and it was really funny which kind of confusion happened to her during the preparation of a fashion presentation...\n\nMoral: If you are using an overly correct language you will miss a lot of interesting and sometimes funny things. ;-)\n"
    author: "Arnomane"
  - subject: "Re: Carsten Niehaus - teaching in a Gymnasium?"
    date: 2007-03-29
    body: "> a \"beamer\" is in the UK well a slang word for a BMW (does this car \n> beam people from A to B?!)\n\nIf you pronounce BMW in the German way (bay-emm-vay) then it does seem strange. However, if you say bee-emm-double-yew as they do in most English speaking countries then \"beamer\" does make a little more sense. BM -> beam -> beamer. :)"
    author: "Paul Eggleton"
  - subject: "KDE Edu can make THE difference"
    date: 2007-03-29
    body: "I want to say thank you to all KDE edu people. When the KDE edu module was young I thought \"oh yet another so called educational software I had to use in school\" but I was totally wrong. KDE edu is no \"let us make some kid software\" project. KDE edu software can be used not only by kids but by everyone for his/her and other peoples education and leisure.\n\nI am not aware that there is anything compareable to KDE edu in the free software world.\n\nWhen I had the chance getting in touch with on of the 100 Dollar laptops I thought: \"Wow this is cool hardware but what a pitty that they reinvent the wheel and invent yet another user interface in PyGTK and simply do not have a single working real world educational program for it\" (the existing programs I am aware of are all the same poor mostly imature quality I whitnessed in school and are too \"kid-like\").\n\nSo how about creating a cool alternative software image for the 100 dollar laptop that uses Qt (instead of GTK + Python) and features KDE edu? Just for the fun of it in order to proof people wrong that KDE is not bloat but even works smoothly on tiny hardware.\n\nAlthough there is only 500 MB free disk space it should be manageable as OLPC people managed it to ship even a gecko based browser on it (this browser alone consumes 65 MB) and IMHO a browser is the least thing a computer needs to provide that is intended for use without internet access Much more important are applications like Kalzium and KStars. Imagine school kids in rural area where they need to know about clean water (knowledge about arsenic for example is very important!) and want to learn something about the stars above (there is very often no light pollution and stars on a perfect dark sky in a summer night impress everbody).\n\nSo KDE has the potential to make THE difference in education and of course as they don't need browsers and such there is plenty of room for disk usage optimsation so that a KDE based desktop fits into this hardware.\n\nHow about that?\n"
    author: "Arnomane"
  - subject: "Mandriva konquering class rooms around the world"
    date: 2007-03-30
    body: "Mandriva, the France-based Linux distributor, will have a version of its Mandriva Linux 2007 pre-installed on Intel's new low-end laptop for students, the Classmate PC.\n\nhttp://www.desktoplinux.com/news/NS4146178789.html"
    author: "Waldo Bastian"
---
We are here today to talk about the developers of the <a href="http://edu.kde.org/">KDE-Edu</a> Project. The purpose of this interview is to feature and present their work and motivation, which is often not as well-known or regarded as other, more prominent work within the KDE project. The KDE-Edu developers are developing high-quality educational software for the K Desktop Environment. Their primary focus is on school children aged 3 to 18, and the specialised user interface needs of young users. However, they are also have programs to aid teachers in planning lessons, and others that are of interest to university students and anyone else with a desire to learn! Read on for the interview.






<!--break-->
<br>
Note: The following interview was originally released at the <a href="http://www.kde-it.org/">KDE Italia web site</a>, and is also <a href="http://www.kde-it.org/index.php?option=com_content&task=view&id=107&Itemid=1">available in Italian</a>.<br><br>

<h2>Carsten Niehaus</h2>

<b>Can you introduce yourself to the KDE Italia readers? What did you study? What is your day job?</b><br>
I am a teacher for Biology and Chemistry. I am 27 years old and I live in Osnabrück, North Germany, but am teaching in a Gymnasium in a small town called Bad Essen. I am teaching students from 5th to 13th grade (that translates to 10 to 19 years old).
<br><br>

<b>When did you hear of KDE for the first time? When did you start using Linux and why?</b><br>
When I did my military service in the German Air Force I had quite some time in the evenings. I bought a box of SuSE 6.4 and simply played around with it. I started sending translations for a couple of KDE games and after some patches somebody gave me a CVS account. That was... 1999. I heard about Linux perhaps a year or two earlier, but never saw a computer using it.
<br><br>

<b>How and when did you get involved in KDE?</b><br>
Well, see the question above. When I studied chemistry I needed a periodic table and thought that having an electronic version would be nice. So I started to work on Kalzium, my main application. I had never coded C++, and only knew TurboPascal. That is because in German schools, TurboPascal was <em>the</em> programming language in the 90s. Currently most schools teach Java, though.
<br><br>

<b>What makes you contribute for KDE instead of the competition?</b><br>
The API of KDE and the Qt toolkit. As I started with SuSE, I started with KDE. Soon, I had social contacts with several KDE folks so I stuck with KDE. When I started coding I had a look at several APIs and Qt is just too simple to resist. I never liked the API of GTK+. So my choice was obvious.
<br><br>

<b>Can you say that programming for KDE was an investment? You got C++/Qt programming experience that helped you in enriching your personal skill set. Can this be of help getting a job? Could it be a good attribute to take to a job interview with a software company?</b><br>
Well, programming is a time investment. But it is worth it! I am doing this all in my spare time, but it is a hobby (and a good one!). I now know most of the KDE developers personally because of all the social events and fairs. This is a really nice community and I guess that a KDE developer has a free bed in almost every town on the world, right?
<br><br>
For my job, my involvement in KDE doesn't help me. But my school uses Linux/KDE for all student PCs. But that is totally unrelated to my involvement, as the installation was already there two years before I entered the school :)
<br><br>

<b>Have you ever stayed at an aKademy conference? If so, can you tell us briefly about it? What did you do? Would you say that a KDE user/developer should participate in at least one KDE developers conference (aKademy) in his/her life?</b><br>
I have been in to aKademy 2005, in Malaga, Spain. I think that participating in a &#8220;Free Software Event&#8221; is really important, not necessarily an aKademy (though that is surely a great event!). For me, the social aspect is the most important factor in my contributions to F/OSS. In other words, putting faces on names is really important - if possible, you should really try to visit on of the bigger Linux/KDE-events, yes.
<br><br>

<b>What are the most beautiful experiences with KDE? aKademy itself? To know other developers? Or something else?</b><br>
As I said, the social component is the most important to me. So I guess, the fun hours with other developers, be it aKademy or LinuxTag were the most beautiful experiences.</b>
<br><br>

<b>Do your parents and friends use Linux and KDE?</b><br>
My partner Julia is using Linux for a couple of years now. She never missed Windows. My father tries hard to find stupid arguments why he wants to use Windows XP, but the day he'll switch will come, I swear. Beside me and Julia I have no friends using Linux, but I already converted many to use OpenOffice!
<br><br>

<b>What could be your slogan to attract people to KDE? Can you give also some "reasons to stay with *nix/KDE"?</b><br>
I am really not competent enough for these kinds of promotional things! What about &#8220;KDE rocks&#8221; &lt;/joke&gt;. There are plenty of reasons to use KDE, but more importantly, there are hardly any reasons to <em>not</em> use KDE! I think *nix + KDE is a great environment for almost everybody.
<br><br>

<b>If, one day, you stopped working on KDE, what could be the reason? Too much time to dedicate to a new job, to your family or what else? Or, simply, your passion for KDE is spent and so you leave the KDE team? What would you miss of the KDE experience? (Obviously we hope you can work in the KDE team for at least the next 30 years :))</b><br>
Well, I guess Kalzium will remain my main application for a long time. I guess I will always (famous words, I know) be a KDE user. In KDE 4, the <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> and <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a> stuff is the "killer feature" in my eyes, and I can make very good use of it.
<br><br>

<b>How much time do you usually spend on KDE?</b><br>
It really depends on my motivation and my free time. It varies between 0 and 10 hours a week, I guess.
<br><br>

<b>What are your plans for KDE 4?</b><br>
I want to make more use of some features provided by <a href="http://openbabel.sf.net/">OpenBabel</a>. OpenBabel is a file conversion tool which allows you to convert one chemical file format to another. I already added a GUI frontend to it, based on KOpenBabel.
<br><br>
Then Benoit Jacob and I added a 3D-viewer which is able to view any molecule in 3D. It also uses OpenBabel (to read the chemical file) but also Avogadro. Benoit coded all the math-stuff in Kalzium and is now moving that code to Avogadro so that other apps can use it as well (Avogadro is providing a library which other apps can use).<br><br>
All of our source data is now based on <a href="http://www.blueobelisk.org/">BlueObelisk</a>, so that it is also shared with other applications.<br><br>Then, of course, we have many smaller features, like an iconic view in which every icon is symbolised by an icon rather than its symbol.
<br><br>

<b>What was your first Linux distribution and why? Did you try many ones before you found the right one?</b><br>
So far I have used Gentoo, Mandrake (before the rename to Mandriva), SuSE, OpenSUSE and Ubuntu. Currently, I am using OpenSUSE 10.2 which I really like. I started with SuSE 6.4 and stayed on SuSE for one or two years. SuSE was (and still is) very popular in Germany so I simply ordered it over the internet. Other distributions were basically not available in Germany (or at least I didn't know about them).
<br><br>

<b>Which distribution do you use now? Why?</b><br>
I was using Kubuntu for about 1.5 years, but recently switched to OpenSUSE 10.2. The reasons are that, for one, it is really stable (I had a data loss bug with OpenOffice in Kubuntu which wasn't fixed after quite some time). Furthermore, OpenSUSE makes it really easy to develop for KDE 4 because it provides you with the latest version of kdelibs every Tuesday. That means I don't have to compile kdelibs myself but can concentrate on compiling kdeedu :)
<br><br>

<b>What is your favourite place in the world?</b><br>
I have seen many places in the world. New York, Dublin and Prague are probably my three favourite towns. There is no single place in the world for me, there are just too many great places to be!
<br><br>

<b>Have you ever been to Italy? If not, do you plan to come here and visit our country?</b><br>
Of course I have, several times even. But for some reasons, I never visited your capital, Rome!
<br><br>


<h2>Albert Astals Cid</h2>

<b>Can you introduce yourself to the KDE Italia readers? What did you study? What is your day job?</b><br>
I'm usually not much good introducing myself, but let's try :-) Hi, I'm Albert, I'm 25 years old, I'm from Barcelona and I'm a KDE developer, I finished my studies in Computer Science a year and a half ago and I'm currently working for a Traffic Simulation company.
<br><br>

<b>When did you hear of KDE for the first time? When did you start using Linux and why?</b><br>
Not sure about the time frame, I just remember trying to install a Mandrake 7.x disc that came with a PC Magazine and completely destroying the contents of my hard drive. That was the start of a love relationship :D I started using Linux seriously the first year of my computer studies because they began teaching us how to program in Linux and Emacs (I hate it since then).
<br><br>

<b>How and when did you get involved in KDE?</b><br>
I started translating KDE applications to the Catalan language back in 2003, and approximately one year later I thought, look, I know how to program and some of these applications could need a little love here and there, so I started creating some patches.
<br><br>

<b>What makes you contribute for KDE instead of the competition?</b><br>
Qt and People, simple as that, Qt is in my opinion almost the best toolkit around for building applications (kdelibs is the best :D) and has the best documentation. And KDE contributors are the most friendly bunch of people i've found in the virtual world, and that gives a good feeling.
<br><br>

<b>Can you say that programming for KDE was an investment? You got C++/Qt programming experience that helped you in enriching your personal skill set. Can this be of help getting a job? Could it be a good attribute to take to a job interview with a software company?</b><br>
Actually I got my current job thanks to the C++ and Qt experience I have so, yes I can definitely say that it was an investment, although that has been more of a pleasant side effect than what I really wanted when starting to contribute to KDE.
<br><br>

<b>Have you ever stayed at an aKademy conference? If so, can you tell us briefly about it? What did you do? Would you say that a KDE user/developer should participate in at least one KDE developers conference (aKademy) in his/her life?</b><br>
I was in Dublin in 2006 and Malaga in 2005 - I can completely recommend it to anybody that's remotely interested in KDE and of course any KDE contributor should be at aKademy each year, not only once in his life!
<br><br>

<b>What are the most beautiful experiences with KDE? aKademy itself? To know other developers? Or something else?</b><br>
I joined KDE because it is fun :-) Knowing developers is good but the best thing and what keeps me going through the times I think of reducing my time investment in KDE is when randomly you receive a user mail saying &#8220;Thanks&#8221;.
<br><br>

<b>Do your parents and friends use Linux and KDE?</b><br>
Some do and some do not, mostly they do not, i'm not of the evangelising kind of person so if people contact me with questions i'll answer them, but I don't go actively seeking out new followers.
<br><br>

<b>What could be your slogan to attract people to KDE? Can you give also some "reasons to stay with *nix/KDE"?</b><br>
As I said, i'm not the evangelising kind of person, but what I usually say to people when they ask is &#8220;KDE does all I need and does it better than Windows for me and does it for free, so give it a try and see if it works for you too&#8221;.
<br><br>

<b>If, one day, you stopped working on KDE, what could be the reason? Too much time to dedicate to a new job, to your family or what else? Or, simply, your passion for KDE is spent and so you leave the KDE team? What would you miss of the KDE experience? (Obviously we hope you can work in the KDE team for at least the next 30 years :))</b><br>
Time, time and time. KDE is the most absorbing hobby i've ever had, and this is sometimes bad as all absorbing things can be. So the time I decide to cut down working on KDE will be when I decide that hobby has gone too far.
<br><br>

<b>How much time do you usually spend on KDE?</b><br>
About 20 hours per week, sometimes more, sometimes less.
<br><br>

<b>What are your plans for KDE 4?</b><br>
Too many :D I'm working on a scrabble game, a pdftk frontend, a Gtali clone, lots of new content users have sent me for <a href="http://edu.kde.org/kgeography/">KGeography</a>, SVG support in Blinken, the usual duties of being <a href="http://kpdf.kde.org/">KPDF</a> mantainer and, of course, trying to make <a href="http://okular.org/">okular</a> rock even more with threaded text search, form support, and so on.
<br><br>

<b>What was your first Linux distribution and why? Did you try many ones before you found the right one?</b><br>
Mandrake, because it was the first I had a physical CD for it worked quite well. I shortly previewed SuSE, but YAST was too complicated for me. The other distribution i've tried is my current one, Kubuntu.
<br><br>

<b>Which distribution do you use now? Why?</b><br>
I use Kubuntu because Mandriva stopped working for me and then I tried SuSE and I did not like it. Finally, I tried Kubuntu and it seemed to work quite well so I stopped there!
<br><br>

<b>What is your favourite place in the world?</b><br>
Home of course.
<br><br>

<b>Have you ever been to Italy? If not, do you plan to come here and visit our country?</b><br>
I was in Italy when I was a young boy, maybe 4 or so, specifically in Venice and the Italian Alps zone, I don't remember much so I definitely plan coming back and visiting your (hopefully wonderful!) country :-).
<br><br>


<h2>Anne-Marie Mahfouf</h2>

<b>Can you introduce yourself to the KDE Italia readers? What did you study? Do you have a day job?</b><br>
I am the mother of 5 children and I have a diploma as a social worker (so nothing related to IT). I am not in employment as my youngest child just turned 1 years old so I am a mum at home! My husband is a researcher in Meteorology and we chose to live for 7 years in England and then for 4 years in Quebec, Canada. We are now back in France as it was time he re-integrated his job here in France.
<br><br>

<b>When did you hear of KDE for the first time? When did you start using Linux and why?</b><br>
I installed Linux for the first time in 1999 I think. I got a CD from a magazine and was curious to see what it was. On the desktop I ran Window Maker until I installed Mandrake which had KDE as default. I liked KDE at first sight.
<br><br>

<b>How and when did you get involved in KDE?</b><br>
I started to translate from English to French (I started to translate the KDevelop website for example, which I still do) and then I got involved in KDE-Women. I founded KDE-Edu because I thought that the good Educational software that existed at the time (KStars, KVocTrain, KTouch) deserve to be in a module and to be better known.
<br><br>

<b>What makes you contribute for KDE instead of the competition?</b><br>
I just liked the look of KDE and when I first get involved in contributing I also liked the community, I felt at ease.
<br><br>

<b>Can you say that programming for KDE was an investment? You got C++/Qt programming experience that helped you in enriching your personal skill set. Can this be of help getting a job? Can it be a good attribute to take to a job interview with a software company?</b><br>
Hmmm, I am getting too old for a job! And I don't think my programming level is good enough, I am still studying in books to improve it. For younger people, I would say that being involved in KDE programming will help them in a real-life job as even if it's a Free Software project, developers still have to follow rules and be able to interact with other people.
<br><br>

<b>Have you ever stayed at an aKademy conference? If so, can you tell us briefly about it? What did you do? Would you say that a KDE user/developer should participate in at least one KDE developers conference (aKademy) in his/her life?</b><br>
No, I never went to aKademy as I lived in Canada for the past 4 years. I hope I will go this year and yes, I think it is good to see people in real life. It's a good way to know other developers better and to work differently than using mailing lists or IRC.
<br><br>

<b>What are the most beautiful experiences with KDE? aKademy itself? To know other developers? Or something else?</b><br>
I joined KDE first for fun and then by being aware that Free Software is important. I have a shirt that says: "Free Software, Free Society" and I like that very much.<br><br>
What I like a lot in KDE is the ability to meet so many different people whom I call "virtual friends" as I come to know some of them quite well (we exchange pictures for example).<br><br>
The most extraordinary thing is that we manage to actually build a working desktop which can compete with proprietary software. All those individuals working all over the world on some bits of KDE contribute to make it an extremely accomplished piece of software.
<br><br>

<b>Do your parents and friends use Linux and KDE?</b><br>
Hmmm, no, not really. People my age don't use the computer a lot and tend to use what they bought pre-installed. My husband uses Linux as he is a researcher but my kids use Windows unfortunately.
<br><br>

<b>What could be your slogan to attract people to KDE? Can you give also some "reasons to stay with *nix/KDE"?</b><br>
I am not very good at slogans but I think KDE is very intuitive, especially for people not used to computers. KDE and Linux provide everything you need if you are not obliged to use some other software at your school or workplace.
<br><br>

<b>If, one day, you stopped working on KDE, what could be the reason? Too much time to dedicate to a new job, to your family or what else? Or, simply, your passion for KDE is spent and so you leave the KDE team? What would you miss of the KDE experience? (Obviously we hope you can work in the KDE team for at least the next 30 years :))</b><br>
I cannot see that coming. I hope it won't happen. I'll probably miss the intellectual challenge of learning lots of new things and the community.
<br><br>

<b>How much time do you usually spend on KDE?</b><br>
It really depends. When my children are on holiday I have little time for KDE and when they are at school I have more time. Last year I moved country so I spent several months without an internet connection. Right now I spend about 20 hours/week I think working on KDE. Not counting that in bed I read a Qt programming book!
<br><br>

<b>What are your plans for KDE 4?</b><br>
Well, I have many and the most important is to do more promotion on KDE-Edu. I started at FOSDEM to have a meeting with other people involved in Free Educational software and we tried to see how we could better work together. I am the kdeedu module coordinator: that means that I check if everything is OK in the module and that the applications there all compile and have no showstopper bugs.<br><br>
I also maintain KAppTemplate which is a script in kdesdk which generates KDE4 templates for people who want to start programming. I also do some work with KDevelop templates and I translate to French when I have time. What I wish for KDE 4 is to have neat KDE-Edu applications and I'd like to thank all the KDE-Edu developers for their work. We were one of the first modules to develop new features for KDE4. Our programs really improved and took advantage of Qt4 improvements.
<br><br>

<b>What was your first Linux distribution and why? Did you try many ones before you found the right one?</b><br>
I started with Red Hat then switched to Mandrake as it is French and I am French :)
<br><br>

<b>Which distribution do you use now? Why?</b><br>
I still use Mandriva as I know my way around and it works well for me. My husband also uses it and he keeps it as such on his laptop until he changes laptop 4 or 5 years later. He even insisted on buying boxes from Mandriva to contribute.
<br><br>

<b>What is your favourite place in the world?</b><br>
Paris! Where else?
<br><br>

<b>Have you ever been to Italy? If not, do you plan to come here and visit our country?</b><br>
I've been to Italy several times and I love it. I visited Roma and Venice and some other places, and have spent some short holidays in Italy a few times. I enjoy the food and the spirit Italian people have, the way they laugh - it seems to me that they love life, which I also do.
<br><br>

<i>Albert, Carsten, Anne-Marie - thank you all of you for your time!</i><br>
Giovanni Venturi - KDE Italia - <a href="http://www.kde-it.org/">http://www.kde-it.org/</a><br>








