---
title: "KOffice 1.6.3 Released"
date:    2007-06-08
authors:
  - "cberger"
slug:    koffice-163-released
comments:
  - subject: "Low Saxon"
    date: 2007-06-08
    body: "Unfortunately due to some bugs Kubuntu feisty does not support low saxon. It is certainly not the only quality problem of that KDE distribution. "
    author: "Bert"
  - subject: "Re: Low Saxon"
    date: 2007-06-08
    body: "koffice-l10n isn't in backports yet.  I will probably wait until I have admin rights on the archive sometime \nnext week before putting it in.\n\nWhat other 'quality' problems do you have?\n\n"
    author: "Jonathan Riddell"
  - subject: "Re: Low Saxon"
    date: 2007-06-08
    body: "Lox saxon does not work at all with Kubuntu because there is no nds language pack. So you also cannot configure low saxon as a language. Other problems? Firefox integration is a mess. Blogger.com login does not work with Konqueror. Same problems for Wikidot. Scanning (I have a Canon Lide25) is broken (because of an kernel USB bug), but commandline tools work though, playing midi files does not work. Printer configuration hangs here and then. And of course a certain amount of crashes I was not used to before."
    author: "Bert"
  - subject: "Re: Low Saxon"
    date: 2007-06-10
    body: "language-pack-kde-nds-base contains 563 .mo files for Low Saxon, what's the problem with it?\n\n\"integration is a mess\" doesn't tell me much of use, can you point me at bug numbers?\n\nI've never had any problems with logging in to blogger.com it works fine for me, although browser issues are likely to be upstream issues since it's not something that distributions tend to touch.\n\nIf you get me bug numbers for the rest I can point the appropriate people towards them.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Low Saxon"
    date: 2007-06-10
    body: "You cannot install \nlanguage-pack-kde-nds\nlanguage-pack-kde-nds-base\n\nBug #54581\n\nit says: Please note that you should install language-support-nds \n\nAccording to\nhttp://packages.ubuntu.com/feisty/translations/language-pack-kde-nds-base\nthe dependency language-support-nds is not available\n\nsudo aptitude install language-pack-kde-nds-base\nfails to install the package. \n\nvia manual download I found out some mirrors seem to have the package, others not, so I downloaded it manually with firefox.\n\n\nFirefox 2.0.0.4: when I download a file the option \"open containing folder\" in the download manager does not work at all. With the previous kubuntu it opened that ugly nautilus with the directory. Should be Konqueror or dolphin of course. when you download a file it is very natural that you want to find out where it was saved. currently the only way is to have a look at the properties, then I find our that firefox saved the nds-support package to my wallpaper folder, so I manually open Konqueror to get to that directory.\n\nNow, the package is manually installed.\n\nSame for language-pack-kde-nds_7.04+20070412_all.deb\n\nI forgot, when I take the Firefox download manager with the deb package and select open as an option it gets opened with Ark. Not really what you want. this time the file was downloaded to the desktop folder.\n\nSo now I take the system settings. I select langauge setting. I try to select \"add language\", but Low saxon does not show up and a option. Interestingly the menu features languages that are already installed.\n\nThen we have that confusing other option \"Install new language\"- the only reaction I get is that these buttons are now made unselectable.\n\nNow I remember there was a tool called Kcontrol. Interestingly Kubuntu does not support it. hmm.\n\nhttp://packages.ubuntu.com/feisty/translations/language-pack-nds\nher it tells me I should also install\nlanguage-support-nds \n\nAnyway, I also try to manually install that language-pack-nds file. I click on the Czech mirror. firefox is so smart to display that binary file in a window. umm, okay so download as last time.\n\nAn ugly gnome file dialogue opens on where to save the file. I select desktop. interestingly the naming proposal of the file is lost then. That really sucks. I hate this Gnome shit but Konqui does not render everything correctly. Try again. So I save again to my wallpaper folder. Use dolphin to check into my wallpaper.  folder. Again I install the package manually.\n\nTry again system settings. Unfortunately the window is still grey and I cannot select anything. Close it. Open it again. Add language. Still no low saxon.\n\nNext package: language-support-nds.\n\nOh, hmm, there is no. Okay. I give up.\n\n\n \n\n\n\n\n"
    author: "Bert"
  - subject: "Re: Low Saxon"
    date: 2007-06-10
    body: "You can use Kcontrol in kubuntu, just press alt+F2 en then type: \"kcontrol\" or \"sudo kcontrol\" if you need administration rights. But I agree, Firefox integration in kubuntu is a mess, it uses gnome stuff everywhere, but I don't mind as I love konqueror."
    author: "Terracotta"
  - subject: "Re: Low Saxon"
    date: 2007-06-12
    body: "It doesn't depend on language-support-nds, but it does Recommend on it which is a bug, I've assigned it to the appropriate person.\n\nHowever you can still apt-get install language-pack-kde-nds-base  without problems, if you have an issue try a different mirror.\n\nI can select Low Saxon in both System Settings and KControl and it works fine.\n\nWe are working on a .deb install programme for Gutsy.\n\nFirefox being ugly you need to take up with the appropriate team, we only do KDE here.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Low Saxon"
    date: 2007-06-13
    body: "no, you cannot select it, even when the package is installed. Maybe commandlines work. Who knows."
    author: "ben"
  - subject: "Re: Low Saxon"
    date: 2007-06-10
    body: ">>\"integration is a mess\" doesn't tell me much of use, can you point me at bug numbers?\n\nAlso, firefox wants to open pdf files with kghostview instead of KPDF."
    author: "Patcito"
  - subject: "Re: Low Saxon"
    date: 2007-06-11
    body: "What I really find nasty is that Kubuntu tries to restore the previous session and that means when you have opend ten pdf files from the net, it will inform you that these files are not available anymore in the temp-folder. Surprise, surprise!\n\nIt is very annoying to close all these messageboxes."
    author: "Wolfram Peters"
  - subject: "Re: Low Saxon"
    date: 2007-06-11
    body: "That is a bug with firefox or whatever browser you use. Every KDE (and I think Gnome too?) application, including the PDF viewers, is able to open the file directly from the net if given the URL. So there's no need to save it in the temp directory."
    author: "Andrei"
  - subject: "Re: Low Saxon"
    date: 2007-06-11
    body: "> Also, firefox wants to open pdf files with kghostview instead of KPDF.\n\nProbably doesn't use xdg-open\n\nxdg-open would open the user's preferred PDF reader as well as open the preferred file manager when trying to view the download target folder.\n\nYou could try installing the xdg-utils package manually and then edit the application associations in about:config\n\n\n"
    author: "Kevin Krammer"
  - subject: "Fedora 7 Packages"
    date: 2007-06-08
    body: "Straight out of our build system, reaching updates-testing any time soon:\nhttp://koji.fedoraproject.org/koji/buildinfo?buildID=8411"
    author: "Kevin Kofler"
  - subject: "Re: Fedora 7 Packages"
    date: 2007-06-08
    body: "And the langpacks:\nhttp://koji.fedoraproject.org/koji/buildinfo?buildID=8416"
    author: "Kevin Kofler"
  - subject: "Re: Fedora 7 Packages"
    date: 2007-06-08
    body: "Yay! Thanks, Kevin!"
    author: "Boudewijn Rempt"
  - subject: "Re: Fedora 7 Packages"
    date: 2007-06-09
    body: "Thank Rex Dieter for getting it built that fast. :-)"
    author: "Kevin Kofler"
  - subject: "Re: Fedora 7 Packages"
    date: 2007-06-09
    body: "They're now in updates-testing, so please get them from there (preferably from a mirror) so you don't saturate the build system's bandwidth. ;-)"
    author: "Kevin Kofler"
  - subject: "Re: Fedora 7 Packages"
    date: 2007-07-04
    body: "Now in updates. (The delay was mainly caused by the KDE 3.5.7 update being worked on at the same time, and a KOffice built against 3.5.7 refusing to run with only 3.5.6.)"
    author: "Kevin Kofler"
  - subject: "Gentoo"
    date: 2007-06-09
    body: "It's now in Gentoo's Portage. Compiling right now."
    author: "reuben"
  - subject: "Re: Gentoo"
    date: 2007-06-09
    body: "Compiled? ]:->"
    author: "kado"
  - subject: "Re: Gentoo"
    date: 2007-06-10
    body: "lol, yes"
    author: "reuben"
  - subject: "Krita"
    date: 2007-06-10
    body: "* Open normal digital camera image with 3-10 Mpixel.\n* Do basic sharpening with \"unsharp mask\" filter\n* In the (small!) preview, zoom in to 1:1 display (that's necessary to evaluate the sharpening effect) - bang - Krita becomes unusable slow....\n\n"
    author: "Max"
  - subject: "\"Asia and India\"?"
    date: 2007-06-11
    body: "In the announcement, Press contacts, it says: \"Asia and India\". Looks funny. It does NOT say \"Europe and Sweden\" for example."
    author: "Erik"
  - subject: "Re: \"Asia and India\"?"
    date: 2007-06-11
    body: "Sweden isn't a subcontinent."
    author: "MamiyaOtaru"
  - subject: "Re: \"Asia and India\"?"
    date: 2007-06-11
    body: "True, but it's bizarre nonetheless that the continent and a part of it are listed. Asia naturally includes the Indian subcontinent."
    author: "Thiago Macieira"
  - subject: "Re: \"Asia and India\"?"
    date: 2007-06-12
    body: "India isn't a subcontinent but a country. The Indian subcontinent is part of the Asian continent and containts (parts of) the countries Pakistan, India, Nepal and Bangla Desh.\n\nSweden is a country, part of Scandinavia which is all in Europe. So the original remark is perfectly true."
    author: "Henk"
  - subject: "Re: \"Asia and India\"?"
    date: 2007-06-12
    body: "Oh yeah?  Says who?  :-)"
    author: "Inge Wallin"
  - subject: "KDict - should be in KDE-Edu"
    date: 2007-06-11
    body: "Because KDict is a dictionary (client), it helps in education and KDict is one of the Killer Applications KDE has. So, it should be put in Edu or in Utilities or in Office, anyhow, it should be available with Default KDE installation.\n\nKDict is my 3rd most used application in KDE:\n\n1. KTorrent\n2. Konqueror\n3. KDict\n4. SMplayer\n5. KWord\n6. KPdf\n\nI hope KDict will be available in KDE 4.\n\n\"kdict -c\" is just superb - select a word and run \"kdict -c\" for an instant dictionary lookup :) //one can have a .desktop file with the above command too!"
    author: "Fast_Rizwaan"
  - subject: "Re: KDict - should be in KDE-Edu"
    date: 2007-06-12
    body: "Stop complaining and telling others what they have to do, and start coding. Sound harsh? Maybe."
    author: "Vide"
  - subject: "Re: KDict - should be in KDE-Edu"
    date: 2007-06-12
    body: "KDict IMHO has basically nothing of educational, it's just a simple tool/utility that finds the definitions of words.\nIt's something for \"everyday usage\" more than an educational application.\n\n> anyhow, it should be available with Default KDE installation.\nIt seems you never cared to look at kdenetwork.\n\n> I hope KDict will be available in KDE 4.\nNope, it won't be in KDE 4.0, as it was unmaintaned for years.\nAnd it won't be in KDE 4.1, 4.2, etc if nobody will work on it, or on a brand new application with KDict's functions."
    author: "Pino Toscano"
  - subject: "Arch Linux"
    date: 2007-06-11
    body: "KOffice 1.6.3 is at the Arch Linux testing repository.\nuploaded at june 09\n\nhttp://archlinux.org/packages/3180/\n\n:-)"
    author: "V Br"
---
The KOffice team today released the third minor release of the 1.6 series. As the development focus has shifted to the next major release, this new version was aimed at polishing and fixing bugs.  With this new version, three new languages are added to the list of translations: Bulgarian, Low Saxon and Nepali. You can read more about it in the <a href="http://www.koffice.org/announcements/announce-1.6.3.php">full announcement</a>. A <a href="http://www.koffice.org/announcements/changelog-1.6.3.php">full changelog</a> is also available. Currently, you can download binary packages for <a href="http://kubuntu.org/announcements/koffice-163.php">Kubuntu</a> and <a href="http://software.opensuse.org/download/KDE:/Backports/">openSUSE</a>. 




<!--break-->



