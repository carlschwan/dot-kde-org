---
title: "The Road to KDE 4: Solid Brings Hardware Configuration and Control to KDE"
date:    2007-04-24
authors:
  - "tunrau"
slug:    road-kde-4-solid-brings-hardware-configuration-and-control-kde
comments:
  - subject: "Thanks!"
    date: 2007-04-24
    body: "Another great article!\n\nSolid sounds very promising. One area where I would like to see Linux improve is power management. Linux (Kubuntu Feisty here) drains the battery faster than Windows. If I understand it correctly though the dynamic tick patch that's included in upcomming 2.6.21 kernel should help with that: http://lwn.net/Articles/138969/"
    author: "Joergen Ramskov"
  - subject: "What about the windows port?"
    date: 2007-04-24
    body: "What about the windows port of KDE4, if Solid can only manage unix-like systems?"
    author: "Capit. Igloo"
  - subject: "Re: What about the windows port?"
    date: 2007-04-24
    body: "What makes you think Solid can only support unix systems? It may be the case, that currently only support for unix systems is implemented.\nBut that's exactly why Solid is so useful. There only needs to be a Solid-backend for Windows (and OS X, and ...) implemented to make hardware-interaction reality for all KDE-appliations on that new platform. And that without changeing a single line of code in the applications."
    author: "Birdy"
  - subject: "Re: What about the windows port?"
    date: 2007-04-24
    body: "I go along with you, there's no Solid-backend for Windows, therefore Solid can't manage Windows, therefore a lot of things from KDE4 won't work with windows, but It was said KDE4 will run with windows..."
    author: "Capit. Igloo"
  - subject: "Re: What about the windows port?"
    date: 2007-04-24
    body: "> but It was said KDE4 will run with windows...\n\nLike the rest of KDE, it will only happen if contributors make it so.  The major difference with respect to Windows between KDE 3 and KDE 4 is the licensing change for Qt which makes it possible to legally develop and distribute KDE software on the platform.\n"
    author: "Robert Knight"
  - subject: "Re: What about the windows port?"
    date: 2007-04-24
    body: "That's quite a leap there... Has it not occurred to you that the people porting KDE to Windows might, as part of the port, write a Solid backend? KDE's not due out for another six months or so, a lot can happen in that time.."
    author: "Bryan Feeney"
  - subject: "Re: What about the windows port?"
    date: 2007-04-24
    body: "No, it was said that individual KDE4 apps will be able to run on windows. Not the complete environment. This main focus will still be on unix.\n\nThere will surely be a Solid backend for windows sometime, but it might not be included in KDE 4.0."
    author: "cloose"
  - subject: "Re: What about the windows port?"
    date: 2007-04-24
    body: "and there will be lots of things that do work on windows even without solid functioning properly. same with phonon.\n\nwhat you're missing here, and which has been touched on by others, is that this isn't a hand out. it isn't a hand-out on linux, bsd, solaris or the other unixes, and it won't be on windows or mac os either.\n\npeople working on the windows platform can write a backend for solid, a backend for phonon, etc. what we've done is made this _possible_. now it is up to the people who have this itch to start scratching it.\n\nit'll happen, because it can happen."
    author: "Aaron Seigo"
  - subject: "Re: What about the windows port?"
    date: 2007-04-25
    body: "Actually KDE 4.0.0 will probably only have a Windows beta released at most (thats what the word is).\n\nKDE 4 is going to be with us for a while, all its features won't come at once!"
    author: "Ian Monroe"
  - subject: "Here's a start for a Win32 Phonon backend"
    date: 2007-05-01
    body: "If someone wishes to write a Phonon backend \nfor OpenAL, Windows or PortAudio, irrKlang\nhere's some documentation:\n\nWindows Sound API:\n#pragma lib \"winmm.lib\"\n#include <mmsystem.h>\nBOOL PlaySound( LPCSTR pszSound, HMODULE hmod, DWORD fdwSound ); \nBOOL successful = PlaySound(\"explosion.wav\", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);\n\nMCI PlayBack:\nmciSendString(\n    \"open myfile.wav type waveaudio alias myfile\", \n    lpszReturnString, lstrlen(lpszReturnString), NULL);\nmciSendString(\"play myfile\", lpszReturnString, \n    lstrlen(lpszReturnString), NULL);\nmciSendString(\"close myfile\", lpszReturnString, \n    lstrlen(lpszReturnString), NULL);\n\nOther examples:\n- http://www.codeproject.com/audio/midiwrapper.asp\n- http://www.portaudio.com/  for Windows, Macintosh (8,9,X), Unix (OSS), SGI, and BeOS\n- http://portaudio.com/docs/v19-doxydocs/\n- http://www.openal.org/\n- ftp://216.61.164.51/OpenAL_Programmers_Guide.pdf\n- http://www.ambiera.com/irrklang/\n- http://www.onasticksoftware.co.uk/oas-audio.html\n- http://www.voiceage.com/codeclib.php\n- DirectSound: http://msdn2.microsoft.com/en-us/library/bb219818.aspx\n- http://www.codeproject.com/audio/dxsnd.asp\n- http://www.programmersheaven.com/2/FAQ-DIRECTX-Difference-Between-DirectSound-and-DirectMusic\n- http://www.codeguru.com/cpp/g-m/multimedia/article.php/c1575/\n- http://www.codeguru.com/cpp/g-m/multimedia/audio/article.php/c1561/\n- http://www.codeproject.com/info/search.asp?target=master+volume&Submit1=+Search+&st=kw&qm=all\n- http://www.hal9k.com/cug/links/subject169.htm\n- http://www.codeproject.com/cs/media/\n- http://www.codeproject.com/audio/\n"
    author: "fp26"
  - subject: "Here's a start for a Win32 Solid backend"
    date: 2007-05-01
    body: "WIFI API:\n- http://msdn2.microsoft.com/en-us/library/aa816369.aspx\nOther API\n- http://www2.hawaii.edu/~hermany/api.htm\nUSB API\n- http://libusb-win32.sourceforge.net/\n\n- http://osdir.com/ml/security.forensics/2005-02/msg00002.html\n- USB devices are created dynamically in: HKLM\\System\\CurrentControlSet\\Enum\\USBStor\\*\n\nNetwork API:\n- http://www.sockets.com/sample.htm\n- http://www.auditmypc.com/process/msnet32.asp\n\n"
    author: "fp26"
  - subject: "Hardware browser?"
    date: 2007-04-24
    body: "The solid webpage mentions a hardware browser. Has this program\nmaterialized yet? I would like to try it out, but I have yet to\nfind such a program after building KDE4. Is solidshell right now\nthe only way to see what hardware is detected?\n"
    author: "Bakterie"
  - subject: "Re: Hardware browser?"
    date: 2007-04-24
    body: "The hardware browser is still in Solid work branch where Solid work began.\nI started the hardware browser but I left it because I have higher priority in KDE.\n\nSee it in http://websvn.kde.org/branches/work/kdehw/"
    author: "Micha\u00ebl Larouche"
  - subject: "A little notice"
    date: 2007-04-24
    body: "It is 'solidshell --commands', not 'solidshell --comands'.\nCopy-paste doesn't work that way. )"
    author: "CHX"
  - subject: "Re: A little notice"
    date: 2007-04-24
    body: "Heh - I was playing with solidshell in one X session (KDE 4) and writing the article in the other X session (KDE 3), so I wasn't copy'n'pasting.  Which means it's just a typo on my part :)  Maybe one of the dot editors can fix it :)\n\nCheers :)"
    author: "Troy Unrau"
  - subject: "A few Updates Forthcoming"
    date: 2007-04-24
    body: "Hey, this article got pushed through to the dot just as I was in discussion with Ervin and Will about a few changes that took place during the Solid-Phonon sprint last week.  So, while this article was accurate when I wrote it, some small changes have already taken place...\n\nFirst: the term 'domains' has disappeared.  Solid has been split into a hardware detection frame (mostly a KDE style API for HAL and friends) and a configuration framework that has just moved into kdebase.  This was done to lighten kdelibs, and clean up the API.\n\nAnd yes, this stuff is part of KDE 4 in trunk already, so you'll be able to snag it as part of Alpha 1 on May 1 (assuming no delays).  \n\nCheers"
    author: "Troy Unrau"
  - subject: "an article request"
    date: 2007-04-24
    body: "as now KDE is adopting more freedesktop standard ( DBUS, XDG ..) can you please write an article about how the next version of KDE will bring as enhassement to make other non qt application( gtk mainely) better integrated to KDE\n\nbecause honestly it is a nosense situation to see that for example FIREFOX is better integrated to windows xp then to KDE\n\n\nfriendly mimoune"
    author: "djouallah mimoune"
  - subject: "Re: an article request"
    date: 2007-04-24
    body: "> because honestly it is a nosense situation to see that for example FIREFOX is\n> better integrated to windows xp then to KDE\n\nIt is also a nonsense complaining with us. Please report to Firefox, thanks."
    author: "Pino Toscano"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "You are right here. However, it hits you with a certain surprise that key applications as firefox do not integrate well enough."
    author: "andre"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "Not a KDE dev myself, but I'll repeat what was already said:  Firefox isn't a KDE app, therefore not the responsibility of KDE.\n\nWant a slap in the face?  Run Firefox on OS X.  That's a major surprise. :->  I've yet to get the Firefox Quicksilver plugin to work properly.\n\nBut back to KDE:  I agree, I wish there were more work done to integrate GTK+ apps into KDE.  But short of people patching GTK+ apps to work more closely with KDE, what should be done, eh?  GTK+ apps also tend to be GNOME apps, which is a similar-yet-completely-different desktop designed to replace KDE, with some work going into integrating Firefox into GNOME (though it doesn't integrate well with GNOME either.)  I don't think you'll see a KDE-friendly GNOME anytime soon, but that's my own opinion from the perspective of an outsider ;-D"
    author: "regeya"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "The point of Freedesktop.org is to allow Gnome and KDE to work together where we can.   HAL was created with the aim of making something useful for both desktops.   Dbus is based on dcop, but the goal wasn't just to improve dbus, but to make something more useful for Gnome.   There are many other little things going on there.\n\nKDE and GNOME disagree on things like keyboard shortcuts.  Both sides of excellent reasons behind their defaults, so it is unlikely we will ever come together.\n\nToolkit issues (gtk vs qt) are trivial to work around.   A lot of effort is going in in this.  Have you noticed that copy and paste works a lot better between applications now?   That is because freedesktop.org defined how it should work, and the toolkits all do the same thing now.  Expect to see more in the future - but only where there is an agreed upon best way.   When there are two equally good ways of doing something we are better off if the two desktops go their own way (yet attempt to work together in everything else)"
    author: "Henry Miller"
  - subject: "Re: an article request"
    date: 2007-04-24
    body: "There is the GTK-QT theme, and there are ways to easilly use the KDE filedialogs in those applications (note, from gnome, there are no ways I know off to do the reverse). Aside from that, tell me what specific things you expect from the KDE developers? Most work for integration will have to come from the firefox (or other app) ppl..."
    author: "superstoned"
  - subject: "Re: an article request"
    date: 2007-04-24
    body: "\"Aside from that, tell me what specific things you expect from the KDE developers? Most work for integration will have to come from the firefox (or other app) ppl...\"\nintegrating with KDE4 GTK-QT for example.\nhttp://www.freedesktop.org/wiki/Software/gtk-qt\n\nUbuntu uses Gnome, gnome apps are popular. Also users from windows now Thunderbird, Firefox etc and will use them. So it is important to show gtk in KDE as much consistient as it is possible."
    author: "elo"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "integrating with KDE4 GTK-QT for example.\nhttp://www.freedesktop.org/wiki/Software/gtk-qt\n\nYes, this theme is written by a KDE engineer to let Gnome apps fit in the KDE environment. You can find it (if installed) in Kcontrol: style and look of Gnome apps -> let Gnome apps use my KDE style. It's lovely, that's why I mentioned it... and it would be indeed great if it where ported to KDE 4.\n\nAnyway, I wouldn't say this is something that is lacking - it's already there, not ported yet but there is no reason to not expect it to be ported, I guess. And such a theme doesn't exist on the gnome side, you can't let your KDE apps look like gnome apps. That's why Trolltech created the clearlooks-for-Qt4 style, to make KDE apps look like gnome apps. Qt 4 even automatically reverses ok/cancel button order when run in Gnome (again, gnome apps don't do such a thing). Trolltech also made it possible to plug GTK code in KDE/Qt apps, maybe in the future KDE apps will use gnome filedialogs (poor users...) when running in Gnome.\n\nIn other words, most integrative work comes from KDE/Trolltech already.\n\nNow, as I already mentioned gtk-qt, anything I did NOT mention, and which is NOT being done, and which CAN be done by KDE in a reasonable time, which you would like to see?"
    author: "superstoned"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "> there are ways to easilly use the KDE filedialogs in those applications (note, from gnome, there are no ways I know off to do the reverse).\n\nAges ago KDE published some proof-of-concept work to allow GTK+ apps to access the Qt main event loop, allowing them to use KDE/Qt dialogs: http://dot.kde.org/1073668213/\n\nLooks like nobody was interested."
    author: "Eike Hein"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "And now, Qt4 uses the glib main loop."
    author: "alex"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "no it doesn't. qt4 still has its own event loop. but you can now integrate a glib eventloop into a qt4 application."
    author: "ac"
  - subject: "Re: an article request"
    date: 2007-04-28
    body: "*no it doesn't.*\n\nYes it does. Copy and pasted from qt 4.2 configure script:\n----\n    -no-glib............ Do not compile Glib support.\n +  -glib............... Compile Glib support.\n---\n\nGlib support means, using the Glib event loop"
    author: "Anonymous Ferret"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "fine\ni had got a lot of expectation from the next KDE, i thought it will be independant from any particular OS,any particular language, independant from engines( xine, gstreamer etc)and  even of course it use QT as the main toolkit,  other toolkits as GTK, java or whatever will get the same love. i.e; integration using some open standard\n\nwe users, we just want applicationn we don't even care/know what is the toolkit used, we are just fed up with all those stupid Kde vs Gnome, GPL QT vs LGPL GTK\nok perhaps i am just a stupid dreamer.  \n\n\nfriendly\n\n@superstoned hi man, opensuse rock"
    author: "djouallah mimoune"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "well, thats just you.\n\nwhat you want is totaly impossible. where did you ever read kde4 would do this!?\n\nusers should learn that allready care. they want features. features don't come out of nowhere...."
    author: "ac"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "I would be very happy to have GTK apps to support the KDe toplevel menu mode.\n\nAnyway, on kubuntu feisty you don't really see a difference anymore. It is like people see a crystal theme and think its a KDE app etc.\n\nThe about window is different for GTK apps but that's all. And the rest is integration."
    author: "andre"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "Sorry, but do you have any idea what a toolkit is? it's like bricks, you use to build an application. If you want to change the type of bricks a house is build upon, you must rebuild the house. So is Qt in KDE - you can't remove it, that's just totally impossible.\n\nKDE 4 is independant of a OS, sound engines, language, hardware, but not of it's toolkit - that's just not possible.\n\nKDE has gone out of it's way to integrate gnome apps in KDE - Qt can indeed use the glib event loop so you can use gtk code in KDE apps, Qt 4 automatically reverses the button order (cancel/OK) depending on it's environment, non-kde apps can easilly use the KDE file dialog (so when they detect they're running in KDE, they could use that) and there is the GTK-QT theme which hopefully will be ported to KDE 4, which lets Gnome apps use the KDE style, colors and icons. Qt even has a style similair to the Gnome Clearlooks style, because the gnome ppl haven't offered something like the GTK-QT style to integrate KDE apps in Gnome.\n\nSo KDE has done it's part, imho. It won't solve the flamewars, ppl just love to hate things, but I don't think end-users ever really cared about the difference between KDE and Gnome apps. On windows, you have far more styles and weird looking applications (eg compare IE7 with MS Word 2007 with MS Outlook 2007 with Media player 2007 with the latest messenger with notepad and you've got such a mess it's hard to believe - still windows users never complain. Same with mac, which has had a pletora of different styles in mac OS X, nobody cared)."
    author: "superstoned"
  - subject: "Re: an article request"
    date: 2007-04-25
    body: "eh ok i am not a developer, but i know what is a toolkit, i even used java swing when i was at university.\n\nsuppose i am new ISV to linux desktop, i want to develop a closed source software under linux and i don't want to pay trolltech for using the commercial licence for whatever reason ( anyway he already don't pay microsoft for developping software under windows) so what's the answear ?\n\ni see KDE, as THE DESKTOP for linux, not one of many DE, so any enhassement to make GTK even better integrated it is welcome, after all it is a free software.\n\nfirendly   \n\n"
    author: "djouallah mimoune"
  - subject: "Re: an article request"
    date: 2007-04-26
    body: "windows development isn't free. you have to buy a windows license at least. if you seriously want to make windows apps you also will buy visual studio, and if you have more than a hand full of developers you will pay for msdn. \n\n"
    author: "ac"
  - subject: "Re: an article request"
    date: 2007-04-26
    body: "For any non-trivial project the effort of doing what qt does automatically will cost more (as in $$$ you pay your developers to create the same thing) than then buying qt licenses.\n\nDevelopers know this and keep telling non-developers this, but it never seems to sink in.   No I don't work for trolltech.   I am a developer though, and trolltech has done large parts of UI work that I find boring.  (OTOH I get excited about individual bits on a wire, where I'm sure the trolltech guys would be bored out of their mind)"
    author: "Henry Miller"
  - subject: "Scanning"
    date: 2007-04-24
    body: "Well, I'll put up a vote for scanners as a hardware 'domain' to be added to Solid, but I'm not seeing it happening until 4.1 :-)\n\nJohn."
    author: "Odysseus"
  - subject: "HAL"
    date: 2007-04-24
    body: "I am no friend of HAL. KDE is adopting a lot of material that has it seeds in the GNOME area. HAL is no solution at all - making it hard getting KDE or GNOME stuff ported to other operating systems."
    author: "Zeffas"
  - subject: "Re: HAL"
    date: 2007-04-24
    body: "And the reverse is happening as well.  DBus is definitely inspired by dcop, and in most projects the 2(+) groups create the stardard together.  HAL is meant to be portable to non-Linux OSes (even though afaik most ports don't exist yet).  Fortunately Solid doesn't DEPEND on HAL, HAL is simply a backend for it, so that way if something better comes along, KDE can dump HAL in a heart beat and not break the API/ABI as well as not having to rewrite any applications.\n\nI really don't care which camp comes up with or seeds the ideas, just getting cooperation between the camps on the stuff that has pretty much only been different for the sake of being different (the icon naming scheme is one example).\n\nAlso to address your last sentence, Solid using HAL as a backend on platforms with HAL available won't make it any harder to port KDE apps to different platforms.  All it'll take is 1 person making a Solid backend for the platform and then the porting is done, instead of having to port all the hardware related stuff in EVERY single KDE app one at a time (not to mention all the developers having to reinvent the wheel for each platform)."
    author: "Sutoka"
  - subject: "Re: HAL"
    date: 2007-04-24
    body: "Exactly. Solid and Phonon make it possible for KDE apps to not depend directly on HAL etc to get the job done on other platforms, whereas GNOME made the pragmatic decision to tightly couple to HAL, gstreamer, NetworkManager etc."
    author: "Bille"
  - subject: "Re: HAL"
    date: 2007-04-25
    body: "HAL itself is an abstraction layer portable to other platforms.\n"
    author: "thebluesgnr"
  - subject: "Re: HAL"
    date: 2007-04-25
    body: "yeah, and how many does it support? Windows, Mac OS X, BSD and Linux, I suppose?"
    author: "superstoned"
  - subject: "Re: HAL"
    date: 2007-04-25
    body: "\nLinux, freebsd and solaris. More if you send patches. "
    author: "jef"
  - subject: "Re: HAL"
    date: 2007-04-27
    body: "That wasn't his point. The problem is, that you can not port HAL on every architecture and you can not simply sent in patches. As if things were that simple. Not every architecture shares the same metaphor or philosophy in doing things. And a lot of systems simply have their own HAL, there is no need to duplicate efforts."
    author: "Snobby"
  - subject: "Re: HAL"
    date: 2008-09-13
    body: "Not sure what your point was.  Are you saying Linux doesn't need a HAL because other systems have a HAL?  That makes no sense.\n\nAs I understand it, Solid is a layer between HAL and KDE, so it could work with other HALs, I'm sure.  iirc Phonon works on OS X now, so it wouldn't surprise me to see KDE apps on OS X dealing with plug-and-play events."
    author: "regeya"
  - subject: "Re: HAL"
    date: 2007-04-25
    body: "I wholeheartedly support taking KDE back to simple days of Sinclair ZX. I already cannot run KDE on my TexasInstruments 83 calculator... Now, it seems I will need to upgrade the video card in my e Nintendo Famicom just to be able to see shadows in KWin. Progress sux!"
    author: "Daniel"
  - subject: "Re: HAL"
    date: 2007-04-25
    body: "Note Kwin-with-composite can use OpenGL acceleration, but it'll run almost equally well with just XRENDER (eg simple 2d acceleration). Unlike compiz and beryl, I must note."
    author: "superstoned"
  - subject: "Re: HAL"
    date: 2007-04-25
    body: "How dose that work? dose it automatically detect if you can support OpenGL acceleration or dose the user turn of the 3d effects?"
    author: "Ben"
  - subject: "Re: HAL"
    date: 2007-04-25
    body: "Last I checked, it was just a matter of having a good sequence of fallback mechanisms... so if openGL was there, it did things in openGL -- if it isn't there, it tries using XRender for those effects that can be supported that way, and disables those that absolutely require openGL... and if XRender isn't available, it drops down into plain old kwin that everyone already knows and loves... :)"
    author: "Troy Unrau"
  - subject: "Re: HAL"
    date: 2007-04-25
    body: "So, because HAL wasn't invented by KDE, KDE shouldn't use it as a back end? You're kidding right? KDE should just use the best technology for the job, whether that means making it \"inhouse\" or using existing technologies from elsewhere. Furthermore, KDE seems to have taken an approach of adding an additional layer of indirection to on the one hand provide KDE applications with a consistent and easy to use API, and on the other hand not become too dependent on any single outside system at any time. I think that is a good approach, even if the \"just writing a new back end\" may end up being harder than it sounds or never takes place. "
    author: "Andre"
  - subject: "Re: HAL"
    date: 2007-04-28
    body: "HAL is one possible backend, and the only one currently available. There is nothing preventing one from writing a Windows (or other) backend for Solid. Its also possible to port HAL to other OSes. Solid should greatly ease cross platform compatibility, not hinder it."
    author: "Ryan Bair"
  - subject: "KDE"
    date: 2007-04-25
    body: "What I like the most about KDE is the clean and lean plattform. You don't have to install 20 packages when you want a tool to get run. KDE application developers build on the ke plattform and don't try to infect you with dependencies on their favourite packages."
    author: "andre"
  - subject: "Re: KDE"
    date: 2007-04-25
    body: "Of course, many people view that as a bad thing.  I've read many comments by people who don't use KDE as their DE, and refuse to use any KDE apps at all because they claim they don't want to install the \"mess that is the KDE libs.\"  Of course, I think that's absurd, but there are people who think that way.  :)"
    author: "A Debian User"
  - subject: "Re: KDE"
    date: 2007-04-25
    body: "Yes, and most of these people are runing mono, ( and often java too ) to get beagle indexing, and they don't complain about the mess that mono (java) is :("
    author: "kollum"
  - subject: "Better (up to date?) HAL support"
    date: 2007-04-25
    body: "Sure, I know this is also something of a distro issue, but I would really like to see better support for HAL in the future. Currently, I can only use an outdated version of HAL with KDE 3, which is annoying in terms of package management. Is HAL really changing that much between versions?"
    author: "Andre"
  - subject: "Re: Better (up to date?) HAL support"
    date: 2007-04-25
    body: "thanx to solid, this will be easier to do. Now, you have to patch and rebuild ALL KDE 3 applications using HAL, in KDE 4, just change solid, and all apps continue to work with the newer HAL. so it is much more likely that KDE will support the latest HAL!"
    author: "superstoned"
  - subject: "Porting KDE3 apps"
    date: 2007-04-25
    body: "I like the way K3B and KPlayer autodetect devices, and also when you insert a disk they recognize it. I hope they will be able to port that functionality to KDE4 and Solid, because it's really great!"
    author: "aha"
  - subject: "Re: Porting KDE3 apps"
    date: 2007-04-25
    body: "AFAIK, it is the whole point of Solid to make this kind of behaviour easy to accomplish for applications, so you can bet that you will see this (and more) in KDE 4 as well. Not necessarily 4.0 though."
    author: "Andr\u00e9"
  - subject: "Re: Porting KDE3 apps"
    date: 2007-04-26
    body: "Really, kplayer version 6.0 onwards does an excellent job of detecting \nremovable devices, like video/audio cds.  The way it handles these \ndevices and lists the tracks therin is simply brilliant.  Other programs\nhave a lot to learn from the way kplayer has been designed\n\n-ask"
    author: "Ask"
---
One of the many new technologies for KDE 4 is the often mentioned, but seldom explained Solid hardware API.  Hardware has always been a bit of an annoying element of using Linux and other UNIX-like operating systems, but <a href="http://solid.kde.org/">Solid</a> hopes to fix that for KDE 4. In many ways, Solid is like <a href="http://phonon.kde.org/">Phonon</a>, in that it's a Qt/KDE style API around already existing components at the lower level, such as freedesktop.org's <a href="http://freedesktop.org/wiki/Software_2fhal">HAL</a>.  It is already quite functional in the backend, and it's already affecting visible KDE components.  Read on for more...
<!--break-->
<p>Solid is an API for accessing device information such as available disks or networks.  It does not deal with device drivers, leaving that to the OS.  It does not deal with low level device information, leaving that to already existing, and very good tools such as HAL or other underlying subsystems.</p>

<p>Solid was introduced to the KDE world at large at aKademy, and the first signs of work were the appearance of its <a href="http://solid.kde.org">website</a>. Since then it has mostly flown under the radar, with the odd mention here and there thanks to Danny Allen's <a href="http://commit-digest.org">Commit-Digests</a>, or the occasional blog posting on the <a href="http://planetkde.org">planet</a>. If you visit the #solid channel at irc.kde.org (freenode), you'll find that it's sparsely populated, and mostly quiet. But appearances can be deceiving...</p>

<p>Solid's code base has been growing steadily for the last year and a half, with many parts of it becoming stable and usable already.  In fact, things like Dolphin and the File Dialog are already using it in places to do removable storage management.</p>

<p>Internally, Solid is broken up into a variety of hardware domains, with each domain operating somewhat independently. For example:</p>

<p>One of the long term problems with UNIX ease of use has been access to removable devices.  Many solutions have come up in the past, including kernel-based automounters (Mandrake of several years ago). HAL is the most recent project to tackle removable devices, and it does so quite well, but some distros still ship without, preventing across the board progress. KDE is building a generic API for removable devices so that applications don't have to know what's happening in the background.  And by removable devices, I mean any removable devices, not just storage. Solid already does removable audio devices, laptop batteries and more...</p>

<p>Currently, the only backend supported is HAL, so removable devices will require HAL for KDE 4.  Other backends for other OS's may be developed down the road, as HAL does not exist for every platform, but it should cover many of the more common UNIX platforms. One could even, if they really wanted to, write a backend that interfaced directly to the kernel.</p>

<p>It's not all just about removable hardware, but also about what is built into your system.  Phonon uses Solid to detect available sound devices on your system, and can seamlessly switch between output devices, including hotpluggable sound devices.  You may remember this from the demonstration in the <a href="http://dot.kde.org/1170773239/">Phonon article</a> from several weeks back of switching audio devices on-the-fly. This is not just Phonon you're seeing, but also Solid providing the available device list.</p>

<p>There are other domains underway, including incorporating existing functionality from the NetworkManager Program into Solid so that more KDE applications can become aware of it. Most of the work will be done by a backend daemon that already handles ethernet and wireless ethernet (WiFi) connections, assuming the underlying drivers are available, and includes most forms of wireless encryption that exist. By the end of this week, VPN and dialup support should be available.  We'll have to see what happens to KPPP as a result of this progress, but programs like this still have a role to play. The goal of this network work is to allow KDE applications to have a real implementation of an "offline mode", so you can read your mail, etc. without programs complaining about a lack of net connections. Will Stephenson suggests that cellular connections could automatically disconnect when no program is using the network, and so forth, and many other valuable applications will emerge as KDE 4 develops.</p>

<p>A third domain is Power Management, which is one of those areas on the Desktop where each distribution has done their own thing. Hopefully distributions featuring KDE 4 will present a more unified Power Manager interface. This domain presents an API that lets you configure and tune power and resource consumption of various elements within your system. This one is once again powered by HAL.</p>

<p>And very recently, Bluetooth support has been added. While it's still very young, it already allows device detection and connection. This is one I couldn't test, as I do not own a bluetooth device. :)</p>

<p>There is a very nice command line utility to interface with the various elements of Solid's API from scripts, or if you just want some manual control over your hardware. This program is called 'solidshell' and ships alongside the Solid libraries as part of kdelibs. An example command would be <pre>solidshell network set wireless disabled</pre> or <pre>solidshell hardware list details</pre> which will query HAL and return a list of all sorts of devices alongside some information about their capabilities.  For those of you that have KDE 4 installed and want to test this feature, <pre>solidshell --commands</pre> is your friend.</p>

<p>Down the road, support for more devices within the Solid framework is definitely possible. I can imagine support for using additional input devices on-the-fly, or using Solid to detect changes in display (new monitor plugged in) so that it becomes easier to deal with X display settings on the fly.  These domains are not yet a part of Solid, but with this architecture, they should be possible down the road.</p>

<p><em>How to help:</em></p>

<p>Kevin Ottens (aka "ervin"), the Solid lead developer, has a few suggestions for those willing to help Solid progress. The first thing he suggests is to use the API - the more applications that take advantage of Solid's features, the more complete the API will become.  Also for developers, if you wish to extend Solid to other domains, or add backends for systems that are not supported by HAL, help is welcome.</p>

<p>Other ways to help include testing hardware, and reporting problems. Especially useful are reports of hardware that for some reason works in KDE 3.x, but doesn't work with Solid/HAL in KDE 4.  If you find one of these, I'm sure the Solid developers would like to hear about it. </p>

<p><em>Editorial Aside:</em></p>

<p>While there has been some adoption of HAL (a freedesktop.org project for hardware detection and more) by distributions, each distribution has (generally) in the past had their own implementation of hardware configuration and control interfaces.  KDE of old had a policy of leaving hardware to the distributions, so this situation is partially one of our own making.  However with the advent of HAL and now Solid, it is hoped that a more uniform system of hardware config and control will arise.  Therefore, the KDE developers request that as distributions take a look at KDE 4 for adoption, they consider migrating their in-house hardware control solutions upstream into Solid, thereby benefitting all KDE users and making user support easier to provide by the community.  The ongoing work of the distributions to make hardware better for their users is always appreciated - hopefully these new KDE technologies will facilitate collaboration.</p>

<p>Until next time...</p>