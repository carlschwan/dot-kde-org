---
title: "KDE Commit-Digest for 16th December 2007"
date:    2007-12-20
authors:
  - "dallen"
slug:    kde-commit-digest-16th-december-2007
comments:
  - subject: "errors!"
    date: 2007-12-20
    body: "No introduction or content - only PHP errors about missing files..."
    author: "Dima"
  - subject: "Re: errors!"
    date: 2007-12-20
    body: "Just fixed the errors (really annoying, as it strikes while I sleep!)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: errors!"
    date: 2007-12-20
    body: "Thanks for the Digest Danny!  You rock.  This gives me some holiday reading :)"
    author: "Leo"
  - subject: "Thanks ... but"
    date: 2007-12-20
    body: "Thanks a lot, but apart from the statistics I see only this:\n\n Warning: file(): Unable to access /var/network/www/docs/commit-digest.org/issues/2007-12-16/introduction.txt in /var/network/www0/docs/commit-digest.org/content.inc on line 101\n \n Warning: file(/var/network/www/docs/commit-digest.org/issues/2007-12-16/introduction.txt): failed to open stream: No such file or directory in /var/network/www0/docs/commit-digest.org/content.inc on line 101\n \n Warning: array_search(): Wrong datatype for second argument in /var/network/www0/docs/commit-digest.org/content.inc on line 118\n \n Warning: array_search(): Wrong datatype for second argument in /var/network/www0/docs/commit-digest.org/content.inc on line 177\n\nCould you ...?"
    author: "Flitcraft"
  - subject: "Re: Thanks ... but"
    date: 2007-12-20
    body: "Weird. Earlier this morning everything was fine."
    author: "Boudewijn Rempt"
  - subject: "Re: Thanks ... but"
    date: 2007-12-20
    body: "Indeed, tis annoying... And trying to hack it by using http://commit-digest.org/issues/2007-12-16/introduction.txt doesn't work either."
    author: "J Klassen"
  - subject: "Re: Thanks ... but"
    date: 2007-12-20
    body: "> And trying to hack it [...] doesn't work either.\nWell, like the warning says...\n\nfile(/var/network/www/docs/commit-digest.org/issues/2007-12-16/introduction.txt): failed to open stream\n\nwhen there is no file, you cannot \"hack it\" to work. ;-)\n\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: Thanks ... but"
    date: 2007-12-20
    body: "I don't suppose anyone cached a copy?  I'm getting withdrawal symptoms...... ;-)"
    author: "Adrian Baugh"
  - subject: "Re: Thanks ... but"
    date: 2007-12-20
    body: "Just fixed the errors (really annoying, as it strikes while I sleep!)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Thanks ... but"
    date: 2007-12-20
    body: "We can forgive you anything, dannya - thanks for the digest! :)"
    author: "Anon"
  - subject: "KWin4 is renamed KFourInLive"
    date: 2007-12-20
    body: "Finally! Thank you very much for this, it was due for a long time."
    author: "AC"
  - subject: "System Settings Idea"
    date: 2007-12-20
    body: "Thanks Danny for an other interesting digest. I managed to read most of it in the morning before the errors happened :)\n\nI've been thinking about System Settings and how some have complained that it's annoying to navigate to Overview every time you want to go somewhere else, lacking the hierarchical structure that KControl offered. I also looked at Phoronix's cool gallery of Kubuntu + KDE4 images (http://www.phoronix.com/scan.php?page=article&item=941&num=1)\n\nAs I looked at Dolphin, I thought \"why can't we also implement a breadcrumb navigation system into System Settings?\" I hacked up an example which could show how it might work (though you can tell I'm not a graphic artist :D). In this example, you want to go from Desktop to Accessibility. You first navigate from Look & Feel to Personal, and then choose Accessibility. Or maybe we omit the top-level categories such as \"Personal\" and \"Look & Feel\" from the path (leaving them in Overview) so you could go straight from Desktop to Accessibility in the same drop-down (there would be 15 options from the General tab). To switch between General and Advanced, you'd still use Overview.\n\nI realize that the KDE developers are busy and probably won't have time to implement such an idea in time for 4.0 even if there was support, but it's something to keep in mind for the future. What do other people think? Any comments or improvements?"
    author: "Darryl Wheatley"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "Very cool idea.  The more I use breadcrumb the more I like it, and it would be nice to have a better settings navigation than the current one."
    author: "Adrian Baugh"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "but still kcontrol navigation style is much easier and clear"
    author: "non7top"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "jep kcontrol was perfect. Hierachical and Googlebased search approch, \n\nno way to top this"
    author: "doodle"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "Absolutly. Hell why was the 'minimize-items-visible-on-screen'-mantra ever introduced into kde? I hate it. bah. Leave that to the Vista/Gnome fraction..!"
    author: "eMPee584"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "I thought the kcontrol system sucked.  There was no structure at all.  System settings has its problems, sure, but kcontrol was an abomination.  However, kde is supposed to be about choice - how hard could it be to have the choice of kcontrol \"big flat list with everything jumbled together\" navigator or a nice smart breadcrumb navigator?  Hey, there could even be a settings module where you can set how settings is displayed!"
    author: "Adrian Baugh"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "Actually, kcontrol wasn't any less organized than system settings. A tree view is not flat. There were the different sections - appearance & themes, Desktop, Internet & Network, etc. - with the modules underneath. System settings has many of the same categories, and modules even, it just hides access to them when you choose a module. It also doesn't give access to all the modules available at the moment.\n\nI understand there's still work being done on system settings to allocate all the modules and update them, but the overall design doesn't appeal to me.\n\nAs for breadcrumbs, I don't see the point for system settings, because you never get further than one click away from the overview. Infact, that \"overview\" button is basically a breadcrumb already.\n\nFinally, I fail to see how spreading all the icons over a large window makes it any easier to find the setting you want than an organized list. It's the same annoyance I had with the control panel in windows.\n\nSorry about having such a negative post, I usually try to be more positive. The solution for all my problems would be implementing a tree-view like kcontrol in system settings. The tree-view could be on a panel that can be hidden by default to preserve the system settings design, but make it available to those of us that find it significantly easier."
    author: "Soap"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "Oops, criticized the breadcrumbs before properly looking at the mockup. It would be better than the current state of affairs. I forgot about the pop-up menu bit."
    author: "Soap"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "I think the real problem is that when we envisioned the kcm module we broke it up in kind of a weird way; as we added modules, we added them as new features we're added.  Take a look at Mac OS X's \"system settings\" (its very, very similar).  As much as I don't like Mac, its very well organized.  Users have a view of where things belong; this is especially the case in appearance; there is a lot that a user would consider to be an appearance option that doesn't appear (heh) in appearance.  \n\nThe best example of this, I think is that \"screen saver\" is part of desktop.  It should have its own kcm or be part of appearance.*\n\nAlso, personally I never understood the logic in the division between \"Overview\" and \"Advanced\".\n\n*Okay I looked; Mac OS X does the same thing we do, what do you know?  They do say, for clairity \"Desktop and Screen Saver\" as opposed to just \"Desktop\"; thats clearer.\n\nhttp://www.appleinsider.com/print.php?id=3343  scroll to the bottom.\n\nEh, maybe now that I look at it, its not the well organized, never mind.  System seems to just be a catch-all for those that didn't fit.  And what does quicktime have to do with Internet?"
    author: "Level 1"
  - subject: "Re: System Settings Idea"
    date: 2007-12-29
    body: "You got to think like a new user coming across the thing for the first time.  Is the expectation that KDE Linux use will grow or NOT??? Or is it just going to be an also ran for it's own little clique???  If so then fine just carry on as is, if not, then someone had better start thinking like a NEW USER, one who hasn't seen any of this before.\n\nI was a new user to KDE once, and when I ran across this Kcontrol my first reaction was what a piece of crap, not intuitive at all, I had to open each damed thing up to figure out what it does.  Most times only to discover that I had opened up something that didn't do what I wanted.  With the System Settings lay out I have a fair idea what each thing does, BEFORE I OPEN IT UP.  But if you want to keep KDE in geek land, then go right ahead, don't even consider the needs of NEW USERS."
    author: "Bigpicture"
  - subject: "Re: System Settings Idea"
    date: 2007-12-21
    body: "The minimalist-aproach has its limits.\n\nLet me put it like this:\n\nIf there are too many things visible, one will take too long to locate the wanted item.\nIf there are too few things visible, one no longer has an overview of which possibilities one has.\n\nThink of a command line. For a command line, all the items and possibilities have to be kept in your head. That is a damn fast and efficient way of interface, as long as you almost exclusively do things you know from the top of your head. If you have to look through a manual page, efficiency compared to a GUI is abysmal.\n\nThat should be a guide for a desktop interface. Frequently used parts of the desktop have to get slimer and more of the GUI can be offloaded to the user's brain. Seldom used stuff (like the control center) should display all possibilitie it offers to the user. For the control center it does not matter if the user searches for audio settings for 2 minutes, as long as the user is shown all the other possibilities he has.\n\nThat is exactly the thing Gnome got SO wrong. You can customize Gnome equally deep as KDE, but you have to digg your way through Key/Value pairs in a registry file and what effect they relate to. I understand that for KDE4 still everything will be cutomizable from the control center without having to resort to config file editing. But think carefully about \"gnomifying\" the interface too much. Less is not always better, because the less you display on a screen, the more has to be remembered by the user if it relates to possibilities.\n"
    author: "gustl"
  - subject: "Re: System Settings Idea"
    date: 2007-12-21
    body: "I've done a good job of keeping myself away from this discussion, all the way up to about..... now.\n\nI wish people would just drop this ridiculous notion that everything was milk and cookies back in the old kcontrol. The tree widget used for navigation was an abomination. For a start, kcontrol opens and all the categories in the tree are closed. So the first thing the user has to do is manually click on the tiny, and I mean *real* tiny little plus icons to open up the categories to find what is actually available before they can actually go anywhere. The icons in the tree are way too small to be recognisable or useful, and the click targets for the items themselves are way smaller than systemsettings (small=slow). And then when the use has opened the categories we add insult to injury by scrolling the tree off the bottom of the dialog and then forcing a scrollbar on the user if they want to see the rest of the list(!). \n\nThe tree was a painful thing to use and a lousy way of presenting the options to the user. So let's just give it a rest OK.\n\nAnyone who still wants a tree can exercise their \"freedom of choice\" by coding one themselves. Despite the vocal minority I haven't seen anyone motivated enough by this \"problem\" to actually do something about it, which suggests to me that this issue isn't even as remotely important as some people like to think.\n\nMerry Christmas in either case (or Happy Holidays)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: System Settings Idea"
    date: 2007-12-21
    body: "Actually, once KDE 4.0 is out, I may just do that... assuming PyKDE is capable of it. I just don't have time to stumble my way through learning C++ right now."
    author: "Stephan Sokolow"
  - subject: "Re: System Settings Idea"
    date: 2007-12-21
    body: "Let's first say that I have no preference in that particular matter, and I don't really care if the settings will be configured in KDE 4 with System Settings or with a renewed KControl.\n\nAnyway, I must say that the main issue I see with this Configuration apps, whether it's System Settings, KControl, the Mac one or Control Panel in Windows, is basically how things are sorted. It has actually little to do with the GUI itself and most of the things you actually criticize in KControl are just about how things were implemented, but not really a proof of a bad concept.\nAs someone else posted, with a setting quite similar as System Settings, there still quite a lot of things you can't find easily in the Mac app and I actually expect to have some issues to find some specific settings just the same with System Settings. And in that case, I'll have to go back and forth between different screens to get what I want, which will be probably just as painful, if not worse than clicking on various icons in a left column.\nIf it's badly sorted (and it's quite a difficult task with the amount of settings in KDE), it will always be painful. For the rest, nothing that can't be fixed with a bit of work."
    author: "Richard Van Den Boom"
  - subject: "Re: System Settings Idea"
    date: 2007-12-22
    body: "No, you don't have to hit the tiny plus at all. You can click outside the plus, you can click on the text label or the blank area right of it, and if that's not enough you can use the right and left cursor keys to expand and compress the submenus. If you don't want the tree view at all, you can switch to icon view mode in the menu and then choose \"Huge\" as icon size to get 48x48 pixel buttons. Is that big enough for you?\n\nFinally, if you're looking for a specific setting you can also use the search toolbar right above the tree, \n\nWith the regards to the scroll bar, most mice have scroll wheels these days, as do many touch pads on laptops (or some other scrolling mechanism like moving your finger along the bottom of the pad).\n"
    author: "SP"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "I agree with you thoughts. The largest complaint I have with system settings is the navigation.  The mock-up you did is much better.  I would rather have the old kcontrol style or just have each module open a seperate window.  I think the gnome-control-center is using that approach and of course Windows uses also."
    author: "David"
  - subject: "Re: System Settings Idea"
    date: 2007-12-21
    body: "I like the idea of putting several settings together, as system-settings does it. I also think the way the Gnome control center currently looks is better - I love that sidebar. Of course, I loathe the fact it opens a separate WINDOW for each setting - that's just play silly."
    author: "jospoortvliet"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "Nice idea! A system settings like this would be better than the actual :)\nPlease takt your idea into kde wish list! :)"
    author: "Marco"
  - subject: "Re: System Settings Idea"
    date: 2007-12-20
    body: "+1, great idea !"
    author: "Anonymous"
  - subject: "Re: System Settings Idea"
    date: 2007-12-21
    body: "+1 for me too, as the number of reply on the dot seem to have a real impact since a few week ;)\n\nGreat idea, it is much, much better than the actual no navigation at all (i use kde4 as my primary DE since few month now)"
    author: "Emmanuel Lepage Vallee"
  - subject: "Re: System Settings Idea"
    date: 2007-12-21
    body: "That's indeed a nice idea, but IMHO the best approach for this kind of application was Mac OS X 10.3's (=Panther's). I had the feature to drag the most common used icons to the toolbar. I don't know why Apple removed that feature from 10.4.\nSee http://www.guidebookgallery.org/pics/gui/settings/menu/macosx103-1-1.png "
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: System Settings Idea"
    date: 2007-12-24
    body: "That one is really cool"
    author: "k"
  - subject: "Re: System Settings Idea"
    date: 2007-12-22
    body: "Cool to see so many comments :)\n\nI agree that a sidebar with a similar but refined treeview a la kcontrol would also be a great idea. A popular suggestion seems to be the option to add favorite control applets to a central place like in OSX. Knowing our great community, some charitable soul will probably add that in future.\n\nJos may be onto something when he talks about getting inspiration from the clean design of the GNOME control center, such as putting the filter to the side and extending the idea to have a \"Favorite Applets\" bar that lists commonly used controls. This would leave more room for the breadcrumb thing when System Settings gets longer path names. But then again, if you look at the Desktop control in the current implementation, there is already a sidebar on the left that divides up the applet between Desktop Effects, Launch Feedback and Screen Saver, so maybe the new sidebar could be on the right. I also reckon it would be cool to see an \"Options\" button so that people can configure the look & feel of System Settings to their heart's content. These brainstorms are pretty fun!\n\nI hope everyone has a joyful Christmas (not Khristmas :P) or whatever you celebrate and have a great 2008!"
    author: "Darryl Wheatley"
  - subject: "The Phonon website really needs updating!"
    date: 2007-12-20
    body: "That Phonon website is linked over and over in announcements, but it is very out of date, the roadmap appears not to have been updated since 2006!"
    author: "Kevin Kofler"
  - subject: "Re: The Phonon website really needs updating!"
    date: 2007-12-21
    body: "Same goes for many of these cool sites. Unfortunately, nobody steps up to fix it..."
    author: "jospoortvliet"
  - subject: "Re: The Phonon website really needs updating!"
    date: 2007-12-21
    body: "until 4.0 is out nobody writing code really has time for this stuff. and people not writing code have a hard time generating the content.  "
    author: "Aaron J. Seigo"
  - subject: "This week we have better news..."
    date: 2007-12-20
    body: "Until the digest database problem is resolved, we have better news this week to read and play with:\nTrolltech is working hard on Qt 4.4 and released a first technology preview! While this will not be finished in time for the KDE 4.0.x release, we can have a sneak preview of what the future of KDE 4 will offer.\nMerry Christmas to all!\n"
    author: "christoph"
  - subject: "Re: This week we have better news..."
    date: 2007-12-20
    body: "Qt 4.4 is quite exciting indeed!\n\nWidgets on GraphicsView, Phonon, Alien (using non-native widgets to solve the ugliness that happens when you resize windows on X11) and WebKit integration are four of the (IMO) more interesting things in the release.\n\nHopefully whatever problems the Digest is having will be fixed soon and the rest of us can get our weekly fix of the Digest ;)"
    author: "Sutoka"
  - subject: "Kwin composite getting faster"
    date: 2007-12-20
    body: "This week with the introduction of libkwinnvidiahack.so.4 I noticed a noticeable speed improvement in Kwin's composite mode using the propietary nvidia drivers.\n\nKeep up the good work!"
    author: "Josep"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-20
    body: "Orly? I'll have to try it!"
    author: "Ian Monroe"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-20
    body: "That's true and the effects are smoother than before although they still haven't reached Compiz Fusion's quality as yet. The window thumbnail in the task bar is now working. I have been using KDE4 for the whole day today with very little problem. There are still rough edges and the programs aren't that smooth but it is shaping up! The biggest problem that I have encountered so far is trying to use Lancelot after adding it's plasmoid to the desktop, it only showed a black window looking like a menu and then crashed the whole session. I have been having this problem for quite a while now. Don't know what's the cause.\n"
    author: "Bobby"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-20
    body: "It seems like a problem that plasmoids can crash the whole session - I thought the whole idea of them is that they should be writeable with comparatively little programming experience, and publishable by anyone without any quality control.  Is this a design problem with plasma, or just a bug in the current state of things?"
    author: "Adrian Baugh"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-20
    body: "that's the point of the script bindings, yes. many plasmoids in svn, for various reasons, are in c++."
    author: "Aaron J. Seigo"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-21
    body: "If c++ plasmoids can crash the entire session, do you plan to remove them once the script bindings are ready? Or create some safe sandbox for them?"
    author: "Ben"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-21
    body: "You can't sandbox c++, really (except for using stuff like AppArmor/ SELinux, which will be an immense headache).  Scripted applets will inevitably swallow more resources than native applets, so stuff like the (default) systray, taskbar, menu etc should ideally be native code.\n\nHonestly, the best thing to do is to write core applets that simply Don't Crash.  Applets are generally very simple, so this really shouldn't be that hard to do.  Applets from third-party dudes are really best as scripted applets: More stable, more secure (maybe not Ruby and Python as their sandbox implementations are not currently great but, perversely, Javascript should be good), easier to install and, since they are deliberately added by the user and not part of the default install, the small amount of extra resources they take up is not too much of a disadvantage."
    author: "Anon"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-20
    body: "This isn't a kwin problem, its a plasma problem.  Since plasma is now such a major part of kde, when plasma breaks it seems like everything has broken, but that's not the whole story.\n\nBut your right, a plasma applet, no matter how malicious, should not be able to crash plasma. Hopefully, as plasma matures, it will be less of a problem.\n\nAs for kwin 4 -- yeah!  we can use that name without confusion now!  I hope the developer of the game kwin4 (kfourinline) is happy with this change, don't want to hurt anyone's feelings!\n\nAnyway, as for kwin 4, I don't find it a lot of fun; I have a very bad graphics card (ati X1400, possibly the worst ever made...) I have 4 \"usuable\"* drivers but they all suck in kde 3; in kde 4, well....\n\n*fglrx, radeon, radeonhd, and vesa.  So far, you know which one I like best?  Vesa.  Yeah, this card sucks."
    author: "Level 1"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-20
    body: "\"But your right, a plasma applet, no matter how malicious, should not be able to crash plasma. Hopefully, as plasma matures, it will be less of a problem.\"\n\nNote that no matter how \"mature\" plasma gets, native applets will *always* be able to bring the whole thing down - it's unavoidable.  Scripted applets should be immune, though, and are easier to write, download and install, anyhow, and also more secure since they are easier to sandbox (Javascript in particular)."
    author: "Anon"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-21
    body: "Of course a c++ applet will always be able to bring down the whole process if it really wants to. (Actually this issue reminds me a bit of Windows explorer.exe crashing and taking all the desktop with it) However i wonder if there isn't a mechanism, that can be used to catch segmentation fault for example. On thing that comes to my mind would be catching SIGSEGV, but i don't know how much that really helps.\nI have seen no (trivial) way of determining *which* portion of code or which plugin actually caused the signal to fire.\n\nWell, I trust the plasma devs, that they will come up with something smart, that deals with this issue."
    author: "Daniel St\u00f6ckel"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-21
    body: "> that can be used to catch segmentation fault for example\n\nit already does."
    author: "Aaron J. Seigo"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-21
    body: "Great to know :-)\nA pity that this is not a magical cure... Nevertheless I am looking forward to my first KDE4 desktop. Thank you for your hard work!"
    author: "Daniel St\u00f6ckel"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-21
    body: "Someone tell me that the nightmares I have about KDE becoming a security and stability disaster because of this single process access to everything design is because I ate cabbage for supper.\n\nPlease.\n\nDerek"
    author: "D Kite"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-21
    body: "it doesn't have \"access to everything\". that's why krunner was split out, kwin still exists on its own, plasma and krunner auto-restart on crashes, we have scripted applets. etc, etc.\n\nin kde3 we had kdesktop and kicker. now we have plasma and krunner. and in kde3 there were no scripted applets. and kdesktop wouldn't auto-restart (though kicker would)."
    author: "Aaron J. Seigo"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-23
    body: "I know that this is on of \"our\" best \"weapon\" to attack Windows but let's face the truth: downloaded content is at the very own user's risk. Fullstop.\nSecurity bugs only imply remote exploitable attacks (such as, I send you a particulary forge email which triggers a bug that let remote code execution). But a downloaded plasma applet, dashboard applet, vista gadget or what else are just downloaded software... it doesn't matter if they bring you local machine down or if they start to do a perfectly legal (to the system) action like sending personal data over the net or starting a spam run. IT security is different from 10 years ago. "
    author: "Vide"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-21
    body: "The performance it is even better with the latest nvidia driver 169.07, and also fixes a bug related to Qt ARGB scrolling which affects Plasma."
    author: "Josep"
  - subject: "Re: Kwin composite getting faster"
    date: 2007-12-23
    body: "Hi there, i'm still using the RC2 with Kubuntu gutsy, and kwin really impressed me featurewise. \n\nBut with RC2 and the kubuntu packages (don't know what part is to blame there) it's just unusable slow, even though i upgraded my XP2800+, 1Gig RAM to a Geforce 6600GT... hell, i can play FarCry on high settings, but kwin4 is like 5fps with it. I really hope that for the 4.0 release i won't have to use compiz."
    author: "Marc"
  - subject: "Sonnet"
    date: 2007-12-20
    body: "Its nice to see something about Sonnet.just...currently Spell Checking doesnt works, so is there any plan to provide a working spell checking for 4.0?"
    author: "Emil Sedgh"
  - subject: "Re: Sonnet"
    date: 2007-12-21
    body: "What exactly do you mean? Integration in Konqui, for example? Sonnet works fine and the spell check runner works, at least using English. Haven't tried any other languages though."
    author: "Ryan P. Bitanga"
  - subject: "Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "IIUC, the entire desktop is now Plasma. :-(\n\nIMHO, Plasma is all hype and very little substance.  Perhaps it just turned out to be a bigger job than anticipated (Murphy's Law will always apply).  However, we have 3 weeks to release and much of the functionality in the KDE3 DeskTop is still missing.\n\nIcons the DeskTop don't work yet.  Perhaps it would be better to report a feature after it is finished rather than when is is barely started.\n\nIf you place files in the Desktop folder, they do appear on the desktop.\n\nThe size setting in SystemSettings doesn't work.\n\nThe text under them is too large and is not the font selected in SystemSettings for 'Desktop'.\n\nNOTE: these aren't the only instance where Plasma doesn't use system settings.\n\nIf I drag a file icon to the DeskTop, I get a message box which says: \"This object could not be created\"\n\nDragging a URL to the DeskTop has the same result.\n\nI don't know how I would try to create an icon to start an application on the DeskTop.  Creating an icon using the Add Widget function doesn't seem to be ready to use yet -- is simply doesn't work.  I can't drag a menu item to the DeskTop -- actually can't drag a menu item period.\n\nI don't want this to be taken negatively.  I am just reporting what I found.  I hope that it is finished in time for the release.  For now, all I can say is that I am very disappointed and would rather have KDeskTop and Kicker."
    author: "JRT"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "Hey, I had a little graphic done in STG (scalable text graphics) to show what's happening :\n\n\nKoolness\n^\n|                                                      \n|                                     x       ------\n|                             x              | ???? |\n|                        x             x      ------\n|                     x                |\n|             x     x                  |\n|          x      x                    |\n|      x x      x                      |\n|   x   x       |                      |\n| x     |       |                      |\n|x      |       |                      |\n|       |       |                      |\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>time\n  KDE1  \\ KDE2  \\          KDE3        \\     KDE4\n\n\n\nWhere koolness is a balance between featurefullness, bugfreeness, speed, eyecandy ...\n\nIt's likely we will have to wait for 4.1 or 4.2 to use KDE4 fulltime fullspeed."
    author: "kollum"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "Ooops, since STG is not that standard, you may have to copy-past the graph in, for instance, KWrite (tested) to show it displayed correctly."
    author: "kollum"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "Just a little question...How much time did you spent(/waste?) to create this?\n(btw, i like the idea of STG so much!)"
    author: "Emil Sedgh"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "Hy.\n\nAbout one minute and a half.\n\nUnfortunately, STG needs to keep every spaces to display the cross x aligned verticaly. But puting it in a HTML frame seems to reduce any number of spaces to one. So my graph is all fucked up.\n\nI think reworking thing ( 20 seconds ) may render a lot better thought. Here I go :\n\nKoolness\n^\n|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~x~~~~~~~------~~~~\n|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~x~~~~~~~~~~~~~~| ???? |~~~\n|~~~~~~~~~~~~~~~~~~~~~~~~x~~~~~~~~~~~~~x~~~~~~------~~~~\n|~~~~~~~~~~~~~~~~~~~~~x~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~\n|~~~~~~~~~~~~~x~~~~~x~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~\n|~~~~~~~~~~x~~~~~~x~~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~\n|~~~~~~x~x~~~~~~x~~~~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~\n|~~~x~~~x~~~~~~~|~~~~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~\n|~x~~~~~|~~~~~~~|~~~~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~\n|x~~~~~~|~~~~~~~|~~~~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~\n|~~~~~~~|~~~~~~~|~~~~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>time\n~~KDE1~~|~KDE2~~|~~~~~~~~~KDE3~~~~~~~~~|~~~~~KDE4~~~~~~~\n\nLet's see the preview now....\n\nYeepee, this time, things are nearly aligned, and the graph may be read.\nOh, and I think that STG was caled ASCII art, but it's ancient time, when people saying they where computer savy still knew that text was made of ASCII caracters (or not), they didn't just knew how to donwnload the last Quake no CD crakc and click the exe to apply it and then call themselves computer experts ^^.\n\nWell, here is a tip :\nCopy and past the STG in Kwrite. press CTRL+R or find the replace tool in menu.\nand replace  ~  by one space. You may see the graph more clearly.\n\nThis is an old cryptography technic. STG is realy full featured, isn't it ^^"
    author: "kollum"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "It's not like it has been said a thousands/milions/billions time, but 4.0 has never been intended to be a \"full\" replacement for 3.5. There are area where it is better, and they are features that have disappear and will come back. If you can't live without some of them, just wait. 3.5 is still a very good KDE release. (and no releasing 4.0 when it is a \"full\" replacement of 3.5 will just be stupid as it would prevent people who are happy with the feature set of 4.0 to use it)"
    author: "Cyrille Berger"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "\"IMHO, Plasma is all hype and very little substance.\"\n\"I don't want this to be taken negatively.\"\n\nHow these statements can be combined in one post is definitely beyond me."
    author: "AC"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "don't bother!\nhe's a wanker"
    author: "asdf"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "> Icons the DeskTop don't work yet. Perhaps it would be better to report a \n> feature after it is finished rather than when is is barely started.\n\nThat's not the way the Digest works - the KDE Commit-Digest is a report of the current (and so naturally, often in-progress or unfinished) state of development.\n\nDanny\n\n\n"
    author: "Danny Allen"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-22
    body: "\"Work started on ...\"\n\nIs very clear."
    author: "whoever"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "Do you realize that most of your complaints affect areas that are under active development (check panel-devel)?"
    author: "Luca Beltrame"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-22
    body: "I think that the point is that they aren't finished yet."
    author: "whoever"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-20
    body: "> Plasma is all hype and very little substance\n\noh man, now you're not on my christmas list anymore! and i had picked out something really nice for you.\n\nseriously though .. you're complaining that a system that is designed not as a host for a \"traditional\" desktop layout but which can, with very little code actually, support such a thing is ... \"all hype and very little substance\"? heh. i'd love to see kdesktop do 1/10th of what plasma is doing. then we can talk about substance.\n\nbtw, they couple of people working on the trad support stuff are new developers .. i'm just coaching/mentoring .. if need be i'll step in to get it done but i'd like to let them get it sorted out and learn.\n\n> The size setting in SystemSettings doesn't work.\n\nfixed.\n\n> The text under them is too large and is not the font selected in\n> SystemSettings for 'Desktop'.\n \nyou didn't even mention the real problems with the text under the icons. you really suck as a vapid nitkpicker. i was all ready to give you an A+ for opening with some less than glamorous ranting (always a classy start) and then .. this. *sigh* i'm sure you can do better next time! ;-P\n\n(no, i'm not going to give away what those problems are as that would remove all the fun for you! a trip to b.k.o might help you out though! and yes, we'll be fixing those issues too) \n\n> NOTE: these aren't the only instance where Plasma\n> doesn't use system settings.\n\nother examples?\n\n> If I drag a file icon to the DeskTop, I get a message box which says: \n> \"This object could not be created\"\n\nalready fixed. we have been doing some clean ups with the applets, the names and mimetypes, etc... just hadn't gotten to the drop code yet.\n\nso your big vapour complaints are a couple of bugs that the fixes for ranged from 1-4 lines of code each. not only are you not on my xmas list for this year, i'm not getting you anything for valentines either now.\n\n> For now, all I can say is that I am very disappointed\n> and would rather have KDeskTop and Kicker.\n\nthen use them. no one is forcing you to use plasma. if you can't see beyond a few point in time bugs, then i really couldn't be bothered to try and help you.\n\nbtw, dot.kde.org isn't b.k.o or panel-devel@kde.org. it's lucky i decided to read the comments here today."
    author: "Aaron J. Seigo"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "\"seriously though .. you're complaining that a system that is designed not as a host for a \"traditional\" desktop layout but which can, with very little code actually, support such a thing is ...\"\n\nTo litle code maybe but till now plasma can't get in shape and won't be in shape for long.\n\nLets get real here, plasma is not the holy grail eather."
    author: "Div"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "> but till now plasma can't get in shape and won't be in shape for long.\n\n+1 uninformed\n\n> plasma is not the holy grail eather.\n\ncan't argue with a strawman, can i?"
    author: "Aaron J. Seigo"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "\"+1 uninformed\"\n\nThe Release candidates and daily builds are informative enougth. aren't they?\n\n\"can't argue with a strawman, can i?\"\n\nLets play the victim here, ain't that your favorite game?"
    author: "Div"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "> The Release candidates and daily builds are informative enougth. aren't they?\n\nno, 4.0 will be.\n\n> Lets play the victim here, ain't that your favorite game?\n\nhttp://en.wikipedia.org/wiki/Strawman_argument\nhttp://en.wikipedia.org/wiki/Ad_hominem\n\nhave a good day. preferably somewhere else. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "\"no, 4.0 will be.\"\n\nAnd what excuse will you made when 4.0 is released then? 4.1? and whene 4.1 is released? 4.2?\n\n\"http://en.wikipedia.org/wiki/Strawman_argument\nhttp://en.wikipedia.org/wiki/Ad_hominem\"\n\nLinks that I already read interestingh both.\n\nI got one for you:\nhttp://en.wikipedia.org/wiki/Pride  (arrogance?)\n\n\"have a good day. preferably somewhere else. =)\"\n\nWow, that is something I can quote from now on thx =).\n\n\n\n\n\n\n"
    author: "Div"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "\"And what excuse will you made when 4.0 is released then? 4.1? and whene 4.1 is released? 4.2?\"\n\nThen you should wait until those times come."
    author: "Div2"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "> And what excuse will you made when 4.0 is released then?\n\ni never said the rc was going to be \"informative enough\" to use your phrase. it's not a moving target excuse.\n\n> http://en.wikipedia.org/wiki/Pride (arrogance?)\n \nyou accuse me of arrogance for standing up against ignorance mixed with rudeness? please."
    author: "Aaron J. Seigo"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "Unsurprisingly Aaron won that one, I wonder when people will learn not to pick a fight with someone much better at it than them"
    author: "Ben"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "LOL indeed ;-)\n\nHe's pretty good at arguments. Of course, to save my selfrespect, I always attribute that to the fact that he's a natively US ENGLISH speaker (but deep in my hart I know he's just f***ing smart)."
    author: "jospoortvliet"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-26
    body: "Actually Aaron is very good in conception and understanding of structure of issue at point. A corrollary of that is that he is good at arguments, understanding and organising."
    author: "Vinay Khaitan"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "\"i never said the rc was going to be \"informative enough\" to use your phrase. it's not a moving target excuse.\"\n\nThen you confused the meaning of RC, but lets forget about it now that you say 4.0 will be, I just wonder what excuse will you invent by then.\n\n\"you accuse me of arrogance for standing up against ignorance mixed with rudeness? please.\"\n\nYes, I acuse you of arrogance, you call people complaining of plasma stability or people not putting plasma in a pedestal and looks like it makes you angry, And that's not being ignorant, we give our opinion based in what we have, we use and see, but you call us ignorants because we cannot see what doesn't exist, now rudness? come on, you are the only rude here, to defensive."
    author: "Div"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "Div, are you smart enough to be embarrassed by your little performance here?\n\nAaron, I don't know how you manage to keep be polite to a clown like this"
    author: "Borker"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: ">And that's not being ignorant, we give our opinion based in what we have, we use and see,\n\nImagine two applications. One looks and works fine. The other one doesn't. Is the second one worse? You can't say that unless you had a look at the code. The second one might be infinitely better - but it's behaving badly because of ONE line of code which contains a bug. Fix that one line, and the app goes beyond what the first one could even dream.\n\nGet my point?\n\nSure, plasma doesn't look very finished. But unless you had a look at the code, you can't really judge it. So wait with criticizing it until it's released - at that time the basics should work.\n\nAnd even then - 4.0.1 can fix some more oneliners ;-)"
    author: "jospoortvliet"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-23
    body: "Hello Jos,\n\neverybody can tell from what has been said by now, that Plasma will be virtually untested in that first release. \n\nIf Aaron thinks that people can test it for the first time only when it's release time, when do you think the first release candidate can be done?\n\nIs there something about Plasma that is making it super-trivial to test and code, and so easy to get right with the many configurations and situations there can be, that no time is needed to decide if it can be released? No release candidates are needed for that part of KDE.\n\nBut hell, Google is making a release party. That has terminated any possibility of delay by a week or two. Say it ain't so.\n\nIn the end, releases are overrated. Lets just wait for when Plasma is finished.\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-25
    body: "Yes, Kay, the fact we have a release party contributes greatly to this issue. Plasma won't be tested much, won't be incredibly stable. Unfortunately that's simply true..."
    author: "jospoortvliet"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-25
    body: "I must say that im tired of see how KDE developers bash people who don't like plasma because is unstable and is not the big deal eather calling them ignorants.\n\nNews flash, they are normal users not ignorants, be nice with them, if you can't then ignore them at least, but please don't look like a complete ass."
    author: "Gentoo User"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "Actually, plasma IS the holy grail! And Aaron Seigo is a god. :-P He's writing the darn thing; he knows what he means when he says \"with very little code, can support such a thing.\""
    author: "kwilliam"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-23
    body: "Hello Aaron,\n\nno you do read these comments every time and we love you for that. Really. I just hope you don't get the idea that this comment was representative of anything or gets you depressed.\n\nBut seriously, do you think there was a historical necessity that Plasma was finished last? Is there an expectation within you that testers of Plasma are not let down by RC2? \n\nI personally am not going to be disappointed, by however incomplete the Plasma code is. And few people have that right, as they didn't contribute to it. And no, whining comments do NOT count as helping.\n\nBut as a community member, I am sure going to be disappointed, if that unfinished state of Plasma is going to take away from the pride that every KDE developer deserves. \n\nBut guys, it's Free Software, but one big enough that \"it's released when it's ready\" can no longer apply. So we are seeing RCs with core functionality that didn't approach beta level yet. And probably with a lot of focus indeed, the final release will not be entirely unfinished. But no doubt it about, soon after release, one of the next releases, Plasma will be ready and completed in a minor release. And everybody will rejoice. And this discussion will be ended. And everybody will be proud again.\n\nAaron, why not just skip fast forward and ignore the fruitless phase of discussion now. Now the only thing that matters is code, not dot comments, and those that chose not to help months ago, should be ashamed or silent.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "I have to agree with some points about plasma:\n- too much hype (this can be told about KDE4 as a whole)\n- plasma is late, very late\n\nBUT, big BUT, I've already understood that KDE4 will be actually just a technology preview, so I don't mind anymore ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "That would be OK if it is released as KDE-4Preview rather than KDE-4.0.0"
    author: "JRT"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "And after that, KDE 4.1.x should be released as KDE-4MayWorkForSomeAndMayNotWorkForOthers.\n\nThen, KDE 4.2.x should be KDE-4Stable.\n\nKDE 4.3.x should be KDE-4ReallyStableNow.\n\nAnd so on and so forth."
    author: "JRT2"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "Sounds familiar. Isn't that how software releases usually work?"
    author: "reihal"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-22
    body: "Yes, but users don't like it.\n\nUsers would like for 4.0.0 to work 100%, but at least, 4.0.1 shouldn't have major problems."
    author: "whoever"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-22
    body: "Only the naive believes in 100% and Santa Claus.\nHey, thats it. It's Christmas and they want their free toy for Christmas.\nThats why they are crying. Stupid kids."
    author: "reihal"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: ">IMHO, Plasma is all hype and very little substance. Perhaps it just turned out >to be a bigger job than anticipated (Murphy's Law will always apply). However, >we have 3 weeks to release and much of the functionality in the KDE3 DeskTop is >still missing.\n\nLet's get one thing straight, KDE 4.0.0 will *NOT HAVE ALL THE KDE3 desktop functionality, we're trying to get the basics done here first please =) \n\nYes, plasma is a BIG project and by far, there is a lot of work going on.\n\n>Icons the DeskTop don't work yet. Perhaps it would be better to report a >feature after it is finished rather than when is is barely started.\n\nThey will work, can you just have patience please? Don't expect full functionality on the icons however. I don't know the status of the desktop plasmoid.\n\n>If you place files in the Desktop folder, they do appear on the desktop.\n\nSee above.\n\n>I don't want this to be taken negatively. I am just reporting what I found. I >hope that it is finished in time for the release. For now, all I can say is >that I am very disappointed and would rather have KDeskTop and Kicker.\n\nWhile it's nice that you point out issues. Accusing the KDE developers (and I am one of them) of '[fill in KDE4 component/Plasma] is all hype and very little substance' does little to motivate people.\n\nShawn."
    author: "Shawn Starr"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "How many of you that complain a lot about things like \"KDE4 is a Hype, plasma isn't a holy grail, blablabla\" have actually taken a LOOK of what's being done? Have you looked at the code? Read the wikis? Visited the irc channels? Subscribed to any mail list (like panel-devel, for instance)?\n\nSome kind of questions the are read here really show that some people aren't really even prepared to make criticism! If these people even had taken a little closer look at KDE now they would have to chance the way they make questions.\n\nKDE is in a really great shape now, considering things like numbers of people working on this project, the number of INNOVATIVE THINGS being done, etc...\n\nHave any of you even taken a look at plasmarc file or plasma-appletrc to have a tiny knowledge of how plasma works? (they're usually at $HOME/.kde4/share/config). \n\nIf you are not the kind of person who research before talking PLEASE be more polite when making questions or critics because it is very possible that you don't know what you are talking about!!!! \n\nThis is a COMMIT-DIGEST forum, so it is VERY important that you KNOW what you are talking about ... We're not at an Orkut Community named \"KDE is a hype\"...\n\nKDE is on release candidate, and release candidates are NOT ready yet...\n\nYou don't have to be a coder, you don't have to know how to make programs in C++,how to draw an SVG etc... you just have to be polite!!\n\nYou developers REALLY deserve a A++++++ for your work! I don't think I would bear all these negative (nonsense and uncontructive) opinions... The will you have to make KDE really a great thing is sometimes unbelieveble!! Thank you VERY MUCH!\n\nSorry for my English, still learning and it's terrible! Hope you can understand what I REALLY MEAN..."
    author: "SVG CRAzy"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-21
    body: "Well, Jesus turned water into wine, fed 5 thousand with a few loaves, healed the sick and raised the dead and do you remember what they did with him in the end? Yes we human beings are damn ungrateful!"
    author: "Bobby"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-23
    body: "I say that Plasma is much better than Jesus.  At least I get to play with Plasma on my machine *for real*, whereas that Jesus thing belongs in the same category as Santa Claus and the Leprechauns.  And when was the last time a Leprechaun made your desktop look 1337?\n\n\n"
    author: "Dario"
  - subject: "Re: Icons on the desktop in Plasma"
    date: 2007-12-29
    body: "Oddly Christmas makes some people pissed. ^^''"
    author: "Antonio"
  - subject: "thanks"
    date: 2007-12-20
    body: "Hy.\n\nThanks everybody involved in the digest for yet another interesting issue. Last week was realy gread, but this week shows a lot of little interesting things.\n\nAnd thanks danny for fixing the web page fast enought for me to read it before going to work.\n\nIt's especialy nice to see, in the whole lot of KDE4 commits, that some KDE3 stuff still happen. And every week, to see a few KDE4 rought edges be smoothed.\n\nGood work everybody"
    author: "kollum"
  - subject: "WebKit"
    date: 2007-12-20
    body: "In KDE 4.0 RC2+ (at least in openSUSE) Konqueror uses WebKit by default for web browsing. Is this intentional? It still has several issues (no cursor in text fields, not redrawing scrollbar, not reacting to submit button)."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: WebKit"
    date: 2007-12-20
    body: "And, there is no WebKit in the bugs.kde.org application list. Where should I report WebKit bugs?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: WebKit"
    date: 2007-12-20
    body: "Your distribution, or webkit.org. It's not a KDE project.\n\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "webkitpart is in KDE's playground SVN - why should those bug reports go elsewhere than bugzilla.kde.org?\n"
    author: "binner"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Because the only people who are likely to read them there are \nthe people who do not want to read them."
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "That's kind'a sad... Really, I can understand your frustration, but you're going a little too far lately."
    author: "jospoortvliet"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "It's not too far if one considers the sort of BS one has to put up with.\nCrap like what this patch reverted:\nhttp://lists.kde.org/?l=kde-commits&m=119323520703513&w=2\n\nCrap like articles about KHTML future which are wrong and published \nwithout asking a single person involved in the project.\n\nCrap like people playing PR games, and forcing me to waste time writing FAQs, and trying to explain things, which I am not very good at, and when I could be debugging. \n\nCrap that nearly blew the KDE4 release, by discouraging core contributors who have to deal with FUD from people who are supposed to be friends. Not that KDE4 is likely to be something to show off to one's parents with pride, being that some people feel that marketing solves everythign. Heck, I nearly went \"screw it\" myself, but fortunately my tendency is more to get pissed off then discouraged. \n\nThe main positive of this is that it really reinforces that our differences with Apple are those of goals, and that most of their folks are of utmost integrity, and that they have never done anything deserving aprobation.\nSure, I dislike many things they did on technical grounds, but people's technical styles differ.\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Uff. Though this was a reply to the other post.\n\nTo address this one, it's actually true: the only person I know that's working on the kpart is George Wright, on occassion. It's basically barely ever touched otherwise. Sure, TrollTech works on the Qt port, and Apple and some outside contributors work on the core renderer, but that's why those bug reports should go to them, other then wasting our time.\n\nP.S. Reading kde-bugs-dist ain't much fun as is.\n\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "Hmmm, I see your point. Sorry for accusing you of saying something sad while it was actually true ;-)"
    author: "jospoortvliet"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "I think these bugs are in the Qt port of webKit, not in WebKit in general."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Oops, forgot about that. TrollTech would want them.\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "See this post:\nhttp://zrusin.blogspot.com/2007/10/khtml-future.html\n\nAll the big KHTML hackers including the guys that actually created KHML are working on qt-webkit now. Ditching code is part of a developer life. I ditch code all the time, aseigo did ditch a lot of code when he started plasma, everybody does it. So why do you make such a big deal about ditching your KHTML patches? If you're a developer you should know that this is part of a developer life. WebKit is clearly superior, it has better support for design mode and many websites outthere (gmail, youtube, facebook, google doc etc) and it has something that KHTML will never have: market share.\nSo get over it and stop spreading FUD about webkit, it's coming to KDE, every big KHTML hackers want it, all big distros want it (opensuse and kubuntu), users want it so you're all alone now."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Funny that you mention FUD. None of the guys there are KHTML hackers.\nThey used to be, but have not been in involved in 4 years or so.\n\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Well, I've never said they are working on KHTML right now have I? Those guy are the best  web rendering engineers KDE ever had according to zack and given that zack is pretty smart and that those guys invented the whole KHTML in the first place I think we can agree on that. Now those guys, Lars, Antti and co decided to go with webkit because it is clearly superior to KHTML, if KHTML was better they would have gone for it unless they were masochistic.\nAlso these guys are working on Qt, the foundation of KDE, they moved to Norway in order to be able to help KDE. That's a pretty strong move I think, would you move to Norway just to help KDE? You're not even ready to ditch some patches to help KDE so... By the way just because they didn't work for it in the past 4 years doesn't make their opinion doesn't count especially since the KHTML developpment has been pretty static (still no design mode or gmail etc).\nSwallow your ego and get over it."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Antti works for Apple. And the other people 'decided' because their employer wanted a product, and one not tied to KDE. They weren't heard of before that. Those same people entirely abandoned KHTML for many years, leaving it basically unmaintained until the current generation showed up. And now they suddenly reappear and their opinion matters? I don't think so.\n\nWhat I am not willing to do is tie KDE's future to two corporations without an escape hatch and any sort of representation of KDE's values. I am not willing to see KDE use half-integrated codebase that's maintained only by people whose primary users aren't KDE users. I /am/ willing to throw out lots of my work; though I am definitely not willing to let other people throw it out for me. Especially people who spend more time self-promoting than fixing bugs. And doubly so people who never touched it the first place. If you're not doing the work, your opinion is irrelevant. That's the OSS way.\n\n\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Look, you have a point. We all get that point - really. I mean, really, we do. You feel neglected - but that's not true. We just weigh the arguments different than you do - and not even as different as you might think.\n\nKHTML is the default rendering engine for KDE 4.0, and probably will be for 4.1. Your work is recognized in WebKit - and will be, even if it might replace KHTML within KDE. Your opinion is valued - more than you (apparently) think. Everyone appreciates what you guys have done for the World Wide Web. KHTML and it's offspring WebKit are major players when you're talking about advancing the state of technology on the Web. We love you guys, always have...\n\nYet I do want to remind you that what happens to KHTML isn't *only* up to the KHTML developers. KDE is a community, and the community as a whole will have the final say about the rendering engine in KDE. Sure, what you say is worth more than what I say (tough you can diminish the value of what you say by being so extreme). But refusing to go with the community might lead to you and the other KHTML hackers finding yourself in the same situation XFree86 is in - sure, you can go on. Forever. But nobody will notice anymore... That is ALSO the OSS way.\n\nAllowing that would be a disservice to both yourself and the community. Please don't."
    author: "jospoortvliet"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "You're certainly quite reasonable, but it's hard for me to view the word community as anything other than a farce since some \"community\" members have basically tried to ignore what we think and to force our hand by playing PR games. Behind our backs, too.\n\nThe reality is that I am not actually opposed to incorporating WebKit, inside libkhtml. I think it has to be done the right way, a way that doesn't endanger KDE's long term interests, and in a way that actually integrates into KDE. Integrating into Qt isn't good enough. A reasonably setup permitting that sort of integration might happen for 4.1. It might not. That might happen for 4.2. It might not. It depends on whether we can work out difficult issues, both technical and social.\n\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "I can't understand how an opensource project, forked from KHTML, cannot be integrated into KDE and cannot be used by KDE in the future. I mean, today Apple seems to be happy to collaborate. If one day they change they mind, well, fork again. I can't understand your point. You're putting effort in maintaining two very similiar but still different products, so a possible future re-fork shouldn't scare you, should it?\nI'm reading your posts/messages on the net, SadEagle and while I say you a big thank you for all your great work, it seems you're suffering the NIH syndrome."
    author: "Vide"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "For all the reasons many people in FOSS remake apps that already exist : because they think they can do better.\nKHTML devs obviously tend to believe many parts of WebKit are badly designed and written. If it does not seem possible for them to implement them properly in Webkit, I can understand they want to keep their own approach.\nSo my question back to you is : why would you want them to stop it? Are you so sure of the outcome? Not me, and as such, I'm willing to have both solutions working to compare them and choose the best instead of having the choice being made before for me in that matter.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "The \"escape hatch\" is the LGPL. WebKit is free software and it stays free software. If WebKit moves into a direction that's harmful for KDE, WebKit can be forked again. WebKit is maintained by lots of full-time employed people from Apple, Trolltech, Nokia, Adobe, Google, etc. + hobby developers. How are a handful of hobby KHTML devs supposed to keep up with so many WebKit devs? Are the KHTML devs cyborgs that work 24/7?\nWhy does KDE have to stick with the current KHTML code base anyway? If the KHTML devs think that the rendering engine must be developed in KDE's SVN, then just fork it now. The platform integration work is easier than the compatibility and speed work that KHTML is missing right now. WebKit is faster and has a more compatible \"quirks mode\" than KHTML. I use mainly three computers: a 2.3 GHz Athlon 64 X2 PC with 2 GB RAM, a 2.6 GHz Pentium 4 PC with 512 MB RAM, and a 1 GHz iBook with only 394 MB RAM. I use Linux with Konqueror (v3 and v4) on both PCs and the waaaay slower iBook renders websites in Safari much faster than Konqueror."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "It doesn't work like that. You need people with skills and background to fork something, and really, you need continuity of development, otherwise things just bitrot. If you look at the history of Apple forking KHTML, it really took them awhile to get up to speed, and they had (and have) some phenominally skilled people working for them, and those were people whose job was to get this done, and who weren't juggling 5 other projects, as OSS people frequently are. Developers don't just pop out of thin air all the time to pick something up, they have to come from somewhere. And pissing off actual KHTML developers by playing games behind our backs isn't very conductive to that.\n\nAnd yes, we're considering a KHTML based on WebKit sources, as has been stated before, many times, I might add, but we won't want to fork things, but rather to try to find a way to work together. That might happen. It might not, like about 5 previous attempts, but at least things like git can make some things easier. And, again, I'd rather all the people playing PR games helped --- but of course, most of them don't have much of a background, and seem they would rather confuse our users instead.\n\nP.S. Most (all non-Apple ones?) of the companies you listed just contribute platform ports, per one of the nice Apple folks who hangs around in #khtml and who we work with on JavaScript stuff.\n\nP.P.S Platform integration isn't easy, and a lot of it is speed work - X11 sucks.\n\nP.P.P.S As for keeping up.. You're right that we can't keep up entirely, but it's actually not as dramatic as you would think. Basically, working with limited time can actually be helpful, as it forces one to make smaller, more focused changes, while someone with a lot of manpower can just brute force it.\nIt's more of \"think of a change periodically for a couple of weeks, say while taking a shower, and then make a 10-line patch\" rather than \"sit down for 3 days and make 3000 lines of changes\". One also avoids a lot of pointless changes, and doing things for tiny short-term benefits which would have to be replaced soon anyway. This sort of thing is actually one reason while working together is difficult -- there is a huge difference in work styles.\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Your reasoning is most interesting.\n\nWhatever happens with khtml vs webkit, please keep expressing your opinions in such a clear manner.\n\nIn the end, Excellency (in software) often comes from the synthesis of diverging viewpoints.\n\n\nRegards,\nKig."
    author: "Kig"
  - subject: "Re: WebKit"
    date: 2007-12-22
    body: "> WebKit is faster and has a more compatible \"quirks mode\" than KHTML.\n\nI'm afraid you are wrong on both accounts, and a propaganda victim.\n\nJust see the only DHTML benchmark the WebKit guys have been boasting about in http://webkit.org/blog/122/webkit-3-10-new-things:\n\n   http://nontroppo.org/timer/progressive_raytracer.html (Full Render)\n\nKHTML kicks WebKit's ass at it by a factor 2.\n\nAnd I really mean WebKit. When you take qtwebkit, it was actually a factor 5 last time I tested. Same goes for CSS speed tests where KHTML consistently beat the living crap out of WebKit.\n\nThere are various reasons for that. \nThe WebKit team did a lot of corporate-needs-driven premature optimizations a long time ago, that they are paying now in term of entropy and added complexity.\n\nThe rendering code has been especially lagging behind in this respect since there is only one really knowledgeable developer for it in the whole project (dwh), and he has become quite a busy man, with no replacement at all in sight.\n\nFor instance they tried to deterministically compute the visual rectangles for part of the CSS specification that was not meant to be deterministic (in-flow elements). This was all the more silly than the expected gain is modest and far offset by the decrease in maintainability, which prevent optimizing far more substantial aspects in the end.\n\nApparent pressure from their hierarchy lead their developers to do all sort of code uglifications for the sake of a \"performance holy grail\" that were actually blatant violations of the 80/20 rule leading nowhere (as a rule of thumb, you spend 80% of code execution time in 20% of the code, so it is pointless and damageable to evolutivity to try and optimize the 80% portion of the code that's not performance-critical).\n\nTheir are lot of examples along this line of too early solving, like the fact\nRenderObjects coordinates cannot be simply iterated upon in WebKit, because of a design problem with table rows . This is even more damaging since another design problem with table was solved with a less-than stellar patch that requires you to add some random offsets here and there to absolutely positioned objects (the ugly absolutePositionForContent).\nIn the end, the new developer is just facing an ugly mess.\n\nThat's not helping, and probably explains why so few rendering patches have been contributed by developers other than Apple.\nTrolltech, for instance, has not contributed a *single* core rendering patch since they began working on their backend. That's something very unsettling.\n\nAs for web compatibility, well, I just have before me a list of a good 30 (thirty) very significant compatibility defects that are not in any other engine in the market; and certainly not in KHTML.\n\nCompatibility has never been the forte of WebKit.\nSince the beginning Apple has preferred moving forward and having web designers turnaround the compatibility bugs by agressively marketing the Safari web browser and its User Agent string.\nIt's not by chance than Zack Rusin has been defensively stating \"we'd better match Safari bug-for-bug\"... it's rhetoric defense of WebKit's severly lacking in this domain.\n\nThis is a bit saddening when you realize your derived webkit browser is not going to have the same user agent string, and will therefore never have the web \"compatibility\" that the Safari browser enjoys and which is based on User Agent string sniffing from JavaScript.\n"
    author: "Spart"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "Wow.  That sounds messed up.\n\nI'm of the opinion that Apple chose KHTML as a base for a reason (cleanness and correctness).  They've since moved away and it sounds like they are maving away in ways that aren't good.  \n\nI thought before that KDE should stick with KHTML (own project, one not driver by corporate concerns and tied to a specific platform outside of second class ports).  Hearing this makes me think there are other reasons to stick with KHTML as well: the correctness and cleanness that made it what it is. \n\nSo I'm all for staying with KHTML.  Fortunately, unless someone steps up and codes (and shows his solution is superior), that is exactly what will happen."
    author: "MamiyaOtaru"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "You should blog about this (maybe just copy your post)... Get this info out, it is very interesting and it would be informative to get replies from others on this."
    author: "jospoortvliet"
  - subject: "Re: WebKit"
    date: 2007-12-22
    body: "The reason why they \"abandoned\" KHTML is because they couldn't keep up with WebKit just as you can't. But instead of sticking their head in the sand they tried everything to find a good engine for KDE. So they first went for Gecko but the code was so messy that they had to give up. By that time, webkit was born and doing great, so they did the smart thing and tried to integrate it into KDE. That shows how much they care about KDE and about KDE HTML engine, in other words: they care about KHTML. And for this reason, their opinion matters a lot, just like yours.\n\nAlso, all the things you say about webkit and apple makes no sense. WebKit is LGPL so if things go wrong you can always free yourself from apple. Especially thanks to the wonders of Git and now that Trolltech is doing exactly that (staikos Git repository), there is no reason to worry already."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-22
    body: "Excuse if this comes across in the wrong way but who exactly are you and why should I as an uninformed bystander pay any value to what you say?  I have worked out who 'sadeagle' is and his credentials but 'Paul', who are you, and what have you contributed in any real sense apart from these sniping comments?  You obviously aren't a professional/mature coder from either camp as you appear to have no clue about either codebase.  \n\nI hate these armchair (backseat) code architects chirping up in matters which they invest little in themselves.  If it's not khtml/webkit it's plasma or the menu..  does anyone really believe things get sorted out for the better in this forum?  Far more likely is this is evolved by the experts and *do-ers* behind the scenes and between themselves, at least I hope so."
    author: "newbie"
  - subject: "Re: WebKit"
    date: 2007-12-22
    body: "The problem is that SadEagle always systematically downplay qtwebkit and people who work on it as being non interested in KDE affairs. People who don't know about the situation might believe everything SadEagle say about KHTML and so I'm just stating what Zack, all qtwebkit developers and many users think of the whole situation to inform people and put some balance in there.\nYou have the right to disagree and keep on working on KHTML for the rest of your life as nobody cares, but don't try to say that KHTML will never get webkit or that there is a secret ploy against KDE coming from qtwebkit. That's just ridiculous, false and hurting KDE."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-22
    body: "Well, QtWebkit alone won't satisfy KDE's needs as it has to be turned into a KPart to fully replace KHTHL and right now it seems to be more or less moldering in svn. For all the pressure some users and developers are exerting on the KHTML developers I don't see anybody standing up and saying \"Screw SadEagle and his lunatics, we want WebKit and we'll make it happen NOW!\". Maybe the situation will change after the release of KDE 4.0 and Qt 4.4 and some non-KHTML developers will divert their attention to developing and maintaining(!!!) a Webkit KPart but trying to bully the existing KHTML people into doing the work although they have reasonable reservations and/or simply don't want to is very untactful and very ungrateful considering the work they have done. \"He who codes also decides\" still applies here."
    author: "Erunno"
  - subject: "Re: WebKit"
    date: 2007-12-22
    body: "I second that. KHTML does provide quite a good level of service and instead of playing the FUD part, Webkit supporters should just make it working for KDE and allow the users to compare instead of trying to tell people what to do. Maybe they're right and Webkit is the future (I tend to believe it's the case) but I think they need to prove it and not just try to discourage KHTML devs by claiming they're making useless work. Actually, I'm sure that's what they're doing, but they should do away with the FUD part, IMO."
    author: "Richard Van Den Boom"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "Damn, my last sentence reads like I claim KHTML devs do useless work. I meant I think that I believe Webkit people are working to prove Webkit is the future.\nAfter reading Sprat's post above, though, I must say I'm a bit dubious and rather glad to still have KHTML."
    author: "Richard Van Den Boom"
  - subject: "hardly moldering"
    date: 2007-12-25
    body: "I think pretty much everyone thinks that a completed QtWebKit KPart is inevitable, no is bullying current KHtml devs. QtWebKit is relatively new project, you can hardly say that its even newer KPart (that I didn't even know about until this thread) is \"moldering\" in SVN."
    author: "Ian Monroe"
  - subject: "Re: WebKit"
    date: 2007-12-22
    body: "To answer your post, I'm not a KHTML developer but as a developer I ditch code all the time. Sure it's a pain sometime to throw away hour of work. But when I do so it's generally because the new solution is way better than all the code I've been writing for hours. Now if I were working on a web engine, I would throw my code anytime for WebKit :) If I could get something as powerful as webkit backed by many coorp to replace any of my projects, I would ditch them right away. This is one of the reason I use Free Software, you know linux, emacs, kde etc :)"
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "Well, that would be wise if WebKit was better than KHTML. But unless you had a good look at both codebases you can't know that. I haven't seen many wellinformed comments about that, except http://dot.kde.org/1198130504/1198185607/1198186308/1198187782/1198229673/1198254292/1198263096/1198264481/1198265184/1198265916/1198272132/1198359667/  and I would love to see comments from other developers who know both codebases about this topic."
    author: "jospoortvliet"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "Who cares about that benchmark, here is one that runs faster on qtwebkit than khtml:\nhttp://webkit.org/perf/sunspider-0.9/sunspider.html\n\nAnyway, nobody cares about javascript benchmarks. What users care about is being able to use their favorite browser on their favorite website such as gmail, google doc, youtube, facebook you name it. Now webkit allows me to do just that. And for the matter KHTML on KDE 4.0 doesn't even work with youtube and screws up with digg comments while qtwebkit does."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "> Now webkit allows me to do just that\n\nWhich WebKit browser other than Safari did you test this with?\n\nBecause last time I checked even Google's browser check was broken and checking for Safari instead of WebKit, so any other WebKit browser would still be served the fallback."
    author: "Kevin Krammer"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "I compile qt4.4 from staikos Git repository and it comes with a demo browser that uses qtwebkit and works really great, it gets really better everyday and already beats the crap out of KHTML in KDE4.0 as it works with gmail, facebook, google doc etc..."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-23
    body: "KHTML works with gmail and facebook. I can't comment on google doc.\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "gmail standard view with chat on? hm last time I checked it didn't work. And why can't you comment on google doc? cause it doesn't work? :D"
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "SadEagle's above answer was IMO just saying he can't comment because he has not tested. And don't tell me your sentence was humor, after all the bashing you've done on KHTML, putting a smiley at the end does not help.\nApart from that, I use Gmail and Youtube everyday with Konqueror 3.5.8 without issue yet (I don't use Chat though so I assume you may be right, but I suppose that's nothing that can't be fixed). In fact, there are very few sites where I have incompatibility issues with KHTML, and it's quite notably faster than Gecko on large page with a lot of CSS. \nIn any case, I don't see the point of all this : just make a kdewebkit available to everybody to check if it works better or not tha KHTML, for all the uses made of KHTML. The better will win, period. There's no reason to throw in any propaganda before that happens."
    author: "Richard Van Den Boom"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "I'm not bashing KHTML, I'm just saying it is way inferior than webkit and that's not because KHTML devs are incompetent or lack skills, that's because a bunch of people can't keep up against Apple+Adobe+Nokia+Google+trolltech+KHTML creators and they all provide bug fixes and features not only platform ports. \n\nAs for youtube, did you try to scroll down the related videos box? it doesn't show the video thumbnails and did you try fullscreen videos? Ok.\nNow try this:\nhttp://www.carto.net/papers/svg/dock/index.svg\nand that:\nhttp://webkit.org/demos/editingToolbar/\nit works with FF and webkit.\n\nKHTML still doesn't support design mode so no fckeditor or google doc or any WYSIWYG editor whatsoever and that sucks. People are already working on webkitpart and qtwebkit, we might even get it for KDE4.0 actually and certainly for 4.1."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "Well, you have a problem with your settings as video thumbnails and fullscreen videos have been working for me for monthes. My daughters use it all the time to watch Simpsons and Malcom in the Middle. Maybe you have some remaining old configuration settings in .kde that screw some things, I know I sometimes had such an issue in the past. Or maybe you should use the Flash 9 player if you don't yet, it works quite well for me (well Youtube fullscreen mode, with the resolution and bitrate,... never mind). But it does not seem related to a KHTML issue in itself to me. \nAs for your reasoning about Adobe+Apple etc., that's the obvious propaganda heard by Zack Rusin and co and it **sounds** reasonable in the first place. But I actually have my doubts about it. Corporate habits are very different from open source ones (except maybe for Trolltech) and making companies work together is often a headache, as each one tries to push their solution that best suits their own needs. Having quite a lot more companies behind it has not helped Gnome being a far more better desktop than KDE, despite all the claims made 5 years ago. It may bring very good things but it can end up an ugly ego and interest mess.\nAs for WYSIWYG, I used Dreamweaver years ago, but I've been quite happy moving to coding HTML with tools like Quanta recently. WYSIWYG may help people to code their blog, but I don't see it as a mandatory service provided an HTML renderer and it obviously doesn't help you code complicated DHTML pages. \n\nAnyway, I still find surprising your claims that Webkit is \"far superior\", based apparently on a rather partial experience. Especially when you discard not so \"superior\" benchmarks that even Apple use.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "I think you've never used youtube with konqueror or you don't know what you are talking about, here is a screenshot, it doesn't work with any of my kde installs on different PCs and different distributions. Scroll down the \"related video\" box and see that the thumbnails don't show up, they do show at the top but not when you scroll down and this has nothing to do with flash, it's pure AJAX and HTML. Talking about dev, WebKit comes with inspector that is almost as great as FireBug, something that most web developer can't live without.\n\nAs for WYSIWYG, I'm not talking about designing webpage using it, of course it is silly to develop a webpage that way. I'm talking about text_area in HTML form that use WYSIWYG such as the fckeditor, blogger.com, gmail, google doc, yahoo mail  etc... I also forgot http://meebo.com which is a real useful site that doesn't work at all with khtml.\n\nAs for the benchmark, checkout this one:\nhttp://webkit.org/perf/sunspider-0.9/sunspider.html\nwebkit is way faster than khtml on that one but I wouldn't care even if it was a bit slower as long as I can actually use my favorite webpages with my browser.\n\nAbout the development of Webkit, they have an open process and SVN and mailing list, it is very friendly and nobody is \"pushing their agenda\", if you think there is some kind of conspiration check the ML and see for yourself."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: "I have Debian testing/unstable 64bit installed and ever since the nsplugin wrapper has been available for use in Konqueror flash has pretty much worked without fail on any site I have tried including Youtube with the little previews you seem fond of. As to the meebo I get to the login page so that works I would think not at all would mean the page would not load ... holding that thought I just said to myself why not try to create an account guess what it worked without problems the rendering could be a little better but oh well you are so full of it it is not funny. Here is a couple of screen shots on your non-working websites.\n\n\nhttp://users.eastlink.ca/~stephencormier/youtube.png\nhttp://users.eastlink.ca/~stephencormier/meebo.png\n\nWho knows though perhaps they just don't work properly on your install but it only took me about 40 seconds including signup to disprove the meebo assertion, the youtube always worked for me so it does not help make your points with me at all."
    author: "HappyTux"
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: "Here a sreen shot from the KDE 4 SVN showing what you say is wrong.\nNow please go spread your lies elsewhere, you clearly don't now what\nyou are trolling about.\n\n"
    author: "m."
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: "Well youtube and meebo still don't work on KDE3.5.8 and KDE 4.0 RC2 at least here, maybe it was fixed in the mean time. Anyway design mode won't work, that's just a turn off not to mention the lack of debugger and the fact that nobody tests its browser on KHTML and that it will always stay that way. As a Web developer I would love to support KDE but with KHTML it justs give me unpaid extra work my clients don't even care about so this is why I and 99.9% of web devs will never design their webpage with KHTML in mind and won't test it and this is the reason why kHTML will always be lagging. Notice that I'm not bashing the KHTML devs here, it's just the reality of the market, no fuck the market, it's just how devs work it is really annoying already to work around IE quirks, firefox, safari and opera so don't expect the situation to improve. Youtube and meebo might be working but it took 4 years already, do we have to wait that long every time a new website comes around? Knowing that gmail and google doc and design mode in general still don't work it looks like KHTML will always need a few years to catch up. Sorry but I and most users won't wait.\nIt doesn't even matter how good KHTML is, if people don't test their websites on it it won't work, don't you get this? This is why Opera is suing Microsoft because even with the best engine around they can't support as many sites as IE.\nAnyway, aseigo and the principal KDE guys have made it clear that WebKit is coming and will replace KHTML so I'm happy and I don't care if you still want to develop it until death :)"
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: "> not to mention the lack of debugger \n\nwow, you really are not much current with KDE development are you? :)\n\n> It doesn't even matter how good KHTML is, if people don't test their\n> websites on it it won't work\n\nthis look like a typical Windows developer mindset :)\n\nHave you ever heard about standards?\n\nLook, I'll take a real example to make it very plain to you why you are wrong.\nSay a developer will use this very basic html snippet:\n\n<style>b{vertical-align:-40}</style><b>___<b>___<b>___<b>___\n\nAs you can see, in Opera, MSIE 6/7, FireFox and Konqueror this will render as a staircase, as the CSS specification *mandates*.\n\nIn Safari, on the other hand, it will erroneously render as a flat line because of so-so compatibility.\n\nNow, what will the Web developer do?\n\nEither he doesn't care about Safari and let the bug stay, or he'll just sniff the User Agent string and make a workaround for Safari.\n\nEnd result: it will work in Konqueror, it will work in Safari, and it won't work on your WebKit-derived also-ran browser nobody has heard about.\n\nQED.\n\n"
    author: "Spart"
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: "You mean KHTML has something like the web inspector and firebug? cool I hope we'll have it in KDE4.0.\n\n> In Safari, on the other hand, it will erroneously render as a flat line because of so-so compatibility.\n\nI think you don't get my point, of course that WebKit has its quirks, every brothers has theirs and so does KHTML. The thing is that web developers will only work around big browsers quirks because nobody uses KHTML so it's not worse looking at them.\nAnd for the matter, I'm a Gnu/Linux developer, I code using emacs and I test on qtwebkit, FF, Opera, IE4Linux and even KHTML actually sometimes but I'm afraid I'm kind of the only one there :)"
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "a \"quirk\"? that is an interesting understatement for a\nfailure at handling a basic CSS 1 property :)\n\n\n"
    author: "Spart"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "> a \"quirk\"? that is an interesting understatement for a\n> failure at handling a basic CSS 1 property :)\n\nWhatever you want to call it. My point is that web developers will try to work around that failure whereas they won't try to work around KHTML failures or quirks or whatever so KHTML will always need to play catch up and need a couple of years to be able to render high profile websites such as youtube, google mail, yahoo mail or whatever, therefore the need to replace KHTML with webkit which was my point in case you haven't noticed yet :)"
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: ">> not to mention the lack of debugger \n> wow, you really are not much current with KDE development are you? :)\n\nCurrent? It's been there since 3.2, from close to 5 years ago. Now, it had its share of problems, as does the current incarnation (the third one, I think), but sucking isn't the same as not existing.\n\nP.S. Do you think we should move the DOM Tree Viewer to kdebase for 4.1, perhaps? I hate it getting stuck in extragear..\n\n\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "> P.S. Do you think we should move the DOM Tree Viewer to kdebase for 4.1, perhaps?\n\nI'd definetly welcome that... it doesn't get half the care it deserves and it's an important tool.\n"
    author: "Spart"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "Still trying to deny the facts?\nYoutube has not just \"been worked out recently\". We have been using it without any issue for 8-10 monthes at home using KDE 3.5.X. And I'm using Slackware with a prefect vanilla KDE, without any added twicks by the distro that could make KDE break. So try a fresh install or another distro, your settings are obviously faulty but not KHTML rendering.\nAs for the \"principal KDE guys\" decision, I haven't read anything yet providing such choice with such clarity. And I'm sure they'll just test and compare before making such fondamental choice, they won't just decide out of whim or religious belief.\nFor the rest, your view of open source seems really different than mine. Adobe doesn't check if KPDF renders PDF well, does it stop you from using it? BTW, the coming IE8 seems to have passed the Acid2 test, so there's a faire chance that in the future, sites will be more and more standard compliant. So there's not reason to believe KHTML will always play catch-up.\nI must say that I don't really feel any empathy with the way you view things. Seems like using your arguments, there would never have been any free software."
    author: "Richard Van Den Boom"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "I'm not denying the fact, I'm just saying it doesn't work here (just like gmail, yahoo mail, meebo and everything that has design mode in it), that's why I sent a screenshot to prove my good faith.\n\nAseigo said numerous time that he wanted webkit though he said let the market make the choice to not piss off the few KHTML devs touchy feelings, so did Zack and distributions are already making that choice: Kubuntu and OpenSuse, the two biggest sponsor and supporter of KDE, the two distro that offer best support for KDE has gone WebKit.\n\nThere is one thing you don't seem to get, in web browsers, market share is everything, it doesn't matter (unfortunately) how good a browser render standards. Browsers will always be buggy and will always render some stuff differently from each other and web devs will only test the big browsers out there. So yes, KHTML will always need to play catch up and be a couple of years behind, like it or not. And Acid2 doesn't mean anything, it's just a way to show that the browser handle some errors correctly, even the KHTML devs had said that in the past. Give me CSS2 compliance from IE and fix bugs such as minmax and absolute positioning and we might take them seriously.\n\nWhy do you say there wouldn't have been any free software? WebKit is Free Software last time I checked and it's the best out there. Even RMS is saying that keeping developing the HERD is not useful now that we have a better alternative called Linux."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "> Kubuntu and OpenSuse, the two distro that offer best support for KDE has gone WebKit.\n\nThat's not true, not for openSUSE (the devel distro doesn't contain a WebKit package currently at all) nor Kubuntu (Jonathan said that no decision has been made about this)."
    author: "binner"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "> Anyway, aseigo and the principal KDE guys have made it clear that WebKit is \n> coming and will replace KHTML so I'm happy and I don't care if you still want >to develop it until death :)\n\naseigo has no right to make such a statement, since it's not a decision he can make, no matter how many interviews he gives and blog entries he writes. And I don't know who the heck \"principal KDE guys\" you are speaking of are. \n\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "\"aseigo has no right to make such a statement, since it's not a decision he can make, no matter how many interviews he gives and blog entries he writes. And I don't know who the heck \"principal KDE guys\" you are speaking of are.\"\n\nHe certainly does have a right to make that statement, just as you have a right to say that you don't want that to happen by making KHTML better than WebKit in Qt and able to render the vast majority of web sites out there reliably. However, after years of KHTML's *failure* at being able to do that, you and I both know that isn't going to happen. It simply doesn't have the market share behind it for web developers to ever take notice, or the wider development community that WebKit has in order to make that larger market share happen. You can only close so many bugs as 'WONTFIX' asking web developers to change. They won't, and what's more, KHTML doesn't have the browser share clout to do it.\n\nUsage of a particular engine will gravitate to what users use, what works best for KDE's developers and what generates fewer bug reports in the 'this web site doesn't work' department. The odds are, logically speaking, that's going to be WebKit. You have no control whatsoever over what users and KDE's developers actually use I'm afraid.\n\nYou're free to denounce WebKit all you like, but it isn't going to make a difference to everyone else or to those facts - no matter how many blog entries and comments you make ;-)."
    author: "Segedunum"
  - subject: "Re: WebKit"
    date: 2007-12-27
    body: "I see that lying to our users is now acceptable. I'll keep it in mind.\n\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-27
    body: "> I see that lying to our users is now acceptable. I'll keep it in mind.\n\nYou could also keep in mind that KDE will switch to WebKit, might help some day when you'll end up like XFREE86 devs. They used to be almighty and now those that stayed there just have zero influence and nobody use their stuff anymore. But sure, KHTML is the future, yay..."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-27
    body: "\"I see that lying to our users is now acceptable. I'll keep it in mind.\"\n\nWhatever. Users and KDE developers will use the rendering engine component that works best on the vast majority of sites out there, without having to do jiggery-pokery with user agents and having to file lots of bugs for stuff that works in IE, Gecko and WebKit.\n\nIn terms of your 'users', those are the only 'lies' that matter. No one is denouncing or trying to destroy KHTML."
    author: "Segedunum"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "Well, you have a problem with your settings as video thumbnails and fullscreen videos have been working for me for monthes. My daughters use it all the time to watch Simpsons and Malcom in the Middle. Maybe you have some remaining old configuration settings in .kde that screw some things, I know I sometimes had such an issue in the past. Or maybe you should use the Flash 9 player if you don't yet, it works quite well for me (well Youtube fullscreen mode, with the resolution and bitrate,... never mind). But it does not seem related to a KHTML issue in itself to me. \nAs for your reasoning about Adobe+Apple etc., that's the obvious propaganda heard by Zack Rusin and co and it **sounds** reasonable in the first place. But I actually have my doubts about it. Corporate habits are very different from open source ones (except maybe for Trolltech) and making companies work together is often a headache, as each one tries to push their solution that best suits their own needs. Having quite a lot more companies behind it has not helped Gnome being a far more better desktop than KDE, despite all the claims made 5 years ago. It may bring very good things but it can end up an ugly ego and interest mess.\nAs for WYSIWYG, I used Dreamweaver years ago, but I've been quite happy moving to coding HTML with tools like Quanta recently. WYSIWYG may help people to code their blog, but I don't see it as a mandatory service provided an HTML renderer and it obviously doesn't help you code complicated DHTML pages. \n\nAnyway, I still find surprising your claims that Webkit is \"far superior\", based apparently on a rather partial experience. Especially when you discard not so \"superior\" benchmarks that even Apple use.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: ">  a bunch of people can't keep up against  Apple+Adobe+Nokia+Google+trolltech+KHTML creators and they all provide bug\n> fixes and features not only platform ports.\n\nRight.. you should read \"The Mythical Man-Month\". It would explain to you why\nyour mighty list is completely irrelevant.\n\nWe have been 'keeping up' allright since four years without major problems.\nThe only thing we can't keep up with, to be honest, is the nauseating FUD from\n people who are apparently most ennoyed by KHTML's mere existence and have no intention of letting us compete freely.\n\nBut lets review together what must amount, if you are right, to an incredible\n and steady flow of patches:\n\n> Adobe\nmade the Apollo fork. Never contributed back anything to Apple. Some developers of the GTK port recently backported a few GTK-painting patches.\nNet result: void.\n\n> Nokia \nthe oldest fork and completely outdated. Only ever contributed a single patch\n in two years, which I still haven't decided if it is worth merging.\n\n> Google\nforked as part of the Android package. Did not contribute back.\n\n> the KHTML old timers\nGeorges Staikos contributed a trivial rendering patch, which is wrong and breaks alignment within the fieldset tag.\nLars Knoll didn't contribute anything of general interest to the core.\nZack Rusin didn't contribute to the core.\n\nAll this to the best of my knowledge, I'd be happy if you prove me wrong\nof course.\n\n> As for youtube, did you try to scroll down the related videos box?\n\nI see you didn't test KHTML from KDE 4 which hasn't any problem of this sort.\n\n> http://www.carto.net/papers/svg/dock/index.svg\n\ngreat, an academic site demonstrating dynamic SVG. That's useful ;(\n\nSee, in the ocean of possible features, implementing everything and the kitchen sink is not necessarily a good plan. It leads to bloat, code duplication, and heavy maintenance burden.\n\nAs of now, we have outstanding Canvas support, which is lightweight, and well suited to the kind of task dynamic SVG is supposed to address on the web.\n\nI would happily use a Corba vs. DCOP metaphor here but I'm not completely sure it would still be understood.\n\nAnyway, dynamic SVG as implemented in the Apple tree is a very significant bloat and a great set back for componentization. The rendering engine for HTML+SVG is a monolithic design. We will really have to give a good, long thought to this, supposing dynamic SVG becomes really useful someday.\n\n> http://webkit.org/demos/editingToolbar/\n\nmmh, well the source says it's a tech demo made for FireFox and WebKit only, so of course it's going to work in FireFox and WebKit.\nI could also point you to some early Audio object demo and boast that it works only in Opera9 and KHTML4... what's your point?\n\n> KHTML still doesn't support design mode\n\nYes! That is a fair statement !\n\nIt is unfortunate that Leo Savernik, after he put an enormous amount of effort in this, was eventually discouraged to polish his work by the scaring tactics of WebKit supporters.\nBut, good news ! His code still lives, we are working on it, and the target\nfor the feature is set to KDE 4.1, which seems just about right given the specification for design mode is still in its early infancy and has not even passed the \"editor draft\" state.\n\nMerry Christmas to you too.\n"
    author: "Spart"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "Google did make an improvement for WebKit in Android and that involved displaying text before images and making it faster for mobile, I don't have the link of the blog post here but they said they submitted it to the webkit team.\n\n> http://www.carto.net/papers/svg/dock/index.svg\n> http://webkit.org/demos/editingToolbar/\n\nThose work for Firefox, Safari and Opera out of the box so KHTML is the only one that doesn't support them.\nBy the way firefox3 also supports audio and video.\n\nAnd you do realize that not having design mode make khtml doesn't work with a huge bunch of websites. You know how annoying it is to hear \"hey why can't I add smileys to email in yahoo?\" or \"blogger doesn't work!\" or \"where's the chat in gmail and gmail??\" etc.\n\nYoutube still doesn't work here in KDE4.0 RC2.\n\nTalking about DCOP, if it wasn't for Trolltech, we would still be using it instead of DBUS and I remember many people were bitching about how DCOP rulz and DBUS sucked even though everybody was using it. Now we have much more compatibility and we can share stuff such as tapioca or packagekit, that sounds like webkit by the way :)\n\nEverytime someone comes along and say 'apple does this feature which KHTML doesn't', some KHTML person comes along and say \"but the way it is implemented is such a mess, we KHTML devs are working on such a great implementation it's gonna kick ass\", well fair enough, that's what they said about design mode 3 years ago.\n\nBy the way, why do christians always assume the world celebrates christmas? Half of the world don't, heck half of the world doesn't even know what it is. Do jews say \"happy hanouka\" to everybody and muslims \"col sana ua inta b'kher?\"."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "> Talking about DCOP, if it wasn't for Trolltech, we would still be using it instead of DBUS\n\nHigly unlikely. We would be using community maintained bindings like everybody else but we would definitely be using D-Bus.\n\nIf not for the outlook of sharing services on the session bus, at least for using the services on the system bus, since we already had been doing this.\nSession services has just been the next logical step."
    author: "Kevin Krammer"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "> Who cares about that benchmark\n\nApparently Apple cared enough about it to make it prominently appear in a \"What's new in WebKit 3\" article.\n\n> Anyway, nobody cares about javascript benchmarks.\n\nThe nontroppo.org benchmark is not a JavaScript benchmark like Apple's sunspider. It is an independant DHTML benchmark, testing the full JavaScript + DOM + Rendering stack.\n\n> their favorite browser on their favorite website such as gmail, google doc,\n> youtube, facebook you name it.\n\nthis looks like pointless trolling.\nIs your browser named Internet Explorer, FireFox, Safari or Opera?\n\nThen I'm afraid it is not actively supported on any of those sites, and will not fare any better than any other unsupported browser.\n\nThings will break and stating otherwise is either delusion or deception.\n\nThere is no doubt you can try hard to make things kinda work nevertheless, and fix breakages as fast as you can. But then you really ought to master you engine from top to bottom as we do in KHTML.\n"
    author: "Spart"
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: ">> their favorite browser on their favorite website such as gmail, google doc,\n>> youtube, facebook you name it.\n\n> this looks like pointless trolling.\n> Is your browser named Internet Explorer, FireFox, Safari or Opera?\n\nOk, I don't get it. In what way is that trolling? What's wrong with that:\n\n\"What users care about is being able to use their favorite browser on their favorite website\" \n\nisn't that true? For the matter I use firefox for most things and konqueror some times but less and less unfortunately.\nYes it is true that if I don't use Internet Explorer, FireFox, Safari or Opera, then my browser is not supported and this unfortunate situation is not going to change, this is why we need to switch to webkit. You are delusional if you think web designers are going to care about KHTML."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "With this reasoning, you should use Gnome, which has quite a lot more corporate backing than KDE, and Firefox."
    author: "Richard Van Den Boom"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "Hey now, no reason to bring a fellow OSS project in there. Clearly, if corporate backing is the most important thing, I would suggest using Microsoft Windows.\n"
    author: "SadEagle"
  - subject: "Re: WebKit"
    date: 2007-12-24
    body: "no, the most important thing is having the best free software solution, in this case webkit."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: "> no, the most important thing is having the best free software solution, in this case webkit.\n\nThere are two kind of fools, the one saying: \"This is old, and therefore good\",  \nthe other saying: \"This is new, and therefore better.\""
    author: "stefan"
  - subject: "Re: WebKit"
    date: 2007-12-25
    body: "> There are two kind of fools, the one saying: \"This is old, and therefore good\",\nthe other saying: \"This is new, and therefore better.\"\n\nThere is another kind of fool, the one that think there is only two kinds of fools :p\n\nSeriously, I couldn't care less about what's new or old. I love KHTML and to me, WebKit is just KHTML supported by tons of website with design mode and support for many more websites *on time*. Also the webkit widget TT is working on is really amazing stuff."
    author: "Paul"
  - subject: "Re: WebKit"
    date: 2007-12-26
    body: "I didn't mean to say anything wrong about Gnome (and actually I don't think my post seemed to imply anything by it), just that if corporate backing was an essential part of choosing open source project over another, Gnome would be the logical choice over KDE.\nSo when you decide to use KDE anyway, you should not worry about corporate backing and appreciate what the community brings to you."
    author: "Richard Van Den Boom"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "First webkitpart (part of playground-libs package) is an optional install and second has a lower preference than khtmlpart so it's not \"default\"."
    author: "binner"
  - subject: "Re: WebKit"
    date: 2007-12-21
    body: "Hmm, this also seems to happen in the Kubuntu RC2 LiveCD, but not in the KDE4 packages that Kubuntu also provides."
    author: "Jonathan Thomas"
  - subject: "OpenGL and QT4 Widgets"
    date: 2007-12-21
    body: "Just a question ...\n\nCompletely out of topic but I will ask it anyway...\n\nToday I read and article at\n\nhttp://macslow.thepimp.net/?p=150\n\nwhich shows the possibility to use OpenGL to actually enrich the user interface using GTK+.\n\nIt seems that GTK+ people are a little reluctant about these changes:\n\nhttp://arstechnica.com/news.ars/post/20071028-making-linux-application-user-interfaces-richer-with-opengl.html\n\n\n\nIs it possible to make such things with QT Toolkit (and , of course, make these cool things work on KDE)?\n\nI know plasma applets are OpenGL and ARGB compatible (plasma rocks). But I don't know if we could do it in the Qt4 widgets themselves (tabs, buttons, etc).\n\nAn example:\n\nMake the Dolphin window translucent and its control widgets (buttons, comboboxes etc) animated by OpenGL.\n\nSorry for my English, still learning..."
    author: "SVG Crazy"
  - subject: "Re: OpenGL and QT4 Widgets"
    date: 2007-12-21
    body: "> Is it possible to make such things with QT Toolkit\n\nwe've able to mix argb visuals, gl, etc within apps for quite a while in Qt. plasma wouldn't work very well otherwise =)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: OpenGL and QT4 Widgets"
    date: 2007-12-21
    body: "You might be interested in this comment that I wrote in response to another Ars article about Mueller's GTK+ OpenGL work the other day, with regard to how Qt stacks up in comparison (short teaser: very well): http://episteme.arstechnica.com/eve/forums/a/tpc/f/174096756/m/620009729831?r=542003829831#542003829831\n\n"
    author: "Eike Hein"
  - subject: "Re: OpenGL and QT4 Widgets"
    date: 2007-12-21
    body: "WOW, it seems that there will be a really interesting and exciting future for KDE! \n\nThanks for the answer..."
    author: "SVG Crazy"
  - subject: "KFourInLine??"
    date: 2007-12-21
    body: "Why not call it Konnect4?"
    author: "Apple Pie"
  - subject: "Re: KFourInLine??"
    date: 2007-12-21
    body: "I believe it's to avoid trademark troubles."
    author: "Luca Beltrame"
  - subject: "Gdesklets"
    date: 2007-12-22
    body: "What is the diff between\ngdesklets\nand \nPlasma?"
    author: "gerd"
  - subject: "Re: Gdesklets"
    date: 2007-12-22
    body: "In a simple and fast way to say...\n\ngdesklets are only desktop widgets.\n\nPlasma is the whole desktop, with panels, applets, datasources and more."
    author: "SVG Crazy"
  - subject: "Re: Gdesklets"
    date: 2007-12-23
    body: "The comparative is ridiculous, Plasma is a lot more than GDesklets/SuperKaramba/Screenlets/Kdesktop/Kicker/Dashboard (this is one is by itself a lot better than the others too)/etc "
    author: "Luis"
---
In <a href="http://commit-digest.org/issues/2007-12-16/">this week's KDE Commit-Digest</a>: A Sonnet-based spellcheck runner, and icons on the desktop in <a href="http://plasma.kde.org/">Plasma</a>. Continued work revamping KBugBuster, more work towards <a href="http://www.kdevelop.org/">KDevelop</a> 4. GetHotNewStuff support for downloading maps in <a href="http://edu.kde.org/marble/">Marble</a>. Image and audio dockers in <a href="http://edu.kde.org/parley/">Parley</a>. The start of Glimpse, a new scanning application based on libksane. The beginnings of a generic resource display framework for <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>. Various work in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. Music Service configuration work, and the integration of last.fm code in <a href="http://amarok.kde.org/">Amarok</a> 2.0. Printing work in <a href="http://www.koffice.org/">KOffice</a>. A Sybase database driver for <a href="http://www.kexi-project.org/">Kexi</a>, panorama work in <a href="http://www.koffice.org/krita/">Krita</a>, and ODF work in <a href="http://koffice.kde.org/kchart/">KChart</a>. <a href="http://www.caffeinated.me.uk/kompare/">Kompare</a> becomes usable for KDE 4.0, and gets a new maintainer. The confusingly-named game KWin4 is renamed KFourInLine. <a href="http://trolltech.com/">Trolltech</a>-supported <a href="http://phonon.kde.org/">Phonon</a> backends for all major platforms (Quicktime 7, DirectShow 9 and GStreamer) are imported to KDE SVN. <a href="http://commit-digest.org/issues/2007-12-16/">Read the rest of the Digest here</a>.

<!--break-->
