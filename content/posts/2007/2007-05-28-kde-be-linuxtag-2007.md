---
title: "KDE to be at Linuxtag 2007"
date:    2007-05-28
authors:
  - "jhoh"
slug:    kde-be-linuxtag-2007
---
This year Germany's <a href="http://www.linuxtag.org/2007/">LinuxTag</a> conference and exhibition takes place in in Berlin's Messe for the first time. As with previous years there will be a KDE booth, where you can meet some of the people behind KDE. Meet them and talk about the project, programs, features and, of course, about the upcoming KDE 4 platform.  On Thursday, <a
href="http://www.linuxtag.org/2007/de/conf/events/vp-donnerstag/vortragsdetails.html?talkid=58">KDE 4 will be the topic of a talk</a> in the "Linux, Wow" track.








<!--break-->
