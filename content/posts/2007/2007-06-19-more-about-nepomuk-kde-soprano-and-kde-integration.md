---
title: "More About Nepomuk-KDE: Soprano and KDE Integration"
date:    2007-06-19
authors:
  - "liquidat"
slug:    more-about-nepomuk-kde-soprano-and-kde-integration
comments:
  - subject: "current status"
    date: 2007-06-19
    body: "I wonder about the current status of integration of Nepomuk in KDE... Are there already apps in Trunk using it? Are there plans?"
    author: "superstoned"
  - subject: "Re: current status"
    date: 2007-06-19
    body: "And what will we see in Beta 1?"
    author: "superstoned"
  - subject: "Re: current status"
    date: 2007-06-19
    body: "I know that Cyrille Berger is really busy working on the metadata framework for Krita and that he is quite aware of Nepomuk: http://wiki.koffice.org/index.php?title=Krita/Metadata#Metadata_and_Nepomuk. We might not be able to completely support everything now, but it's on the agenda."
    author: "Boudewijn Rempt"
  - subject: "Re: current status"
    date: 2007-06-19
    body: "I think so.\nDolphin in svn displays a message on startup saying something like the Nepomuk-Service was not running. "
    author: "Tom Vollerthun"
  - subject: "Re: current status"
    date: 2007-06-19
    body: "The current SVN version of Nepomuk and Dolphin are already working together: start the Nepomuk daemon and afterwards, start Dolphin, and you are able to tag, rate and comment all files from within Dolphin."
    author: "liquidat"
  - subject: "Re: current status"
    date: 2007-06-19
    body: "You don't even have to do it manually on my system - gets started automagically assuming everything you needed was detected at build time and such..."
    author: "Troy Unrau"
  - subject: "Re: current status"
    date: 2007-06-20
    body: "Well, I guess nepomuk then just can't start or compile or something... I'll have a look at it. After all, Nepomuk should be mentioned in the KDE 4 beta 1 article which I'm working on ;-)"
    author: "jospoortvliet"
  - subject: "Video of Nepomuk and Dolphin in action"
    date: 2007-06-20
    body: "http://www.dailymotion.com/related/3900540/video/x2bf36_nepomukdolphin/1"
    author: "Carsten Niehaus"
---
In a follow up story to the "State and Plans of Nepomuk-KDE" post, <a href="http://liquidat.wordpress.com/2007/06/19/more-about-nepomuk-kde-soprano-and-kde-integration/">the second post</a> covers Soprano and the overall integration of <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a> within KDE.




<!--break-->
