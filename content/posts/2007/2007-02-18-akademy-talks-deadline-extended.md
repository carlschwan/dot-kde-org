---
title: "aKademy Talks Deadline Extended"
date:    2007-02-18
authors:
  - "jriddell"
slug:    akademy-talks-deadline-extended
---
Due to a beastie in the submissions system, the aKademy 2007 Programme Committee has extended the deadline for talk proposals until February 23rd.  See the <a href="http://akademy2007.kde.org/conference/cfp.php">Call for Participation</a> for some guidelines and how to submit.  Confirmation to those who have already submitted has been sent out, let us know if you have no heard from us.  If you contribute to KDE in any way it is likely others will want to know about it, so send us your abstract before next Friday.
<!--break-->
