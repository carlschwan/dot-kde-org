---
title: "Konsole Usage Survey"
date:    2007-03-20
authors:
  - "liquidat"
slug:    konsole-usage-survey
comments:
  - subject: "Thanks Robert!"
    date: 2007-03-19
    body: "Thank you Robert for working on improving Konsole even more! I first heard of the survey while reading PlanetKDE and have already completed it :)\n\nVlad"
    author: "Vlad"
  - subject: "Re: Thanks Robert!"
    date: 2007-03-19
    body: "That was a boring survey but I'm sure it'll be worth it :D."
    author: "Kingsley"
  - subject: "Re: Thanks Robert!"
    date: 2007-03-19
    body: "> That was a boring survey but I'm sure it'll be worth it :D.\n\nDid I mention that everyone who completes it gets a FREE copy of KDE 4? ;)\n\nBTW.  Before anyone posts about the mistake in Question 8, I know.  Unfortunately the software used does not have provisions for making changes after publishing the survey."
    author: "Robert Knight"
  - subject: "Re: Thanks Robert!"
    date: 2007-03-20
    body: "> Did I mention that everyone who completes it gets a FREE copy of KDE 4? ;)\nAlright, I did, where is my copy? Come on, give me the damn link!\n\n\nlg\nErik"
    author: "Erik"
  - subject: "oops"
    date: 2007-03-19
    body: "I didn't mean to quote the guy above me."
    author: "Kingsley"
  - subject: "Impressive!"
    date: 2007-03-19
    body: "AFAIC, Konsole is already the best terminal app I've ever used, so congratulations on this one.\n\nIt is for me KDE's killer app (even more so than Konqueror & all the others).\n\nI find it very nice though to have this survey to improve it even more, even though I don't understand how you can make a better-than-perfect app! ;-)\nThanks for your efforts on this!\n\nWackou."
    author: "Wackou"
  - subject: "Re: Impressive!"
    date: 2007-03-19
    body: "we are in 2007, and still somme geeky people think that a console is a perfect tool for the desktop.\n\n"
    author: "djouallah mimoune"
  - subject: "Re: Impressive!"
    date: 2007-03-19
    body: "sheez - relax. he did not say so..."
    author: "anonymous"
  - subject: "Re: Impressive!"
    date: 2007-03-19
    body: "We are in 2007 and I still use, aside from firefox and gaim, almost only konsole, or term based applications to be more specific. Do you have any problem with that?\n\nI don't feel geek, but I do feel that I'm using the right tool for what I need. If it's not your case... man who cares! This is about Konsole, not about \"ohh god! still using vim instead of gvim or kate or blablabla\"\n\nCheers,\n"
    author: "Rodrigo"
  - subject: "Re: Impressive!"
    date: 2007-03-19
    body: "Well, the truth is that if you don't like it, please use a gui instead. I use the  shell primarily for all my tasks. Trust me, the power of the shell has to be learnt and experienced to understand.\n\nAll said, konsole is the best shell emulator - the others don't even come close to the tabs, tab shortcut, and bookmark features. I've seen a lot of people flame kde in general, but they all seem to agree that konsole doesn't have a competition :)"
    author: "Sarath"
  - subject: "Re: Impressive!"
    date: 2007-03-19
    body: "well, to be honest, I sometimes fire up kde simply because doing console work in konsole is so much more pleasant then using F1-F7\n\nI would morn the day kde removed konsole from it's list of apps."
    author: "Mark Hannessen"
  - subject: "Re: Impressive!"
    date: 2007-03-19
    body: "If you read carefully my post, I didn't say that, however sometimes you *need* a console. I use if mostly for work, but even on the desktop it can prove a useful shortcut in certain occasions.\n\nJust from the top of my head, I had to do this today:\nfrom a base directory, find all mp3 files, and write a text file containing the name of the file without the extension, colon, the full path to the file.\ni.e.:\nsong1: artist/album/song1.mp3\nsong2: another_artist/different_album/song2.mp3\n\n(the actual task was slightly more complicated, but I simplified it for clarity)\n\nNow, the day some GUI application allows me to do this, in a simple, clean and intuitive way, maybe I'll stop using a terminal. Until then...\n\nRegards,\n\nWackou.\n\nPS: remember that some people work with their computer, not only use it as a media center..."
    author: "Wackou"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "Well, if we were able to use directory trees as spreadsheet, I wouldn't mind.\n\nYou would be doing \"basename\" functions there on cells instead of `basename ...` calls, etc.\n\nMay be even more powerful, if you ask me.\n\nBut until then, I agree, the console allows automation with tremendous flexibility that is unmatched. \n\nMy digital photos e.g. are mailed to the studio that prints them with KMail and need to be attached. I can't do all in one mail. So it takes me only a little script to feed 10 images per mail automatically into Kmail, with To: and Subject: correctly filled. Would I do this manually, it would hurt me, with like 300 pictures. (Uploading them per HTML form in terms of one by one on a 10 element form would hurt even more, which is their alternative. I could do that with wput, but the email way has the nice advantage of being async and retry, and everything in the background with state of completion, and their email confirmation too).\n\nHow on earth would I otherwise e.g. find all \".tar.gz\" and repack them into \".tar.bz2\" in the background for space savings, without any manual intervention.\n\nFor mass renaming operations I used to create scripts, but I now prefer krename for that, as it's more flexible with greater ease (although not more flexible absolute). So that's no longer a point for console. I actually start krename from konsole often though as it's e.g. easier to find files that are not yet renamed with find.\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "Wackou \n\nI use computers for my daily work,  no problem at this point, yesterday I was reading an IT blog and it seems adobe has released a cross platform  runtime environment to deploy a new generation of application like ajax but they still work even offline, it is crazy man, just click a link and it is up and running.\nOk after that I come to the dot, and guess what, people are excited about how Konsole is so cool and sexy. I used console especially for linux, sometimes I have no choice or perhaps I don\u0092t know other ways to do it in a GUI mode. \nSo my post was rather subjective. And sorry if I hurt your beloved console.\n\nFriendly  \n"
    author: "djouallah mimoune"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "Hi,\n\nNo offense meant (nor taken), don't worry :-)\n\nThe thing is, as someone mentioned it before, this article is about Konsole, so yeah, you'll fine people praising a console application here, not that brand new Apollo thingy... I guess people weren't talking about Konsole in the Apollo article, either. It's just the context.\n\nSo yeah, among others posts, there's mine saying that for a console app, Konsole is pretty sexy. Of course, at the sexy level, it doesn't even compare to Amarok or other GUI apps, but hell, people never talk about it (Amarok, K3b and Konqueror steal all the fame), so I was quite thrilled that for once, someone was putting Konsole in the spotlight. Which may explain my over-enthusiasm!!\n\nRegards,\n\nWackou."
    author: "Wackou"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: " 'I guess people weren't talking about Konsole in the Apollo article, either. It's just the context'\nNo they were speaking about the next generation desktop application paradigm, not about eh 40 years old (perhaps more) user interface  \nFriendly \n\n"
    author: "djouallah mimoune"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "And one article the 'next generation desktop application paradigm' (yeah just like XUL was the next generation last generation) was on topic, and on the other the '40 year old user interface' was on topic.\n\nTalking about Apollo here is pointless (unless you're references it as somehow that Konsole can be improved, which hasn't happened)."
    author: "Sutoka"
  - subject: "Re: Impressive!"
    date: 2007-03-19
    body: "we're in 2007 and some people still don't understand that advanced users are users too.\n\nYou can go ahead and make the desktop as easy to learn, and as easy to use (two completely different, and often conflicting goals) as you like - as long as you don't cripple it for those of us who actually know what we're doing.\n\nEven Apple ship a command line these days."
    author: "mabinogi"
  - subject: "Re: Impressive!"
    date: 2007-03-19
    body: "Perfectly agreed.\n\nI use Amarok for listening to my music (and rediscovering it!), K3b to burn CDs, and more generally any GUI app that lets me do the stuff I want the way I want. However, sometimes I _need_ to to console-based stuff, and I am glad to have an equally good application for this, not just some old terminal that sucks...\n\nPeople do not wonder why we still write programs in a text editor (or IDE), using (oh my god!) a keyboard, rather than connecting blocks with a mouse.\n\nI think it all boils down to the same problem: GUI apps are tailored to a specific task, and allow you (arguably) to perform it in an easier and more intuitive way. However, sometimes you need the flexibility to describe exactly what you want to do, and more often than not, it is much faster to do this by writing it by hand, rather than clicking in a list of options pre-selected for you...\n\nAnd to expand on the parent's comment, even Microsoft realized they needed a better shell in Vista.\n\nWackou."
    author: "Wackou"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "Well, I'm a pretty non-advanced guy who prefers using GUI for nearly anything. And yet I couldn't miss a console, to do certain things.\n\nKonsole is the best for me, although I usually use xterm if I'm in a hurry. Starts fast, no-frills, and it works. And nearly all distro's have it."
    author: "Darkelve"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "We have very powerful and precise machine tools today, but sometimes a hammer and a saw, ancient devices to be sure, are still quite useful.  Use the right tool for the job and the one that fits your thinking patterns.  For many of us, the terminal is appropriate for many jobs.  For others it is not.  Use what's best for you and leave everyone else alone."
    author: "me"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "We are in 2007 and there are system administrators who HAVE TO do console work with remote servers that have need to use a graphical desktop as well.\nSo, what's your point?"
    author: "Davide Ferrari"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "Fine, I must admit my post was inappropriate for the topic of this dot, I just hate terminal, it remind me of all those dumb terminal we used in university learning FORTRAN ( it was an altrix DEC system) \n\nCheers     \n"
    author: "djouallah mimoune"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: "I don't know about you, but I type _much_ faster than I can use a mouse (i.e. my laptop's touchpad).  Also, many tasks are far better suited for a terminal (e.g. system administration, running Vim/Emacs, remote system administration via SSH or telnet (!), tasks involving piping output to input like grep/awk/sed/perl/find, movie encoding (!), and many others).\n\nAnd as many others have already pointed out, we power users are still users, and my guess is that the majority of KDE users are power users (the newbies all seem to flock to GNOME; I blame Ubuntu ;p), so I would hope that they work on programs for us! :)"
    author: "Matt"
  - subject: "Re: Impressive!"
    date: 2007-03-20
    body: " in my case; I use only linux in my home \u0093mepis\u0094, but in work we use manage very complex projects using planning software and stuff like dynamically linked spreadsheet to database servers, and guess what my colleagues don\u0092t even know what is the meaning of this strange thing \u0096 console- \nOk i I don\u0092t know what\u0092s your definition of those users, I suppose as they don\u0092t use console they are just newbie,\n\nMy point is stay humble, and the day when we don\u0092t need to open all those weird programs under console just to do a rudimentary operation, then Linux will be a serious choice for desktop users. \n\nFriendly \n"
    author: "djouallah mimoune"
  - subject: "konsole?"
    date: 2007-03-19
    body: "So where is this Konsole anyway?  I can't find the icon anywhere on my desktop toolbar. :("
    author: "KDE Troll"
  - subject: "Re: konsole?"
    date: 2007-03-20
    body: "Well, your KDE is too new then. It used to be there."
    author: "Debian Troll"
  - subject: "Re: konsole?"
    date: 2007-03-20
    body: "Do you at least have an apt-unget in Debian to revert to the previous version of KDE?  In Gentoo all I have to do is download the source code for the previous version, recompile it and install it...  all in portage."
    author: "Gentoo Troll"
  - subject: "Re: konsole?"
    date: 2007-03-21
    body: "\"In Gentoo all I have to do is download the source code for the previous version, recompile it and install it\"\n\nYou consider this as an advantage?"
    author: "Troll Master"
  - subject: "Re: konsole?"
    date: 2007-03-22
    body: "Flame war alert!"
    author: "zonk"
  - subject: "Re: konsole?"
    date: 2007-04-02
    body: "apt-get does it. apt-get install package=versionnumber\n\nKyle"
    author: "Kyle Gordon"
  - subject: "Re: konsole?"
    date: 2007-03-20
    body: "It should be somewhere in the menu. "
    author: "Ben"
  - subject: "Re: konsole?"
    date: 2007-03-20
    body: "Man, can't you realize that your desktop is a Windows XP.???\n\nYou are at the wrong place."
    author: "Pa Troll"
  - subject: "the best feature is :"
    date: 2007-03-19
    body: "taaadaaaa : type in multiple tabs simultaniously, perfect for updating multiple machines at once :-)"
    author: "lukilak@we"
  - subject: "Re: the best feature is :"
    date: 2007-03-20
    body: "Wow - thanks for that - one I did not know - along with a few others that the Survey pointed out.... \nKonsole Just keeps getting better"
    author: "Andrew Lowe"
  - subject: "Re: the best feature is :"
    date: 2007-03-20
    body: "Oh, wow.  That's pretty cool."
    author: "Michael"
  - subject: "Re: the best feature is :"
    date: 2007-03-20
    body: "oww, I have a nice feature!!\n\n\"konsole lists\"\ndescription: allows you to insert a list horizontally over all tabs.\n\nstep one: create a list with a name:\nname: domainlist\n\nwith content like:\n1: ssh kde.org\n2: ssh bugs.kde.org\n3: ssh blah.org\n4: ssh etc.org\n\nnow when you right click on a tab there would be an option \"insert list\" which inserts content 1 in the tab you are currently in, content 2 in the next tab, etc.\n\nother usefull features would then be:\n\"auto spawn tabs\" when needed, which spawns tabs if there are more list items then tabs. (so you don't have to open 15 consoles before setting the insert in all tabs option)\n\"switch to insert in all tabs on insert list\", allowing you to be extra lazy.\n"
    author: "Mark Hannessen"
  - subject: "Re: the best feature is :"
    date: 2007-03-22
    body: "Pretty good idea. You could set up a 'default environment'. But it would have to save your passwords too, I guess... might be hard to keep it secure."
    author: "superstoned"
  - subject: "Re: the best feature is :"
    date: 2007-03-20
    body: "Sweet.  How is it going to handle viewing all the sessions?  I use clusterssh for that sort of thing and it opens an xterm for each ssh connection and tiles all the windows.  With as many machines I admin, I require dual screen to use it, but I don't see any other way.  My point is, I need to see all the sessions I'm typing into.  Kick ass feature none the less!"
    author: "Carl"
  - subject: "Re: the best feature is :"
    date: 2007-03-22
    body: "Maybe the split-screen function in Konsole in KDE 4 will make it possible???"
    author: "superstoned"
  - subject: "Show Terminal Emulator"
    date: 2007-03-19
    body: "In the survey, I asked about \"Show Terminal Emulator\" in Konqueror. I'm worried that KDE 4 will lose this feature, which I use more often than Konsole. Kate has \"Show Terminal\" hidden in its menus, and I use this very often as well. I also suggested that \"Show Terminal Emulator\" be added as a button on all KDE windows. \n"
    author: "Paul Leopardi"
  - subject: "With fish:// too!"
    date: 2007-03-19
    body: "Agreed. This is a very convenient feature! I am dreaming of the same feature with fish:// in Konqueror. The terminal would launch ssh and would move to the right directory."
    author: "Hobbes"
  - subject: "Re: With fish:// too!"
    date: 2007-03-22
    body: "yes, supporting KIOslaves as good as possible would be great..."
    author: "superstoned"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-20
    body: "also a very dangerus feature, ever started to write something and then decided to change directory using the gui and it will run that command and change dir. If there is something with rm there you could if your unlucky wipeout your home directory or something"
    author: "arne"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-20
    body: "unlucky and stupid, that is...\nfor real yes, that is a design flaw... rather it should save the typed command in a buffer, remove from the terminal, cd into the new folder and reinsert from the buffer. anyone filed a report yet? X:=P"
    author: "eMPee"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-22
    body: "WONDERFULL idea..."
    author: "superstoned"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-20
    body: "> I'm worried that KDE 4 will lose this feature\n\nbased on what, exactly?"
    author: "Aaron J. Seigo"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-20
    body: "Sorry, just a feeling based on no real evidence. The KDE 4 stuff I have seen so far talks about Dolphin as being the new default file manager. See http://dot.kde.org/1172721427/\n\nThe survey is all about Konsole and does not mention \"Show Terminal Emulator\" in Konqueror. So I jumped to the conclusion that nobody doing KDE 4 cares about this feature any more."
    author: "Paul Leopardi"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-20
    body: "Hello Paul, please don't worry, it is planned to include \"Show Terminal Emulator\" also in Dolphin and for sure nobody aims to remove this functionality from Konqueror.\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-20
    body: "For one, it is removed from Kubuntu.\n\nI think having YaKuake built into KDE 4 would be a better solution. Then you could just have a tab that automatically syncs to the directory open in dolphin/konqueror/text editor/whatever.\n\nOTOH, I think this is the advantage of Dolphin. since this isnt an often used feature, it can be removed from dolphin but remain in konqueror for the people that want it(and if they use this, chances are, they will also want many of the other features in konqueror that arent found in dolphin)."
    author: "D"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-20
    body: "Good point. Dolphin can be easy and for newbies, as there is konqi for those advanced users..."
    author: "superstoned"
  - subject: "Re: Show Terminal Emulator"
    date: 2007-03-20
    body: "Well YaKuake is a nice app, but the built-in terminal in Konqui and Dolphin would automatically switch to the cuurent directory and YaKuake wouldn't. "
    author: "Sebastian"
  - subject: "I don't use Konsole"
    date: 2007-03-20
    body: "I much prefer Yakuake with it's docking ability. When I really need an independant console, I use urxvt because it's lightweight compared to Konsole. And at least, the fake transparency works with this one, and I can get a completely transparent window, no grey widgets everywhere. I've never managed to get the fake transparency working since KDE 3.4. When I choose a transparent theme, it gets completely black or white (I use Kubuntu)."
    author: "Cypher"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-20
    body: "yakuake is konsole. =) it just embeds it in a self-managed window that \"slides\" in and out of the \"side\" of the screen.\n\np.s. transparent konsole works just fine here on kubuntu =)"
    author: "Aaron J. Seigo"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-20
    body: "Yes, that is why you get the horrible configuration context menu."
    author: "benk"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-20
    body: "What about shipping Yakuake by default binded to some function keys? it would be a killer feature if you need to do remote help desk \"press F9 and type ifconfig, and tell me what it says\""
    author: "Vide"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-20
    body: "and the answer would be \"it says 'command not found' because i'm not root.\"\n\ni get your point, though.\n"
    author: "Mathias"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-21
    body: "Um, no.  It says command not found because the command is not found.  Put it in your path or use the full path name."
    author: "KDE Troll"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-27
    body: "In kubuntu /sbin/ifconfig is in the users path."
    author: "Greg"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-20
    body: "Fake transparency works when I'm in a KDE session but not when i use Fluxbox.\n\nP.S. Great work on Konsole and the survey."
    author: "some1"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-22
    body: "the fake transparency needs the background to know what to fake ;-)\n\nIt just doesn't know how to ask fluxbox for the background, I guess."
    author: "superstoned"
  - subject: "Re: I don't use Konsole"
    date: 2007-03-21
    body: "What I really would like to have is a docking Dolphin.  Then, no matter in which desktop you are working and no matter how many windows are open, you press F11 (or any other key) and a file management program pops up.  This would be much easier than first closing all windows and pressing that \"Home\" icon on the desktop or selecting \"Home\" in the System menu on the kicker.  Also it would avoid to have to search the file manager in your Alt-Tab list or on the taskbar when it loses the focus: whenever you want the file manager back in front, just press F11."
    author: "Vlad"
  - subject: "Konsole Rules!"
    date: 2007-03-20
    body: "one of the first apps that I always have open is Konsole - its simply powerful!!"
    author: "Shelton"
  - subject: "Clickable URLs?"
    date: 2007-03-20
    body: "I'm hoping that there is some config option for this that I've just missed, but I think parsing and making URL-like text clickable would be a great addition. Manually selecting the URL and using Klipper's popup to open is not too much hassle but is not exactly easy either... Hmm.. Wondering about word-based selection (rather than character-based selection) I just found that double clicking on a URL will select it. With my setting of Klipper to \"ignore selections\", I still need to manually copy it though..."
    author: "Jason Stubbs"
  - subject: "Re: Clickable URLs?"
    date: 2007-03-20
    body: "This features is asked for so much, I can hardly believe they won't implement it for KDE 4 ;-)"
    author: "superstoned"
  - subject: "Re: Clickable URLs?"
    date: 2007-03-20
    body: "Yeah, I remember frustration at submitting a feature request to bugs.kde.org for this feature, the responce was: 'use actions in klipper'\nI'm not a big fan of actions in klipper, I just want links I can click ;-)"
    author: "Mattz0r"
  - subject: "Konsole is really nice"
    date: 2007-03-20
    body: "But I prefer urxvt with screen.\n\nurxvt starts _really_ fast. And it does have real transparency too ;)\n"
    author: "anonymous"
  - subject: "Germinglish"
    date: 2007-03-20
    body: "When I fill out the survey, there is a language-mix (German, English). Is this testing my intelligence ;) ?"
    author: "Flow"
  - subject: "Re: Germinglish"
    date: 2007-03-20
    body: "Are you german, and is your browser set to german? \n\nMaybe it was translated automatically. "
    author: "Arne Babenhauserheide"
  - subject: "Re: Germinglish"
    date: 2007-03-20
    body: "Odd.  I wrote the survey entirely in English, perhaps the library I am using translates some of the text depending on the locale of the visiting browser.\n\nJust out of interest - what text shows up in German?"
    author: "Robert Knight"
  - subject: "Re: Germinglish"
    date: 2007-03-21
    body: "The word konsole ;)"
    author: "Allan Sandfeld"
  - subject: "Re: Germinglish"
    date: 2007-03-21
    body: "The Answer Categories Yes/No are Ja/Nein.\n\nAnd Yes - I am german, so probabaly the browser is identified as such...\n\nWhich Script are You using for this survey?"
    author: "Flow"
  - subject: "Konsole is the best!"
    date: 2007-03-20
    body: "I have to administer a lot of unix/linux systems daily. Konsole is very suitable for this task and simply the best console program what I've ever seen. Keep up the good work!"
    author: "toth"
  - subject: "Yakuake"
    date: 2007-03-20
    body: "I use yakuake and that is the killer app for me in KDE. I uses konsoles settings and I guess inherits Konsoles features. Love it! :D"
    author: "Thomas Jansson"
  - subject: "The way to go!"
    date: 2007-03-20
    body: "\nRealy professional way to do things! When I saw this survey, I' filled it immediately!\n\nRealy great application and great way to recieve feedback from people around the world.\n\nFar better initiative than doing \"koffeshop\" surveys! ;-)\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: The way to go!"
    date: 2007-03-20
    body: "That was really usability testing, not surveying ;)\nBoth have their use."
    author: "logixoul"
  - subject: "Re: The way to go!"
    date: 2007-03-20
    body: "I think it's hard to get a meaningful sample of commandline users in the average coffeeshop :-)\n\nBut yeah, konsole is certainly a killer application -- and it looks like it'll be improved even."
    author: "Sebastian K\u00fcgler"
  - subject: "Ouch!"
    date: 2007-03-20
    body: "Hi, I already answered the survey and concluded I didn't quite profit from all Konsole features. On a second thought, something can be done about it and thus I'm here to suggest it.\n\nWhy I don't use tabs? Because they don't call my attention! Make them stand out, make them easy to create -- maybe another toolbar with session icons would help, who knows? Maybe special mouse usage like clicking on an empty place on the tab-bar would lead to some action...\n\nIn other words, make Konsole more like Konqueror and/or other browsers, i.e., maybe (customizable) icons suck less than menu/submenu/subsubmenu options...\n\nAlso, you can further integrate Konsole and Konqueror. Instead of just Konqueror starting Konsole, there could be a session with a Konqueror-file manager embedded, e.g., for easier file selection/visualization... plus, if one could view two sessions at once it would be possible to drag visual objects to the command-line for further use (much like pasting from Windows Explorer to a DOS window...)\n\nAnyway, thanks (really), keep up the good work and I also wish you continued success!\n\nGr8teful\n\nPS: Just my personal opinions above, not someone else's.\n"
    author: "Gr8teful-1"
  - subject: "Re: Ouch!"
    date: 2007-03-22
    body: "Wow, yes, having konsole as a kpart in konqueror, love the idea! Would be very very usefull... And indeed, clicking the empty space in the tab-bar should create a new one."
    author: "superstoned"
  - subject: "Contextual help"
    date: 2007-03-20
    body: "A nice feature in Konsole would be to have the possibility to have a dynamic contextual help window next to the Konsole.\n\nWhen I type 'ls' in my Konsole, all the different options for 'ls' would appear magically in the contextual help window. Something like 'man' on steroids. Long time ago, there was something similar on Dos but more crude calledn if I remember right ni, one of the first Norton programs"
    author: "cmiramon"
  - subject: "Re: Contextual help"
    date: 2007-03-20
    body: "> When I type 'ls' in my Konsole, all the different options \n> for 'ls' would appear magically in the contextual help window\n\nThe fish ( friendly interactive ) shell provides much better contextual help than bash does.\n\neg.  In fish, type \"ls --\" then press the Tab key and a list of possible options is shown alongside brief explanations.  \n\nThe bash equivalent lists the options but without explanations."
    author: "Robert Knight"
  - subject: "Re: Contextual help"
    date: 2007-03-21
    body: "Thanks, I've installed fish and I'm playing with it.\n\nIt looks very nice and I'll give it a try. \n\nI'm still wondering if some ideas of fish could not be adapted for a graphical shell where we can have contextual windows. "
    author: "cmiramon"
  - subject: "My thoughts"
    date: 2007-03-20
    body: " Please enter any other comments you have about changing Konsole settings ( including creating new color schemes or sessions ).\n\nI would like only these setting to be changed:\n * default tabs position: up\n * don't hide the tabs bar if the only tab is shown\n * default colour scheme: Linux colors\n * default and _supplied_ with kdebase font: Terminus and Andale Mono\n\nPlease enter any other comments you have about Konsole which you would like the developers to read.\n\n* I'm missing much such Gnome console functionality: You can scroll back  the history and then copy paste some contents without falling back to  the active screen. Right now if you paste something from behind the screen then Konsole will force you back to the main screen."
    author: "Artem S. Tashkinov"
  - subject: "Real transparency"
    date: 2007-03-20
    body: "Please, work on real transparency support. Fake transparency is _slow_\n\nI'm currently using a patched Konsole with Beryl support that does real transparency and it is FAST :)"
    author: "lqtys"
  - subject: "Opportunity"
    date: 2007-03-20
    body: "Thanks for the opportunity of saying that I don't use the console at all, and I would be happy if it were removed from KDE.\n\nI have missed the question: \"how much do you hate typing commands?\"\n\nI hope you can include it the next time.\n\nMany thanks."
    author: "David"
  - subject: "Re: Opportunity"
    date: 2007-03-20
    body: "i can't agree more"
    author: "djouallah mimoune"
  - subject: "Re: Opportunity"
    date: 2007-03-26
    body: "Perhaps Linux is not for you.  Seriously.  \n\nNot to want to ahve to type commands is one thing (if pie in the sky) but to wish for Konsole to be removed from KDE so no one can?  I think you want Linux to be something it isn't, and you'll be a lot less frustrated when you realize it will never be, not entirely anyway."
    author: "MamiyaOtaru"
  - subject: "Re: Opportunity"
    date: 2007-03-20
    body: "Just type:\n  rm $(which konsole)\nin your konsole window. :-)"
    author: "Skoot"
  - subject: "but how do you . . ."
    date: 2007-03-21
    body: "run irssi and mutt?\n\n"
    author: "Velvet Elvis"
  - subject: "Re: Opportunity"
    date: 2007-03-21
    body: ">>I have missed the question: \"how much do you hate typing commands?\"\n\nThat should be placed in another survey :)\nThis one is about when you do need to type commands, if konsole does the job well enough for you..."
    author: "whatever noticed"
  - subject: "Re: Opportunity"
    date: 2007-03-22
    body: "Well, some people (like myself) hate having to go through all those menus, dialogs, buttons and options, if the work could be done with a simple command. That's why my kicker doesn't contain a K menu or any app buttons. It's faster and more convenient to simply Alt+F2 and type the command... I don't use konqueror, also. Shell is faster for file management."
    author: "slacker"
  - subject: "Re: Opportunity"
    date: 2007-03-23
    body: "Well said. Because everything is all about you, and if there is something you don't need, it should be removed."
    author: "Spank Me"
  - subject: "Of course konsole!"
    date: 2007-03-20
    body: "Konsole is by far my favourite application of KDE. :)\n\nI use it basically to do everything (until i have to start sth like \nopenoffice or gimp or similar, but i start everything from a konsole tab)\n\nPersonally I love konsole, but if things could be made possible:\n\n- I would like something like quadkonsole BUT in a konsole tab.\n\nI heard this would maybe be possible in KDE4 so I hope that it \nmight become true (I'd use one tab to have a htop on the left \npane and use the right pane to kill modify etc.. running processes)"
    author: "shevegen"
  - subject: "Win32 Konsole Running The Windows Power Shell"
    date: 2007-03-21
    body: "Anybody else dreaming of a native win32 konsole running the windows power shell as well as cygwin based shells?\nThis would make my life easier..."
    author: "Manuel"
  - subject: "Re: Win32 Konsole Running The Windows Power Shell"
    date: 2007-03-26
    body: "Would be nice."
    author: "MamiyaOtaru"
  - subject: "Thanks!"
    date: 2007-03-24
    body: "Thanks to all the people who have developed Konsole (and Kvt) over the time.\n\nMy desktop consists of a fullscreen Konsole window on the foreground most of the time, and in the background Amarok, Kmail, Konqueror and possibly other Konsole window on another virtual desktop wich I may be using for some different work.\n\nI've prefered Konsole over xterm since KDE 2 (tabs). Now it's really nice to have a konsole window on a virtual desktop where you run networked uml instances, set tab titles and rearrange tabs according to current needs with just some quick keystrokes. Konsole was a central part of my \"network lab\" on a previous job.\n\nAnd now I have konsole starting fullscreen on login to KDE on my home computer,  because I know it will be the window I will be mostly using :)\n\nAnd all that even when having the wonderful KDE minicli (ALT+F2) handy.\n\n\nSo: Thanks, thanks, thanks.\n\n\nP.S. You know you use Konsole too much when you press Alt+UpArrow to scroll up one line on the linux console every time you use it.\n"
    author: "nin"
---
Robert Knight, lead maintainer of <a href="http://konsole.kde.org/">Konsole</a> has launched a <a href="http://www.robertknight.me.uk/survey/public/survey.php?name=konsole_settings">Konsole Usage Survey</a>. 28 questions are waiting for your answers. Use this chance to give useful feedback about a vital and often-used base application of KDE to enable Robert to make Konsole the best console application for KDE 4.



<!--break-->
