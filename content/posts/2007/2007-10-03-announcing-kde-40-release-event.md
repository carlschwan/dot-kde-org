---
title: "Announcing the KDE 4.0 Release Event"
date:    2007-10-03
authors:
  - "Wade"
slug:    announcing-kde-40-release-event
comments:
  - subject: "Wonderful!"
    date: 2007-10-03
    body: "It's just amazing to see this become a reality.\nI wish everyone the best, and thumbs up for the release party crew! :-)"
    author: "Diederik van der Boor"
  - subject: "Re: Wonderful!"
    date: 2007-10-04
    body: "Amazing that it's being held in Google HQ.\n\nWhat is Googles interest in KDE? Surely it's not just some random altruism."
    author: "reihal"
  - subject: "Re: Wonderful!"
    date: 2007-10-04
    body: "Google's involvement does not bode well in my opinion.\n\nI agree with the above question; What is Google's interest in KDE. It's bad enough you can't find a browser that doesn't have Google built into the search bar.\n\nI don't like Google, I don't trust them, and when KDE, my preferred *nix environment starts partying with Google, I get worried."
    author: "Wreck"
  - subject: "Re: Wonderful!"
    date: 2007-10-04
    body: "Uhm...? Did you forget your tinfoil hat or something? Google is gogogogreat! (Yes, I've eaten pizza paid for by Google)."
    author: "martin"
  - subject: "Re: Wonderful!"
    date: 2007-10-04
    body: "No paranoia here...\n\nI'm just tired of all the Google hype. You've obviously drank the Google Kool-Aid and want others to drink it with you. I refuse. Google do no evil? Those days are long gone. I stopped using Google in 2004 and have never looked back.\n\nKDE should have picked another venue."
    author: "Wreck"
  - subject: "Re: Wonderful!"
    date: 2007-10-04
    body: "meh, why? Nothing wrong with Google. They're a big company, sure, but they haven't abused their power in any big way yet - unlike pretty much any other company even 1% of their size. They're just very supportive to FOSS, and they know KDE 4 is gonna be big for the Free Desktop - thus they want to support it."
    author: "jospoortvliet"
  - subject: "Re: Wonderful!"
    date: 2007-10-04
    body: "I won't even get started on this, but suffice it to say that a few large searcg companies have assisted the Chinese government with the great Chinese firewall. Google and Yahoo, among others, have assisted these tyrannical governments in blocking access to things from within those countries. Do no evil, huh? If that's is the cost of doing business in those countries, then the cost is too high. People should always come first. Always. Not profit. People. After all, a search engine is built for people to use. Breaking or blocking search to please a sitting government is evil and shows you can be bought not only monitarily, but politically. Search engines exist to deliver search results only, not be gamed, bought, SEO'd, you-name-it."
    author: "Wreck"
  - subject: "Re: Wonderful!"
    date: 2007-10-08
    body: "As far as i know google has nothing to do with the great firewall. They have self-censored their chinese version of google to avoid being filtered by the great firewall. Can you explain why you think complete withdrawal from china would be a better choise?"
    author: "falde"
  - subject: "Re: Wonderful!"
    date: 2008-01-10
    body: "Chris DiBona is great\nSummer of Code is great\nInvestment in nano-solar is great\n\nGoogle is a friend of Linux, they've also got our back in the patent wars vs MS.  It's a lot harder for MS to bully us now."
    author: "BobCFC"
  - subject: "Re: Wonderful!"
    date: 2007-10-04
    body: "\"I don't like Google, I don't trust them, and when KDE, my preferred *nix environment starts partying with Google, I get worried.\"\n\nWhy? What possible damage could they do? You obviously have concrete reasons for why Google getting involved with KDE could be damaging based on your \"no paranoia here\" comment; let's hear them.  Or are you just spreading vague FUD?"
    author: "Anon"
  - subject: "Re: Wonderful!"
    date: 2008-01-10
    body: "It's about the money.  Free software has some difficulties generating revenue unless they have a sponsor.  You know someone who feeds the coders, and sponsors events etc.  It's symbiosis, keeps free software going, and promotes it to the public, and Google may even get some software for free.  That's not really that bad, unless you want MS to completely take over the world, and Bill to be the next President. "
    author: "Bigpicture"
  - subject: "Travel costs"
    date: 2007-10-03
    body: "Hmm, why doesn't Google cover all the travel costs?\nThe probably could even deduct these costs from their taxes... ;)"
    author: "Evil Eddie"
  - subject: "Re: Travel costs"
    date: 2007-10-03
    body: "Right, and while we are at it: why doesn't Google just pay everyone to work on KDE? KDE e.V. is a charity after all...\n\n\"Give him an inch and he will take an ell\" is something that totally applies to the community when it comes to responses to news that involve companies giving away something, and it sheds a very bad light on all of us.\n\nThis is by far not the first time that I've seen such replies, it happens a lot. And even if it contains smileys, all it does is offending the people that have actually worked towards makeing this happending (thanks again Google, Wade & Co.).\n\nIt would be a lot cooler if people sat down just cheered that something is actually donated to their favorite project by Google and if that doesn't calm them, they could even start using their brains (most journalists and IT celebrities will get their expenses covered by their employees anyway, and sponsoring flights for everyone would significantly increase Googles expenses for little gain of everyone who will actually be there).\n\nCan we just agree to be happy about this?"
    author: "danimo"
  - subject: "Re: Travel costs"
    date: 2007-10-03
    body: "Hear hear!"
    author: "bsander"
  - subject: "Re: Travel costs"
    date: 2007-10-03
    body: "http://www.amazon.com/You-Give-Mouse-Cookie-Give/dp/0060245867\n\n:)"
    author: "Ian Monroe"
  - subject: "Re: Travel costs"
    date: 2007-10-06
    body: "awesome book! cant wait for kde 4.0 and i have no problems with google, wearing a tshirt that says google right now.\n---------------\nhttp://www.antiyes.com"
    author: "John"
  - subject: "i guess it had better get released by then!"
    date: 2007-10-03
    body: "i've learned to be _very_ skeptical of release dates, so even though KDE4 is planned for final release in early December, i assume this will get pushed back again at least some, either weeks or even months (this also helps prevent any disappointment i get when things do get pushed back). \n\nbut seeing this release party planned for jan 17 is hopeful! that means kde4 really _will_ need to be released by then!\n\nand thanks to google for hosting, I sure hope kde4 is as good as i think it will be.\n\n "
    author: "jetpeach"
  - subject: "Re: i guess it had better get released by then!"
    date: 2007-10-03
    body: "You did NOT notice that no year was selected yet? ;-)\n\nAnd Google does cool to associate its name with KDE4 I think, while KDE owes Google (in a weak sense) at least that for the GSoC stuff.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: i guess it had better get released by then!"
    date: 2007-10-04
    body: "I must say I also expect another small delay, hopefully not too long. But things are shaping up nicely, so I hope it'll work out for this year... Of course, the Release Event means a hard deadline :D"
    author: "jospoortvliet"
  - subject: "Logistics"
    date: 2007-10-03
    body: "Are there logistics information posted somewhere so we can make plans to go if we weren't sponsored?"
    author: "seele"
  - subject: "Re: Logistics"
    date: 2007-10-04
    body: "Soon, soon. It's a lot of work, organising all this stuff ;-)"
    author: "jospoortvliet"
  - subject: "Perhaps..."
    date: 2007-10-04
    body: "Awesome!\n\nCan we just fly to San Francisco ourselves, but get room somewhere?"
    author: "Eugeniu"
  - subject: "Re: Perhaps..."
    date: 2007-10-04
    body: "There are a fixed amount of rooms given by Google. If more people come (not even sure that's possible) they have to find a place to stay for themselves."
    author: "jospoortvliet"
  - subject: "Grrrr"
    date: 2007-10-04
    body: "Hopefully this doesn't mean that the release of KDE4 will be put off til next year.     Hopefully this does not mean that you have yet again failed to reach the release date you have given your loyal followers who if they are like me, are getting tired of release dates being put back.  IF so, perhaps, you are reading the microsoft book on how to not meet release dates, and not the opensuse book on how to meet release dates ;)  Hopefully it is just the release of KDE4 for windows that will be delayed.  But I guess I might give Gnome a try, getting a bit bored of KDE and was hoping for a release to take away the boredom.  Thank God for opensuse releasing 10.3 in a few hours, at least they keep to their release schedule. \n\nGrrrrrrrrrrrrrrr"
    author: "Richard"
  - subject: "Re: Grrrr"
    date: 2007-10-04
    body: "The release event was intentionally planned to be some time after KDE 4.0 was tagged. This was to ensure that distros and integrators have had some time to play with 4.0 before we go shouting about it from the rooftops (and to allow the schedule some flexibility).\n\nAnyway, Open source will release when it's ready, not before. If you can't wait, use the beta and help is find bugs.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Grrrr"
    date: 2007-10-06
    body: "You should demand a full refund."
    author: "Se\u00e1n"
  - subject: "In Re: Grrrr 'n google 'n such ..."
    date: 2007-11-15
    body: "I suggest that, in anticipation of any possible error(s)/omission(s) on the part of those making any good-faith and/or open-source effort to improve our worlds (meaning both the virtual and the real), we cut their pay.\n\nNow, it's gonna be very difficult to identify all those that have helped in making the KDE environment tick, so as to insure they receive less monies for those services provided at no personal expense to you 'n me. \n\nBut, let's start by docking the many listed here:\n\nhttp://www.kde.org/people/credits/\n\n\n:: a footnote, in regard to the google thread above ::\n\nSince way back when, they've ranked a few of my Websites as the #1 result, without ever being paid (and, I still prefer using their free Gmail system over any of the three options found upon my server ~'-)\n\n\nSeriously, folks ... congratulations have been earned by all involved, even if gratitude and financial reward(s) never finds each 'n every one of 'em, no matter where in this predominantly unappreciative world of today that they, too, must live in.\nBut, they, too, may now enjoy the many priceless benefits that giving of ourselves brings, despite that slightly bitter aftertaste we must ocassionally endure.\n\nWarmest Regards,\n\n\nd@niel"
    author: "d@niel"
  - subject: "Google & Konqueror"
    date: 2007-10-08
    body: "The reason Google are hosting this event is they plan on using Konqueror / KHTML engine the the Google Web Browser / Google Phone.\n\nThat's what I've been thinking for a while anyway ever since KDE devs announced that they and Google has some big news.\n\nIt could be a thank you for their code.\n\nMike"
    author: "Mike Lothian"
  - subject: "Re: Google & Konqueror"
    date: 2007-10-11
    body: "Hi Mike,\nDo you think this big news will also come out at the same release party?\nThe wide waiting world is anxious to finally know what Google is up to with it's GPhone or GSoftware for Phones...\nVincent\n"
    author: "Vincent"
  - subject: "Re: Google & Konqueror"
    date: 2007-10-11
    body: "From the OP:\n\n\"That's what I've been thinking for a while anyway ever since KDE devs announced that they and Google has some big news\"\n\nFrom what I remember, the enigmatic remark about the \"big [Google-related] news\" the OP is referring to was made before the announcement of the Google-hosted release party, so I wouldn't be at all surprised if the so-called \"big news\" actually *was* the fact that Google will be hosting the release party, and that there is no further \"big news\" to announce.\n\nAlthough it would be nice if Google started giving cash to KDE the way it gives it to Mozilla - 10's of millions of dollars of funding would make KDE even more awesome.  A man can dream :)\n"
    author: "Anon"
  - subject: "Re: Google & Konqueror"
    date: 2007-12-21
    body: "Superior though Konqueror may be, it cannot be the most important reason for Google to sponsor KDE4.  At the risk getting shot for pointing out the obvious:\n (1) Google is composed mainly of geeks.\n (2) Geeks prefer Linux.\n (3) *x users with any sense of aesthetics prefer KDE, or course.\n\nErgo Google is full of geeks who use and enjoy KDE and want to celebrate KDE4.  Simple logic, really.\n\n/me: returns to padded cell\n"
    author: "random bolshoi"
---
On January 17-19, the KDE community will present KDE 4.0 with a Release Event at the Google headquarters in Mountain View, California. The purpose of this event is to celebrate the anticipated release of KDE's new desktop environment and development platform. In addition to the KDE community, representatives from businesses, press and other Free Software groups will attend. We hope this event will help spread the word about KDE's new release and how it impacts the future of the Free Desktop. Read further for more information about this event.






<!--break-->
<p>Because this event aims both to be informative and productive, we have created an ambitious schedule. There will be plenty of time and opportunity for interaction between all attendees; but there will also be many interesting presentations, involving both technical and non-technical topics. We have invited over 200 members of the media, I.T. business, distributions and other Free Software groups, completed of course with many members from the North-American KDE community. Further, several international KDE hackers will attend to give talks or help organise the event. The major keynotes and talks will be available live over the Internet thanks to support from Fluendo.</p>

<p>Due to financial and space constraints we were unable to invite all KDE developers, so we encourage local and regional KDE 4.0 release parties all over the world. We have tried to ensure the main Keynote by Aaron Seigo, which will of course have a live stream, is scheduled at such a time the international community meetings can see and listen to it.</p>

<p>We hope this event will both strengthen the KDE community in the United States and increase awareness about KDE and what we have to offer.  The KDE community should be proud of both the upcoming KDE 4.0 release and the foundation prepared for the life of the KDE 4 series; we hope that this release event will introduce and educate those not familiar with KDE, and strengthen relationships with those that are.</p>



