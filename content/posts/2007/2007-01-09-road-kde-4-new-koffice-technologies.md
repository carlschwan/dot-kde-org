---
title: "The Road to KDE 4: New KOffice Technologies"
date:    2007-01-09
authors:
  - "tunrau"
slug:    road-kde-4-new-koffice-technologies
comments:
  - subject: "Looking forward to it..."
    date: 2007-01-08
    body: "I'm really looking forward to the next KDE and associated programs. And I'm really glad to see these articles about the progress.\n\nWith KOffice my biggest concern is ODF compatiblility not so much the UI, just read an articlen in Linux Magazine about it and the compliance in the different tested office suites were somewhat depressing (they did the test on an older version 1.5, so most have improved since).\nCompatiblility between OOo and MS Words .doc, seemed better then between OOo and other ODF-enabled software, which is sad since ODF is an actual standard and .doc  isn't."
    author: "m_abs"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-08
    body: "Linux Magazine was, well, less than thorough and the author didn't quite understand the issues involved. I was a bit unimpressed with the articles. Thomas has got my copy now, so I cannot look back and give definite criticisms.\n\nStill, it's clear that we're all working towards ODF support, haven't reached it fully yet. Not OpenOffice, not KOffice, nor anybody else. That said, it's encouraging to read things like:http://www.openmalaysiablog.com/2007/01/bidirectional_m.html."
    author: "Boudewijn Rempt"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-09
    body: "KOffice allready is looking very nice, but I have to agree with ODF compatibility problems. I recently switched to Koffice from OO.org, but switched back, after exchanging documents turned out to be a nightmare. This is not necessarily have to be a Koffice-problem, could also be a problem of OO.org. But how can the user figure out where the problem lies and has to be reported to? Since OO.org is more used, I now use OO.org again."
    author: "Samba"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-09
    body: "The user could check the same document in different viewers, but in the end he can't be sure which one is right. The devs have to look into the ODF spec. In some cases the incompatibility might also be caused through inaccuracy of the spec."
    author: "ac"
  - subject: "Re: ODF compatibility"
    date: 2007-01-09
    body: "I tried to reproduce our masthead, which we have in openoffice and M$Word, in KWord and in Abiword recently. It was a nightmare. The OpenOffice version is attached to this posting.\n\nIn Kword, I was able to create something which at least looked similar. In Abiword I wasn't even able to create something similar. Once I got close, but then closing and reopening the file (native Abiword format) messed everything up again.\n\nOpening the odt file in another ODF compliant application is a different matter though. Try to open the attached masthead with KWord, and it will be all messed up. In Abiword it's the same.\n\nMy recreation of the masthead in KWord will also not open correctly in OpenOffice, not to mention Abiword...\n\nODF standard - that sometimes sounds more like wishful thinking."
    author: "Andreas Speck"
  - subject: "Re: ODF compatibility"
    date: 2007-01-09
    body: "Your masthead doesn't look very complex, but you did choose to use some items that are not well supported in OOo (frames).\nThe alternative is to use an anchered image, which may take some tweaking, but will give you better results. The KWord support is not 100%, but it should be enough\n\nAnchored content in 2.0 are in design right now, but they really look like they will be awesome!"
    author: "Thomas Zander"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-09
    body: "Even better; there is a testsuite.\nhttp://testsuite.opendocumentfellowship.org\n\nThe tests are written based on the spec, not on the implementation of one application. There you can see how well or bad each app behaves in each section. (people willing to make screenshots of the tests in different versions of the applications,  please report to; http://www.opendocumentfellowship.org/~testsuite/ )\n\nDo note that ODF is pretty new; its expected to see compatibility grow over time as applications find bugs and fix them."
    author: "Thomas Zander"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-13
    body: "i can agree 100% to the persons complaint, i use koffice on my freebsd machines and openoffice in my girlfriends laptop and interoperability is hell.\ni even get better results exchanging files in .doc\n\na plain text with two pictures created in openoffice and opened in koffice is a mess! the pictures are in wrong places, the fonts are bigger in kofffice, certain pngs dont load at all...\n\ni am not saying its koffice's fault, but you really need to work with ooo on interoperability!!!"
    author: "hannes hauswedell"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-09
    body: "There's still work to be done, but already there are benefits from a common format - as KOffice has improved greatly in the recent releases I've been using it more and more on documents I created in OOo without having to think about doing a conversion first"
    author: "Simon"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-10
    body: "I'm a KDE user all-around, but still I need to use OpenOffice if I need\nsome compatibility with the MS Office, which, sadly, is the ACTUAL standard.\n \nSorry but this isn't the way to go, peek into OpenOffice, do what you want\nbut at least .doc (word) and .xls (excel) document compatibility should\nbe granted (and actually isn't )....."
    author: "matte"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-10
    body: "Feel free to be the umpteenth person who tries to mooch the MS file format code from OpenOffice... Unless you've tried you don't realize just how impossible a task it is.\n\nAs for us, we're going for OpenDocument. Even Microsoft will soon be forced to support it."
    author: "Boudewijn Rempt"
  - subject: "Re: Looking forward to it..."
    date: 2007-01-10
    body: "http://ariya.blogspot.com/2006/04/why-koffice-not-using-openofficeorgs.html"
    author: "AC"
  - subject: "misspelled word"
    date: 2007-01-08
    body: "You misspelled 'definitely' as 'definately'. :-)\n\nNow send me my cookie!"
    author: "Pram"
  - subject: "Re: misspelled word"
    date: 2007-01-08
    body: "Okay, I'll send it via an HTTP Header :P  I hope you find it tasty.\n\nBut yeah, the KOffice people caught that mistake during proof reading, but since I didn't want to bother creating a new screenshot (being lazy as I am), I managed to turn it into a positive mention for the upcoming Sonnet spelling and grammar engine. :)  "
    author: "Troy Unrau"
  - subject: "Re: misspelled word"
    date: 2007-01-09
    body: "Hmm, but they didn't catch the extra 'to' in 'comes to time to print'?  :)\n\nMaybe the grammar checker will!\n"
    author: "Martin Fick"
  - subject: "Re: misspelled word"
    date: 2007-01-11
    body: "What's wrong with 'when it comes to time to print'? It's a bit clumsy but not grammatically incorrect as far as I can see."
    author: "Tim"
  - subject: "Re: misspelled word"
    date: 2007-01-09
    body: "Devious!\nVery nice article; many thanks :)"
    author: "Thomas Zander"
  - subject: "Thanks for this!"
    date: 2007-01-08
    body: "I'm really liking this series, Troy - it's providing a great deal of interesting tidbits, and providing some nice visibility into the KDE4 development process.  Thanks for putting it together! (And thanks for all the KOffice guys, too, for their excellent work in providing some a comprehensive, lightweight and well-integrated suite!)"
    author: "Anon"
  - subject: "Re: Thanks for this!"
    date: 2007-01-09
    body: "++"
    author: "Joergen Ramskov"
  - subject: "Great stuff, but..."
    date: 2007-01-09
    body: "...will it support changing the background colour away from the KDE UI settings? That's the reason I don't use KOffice, because I like my dark grey colour scheme, but I don't like document backgrounds being dark grey."
    author: "Steve"
  - subject: "Re: Great stuff, but..."
    date: 2007-01-09
    body: "AFAIK you can define a color scheme per application\nSo you could define another color scheme for koffice"
    author: "otherAC"
  - subject: "Re: Great stuff, but..."
    date: 2007-01-09
    body: "You're kidding, right? Are you seriously claiming that you don't use an office application because you don't like the background colour behind the documents? I find that really hard to believe. "
    author: "Andre"
  - subject: "Re: Great stuff, but..."
    date: 2007-01-13
    body: "I dont use Koffice by two reason, sheet color what changes as neutral-gray what i need to use because editing pictures, and i would like to have sheet paper as white so i can see what it would look like on paper with pictures and other objects.\n\nsecond reason is that sheet papers dont have any space between them. It's more like toilet paper what you are writing and it just dont look good. I made few wishes for middle sheet and those were taked to use so paper is on middle but there is no space still between sheets... when it comes, koffice will go over OOo."
    author: "Fri13"
  - subject: "Re: Great stuff, but..."
    date: 2007-01-13
    body: "Both these issues are already done in svn. You'll be able to switch when 2.0 comes out :)\nsee this old image that shows page-spreads and the page spacing;\nhttp://members.home.nl/zander/images/200606-KWord-PageSpread.png\n\nIn case you wonder; the page fully in view is a 'pagespread' which means its one page that will be printed to be 2 pages.  Some people need that. The direct effect is that you don't see a split in that page. But the other pages lower will have the spacing."
    author: "Spacing."
  - subject: "Re: Great stuff, but..."
    date: 2007-01-09
    body: "Currently that feature you don't like is not implemented; the background is always the paper color :)\n\nThis is a controversial feature indeed; I'm sure that if we re-add it we will do so with a config setting."
    author: "Thomas Zander"
  - subject: "Killer apps..."
    date: 2007-01-09
    body: "I believe KWord and co. could become killer applications for people moving to KDE.\nI really hope KOffice 2.0 is a big success, and I would like to say a big thankyou to all the KOffice devs!"
    author: "person dude"
  - subject: "On text layout and dancing characters"
    date: 2007-01-09
    body: "With Scribe, the new text layouting engine, these issues should be fixed with KOffice 2, and most text in the first screenshot looks good so far with regard to this.\nStill, i found some issues which still show some spacing issues:\n- second paragraph: the first \"text\" looks like \" t ext\"; in the second \"text\" the \"xt\" looks quite condensed; \"technical looks like \"t echnical\".\n- the paragraph below the red arrow: the dot after \"dynamically\" is moved into the word. Looks like wanted kerning (compare with the other dots and the comma after \"library\", second paragraph)\n- Most obvious: both of the rotated paragraphs look quite much much like the old dancing characters: almost all characters are somehow tilted and are a tad off of the ground line, moved a bit up and down. Didn't pick any words, should be visible with all of them. Looks more abvious on the two-line, stronger rotated one.\n\nWhat's the reason for this? Is this what you mean by \"not complete\", so does this get addressed with time, so it's not like this can't be fixed like in KOffice 1.x?\nI know it's a work in progress and it's only stacking up right now and still rough around the edges; still I wanted to point it out, as I thought this was already gone with the new layout engine and to make sure these issues don't get overlooked."
    author: "Phase II"
  - subject: "Re: On text layout and dancing characters"
    date: 2007-01-09
    body: "Yeah, I'm concerned as well.  Text appearance has always been my major problem with KDE apps, and everything I've read about Scribe indicates that this will fix all of those problems.\n\n...so screenshots that still have these problems make me nervous.  This'll be fixed in the final version, right guys?  Anyone?"
    author: "ac"
  - subject: "Re: On text layout and dancing characters"
    date: 2007-01-09
    body: "let's hope so. at least it got better already, and this is so much a work in progress i find it hard to believe they won't spend time on it. even if you do daily SVN updates, the progress on Koffice is amazing - they easily change over 60 files each day, frequently adding more than 10 or 20 files... it's a very active KDE module."
    author: "superstoned"
  - subject: "Re: On text layout and dancing characters"
    date: 2007-01-09
    body: "Ops... it really looks bad, just the same type of problems that were plaguing 1.x. Hmmmm, I really, really hope that this is due to not using more advanced text rendering features that *will* be turned on later during development (anybody can confirm / decline ?). BTW - it seems to me that with such text rendering KOffice can be only treated as a toy, not a real tool ;-)"
    author: "Gawron"
  - subject: "Re: On text layout and dancing characters"
    date: 2007-01-09
    body: "Hi,\n\nI'm personally already quite impressed with the way that things look a magnitude better than KOffice1.x\nI do grant you that there are several slight problems.  (see http://bugs.kde.org/show_bug.cgi?id=139130 for instance)\n\nNot all issues have been solved even in the bugreport issue, but they are solvable. And we aim to solve all of them in time for the release.\n\nThis is a coordinated effort between the KWord + Qt + Pango guys.\nSee http://www.freedesktop.org/wiki/Software/HarfBuzz"
    author: "Thomas Zander"
  - subject: "Re: On text layout and dancing characters"
    date: 2007-01-09
    body: "I followed your link, and it seems to be saying that the remaining rendering problems aren't due to issues with KOffice 2.0 code but rather issuess with the fonts that were used.  I'm I correct in this understanding?  If so, does this mean that only fonts that have been optimised for KDE/KOffice use will display correctly?  That could be an issue."
    author: "cirehawk"
  - subject: "Re: On text layout and dancing characters"
    date: 2007-01-10
    body: "Correct it's issues with the font used. But it's not about beeing optimized for KDE/KOffice, it's about fonts having good/correct hinting. The bad font's will render just as crappy all places that use FreeType.\n\nAnyway I think KOffice should default to a better font than the one used in these screenshots. Using that particular(crappy) font gives a bad impression of KOffice. It's about first impressions and using a better font will help a lot."
    author: "Morty"
  - subject: "Re: On text layout and dancing characters"
    date: 2007-01-10
    body: "Agreed!\nMy first reaction on all these screenshot is 'ugly', and I have real difficulties to 'forget' the poor font kerning."
    author: "renoX"
  - subject: "Hinting?"
    date: 2007-01-10
    body: "I just wonder, instead of what happened with the kerning: where did the hinting of the fonts go?\n\nThe UI has nicely hinted letters (Vera Sans most likely, and using the hinting instructions in the font), but for some reason it produces awful fuzzy blobs in the document itself, even though it seems that the same font is used there. If this is really Vera Sans there, then either the font rendering in the document is broken or the person who made the screenshots has some weird settings.\n\nI should install kde4 and koffice2 myself once to see how it looks like, the screenshots posted here don't really tell enough to see what's going wrong.\n"
    author: "Selene"
  - subject: "Re: Hinting?"
    date: 2007-01-10
    body: "This is a pre-pre-pre alpha release, guys.\n\nPlease take it like that and don't burn something that actually is quite is step up from the previous release because its not perfect yet.\n\nCelebrate progress, be patient for perfection.\n\nWell, unless you want to submit patches ;)"
    author: "Thomas Zander"
  - subject: "Re: Hinting?"
    date: 2007-01-10
    body: "Oh I wasn't burning, I was trying to understand your comment.  I am excited about the upcoming 2.0 version of KOffice."
    author: "cirehawk"
  - subject: "Re: Hinting?"
    date: 2007-01-11
    body: "It isn't burning either, but if the issue is just with some fonts as said above, then you'd better use fonts which render correctly to really show-off your work: even when trying, it's hard to overcome the 'ugly font rendering' first impression to appreciate your work..\n"
    author: "renoX"
  - subject: "Re: Hinting?"
    date: 2007-01-11
    body: "> then you'd better use fonts which render correctly to really\n> show-off your work\n\nYou are right. I'll correct that right here and now.\nI've been working with type years ago. Must have gotten rusty.\n\nThe one attached to this post better?"
    author: "Thomas Zander"
  - subject: "Re: Hinting?"
    date: 2007-01-12
    body: "Not really, this font is really weird: the 'u' is bigger than the 'm'..\n"
    author: "renoX"
  - subject: "Re: Hinting?"
    date: 2007-01-12
    body: "I wouldn't say \"not really\" I'd say \"yes!\"  Because it *is* better.  It's not perfect, as you noted the 'u' is taller.  Assuming that's not intended by the font, then it's less than perfect, but without the kerning issues.\n\nThanks Thomas and all the other KOffice devs, we really do appreciate your hard work!"
    author: "mactalla"
  - subject: "Re: Hinting?"
    date: 2007-02-10
    body: "This font doesn't look *that* good. 'b' and 's' seem to have more space around them than some other letters."
    author: "ruben"
  - subject: "A great series!"
    date: 2007-01-09
    body: "Thanks a lot for these articles! \n\nThey provide exactly the information, I long to get. \n\nAnd someone beat me on definately ;) \n\nBest wishes, and keep it up! \n\nArne"
    author: "Arne Babenhauserheide"
  - subject: "Qt-Only Version?"
    date: 2007-01-09
    body: "Hi,\n\nGlad to see that OOo might be getting some tougher competition!  Does anyone know if it is possible or if it will be possible to create a Qt-only version of Koffice, which does not require all the KDE deps?  I ask this because a low-resource office suite is desperately needed in the Linux world, and I imagine that most users who are using an old computer aren't going to  be running KDE.\n\nThanks!"
    author: "Rahim"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "a few kde libraries wont really do much damage, a full loaded kde desktop isnt required."
    author: "redeeman"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "So, on Debian, it would require kdelibs or something like that?  It's just I remember when I used to use XFCE on an old computer, if I opened even something little like Kcalc, it took ages to load.  But I suppose with DCOP being gone, that will help the initial overhead quite a bit?"
    author: "Rahim"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "it's not dcop, it's loading all the libs and starting necessary support daemons such as kded and what not.\n\nyou're probably not saving as much with xfce as you think if you're running kde apps in it."
    author: "Aaron J. Seigo"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "Yes, I agree.  I guess that's sort of my point, is there any way to make Koffice compile as Qt-only or something? "
    author: "Rahim"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "No, but you could try AbiWord perhaps?  "
    author: "Robert Knight"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "The simple answer is no.  \n\nThe long answer is due to all the functionality that KOffice pulls in from kdelibs so it doesn't have to do all that work itself.  Like the file dialogs, config dialogs, toolbars, printing, auto-saving, config backend, kparts, and so forth... removing the kdelibs dependency would cause KOffice to re-implement this all in-house, so you really wouldn't be saving anything."
    author: "Troy Unrau"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-10
    body: "indeed; you'd get open office all over again at the cost of a gargantuan development effort."
    author: "Aaron Seigo"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-10
    body: "and - Koffice with the KDElibs is still smaller than OO.o itself..."
    author: "superstoned"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "You could load kdelibs with xfce, so that you won't have the startup delay when launching a kde application.\n"
    author: "otherAC"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "XFCE uses less memory when you start a clean session. With more apps open, XFCE can use more memory because every application (AbiWord, OpenOffice, Firefix) has it's own set of libraries. At some point KDE wins here. See the following: http://ktown.kde.org/~seli/memory/desktop_benchmark.html\n\nFor starting KDE applications outside KDE, also try:\nhttp://wiki.kde.org/tiki-index.php?page=Performance%20Tips#_Faster_startup_of_KDE_applications_outside_of_KDE\nhttp://wiki.kde.org/tiki-index.php?page=Performance%20Tips#_Directory_prescanning_in_KDM"
    author: "Diederik van der Boor"
  - subject: "Re: Qt-Only Version?"
    date: 2007-01-09
    body: "Not depending on the kdelibs would not actually mean less code (in total) or a lower functionality requirement.\nSo its like saying you want to eat a full meal, but don't want to gain any weight. So lets only eat crackers!.\nWell, in both cases all you are doing is move the requirement to another place and you'll just be adding lots of code in KOffice itself.  Just like you'll eat a whole lot of crackers to be able to sustenance you.\n\nAs another poster in this thread said; sharing the libraries with many other applications will in the end save you space since not every application will load a _different_ set of libraries even though they all need a large total amount of libraries.\nSo you can see it like you are carpooling libraries.  You save in fuel money by sharing the car with your neighbor. That doesn't change the fact that you both need to travel a long distance, it just means it has less of an impact.\n\nCarpooling is the real solution; buying a smaller car of your own doesn't actually help much.\n\nHope that explains this rather complex mental model a bit better :)"
    author: "Thomas Zander"
  - subject: "track changes"
    date: 2007-01-09
    body: "IMHO, a 'track-changes' feature is a must for a serious word proccesor. OOo has it, Word has it and i think even AbiWord has it.\nThen, of course, one need to compare two document in a clear visual way..."
    author: "yuval"
  - subject: "Re: track changes"
    date: 2007-01-09
    body: "Definitely. Also cross-referencing and indexing features (at least, these were still missing last time I looked which has been a while). From what I see in the screenshots, there is still too much focus on \"fancy layout stuff\" (which IMO they could leave to Scribus and vector graphic programs) and not enough on basic text processing and every day tasks. To concentrate on the usual \"dull, square documents\" seems still the biggest problem for the developers. ;-)"
    author: "xyz"
  - subject: "Re: track changes"
    date: 2007-01-09
    body: "The big work going in is about laying foundations and adding things like proper borders around paragraphs or doing proper lists and counters.\nAll pretty boring work, so you won't see an article dedicated to it (but be sure to check out my blog where I list some less boring parts more often then its on the dot).\nAll in all; the 'boring text editing' is certainly a high priority."
    author: "Thomas Zander"
  - subject: "Re: track changes"
    date: 2007-01-11
    body: "...and as someone who actualy even likes those boring stuff I like to add, that there was and still is a lot of great progress on it even if not direct visible like the \"fancy layout stuff\" :)"
    author: "Sebastian Sauer"
  - subject: "Kross and QtScript"
    date: 2007-01-09
    body: "Trolltech will release QtScript, successor of QSA, with Qt 4.3. Will this have any effect to Kross?"
    author: "MM"
  - subject: "Re: Kross and QtScript"
    date: 2007-01-09
    body: "Kross is not the equivalent of QtScript. The latter will just supply javascript while kross provides a bridge to several different languages and not even have its own implementation of javascript. It uses kjsembed for that.\n\nThe author of kjsembed has been working together with the Qt guys to make sure there will be one great javascript implementation for the KDELibs."
    author: "Thomas Zander"
  - subject: "Re: Kross and QtScript"
    date: 2007-01-09
    body: "So QtScript and KJSEmbed will merge?"
    author: "MM"
  - subject: "Re: Kross and QtScript"
    date: 2007-01-12
    body: "IMO No. KjsEmbed uses Kjs which is rock stable + got adopted by WebCore while QtScript does seem to provide an own JavaScript implementation. See also http://dot.kde.org/1051644663/1051681934/1051776581/ which is still valid afaik."
    author: "Sebastian Sauer"
  - subject: "Re: Kross and QtScript"
    date: 2007-01-12
    body: "But from reading over the post, I think it suggests that KjsEmbed and QSA (predecessor of QtScript) should merge?"
    author: "MM"
  - subject: "Re: Kross and QtScript"
    date: 2007-01-13
    body: "Well, IM(H)O merging is maybe not the best idea since Kjs+KjsEmbed and QtScript are just different implementations. Merging them is like to suggest to merge Python with Ruby cause you like both and cause they are very similar (OO, dynamic, written in C, scripting languages, etc.). Anyway, that's only my personal point of view and everybody who believes it's possible to merge them (KjsEmbed with QtScript or maybe even Ruby with Python) and likes to start working on it, is free to do so and it wouldn't be the first time that an unique vision becomes reality within the KDE-project :)"
    author: "Sebastian Sauer"
  - subject: "Re: Kross and QtScript"
    date: 2007-01-13
    body: "btw, to close the circle the title of this thread was dealing with; for sure there is nothing what would prevent us to also add QtScript-support to Kross which would then support both js-implementations. Nothing except that myself does not like JavaScript at all and would like to see Java-support first much more ;)"
    author: "Sebastian Sauer"
  - subject: "Re: Kross and QtScript"
    date: 2007-01-16
    body: "Thanks"
    author: "MM"
  - subject: "eye candy"
    date: 2007-01-09
    body: "You see... kword has not such lots of buttons that the other office suits have (especially MS Office 2007). This will confuse the users- they will think that the kword is not as powerful as they are..."
    author: "Aceler"
  - subject: "Re: eye candy"
    date: 2007-01-09
    body: "Please remember that Koffice 2 is in development.  I'm sure this will change."
    author: "Vlad"
  - subject: "Re: eye candy"
    date: 2007-01-10
    body: "let's hope not, one of the good things of Koffice is it's clean interface..."
    author: "superstoned"
  - subject: "Minor complaint"
    date: 2007-01-09
    body: "If that is the default font setting for KSpread, please change it.\n\nThe numbers in the left column are too big for the row height and the text on the first row has some issues with their kerning.\n\nOtherwise, brilliant visions!"
    author: "Dr No"
  - subject: "Freeze panes"
    date: 2007-01-09
    body: "Still there's no news about including the beloved \"Freeze Panes\" feature. For me, it means KSpread is unusable. How can you work on anything without seeing a header of the spreadsheet? \n\nI think there must be a similar feature I didn't notice. I would appreciate if someone could point it out for me.\n\nSee also http://wiki.kde.org/tiki-index.php?page=KSpread+Development .\n\nregards, Gabor"
    author: "LGee"
  - subject: "Re: Freeze panes"
    date: 2007-01-09
    body: "View -> Split View"
    author: "Stefan Nikolaus"
  - subject: "Re: Freeze panes"
    date: 2008-01-14
    body: "Split View is not the same at all. Window freezing will keep one or more rows or columns still while you scroll through the rest of the document. Try it yourself scrolling through 1000 rows of data. Split view shows me two windows and one doesnt have row and column headers so they aren't even aligned."
    author: "Dave"
  - subject: "Text flow"
    date: 2007-01-09
    body: "Will the text flow algorithm be augmented to support enforcing an optional bit of space in between objects? (This could be a property of each object.) \n\nCurrently the 'g' in \"upcoming\" almost touches the flower and the 'f' in \"features\" is very close to the arrow."
    author: "r-jon"
  - subject: "Re: Text flow"
    date: 2007-01-09
    body: "I fixed a bug in that code shortly after the screenshot was taken.\nAnd you can configure the distance for each object individually as well."
    author: "Thomas Zander"
  - subject: "Very poor printing support"
    date: 2007-01-09
    body: "When it comes to printing, KWord & Co. fall flat on their faces. Unfortunately. I'd very much like to love KOffice, but can't (since it doesn't love me either). \n\nBecause, what do you do with a document you created or modified usually? Yes, maybe send it off by email. Very often you'll convert it to PDF and mail that format ('cause most people out there do not yet know about KWord). And in the end, more than 50% of documents will end up getting send to a printer at least once.\n\nSo it is very crucial that some elementary things in respect to printing support are implemented in all document-creating applications: the most important one is font fidelity from screen to paper (otherwise you get unwanted line- or page-breaks), one other is support not just for all standard media sizes for printing but also for any custom format you may define.\n\nIn KWord, these very basic features for everyday use are missing. I'm not even talking with the poor (rather: non-existing) WYSIWYG support for fonts (you usually don't get the same page image painted on paper as you get painted on screen).\n\nI'm more concerned about custom page sizes. Which we use a lot. Or envelopes. Can't print on envelopes (addresses, batch printing from database), because KOffice can't even create a correct PostScript print file with the correct settings for PageSize and BoundingBox. Same for custom page size. Internally, KWord boasts that it can create 10x10 or 12x9 or 42x21 inch sized documents. \"Great!\", you think, and start working on them. You finish it, and want to print it (yes, there are printers that allow for custom media sizes). No joy: KWord gives you an A4 PostScript document, with everything overflowing just cut off. Not even \"Print to file (PDF)\" does work correctly (I had hoped for being able to use *that* for printing, because our commercial printshop around the corner *can* do 42x21 inch poster printouts).\n\nI'm aware that this is probably Qt's responsibility (it is only capable of PostScript Level 1, and it is rather buggy when it comes to embed fonts into its PostScript export files). But this shifted point of responsibility doesn't help me, the user. I just suffer from this overall system being unusable for my document creation and processing needs.\n\nAnyway... I sure hope KOffice 2.0 will rock with all its playful features that the developers do enjoy so much programming for!\n\nBut please, please, please do not forget the everyday bread-and-butter needs we normal users in the offices do have to cover.\n"
    author: "ac"
  - subject: "Re: Very poor printing support"
    date: 2007-01-09
    body: "Its feels odd that you complain about the level of WYSIWYG of koffice1.x while this article makes clear the text engine has gotten a major revamp.\n\nAnyway; in short. The printing of the pages will go directly to PDF in KWord2. I already have that working and the text looks very good.  This includes DTP like features like automatic page-bleed and font embedding.\nCups will use these PDFs to convert it to your printers optimal format.\n\nAbout custom page size; yes thats indeed a missing feature in Qt; see http://www.trolltech.com/developer/task-tracker/index_html?method=entry&id=99441\nIts scheduled to be included in 4.3. Which is what KWord2 will use when it comes out.\n\nIn other words; have patience and your dreams will be fullfilled :)\n\nOh; you don't have to wait until the release to send me pizza's, you can do that right now already ;)"
    author: "Thomas Zander"
  - subject: "Re: Very poor printing support"
    date: 2007-01-09
    body: "I'm sorry to say it, but \"ac\" very much expressed feelings that are very close to my own. (On some points, I'd even go farther than he did in his \"puring cold water over the hype\").\n\nYes, it feels odd to read such comments underneath an article where the great achievements of KOffice 2.0 development are highlighted. But the very same article also tried to convince me that \"KWord 1.6.1 is already a powerful KDE-integrated word processor\"...\n\nSo I also think a bit of a reality check is in order. Sometimes it helps."
    author: "Kurt Pfeifle"
  - subject: "Re: Very poor printing support"
    date: 2007-01-09
    body: "\"KWord 1.6.1 is already a powerful KDE-integrated word processor\"\nThat statement is correct :) it's might not be powerful enought for you yet. But it allready covers quiet a lot of use cases (and it's integrated in KDE)."
    author: "Cyrille Berger"
  - subject: "Re: Very poor printing support"
    date: 2007-01-10
    body: "indeed. i know it's far from perfect (but so is anything). but i can use it for most of my writing, i just create the final pdf with OO.o (after fixing the incompatibilities...). OO.o is just a pain in the ass, slow and harder to use. so i use Koffice, even tough it has some flaws... i really look forward to KDE4 and Koffice2, as they are supposed to fix the few problems they still have :D"
    author: "superstoned"
  - subject: "Re: Very poor printing support"
    date: 2007-01-09
    body: "Glad to hear about the text engine revamp. :-)\n\nI raised the points for those developers who may be interested in acknowledging them. When/where else but here could I raise them?\n\nI don't want to have to raise them again when the new text engine for KWord3 is promote in a Dot article of 2012, you see?  :-)\n\nSo font fidelity as well as page size setup are now acceptable in KWord2?"
    author: "ac"
  - subject: "Re: Very poor printing support"
    date: 2007-01-09
    body: "IMHO it did serve a good purpose to hint at the current KWord 1.6.x weaknesses -- so you guys don't forget what the real world requirements are for people who aren't busy to design great, elegant and powerful software libraries such as Flake (please: this is by no means meant to sound sarcastic, Thomas!).\n\nHmmm... maybe I should look at this from even another point of view, and say it differently: \"...so you guys are fully aware about the real world requirements of people for whom you are design your great, elegant and powerful software libraries.\" Yup, I think that's better.   :-)\n\nI sure have patience to wait for my dreams to be fullfilled.\n\nBut I also have fears.\n\nFears, that my dreams aren't understood at all by you guys because you simply care about completely different things. You have different priorities. Most of you are doing Free Software for fun, not because you are paid for it. That one factor alone makes for an entirely different set of interests to persue when you are coding at night. And caring for acceptable print results on paper may simply be too boring for you. (That's not an insult, just a statement of facts.)\n\nYes, despite my fears, I'm *still* really looking forward to see KWord 2.0 come to life with all its great new features.  :-)\n\nIn any case -- tell me the closest Pizza service (or do you prefer a Restaurant?) to your home address (and if possible, their phone number). I'll see what I can do. Peperoni or Salami? :-)\n\nOr something completely different?  :-)"
    author: "Kurt Pfeifle"
  - subject: "Re: Very poor printing support"
    date: 2007-01-09
    body: "Its funny that some people in this thread have stated I focused on DTP like features too much. And then you state kind of the opposite.\n\nAmusing world this is :)\n\npop quiz; how many wordprocessors print PDFs using proper bleed? Out of the box without the user setting anything?\n\nso, as you are someone not a stranger to gcc, please take the time to compile koffice2 and see for yourself.  AFAIK all the features you want are already there.  Assuming you can work with PDF instead of postscript ;)\n\nps. almost all, see the other post about custom page sizes."
    author: "Thomas Zander"
  - subject: "Re: Very poor printing support"
    date: 2007-01-10
    body: "Hahaha... that link to the Troll's bug tracker is a good joke. \n\nAs you can see, in their current bug tracker they had at first the version to fix this to '4.1.0', then to '4.2.0' and currently it is '4.3.0'.\n\nBut I can counter your joke with an even better one:\n\n  http://bugs.kde.org/show_bug.cgi?id=54410#c2\n\nAs you can see, Philipp Mueller wrote nearly 4 years ago in KDE's bug tracker about \"Custom paper layout settings not honoured\": \n\n\"Trolls say, this is intended to be implemented in QT 3.2, so we need to wait until it is also implemented in KDE 3.2\".\n\nWhy, oh why do I have a hard time to believe you? Why, oh why do I really wonder how the Trolls run still such a successful business?\n"
    author: "ac"
  - subject: "OT: Tear-offs?"
    date: 2007-01-09
    body: "May somebody explain to me, why tear-off menus are going to completely disappear in KDE? I believe, the last official app using it, is Konsole."
    author: "Sebastian"
  - subject: "Re: OT: Tear-offs?"
    date: 2007-01-11
    body: "No idea, but i can't find the tear off menus in konsole?\nHaven't seen them since kde 2.x or even earlier.."
    author: "otherAC"
  - subject: "Re: OT: Tear-offs?"
    date: 2007-01-11
    body: "Ups. right. seems they are finally gone even there. worked until 3.3"
    author: "Sebastian"
  - subject: "Re: OT: Tear-offs?"
    date: 2007-01-11
    body: "Possibly because no-one else uses it and new users don't understand how to use it. That I think is a legitimate reason, even if I don't know if it is correct."
    author: "Dr No"
  - subject: "Faster and lighter?"
    date: 2007-01-09
    body: "First of all, your series of articles about KDE4 apps is very interesting. A good preview.\n\nBut one thing that gathered my atention is that KOffice 2 is slower than current version. Wasn't the move to Qt4 suposed to make aplications faster and lighter? I don't want to see an full featured KOffice as bloat as OpenOffice is now.\n\nBy the way, everything in the KDE4 snapshot versions is already runing in top of Qt4? Or at least the base desktop? If so, how is memory consumption and starting time in KDE4, with only the porting and basic changes? I hope it is lighter than current KDE 3.5.x...."
    author: "Manabu"
  - subject: "Re: Faster and lighter?"
    date: 2007-01-09
    body: "First off, everything compiled for KDE 4 has a lot of debugging overhead.  This will go away in the final release.\n\nWhat happens is that with all the debugging info still in the binaries, the filesize gets large (sometimes doubling the app size), which then takes more memory and so forth.\n\nThe main problem I have right now for speed is startup speed, and that's due to debugging more than anything else.  Qt4 is very very fast, and it's most evident when running a Qt-only application since Qt 4 has been stable for quite a while.\n\nKDE 4 is still in development, and there is legacy KDE 3 stuff that is slowing things down that is slowly being purged.  The slowest thing at the moment that I've found is the drawing of kicker tooltips... However, since kicker will never make the full transition to KDE 4, that is probably not going to be fixed... just replaced.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Faster and lighter?"
    date: 2007-01-12
    body: "I forgot these things... \n\nI will search for more Qt4-only aplications, and leave KDE4 for when it its ready, or at least release candidate.  ;)"
    author: "Manabu"
  - subject: "Re: Faster and lighter?"
    date: 2007-01-10
    body: "> If so, how is memory consumption and starting time in KDE4, \n> with only the porting and basic changes?\n\nShort answer:  Come back and ask again in a few months time.\n\nLong answer:\n\nIt really isn't possible to answer that question accurately with the current build, which is still very much work-in-progress.  For example, on my machine the startup splash gets to the \"loading the desktop\" phase and then sits and does nothing for 10 seconds or so.  You can see Konqueror removing all of the toolbars and then placing them back again when switching from one tab to another, and so on.\n\nLeaving aside major architectural issues, optimisation is one of the last phases of development.  It is much more important to get the product functioning properly first.  This isn't just KDE development either.  The first public beta of Windows Vista performs much more slowly than the final release, despite all of the architectural changes which had already been made under the bonnet to improve performance ( and no, I am not implying that KDE 4 will have the memory requirements of Vista )"
    author: "Robert Knight"
  - subject: "Re: Faster and lighter?"
    date: 2007-01-11
    body: ">>You can see Konqueror removing all of the toolbars and then placing them back again when switching from one tab to another\n\nWell, that's what my konqueror also does in kde 3.5.5 :o)\n"
    author: "otherAC"
  - subject: "Re: Faster and lighter?"
    date: 2007-01-11
    body: "Then either compile Qt+KDE without debug or double your RAM by buying another 64MB ship ;)"
    author: "Sebastian Sauer"
  - subject: "kspread usability"
    date: 2007-01-09
    body: "Looks like that kspread 2.0 will finally be compatible and usable for day to day business.\nhttp://bugs.kde.org/show_bug.cgi?id=58652"
    author: "ferdinand"
  - subject: "Icons and text for toolbars by default"
    date: 2007-01-09
    body: "Is the default for KDE4 still going to be icons and text for toolbars as default? These screenshots just illustrate what a bad call that is in my opinion. It is a complete mess. On top of that some of the buttons don't have text at all which defeats the purpose does it not? I fail to see how my usability is enhanced by knowing that the icon of a printer means print, if so much of my screen is eaten up that I can't quickly press a selection of buttons to access lots of regularly used functions.\n\nJust look at how much space is consumed by the print preview button. I know the German language for example is fairly prone to having quite long words, so I dread to think what the screenshot might look like with with translation applied.\n\nHow are developers to cope with long translations affecting an aestheticly sane default layout in English that looks horrible translated?\n\n"
    author: "Matt"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-10
    body: "I see no concern here.\n\nThe underlying architectures are going through intense development, and when that's finished the devs will probably pay more attention to stuff like default settings. Right now the screenshots are simply demonstrating new text/shape layout technologies, not final release defaults!"
    author: "random ac"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-10
    body: "I think you're right. And when looking at microsoft word 2003 for example. I think oxygen icons is one thing that could make it look better, the other is just those yellow tooltips (description + shortcut), would make the whole look a little more professional. \n\nIt's a little bit like those KDE programs under windows, okay it works, but it somehow doesn't really look equally to a windows program or to firefox for example...\n\nWell we will see, it is still work in progress. A nice theme and some cleanup, could change the whole look and feel.... (I hope....)"
    author: "boemer"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-10
    body: "The solution for this problem is what Windows calls \"Selective text on right\". I believe it is used both in Windows and in Gnome applications for frequently used buttons (thus giving a larger click-area).\n\nAlmost 3 years ago, a bug on this matter was marked as WONTFIX.\nhttp://bugs.kde.org/show_bug.cgi?id=75970\n\nI believe it should be reopened. (as a side-note, I believe this should be the default mode for the icons - Windows has it as default, don't know about Gnome).\n"
    author: "Mircea Bardac"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-11
    body: "gnome has icons & text as default mode.\nAFAIK there is no easy way to turn it into icon mode (e.g. no right clicking on the toolbare, like in kde or windows)\n"
    author: "otherAC"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-14
    body: "there is a control panel for those menu toolbar and icon settings, which works for most Gnome applictions.  unfortunately not all GTK apps are quite so good at supporting.  it would also be better if it were built directly into the toolbar widget as standard.  "
    author: "AC"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-11
    body: "Perhaps a little late but I just wanted to add that I think thats really amazing stuff going on below the toolbar line! :) I just hope you can find a magic third way for above.\n\nI had a look at \"selective text on right\" and while its nice that frequently used buttons have larger click area, it also means that the ones that aren't selected to have text and also have an unintuitive icon are still hard to understand for newer users.\n\nHow about the text on right becomes available for all buttons when highlighted by the mouse, like a tooltip, but inside the button producing the larger click area and a nice description. The toolbar might dynamically go onto a new line  to make space for the text but only while the user is exploring buttons.\n\n"
    author: "Matt"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-12
    body: "How about the description just appear in the status bar as the user rolls their cursor over the toolbar icons? ..."
    author: "a.c."
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-12
    body: "If the icons are unintuitive, they need to be fixed :) The Oxygen team already did a great job with the KMail Icons - http://oxygen-icons.org/users/david/?p=31\n\nText on the right when highlighted would produce great flicker IMO. I don't think it's a feasible solution. I've seen no applications doing this."
    author: "Mircea Bardac"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-13
    body: "I don't find all those new KMail icons particularly intuitive. Sure, they look good (fairly crystal-esque? Is that just me?) but I mean, the reply/forward buttons -- no idea what they did when I first scanned the interface. There are another couple of buttons with arrows next to them. I wouldn't have guessed by the colour or way they were facing that one signified reply and the other forward.\n\nAs for \"great flicker\" -- all Qt4 widgets are double buffered by default. Flicker should be a non issue in KDE4, so that's not a constraint."
    author: "Cerulean"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-13
    body: "I tried such an interface where the buttons would become larger when you hover over them. It's a nice feature when you move from low to high. But when you move your mouse over them, it is almost impossible to select all buttons, because there position is constantly changing.\n\nIf you are on a button to the left, and you want to go to the next button you would move there, at the moment you leave the first button it would become smaller, automatically maybe putting you on the third button.\n\nIt think the selective text on the right like outlook 2003 is doing it, is not bad...."
    author: "boemer"
  - subject: "Re: Icons and text for toolbars by default"
    date: 2007-01-13
    body: "I see what you mean, but if they slowly returned to their original size without the text then you could mitigate some of the clumsiness of it, especially if the new button grew at the same rate as the last highlighted button shrank. \n\nI tend to dislike horizontal stuff like that anyway, I have widescreen and like to make use of the width rather than the height. You could have these buttons working nice low to high as you said if the toolbars were down the side rather than across the top. Why waste the height with toolbars when height is almost always the shortest screen dimension?\n\nMaybe a better idea would be to have a column of icons on the side and have a delay before all buttons in the column showed their text by growing the buttons sideways over the document. That would give big click area and very rapid visual scan of all icon descriptions."
    author: "Matt"
  - subject: "Users?"
    date: 2007-01-11
    body: "Some of the comments expressed are real concern - I suppose. Others of them I wonder if they are Gnome users or MSword users or someone just having a bad day.\n\nIn short, if you think you want to use something else, then use it. When something or someone tries to please everyone, it becomes useless. You can spread yourself out too thin.\n\nI am very thankful for Koffice and I like it much more than OpenOffice. It's fast and handles what I need to do fine. I hope Koffice2 fixes the concerns of most users.\n\nThanks for the good work guys!"
    author: "winter"
  - subject: "kword and ms-word pictures"
    date: 2007-01-11
    body: "Will kword 2 be able to correctly import, display and export ms-word docs containing pictures?  \nCurrently kword seems unable to display and manage pictures in ms-word documents.  "
    author: "xpol"
  - subject: "Never satisfied"
    date: 2007-01-12
    body: "As the old saying goes \"You give them an inch, they want the whole 9 yard\"\nI am sure some of you are sincere but others are just devious. Either way, you guys should be thankful and happy about all this great work that is being done. The developers are pretty excited about their accomplishments, instead of congratulating them, you guys chose to steal their thunder. Shame on you. I really mean it. SHAME ON YOU.\nI for one think what is being done now is astonishingly amazing. Great work guys and congratulation on a well done great job."
    author: "Abe"
  - subject: "Re: Never satisfied"
    date: 2007-01-12
    body: "+++"
    author: "ac guy"
  - subject: "Re: Never satisfied"
    date: 2007-01-13
    body: "> instead of congratulating them, you guys chose to steal their thunder\nNah, it's not that easy to steal their/our thunder :)\n\nAlso it's quit human to don't note what works fine but to complain about things that don't work or do not work as expected. To strive for perfection is a useful characteristic and it does help them/us to note where things need to be improved to be one step closer to deliver a perfect product (what is in reality never possible since humans themself arn't perfect what is good cause perfect humans would be sooooo boring).\n\n> I for one think what is being done now is astonishingly amazing. Great work guys and congratulation on a well done great job.\nI absolutly agree there and also like to say thanks to all their/our users, contributors, artists, translators, developers, bugreporters, helpers, friends, ...\n"
    author: "Sebastian Sauer"
  - subject: "Re: Never satisfied"
    date: 2007-01-13
    body: "  I understand that constructive criticism is part of the process of reaching high quality and I am glad you are taking it this way. But here, we are talking about a product that is still in its early development process and nothing has been release yet.\n\n The developers are pretty excited to let us know about their progress and accomplishment so far and for us to join them in their excitement. Why not wait until they have enough done to give them all the feedback they need?\n\n At this stage, all they deserve is praise and encouragement to boost their self esteem and make them feel good about their great work. They are humans too and they only can do so much at a time. Besides, some might be sensitive about issues where too much criticism could hinder their fast progress.\n\n My post was not only to criticize the posters but to make sure that the developers feel that their accomplishments are well appreciated."
    author: "Abe"
  - subject: "Re: Never satisfied"
    date: 2007-01-14
    body: "> Why not wait until they have enough done to give them all the feedback they need?\n\nIn my eyes even \"bad\" feedback is better then no feedback since it shows that ppl actualy care.\n\n> where too much criticism could hinder their fast progress.\n\nI guess to have that happen someone really needs to be blind since it is enough to take a look at the sourcecode to see what great work was done already and that (what is at least most important for me) it was done right even if it did took much more time then to hack quickly something together. IM(HO) the quality of the sourcecode speaks for itself and it makes just fun to read it.\nI would even go so far that it's evil to have such nice sourcecode since it's not easy to take away the \"book of code\" to continue reading other non coding related books. KOffice2 even hinder me to finish the latest fantasy-book I got around xmas cause the story the sourcecode tells is much more interessting :)\n\n> My post was not only to criticize the posters but to make sure that the developers feel that their accomplishments are well appreciated.\n\nThanks for that and the intention of my post was only to outline that it's not needed to shame cause of beeing human. It's much easier to just ignore postings that are \"to much human\" and just concentrate on those postings and ppl that provide constructive feedback, where it spends a nice feeling to deal with and where the positive karma is visible :)"
    author: "Sebastian Sauer"
  - subject: "Re: Never satisfied"
    date: 2007-01-15
    body: "OK, I understand your are a saint, but many others aren't.\nAs the saying goes \"you will catch more flies with honey than vinegar\". So feedbacks in more pleasant fashion will get more attention."
    author: "Abe"
  - subject: "Re: Never satisfied"
    date: 2007-01-15
    body: "I will certainly agree to that :)"
    author: "Thomas Zander"
  - subject: "thak you"
    date: 2007-01-12
    body: "thank you for these interesting articles!\n"
    author: "martin"
  - subject: "Kword DTP / Thanks"
    date: 2007-01-14
    body: "What I really love in kword is that there are some technics taken from the DTP world into the \"normal\" word processor. It is not a MS Office clone, it is an individual application trying to go new ways (not always, maybe not enought, but the developers do)\n\nThanks for you series again !\n\nGreetings\nFelix"
    author: "Felix"
  - subject: "Konqueror crash"
    date: 2007-01-22
    body: "I clicked on the link in the article about Sonnet architecture http://jrideout.blogspot.com/2006/12/how-is-sonnet-stacking-up.html\nand my Konqueror crashed (sic!). I did it three times and every time it crashed :D\n\nKDE 3.5.4"
    author: "ZEM"
  - subject: "Re: Konqueror crash"
    date: 2007-01-23
    body: "Yes, so do my beloved konqui, one of the few pages that make me use firefox.\nIt seems you can't go on any of the page of this blog whith Konqueror."
    author: "kollum"
  - subject: "Re: Konqueror crash"
    date: 2007-01-29
    body: "Working fine with kde 3.5.5"
    author: "-hector-"
  - subject: "Re: Spellchecking"
    date: 2008-05-26
    body: "The incorrectly spelled word was, \"definately\".\nI know because I always get that word wrong: it's, \"definitely\". :P\n\nI would very much like to use Koffice above anything else (due to desktop integration), but I found that, when I save a file in ODF format, I loose all the actual formating (such as different sized headings), and that .kwd format actually doesn't work (Complaints about a parsing error every time).\n\nIf these problems are fixed, then I'll take Koffice as it is - I still find myself depending on Openoffice.org, which is a shame."
    author: "Madman"
---
In this week's edition of the Road to KDE 4, we'll take a look at 
the up and coming KWord 2.0 as part of the <a 
href="http://www.koffice.org/">KOffice</a> project. KWord 1.6.1 is 
already a powerful KDE-integrated word processor, but with KDE 4 
technologies, KWord 2.0 promises to be among the most powerful free word 
processors available. Read on for more details.


<!--break-->
<p>KWord is part of the KOffice suite of applications which, with a few exceptions such as <a href="http://kexi-project.org/">Kexi</a>, has been visible thus far as a KDE-only application living under the shadow of the much larger <a href="http://www.openoffice.org/">OpenOffice.org</a> suite. But this won't always be so, as the new KDE 4 technologies allow KOffice to exist as a native application on other platforms such as Windows and Mac OSX. Look out for more details on KDE support for these platforms in a future article.</p>

<p>One of the biggest assets of KOffice and KWord is its native support for the <a href="http://en.wikipedia.org/wiki/OpenDocument">OASIS OpenDocument</a> standard, which is shared by many office applications these days (including OpenOffice.org, <a href="http://docs.google.com/">Google Docs</a> and others). Expect improved ODF document compatibility for KWord in the future as the developers strive for complete specification support.</p>

<p>Lets take a look at some screenshots from the development version of KWord. Notice the nice anti-aliasing of every element of the UI. On my system, it doesn't appear noticeably slower than KOffice 1.6.1. One of the most improved areas in KWord 2 is the text formatting and layouting, which definitely deserves some more exposure. It's not yet complete, but as you can see below, it's definitely much improved from previous versions. You really have to experience it yourself to appreciate how smooth moving, resizing and rotating <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a> shapes is in this new version.</p>

<p align="center"><img src="http://static.kdenews.org/dannya/vol2_devel_kword-flake.png" alt="KWord Devel version showcasing Flake"></p>

<p>All manner of objects are being converted to the new Flake library, for instance KFormula elements, so you can insert nicely rendered math into your documents without any trouble. This support could make KWord as exciting to use for page layouts as KPresenter, as you are no longer restricted to dull, square document shapes. These changes should enable KWord 2 to behave as a respectable basic desktop publishing application.</p>

<p>Also noticeable in this early preview version is the lack of spell checking support, as this is being reworked for the upcoming <a href="http://jrideout.blogspot.com/2006/12/how-is-sonnet-stacking-up.html">Sonnet architecture</a> for spelling and grammar corrections. (Which word did I misspell in my screenshot?)</p>

<br>

<p>But this is not the only improvement new to KOffice 2. Also in the works is scripting support for applications through the new and extensible scripting framework dubbed <a 
href="http://wiki.koffice.org/index.php?title=KWord/Scripting"> Kross</a>. It has received a lot of work and looks to be one of the killer features of KOffice 2.</p>

<p>The following screenshot shows the new scripts menus in KWord:</p>

<p align="center"><img src="http://static.kdenews.org/dannya/vol2_devel_kword-scripts.png" alt="KWord Devel version showcasing Kross"></p>

<p>Also notice how I moved the tear-off toolbars from the previous screenshot. I placed them by drag-and-drop, and they automatically tabbed up. This is all done very smoothly by Qt with no noticeable interface flickering.</p>

<br>

<p>Of course, the same scripting and rendering features have made their way into other KOffice apps as well. KSpread and scripting are a perfect fit, and there is a lot of power exposed to the advanced user.</p>

<p align="center"><img src="http://static.kdenews.org/dannya/vol2_devel_kspread.png" alt="KSpread Devel version"></p>

<p>For people interested in more details about Kross, check out <a href="http://wiki.koffice.org/index.php?title=KSpread/Scripting">this article</a> on the development and usage of Kross in KSpread.</p>

<p>These are just some of the many improvements in the works for KWord and KOffice when the KDE 4 platform rolls out. Of course, these screenshots are of the development versions, which are quite unstable at the moment, but jugding by the level of activity today in the developer channels (like #koffice on irc.freenode.org) there is a large amount of momentum behind this release.</p>

<p>KOffice has a separate release schedule from KDE 4, so they may or may not release concurrently.</p>

