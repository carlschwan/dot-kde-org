---
title: "People Behind KDE: Pino Toscano"
date:    2007-03-16
authors:
  - "dallen"
slug:    people-behind-kde-pino-toscano
comments:
  - subject: "A \"do-it-all\" developer"
    date: 2007-03-16
    body: "I'd like to warmly thank Pino for all his work in KDE especially in KDE-Edu. I very often ask his advice and assistance and he always answers my questions and helps me a lot when I don't know how to do this or that.\nHe is able to hack anything in KDE and from kdelibs fixes to websites PHP functions he knows everything! \nHe should blog more often so that people get to know his work better. Thanks Pino! And I hope aKademy will be in Italy soon!"
    author: "annma"
  - subject: "grazie"
    date: 2007-03-16
    body: "grazie Pino"
    author: "vincenzo"
  - subject: "Re: grazie"
    date: 2007-03-21
    body: "Grazie per il tuo impegno"
    author: "andrea"
  - subject: "Thanks Pino!"
    date: 2007-03-16
    body: "I really like what Pino is doing and watching its commits on weekly basis. He is spreading his words all over KDE ;-)\nSince when he entered our community, he has put a lot of efforts in bringing things into shape in many areas, while ensuring a good quality and implementing the coolest fetures...\nEven since we share the same country, I never had the chance to meet him, but I'd like it to happen one day or another.\nKeep up the good job, Pino!\n"
    author: "Enrico Ros"
  - subject: "Thanks"
    date: 2007-03-16
    body: "As always, it's nice to learn more about someone whose name has been assocaited with KDE but you're not sure where the main involvements are.\n\nSo, thanks Pino. Looking forward to okular."
    author: "Simone"
  - subject: "Thanks"
    date: 2007-03-16
    body: "Just want to drop a big thank-you for Okular. I love to see it improve, especially because it's predecessor (KPDF) was such a refreshing experience.\n\n"
    author: "Diederik van der Boor"
  - subject: "small question"
    date: 2007-03-16
    body: "maybe a dumb question, but what's the utility on the right side of his desktop? the one with CPU, Memory, swap, ...?"
    author: ".."
  - subject: "Re: small question"
    date: 2007-03-16
    body: "That's my gkrellm, monitoring CPU, processes, global disks accesses, network and memory."
    author: "Pino Toscano"
  - subject: "Thanks Pino"
    date: 2007-03-16
    body: "We on #kalzium and #kdeedu all know how helpful and knowledgeable Pino is! Sometimes I've used him as a better version of \"Alt+F2 qt:\" ;)"
    author: "Benoit Jacob"
  - subject: "Re: Thanks Pino"
    date: 2007-03-20
    body: "Pino has been one of the most helpful guys on IRC for sure. Thanks Pino."
    author: "kunal"
  - subject: "Thanks"
    date: 2007-03-18
    body: "grazie Pino for helping in KDE! and Linux !!!!!"
    author: "Shelton D'Cruz"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we travel back to Europe, visiting Italy to meet someone who wants to enable you to read your documents and help you to do the math, who's "fiery" passion for KDE may be linked to his location - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/pino/">Pino Toscano</a>.
<!--break-->
