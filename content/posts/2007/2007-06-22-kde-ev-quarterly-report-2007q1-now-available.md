---
title: "KDE e.V. Quarterly Report 2007Q1 Now Available"
date:    2007-06-22
authors:
  - "dallen"
slug:    kde-ev-quarterly-report-2007q1-now-available
comments:
  - subject: "Thanks to Danny"
    date: 2007-06-23
    body: "for preparing it and thereby making the KDE e.V. a more transparent organisation.\n\n"
    author: "Sebastian K\u00fcgler"
  - subject: "What does the e.V stand for"
    date: 2007-06-23
    body: "Looked the website over, but still could not find a meaning of e. V. Perhaps I did not look close enough\n\n "
    author: "David"
  - subject: "Re: What does the e.V stand for"
    date: 2007-06-23
    body: "I think it means eingetragener Verein (http://de.wikipedia.org/wiki/Eingetragener_Verein)"
    author: "Marijn Kruisselbrink"
  - subject: "Re: What does the e.V stand for"
    date: 2007-06-23
    body: "It is German and means \"registered association\"."
    author: "stefanm"
  - subject: "correction"
    date: 2007-06-23
    body: "its Computer and Information Sciences :P"
    author: "Kenny Duffus"
---
The <a href="http://ev.kde.org/reports/ev-quarterly-2007Q1.pdf">KDE e.V. Quarterly Report</a> is now available for Q1 2007, covering January, February and March 2007. Topics covered include the <a href="http://dot.kde.org/1170027049/">KDE PIM Meeting at Osnabrück</a> in January 2007, progress on the Copyright Assignment (Fiduciary Licence Agreement) and reports from the Marketing Working Group, Human Computer Interaction Working Group, and Sysadmin Team. All long term KDE contributors are welcome to <a href="http://ev.kde.org/getinvolved/members.php">join KDE e.V.</a> 

<!--break-->
