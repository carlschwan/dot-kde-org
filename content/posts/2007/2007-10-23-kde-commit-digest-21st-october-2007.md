---
title: "KDE Commit-Digest for 21st October 2007"
date:    2007-10-23
authors:
  - "dallen"
slug:    kde-commit-digest-21st-october-2007
comments:
  - subject: "Kate Menus"
    date: 2007-10-23
    body: "As more languages are added (a good thing tm), maybe it's time to Kate find a better way to allow the user to change the syntax highlight.\nNow it's a very big menu with some types of languages and in each type a new menu with a big list of options. I dislyke the usability of menus with a big number of items, bu K menu is reduced to a minimum in Ubuntu as I add more sub-menus (for example internet->browsers or graphics->editors).\n\nMaybe using a new window with list fields for selection is a good idea (I don't recall how it's called in Qt, but in html whould be a select field with rows enought to address all types and items).\n\nJust a idea."
    author: "Iuri Fiedoruk"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: "Why, what's the point? \n\nHow often does the user need to change the syntax highlight(it's decided by filetype in the waste majority of cases anyway)? \n\nWhat would the *actual* impact usability wise be, to not use todays menu with many entries(other than your dislike for such menus)?\n\nMy guess the answer on those questions would be very seldom and minimal, supporting my \"what's the point\" rather nicely. Any so called usability improvement should at least be undertaken on places where it actually matters, not where they are not only baseless but irrelevant. "
    author: "Morty"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: "Well, for example, in quanta (I don't recall if it's the same for kate), after creating a new php file, you have to close and reopen it or change the highlight. \n\nYes, it's not often used I do agree, but you see, you should read well the text and see that a SUGGESTION isn't the same as demanding a new feature. Anyway, in case you didn't understood the point is to avoid big menus, once they are bad (and don't forget sompe people still uses 800x600 resolution)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: "well, file a bug for quanta"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: Kate Menus"
    date: 2007-10-24
    body: "I quite offten have to change the syntax highlighting as I use kate at work and don't have control over the file extensions of the files I edit. I quite often have to open XML files that don't have a .xml extension and then I need to change to the XML highlighting."
    author: "JP"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: "And an incremental search bar to find your language without having to decide whether it is best classified as scientific, markup, scripts, or sources :)"
    author: "Leo S"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: "Let's be cracy, let's add a searchbar to *every* menu at the toplevel. So any application can gain a usability-boost by default. Or probaly, it would be more wise, to use a separat windows to search through every menu, which make into something like kickoff or raptor, but for application-internal commands. On the long run, this gives the chance, to integrate something like a commandline, per window ;)"
    author: "Chaoswind"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: "While perhaps you meant to be sarcastic, I think this is an excellent idea, for which I provided a mockup some months ago on kde-look: \nhttp://kde-look.org/content/show.php/TBC+-+Searchable+menus?content=53580\n\nA complex application like Kate has over 80 first level menu items.  Something like KDevelop has probably even more.  Finding something by scanning through each of these is insane, and for less commonly used options, you can't remember which menu it is in."
    author: "Leo S"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: "The only problem I see with that suggestion is that the search bar takes up space from the toolbar where I might want icons for my favorite actions.  Would it be possible for the search bar to be half size when not in use, and have the search bar expand when you type in it?"
    author: "T. J. Brumfield"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: ">> The only problem I see with that suggestion is that the search bar takes up space from the toolbar\n\nThe search bar is not on the toolbar, it is on the menu bar.  Also, did you look at the third screenshot?  The search bar is not visible (it's just an icon) until you activate it."
    author: "Leo S"
  - subject: "Re: Kate Menus"
    date: 2007-10-24
    body: "There should'nt be a separate menubar."
    author: "Chaoswind"
  - subject: "Re: Kate Menus"
    date: 2007-10-24
    body: "But there is :)  And that won't change until someone proposes a new interface concept (the Ribbon still has the equivalent of a menu bar, it is just a \"tab\" bar instead)."
    author: "Leo S"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: "> While perhaps you meant to be sarcastic\n\nWell, partly ;)\nIt's not easy to implement such a feature in a good manner, and i thing it won't be a service, to add half eaten features to only a handful apps. There to many of them in kde already ;)\n\nIMHO it would be better to overthink the whole concept of menu and toolbar as an interface, an develop something more like microsofts ribbons from office 2007."
    author: "Chaoswind"
  - subject: "Re: Kate Menus"
    date: 2007-10-23
    body: ">> It's not easy to implement such a feature in a good manner\n\nI can't think of any reason why not.  It's just simple filtering on all the QActions that the menu knows about.\n\n>> IMHO it would be better to overthink the whole concept of menu and toolbar as an interface, an develop something more like microsofts ribbons from office 2007.\n\nMicrosoft's ribbon does nothing to solve the problem of \"I know what I want, but I don't know where it is\".  Yes, the organization of actions in their menu has improved over previous implementations, but the general UI organization of \"actions within groups\" is the same, and you need to still look through each group to find what you want if it isn't already visible or obvious where it might be."
    author: "Leo S"
  - subject: "Re: Kate Menus"
    date: 2007-10-24
    body: ">> It's not easy to implement such a feature in a good manner\n> I can't think of any reason why not. It's just simple filtering on all the\n> QActions that the menu knows about.\n\nSo? What about dynamic-generated menus or contextmenus for example? Or without a menuentry, but a hotkey or toolbar-button? There are to much holes to fill, for a simple scanning-solution. But well, ok, at kde there is a tendecy for such half eaten thinking :-/\n\n> Microsoft's ribbon does nothing to solve the problem of \"I know what I want, \n> but I don't know where it is\".\n\nBecause, that's not there question. The great think about ribbons, is the scripting-ability and that they get rid off the overaged separation between menu and toolbar (or you could say: small icons and big icons). Of course, there ist more then enough space for development, after all it's from microsoft ;)"
    author: "Chaoswind"
  - subject: "Re: Kate Menus"
    date: 2007-10-24
    body: ">> What about dynamic-generated menus or contextmenus for example?\n\nDepends on what kind of dynamic generation you're talking about.  There is no problem with dynamically generated menus if they are generated with respect to content.  If the menus are generated when the user clicks on it, then that could be a problem.  It really depends on how extensively this is used.  For menus such as a list of bookmarks, it is not important, since we don't need to necessarily search those anyway (would just clutter results).  \n\n>> Or without a menuentry, but a hotkey or toolbar-button?\n\nIf you have an action in the toolbar that's not in your menu, your application is fundamentally broken.\n\n>> There are to much holes to fill, for a simple scanning-solution. But well, ok, at kde there is a tendecy for such half eaten thinking :-/\n\nOf course everything is not trivial, but technical challenges can be overcome.  Actually Micorosft had an addon to the Ribbon that provided search capability at one point.  http://www.istartedsomething.com/20070124/scout-office-2007/\n\n>> Because, that's not there question.\n\nExactly. That's why the Ribbon is flawed from that perspective.  The ribbon is great as a menu replacement (although it has its own set of problems), but it doesn't make any fundamental leaps towards providing a better way to find things in an application.  Anyway, a search doesn't impede your ability to access functions the same way you always did.  It's merely a supplement.  "
    author: "Leo S"
  - subject: "Re: Kate Menus"
    date: 2007-10-24
    body: "This looks very similar to \"Quick Access\" in Eclipse, which is very useful. It lets you find not only menu items but settings and other things as well."
    author: "Henrik"
  - subject: "Re: Kate Menus"
    date: 2007-10-24
    body: "Colour selection for Web colors or at least a possibility to display the associated colour would be great"
    author: "benja"
  - subject: "KHTML future?"
    date: 2007-10-23
    body: "First, kudos to all khtml dev's, you already did a big job.\n\nI should say that im really happy to see khtml in development and I personally, do not like to see webkit as rendering engine for kde,  why use forked khtml while we have khtml in active development?\n\nbut i really want to know that what are the plans for khtml's future.anything that dev's are planning to support and are already working on it? for example wysiwyg, better js debugging output, supporting html5 and/or css3 or...\n\nThanks"
    author: "Emil Sedgh"
  - subject: "Re: KHTML future?"
    date: 2007-10-23
    body: "All of those are on the table. There is a foundation for a better JS debugger in trunk, but it's not clear we'll have enough time to debug it before 4.0.\n"
    author: "SadEagle"
  - subject: "Re: KHTML future?"
    date: 2007-10-23
    body: "all of them? even wysiwyg? Oh My...\nYou guys Rock!\n\nand sure im not talking about 4.0, its almost clear that what we have in 4.0...\n\n(Today was the release date in last schedule, we all waited for today for a long time)\n"
    author: "Emil Sedgh"
  - subject: "Re: KHTML future?"
    date: 2007-10-23
    body: "WRT to editting we've discussed it a bit; the infrastructure is there, \nbut obviously right now we have to focus on bugfixing. I can't commit or \nanything to it, but it seems doable w/a reasonable amount of effort. "
    author: "SadEagle"
  - subject: "Re: KHTML future?"
    date: 2007-10-23
    body: "If I had to make a bet, I think we'll see Webkit in Qt becoming KDE's renderer. Trolltech are putting some resources into it, and it's sensible to see it in the common toolkit."
    author: "Segedunum"
  - subject: "Re: KHTML future?"
    date: 2007-10-24
    body: "Not trying to bug anyone, but it sounds to me like it just makes sense to move to webkit when it is ready. \nAssuming kde developers have commit access, it would allow many communities to share the load of webengine development. Apple, Nokia, Gnome, Trolltech, and KDE can all use and develop the webkit engine. Every group can take advantage of the shared core of development and develop a common project. Not to mention (as broken of an idea that it is) websites that look for the browser string to change the content it displays would work much better with a minorly sizable marketshare of webkit, compared to the unknown khtml. \n\nLast week there were comments about not wanting to be unpaid apple employees, but currently for khtml everyone is in effect unpaid KDE employees. While cooler, it does not actually change the goal, to provide what is best for users (the best possible html rendering engine). While it might be decided that khtml is the best way to achieve that goal, I think it makes sense to at least look into migrating to webkit as an alternative. With Trolltech putting resources into integrating webkit into qt, it makes even more sense. \n\nSo kudos for improving khtml but please don't discount using webkit just because the apple folks were jerks early on. It sounds like some other organizations have managed to move past that and also the NIH'ed'ness."
    author: "Christian"
  - subject: "Re: KHTML future?"
    date: 2007-10-24
    body: "Go SadEagle Go! I love your work in KHTML ;) and also kudos to other KHTML devs."
    author: "eds"
  - subject: "KHTML"
    date: 2007-10-23
    body: "Ok, I know some people love khtml, but I think keeping it is a bad thing in the long run (that dosen't mean keeping it for 4.0 is bad, but we should think hard about webkit).\nA comunity of developers is being built around webkit, with major players as Apple and Nokia. Staying out without a good motive (trowing away code is sad, but IMHO not a very good reason if this result in a better thing in the end) seems to me a bad idea, because merging code from webkit isn't easy.\n\nNotice I stated this is my personal opinion and I really don't know all the details on policies fro access to webkit CVS and acceptance terms for patches, so I could be very wrong, so please, don't start a flame war here, that not my intention, I just want to discuss it as adults (not saying kids can't have fun either, heeheh)."
    author: "Iuri Fiedoruk"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "so let me ask:\nkhtml is KDE's HTML Rendering Engine that is under very nice development, then why should kde switch to webkit? \n\nI think its all because of the names of 'Apple', 'Nokia' and 'Adobe', if these were three smaller companies, people were not going to look for webkit in kde...\n\nif you want something that webkit has, and khtml hasnt, go to bugs.kde.org, open a new bug and tell you want it, then vote for it\n\nfor example: i want opacity support! ;)"
    author: "Emil Sedgh"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "Well two reasons I personally would use:\n- plataform independance. Good even for mobiles, khtml is KDE-only\n- safari is better than konqueror in html rendering in my ver humble opinion\n- one thing I really dislike about open-source projects is the duplication of effort. For example: why can't ubuntu use Suse's Yast? Even more than now it have a gtk front-end."
    author: "Iuri Fiedoruk"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "The world would be a dull place if we all became the same don't you think?  Besides, if you can tear yourself away from 'buntu, Oracle's RH clone uses Yast I believe."
    author: "no thanks"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "--i cannot undesrtand, use webkit in kde because webkit is cross-platform? when you have kde it means that khtml is there with you\n\n--so fill a bug report if you see a bug, but i cannot confirm this, also for example khtml has support for css3 selectors, i mean it is under active development...\n\n--duplication is what apple did, they forked khtml because of their policy, why they just didnt commit to khtml?"
    author: "Emil Sedgh"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "They did much more than just fork. They expanded the entire platform for their needs and more.\n\nWhy didn't the GTK team just add to khtml without moving to WebKit?\n\nMe thinks having broad inroads into potential clients and ability to also move GTK+ to OS X was another consideration why it made sense to expand GTK+ to OS X."
    author: "Marc J. Driftmeyer"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Probably because they would rewrite whole damn thing rather than use anything with name starting with 'K'."
    author: "JS"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "Platform independency is a good thing for spreading the renderer to other targets, such as handheld devices/cell phones (which is one major factor in the interested in Apple's fork). It's not so hot when it comes to running well on a specific platform. For instance, handling images on X11 requires /a lot/ of care, to avoid alpha-blending if not needed, to cache things on the server, to cache scaled versions, to pretile certain images, etc. KHTML has collected quite a collection of  optimizations of the sort.\n\"Platform-independent\" code for such areas will likely run quite well on OS X, and quite poorly on X11. And we know where most of our users are.\n\n(And, actually, KHTML has ran on handhelds well before apple's fork ever did, thanks to Konqueror/Embedded)\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "So, the problems reside with Qt and X11: especially the design tradeoffs for the X11 client/server model versus OS X WindowServer and Quartz?\n\nFrom my checkout of WebKit trunk on OS X, Gtk and Qt seem to be growing steadily.\n\nWhat's stopping all your optimizations and interface abstractions from being under their own branch in WebKit, besides not having commit access?"
    author: "Marc J. Driftmeyer"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "It looks like that the \"the official KDE web rendering team\" has an other opinion about that:\n\nhttp://zrusin.blogspot.com/2007/10/khtml-future.html\n\n"
    author: "kiwiki4e"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Thanks for posting that.\nSome good points, and help my belief that khtml will die for good - thanks for while it lasted, but I prefer firefox to it - and be replaced by webkit."
    author: "Iuri Fiedoruk"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "khtml does not need to die for kde apps or the kde workspace to use webkit. khtml can happily continue to develop even if kde by and large ends up using webkit, or if OSVs choose webkit for final integration.\n\nthe whole \"khtml must die or webkit must be shunned\" is a false dichotomy. khtml can continue on as is regardless of what the rest of KDE does."
    author: "Aaron J. Seigo"
  - subject: "Re: KHTML"
    date: 2007-10-27
    body: "actually while i think safari or gecko engine etc... is a lot better than KHTML\ni STILL think it is VERY good, in fact very IMPORTANT, that KHTML continues"
    author: "she"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "I'm against Webkit, because it doesn't support proper text-shadows.(with which you can do very nice glow- and metal-effects)\nFor a comparison view my website in Konqueror and Safari: www.iamwhoiam.net/max\n(and don't bother with the text, it's just a placeholder)\n\nAnd I'm not sure about that, but I think I read that Webkit is much bigger than khtml.\n\nbtw: I don't know for what you need opacity, but for the background you could simply use a transparent png."
    author: "maxjen"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "as i said im against it too, opacity was just an example, its already there for khtml in kde4.0...\n\n"
    author: "Emil Sedgh"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "Oh, right.:)"
    author: "maxjen"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "so in a choice between \"canvas/wysywig editting/web 2.0 site access (think: facebook)/works-on-QGraphicsView\" and text shadows ......  you pick text shadows?\n"
    author: "Aaron J. Seigo"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Uhm. We have canvas. And facebook works. And editing can be done... but I am 100% sure Apple will always have nice text shadows.\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Not here. e.g. you can set more than one text-shadow like this:\ntext-shadow:0px 0px 7px #ffaa00, 0px 0px 3px #ffffff;\nbut Safari only shows the first text-shadow."
    author: "maxjen"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "> canvas\n\nthat's right, i saw those commits recently ... does it have full, properly working support for the entire kit and kaboodle? (serious question there: I haven't had a chance to try it out yet)\n\n> facebook works\n\nit does work *much* better in the latest 3.5.x than earlier releases, but it's still not perfect. it's like maps.google.com; it works *pretty* good, but not perfectly (most common buggaboo on google maps is how sometimes certain map squares won't fill in... permanently). i haven't tried these sites out very heavily with khtml in kde4 as the khtml there has only recently become usable for me (porting breakage sucks, i know =), but it's that difference between \"works .. mostly\" and \"works, absolutely.\" that drives KDE users to alternatives like firefox. i experience this _constantly_ among the userbase and it's really unfortunate.\n\nthe things i listed are the sorts of things people actually notice and care about, as opposed to text shadows ... which was the point of my comment =) if we're going to get all hyper-aligned over things, let's at least do it over sane reasons.\n\n> editing can be done\n\ncan be, yes. but then everything can =)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "As for full support of canvas... We do not do all HTML5 stuff, but then the stuff we don't support Safari doesn't either, IIRC (getImageData/putImageData). And the reason I didn't put those things in is because I think they're insane. Which means I'll probably have to implement them anyway, but I'll at least postpone the insanity.  (There is also the data: URL conversion, which is of even more dubious value and requires some   security paranoia).  A few problems we have are limitations in Qt, such as lack of 2-radius radial gradients, and setting of repetition on pixmap/pattern brushes. It also doesn't help that path intersection in Qt sucks. Which I should file a bug report about (see below ;-) )\n\nAnd as for Facebook.. Well, you know what I say about bug reports? The only problem I know of with FaceBook is that it's basically a DoS on our selector code; and yes, the partitioning by classname code in Apple's tree does handle it a lot better.\n\nUnfortunately, my preferred solution of beating up web designers who think 3000 clause CSS is good is not within bounds of the law.\n\n\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Uhm. We have canvas. And facebook works. And editing can be done... but I am 100% sure Apple will always have nicer text shadows.\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Well.. maybe Webkit is the better solution, but from my point of view it isn't, because khtml worked perfectly fine for me, and webkit works perfectly fine, except for the text-shadow. And I think (maybe I'm wrong) that using Webkit would mean giving up control. e.g. could you ever change the license without Apple's permission?"
    author: "maxjen"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "We can't change the license w/o Apple's permission anyway, since we have merged many things. And one really can't change the license of anything in KDE with any sort of history without asking hundreds of people. This is really a non-issue.\n\n\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "ok, I was wrong then. So it's just the small personal text-shadow-thing I dislike about Webkit. :)\nThe only thing that bothers me is that Apple seems to have more rights on the code than the people who actualy started it."
    author: "maxjen"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "How much work would it be to provide missing features to webkit as opposed to duplicating the efforts of others into khtml? Filling out bugs on bugs.kde.org is cool 'n all, but why should khtml remain when there is the more actively developed webkit? Seems like it would take more work to duplicate webkit features in khtml then to fix webkit and bring it up to the level of existing khtml's features and run with it from there. Then everyone would get the benefits. "
    author: "Christian"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "If my memory serves me right, it'd require going through ~6000 or so different commits, figuring out how they relate, and disentangling things, which is made much harder by some pointless rearrangements Apple made. That's not practical with our manpower[1]; it will likely be much easier to get 90% of the useful things that the fork does with 10% of effort by being smart about it. After all, it's always been about doing more with less, in KHTML and all of KDE.\n\nYou see, having more manpower does not mean you're more productive, since it also means you can throw manpower at a problem, or even invent a problem to throw the manpower at (OK, so there is Google doing that for people now). I would never spend 1 hour thinking of a change and then 50 implementing it and 20 more fixing it up. I would rather spend 5 hours thinking (in chunks, like say when walking someplace or taking a shower), and then 5 implementing it. It also means we don't have much noise, commits for some abstract sense of beauty[2], etc. To give an example, note that we do work closely on the JavaScript engine, but in our own repositories. And when I merge stuff perodically, I find that some ultra-great changes, which were often discussed with us, are mixed in with tons of commits which basically do nothing. \n\n------\n[1] It might be easier to do by making the other tree run with our regression tester, but that's quite tricky, since the baselines are finicky, and that would require someone from the other end to put in the bulk of the effort. \nAnd now, perhaps I am being silly, but I would hope some of those well-wishers claiming to be involved in KHTML development would have done some of this already. I would certainly help with some of the black magic in tr's fontoverload code.\n\nGeorge Staikos did something like this for JavaScript. That was a much easier case, and it worked. But it also didn't work out quite as he expected, since we still kept our own tree, and for good reason. Unfortunately, most people just talk.\n\nDoing this sort of integration would indeed get us very close to the ideal solution; leaving some social problems --- and there may be technical solutions to the social problems. \n\nOf course, right now they are solving the social problems by reminding us that \nApple folks are all pretty nice, just with different goals and design philosophies (even if those philosophies annoy the heck out of me sometimes, but heck, they're doing it less than some KDEers lately --- see footnote #2), \nand these nice folks would never do something like publish a misleading article forcing a rectraction via a FAQ, unlike people who think they're doing you a favor.\n\n[2] Coincidentally, one such commit in kde libraries broke some form submissions pretty badly in trunk, and many more have broken things greatly before.\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "No matter how good KHTML is, it has and will have very few users. That means webdevelopers won't test on it, so it won't be usable on many sites. WebKit, on the other hand IS tested with many sites. Joining forces with them allows KDE to take advantage of that, while it keeps the possibility of adding whatever we like. Imho, that's the way to go."
    author: "jospoortvliet"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "Can you trust apple? History says you can't. In fact, if their market position allowed it they would be wayyyy worse than microsoft. It is in their culture not to be open.\n\nIf someday the decide that their CVS should not be accessible anymore, what of the Free/Open devs?\n\nAs a KDE user, I want KHTML to be tailored to KDE's needs, not to have webkit retrofited... In the end, it is all about having control of your creation. And I believe the KHTML developers have every right and motive to keep working on their project. And you should support them, because it is because of people like those that you get KDE, not because of big companies.\n\nAnd on a philosophical note, what message does this would send: \"A Free project is useful/competition? Why compete? fork it and kill the original! People will praise you for it!\".\n\nObviously, you don't really want Free software, just free... And if possible provided by a company with a bad ego problem."
    author: "hmmm"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "<em>\"If someday the decide that their CVS should not be accessible anymore, what of the Free/Open devs?\"</em>\nThey would do what the rest would do: if Apple closes the CVS again and only delivers code blocks as patches the \"rest\", meaning Nokia, Adobe, Gnome and the KDE project, would set up a new, fully open place to continue development.\n\nWebKit is much more these days than just Apple."
    author: "liquidat"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "That's not so simple. Development is not a bunch of random people, it has a community and a structure. Right now, the development in Apple's fork is very much centered around their employees (and their needs), partly because the project culture is very inefficient and not well-suitable for part-time contributors. If they were to stop it, the project might not survive, since the interests of the outside contributors may not be sufficient to sustain development. And don't expect KHTML contributors to step in and take things over should that happen, since there might not be any involved, especially if this stuff is forced through the back door, over the development team's objection --- heck, the FUD designed to do that has already discouraged some of our contributors from working in our tree, never mind working in Apple's interests.\n\n(Not that I expect Apple to close development --- they gets tons of free labor right now, w/o giving up much of control).\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "Indeed. Even if KDE would switch to webkit (or offer it as an option - eek), there would still be this big cultural difference which drives khtml and webkit appart, a little bit more every day. The focus and needs are not the same. In the long run, KDE would be as unhappy with webkit as Apple would be with khtml.\n\nTechnically, neither khtml nor webkit is a clear winner (AFAICS). The best we can hope for is improved cross-polinisation between the two projects (patches, testcases...).\n\n\nDisclaimer : I'm not a kde/apple dev; my view is a very subjective one."
    author: "Moltonel"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "In the end, the ball is really in apple's court. If they start playing nice and do all the things that a good participant in an open source project should do then it would reduce the arguments against using webcore (and gain apple all the benefits of free software participation) but pretty clearly, that is not high on apple's agenda. As mentioned in the article, apple are more about closed door development followed by a flashy reveal by Jobs than they are in an open, reciprocal development process."
    author: "Borker"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "... and you are involved in the webkit development process how exactly? i ask because you're making some remarks that have the ring of being informed, and yet it goes against what i've seen."
    author: "Aaron J. Seigo"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "KHTML vs Webkit is far more simple in the ideal solution.\n\nIn an ideal world, open source means you take the best code and try to merge the best aspects of both.  Since the fork, both projects have made innovations and improvements.  Merging it isn't necessarily simple, but ideally it is the best solution."
    author: "T. J. Brumfield"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "I think it's really strange how some people defend khtml against webkit.\nSome of them because of it being contamined by companies like apple I can understand, but other not. They say to not trash code, they say khtml have better things than webkit, but let's take a look:\n\nKDE 4.0 is replacing kicker for a mix of a plasmoid and kickoff. At start it will have much less features than kicker. \nKDE 4.0 is replacing konqueror for dolphin in file management, and most people that used it I heard says it's much better than konqueror.\n\nI don't see a so hard defense of those two softwares as I see in khtml/webkit case. You know, khtml could still be in as optional, same as the case of konqueror as file manager."
    author: "Iuri Fiedoruk"
  - subject: "GoogleReader bug with Issue81"
    date: 2007-10-23
    body: "Issue 81 (2007-10-21) is not clickable in Google Reader. \n\n\"Issue 81 (2007-10-21)  <- THIS SHOULD BE BLUE AND HYPERLINKED, IT ISN'T\nfrom KDE Commit-Digest\nFortune-teller and Keyboard Layout applets for Plasma, KNewsTicker resurrected for KDE 4.0 as a Plasmoid. Rewrite of tag support in KHTML. Various new language syntax highlighting in Kate. Internal database storage work in Digikam. More playlist handling work, and support for Magnatune \"streaming membership\" in Amarok 2. OpenDocument loading of charts in KChart for KOffice 2. Various graphics fixes and a user handbook for the Bovo game. Kolourpaint is now fully ported to Qt4. Continued work on the Eigen 2 library. Further porting away from KDEPrint to the printing facilities provided by Qt4.<br><br><br>Discuss at <a href=\"http://dot.kde.org/1193149851/\">http://dot.kde.org/1193149851/</a> http://commit-digest.org/issues/2007-10-21/\"   <- THIS HREF IS ALSO WEIRD\n\ngreetings"
    author: "Dolphin-fanatic :)"
  - subject: "KHTML"
    date: 2007-10-23
    body: "Wow - at least a decision has been made.  I don't know though, it seems khtml is always playing catchup.  I still have to open firefox all the time for many different sites.  I continue to use khtml because of my love for konqueror, but it would be nice to be on the radar of web developers for a change and not have to wait for the next version to fix [x] problem.\n\n\"Apart from dynamic scripting there is Java, Flash, cookies, URI shortcuts, password managers, security and networking support\"\n\nI can see how dynamic scripting, java and flash could be a problem, but the rest should not be a concern for an HTML viewer.  If the are, then the code is not properly modularized.\n\nThat quoted paragraph makes this decision seem like an emotional rationalization.  Yes, KHTML is our baby - but we need to let it go now.  If it's legacy ends up having been the core of WebKit - that's not a bad legacy.  Please don't make the user community suffer more html compatibility issues just for the sake of not being able to accept that your child has grown up.\n"
    author: "Tim"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "In this case, its not really about modularity. Its mostly a question of: do you want to stick to internationally accepted and well defined w3c standards, or do you want to imitate internet explorer's functionality as closely as possible*? You have to be willing to answer \"2\" to be able to render a lot of websites \"correctly.\"\n\n*ick."
    author: "Level 1"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "Add \"mozilla's functionality\" to #2 as well. These days using non-standard Mozilla stuff is more in style.\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "/me __HATES__ browser compatibility issues, specialy IE problems...\n\nhearing that mozilla is going to be like IE and include non-standard things (like -moz-foo) is so bad"
    author: "Emil Sedgh"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "Er... AFAIK, using \"-foo-new_property\", where \"foo\" is the name of the browser or rendering engine, is not only fine, but the way recommended by the W3C for adding plattform specific properties. Do a grep for \"-khtml-\" in kdelibs, and be surprised. :)\n\n\nBrowser compatibility issues, is all browsers understanding \"width\" as \"width of the content\", and MSIE understanding \"width\" as \"width of the content, plus the padding, plus the borders\". This is completely different.\n\n"
    author: "suy"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "i know that even -khtml-opacity was there too, but why now that w3 is recommending 'opacity', mozilla uses '-moz-opacity', ie that sucks and im even not going to talk about it...\n\ndifferent function and tag names is an issue too"
    author: "Emil Sedgh"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "I actually wasn't referring to that. Most -moz- properties are just prerelease CSS3 stuff (and most -khtml- ones are prelease CSS3 stuff, or for internal use), and people /usually/ do not mess this stuff up.\n\nBut take for example the gmail complaints below. For a while, gmail stopped working for us when spoofed as Firefox. Why? Because there is this non-standard thing MSIE invented as innerHTML, which everyone emulates. We implemented it as close to IE as possible, but... Mozilla decided to generalize it. And the gmail relied on it. In this case, the difference was trivial, but in all honesty, if MS was doing it, people would be crying \"embrace and extend\".\n\nA better examples are those things like HTMLDocumentElement.prototype. I had people asking me why we were lacking this \"DOM\" feature. Well, it's because it's not part of any DOM spec, and just happens to be the way Firefox worked. \nAKA nearly the exact equivalent of  window[\"[[HTMLDocumentElement.prototype]]\"] in KHTML, just less well-hidden. Of course, because people started using it, now everyone has to implement it -- Opera, Safari, us. Accidental? Fair enough. Extra work for us for non-spec'd behavior either way.\n\nMozilla folks also feel like they own the JS language. Which, given how ES4 seems to follow the JS2.0 inanity, may be they do. But, well, they added the getter/setter feature that's not part of ECMA 262-3 spec, and guess what? \nOther browsers have to implement that. \n\nP.S. Extra bonus (bogus?) points go to whoever named non-standard events like DOMContentLoaded."
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "It is unfortunate that a lot of developers seem to think that works in gecko == standards compliant. Nowadays I tend to test in Konqueror first of all.\n\nHaving said that, I would like to see something like DOMContentLoaded in the standards and in KHTML"
    author: "Simon"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "ACK for the first point. What works in Konqueror seems to work in all other browsers (except IE, of course; but these are mostly CSS issues). Perhaps I should rethink implementing AJAX features in my web application. Because thinks are working so good at the moment without JavaScript."
    author: "Stefan"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Konqueror really needs webkit to be taken seriously at all."
    author: "B"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "You'll continue to have to open firefox, unless you file bug reports.\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Well, I have to open Firefox for wellsfargo.com, since Konqueror almost freezes there. I filed a bug two and a half months ago:\nhttps://bugs.kde.org/show_bug.cgi?id=148528\n\nNo activity there at all.\n"
    author: "Dima"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "Well played, well played. Will try to take a look during the weekend.\n"
    author: "SadEagle"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "BTW, am I the only one unable to use Konqueror correctly with EBay? Neither can I upload pictures nor properly use the popup menus in \"My Ebay\" (they pop up as blank white rectangles without text).\n\nThis is so for quite some releases, I think there was even a bug report. I was just wondering as EBay isn't the most unusal site on the internet."
    author: "Peter Thompson"
  - subject: "Re: KHTML"
    date: 2007-11-04
    body: "  Mine show up as gray blank boxes. Actually I can see them for a split second before they go blank."
    author: "Ed"
  - subject: "Re: KHTML"
    date: 2007-10-24
    body: "So posting bug URLs in forums actually works :)\n\nAnyway, I realized that KDE 3.5.8 improved things (as I commented in the bug), so it's usable now."
    author: "Dima"
  - subject: "Re: KHTML"
    date: 2007-10-23
    body: "My support goes to the KHTML developers and I agree with their point of view.\n\nIf there's some issue with compatibility maybe adding an option to switch the rendering engine to WebKit (or even Gecko - what happened to Kecko?) could be made."
    author: "AC"
  - subject: "KHTML in kdelibs"
    date: 2007-10-23
    body: "Ok,\n\nSo, some people think KDE should go with WebKit (which might be part of a future Qt 4 release) while others think KDE should develop  KHTML. (I'm pretty sure where this is going: KDE will support both, and pass the choice on to users).\n\nHowever, I have to ask: is it smart to release KHTML as part of kdelibs-4.0.tar.bz2? \n\nEven *IF* WebKit turns out to be the prefered solution KDE will still have to support KHTML with security updates until the end of the 4.0 cycle. It just doesn't seem smart to commit to that at this point. (remember, this is an HTML rendering library. There are security issues there that are going to have to be fixed).\n\nMaybe by 4.2 KHTML is viewed as the clear winner; in that case, it could be added to kdelibs. Until that happens, I think it's in KDE's and KHTML's best interest to have KHTML live in its own package and be a dependency of KDE 4.0.\n"
    author: "Reply"
  - subject: "Re: KHTML in kdelibs"
    date: 2007-10-23
    body: "KHTML is a library which many applications rely on to operate.\n"
    author: "SadEagle"
  - subject: "Re: KHTML in kdelibs"
    date: 2007-10-24
    body: "Could you expand on this.  Do packages really rely on khtml or do they rely on the registered html viewer."
    author: "Tim"
  - subject: "Re: KHTML in kdelibs"
    date: 2007-10-24
    body: "They rely on the DOM APIs.\n"
    author: "SadEagle"
  - subject: "Re: KHTML in kdelibs"
    date: 2007-10-24
    body: "Thats exactly the point, should they depend on khtml (which is going to fall further and further behind) or should they just go for the QtWebkit stuff which provides all the same functionality and more. Plus actually is a well known player on the market."
    author: "Reply"
  - subject: "Re: KHTML in kdelibs"
    date: 2007-10-23
    body: "I don't see your point, having khtml inside kdelibs or in its own tarball doesn't make any difference maintenance-wise. Actually, separating the libs would needlessly put more burden on packagers."
    author: "Moltonel"
  - subject: "Re: KHTML in kdelibs"
    date: 2007-10-24
    body: "Nah, for 4.0, the decision is clear: KHTML stays where it is. It is not practical otherwise. We can reconsider for 4.1 and further (and we might do that)."
    author: "jospoortvliet"
  - subject: "Re: KHTML in kdelibs"
    date: 2007-10-25
    body: "Well, I'm already aware of that. Which is why I made the post.\n\nTo you and Moltonel:\n\nHaving KHTML inside the kdelibs-4.0 tarball means you have to support it through all KDE 4 releases, which is a guarantee KDE can't possibly make as far as I can see. You can't guarantee there will still be enough people intersted to give it the maintainence it requires.\n\nIf it moves to its own package that's not a problem: KDE only guarantees API and ABI stability for kdelibs."
    author: "Reply"
  - subject: "Web Developers test in Safari"
    date: 2007-10-23
    body: "Many Web Developers test their pages in several Browsers: IE6, IE7, Firefox and Safari. Some on Opera. But not too many have a linux machine just for testing in Konqueror.\nAnd: they don't get payed to make things work in konqueror!\n\nAlmost all simple pages work without any problem - but some of those ajax pages do not. (gmail is a perfect example)\n\nIf Konqueror would use WebKit (and identify as WebKit - so those browser-sniffers get something they know) many problems would be solved.\n\nniko\n\nPS: I'm a Web-Developer using Konqueror the whole day :D"
    author: "niko"
  - subject: "Re: Web Developers test in Safari"
    date: 2007-10-23
    body: "my Konqueror works very well on (to, in, with?) gmail "
    author: "Juhani Lyly"
  - subject: "Re: Web Developers test in Safari"
    date: 2007-10-24
    body: "I can identify with that. I'm also a web developer and the decision from our management is to support IE6, IE7, Firefox and Safari. My development environment is Gentoo + KDE + Zend Studio and my favorite browser (by far) is Konqueror.\n\nI make every effort to test and make things work in Konqueror but when we have tight deadlines and rushing to complete, I cannot afford the time to debug and figure out what's wrong (IE gives me enough problems on that front). Once it works in the required list of browsers, it's done.\n\nIn the end, I cannot deny there are days I wish Konqueror was using webkit because it could solve many problems for me (sorry, I'm being selfish here :/), and we could add Konqueror as a supported browser with little (none?) testing overheads for us.\n\nTim."
    author: "Timoth"
  - subject: "Re: Web Developers test in Safari"
    date: 2007-10-24
    body: "Konqueror should display valid HTML pages perfectly (as much as possible) and web developers should create valid HTML pages. So KDE should choose from KHTML and WebKit which more properly supports standards."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "works well"
    date: 2007-10-23
    body: "Konqueror works well in (?) gmail\n"
    author: "Juhani Lyly"
  - subject: "http://www.khtml.info/"
    date: 2007-10-23
    body: "it looks like the site has been bought by someone else http://www.khtml.info/"
    author: "Patcito"
  - subject: "Re: http://www.khtml.info/"
    date: 2007-10-23
    body: "that site was never very well maintained anyway"
    author: "Andreas"
  - subject: "Statistics?"
    date: 2007-10-24
    body: "Thanks for the weekly digest!\n\nI am missing the statistics section (e.g. number of bugs opened and closed), is there any other way to get that information?\n"
    author: "christoph"
  - subject: "Some suggestions for eye candy"
    date: 2007-10-24
    body: "Okay, I haven't been able to put KDE 4 on my machine yet because others need it for production use, but I have some ideas that would be great to see implemented:\n\n1) A different default color scheme for the Oxygen window decoration. Gray is certainly professional, but a light sky-blue could look even better, especially if it had a nice soft gradient. \n\n2) While it's a great idea to see the Plasma Applet Browser pop up when that wrench icon in the corner of Plasma is clicked, perhaps it could expand into a hybrid of \"Desktop Properties\" (e.g. to change wallpapers etc.) and Ubuntu's new \"Appearance Preferences\" applet (http://cybernetnews.com/wp-content/uploads/2007/09/ubuntu-7.10-compiz-fusion.jpg), with options to have \"no\", \"minimal\" and \"maximum\" 3D compositing effects. \"Applets Browser\" could be one tab of several in that dialog, all to do with appearance settings. I guess some people might worry this would bloat the dialog and could duplicate a similar one in System Settings, but the redesign of Konsole's options dialog proved that a well-organised settings dialog can have excellent usability despite a multitude of options, and need not have a spartan interface with very few.\n\n3) Looking at recent screenshots of Dolphin, I have no idea whether the actions panel available in the D3lphin version (e.g. Compress here etc. like at http://cybernetnews.com/wp-content/uploads/2007/10/dolphin-network.jpg) has gone to in the KDE 4 version. Is it simply hidden by default?\n\nI mean all the above comments/questions as an innocent noob, and hope no-one becomes offended by anything contained above. I'm really impressed with the way KDE 4 is going - it;s exciting to see all the pieces come into place! Keep it up everyone!"
    author: "Darryl Wheatley"
  - subject: "Re: Some suggestions for eye candy"
    date: 2007-10-24
    body: "Another thing, with the grippy things, can they be turned off like in The OS That Must Not Be Named? Perhaps the grippy things could be made to only appear when you hover over a toolbar that can be moved?"
    author: "Alan Ramm"
  - subject: "Re: Some suggestions for eye candy"
    date: 2007-10-24
    body: "I suppose you mean the grippy things in toolbars; yes they can be hidden by locking the toolbar (like you lock the Kicker)."
    author: "Hans"
  - subject: "Re: Some suggestions for eye candy"
    date: 2007-10-24
    body: "I see. Thanks very much! :)"
    author: "Alan Ramm"
  - subject: "Re: Some suggestions for eye candy"
    date: 2007-10-24
    body: "> when that wrench icon in the corner of Plasma is clicked,\n\nmouse over, not clicked. i'm trying to get rid of clicking as much as possible (though no more than is possible =)\n\n> expand into a hybrid of \"Desktop Properties\"\n\nit will provide access to various bits of configuration UI, yes. that's the whole point of it =)\n\n> \"Applets Browser\" could be one tab of several in that dialog\n\ni'm hoping to have multiple entries that expand out of the toolbox rather than have one massive dialog full of tabs in a bit to find a maximal breadth-vs-depth arrangement.\n\n> I have no idea whether the actions panel\n\nthe information panel is still there; AFAIK the actions are being reworked however. a generic class for them was just written up (by dfaure) and moved into libkonq recently."
    author: "Aaron J. Seigo"
  - subject: "Re: Some suggestions for eye candy"
    date: 2007-10-25
    body: "Thanks very much for the update Aaron! I enjoy your blog posts and your youtube videos. Just like everyone else, I'm really impressed with the progress - KDE4 may well become the gold standard against which all other desktop ecosystems are compared. For me at least anyway :)"
    author: "Darryl Wheatley"
  - subject: "KXKB"
    date: 2007-10-26
    body: "IIUC, this KCM is now a Plasma applet.\n\nIs this going to work with Xorg-7.3\n\nNote that it does not install a file:\n\n<datadir>/X11/xkb/rules/xorg"
    author: "James Richard Tyrer"
  - subject: "KStringBuffer"
    date: 2007-10-27
    body: "Can someone includes this in KDELibs4 ?\n\nhttp://kde-apps.org/content/show.php/QStringTemp+for+KDE+3.x+%28CVS%29?content=12191\n\nIt basically, allows you to write this:\n\nQString s(\"is\");\nKStringBuffer buffer;\nQString result = buffer + \"Th\" + s + ' ' + s + \" way \" + 't' + \"oo cool\";\n\nInstead of writing this, due to the Proxy String Design Pattern:\n\nresult += \"Th\";\nresult += s;\nresult += ' ';\nresult += s;\nresult += \" way \";\nresult += 't';\nresult += \"oo cool\";\n\nIf you LHS object is a QStringBuffer, then the compiler will know,\nthat you are concatening a \"set of\" strings instead of creating many temporary QString.\n\n#include \"qstring.h\"\nclass Q_EXPORT KStringBuffer : public QString {\npublic:\n  Q_EXPORT inline KStringBuffer() : QString() {\n    QString::reserve( 128 );\n  }\n\n  Q_EXPORT inline KStringBuffer(register size_t sz) : QString() {\n    QString::reserve( sz + 64 );\n  }\n\n  Q_EXPORT inline KStringBuffer& operator+( const QString &s2 ) {\n    QString::operator+=( s2 );\n    return *this;\n  }\n\n  Q_EXPORT inline KStringBuffer& operator+( const QLatin1String &s2 ) {\n    QString::operator+=( s2 );\n    return *this;\n  }\n\n  Q_EXPORT inline KStringBuffer& operator+( const QByteArray &s2 ) {\n    QString::operator+=( s2 );\n    return *this;\n  }\n\n  Q_EXPORT inline KStringBuffer& operator+( const char *s2 ) {\n    QString::operator+=( s2 );\n    return *this;\n  }\n\n  Q_EXPORT inline KStringBuffer& operator+( QChar c2 ) {\n    QString::operator+=( c2 );\n    return *this;\n  }\n\n  Q_EXPORT inline KStringBuffer& operator+( char c2 ) {\n    QString::operator+=( c2 );\n    return *this;\n  }\n};\n"
    author: "fprog"
  - subject: "Re: KStringBuffer"
    date: 2007-10-27
    body: "This is certainly not the right place to post a wish like this.\n\nkde-devel or kde-core-devel are surely more appropriate to suggest such an addition for KDE 4.1, but remember to have at least the names of two applications which use it.\n\nWhat exactly is the difference between using QString directly?\nI.e. why not\n\nQString s(\"is\");\nQString result = \"Th\" + s + ' ' + s + \" way \" + 't' + \"oo cool\";\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KStringBuffer"
    date: 2007-10-27
    body: "You just constructed and allocated memory for 6 temporary QString instead of appending them to result, one for each dummy operator+(lhs,rhs). \n\nWhile this creates one large enough KStringBuffer and appends to it, WITHOUT creating temporary QString, the reason is that the buffer prefix changes the way the concatenation work by appending to itself, instead of creating temporary QString that will be allocated and freed successively.\n\nAnother way of doing it properly would have been to return this type of subclass directly from the QString::operator+() construct, but this breaks QT4 which is undesirable, so this work-around is required instead, which also works without modifying the core QString class.\n\nFor small strings the gain is negligeable, but if you concatenate big statements like a 16 lines SQL query or similar then the gain is non-negligeable. It also makes the code way more readable than using .append() or operator+=() all over the SQL parts.\n\nKStringBuffer buffer;\nQString result = buffer + \"Th\" + s + ' ' + s + \" way \" + 't' + \"oo cool\";\n"
    author: "fprog"
  - subject: "Re: KStringBuffer"
    date: 2007-10-28
    body: "Well, if it is a good way to improve performance, it probably won't be a problem to find some developers who want to use it.\n\nYou'll need a suitable licenced and documented file and advertise it to application developers.\n\nOnce the rule for at least two application's using it is met, anyone can suggest it for inclusion in the KDE 4.1 kdelibs"
    author: "Kevin Krammer"
---
In <a href="http://commit-digest.org/issues/2007-10-21/">this week's KDE Commit-Digest</a>: Fortune-teller and Keyboard Layout applets for <a href="http://plasma.kde.org/">Plasma</a>, KNewsTicker resurrected for KDE 4.0 as a Plasmoid. Rewrite of &lt;canvas&gt; tag support in KHTML. Various new language syntax highlighting in <a href="http://kate-editor.org/">Kate</a>. Internal database storage work in <a href="http://www.digikam.org/">Digikam</a>. More playlist handling work, and support for Magnatune "streaming membership" in <a href="http://amarok.kde.org/">Amarok</a> 2. OpenDocument loading of charts in <a href="http://koffice.kde.org/kchart/">KChart</a> for <a href="http://koffice.org/">KOffice</a> 2. Various graphics fixes and a user handbook for the Bovo game. <a href="http://kolourpaint.sourceforge.net/">Kolourpaint</a> is now fully ported to Qt4. Continued work on the <a href="http://eigen.tuxfamily.org/">Eigen</a> 2 library. Further porting away from KDEPrint to the printing facilities provided by Qt4.


<!--break-->
