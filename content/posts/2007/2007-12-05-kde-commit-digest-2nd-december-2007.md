---
title: "KDE Commit-Digest for 2nd December 2007"
date:    2007-12-05
authors:
  - "dallen"
slug:    kde-commit-digest-2nd-december-2007
comments:
  - subject: "KGet and KUIServer duplication of functionality"
    date: 2007-12-05
    body: "There seems to be a duplication of functionality between KGet and KUIServer: they both list and control file transfers. I can see users getting confused by the two GUIs. \n\nHere's an example: every transfer listed in KGet also appears in KUIServer, but not every transfer in KUIServer is listed in KGet (because KGet doesn't deal with uploading). Very confusing. How about merging all functionality of KGet into KUIServer?"
    author: "Laurie"
  - subject: "Re: KGet and KUIServer duplication of functionality"
    date: 2007-12-05
    body: "There was also a plan to move towards a centralised \"Factory\" (working title, maybe?) applet, which maintained/controlled/showed background tasks for all apps in one central place in the systray.  It would be nice to see KGet, KUIServer's, and KTorrent's UIs disappear completely, in favour of this."
    author: "Lee"
  - subject: "Re: KGet and KUIServer duplication of functionality"
    date: 2007-12-05
    body: "No, the plan was to show tasks in KUIserver 'too'.not to replace them.means that you will have tasks both in the application ui and in the kuiserver.\n(I think so)\n"
    author: "Emil Sedgh"
  - subject: "Re: KGet and KUIServer duplication of functionality"
    date: 2007-12-05
    body: "KUIServer is very basic for each Transfer...\nThe other Applications(KGet, KTorrent...) do the more specialised parts, like Multithreaded-Downloads..."
    author: "Lukas Appelhans"
  - subject: "Screen hotplug detection in Plasma"
    date: 2007-12-05
    body: "The beginnings of screen hotplug detection in Plasma...\nSo no more restarting X when you plug in a second monitor?  Sounds sweet!"
    author: "J Klassen"
  - subject: "Re: Screen hotplug detection in Plasma"
    date: 2007-12-05
    body: "I was just browsing through the Solid API pages, and I noticed that screens seem to be missing there. Wouldn't it make sense to make Solid capable of detecting screens and notifying applications about their (dis-)appearance? Or does that work through the GenericInterface of Solid?"
    author: "Andr\u00e9"
  - subject: "Re: Screen hotplug detection in Plasma"
    date: 2007-12-05
    body: "Basically it wouldn't be very useful, you need much more information on top of screen management like the new geometry of the desktop and how it spans on the various screens.\n\nThat why IMO for this case it's better to rely on X (at least for now)."
    author: "ervin"
  - subject: "Re: Screen hotplug detection in Plasma"
    date: 2007-12-05
    body: "But it's quite inconsistant! Of course X is the better choice, but why not inform the system about a new screen. at least some applets like the hardware notifier would give feedback leading to making it a more consistant experience for the user as well!"
    author: "Thomas"
  - subject: "Re: Screen hotplug detection in Plasma"
    date: 2007-12-06
    body: "> So no more restarting X when you plug in a second monitor?\n\nwith new drivers and the latest xrandr, yes. there's a bug in QDestkopWidget, xrandr or the driver atm; however that i haven't had a chance to track down yet 9haven't even started looking, just made a note for myself that i need to delve into it later), but plasma itself is all ready: when a second monitor appears, a containment is indeed made for it. the geometry is not always right though due to the aformentioned bug.\n\nbtw, this got implemented solely because a person came onto irc a few evenings ready to report problems and test patches. that's the kind of stuff that really, really helps move these kinds of features forward =)"
    author: "Aaron J. Seigo"
  - subject: "Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Wouldn't it be better to focus on improving the FTP and SFTP KIO slaves than the KFTPGrabber client? The KIO slaves are much more convenient as they can be used from the regular file manager (Dolphin) or from Open/Save As dialogs. It's cumbersome to open up a separate application just to deal with FTP and SFTP transfers (KFTPGrabber), another application to deal to with Windows/SMB (Smb4K), and another to deal with local files (Dolphin).\n\nThe FTP and SFTP KIO slaves are installed by default in KDE and improvements in the KIO slaves would help every single KDE app. Very few people even bother to install KFTPGrabber anyway."
    author: "Bob"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "I noticed that, too. Somehow the trend in KDE4 seems to lead away from the interacting modules as it was in KDE3, and back to the paradigm \"one app for one purpose\".\n\nDolphin insteasd of Konqueror, different apps instead of just different I/O-slaves - I don't like that.\n\nYes, I know, all the modular technology is still there, but it gets more and more hidden (for the so-called \"poweruser\")."
    author: "Peter Thompson"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "KFTPGrabber also exists for KDE3, see http://extragear.kde.org \n\n> different apps instead of just different I/O-slaves\n\nwhy \"instead of\"? Isn't it more \"additional to\"?\n"
    author: "Sebastian Sauer"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "KFTPGrabber is an extragear application, therefore it's by definition an add-on rather than a piece of core technology.  And KDE is a very diverse free software project, where people work on what they want to.  Just because there is a very specific app mentioned in Commit Digest, don't conclude that KDE is moving away from being the most well-integrated Free Software Desktop.\n\nPut another way: If you were going to start hacking on KDE tomorrow, would you be more inclined to start by improving these kioslaves at the core of KDE, or to develop a standalone app in playground?  Would doing that change the nature of KDE?"
    author: "Will Stephenson"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "I actually sometimes prefer a single app - the problem is that KIO slaves does not seem to close when they should - for instance using krusader as an example, the FTP connections always seem to linger long after I have closed them (and indeed they do, because a lot of times I get shut out becauseee I have logged in X amount of times when the max limit is X (and I am really only trying to log in with my first user).\n\nAnother plus is you can easily ajust things like bandwidth control, activity times etc from a specialized program."
    author: "Thomas Bergheim"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-08
    body: "> Another plus is you can easily ajust things like bandwidth control, activity times etc from a specialized program.\nWhich would definitly be kool to have integrated into the kio-slaves and kuiserver dialogs."
    author: "eMPee584"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "I dont see how the two are exclusive...\n\nOn one hand the core dev can iron out the kinks of the various KIOs (there arent much... if any?) but at some point they must remain generic enough to be used kde-wide (their whole point)\n\nHowever some people *do* prefer very simple app that serves one well-defined purpose. This app will use the KIO slave, obviously. So the fact that this app exists / is actively developed does not make the KIO any worse.\n\nIn fact, if it comes to push the boundary of the underlying libs, those will be adjusted to be better -- everyone win.\n\nThis is said very often: one dev working one \"KFTPGrabber\" does not mean -1 dev working on the ftp:// KIO-slave"
    author: "Anon"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "> This app will use the KIO slave, obviously.\n\nActually, it doesn't. The commit says that KFTPGrabber is \"Now based on libssh2\". Libssh2 != SFTP KIO slave."
    author: "Bob"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "...and that is indeed a shame. It would be much better if the KFTPGrabber author forced himself to use the KIO slave, improving it as necessary, adding features perhaps (such as FTP parameters) to support the needs of the app. Then everyone would benefit from this work."
    author: "Martin"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "There certainly seem to be bugs in the kio_imap support."
    author: "Lee"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "I agree. One i'm missing at kio-slaves, is something like a control-device for actually used slaves. I mean something where i can see what app use which slave, and where i can terminate slaves. fish:/ for example seems every time endlesly open."
    author: "Chaoswind"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Hell YES. Great idea! I wonder if it's anything simple enough to be tackled by someone without (much) C++/KDE experience?"
    author: "Balinares"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "+1\n\nThis would be really great, and it would more easily solve my \"multiple-lingering-logins\" ftp problem I described above, for instance.."
    author: "Thomas Bergheim"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Well, the protocol abstraction system KFTP uses is quite different from KIO slaves (just look the 'src/engine' subdirectory in svn repo). Not only (speaking for the FTP part) that it is state based instead of using blocking socket commands, it also includes things like speed limiting, keepalives etc.\n\nAs far as SFTP support goes, kio_sftp actually talks to openssh via stdin and that I don't like at all. Using a library like 'libssh2' that implements the underlying protocol seems like the proper way (yes I know, when kio_sftp was designed there was no such library available).\n\nOfcourse there are similarities between the two systems and one could try to port it, the code is readily available ;-)"
    author: "Jernej Kos"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Are you working on this ftp program?\n\nPlease, please improve kio_sftp instead.  Using libssh2 sounds like a great idea - please port your changes across and use the kioslave instead if possible.\n\nCan't speed limiting etc be done in kio_sftp?  I mean, can it be implemented, or does the api simply not allow that?\n"
    author: "John Tapsell"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Probably using job meta data, like passing user agent and referrer to kio_http"
    author: "Kevin Krammer"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Can these be altered while a job is in progress? Changing upload speed mid-upload is a common use-case for FTP transfers."
    author: "Anon"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Good question, but I don't know.\n\nProbably not (yet)\n"
    author: "Kevin Krammer"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Yes I am the author of KFTPgrabber. There are the following problems with KIO slaves:\n\n1. KIO slaves should provide a standard way to do speed limiting across multiple connections (KFTP uses the token bucket algorithm to allocate bytes to individual connections) in some logical group. That way you can limit the speed of all transfers inside an application in a fair way and modify the limits at any time.\n\n2. The FTP part cannot be easily ported because (as said before) KFTP uses an async state-based engine (think FSM) with command chaining that requires an active QEventLoop. But as I currently see, all KIO slaves are made around blocking socket operations.\n\n3. KFTPgrabber's FTP engine supports things like FXP (site-to-site) transfers where two separate connections have to synchronize their states to be able to  transfer a file. I don't know how this could be done with KIO slaves.\n\nSo currently changing the core architecture in KFTP is a no go. I do want to improve kio_* in the future, but at the moment I barely have the time to work on KFTP."
    author: "Jernej Kos"
  - subject: "Trolltech changes to revamp KIO Architecture"
    date: 2007-12-05
    body: "> So currently changing the core architecture in KFTP is a no go.\n\nThe KIO architecture will have to be revamped soon because Trolltech is building a new networking stack:\n\nhttp://labs.trolltech.com/blogs/2007/11/24/one-more-piece-falling-into-place-network-access\n\nThere's never been a better time to improve KIO so that it is more flexible and so that it can do speed limiting and everything else KFTP is capable of."
    author: "Bob"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-05
    body: "\"The KIO architecture will have to be revamped soon because Trolltech is building a new networking stack\"\n\nHuh? How does this impact KDE at all?"
    author: "Anon"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-05
    body: "Because KDE will reuse as much as possible from TT's networking stack?"
    author: "jospoortvliet"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-05
    body: "I don't see how it would, considering that KIO binary interface is part of the platform ABI. It might change some backends, of course.\n\n"
    author: "SadEagle"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-05
    body: "> I don't see how it would, considering that KIO binary interface\n> is part of the platform ABI.\n\nThe ABI will change for the next KDE version release, KDE5. Or KIO can take the \"Plasma approach\" and not guarantee binary compatibility in the KDE4 release.\n\nBinary compatibility is overrated anyway -- it goes against the \"release early, release often\" methodology that best suits open source software. "
    author: "Bob"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-05
    body: "KDE5 is years away. And KIO -will- guarantee BC and SC. \n\nAnd you could argue about BC being overrated, but SC certainly isn't, and BC helps with that. I've fixed half a dozen of critical bugs that were caused by tiny semantics changes. There is absolutely no way that changing the internals of a heavily used and mature subsystem won't cause a substantial number of regressions. That includes app compatibility, and also server compatibility --- kio_http contains tons of workarounds for broken stuff that's out there.\n\nReplacing working code with something new and shiny just because it's new and shiny is simply an invitation for problems. \n\nAt any rate, we're sure as heck are not obligated to throw out our code just because Qt is duplicating kdelibs functionality yet again.\n"
    author: "SadEagle"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-06
    body: "> heavily used and mature subsystem\n\nIf KIO was that great, KFTP wouldn't need to resort to using more feature-rich and updated libraries like libssh2. The very existence of KFTP and SMB4K reveals that KIO lacks required features, pushing many contributors to develop/improve peripheral technologies that don't benefit KDE as a whole. And even if they want to bring KIO up to speed, they're confronted with a \"you have to wait 5 years for KDE5\" argument. This policy only leads to stagnation in KIO and fragmentation in the development effort.\n\nLook at GNOME, they're dumping their old gnome-VFS layer for GVFS/GIO while in the stable 2.X release. KDE should be able to do that too. \n\n> we're sure as heck are not obligated to throw out our code just\n> because Qt is duplicating kdelibs functionality\n\nDuplicated functionality means wasting users' RAM, but I won't argue who's to blame ;)"
    author: "Bob"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-06
    body: "> If KIO was that great, KFTP wouldn't need to resort to using \n> more feature-rich and updated libraries like libssh2\n\nif you read the author's comment, you'll see how this statement should've been \"if kio_ftp was that great\". it's not about kio at all.\n\n> Look at GNOME, they're dumping their old gnome-VFS layer \n> for GVFS/GIO while in the stable 2.X release. \n\nwhich is made easier by the fact that so few apps use it. there are development  benefits to a low-integration environment.\n\n> KDE should be able to do that too.\n\nit's slightly more difficult when it is used so pervasively. we could certainly offer a second stack and apps could slowly move over to it, but we would still have to keep kio around until kde5. it's about respecting your developers and users (since making developer's lives harder means taking features and bug fixes away from users)\n\nso yes, it's possible. but for what gain and at what price? kio is actually pretty decent and did see improvements for kde4."
    author: "Aaron J. Seigo"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-06
    body: "> it's not about kio at all.\n\nSure it's about the KIO architecture. Let's take Jernej's comments one by one:\nhttp://dot.kde.org/1196818371/1196822000/1196849333/1196855480/1196871644/\n\n1) Speed limiting -- is there any API to allow a KIO client to control the speed of a download/upload in _any_ protocol? Nope, so that a KIO-wide deficiency, not just kio_sftp.\n\n2) Can _any_ KIO slave be written around an async state-based engine (ie. \"mounting\")? Not according to Jernej, so that KIO-wide as well. GVFS/GIO has made great strides in this area compared to its predecessor, btw. And I agree that it's needed in order to avoid the penalty of re-authentication with every access to the server.\n\n3) Site to site transfer without downloading locally -- will probably have to create a new job class to cover this. So again, KIO-wide deficiency.\n\nHere are my own wish list for KIO:\n\n4) Permissions from KIO::StatJob are worthless because they represent users/groups on the remote system, not the local one. Consequently KFileItem::isWritable() returns a bogus result for remote files.\n\n5) Can't start/use a KIO job from a non-main thread. It's excruciatingly cumbersome to have to shuttle all the requests to the main thread and the results back to the worker thread."
    author: "Bob"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-06
    body: "> Can _any_ KIO slave be written around an async state-based engine\n\nSure, why not?\n\nBlocking on a socket is sometimes convenient if one doesn't have to do anything else, but it can also be done non-blocking using event notification (e.g. select())"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: "Also, KIOs still can't be chained (like opening zip inside tar inside arj, etc),\nwhich is something mc for instance can do for ages.\n"
    author: "Michal"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: "We used to support that and removed it. It was rarely used and leads to a number of bugs and ambiguities in things like URL parsing.\n"
    author: "Richard Moore"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> leads to a number of bugs and ambiguities in things like URL parsing.\n\nIf introducing abmiguities in in URL is the only problem with chaining KIO slaves, then this can be easily fixed through repeatedly \"mounting\" each component. Say you have a file essay.txt saved in bundle.tar compressed as surprise.bz2 saved on sftp://school.edu/home/username. \n\nFirst you'd mount sftp://school.edu/home/username in ~/network/school. Then you'd mount ~/network/school/surprise.bz2 in ~/archives/surprise. Then you'd mount ~/archives/surprise/bundle.tar in ~/archives/bundle and your file will be accessible as ~/archives/bundle/essay.txt.\n\nThere'n no URL ambiguity in ~/archives/bundle/essay.txt."
    author: "Bob"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-06
    body: "> And even if they want to bring KIO up to speed, they're confronted with a \n> \"you have to wait 5 years for KDE5\" argument. This policy only leads \n> to stagnation in KIO and fragmentation in the development effort.\n\nOf cource, you can always make an kio2 and use it beside kio, until it become kio with kde5."
    author: "Chaoswind"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-06
    body: "> Of cource, you can always make an kio2 and use it beside kio\n> until it become kio with kde5.\n\nYup, that's a plausible idea."
    author: "Bob"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-07
    body: "I know that sounds radical, but i think for network transparent file-management (http is a different story) KIO should simply be deprecated in favor of a thin QT/C++ \"QGio\" binding for GIO/GVFS. To help migration, KIO netaccess and jobs could wrap this binding. \n\nThere is no sane reason to maintain multiple incompatible network transparency layers - Just a waste of time and a usability desaster. GIO/GVFS is nice and lean  - and there is no doubt it's design is a step forward. Competition between KDE and Gnome is good, but at the file-system layer it's unhealthy.\n\nIf i had time i would start coding \"QGio\" and the KDE/GIO bridge today...\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-07
    body: "Two big reasons why KDE shouldn't depend on GIO/GVFS:\n\n1) Linking to two frameworks that do the same basic functionality (QtCore and Glib) means wasting RAM.\n\n2) If a KDE developer wants to fix a bug in file-transfer, he would have to code in C and learn Glib programming. Dealbreaker right here.\n\nHaving said this, GIO/GVFS has a lot great ideas that can be incorporated into a new version of KIO."
    author: "Bob"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: "1) Loading two VFS libraries with all their daemon/slave processes, which will happen as soon a GTK app runs on KDE (quite often, to be realistic) means wasting even more RAM. The whole point of sharing code and libraries is saving RAM. Pure GNOME or KDE desktops may perhaps exist in the dreams of desktop developers - but nowhere else...\n\n2) KDE developers learning C? So what? Being \"multilingual\" is not a big deal for software developers. It's just normal. I - for instance have to code in Java, PHP, C#, C, C++,... to make a living.\n\n+3) What about usability? Isn't file-management consistency more important than loving a certain programming language or not? Or having another tiny library in memory - who cares...\n\nThe whole situation of an estimated third of the applications linking one VFS layer, another third a completely different one, and the rest without access to network resources at all is a big joke. Honestly - i think it's quite pathetic. You will never be able to explain this chaos to anyone who isn't a computer guru. \n\nMonopolizing parts of their library infrastructure has been an unfortunate consequence of the \"desktop wars\". Using GIO/GVFS in KDE/Qt is a chance to fix this \"bug\"...\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: "I think the misunderstanding is that the application would somehow link against GIO/GVFS.\n\nQuite likely the architecture of this new VFS is quite similar to KIO, e.g. the application is talking to some other process which in turn is handling the \"remote\" file system.\n\nThus the application does not get any additional dependencies, e.g. just like KIO slaves can always be used without KIO Jobs which is basically just the KDE API to file access."
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: "Depends how you design it. I think the best and leanest solution would be linking the GIO client in-process. Because QT4 already links GLib for the main loop by default (i think), that wouldn't be a problem. And it's the best way to avoid redundant code. You only need a thin C++ wrapper layer and you get all the threading stuff for asynchronous local file access, file/directory monitoring, drive/volume management for free. \n\nI my opinion the \"lingua franca\" for cross desktop libraries should be GLib+C. That's a bit revolutionary, but it would make a lot of things easier. You don't have to code client side libraries twice, especially when they are asynchronous (because GLib provides the main loop). Desktops can move forward instead of wasting time duplicating and maintaining code which practically does the same thing.\n\nBut - of course - there are other ways to bring GIO/GVFS to KDE. The easiest way would be to link the GIO client inside a generic KIO slave which handles multiple protocols (file:, smb:, sftp:, ftp:,...). With this you hardly have to change the current system and you can still have slaves for the protocols which GIO/GVFS doesn't provide (http,...). But it's a bit more bloat regarding deamon processes.\n\nAlexander Larsson said that it would also be possible to talk to GVFS mount daemons directly via DBUS. That's the third option, but IMHO a waste of time, duplicating client code.\n\nIf KDE wants to move in the GIO/GVFS direction i would start with the second option (the generic multiprotocol GIO/GVFS IO-slave). It would also work for KDE3, thus KDE3 and KDE4 apps can work on the same network shares.\n\nAnyway, i think KDE developers who are interested should join the GIO/GVFS project by subscribing gnome-vfs-list@gnome.org as soon as possible. It would be cool for desktop file-system experts to share their knowledge (which hardly happened in the past i think). And also, because GIO/GVFS is not final and API/design changes are still possible.\n\n\n\n\n\n\n\n\n\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: "> Depends how you design it.\n\nRight. I couldn't find anything about GIO architecture, so I assumed that some kind of daemon would handle the IO on behalf of the applications and they would connect to it.\n\n> I think the best and leanest solution would be linking the GIO client in-process\n\nWell, if all the functionality is implemented by threads or select() then you more or less have to link to the library. If it is some kind of daemon it is usually better to implement the protocol natively as this avoids multiple conversions, doesn't depend on any symbol/behavior assumptions and helps find errors in the protocol.\n\n> Because QT4 already links GLib for the main loop by default (i think)\n\nI think this isn't default on even Unix, but even more unlikely on any other platform.\n\n> You only need a thin C++ wrapper layer\n\nReally depends on the library. For example it would make no sense to wrap libhal or the networmanager client library if you can use actual API (D-Bus) directly.\nOf course if a library is the functionality, then wrapping makes sense, unless source and target framework differ too much in concepts or one would convert data a lot.\n\n> I my opinion the \"lingua franca\" for cross desktop libraries should be GLib+C. \n\nTrue. If I remember correctly there have been numerous proposals to split GLib into the data handling part and the object model part, so that the data handling part can be used without interfering with other frameworks' object models. So far there has been no move into this direction, usually people who want the convenient C function take the data handling part of GLib and include it with their libraries.\n\n> The easiest way would be to link the GIO client inside a generic KIO slave\n\nRight, would especially help to see how each architecture's concepts can be mapped to the other\n\n> and you can still have slaves for the protocols which GIO/GVFS doesn't provide (http,...)\n\nAnd https, webdav, ftp (according to http://live.gnome.org/GioToDo)\n\nSince googling for GIO on site:gnome.org renders a lot of blogs, minimal API docs but no architectural or design information, it would be interesting to at least get some information about how the integration with the respective desktop's authentification system is handled, how connection state data like cookies are shared between MIME inspector process (e.g. application launcher) and MIME handler (e.g. preferred application) and how user interaction (e.g. asking about overwrite, skip, etc) is performed.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: ">  So far there has been no move into this direction...\n\nCorrecting myself:\nseems to be a only a packaging issue, i.e. there is a separate libgobject but it is part of a package called libglib. So it is no longer a dependency issue, I stand corrected"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: "GIO actually uses the GObject model, but i don't see why wrapping a GObject based library with C++ classes or using it in a KIO slave should be a problem.\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "\"GIO actually uses the GObject model, but i don't see why wrapping a GObject based library with C++ classes or using it in a KIO slave should be a problem.\"\n\n\"Oh, you can just wrap it!\" is not a solution."
    author: "Segedunum"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "I agree, what KDE needs is a more capable KIO API built from the ground up on C++ and Qt. \n\nThe reason why KDE had to compromise (and I'm using \"compromise\" with all its negative connotations) to use HAL, NetworkManager, PackageKit, and PolicyKit even though these are built on GLib, is that RedHat is pushing GNOME and doesn't give a hoot about KDE. If you look at the projects listed above, they were all started by RedHat employees.\n\nNow GIO/GVFS is different because KDE does *not* need to rely on it -- we have plenty of dedicated developers interested in making file access easy (look at KFTP and SMB4K). But what is missing is a flexible and powerful framework that will fit the needs of these developers.  I'm going to propose such an API for KIO2 on kde-devel@kde.org in about a week in order to get feedback before implementing it."
    author: "VladC6 (aka. Bob)"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> even though these are built on GLib\n\nYou are missing an important point: out-of-process infrastructure is being used through a communication protocol, e.g. HAL's API are a set of D-Bus interfaces.\n\nThus the client is totally unaffected by whatever dependencies are used to implement the service and in the very unlikely event that such an implementation is not acceptable, it can be replaced without any change on the client side.\n\nI am not aware of any compromise KDE had to make in order to use such shared infrastructure implementations because the respective developers chose different means of implementing their side.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> out-of-process infrastructure is  being used through a communication\n> protocol, e.g. HAL's API are a set of D-Bus interfaces.\n\nI was certainly aware of that :)\n\n> I am not aware of any compromise KDE had to make\n\n1) The fact that someone with Qt/KDE skills has to learn C/Glib programming in order to fix bugs in HAL, NetworkManager, PolicyKit, PackageKit is IMHO a big disadvantage.  Many new developers don't have the time or patience to learn two frameworks (QtCore and Glib), so knowing that they'll have to learn Glib anyway to solve hardware and low-level issues, they'll probably go ahead and improve GTK/GNOME apps due to their lower learning curve. Great way for KDE to lose potential developers to \"the competition\", don't you think?\n\n2) Another drawback is that users have to install GLib on their system -- wasted hard drive space considering QtCore already does approximately the same functionality. I'm all for lean and mean systems with no bloat :)"
    author: "VladC6 (aka. Bob)"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> The fact that someone with Qt/KDE skills has to learn C/Glib programming in order to fix bugs in HAL, NetworkManager, PolicyKit, PackageKit is IMHO a big disadvantage.\n\nI don't buy this argument, because those things are shared infrastructure and maintained by their own development teams, just like Xorg, libc, etc.\n\nAnd, as I wrote before, should there really be a huge problem with the current implementation (very unlikely since quite some projects depend on them), one can always replace it with a new implementation.\n\n> so knowing that they'll have to learn Glib anyway to solve hardware and low-level issues\n\nMost application developers don't get into situations where they want to solve harware or low-level issues, not just because it might require a different language, but because it requires a respective skill set, domain know-how, etc.\n\nFor those who have the necessary skills, e.g. our library/core developers, it is a bit less convenient but nevertheless possible, especially since they'll probably just need to find the issue and let the other project's developers fix it.\n\n> Another drawback is that users have to install GLib on their system \n\nWhich they of course have anyway, especially on Linux distributions that are LSB compliant.\n\nI really think this is blown out of proportion in this context (\"desktop\" services), because the IPC shields the clients from the service dependecies.\n\nConsider how stupid it would look from your perspective if someone claimed they cannot use a desktop service because it's using portions of Qt (e.g. QtCore) for its implementation.\n\nThe project which develops and maintains a service decides on *its* implementation and tools, the projects developing and maintaining the various clients decide on *their* implementation and tools, neither choice affecting the other, everybody is happy.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> For those who have the necessary skills, e.g. our library/core developers,\n> it is a bit less convenient\n\nIt would be interesting to see if the authors of KFTP and SMB4K agree to start working on the GVFS implementation of FTP/SFTP/SMB. My guess is that C and GLib will prove to be a disincentive to doing so.\n\nAnyway, what I'm going to focus on first in KIO2 is the frontend (ie. Job classes, KFileItem, UDSEntry) in order to make it more flexible and more in line with GIO. That should actually make it easier to write a GVFS backend for KIO2."
    author: "VladC6 (aka. Bob)"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> It would be interesting to see if the authors of KFTP and SMB4K agree to start working on the GVFS implementation of FTP/SFTP/SMB\n\nMy guess is that those protocols will be available from the actual GVFS team.\n\nIf the respective developers IO slave developers think they can do better, they can always implement the GVFS protocol daemon using Qt instead of GLib.\n\nRemember that the protocol shields the other side from implementation details, thus if it is possible to implement a client using its technology stack, it is equally possible to implement a service using the same technology stack.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> If the respective developers IO slave developers think they can do better, they \n> can always implement the GVFS protocol daemon using Qt instead of GLib.\n\nFrom reading yours and Vlads comments i get the impression that KDE developers  really have a wild passion for code duplication.\n\nWhile i think having a KIO2 with pluggable backends is a very good idea, i'd just link GIO in-process (perhaps optionally with a default implementation for local filesystem only).\n\nQt and KDE provide and link tons of libraries, load thousands of icons, but linking a tiny GLib based library seems to be a big drama. \n\nSorry for being a bit provocative. ;-)\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "I am merely detailing that an implementation can be replaced by a better implementation if necessary.\n\nI am afraid I don't get the code duplication part, though.\nKDE is basically always using suitable external implementations when available while even independent infrastructure work gets ignored for wild reasons if KDE developers started them.\n\n> Qt and KDE provide and link tons of libraries, load thousands of icons, but linking a tiny GLib based library seems to be a big drama. \n \nI thought it would be clear enough that the problem is not linking but rather layering a high level API on top of another high level API when one could rather implement the high level API directly.\n\nYou even acknowledge this in one of your own postings: \"...This could also be seen as an advantage, because having a plain C API and another GObject API around would duplicate the number of symbols....\"\n\nIt is a matter of how a project is designed. The common layer can be the protocol specification, e.g. HTTP, or a low level protocol handler library, e.g. D-Bus.\n\nBoth have advantages and disadvantages. A low level library makes it easier to use the protocol, but on the other hand is a lot more difficult to write.\n\nAbusing high level API libraries for convenience leads to unnecessary restrictions, e.g. how xlib being used in toolkits basically forces synchronous calls while a low level protocol handler like XCB doesn't.\n\nLibraries which have application developers as their target audience are usually designed differently than libraries for other library developers.\n\nAgain, the D-Bus library stack is an excellent example. The base library cannot dictate how bindings want to watch for socket activity, as some will want to do select() like constructs with event loops and other will want to use threads blocking on the socket.\n\nThe bindings on the other hand know which context they are usually used in and therefore can make this choice, e.g. Qt bindings using eventloop bases socket notifiers, Java bindings using threads.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> I thought it would be clear enough that the problem is not linking but rather \n> layering a high level API on top of another high level API when one could rather\n> implement the high level API directly.\n\nI disagree. Layering a high level API on top of another high level API is ideal from design. Just like KIO::Netaccess wraps KIO jobs.\n\nAs KIO is a lot more high-level than GIO (i wouldn't call GIO very high level), things might fit together quite well. And if there are incompatibilities i wouldn't expected them to be related to GObject style C, but rather hidden somewhere else in the implementation details. And if they exist, one should first try to sort them out by improving the GIO API. Rewriting everything from scratch should really be the last option to consider.\n\nI think it's a wrong assumption that a C-API automatically turns into a high level API, just because it's using GObject.\n\n\n\n\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> As KIO is a lot more high-level than GIO\n\nYes, this would probably work.\n\nI had been thinking about using the GIO model directly, since it would make file identifiers compatible.\n\nKIO's Job API would probably be layered on top for compatability with older applications, but I am not sure one would gain a lot by only swapping the implementation.\n\nBut lets assume for a moment that such a backend change would have any advantage: does it work to have two D-Bus bindings work on the same connection?\n\n> Rewriting everything from scratch should really be the last option to consider.\n\nThat's why I like the D-Bus software stack. All the protocol details are shared in libdbus and the application frameworks only have to implement their platform adapter code but have full control how they do it.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> I had been thinking about using the GIO model directly, since it would make file\n> identifiers compatible.\n\nyes - and it would align the whole data model behind file-management (the hierarchy displayed in file choosers, where removable media and network mounts are listed,...). a big step forward i believe.\n\nif you want this, you definitely have to link GIO directly. if you only want the GVFS mount daemons it's probably better to write a separate client with QtDbus.\n\nperhaps there could be still different flavors of the data-model, but at least it's desktop wide for all applications.\n\n> does it work to have two D-Bus bindings work on the same connection?\n\nI once discussed this on the D-Bus list. Surprisingly i found out that the QT-Dbus bindings don't use the default session bus connection (That's not optimal and should be probably fixed). Thus it will work, as GIO uses the default connection. Of course you have to compile Qt with GLib main loop support.\n\n\n\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-11
    body: "> if you want this, you definitely have to link GIO directly.\n\nI wouldn't see a reason why.\nThe architecture doesn't sound like it got hacked together and nobody any longer knows how it works. That's how Microsoft works, not Free Software developers.\n\nSince I am a D-Bus bindings maintainer this is what I mainly base by reference on when it comes to wrapping.\nBasically all bindings have their own set of assumptions, options and restrictions. It would be pretty close to impossible to implement one on top of the other, while it is pretty straight forward for all of them on top of the base lib.\n\n> Surprisingly i found out that the QT-Dbus bindings don't use the default session bus connection\n\nRight, I think the Qt4 bindings use private connections.\n\nHowever I haven't seen an application using more than one binding on the same bus yet, would be interesting to see if this works.\n\nAnyway, as I said I'll probably have time to look at this somewhen in spring and I am not ruling out wrapping the GIO implementation, but I will definitely also investigate the doing a native implementation.\nAt that time there will likely also have been work on its language bindings and all kind of wrapper issues already been solved.\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-11
    body: ">> if you want this, you definitely have to link GIO directly.\n\n> I wouldn't see a reason why.\n> The architecture doesn't sound like it got hacked together and nobody any longer\n> knows how it works. That's how Microsoft works, not Free Software developers.\n\nwhat i meant is that a lot of the data model is managed in the in-process part of GIO/GVFS like maintaining the drives/volumes list (parsing/monitoring mtab etc). therefore having two implementations will likely cause a incoherent file-manager/file-chooser experience (for instance a FUSE mount getting listed in GTK filechoosers but not in KDE apps).\n\nalso, the IPC protocol might not always be the best place to standardize an interface. because GIO uses a model of dlopening the actual implementation, the frontend API might be the \"standardized\" interface, not the more complex IPC stuff behind that.\n\n> However I haven't seen an application using more than one binding on the same \n> bus yet, would be interesting to see if this works\n\nIf it doesn't, then that's a bug and should be fixed. It's not acceptable having to write cross desktop libraries in a terrible style like D-BUS bindings (dealing with main loop watches etc)\n\n\n\n\n\n\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-11
    body: "> like maintaining the drives/volumes list (parsing/monitoring mtab etc)\n\nI surely hope that it does not do this on Linux, where HAL is managing deviced and volumes.\n\nBut this is an interesting issue none the less. Since it probably manages user specific \"virtual mounts\", part of a KDE implementation would probably be \"below\" Solid, e.g. in a backend or additional backend, and part of it probably \"above\", e.g. the IO parts. Or, since those are not real mounts/volumes, probably totally bypassing Solid.\n\n> therefore having two implementations will likely cause a incoherent file-manager/file-chooser experience\n\nQuite unlikely. Assuming that the input data is equal, any derivation in the result would either be a bug in one of the implementations or an ambiguity in the specification, both fixable causes.\n\n> also, the IPC protocol might not always be the best place to standardize an interface\n\nRight. It depends on the goal, e.g. being reimplementable by other parties (a.k.a. open specification) or being private knowledge (a.k.a. proprietary, though legally reverse-engineerable)\n\n> because GIO uses a model of dlopening the actual implementation, the frontend API might be the \"standardized\" interface, not the more complex IPC stuff behind that\n\nInteresting!\nWhen KDE developers do this they get flamed about \"abstracting abstraction layers\".\n\nNice to see that separating interface from implementation, even using plugable implementation, seems to be used somewhere else as well.\n\nIncidentally this offers a third option for adoption: wrapping the backend loading and access in a native frontend interface.\n\n> If it doesn't, then that's a bug and should be fixed.\n\nIt probably depends if the base library allows more than one callback registered for the same hook, e.g. the callback for incoming messages.\n\nProbably easier to use private connections in libraries, so they don't interfere with anything the application is doing. After all the application should not have to care about implementation details of the library, e.g. if it uses threads or a certain event loop, unless the library is specifically written for such a use case.\n\n> It's not acceptable having to write cross desktop libraries in a terrible style like D-BUS bindings\n\nWell, it'sa tradeoff. The other option is to use threads internally for any kind of active functionality and let the application decide whether it wants foreign thread callbacks to be transformed into event loop events or let them directly access application data.\n\nTraditionally Unix developers avoid threads for reasons like increased difficulty when debugging, but that is likely to change since multiple execution units are becoming pretty much standard.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "I'm afraid writing a better KIO won't fix the problem. Having the core engine of file management in the desktop layer is just wrong from design."
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "While it would be nice to have it lower, i.e. on the operating system level, this would make desktop applications quite dependent on those operating systems.\n\nNot even talking about the necessary API changes for authentification callbacks, async IO, etc.\n\nDoing it at the user session level like KIO, GIO, GNOME-VFS is currently the only sensible approach, which might be retired in favor or a more system integrated approach when it becomes widely available.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> Having the core engine of file management in the desktop layer\n> is just wrong from design.\n\nI agree. KIO2 won't have a GUI dependency. It will depend only on QtCore, libsmbclient, libssh2, etc. I also plan to keep the separation between frontend (KIO API) and backend (kioslaves). So it will be possible to create a GVFS backend to KIO2. In fact, that might be even easier because the KIO2 frontend API will be quite similar to GIO."
    author: "VladC6 (aka. Bob)"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "KIO slaves don't depend on GUI either, they contact an UI server for that.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-10
    body: "> KIO slaves don't depend on GUI either, they contact an UI server for that.\n\nSlaves don't depend on the GUI, but other parts of KIO (ie. Job classes) do. I hope to change this in KIO2. Here's what Thiago said about KIO:\n\n----\nKIO does GUI: it shows rename dialog boxes, download progress, delete \nprogress, it can ask for passwords, etc. Saying KIO is GUI-less is \nincorrect. And I think that trying to separate things would just be too \ndifficult and add unnecessary complexity.\n\nKIO has been made for KDE applications. And KDE applications have a GUI.\n----\n\nhttp://lists.kde.org/?l=kde-devel&m=119238531527435&w=2"
    author: "VladC6 (aka. Bob)"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-11
    body: "So, that's the source of all those stupid dialogs, which bug's me every day and ever."
    author: "Tempura"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> GIO actually uses the GObject model\n\nFrom what I read in some mailinglist archives yesterday, this is unlikely. They were talking about including it in GLib which makes it impossible to depend on GObject.\n\n> but i don't see why wrapping a GObject based library with C++ classes\n\nLooks like more work than necessary, mapping the probably different way of dealing with things (e.g. signals) onto each other.\nFor example the D-Bus base library is pretty easy to deal with in language bindings, but the GLib D-Bus bindings assume a lot about its type model, etc. \n\nThe bindings are meant to be used by application developers, therefore use some convenience stuff, which makes them harder to wrap than the base library which is designed to be wrapped.\n\n> using it in a KIO slave should be a problem\n\nTrue, in this one doesn't need to mix object models, the implementation can use just GObject, no C++/Qt/KDE required"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: ">> GIO actually uses the GObject model\n\n> From what I read in some mailinglist archives yesterday, this is unlikely. They \n> were talking about including it in GLib which makes it impossible to depend on \n> GObject.\n\nI think they want to include it in the GLib package, not in the libglib shared library. That way they can link libglib and libgobject. At the moment it's not included in the GLib package, the package is \"gio-standalone\".\n\nJust have look at the API docs - it's GObject based. I think they are using GObject interfaces for the modular design: To plug GVFS into GIO where it's available. For instance on Windows they don't want to use GVFS.\n\nHowever, the API is pretty \"flat\" C:\n\nhttp://library.gnome.org/devel/gio/unstable/GFile.html\n\nA bit more OO are the stream classes:\n\nhttp://library.gnome.org/devel/gio/unstable/GInputStream.html\nhttp://library.gnome.org/devel/gio/unstable/GFileInputStream.html\nhttp://library.gnome.org/devel/gio/unstable/GSeekable.html\n\nI still think in most cases it makes no big difference if you wrap a GObject style library or a C library with a hand-crafted object model like D-Bus. And forwarding GObject signals or plain C callbacks is quite a similar story as well.\n\nAFAIK, GIO and GVFS are completely modeled in GObject (also the daemon side), therefore there is no \"base library\" without GObject. This could also be seen as an advantage, because having a plain C API and another GObject API around would duplicate the number of symbols.\n\n\n\n\n\n\n\n\n\n\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> I think they want to include it in the GLib package, not in the libglib shared library.\n\nHmm, must have been a misunderstanding on my part then. They were discussion glib vs. gobject things so I assumed is was a matter of API vs. gvfs implementation.\n\n> For instance on Windows they don't want to use GVFS.\n\nWhat is planned to be used on Windows for remote access then?\n\n> However, the API is pretty \"flat\" C\n\nUhh, evil!\nI am C++ developer, I prefer \"grouped\", you know stuff like data and related function pointers, implicit context :)\n\n> I still think in most cases it makes no big difference if you wrap a GObject style library or a C library with a hand-crafted object model like D-Bus.\n\nIt really depends on the API.\n\nWhat I tried to explain with the example of D-Bus is that the base library is easier to wrap because it doesn't assume stuff left and right. The GLib based bindings for example assume that an object to be exported is derived from GObject.\n\nThis is obviously good design for the libraries use case, e.g. being used by GLib based applications, but would be horrible for something using another object model because instead of implementing a simple callback and handling method calling yourself, you'll have to write some kind of generic object model adapter which usually gets veeery messy, if possible at all.\n\nThe way D-Bus's softwarestack is designed allows sharing the base library between otherwise different toolstacks, only frameworks where accessing a C library is hard work, e.g. Java, Mono, are using their own protocol implementations.\n\nIn the case of GIO I don't think it would be worth the effort of splitting the current client library in such a way, since it would be quite some effort for probably very little gain (depends how complex the protocol is).\n\nI haven't found any protocol specification yet, so I assume the control flow (and probably metadata) is D-Bus based and the data is using unix domain sockets (at least on Unix).\n\nShould be pretty trivial to implement the D-Bus API using Qt bindings and reading/writing/waiting on a local sockets isn't rocket science either.\nCould be a candidate for KDE 4.1, any pointer to the respective specs?"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> What is planned to be used on Windows for remote access then?\n\nNothing i guess - just linking the native filesystem. Windows has remote access built in.\n\n> ... so I assume the control flow \n> (and probably metadata) is D-Bus based and the data is using unix domain sockets\n> (at least on Unix).\n\nAFAIK, yes.\n\n> Should be pretty trivial to implement the D-Bus API using Qt bindings and \n> reading/writing/waiting on a local sockets isn't rocket science either.\n> Could be a candidate for KDE 4.1, any pointer to the respective specs?\n\n\nI don't think there are any specs, but i'm sure Alexander Larsson can tell you   more. Also about the client side intelligence required to work with the GVFS daemons directly.\n\nYou can ask him here:\nhttp://mail.gnome.org/mailman/listinfo/gnome-vfs-list\n\n\n\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> Nothing i guess - just linking the native filesystem. Windows has remote access built in.\n\nInteresting.\nI have to admit I am not very up-to-date with Windows technology, my experience is largely XP based and this version could barely do FTP, no SFTP or similar.\n\n> I don't think there are any specs, but i'm sure Alexander Larsson can tell you more.\n\nRight.\nI'll probably have time for this between 4.0 and 4.1, sounds like a good thing to do.\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> I have to admit I am not very up-to-date with Windows technology, my experience \n> is largely XP based and this version could barely do FTP, no SFTP or similar.\n\ndon't know either, i've read somewhere there is a SFTP plugin. but at least it has SMB built in and available for all apps. \n\n> I'll probably have time for this between 4.0 and 4.1, sounds like a good thing to do.\n\nvery cool!"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-08
    body: "There are some docs here:\n\nhttp://svn.gnome.org/viewvc/gio-standalone/trunk/txt/\n\nAFAIK GIO does the local-file stuff in-process (with threads for async ops) and for remote file systems it uses the GVFS plugin to connect to a main daemon and to a number of multi-threaded \"mount daemons\" where each of them represents a connection (or a pool of connections) to a certain server. Because of the stateful \"mountpoint\" model, GIO/GVFS never has to talk to authentication systems actively. Passwords are passed to the mount operation in the GIO client, triggered on the users demand (only in file-managers or -choosers, normally). Once you have \"mounted\" a server or share, you will never be asked for a password again.\n\nAlexander Larsson wanted to avoid that password dialogs pop up unexpectedly in normal file-operations like they do in KIO or Gnome-VFS. This might answer your question about user interaction: It's always done on the client side by the file-manager, file-chooser or application (or perhaps some helper layers built into toolkits). Because of it's \"passive\" design, GIO/GVFS can stay completely desktop/toolkit independent, therefore it should be quite easy to wrap KIO around it.\n\nCookies i think are off-topic for GVFS, because it is not designed to provide the infrastructure for web-browsers. It's target is file management.\n\nTo understand the API, have a look at the sources:\n\nhttp://svn.gnome.org/viewvc/gio-standalone/trunk/gio/\n\nparticulaly gfile.h is a good starting point.\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> http://svn.gnome.org/viewvc/gio-standalone/trunk/txt/\n \nThanks, I will have a look.\n\n> AFAIK GIO does the local-file stuff in-process [...] and for remote file systems it uses the GVFS plugin to connect to a main daemon and to a number of multi-threaded \"mount daemons\" ...\n\nAh, I see.\n\nWell, in this case it would probably be best to also handle local file access traditionally (whatever the application wants to do) and implement a KDE or probably even a Qt library for communicating with those daemons.\n\nNo change for application developers who only access local files, no dependencies with probably different release cycles, but still common handling of remote access, shared infrastructure.\n\n> Passwords are passed to the mount operation in the GIO client, triggered on the users demand\n\nHmm. This is probably part of the docs you linked to, but how does the user or client know when authentification is needed?\n\n> This might answer your question about user interaction\n\nYes. The client or rather the library the client uses does all the interaction handling, one more reason to implement this correctly (as in \"suiting the expected behavior of KDE application developers\") from protocol level up.\n\n> Cookies i think are off-topic for GVFS, because it is not designed to provide the infrastructure for web-browsers.\n\nCookies were just an example of connection related data. I guess in the GIO model the application has to receive this data and then pass it to the other application through a different channel, e.g. D-Bus.\n\nWould be intersting if a connection can be \"transferred\" to another application, e.g. the file manager getting the first few bytes of a remote file for MIME detection and then transferring the connection to the \"viewer\" application, so that it doesn't have to connect again.\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> Well, in this case it would probably be best to also handle local file access \n> traditionally (whatever the application wants to do) and implement a KDE or \n> probably even a Qt library for communicating with those daemons.\n\ndon't know. for the local file system it might also make sense to share code, as this would make the whole drive/volume and trash handling more harmonic (also quite confusing at the moment).\n\n> Hmm. This is probably part of the docs you linked to, but how does the user or \n> client know when authentification is needed?\n\nUsually the filemanager or application would find a dir-entry with the type \"mountable\" (a share on an smb-server for instance). the next step would be running the mount operation.\n\n> Would be intersting if a connection can be \"transferred\" to another application,\n> e.g. the file manager getting the first few bytes of a remote file for MIME \n> detection and then transferring the connection to the \"viewer\" application, so \n> that it doesn't have to connect again.\n\nForget about the weird model of KIO, passing single threaded slaves around. GIO/GVFS mount daemons work very much like FUSE filesystems, and accept multiple client connections in parallel. Therefore this is just a matter of caching in the mount daemon process.\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> for the local file system it might also make sense to share code, as this would make the whole drive/volume and trash handling more harmonic\n\nFor \"mount\" operations probably yes, I was thinking about file data access, e.g. mmap vs. read/write access depending on application needs.\n\n> ... the next step would be running the mount operation.\n \nAfter reading the overview docs you had linked to earlier it is now clear that the mount operation does callbacks when it needs authentification data, so password/keystore integration would be possible in client library implementation, good.\n\n> Therefore this is just a matter of caching in the mount daemon process.\n\nYes, excellent. Also clear from the docs, should have read them earlier, sorry for that."
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "> For \"mount\" operations probably yes, I was thinking about file data access, e.g.\n> mmap vs. read/write access depending on application needs.\n\nProbably apps or wrapper layers are supposed to use a combination of \n\ngboolean g_file_is_native(GFile *file);\n\nand\n\nchar* g_file_get_path(GFile *file);\n\ni they want to work with mmap or O_RDWR (when done with the mounting etc...)\n\n\n\n\n"
    author: "Norbert"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-09
    body: "Right, this is what I have been referring to as local file access.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Trolltech changes to revamp KIO Architecture"
    date: 2007-12-06
    body: "> take the \"Plasma approach\" and not guarantee binary compatibility\n> in the KDE4 release.\n\nplasma is a new technology that isn't in kdelibs. libkicker *never* guaranteed binary compat, though the 3 classes in kdeui that kicker used were. it's a slightly different story to a 7 year old kio library that is used pretty much everywhere, works well and resides in kdelibs."
    author: "Aaron J. Seigo"
  - subject: "Re: Better improve FTP/SFTP KIO slaves than KFTP"
    date: 2007-12-05
    body: "Hopefully the mimetyp recognition will improve for \"remote\" protocols. (as it does not work well or at all in KDE3)"
    author: "ferdinand"
  - subject: "Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "The new oxygen sounds reminds me of Mac OS 7.\n\nhttp://websvn.kde.org/trunk/playground/artwork/Oxygen/sounds/new/\n\nThe sounds are all played on a keyboard synthesizer and all sound very similar, making it hard to tell the difference between and error and a notification. IMHO they don't have any relation to the topic they are trying to convey. The error and warning sounds sound too mellow. Doesn't anyone know how to do fancy effects ala shattering windows?\n\nAlso, the log in and log out sounds are way too long -- almost as bad as:\n\nhttp://www.youtube.com/watch?v=Mt1bgsvsWms"
    author: "Kory"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "I like the sounds, mellow is good. But I agree that some of them are too long and lack distinction."
    author: "Skeith"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "I like them too.  I listened to them cold, ie without reading the design yet, to get a clean first impression.\n\nfor i in *.ogg; do echo $i; sleep 1; ogg123 $i; done\n\nso i could see what they are supposed to be, and listen to them \"hands off\".  The first time I thought they sounded a bit Bontempi but that was through the X60's tiny speaker.  With desktop speakers they sound good, plenty of breadth to the sounds.  \n\n2 observations, forgive me if these are known about:\n\n*) The login/logout sounds are distorted at any level on my sound card.  It's the third to sixth notes played on the right (i think) hand at the beginning of the login sound that are too loud and especially distorted.\n\n*) The im.msg.in/out sounds are fine played alternately (try \"while true; do ogg123 im.msg.in.ogg; ogg123 im.msg.out.ogg; done\") but get monotonous when one side is not replying.  Could I suggest the maker does an additional 'in' and 'out' sound, slightly lower than the first one, that quality IM clients (Kopete ;)) can play alternately with the first one.\n\nThat way you would get a 'conversation' effect, read this in a fixed width font*, from left to right with each char representing one SFX:\n\n<tt>\nIn:   ^_   ^_^_^ ^_^_      ^_\n\nOut:    *.*.    *    *.*.*.    and so on.\n</tt>\n\n*I thought Dot could do some HTML - at least the posting form says it can."
    author: "Bille"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "Take a look at\nhttp://techbase.kde.org/Projects/Oxygen/Sound_Theme_Technical_Specification\nhttp://techbase.kde.org/Projects/Oxygen/Sound_Theme_Naming_Specification\n\nTheres a lot of thought, time, and effort gone into the sound theme by Nuno Povoa, so I think to dismiss them and say they have no meaning, because they sound similar is a bit unfair. I would give him the benefit of the doubt.\n\nPersonally, I think having sounds like shattering windows/glass would make the desktop sound like a cartoon, and Oxygen (including sounds) is trying to move KDE away from things like that.\n\nI do agree that the login/out sounds are *just* a bit too long, but i believe those are being shortend."
    author: "David"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "The Oxygen widget style is in my opinion moving KDE closer to a cartoon-like desktop: the buttons are too round and have a too big border, also the scrollbars are too heavy (and why is their color green?), the shading of the window background is also not reducing the cartoon-like effect, the fact that the title bar cannot be distinguished from the rest of the window gives the impression that usability has to make place for playful fanciness (if I want to move the window by dragging its titlebar, where do I have to click?).  The roundedness and the shadings in the Oxygen widget style does (in my opinion) not fit the Oxygen icons which have a more square-like look with sharp edges (the Oxygen icons are very good and look professional).  In my opinion, a style like QtCurve is much better suited for the Oxygen icons and looks also more professional (with \"professional\" I mean: non-cartoonish; the Oxygen widget style is nice and is clearly made by professionals, but it is not suited to be used in a professional environment).\n\nNow that I am giving my opinion on the look of KDE, I also want to say something about the color scheme: I think that a gray window background color is so old-fashioned, so Win95-like, that it should finally be replaced by some nicer color (any other OS, even windows, use better colors nowadays).  Some beige color (e.g. #F6F1E3) as the window background would be much better and would also look professional (this color also fits nice with the Oxygen icons).  I know that anyone can change the style and the color scheme, but we want to give a nice *default* look to KDE (one that does not repel potential new users; gray is repelling for me), don't we?"
    author: "anonymous"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "> if I want to move the window by dragging its titlebar, where do I have to click?\n\nnormally I click the top of a window for the titlebar.\nA good guess is often near the buttons or where the window title is shown.\n"
    author: "anonymouse"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "The Oxygengrey is quite different from Win '9x, so I think that argument doesn't go. I like the colorful parts contrasting with the white-grey, and I think it doesn't look unprofessional at all. At least more professional than XP and Vista and Mac OS X look... OK, maybe a bit less than plastik, but plastik was imho very corporate - guess that's why Gnome copied it ;-)\n\nAnyway, after some time using it, I really grew fond of it - to the point of looking for a good looking style for KDE 3.x and not finding one :("
    author: "jospoortvliet"
  - subject: "Yay for the cartoon-like desktop"
    date: 2007-12-05
    body: "It seem like KDE always are going to have the complainers stating it's too cartoonish. I'm not sure if the term was ever used about KDE 1, back then it may not have been invented yet.  \n\nBut at the first pre-releases of KDE 2, the complaing about the icons looking too cartoonish started. This also started to apply to the style, making the whole interface look childish and unprofessional. The old KDE 1 looks was much better in this regard.\n\nWhen KDE 3.1 was released with the new Crystal icons and the Keramik style the complaints was that it made KDE look too cartoonish. And that the old style and icons, looked much more professional. Later when the style was changed to Plastik, naturally it looked unprofessional being cartoonish and childish.\n\nNow with the artists hard at work with the new Oxygen style, there are no surprise it will become too caroonish. And thus unprofessional and childish. Compared to the old Crystal and Plastik, which look much more professional.\n\nTo remedy this I propose KDE change to Everaldo's Kids icons[1] as default, combining it with the creation of a matching style and window decoration. This will enable the KDE comunity to meet every complaint about the childish looks with a loud duh! And righteously ignore the silly complaint.\n\n[1]http://www.kde-look.org/content/preview.php?preview=2&id=9144&file1=9144-1.jpg&file2=9144-2.jpg&file3=&name=KDE+4+Kids\n"
    author: "Morty"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-07
    body: "This is so freaking unmotivating... Nuno Povoa did a wonderfull joob documentation included somthing kde never had, but all people can say is that its crap and cant point the reason for it....\nNo wonder people get fed up with this.\n\n  "
    author: "nuno pinheiro"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-09
    body: "its my understanding all of this can be changed, so why would anyone really care? you have the option to change it so you do, end of story. NOTHING in this world will appeal to everyone, dont like the theme? make one you feel is better. done."
    author: "mike"
  - subject: "Why reinvent the wheel?"
    date: 2007-12-05
    body: "Interesting point raised. I started to think about the very popular Borealis sound theme on KDELook (http://www.kde-look.org/content/show.php/%22Borealis%22+sound+theme?content=12584&PHPSESSID=41f5909f9dd8c995f96379c8343c825b). Couldn't this theme have simply been implemented for 4.0 instead of a new one? It would've been much easier. Hopefully there will be an easy way to install alternate sound themes in KDE 4."
    author: "Darryl Wheatley"
  - subject: "Re: Why reinvent the wheel?"
    date: 2007-12-05
    body: "ACK because I'm also using Borealis. But if someone did import it, the window sounds should be disabled by default. I like dialogues being shown with a *plop*, but surely not anyone wants this."
    author: "Stefan"
  - subject: "Re: Why reinvent the wheel?"
    date: 2007-12-05
    body: "\"But if someone did import it, the window sounds should be disabled by default.\"\n\nIIRC, this was discussed on #oxygen and it was decided that, yes, window sounds will be disabled by default."
    author: "Anon"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "I would prefer some discordance(?) when something is wrong. And the log in and out was too similar (As long as the shutdown/startup process doesn't pause while it plays the sound, I don't really care much for the length).\nOther than that, I thought it sounded kind of professional;)"
    author: "martin"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "I don't want to sound harsh, but the Oxygen sound theme is mediocre at best, especially when compared to Vista and OSX counterpart. Just my opinion."
    author: "AC"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-05
    body: "and your suggested improvements are?"
    author: "Borker"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-07
    body: "Realy #oxygen is allways open the sound theme must have been in playgroung for as long as I can remember I anounced it at a blog entery and the only feed back we got was extremly positive, and now you guys decide its mediocre???? Like where were you?  and where are yours sounds giii ....    "
    author: "nuno pinheiro"
  - subject: "I'm really scared!"
    date: 2007-12-05
    body: "Call me stupid (I hope I am), but are these sounds actually going to sound like this? It's dreadful! Sounds like a kid's keyboard. Please tell me the actual sounds are going to be played on something different . . . ?\n\nOh and I agree that it wouldn't hurt at all for them to be played faster."
    author: "Rod"
  - subject: "Re: Oxygen sounds have no meaning, too long"
    date: 2007-12-10
    body: "Sounds that sound similar have a name to it: consistent.\n\nthere is *no* problem differentiating the sound for an error (ANY error) to a notification (ANY notification)...you can even figure out how serious an error is just by the sound that's played. if didn't listen to the sounds one by one and actually setted up your current KDE enviornment with them you'd realize that. (of course you don't like the sound theme, so you might be better off stopping by kde-look.org and pick something else that doesn't make you cringe)\n\nyes, the error sounds are mellow, all sounds *are* mellow. it was sort of a requirement.\n\nnow as to the shattering window...I will try not to curse as I explain this:\n\nWhat is so amazing about blushing when that sound is played during a meeting/class/whatever by accident, the little awkward giggle and hiding your head in your shoulders as you realize that sound is outrageously annoying to anyone in the room but you (the only one hearing it in context). Because that's really the gimmick here: can you imagine an open space with 20 or 30 KDE machines playing those sorts of sounds through the speakers? could you work? could you be productive?\n\nas to the length of the login and log off sound it will be shortened, since it was brought to my attention that the login time into KDE is much much shorter then it used to be. also, the long login sound was a requirement from the team, since there was some musing as to creating some sort of promo for KDE based on the login sound (and then some).\n\nkudos on being silent during the development time and speaking your mind 1 month prior to release.\nthe specs have been on KDE's techbase since the early development of it, the sounds available on SVN, for the first few months of development I was at #oxygen on freenode, I've always been more then keen to reply to email and welcomed any good ideas that were proposed. 1 month from release...you better have a pretty clear idea of what you want to see changed. this is hardly the time to brainstorm.\n\nCheers,\nnpovoa"
    author: "Nuno Povoa"
  - subject: "Thank you Danny !"
    date: 2007-12-05
    body: ":)"
    author: "shamaz"
  - subject: "Re: Thank you Danny !"
    date: 2007-12-05
    body: "+!"
    author: "Emil Sedgh"
  - subject: "Applet Designer"
    date: 2007-12-05
    body: "I don't know how to contact Tim Beulen, but his applet designer concept sounds a lot like something I posted recently:\n\nhttp://kde-look.org/content/show.php/Another+take+on+Object-Oriented+Desktops?content=69144\n\nIt would be good to work together on this.\n"
    author: "Lee"
  - subject: "Re: Applet Designer"
    date: 2007-12-05
    body: "You can find me on irc, nickname tbscope and usually in the channels #kde4-devel, #plasma and others."
    author: "Tim Beaulen"
  - subject: "Tasty Menu"
    date: 2007-12-05
    body: "It looks, well... tasty :)\nJust one thing: is there overlap between the Tasty Menu concept \"Favourite applications\" and kcontrol's \"Default applications\", and do/should they share a common implementation?"
    author: "Rob"
  - subject: "Re: Tasty Menu"
    date: 2007-12-05
    body: "There is no overlap.\n\nFavorite applications list apps that you'd like to run a lot. This are your own choice of applications that you'd want to be easily accessible, so you put them in an easy to reach place or list. Think of it as \"My Favorites\" or \"Most Frequently Used\".\n\nDefault applications are the applications that the system will run for a given task. For example, if you have multiple web browsers installed, but you want one to be the default, primary browser, you set that in Default Applications. So whenever a web browser is needed/called, it will launch that browser you set."
    author: "Jucato"
  - subject: "Re: Tasty Menu"
    date: 2007-12-05
    body: "the default applications in the favourite list are konqueror and kmail only as an example, but you can put every application you want in that list, so it's a little bit different"
    author: "Mart"
  - subject: "Where's the code for the Fifteen Puzzle?"
    date: 2007-12-05
    body: "Where can I find the code for the fifteen Puzzle applet?  I'd like to take a look at it (I suspect it has a bug), but I can't find it via the websvn interface (which desperately needs a search option!).\n\nThis is where I looked: http://websvn.kde.org/trunk/KDE/kdebase/workspace/plasma/applets/\n"
    author: "Cultural Sublimation"
  - subject: "Re: Where's the code for the Fifteen Puzzle?"
    date: 2007-12-05
    body: "Very new apps generally start in playground:\n\nhttp://websvn.kde.org/trunk/playground/base/plasma/applets/fifteenPuzzle/\n\nAnd yes, a search option in WebSvn would be heaven :)"
    author: "Anon"
  - subject: "Re: Where's the code for the Fifteen Puzzle?"
    date: 2007-12-05
    body: "Try googleing: \"site:websvn.kde.org appyou'relookingfor\"."
    author: "martin"
  - subject: "Re: Where's the code for the Fifteen Puzzle?"
    date: 2007-12-05
    body: "I don't know which bug you found, but I did find one bug/gotcha just by taking a cursory look at the source code: it seems that method Fifteen::shuffle() uses a naive algorithm to shuffle the tiles.  It essentially inserts tiles at random, only making sure the same tile is not used twice.  Now, if I remember my kindergarten days correctly, the Fifteen Puzzle has this interesting property that only half of random starting positions are solvable...\n\n(The easiest way of guaranteeing that a Puzzle is indeed solvable is perhaps by simply starting from the solved solution and iteratively moving one piece at a time at random).\n\nAnyway, has anyone ever actually tried to solve the Fifteen Puzzle plasmoid?  If I'm right you should get stuck 50% of the times.  (Excuse me while I file a bug report...)\n"
    author: "Dario"
  - subject: "Re: Where's the code for the Fifteen Puzzle?"
    date: 2007-12-10
    body: "All positions are easily solvable.\nAnd yes, you're right, it is more a game for the kindergarden.\nIt's too easy for an adult brain.\n"
    author: "abc"
  - subject: "Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "It's bad enough that the preferences panels in KControl/System Settings barely fit on a 1024x768 screen, but now even a Start menu that takes so much space? Well, as long as it's optional I'm fine with it, but hopefully that one doesn't become the default some day..."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "If you think about it: what's wrong with a full screen start menu? It's not like you'd need to use a start menu next to another application, is it? I actually like this start menu on KDE 3. But don't fear: it is not the default for KDE 4. In fact, if I read the Digest correctly, the port to KDE 4 hasn't even really started."
    author: "Andr\u00e9"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "1. It's configurable\n2. Kickoff can take up as much space too (at least in Kickoff 3 and earlier incarnations of Kickoff 4)\n3. It's not the default for 4.0\n4. If it becomes the default, you know you have other alternatives existing and or coming.\n\nI once thought that way too, that big main menus were crazy. I even thought Kickoff was too big. But after reading the Usability studies done on the menu system (by our very own Celeste Lyn Paul, no less), I got thinking differently. It's a good read, not difficult to understand at all.\n\nhttp://student-iat.ubalt.edu/classes/paul/documents/infm702/INFM702_Menu_Systems_Presentation.pdf"
    author: "Jucato"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "I just read through the presentation, but I did not see anything about bigger menu's being better. While I don't personally object to big menu's, I don't think it's a good idea to point to sources who do not support these ideas. Furthermore, I think this particular bit of research is very questionable material to use. The issues mentioned on slide 16 are very real and severaly take away from any value the research may have. Personally, I don't see a lot of substantial suggestions or pointers for the actual design of a start menu here. I think that for that, work based on averages of self-indicated preferences isn't a good method to use. "
    author: "Andr\u00e9"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "Mea culpa on the wrong resource. I made a research before on studies done with different menu systems, this one was one of the sources. Unfortunately it seems that I have lost the others. Some of the points I do remember are the following (I will just try to recall straight from memory, so don't quote me on these):\n\n1. Fitt's law, giving you a larger target area, or something like that. :D\n2. User use the main menu (K Menu, GNOME menu, Start menu, etc) more than just to launch apps. They use it browse for installed apps, go to important system locations (system, places) and settings (control center), or log out. Basically the main menu is a portal to their computer, not just a simple menu like the File or Edit menu in applications.\n3. Important information (or apps) at a glance, without having to go through horizontal cascading menus (this seems to be bad, usability-wise, still trying to research on that).\n4. When users use the main menu, they totally focus on that menu, not the other windows on the desktop. Just as you wouldn't want your current focus of attention to be as painfully small as possible, there is not reason (aside from \"minimalistic\" preferences) that the main menu should be just as small.\n\nThese factors (taken together) lead to an implementation of a main menu that is  (reasonably) large enough to accommodate such information. At least that's how I understood what those studies say. Unfortunately, I lost almost all my resources except that one.\n\n>> Furthermore, I think this particular bit of research is very questionable material to use. The issues mentioned on slide 16 are very real and severaly take away from any value the research may have.\n\nThe study was done by a usability expert, a member of openusability.org, an Information Architecture student, a member of the KDE 4 HIG working group, and one who does usability for a living. Forgive me if I trust her work and methods more than any other person who shouts \"usability\"."
    author: "Jucato"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "But as Andr\u00e9 mentioned, the \"Limitations of Study\" slide (no. 16) do take away from the value of the research. Notably: \n\n-Number of survey participants were not statistically significant\n-GOMS modeling is limited in its application\n-Method of delivery may have limited participation, and\n-the time constraints.\n\nSure, she does it for a living, etc. but even she acknowledges her study is incomplete and lacking. It's better than nothing, and probably better than the average person could come up with (with the same constraints), but it's not infallible.\n\nThe main problem I see with a large menu is searching for information, as in scanning it with your eyes. Professionally typeset documents - books, etc. - tend to have narrower regions for information. As far as I understand, this makes it easier to read and retain information, because you can see an entire line - all the immediate context -  without moving your eyes."
    author: "Soap"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "Well, in kde3, tasty menu doesn't take ALL my screen, and it's a shame. I installed it because it seemed to fullfill this, but was disapointed.\n\nSo I tried to maximise it, but it would automaticaly scale down...\n\nSo I returned to  old Kmenu."
    author: "kollum"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "Right click -> configure -> set width and height to 100%."
    author: "martin"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-05
    body: "I wanted to have a menu that takes a fixed amount of screen rather than the minumum possible like the default kmenu, where if there is a garzillion of installed applications some subcategories can grow to be much bigger than tasty menu is.\nbtw, the default is 70% of the screen width and 70% of screen height, in 1028x768 it's still pretty usable setting 50% and 50%\nbut after all it's a matter of well, taste :)"
    author: "Mart"
  - subject: "Re: Wow, a fullscreen Start Menu"
    date: 2007-12-06
    body: "TastyMenu hasn't even started a port to KDE4, has never been part of the primary KDE trunk, and isn't likely to be the default anytime soon.  It is just an option out there.\n\nFrankly, I don't care for the look, and scroll-bars within a start-menu just bother me.  They are my least favorite part of the Vista menu.  Again, I think some people design for what will look good, as opposed to what works well from a usability standpoint.\n\nWMP 10 and 11 look great in a screenshot, but are a pain to use.  Many design elements of Vista go along these lines, including the new menu.  It looks cleaner to have the menu take up a set amount of real estate and not spill all over the place.  But it is simpler to navigate and unfold the menu as you need it, rather than go over to the scoll bar, go back to the menu, go back to the scroll bar, etc."
    author: "T. J. Brumfield"
  - subject: "KDevelop 4?"
    date: 2007-12-05
    body: "Now that KDevelop 3.5 is mentioned, I wonder what happened to KDevelop 4. http://www.kdevelop.org/mediawiki/index.php/KDevelop_4 doesn't give a lot information about its current status."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: KDevelop 4?"
    date: 2007-12-05
    body: "http://techbase.kde.org/Schedules/KDE4/Application_Porting_Status says the port is done and work on a development releases is going on."
    author: "Sebastian Sauer"
  - subject: "Re: KDevelop 4?"
    date: 2007-12-05
    body: "You can \n  * read the blogs of Andreas Pakulat or Adam Treat(for example):\n      http://apaku.wordpress.com/\n      http://www.kdedevelopers.org/blog/105\n  * read www.planetkde.org (not everything is related to kdevelop of course)\n  * read kdevelop-devel mailing list archive :\n      http://lists.kde.org/?l=kdevelop-devel&r=1&w=2\n\ngood luck"
    author: "shamaz"
  - subject: "tasty menu"
    date: 2007-12-05
    body: "i really like the way tasty menu works. one thing i don't like about most GUIs is that you get a lot of popups (menus, message boxes, etc) which should be as few as possible. for example in many applications (kde or not) when an error occurs you usually get an error box that stops the flow of your work. at least for the non-critical errors there should some kind of bar or something that lists everything that happened. (this is also useful if you get many errors/warnings and you can't remember them)"
    author: "voicu"
  - subject: "Kickoff - very good menu"
    date: 2007-12-06
    body: "initially i started as kickoff hater, but after using it for a while in opensuse (kde3) i find it absolutely essential. it's really an all in one place for information/devices/application in your system.\n\nthe only place kickoff seems annoying is finding application like the old win95 start menu.\n\nbut there is a much better and easier way..\n\njust type \"player\" (or part of the name) to find all media players.. or type \"browser\" to find all browsers installed, which is the fastest way to find an application by name or by application type.\n\nkickoff for kde4 needs a few improvements like in kde3's version and it will become one of the most loved application of kde4.\n\nhopefully implemented by kde4.0\n\n1. resizeable kickoff like kde3.\n2. clicking on applications button/tab will bring to the Top-Level of application menu.\n\nother usability feature i would like to see is:\n\n1. the right-left sliding is rather slow and hence visually annoying.. it can be sped up or no slide-animation or some fast fade-in animation would be nice.\n\n2. the < top-level back bar on the left, should blink to inform the user that the parent menu is on the left side.\n\n3. and a dolphin like location-bar on top  for \"application tab/button\" should be implemented for kickoff4 too, which allows quicker access to top-level folders/menus. when implemented, users can know where in the menu they are.\n\nthanks aaron, and kde4 team for making kde4 cool :)\n\nps. just before you jump to slaughter kickoff, please use it with opensuse's kd3 for at least a few hours.. and feel the difference ;-)"
    author: "fast_rizwaan"
  - subject: "Re: Kickoff - very good menu"
    date: 2007-12-06
    body: "I agree.  In fact they just added the \"old\" style menu back as an option in svn. \n\nI found that I could no longer stand it.  I much prefer kickoff now."
    author: "JackieBrown"
  - subject: "Re: Kickoff - very good menu"
    date: 2007-12-06
    body: "Step 1 - Click in field.\nStep 2 - Type in \"player\"\nStep 3 - Wait two seconds for it to search\nStep 4 - Look through the list for mplayer, kplayer, etc.\nStep 5 - Click on the entry you need.\n\nOr...\n\nUse a standard menu, hover to kplayer in about 2 seconds.\n\nOr...\n\nHit Alt-F2 and type kplayer\n\nKickoff is easily the worst of these three options.  Again, you say search is a big benefit.  openSUSE proved you can add search to the old style menu.  Raptor and Lancelot will have search.  People aren't hating on search.  Add search options for those who want to use them.  It is the clicking, and navigation style of Kickoff that is horrible.  There is no reason why you can't have the preferred old navigation style, and also include search.\n\nAnd the other thing that gets me is how we've sacrificing usability in one of the most commonly used part of the entire desktop for what?  Search within the menu is something I can't imagine will get a lot of use in the long run.  If you are new to Linux and KDE, and need to find an FTP app, it might be useful to search for what FTP apps you have installed with your distro.  But in the long run, eventually you'll know what FTP app you have, and just start it rather than searching for it every time.  And if we're conditioning people to search every time, well that is just stupid.\n\nHow did I live without search before?  I just hovered and navigated to the appropriate hierarchy of the menu and just looked at what was installed."
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff - very good menu"
    date: 2007-12-06
    body: "my friend, the 3 methods you've talked about applies to:\n\n1st - to the newbies to KDE/unix\n2nd - to ex-windows users 9x or 2k or xp\n3rd - experienced Linux/Unix users who knows the name\n\nshould the menu only work for one type of user base? experts as you are or should also consider the not so informed users about menus and computers?\n\nsearch helps the new users\nold menu style - application tab - for intermediate users\nalt+f2 (with search) -> for experts.. who has better memory remembering odd names of application.\n\nanyways, i appreciate your concern.. take care :)\n\n"
    author: "fast_rizwaan"
  - subject: "History support in KRunner"
    date: 2007-12-06
    body: "Aaron, why did you implement it?!? Do not listen to all those who are whining about plasma... history in Krunner dialog is absolutely useless, because in KDE3 we didn't have the results preview as we have in KDE4, so history was needed. Now with history support in KDE4 we have 2 visual ways to complete the same task competing in the same tiny windows.\n\nI hope you get my comment right, I'm not whining, I just think that history's absence wasn't a regression at all."
    author: "Vide"
  - subject: "Re: History support in KRunner"
    date: 2007-12-06
    body: "\"I just think that history's absence wasn't a regression at all.\"  \n\nAnd its addition isn't a detriment.  I'm a big fan of adding handy little features, *especially* if they do not clutter the interface or make things harder for \"new\" users.  Thanks Aaron!\n\nOn this topic - I hope someone could be persuaded to re-add the \"Add Place\" to the right-click menu in the Places sidebar in the File Dialogue, which was apparently removed for \"usability\" reasons.  The functionality is still there - you just have to *drag and drop* a folder onto the bar rather than the far more discoverable act of right-clicking on it (I only found out that the functionality had not been removed completely by reading the mailing lists!).  Not many new users even right-click on that sidebar, and I'm sure very, very few would be confused by a single entry saying \"Add Place\".  *sigh*"
    author: "Anon"
  - subject: "Congratulations"
    date: 2007-12-08
    body: "I am using kde4daily to test as evolving development kde4 and I am really impressed.\n\nMy most sincere congratulations to all the team.\n\n"
    author: "Jesus R. Acosta"
---
In <a href="http://commit-digest.org/issues/2007-12-02/">this week's KDE Commit-Digest</a>: The beginnings of screen hotplug detection in <a href="http://plasma.kde.org/">Plasma</a>, KRunner gets history support. Fifteen Pieces puzzle becomes the first Plasma applet in the game category. A block of bugfixing in <a href="http://www.kdevelop.org/">KDevelop</a>, with various other developments in areas such as a threaded debugger. Support for inequality constraints in <a href="http://stepcore.sourceforge.net/">Step</a>, continued progress in the port of <a href="http://edu.kde.org/keduca/">KEduca</a> to KDE 4. Work on printing in <a href="http://okular.org/">okular</a>. Work on <a href="http://solid.kde.org/">Solid</a>-based network management through NetworkManager. Various work towards <a href="http://amarok.kde.org/">Amarok</a> 2. Milestones reached in the BitTorrent plugin for <a href="http://kget.sourceforge.net/">KGet</a>. Subsystem rewrites (SSL, SFTP) in <a href="http://kftpgrabber.sourceforge.net/">KFTP</a>. OpenDocument format loading and saving work in <a href="http://koffice.kde.org/kchart/">KChart</a>. Colour work in <a href="http://www.koffice.org/krita/">Krita</a>, with Krita becoming one of the first applications to be able to paint in HDR. New <a href="http://oxygen-icons.org/">Oxygen</a>-themed sound effects, Oxygen icons are optimised for small sizes. New colour schemes added for KDE 4.0. Ruby language bindings based on the Smoke2 framework. Experiments in KBugBuster and on a Plasma "applet designer" application. <a href="http://commit-digest.org/issues/2007-12-02/">Read the rest of the Digest here</a>.

<!--break-->
