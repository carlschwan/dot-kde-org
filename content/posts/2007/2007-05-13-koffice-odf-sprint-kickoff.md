---
title: "KOffice ODF Sprint Kickoff"
date:    2007-05-13
authors:
  - "jpoortvliet"
slug:    koffice-odf-sprint-kickoff
comments:
  - subject: "Make .odf in Koffice compatible with OO.org"
    date: 2007-05-13
    body: "\"It is important to create a good infrastructure to support ODF throughout KOffice, so developers won't have a hard time getting their apps to use it.\"\nnice plans but firstly make koffice-odf to be 100% like openoffice-odf. Now we have sad sad situation when making huge text in OO.org, saving as .odf and Koffice won't handle it correctly ;/"
    author: "Dolphin rulez ;)"
  - subject: "Re: Make .odf in Koffice compatible with OO.org"
    date: 2007-05-13
    body: "IIRC both OO.org and Koffice implement a differnet subset of the specification. i trust however that the developers of both projects are aware of this issue and its importance."
    author: "Andreas"
  - subject: "Re: Make .odf in Koffice compatible with OO.org"
    date: 2007-05-13
    body: "And vice-versa, really. If you open a perfectly conforming KPresenter odp document in OpenOffice you'll see OpenOffice drops a lot of balls there. There is only one ODF -- but no application completely supports it yet. We are more aware of that issue than the OpenOffice developers who tend to think that OpenOffice implements the standard completely. The thing is, In any case, I finished the last bit of the loading infrastructure tonight on the train home, so now we \"only\" have to implement loading everything :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: Make .odf in Koffice compatible with OO.org"
    date: 2007-05-14
    body: "The order to achieve better odf compatibility a good infrastructure is very important. It's better to spend some time to build a good base instead of fixing everything later. Also the odf spec is pretty big and it will take very long to implement it, so this doesn't make a big difference.\n\nThere is a testsuite: http://testsuite.opendocumentfellowship.org/\nThis is in flux, so it can be expected that compability will get better and better in 2.x"
    author: "Sven Langkamp"
  - subject: "who is this guy"
    date: 2007-05-13
    body: "eh who is this cute guy with the glasses"
    author: "tooooshy"
  - subject: "Re: who is this guy"
    date: 2007-05-13
    body: "Not being sure what's cute in a guy, and my daughters having gone to sleep so I cannot ask them, that would be either (reading from left to right) Pierre, Martin, Thorsten, me or Sebastian :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: who is this guy"
    date: 2007-05-13
    body: "in the last pic !!!"
    author: "tooooooooshy"
  - subject: "Re: who is this guy"
    date: 2007-05-13
    body: "Ah, that's Martin, the winner of the KOffice UI redesign competition and KFormula hacker."
    author: "Boudewijn Rempt"
  - subject: "reuse "
    date: 2007-05-13
    body: "It would be nice to reuse the scripts on this page to make an annotated image.\n\nhttp://static.kdenews.org/jr/akademy-2006-group-photo.html"
    author: "Mr Reuse"
  - subject: "Re: reuse "
    date: 2007-05-14
    body: "check the KOffice ODF sprint page, it has names..."
    author: "superstoned"
  - subject: "poor kplato"
    date: 2007-05-13
    body: "it is very sad, apart from dag no one loves kplato"
    author: "djouallah mimoune"
  - subject: "Re: poor kplato"
    date: 2007-05-13
    body: "I think KPlato is a great tool. I HAD to get something like it few years ago, and I ended up with Gnome Planner.\nNow I'm looking forward to see new features and development of KPlato, because I know sometimes you just need it. And for me, it's a great feeling to know KPlato is around.\nBIG THANK YOU\nBIG HUG to all of the KOffice TEAM :)"
    author: "ZEM"
  - subject: "Re: poor kplato"
    date: 2007-05-13
    body: "i tried them all, gnome planner; taskjugler and kplato and to be honest they are in the enfancy stage ( to be kind) but what to say this life. and we can't moan about a  developper for his free work. hope perhaps some proprietary software can release their code, and manage to gain money per service.\n\n\n "
    author: "djouallah mimoune"
  - subject: "Re: poor kplato"
    date: 2007-05-14
    body: "djouallah, (almost )every time you post in here is to say how much kde apps suck and how this or that is still beyond windows equivalent. It must be a torture for you to use KDE as (almost) every single app you use is inferior to what you use on windows. To me all the apps I use are way better (amarok, konqueror, kopete, k3b, katepart, konversation, emacs :-D, kio (++ for fish:/), ssh,  akregator, kword, kmix, knetwork, krita etc etc etc).\nI have one question for you, why do you keep using KDE if windows beats it at every level???"
    author: "Patcito"
  - subject: "Re: poor kplato"
    date: 2007-05-14
    body: "eh who say that, the only fields where linux sucks for me is project management software, where simply they don't exist( i am speaking in the context of entreprise use ) and of course games but fortunately i am not a gamer( although i wa such deceived to see that blob and conquer is a great game but it can't use dri in my 945g)\n\nand to answer your question, i just love it, "
    author: "djouallah mimoune"
  - subject: "Re: poor kplato"
    date: 2007-05-14
    body: "There were students who did work on KPlato as their university project, and brought quiet a lot of improvements. And one of them is still contributing."
    author: "Cyrille Berger"
  - subject: "Re: poor kplato"
    date: 2007-05-14
    body: "i just think that some  software like project management are very complicated and need a support from a whole company to make them sustainable ( hehehe i don't know where i get this word) "
    author: "djouallah mimoune"
---
The day before the real start of the <a href="http://www.koffice.org/sprints/odf1/">KOffice meeting in Berlin</a>, most developers had already arrived. After checking in and having dinner, they started hacking away at the <a href="http://www.klaralvdalens-datakonsult.se/">KDAB</a> office. Read on to learn about how this went and the plans the developers have for KOffice 2 and the coming weekend!



<!--break-->
<p>
<div style="float: right; padding: 1ex; ">
	<a href="http://static.kdenews.org/dannya/teamlarge.jpg">
		<img src="http://static.kdenews.org/dannya/teamsmall.jpg" border="0" 
		title="The KOffice ODF Team"
		alt="The KOffice ODF Team" />
	</a>
</div> 

After arriving and checking in, Till and several other <a href="http://www.klaralvdalens-datakonsult.se/">KDAB</a> people bought us dinner and then took us to their Berlin office. The hackers enjoyed seeing each other in real life (again), so they had fun and some beverages where consumed, but all this talking didn't keep the hackers from hacking.

<p>
Developers talked about the progress in several areas of <a href="http://koffice.kde.org">KOffice</a>. According to <a href="http://www.kdedevelopers.org/blog/91">Thomas Zander</a>, KOffice currently has almost all the features that version 1.6 had, and much more already. <a href="http://www.valdyas.org/fading/index.cgi">Boudewijn Rempt</a> tempered this optimism a little, and discussed how some applications aren't ready yet for porting to the new infrastructure. Talking to <a href="http://www.gfai.de/~jaham/">Jan Hambrecht</a>, the <a href="http://koffice.kde.org/karbon">Karbon</a> maintainer made clear how Qt 4 is helping development. Karbon is a vector graphics application, and the new graphical features introduced in Qt 4.3 can do most of the heavy lifting in Karbon.

<p>
<div style="float: left; padding: 1ex; ">
	<a href="http://static.kdenews.org/dannya/lockinlarge.jpg">
		<img src="http://static.kdenews.org/dannya/lockinsmall.png" border="0" 
		title="They lock us in the office..."
		alt="They lock us in the office..." />
	</a>
</div> 
<a href="http://ariya.blogspot.com/">Ariya Hidayat</a> was made happy by Boudewijn, when Boud offered to let the <a href="http://koffice.kde.org/kspread">KSpread</a> developers meet in his house. They would be able to get together for 2 days to discuss some important architectural issues for KOffice 2. <a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office">ODF</a> support, memory usage and some other issues are just a bit too complex to solve through mail or IRC, and discussing them face-to-face is much more efficient.

<p>
<h1>Plans</h1>
What are the KOffice developers planning to work on, or what do they want to discuss with their fellow hackers? <a href="http://behindkde.org/people/ingwa/">Inge Wallin</a> explained his main goals for the Berlin meeting in an email sent a few days before the meeting started. The big target for the meeting is ODF. First, the KOffice hackers will go through the current ODF support and try to improve it. It is important to create a good infrastructure to support ODF throughout KOffice, so developers won't have a hard time getting their apps to use it. The second target for the meeting will be the integration of ODF in KDE. The plan is to create a library which will make it easy for any application in KDE to have basic read and write support for Open Document files. Writing such a "Phonon" or "Solid" for ODF won't be an easy task, so the KOffice developers invited <a href="http://pim.kde.org/people.php">Tobias K&ouml;nig</a> from <a href="http://kpdf.kde.org/okular/">Okular</a> fame, as this application will be one of the users of this library. Also, although <a href="http://aseigo.blogspot.com/">Aaron Seigo</a> was unable to attend, he promised to be available through Skype or other real-time communication means, and the KOffice team of course has <a href="http://www.kdab.net/~dfaure/">David Faure</a> and Thomas Zander who are experienced library hackers.

<p>
<div style="float: left; padding: 1ex; ">
	<a href="http://static.kdenews.org/dannya/happylarge.jpg">
		<img src="http://static.kdenews.org/dannya/happysmall.png" border="0" 
		title="Happy Hackers"
		alt="Happy hackers" />
	</a>
</div>

But of course this is not all. Martin Pfeiffer and <a href="http://alfredobeaumont.org/blog.cgi">Alfredo Beaumont</a>, both proud <a href="http://koffice.kde.org/kformula">KFormula</a> hackers, are thinking about the details of their ODF/MathML infrastructure. Though Martin got the <a href="http://techbase.kde.org/Development/Tutorials/Write_a_Flake_Plugin">flake plugin</a> to initialize months ago, only last night did Alfredo manage to get it to render something. They are still very much working on the basics, as MathML, the open formula format, is a very big and complex piece of work, and fully supporting it is really hard. Still, they aim to surpass KFormula 1.6, which had the most elaborate MathML support in the Free Software world.

<p>
Jan Hambrecht is also thinking about ODF and Karbon. He is awaiting the results of the design talks planned for Saturday, as the ODF infrastructure they plan to discuss and build will enable him to get Karbon support to ODF quickly and properly. Writing it himself would take far too much time, but in good KDE tradition, he expects the common framework they will build together to do a lot of the hard work for him. After effective ODF support, he can start porting all current export and import plugins to the new structure, ensure there are no regressions compared to the 1.6 release, and add a few useful new features.

<p>
<a href="http://annma.blogspot.com/">Anne-Marie Mahfouf</a>, mainly a <a href="http://edu.kde.org">KDE-Edu</a> developer, is listening with rapt attention to the discussion about ODF. She told me how she has been thinking about ODF as a unified solution for the several XML-based fileformats they use for data in the Educational applications. Aside from this and all other ideas she has about the development of educational and scientific applications for KDE, she is present at the meeting for testing, bug hunting and documentation. Thanks to Anne-Marie and Pete Murdoch, you can view the <a href="http://koffice.org/sprints/odf1/">details of the ODF meeting online at the KOffice website</a>.

<p>
We want to thank the sponsors, <a href="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens Datakonsult AB (KDAB)</a>, <a href="http://www.trolltech.com/">Trolltech</a> and the <a href="http://ev.kde.org/">KDE e.V.</a> for their support. Trolltech is sponsoring the travel costs of the ODF Meeting through KDE e.V. and KDAB is not only giving us their office to use, but is also sponsoring our food. Without their support organizing this event would have been much more difficult. Of course, we're looking forward to the next two days!
