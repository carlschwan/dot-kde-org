---
title: "The Road to KDE 4: KWin Composite Brings Bling to KDE"
date:    2007-05-30
authors:
  - "tunrau"
slug:    road-kde-4-kwin-composite-brings-bling-kde
comments:
  - subject: "Only composites?"
    date: 2007-05-30
    body: "Ok, it's nice to see, how much kwin have grown on the optical side, and there are very usefull things, but, is this all? IIRC there were plans to implement features like the window-handling as seen in ratpoisen(?), ION and wmii, or tabbed windows like fluxbox have them. Also, there were talks about some layer-things, but i don't remeber if this were for kwin or for plasma."
    author: "Under7"
  - subject: "Re: Only composites?"
    date: 2007-05-30
    body: "KDE has had \"tabbed windows\" since the B-II window decoration was created in KDE 2.0.  It's one of those nifty features that no one used, so it never really evolved from there.  I still think it's kind of neat...\n\nThere are other things happening in KWin, it's just not the focus of this article.  I have several months of weekly articles still to write before 4.0 comes out, so I need to leave myself some material :)"
    author: "Troy Unrau"
  - subject: "Re: Only composites?"
    date: 2007-05-30
    body: "BII has nothing to do, with tabbed windows. It's also really ugly, and seems a bit buggy. The advantage of tabs, is the possibility to group windows, but BII does'nt do this, it handle all windows the same, and waste space."
    author: "Kami"
  - subject: "Re: Only composites?"
    date: 2007-05-30
    body: "Well, that depends on how you define tabs.  It has window titles that auto-arrange themselves like the tabs on top of folders, assuming they are lined up to be the same size.  Tabs are still just a metaphor for the folder tabs you find in the common filing cabinet, and BII's titlebar arrangement fits this requirement.  That said, BII is in a pretty bad state of affairs these days..."
    author: "Danny Allen"
  - subject: "Re: Only composites?"
    date: 2007-05-31
    body: "I don't think it's really in bad shape. There are a few things that may be improved, but the only real problem I find in it is when the composite extension is enabled. I just noticed this when trying composite on my new laptop, since composite has never really worked on the desktop PC.\n\nThe problem is that BII is the only KWin decoration that can change window shape dynamically, and with composite enabled, the title bar is badly clipped when the window title changes, or it is moved around with shift-click.\n\nComposite also prevents the titlebar unhide code to work.\n\nI'll have to find a fix for those problems, but it's not been high priority for me, and I'd have to find out more about how composite works.\n\n"
    author: "Luciano"
  - subject: "Re: Only composites?"
    date: 2007-05-31
    body: "B-II is great, I use it on each of my computers since KDE 2.0 and I hope it will still be there (hopefully improved) in KDE4!"
    author: "Renaud"
  - subject: "Re: Only composites?"
    date: 2007-06-08
    body: "Not tabbed as in graphical widgets, but tabbed as in functional capabilities with semantic grouping, tagging and scripting.  KDE 3.x works well with ion3, by the way, and ion3 provides much of what I wanted that the 3.x series KWin couldn't provide... and I pretty much maxed out the capabilities in terms of scripting and settings for KWin 3.x.\n\nPersonally, I don't really think KDE's default Window Manager really needs to have such advanced capabilities.  KDE should just to make sure that the apps work well within the X standards so that they work with alternate window managers that do support such capabilities.  After all, dcop and ion3's lua scripting is a heck of a powerful combo I'd hate to give up in KDE4 (other than a transition to dbus)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Only composites?"
    date: 2007-07-14
    body: "Correct me if I'm wrong, but Konqueor is tabbed. I can view any part of Kcontrol (via \"settings:/\") in one tab, a text file in another, a video in another, a website in another, any part of my filesystem in another,fish://.. in another (and then some) simultaneously."
    author: "Scott"
  - subject: "Re: Only composites?"
    date: 2007-12-05
    body: "Uhm, we're talking about window management, not browsers, here."
    author: "spoons"
  - subject: "Re: Only composites?"
    date: 2007-05-30
    body: "> and there are very usefull things, but, is this all?\n\nYes, it opens a whole new world of options!\n\nfor people with disabilities:\n- dim inactive windows\n- zoom windows\n- apply a negative effect over the whole desktop in real-time.\n\nfor usability:\n- The real-time Alt+Tab / taskbar thumbnails make it easier to find windows.\n- The desktop grid / beryl cube make it much easier to grasp the concept of virtual desktops.\n- Subtle shadows and gray-out make windows easier to distinguish (like what's in front)\n- The shadows and window open/close effects make it easier for new computer users to grasp the concept of windows in general.\n\nAnd for speed improvements:\n- Windows no longer need to be redrawn when you move them over other windows.\n- The desktop takes advantage of your - otherwise idle - graphics card hardware, lowering CPU requirements.\n\nDid I miss anything? :-)"
    author: "Diederik van der Boor"
  - subject: "Re: Only composites?"
    date: 2007-05-30
    body: "> Did I miss anything? :-)\n\nYes, features for advanced users :->"
    author: "Chaoswind"
  - subject: "Re: Only composites?"
    date: 2007-05-31
    body: "> Yes, features for advanced users :->\n\nArguably KWin already has a lot of those."
    author: "Paul Eggleton"
  - subject: "Re: Only composites?"
    date: 2007-05-31
    body: "Having some, do'nt mean, having enough.\nThere is a wide world of windowmanaging waiting :)"
    author: "Chaoswind"
  - subject: "Another interesting video (appendix)"
    date: 2007-05-30
    body: "For an interesting wrap up of the KWin Composite work, check out this video by Rivo Laks:\n\nhttp://www.youtube.com/watch?v=4WBLlc6xCQ4\n\nIt is 6 minutes long, and covers configuration, setting up the composite effects, and some more effects in action."
    author: "Troy Unrau"
  - subject: "Re: Another interesting video (appendix)"
    date: 2007-05-30
    body: "Hey sweet! thanks for posting this one Troy. ;) It's pretty valuable for those that don't read planetkde.org on a regular basis... "
    author: "Danny Allen"
  - subject: "Aquamarine + beryl"
    date: 2007-05-30
    body: "Good work everyone on kwin. However, what is the difference between using kwin and using beryl with the aquamarine decorations (this uses the kwin decorations)? I just don't understand why it is better to work on kwin directly rather than build on beryl with aquamarine."
    author: "Bob"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-30
    body: "KWin is a full-fledged WM, well integrated in KDE, with a long histroy and strong codebase; while Beryl/Compiz is it not."
    author: "Chaoswind"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-30
    body: "Well, it's not just about decorations, but features that are built into the window manager.  Little things, like mouse focus modes, switching virtual desktops, snapping windows the screen edges, these sorts of things.  While many of these features will be in beryl now, they have been in KWin for a decade, and just about every fringe use case has been programmed for.  You should try running X without a window manager once just to see how much work a window manager actually does for you."
    author: "Danny Allen"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-30
    body: "the answer is quite straight forward:\n\n- kwin works everywhere (degrades gracefully on less capable hardware), beryl doesn't\n- kwin is a production ready *window* *manager*, which means it has all sorts of window management oddities fixed; take a look at the handling of transients in the code to see just how crazy this is. how lubos maintains his sanity i'll never know ;)\n- kwin has various kde integration features and we can continue to maintain that easily\n- it is easier to add what beryl has on top of the above than it is to add the above to beryl\n- we all share the benefits of what is going into the shared infrastructure (x.org) so it's not all lost efforts\n\nand if you care .. s,beryl,compiz,g"
    author: "Aaron J. Seigo"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-30
    body: "I'm using Beryl for quite some time now and it works so flawlessly with KDE that it just seems like a duplication of efforts to implement all those effects again in KWin. Everything in Beryl just seems to work. What are those \"fringe-cases\"? Why is it I never experienced those? Just out of interest: Could you or some other expert give an example of one end-user-understandable thing that wouldnt work right now in Beryl and couldnt be easily implemented? What are \"transients\"? Please don't get my wrong. I'm not trolling here but I'm really interested in technical behind-the-scenes info on what makes KWin so special so maybe I can appreciate it more in the future ;-)\n"
    author: "Michael"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-30
    body: "\n> Why is it I never experienced those?\n\nthey are called \"fringe\" for a reason. a production window manager that is relied on as heavily as kwin is needs to be solid in all cases. these include all sorts of things such as the handling of certain types of java windows, apps that try to self-manage their windows, etc.\n\n> What are \"transients\"?\n\nwindows that only exist in conjunction with another window; e.g. a warning dialog associated with a main window.\n\nyou also seemed to have skipped over the \"works on all hardware\" bit."
    author: "Aaron Seigo"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-06-03
    body: "Your arguments are convincing me, Aaron.\n\nI'm glad to see this feature additions now happening in KDE's own window manager. That's for sure the most logical thing to happen.\n\nBut imagine for a moment that something \"bad\" had happened to Lubos in the last 6 months (the infamous \"bus case\"... or that he got fed up with KDE4 coding... or $whatever): who would then have taken over kwin maintainership (*and* kept his sanity)??\n\nAnd would Lubos have felt so much incentive and/or inspiration to defend his kwin territory's supremacy if there would not have been the challengers of compiz+beryl??  :-)\n\nSo, to me, it looks like it was not bad at all to have seen that crazyness of the \"competing\" window managers' speedy feature additions happening in the last year or so (and see them being able to work with KDE3 just fine), and experience a serious \"migration\" of the more playful and adventurous-minded section in our user base over to the camp the competition (using a non-KDE window manager to run their KDE desktops).\n\nBeryl+compiz certainly didn't hinder kwin's development, and provided a lot of examples (to follow as well as to avoid). So hats off to them!"
    author: "dude"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-30
    body: "Some examples of how KWin is better integrated with KDE than Beryl are:\n* the KDE Desktop Pager doesn't work with Beryl (at least it didn't originally - there's a patch available now I think). The \"single desktop 4x as wide\" paradigm threw it off.\n* KWin has a DCOP interface, allowing other KDE programs to communicate with KWin in a standard way. KWin 4 will probably use DBus (DCOP's successor) now that the rest of the Linux world has caught on, but DCOP was around for years before DBus.\n* I personally experienced a lot of problems with Beryl+KDE. The window decorations would disappear for no reason, windows wouldn't get focus events properly (I would have to hit F12 *twice* to bring the Yakuake console to the front), etc. So maybe Beryl worked perfectly for you, but you were one of the lucky ones. I wasn't, so I'll enjoy Kwin.\n"
    author: "kwilliam"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "> * KWin has a DCOP interface, allowing other KDE programs to communicate with \n> KWin in a standard way\n\nHm, I checked KWin's interface with kdcop, but only found some neat things for configure the desktop and opacity. So, is this really all, or is there somewhere a switch to expose a mighty interface for poweruser and their scripts?"
    author: "Chaoswind"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "To use kwin with dcop, just call dcop on an window, not on kwin..."
    author: "gnumdk"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "What?\n\nI can't call dcop a window. I can only use the interfaces, exposed by the applications, and there is nowhere an advanced window-managing interface to found."
    author: "Chaoswind"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "As far as I know you can't address an arbitrary window. However all kde applications have a dcop module (or whatever that's called) that corresponds to their window.\nFor instance if I type dcop konsole-4414 konsole-mainwindow\\#1 (4414 being my konsole pid atm)\nI get a list of 135 functions, like setSize, focus, maximize, raise, etc etc."
    author: "tendays"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "Exactly. But not every application is a kde-application. And in fact, there are also kde-application which does'nt offers this interface, ktorrent oder dialogs for example.\n\nSo, a complete scriptable interface, for every window, would be nice. I know, there exist wmctrl, but this prog has limits."
    author: "Chaoswind"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-06-06
    body: "your problem with yakuake not showing up immediately is probably because you first start kwin, and then replace it with beryl at the time yakuake is already up. i had the same problem and i solved it by writing a file ~/.kde/env/use_beryl in which i wrote the single line KDEWM=/usr/bin/beryl . (and remove beryl from autostart after that manually!) startkde will source that file and will read that variable to start beryl instead of kwin. that solved all the windows-are-not-working-for-me problems i had before.\n\nbut, nevertheless, i also think it is right to extend kwin instead of using beryl, which still has alot of problems. :) "
    author: "litb"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "> I'm using Beryl for quite some time now and it works so flawlessly\n> with KDE that it just seems like a duplication of efforts to implement\n> all those effects again in KWin. Everything in Beryl just seems to work.\n> What are those \"fringe-cases\"?\n\nWhat's broken in 0.1:\n- focus stealing prevention\n- OpenOffice dialogs\n- starting windows as minimized (e.g. for chat clients)\n- re-focusing existing windows from the application itself.\n- good positioning like kwin does.\n- handling modal dialogs on top properly with focus events.\n- displaying modal dialogs in the alt+tab bar.\n\nshall we continue..? :-)\n\nI actually get bug reports for things in my application which are caused by Beryl.. :-|"
    author: "Diederik van der Boor"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "And don't forget all java apps! Beryl simply show a grey window with no widget/text in it."
    author: "Davide Ferrari"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-08-07
    body: "Try export AWT_TOOLKIT=\u0094MToolkit\u0094 to solve this \"grey-windows\" problem under Beryl/Compiz :)"
    author: "naibed"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "I don't know about what makes KWin better, but Beryl still has a lot of problems. I have been unable to getting is working on my system no matter what driver I use. Having the interface build in as part of KDE without extra setup would be nice.\n\nChad\nhttp://linuxappfinder.com"
    author: "Marsolin"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "And may I add that Aquamarine is just a Beryl window decoration that allows you to use KWin window decorations and nothing more. It doesn't magically make Beryl behave more like KWin. It just makes Beryl *look* like KWin.\n\nI guess the greatest loss for me when using Beryl is the lack of integration with KDE. That's a very big loss considering that one of the reasons I use KDE is the integration. Hopefully, KWin 4 will give me that bling I want without sacrificing the features I need.\n\nKudos to Seli and company! Great job!"
    author: "Jucato"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-30
    body: "First of all, KWin is MUCH more advanced. Not on the visual side, but instead it has all those little things that go unnoticed while making the work more pleasant and fast. Clever auto-arranging of windows is only one of those underestimated features. The second reason would be that Compiz/Beryl architecture is a mess. Those WMs were a trip into an uncharted territory, and as such they could not benefit from existing experience in writing compositing managers. The effect is a messy codebase that either way should be rewritten. So if a complete rewrite is needed, why not stick with the good and mature KWin codebase, and extend it to embrace new possibilities given by compositing?"
    author: "zonk"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-30
    body: " You could just as easily say why did the beryl developers create aquamarine when they could have added the same features to KWin.  I do believe KWin was first.  See how that works? :)\n\nSince everyone is accomplishing the same thing, the functionality will eventually be delegated to a shared library that everyone uses, i.e. KWin, Aquamarine, compiz etc will all link against the same lib3DDesktop.so or whatever.  I'm not saying it will happen but it could and probably will sometime in the future just for practical reasons.\n\n"
    author: "josh"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-05-31
    body: "It would happen if it was possible. But, as Troy mentioned, this was discussed - using beryl and compiz stuff. Sadly, Beryl and Compiz don't really have a proper plugin system, they just expose all of their internals to the so-called plugins. It's bad security-wise, compatibility-wise etc - and makes it impossible to share plugins."
    author: "superstoned"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-06-01
    body: "I believe you superstoned but maybe there can be made one exception.  \"Compiz: Screenlets!\" are really great, it would be good if Kwin-composite could use screenlets from compiz :D\n\nThen, if it's true that KDE4 could use dashbords from Mac, with additional compiz-screenlets KDE4 would rock!"
    author: "kado"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-06-03
    body: "KDE can't use mac's dashboard applets.  This is a rumour, and we don't know where it started..."
    author: "Troy Unrau"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-06-03
    body: "\"This is a rumour, and we don't know where it started...\"\n\nhttp://www.kdedevelopers.org/node/1715"
    author: "Anon"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-06-03
    body: "It started from Zack Rusin:\nhttp://arstechnica.com/news.ars/post/20060102-5881.html\nhttp://www.kdedevelopers.org/node/1715\n"
    author: "Kami"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-06-04
    body: "Interesting that his quote is such that he 'intends' to develop a compatibility layer, not that one exists.  *sigh*"
    author: "Troy Unrau"
  - subject: "Re: Aquamarine + beryl"
    date: 2007-06-04
    body: "That's the way of rumors. On the other side is kde4 a large series of promises, much broken, more realized. So, until it's finished or another message regarding this appears, the people believe in the promise. No one knows what is coming, until it's finished."
    author: "Kami."
  - subject: "ATI / fglrx?"
    date: 2007-05-30
    body: "very nice! Would like to have this on my ATI Mobility Fire GL T2/T2e as well.\n\nWill ATI's proprietary fglrx driver be supported in the near future?\n"
    author: "Jan Mentzel"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "Ask ATI. \nAFAIK their drivers don't support compositing.\nBut KWin will just fall back to plain X when used with fglrx. Not that much eyecandy, but it will work;)"
    author: "Martin"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "AMD has claimed that they will be releasing ATI drivers as opensource - so I assume that once this happens, it will be supported in short order... I'm pretty certain this works under the opensource driver already, but there may be some speed problems.\n\nI guess as long as AIGLX or XGL work with the fglrx drivers, you should be okay.  The reason I mention the nvidia driver directly in the article is that for nvidia, it works without aiglx or xgl, which may or may not be an advantage, depending on your perspective... (one less layer means slightly faster, vs. proprietary driver implementing things in it's own way....)\n"
    author: "Troy Unrau"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "If I'm not totally incorrect, NVidia has implemented something very similar to AIGLX on their own.\nATI's proprietary drivers (fglrx) doesn't even support composite yet. But once they (hopefully) release their source code, the extensions will be added quickly to the drivers/it will be merged with the radeon driver.\nAlso noteworthy, is that I read somewhere that ATi was blocking the release of open source drivers that supported their newest card (the code was ready, but somehow the programmer wasn't allowed to release it, probably an NDA)."
    author: "Martin"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "About NVidia: that is correct to the best of my knowledge.  \n\nAlso, about fglrx, last I checked, it does support Composite, and it does support GL, but not both at the same time forcing you to choose.  Normally, users will elect for GL, since you are reduced to using Mesa if you enable Composite.\n\nNot sure about the ATI rumour, but when companies release the source code, they usually try to clean it up somewhat to ensure they aren't accidentally releasing IP that they do not own the rights to... think of the big delays in opensourcing java, for example, or star office :)  A driver is a little smaller, but the principle is the same."
    author: "Troy Unrau"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "Troy Unrau, you say NVIDIA's drivers work without XGL or AIGLX. I know XGL is a separate X server, but isn't AIGLX a part of X.org?\n\nhttp://en.wikipedia.org/wiki/Aiglx\n\n\"The AIGLX project has been merged into X.Org and is available as of X.Org 7.1.\"\n\nhttp://en.wikipedia.org/wiki/Xgl\n\n\"Xgl is an X server architecture...\"\n\nSo how can you work without AIGLX if you run X.org and use NVIDIA drivers?\n"
    author: "Alexander van Loon"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "NVidia's drivers perform the same functions as AIGLX or XGL without their presence.  They do it internally within the driver, and as such, will run KWin Composite on X.Org version 6.9, for example, when using the proprietary nvidia drivers."
    author: "Troy Unrau"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-31
    body: ">AMD has claimed that they will be releasing ATI drivers as opensource [..]\n\nIf you're referring to the keynote at the Red Hat Summit, opinions diverge and there was nothing like a promise of any kind, except that AMD's marketing person claimed that AMD is willing to work on better Linux support for their drivers - like AMD/ATI did so often. Only (tabloid) news sites Slashdot and OSNews reported in a \"It's coming soon\" way, linking as proof to a blog of someone who thinks to have heard a promise of open graphics drivers.\n\nJust to consider: AMD doesn't even care enough to clarify. And then recall again their promise of having to work better with the Linux community. All I'm saying is, don't get your hopes too high with AMD."
    author: "Anonymous"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-31
    body: "They'd rather just release their hardware's specs/documentation freely so we stop depending on them and their flawed drivers. Mind you, ATI drivers have never been good (never even MS Windoze's)."
    author: "Jack Ripoff"
  - subject: "Re: ATI / fglrx?"
    date: 2007-08-31
    body: "You're telling me. I have an old ATI Rage128 card that guarantees a WinXP blue screen (with the newest version of the driver) if you start up aMule but it works like a charm on the reverse-engineered X driver."
    author: "Stephan Sokolow"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "this question you have to ask the other way around. because the problem lies with the proprietary drivers. they dont support one extremely important fonction that all those fancy WMs use, and i think there is no way to make it work with the current ati drivers. But i'm no developer.\nChange to nvidia or intel if you can. i'm out of luck too with my laptop.. at least those things work with the open source driver."
    author: "Beat Wolf"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-31
    body: "having laptop here as well. so changing is on option.\nThe open source driver neither supports OpenGL hardware accelleration nor directdraw. I know, thats not the developers fault."
    author: "Jan Mentzel"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-31
    body: "sorry, \"direct rendering\""
    author: "Jan Mentzel"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "With fglrx, Xgl is needed to run Compiz/Beryl. fglrx will run in GL Mode, Composite is somehow emulated or so by XGL (afaik).\n\nI wonder if kwin_composite will work with XGL and gl enabled fglrx..."
    author: "Robin"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "Since it is doing much the same thing as Beryl, it should work.  I've never played with Xgl though with fglrx.  I got so upset with the drivers for my fairly new (onboard) radeon express 200, that I went and bought myself an nvidia card just to avoid the driver hassle."
    author: "Troy Unrau"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-30
    body: "I share your pain, I have the same card, and it is absolutely atrocious.  In the beginning it didn't even work in 2D mode.  At least that works now, but the performance is dreadful, and fglrx is such an awful driver that I don't even want to try it anymore.  I spent weeks on it in the past with more or less garbage results.  My only hope now are the ATI open source drivers (if they actually release them). "
    author: "Leo S"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-31
    body: "Opensource or not, their drivers will remain buggy.\n\nOur real hope would be pressuring them to release their docs so we can code our own decent drivers."
    author: "Jack Ripoff"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-31
    body: "so now specs are better then code? the opensource driver community could easier write a new driver from scratch than fixing atis maybe-very-soon-opensource-driver?"
    author: "ac"
  - subject: "Re: ATI / fglrx?"
    date: 2007-05-31
    body: "\"so now specs are better then code?\"\nAs far as device drivers are concerned, it always has been.\n\n\"the opensource driver community could easier write a new driver from scratch than fixing atis maybe-very-soon-opensource-driver?\"\nProbably. That driver's just so buggy, it's code may be a complete spaghetti mess. I could be wrong about this thou, perhaps it may just be a matter of cleaning the code up."
    author: "Jack Ripoff"
  - subject: "Re: ATI / fglrx?"
    date: 2007-10-30
    body: "I have a Radeon XPress 200M, and I installed the newest drivers from AMD. They are a completely new codebase and fully support AIGLX. I am running Beryl with few problems,so I am sure that this will work excellently with KDE4."
    author: "Daniel"
  - subject: "What are the minimum PC to run these effects?"
    date: 2007-05-30
    body: "I mean, what are the minimum requirement to play GL or XRand efects? For example, Can I play the efects withslowdowns in a ati r128? Can we use a xinerama setup? And a remote X display?"
    author: "cruzki"
  - subject: "Re: What are the minimum PC to run these effects?"
    date: 2007-05-30
    body: "It depends on what features you turn on. With all effects, it won't run smooth. But some basic effects should work fine... Maybe lubos knows some details here. And there's an article about performace, but it's about Beryl. Kwin won't be a 100 times faster I guess, so it should give a pretty good idea of what is needed. http://www.phoronix.com/scan.php?page=article&item=730&num=1"
    author: "superstoned"
  - subject: "Re: What are the minimum PC to run these effects?"
    date: 2007-05-31
    body: "So us with older hardware can turn them All off. Excellent!"
    author: "Ben "
  - subject: "Re: What are the minimum PC to run these effects?"
    date: 2007-05-31
    body: "I could see some benchmarks of Intels latest chipset, but what about their older chipsets? Anyone knows?"
    author: "Joergen Ramskov"
  - subject: "Re: What are the minimum PC to run these effects?"
    date: 2007-05-31
    body: "It depends on what effects you're talking about. The most basic (and most useful) composite goodies should work flawlessly on an ancient TNT2 and a 300MHz CPU :). I have myself seen compiz work like a charm (and with negligible CPU usage) on my friend's old Pentium II with S3 Savage4 accelerator (not as powerful as a TNT2, though with comparable capabilities). If you have a PS 2.0 capable GPU like FX5200, you won't have any problems with any effects :). Compositing a desktop is really no burden even for very old graphics accelerators."
    author: "zonk"
  - subject: "troy, you do your job very well"
    date: 2007-05-30
    body: "Your article are to me good.\nYou make it a way I so rarely have questions left after I read, you rock.\n\nAnd good work kwin devs.\n\nNow, past the thanks :\nI am lucky enought to have an old ATI runing with OS drivers, with both comiz or beryl.\nBut I quickly returned to Kwin.\nWhy ? because even whithout the fancy eye candy, kwin is actualy usable. the two above can't claim the same. that's all\nAnd no, I don't mean they are buggy ( ok, freezed the computer about two or tree times a mounth, but not so bad to stop using them if they had been good,hey )\n\nuntil next time :)"
    author: "kollum"
  - subject: "Re: troy, you do your job very well"
    date: 2007-05-30
    body: "Thanks for the kind words.\n\nBasically, if it was supported by Compiz and/or Beryl (the two projects have now merged, but I still cannot figure out what the proper thing is to call them) then it should work in KWin as well.  I haven't seen a side-by-side performance comparison, but Seli has done performance related profiling within KDE before, so I'll trust his capable hands."
    author: "Troy Unrau"
  - subject: "Wobbly Windows ?"
    date: 2007-05-30
    body: "Will be there a wobbly windows effect ? I know this effect can look useless (and I agreed at first) but now I like it a lot"
    author: "DanaKil"
  - subject: "Re: Wobbly Windows ?"
    date: 2007-05-30
    body: "If you check out Seli's youtube page, there is (or used to be) a wobbly windows video up there.  I did see that effect when I was scrolling through the list :)"
    author: "Troy Unrau"
  - subject: "Re: Wobbly Windows ?"
    date: 2007-05-30
    body: "It wasnt wobbly effect, it was a wave effect - all windows were just waving, whether you moved them or not."
    author: "franta"
  - subject: "Re: Wobbly Windows ?"
    date: 2007-05-30
    body: "Oh, right.  There are a number of plugins that implement 'demo' modes, and I believe that one was called the 'Drunken Windows' demo or somesuch :)"
    author: "Troy Unrau"
  - subject: "Re: Wobbly Windows ?"
    date: 2007-06-08
    body: "My personal opinion (hence a end user vote) - \n\nwobbly windows *BAD*. \nSpecially the minimize effect in beryl (do not have good enough english to properly term it) really SUCKS.\n\n "
    author: "Soyuz"
  - subject: "Different shadows size"
    date: 2007-05-30
    body: "I may be cool to have different shadows sizes : if a window is active, the shadow is larger (like if the windows higher) and if the window is inactif, the shadow is small, as if it is put on the desktop"
    author: "Heller"
  - subject: "Re: Different shadows size"
    date: 2007-05-30
    body: "this is already implemented in Kwin for KDE since 3.3 :D\nBut they'll have to rewrite it, so let's hope they don't forget this."
    author: "superstoned"
  - subject: "Great Work"
    date: 2007-05-30
    body: "I think this work is great - what he has also done is provide a way to capture the screen shots, plus that rather neat red highlighting with the mouse. As far as I can see this will be a very usable desktop. In my opinion the eye-candy is useful in adding a tactile feel to the desktop. This is important and also provides useful accessibility options too.\n\nAs I have played with Compiz and Beryl they are pretty, but complex to configure (this already looks clearer), and miss the quality window management stuff described above. \n\nWhatever the eye-candy brings the stability of Kwin is a great asset - I want to do work after all and I need a stable WM to do that."
    author: "Kevin Colyer"
  - subject: "Re: Great Work"
    date: 2007-06-06
    body: "i really hope they implement a filter field at the top of that configuration dialog (like most kde components have now) - finding any option in beryl configuration was PAIN when i tried it in knoppix 5.1 ;)"
    author: "richlv"
  - subject: "Another video I didn't get to see :("
    date: 2007-05-30
    body: "Great work, but it would be really amazing if these videos were also \navailable in theora format [insert the regular rant about\nfree vs closed formats here]. It's really depressing to miss out\non all the fun...\n\nAlso, off-topic but, is there some way for third party applications \nto know when it's running under the gl version of kwin? Does it still \nuse \"kwin\" for _NET_WM_NAME or something distinct? \n\nthanks\n\n\n"
    author: "j.v."
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-05-30
    body: "For the former question: part of the reason we're hosting on youtube is to limit our bandwidth consumption on the dot.  \n\nOn a related note: one of the interesting features that KWin has implemented via the new effects system is a screen recorder which works almost as easily as ksnapshot does for screen caps.  It doesn't do any encoding on the fly, as that would slow down the system taking the video, but it dumps to a raw format that just about any encoder, open or otherwise, can use.  This means there is likely to  be more videos of KDE 4 stuff showing up in the wild-- unfortunately, most of these will end up on youtube, simply because the price of hosting is the right price... c'est la vie.\n\nAdditionally, if not available via some other part of the NETWM spec, this information would probably be obtainable via dbus queries.  KWin is still called KWin though, so no name change.\n\n"
    author: "Troy Unrau"
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-05-30
    body: "Maybe archive.org could provide a solution to the bandwidth problem?\n\n>>On a related note: one of the interesting features that KWin has implemented\n>>via the new effects system is a screen recorder which works almost as easily\n>>as ksnapshot does for screen caps.\n\nActually , that's what's bugging me. I've noticed that some time ago on the\nwebsvn and I'm looking for ways to \"compete\" with this feature :).\nI'd hate to see my last year's effort (http://recordmydesktop.sf.net)\nhit /dev/null and get obsoleted...\n\nThanks for answering."
    author: "j.v."
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-05-30
    body: "KDE isn't microsoft; we're not going to crush you out of existence :)  Take a look at the success of yakuake, konversation, or kommander, etc. as alternates to the applications that are shipped with KDE that do quite well.  \n\nOr, if you're intereseted, we can hook you up with an SVN account and let you design that part of KDE.  As far as I know, there is no UI to configure or control this extension yet, and your expertise would likely be very welcome by the kwin developers :)\n\nEither way, I didn't know your project existed until just now -- it looks pretty shiny already :)"
    author: "Troy Unrau"
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-05-30
    body: "Yes, recordmydesktop rocks. I wonder why it isn't integrated in Beryl, the beryl-vidcap thing is based on seom and produces really ugly videos in an inusable format. Maybe you should mail some Beryl-devs."
    author: "blueget"
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-06-01
    body: "well, seom is certainly not producing videos in an ugly format, whilest it is not developed by me I still feel I shall argue here.\nin fact, you just can't use XviD4/Ogg encoding to record the desktop on the fly, you just won't have the horse-power for that - trust me (as the recordmydesktop dev most obviousely noticed himself!)\n\nthe config part in the video record effect are hard coded as they've been actually used for demonstration by lunak only, so you are all welcome to improve this effect if you want to.\n\nAlthough, recordmydesktop[1] is a standalone application, not ideal for integration into kwin, yet you could use it on your own, but there is a need for something more speedy, ideally not capturing via XImage/XShm source but via OpenGL, as kwin now primarily renders in OpenGL mode.\n\nSo the kwin maintainer tried out seom[2], while this looked promising, lunak still wanted more, that is: partial screen capturing, that can be integrated into an Xdamage-featured compositing desktop, so I implemented this requested feature in my the capturing framework I am developing myself[1] and showed him some demos using it.\n\n[1] http://recordmydesktop.sf.net/\n[2] http://neopsis.com/projects/seom/\n[3] http://rm-rf.in/captury/\n[4]  http://websvn.kde.org/trunk/KDE/kdebase/workspace/kwin/effects/videorecord.cpp?view=markup"
    author: "Christian Parpart"
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-05-31
    body: "Try swfdec or youtube-dl.\n"
    author: "Reply"
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-05-31
    body: "You don't need Flash to play videos on YouTube. They are encoded in Flash Video (.flv), which mplayer can handle.\n\n1. Get mplayer and youtube_dl (http://www.arrakis.es/~rggi3/youtube-dl/)\n2. youtube_dl http://www.youtube.com/watch?v=4WBLlc6xCQ4 && mplayer 4WBLlc6xCQ4.flv\n\nIf you want to download multiple videos in one go, get my (Ruby) script here: http://membres.lycos.fr/madleser/coding/yt"
    author: "madleser"
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-06-01
    body: "http://movie-get.org/link/\n\nMakes MPGs via the website, or via downloaded script if you prefer that."
    author: "Stoffe"
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-06-01
    body: "I appreciate your answer(as well as the others on this matter), but\nthe issue wasn't on whether it's technically possible to see the files\nusing only free software (and btw the previously pointed youtube-dl \n+ mplayer  requires the binary codecs. youtube-dl + ffmpeg2theora though \ndoes the trick).\n\nWhat I was trying to point out is that we should strive for solutions\nthat are accessible to everyone, without punishing people with extra \ntedious steps, if they choose not to install  a non-Free component \nlike flash.\n\nThanks for the kind answer(s), though."
    author: "j.v."
  - subject: "Re: Another video I didn't get to see :("
    date: 2007-08-31
    body: "While I agree with your viewpoint, I'd like to also point out that you're running an old version of ffmpeg in your MPlayer.\n\nGnash 0.8.0 with the GTK+ frontent and ffmpeg FLV-decoding backend works with only a few minor glitches relating to font rendering and widget positioning in the YouTube player. (the font rendering is fixed in current CVS) and current stable MPlayer on Gentoo decodes FLV (as well as the Sorenson codec (for Quicktime) that it's based on) using only ffmpeg. \n\nAnd I'm on a native x86_64 system so I can quite honestly praise the progress that's been made. The only thing I need amd64codecs for is RealPlayer videos and the only thing I need mplayer-bin (32-bit) for is the Indeo 5 codec from win32codecs. (I collect AMVs and about half a dozen of them use Indeo 5)\n\nHeck, I get full browsing functionality without needing nspluginwrapper, a 32-bit nspluginviewer binary, or a 32-bit Firefox. (I'm using Konqueror with KMplayer, Gnash, and 64-bit Java, Firefox with MPlayerPlug-in and Gnash, and I don't need Java in Firefox)"
    author: "Stephan Sokolow"
  - subject: "Performance"
    date: 2007-05-30
    body: "Why are the Kwin-Videos so laggy compared to the Compiz-Videos? And do I have to disable the effects everytime I want to play games like ut2004?\n\nWould be great if I'm wrong."
    author: "EMP3ROR"
  - subject: "Re: Performance"
    date: 2007-05-30
    body: "I tried kwin_composite went it was merged into trunk and found it much, much slower than Beryl on the same hardware (an nvidia 5200) : Beryl was *wonderfully* smooth on that card, but kwin_composite was very poor - you could easily count the individual frames on even as simple an effect as the \"minimise\" one (about 3-4 fps).\n\nSo, lots of optimisation work still to do :) Based on progress so far, I'm sure they'll have no difficulties in getting it to run well in the long term,  but I'm not sure about having a silky smooth kwin_composite for KDE4.0."
    author: "Anon"
  - subject: "Re: Performance"
    date: 2007-05-30
    body: "Well, you should try to play with the settings. it was very slow for me as well, until i changed a few things - now it's perfectly smooth."
    author: "superstoned"
  - subject: "Re: Performance"
    date: 2007-05-30
    body: "A: I have no idea.\nB: You probably wont have to, if you have a non-ATI card. (The others can do composite on the normal X server, ATI's can only work with XGL, which can't accelerate anything but the WM properly)."
    author: "Dennis"
  - subject: "Re: Performance"
    date: 2007-05-30
    body: "In one of the Youtube comments someone said it had something to do with the recording slowing down the system, rather than the actual performance of the effects."
    author: "Paul Eggleton"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "Does anyone know the answer to the second part of the question?  I had to get rid of xgl/compiz because it seriously degraded any other app that needed acceleration (Krita, Google Earth, games, etc.).  I like cool desktop effects as much as the next guy, but I don't want to give up actual, usable features in apps I use regularly."
    author: "Louis"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "It does not depend on the window manager. If you use XGL it might be the case, that hardware-rendering can only be done by the window manager but no application. Using AIGLX this should not happen and every application can use the  GPU's complete features/speed.\nBlame XGL for having a brain dead design (running a Xserver in a Xserver?!?). But XGL is going to die anyway and AIGLX is the road to go (only support from AMD/ATI needed).\n"
    author: "Birdy"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "For me even Aiglx slows down applications. I use Ubuntu 7.04 and e.g. glchess is more laggy, ut2004 has 30% less fps, ... when Compiz is activated.\n\nWould Xegl improve the situation?"
    author: "EMP3ROR"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "Which hardware? Intel and NVidia should do fine...\n"
    author: "Birdy"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "Nvidia Geforce 6600\n\nOpenGL and everything works, but it's slower."
    author: "EMP3ROR"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "Strange. I'd guess there is something wrong with the settings :-("
    author: "Birdy"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "I also have this, running on a old Geforce4 64MB.\nRunning with Xgl I have much smoother visual effects, but CPU usage goes sky, with AixGL there is almost no extra CPU use, but the effects runs with a bad framerate."
    author: "Iuri Fiedoruk"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "remember, these techniques are just very immature yet. X.org doesn't really have the proper infrastructure, and it'll take years to reach the quality Vista has (yeah, it's weird, but they got this right). Or Mac OS X.\n\nThough I expect a lot from Kwin ;-)"
    author: "superstoned"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "Is a better infrastructure in development?\n\nAnd what happend to Xegl? I think I read somewhere that David Reveman is still interested."
    author: "EMP3ROR"
  - subject: "Re: Performance"
    date: 2007-05-31
    body: "I'm running an older ATI Radeon 9200 card, one of the last ones with specs released so it has solid freedom software support.  With it, CPU usage is *HIGHLY* dependent on rendering choice.  The old XAA rendering is incredibly CPU bound with composite on.  The new EXA rendering is MUCH MUCH more efficient, and while there were stability issues earlier on, with xorg 7.2, EXA with composite enabled is about as stable and CPU efficient as XAA without.\n\nSo, if composite is eating your CPU cycles for lunch, try turning on EXA instead of XAA, and see if that helps.  With the xorg native Radeon driver, it's a setting in the xorg.conf graphics card \"Device\" section, as so (rats, plain text is still eating the indents, and the markup won't work either, but they aren't absolutely critical, so...):\n\nSection \"Device\"\n   ...\n   Option \"AccelMethod\" \"EXA\"\n   ...\nEndSection\n\nTo switch back to XAA, a simple # at the beginning of the line comments it, returning xorg to the old XAA default.\n\nDuncan"
    author: "Duncan"
  - subject: "Re: Performance"
    date: 2007-07-05
    body: "I'm also using an ATI Radeon 9200 with the open source driver and my experience is exactly the opposite. With AIGLX CPU usage is very high with EXA and it is somewhat OK with XAA. Maybe some other configuration options influence the performance."
    author: "Cristian"
  - subject: "Metisse"
    date: 2007-05-30
    body: "I think kwin could take some things from metisse - it has some pretty neat ideas too worth integrating into kde directly"
    author: "cenebris"
  - subject: "Very nice"
    date: 2007-05-30
    body: "Okay, that's it - i'm definately switching to KDE 4.0 ;)\n\nI think KWin should take the best ideas from existing solutions like compiz and metisse."
    author: "blade"
  - subject: "Re: Very nice"
    date: 2007-05-31
    body: "It does :D"
    author: "superstoned"
  - subject: "Desktop Grid - double plus good"
    date: 2007-05-30
    body: "The Desktop Grid effect is AWESOME, and definitely seems way more useful than the beryl/compiz Cube (which, frankly, is only good for eye candy). I think it will make virtual desktops much more usable. Great to see that Kwin is doing sensible and exciting things with these capabilities!"
    author: "saurabh"
  - subject: "Re: Desktop Grid - double plus good"
    date: 2007-05-31
    body: "Desktop Grid is already available in compiz, it's called \"wall\" plugin..."
    author: "gnumdk"
  - subject: "Re: Desktop Grid - double plus good"
    date: 2007-05-31
    body: "and was available in metisse before that..."
    author: "Morreale Jean Roc"
  - subject: "Re: Desktop Grid - double plus good"
    date: 2007-05-31
    body: "Absolutly 100% agree.\n\nI use Beryl on my work PC, so I'm in front of it a lot.  The cube is fun, but a bit annoying when you've got stuff on many desktops.  This approach look vastly superior.  Plus, you aren't limited to 4 desktops!"
    author: "Robin"
  - subject: "Re: Desktop Grid - double plus good"
    date: 2009-01-13
    body: "umm... the compiz cube isn't limited to 4 workspaces, you can use as many as you like. seriously though, why all the cube haters?"
    author: "Author"
  - subject: "3d Cube?"
    date: 2007-05-30
    body: "Another useless candybar but one that I like none the less.\n\nYAY for wobbley windows!"
    author: "Juan"
  - subject: "Lookin Good"
    date: 2007-05-30
    body: "As always The Road... series delivers an excellent run down.  I really like how you pointed out that Kwin is mature, whereas compiz and beryl are still the new guys (yeah beryl is sweet, but it is crash city sometimes).  I totally support the home team solution.\n\nSince other people use this as a place to deliver critiques I have a small one.  The desktop grid should scale so that the virtual desktops are still in the same aspect as they are on the monitor.  Maybe the addition of black bars like on a widescreen movie would be a good solution.\n\nCan't wait to fire up 4.0!  You guys rock.\n\n-Nobbe"
    author: "Nobbe"
  - subject: "Re: Lookin Good"
    date: 2007-05-31
    body: "> The desktop grid should scale so that the virtual desktops are still in the same aspect as they are on the monitor.\n\nExactly! I find the squashed appearance of the Grid sort of disturbing. Maybe they'll fix it. Some good looking implementations of the grid are from:\n\nMetisse: http://insitu.lri.fr/metisse/screenshots/virtual-desktop.png\nMac OS X Leopard, called \"Spaces\": http://www.apple.com/macosx/leopard/spaces.html (Wow! They're catching up! :P) \n"
    author: "Jucato"
  - subject: "Fragmentation"
    date: 2007-05-30
    body: "Speaking as an exclusive KDE user I am very disappointed. I see this as nothing more than fragmentation.  I know it was said that there were technical issues in working with the existing Compas+Beryl project. However I can not really see anything but ego here and it is bound to promote some sort of Desktop War Revisited. I do note the great pains that they have taken to let people jazz up their desktops and that must be some great code. However it is my opinion of ego, fragmentation and a new desktop war happening."
    author: "Brotherred"
  - subject: "Re: Fragmentation"
    date: 2007-05-30
    body: "\"I know it was said that there were technical issues in working with the existing Compas+Beryl project. However I can not really see anything but ego here and it is bound to promote some sort of Desktop War Revisited.\"\n\n\"I know perfectly valid technical reasons were given, but I'm going to go ahead and spin it as a petty, purely political issue, with nothing to back that up whatsoever!\""
    author: "Anon"
  - subject: "Re: Fragmentation"
    date: 2007-05-30
    body: "He won't be the first - I'm sure when this hits osnews or similar websites, there'll be a lot of opinions like that.  However, I'd like to restate that those wishing to use compiz/beryl will be able to continue doing just that.  We're not locking you into KWin or anything -- just like you can use the gimp in KDE or Amarok in Gnome, or KDE with Enlightenment, or any other random combination of Unix apps - they should all play nicely with one another as we do work hard on cooperative standards.  KWin isn't inventing anything new that'll lock you out of using alternate window managers :)"
    author: "Troy Unrau"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "Back up? No Back up? The Gnome vs. KDE history is my back up!"
    author: "Brotherred"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "You mean the fact that we now have two excellent desktops (one more excellent than the other obviously ;) is a reason for not working on similar projects?  \n\nGnome and KDE evolved in very different directions.  If we only had KnomE it would be a compromise, and lots of people would lose out on their optimal environment."
    author: "Leo S"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "I think the fact we could have one <b>perfect</b> desktop instead of two excellent desktops is disturbing. Besides two desktops means two learning curves.\n\nI can't force anyone to agree with me thou and everybody's free to create as much desktop environments as wanted (after all, we praise freedom, don't we?)."
    author: "Jack Ripoff"
  - subject: "Re: Fragmentation"
    date: 2007-06-01
    body: "\"I think the fact we could have one <b>perfect</b> desktop instead of two excellent desktops is disturbing. Besides two desktops means two learning curves.\"\n\nWhat makes you think that we could have one \"perfect\" desktop? KDE and GNOME are very different in many ways. People who prefer GNOME obviously prefer the approach taken by GNOME, whereas KDE_users prefer the KDE-way. How would you suggest combining the two so that you can keep the strengths of both desktops, while not forcing users to suffer the weaknesses of either?\n\nMany KDE-users like KDE because of the features and tweakability it gives them. Many GNOME-users prefer GNOME because is simple, clean and streamlined. In many ways the two approaches are diametrically opposed. How would you combine them? In the end you would get compromises and \"designed by a committee\"-software that would leave both KDE and GNOME-users frustrated.\n\nWhat about developement? KDE-developers like Qt and C++. GNOME-developers like GTK+ and C. Again: how would you combine those?\n\nAnyone who suggest that combining KDE and GNOME would result in one perfect desktop, is quite simply deluding himself."
    author: "Janne"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "No what I mean is this. I applaud Compas and Beryl for reemerging because they are a better product as a whole. No need here for two halves of a company to do mostly the same things. It is the same with Kwin. I think it would be better served if Kwin did their own branding of Compas+Beryl. \n\nSimply put the reemerging of Compas and Beryl avoided a useless desktop battle. Falling in lock step is pointless and so is endless forking."
    author: "Brotherred"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "I honestly do not care about wobbly windows, cubes, or whatnot. I care about a fast and stable way to handle my 12 Desktops. I care about Expose-like features. KDE is my work environment. I want my WM stable and proven and supporting all the fringe cases I might run into without me noticing as much as a \"whoops\". I want my WM integrated with KDE in such a way that I do not even notice how nicely they play along.\n\nI do not need one of the \"good looking\" new kids on the block when my productivity and fun is at stake. But I do appreciate the tons of work hours and tons of code which went into a WM which I do not even think about any more. KWin is more mature and has more features I care about than compiz.\n\nThanks, kwin developers."
    author: "cassens"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "\"I think it would be better served if Kwin did their own branding of Compas+Beryl.\"\n\nit has nothing to do with branding or ego. the technical reasons have been explained repeatedly, and they are purely technical. the first thing we (kde) did when trying to decide what to do was to look at the options and play with each of them; they included:\n\n- writing a new WM from scratch (there were even experiments here)\n- adapting compiz/beryl (either wholesale or just the compositing technologies)\n- improving kwin\n\neach was given a lot of thought and testing. there is no perfect solution, agreed, but there are better solutions and worse solutions where \"better\" and \"worse\" are judged in relation to the effect on our userbase.\n\nand remember that our userbase includes schools in \"third world\" countries, banks and governments in the \"first\" world, hard core hackers, hobbyist computer users, SMBs, 'regular' home users ......."
    author: "Aaron J. Seigo"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "point is, Beryl and Compiz are the new projects, they should've worked with the existing projects. They didn't because they just wanted to show what's possible, not rewrite the current windowmanagers.\n\nNow of course, that's exactly what they're doing. They should've stayed at being a research project, not try to be a real windowmanager.\n\nIn any case, the most important problem here is: Compiz and Beryl don't have a real plugin structure, or api. If they had, we wouldn't have a problem - Kwin could adopt the API, and they could share plugins. Instead, Compiz and Beryl are a mess, in terms of plugin-structure: there is none. The plugins just hook into the basics of Compiz and Beryl, and if you have a different base, they won't work.\n\nThis is stupid, non-secure, unstable, un-portable and user & developer-unfriendly. Not strange for research projects, of course, but a pity for projects trying to be taken serious."
    author: "superstoned"
  - subject: "Re: Fragmentation"
    date: 2007-06-01
    body: "If Compiz/Beryl doesn't have a good plugin API, I understand why KWin didn't aim for compatibility with them, but that doesn't prevent them from working together.\n\nDid the KDE developers try to work with Compiz/Beryl to develop a good plugin API? If I wanted, could I write my own window manager that would work with KWin plugins? Is there a specification somewhere for this plugin API?\n\nIf not, the not-invented-here syndrome has at least something to do with this.\n\nI really hope KDE developers aren't trying to lock-in KWin plugins to prevent other projects from using them, but I wouldn't be surprised if they were."
    author: "James Justin Harrell"
  - subject: "Re: Fragmentation"
    date: 2007-06-01
    body: "Umm, the source code is available... so there's no way to, as you decribe, 'prevent other projects from using [the effects]'... however, many of the effects are done using Qt APIs, which would probably need to be ported anyway if another window manager wanted to use them.  Which they can do easily, since, once again, the source code is available.\n\nIn fact, according to Seli, many of KWin's composite's technology comes from reading the source code for 'glcompmgr', a much simpler composite manager than Compiz/Beryl.  So, reinventing the wheel this is not... at worst, KWin is guilty of porting some code. :)"
    author: "Troy Unrau"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "\"The Gnome vs. KDE history is my back up!\"\n\nyou're taking two non-comparable sets of events and trying to compare them. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Fragmentation"
    date: 2007-06-01
    body: "Then you should complain to the Gnome people. They were the ones who fragmented the desktop in the first place, since before that only KDE existed."
    author: "Derek R."
  - subject: "Re: Fragmentation"
    date: 2007-06-03
    body: "The issue isn't as clear-cut as that.  GNOME was actually the first Free desktop since Qt was non-free at the time."
    author: "Antifud"
  - subject: "Re: Fragmentation"
    date: 2007-06-03
    body: "But KDE itself was already free software and advanced enough to predict it would never go away. Creating another desktop from scratch was the worst possible solution to the problem. I personally believe the whole Qt thing was only used as a pretext to start up Gnome."
    author: "Derek R."
  - subject: "Re: Fragmentation"
    date: 2007-06-04
    body: "> But KDE itself was already free software and advanced enough to predict it would never go away.\n\nNo it wasn't.  KDE was far from advanced at the time of GNOME's inception (August 1997).\n\n> I personally believe the whole Qt thing was only used as a pretext to start up Gnome.\n\nYou are sadly mistaken.  The licensing issues were very, very real.  Before the relicense under the GPL, Qt (and by extension KDE) were non-free software.\n\nBefore GNOME was started, intense efforts were made to resolve the Qt licensing issue.  One such effort was to rewrite Qt and clone the Qt api via the Harmony project.  When those efforts failed, there was little else to do but to start a competing project."
    author: "Antifud"
  - subject: "Re: Fragmentation"
    date: 2007-06-04
    body: "Harmony never failed, it was never given a chance to succeed. KDE was announced in 1996, and Gnome was born on 1997. There was no way that in only one year something like Harmony could be built. Some people just wanted very badly to start their own desktop project."
    author: "Derek R."
  - subject: "Re: Fragmentation"
    date: 2007-06-04
    body: "The reason Harmony failed was that the KDE project was perfectly happy with Trolltech (and the Qt licensing assurances made by Trolltech), and would therefor not commit to Harmony.  At the time, there were also grumblings from Trolltech about Harmony which cast an ominous shadow over the long term validity of the project, and their chances for success.  This uncertainty is similar to what detractors of Mono point out as a sore point of /that/ project today.\n\nThere is no conspiracy.  Licensing was the main reason behind the creation of GNOME."
    author: "Antifud"
  - subject: "Re: Fragmentation"
    date: 2007-06-04
    body: "/me still doesn't know what was really wrong with the QPL... the spirit of the license is sound, even if it has some minor conflicts with the GPL..."
    author: "Troy Unrau"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "  \"I know perfectly valid technical reasons were given, but I'm going to go ahead and spin it as a petty, purely political issue, with nothing to back that up whatsoever!\"\n\nThose 'perfectly valid reasons' were things like \"It doesn't handle the fringe cases like transients\" yet there's still no concrete example given of a specific application that fails.  I can't imagine adding the same fixes that were in kwin's code base to theirs for whatever application we've still not heard of that fails would be that hard.  I REALLY doubt it would be anywhere near as hard as duplicating that much of Beryl's functionality was.  And yet, it happened, and we march on to massive duplication of code as usual rather than people bear working together on small things.\n"
    author: "JSanchez"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "The technical reasons were little things like, oh, the fact that they don't _have_ a plugin api to follow.  Beryl just exposes all its internals.\n"
    author: "John Tapsell"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "So KDE should start using GTK instead of Qt?"
    author: "Anonymous coward"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "Using crappy technology because of political reasons it's Gnome's game, not KDE's."
    author: "Derek R."
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "\"I can't imagine adding the same fixes that were in kwin's code base to theirs for whatever application we've still not heard of that fails would be that hard. I REALLY doubt it would be anywhere near as hard as duplicating that much of Beryl's functionality was.\"\n\n[~]>cd kdebase/workspace/kwin\n[~/kdebase/workspace/kwin]>(find . -iname \"*.cpp\"; find -iname \"*.h\") | xargs cat | wc -l\n65271\n[~/kdebase/workspace/kwin]>cd effects\n[~/kdebase/workspace/kwin/effects](find . -iname \"*.cpp\"; find -iname \"*.h\") | xargs cat | wc -l\n7277\n\nSo the sum of \"that much of Beryl's functionality\" is about 11% of the codebase of kwin.  You seem to think that adding an expose clone is some kind of momentous achievement that must have taken masses of hard code to duplicate.  In fact, all it took is this:\n\nhttp://websvn.kde.org/trunk/KDE/kdebase/workspace/kwin/effects/presentwindows.cpp?view=markup\n\nA mere 700 or so lines of code, a lot of which is boilerplate.  That neat grid?\n\nhttp://websvn.kde.org/trunk/KDE/kdebase/workspace/kwin/effects/desktopgrid.cpp?revision=669033&view=markup\n\nA mere 500 or so lines of code.  That spiffy \"live thumbnails on the taskbar\" effect? \n\nhttp://websvn.kde.org/trunk/KDE/kdebase/workspace/kwin/effects/demo_taskbarthumbnail.cpp?view=markup\n\nAbout 100 lines.\n\nI'd also remind you that kwin has been under development since KDE2.0, whereas the entirety of the effects you see here - whose development was most definitely not the sole focus of Lubos, Rivo and Phillip - has occurred since October of last year.\n\nI really don't don't know how Free Software developers do it - being constantly criticised based on the thoroughly misinformed guesses of random strangers all day long would probably make me quit in disgust.  The fact that they don't makes me respect them all the more.  Keep it up, ladies and gentlemen - your work is much appreciated :)"
    author: "Anon"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "> I really don't don't know how Free Software developers do it - being\n> constantly criticised based on the thoroughly misinformed guesses of random\n> strangers all day long would probably make me quit in disgust. The fact that\n> they don't makes me respect them all the more. Keep it up, ladies and\n> gentlemen - your work is much appreciated :)\n\nI agree. I guess you really have to have a \"thick skin\" to endure all this false claims...\nSo, a big thank you to all devs!"
    author: "b"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "\"I really don't don't know how Free Software developers do it - being constantly criticised based on the thoroughly misinformed guesses of random strangers all day long would probably make me quit in disgust. The fact that they don't makes me respect them all the more. Keep it up, ladies and gentlemen - your work is much appreciated\"\n\nOkey on that you got me. Clearly I am coming at it from a end user and New user perspective. As I said I was glad I was very glad to see Compas and Beryl join forces again, for reasons I have made clear.\n\nGnome and KDE have came from different areas and methods and that is all cool. Now however they have been agreeing on common APIs and that to me is a win for all and that with in their own idea of how each is wanting to do things.\n\nI still can not get passed how commercial developers will not know what to do with now not only two different sets of window manager APIs that are not completely merged but now three (two??) different 3D compastating options.\n\nAll very confusing and I watch this stuff constantly just like the rest of you."
    author: "Brotherred"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "I'm not sure how likely it is that commercial developers would want to write a plugin for beryl/kwin.\n\nIf you mean general composite stuff that an app would use, then that is totally standard, and is done by the x server.\n\nAny gnome app using composite will work in kwin and vice versa.  Everything is standard except the plugins, and that's a tiny amount of the code."
    author: "John Tapsell"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "Thank you for giving specific information. As much as I might tend to agree with the original author, it is sad that I had to dig this far into the comments to find specific information about what was implemented. From an initial reading of the article, it DOES sound like an ego issue. Thank you, Anon.\n--Drew"
    author: "Andrew"
  - subject: "Re: Fragmentation"
    date: 2007-06-01
    body: "*clap* *clap* *clap*\n\nI enjoy being a bystander, well said sir!\n\n\"I really don't don't know how Free Software developers do it - being constantly criticised based on the thoroughly misinformed guesses of random strangers all day long would probably make me quit in disgust. The fact that they don't makes me respect them all the more. Keep it up, ladies and gentlemen - your work is much appreciated :)\"\n\nYeah as far as I can tell, Free Software people take a lot of shit all day long.  Just from reading the planet and peeking in on some blogs you can see what some KDE devs have to put up with!  I suppose KDE really is a labor of love."
    author: "Nobbe"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "OK, let's look at your question.  Kwin has existed and been worked on for > 6 years.  Beryl/Compiz has existed for < 2 years.  So why do you think it would be quicker to add kwin features to Beryl than it would be to add Beryl features to kwin?  \"Let's see: 6 years of polish vs 2... Which would be easier to add to another project?  Why the 6 years of course!\"  wtf"
    author: "MamiyaOtaru"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "And yeah, I'm sure my year counts are off.  Point was kwin has far more years of work behind it."
    author: "MamiyaOtaru"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "\"'perfectly valid reasons' were things like\"\n\nsomeone else filled in the details for you, if you look above there is a list of things kwin does right today that beryl/compiz doesn't. i try not to spend more than a minute or two writing replies here, so apologies for not being more thorough.\n\n\"I REALLY doubt it would be anywhere near as hard as duplicating that much of Beryl's functionality was\"\n\nwell, it is. in part because the proper window management takes a -lot- of testing under all kinds of conditions and compositing tech is far more self-contained of a problem space: it either works or it doesn't. neither is trivial, but one is faster to get through. the work compiz/beryl have done also shows the way quite nicely; they deserve massive kudos for that.\n\nwe have a known good solution that our userbase absolutely relies on. beryl/compiz doesn't work for the entirety, majority even, of that user base. ergo the chosen solution.\n\nbtw, this isn't a kde thing. metacity is doing the same thing and, as i understand it, for the same reasons."
    author: "Aaron J. Seigo"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "Yea! There's only one window manager needed! Oh - even more! There's only one desktop environment needed! And - uh! Oh! We only need one operating system! So why don't we all use Windows?\nWhy do people waste resources by making OSX, BSD, Linux, Symbian, ...?\n\nMaybe competition and choice is a good thing? Maybe without competition there wouldn't be evolution (see Windows Vista as an example).\n"
    author: "Birdy"
  - subject: "Re: Fragmentation"
    date: 2007-06-01
    body: "> We only need one operating system! So why don't we all use Windows?\nThe \"don't duplicate efforts\" principle is only valid in the Free Software world. Does that answer your question?"
    author: "logixoul"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "The only answer to this kind of absurd post is: go, buy a Mac and use the one and only OSX+Apple HW. You won't bother (or brotherr? ROTFL) anymore about fragmentation."
    author: "Vide"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "And anyway, if you really wnat to blame someone for creating \"fragmentation\", blame the author of compiz who started a new WM and not implemented it in KWin or Metacity (since it was a Novell employee)."
    author: "Vide"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "Well thank you for the education on these issues. I do not know where I stand now. I do not think my concerns for what is best for the desktop are lessened but you all do make great points. "
    author: "Brotherred"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "The simple point is: Compiz was a research project. It is now indeed duplicating stuff found in current windowmanagers, but that's THEIR waste of time.\n\nThe lack of a plugin infrastructure in compiz makes it impossible to directly re-use their plugins (though we can and do port code and ideas from them). And windowmanagment is much larger than compositing, so it makes more sense to add compositing to a windowmanager than a windowmanager to a compositor."
    author: "superstoned"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "These words should be carved in stone... or at least used as disclaimer in every \"KWin bling\" post ;)"
    author: "Vide"
  - subject: "Re: Fragmentation"
    date: 2007-05-31
    body: "Vi forever!!!!1111\n\nWhat do you suggest? That people shouldn't work on what they prefer to work on in their freetime? \n\nChoice is one of the great things in regards to open source. \n\n"
    author: "Joergen Ramskov"
  - subject: "Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "While I like these video's, there is one thing that Kwin still cannot do: really maximize a window! By this, I mean that the border around the window really disappears, and I can, finally, scroll in konqueror without having to look at the right edge of the screen to see if my cursor is on the scrollbar. I could just flip my mouse to the side and scroll.....\nsigh.\nwell, you can allways hope..! :-("
    author: "Paul"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "You can disable window decorations in Window menu (Alt+F3) sub-menu Advanced."
    author: "rastos"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "I can't remember offhand, but I think\n\nkstart\n\noffers an automated solution for this.  Try\n\nkstart --help"
    author: "Anon"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-06-03
    body: "Thank you. I had no idea that Special Window Settings dialog existed."
    author: "Soap"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "I use Plstik window decorations and it really maximalizes windows."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "I use Plastik window decoration and it hides the border at maximalization. (Try to scroll with the mouse in xterm on the border and on the edge after maximalizing.) Maybe the problem is with the application? (E. g. Konqueror itself has a border.)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "Same with baghira. No border, also with konqueror."
    author: "Guu"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "there are three aspects to this problem:\n\na) the window decoration needs to \"get out of the way\" on maximized windows. most window deco themes do this now.\n\nb) the window manager should allow non-bordered windows and remove the window decoration from the equation on request; kwin (and most others) allow this\n\nc) the application needs to avoid adding internal borders/frames at the window edges that get in the way. there is nothing the window manager can do about this one.\n\nand (c) is what you are running into. the tab widget in qt3 has a real problem here where it insists on putting a border there.\n\nso you're right that kwin can't do this, but that's because kwin, or any other wm, can't."
    author: "Aaron J. Seigo"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "With Konqueror you can press Ctrl+Shift+F to make it fullscreen without borders."
    author: "Vexxo"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "That can already be done - I'm surprised no one told you how -\n\nA window can be in one of four states\n1. minimised\n2. \"restored\"\n3. maximised\n4. full screen.\n\nI think what you are asking for is a way to reach the fourth state. Well, the window menu, in the \"advanced\" submenu, has a \"full-screen\" option. I set up a shortcut for that (alt-enter - there's no default shortcut, however).\n\nOne way to set up a shortcut is in the kde control center, region and accessibility -> keyboard shortcuts. In the first tab (global shortcuts), in the \"windows\" group you have the \"full screen\" entry.\n\n(By the way, don't say \"one thing kwin cannot do\" when that's not true, at least add a \"I think\")."
    author: "tendays"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-05-31
    body: "Easy.\n\nK-Menu > Control Centre > Desktop > Window Behaviour > Moving\n\nand de-select 'Allow moving and resizing of maximised windows'.\n\nThere, wasn't too hard, was it?"
    author: "forrest"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-06-01
    body: "No matter what you do you (disable \"Allow moving and...\" or F11 fullscreen) you still end up with a couple of pixels right to the scrollbar.\nYou should be able to just move the cursor to the right edge of the screen to use the scrollbar. Right now, this is not possible."
    author: "JustMeh"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-06-01
    body: "Not exactly. On Kubuntu with Baghira as decoration, my Konqueror have no such burder, if he is maximized. Only in 'normal' mode, there is such a border. "
    author: "Chaoswind"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-06-01
    body: "Here is a little Screenshot of a maximized Konqueror (the active window in the back) and a normal Konqueror (the little window in front). As you can see, there is no border beside the scrollbar of the maximized window, but a gray at the small window."
    author: "Chaoswind"
  - subject: "Re: Great. How about adding standard behavior?"
    date: 2007-09-02
    body: "I use a dirty hack to correct this annoying issue....\n\nUse window specific settings to force the window to be displayed with the scrollbar at the edge of the screen. The only option that seems to work is 'force' while for 'apply initially' and 'remember', the setting is not applied on new windows for some weird reason..."
    author: "forgetfool1"
  - subject: "kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "Although KDE support d&d (drag and drop) but KWin fails KDE applications by making it impossible to do drag&drop.\n\nbecause the focus changes to the window as soon as the mouse button is pressed (clicked and held), which makes it impossible to do drag and drop in KDE.\n\nwhereas, in windows, when the user releases the mouse button only then the \"focus\" shifts to the window.\n\nTry for yourself:\n\n1. open konqueror file manager or Dolphin (first window)\n2. maximize the first window\n3. open another instance of konqi or dolphin (second window)\n4. but do not maximize it.\n5. Now, try dragging a file from the \"maximized\" first window to the \"unmaximized\" second window.\n\n6. as soon as you click on the file in the maximized window the focus is given there and lo and behold! your whole idea of drag&drop is destroyed.\n\nit was reported in KDE 3.3 by me, and this thing is\nfixable.. just use \"release to focus\" instead of \"click to focus.\"\n\nWill KDE 4 really support d&d which works???"
    author: "fast_rizwaan"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "> it was reported in KDE 3.3 by me, and this thing is\n> fixable.. just use \"release to focus\" instead of \"click to focus.\"\n\nWith KDE 3.5, there is some advanced configuration for this things. I does'nt the English name for modul, but it must be something like 'windowattributes', where you can also configure the click-behavior."
    author: "Chaoswind"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "Well if you set Focus follows mouse that will work fine, lets hope they fix it compleately for KDE4 :)"
    author: "Ben "
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "I didn't yet think of it, but now after you mentioned it: It really bugs... \n\nBut I think it should be amended: \"focus on release + focus on hold for more that 5 seconds\" so I can still get a window to the front where I have some folder below my current window. "
    author: "Arne Babenhauserheide"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "This kind of drag and drop works with tabs in Konqueror, which has a nice smart behaviour if you drag an item over the tab, so it works as expected. It's also less work (e.g. to manage the windows) than having multiple windows.\nSadly, the developers are reluctant to implement these tabs known from Konqueror in Dolphin."
    author: "Anonymous"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "7. press \"ALT-TAB\" to bring the \"unmaximized\" second window to the top\n8. Drop\n\nAs soon as I click on a window, I expect it to be active and on the top. So that's exactly the behaviour I'd expect.\n"
    author: "Birdy"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "me too"
    author: "neurol23"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "i do it ike this: drag and hover in the task bar and the application comes into focus and that all there is to it..."
    author: "KoRnholio8"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "hehe, that's exactly what my self-created taskdock do. I think with kde4 and plasma+kross, there will come a huge wave of alternative task bars and docks, because, with scripting-lang this is really easy."
    author: "Chaoswind"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "> i do it ike this: drag and hover in the task bar and\n> the application comes into focus and that all there is to it...\n\nI like the Mac OS X way even more:\n- just drop it on the taskbar item (the dock icon).\n\nThe application opens the file automatically with it's default settings. No need to wait to have the window in focus if you want the default behavior."
    author: "Diederik van der Boor"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "Yes, event hat I don't use a Mac, this is the expected behavior almost any user would think of at a first time."
    author: "Iuri Fiedoruk"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "On my KDE it's possible actually : just try to drag&drop a Konqueror tab to the konqueror icon of your kicker shorcut bar.\n\nAnother thing i like is that when dragging, you can wait over a task bar item, it will give the focus to the app and you'll be able to drop it where you want."
    author: "bluestorm"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "First: Use ALT-TAB to bring on top the window that is behind the maximized one.\n\nSecond: Drag files to the button of the second windows where you want to drop, but wait a few senconds. The window will pop up and you will be able to drop files.\n\nYour problem is not big enoguth to say that KWin makes imposible to drag & drop on KDE, or that the whole idea of drag&drop is destroyed.\n\n"
    author: "Git"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "If you try to drag and drop in this specific way, it doesn't work!\n\nAnswer: don't do it that way.\n\nI sympathize; it took some getting used to, but there are plenty of workarounds suggested.  It's certainly not something that should be changed by default.  *Maybe* an option somewhere."
    author: "MamiyaOtaru"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "> If you try to drag and drop in this specific way, it doesn't work!\n\nWorks fine for me, this way, with the correct configuration, at kde 3.5.7.\nBut, I agree, the config for such a behavior is'nt really useful for daily work."
    author: "Chaoswind"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "There are workarounds, but the real thing is that this won't ever work - the way X.org works makes it impossible (according to the kwin developers, that is).\n\nI do find it unfortunate, but :("
    author: "superstoned"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-06-01
    body: "X have support for this!!\n\na focus can rise the window or not, most people expect that to happen, but there are several WM that allow it... fluxbox is one of then, it have a option \"click to rise\"... as soon you click on a window, it rises (the focus is another option), so disabling this will allow one to click and work on a window that is below another one... i maybe wrong, but i think i saw one option in kwin to not \"rise on focus/mouse button\"  when i used it\n\nhiguita"
    author: "higuita"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-05-31
    body: "Actually, I prefer the \"focus follows mouse\" policy. That way, there is no problem with drag and drop (as opposed to bloody window$, which bring the window to the top when it is clicked. No way to simply _activate_ a window, while keeping it behind, where it was the whole time :(. Now THAT'S annoying.)"
    author: "zonk"
  - subject: "Re: kwin is bad for drag & drop!"
    date: 2007-09-18
    body: "Uhm, I don't know where you get the idea from, but drag'n'drop works fine for me... Just one tiny thing you must change:\nKDE control center-> Desktop -> Window behavior -> Window actions (tab)\n\nand there is an option titled \"Inactive inner window - Left button\".\nChange that to \"activate & pass click\" instead of \"activate, raise & pass click\".\n\n"
    author: "Ian"
  - subject: "Gentoo packages of KDE4"
    date: 2007-05-31
    body: "You can find gentoo-packages of KDE 4 and a guide for installing in the gentoo overlay-wiki: \n\nhttp://overlays.gentoo.org/proj/kde/wiki"
    author: "Arne Babenhauserheide"
  - subject: "Youtube comments"
    date: 2007-05-31
    body: "Could someone from here with a youtube account posts some comments? \n\nI just saw the wavy-windows effect (via youtube-dl and mplayer) and I rather liked it, since it would make a great screensaver. \n\nMany of the people there seem to just think \"not what I know\" or \"doesn't work for what I want it for, therefore it's useless\" or similar, and I think that the videos should get some really fair feedback. "
    author: "Arne Babenhauserheide"
  - subject: "The first screenshot..."
    date: 2007-05-31
    body: "In the first screenshot... Why is the \"Configure window effects\"-field wider than the field (the one with \"PresentWindows\", \"blur\" etc.) beneath it? A tiny nitpick, I know, but I think that details like that are quite important (and quite easy to fix)."
    author: "Janne"
  - subject: "What about KDE theme manager?"
    date: 2007-05-31
    body: "Hi, I'm very impressed with the progress KDE 4 is making - keep it up!\nJust wondering about KDM (the theme manager from http://www.kde-apps.org/content/show.php?content=22120) - apparently it is being integrated into KDE 4? What's the latest? Will the theme manager be as easy to use as GNOME's (i.e. drag-and-drop the theme file into the manager and it's ready to go)? How will it interact with kwin_composite and plasma? I really hope theming becomes easier with the next KDE release.\n\nGood luck with everything,\n\nJan"
    author: "Jan Dixon"
  - subject: "Re: What about KDE theme manager?"
    date: 2007-05-31
    body: "kdm is not a theme manager, its kdes login application. the link you posted is only used to choose a theme for kdm, not kde.\n\nalso, kde3.5 allready has a theme manager (to be found under appearance and themes in controll center) that makes it possible to to configure desktop-background, colors, widget theme, icons, fonts and the screensaver with one file.\n\n\nso it works exactly like gnome. the only difference is that gnome users tend to produce more \"theme-files\" than \"widget-theme-engines\", and kde users do the opposite.\nbecause widget-engines are compiled programs, they should be installed by the packagemanager of your distribution."
    author: "ac"
  - subject: "Re: What about KDE theme manager?"
    date: 2007-05-31
    body: "I'd be happy if they just made it easier to configure KDM from the text files. I still haven't been able to get KDM to do multiple sessions on different local X servers. GDM does this quite nicely with a little editing of the config file."
    author: "Ryan"
  - subject: "Thanks again!"
    date: 2007-05-31
    body: "Yet another great article Troy\n\nGreat work Kwin devs and the rest of the KDE4 devs - KDE4 is shaping up to be something quite amazing!"
    author: "Joergen Ramskov"
  - subject: "Youtube?"
    date: 2007-05-31
    body: "well you don't want kwin to be ported to windows so people will have more reason to use a more open platform, but require me to use non-free-software to be able to follow this article about free software?\n\n\ncome on guys, we really should be better than this. and dont tell me its a matter of bandwidth. if it is, go send me the files in ogg theora and i'll host 'em!\nor just host them on theorasea.org, maybe? same features as youtube, but free codecs and free software...."
    author: "hannes hauswedell"
  - subject: "Re: Youtube?"
    date: 2007-05-31
    body: "As a journalist writing about KDE, sometimes getting wider exposure is more important than purely utopian format choices - if more people use KDE (and *nix in general) because of what I write, then getting them over to open formats also becomes easier.  Pretty much 100% of *nix media players play theora (and probably 95% play flash).  But we only have ~5% of the market, and I doubt 2% of windows/mac play that format.  In order to get them to view the videos, I have to hit them in their format.  It's sad, but it's a critical mass thing.\n\nFeel free to use free software decoders, like swfdec or even mplayer to play the files (as discussed above), and/or recode them into theora format.\n\nAnd finally, a power user tip, for the amd64 users that are trying to use the official flash player, and don't want to have to do the whole 32bit jail and all that stuff... you can use konq64 with flash32.  You just need to overwrite the programs 'nspluginscan' and 'nspluginviewer' with the 32-bit versions - and assuming you have enough of the 32bit libs installed, you can simply use the 32bit plugins within the 64bit konq.  This doesn't work for firefox - in that case, you need to use the 32bit firefox.  "
    author: "Troy Unrau"
  - subject: "Re: Youtube?"
    date: 2007-06-01
    body: "nspluginwrapper (http://gwenole.beauchesne.info/projects/nspluginwrapper/) does a notable job as well. It lets you use 32 bit plugins on 64 bit browsers. On kde 3.5.6 you need to apply a patch+recompile kdebase before konqueror uses it, but i believe the patch was included on 3.5.7, so it's just a matter of installing nspluginwrapper, and registering the flash plugin with it."
    author: "Fede"
  - subject: "Re: Youtube?"
    date: 2007-06-01
    body: "thanks for your reply.\n\nfirst of all i am kind of dissapointed that you think of the dot as a webpage primarily for convincing windows users and not as an information platform for current kde-users.\n\nsecond: if you had had a look at the link i sent you (theorasea.org -> iTheora), you would have seen that the plugin used for displaying the theora videos works BOTH with a mediaplayer supporting theora AND with a builtin java-based theora player that works on all platforms that have java. and i am pretty-sure the masses of windows users with flash that regularly read the dot also have java or not?\nplease prove me wrong if i say that iTheora is as easy as the flashvideoplayer.\n\nand right now flash does not work properly with free software! yes i could read the sourcecode or use certain programs to find out the location of the flv and then fetch that and then look at that with mplayer, BUT i dont think it is right.\nflash is a major problem for free software nowadays and by using it you counteract the hard work of other free software developers trying to establish free platforms.\n\nyour flash videos might make some windows fanboy somewhere think wow, but if this person is not even willing to watch a video in another format (and i could similarly argue that most somewhat technical windows users use media palyers that support theora) than he will not install another operating system, nor will you be able to convince him of what \"software freedom\" means.\n\nthanks for your work, this is not meant as flame, i am just seriously afraid of what is happening to the community..."
    author: "hannes hauswedell"
  - subject: "Re: Youtube?"
    date: 2007-06-03
    body: "As just a side note - I did not personally make those videos.  They were made weeks (if not months) before I wrote this article.  At the time while I was writing the article, I could not get the GL composite effects to work for my system to make new ones, so I simply reused them with permission.  I've since been able to get it running (by updating the proprietary nvidia drivers to the latest version, which you are probably also opposed to using).  Should future videos be made by myself, I will try my best to offer it in an open format.  \n\nHowever, there may be another moral quandary for you then, since I'd be showing off effects that you can get while using a closed driver, which detracts from the open source driver development.  \n\nCan't win them all I guess..."
    author: "Troy Unrau"
  - subject: "Re: Youtube?"
    date: 2007-06-13
    body: "Thanks for responding and caring, really :-)\n\nI personally use intel graphics which actually do provide decent 3d-acceleration wit 100% free drivers. i don't even object  to USING the nvidia driver if someone bought hardware some time ago and doesnt want to / cant afford to buy other hardware. BUT it is a difference whether you privately USE a piece of non-free-software or whether you release media in a non-free format, requiring everyone else to also use non-free software and thus actively promoting the use of non-free software, while discouraging those of us who try to establish alternatives (in contrast to the NVIDIA-blob there actually are alternatives to flash-video).\n\nanyway thanks for listening and for your work in general.\n\np.s.: just found out that www.mux.am offers to convert youtube-videos to ogg theora. so if you are stuck with youtube videos again,drop me a note and i will convert them to theora and host them, so you can instead use iTheora ;)"
    author: "Hannes Hauswedell"
  - subject: "Re: Youtube?"
    date: 2007-08-31
    body: "While I do agree with you, I didn't really have much of a choice. When I bought this computer, the options were:\n\nIntel onboard graphics:\n+ Open-source driver.\n- Onboard-only. Would force me to also buy an Intel processor.\n- Lacks an equivalent to nVidia TwinView.\n- No Dual-DVI. (I use dual 19\" LCDs and I'm very sensitive to analog blur)\n\nnVidia:\n- Has a binary blob for a driver\n+ Easy to get a card with two DVI connectors\n+ Has TwinView (I'm big on offloading operations to secondary processors)\n+ Works with an Athlon64 X2 on an ASUS motherboard.\n\nLet's hope that AMD open-sources the ATI drivers and that puts enough pressure on nVidia to get them to do the same. Heck, let's hope that it improves the quality of ATI drivers in general. It's embarassing to be Canadian and shunning the Canadian graphics card company."
    author: "Stephan Sokolow"
  - subject: "One large desktop"
    date: 2007-05-31
    body: "Is it possible to have just one desktop, larger than the physical screen, and zoom in and out?\n\nAnyway, this finally made the multiple desktop function useful on a single display-computer..."
    author: "Axel"
  - subject: "Re: One large desktop"
    date: 2007-06-01
    body: "Great idea!!! i vote on this one!!\n\nhiguita"
    author: "higuita"
  - subject: "Re: One large desktop"
    date: 2007-06-03
    body: "Should be possible.  X already allows desktops that are larger than the physical screen.  Try hitting CTRL-ALT-+ or CTRL-ALT-- (last key is the minus sign) to zoom in and out already...  Combined with KWin's zoom feature, this would likely be a lot easier..."
    author: "Troy Unrau"
  - subject: "Re: One large desktop"
    date: 2007-06-03
    body: "I've been thinking for a while now on something similar, that I call the 'toroidal desktop'.\nThere's only one desktop of no fixed size, it area is always the sum of the open windows area. Windows are generally (with exceptions like modal dialogs) not allowed to stack on top of each other; opening a new window makes the virtual desktop size increase.\nShowing all open windows is just a matter of zooming out, drag and drop can be improved this way.\nWhy 'toroidal'? Well, you can move around your desktop by dragging it, like you move around Google Earth, with the difference that when you reach, say, the rightmost window and continue dragging to the right, the leftmost window appears; same for up/down dragging. In practice, it's as if windows lived on the 2d surface of a torus.\nTaskbar and desktop icons can be sent to the background and raised when needed, or  stay on top and behave like the health/ammo levels in a first person shooter."
    author: "dafnis"
  - subject: "Re: One large desktop"
    date: 2007-06-04
    body: "This is not at all a terrible idea, and could probably even be implemented within the current kwin framework, however you might have trouble finding an interested developer :)  You could always have a go at it yourself if your C++ is up to spec :)"
    author: "Troy Unrau"
  - subject: "Re: One large desktop"
    date: 2007-06-30
    body: "I was just thinking about something similar and decided to google for toroid desktop. I would like a large virtual desktop that can zoom in and out and pan and whose edges wrap. I would definitely want it to seamlessly add space. I might want to allow overlapping though because often I don't need to see the whole window.\n\nI think I would also like to be easily expand one window or a group of windows while leaving the rest the same size. Think of pulling some of the windows closer to you while leaving the rest at a distance."
    author: "cardiffgiant"
  - subject: "Re: One large desktop"
    date: 2007-09-03
    body: "Just the sort of thing I have always wanted too."
    author: "Benny"
  - subject: "real 3D please"
    date: 2007-05-31
    body: "Hi!\n\nCongratulations for nice 3D effects.\nHowever, I would really appreciate \"real\" 3D effects like Sun have it in their Java Window-Manager and Metisse can do. e.g. turn windows around and have some things (like the options dialogue or just the possibility to write notes onto the back side) on the back side. e.g.\n\nAnother cool thing would be to support the now ever more upcoming \"virtual reality\" 3D goggles and have REAL 3D desktops.\n\nWhat about that?"
    author: "sulla"
  - subject: "Re: real 3D please"
    date: 2007-06-01
    body: "Honestly, the \"real\" effects you describe sound more like a gimmick than, for example, an Expos\u00e9-like feature, which I've found indispensable ever since it appeared in Beryl. The fact that it will be in KWin is a huge relief, as I've never been able to keep Beryl stable."
    author: "Till Eulenspiegel"
  - subject: "Claiming that technical lock-in is an advantage..."
    date: 2007-05-31
    body: "\"Additionally, while KDE 4 will be supporting a number of platforms with libraries and applications, KWin is one of the applications that will not be making the switch as it is inextricably tied to X. This should be considered to be a Good Thing(tm), as it ensures that KDE will always be the best looking when used with Linux/UNIX, and hopefully it (and related KDE Workspace technologies, like Plasma) will remain a unique benefit of using a more open operating system.\"\n\nThis is a REALLY weird attitude. Actually it's bizarre. I know it is common among Linux people nowadays to say \"hey, let's not port this and that app to Windows, so we can make Linux look cooler\". This whole attitude smells of Bill Gates. Linux and open source projects can only be truly successful (and by successful, I mean more than just having most of the market share or whatever) if they show respect to ALL users.\n\nAdditionally, what if I actually need to use Windows or Mac OSX some time? Or what if I want to do it? Why shouldn't I be able to run KDE - my favourite environment then?\n\nNow, I'm pretty confident that the KDE devs didn't mean to \"lock\" KWin to keep Windows and Mac users out in the cold, but it's still nothing to cheer about. And I hope that future versions of KDE will be even more portable.\n\nOther than that paragraph, this was a great article, and I'm looking forward to KDE4 more than ever. I think the KDE devs made the right move to work on KWin instead of Compiz/Beryl, and it looks like it will be easy to add more functionality and usability along the way aswell. Great work!"
    author: "mirshafie"
  - subject: "Re: Claiming that technical lock-in is an advantage..."
    date: 2007-05-31
    body: "It seems to me that this technical lock-in is a side-effect of using technologies tied to X - if it could be easyly ported, somebody will port it because it is open source. By the way, how could a window manager be ported to Windows?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Claiming that technical lock-in is an advantag"
    date: 2007-06-01
    body: "That is exactly why it cannot really be ported - it is stuck with X.  Where X goes, KWin may follow, assuming AIGLX/Xgl/etc. and XRender and ... and ... and... are all available on that platform.\n\nSo, for the time being, Plasma and KWin will stay on X.  The applications can be ported, and they can even adopt a native theme on whatever system they are running on, should you like it that way - or use the regular KDE themes (but not window decorations...) Whatever makes you happy."
    author: "Troy Unrau"
  - subject: "Re: Claiming that technical lock-in is an advantag"
    date: 2007-06-02
    body: "Yes, I understand that KWin is tied to X for technical reasons. But to claim that it is actually good that it is that way is a completely different matter."
    author: "mirshafie"
  - subject: "Re: Claiming that technical lock-in is an advantag"
    date: 2007-06-01
    body: "-I thought you could run X inside Windows?\n--Probably not the best idea though.\n\n-Can't you change the WM in windows from the default explorer?\n--I know I saw this on slashdot... (dunno about Vista though)\n\n-Run a virtual machine maybe?\n--It's becoming ALL the rage\n--Better yet, get a mac, triple boot and/or run VMs!\n\n-How many Windows users know what a WM is?\n\n-And it is a Good Thing(tm)\n--Why? Because Windows isn't a Good Thing(tm), or a free thing for that matter.  Wouldn't it be better to entice people to use the free alternative?  Besides the people who are being \"locked out\" are either lazy, completely computer illiterate, or they genuinely like Windows.\n\n-AFAIK there are a lot of open source apps that have been ported to Windows, and a lot of them aren't as good on it (but hey, I haven't used Windows regularly in several years).\n\nAnd this might just be bias speaking but, why shouldn't people make Linux look cooler?  I don't know myself but wasn't there some concern about losing users to Windows with the port to QT 4, and KDE 4?\n\nSorry for the cynicism, but I feel that way right now.  And most of my questions are questions, not necessarily rhetorical.\n\n-Nobbe"
    author: "Nobbe"
  - subject: "Re: Claiming that technical lock-in is an advantag"
    date: 2007-06-02
    body: "\"-And it is a Good Thing(tm)\n --Why? Because Windows isn't a Good Thing(tm), or a free thing for that matter. Wouldn't it be better to entice people to use the free alternative? Besides the people who are being \"locked out\" are either lazy, completely computer illiterate, or they genuinely like Windows.\"\n\nThe reasoning behind this attitude, as I understand it, is that we shouldn't give people open source alternatives for Windows so that people completely make the switch to Linux. And that's fine for anyone who hates to play games without hours upon hours of tinkering with something, OR for people who are actually tied to Windows by some application. Look, there are HUNDREDS of reasons to use Windows instead of anything else simply because lots of completely irreplacable software is designed for Windows. Deal with it.\n\nAnd what if people actually WANT to use Windows, but with as much open source software as possible? Who are we to judge them? I'm not saying KDE developers should use their limited time to port KWin or really any other app to Windows, but to say that it is GOOD (!) that it's hard or even impossible to port some of the stuff is WEIRD.\n\n\"And this might just be bias speaking but, why shouldn't people make Linux look cooler? I don't know myself but wasn't there some concern about losing users to Windows with the port to QT 4, and KDE 4?\"\n\nIf \"we\" lose users to Windows, so what? The important thing is that people use computers the way they like. It's not a war people, and it's not a competition...\n\nIf some KDE users would actually be happier with KDE on Windows then LET THEM GO."
    author: "mirshafie"
  - subject: " po"
    date: 2007-06-02
    body: "I beleive parts of the WM in Windows are built into the kernel.\n\nThink about it..."
    author: "forrest"
  - subject: "Great work!"
    date: 2007-06-01
    body: "Impressing! I especially like the\nliving thumbnails and the zoom feature.\n\nSebastian\n"
    author: "Sebastian Pipping"
  - subject: "Interesting, but"
    date: 2007-06-01
    body: "It's some good effects with potential, but I think some thought needs to be put into keyboard/mouse interaction.  Switching between them involves some lag time, and when I see the mouse pointer move, and then typing for an expose-like extension, I think some refinement could help."
    author: "Daniel Rollings"
  - subject: "sighs"
    date: 2007-06-08
    body: "all the changes in KDE4 really make me wonder what the future holds, I love KDE. and couldn't imagine useing any other Desktop, but more and more sillyness keeps getting added. and once something is added to kde its there forever, dolphin, kbfx, i swear any app written for kde will just be thrown in. and now were talking about composite window managers. which offer very little in useful functionality  compared to how much recourses they suck up. It seems a usable effective desktop is not the goal, but how many cool tricks can we throw in. Its free and the developers do a great job, I dont mean to discount all their efforts. its much appreciated. but theres also a user base who has come to love kde. its only natural they might want to voice there opinions or concerns about KDE. just my thoughts for what its worth..\n\nleave the silly, composite effects to someone else berl or whoever, and kbfx umm why kde already has a menu. same with dolphin. and how many mp3 players are we up too right now ?\n\nI guess bash me all you want for having an opinion and caring about my favorite desktop"
    author: "bm"
  - subject: "Re: sighs"
    date: 2007-06-08
    body: "> and now were talking about composite window managers. \n> which offer very little in useful functionality compared to how \n> much recourses they suck up.\n\nIf you don't want the effects (and arguably not all them are useless eye-candy as you seem to be suggesting) then you can easily turn them off, just as you have been able to with other effects in previous versions of KWin.\n\n> and how many mp3 players are we up too right now ?\n\nHow many mp3 players are being added to KDE 4? I count zero so far."
    author: "Paul Eggleton"
  - subject: "Re: sighs"
    date: 2007-06-08
    body: "\"and once something is added to kde its there forever\"\n\nLots of things have been removed from the stock KDE install - Kedit being just one recent example.\n\n\"dolphin,\"\n\nWhat's wrong with Dolphin? It's shaping up to be an excellent file manager.\n\n\"swear any app written for kde will just be thrown in\"\n\nkde-apps.org lists 47858 contents.  I don't know how much of this is actual apps, but it's clear that your comment is hyperbole designed only to irritate the developers.\n\n\"and now were talking about composite window managers. which offer very little in useful functionality compared to how much recourses they suck up.\"\n\nI think kwin_composite is great, and the \"Expose\", live tabbing and grid effect are incredibly useful.  And - guess what! - it's a plugin that's off by default.\n\nI'm endlessly irritated by people who whinge about defaults that can be changed in 10 seconds - the \"Dolphin\" article here on KDE had some of the most spoiled, ungrateful and, frankly, downright stupid displays of belly-acheing I have ever seen, short of a tantrum my 3 year old nephew once threw.  But to complain about something that won't even inconvenience you unless *you explicitly turn it on* - that's a whole new level of whininess.  Well done!\n\n\"It seems a usable effective desktop is not the goal\"\n\nYes it is, and I'm sure the overworked usability team who work tirelessly to try and design you a better desktop appreciate your thoroughly negative and wholly incorrect comments.\n\n\"leave the silly, composite effects to someone else berl or whoever\"\n\nWhy?\n\n\n\"and kbfx umm why kde already has a menu\"\n\nWell, you'd better let Aaron Seigo know that Plasma has a menu already - he'll be thrilled to bits as it would be one less thing to do before October! What's the name of KDE4's menu?\n\n\"same with dolphin\"\n\nHow many specialised File Managers with a focus on usability already exist in the stock KDE install? HINT: none.\n\n\n\"and how many mp3 players are we up too right now ?\"\n\nPaul has fielded this one with far more patience that I could muster.\n\n\"I guess bash me all you want for having an opinion and caring about my favorite desktop\"\n\nThis, for me, was probably the most grating part of your post.  I'll bash you, alright, but not for \"having an opinion\" or \"caring about my favourite desktop\" - I don't, after all, want to fuel that little martyr syndrome you've spent time nurturing.  I'll bash you for your stream of pointless and thoroughly unconstructive nitpicking; your casual dismissal of the hard work of a team of volunteers that either benefits you directly or doesn't impact you at all; your statement about the usability goals of KDE which are just hilariously wrong; and your complete lack of research about what's actually going in to the default KDE install (a lot of effort is going in to trimming down the set of apps so that it is lean and lacking in redundancy, but still functional).  \n\nThe number of people I've seen on these comments doing something like \"Well, I can't be bothered to research this so I'll just assume the KDE developers are doing the stupidest possible thing and directly accuse them of it!\" is mind-boggling, and they invariably put that clever little disclaimer on the end: you know, the one that means that anyone who disagrees with you can instantly be derided as trying to deprive you of a voice, or who just doesn't *care* about KDE as much as you do, regardless of the validity of their criticisms of your post.\n\nSo, if you *really* care about KDE as much as you claim to, I'd suggest the following:\n\na) Learn something about what you are criticising, rather than just assuming the KDE developers are doing something purely because it's the stupidest path possible/ to annoy their fanbase/  they were drunk and it seemed cool at the time.  KDE does not just throw stuff in willy-nilly, it does not have however many MP3 players you think it has in the default install, and it is very focussed on usability.  \n\nb) If you have a legitimate complaint, don't whinge about it and berate the developers and then whine \"Just my opinion! Just my opinion!\".  It's the equivalent of sidling up to a guy, kicking him in the nuts, and running away screaming.\n\nMake sure the complaint is valid, and *give reasons* that have not been discussed and shot down ad nauseum.  You have not given a single reason why you think, for example, that Dolphin is bad, so your slur against it is the exact opposite of the \"constructive criticism\" I'm sure you'd try to spin your post as! \n\nWhen you've done this, *raise your objections like an adult*.  Don't throw yourself on the dot.kde.org stage and cry, theatrically, \"Oh, Lords of KDE - why have you turned upon humble and devoted servants such as I with your cruel caprice! Oh woe is me, that they shall not hear my pleas born of love; for their concerns are those of Bling, and Bloat, and the sowing of Confusion amongst their followers, and they strike the faces of any who dare speak against them, even though my words are intended to guide them down the True Path (i.e. the path I selfishly want them to take).  PS don't criticise my misguided words because they're Just My Opinion(TM)!\".  This kind of approach is something you should grow out of when you're a teenager.\n\nIf you are concerned about some choice that the KDE devs might make in the future, then *give them the benefit of the doubt*.  Don't simply assume that they have already made up their minds to commit to whatever nefarious decision you've conjured up in your head - raise the issue and say something like \"Hi, I'm a little concerned about $X - have you made any decisions on what you are going to do about it? I think most KDE fans would prefer $Y\".  Don't say things like \"I have absolutely no reason to assume this, but I just *know* that Plasma is going to have Goatse on every panel and will play loud high-pitched white noise continually and destroy all your work etc.  Why have the KDE developers decided to do this? Don't they care about all their fans who have selflessly used the work they have produced for years [as if Free Software was a game of Populous and developers gained manna simply by having people passively consume their products and not lift a finger to help them].  I guess I'll have to go on back to GNOME :'(\" followed by the theatrical *SIGH* intended to conjur up images of grubby-faced orphans collapsing in the gutter.  Seriously, voice your opinions like an adult, and knock off the pitiful attempts at emotional blackmail - it's embarrassing.\n"
    author: "Anon"
  - subject: "Re: sighs"
    date: 2007-11-17
    body: "just ignore this troll"
    author: "asdx"
  - subject: "how to enable effects module?"
    date: 2007-12-23
    body: "Great article -- one piece is missing though. How to get this extra module with effects? All I can find in google is how to set up the composite for Xorg (done) or how to click in the translucency checkbox. \n\nBut what to do in order to get extra tab/module with KWin effects? I didn't find the answer while googling and I didn't find anything in repos (opensuse).\n\nThanks in advance for your help."
    author: "Maciej Pilichowski"
  - subject: "Re: how to enable effects module?"
    date: 2007-12-23
    body: "Mystery solved. I was misguided by the similarity in the videos (in comments) to KDE3 ;-) I thought it is available here too."
    author: "Maciej Pilichowski"
---
KWin, KDE's window manager, has been around since KDE 2.0 (replacing KWM in KDE 1.x) and has grown to be a mature and stable window manager over the years. For KDE 4, however, there were a few people rumbling about visual effects, and perhaps KWin was feeling a little envious of its younger cousins Compiz and Beryl. While these new effects have created a lot of buzz around Linux and UNIX, long-term KDE users have wished they can enjoy the effects of Compiz/Beryl while still having the tried and tested window manager that is KWin. As a result, for KDE 4, KWin has received a huge graphical upgrade, with composite and GL support. Read on for more details.




<!--break-->
<p>KWin has implemented effects in a way that allows a number of different rendering methods to be used, depending on your specific combination of hardware and drivers. These features have brought KWin rapidly into the era of dazzling eyecandy, along with some pleasant surprises on the usability front. This effort has been spearheaded by Lubos Lunak (a man known for his efficient code) and his team, with special mention to Rivo Laks and Philip Falkner for their contributions. </p>

<p>Effects are disabled by default at the moment, although that may change before KDE 4 ships, and distributions may decide to alter this setting anyway. When enabled, the effects are designed to degrade gracefully. If GL is not available, KWin disables GL effects but still allows Composite effects where possible via XRender. If XRender is not available, it falls back to plain X, running in the same fashion as the present KDE 3 version. To get the full array of effects, you need to have a video card (and driver) that supports AIGLX, XGL or use the proprietary Nvidia driver.</p>

<p>Once the effects are enabled, it's simply a matter of choosing which effects you'd like to activate. So far, Rivo Laks has been working on the effects plugin selection interface (see the screenshot below). The new plugin selection widget shown is making its way into various parts of KDE - it does automatic dependency checking, so once the dependency tree is known, it will intelligently enable or disable dependent plugins. This widget is also showing up in other parts of KDE 4.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol16_4x_kwin_effectselection.png"><img src="http://static.kdenews.org/dannya/vol16_4x_kwin_effectselection_thumb.png" alt="The plugin selector for KDE 4" /></a><br />(as you can see in the image, this dialog is quite new - less than a week old - and is missing all the icons...)</p>

<p>Lubos has been periodically blogging about the effects that KWin is now capable of, and has recorded a number of videos showing them off. Since video capture on my system is rather chunky, I will present his recordings instead. So, without further ado, I present some of the more popular of his flash videos, hosted by YouTube. If you are interested in more, please visit his <a href="http://www.youtube.com/profile?user=Seli158">YouTube User Page</a></p>

<p align="center"><object width="425" height="350"><param name="movie" value="http://www.youtube.com/v/SWaSz4smYlg"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/SWaSz4smYlg" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed></object><br />
The Present Windows Effect - a very useful effect that falls into both eye-candy and usability categories...</p>
<p align="center"><object width="425" height="350"><param name="movie" value="http://www.youtube.com/v/LMnmGdk1ODs"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/LMnmGdk1ODs" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed></object><br />
The Desktop Grid Effect - those familar with the Cube effect may note that this is not quite as flashy, but probably more useful. Doesn't mean there cannot be a Cube effect for KWin though.</p>
<p align="center"><object width="425" height="350"><param name="movie" value="http://www.youtube.com/v/zLSsAPBiudE"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/zLSsAPBiudE" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed></object><br />
This one shows the above two effects, as well as the Alt-Tab thumbnails, but it shows that the effects work great, even when the windows contain active videos.</p>
<p align="center"><object width="425" height="350"><param name="movie" value="http://www.youtube.com/v/zGRdfI5WKIg"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/zGRdfI5WKIg" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed></object><br />
Zoom Effect and Magnifier Effect: some accessibility related features that everyone may find useful, depending on your specific needs.</p>
<p align="center"><object width="425" height="350"><param name="movie" value="http://www.youtube.com/v/ZQWkt6_6pGs"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/ZQWkt6_6pGs" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed></object><br />
Effects like this one make people go "Wow". The first part of this video features the Fall Apart Effect, which basically has a window blow up. It's amazing how well this effect can be demonstrated in a low quality flash video...</p>

<p>Aside from Lubos, many of the new effects and underlying core components were programmed by Rivo Laks and Philip Falkner. They are responsible for many of the effects you see in the videos, including the Present Windows Effect, and the improved Alt-Tab dialog. There have been a number of contributions from others as well, and they are always looking for new and interesting ideas. In addition, KWin for KDE 4 builds on the already existing KWin version which has had dozens of people contribute to it over the years.</p>

<p>The window decoration shown above is called 'kwin3_crystal' and is still set as the default in SVN. It is simply a port of the existing KDE 3 Crystal window decorations, however, a new KWin window decoration is still in the works for KDE 4 - it hasn't been made the default yet, so I haven't been featuring it. When it does eventually become the default, you'll be sure to hear about it here (and likely in Danny's <a href="http://commit-digest.org/">KDE Commit-Digest</a> as well...).

<p>KWin for KDE 3.x implemented a very simple composite manager, allowing simple effects such as window transparencies, fading menus, shadows, and so forth. The code was not too complex, but the infrastructure was not in place to seriously extend the effects to GL powered goodness. When the KDE 4 development series opened, it was seen as an excellent time to rewrite some of KWin's internal structures in order to support such effects. There were initial considerations of implementing support for the existing Compiz and/or Beryl system of effects via plugins, but there were technological hurdles that prevented this. I won't go into the technical details as to why this decision was made, however, it is important to note that KDE 4 will still work with Compiz/Beryl should the users choose to use that software instead of KWin.</p>

<p>Additionally, while KDE 4 will be supporting a number of platforms with libraries and applications, KWin is one of the applications that will not be making the switch as it is inextricably tied to X. This should be considered to be a Good Thing(tm), as it ensures that KDE will always be the best looking when used with Linux/UNIX, and hopefully it (and related KDE Workspace technologies, like Plasma) will remain a unique benefit of using a more open operating system.</p>

<p>KWin promises to ensure that KDE get the graphical boost it needs to keep the eye-candy folks happy, while providing new and usable features for the desktop environment that would not have otherwise been possible. Yet, it maintains the rock-solid foundation that a long history as an integral part of KDE has provided. It will still work (with reduced levels of effects) on any system that KDE 3 ran on, so no-one is left out in the cold. It is already the default for KDE 4 in SVN, and will be showing up in future beta releases.</p>

<p>On a personal note, I've found that KWin on my system was dropping down into XRender mode due to some X settings I need to fix, but it has been perfectly stable for me over the last two weeks. In fact, every week when I'm rebuilding KDE 4 to write these articles, I am more amazed at how quickly it is becoming stable and useful. If you are interested in testing it out for yourself, check to see if your distribution has packages available. I am aware of the existence of at least one live CD (where you don't have to risk messing with your system) that is available at the <a href="http://home.kde.org/~binner/kde4-live-dvd/">KDE Four Live</a> website. They update the Live CD every few weeks, and currently has the KDE 4.0 Alpha 1 packages. Additionally, if you are brave enough to test the Composite features, and are having problems, have a quick look at the <a href="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/workspace/kwin/COMPOSITE_HOWTO">Composite HowTo.</a>. If you have problems, please report bugs using the <a href="http://bugs.kde.org/">the KDE Bug Tracker</a> by selecting the KWin program, and the "composite" component.</p>

<p>Until next time.</p>



