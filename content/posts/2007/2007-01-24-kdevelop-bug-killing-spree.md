---
title: "KDevelop Bug Killing Spree"
date:    2007-01-24
authors:
  - "jgaffney"
slug:    kdevelop-bug-killing-spree
comments:
  - subject: "IRC"
    date: 2007-01-23
    body: "Note that people doing bugsquad are to be found on the #kde-bugs IRC channel @ FreeNode."
    author: "Bram Schoenmakers"
  - subject: "Re: IRC"
    date: 2007-01-24
    body: "Isn't there a MUC room?"
    author: "fddf"
  - subject: "Ehre wem Ehre geb\u00fchrt"
    date: 2007-01-23
    body: "Note that the incredible decrease of bugs was almost completely the work of two dedicated people, namely Andreas Pakulat (apaku) and Jens Dagerbo (teatime). In an article like this, they should really be mentioned in a prominent place. Hooray for the KDevelop bug squad!\n\nAnd of course, all of those fixes will be in the (already tagged, now only days away) KDevelop 3.4 release. Which is, without doubt, the best KDevelop ever released."
    author: "Jakob Petsovits"
  - subject: "Re: Ehre wem Ehre geb\u00fchrt"
    date: 2007-01-24
    body: "Indeed, Andreas and Jens have been on the top 10 bug killers list for the last 7 weeks!\n\nPS: download KDevelop 3.4 with all these bugfixes and other impressive changes now: http://www.kdevelop.org/index.html?filename=3.4/download.html\n"
    author: "Alexander Dymo"
  - subject: "Re: Ehre wem Ehre geb\u00fchrt"
    date: 2007-01-24
    body: "i havent found any KDE4 templates (so i'm playing with qt4 one right now:)"
    author: "nick"
  - subject: "Re: Ehre wem Ehre geb\u00fchrt"
    date: 2007-01-24
    body: "http://www.google.com/search?q=kde+4+template&ie=UTF-8&oe=UTF-8http://www.google.com/search?q=kde+4+template&ie=UTF-8&oe=UTF-8\n:)"
    author: "nick"
  - subject: "I've been running svn kdevelop"
    date: 2007-01-24
    body: "...and I just have to say it is excellent! The best KDevelop yet. Thanks guys!"
    author: "Marble"
  - subject: "Best C++ IDE"
    date: 2007-01-24
    body: "With KDevelop 3.4 KDevelop is one step nearer to it's goal to be the best C++ IDE for Linux. Well - I'd even say: \"goal reached!\".\nNext step: \"Best C++ IDE of all\".\n\nI'm working with KDevelop and VisualStudio. And KDevelop is doing very well. Navigating in code for example is clearly better.\n* Bookmarks\n* Quick open class / method\n* Switch header / implementation\n* No dog-slow intellisense\n\nHere's a small list VS is doing better:\n* Debugger (container of STL for example)\n* Project management\n* Call tree (extremely useful if you want to know all functions that call a specified other one)\n\nThe improvements from KDevelop 3.3 to 3.4 where amazing. Thanks for your hard work!\n"
    author: "Birdy"
  - subject: "Re: Best C++ IDE"
    date: 2007-01-26
    body: "Code completion in KDevelop is still slower than and not as good as intellisense. I can provide examples of simple code it doesn't complete if you dispute this!\n\nThat said, 3.4 is much better than previous releases."
    author: "Tim"
  - subject: "KDevelop 3.4 and Qt4?"
    date: 2007-01-24
    body: "Does KDevelop handle Qt4 in a seamless way? For someone that wants to develop pure Qt4.2 (or higher) apps with QMake, Assistant, Designer, Linguist & integrated, is KDevelop a good choice? I don't want to have any Automake stuff nor CMake (don't have it installed) so pure QMake is preferred.\n\nNevertheless, I'm an \"Emacs + separate apps\" guy myself, but I'll definitely give KDevelop 3.4 a spin once it is released.\n"
    author: "Chakie"
  - subject: "Re: KDevelop 3.4 and Qt4?"
    date: 2007-01-24
    body: "Yes, KDevelop 3.4 handles pure Qt4 projects very neatly. KDevelop has a qmake manager that allows to graphically add/remove/manage your files. Designer is called when you click on .ui files. So yes, you can give it a go!\n\nAnne-Marie"
    author: "annma"
  - subject: "vim with kdevelop?"
    date: 2007-01-26
    body: "I'm wondering if it's going to be possible to embed vim as the text editor in kdevelop again.  It used to be possible but it seems to have been discontinued.  I know some people recommend yzis as a replacement but it isn't really good enough.  Vim in kdevelop would be simply amazing, but without it I feel shackled."
    author: "bob"
  - subject: "Re: vim with kdevelop?"
    date: 2007-09-25
    body: "oh yes. i'm looking for that feature for a long time now. still nothing. yzis is not even close to vim :("
    author: "guest"
  - subject: "kdevelop3.4 /qt4"
    date: 2007-01-27
    body: "works and works great : So long waited and here now...\nyou make a happy \"papy\" kdevelop :)\nthanks all\nHenri\n"
    author: "Henri Girard"
---
Thanks to the efforts of the <a href="http://www.kdevelop.org">KDevelop</a> team and the bug squad, the number of bugs in KDevelop has been reduced by more than half -- as of the current count, 186 bugs!  To find out more, please check out the <a href="http://bugs.kde.org">KDE Bug Tracking System</a>.

As always, everyone is invited to help with bug triage.  Bug triage is a great way to contribute to KDE, without requiring programming skills.  All you need to do is have the latest version of the application installed to help confirm bugs.  To get involved, check out the <a href="http://developernew.kde.org/Contribute/Bugsquad">Bug Squad Wiki Page</a> and help the KDE developers spend more time fixing bugs instead of identifying them.

Thanks again go to the KDevelop team and the bug squad, great work!


<!--break-->

