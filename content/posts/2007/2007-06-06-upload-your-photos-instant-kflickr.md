---
title: "Upload Your Photos in an Instant with Kflickr"
date:    2007-06-06
authors:
  - "jbrockmeier"
slug:    upload-your-photos-instant-kflickr
comments:
  - subject: "Why not integrate with digikam?"
    date: 2007-06-06
    body: "Why not integrate such apps with digikam? It makes sense to have the user download his photographs and then upload it using the same software and interface."
    author: "Amit"
  - subject: "Re: Why not integrate with digikam?"
    date: 2007-06-06
    body: "If you read the article, you'll see they mention that digiKam already includes this functionality :-)"
    author: "Antonio"
  - subject: "Re: Why not integrate with digikam?"
    date: 2007-06-06
    body: "Doesn't work, though. Gives eternal invalid token errors, and there seems to be no workaround I could find. So I use send2kflickr instead.\n\nNo, I did not file a bug report because I just found out about it (I had never imagined this would be under File->Export :-)"
    author: "Roberto Alsina"
  - subject: "Re: Why not integrate with digikam?"
    date: 2007-06-06
    body: "The digikam \"export to flickr\" plugin works fine for me. I've had situations where it was not loaded at startup a few times and had to restart digikam but apart from that, it's working fine here. It would be nice to know why it does not work for you.\n\nJP\n\nPS: there is a cosmetic but annoying problem with the \"flickr export\" being in the background though."
    author: "JP Renaud"
  - subject: "Re: Why not integrate with digikam?"
    date: 2007-06-06
    body: "Honestly? No idea. Tells me my token is invalid, sends me to the page, it's authorized, I click ok, then back to \"the token is invalid\".\n\nI can't find where the token is supposed to be stored, so no clue as to how to cjeck the token."
    author: "Roberto Alsina"
  - subject: "Re: Why not integrate with digikam?"
    date: 2007-06-07
    body: "Its a problem with your kipi-plugins package. The older version has some problems. Just update it and you'll be all set."
    author: "Sarath"
  - subject: "Re: Why not integrate with digikam?"
    date: 2007-06-06
    body: "The digikam flickr plugin is as basic as it gets. Kflickr is much more convenient, especially for large batches where you want to name and tag all the photos."
    author: "Guillaume Laurent"
  - subject: "Re: Why not integrate with digikam?"
    date: 2007-06-08
    body: "It is!! KFlickr is implemented as a KPart allowing this to happen. It is known as Send2KFlickr. There are limitations which I would love to get around, mostly the ability to pass tags from digikam to kflickr. The limitations are due to the plugin structure provided by digikam. With some collaboration with the digikam folks this can be addressed, however, having just had a second child I just don't have time to do this at the moment. "
    author: "TundraMan"
  - subject: "Re: Why not integrate with digikam?"
    date: 2007-06-09
    body: "\nCongratulations !"
    author: "physos"
  - subject: "PicasaWeb ?"
    date: 2007-06-06
    body: "Is there a similar KDE solution for PicasaWeb ?\nI've tried f-spot which didn't work for me, the linux picasa version doesn't upload photos (yet, but I've read it will). I have found a solution, which is to install windows picasa version with wine. It works fine, but uploading is very very slow.\nAnyway, an integration with digikam would be much better :)"
    author: "Fran\u00e7ois"
  - subject: "Re: PicasaWeb ?"
    date: 2007-06-06
    body: "F-Spot works perfectly for me...  I guess you should file a bug on why it doesn't work for you.  And digiKam should just re-use the F-Spot backend code for the picasaWeb uploeding!! :) "
    author: "Guga"
  - subject: "Re: PicasaWeb ?"
    date: 2007-06-06
    body: "I did a small script in python it is very very ugly but at least it works. Feel free to improve it...\nI then added a konqueror service menu to resize & upload in\n.kde/share/apps/konqueror/servicemenus/imageuploader.desktop:\n\n[Desktop Entry]\nServiceTypes=image/*\nActions=upsmall;upmedium;uplarge;uporig\nX-KDE-Submenu=Upload\nTryExec=convert\n\n[Desktop Action upsmall]\nName=Small (640xYYY)\nIcon=image\nExec=/home/bilibao/upload-picasa.py -r 640 %F\n\n[Desktop Action upmedium]\nName=Medium (1024xYYY)\nIcon=image\nExec=/home/bilibao/upload-picasa.py -r 1024 %F > /tmp/logupload\n\n[Desktop Action uplarge]\nName=Large (1280xYYY)\nIcon=image\nExec=/home/bilibao/upload-picasa.py -r 1280 %F\n\n[Desktop Action uporig]\nName=Original\nIcon=image\nExec=/home/bilibao/upload.py %F\n"
    author: "Andrea Rizzi"
  - subject: "Re: PicasaWeb ?"
    date: 2007-06-07
    body: "PicasaWeb has API now. Flock is working on integrating this in same way as Flickr ( http://erwan.jp/2007/05/16/flock-photo-interfaces/ ). So it is possible."
    author: "Dolphin-fanatic :)"
  - subject: "photo album ripper"
    date: 2007-06-06
    body: "I'd love an application that does the reverse; rips pictures from an album in various web album sites. Often I and some friends go on a trip, agreeing to share the pictures we take afterwards. But then they put theirs on a web album where they will only live for a short while, since you pay for the storage space and they will want to put up new pictures after some time. My generous offer to use the various means to upload to my Gallery server (I even offer FTP and SFTP) are met with blank stares.\n\nIs anyone aware of such an application? Or what would be the best framework for writing this? It would require a semi-smart crawler to navigate the different pages of an album, go to the highest resolution version of each picture and download that. If I get a choice of language, it would be Ruby. Integration into Digikam comes next, I suppose."
    author: "Martin"
  - subject: "Re: photo album ripper"
    date: 2007-06-07
    body: "right tool for the job; use wget ;)"
    author: "Thomas Zander"
  - subject: "Re: photo album ripper"
    date: 2007-06-07
    body: "It seems that you are joking... Of course the problem is not to download the page, but to parse the HTML and crawl. Oh -- and in many cases, I will have to log on to the site (and accept the resulting cookie) before being able to access an album that was shared by a friend. So starting from some web robot toolkit would be the way to go.\n\nAny such toolkit that you could recommend?"
    author: "Martin"
  - subject: "Re: photo album ripper"
    date: 2007-06-08
    body: "Ahem.  I'll repeat his decidedly non-joking recommendation.  Try wget.  Read the documentation.  You might be surprised, but it can do exactly what you're talking about, including intelligently parse and crawl html.  It will fetch but not parse css and javascript (unless it's been added since last I checked)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: photo album ripper"
    date: 2007-06-08
    body: "I see. I was vaguely aware that wget had some power built into it, but it seems that it had a bit more than I knew of.\n\nAlso, please understand that it is decidedly hard to parse both the mood and the technical content from a comment that contains seven words + a smiley :-)"
    author: "Martin"
  - subject: "Re: photo album ripper"
    date: 2007-06-10
    body: "Python + BeautifulSoup"
    author: "Mitchell Mebane"
  - subject: "Re: photo album ripper"
    date: 2007-06-10
    body: "Check out Box.net. It's free. Your friends upload pictures there and invite you to share or even download the pictures you want from the share folder. You do the same and everyone is happy. Hope this works until an album ripper comes along. GerryB"
    author: "GerryB"
  - subject: "KOffice 1.6.3"
    date: 2007-06-09
    body: "Found it in openSUSE backports.\n\nAm I the only one wondering why it didn't make it on to the frontpage of KDE?"
    author: "Gerry"
  - subject: "PicasaWeb exporter for Linux"
    date: 2007-07-18
    body: "Hi guys, \nThis is to let you know that Picasaweb exporter is now available for Digikam. I committed the same yesterday and its available in the branch version (i.e KDE 3).\n\nIncidently I am responsible for the complaints to flickr plugin :) I have recently applied a fix to make it work better than it did currently as far as token handling was concerned. \n\n"
    author: "Vardhman Jain"
---
Uploading pictures to Flickr via its Web-based interface can be a hassle, particularly if you have dozens of shots to upload. Linux users have a better choice, though, in the form of <a href="http://kflickr.sourceforge.net/wikka.php?wakka=Kflickr">Kflickr</a>, a simple application for uploading shots to Flickr that will have your <a href="http://www.linux.com/article.pl?sid=07/05/29/1556207">family photos online in no time</a>. 


<!--break-->
