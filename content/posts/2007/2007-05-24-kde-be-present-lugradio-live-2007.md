---
title: "KDE to be Present at LUGRadio Live 2007"
date:    2007-05-24
authors:
  - "blamb"
slug:    kde-be-present-lugradio-live-2007
comments:
  - subject: "Thank God for that"
    date: 2007-05-26
    body: "Thank God for some KDE exposure - just be sure and give the LugRadio guys a hard time, they're thru and thru Gnome! "
    author: "RussH"
  - subject: "Re: Thank God for that"
    date: 2007-05-26
    body: "The UK is an evil place."
    author: "reihal"
---
KDE will be exhibiting at <a href="http://www.lugradio.org/live/">LUGRadio Live 2007</a> which is back in Wolverhampton, England on July 7th and 8th. The event, now in its third year, is the largest gathering of Free Software projects in the UK.  LUGRadio Live overlaps with the end of <a href="http://akademy2007.kde.org">aKademy</a> in Glasgow so you may want to stop by on your way home. 








<!--break-->
<p>KDE will have a stall at the show on both days where visitors will be able to see demonstrations of the latest KDE applications, pick up some live CDs and buy t-shirts to support the project. We will also endeavour to answer any questions you have about KDE or Open Source on the desktop. <a href="http://www.lugradio.org/live/2007/schedule.html">Sunday opens</a> with Aaron Seigo talking about his work on KDE 4.  KOffice developer Inge Wallin will also be on hand to show off KOffice 2.</p>


<!--Ben Lamb will be giving a talk</a>, "Conquering Your Desktop with KDE 4", demonstrating the upcoming release of KDE 4.0 on Saturday and the -->




