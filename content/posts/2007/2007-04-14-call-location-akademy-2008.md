---
title: "Call for Location of aKademy 2008"
date:    2007-04-14
authors:
  - "jriddell"
slug:    call-location-akademy-2008
comments:
  - subject: "no capital :>"
    date: 2007-04-15
    body: "the smaller the town - more concentration on the *real* work will be :)\n\n"
    author: "funnyfanny"
  - subject: "Re: no capital :>"
    date: 2007-04-15
    body: "And here I though the \"real work\" was to get to know eachother on another level, and make developing open source more fun.\n\nThe only redeeming quality of a smaller host city is cheaper accommodation."
    author: "Carewolf"
  - subject: "Re: no capital :>"
    date: 2007-04-16
    body: "I see other benefits of small cities as well, and the only problem with them is that they are usually harder to reach. Nove Hrady was a small (really small) city and I think it was real fun to be there!"
    author: "Andras Mantia"
---
While you are booking your travel for <a href="http://akademy2007.kde.org/">aKademy 2007</a>, have a thought about whether you could host aKademy 2008.  We are looking for a large institution to host our world summit sometime in the summer of 2008.  There needs to be a dedicated local team to organise an event like this who will work in partnership with KDE e.V.  The <a href="http://ev.kde.org/akademy/requirements.php">aKademy requirements</a> describe what is needed in greater detail.

<!--break-->
<p>aKademy is a week-long conference with over 200 people (and growing yearly) for KDE.  Past events have happened in universities and colleges.  Other locations are entirely possible but they must have facilities for talks, group sessions and hacking on KDE solidly for a week.  Plenty of bandwidth is of course essential, as is local connectivity with wifi and lots of ethernet ports.  </p>

<p>The location should be in a town with good transport links.  Past events have all been in Europe, applications from elsewhere are welcome but extra sponsorship to cover the increased travel costs would be needed.  </p>

<p>The local team will need to handle, together with KDE e.V. organisation details including sponsorship, call for papers, registration and the website.  Cheap local accommodation needs to be provided by the team in student accommodation or a youth hostel with hotels and other options available nearby.  Opportunities to see the local sights are often appreciated and evening entertainment on some days will make it a perfect conference.</p>

<p>Organising aKademy is hard work but rewarding and vital to KDE.  If you want to apply to be a host for aKademy 2008 please read <a href="http://ev.kde.org/akademy/requirements.php">the requirements</a> and submit a concrete proposal to kde-ev-board@kde.org. This proposal should include information about how the requirements for the location will be addressed, who will be the local organising team and a responsible person acting as contact to the KDE e.V. and head of the local organising team.</p>

<p>The location will be discussed at the KDE e.V. assembly at this year's aKademy, so the deadline for submitting an aKademy 2008 proposal is June 30 2007.</p>

<p>If there are any questions please feel free to send them to this year's team akademy-team<span>@</span>kde.org or the board of the KDE e.V. at kde-ev-board<span>@</span>kde.org.</p>








