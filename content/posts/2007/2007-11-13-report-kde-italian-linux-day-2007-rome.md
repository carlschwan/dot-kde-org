---
title: "Report: KDE at The Italian Linux Day 2007 in Rome"
date:    2007-11-13
authors:
  - "gventuri"
slug:    report-kde-italian-linux-day-2007-rome
---
On October 27th, <a href="http://www.kde-it.org/">KDE Italia</a> attended the <a href="http://www.linuxdayroma.org/">Italian Linux Day 2007</a> in Rome at the Tor Vergata University. Other cities had their Linux Day throughout Italy at the same time. For the people that have never attended to these kind of events, it is important to remember that the Italian Linux Day is a day dedicated to spreading Free and Open Source Software and specially the GNU/Linux Operating System and its software components such as KDE. The talks for the day were at different levels of difficulty and you could find widely accessible talks or talks for an expert audience with more technical and specific topics.


<!--break-->
<p>Giovanni Venturi of KDE Italia represented KDE with 2 different talks, one for potential new Linux users and another for C/C++ experts. The slides are freely downloadable from the <a href="http://www.kde-it.org/">KDE Italia website</a> (in the download section of course). The two talks were titled:</p>
<ol>
  <li>Alla scoperta di KDE 3.5 e KDE 4 (Discovering KDE 3.5 and KDE 4)</li>
  <li>Programmare Qt (Programming Qt)</li>
</ol>

<p>The first talk started with a very brief introduction to the basic elements of a computer, including the purpose of an Operating System with reference to Linus Torvalds, the Linux creator.  This was followed up by talking directly about KDE, briefly remcalling its story: when and why the project was born and the Trolltech role. After these first minutes, the talk showed some screenshots of KDE 3.5.8 and KDE 4.0 beta 4 pointing out many of the features of KDE and the difference between the 3.5 series and the 4.0 beta. </p>

<p>New Linux users (about 70 people) asked about how it is possible that KDE 4 was released also for Windows and Mac. [ed: This is due to Qt4's new GPL licences for these platforms, plus the ease of using CMake to compile for alternative platforms.] They liked the fact that with KMPlayer you can record streamings and that you can get the audio files from a CD Audio in very simple way with Konqueror, K3b and KAudioCreator.</p>

<p>The second talk was more technical: it was created for people who know C/C++ programming. In this speech an introduction to Qt programming with the Trolltech <a href="http://trolltech.com/products/qt/">Qt toolkit</a> was given, explaining why people should use this multiplatform framework for the Unixes, Mac and Microsoft Windows. Qt Designer and Qt Assistent was demonstrated during the talk complete with examples compiled in real time. About 50 people attended this talk. At the end of the talk people were invited to give their contribution to KDE through development or alternately helping KDE Italia, the KDE Italian translation team, documentation, the KDE promo team and more. This talk's slides are available as two files, the presentation in ODF and the Qt example ZIP file.</p>

<p>The event was carried out at the same time as the <strong>Game Developer Day</strong>, an initiative begin run for the first time. This other event presented a video game creation theme, explained through a series of presentations and useful examples. In another of the rooms was the install fest featuring real time installation of GNU/Linux (Kubuntu and Ubuntu) distributions on the users' laptop and desktop computers for those people that wanted to become part of the GNU/Linux world.</p>

<p>Meanwhile in Bergamo, thanks to <a
href="http://www.kde-it.org/news.php?extend.27.4">Flavio Castelli</a>, there
was another KDE 4 talk at the <a href="http://bglug.linux.it/">Bergamo Linux
Users Group</a> Linux Day.</p>

<p>We hope to help grow the KDE Italian community by the active involvement of new end users. For this event, we want to specifically thank <a href="http://www.torlug.org/">Tor Vergata Linux Users' Group</a> and <a href="http://www.ls-lug.org/">La Sapienza Linux Users' Group</a>, two groups with which we have been collaborating with over the last year. In fact, we also collaborated with these Linux Users' Groups last year during the Italian Linux Day 2006. We invite all people that want to join our community to contact us using the website, via email at wewantyou@kde-it<span>.o</span>rg or the <a href="https://mail.kde.org/mailman/listinfo/kde-italia">KDE Italia mailing list</a>.</p>

