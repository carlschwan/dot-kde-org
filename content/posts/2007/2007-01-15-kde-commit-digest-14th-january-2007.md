---
title: "KDE Commit-Digest for 14th January 2007"
date:    2007-01-15
authors:
  - "dallen"
slug:    kde-commit-digest-14th-january-2007
comments:
  - subject: "Nepomuk"
    date: 2007-01-14
    body: "What is Nepomuk? I mean, I read the associated website but I have actually no idea."
    author: "goofoo"
  - subject: "Re: Nepomuk"
    date: 2007-01-14
    body: "As I understand, Nepomuk is a standard for saving and sharing information on the desktop.\nOne of the things that the KDE implementation of kde is alreadi capable is to add tags and comment to files. So you can tag your photos whithout the help of digikam or kphotoalbum, and then you can find your photos whithin the file manager.\n\nBut later it must be possible to automaticaly add a comment to a file you download. for instance, download a new wallpaper on kde-look and it will automaticaly add http://www.kde-look.org/xyz. Know, if you had forgoten when this amazing piece of art came from, you can know it.\n\nOther example : you recieve an email from john, whith this wallpaper. If you search in the wallpaper, you will know that it was from john, and eventualy that john downloaded it from kde-look.\n\nBut in the long run, neopmuk whant to extend it to the internet. You may just ask \"search for related wallpaper\", and it will bring you not only kde-look, but any wallpaper taged whith the same tag as yours.\n\nOr you look at a website about dolphins, and you automaticaly have a list of dolphin related websites becauses people taged them as beeing so.\n\nHope that what I understood is correct and that helped you to understand."
    author: "kollum"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "Thanks kollum.  I finally get it. :)"
    author: "Ellis Whitehead"
  - subject: "RDF in NEPOMUK?"
    date: 2007-01-16
    body: "I'm REALLY glad to see NEPOMUK/Tenor(/Dashboard) materialising as a mainstream tool for KDE :D\n\nHaving recently found out just how powerful RDF is (I originally mistook it for RSS 0.5 or something -- big mistake!!)... will it be possible to add RDF feeds to NEPOMUK's ontology (preferably in an auto-discovery-like way, much like Konqueror lets you find and add feeds to akregator)?"
    author: "Lee"
  - subject: "Re: Nepomuk"
    date: 2007-01-14
    body: "you read the website and dont know what its about? :) perfect website !"
    author: "chri"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "I sounds like academic vapourware and its difficult to translate it into something pragmatic. Maybe that language is useful to get funds and keep your research option open."
    author: "fun"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "exactly. i'm pretty sure that's the case..."
    author: "superstoned"
  - subject: "Re: Nepomuk"
    date: 2007-01-14
    body: "Nepomuk (with strigi) is Tenor materialized."
    author: "Patcito"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "Speaking of Nepomuk, where does the name come from? It seems like Kum Open spelled backwards, which kind of makes sense, I suppose, given what it is trying to do..."
    author: "Anonymous"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "It's a dragon from the german novel \"Jim Knopf und Lukas der Lokomotivf\u00fchrer\"\n"
    author: "ac"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "Man i loved that book! Spanish translation though..."
    author: "Anonymous"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "Nepomuk is just a German name which is not common at all anymore. I wouldn't try to read anything into it, and it seems that even the k in Nepomuk is there by accident, as the KDE integration is just part of a greater EU-sponsored mother project :D"
    author: "Jakob Petsovits"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "It seems I hit submit a bit too late.  ;-)\n\n\nIs the KDE integration also sponsored by the mother project? \n\n"
    author: "cm"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "> Is the KDE integration also sponsored by the mother project?\n\nI believe so. In any case, Sebastian Tr\u00fcg knows for sure."
    author: "Jakob Petsovits"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "It's a first name. See http://de.wikipedia.org/wiki/Nepomuk_(Vorname) for a list of historical people who carry that name (Sorry, that page is in German).  \n\nI didn't notice that it's also the name of a dragon in the book mentioned above before I read that comment.  That's a fun fact but I'm not sure this is more than a coincidence since Nepomuk is not a KDE project (see http://nepomuk.semanticdesktop.org/).  KDE works with them a lot though, enough to be mentioned on that page.\n\n"
    author: "cm"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "It is an Austrian-Bohemian, to be precise a catholic Czech name, a low reputation name.\n\nnePomuk, see also Posemuckel."
    author: "Fredel"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "it's a town in Bohemia; see http://en.wikipedia.org/wiki/John_Nepomucene\nand, no, I have no idea why the software is so named"
    author: "Peter Robins"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "it actually stands for \n\nNetworked\nEnvironment for \nPersonal\nOntology-based \nManagement of\nUnified Knowledge ...\n\nsee here http://nepomuk.semanticdesktop.org/xwiki/bin/view/Main1/Project+Summary\n\n"
    author: "AJS"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "I was just going to post the same URL...  :-)\n\nCongrats -- finally someone who even went to the trouble of looking up the website itsel, and is not just talking and speculating out of their a***'s gaseous ejections when it comes to explaining the name....  :-)\n\nNot that this were an easy to grasp name either. They probably constructed it so to make up the funny-sounding rare name, but have a \"serious\" explanation for it as well (they receive European Union $&#8364;&#8364;&#8364; for part of their funding) ...."
    author: "Kurt Pfeifle"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "> Congrats -- finally someone who even went to the trouble of looking up the\n> website itsel, and is not just talking and speculating out of their a***'s\n> gaseous ejections when it comes to explaining the name.... :-)\n\nYo c'mon, I had looked it up before, and like all EU-related project acronyms its real name is completely irrelevant. Although I highly respect people who manage to come up with actually fitting names for those abbreviations, it's just, well, irrelevant. Just like GNOME doesn't need to be a networked object model environment to stand on its own, Nepomuk doesn't need to be a big chunk of ontology and knowledge.\n\nLet it be just that, plain Nepomuk."
    author: "Jakob Petsovits"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "> They probably constructed it so to make up the funny-sounding \n> rare name, but have a \"serious\" explanation for it as well\n\nOh, really?  Who would have thought?  ;-)\n\nI guessed that part was obvious enough that I explained only what the complete name meant (i.e. not \"kum open\", I think no KDE devs in their right minds would have agreed on that after Kant was renamed to Kate. If they had a say in the naming, that is, which the original poster seems to have assumed). \n\n"
    author: "cm"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "About that subject, I submitted a short new about few nepomuk presentation/workshop last week, I hope it'll get posted soon as the dates are for the the end of january and beginning of february."
    author: "Morreale Jean Roc"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "Did you have a look at http://nepomuk-kde.semanticdesktop.org. If so then I definetely have to update that page. ;)\n\nAs for Nepomuk - It aims to implement standards for the semantic desktop. In the long run this includes some very fancy features that I will not enumerate here (even I don't know all the details of all the working groups). As far as Integration into KDE goes desktop search and improved metadata is actually the first goal. Thus, you will be able to tag everthing on your desktop and reuse that information in every application. But it is not restricted to tags (tags are just the first step to show off something), we aim to provide a complete ontology (basicly a collection of classes and relations between them) for the desktop which allows to define more elaborate relations between for example a file and a contact in your addressbook or a page in a pdf file. Step two is reusing this information in an \"intelligent\" manner within the applications. Sure, this includes desktop search.\n\nI hope this will give you a better picture of our goals. Development is still in an early stage but I hope that the integration into KDE can start soon."
    author: "Sebastian Tr\u00fcg"
  - subject: "Re: Nepomuk"
    date: 2007-01-15
    body: "Nepomuk is realizing (i) APIs for managing metadata on the desktop, for sharing it across desktops in P2P over a trust network, and for enabling advanced shared workflows (ii) several implementations of these APIs: KDE implementation, Eclipse implementation and XUL implementation (iii) case studies putting the framework into practice in different contexts: bioscience researchers community context (by Cognium Systems http://www.cogniumsystems.com), SAP context (http://www.sap.com), TMI consulting network (http://www.theprcgroup.com/), Mandriva Club community context (http://club.mandriva.com).\n\nAs for the Nepomuk-KDE implementation, more can be read on where we are in the following report, issued 31.12.2006: http://nepomuk.semanticdesktop.org/xwiki/bin/view/Main1/D7-2"
    author: "arkub"
  - subject: "khtml form background ?"
    date: 2007-01-14
    body: "Support for styling the background of forms in KHTML.\nIs this http://bugs.kde.org/show_bug.cgi?id=95690 ?\nI sure hope it is.. "
    author: "viseztrance"
  - subject: "Re: khtml form background ?"
    date: 2007-01-15
    body: "yes, it's supposed to be about 95690.\nSee the attached image for a sample rendering..."
    author: "g."
  - subject: "Let's konqueror WIndows desktop"
    date: 2007-01-14
    body: "And then let's distroy it, for the well being of Gates, Bill Gates >:)"
    author: "zvonsully"
  - subject: "java svg vs. kaffeine svg"
    date: 2007-01-14
    body: "The two svg images for java and kaffeine are in a way very similar:\nhttp://websvn.kde.org/*checkout*/trunk/playground/artwork/Oxygen/theme/svg/apps/kaffeine.svg?rev=585693\nhttp://websvn.kde.org/*checkout*/trunk/playground/artwork/Oxygen/theme/svg/actions/java.svg?rev=621739\n\nSure, the kaffeine image has the black thing in the background, but the main part which is identified with the program (the coffee bean) is very similar. Or, the other way around, people might guess that kaffeine is the mixture of java and video stuff - what ever that would be ;)\n\nliquidat\n\nPS: Why can I only chose plain text in this field? Well, copy&paste the URLs yourself, it's not my fault ;)"
    author: "liquidat"
  - subject: "Re: java svg vs. kaffeine svg"
    date: 2007-01-15
    body: "OK i will make diferent coffe been for java."
    author: "pinheiro"
  - subject: "Re: java svg vs. kaffeine svg"
    date: 2007-01-15
    body: "Nino, what about a coffee mug.  That's kind of synonymous with Java, right?  It doesn't have to be a teacup style like the official logo; maybe a more traditional coffee mug?  Just a thought."
    author: "Louis"
  - subject: "Re: java svg vs. kaffeine svg"
    date: 2007-01-15
    body: "A coffee mug. What's what's popping up in my mind when I hear \"Java\". "
    author: "Lans"
  - subject: "Re: java svg vs. kaffeine svg"
    date: 2007-01-15
    body: "Keep in mind however that I'm not a graphics specialist at all, it just came to my mind when I saw the image. So I can also be totally wrong ;)"
    author: "liquidat"
  - subject: "KWin and Beryl"
    date: 2007-01-15
    body: "are KWin and Beryl supposed to work together? Or will the kwin devs have to  recode all the cool Beryl features into kwin?"
    author: "Patcito"
  - subject: "Re: KWin and Beryl"
    date: 2007-01-15
    body: "The latter one.\n\nCompiz and Beryl have all kinds of neat 3d effects and need to make a good window manager out of it. KWin is a great window manager, but needs to make all kinds of neat 3d effects.\n\nTwo different approaches to the same problem. I would highly appreciate if Lubos' bling and OpenGL acceleration improvements make it possible to keep KWin as window manager, because Beryl still has a long way to go if it wants to be anywhere near the much better usability that KWin currently offers."
    author: "Jakob Petsovits"
  - subject: "Re: KWin and Beryl"
    date: 2007-01-15
    body: "So it would be possible to use kwin on beryl in the future?"
    author: "Patcito"
  - subject: "Re: KWin and Beryl"
    date: 2007-01-15
    body: "It'll be kwin *or* beryl (or compiz or metacity or...) they're all window managers, kwin is kde's and so will be the default for kde4 (you would think) with some of the more useful compositing stuff thanks to lubos\n\nI wonder if it's intended for kwin to run on top of xgl, aiglx - if i understand correctly the current versions can run with nvidia's aiglx-like extensions in their new drivers, not sure about the xorg version of aiglx -> intel and (old) ati cards with free driver support"
    author: "Simon"
  - subject: "Re: KWin and Beryl"
    date: 2007-01-15
    body: "aside from that, beryl will most likely need some serious refactoring, and they can then probably build a decent theming mechanism (currently, according to Lubos, beryl just exposes it's whole self to plugins, allowing max flexibility, no backwards compatibility and no security)."
    author: "superstoned"
  - subject: "Thanks Danny!"
    date: 2007-01-15
    body: "You do a wonderful service :)"
    author: "Slubs"
  - subject: "Re: Thanks Danny!"
    date: 2007-01-15
    body: "I just wanted to second this! I waited the whole day for the new digest. Thank you very much for your hard work and bringing us a nice end of the weekend ;-)\n\nM\n\nPS: If anybody of the people making Danny's work possible by actually commiting stuff to SVN read this: Thank you, too! You're making such a great job :-)"
    author: "Marcel"
  - subject: "KDE on Windows"
    date: 2007-01-15
    body: "Thanks for the impressive update! As I have to use Windows in my office I am very happy to see the porting progress :-) \n\nBTW I am convinced that the KDE project will benefit strongly from the porting efforts: more users will get into contact with the beautiful KDE apps and furthermore, many Windows developers will fall in love with the free software world and the nicely designed Qt/KDE APIs.\n\nIn the long run even those who do not (have to) use Windows will benefit from the ports! Thanks guys! \n\nIn order to get more attention you might want to write a full Dot story and hope that it is linked by the usual suspects (Slashdot, c't, ...)"
    author: "MK"
  - subject: "Re: KDE on Windows"
    date: 2007-01-15
    body: "Hope it will work for Reactos too.\n\nhttp://www.reactos.org"
    author: "fun"
  - subject: "Re: KDE on Windows"
    date: 2007-01-15
    body: "I'm still trying to make the leap from windows programming to linux, but until now it is still a lot simpler to program under windows. Used to be different more than 10 years ago...\n\nI want to program using Qt. So under windows I just download some installer from the Qt website and run it. Afterwards there is an shortcut, which start a console and in it all the environment settings are done, I can just run qmake -project & qmake & make. And my program is finished. There is even a shortcut for the designer.\n\nUnder Linux, I found by using synaptic Qt-4.2, installed it. And had to use qmake (qt3 version) with -project, afterwards the qmake-qt4 create the makefiles, and just make to compile. Works fine, only got a little out of luck with the OpenGL Widget, and I'm still looking for the designer.\n\nWould be nice if there was a getting startet section on kde-developer website. Well actually there is, but I'm still missing these basic instructions. There is a section somewhere, which explains how to make an extra user for compiling kde4. But I somehow think that is still a little to advanced for me.\n\nAnd suggestions ?"
    author: "boemer"
  - subject: "Re: KDE on Windows"
    date: 2007-01-15
    body: "Well, the last time I developed with Qt, it was just as easy as you described it under Windows (if not even easier). The issues you mentioned are subject to two competing installations: 3.x and 4.2 (and I wonder what happens to Windows in such cases?). I suggest to uninstall the 3.x-devel packages and check the PATH variable in ~/.bashrc to direct it to your 4.2 directory. \nIf this advice won't help you, just enter the Trolltech Qt mailing lists. It is free and they reply within hours. "
    author: "sebastian"
  - subject: "Re: KDE on Windows"
    date: 2007-01-16
    body: "Ah Thank you for the mailinglist tip, didn't know it existed...\n\nI also didn't think about removing Qt-3.x developer packages. Got a little scared that it would leave KDE 3.5 disfunctional...."
    author: "boemer"
  - subject: "Demographics bug"
    date: 2007-01-15
    body: "Danny,\n  Thanks once again :-)\n\n  Just a small nitpick - I think the \"Commit Demographics\"  doesn't work correctly for the \"age\".  The colours and percentages in the key don't match the picture in the pie chart.\n\n"
    author: "John Tapsell"
  - subject: "toolbars"
    date: 2007-01-15
    body: "I read the kde-core-devel archive last night and stumbled over this mail:\n\nhttp://lists.kde.org/?l=kde-core-devel&m=116881196802354&w=2\n\nWho else thinks it is a *GREAT* idea to have toolbars like the macos startpanel? I like the dynamic resizing a lot better than always popping up clumsy tooltips. This way, we could have both a lot of icons AND a clear description for each one."
    author: "ben"
  - subject: "Mailody"
    date: 2007-01-15
    body: "What's the status of Mailody, exactly? Is it going to be the new mail-client in 4.0? If that is the case, the icons should REALLY be changed. Currently, it's just confusing: An envelope and a musical-note. It seems like an audio-app. Yes, the icon refers to the name of the app. But the problem is that such a wordplay only really works in some specific languages. It wouldn't work in Finnish for example.\n\nThe icons should be descriptive of the purpose of the app, instead of being a clever puns on the name of the app. It's like that old GNOME (IIRC) log-reader which had an icon of a log (as in wood). it told nothing of the function of the app, and every single non-english speaker was left utterly confused as to what the app/icon does."
    author: "Janne"
  - subject: "Re: Mailody"
    date: 2007-01-15
    body: "This has not been decided yet.   The Kdepimp developers generally hate the source code for kmail (it is a mess), but on the other hand Mailody is still an early project.   It isn't clear if Mailody can expand to do everything kmail does without making a mess as well, and kdepim needs some of those things.\n\n"
    author: "Hank Miller"
  - subject: "Re: Mailody"
    date: 2007-01-15
    body: "> The Kdepimp developers \n\nCute typo.  Pimp my MUA!"
    author: "cm"
  - subject: "Re: Mailody"
    date: 2007-01-15
    body: "and how does akonadi fits in the whole picture if mailody would replace kmail ?  despite all its weaknesses (like crashes, occasional mail loss, single-threadedness, poor html composing, ...) i still use kmail because it does some things very well (like filtering, and talking to cyrus imap servers - more than just checking mail like setting permissions on mail folders). And i definately need disconnected imap too.. I read earlier that mailody would not support that."
    author: "anon"
  - subject: "Re: Mailody"
    date: 2007-01-15
    body: "well, akonadi would take away a lot of work from the apps into the framework, making it easier to build a new email application (like mailody). thus it increases the chance mailody becomes the foundation of new kde email client.\n\nif it's framework is really that much better than kmail, the kmail dev's can jump aboard and port their code to mailody. would be a big thing, tough, to leave your work for someone else's, even if you port some of your code. the khtml dev's have big talks about this (use WebCore or not), even tough there's the point of a big corporation running WebCore - while Tom is a nice guy :D\n\nanyway, Tom is really a part of the KDE PIM family, so it's relatively easy to come to a solution there."
    author: "superstoned"
  - subject: "Re: Mailody"
    date: 2007-01-16
    body: ">>thus it increases the chance mailody becomes the foundation of new kde email client.\n\nWow, mailody started as a simple imap mailclient, and now weeks later it is candidate for replacing kmail?\n\nWOW :)\n"
    author: "otherAC"
  - subject: "find icon drops footprints"
    date: 2007-01-15
    body: "the find icon dropped the footprints:\nhttp://commit-digest.org/issues/2007-01-14/moreinfo/621405/#visual\nso it's now identical to the magnify icon:\nhttp://websvn.kde.org/*checkout*/trunk/playground/artwork/Oxygen/theme/svg/actions/viewmag.svg?rev=615389\nhope that's temporary. ;)"
    author: "logixoul"
  - subject: "Re: find icon drops footprints"
    date: 2007-01-15
    body: "I preferred good ol' Adobe's find icon being a telescope over all successors."
    author: "sebastian"
  - subject: "XP style?"
    date: 2007-01-15
    body: "The KDE on Windows (TM?) screenshots look promising. I wonder if it is possibe to use the Windows theme engines (amd even icon and color themes) to improve consistency."
    author: "sebastian"
  - subject: "Re: XP style?"
    date: 2007-01-16
    body: "It should be.  KDE4 (or rather, QT4) has a whole new graphics subsystem -- mainly for platform independence, but also for integration with native widgets, if I recall correctly."
    author: "Lee"
  - subject: "Is Plasma still alive?"
    date: 2007-01-15
    body: "I can't see any changes for long time on plasma.kde.org."
    author: "Franz-Rudolf Kuhnen"
  - subject: "Re: Is Plasma still alive?"
    date: 2007-01-15
    body: "Plasma is progressing slowly. But as it isn't time yet to begin coding, there is not much to do.\nThe design is nearly done, and then, core functionalities have to be finished before plasma start to be coded.\n\nBut parts of plasma already are there.\nLook at the svg support in KDE-games and ksysguard, this is a little bit of plasma !!\nThe port of the alt-F2 run dialog to a standalone app name kruner is an over little bit.\n\nWhen it is time, plasma will take focus. hopefully soon :)\n\nAt least it's what I understood."
    author: "kollum"
  - subject: "Re: Is Plasma still alive?"
    date: 2007-01-16
    body: "Watch kbfx. It's developer seems to be the person to watch for plasma like functionality.\n\nCheers\nBen"
    author: "Ben"
---
In <a href="http://commit-digest.org/issues/2007-01-14/">this week's KDE Commit-Digest</a>: <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a> integration, and a new "browser" interface added to <a href="http://pim.kde.org/akonadi/">Akonadi</a>. Refactoring work in <a href="http://kate-editor.org/">Kate</a> and <a href="http://pim.kde.org/components/kpilot.php">KPilot</a>. Experiments with a new Kate-alike session list in <a href="http://konsole.kde.org/">Konsole</a>. Expose-like window management effects in KWin. Support for styling the background of forms in <a href="http://www.khtml.info/">KHTML</a>. A <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>-based metadata indexer for KIO. Signature support in <a href="http://www.mailody.net/">Mailody</a>. Improved support for metadata internal storage and display, and a new <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a> shape for video in <a href="http://koffice.org/">KOffice</a>. Large code update in <a href="http://uml.sourceforge.net/">Umbrello</a>, part of the <a href="http://dot.kde.org/1165100724/">Student Mentoring program</a>. New tileset selector for kdegames, to be shared between KMahjongg and KShisen. Support for the <a href="http://en.wikipedia.org/wiki/FictionBook">FictionBook</a> format in <a href="http://okular.org/">okular</a>. Import of an initial version of the <a href="http://oxygen-icons.org/">Oxygen</a> sound theme for KDE 4. Import of user documentation for <a href="http://www.caffeinated.me.uk/kompare/">Kompare</a>. kaction-cleanup-branch merged back into the main kdelibs. Security fixes in <a href="http://kpdf.kde.org/">KPDF</a> and KSirc.

<!--break-->
