---
title: "Amarok 1.4.6 Released"
date:    2007-06-25
authors:
  - "lpintscher"
slug:    amarok-146-released
comments:
  - subject: "Let me be the first to say.."
    date: 2007-06-24
    body: "awwwwuuuuuuuuuuuuuuuuuuuooooooo!!!!\n"
    author: "Mark Kretschmann"
  - subject: "Yeah, but..."
    date: 2007-06-26
    body: "does anyone know exactly how is Amarok supporting rockbox iPods? I plugged mine in, but no songs showed in the panel."
    author: "Humberto Massa"
  - subject: "Hey! You got GNOME in my KDE"
    date: 2007-06-25
    body: "Reading through responses on the Amarok website regarding the newest release, apparently they've removed the \"hide menubars\" feature as some people were hitting it by accident and getting confused. So they removed the feature completely, rather than moving or hiding it. Which led to several people complaining (and rightly so!) An (apparent) dev commented in the thread stating that the feature was gone and not coming back because of \"support costs\".\n\nReally now, I thought this was the Desktop Environment where user != loser. It's disappointing to see developers take the \"that's too much feature for you to handle, user!\" stance, especially when it seems that feature is in use and appreciated. Once I get my Arch64 system back up and going (busted heatsink bracket), I might have to start maintaining a patched version of Amarok.\n\nThat kind of comment grates on me the same way that comments from the Mozilla \"community\" do. It puts up this imaginary barrier between the developers and the users. Support becomes a cost, suggestions become complaints, and users stop being potential resources (people who find bugs, who translate, who make suggestions, and help newcomers) and start turning into problems. In other words, it's the kind of comment I expect to see from a proprietary vendor turning down a customer."
    author: "Woogs"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-25
    body: "Nah, I can appreciate what it's like.  Even as an open source developer you have to consider the support cost of features.  For example in ksysguard I removed the feature that lets you have a different update speed for each sensor, but instead made it per page.  It let me simplify the UI, and simplify the code.\n\nIt's always hard to remove features - users fall in love with the strangest of quirks.  I know personally that I've exploited bugs in programs because it makes a task faster, then been upset when they fixed the bug :-)"
    author: "John Tapsell"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-25
    body: "I really do have to agree. When I hear about Amarok lately, it sounds to me increasingly like something I'd expect from GNOME's \"we're going to give you zen, and you're /going/ to like it\" attitude.\n\nWhile I'm keeping an open mind about the direction they're taking with the playlist in version 2, but I don't see how completely removing the 'Hide Menubar' option for the 1.4 branch, and presumably the 2.0 branch as well, was the best choice. It's not like hiding the menubar is even an Amarok-specific feature; quite a number of other KDE apps have the capability, including Konqueror, Konsole and Kopete.\n\nIf it is causing a lot of users to have to ask for help then, yes, there's a problem. But I'd say it would be better fixed by having a disable-able popup which asks the user for confirmation when they click it (You have attempted to hide the menubar. Once it is gone, you can bring it back with the Ctrl+M shortcut. Do you want to continue? Yes/No). Failing that, I'm sure it could've been removed from the menu by default, with the option of putting it back given in Amarok's configure window, or failing that, at least tucked away in a config file somewhere. Personally, I won't really miss the Hide Menubar functionality. I do however hope that \"x feature has been removed because it's too confusing for new users\" doesn't become a recurring trend in KDE's changelogs.\n\nWith that said, congratulations to the Amarok devs on a great new release. The majority of the changes are looking very promising, and I can't wait to upgrade my favourite music player."
    author: "Aster"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-25
    body: "I agree, this is a sad day for amarok. I sincerely hope they rethink this."
    author: "redeeman"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-25
    body: "Why not have a DCOP command to disable (and enable) menubar in Amarok. Document this command on the Amarok handbook. This way, power users will be able to run a simple command to get the desired feature. Since there will be no UI button for disabling the menubar, a not-so-geek user will not be bothered by this feature."
    author: "anonymous"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-26
    body: "that sounds like that silly gnome config thingummy. I mean, it's better than no option at all, but it's still crazy."
    author: "Chani"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-25
    body: "Hi all,\n\nYes, I think if we follow that concept of \"some users get confused by this or by something\" then we end in a \"Gnome like\" DE...\nIf it was a completely useless feature..., but it is not. I use menu bar only once: when I am first configuring amarok. Then I don't need it, and for me every little inch of space is precious.\nI think maybe the menu bar could be disabled in the preferences dialog with a big waring for the new users."
    author: "Crist\u00f3v\u00e3o"
  - subject: "so ++... "
    date: 2007-06-25
    body: "yeah right screw the simplicity attitude. We WANT the power - we are in no position to demand it though. But we can always fork. However I do hope it will not be necessary to fork KDE into Knome and KDEPro... but this is not the first time a feature widely used gets removed because of developers' 'more-thorough understanding of usability ideals'... I've had the experience before with some guy called Aaron, see http://bugs.kde.org/show_bug.cgi?id=96570 .. seems I'll rather fix one-liners like that myself (after finishing my SoC project and learning some QT)... damn that issue ist still annoying me each and every day.. how pathetic *g"
    author: "eMPee584"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-25
    body: "I think maybe we should post ours thoughts here http://amarok.kde.org/forum/index.php/topic,14165.0.html"
    author: "Crist\u00f3v\u00e3o"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-25
    body: "I tend to agree.  I like that I can hide the menu bar in most apps with CTRL-M.  I do however realize that it is a very confusing feature for newbies.  I was confused by it at one point as well.  But removing the feature entirely is not the right answer.  Perhaps it should be made a global setting in KDE.  Allow hiding of menus.  Default it to off if you must.  \n\nIf you are arguing that it confuses users (which is true for some users), then we should also remove the fullscreen option from Konqueror and all other apps which support it, because it hides the window and menu bar and could be even more confusing (it's even easier to activate, just one key press (F11) will do it).  This however is not the right path to go down."
    author: "Leo S"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-26
    body: "Since I don't contribute to Amarok anymore I feel I can be blunt.\n\nYou don't know shit.\n\nDeveloping applications of this scale is an exercise in intense compromise. Handling email after email from upset users who have lost their menubars is painful. Understanding that thousands more have accidentally lost their menubar and don't know how to get it back is troubling.\n\nWhen you have 5 or 10 million active users you have to take actions that a minority, yourself included will not like. This is one of them. Deal with it man.\n\nFrankly if moving to Gnome would mean less of your shit, I wouldn't be surprised if Amarok switched. With the Qt4 cleanlooks theme, and Gnome integration, this is entirely possible, so I suggest you watch it.\n\nThanks,\n\nMax"
    author: "Max Howell"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-26
    body: "\"Suggest you watch it\"\n\nI don't even know how to respond to this. You're serious in that your hope amarok is developed such that:\n\n1) It has less features people want, and\n2) Has less users\n\nYou know, I've heard that Hurd is looking for a few good developers ...."
    author: "Woogs"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-28
    body: "For as good an application as Amarok is, even if this were to happen, KDE still has several other multimedia apps (Juk anyone?) that could be focused on to replace it.  I'd hate to see Amarok become another GNOME app that has the bare minimum of features and customisability that we KDE users all despise."
    author: "Matt"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-07-02
    body: "I'm missing constructive ideas in this thread. So here is one: Can KDE (as a whole) support another button in the window title bar which is dynamically shown once a menu is switched of? Pressing the button gives you back the menu and hides that button - consistently for all KDE applications.\nIf you think it worth it you may want to consider it as a feature for KDE 4."
    author: "Andreas"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-26
    body: "#$@#@!!!\nhow dare they remove my menu-hiding feature!? my little screen is small enough already without unnecessary menubars all over the place :P\n\nI expect my kde apps to act like kde apps, goshdarnit, and that means NOT removing standard kde features like that. if I wanted this kind of condescending attitude I'd be on gnome, wouldn't I?"
    author: "Chani"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-26
    body: "oh, and I should add that they already had a 'menu' button that showed up down below when the menu bar was hidden... so it's not like there was no graphical way to get it back. I wonder what'll happen to that menu button now - is it gone too? it had a nice convenient subset of the menu options..."
    author: "Chani"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-06-26
    body: "Well, in fact, I'm quite happy they removed this in the 1.4.6 version.\n\nThis way, with all these complaints, I just discovered this one feature, I mean, hide menubar, wich is still available in 1.4.5, so I can know use it.\n\nThe worst thing ?\nI didn't even tried the well know ( by myself ) ctrl+m  thing to hide this anoying menu by miself, had to read the changelog to figure it out :(\n\nWell, lesson learned, now I will try what works in konqueror in other apps\n( I had the same thing with fish in fact, I did know konqueror was able do do that, and for long, I used it to download files, open in kate, and then upload...  Then I just tested directly from Kate and man, it's good to work on something werever it is just giving the URL, no down/up load anylonguer )"
    author: "kollum"
  - subject: "Re: Hey! You got GNOME in my KDE"
    date: 2007-07-02
    body: "Control-M...  AH SO DESK\u00c1!  I think this Zen is WORKING!\n\nNow for the Good Medicine that reliably prevents the removal of things that some people want/need/gottahave, while keeping the rampant technogeekerie and special-feature candypops out of the gnashing gobs of them pore klueless nuubz who're still too \"Winduhfied\" in the wetware to go full-tilt cyber-satori and Just Do It.  (Like I was, only two short years ago.)\n\nCommunication.  Simple developer-to-first-timer communication.\n\nA first-time wizard, such as I think I remember Amarok already featuring, sure can appropriately provide such a first-clues-for-you feature to the benefit of all (and with no legitimate annoyance to any).  A well-placed hint therein re menus, taking charge of ones' own desktop space, and a reference to control-M  in passing (Click <CONTINUE> Whewn Done) is plenty 'nuff imho.  (Oh, and maybe a checkbox to \"never remind me again\" for later, if the Wizard needs running ever again.)  \n\nIMHO, anyone so blindly brain-beboggled/dead as to not see the Wizard'll prolly email the developers in a non-consolable emo-driven huff anyway, no matter what.  No law against running a computer while blind even with ones' eyes wide open, is there now?   (The po-o-o-r of spirit are always with us, ain't they just?)\n\nBut please, Amarok Development Team People, kindly restore that toggle?  Kindly I pray ye:  Do not cut off all our legs just because some folk are so short of gorm, pelf *and* stature at first touch of Sweet Tux's Holy Outreach unto All Humanity's Holy Desktops as to seemingly have no reason to deserve full-featured performance!  That is no way to \"equalize\" us - mighht as well blind and deafen us all, too, and then what oif your peretty, happy, good-sounding Amarok entirely?\n\nThis proscriptively-implemented move, I do believe, just might be a clear sign of apparent team stress even unto burnout on the rise.  But there is Good Medicine for this.\n\nFact: Failure to communicate key points of operating procedure and order up-front is the Number One enemy of genuine communication, kindly do believe me if ever any here indeed can.  IMHO by way of different-real-world example:  Had N'Orlunz Loozeeannah YouEssHay (now long since fallen under Hurricane Katrina's airy fist) ever been well-warned in advance that no real help was ever really on the way at all, for example, many lives could have just as readily have saved thhemselves.\n\nSo have the Wizard sound a note or two by way of attentiuon-getting at the Critical Point.  Flash the screen.  Do both at first.\n\nJust a retired Documentation Specialist's thoughts on the debate.  From where I sit, all human Amarok developers' regrettable agony over the consequences of the mishandling of this helpful menu-toggle feature in the face of human nature notwithstanding, the slapdown approach apparently adopted so recently as Expedient Remedy really sort of looks like avoidable burnout from here.\n\nBut turning to Procrustean cyber-methodology \u00e1 l\u00e1 Hegel's Infamous Dialectic just ain't right.  Going that way leads to the Sphere of Noble Lies, just where Fort Redmond's marketroid wonkies would just love to have us all dogged down and no way out.  I can't back that.\n\nI think I'll just keep my toggle-menu'd prior-release Amarok RPM a while longer than most - long as the dependencies don't drive me off, anyway, as time and other KDE developments struggle forward every day.  Now that I know of them, and how these two sets of menu-toggle controls (KB + mouse) work for sure) in KDE overall, I really LIKE them!\n\nPlease, let's just keep them inconspicuously available in the appropriate manner, shall we?  (Thanks ever so much unto Team KDE from all six or seven (depending) of the Fitchburg Linux Operators' Group, if from NO-ONE else,  if ever so!)"
    author: "Rev. C. Walking Turtle Mann (ULC) of the FLOG"
  - subject: "Kubuntu Version"
    date: 2007-06-25
    body: "Why is the Kubuntu version of 1.4.6 sooooo laggy?  It lags on *everything*, 2 to 3 seconds, without any scripts running.  Sometimes it'll even lag for 5-10 seconds.  New build for Kubuntu in the works?"
    author: "Mike Dean"
  - subject: "Re: Kubuntu Version"
    date: 2007-06-25
    body: "What I don't understand is why mp3 is not supported anymore. I configured it to work."
    author: "Andre"
  - subject: "Hide menubar..."
    date: 2007-06-26
    body: "So, a little comment from me about this topic: I know _no one_ who intentionally used this feature. Even in this thread I just saw two reasons for it:\n1.) it saves some space on your screen\n2.) \"removing it makes Amarok a GNOME app\"\n\nNumber one may be valid, number two ist just trolling. Guys, don't you see: nearly no one wants this feature!\n\nOk, removing a feature because some users get confused is wrong, you are right. But in that case there were not \"some\" users. Nearly _every day_ we had people with a hidden menu bar in the channel. Sometimes several times a day. And I'm not online 24/7 so see all support requests...\n\nNow think of the users that don't go to irc when they have problems...\n\nAnd now everybody repeats three times:\n\nKeeping a useless, confusing, fucked up feature is WRONG!\nKeeping a useless, confusing, fucked up feature is WRONG!\nKeeping a useless, confusing, fucked up feature is WRONG!\n\nSo it got removed. Because that's right.\n\nSo, if you have a good idea for a different implementation just go ahead to bugs.kde.org. But I hope now everybody understands why keeping it the way we had it was not an option."
    author: "Sven Krohlas"
  - subject: "Re: Hide menubar..."
    date: 2007-06-26
    body: "If someone wrote the patch, would you be willing to have the removal of the menu bar configurable, with it switched off by default via an \"invisible\" config option (i.e. one that does not appear in the UI, only in the config file) that only advanced users would know about it?\n\nThis way, all users are happy: someone will only be able to hide the menu bar if they have a lot of knowledge and know what they are doing; and power users get to keep a piece of functionality they like.\n\nWhaddya say? :)"
    author: "Anon"
  - subject: "Re: Hide menubar..."
    date: 2007-06-26
    body: "\"I know _no one_ who intentionally used this feature.\"\n\nNow you know at least two people that intentionally use this feature. And I suppose you know a lot of people with giant monitors, too, because the screen \"real estate\" is the main reason _I_ use this feature -- in Amarok, in Konqi, in Kopete...\n\n\"Keeping a useless, confusing, fucked up feature is WRONG!\"\n\nYes, but the feature we're talking about is neither useless, nor confusing (unless you use the wrong activation place -- the config dialog would be great, thank you) nor fucked up (specially if you enable the old \"menu\" button).\n"
    author: "Humberto Massa"
  - subject: "Re: Hide menubar..."
    date: 2007-06-28
    body: "I don't quite see why a feature is useless, if it has \"only one reason\". Many features out there have only a single purpose.\n\nI can see why a disappearing menu is confusing, if you've have not seen it in other apps. But I, for one, love and use this feature in Kopete, Konqueror, Konsole and Amarok. And I'm not alone, because majority of the comments in this Dot story are about removing this feature. Most of them against.\n\nBeing able to hide a menu saves screen estate. So why is that important? Because your 12\" ultraportable doesn't have a whole lot of it. Because your 20\" widescreen has *tons* of other programs open and you want to have space for them all.\n\nBy removing a menu you don't need you're removing an user interface element you don't use. That way you have more space for those elements you *do* use. That is, you're seeing more valuable information in the same space.\n\nAnd the menubar is still easily accessible. Just Ctrl+M away. Now, users that lose their menubars and seek for help don't know that. But I'd say instead of removing that feature you should find a nifty way of telling users what they did and how they can cancel that. Or, even better, how they can put it to good use.\n\nRemoving a feature that is \"useless, confusing and fucked up\" is right. But if the feature is only confusing, perhaps it would be worth it to explore possibilities to make it not confusing?\n\nI do agree that developers have to make decisions that all users will not like. For example I'm eagerly waiting for the new playlist stuff. But, there's a big difference between those two things.\n\n1) One of them is DISABLING you for doing something, giving no other way to achieve the same thing.\n2) One of them is CHANGING the way you'll do things. Some people will like the new way right away, some will grow into it and others will forever hate it and seek alternatives.\n\nCan you guess which is which? Do you know which you should aim to do?"
    author: "Kari Oikarinen"
  - subject: "Re: Hide menubar..."
    date: 2007-06-28
    body: "I used the feature. I use KDE for it's great customizability and overall feature integration. This move goes against the two reasons why I prefer KDE over the rest and this move is disappointing."
    author: "Glen Kirkup"
  - subject: "Re: Hide menubar..."
    date: 2007-06-29
    body: "Hi,\n\n   I wanted a menu item for hiding the statusbar in konqi, but was advised to write a simple line to the .rc config file. I think this is a valid way to deal with this."
    author: "miro"
  - subject: "Re: Hide menubar..."
    date: 2007-08-03
    body: "Hi\n\nI just happen to click this \"Hide menu bar\" option and I got stuck trying to figure out how I an get it back. I can believe some people use and need this feature, but I'm not supposed to know I have to press Ctrl+M to make reappear my menu bar. The point is, there is no clear and simple way to deactivate this option for someone who's not really used to KDE. The last time I had to delete my KDE conf files in my home directory... Maybe I'm a bit stupid and I would have found the solution if I had tried harder... Or maybe a good GUI should be easy to use, even for (slightly) stupid people..."
    author: "St\u00e9phane"
  - subject: "A lot of thanx"
    date: 2007-07-10
    body: "Thank you guys for the great software. It's IMO the best universal music player for Linux. You run it and that's it you don't need any external soft. May be you can find some specific software which does some specific things better then Amarok but who cares."
    author: "Grindz"
---
The Amarok team released version 1.4.6 of their player. The newest release includes a new icon set, faster SQLite and many bugfixes. Release notes can be found at on <a href="http://amarok.kde.org/en/node/234">the Amarok website</a> and <a href="http://amarok.kde.org/wiki/Download">packages are available for download</a> for Kubuntu, SUSE, Fedora, Gentoo and others.  Their website announces that <em>"next week the annual KDE conference, Akademy, in Glasgow is starting, keep an eye on the developer blogs to follow the happenings there. Thanks to your continued donations and support from KDE e.V, 7 Amarok developers will be present They are looking forward to a very productive week hacking on exciting new Amarok features."</em>






<!--break-->
