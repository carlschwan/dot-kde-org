---
title: "KDE at Guademy, Spain and FOSS MEET, India"
date:    2007-02-21
authors:
  - "jriddell"
slug:    kde-guademy-spain-and-foss-meet-india
---
A joint KDE and Gnome meeting is taking place in Spain next month called <a href="http://www.guademy.org/index.php?lang=en">Guademy</a>. The objectives are to create new projects and initiatives of collaboration between both Desktops and allow new developers to get started. <a href="http://aseigo.blogspot.com/2007/02/d-bus-tutorials-guademy.html">Aaron Seigo</a> will give an update on KDE 4 and <a href="http://tsdgeos.blogspot.com/2007/01/guademy-2007.html">Albert Astals Cid</a> will talk about Okular.  Meanwhile in India Pradeepto Bhattacharya of <a href="http://kde.in/">KDE India</a> will be talking at <a href="http://www.foss.nitc.ac.in/">FOSS MEET</a> in NIT Calicut about KDE 4 and why you should develop with Qt.
