---
title: "Linux.com: K3b Enters New Era With Approaching 1.0 Release"
date:    2007-03-12
authors:
  - "numanee"
slug:    linuxcom-k3b-enters-new-era-approaching-10-release
comments:
  - subject: "Videos"
    date: 2007-03-11
    body: "It is a pity that we don't have a mature video editor, then I could take my movies from the camera via firewire and edit them. Yet, that's the reason why I still run dual-boot."
    author: "Fanta Ma"
  - subject: "Re: Videos"
    date: 2007-03-11
    body: "Kdenlive looks quite good."
    author: "Evil Eddie"
  - subject: "Re: Videos"
    date: 2007-03-11
    body: "When I first started using Linux I took out a subscription to Linux Journal. That was in 1994 or 1995. In the very first issue a letter was published that said \"It's a pity that X doesn't exist. It's the reason I still dual-boot\". At that time it was the possibility to run SCO binaries on linux. \n\nThe complaint remains perennially the same old thing, \"if only someone _else_ would provide X, _I_ would use their stuff.\" And over the years, time and again, X would be provided by people a little more pro-active, a little more adventurous. \n\nSCO binaries were already supported in 1995, and in 2007 there are video editors that people actually use for their video editing jobs. So... Try jahshaka, cinelerra, kdenlive or Vivia. And if they aren't good enough, try helping to improve them."
    author: "Boudewijn Rempt"
  - subject: "Re: Videos"
    date: 2007-03-11
    body: "I have to agree with the other guy. The last time I looked into linux video editors (about a year ago) I tried the following:\n\njahshaka: If you can actually manage to install this you will find it has about the worst UI ever designed.\n\ncinelerra: Again, if you can install this, you will find that it actually has *The Worst UI Ever (tm)*. And it crashes a lot.\n\nkdenlive wasn't very complete then (or now) but it looks the most promising.\n\nBlenders video editing isn't too bad (well compared to the alternatives anyway!) I couldn't get it to load any sound at all though.\n\nLinux does still lack a basic video editor that can open a decent variety of formats (i.e. more than just DV). Prove me wrong."
    author: "Tim"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "You missed a very crucial bit of Boudewijn's post:\n\n\"And if they aren't good enough, try helping to improve them.\""
    author: "Leo S"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "I think kdenlive is the most promising, but don't forget LiVES (http://lives.sourceforge.net), PiTiVi (http://www.pitivi.org) or Avidemux (http://fixounet.free.fr/avidemux/). Avidemux is pretty simplistic but if you're looking for something similar VirtualDub then that's the one to go for.\n"
    author: "Paul Eggleton"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "Kino does look promissing as well, and I'm sure you don't mind that it is gtk based."
    author: "Philipp"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "Avidemux + Mandvd"
    author: "Lory"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "Not exactly in the sapme cathegory, ManDVD is quite good software. The GUI design is realy cool except the menu editor which is not very practical.\n"
    author: "olahaye74"
  - subject: "Re: Videos"
    date: 2007-03-13
    body: "Unfortunately, I have to agree : one of my work is related to animation short movies and I tried almost all Video Editor on Linux to cut short films, but none are actually anywhere near Premiere, Vegas, Final Cut or Avid Xpress. The closest is not open source, it's MainActor.\nConsidering that impressive bits of software engineering like Blender, KDE, Gimp for instance have already been done, I find this weird and too bad.\nI ended cutting with series of PNG images and rendering them in the end in video using transcode, but that's not something you can put in production as a day to day way of working.\nAnd no, not everybody can help : I don't know how to code in C or C++, nor Java either, just a bit of Python. I don't blame anyone for not engaging in such a task, I would have just liked to have an open-source, stable video editor providing at least video editing feature (resolution-free tracks, basic compositing, the most simple transitions (no need for fancy rubbish), ability to import/export in various format, etc). I can provide hints of what's needed, test (that's usually what I do), but little more.\nI've not tried kdenlive yet, will give it a try but the UI seemed a bit too simple for me."
    author: "Richard Van Den Boom"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "I don't see how that could be given that X existed *way* before Linux, let alone before Linux Journal did.  Remember we are now at X11... 11 literally stands for version 11, X existed in some form or other way before that.\n\nOkay, this is a little bit of exaggeration given that one of the original X founders (Jim Gettys) is still around and even works with the GNOME project... so X can't be that old, but the point remains.\n\nGranted, I am not sure when X was first ported to Linux itself so this may be why you were initially trying to run the SCO binaries but I'm pretty sure it must have been ported by the time Linux Journal was around."
    author: "X11 User"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "dude he's not talking about X windows anywhere. Read his post again. He has just used the letter X as placeholder for \"something user Y wants to have on his system\". cheers"
    author: "X"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "Dude please, read this http://en.wikipedia.org/wiki/Humour"
    author: "LiFo2"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "Why not stick to standards? X and Y are for mathematics. Computer-related examples are supposed to use Foo or Bar (I forget the RFC's number)."
    author: " "
  - subject: "Re: Videos"
    date: 2007-03-13
    body: "ahh, standards are great. so many to choose from! ;)"
    author: "Chani"
  - subject: "Re: Videos"
    date: 2007-03-11
    body: "What about MainActor? \nhttp://www.mainconcept.com/site/index.php?id=395\n\nI'm sure you pay for software on your dual-boot."
    author: "nobody"
  - subject: "Re: Videos"
    date: 2007-03-11
    body: "The last MainActor version I tested was very buggy. That was some time ago, is MainActor still so much crashing?"
    author: "MaX"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "never had any crashes with mainactor.\nDid you contact MainConcept about this?"
    author: "otherAC"
  - subject: "Re: Videos"
    date: 2007-03-11
    body: "Blender might also be worth looking into for a video editor.  Last time I tried it it was buggy, but all of the infrastructure is already in Blender, the bugs seemed quite superficial, and there has been a focus on video editing since.  Of course, usability is an issue :/"
    author: "Lee"
  - subject: "Re: Videos"
    date: 2007-03-12
    body: "Try Vivia."
    author: "logixoul"
  - subject: "Re: Videos"
    date: 2007-03-13
    body: "Instead of dual-booting, did you try running your favorite windoze video editor under Wine?"
    author: "zonk"
  - subject: "a great work from a single man !!"
    date: 2007-03-11
    body: "\nIt is happy news for all of us to see K3b reached this milestone, and furthermore it is mainly the work of a single man.\n\nSo great work and good luck for the next K4b ;)\n\nAn Algerian kde fan. \n"
    author: "djouallah mimoune"
  - subject: "Re: a great work from a single man !!"
    date: 2007-03-11
    body: "Yes, great work. \n\nLuckily this man is standing on the shoulders of giants as they say. Those would be the makers of kdelibs, cdrecord (and that equiv thing for dvd), Linux and last, but not least, GNU.\n\nThanks to k3b, I have for years now, found it much easier to burn under Linux than anywhere else I saw.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: a great work from a single man !!"
    date: 2007-03-12
    body: "That equivalent thing for dvd is called growisofs."
    author: "Josh"
  - subject: "Why K3B is great"
    date: 2007-03-11
    body: "- it does what you want and works\n- you don't get error messages you don't understand\n- you won't find a bug within 5 minutes of use\n- it looks nice.\n\nNough said. Most linux non-commandline software does not meet these expectations."
    author: "Andy"
  - subject: "Re: Why K3B is great"
    date: 2007-03-11
    body: "unfortunately you are right;("
    author: "djouallah mimoune"
  - subject: "One thing"
    date: 2007-03-12
    body: "I would love to see support for OGM (including advanced features, like chapters and named audio streams)."
    author: "Michael"
  - subject: "Thank you Mister Trueg !"
    date: 2007-03-12
    body: "and congratulations for the 1.0 release :)"
    author: "shamaz"
  - subject: "Excellent package"
    date: 2007-03-12
    body: "Well done to the author(s). This is a supreme bit of software and reliably does the job it claims to do. It's another of KDEs jewels."
    author: "DSC"
  - subject: "Looking forward to 1.0 and beyond"
    date: 2007-03-12
    body: "K3B has been a great program for years now, and it only keeps getting better.\n\nK3B, KPlayer and Konqueror are the three programs that make me love KDE!"
    author: "I love KDE"
  - subject: "Re: Looking forward to 1.0 and beyond"
    date: 2007-03-12
    body: "Software that makes me addict to KDE:\n\n- digikam\n- konqueror\n- k3b\n- kaffeine\n- amarok\n\nLooking forward for better import filters in koffice (would remove the openoffice requirement (heavy))\n\nAlso looking forward to kdenlive.\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: Looking forward to 1.0 and beyond"
    date: 2007-03-12
    body: "\nForgot 2 great peice of software as well (I'm not a developper :-( ):\n\n- kdevelop\n- valgrind and its plugins (kcachegrind)."
    author: "olahaye74"
  - subject: "Re: Looking forward to 1.0 and beyond"
    date: 2007-03-13
    body: "yep, i also think those are the 3 best programs, including amarok."
    author: "litb"
---
<i>"One of free software's premier applications, KDE's CD and DVD burning suite <a href="http://k3b.plainblack.com/">K3b</a>, is about to hit the big 1-0. This milestone touts rewritten DVD video ripping and a refocused interface design. The new release represents a level of feature-completeness and stability that surpasses all previous K3b releases and, perhaps, all free software competitors."</i> Read <a href="http://www.linux.com/article.pl?sid=07/03/05/181224">more at Linux.com</a>, including plans for KDE 4.


<!--break-->
