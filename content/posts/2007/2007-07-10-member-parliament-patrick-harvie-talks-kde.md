---
title: "Member of Parliament Patrick Harvie Talks to KDE"
date:    2007-07-10
authors:
  - "jriddell"
slug:    member-parliament-patrick-harvie-talks-kde
comments:
  - subject: "learning the right things"
    date: 2007-07-10
    body: ">Asked about use in education he said it was utterly wrong to teach children there is only one product in the market.\n\nI've said this before, but I'll keep repeating it in hopes that someone can forward this to the right ears:\n\nWhen I was a kid I wanted WordStar, because that is what everyone else had.  (I got stuck with Atariwriter which wasn't as good)   When I got into High school they were proud of their new computer lab where they would teach us Word Perfect (5.1) - THE SAME AS INDUSTRY WAS USING!!!  (yes they were shouting that).   In college Microsoft Word was the only word processor available, so that is what I used.   Then I got a real job, and Adobe Framemaker is what they had me use.   I've changed jobs several times, and each time the job came with a different word processor, and sometimes they upgraded while I was working there.  \n\nIn short, he is right, teaching any product wrong.   Schools need to teach how to learn to use whichever product you might encounter."
    author: "Henry Miller"
  - subject: "Re: learning the right things"
    date: 2007-07-10
    body: "They actually teach kids how to use computers in your country as a course? Isn't it enough to have a library with computers and an assistant to aid kids when they need some help? Kids are very fast learners... Do they get grades too?\nSomething I have been advocating in this country is to allow high-school students to sign up for a programming class (in here, they either choose a IT curriculum and have a bunch of them, or they have nothing).\n\nAnyway, I have known of people that teach on formation courses to adults. It seems attendees have a tough using the computer; a lot of times they write some usage \"recipe\" in the paper and then have a hard time reproducing it on a different version of the software, or simply if it is a bit customized. Something I have started wondering is if it would be better if they dedicated part of the course to teaching attendees how to design interfaces; making them seeing it from the perspective of the developer. As a bonus, I would expect it to result in more resilient users against crappy interfaces. Good idea?"
    author: "fast penguin"
  - subject: "Re: learning the right things"
    date: 2007-07-10
    body: "> They actually teach kids how to use computers in your \n> country as a course? \n\nYou're kidding, right?\n\nWhen my youngest daughter - who'll be 28 the coming September - was 16, they had to learn to push the right MS Office buttons at school. I've been at odds whith the Swedish school system ever since (or maybe long before that, but that's another story).\n\nMy oldest daughter, who's 31, actually had a better start with computers in school, since it was before Windows '95 - they had to learn something about computers and computing as such, and even learned some (very Basic) programming.\nThank gods there are people like Mr Harvie who at least try to keep things sane. I think someone should buy him dinner, or at least a beer :-)\n\n/goranj\n"
    author: "G\u00f6ran Jartin"
  - subject: "Re: learning the right things"
    date: 2007-07-11
    body: "I'm 29, from Sweden, and I had to learn DOS WordPerfect keyboard shortcuts in school when I was 16. We even had keyboards with F7 (Cancel) marked in red. \n\nWhat I _didn't_ have to learn was general concepts about files, folders, operating systems etc; I only had to learn program-specific crap for software that's not even around anymore. Total insanity.\n\nIt got better in the Gymnasieskola (17-19 years old), though.\n\n"
    author: "Apollo Creed"
  - subject: "Re: learning the right things"
    date: 2007-07-10
    body: "Speaking as someone who did GCSE ICT last year, the whole thing is a farse.\nCoursework has a large bearing on the final grade and the mark scheme is incremental and awful. If you miss out one of the first few marks on the scheme, you can't get any mark past that one. You could be an expert and still fail if you miss one of their ridiculously specific points near the beginning.\nYou are essentially told what to do for most of it, using only Microsoft software, and if you try to some up with your own method (as I did) even if it is a better way and more suited, the teachers don't understand what you are trying to do (I wanted to use Ruby on rails instead of MS Access and forms) so you have to do it their way, or risk losing the marks where they can't follow it.\n\nThe computers at my school have OSS alternatives to the commercial software also present (Paintshop pro/Gimp, MS Office/OpenOffice) but they aren't taught or even drawn attention to, a few friends and I use them, but we use them anyway.\n\nI think it is a shame that nothing is done to dispel the myth that the is nothing but Microsoft (I know people who think that windows is part of the computer and is free, pah!). When asked to name 5 operating systems, the average member of the class would (after having the term operating system explained to them) probably say something along the lines of, windows Vista, windows XP, windows 2000, windows 98, windows 95.\nOf course my experience might be entirely school/exam board specific but it still saddens me.\n\nrant over"
    author: "Jack"
  - subject: "Re: learning the right things"
    date: 2007-07-10
    body: "We had computer classes in my time in high school (were taught first DOS, then came some Pascal programming), and I'm 24 now.\n\nThis was Hungary, by the way."
    author: "Henrik Pauli"
  - subject: "Re: learning the right things"
    date: 2007-07-11
    body: "\"They actually teach kids how to use computers in your country as a course?\"\n\nAt least from my experience here in the US they don't teach kids hot to USE computers, only how to type, and maybe 'do specific things' (i.e. you do this, this, this, then that to do blah).  At my high school there was an AP Computer Science class which was a LOT better (i.e. didn't act like the computer was a magic black box, then again pretty much everyone in that class was probably the people played Quake while \"typing\" ;).\n\nIt's a real shame that the school systems don't recognize how important computers have become to how the world operates and go into more detail with teaching about that (though maybe being required to learn how to sew is more important and useful in your daily lives, eh?).\n\n\"in here, they either choose a IT curriculum and have a bunch of them, or they have nothing\"\n\nAt least at my high school we didn't even get that much of an option, it was 'typing' and if you even knew about it, 1 AP CS course (which is probably not that common to even have available)."
    author: "Sutoka"
  - subject: "Re: learning the right things"
    date: 2007-07-12
    body: "I have also used many different word processors according to where I worked, but not everyone will experience this. If you are an average user and you don't change your work environment often, you won't come across too many different types of software. So, the mean denominator MS Office is going to be taught and implemented (market dominance from a \"seemingly\" single player). The logical turnaround comes when more and more open source programs infiltrate existing markets. In my view this is logically done through raising awareness and implimentation at the political and social level. In education maybe not necessarily teach specific programs, but just have opensource be part of the curriculum. Practically have a working Open source environment installed in the classroom for children to simply experiment with. Really just to open their mind to the alternatives. Perhaps there could be an opensource roadshow initiative where schools are visited and opensource is advocated and displayed in the classroom. Since opensource is still not commonground in schools, this could be a good way to start. Children are highly inquisitive, and if they see something that inspires them and like, they'll take it from there. If schools are openminded enough to see where their pupils are heading, they'll follow their lead."
    author: "Pieter Philipse"
  - subject: "Schools"
    date: 2007-07-11
    body: "[It's probably worth pointing out for those not familiar with the UK political system that the Green Party is not a major political player.  However, the government is doing various studies on OSS, and there doesn't appear to be any real resistance from any political parties to the idea of OSS.  Neither is there any great push from any of the parties.]\n\nWhen I went to school (which wasn't all that long ago), IT lessons were spent learning to type quickly (Mavis Beacon, anyone?), and some basic word processing.  We had Mac LCIIIs, with ClarisWorks, but our IT teacher was good at teaching us how to do word processing in general (things like how to lay out a letter etc.), rather than the specifics of ClarisWorks.  I think it helped that the IT teachers had business degrees rather than IT-related degrees.  Learning to touchtype was one of the most useful things I've done.\n\nAbout halfway through high school (year 9), they decided to rip out the Macs and install a windows network.  Before that, only the geekiest of students messed around with the computers (those of you who used Macs before the days of OSX will know why :-P).  Afterwards, every Tom, Dick and Harry was downloading silly screensavers, trying to hack other people's accounts and playing stupid online games - all stuff that you couldn't do on the LCIIIs, because they simply weren't capable.  Quite frankly, I'm not sure what the school gained in the network upgrade, apart from larger bills, a higher-maintenance network and students wasting more time.\n\nEh, I sound like an old man reminiscing about the good old days..."
    author: "Alex Merry"
  - subject: "Re: Schools"
    date: 2007-07-11
    body: "current seats in Scottish Parliament, (in braces before the last election in 2007)\n\nScottish Labour Party: 46 (50)\nScottish National Party: 47 (26)\nScottish Conservative and Unionist: 17 (17)\nScottish Liberal Democrats: 16 (17)\nScottish Green Party: 2 (7)\n\nThe Greens went down from 7 to 2 seats. So Patrick Harvie is one of the two delegates of the Green party?\n"
    author: "Thomas"
  - subject: "Re: Schools"
    date: 2007-07-11
    body: "They are a major player in so far as the government depends on them for support of their First Minister.  In a proportional representation system with minority government small parties tend to have more influence than might be expected.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Schools"
    date: 2007-07-11
    body: "Patrick also convenes the Parliament's committee on Transport, Infrastructure and Climate Change [1] so he has a bit more influence than your average minor party backbencher.\n\n[1] http://www.scottish.parliament.uk/s3/committees/ticc/index.htm"
    author: "Ian Ruffell"
  - subject: "Scottish Paraliment make-up"
    date: 2007-07-26
    body: "The Greens do have two MSPs (Members of the Scottish Paraliment).\n\nHowever, because the SNP had the largest number of MSPs,\nthey had first chance to form a paraliment.\n\nThe Labour and LibDems were in coalition in the last Paraliment.\nLabour, SNP and Conservatives are opposed to each other; they wouldn't do a\ndeal.\n\nThe LibDems are opposed to Scottish Independence; they said before the election that they wouldn't do a deal with the SNP if the SNP went ahead with referendum on Scottish Indpendance; this is a manifesto commitment of the SNP. Apart from this the SNP and LibDems agree enough for a coalition.\n\nThe Scottish Socialist Party (6 MSPs last time) are pro-independence, as as are the Greens.\n\nThe Greens and SNP are in coalition, but since they don't have a majority of MSPs the executive is taking issues one at time, mainly the uncontentious ones, gathering enough support from the other parties.\n\nBTW,\nThe Greens are also opposed to software patents."
    author: "CSMiller"
  - subject: "Broken video"
    date: 2007-07-11
    body: "Is it just me or is the video broken?  mplayer complains about it and won't display it properly.\n\nWould an audio-only version be available somewhere at least?"
    author: "Francois"
  - subject: "Re: Broken video"
    date: 2007-07-11
    body: "The problem is that the videos are in Theora, which just plain never works. In some videos the picture freezes for tens of seconds while the audio keeps going, in others you get stutter for quite a long time (not a bug in the player but in the video stream, you can fast forward past the erroneous parts). Something as simple as displaying the length of the video is horribly broken. It always displays a total length of 59 hours 32 minutes and 16 seconds or something like that. This happens in MPlayer, Kaffeine, VLC, Xine, etc."
    author: "Anonymous"
  - subject: "Re: Broken video"
    date: 2007-07-12
    body: "Never works ey? then how come all the streams i've seen has always worked perfectly? oh in fact i even have lots of them saved to my disk, and big surprise: they STILL work? what a fantastic miracle for something that \"just plain never works\", i guess i must be special, seeing as though im able to compile a totally standard mplayer/ffmpeg/xine that is able to play them..\n\nhail me!!!!!\n\nor wait, perhaps im not special, and theora actually works, but you are doing something so weird i can not even think up what? just a few things to consider..."
    author: "redeeman"
  - subject: "Re: Broken video"
    date: 2007-07-11
    body: "The video seems to work perfectly fine for me the volume is quite low (in Kaffeine using the Xine engine)\n\nKaffeine version is 0.8.4 and Xine version 1.1.7 on Suse 10.2 (I think with xinelib from packman repository)."
    author: "Sutoka"
  - subject: "Re: Broken video"
    date: 2007-07-12
    body: "After watching from beginning to end I noticed that the picture does freeze every now and then for a few seconds at a time, though the audio never has any problems.  Maybe a buggy encoder?"
    author: "Sutoka"
  - subject: "Right wing ?!?"
    date: 2007-07-11
    body: "\"from the right wing Conservative party\"\nThe UK Conservative party is very much in the centre."
    author: "Tom Chiverton"
  - subject: "Re: Right wing ?!?"
    date: 2007-07-11
    body: "Um, that's a matter of quite some debate - even with Cameron's recent window-dressing.\n"
    author: "Ian Ruffell"
  - subject: "Re: Right wing ?!?"
    date: 2007-07-14
    body: "And posturing on \"piracy\" in front of various happy entertainment industry executives. If that were to end up as Tory policy, Cliff Richard's descendants (if he has any - I don't keep up with celebrity gossip) would still be getting royalties one thousand years from now."
    author: "The Badger"
  - subject: "Re: Right wing ?!?"
    date: 2007-07-12
    body: "These days UK parties transcend left and right politics, a party will be left wing on one issue and right on another. \n\nThe clearest example is the British National Party, that's highly left wing and socialist, except on immigration where it is extremely right wing. "
    author: "Ben"
  - subject: "BNP policies are far-right"
    date: 2007-07-12
    body: "I'm not sure which of the BNP's policies could be described as in any way left wing or socialist. They are far-right and fascist, plain and simple.\n\nThey hate non-whites, they hate gay people.\nEconomically, they hate trade unions.\nGenerally, they believe in a hierarchy and obedience to strong leaders.\n\nThis isn't the place to discuss politics, but what you said is just plain wrong.\n\nThanks for the excellent article. Now I feel even worse for not making it along to aKademy, despite my lack of coding knowledge :)"
    author: "CH"
  - subject: "Student/KDE user"
    date: 2007-07-15
    body: "Funny, in elementary school and mostly through middle, I was taught on Apple equipment. Then the other half of middle, high school, and college has me working on Microsoft. Either way, it's something proprietary. At home, for reports, for assignments, for gaming and such, I use FreeBSD with KDE, OpenOffice.org 2.0 and a slew of games. Something about school is that they teach you everything about how the computer works and how Microsoft Office can help you be productive, and how to create applications using VB and Visual C++. It's all about proprietary, \"this is what we're using, you should use it too\" apps. They don't teach students how to debug applications outside of \"put a print here and there and see where it crashes\"... students don't learn the first thing about backtraces, or stepping through code (a UNIX process)... even in the United States. I think if more people in government, society, etc. could be \"shown the light\" it might just change the way things are run."
    author: "Mike"
---
The final talk on Saturday at <a href="http://akademy2007.kde.org">aKademy 2007</a> was from <a href="http://www.scottish.parliament.uk/msp/membersPages/patrick_harvie/index.htm">Patrick Harvie</a>, a Member of the Scottish Parliament for the Green Party.  While not a technical wizard like most of the other talks of the day, Patrick was able to describe to us the attitudes to free software from the Government he is elected to keep an eye on, and how the work of KDE developers applies to more than just software.  Read on for a summary of his talk.








<!--break-->
<div style="float: right; width: 300px; margin: 1ex; padding: 1ex; border: thin solid grey">
<img src="http://static.kdenews.org/jr/patrick-harvie.png" width="300" height="220" alt="Patrick Harvie talking" />
Patrick Harvie talking at aKademy
</div>

<p>Like most people he used to think computers equals windows, since that is what you see every day when you switch your computer on.  In schools we are teaching our young people that this is what computers are.  He  gradually led through using free software until he realised he no longer needed Windows at all.  When a company tells you you're not allowed to share and cooperate, that's important to reject.  He now considers the operating system he uses at home to be better than the proprietary one he uses at work in parliament.  However it takes a lot of work to persuade people who are not technically minded that there might be something better, both functionally and politically.  Free software sits better with how the public service should be run for the common good.  As an aside he considers that creative commons is reinventing ideas that were fundamental to the Scottish Enlightenment.  </p>

<p>Last year he put in written questions to the government asked how much money they spend on Microsoft licences.  Before getting an answer from the executive he got an e-mail from Microsoft inviting him out to dinner.  He was briefly tempted by the dinner but decided to invite the representative to have coffee at his office instead, wondering why the man from Microsoft wanted to talk to him.  It became clear that they have a vast amount of money to persuade people like him that Microsoft is open and not so bad but they are, of course, a waste of money.</p>

<p>He went to see Eben Moglen (lawyer behind the GPL 3) talk last week in Edinburgh.  One other MSP, from the right wing Conservative party, went expecting to be get some nice drinks with some lawyers but from the talk even he started to realise the benefits of free software.  Eben made the case that applying that same open process to the legislative process is something governments can learn from.  Huge numbers of people are bored and cynical of politicians in jobs like his and feel that parties have it all stitched up and the public can not cooperate with the process.  The Scottish Parliament has tried to improve the process, for example the petitions committee lets anyone ask questions or propose ideas.  Other areas include press review where the current system is closed but an open system would let people review complaints made to the press.  He is convinced the work KDE is doing has more to say than just how we make software but also how we run society too.</p>

<p>In answering a question about how we can use the political process more he describes how Microsoft had a government conference in the Scottish Parliament on the day of Windows Vista's launch.  There Bill Gates could tell governments that Windows is the norm.  He tried to organise a counter conference on the same day but there was no room.  However such an event could be organised in parliament and he says MSPs and civil servants would turn up.</p>

<p>Asked about use in education he said it was utterly wrong to teach children there is only one product in the market.  However schools are very reluctant to even give use of a different browser.  In education he thinks school children should have at least one experience of collaborating on free software.  </p>

<p>If we can get people to think about what they can use free software for, and not just the cost, that will win them over.  Free (no cost) things are thought of as being low quality.  He agrees with the quote that proprietary software is as absurd as proprietary geometry.  </p>

<p>You can watch the full video of the talk at <a href="http://home.kde.org/~akademy07/videos/">the aKademy 2007 video page</a> talk number 1-11.</p>









