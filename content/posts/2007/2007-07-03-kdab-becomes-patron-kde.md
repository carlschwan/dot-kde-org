---
title: "KDAB Becomes Patron of KDE"
date:    2007-07-03
authors:
  - "sk\u00fcgler"
slug:    kdab-becomes-patron-kde
comments:
  - subject: "Thanks"
    date: 2007-07-04
    body: "Cookie goes for KDAB for the patronage :-)"
    author: "Albert Astals Cid"
  - subject: "Thanks"
    date: 2007-07-04
    body: "Thanks for the KOffice meeting and for everything else. You guys rock :)"
    author: "Sebastian Sauer"
  - subject: "Congrats!"
    date: 2007-07-04
    body: "This is really a great time to be involved in KDE! The project is on a roll these days. \n\nCongratulations to the KDE e.V. team and to KDAB!"
    author: "Askrates"
  - subject: "Other new patrons"
    date: 2007-07-04
    body: "Note also that Intel and Novell are listed on above page."
    author: "Anonymous"
  - subject: "Re: Other new patrons"
    date: 2007-07-04
    body: "I hadn't noticed those on the dot (if they were)\n\nAnyway, thanks to KDAB and all the other patrons and supporters"
    author: "Simon"
  - subject: "15 links !!"
    date: 2007-07-04
    body: "wow !\nthis article features 15 links to KDAB's website. on my monitor, this is an average of 1.25 links per line. i must say i'm impressed..."
    author: "brazzmonkey"
  - subject: "Re: 15 links !!"
    date: 2007-07-04
    body: "Yeah, Control+R totally rocks, and if you uncheck the \"confirm\" box, it's even consistent! \n\nChocolate, espresso and mojitos for the KDAB guys, btw!"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: 15 links !!"
    date: 2007-07-04
    body: "I particularly like the link embedded in 'KDABians' :-)"
    author: "Simon"
  - subject: "more KDAB anyone?"
    date: 2007-07-04
    body: "Can i have one more link to KDAB i forgot what the address was."
    author: "lol"
  - subject: "Re: more KDAB anyone?"
    date: 2007-07-04
    body: "http://www.kdab.com, but real men and women use http://www.klaralvdalens-datakonsult.se :)\n\n(Note that that's an automatic feature of the dot, not us being asses about having links to our website everywhere.)"
    author: "Till Adam"
  - subject: "Re: more KDAB anyone?"
    date: 2007-07-04
    body: "Actually, if you're referring to plain text like www.kdab.com (there we go again!) becoming a link, that's done semi-automatically by yours truly in the original document.\n\nThe fact that people start discussing the number of links in this document however, impresses me in a very interesting way."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: more KDAB anyone?"
    date: 2007-07-04
    body: "Just www.kdab.com, or http://www.kdab.com/ ?  So a www.kdab.com would work?\n\n(Three more links to http://www.kdab.com/!)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Thanks!"
    date: 2007-07-04
    body: "That's really cool!"
    author: "Joergen Ramskov"
  - subject: "Pronounciation?"
    date: 2007-07-04
    body: "Can someone spell \"Klar\u00e4lvdalens\" phonetically?"
    author: "AC"
  - subject: "Re: Pronounciation?"
    date: 2007-07-04
    body: "I think it's about time for Kalle to record a MP3 \"Hello, I am Kalle Dalheimer and I pronounce Klaralvdalens Datakonsult AB as Klaralvdalens Datakonsult AB\""
    author: "Frank Osterfeld"
  - subject: "Re: Pronounciation?"
    date: 2007-07-05
    body: "Though, amusingly, him not being Swedish, would it really be authoritative?  :-)"
    author: "Scott"
---
The KDE e.V. and <a href="http://www.kdab.com/">KDAB</a> are happy to announce continued
collaboration on the Free Desktop, with <a href="http://www.kdab.com/">KDAB</a> becoming the latest new <a href="http://ev.kde.org/supporting-members.php">Patron of KDE</a>. <a href="http://www.kdab.com/">KDAB</a> is known for its high-quality software services. Read on for more details.


<!--break-->
<p><a href="http://www.kdab.com/">KDAB</a> explains <em>"KDE and <a href="http://www.kdab.com/">KDAB</a> have a very strong relationship that dates back to the
beginning of the KDE project, with the <a href="http://www.kdab.com/">KDAB</a> CEO, Kalle Dalheimer, being one of the founders of the KDE project. Since then, <a href="http://www.kdab.com/">KDAB</a> has supported KDE consistently both with code and funding, especially for the past aKademies. We believe that a strong KDE community is good for all of us, and thus want to provide KDE e.V. with a stable and plannable stream of funding."</em></p>

<div style="border: 0px; float: right;">
    <img src="http://enterprise.kde.org/logos/kdab.png" />
</div>

</p>Sebastian K&uuml;gler of the KDE e.V. Board of Directors adds: <em>"<a
href="http://www.kdab.com/">KDAB</a> is one of the companies that understand their role in a Free Software community very well. <a href="http://www.kdab.com/">KDAB</a> is actively involved with making the KDE software the success it is and will be. Additionally, <a
href="http://www.kdab.com/">KDAB</a> takes its contribution to KDE very seriously. This is shown by their strong commitment to a stable, reliable and well-maintained codebase, and is stressed by
their involvement in KDE as an organisation. <a href="http://www.kdab.com/">KDAB</a> serves as an example for excellent collaboration with other parts of the community. We're thrilled that we're
able to manifest our collaboration also in this more formal way."</em></p>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left;">
    <img src="http://static.kdenews.org/jr/patron_small.png" width="91" height="24"/>
</div>

<p>Mirko Boehm, KDE core developer and head of <a href="http://www.kdab.com/">KDAB</a>'s Berlin office - and thus host of the recent KDE-PIM and KOffice meetings - explains <a
href="http://www.kdab.com/">KDAB</a>'s involvement in the KDE community as follows: <em>"<a
href="http://www.kdab.com/">KDAB</a> employs numerous KDE developers, who work directly or
indirectly on KDE in both their spare time and their work hours. In fact, we are a company mostly of
KDE developers. Some of the top ten committers to KDE are <a href="http://www.kdab.com/">KDAB</a>ians, according to <a href="http://www.ohloh.net">ohloh.net</a>, including David Faure, one of the major
contributors to KDE's core. We are actively trying to increase the visibility of KDE through our
customer contacts and look forward to promote the powerful KDE 4 platform in future projects."</em></p>

