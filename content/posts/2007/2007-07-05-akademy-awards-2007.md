---
title: "aKademy Awards 2007 "
date:    2007-07-05
authors:
  - "jpoortvliet"
slug:    akademy-awards-2007
comments:
  - subject: "Very cool"
    date: 2007-07-05
    body: "Sebastian Trueg definitely deserves as much accolades heaped on him as possible, (as well as donations) :P K3B is somthing I use almost every day. \ncannot wait to see what K3b will be like in KDE4!\n\n kudos to all the great people associated with the creation KDE. exciting times, \nwhy would I want to use any other OS or DE. "
    author: "madpuppy"
  - subject: "Re: Very cool"
    date: 2007-07-06
    body: "In case you didn't know, Sebastian is employed by Mandriva (so am I :>), we pay him to work on both k3b and Nepomuk. I'm sure contributions are still welcome though ;)\n-- \nAdam Williamson, Mandriva"
    author: "Adam Williamson"
  - subject: "Re: Very cool"
    date: 2007-07-06
    body: "hey, of course, I am a club member of Mandriva, but I still like to give to show my appreciation. especially for a program that beats hands down anything proprietary that I have used. "
    author: "madpuppy"
  - subject: "Re: Very cool"
    date: 2007-07-14
    body: "Wow, I didn't know that. Kudos to Mandriva for hiring such talented coders!"
    author: "Koshu"
  - subject: "Congrats..."
    date: 2007-07-05
    body: "And thanks to all the winners!"
    author: "Joergen Ramskov"
  - subject: "Weekly Commit Digest"
    date: 2007-07-05
    body: "Congratulations to Danny Allen and the Commit Digest series. I really agree with \"the most anticipated weekly writing\". Specially with the KDE4 progress, it really shows the amount of effort put in KDE and the liveness of the community.\n"
    author: "Cl\u00e1udio Gil"
  - subject: "Re: Weekly Commit Digest"
    date: 2007-07-05
    body: "Yeah, I have to agree to this. Communication is very important. What good is all the progress if noone finds out about it. Thanks Danny. I started reading the dot recently and I look forward to all articles."
    author: "Oscar"
  - subject: ":)"
    date: 2007-07-05
    body: "Congratulations for the winners as well as for the developers of KDE! Great job :)"
    author: "mikroos"
  - subject: "phonon is awesome"
    date: 2007-07-06
    body: "Phonon is shaping up to be a great API that does just loads of stuff. Kretz really deserves the credit for making it happen... I'm sad he couldn't make it. :("
    author: "Ian Monroe"
  - subject: "Re: phonon is awesome"
    date: 2007-07-07
    body: "yeah, of course more ppl = more fun, and Matthias is one of the many developers we missed..."
    author: "jospoortvliet"
---
At the second day of aKademy 2007, the contributors conference closed with the <a href="http://www.kde.org/history/akademyawards/">aKademy Awards</a> Ceremony. Two of last years winners, Boudewijn Rempt and Laurent Montel awarded no less than four awards to Sebastian Trueg, Mathias Kretz, Danny Allen and Kenny Duffus. Read on for more details.






<!--break-->
<p>
<b>This year's winners:</b>
<ul>
<li><b>2007 aKademy Award for Best Application</b><br /><a href="http://www.behindkde.org/people/trueg/">Sebastian Trueg</a> for what might be the application with the best reputation on linux: <a href="http://www.k3b.org">K3B</a>.</li>
<li><b>2007 aKademy Award for Best Non-Application</b><br /><a href="http://vir.homelinux.org/blog/">Matthias Kretz</a> for his great work on <a href="http://phonon.kde.org">Phonon</a>.</li>
<li><b>2007 aKademy Awards Jury's Award</b><br /><a href="http://www.kdedevelopers.org/blog/1209">Danny Allen</a> for making the most anticipated weekly writing in the KDE community: his <a href="http://commit-digest.org/">Weekly Commit Digest</a>.</li>
<li>Finally, we had an unofficial category, the <b>2007 aKademy Award for aKademy</b>, which went to <a href="http://www.duffus.org/">Kenny Duffus</a> and his team for the enormous amount of time, energy and work devoted to making the conference possible.</li>
</ul>
<a href="http://static.kdenews.org/dannya/akademy_award.jpg"><img src="http://static.kdenews.org/dannya/akademy_award_small.jpg" alt="award photo" width="640" height="480"></a>

<p>The awards ceremony is becoming a valuable tradition at aKademy. It is good to award those who have been an inspiration to the community. So this year, we went even further than last year, and had statuettes of cast-metal gears made. Unfortunately, it was not fully appreciated that 'real' things actually take time to be manufactured, and so were not presented at the ceremony. Two of the four winners were personally at aKademy, and thus they
received a picture of Konqi, signed by all KDE people at aKademy. And of course, the promise that the statuette would be sent to their home address. Furthermore, each winner received a "Mastering CMake" book, sponsored by <a href="http://www.kitware.com/">Kitware</a> and a one year subscription to <a href="http://www.linux-mag.com/">Linux Magazine</a>, sponsored of course by Linux Magazine. Kitware also sponsored a digital camera, which was given to Danny Allen.</p>

<p>The KDE project looks forward to recognising the work of contributors in the coming year, when this year's winners will decide the recipients of this most worthy award of the KDE community.</p>





