---
title: "KDE 4.0-alpha1 Released: \"Knut\""
date:    2007-05-11
authors:
  - "sk\u00fcgler"
slug:    kde-40-alpha1-released-knut
comments:
  - subject: "First Post!"
    date: 2007-05-11
    body: "First post! Everyone should post suggested KDE 4 alpha T-shirt designs here."
    author: "Adriaan de Groot"
  - subject: "Re: First Post!"
    date: 2007-05-11
    body: "A T-Shirt with Konqi on it, wearing a T-Shirt with the words \"I <heart> Knut\" in it :)"
    author: "Kevin Krammer"
  - subject: "Re: First Post!"
    date: 2007-05-11
    body: "How about a T-shirt of Konqui wearing a T-shirt of Konqui wearing a T-shirt of Konqui wearing... until you run out of resolution ;)"
    author: "Godji"
  - subject: "Re: First Post!"
    date: 2007-05-11
    body: "How about simply \"i <heart> Knut\" ? :P"
    author: "zonk"
  - subject: "Re: First Post!"
    date: 2007-05-11
    body: "No, no. See, now I have to hurt you. Like the attached T-shirt."
    author: "Adriaan de Groot"
  - subject: "Re: First Post!"
    date: 2007-05-11
    body: "I got an advanced preview of this visionary artwork and had a tough time keeping my mouth shut until Ade unveiled it.\n\nIt's like staring into the face of God.  If God were 8-bit anyway."
    author: "Wade Olson"
  - subject: "Re: First Post!"
    date: 2007-05-11
    body: "Yeah this Tshirt is so cool, and the text on it !!!!!!!!!!!!!!!!!"
    author: "lukilak@we"
  - subject: "What is that tool with the dog?"
    date: 2007-05-12
    body: "Sorry folks I don't agree with this picture.!!\nI think we must keep our image away of nasty sex behaviours.\nThat tool looks like a german sex toy by Beate Uhse."
    author: "Yuri Romanov"
  - subject: "Re: What is that tool with the dog?"
    date: 2007-05-12
    body: "The tool is looks like something generally used to scrub toilets. Maybe thats where your mind is? ;P\n\nThough some scrubbers used to clean dishes also look similar to that, but are generally much smaller than the toilet version and have more of the thingies (give me a break, I just woke up like 5 minutes ago!)"
    author: "Sutoka"
  - subject: "Re: First Post!"
    date: 2007-05-13
    body: "Cute but does it give the correct image?  It doesn't give credit to the professionalism or quality of the work done by the KDE team, it gives the opposite impression that KDE is immature. I cringe at things like \"firsty fawn\" etc at it sounds so childish - in-jokes always do when launched on the unsuspecting public. Unfortunately most of the people with buying power in the internatinal bank i work in dismiss products with labels like \"Fiesty Fawn\" etc as not serious and therefore don't deserve any attention.\nI may have missed some hounour in this thread about this shirt (i still haven't had my first tea of the day yet), apologies if I have.   \n\nBut thanks to all the KDE development team for the fantastic product, it is still the best out of all the desktop enviromonents. "
    author: "Ian"
  - subject: "Re: First Post!"
    date: 2007-05-14
    body: "Well, The Knut release IS immature, is it not? Furthermore, as such it is not meant for wider public (at least I thought there was some mention of Alpha there...)\n\nSumma summarum, I see no problem giving it an impression of being immature :)"
    author: "outolumo"
  - subject: "Re: First Post!"
    date: 2007-05-11
    body: "I Want a T-Shirt with Konqi eating a dolphin on a skewer."
    author: "G Child"
  - subject: "Re: First Post!"
    date: 2007-05-13
    body: "LOL! Nobody forces you to use Dolphin. Konqueror is still out there, and Krusader is going to start their KDE 4 port soon too.\n\nAnd you do know that that would be promoting illegal behavior (eating an animal of a protected species), right? ;-)"
    author: "Kevin Kofler"
  - subject: "Knut?"
    date: 2007-05-11
    body: "Damn, that's a typo just waiting to happen! :)"
    author: "Anon"
  - subject: "Re: Knut?"
    date: 2007-05-11
    body: "you mean knuth, like donald knuth?\n\nor, omfg, no you can't! you do mean Kant!? \n\n;)"
    author: "Gregor"
  - subject: "Knur? Knot?"
    date: 2007-05-11
    body: "Especially in polish language. The first two words that I thought about were:\n\"knur\" - meaning \"boar\", or (colloquially and somewhat offensive) \"swine\".\n\"knot\" - meaning \"something fscked up\" ;).\nNot too nice associations, I think :P."
    author: "Name"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-11
    body: "It's probably closest to \"knot\" then.\n\nKnut is an adorable little polarbear that made the news a couple of weeks ago, and since that bugger's got its own wikipedia page, we're tapping into his popularity:\n\nhttp://en.wikipedia.org/wiki/Knut_(polar_bear)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-11
    body: "Well, my bad. It's the first time I heard about that bear. But the article you pointed mentioned that the zoo has trademarked \"Knut\". Won't there be any problems for KDE stemming from this? ;)"
    author: "Name"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-11
    body: "I'm looking forward to reading articles like \"Zoo sues Free Software Project\" or \"Sue zoos ...\"\n\nIt must be Friday."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-11
    body: "\"Polarizing Knut: Illegal Use of Imagery Unbearable and Seals Fate\"\n\nTriple Pun FTW."
    author: "Wade Olson"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-11
    body: "best comment ever. thumbs up wade."
    author: "eol"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-11
    body: "How the heck can one trademark a name?  Knut was the name of a king of Denmark and England (anglicized as Canute).  It didn't appear with this bear.  To what does the trademark apply?  Pairings of the name with the image of a bear?  I suppose it's like restricting usages of the name McDonald that have to do with food.  It would suck to be a resterateur named McDonald or a computer software guy named Mike Rowe ;) (I know)"
    author: "MamiyaOtaru"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-12
    body: "As you mentioned McDonald's...\n\nhttp://www.papascott.de/archives/2007/04/21/knut-wochen/"
    author: "gizz"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-12
    body: "Excellent :)  And from following links: laffo @ knutwurst, knutella and b\u00e4rate uhse"
    author: "MamiyaOtaru"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-12
    body: "Knut is so cute ~ he will always be in my heart. I think it's great they have named this alpha after him... He is incredible cute - And the polarbear lives on the other side of the world than the penguin... But: Does this mean - that KDE is working completely into the other direction of the usual linux-world-philosophy? i am confused.\n\n@_@\n\nGreets from \"Knut\"-Germany,\nAlexa"
    author: "Alexa"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-14
    body: "Thank you! But why do you mention polarbears ?\n:-)"
    author: "Knut Mork"
  - subject: "Re: Knur? Knot?"
    date: 2007-05-14
    body: "Reminds me of my recents german week-end in Rheinland :\n\n1\u00b0) Knut everywhere, and clearly too much for me...\n2\u00b0) Sporgel everywhere, it's a pity there were so much of it & I ate so few, I'm a wreck !\n3\u00b0) Gr\u00f6nemeyers 12, just catch it & leave the country :)\n\nI think KDE will remain quite German-Styled in meeting locations as in cultural references and I can't see why it should not be good !!"
    author: "Jean-Alex Silvy"
  - subject: "Tribute to Trolltech-Knut?"
    date: 2007-05-12
    body: "Could it be a tribute to Trolltech's \"community manager\" Knut Yrvin? I mean, Trolltech produces Qt, which is essential to KDE ...\n\nHere's some short info about Knut:\n - http://www.linuxdagen.no/?p=8#yrvin\n - http://en.wikipedia.org/wiki/Knut_Yrvin"
    author: "Not Knut"
  - subject: "Re: Knut?"
    date: 2007-05-12
    body: "Knut is a nordic name, and my name =p\nSeeing Knut as the codename for a KDE alpha put a smile on my face on a rather miserable day =)"
    author: "Knut Ness"
  - subject: "Re: Knut?"
    date: 2007-05-13
    body: "Were they thinking \"Knut\", as the male name, or the more KDE-mandatory K-as-frontal-letter-ification of \"nut\", \"Knut\" (K-nut).\n\nThey're all (K)nuts!!"
    author: "Un-knut"
  - subject: "konqishirt"
    date: 2007-05-11
    body: "konqi t-shirt..  i'm happy to render out version of him wearing (or not wearing) anything, so just let me know!  I already have some quite strange poses of him in KDE4 fashion... but no place really to put them.\n\n.b"
    author: "basse"
  - subject: "Re: konqishirt"
    date: 2007-05-11
    body: "Can you send them to me? I'm always happy to collect artwork, and would be delighted to be able to nag you about new stuff. :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: konqishirt"
    date: 2007-05-14
    body: "How about a Konqi wearing an Oxygen mask, playing with a Dolphin..."
    author: "Andr\u00e9"
  - subject: "What a Geek!"
    date: 2007-05-11
    body: "The projected final release date, according to the above release schedule:\n\n\"2.4.12 October 23, 2007: Targeted Release Date\"\n\nAnd the chemist in me celebrates! <a href=\"http://en.wikipedia.org/wiki/Mole_day\">Yay for Mole Day!</a>\n\nI sure hope that they release at 6:02 in the morning! ;-)"
    author: "Kris Kerwin"
  - subject: "Re: What a Geek!"
    date: 2007-05-11
    body: "That's a T-shirt waiting to happen, too. KDE logo is a trademark of KDE e.V. and the Mole logo is a trademark of the FLOSSMole project (yay Megan!)."
    author: "Adriaan de Groot"
  - subject: "Kicker UI"
    date: 2007-05-11
    body: "First of all congratulations to all the devs! You all rock!\n\nI have to say I'm a little surprised to see in the screenshot the same old \"kicker\" UI. When KDE4 planning was starting it seemed like the whole desktop/taskbar/program launcher/systray area would undergo a revolution. What are the plans in this area? I read on the release schedule that Beta is less than 2 months away. Is the kicker UI as we know it going to stay in KDE4?\n\n(Please don't take this as flame bait, I just like to know where my beloved KDE is heading!)"
    author: "Flavio"
  - subject: "Re: Kicker UI"
    date: 2007-05-11
    body: "Development is being shifted to this kind of things now, it's unclear how it will look like, but it's likely to being changed shortly.\n\nWork on plasma has mainly happened under the hood until now, but the parts are shaping up nicely. There's kross which enables scripting interfaces, graphicsview extended with the necessary features for shiny graphics, libplasma, porting in superkaramba to kde4 / qgv and of course krunner as the most visible thing right now."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Kicker UI"
    date: 2007-05-11
    body: "the KDE4 panel and desktop will Plasma that users talked about it very much.please read the last few news about that.its development started a few weeks ago and will came into KDE in its time."
    author: "Emil Sedgh"
  - subject: "Thoughts on what to develop aganist"
    date: 2007-05-11
    body: "How stable is the API? Could a KDE4 app developer develop againist this alpha without having to worry about someone fixing their app to build againist a newer SVN version?\n\nBasically, should Amarok advise new developers to just get the alpha packages or should they still go ahead and build it."
    author: "Ian Monroe"
  - subject: "Re: Thoughts on what to develop aganist"
    date: 2007-05-11
    body: "kdelibs' API is supposed to be frozen."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Thoughts on what to develop aganist"
    date: 2007-05-11
    body: "I'm interested in developing some toy plasmoids: are the APIs for those also frozen? I tried looking around in Techbase, but couldn't find much info."
    author: "dario"
  - subject: "Re: Thoughts on what to develop aganist"
    date: 2007-05-11
    body: "The APIs pretty much don't exist, yet :)\n\nSee e.g.\n\nhttp://dot.kde.org/1178556406/1178570722/"
    author: "Anon"
  - subject: "Great Work!"
    date: 2007-05-11
    body: "Appreciate you KDE!\nCan't wait to try it!"
    author: "rockmen1"
  - subject: "Hardware-Acceleration"
    date: 2007-05-11
    body: "Why don't you use more hardware-acceleration? e.g. Marble, Krita, Kalzium"
    author: "EMP3ROR"
  - subject: "Re: Hardware-Acceleration"
    date: 2007-05-11
    body: "Krita and Kalzium actually do use OpenGL, and all the hardware acceleration that comes with it.\n\nMarble's objective is not to create an alternative for Google Earth, but to create a reusable geo-location widget for any application that might want to use it, minimize the disk space it needs, and make it as widely available as possible. Because of that, Marble uses a rendering algorithm that is totally different to \"real 3D\" like offered by OpenGL, so it can't take advantage of OpenGL by design. On the other hand, it doesn't have a pressing need for it either."
    author: "Jakob Petsovits"
  - subject: "Re: Hardware-Acceleration"
    date: 2007-05-11
    body: "ah, ok\nI asked because I read this (but it's not really up to date): http://cniehaus.livejournal.com/24404.html"
    author: "EMP3ROR"
  - subject: "Re: Hardware-Acceleration"
    date: 2007-05-12
    body: "I guess you mean this sentence?\n\n\"PS: The rendering was done without hardware acceleration but is very fast\"\n\n\nThat is simply because back then I had no 3D-card in my PC --> no hw-acceleration :) I simply wanted to say that it is really fast even without it!\n\nCurrently you get 30--50fps without 3d-acceleration and _much_ more with."
    author: "Carsten Niehaus"
  - subject: "Re: Hardware-Acceleration"
    date: 2007-05-12
    body: "Great! :)"
    author: "EMP3ROR"
  - subject: "Re: Hardware-Acceleration"
    date: 2007-05-12
    body: "> On the other hand, it doesn't have a pressing need for it either.\n\nRight. However I have a development path that also includes hardware acceleration in mind already. Due to more important Marble tasks I estimate that I won't start to work on this before III/2007.\n\n"
    author: "Torsten Rahn"
  - subject: "Re: Hardware-Acceleration"
    date: 2007-05-12
    body: "Hardware acceleration is great. But just remember that most of it comes at the expense of having to use a proprietary video driver."
    author: "Brandybuck"
  - subject: "Re: Hardware-Acceleration"
    date: 2007-05-15
    body: "Actually, no. The needs for applications like Krita or Kalzium, or for compositing window managers like Compiz/Beryl are modest enough that any lower-scale graphics accelerator will do perfectly fine as well, and still provide enormous speed improvements.\n\nI am writing this from my laptop with integrated Intel graphics, which does not need proprietary graphics drivers at all, and I can play all those Linux games with sufficient frame rates, my Beryl (when it's activated at all) feels smooth and handsome, and accelerated apps are snappy as one could imagine.\n\nIt's just a question of selecting and supporting the right hardware from the right vendor. I actually paid more for this configuration than I would have for an ATI or nVidia card, but it's well worth it."
    author: "Jakob Petsovits"
  - subject: "LiveCD"
    date: 2007-05-11
    body: "Thanks for providing a LiveCD, so everyone can easily help out finding bugs in the user interface (see pervious post on the dot). For the impatient, you can get it here:\n\nhttp://home.kde.org/~binner/kde-four-live/"
    author: "ac"
  - subject: "Re: LiveCD"
    date: 2007-05-11
    body: "Judging from the timestamps on the ISOs, they are not of the Alpha release, but of earlier versions.  I am sure that any time now the Alpha ISOs will be available though."
    author: "dario"
  - subject: "Re: LiveCD"
    date: 2007-05-12
    body: "And in the meantime the ISO for the Alpha version became indeed available!"
    author: "dario"
  - subject: "Re: LiveCD"
    date: 2007-05-12
    body: "To try the LiveCD with qemu, you'll need to instruct qemu to use a larger virtual memory (the default is 128, and the LiveCD complains of lacking of memory and reboots the virtual machine).\nSuch as:\n\nqemu -m 256 -cdrom KDE-Four-Live.i686-0.1.0.iso"
    author: "anonymous"
  - subject: "Re: LiveCD"
    date: 2007-05-12
    body: "Oh, and installing kqemu, and inserting the module to the kernel (as root or using sudo), will make the emulation faster:\nmodprobe kqemu"
    author: "anonymous"
  - subject: "Re: LiveCD"
    date: 2007-05-12
    body: "But it's bloody slow on my system.\nBut had a look at dolphin, and I like what I see, and have one remark:\n\n1-You can show size, type and date information bellow the icon files, but only one of these at a time. Shouldn't these options be checkboxes, to be able to select them at all? Or would that be overkill, and that's what the function of the side informational panel?\n\nGnome does this:\nYou can configure Nautilus to show what information below a icon file is shown in each zoom level. For example:\n\n100% - type\n150% - type size \n200% - type size date\n\nWhen I was using gnome, I didn't zoom in to see this details, mind you (the detailed information list is more handy for me)... But maybe it was just not well implemented. Perhaps the extra information could fit inside the file icon as it enlarges, but this would clash with text file previews. That information could be shadowed, or using another graphical gimmick, so to diferentiate itself from file previews (they should not look like they are part of the images, movies, pdfs, etc. files being previewd).\n\nSo far, the only time I use zoom on file views is to get a better look at the previews. Maybe there's a change to make it more usefull?\n\nP.S.- As I understand it, kde4 will have a \"low ground\" and a \"high ceiling\", i.e.,\n the initial default (although it will probably be changed by distros) will be oriented for people with just some computer knowledge, but through self-exploration will be able to get more proficient at using kde4, and to configure and extend kde4 to their personal tastes and habits (which in my opinion is what true usability should mean, not one size fits all).\n"
    author: "anonymous"
  - subject: "Re: LiveCD"
    date: 2007-05-12
    body: "sorry for the bad spelling. I wish you could change the dictionary of konqueror's spellchecker without having to reload the webpage."
    author: "anonymous"
  - subject: "Re: LiveCD"
    date: 2007-05-12
    body: "happyness and joy in KDE 4, then, as it'll even automatically detect the language and change the spellchecker :D\nBTW afaik you don't have to reload the page, just restart the spellcheck dialog..."
    author: "Jos Boortvliet"
  - subject: "Re: LiveCD"
    date: 2007-05-12
    body: "Hm, you sure? For example, right now kspellcheck is using PT dictionary, and I'm in the middle of writting a text. Now I will change it, with right-click-> spellcheck->dictionary . After I change, a message pops up saying (you'll have to reload the page for settings to be applied). But anyway, cool that that will happen in kde 4! Will be waiting for it.\n\n"
    author: "anonymous"
  - subject: "Re: LiveCD"
    date: 2007-05-12
    body: "This will be another great function! As far as I understand, the new spellcheck system will automatically detect the language you are writing, even if you change language in the same textbox/mail/document..right? If this is true, this is simply a killer feature for me, cause I usually write in four different languages almost every day..."
    author: "Vide"
  - subject: "Re: LiveCD"
    date: 2007-05-14
    body: "yes, it's supposed to detect languages in each paragraph independently..."
    author: "superstoned"
  - subject: "Re: LiveCD"
    date: 2007-05-15
    body: "how can I write it?? nero asks a overdisk writing method and says the ISO needs a 95min (841mb) disc????? is it that true?? is there any other way using k3b or some thing like??\n\nthankx and congrats for first KDE4 binaries!"
    author: "Saulo"
  - subject: "Re: LiveCD"
    date: 2007-05-15
    body: "Nero cant write it properly, but k3b does!!\n\nthanks!"
    author: "Saulo"
  - subject: "Oxygen style/window decorations..."
    date: 2007-05-11
    body: "When are we going to see the new style/window decoration? Plastik is great, but appears a bit old, mixed with all that amazing Oxygen icons...\n\nAnd congratulations for the release. ;)"
    author: "kde.fan.from.brasil"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-11
    body: "I don't know when it is getting merged in, but it looks and feels very mature to me.  You can see a clip of it here:\n\nhttp://pollycoke.wordpress.com/2007/05/11/kde4-istantanea-attuale-del-lavoro-sul-nuovo-stile-oxygen/\n\nIt's a huge memory hog, though - it adds at least 5MB to every app that uses it (and yes, I've accounted for shared memory) :("
    author: "Anon"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-11
    body: "Thank you, the style looks great.\n\n"
    author: "kde.fan.from.brasil"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-11
    body: "But it looks sweet :)"
    author: "shiny"
  - subject: "Oxygen style tab box"
    date: 2007-05-11
    body: "I think it would be better if the tab box didn't extend all the way to the right, but ended where the last (rightmost) tab ends."
    author: "AC"
  - subject: "Re: Oxygen style tab box"
    date: 2007-05-12
    body: "I think it'd be better if the style was a bit more polished. Right now, it still looks a bit too... \"big\". I really like the work done on the Domino KDE3 Style, and it would be great if oxygen could borrow more design tips from there :)"
    author: "lexxonnet"
  - subject: "Re: Oxygen style tab box"
    date: 2007-05-12
    body: "I agree, the Domino KDE3 Style looks great!"
    author: "AC"
  - subject: "Re: Oxygen style tab box"
    date: 2007-05-12
    body: "Polyester is also very nice."
    author: "anonymus"
  - subject: "Re: Oxygen style tab box"
    date: 2007-05-21
    body: "No.  Much respect to the author but any hint of Polyester should be kept out kde4 at all cost.  Ohh.. it dissapointed me that kubuntu picked it for feisty..."
    author: "Anon"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-12
    body: "with kde4 four live, it's possible to activate the oxygen style with \"kcmshell style\"... i just tried it out: One thing i didn't like is that everything looks a bit too bright.. for me it's hard to actually \"see\" the scrollbar, radiobuttons etc., but maybe that wasn't considered yet. Apart from that, it looks pretty good! Especially hovering over the menu bar is a lot of fun :) Thanks to all kde devs, great work!"
    author: "Christian"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-13
    body: "I think that the oxy-widget style is coming along nicely but there are a few issues with it: first of all, buttons and other \"active\" elements of the UI are not highlighted as they deserve (worse usability), secondly I think it needs a little touch of color (you know, the blue in the aqua style ;)"
    author: "Vide"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-13
    body: "Yes, more color in the Oxygen style would be nice.\n\nI also don't like the scrollbar with its up and down buttons both at the bottom and with no arrows on drawn on them. Plus, they don't look connected to the scrollbar in any way. The Domino scrollbars look pretty good, though:\n\nhttp://www.kde-look.org/content/show.php?content=42804"
    author: "Hans"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-13
    body: "mmmmh to be honest I don't like this Domino...it looks like an \"aqua meets keramik\" to me :P"
    author: "Davide Ferrari"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-13
    body: "I like Domino mostly because it has really nice smooth edges in _every_ single widget, and because it can be configured to look almost like anything. At the moment I am using a really simple look with it, much simpler than any of those screenshots in kde-look, and it looks easily more elegant than any style on KDE ever. Of course this is only my opinion, but actually I would like to see something like Domino as the default style of KDE4. Those videos etc. of Oxygen-style look quite scary imho, almost like the second Keramik: lots of animations, effects and bling bling, but style and usability are completely forgotten. It kind of looks like a tech demo."
    author: "Antti P\u00e4rn\u00e4nen"
  - subject: "Re: Nice !!"
    date: 2007-05-12
    body: "niiiice !!\n\ngood work !"
    author: "srettttt"
  - subject: "Hardware acceleration"
    date: 2007-05-13
    body: "Does oxygen use hardware acceleration?"
    author: "szookr"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-13
    body: "Looks like a mix of os x with windows vista window boarders. KDE 3.5 fresh and colorfull current theme seems nicer."
    author: "Petteri"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-13
    body: "Are you kidding?\nJust because it looks like a Mix, is it bad?\n\nThis look one hundrer times better than actual look.\n\nCongratulations Oxygen team!\n\n"
    author: "Luis"
  - subject: "Re: Oxygen style/window decorations..."
    date: 2007-05-15
    body: "It looks awesome!!! _AND_ highly professional even for graphic artists.\n\nGood job guys!\n\n"
    author: "fp26"
  - subject: "better qt integration !!!!!!!!!!!!!!!!!"
    date: 2007-05-11
    body: "ok it sounds odd, but please can you do  something to make pure qt application better integrated to kde, ie,( open file dialogue, print dialogue...)\nfor me perhaps the best thing in kde 3.90.1 is when you try to run plasma in terminal, you get a hehehe wonderful surprise."
    author: "djouallah mimoune"
  - subject: "Re: better qt integration !!!!!!!!!!!!!!!!!"
    date: 2007-05-12
    body: "No, that's just not possible. If a Qt app wants to use the KDE open file and print dialog, they have to use the KDE libs to do that, and they BECOME a KDE app. Though there is work going on to allow run-time detection of environment and using the appropriate dialogs, but you'd have to bug the Qt app dev's to use this infrastructure (when/if it's done), not the KDE dev's..."
    author: "Jos Boortvliet"
  - subject: "\"not possible\" isn't a wise thing to say"
    date: 2007-05-12
    body: "Regarding technology, \"not possible\" is not a wise thing to say.  When the first Pentium came out, everyone said that was the end of the x86 line, and we'd all have to move to PowerPC chips.\n\nI could be wrong, but it seems possible that LD_PRELOAD could be used to force Qt apps to use KDE settings, for one possible solution."
    author: "lee"
  - subject: "Re: better qt integration !!!!!!!!!!!!!!!!!"
    date: 2007-05-12
    body: "TrollTech could make it an option to use KDElibs on X11, same way they support Windows and Mac's dialogs. Why doesn't this happen? (Would KDElibs LGPL affect their QPL licensing or something?)"
    author: "blacksheep"
  - subject: "Re: better qt integration !!!!!!!!!!!!!!!!!"
    date: 2007-05-13
    body: "Ask them. The Top bar menu would be the next wish."
    author: "Benjamin"
  - subject: "Re: better qt integration !!!!!!!!!!!!!!!!!"
    date: 2007-05-13
    body: "Nooooooo\n\nAnything but the top bar, it sucks ass hard.\n\nIt's hell to deal with in anything larger than 800x600, and you'll absolutely love when you are using a trackpad and accidentally clicked while moving from the application to it's 1000px away menu.\n\nIt sucks."
    author: "Some guy"
  - subject: "Re: better qt integration !!!!!!!!!!!!!!!!!"
    date: 2007-05-14
    body: "Uhm... The keyword here would be \"support\" or even \"option\"... The thing is that right now it just doesn't work with pure Qt applications, while it /does/ work with KDE applications. Noone's forcing you to turn the option on ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: better qt integration !!!!!!!!!!!!!!!!!"
    date: 2007-06-10
    body: "I'm way late with this, but if anyone should happen to pass by, have a look at kgtk http://www.kde-apps.org/content/show.php?content=36077\n\nThis little gem lets one use KDE file dialogs with GTK and Qt apps.  Doesn't always work perfectly, but maybe with some help and integration into KDE it could improve."
    author: "MamiyaOtaru"
  - subject: "Knut?"
    date: 2007-05-11
    body: "Argh, ausgerechnet der bl\u00f6de Eisb\u00e4r :P"
    author: "Nobby"
  - subject: "Re: Knut?"
    date: 2007-05-12
    body: "i thought exactly the same thing...\nand the next release will be named \"bruno\"?\nBut maybe we can convince the zoo to sell the soft toy polar bear with a little \"K\" on it\nThat would be geek enough for me, then :-)"
    author: "gordin"
  - subject: "Re: Knut?"
    date: 2007-05-12
    body: "> and the next release will be named \"bruno\"?\n\nSo you want to declare the next release dead on arrival? \nSee http://www.online-grab.eu/ShowGrave.php?graveNo=MM-9 \n;-)\n"
    author: "cm"
  - subject: "KDE 4"
    date: 2007-05-11
    body: "How does one download and install the new alpha version of KDE 4 is not mentioned anywhere on the site.\nI have KDE 3.5.5 with my SuSE 10.2 and KDE 3.5.6 with Kubuntu 7.04.\nBeing a newbie I find it difficult to upgrade to KDE 4 in both the Distro's and would appreciate any assistance given in this regard.\nThanks,\n\nFarid Ansari\nKarachi, Pakistan."
    author: "farid"
  - subject: "Re: KDE 4"
    date: 2007-05-11
    body: "Click on the screenshot in the story above, and go to the \"Getting KDE 4.0 Alpha1\" section.\n\nThere are packages for Kubuntu, OpenSUSE, OSX, and Gentoo. Along with sources."
    author: "Soap"
  - subject: "Re: KDE 4"
    date: 2007-05-11
    body: "For Kubuntu 7.04, the details are here :\n\nhttp://kubuntu.org/announcements/kde4-alpha1.php\n\nRegards,\n\narnaud741\n"
    author: "arnaud741"
  - subject: "Re: KDE 4"
    date: 2007-05-11
    body: "i get an \"Segmentation Fault\" on these Packages on startkde. .("
    author: "srettttt"
  - subject: "Re: KDE 4"
    date: 2007-05-13
    body: "me too :(\n\nstartkde: Starting up...\nkdeinit4: preparing to launch /usr/lib/kde4/bin/klauncher\nkdeinit4: Launched KLauncher, pid = 7179 result = 0\nkdeinit4: Communication error with launcher. Exiting!\nWarning: connect() failed: : Connection refused\nSegmentation fault\nstartkde: Shutting down...\nWarning: connect() failed: : Connection refused\nError: Can't contact kdeinit4!\nstartkde: Running shutdown scripts...\nstartkde: Done.\n"
    author: "Fabian M."
  - subject: "Re: KDE 4"
    date: 2007-05-11
    body: "Even given the above comments, keep in mind that this KDE 4 alpha is a real alpha release. It doesn't bring most great new application features, it crashes, and you certainly don't want to switch from KDE 3 yet.\n\nThis is a release for developers and die-hard testers. If you just want to use it, wait a little longer - at least until the first betas have arrived."
    author: "Jakob Petsovits"
  - subject: "X"
    date: 2007-05-11
    body: "i still can't get x to start in minix, oops, wrong thread"
    author: "joesomeone"
  - subject: "thanks"
    date: 2007-05-11
    body: "thanks for all!!!"
    author: "06labs"
  - subject: "Any chance that the Final will be called"
    date: 2007-05-11
    body: "Kunt ? ;-)"
    author: "Knut"
  - subject: "Re: Any chance that the Final will be called"
    date: 2007-05-12
    body: "No"
    author: "Matt"
  - subject: "Re: Any chance that the Final will be called"
    date: 2007-05-12
    body: "Eew ;-) By the way, the german word for bear (b\u00e4r) is often used as a synonym to the word in question..."
    author: "RAM"
  - subject: "Re: Any chance that the Final will be called"
    date: 2007-05-13
    body: "bah....you stole my joke :("
    author: "Kirby"
  - subject: "Windows build"
    date: 2007-05-12
    body: "Any timeframe on a windows build?"
    author: "Luizg"
  - subject: "Re: Windows build"
    date: 2007-05-12
    body: "No, there are not plans to release a binary for this alpha. A lot of other work has to be done before..."
    author: "Christian Ehrlicher"
  - subject: "Pronunciaton of \"Knut\""
    date: 2007-05-12
    body: "For English speakers, it's pronounced \"cnoot\" with a short \"oo\"."
    author: "Andreas"
  - subject: "Re: Pronunciaton of \"Knut\""
    date: 2007-05-12
    body: "Erm, sorry. The \"oo\" is not short, it's just not long like the usual \"oo\"."
    author: "Andreas"
  - subject: "Re: Pronunciaton of \"Knut\""
    date: 2007-05-12
    body: "Yes, it's somewhere in between \"Linoox\" and \"cnoot\" ;-)"
    author: "Torsten Rahn"
  - subject: "Re: Pronunciaton of \"Knut\""
    date: 2007-05-12
    body: "Give up. It is supposed to be pronounced the same way a kn in knight was originally, so let it suffer the fate.. \n\nGo 'nut!\n\n"
    author: "Carewolf"
  - subject: "Re: Pronunciaton of \"Knut\""
    date: 2007-05-12
    body: "http://upload.wikimedia.org/wikipedia/commons/e/e9/Mgm_pronunciation_Knut.ogg"
    author: "ac"
  - subject: "Re: Pronunciaton of \"Knut\""
    date: 2007-05-12
    body: "Had a American teacher some years ago, she pronounced it \"Key nut\". But she was from California and she was not very bright either, so that may explain it."
    author: "Morty"
  - subject: "Re: Pronunciaton of \"Knut\""
    date: 2007-05-15
    body: "Reminds me of a folk-pop band called \"I am Knoot\".\nAh wait, not quite ;)"
    author: "Jakob Petsovits"
  - subject: "They took out KONQUI?!"
    date: 2007-05-12
    body: "WTF blazes.. .?!  they took out konqueror?  my beloved konqui?!  WAAAAAAAAAAAAAAAAAAAAAAA.................!!!!!!!!!!!\n\ngive her back!give her back!give her back!give her back!give her back!\n\ndolphin is like nautilus. i just hate it.. ."
    author: "zero1"
  - subject: "Re: They took out KONQUI?!"
    date: 2007-05-12
    body: "Calm down, Konqueror is still in kdebase and AFAIK there are no plans to remove it from there. It's still the KDE web browser and it still supports file management too, it's just not the default file manager anymore."
    author: "Kevin Kofler"
  - subject: "Re: They took out KONQUI?!"
    date: 2007-05-12
    body: "Try dolphin more before you claim you hate it, or that its like nautilus.\n\nAlso we already had this same exact discussion on the Dot for the last quite a while."
    author: "Sutoka"
  - subject: "Re: They took out KONQUI?!"
    date: 2007-05-14
    body: "It sprinkles .dolphin files everywhere (fixable) and the 'new' (Vista ripoff) location bar is not an improvement. Why must the user learn two location bars? One for web and one for local browsing."
    author: "K\u00e5re S\u00e4rs"
  - subject: "Re: They took out KONQUI?!"
    date: 2007-05-14
    body: "You don't have to. The traditional location bar still exists in Dolphin and can be set as the default if you don't want the breadcrumb toolbar.\n\nAlso, I don't think it's quite fair to say that it's a Vista ripoff. The breadcrumb toolbar has existed for quite sometime now, on GNOME, on OS X (Pathfinder, I think), and on websites. They just differ in their implementations."
    author: "Jucato"
  - subject: "Re: They took out KONQUI?!"
    date: 2007-05-14
    body: "Hehe! Like what everyone else said, Konqueror is still the default web browser for KDE 4.\n\nOh, btw, Konqi is a guy. His girlfriend is Katie (Wonder why they didn't name her Kate, for the text editor...)\n\nc.f. http://en.wikipedia.org/wiki/Konqi"
    author: "Jucato"
  - subject: "Problem using kdesvn-build"
    date: 2007-05-12
    body: "Hello,\nthe build script gives an error here:\n\nUpdating kdesupport\nsvn: Unrecognized URL scheme for 'https://svn.kde.org/home/kde/branches/KDE/3.5/kde-common/admin'\n        Update failed, trying a cleanup.\n        Executing svn cleanup\n        Cleanup complete.\nsvn: Unrecognized URL scheme for 'https://svn.kde.org/home/kde/branches/KDE/3.5/kde-common/admin'\nError updating kdesupport, removing from list of packages to build.\n\nWhat's wrong?\n\nThanks,\n\nFlorian\n"
    author: "Florian"
  - subject: "Re: Problem using kdesvn-build"
    date: 2007-05-12
    body: "Your svn doesn't have https support. Rebuild it."
    author: "Thiago Macieira"
  - subject: "Re: Problem using kdesvn-build"
    date: 2007-05-12
    body: "Mhh.. I'm using Gentoo and it seems that it doesn't offer an ssl use flag for subversion:\n\n[ebuild   R   ] dev-util/subversion-1.3.2-r3  USE=\"berkdb nls nowebdav python zlib -apache2 -bash-completion -emacs -java -perl -ruby\" 0 kB\n\nAny suggestions?\n\nFlorian"
    author: "Florian"
  - subject: "Re: Problem using kdesvn-build"
    date: 2007-07-19
    body: "USE=-nowebdav emerge -va subversion"
    author: "Seth W. Klein"
  - subject: "whatever happened to plasma?"
    date: 2007-05-12
    body: "In early discussions about KDE4 there was a lot of talk about Plasma. It is even still linked from within the oxygen site. But reading this announcement it is no more mentioned. Has it been merged into s.th. else? Has it completely vanished? Has it been postponed? What's up with Plasma? Since I am only an average KDE user, I admittedly visit dot.kde.org only every once in a while - so please excuse me, if I missed some discussions about Plasma.\n\nAnyway: Looking at http://plasma.kde.org/ and following the link \"Plasma APIs\" the page states: \"The APIs for working with and improving Plasma will appear here as they become available. They will undoubtedly be available sooner than that in KDE's subversion code repository where you are welcome to track Plasma development as it happens.\" Doesn't sound too good to me. "
    author: "rittmey"
  - subject: "Re: whatever happened to plasma?"
    date: 2007-05-12
    body: "Plasma is being worked on. I suggest you look up the past discussions on the dot (recent commit digests). "
    author: "Luca Beltrame"
  - subject: "Re: whatever happened to plasma?"
    date: 2007-05-12
    body: "So it's not on schedule. Big deal."
    author: "reihal"
  - subject: "Re: whatever happened to plasma?"
    date: 2007-05-12
    body: "Follow the link at the end of the article:\nhttp://www.kde.org/announcements/visualguide-4.0-alpha1.php"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: whatever happened to plasma?"
    date: 2007-05-12
    body: "what is the difference between new alt-f2 run and katapult? wasn't katapult to take over the place of that function?"
    author: "hag"
  - subject: "Re: whatever happened to plasma?"
    date: 2007-05-12
    body: "No. AFAIK they will share \"runners\", for example for Bookmarks, Application etc. "
    author: "Lans"
  - subject: "Re: whatever happened to plasma?"
    date: 2007-05-14
    body: "c.f. http://aseigo.blogspot.com/2007/03/in-other-monday-news.html\n\nThey will be hopefully sharing backends (called runners in KRunner and catalogs in Katapult) in KDE 4.\n\nThe two currently have very big differences:\nKatapult is in extragear and KRunner is in kdebase, which makes the latter an essential part of KDE (the replacement for the Run Command dialog box).\nKatapult is basically a launcher, with some catalogs for calculation and spell checking. I believe KRunner is going to be more than that.\nKrunner can display multiple hits. Katapult currently can't.\n\nWe'll hopefully see more development and clarifications in the future. Keeping my fingers crossed.\n"
    author: "Jucato"
  - subject: "Re: whatever happened to plasma?"
    date: 2007-05-13
    body: "That reminds me, I must complain to my painter that he hasn't finished painting the walls on my new house yet.  I know the builders have only just finished putting up the framing and are only now starting to put on the wallboard, but that's no excuse for the painter to only be talking about the top coat instead of already having it done, it just doesn't sound good to me..."
    author: "Odysseus"
  - subject: "Re: whatever happened to plasma?"
    date: 2007-05-13
    body: "Very good metaphor!\n\nUnfortunately this is a general problem in the software industry. Customers assume that because software isn't built with \"real\" matter, engineers would be free to choose an arbitrary building order.\n\n "
    author: "Kevin Krammer"
  - subject: "Beryl integration"
    date: 2007-05-12
    body: "Hi, I was wondering if there is any work done on better support for Beryl. I'm thinking about such things as:\n\n* being able to handle viewports (aka side of a cube) just like virtual desktops, which means:\n* taskbar that has the option to only show windows from current viewport\n* pager that can handle it (but this has already improved in KDE3)\n* more exotic things like different wallpapers on each viewport and give them different names that show in the pager, like you can for virtual desktops now"
    author: "Selene"
  - subject: "Re: Beryl integration"
    date: 2007-05-12
    body: "Hi\nsearch for kwin_composite.Kwin4 is supporting Compositing extensions.there are a few videos in youtube.\nYou will not need Beryl in KDE4."
    author: "Emil Sedgh"
  - subject: "Re: Beryl integration"
    date: 2007-05-12
    body: "ALso, see:\n\nhttp://www.kdedevelopers.org/node/2787"
    author: "Anon"
  - subject: "Re: Beryl integration"
    date: 2007-05-13
    body: "Oh yes, I could have known that :-) I didn't make that link with kwin_composite since it didn't have the \"cube\"... Glad to see the great work going on there in any case.\n\nBut the questions could be asked again, but this time for kwin_composite then :-) As it's still the taskbar and kdesktop that needs to support the viewports."
    author: "Selene"
  - subject: "Re: Beryl integration"
    date: 2007-05-13
    body: "Strange, I have missed this. Have it been presented here on the dot?"
    author: "reihal"
  - subject: "Re: Beryl integration"
    date: 2007-05-13
    body: "It was on planetkde.org IIRC"
    author: "Selene"
  - subject: "Re: Beryl integration"
    date: 2007-05-13
    body: "It has been mentioned in discussions a few times, and perhaps a line or two in some article. But newer as a big part of any article. \n\nOne reason being that is has been developed in a branch, but now it has moved to trunk. But I guess we will see a \"Road to KDE 4\" article about it at some point now, since Troy only covers the stuff in trunk."
    author: "Morty"
  - subject: "Re: Beryl integration"
    date: 2007-05-13
    body: "This seems to be the source for info on Kwin composite:\n\nhttp://www.kdedevelopers.org/blog/280"
    author: "reihal"
  - subject: "Re: Beryl integration"
    date: 2007-05-13
    body: "He, that did the trick, there's more than a 1000 guests there now.\n\nThe Grid rules the Cube!\n\n"
    author: "reihal"
  - subject: "Kmail status"
    date: 2007-05-12
    body: "I've just to say.... wow!\n\nI was very pessimistic reading that there's lack of manpower in kdepim, that there will be problem etc. So when I launched KMail in Alpha1 I was expeting to find a bunch of horrible semi-ported widgets and little more but...\n- there is (finally) a first time wizard that guide you through the creation of a mail account.\n- improved toolbars (especially while composing), that with the power of QT4 really rock (I *love* the effect you have when clicking on >> to show the toolbar's element not shown, simply amazing, the best implementation of this function ever seen)\n- options panel: again WOW! Finally HIG is used and respected! There are still lot of options but they are organized way way better than KDE3.x\n\nSo, maybe we'll have to wait 4.1 to have full Akonadi support, but I'm quite happy anyway.\n\nGreat work!!"
    author: "Vide"
  - subject: "Re: Kmail status"
    date: 2007-05-13
    body: "Yes, but you won't still send encrypted mails if you have not a key (that is necesary just to sign in mails). See this bug of Kmail of 2004-11-17:\n  https://bugs.kde.org/show_bug.cgi?id=93436"
    author: "I\u00f1aki Baz"
  - subject: "Re: Kmail status"
    date: 2007-05-13
    body: "It's nice to hear some positive feedback.  For a change.\n\nEncouragement, patches, \"how can I help\" messages are so much more effective than bug nags and bitches."
    author: "Allen"
  - subject: "Re: Kmail status"
    date: 2007-05-13
    body: "Yeah I know there is a lot of \"bad karma\" around Kmail in bko, but I think you're doing a great job. Sure, Kmail it's not perfect but the work you, the kdepim guys, had put in it it's worth a big THANKS from your users."
    author: "Vide"
  - subject: "Re: Kmail status"
    date: 2007-05-15
    body: "i am a firm believer that the reason you don't hear much in the way of positive feedback is that it works as well as it does... There's no need for hype around KMail, because for the vast majority it simply works... Great job :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Kmail status"
    date: 2007-05-13
    body: "I also would like to thank for all the work done on KMail. I simply love this email client. So many powerful features! And Vide is right, it is coming along much better that it seemed from a relative lack of documentation of the ongoing work on the dot and the planet. Thanks for all your work! There are so many of us who appreciate it. :)"
    author: "mutlu"
  - subject: "Re: Kmail status"
    date: 2007-05-14
    body: "true, it's a pity there aren't that many ppl working on it, but it still is a very good app - which speaks about it's quallity..."
    author: "superstoned"
  - subject: "WTF..."
    date: 2007-05-13
    body: "Waw...\n\nThe new 'GNOME' looks pretty...:(\n\nBTW..\nCongratulations to all KDE developers, that I'm sure has not taken part in this HORRIBLE 'UI' design!"
    author: "kde"
  - subject: "Re: WTF..."
    date: 2007-05-13
    body: "Keep in mind that this is a very early alpha release, and what you *see* here is likely to change drastically. However, with that in mind, if you'd like to itemise any serious suggestions you have for improvements, I'm sure they would be appreciated.\n\nSarcastic ranting, on the other hand, won't help anyone."
    author: "Jeff Parsons"
  - subject: "Re: WTF..."
    date: 2007-05-13
    body: "That's Dolphin Mr. Dumbo, you still have Konqi.\nKDE4 is still KDE. And everything is coming nicely, despite the many trolls who said it was vapourware.\n\nIf you want to troll go to Digg or Slashdot, not here. *freakin' jackass mumble mumble*"
    author: "Some guy"
  - subject: "Questions"
    date: 2007-05-13
    body: "- will there be nightly builds?\n\n- how to develop for KDE 4 or desriptions how to compile sources?\n\n- are there virtual client images?\n"
    author: "Benjamin"
  - subject: "Re: Questions"
    date: 2007-05-13
    body: "On TechBase you find all needed HOWTOs:\n\nhttp://techbase.kde.org/Getting_Started/Build/KDE4\n\nhttp://techbase.kde.org/Getting_Started/Set_up_KDE_4_for_development\n\nhttp://techbase.kde.org/Getting_Started/Increased_Productivity_in_KDE4_with_Scripts\n\n\n\nAbout nightly builds: KDE never provided binaries, ask you distributor."
    author: "Carsten Niehaus"
  - subject: "Oxygen icons"
    date: 2007-05-13
    body: "Wow, they are great! Simply amazing! I started up the liveCD and I thought this not possible. Although I have seen some screenshots, seeing it work is something else. Really, great.\n\nThe only icon I don't like is the folder icon. I don't know, it just looks strange. I think it's 1) the sight from the front and 2) it just doesn't look like a folder. I know this is still work in progress so I hope to see something else soon. I really don't want to offend you, it just doesn't fit with all the other icons that are so detailed and just great...\n\nWhat do the others think?"
    author: "Jonathan"
  - subject: "Re: Oxygen icons"
    date: 2007-05-13
    body: "\"I really don't want to offend you, it just doesn't fit with all the other icons that are so detailed and just great...\n \nWhat do the others think?\"\n\nMany people have said exactly the same thing - to me, it looks like a couple of floppy pieces of paper awkwardly propped up against one another.\n\nI much prefer the older style, which is still used for folder-home:\n\nhttp://websvn.kde.org/*checkout*/trunk/KDE/kdelibs/pics/oxygen/scalable/places/folder-home.svg?revision=640052\n\nPractically everything else is jaw-dropping beautiful, though - check out kbugbuster ( http://websvn.kde.org/*checkout*/trunk/KDE/kdelibs/pics/oxygen/scalable/apps/kbugbuster.svg?revision=640052 ) if you haven't done so already! :)"
    author: "Anon"
  - subject: "Re: Oxygen icons"
    date: 2007-05-13
    body: "Yeah, I tend to agree although I must admit that looking again to the classic crystal folder, you're tempted to think.: what is this?? I mean, once we'll be used to these oxygen folders we'll not see them so badly... I hope :P"
    author: "Vide"
  - subject: "Re: Oxygen icons"
    date: 2007-05-13
    body: "http://davigno.oxygen-icons.org/?p=36\n\nI also think the old folder looks mych better - in larger sizes. Can't get svn to work (almost succeed yesterday, then I ran out of space. Gah. At that time I really hated mt laptop. The Kubuntu packages don't work that well either) but I will try out the new live CD now. Maybe you just have to get used to it?"
    author: "Lans"
  - subject: "Re: Oxygen icons"
    date: 2007-05-13
    body: "Well, it's matter os taste, I love the new folder, the first time I saw it, I thought too that it was ugly, and that it wasn't a folder. But you when you use them both, after you get use to the new one, the older looks like a crap.\n\nTried it, use one some time the old one (in KDE 3 if you want to) and then use the new one ;P."
    author: "Luis"
  - subject: "Re: Oxygen icons"
    date: 2007-05-13
    body: "Holy crap, I wrote too fast...\n\n\"Well, it's matter os taste, I love the new folder, the first time I saw it, I thought too that it was ugly, and that it wasn't a folder. But when you use them both, after you get use to the new one, the older looks like a crap.\n \nTry it, use the old one some time (in KDE 3 if you want to) and then use the new one ;P.\"\n\nYes, I know, I should use the preview.\n\nbtw, sorry for my bad english."
    author: "Luis"
  - subject: "Some cool inspirations"
    date: 2007-05-16
    body: "http://www.stud.fit.vutbr.cz/~xcapmi00/etoile/index.html"
    author: "Andre"
  - subject: "Re: Some cool inspirations"
    date: 2007-05-19
    body: "Looks like MacOSX to me."
    author: "Chris"
  - subject: "Re: Some cool inspirations"
    date: 2007-05-23
    body: "On the surface, yes, but take a look at the two task analyses that he did, in particular Task B - Group Project. Basically, the interface is entirely task oriented, includes collaboration support and all other sorta of nifty things like that..."
    author: "Dan Leinir Turthra Jensen"
---
The KDE Community is happy to announce the immediate availability of the first alpha release of the
KDE Desktop Environment, version 4.0. This release is a basis for the integration of powerful new
technologies that will be included in KDE 4. Read on for more details.


<!--break-->
<p>Highlights of KDE 4.0 Alpha 1 are:

<ul>
	<li> A new <em>visual appearance</em> through <a
href="http://www.oxygen-icons.org">Oxygen</a></li>
	<li> New <em>frameworks to build applications</em> with, providing vastly improved
hardware and multimedia integration (through <a href="http://solid.kde.org">Solid</a> and <a
href="http://phonon.kde.org">Phonon</a>), spelling and grammar checking, to name just a few</li>
	<li>New <em>applications</em> that focus on a smooth user experience, such as Dolphin, the file manager, and Okular, the document viewer</li>
</ul>
</p>
	
<div align="center"><a href="http://www.kde.org/announcements/visualguide-4.0-alpha1.php"><img src="http://www.kde.org/announcements/visual_guide_images-4.0-alpha1/desktop-thumb.png" border="0" title="The desktop in its budding glory" alt="The desktop in its budding glory" /></a></div>

<p>KDE 4.0 Alpha 1 marks the end of the addition of large features to the KDE base libraries and shifts the focus onto integrating these new technologies into applications and the base desktop. The
next few months will be spent on bringing the desktop into shape after two years of frenzied development leaving very little untouched.<p />

<p>The final release of KDE 4.0 is expected in late October 2007.</p>

<p>The work on the framework makes application development easier and more satisfying. This will result in a crop of new features and applications over the next few years, underlining the position KDE holds as the leading Free Desktop and bringing Free Software to new users worldwide. A detailed <a href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Schedule">release schedule</a> can be found at <a href="http://techbase.kde.org">techbase.kde.org</a>, the new technical resource accompanying the platform, that provides extensive and high-quality documentation about KDE.<p />

<p>More information such as screenshots and installation instructions can be found in the <a href="http://www.kde.org/announcements/visualguide-4.0-alpha1.php">KDE 4.0-alpha1 Visual Guide</a>.</p>

