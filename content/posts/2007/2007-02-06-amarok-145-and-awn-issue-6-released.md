---
title: "Amarok 1.4.5 and AWN Issue 6 Released"
date:    2007-02-06
authors:
  - "Ljubomir"
slug:    amarok-145-and-awn-issue-6-released
comments:
  - subject: "Client/server separation for 2.0 ?"
    date: 2007-02-05
    body: "First of all: thanks to the Amarok team for another great release!\n\nHere's a suggestion for Amarok 2.0: client/server separation. My dream would be to be able to run the collection database and the soundsystem on my home server, while using the GUI on my laptop.\n\nWell, of  course this is only a totally uninformed idea from a simple user :)"
    author: "Benoit Jacob"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-05
    body: "You can already do that with X.  Assuming both machines are running linux, just ssh -X USER@IP.ADDRESS on the laptop, then once in there run 'amarok' and it'll appear on your laptop, while everything is being ran from the desktop (including outputting the sound there)."
    author: "Corbin"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "If you are running X over a network with ssh, I would strongly recommend including the -C flag to compress the data as well.  It improves performance considerably.\n\nSo:  ssh -XC user@host"
    author: "Robert Knight"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "ssh -X USER@IP.ADDRESS \"amarok\"\n\nAssuming you have your ssh key loaded in your agent, and have your pub key in .ssh/authorized_keys on the remote machine, you can make an icon on your local desktop that has that line as the executable.\n\nBut this probably isn't the kind of client/server seperation the original poster was asking about.  When amarok quits locally, so does the music.  He's probably looking for something more like mpd[1] or xmms2[2].\n\n[1] http://www.musicpd.org\n[2] http://xmms2.sf.net"
    author: "Nope"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "or, heck, amarok_httpremote[1]\n\n\n[1] http://www.kde-apps.org/content/show.php?content=36970"
    author: "Nope"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "I guess I forgot to say that I don't want to run a X server on my server...\n\nEspecially since the non-graphical part of Qt and now kdelibs has been isolated, I thought it would be possible to do the same in Amarok."
    author: "Benoit Jacob"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "\"I guess I forgot to say that I don't want to run a X server on my server...\"\n\nClassic X misunderstanding #1!  \n\nNo X server runs on your server.  An X client (amarok) runs there and uses the X server on your laptop for its display."
    author: "Will Stephenson"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "Very interesting, I didn't know!"
    author: "Benoit Jacob"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "The only problem with X client/server is that if you close the X server (on your laptop), it stops the client (amarok on your server).\nIt would be useful to have some kind of X proxy to keep the session runing (as screen does for terminal) but I don't know one.\n\nBTW, if Amarok 2 starts using phonon, with the appropriate backend, it would allow you to do that also (open files on the server through KIO, then send the sound back for example with NMM)"
    author: "Anarky"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-09
    body: "you could check out nx/freenx to accomplish this."
    author: "serfiusens"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "I've been wanting this too, but not via X forwarding or anything like that.  I have a server which runs headless, and doesn't have KDE or xorg installed, and I use mpd/mpc to use it as an audio server.  I've always wanted Amarok to be able to control mpd though.  Anyone have any plans of implementing this capability?"
    author: "Mike"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "i think i have seen a amarok plugin in kde-apps.org that does that"
    author: "Beat Wolf"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "You're welcome to try out AmarokMPC [1] and give some feedback.\n\n[1] http://www.kde-apps.org/content/show.php?content=36970"
    author: "Chris"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-08
    body: "Sorry, that went wrong. The link is http://www.kde-apps.org/content/show.php?content=51577\n\nChris"
    author: "Chris"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "I'm not sure whether Amarok should include the server part, but maybe they could provide a nice interface to slimserver (http://www.slimdevices.com/pi_features.html) and/or Jinzora (http://www.jinzora.org/)."
    author: "Joergen Ramskov"
  - subject: "Re: Client/server separation for 2.0 ?"
    date: 2007-02-06
    body: "Having remote sound will become be even cooler than usin the X way of today. With KDE 4, using Phonon with the NMM backend will give lots of interresting new possiblities. "
    author: "Morty"
  - subject: "Outstanding Work!"
    date: 2007-02-05
    body: "A fantastic summary of features as always, ljubomir - you have a great way of presenting information from all over the place in a simple, concise manner that's a pleasure to read.\n\nThank you!"
    author: "dangle"
  - subject: "Re: Outstanding Work!"
    date: 2007-02-05
    body: "I second that - great work! <3\n\nMaybe you could add that the fs-interface is still in development and pretty new..."
    author: "fish"
  - subject: "Re: Outstanding Work!"
    date: 2007-02-06
    body: "Does fs stand for full screen? As in http://www.kde-apps.org/content/show.php?content=52641? If it does I heartily second that. I'd love to see an interface like that in amarok."
    author: "reldruh"
  - subject: "Re: Outstanding Work!"
    date: 2007-02-06
    body: "Its called Amarok Full Screen...\n\nStuff like this is the reason we have a script interface."
    author: "Ian Monroe"
  - subject: "Re: Outstanding Work!"
    date: 2007-02-06
    body: "You're not the only one!  ;)"
    author: "fish"
  - subject: "Re: Outstanding Work!"
    date: 2007-02-06
    body: "Just using it right now :) v cool! there is a little problem when any of the \"album\" or \"track name\" is very long... will report in kde-look"
    author: "NabLa"
  - subject: "Hmm"
    date: 2007-02-06
    body: "Yeah but it doesn't look like they've improved the next track speed (takes about 1/2 second for me), or made the previous track button work in random mode...\n\n"
    author: "Tim"
  - subject: "mockup"
    date: 2007-02-06
    body: "i really like the mockup, tough it has some problems. the top web-2.0-like bar is way too big, imho, and to drag'n'drop songs to the playlist you first have to go past the context window, not really handy.\n\nbut the playlist looks good (better than current), and the bigger context window is great. imho this has a future ;-)"
    author: "superstoned"
  - subject: "Synonyme"
    date: 2007-02-06
    body: "What I personally miss with Amarok and media players in general is a way to detect\nsynonymous artists when you have MP3. Of course you can correct it manually...\n\nH. Gr\u00f6nemeyer should the same as Herbert Gr\u00f6nemeyer etc. On what level can be synonymous or mispelled descriptions be stored and corrected. Would the wikipedia redirect be helpful? \n\nAmarok here creates two different artist categories. "
    author: "Janssen"
  - subject: "Re: Synonyme"
    date: 2007-02-07
    body: "I'm not speaking on behalf of the Amarok-team, but I guess that won't happen. This topic is highly complex and up to now there doesn't seem to be any system that can offer the solution you're asking for. Just to give a short example: Check for \"Prodigy\" and \"The Prodigy\" and you will realise that there are actually two different entries. Merging them would be a hugh mistake.\nChris"
    author: "Chris"
---
The long-awaited <a href="http://amarok.kde.org/">Amarok</a> 1.4.5 has <a href="http://amarok.kde.org/content/view/10/66/">finally been released</a>. Major changes include an integrated Shoutcast stream directory, the new Magnatune music store re-download manager, support for track labeling, and improved sound quality when using the equalizer with xine engine. Many of the new features are explained in the latest issue of the <a href="http://ljubomir.simin.googlepages.com/awnissue6">Amarok Weekly Newsletter</a>.






<!--break-->
