---
title: "KDE Italia at OpenMind 2007 This Week"
date:    2007-02-20
authors:
  - "gventuri"
slug:    kde-italia-openmind-2007-week
---
OpenMind 2007 is an Italian event dedicated to Free Software and free content. The event will be from this Thursday until Saturday (22nd to 24th) February in San Giorgio a Cremano, Naples.  Giovanni Venturi, <a href="http://www.kde-it.org/">KDE Italia</a> webmaster, KDE Italian translator and KSniffer developer will present KDE 3.5 and will give an update on the state of KDE 4 and Qt 4 on the 24th (Saturday) and will be running a KDE booth. Italian readers can find more in <a href="http://www.kde-it.org/index.php?option=com_content&amp;task=view&amp;id=85&amp;Itemid=1">the complete article</a>.



<!--break-->
