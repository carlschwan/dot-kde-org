---
title: "First KDE Education Meeting a Great Success"
date:    2007-12-06
authors:
  - "fgladhorn"
slug:    first-kde-education-meeting-great-success
comments:
  - subject: "software libre?"
    date: 2007-12-05
    body: "excuse me, but what is software libre?"
    author: "anonymouse"
  - subject: "Re: software libre?"
    date: 2007-12-05
    body: "That's free software (as in freedom). As far as I know it is french and it is used sometimes because the English word free is ambiguous(free as in free beer and free as in freedom)."
    author: "ac"
  - subject: "Re: software libre?"
    date: 2007-12-06
    body: "It's Spanish actually :-)"
    author: "Anonymous"
  - subject: "Re: software libre?"
    date: 2007-12-06
    body: "\"Software libre\" is a Spanish phrase not French. You're right on the meaning of it though."
    author: "attendant"
  - subject: "Re: software libre?"
    date: 2007-12-06
    body: "You're certainly right but \"libre\" is also used in french and, AFAIK, got the same meaning as the spanish \"libre\". Only the pronounciation differs :).\n\nIn french we rather say \"logiciel libre\" though."
    author: "Anne Onymous"
  - subject: "Re: software libre?"
    date: 2007-12-06
    body: "excuse me, but what is logiciel?"
    author: "reihal"
  - subject: "Re: software libre?"
    date: 2007-12-06
    body: "software."
    author: "bluestorm"
  - subject: "Re: software libre?"
    date: 2007-12-06
    body: "I think 'Software Libre' is a really good name for what the KDE community produces. If we use the term 'Open Source' it makes it much easier for big business companies, such as Microsoft, to coopt the term and invent other names like 'Shared Source', which aim to blur the difference between communities like KDE, and aggressive businesses which are only in it for profit. Microsoft could never describe themselves as 'Free Software' and so we can differentiate ourselves from them by using terms like 'Free Software' or 'Software Libre' - if we describe ourselves as 'Open Source' it would make it harder to make that distinction."
    author: "Richard Dale"
  - subject: "Re: software libre?"
    date: 2007-12-07
    body: "umm isn't internet explorer free?  "
    author: "matt"
  - subject: "Re: software libre?"
    date: 2007-12-07
    body: "well, yes, free as in free beer, but not free as in free speech... It can be downloaded without paying any monetary costs, but you DO pay in terms of surrendering your freedom to MS."
    author: "jospoortvliet"
  - subject: "Re: software libre?"
    date: 2007-12-07
    body: "Not true, an Internet Explorer license is tied to a Windows License, as such it is not free, in either sense."
    author: "Rasmus Larsen"
  - subject: "Re: software libre?"
    date: 2007-12-09
    body: "Propaganda aside, this is not true.\n\nThe MS licenses are almost ALWAYS only available for paying people.\n\nSo in fact, you pay to get screwed and became slave to the software of a huge company with different interests than yours and a tradition of spending more money to their CEOs and buy off competition, than to improve their products.\n\nInternet Explorer is NOT free anywhere, not even a free beer (once you drink the beer, you kinda \"own\" it until you digested it... you can NEVER do anything similar with IE!)"
    author: "she"
  - subject: "Re: software libre?"
    date: 2007-12-07
    body: "The English word free is not ambiguous; it unambiguously means without.  It should be qualified in order to create a meaningful phrase, e.g. germ-free, free of charge, free of shackles.  Using it unqualified is foolish; it's surprising that RMS made such a basic mistake."
    author: "Rob"
  - subject: "Re: software libre?"
    date: 2007-12-07
    body: "Doublepluscorrect. Please note that this is the only permissible use of the word \"free\" according to the Newspeak Dictionary. \n\nThank you,\n\nYour friendly neighborhood thought officer.\n\nWar is peace - Freedom is slavery - Ignorance is strength.\n"
    author: "Miniluv"
  - subject: "Re: software libre?"
    date: 2007-12-07
    body: "\"I'm a free man\"\n\"If you buy that package you can take this one for free\"\n\nAre you saying both of these are wrong uses of free?\n"
    author: "KBX"
  - subject: "Re: software libre?"
    date: 2007-12-06
    body: "Others have described the meaning well, but to go into more detail of why we use libre and not free: In English the word \"free\" has two different meanings.  The first is free as in free beer.   The second is free as in free speech.   In Spanish they use Gratis for the first, and libre for the second.  \n\nThere is a lot of software that costs nothing, but isn't free in other senses.\n Micorsoft's Internet Explorer is one example of this.   \n\nSee http://en.wikipedia.org/wiki/Gratis_versus_Libre"
    author: "Hank Miller"
  - subject: "Re: software libre?"
    date: 2007-12-07
    body: "Actually libre can mean gratis in Spanish too.\nExemple: entrada libre / free entrance\n\nit's just less comman than in English."
    author: "Patcito"
  - subject: "Guess twice: Which is my favorite KDE module?"
    date: 2007-12-06
    body: "While apps like Amarok, digiKam, KOffice, etc. get much buzz and hype (not that they don't deserve it), my personal little starlets in KDE are the new Kalzium and Step programs (okay, as a teacher, I am biased ;) as well as Kig, which has been in KDE 3 already.\n\nI love the idea of Step, and I hope it is unique in the FOSS world, making KDE the front choice for physics education.\n\nWhatever, just wanted to say this: Thanks to everyone contributing to KDE-EDU!\n"
    author: "christoph"
  - subject: "Re: Guess twice: Which is my favorite KDE module?"
    date: 2007-12-06
    body: "here here!"
    author: "Vladislav"
  - subject: "Re: Guess twice: Which is my favorite KDE module?"
    date: 2007-12-06
    body: "there there!"
    author: "Johhny Awkward"
  - subject: "Re: Guess twice: Which is my favorite KDE module?"
    date: 2007-12-06
    body: "where where!\n\n(I guess parent meant 'hear! hear!')"
    author: "jospoortvliet"
  - subject: "Re: Guess twice: Which is my favorite KDE module?"
    date: 2007-12-07
    body: "What what? :-p"
    author: "Diederik van der Boor"
  - subject: "Mandriva dead link"
    date: 2007-12-06
    body: "\"The meeting took place at the Mandriva office\"\n\nThe mandriva link has 4 ws' in the name and hence is dead."
    author: "Reader"
  - subject: "Re: Mandriva dead link"
    date: 2007-12-06
    body: "fixed"
    author: "Jonathan Riddell"
  - subject: "Small clarification"
    date: 2007-12-06
    body: ">Beno\u00eet and Vladimir spoke about using Eigen2 instead of GMM in Step.\n\nWe did indeed discuss with Vladimir about using eigen2 in Step, and consequently which features would be useful to add to eigen2 in order to cover step's needs.\n\nHowever, for sparse matrix support, my plan is to handle that in eigen2 as a wrapper around GMM. So GMM will still be used, though not directly.\n\nGMM is a great library and there's nothing wrong about it; also it's 18k LOC so we don't want to rewrite it. Only it has a very rough, C-style API and so there's a lot of value that can be added by providing a nice wrapper in Eigen2, with goodies such as Qt-style implicit sharing. Also Eigen2 does things (e.g. fixed-size objects) that aren't done in GMM and we wish to have a unique API, which is why it makes sense to add that wrapper to Eigen2."
    author: "Benoit Jacob"
  - subject: "mr. T cereal"
    date: 2007-12-09
    body: "The guy in the back would look cool dressed up as a vampire, I could see him making a lot of money in Hollywood."
    author: "pee wee"
  - subject: "mr. T cereal"
    date: 2007-12-09
    body: "The guy in the back would look cool dressed up as a vampire, I could see him making a lot of money in Hollywood."
    author: "pee wee"
  - subject: "What is the need of Eigen2???"
    date: 2007-12-11
    body: "As a scientist I am in doubt if the creation of a KDE oriented math library like Eigen or Eigen2 is a good choice. Why do I believe so: Eigen2 seems to be applied to a wide range of problems (each of very special). I foresay the need, for example, to implement various algorithms and data structures for the same problem class depending on parameters like system size, required accuracy, cpu power vs. memory demands... Within the scientific community highly performant libraries have been developed for such purposes such as LAPACK, ARPACK, etc. It is widely acknowledged that algorithms implemented in C/++ are still considerably slower than FORTRAN libraries such that only wrappers/interfaces are provided for FORTRAN libraries. Such a wrapper is for example provided by the trilinos project, one of the most comprehensive approaches in my opinion. I do not know how the Eigen lib is organized internally or if it already wraps around another lib. But inventing the wheel again is a pain in the ass. I simply do not believe that KDE has the power, time and (technological) comptetences to develop its own optimized math codes. "
    author: "Sebastian"
  - subject: "Re: What is the need of Eigen2???"
    date: 2007-12-24
    body: "You might look at it the converse way. The existing math libs may look satisfying to a numerical engineer, but from the point of view of a c++ software developer they are _horrible_ and they don't even cover the needs of simple apps, for instance they don't handle fixed-size objects. Since many projects besides KDE need linear algebra, you can bet that Eigen2 will easily find its public, hence a pool of potential contributor. In fact, even the very minimalistic Eigen1 was received with enthusiasm from game developers.\n\nAnd yes, we are planning to wrap around existing libs for what we don't want to reinvent.\n"
    author: "Benoit Jacob"
  - subject: "Re: What is the need of Eigen2???"
    date: 2007-12-24
    body: "Regarding C++ being much faster than FORTRAN: what you state here was true until 1994, when Expression Templates were invented:\n\nhttp://ubiety.uwaterloo.ca/~tveldhui/papers/Expression-Templates/exprtmpl.html\n\nBy the way, Eigen2 has the world's most concise implementation of expression templates."
    author: "Benoit Jacob"
---
Last weekend the members of the <a href="http://edu.kde.org/events/paris2007/">KDE-Edu team met in Paris</a> for a meeting about the Education project. The meeting took place at the <a href="http://www.mandriva.com">Mandriva</a> office, where the members got to know each other and started vivid discussions about their applications, life in general, as well as the future and vision of <a href="http://edu.kde.org">the Edu module</a>. Read on for the report.













<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right; width: 300px">
<a href="http://static.kdenews.org/jr/kde-education-group-photo.jpg"><img src="http://static.kdenews.org/jr/kde-education-group-photo-wee.jpg" border="0" /></a><br />
KDE Edu Developers: Anne-Marie Mahfouf, Jeremy Whiting, Carsten Niehaus, Patrick Spendrin, Mauricio Piacentini, Vladimir Kuznetsov, Aliona Kuznetsova<br />
Frederik Gladhorn, Jure Repinc, Albert Astals Cid, Peter Murdoch, Johannes Simon, Benoît Jacob, Aleix Pol<br />
See <a href="http://edu.kde.org/events/paris2007/pictures.php">more photos</a>
</div>

<p>While everyone meets quite often on IRC and talks via email, this was the first face-to-face meeting for many of us. New people from all around the world were welcomed heartily.</p>

<p>Vladimir and Aliona of <a href="http://edu.kde.org/step">Step</a>; Jeremy, who seems to be everywhere, takes care of <a href="http://edu.kde.org/kanagram">KAnagram</a> in particular; Johannes and Frederik working on <a href="http://edu.kde.org/parley">Parley</a>; Aleix Pol with his <a href="http://edu.kde.org/kalgebra">KAlgebra</a>; Peter improving <a href="http://edu.kde.org/kpercentage">KPercentage</a>; Jure for translations and Patrick struggling with "He-Who-Must-Not-Be-Named" (Windows). Long term contributors like Albert, Benoît, Carsten and Mauricio worked on their projects and helped the others with their insights. Benoît and Vladimir spoke about using <a href="http://eigen.tuxfamily.org/">Eigen2</a> instead of GMM in Step. And of course the amazing Anne-Marie who helped getting KDE Education started in the first place.</p>

<p>Software libre seems to attract nice people, everyone was really friendly and fun to talk to. The organisation of the meeting was not overly formal but since we all were really motivated, the result was a flow of ideas, code and motivation in all directions. Patrick gave a <a href="http://edu.kde.org/presentations/kde-edu-meeting paris.pdf">presentation covering the state of KDE on Windows</a> and explained his special affinity to the education team. The chance to have Edu well supported on Windows is especially great for us to reach more teachers and pupils to get them interested in software libre. And even schools that use free operating systems will like the opportunity to provide the software used at school to their pupils easier.</p>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left; width: 268px">
<a href="http://static.kdenews.org/jr/kde-education-step.png"><img src="http://static.kdenews.org/jr/kde-education-step-wee.png" width="268" height="215" border="0" /></a><br />
Step, physics simulator
</div>
<p>The entire team was euphoric and somewhat overwhelmed by the talk on <a href="http://edu.kde.org/step">Step</a>, the physics simulator, due to be released with KDE 4.1, but already in a great state. You should definitively give it a try. It is located in playground and to get started we recommend opening some of the great examples. Wow!</p>

<p>On Saturday evening we all went out to visit Paris, lead by Benoît. We saw the Palais Royal, the Louvre and then went to a restaurant where we waited for ages! This allowed us to chat and carry on exchanging ideas.</p>

<p>We are looking forward to getting KDE 4.0 out of the door and our applications into the wild in order to receive more feedback and harvest the fruits of our work. Mauricio worked hard, successfully getting his recently adopted child, <a href="http://edu.kde.org/kturtle">KTurtle</a>, into a releasable state so it will ship with KDE 4.0. Aleix worked on usability enhancements in <a href="http://edu.kde.org/kalgebra">KAlgebra</a>. Albert and Jure fixed some deeper bugs to allow our applications to actually start, and of course Albert was helping here and there just as great as he is on IRC. Jeremy, who lately was pushed to adopt KHotNewStuff2, got pushed by Frederik to actually implement some yet-to-be done parts of the collaborative data sharing framework.  Finding spots that have simply not been implemented is a challenge there.</p>

<p>The chemical division has a lot to offer, and many people are involved. Carsten and Benoît were there to represent them. Not only <a href="http://edu.kde.org/kalzium">Kalzium</a>, but also libraries like OpenBabel, Avogadro and the related Strigi plugin make the chemical desktop very attractive. Johannes worked intensively on redesigning parts of the <a href="http://edu.kde.org/parley">Parley</a> interface. We hope we can turn his very promising ideas into reality for KDE 4.1. Frederik's todo list grew hourly, as Carsten also had some great suggestions for the interface of <a href="http://edu.kde.org/parley">Parley</a>. Anne-Marie gave a short <a href="http://edu.kde.org/presentations/roadmap.pdf">presentation about the state of various applications for KDE 4.1</a>, such as <a href="http://edu.kde.org/kmathtool/">KMathTool</a> that seemed somewhat unmaintained. Also the general consensus was to merge at least <a href="http://edu.kde.org/kbruch">KBruch</a> and <a href="http://edu.kde.org/kpercentage">KPercentage</a> since they are quite strongly related.</p>

</p>Everyone noted just how greatly our communication improved, as well as the friendships and new cooperations which came into existence. We would also like to thank <a href="http://www.mandriva.com">Mandriva</a> very much for providing us with a great room to work in, two laptops, free coffee, tea and chocolate, as well as wifi. A big thank you also goes to <a href="http://ev.kde.org">the KDE e.V.</a> which sponsored travel and lodging. A special thanks from all participants goes to Anne-Marie Mahfouf, who brought us together in the first place by organising this great meeting. Fourteen happy Edu developers have returned to their homes with renewed enthusiasm.</p>












