---
title: "On the Fridge: KDE Invasion in the Canary Islands"
date:    2007-10-02
authors:
  - "rjohnson"
slug:    fridge-kde-invasion-canary-islands
comments:
  - subject: "You rock Aaron"
    date: 2007-10-02
    body: "definitly, we need more sales people - software presenters with elite skills like aaron..."
    author: "jumpingcube"
  - subject: "Re: You rock Aaron"
    date: 2007-10-02
    body: "Marketing and Business Management is my game, at least that is what all of this schooling has told me...now if I can just get them elite \"Aaron\" skills, then I will be truly happy. Oh, and I need one of them \"Jonathan\" kilts too."
    author: "Richard Johnson"
  - subject: "Re: You rock Aaron"
    date: 2007-10-02
    body: "Richard, we need more people with MBA-type experience within Free Software in general and KDE in specific. If you are interested in participating with the project in this manner, please get a hold of me and let's start talking. =)\n\nKDE: It's more than just code. =)\n\np.s. Nobody wears a quilt quite like Jonathan ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: You rock Aaron"
    date: 2007-10-02
    body: "And after your experience with the school project, it would be nice to tell your experience about this specific project too - where's your blog ;)"
    author: "she"
  - subject: "Re: You rock Aaron"
    date: 2007-10-02
    body: "Richard, we need more people with MBA-type experience within Free Software in general and KDE in specific. If you are interested in participating with the project in this manner, please get a hold of me and let's start talking. =)\n\nKDE: It's more than just code. =)\n\np.s. Nobody wears a kilt quite like Jonathan ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: You rock Aaron"
    date: 2007-10-02
    body: "As luck would have it, both spellings equally apply here."
    author: "Wade"
  - subject: "Re: You rock Aaron"
    date: 2007-10-02
    body: "thankfully. stupid typos. and web boards. =P"
    author: "Aaron J. Seigo"
  - subject: "Schools"
    date: 2007-10-02
    body: "I always thought that schools seemed more of a logical place for Linux adoption than the average home desktop."
    author: "T. J. Brumfield"
  - subject: "Re: Schools"
    date: 2007-10-02
    body: "300+ schools in Namibia run LTSP-based labs with Linux and KDE thanks to SchoolNet Namibia (http://www.schoolnet.na). ;-)\n\nMaybe, Aaron would like to come to Namibia as well? I would be very pleased to meet him personally.\n\nUwe\n"
    author: "Uwe Thiem"
  - subject: "Re: Schools"
    date: 2007-10-02
    body: "if we could find a way to get me there, i'd *love* to visit to learn about Namibia, the people who live there and the Free software they (you) use ..."
    author: "Aaron J. Seigo"
  - subject: "Re: Schools"
    date: 2007-10-02
    body: "And its a great place for a holiday, still rates tops of all my African visits..."
    author: "Odysseus"
  - subject: "Power to the People!"
    date: 2007-10-02
    body: "I really like it how small or developing countries make intelligent decisions like this one. It's the huge behemoth companies and governments that can't adjust get stuck in some ridiculous proprietary system. We all know what I am talking about. ;)"
    author: "winter"
  - subject: "Re: Power to the People!"
    date: 2007-10-02
    body: "I'm pretty sure the Canary Islands are part of Spain."
    author: "attendant"
  - subject: "Re: Power to the People!"
    date: 2007-10-02
    body: "Yes, you're right they are part of Spain, but the Islands are indeed quite small. In Spain the provinces have a great deal of control over their education systems, and so the Canaries are able choose an approach independent from the Peninsular."
    author: "Richard Dale"
  - subject: "Kreat!"
    date: 2007-10-02
    body: "Kongratulations to the Kanary islands! I'm sure they will be very happy with KDE."
    author: "Martin"
  - subject: "Primary desktop"
    date: 2007-10-02
    body: "The screenshot for the \"primary\" desktop looks quite interesting...\n\nhttp://www.grupocpd.com/archivos_documentos/info_meduxa/lanzamiento_meduxa_1/"
    author: "AV"
  - subject: "I'm from the Canaries!"
    date: 2007-10-03
    body: "I'm from the Canary Islands and i haven't even heard of this step in the schools (and i'm a linux user :-s ). Show how much attention i pay to the media :P."
    author: "Mars"
---
<a href="http://fridge.ubuntu.com">Ubuntu's Fridge</a> is reporting that <a href="http://fridge.ubuntu.com/node/1142">Kubuntu is taking the Canary Islands</a>. KDE is being installed on all of the school computers in the Canary Islands by the way of <a href="http://www.grupocpd.com/archivos_documentos/info_meduxa">mEDUXa</a> and in their universities with <a href="http://osl.ull.es/bardinux">Bardinux</a>, both derivatives of the Kubuntu GNU/Linux operating system. KDE's Aaron Seigo and Jonathan Riddell toured the schools in which mEDUXa is in use and spoke with the developers during the <a href="http://jornadas.osl.ull.es/2007/doku.php">Jornadas de Software Libre</a> conference. During that conference, Aaron Seigo presented KDE 4 and its exciting capabilities as well as an introduction to KDE and Qt programming.


<!--break-->
