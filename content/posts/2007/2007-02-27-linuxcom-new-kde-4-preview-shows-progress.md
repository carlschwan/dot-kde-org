---
title: "Linux.com: New KDE 4 Preview Shows Progress"
date:    2007-02-27
authors:
  - "jriddell"
slug:    linuxcom-new-kde-4-preview-shows-progress
comments:
  - subject: "Nice article"
    date: 2007-02-27
    body: "That was a good article, me thinks.\n\nIt's good, and probably a result of dot.kde.org too, that the press is well informed.\n\nOne question that I have though, is there really going to be no kwin? I am no Windows user, but for my part, I am certain, that the focus stealing prevention, more flexible Window behaviour, etc. would be really worth it. Or is this understood to be only initially the case?\n\nBest regards,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: Nice article"
    date: 2007-02-28
    body: "As far as I know it's not planned to adapt KWin for Windows.  I suspect that the reason is that the shell is far more tightly integrated than on *nix and that makes it impractical to replace one of its 'components'.  \n\nI'd want people to have some practical reasons left to move to a Free Software operating system, as well :)."
    author: "Will Stephenson"
---
Linux.com is reporting that the <a href="http://www.linux.com/article.pl?sid=07/02/26/1758230">New KDE 4 preview shows progress</a>.  The article has comments from developers Will Stephenson and Aaron Seigo about the new additions to the pre-release and the improved release process which should see more frequent updates.  It also discusses what will happen to the KDE 3 branch and even asks when KDE 4 will be released.


<!--break-->
