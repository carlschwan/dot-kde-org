---
title: "Guademy 2007 Event Report"
date:    2007-03-28
authors:
  - "alarrosa"
slug:    guademy-2007-event-report
comments:
  - subject: "Oh no!!!!"
    date: 2007-03-28
    body: "Are you good at soccer? Want an @kde.org email address. Please sign up for the team right now. No development skills needed!\n\nDid somebody file a critical bug report on this yet?"
    author: "Martin"
  - subject: "From a kde guademy organizer"
    date: 2007-03-28
    body: "Next year I will work more. First I am speaking with a pair of professional football players. Second, arbiter will not be a Gnome user on next Guademy. ;)"
    author: "Juanjo Iglesias"
  - subject: "Keep on losing soccer matchs"
    date: 2007-03-28
    body: "Let's assume it: either we develop great KDE code, or we train to win a soccer match. It looks like KDE people like the former and Gnome people the latter :-)"
    author: "Pau Garcia i Quiles"
  - subject: "Re: Keep on losing soccer matchs"
    date: 2007-03-28
    body: "It comes as no surprise to me that Gnome people would prefer training for a soccer match to writing great KDE code.  I'm sure KDE people would prefer to train for a soccer match if the alternative was writing great Gnome code.  :)"
    author: "Henry Miller"
  - subject: "Events like these..."
    date: 2007-03-28
    body: "Events like these really excite me when I hear them!  Thanks a ton GPUL, and I hope KDE & Gnome developers can continue the good will by supporting each other in the linux/desktop world!\n\n- Vlad"
    author: "Vlad"
  - subject: "Hope this will happen soon again"
    date: 2007-03-28
    body: "This was just a really really nice experience :)) Thx to everybuddy !!"
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Galiza"
    date: 2007-04-10
    body: "There are not many international open source-related events in Galiza. I hope there are more in the near future :D"
    author: "sup"
---
The first <a href="http://www.guademy.org/">Guademy</a> event finished yesterday at the university of A Coruña, Spain. Organized by the <a href="http://www.gpul.org/">GPUL</a> (Grupo de Programadores y Usuarios de Linux), the Linux User Group of A Coruña, it was an event which brought together people from the <a href="http://gnome.org/">GNOME</a> and <a href="http://kde.org/">KDE</a> camp (thus the combination of "<a href="http://guadec.org/">GUADEC</a>" and "<a href="http://akademy.kde.org/">aKademy</a>" that forms the name of the event). Read on for a short report of the event.

<!--break-->
<br />
The Guademy started on the 23rd of March and concluded on the 25th, and was sponsored by <a href="http://www.trolltech.com">Trolltech</a> and <a href="http://www.igalia.com">Igalia</a>. The three days of the conference were filled with talks about KDE and GNOME as well as talks about co-operation between the projects. For a first-hand account of the talks, you can read Rafael Fernandez blog entries <a href="http://www.ereslibre.es/?p=20">here</a> and <a href="http://www.ereslibre.es/?p=21">here</a>.
<br /><br />
<div align="center"><img src="http://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg" alt="Guademy Group Photo" title="Guademy Group Photo"></div>
<br /><br />
After all the talks on Friday, a <a href="http://en.wikipedia.org/wiki/Queimada">Queimada</a> was organized, including the traditional chants that are sung while it's prepared. In order to "release stress", the organizers of the event set up a soccer match between KDE and GNOME developers for the Saturday afternoon. The result of the game is better left unwritten :). On Saturday night, we had a great dinner in a local restaurant, with lots of really nice food from the local area and drinks that forced many people miss the first talks on Sunday.
<br /><br />
All the talks were recorded on video and will be published on the conference site, at <a href="http://www.guademy.org/">guademy.org</a> together with the slides and related information.
<br /><br />
We would like to thank the organizers, who did a great job with the event - everyone had a great time. We hope it is the first of a long series of Guademy co-operative events!