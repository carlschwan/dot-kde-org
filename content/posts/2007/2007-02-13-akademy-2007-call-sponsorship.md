---
title: "aKademy 2007 Call for Sponsorship"
date:    2007-02-13
authors:
  - "jriddell"
slug:    akademy-2007-call-sponsorship
comments:
  - subject: "Will KDE 4 be ready before the summit?"
    date: 2007-02-13
    body: "Is there a schedule?"
    author: "david"
  - subject: "Re: Will KDE 4 be ready before the summit?"
    date: 2007-02-13
    body: "Don't count on it. While the basis of KDE 4 seems to take shape quite nicely, there is still a lot of work to be done. It seems unlikely to me that it will be done before the summit."
    author: "Andre"
  - subject: "Re: Will KDE 4 be ready before the summit?"
    date: 2007-02-13
    body: "There are predictions that an alpha will be ready shortly before. I've got a partition waiting!"
    author: "Ben"
  - subject: "Re: Will KDE 4 be ready before the summit?"
    date: 2007-02-13
    body: "Don't hold your breath. First there will be another developer snapshot (most likely next week) and only then the waiting for the alpha begins..."
    author: "superstoned"
  - subject: "Sponsors!"
    date: 2007-02-13
    body: "That's a huuuge chance. Dear Sponsors, if you consider that this years Akademy may be close to the KDE4 release or at least some alpha/beta prerelease: Your name will be connected to this historic event forever.... yeah... ;-)"
    author: "Thomas"
  - subject: "Oh my god"
    date: 2007-02-14
    body: "That is not how its done. The KDE needs a business association which runs it. You don't ask for money like this. And you never ask for money, you ask a company for an alliance or cooperation or participation.\n\nOMG. You amateurs, I love you"
    author: "OMG"
  - subject: "Re: Oh my god"
    date: 2007-02-14
    body: "Amateurs... it's worked great like this for 11 years now :)"
    author: "Troy Unrau"
  - subject: "Re: Oh my god"
    date: 2007-02-14
    body: "???"
    author: "OMG"
  - subject: "Re: Oh my god"
    date: 2007-02-14
    body: "probably new to KDE :o)"
    author: "otherAC"
  - subject: "Re: Oh my god"
    date: 2007-02-14
    body: "> The KDE needs a business association which runs it.\n\nSeems to be a common misconception... Well, anyway... it's simply honest. It's a call for sponsors for an (important) meeting. That's it. Nothing more, nothing less.\nMaybe the marketing people from KDE or the sponsors will likely have an anouncement with the (buzz-)words you mentioned in it, but the people who decide where the money goes prefer an honest \"call for sponsors\". If you give money/other form of assistance, you want to know exactly what it is used for."
    author: "Thomas"
  - subject: "Re: Oh my god"
    date: 2007-02-14
    body: "The origonal post seemed to be in favour of this \"honest\" style of speach ;)\n\nAs am I. Say what you mean, mean what you say!"
    author: "Ben"
  - subject: "Canonical?"
    date: 2007-02-14
    body: "How about Canonical, they will they be really supporting Kubuntu.\nIt's a good chance to prove it."
    author: "kubuntu"
  - subject: "Re: Canonical?"
    date: 2007-02-14
    body: "Kubuntu and OSDL were the two biggest sponsors of last year's event I believe.  Perhaps more importantly, there was good representation by Kubuntu developers.\n\n\n\n"
    author: "Robert Knight"
  - subject: "KDE4"
    date: 2007-02-14
    body: "Hi,\n\nI saw some screenshots of the KDE4 progress and I'm happy to see the icons with circles around them where you can launch applications from. I posted a similar idea a couple of months ago, don't know if anyone saw it but it's nice to see this type of functionality in KDE4 B)\n\nI had another crazy idea, which I posted on my 2 favorite forums, so I'll just provide the links below so anyone interested can take a read. \n\n1. The first is Automatik (had to call it something...). It's an idea for a mechanism where you have a certain folder in your home dir, and this folder gets monitored for changes. When there's a change a script can scan the filename and/or contents and do certain things with it like moving it to another folder.\n\n2. The second is Paperklip, an icon on your desktop that \"holds\" a stack of several files (thumbnails) and you can browse through the thumbnails.\n\nMore complete information here:\n\nhttp://mandrivausers.org/index.php?showtopic=39448&hl=\nhttp://www.suseforums.net/index.php?act=ST&f=49&t=31300&st=0#entry163319\n\nAlso, is there some kind of feature/idea list where you can post this kind of stuff? I feel kind of guilty for posting it here, but hey, it is the KDE forum..."
    author: "Darkelve"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "wishes/bugs => bugs.kde.org\nmockup => kde-look.org"
    author: "gaboo"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "Thanks."
    author: "Darkelve"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "Cool ideas. Not sure if they are usefull the way you present them, tough. The smart folder you propose would need a user to do some clever stuff like scripting and such, not easy. If it's about automatically put downloaded files on a smart location, kget should propose a clever location each time you download a file (eg for documents it should default to ~/documents). And automated actions could be written and executed using workflow.\n\nPaperklip could just be a plasmoid, not even very hard to write, i'd say. But a bit in the future yet...\n\n\nNow I think about it, Automatik-like functionality could be a feature request for workflow - to start a workflow when you drop files in a certain folder..."
    author: "superstoned"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "\"he smart folder you propose would need a user to do some clever stuff like scripting and such, not easy\"\n\nYeah, for that you could have a dialog box for generating regular expressions and associated actions. OSX Tiger already has a dialog box like this I believe, for their Automator program thing.\n\nAbout workflows, anywhere I can read up on what they are? Is that like, a pre-defined chain of events? For example for a podcast, encode the wave file, generate RSS feed and upload both to a webserver. Or are workflows something completely different?"
    author: "Darkelve"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "I think the OP was referring to the worKflow application. \n\nSearch for \"workflow\" in the commit digest: http://commit-digest.org/issues/2006-10-01/\n\n"
    author: "cm"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "Will do."
    author: "Darkelve"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "Hmmm... worKflow looks a lot like Automator, only less polished ATM.\nBut it's a good thing I guess. Looks like it'll be sweet when it's done.\n\nNow what I want to know is, can it use Beagle (or another desktop search indexing thing) search results as input for the worKflow?"
    author: "Darkelve"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "WorKflow is extensible through plugins and it can execute commands, so if one either creates a suitable plugin or if there is a way to do a commandline based Beagle interaction, it should be possible.\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE4"
    date: 2007-02-14
    body: "\"or if there is a way to do a commandline based Beagle interaction\"\n\nYes, I'm pretty sure this is possible. Beagle has command line tools and I'm sure I saw Nat Friedman demo it one time from the command line.\n\nThis is cool. If no one develops it, I might even give it a try myself."
    author: "Darkelve"
---
The organisers of <a href="http://akademy2007.kde.org/">aKademy 2007</a> have put out a <a href="http://akademy2007.kde.org/sponsors/">Call for Sponsorship</a>.  aKademy is the KDE World Summit, this year taking place in Glasgow at the end of June.  Sponsorship is an opportunity to promote your company or product to the developers, users, deployers and consultants who will attend the conference.  It will also provide a marketing avenue for your company to the thousands who read our website and publications.  Most importantly, it gives vital support which ensures that hundreds of KDE contributors can meet together to plan the future of the free desktop.  If your company would like to sponsor aKademy contact us at akademy-sponsoring&#64;kde.org.  If you know any companies in your local area who could also be potential aKademy sponsors please let us know too.
<!--break-->
