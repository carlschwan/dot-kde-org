---
title: "The Road to KDE 4: CMake, a New Build System for KDE"
date:    2007-02-22
authors:
  - "tunrau"
slug:    road-kde-4-cmake-new-build-system-kde
comments:
  - subject: "Great article"
    date: 2007-02-21
    body: "Another stellar article. Thanks Troy!"
    author: "Paul Eggleton"
  - subject: "Nice article!"
    date: 2007-02-21
    body: "I was so preoccupied when I read this. I really enjoyed. Thanks for the nice article Troy! "
    author: "Totya"
  - subject: "nice article, but..."
    date: 2007-02-21
    body: "Some screenshots of the new build system would have been really nice. :-P"
    author: "KDE User"
  - subject: "Re: nice article, but..."
    date: 2007-02-21
    body: "A screenshot of a Konsole building KDE, with colouring, progress reporting and colorgcc?"
    author: "Thiago Macieira"
  - subject: "Re: nice article, but..."
    date: 2007-02-21
    body: "For goodness' sake, can we make it a video so I can see the window properly wobbling while building?"
    author: "Wade Olson"
  - subject: "Re: nice article, but..."
    date: 2007-02-21
    body: "I'd love that, It would be great fun to pretend I can't understand why no one else finds that screenshot exciteing."
    author: "Ben"
  - subject: "Re: nice article, but..."
    date: 2007-02-21
    body: "...for example of the clean colored and readable output compared to the unreadable mess from autotools"
    author: "Josel"
  - subject: "I just happen to have one"
    date: 2007-02-26
    body: "it's fairly old, but: http://chani.ccdevnet.org/sshots/cmake-is-shiny.png\n\nit seems I can only post in plain text, so I can't make a link. how odd."
    author: "Chani"
  - subject: "Thanks"
    date: 2007-02-21
    body: "Great article Troy. Thanks again for your excellent articles."
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Great Article, and a question"
    date: 2007-02-21
    body: "Great Article as useuall.\n\nAnd I was just wondering if there is any plan for a road to kde4 about Kmail? "
    author: "Ben"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-21
    body: "I've seen several blogs about a new mail program called Mailody.  Where does this fit in with KMail?"
    author: "Matt"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-21
    body: "As far as I know, the kmail +/- mailody situation for KDE 4 has not been decided.  However, it will be decided in a friendly manner as all of the developers involved are on extremely good terms :)  When this decision is made, I'll likely write about it (especially if the decision is to move to mailody).\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-22
    body: "currently Mailody only supports imap (unless things have changed recently) which means at this stage at least it is not a complete replacement for kmail."
    author: "Danni Coy"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-22
    body: "True for now.  However so far the code for Mailody appears much cleaner, which gives us hope that we can add all the features kmail has, AND in the process fix a lot of bugs.   Most likely we can also add a few feature requests that people want."
    author: "Henry Miller"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-22
    body: "And get a better name, just needs a K in it. \n\n"
    author: "Ben"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-22
    body: "Please no random K's!\n\nAlthough Mailody isn't that great a name either."
    author: "Tim"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-22
    body: "And what's wrong with Mailody? its a nice querky pun. KMailody or Mailody-K would be a great name for the official client."
    author: "Ben"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-23
    body: "> And what's wrong with Mailody? its a nice querky pun.\n\nyou nearly answered you're own question ;) quirky puns are neat, but also suck for apps. personally i like \"cool names\" for infrastructure (different audience, different use case) but really like straight forward names for apps (generally easier for users).\n\nand rather unfortunately in this case, the name when spoken sounds rather close to this english word:\n\nhttp://www.m-w.com/cgi-bin/dictionary?va=malady\n\nthe feature set and quality is slightly more important, though"
    author: "Aaron J. Seigo"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-23
    body: "You really underestimate users, and the menu. In KDE3.5 it will have something like \"Konversation IRC clinet\". Any user can figure out that E-mail will either be in internet or perhaps office, and by checking both work out that _mail_ody Mail Clinet is probobly an E-mail program.\n\nUser's who can't will never find anyhting without a tutuor so don't bother trying to make it understandable to them, as the saying goes \"If you think your users are idiots, only idiots will use it.\""
    author: "Ben"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-23
    body: "honestly, with the name and the icon, for Mailody is some kind of programs to send music throught internet :/"
    author: "Cyrille Berger"
  - subject: "Re: Great Article, and a question"
    date: 2007-02-23
    body: "With attachments, it can be :) \n\n"
    author: "Ben"
  - subject: "CMakeLists.txt?"
    date: 2007-02-21
    body: "Excellent article, sorry to be pedantic but isn't it CMakeLists.txt?"
    author: "Richard Dale"
  - subject: "Re: CMakeLists.txt?"
    date: 2007-02-21
    body: "Yes... did I make a typo that needs to be fixed?"
    author: "Troy Unrau"
  - subject: "CMAKE flags"
    date: 2007-02-21
    body: "I personally think the:\n\n% cmake -DCMAKE_INSTALL_PREFIX=/foo \\\n    -DCMAKE_BUILD_TYPE=debugfull .\n% make\n# make install\n\nSyntax is clumsy. It's just not how other UNIX tools work. Double dash for words and single dash for letters works for stacks of other GNU utilities, so why not cmake?\n\nPlease, at least the final release of KDE4, consider wrapping cmake with something that supports the legacy syntax. \"prefix\" is much easier to remember than \"DCMAKE_INSTALL_PREFIX\".\n\nJust my 2p."
    author: "Alistair John Strachan"
  - subject: "Re: CMAKE flags"
    date: 2007-02-21
    body: "A couple of points:\n* Remember that it isn't just a unix tool. It's a cross platform tool.\n* that cmake line is passing a single letter (-D), not a word (-DCMAKE_INSTALL_PREFIX). CMAKE_INSTALL_PREFIX is the name of the variable.\n* It might be more useful to contribute a proposed change than to criticise the tool and its existing usage."
    author: "Brad Hards"
  - subject: "Re: CMAKE flags"
    date: 2007-02-21
    body: "Even if it's not only a *nix tool, the syntax is still clumsy. And it sure does not look like the usual Windows command line tools either, so that point is rather moot.\n\nI'd say it's plain stupid, and it's really bad interfacedesign. It looks like they change from a well known and familiar interface/syntax just to be different. Adding the fact that the other way is easier to read/remember and shorter to type, usability wise that's make really no sense at all. "
    author: "Morty"
  - subject: "Re: CMAKE flags"
    date: 2007-02-21
    body: "Yeah, but in all honesty no build system is perfect.  The syntax is a little clumsier than ./configure was, but the benefits far outweight this one problem.\n\nActually, on my system I took that command and created a bash alias, so I now just type 'cmakekde /path/to/source' and it automatically fills in the variables.  Of course, if I wanted to redefine the variables (to turn off debug, for example), I'd now have to edit my .bashrc file."
    author: "Troy Unrau"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "Have you actually used it?\n\nAlso, see previous comment on providing potential fixes instead of complaining.\n\nThere is another alternative too - use ccmake.\nI often use:\nccmake /path/to/source\nand then you can work through the various options, selecting to build unit tests, checking output and so on. Then just generate."
    author: "Brad Hards"
  - subject: "Re: CMAKE flags"
    date: 2007-09-03
    body: "like said --prefix=  --libdir= is smarter and not just easier but makes more semce. using ccmake is ok I guess for people, that love to just do make make install . not packaging it up etc . But the fact is to ./configure -help and you can see all the <options>. Where is the help for this in cmake to know what replaces --prefix= . took me a lot of reading to find out how stupiod it is to do all this extra work . take wath made more sence and easy to remmeber etc . the list goes on. CFLAGS= CXXFLAGS= CC and how about if I'm running my x86-64 kernel and I could add --build=arch-blah-linux(-gnu for some) so it uses the right arch when compiling . way to many reason not to like cmake . it horrible \n6hrs of googling to find out how to replace those and I only have figured out 3 prefix and maybe libdir and include but not arch at all not --sysconfdir=/etc\n-localstatedir=/var\nmaybe these 3 memeory or not those are not easier at all and take way to much to get used to build script or not . 1200+ apps I compiel and very few use cmake . so glad for the many that don't . \nbeen asking in kde and no one knwo of any where that shows what to replace any of them with , other than the one for prefix. Even when you find out look at it you can't be that blind to how it made it wprse not better.\n-DCMAKE_INSTALL_PREFIX=/usr \\\n      -DLIB_INSTALL_DIR=/usr/lib \\\n\n      -DCMAKE_ADD_DEFINITIONS=\"$SLKCFLAGS\" \\\ntrying to compile 1 stinking lib for an app that uses this errr.\nscons jam all suck . Control and what they use to replace are horrible to find out and they use the same crap.  \n"
    author: "oneforall"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "The syntax is similar to the gcc syntax.   I would expect any KDE developer would be used to commands like \"gcc -dFOO -dBAR -i../include \" and so on for 5 lines.   Most of the code I've compiled ends up with command lines like that.   Of course make takes care of it, so you don't have to pay attention too often."
    author: "Hank Miller"
  - subject: "Re: CMAKE flags"
    date: 2007-02-23
    body: "To the dude complaining about the flags...\n\nFirst get a grip. THIS IS A CROSS-PLATFORM TOOL. What that means is that 'tools' that use other platforms will be using it. No matter *how* the switches were set up it would either be not windows-like or not unix-like so tough, get over it.\n\nBut the more important fact is that if you are seriously doing a large cross-platform project with lots of folks from different platforms, the differences do indeed wind up 'wrapped' on each platform so CMake (or whatever you are using; we use both that and Bakefile on a daily basis) feels like a batch file on Windows (bleh) and it acts like make on unix. This is the way things sort out in the real world.\n\nThe other thing to really get a grip on though is if you are doing cross-platform development of any complexity, you are will have *far* more critical things to worry about than the syntactical style of the make system.\n\nThis too is a freebie from the Real World (tm).\n\nHaving used CMake for years (well, two) and Bakefile for nearly as long they both have their strengths and weaknesses. On unix I can set up a new CMake project faster than I can in Bakefile but bakefile gives me some nice configuration options that CMake does not, not the least of which is a more standard project file format (XML vs whatever CMake is).\n\nBut both are excellent projects and KDE would do well by either one. I know it has saved us TONs of time and work...\n\nCheers.."
    author: "JeffCobb"
  - subject: "Re: CMAKE flags"
    date: 2007-02-24
    body: "Nobody said anything about CMAKE being bad. This isn't a parade against CMAKE per se.\n\nI simply believe that it uses bizarre flags for no really good technical reason. I even suggested that CMAKE need not be changed, just wrap it.\n\nThe point is, it's fine saying to people you've switched from autotools -- I don't think _anybody_ really knows how they work -- but at least try to reduce the learning curve involved with using CMAKE, or fix CMAKE.\n\nOther \"cross platform tools\", like Trolltech's offerings, don't suck like this. It's absolutely no excuse."
    author: "Alistair John Strachan"
  - subject: "Re: CMAKE flags"
    date: 2007-09-03
    body: "well considering I crosscompiled with make I'd say it is allready and cross platform too. Plus I cmpile 32bit on my 64it os that I yes crosscompiled. cmake is not easier at all its just made compiling worse . --prefix= is a heck of a lot smarter and simpler and makes more sence that -DCAMKE_....forgot the rest and no wonder. ./configure -help . I don't think your blind to what thats a 1000 times betrer for cmake umm O.o no idea spent hrs allready trying to find out  whats it uses to replace --prefix= --libdir= . got those and man they are sick . still haven't founf out about CFLAGS CXXFLAGS CC and when i need to do LDFLAGS PKG_CONFIG_PATH  arch-blah-lunux(-gnu for some distros) to make sure it uses um eg; i486 on my 64bit OS . Thats only a few of the horribleness of cmake and to make packages . I patch and edited configure to remove hard coded stuff like /usr/lib to /usr/lib64 etc. But cmake makes them look more peacefull an much better"
    author: "oneforall"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "For KDE4, you can add:\n\nfunction cmakekde { cmake -DCMAKE_INSTALL_PREFIX=$KDEDIR \\\n-DCMAKE_BUILD_TYPE=debugfull $@ && make VERBOSE=1 && make install; }\n## Uncomment if dbus doesn't work\n#alias dbusstart=\"eval `PATH=$DBUSDIR/bin \\\n#$DBUSDIR/bin/dbus-launch --auto-syntax`\"\n\nto your .bashrc, then compiling KDE is even easier.  Then a simple `cmakekde ../kdegames` (for example) from the kdegames-build directory. CMake greatly reduces the barriers to entry for developing KDE, and lets me focus on coding rather than on an incomprehensible (imo) auto* build system.\n\nSee http://developernew.kde.org/Getting_Started/Build/Unstable_Version for details.\n\nKDE is *far* easier to compile than it used to be.  Long live CMake!\n\nRegards,\n\nMark"
    author: "Mark Taff"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "Locally I have a patch for cmake which (almost) does this, it let's you define \"shortcuts\" for the original cmake variable switches (e.g. you can put something like \"CMAKE_INSTALL_PREFIX = prefix\" in some config file and then cmake will translate \"--prefix\" to \"-DCMAKE_INSTALL_PREFIX=\".\nBut it's not done yet, it does translate the things you want to have translated but breaks all other options :-P\n\nBut I hope to be able to make it work before KDE 4.0\n\nAlex\n"
    author: "Alex"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "So if CMAKE becomes popular I could continue installing with ./configure make make-install and never have to worry about what build system is used?"
    author: "Ben"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "It'd be:\ncmake .\nmake\nmake install"
    author: "Pau Garcia i Quiles"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "Alex, this is really great.  Not that the barrier to entry to KDE is very high to begin with, but this would lower it even more...\n\nGreat idea!"
    author: "Troy Unrau"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "Agreed. I have similar problems when compiling a Qt or Python app. I'm suddenly a compile-newbie again. How about providing a ./configure wrapper script? It could translate the --prefix stuff to cmake commands, or at least inform the users about the correct syntax. There are more projects that provide a configure wrapper for user-friendliness reasons, KDE doesn't have to be an exception here."
    author: "Diederik van der Boor"
  - subject: "Re: CMAKE flags"
    date: 2007-02-22
    body: "related bugreport;\nhttp://www.cmake.org/Bug/bug.php?op=show&bugid=3293&pos=0"
    author: "Thomas Zander"
  - subject: "There are auto-updates?"
    date: 2007-02-21
    body: "Hi all,\n\nI'm quite new to KDE and I wanted to know if there are auto-updates. I mean, if I use KDE 4.01, and then appears KDE 4.02, will it be notified and my system updated? If such feature exists, does it work for all KDE apps?\n\nSorry for my ignorance, but this is an important topic for me.\n\n "
    author: "David"
  - subject: "Re: There are auto-updates?"
    date: 2007-02-21
    body: "Your question would more appropriately directed to users of a distro of your choice. While KDE will not tell you of an updated release, they're not really in a position to. That would more properly be a function of a distros package management system."
    author: "Sean Brown"
  - subject: "Re: There are auto-updates?"
    date: 2007-02-21
    body: "So that would be a \"no\" to the original question.\n\nKDE does not have an auto-update feature like the one found in recent Mozilla Firefoxes.\n\nKDE follows the tradition of letting the Linux distribution (SuSE, Debian, Gentoo, Fedora etc.) decide how to handle updates."
    author: "Anders"
  - subject: "Re: There are auto-updates?"
    date: 2007-02-22
    body: "Offtopic but interesting idea out of this thread:\n\n * mozilla autoupdates are useful on windows\n * kde4 could be available for windows\n * kde usually handles updates via linux distributions\n\n -> there needs to be some sort of free software distribution mechanism on windows, like apt.\n\noh - somebody already thought of that: http://www.pcxperience.org/ideas/AptWindows.html"
    author: "nidi"
  - subject: "Re: There are auto-updates?"
    date: 2007-02-22
    body: "actually there is some work going on on a Windows KDE installer/manager, which might indeed do this in some way."
    author: "superstoned"
  - subject: "Re: There are auto-updates?"
    date: 2007-02-22
    body: "As others pointed already out there is no need for this on Linux/Unix as we already have such systems which do exactly what you want for every application you have installed.\n\nOn Windows for example I am always deeply annoyed when 5 programs tell me after system startup \"Hello user there is a new hot cool update for me, I am so cute please update me.\" One central application where different vendors can queue their software repositories and which would allow you to update the whole system with one mouse click would greatly enhance security and usability on Windows. But honestly I am really lucky that MS did spend billions of Dollars but still did forget even the most basic things and that they still love their unstable and slow browser based update system. So Windows including the newest version actually looks like stoneage to me. :p\n\nBut beside that there even is a KDE program called Get-hot-new-stuff (see: http://www.kstuff.org/ ). It allows you to share and search for new application data for your system (for example new document templates, new wallpapers, new game levels...)."
    author: "Arnomane"
  - subject: "Re: There are auto-updates?"
    date: 2007-09-29
    body: "> On Windows for example I am always deeply annoyed when 5 programs tell me after system startup \"Hello user there is a new hot cool update for me, I am so cute please update me.\" One central application where different vendors can queue their software repositories and which would allow you to update the whole system with one mouse click would greatly enhance security and usability on Windows. But honestly I am really lucky that MS did spend billions of Dollars but still did forget even the most basic things and that they still love their unstable and slow browser based update system. So Windows including the newest version actually looks like stoneage to me. :p\n\nAgreed. After leaving my Windows off for a month, it is true agony when I start it up again."
    author: "riddle"
  - subject: "Re: There are auto-updates?"
    date: 2007-02-22
    body: "A distro like kubuntu has a way to do this for all of your system with a nice system-tray item stating there are things to update.  No need for KDE to reinvent that wheel."
    author: "Thomas Zander"
  - subject: "Re: There are auto-updates?"
    date: 2007-02-23
    body: "Updating packages (including the various pieces that make up KDE) is more the responsibility of the operating system on which KDE is running (at least on *nix operating systems).\n\nHaving said that, there are various KDE applications which act as front-ends to the various OS-specific package systems.  These provide eg. icons in your tray that pop up when updates are available, offering some way to perform the upgrade.  Here are some examples to get you started:\n\n(OS - Tool - URL)\nKubuntu (and Debian?) - Adept - http://kde-apps.org/content/show.php?content=44909\nGentoo - Kuroo - http://kuroo.org/\nFreeBSD - Barry - http://www.student.uni-oldenburg.de/frerich.raabe/barry/\nFreeBSD - KPorts - http://www.kde-apps.org/content/show.php?content=24579\n\nIt's not a bad situation because these tools update everything on the system (and sometimes the system itself, too), not just KDE.  The few I have personal experience with have been easy to use.  For example I've got my mum running Kubuntu (which uses Adept) - she's essentially computer illiterate but has no problem keeping the system up-to-date.\n"
    author: "macro"
  - subject: "Re: There are auto-updates?"
    date: 2007-03-03
    body: "This is indeed something that your distribution should do for you. Personally i have Gentoo and just run \"emerge --sync&&emerge -uDNf world&&emerge -uDNtav world\" once in a while. (I do not even have to type it every time, I just press Ctrl+R and type \"sync\", then the command comes back.) But if you want notifications directly from KDE, kde-announce is what you are looking for (see [http://www.kde.org/mailinglists/])."
    author: "Erik"
  - subject: "Richard Dale on CMake"
    date: 2007-02-22
    body: "Just thought I'd add the following link, which points to a blog post by Richard Dale about some of his experiences using CMake to build some other non-KDE projects.  It's a good read, if you don't mind the technical details :)\n\nhttp://www.kdedevelopers.org/node/2685"
    author: "Troy Unrau"
  - subject: "Perfect!"
    date: 2007-02-22
    body: "This is an amazing job! Thank you for the time... This article is just perfect!\nAgain, thank you.\nRegards,"
    author: "Rodrigo F Toso"
  - subject: "offtopic"
    date: 2007-02-22
    body: "i usualy do push-ups while my project compiles."
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: offtopic"
    date: 2007-02-22
    body: "That's good for staying in shape!\n\nHowever, I'm sorry, but we have cut down the time you'll have for this activity. KDE 4 compiles much faster now with CMake :-)\n\n(And there's still potential for being even faster)"
    author: "Thiago Macieira"
  - subject: "Re: offtopic"
    date: 2007-02-22
    body: "Well, that sucks, then. He should sue KDE, when he gains weight and gets the associated diseases...\n\n:D"
    author: "superstoned"
  - subject: "SCons vs CMake"
    date: 2007-02-22
    body: "I've now used both for developing smallish projects and I can see that they are very similar in capabilities now. They both have easy syntax (relative to automake), a bunch of tools for doing different build tasks, and sane dependency specification. However, it seems like CMake is a bit more inflexible if you want to add a custom build task (like running unittests automatically) into the build process because scons is a dsl vs. cmake which is a custom language.\n\nLikewise, scons has little/no support for configuration and reconfiguring the build process for the users machine which is a big negitive. waf tried to address this but I guess it was too little, too late.\n\nSo I guess it's a bit of a toss up. CMake is ok for kde4 need's but I really hope that the kde community reexamines either scons or rake (or another dsl  build system) for kde5.\n\nCheers\nBen"
    author: "Ben"
  - subject: "Re: SCons vs CMake"
    date: 2007-02-22
    body: "Hey, another Ben :) \n\n"
    author: "Ben"
  - subject: "Re: SCons vs CMake"
    date: 2007-09-03
    body: "How can you call either one of those easier for the accual compile. they both are horrible for that. Both are hard to find out what even they use to replace --prefix= (-DCMAKE_INSTALL_PREFIX) look at that the first is a better syntax by far . makes it a lot easier to remmeber and use and the rest to follow. --libdir= --mandir docdir etc sdo easy . ./configure --help for all the options . So far I have spent hrs of googling asking in kde  kde-devel with no help at all for someting they have stuck use with. So where is the replacement for ./configure --help thats probably longgggggggg winded too. No control over where things go and if there is nothign helpfull like ./configure --help . how can I do the one for the arch I want to build ? --build=arch-blah-linux . So its not building x86_64 when I want 32bit etc . cmake even for x86_64 still puts arch-unknown-linux when I want it to put the right one. So manty reason I can't stand this cmake crap \nglad out of the 1200+ apps I compile there is only 1 or 2 that use this horrible crap "
    author: "oneforall"
  - subject: "Re: SCons vs CMake"
    date: 2007-12-24
    body: "Have you ever tried CCMAKE? Just run ccmake . to get a list of options. "
    author: "Michael"
  - subject: "Re: SCons vs CMake"
    date: 2008-03-23
    body: "yes I have looked at that too and it doesn't list them in the way you can see to use them cmake .  <here> . \ncmake is a poor horrid auto tool . Yes with physfs it s a poor done CMakeLists.txt iirc that does the searches and makes the cmake_install.cmake. I dunno maybe I have that backwards. but its a horrible tool for paths and hard coding and making it way harder to figure out how to patch  or sed a line than it is with configure. The hard coded path for physfs I had to fix it after like a sed for libtool . which I never like it being after it should be fixed before. With libtool I figured out how ro fix it with sed in the configure. I didn't have to know how to add code in there like with the physfs one and others that are the same. nywasy I spent too long compared to using configure make etc and it still is not easy to use at all. Mostly because of it not as flexable it geared to things must be installed here and no where else attitude. all capps and long definition that just in that sense are not an improvement to do thing the long scenic route. --libdir= nice and simole easy to remmebr than -DCMAKE_INSTALL_LIB= or is that one -DLIB_INSTALL_DIR= so many that need to addvertize CMAKE and INSTALL no reason it couldn't still be simple -Dlibdir -Dprefix= . Anyways it never took me long to be comfortable with configure and figure out how to use it for most things. Glad that most apps don't use this crap and hope they don't . Even in kde-devl I posted a lot about poor with paths and the fact on a 64bit if you want to compile a -m32 you can't even use or find out how to tell it the frigging arch --build=arch-vendor-linux-(opt gnu) . CFLAGS=\"-m32 (-O2 -fPIC etc if you want )\"  this can be used with cmake too but the arch every one says the same to goto the cross compile page . well with configure I don't need to go back to cross compiling and cross compile tools.  --build= host= but mostly just -build= works =i486-vendor-linux  =i686-pc-linux etc It a frutrating auto compile tool at most . people that just do cmake and no option have no clue any more than some one that does that with configure . \n"
    author: "oneforall"
  - subject: "Portability"
    date: 2007-02-22
    body: "This seems like it's just another attempt to make kde look more interesting for devs on Windows and OSX really. I personally don't get all the craze of kde being able to run on Windows or OSX. QT4 is basically only advertised as being a cross platform toolkit which brings kde to Windows.\nHow about concentrating about the main (and most important) platform? The \"free software\" platform. I don't hear much of that lately.\n"
    author: "Ben"
  - subject: "Re: Portability"
    date: 2007-02-22
    body: "If you read the aticle you'll notice that this makes developing KDE on Linux/BSD much easier. It also makes it much possible to build it on Mac and Windwos but even if you don't give a damm about the non-free platforms its still a major improvement over KDE3\n\nPersonally I havn't herd much about mac/windows relitive to Linux."
    author: "Ben"
  - subject: "Re: Portability"
    date: 2007-02-22
    body: "Well, you will note some things in the article that specifically relate to the free platforms.  Specifically, that CMake can autogenerate kdevelop project files is a big plus, as kdevelop is one of the best IDE's available for open platforms.\n\nBut KDE's support for other platforms is sort of a byproduct of the good code that exists for *nix.  We aren't specifically designing KDE to suit Windows or Mac users so much as the underlying system makes the transition easier.  99% of all KDE developers are linux or bsd people, and even those that are working on the Mac or Windows ports are still free platform users.\n\n/me wonders if you complain about firefox being available on windows as well, even though it has it's roots in *nix."
    author: "Troy Unrau"
  - subject: "Re: Portability"
    date: 2007-02-23
    body: "I'm not against porting applications to other platforms at all actually.\nI just always thought that KDE was about empowering the free software platform. \n\nWith the emphasis on cross platform compatibility usually come compromises you have to make (especially if you have no control over the underlying system - i.e. on proprietary operating systems). That will/could directly affect the usability on the main platform (e.g. like when you realize why the default web shortcut for google in konqueror isn't just \"g:\". bad example maybe because it's so minor, but hey ;-).\nIt also means there will be need for even more maintenance and developer time to support other (less important) platforms (\"[PATCH] to make *thing* work on Windows\"). As far as I know KDE is already lacking manpower.\n\nMy reply wasn't specifically in the regard of cmake but more in general because I feel KDE4 is being advertised as being the big Cross Platform Desktop Opportunity instead of a Free Software Desktop.\n\nWell, maybe it's just me. ;-)"
    author: "BenAgain"
  - subject: "Re: Portability"
    date: 2007-02-23
    body: "the free software platform is and always will be our prime concern. that's why we provide an entire workspace shell for it, which we are putting even more effort into for kde4.\n\nthe win/mac efforts are left up to people interested in win/mac, which is 95% not people who were already working and who continue to work on free platforms."
    author: "Aaron J. Seigo"
  - subject: "Re: Portability"
    date: 2007-02-23
    body: "I just read http://aseigo.blogspot.com/2004/12/how-to-kill-open-source-on-desktop.html.\nAre you still feeling that way now since cross platform KDE (applications) is becoming real?"
    author: "BenAgain"
  - subject: "Re: Portability"
    date: 2008-10-16
    body: "I actually thought the point of KDE (and open-source software in general) was the freedom. So, then, why are so many people so adamant about forcing people to use Linux? Isn't that basically blasphemy, or self-contradiction?"
    author: "Anonymous"
  - subject: "KDE4 et al"
    date: 2007-02-22
    body: "Is  there a www.kde4.org (someone has already got that) type site where all the information about KDE4 is disseminated? \nThere is a momentum building throughout for information about it but at the moment you need to be \"in the know\" of what the constituent parts are going to be in  order to find out information. The ideas are fantastic when read about in their isolated silos but it would be good to see them all together showin in a framework of a single site/link from www.kde.org.  People love roadmaps and technology maps.\nA lot of the names like Plasma, Okular, Decibel don't really tell you what they are about - (sometimes you can guess), how they fit into the total structure, if they depend one each other etc.  They all sort of sound like something a marketing driod has thought up, at least with the \"K\" prefix, you had an idea it was KDE related. (i expect this has all been hashed out already). \nI could go on but I've a train to catch....\n\nThanks to all the developers for a great DE and the apps to match."
    author: "Ian"
  - subject: "Re: KDE4 et al"
    date: 2007-02-22
    body: "There is no such site yet.  Right now, the best way to find out is to build from sources, or try the technology preview releases (there should be one happening this week sometime) which occasionally get packaged by some distros...\n\nWhen KDE 4 is eventually released, the official www.kde.org website will be updated to KDE 4 content.\n\nBut you're right, it'd be nice if there was a website that linked to all of the up-and-coming KDE 4 technology websites, as most of these projects already have a functioning website.  The problem with such a suggestion (like all free software) is that there needs to be someone interested in doing such a project, and then be willing to maintain the content to ensure it's all up date all the time.  That's difficult when KDE 4 is such a moving target..."
    author: "Troy Unrau"
  - subject: "Awesome"
    date: 2007-02-23
    body: "This whole series of articles has been fantastic to read. Thanks Troy!"
    author: "taj"
  - subject: "very pitty"
    date: 2007-02-23
    body: "Pity, \"scons\" was abandoned in favour of an antiquated and non-intuitive \"cmake\" system. Very pitty. Just laxly?\n"
    author: "Gleb"
  - subject: "Why not use qmake ?"
    date: 2007-02-23
    body: "In our project http://pvbrowser.org we use qmake for building our (Qt-)applications.\n\nWhy didn't you use it to build KDE ?\nYou could have extendet it if you needed more custom build steps.\nIn the past i have added custom build steps for corba idl and oracles embedded SQL for example.\n\nThe advantage would be,\nthat you use the original build system trolltech is using anyway.\nAnd qmake is very simple and clear.\nAnd it is portable also.\n\nPS: We always felt uncomfortable with autotools also. And have trown it away a long time ago."
    author: "Rainer"
  - subject: "Re: Why not use qmake ?"
    date: 2007-02-24
    body: "I had the same thought the moment I first heard about KDE dumping autotools (thank you, thank you, a million times thank you!).  Is there a critical weakness in qmake that cmake addresses better?\n\nI've returned to using qmake for my multipart projects (I detoured into KDevelop for a couple years before giving up on it) for a few months now, and I find it to be a capable build system.  It doesn't generate an install target for some strange reason, but maybe I'm just missing a setting.\n\n"
    author: "Tony O'Bryan"
  - subject: "Could a company have made this transition?"
    date: 2007-02-23
    body: "Hi,\n\nDon't bother about the eye candy I prefer thought candy and this article is full of it. It deserves a much wider audience than the Linux community. Why? Because it describes a process that leads many commercial companies to go bust or get in big trouble. It describes how power can be exercised without (it appears) power broking. No doubt there are implications for the democratic process itself but I won't try to expand on that."
    author: "Norman"
  - subject: "Re: Could a company have made this transition?"
    date: 2007-02-23
    body: "huh?\n\nI thought it described an easier way to build software, where dose power brokeing and democracy come in?"
    author: "Ben"
  - subject: "Re: Could a company have made this transition?"
    date: 2007-02-23
    body: "I think he's referring to the whole 'he who codes decides' bit.  Sometimes KDE is referred to as a meritocracy, rather than a democracy.  If your work is good, people accept it."
    author: "Troy Unrau"
  - subject: "Re: Could a company have made this transition?"
    date: 2007-02-23
    body: "Ah, I didn't think of it since that was just a casual mention in the article.\n\nP.S. Meritocracy's are the worlds best *ocracy"
    author: "Ben"
  - subject: "Re: Could a company have made this transition?"
    date: 2007-02-23
    body: "Ours did. I took them from MSDEV/make to CMake and then to Bakefiles. Now everyone gets something that feels pretty native to them and having a simple to understand cross platform make system has been our saviour (doing cross-platform multimedia frameworks)...usually all it takes is someone who has learned the make system well enough to demo to each set of proponents for each platform how it will save them time. Usually the argument that works is it keeps windows programmers from being frightened at the command line (as a properly-configured cmake or bakefile arrangement is a mouse-click to a Win32 dude/dudette) and it keeps unix folks from having to deal with dev studio at ALL. With this system, I can create an entire project under linux and (assuming I have my act together at the code level, something beyond the scope of the article), mail it to a windows dude who then (only the first time) runs 'bakefile -f msdevprj myproject.bkl and the result is a dev studio 6 project. I do 'bakefile -f gnu myproject.bkl and I have a nice unixy make file. Works for Mac OS/X too. IOW, to paraphrase an old rock song, the coders on each platform don't always get what they want, but they always get what they need....\n\n"
    author: "JeffCobb"
  - subject: "The Road to KDE4"
    date: 2007-02-23
    body: "I'm writing a tutorial for new Linux users and I have a question about KDE 3.5.x: if the Kwin window manager handles the drawing of window decorations, window contents, etc. , then does Kicker handle the drawing of all the components and parts of kicker? And for the desktop (wallpaper etc.) that would be Kdesktop? What about KDE4?\n\nThen I have a request for the upcoming \"Oxygen\" article:\n\ncan you explain a bit how Oxygen will try to overcome the problems which exist with the current icon/iconset approach:\n\n- When switching between iconsets, it is possible that you 'loose' some icons because alternatives do not exist in the iconset you are switching too. Mostly you'll get a really bland/ugly icon instead\n\n- In KDE's \"icon overview\" screen, will it be possible to group the icons per theme? Now they are all mixed up and this may give problems when looking for an extra icon for an application, and you want to know if the icon is part of the current iconset you are using\n\n- Are there some kind of icon 'preferences', so that e.g. you can make another icon the default instead of the current default icon. For example, I set the folders in /home/ to use a grey folder icon. But every time I or a program creates a new icon, as well in several Konqueror views, I get a blue icon. Will it be easier to set preferences of this kind in KDE4?\n\n"
    author: "Darkelve"
  - subject: "Re: The Road to KDE4"
    date: 2007-02-23
    body: "These are a lot of seemingly random questions :P\n\nKicker looks after most of it's own drawing, as does kdesktop, but they still use kwin to control their position on screen, whether they live in the background (like kdesktop), and that windows can't overlap kicker, for example.\n\nWith regards to the Oxygen stuff, you'll have to wait for that article... it's probably two weeks away.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: The Road to KDE4"
    date: 2007-02-23
    body: "\"These are a lot of seemingly random questions :P\"\n\nThat is probably because I am talking about two different things here, and because I do not yet understand all of this very well myself ;)\n\nFor the tutorial, I'm trying to explain to people new to Linux what makes their Linux/KDE system tick; GUI elements that are drawn on their screen being one of them.\n\n\"Kicker looks after most of it's own drawing, as does kdesktop, but they still use kwin to control their position on screen, whether they live in the background (like kdesktop), and that windows can't overlap kicker, for example.\"\n\nMuch appreciated :) Can I use that quote above for the tutorial, and do you want me to credit you for it?\n\n\n\"With regards to the Oxygen stuff, you'll have to wait for that article... it's probably two weeks away.\"\n\nWell, I don't mind waiting. I just wanted to know if any improvements are being made to the iconsets and especially the iconset tools (KDE Control Center/Personal Settings), seemed to me to be about Oxygen. Or is Oxygen only about the visual appeal and completeness of the icons?\n\nThanks for the friendly reply.\n"
    author: "Darkelve"
  - subject: "Re: The Road to KDE4"
    date: 2007-02-25
    body: "Feel free to quote or paraphrase...\n\nOxygen is more about the visual style and making sure it's consistent across all of KDE.  It will not be missing any icons when KDE 4 ships.  What will likely happen is that when you choose a non-Oxygen icon theme, and this theme is missing an icon, it'll just default to the Oxygen icon as necessary.\n\nAbout fine tuning icon control, someone would have to write a kcontrol module to do that."
    author: "Troy Unrau"
  - subject: "Typo..."
    date: 2007-02-23
    body: "milage should be mileage.\n\nGood article! I haven't used KDE in a while, but it is nice to get an overview of the build tools change.  Thanks!"
    author: "James"
---
When a project with the size and scope of KDE gets to be as big as it is, sometimes changing a decision established almost a decade earlier is very difficult. KDE has relied on autotools to build most of the project since its inception, but for the last year, KDE 4 has been building with <a href="http://cmake.org/">CMake</a>, a newer build system that is rapidly becoming a heavyweight contender in the world of buildsystems. Read on for more...



<!--break-->
<p>This article focuses on CMake, which is not actually part of KDE, but is developed independently by <a href="http://www.kitware.com/">Kitware</a> and a team of open source developers. It is released under a BSD-style <a href="http://cmake.org/HTML/Copyright.html">license</a>. I can't really show much in the way of screenshots for a build system, but I will do my best to explain why CMake has been a welcome change to the KDE development process.</p>

<p>But before we talk about CMake, a little history lesson about KDE and autotools. KDE has used Qt since the beginning, and one of the nice features of Qt is the meta-object compiler (moc). Autotools had to be extended to support using moc as a pre-processor for most of KDE's header files. That was only the beginning though, as KDE developers wrote the ground-breaking DCOP communication protocol which also had the side effect of adding more types of files that had to be autogenerated during the build process. The developers added document compilers, tools that automatically handled translations, and tools that compiled configuration file classes from XML: Qt's User Interface compiler (for .ui files) was born and needed to be supported by the build system. They also needed to add a whole suite of new configure checks, options, etc. to support all of the features that KDE was using. The KDE build system was evolving into a very complex animal, and using autotools did not scale well.</p>

<p>By the KDE 3 series, there were only a select handful of elite build gurus who could understand the whole of KDE's build system. KDE developers had trouble even moving things from one folder to another without having to spend hours tweaking build files to get it to compile again. And in some cases, starting an independent KDE project meant shipping up to 500 KiB of autotools support files for even a simple 'hello world' type application.</p>

<p>Something obviously had to be done for KDE 4 to improve this situation, and at an Akademy 2005 meeting, it was decided to explore other build system options. Initially, SCons was prototyped for building KDE, but it was slow going, and after several months of work it still did not handle kdelibs well enough to dump autotools. One of the bigger problems with SCons was its lack of modularity.</p>

<p>So, as is often the case with KDE, he who codes, decides: Alexander Neundorf stepped in and did the initial port to CMake which went very smoothly, and he did so with the support of the CMake developers. It took only a few weeks to get much of KDE building smoothly using CMake, and autotools was finally dumped!</p>

<p>The CMake developers have been very supportive of KDE's transition. They even joined KDE's buildsystem mailing list to help out. This relationship has been beneficial to both projects. When KDE started to really stress the capabilities of the still-maturing CMake system, KDE developers started a dialogue with them suggesting improvements - and they were happy for the feedback. As a result, CMake has seen improvements that are beneficial to all projects using the buildsystem.</p>

<p>Our working relationship aside, CMake has greatly improved the process of building KDE. Projects using CMake take less time to get started, since there is less time spent fighting with the build system. One KDE developer says, "CMake doesn't make you want to shoot yourself with a nailgun when building your project anymore."</p>

<p>The way CMake works is by processing an easy to read file called 'CMakeLists.txt' which the developer adds to their source folder. When you run the 'cmake' command, it looks for this file, and based on its contents, it generates Makefiles (on UNIX), or using a command-line switch it can generate XCode project files so you can build Mac applications using OS X's XCode developer tools, or using yet another command, it can even generate MSVC projects from your sources. One of the best KDE-related features of CMake is that it can automatically generate <a href="http://www.kdevelop.org/">KDevelop</a> project files based on the same 'CMakeLists.txt' used to generate the Makefiles.</p>

<p>KDE's code (with a few exceptions) was already pretty portable, however it was nearly impossible to build KDE on systems like Windows, where autotools was not as well suited. Now much of KDE's improved platform support is thanks to the ease of building on these systems (And of course, Qt being available as GPL on these systems).</p>

<p>In KDE 3.x, building KDE using the recommended methods looked something like this:</p>
<pre>
% ./configure --prefix=/foo --enable-debug
% make
# make install
</pre>
<p>As you can tell, this is pretty standard autotools-style building, except that the scripts which controlled the build process are pretty hard to understand. </p>

<p>With CMake, there are some syntax changes (which may actually be less obvious than the old, familiar configure switches), but the commands are mostly the same.</p>
<pre>
% cmake -DCMAKE_INSTALL_PREFIX=/foo \
    -DCMAKE_BUILD_TYPE=debugfull .
% make
# make install
</pre>
<p>This syntax does not look like a huge improvement, but looks can be deceiving.</p>

<p>CMake searches for dependencies several times faster than the './configure' process did. CMake builds KDE 4's kdelibs about 40% faster than autotools build KDE 3.5.6's kdelibs, mostly due to a lack of libtool in the toolchain for CMake. The CMake toolchain (for UNIX) looks something like: <em>cmake + make</em>, whereas the autotools chain for KDE 3.5.6 looked something like: <em>automake + autoconf + libtool + make + sh + perl + m4</em>.</p>

<p>I'm going to rely on some of my own personal experiences here to help explain how much easier CMake is to use: Aaron Seigo asked for some help to port some of kdesktop's components into krunner so that kdesktop could finally be killed off in the KDE 4 tree. This is the sort of task that I would not have attempted to tackle in the KDE 3.x tree, not because the code was any harder, but because the build system was so difficult to deal with. In all likelihood, I would not have volunteered for such a task for KDE 3, but with KDE 4 and CMake, after moving the code around and doing some class renaming and such, I smoothly inserted the ported code into the build system, modifying only two or three lines of krunner's CMake build files. It built after a few minutes, and linked and installed. I was so impressed that I've kept contributing porting help. In KDE 3, I probably would have been stumped by the build system (not the actual KDE code) and given up, not bothering to commit my code to KDE SVN.</p>

<p>This only goes to show that the build gurus of KDE can worry a lot less these days, as anyone can get their projects up and running. Your milage may vary, but many developers have expressed similar feelings when dealing with the 'CMakeLists.txt' syntax vs. 'autotools'. However, almost all of the KDE developers would now be considered CMake newbies. The CMake developers have been personally involved in ensuring that KDE's transition is as smooth as possible.</p>

<p>The switch to CMake isn't the first time KDE has changed one of the technologies central to its development. When KDE was younger, we used CVS to control access to the sources. Maintenance of the CVS server was not scaling well as KDE was growing, amassing enormous amounts of code dating back to its original commits. Subversion (SVN) offerred a promising new revision control system that was a better fit for what KDE needed to accomplish, and was easier on our servers. At the time, however, no project the size of KDE had migrated to SVN, and it was a real test for the Subversion software. But KDE and SVN have been a pretty good fit, and since KDE migrated, many other projects have since followed in its footsteps.</p>

<p>KDE's use of CMake has helped to raise the public profile of the buildsystem, much like how the switch to Subversion helped their profile. Other projects have been switching to CMake too, including (but not limited to): Scribus, Rosegarden (switched from SCons), PlPlot, ChickenScheme, and more. There is also work being done to support building KDE 3.x programs using CMake (KPilot for KDE 3 can build using CMake, for example). Projects looking for an easier way to build whilst adding support for more platforms should give CMake a try. Adding a 'CMakeLists.txt' file to your sources does not interfere with your project's existing build system and can give you a good overview of what CMake can do for you. And, like KDE, if there's something it cannot handle, the CMake team is very open to improvements.</p>

<p>Some links that may be useful for those interested in more information:</p><ul><li><a href="http://lists.kde.org/?l=kde-core-devel&amp;m=113734805302055&amp;w=2">The original post by Alex about the port to CMake</a>, now just over one year old.</li><li><a href="https://lwn.net/Articles/188693/">An earlier article on KDE + CMake</a> published at Linux Weekly News</li><li><a href="http://wiki.kde.org/tiki-index.php?page=KDECMakeIntro">KDE + CMake beginners guide</a>, courtesy of the KDE wiki.</li></ul>

<p>I promise to have some eyecandy next week folks, until then...</p>

