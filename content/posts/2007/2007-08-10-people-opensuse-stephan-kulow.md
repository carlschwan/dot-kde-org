---
title: "People of openSUSE: Stephan Kulow"
date:    2007-08-10
authors:
  - "jriddell"
slug:    people-opensuse-stephan-kulow
comments:
  - subject: "AMD thinks he needs help"
    date: 2007-08-10
    body: "http://news.opensuse.org/?p=101"
    author: "reihal"
  - subject: "Not really an interview"
    date: 2007-08-10
    body: "While I realize it is the most practical way of doing it, I am starting to get enough of these kinds of \"interviews\". Yes, that includes the \"The people behind KDE\" series. I think my main gripe is that they are not really interviews. They are just a list of standard questions the interviewee is supposed to fill out. That results in quite something different from an interview; it is more like a survey instead. In an interview there is an actual conversation and the interviewer can ask further, respond to an interesting aside and the whole thing in general gets less predictable and more deep.\n\nSo, maybe it is time to try a new way of doing this, and go more towards a real interview. I think it would result in far more interesting stories. \n\nP.S. I am aware that I could stop reading them or should make them myself if I think I can do better. Well, I don't. I know interviewing is a tough skill, and I don't think I posess it. I'll stick to programming."
    author: "Andr\u00e9"
  - subject: "Re: Not really an interview"
    date: 2007-08-10
    body: "Agreed. Given than most KDE people hang around #kde-devel or #kde4-devel these days, maybe IRC would be feasible for at least some of the interviews.\n"
    author: "Pau Garcia i Quiles"
  - subject: "Re: Not really an interview"
    date: 2007-08-10
    body: "I agree as well. I've been asked for an interview in the People Behind KDE series before, but the questions felt so formulaic that I couldn't gather the motivation to do it.\n\nNot only is it boring for the interviewee, but also for the readers IMHO. I'd much rather participate in a real interview.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Not really an interview"
    date: 2007-08-10
    body: "+1"
    author: "AC"
  - subject: "Re: Not really an interview"
    date: 2007-08-10
    body: "But complaining is always an option, isn't it?\n\nI for my part have never ever found anything about the questions deficient and value their standard nature. It's big fun to me to see how different people react to them differently.\n\nYou sure feel better now. Why didn't you at least DEMAND somebody do what you want them to? \n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Not really an interview"
    date: 2007-08-11
    body: "So you are complaining that he who complained didn't complain in a way that you could easily complain about using the standard complaint-complaint template? Bummer."
    author: "Martin"
  - subject: "Re: Not really an interview"
    date: 2007-08-11
    body: "No, I was not complaining in fact. I was saying that I found the value of these surveys rather limited, that I was loosing interest in them, why that is the case, and what may be a way to solve that. I did so in an, I think, constructive way. Judging by the other replies, I think it's save to say that I expressed something at least a part of the readers here agree with. That might be a reason for people who want to contribute to KDE in a journalistic way to think about whether it would be possible, and in what form, to make these articles more intersting. I could have, as you say, demanded another form, or even just left it at \"This sucks!\" I did not, did I?\n\nI also explained why I am not able to solve this myself: I think I lack the talent for this. I did contribute to KDE in another way. I also gave the opportunity, unlike you I might add, to respond to me personally, by giving my real name and email address. So, pray, tell me: what is *your* contribution to KDE, except some anonymous bashing?"
    author: "Andr\u00e9"
  - subject: "Re: Not really an interview"
    date: 2007-08-11
    body: "To get back on topic (let's not turn this into a bashfest), this argument just occurred to me:\n\nIf the interviews were more interesting, it would also increase the chances that other websites (digg, etc) picked them up. The current form of interviews isn't very likely to be picked up, I think.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Not really an interview"
    date: 2007-08-11
    body: "I attempted to answer a similar interview a couple of years ago, but my answers sounded really boring, and I declined the interview in the end. Maybe it was partly because I was a nervous wreck because of tax/lack of income problems, and wasn't my usual self, but in a spoken interview you can interact with the interviewer and alter the direction of the interview. When you just answer a set of questions in advance, it is harder for the interviewee to be themselves and say witty or interesting things (the interviewer has the monopoly on the jokes, eg the usual Linus vs Stallman question in KDE interviews). I just felt like a bit of an automaton with exam questions to answer, and a verbal, or otherwise interactive, interview would have been very different."
    author: "Richard Dale"
  - subject: "Re: Not really an interview"
    date: 2007-08-11
    body: "Right. Let's face it: Noone really wants to know whether I prefer Linus or Stallman. Or how my desktop looks (it looks like crap). I could however detail some interesting stories from my field of interest, the stuff I'm working on and that I care for. That's what people want to know, and that's what I enjoy talking about.\n\nMixing some private questions in is absolutely fine, but the focus should be on the things we care about. You can't achieve this with a template questionnaire.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Not really an interview"
    date: 2007-08-12
    body: "I do care, actually. And emacs or vi? "
    author: "Richard Stallman"
  - subject: "Re: Not really an interview"
    date: 2007-08-13
    body: "Agreed on the Linus vs. Stallman stuff (although it does indirectly highlight who is \"only about the technology - use it in tactical nukes for all I care!\" and who is more focused around the ethics). What bothers me is that these interviews are sometimes done in parallel, so you have four people (in the recent Summer of Code interview) answering questions one after the other, whereas just focusing on one person might have done them all more justice, avoiding things like \"what did that guy say before?\" and so on. I don't think \"real life\" panels are that interesting, either, unless there's real discussion going on - you can see from transcripts where panels have been pretty much led by a bunch of tame questions from a template instead of having panelists interacting with each other.\n\nWhat's interesting about template interviews (questionnaires, really) is how they miss the mark and keep doing so. In a recent Linus Torvalds interview it was obvious that the interviewer (erm, questioner) had various questions which didn't really fit with the things Linus wanted to talk about, but since they were all prepared in advance, the interview failed to focus on stuff that might have elicited better responses. Of course, Linus should be quizzed about things like GPLv3 and patents even if he claims to not care about them, before opining about such stuff in various prominent channels once again, but a template interview isn't exactly going to bring about a grilling."
    author: "The Badger"
  - subject: "Re: Not really an interview"
    date: 2007-08-11
    body: "not sure if i have a ton of interviewing skill (ok, i do know: i don't ;) but i did three more audio interviews while on my last trip.. i need to clean up some audio and then i'll be sync'ing them up to kde://radio =)"
    author: "Aaron Seigo"
  - subject: "text beats speech"
    date: 2007-08-13
    body: "I don't have the patience to sit through anything with an audio track, I can read about 10x faster than people talk.  I wonder if speech-to-text has progressed enough to do a useful first pass at transcribing KDE radio, podcasts, video recordings of presentations, etc.  I doubt it.  Maybe if interviewees had to first read a standard text, then y'all yak away, then run the speech through a tool (CMU Sphinx?) for boys wreck a nation, uh voice recognition.\n\nTurning a chat log into an interview would work well for some interviewees.\n\nSome techie blog recently bemoaned how interviews aren't journalism.  A journalist talks to people then writes up the substance of their conversation, which is even harder than interviewing."
    author: "skierpage"
  - subject: "Distrowatch Interview with Stephan Kulow"
    date: 2007-08-13
    body: "http://distrowatch.com/weekly.php?issue=20070813#feature"
    author: "Anonymous"
  - subject: "Re: Distrowatch Interview with Stephan Kulow"
    date: 2007-08-14
    body: "Thanks! That was a really cool interview!\n"
    author: "random user n. 1309"
---
<a href="https://mail.kde.org/mailman/listinfo/kde-oldies">KDE oldies</a> may remember this <a href="http://behindkde.org/people/stephan/">interview with Stephan Kulow</a> from back in 2000.  Well the folks at openSUSE have clearly been watching us, not only have they started their own <a href="http://news.opensuse.org/">openSUSE news site</a>, they have also started their own interview series with a <a href="http://news.opensuse.org/?p=112">brand new interview of Stephan Kulow</a>.  Stephan has been release dude for much of the KDE 3 series and now shares the same honoured title for openSUSE.



<!--break-->
