---
title: "Qyoto C#/Mono Bindings for Qt4, New QtRuby release and PHP Bindings Coming Soon"
date:    2007-07-03
authors:
  - "rdale"
slug:    qyoto-cmono-bindings-qt4-new-qtruby-release-and-php-bindings-coming-soon
comments:
  - subject: "This is wonderful Richard!"
    date: 2007-07-03
    body: "Three cheers for both you and Arno for developing these csharp bindings!"
    author: "manyoso"
  - subject: "Re: This is wonderful Richard!"
    date: 2007-07-03
    body: "Thanks Adam! I hope we can add the Kimono KDE classes, and C#/mono support to KDevelop4 and create a great complete development environment."
    author: "Richard Dale"
  - subject: "Consistency between bindings"
    date: 2007-07-03
    body: "With all these bindings, are the developers looking for consistency between the bindings?\n\nExamples:\n- Korundum seams to use a completely different approach to signals then Qyoto does here.\n- Why the weird namings?\n- Are they all based on Smoke, or does this differ between the bindings?\n- How fast and easy can be the bindings be made up to date with releases?\n\nOff course, some things can't be consistent, but it would be sad if a developer has to relearn Qt programming for every new binding."
    author: "Diederik van der Boor"
  - subject: "Re: Consistency between bindings"
    date: 2007-07-03
    body: "I believe the intent is to sacrifice consistency between bindings in favor of consistency with the environment -- a very very good tradeoff, IMHO.\n\nBasically, in C#, the standard is those `weird namings' and what have you, so that's what those bindings look like. In Ruby, class-level methods that modify the class (such as the signal declarations) are standard, so you get that method of achieving it. It just fits in that environment."
    author: "Antonio"
  - subject: "Re: Consistency between bindings"
    date: 2007-07-03
    body: "Sounds reasonable. After all, a Python developer is not a Ruby developer and visa versa."
    author: "Diederik van der Boor"
  - subject: "Re: Consistency between bindings"
    date: 2007-07-03
    body: "For the naming, what happens if someone uses the C#/Pascal naming convention (uppercase first character) in C++ too and has a signal or slot called \"Clicked\" at C++ level already? Will that confuse Qyoto's binding generator?"
    author: "Kevin Kofler"
  - subject: "Re: Consistency between bindings"
    date: 2007-07-03
    body: "No, it will work fine, as the C++ type signature is added as an argument to the Q_SLOT or Q_SIGNAL attribute for classes that are generated. In your example that would be Q_SLOT(\"void Clicked()\"), instead of Q_SLOT(\"void clicked()\"). In a Connect() call however you would need to give the exact C++ type signature though. For example:\n\nConnect(button, SIGNAL(\"Clicked()\"), myclass, SLOT(\"Doit()\");\n\nBut in C#, emitting the signal is the same whether of not the original one started with a lower case letter or upper case, as it would always be upper case:\n\nEmit.Clicked();\n"
    author: "Richard Dale"
  - subject: "Windows"
    date: 2007-07-03
    body: "QtRuby works on Windows?\n\n"
    author: "self"
  - subject: "Re: Windows"
    date: 2007-07-03
    body: "Yes, and Kenichi Matsumoto has done a great job patching QtRuby 1.4.9 so it can be built with msvc6, and then packaged up everything you need to get going with QtRuby programming on Windows including the Qt libs. You can download it from here:\n\nhttp://www.jzkey.com/qtruby/"
    author: "Richard Dale"
  - subject: "MONO? MONO?!"
    date: 2007-07-03
    body: "For Christ's sake, can you people just leave this crap to microsoft and novell? Just because Miguel de Icasa goes out of his way to bring MS-controlled environment to other platforms doesn't mean we have to use it..."
    author: "name"
  - subject: "Re: MONO? MONO?!"
    date: 2007-07-03
    body: "I don't like Mono either. But anyway: This only provides QT-bindings for those who wish to use them. Why not?"
    author: "Michael"
  - subject: "Re: MONO? MONO?!"
    date: 2007-07-03
    body: "Development !=use. While I agree that buying into the \".NET\" platform is probably a bad long-term choice, the people to convince are other, possible users. Developers will always develop whatever their hearts desire, whether people use it or not.\n\nSo let the devs have fun, and code what interests them. If you don't like .NET, then don't use the bindings, and encourage others to do the same. Just don't discourage the dev ;)"
    author: "woogs"
  - subject: "Re: MONO? MONO?!"
    date: 2007-07-04
    body: "\"For Christ's sake, can you people just leave this crap to microsoft and novell? Just because Miguel de Icasa goes out of his way to bring MS-controlled environment to other platforms...\"\n\nI have no problem with Mono per se, but it really should have been so much more than wannabe a .Net clone. A bit sad really, and a bit of a missed opportunity."
    author: "Segedunum"
  - subject: "Re: MONO? MONO?!"
    date: 2007-07-04
    body: "What else should it be? I was under the impression that Mono's sole purpose was to bring .NET (the CLI, the languages and the library) to other platforms.\n\n\"I guess I don't really have a problem with this horse, but wouldn't it be so much better if it were a car?\""
    author: "Jeff"
  - subject: "Re: MONO? MONO?!"
    date: 2007-07-04
    body: ">>\"I have no problem with Mono per se, but it really should have been so much more than wannabe a .Net clone. A bit sad really, and a bit of a missed opportunity.\"\n\nOh please, all you do is trolling in Mono topics at osnews.com.\n\n\nBTW, I Like the bindings but, Still using slots?, oh come on, use delegates? it is that hard to bind?\n\nI mean GTK# uses delegates and not slots, and that's a better solution."
    author: "aam"
  - subject: "Re: MONO? MONO?!"
    date: 2007-07-04
    body: "I agree with you. Signal and Slots are unknown in the .NET world. Delegates are the native way to catch and respond to events and GTK# already use them. It would be great to have delegates also in Qyoto."
    author: "David"
  - subject: "Re: MONO? MONO?!"
    date: 2007-07-04
    body: "\"BTW, I Like the bindings but, Still using slots?, oh come on, use delegates? it\"\n\nYes, we could do something with anonymous delegates too, and then there are lamdas coming in C# 3.0. \n\nFor a first release we want to stick pretty closely to the C++ api until we get some user feedback. If we rush into implementing something clever with delegates, it is very hard to change subsequently and so it makes sense to start off fairly conservatively. Note that the design patterns implemented with delegates are based on C# 1.0 features, and it may well be a good idea to revisit whether you would want to do things exactly the same way, if you were to use C# 3.0 features."
    author: "Richard Dale"
  - subject: "Korundum and KDE4"
    date: 2007-07-03
    body: "Hi\n\nI would like to start to write a little app in Ruby using KDE4 technology (solid and maybe plasma). When do you think this will be possible? around beta1? already now? "
    author: "Vide"
  - subject: "Re: Korundum and KDE4"
    date: 2007-07-04
    body: "There is a BoF on that tomorrow here at aKademy 2007, in Glasgow:\nhttp://akademy.kde.org/codingmarathon/talks/korundum.php\n\nUnfortunately, BoF sessions are not recorded on video.\n"
    author: "Pau Garcia i Quiles"
  - subject: "Re: Korundum and KDE4"
    date: 2007-07-04
    body: "Korundum in the kdebindings trunk builds fine with the KDE4 libs, and so you can start experimenting now. I'm not sure if I've added the solid classes yet, but if you tell me what you want to do I can see if it can be put into Korundum. You can use the QtDBus classes with ruby, which is useful for KDE4 programming. The ruby support for plasma will be based on the Kross scripting framework, rather than Korundum. I'll be making a small change soon to Korundum to allow Kross ruby instances to be combined with Korundum ones."
    author: "Richard Dale"
  - subject: "Re: Korundum and KDE4"
    date: 2007-07-05
    body: "My idea is to write a little program to easily download photos from a phpto camera, and pu them locally on disk or (in a second future version :P) put them directly on a remote storage (flickr, picasa etc).\n\nThis would be my very first program to learn Ruby, Qt and KDE, so maybe I will need months to do something useful. Anyway if Korundum is already available in current kdelibs, I'm going to try something this weekend!"
    author: "Vide"
  - subject: "Fantastic!"
    date: 2007-07-03
    body: "Thanks for the great work! btw, seems qyoto.org went done - a good sign :)"
    author: "Sebastian Sauer"
  - subject: "Where is Perl in KDE?"
    date: 2007-07-04
    body: "Where are the Perl bindings for KDE library calls?\nWhere is the Perl DCOP interface that can receive messages and bind anonymously?\nWhere is the KDE UI designer that emits Perl code?\nWhere is the Cross support for Perl?\nWhere is the Kommander documentation with Perl examples?\nWhere are the Amarok and Konversation Perl scripts?\nWhere is the Perl work relating to Qt4 and KDE4?\n\n\nIt is fuck all possible to write a KDE application, whole or partial, in Perl. While you neglect a Unix system's most important language after C, GNOME/Gtk2 is eating your lunch. Guys, it *chafes*."
    author: "AC"
  - subject: "Re: Where is Perl in KDE?"
    date: 2007-07-04
    body: "Developers create bindings for the languages they like and/or use.  The reason there aren't any Perl bindings is because no one felt the need to do so (yet).\n\nPersonally, I say good riddance to Perl.  Some years ago I may have agreed with you that Perl is the second most important language in a Unix system, but since then, better languages (namely Python and Ruby) have filled that same niche, and for good reason!  Python and Ruby -- though far from perfect -- are still an order of magnitude nicer, cleaner, and more elegant than Perl. Honestly, when someone asks me why I don't like Perl, the most polite answer I can come up with is that \"I'm not really into that S&M stuff\".\n\nBut anyway, if Perl really is your cup of tea, then go ahead and start a Perl/KDE bindings project.  The more bindings the merrier!...\n"
    author: "dario"
  - subject: "Re: Where is Perl in KDE?"
    date: 2007-07-04
    body: "Where were you, anonymous coward, when there were perl bindings that needed maintaining? Why didn't you step up and work on it? Why didn't you, if you are incapable of doing so, step up and use them? There have been perl bindings, they were not used (although they were usable), and therefore they were unmaintained and dropped."
    author: "Boudewijn Rempt"
  - subject: "Re: Where is Perl in KDE?"
    date: 2007-07-04
    body: "Because I can't speak C++, which is the necessary requirement for everything I named. It's up to the Gearheads, not the Camels, to make it happen.\n\nThere have never been bindings for KDE, only for Qt. A Qt app in KDE feels every bit as anaemic and out of place as a Tk/Gtk2/Wx app. Something as trivial as the lack of the common Open File dialog totally ruins the usability."
    author: "AC"
  - subject: "Re: Where is Perl in KDE?"
    date: 2007-07-04
    body: "C++ knowledge isn't required to write Perl examples for Kommander.\n\n\"Where is the Perl DCOP interface that can receive messages and bind anonymously?\"\n\nDCOP is pretty much being EOLed with KDE4, D-Bus is the new hotness."
    author: "Sutoka"
  - subject: "Re: Where is Perl in KDE?"
    date: 2007-07-04
    body: "So basically you're too lazy to learn what you need to know to solve your own problem, and you want people who typically do things because they're scratching one of their itches to scratch yours for you. And you're not even asking nicely. Yeah, that's smooth.\n\nMaybe it's that attitude that's the reason there are no bindings?"
    author: "AC"
  - subject: "It is here..."
    date: 2007-07-04
    body: "http://perlqt.sourceforge.net/\n\nIf you want it updated or extended to KDE, then go for it.  It is based on the same smoke lib that QtRuby and Qyoto are now using.\n\nJust stop begging other people to scratch your own itch."
    author: "manyoso"
  - subject: "Re: It is here..."
    date: 2007-07-04
    body: "Oh, no. Begging other people to scratch one's itch is OK.\nBut, if you _demand_ other people to scratch your itch then you'll be frowned upon. And/or ignored. :-)"
    author: "Humberto Massa"
  - subject: "Re: It is here..."
    date: 2007-07-04
    body: "In this case we're talking about a *developer* begging other developers for a *perl* (of all things) binding to a C++ GUI toolkit.  In all honesty, he should probably just keep this nasty fetish to himself regardless."
    author: "manyoso"
  - subject: "Re: Where is Perl in KDE?"
    date: 2007-07-04
    body: "I used to write in Perl a lot.\n\nNowadays Perl scares me. It looks like an explosion in an ASCII factory.\n\n*ducks*\n\n;-)"
    author: "Diederik van der Boor"
  - subject: "With the new language bindings..."
    date: 2007-07-04
    body: "With the new language bindings, perhaps it is time to reconsider something that has been brought up before.\n\nAs many of you are aware, the FreeDesktop.org project aims at bringing together some common aspects of the free desktop. However, when developing applications, one is still forced to focus either on GTK or QT and in doing so direct their product at one audience predominantly. QT apps can run in Gnome (or Xfce) and GTK apps in KDE, however they don't look fully integrated. Furthermore, because of the fundamental coding differences between GTK and QT, we often have redundant efforts into relatively mirror software programs, one aimed at each major desktop.\n\nI'm certainly not the first to suggest this, but isn't now specifically a good time to consider more fully merging the two technologies? With the major refactoring of KDE 4 and QT 4, there are some major new core technologies that any developer should be excited about. KDE is also embracing Tango, DBus, and many of the FreeDesktop.org concepts.\n\nFurthermore, one of the major arguments for keeping GTK and QT separate technologies has been C vs C++, however both now have diverse language bindings. Developers should be able to develop in any language they choose, and not have the language be mandated by the toolkit. Honestly, the only good reason to keep them separate is in design. People who prefer GTK styles or widgets opt to develop with GTK, and vice versa. Couldn't there be a universal library that is capable of operating in appearance and usability like both GTK and QT when it comes to widgets and visuals? In fact merging the two might extend both and not only enable developers to reach a broader audience easier, but unlock more power and potential for everyone.\n\nChoice is important, and one should never lose the ability to run their desktop how they see fit. Neither Gnome nor KDE should lose all their efforts into developing their vision of the desktop, however further merging core technologies and libraries means opening up these powerful tools to developers for all free desktops.\n\nImagine any application being able to tap into the potential merged technologies of:\n\n    * Cairo - A sophisticated 2D vector graphics library.\n    * Pango - A library for laying out and rendering of text, with an emphasis on internationalization.\n    * D-Bus - Interprocess communication system.\n    * GStreamer - A multimedia framework.\n    * HAL - A specification and an implementation of a hardware abstraction layer.\n    * Poppler - A PDF rendering library.\n    * Tango Desktop Project - Which aims to provide a common visual standard across different platforms.\n    * Solid - Making a universal hardware layer is CRUCIAL. Given that both projects utilize Hal and DBus, taking it one step further isn't a huge stretch. Further developing Solid could tie into working with kernel developers to examine how to best handle hardware from the kernel into userspace, and reexaming exactly what portions belong in each space.\n    * Phonon - I hate to sound like a dissenter, but audio on the FreeDesktop leaves much to be desired. Phonon aims to fix this.\n    * Sonnet - An advanced dictionary that I believe will be the successor to ASpell\n    * Decibel - Project providing a service architecture to make chat and phone communication universally available to desktop applications\n    * Plasma - Plasma would have to be extended to support and operate like Gnome's deskbar and desktop, but it is a powerful tool to create widgets and plasmoids that would offer great flexibility to all parties.\n    * Strigi - I know there are many search technologies, and I'm assuming the best aspects of each could hopefully be factored into Strigi\n    * Semantic Desktop - I am familiar with NEOMUK, and perhaps there are other projects that could be brought to this table.\n    * Gnome VFS - The Gnome virtual file system.\n    * Gnome Keyring - For storing encryption keys and security information.\n    * Bonobo/KParts - Again, merge the best features of these two technologies to create a powerful universal component model\n    * LibXML - The XML library.\n    * ORBit - The CORBA ORB for software componentry.\n    * A merged composite technology for nifty eye-candy. Compiz and Beryl merged though many thought it wasn't possible. Now Kwin is being rewritten with many of the same features that Compiz would provide, but is duplicating efforts. No doubt Gnome, KDE, Xfce and all the rest will want to retain separate WM's, but a core unified underlying base for composite extensions should be established.\n    * Translation - Obviously different desktop projects each have different apps, and a bunch of different text, but many of the core terms and documents could be brought together to simply translation on the free desktop.\n\nI know KDE is developing an icon-caching system given that KDE 4 is going to heavily utilize SVG to better scale everything on the desktop. I'm not sure if Gnome has a similar system.\n\nIntegrating core technologies involves on getting people who currently see things differently to come together. Some may dismiss this as an impossible goal, however that isn't the case. Ideally these technologies should be flexible enough to achieve the results that everyone is looking for while providing a unified base to avoid duplication of efforts.\n\nLastly, what I'm proposing is no small task. I fully understand that it involves a great deal of work, and in doing so, it would temporarily pause/strain development of projects like Gnome and KDE from moving forward in their current separate ways. However, the initial work may be daunting but imagine how much time and effort would be saved in the long run when we drastically cut down on duplicating efforts.\n\nIn many ways this is a win-win, and really such an obviously beneficial move, it should at the very least be revisited and given considerable thought. The sooner such a merge of core technologies takes place, the more time you save in the long run, and the easier such a merge takes place. As duplication in code continues, the more time consuming it will be to examine all the duplicate code and agree on how to merge the two.\n\nAt the very least, I hope existing efforts can continue and the FreeDesktop.org project should choose one or two new areas to focus on in bringing everyone together, like Strigi/Beagle or Solid. So please, before dismissing this out of hand, at the very least look over the above list and consider if any of those technologies could be or should be merged into a FreeDesktop standard library for everyone to use."
    author: "T. J. Brumfield"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "Please stop.\n\nYou have absolutely no idea what you are talking about.  This is just so much hot air signifying absolutely nothing.  \n\nYou pass around concepts like 'merging' Gtk and Qt that have no chance of any real meaning.  These are two *fundamentally* different pieces of computer code.  They don't share the same license, the same language, the same design, the same ... just about anything.\n\nIt is one thing for actual software engineers to suggest concrete and specific steps that make it easier for free desktop apps to integrate with the different desktops.  It is quite another for ignorant benchwarmers to bloviate endlessly about 'merging' *fundamentally* different pieces of software code into one massive library.\n\nTo even suggest such a thing is to betray your complete lack of the actual requisite knowledge necessary to talk intelligently about the free desktop software stacks.\n\nI hate to break it to you, but you are a crackpot."
    author: "anonymous"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "I understand they are written in completely different languages and are completely different ways of doing things.\n\nYou failed to read what I wrote.\n\nI suggested a unified core technology that could effectively operate like both QT and GTK.\n\nEither fork a new branch, or extend QT to behave and look like GTK.  Then you could develop a desktop (again, perhaps a future iteration of KDE perhaps renamed to reflect a unified desktop) that operates on this new QT, but visually can look and operate as GTK as well.\n\nEvery GTK based add would have to be ported over to use the new QT calls, but to the end-user, they could still look and operate like their GTK apps.\n\nAgain, why all the effort?\n\nBecause there would be one major desktop with all the available core technologies of both, and all future development could be done on one major API that caters to all users.\n\nI would ask that you refrain from being so close-minded in the future regarding my posts, and also to refrain from personal attacks."
    author: "T. J. Brumfield"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "\"Either fork a new branch, or extend QT to behave and look like GTK\"\n\nhttp://doc.trolltech.com/4.2/qt4-2-intro.html#desktop-integration\n\n\"Every GTK based add would have to be ported over to use the new QT calls, but to the end-user, they could still look and operate like their GTK apps.\"\n\nPipe-dream; sorry."
    author: "Anon"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "Thanks for the link.\n\nYou are correct that it would be a pipe-dream for every GTK app to jump ship.  Some people would choose to continue to develop for GTK even if the main Gnome team decided to go to QT, as it clearly can emulate the GTK look.\n\nAgain, this seems however to prove that my original point is clearly not as impossible as you seem to suggest.  You could build one major desktop that can easily look and operate like Gnome just as well as it can be configured to look like Windows or OSX.\n\nWhy not try to migrate other developers to such an approach?\n\nAgain, consider how long it took to port at the KDE apps to KDE 4.  I'm sure it was a lengthy undertaking, that is only now finishing, and that is only for the core KDE apps.\n\nGetting a bunch of GTK apps to port won't be easy, but that is a one time sink of time compared to the amount of time they'd continue to sink into redundant coding for years to come.\n\nFurthermore, it wouldn't be a waste, as there is a huge tangible benefit to porting.\n\nIt will run faster, use less memory, and can now easily integrate all the features of QT and KDE 4 including Solid, Phonon, etc.\n\nAre you saying that is a worthless cause?"
    author: "T. J. Brumfield"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "You are a bleeping idiot.  To port over an ENTIRE DESKTOP that uses C and the LGPL to a Qt desktop styled to look like GTK.  You simply have *no idea* what you are talking about.  Simple as that."
    author: "anonymous"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-06
    body: "go tell that to the GTK developers, then. I don't believe they'll listen for a second. KDE has done lots of work on interoperability...\n\nanonymous is being a jerk, but the facts are on his side."
    author: "Chani"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "\"Merging Qt and Gtk\" isn't possible, as they are two totally different APIs and implementations, even if partially doing the same for the user. (doing UIs, where Qt has a somewhat broader scope). A merge would mean writing a new toolkit from scratch, and I'm sure you have the skilled, ingenious, fast ...and braindead people at hand, who would get it on the Qt level regarding completeness, quality of API and code, stability, availability of professional support in practically no time.\n\nAnd actually, toolkits monoculture isn't needed at all. On platforms with an allegedly consistent UI interface even more toolkits are in use than on a mixed Gnome/KDE environment. Despite that the applications are more or less consistent (On Windows more legend than reality if you leave MS products, and Mac quite good but not as 100% as people think it is) as the platforms define a common look&feel, behaviour, HIG, have platform-wide APIs etc.\n\nWhat you want is common standards and interfaces so that the UI toolkit becomes a mere implementation detail.\n"
    author: "Frank"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "\"'Merging Qt and Gtk' isn't possible, as they are two totally different APIs and implementations, even if partially doing the same for the user.\"\n\nI'm aware you can't simply mash the two.\n\nHowever, consider that both kopete, and gaim promises years ago to work on voice and video support into their respective messengers.  Both have had to dedicate time to implementing new network protocols, and keep up with the changing standards of the big protocols.  Yet from an end-user standpoint, the two applications are pretty much the same.  Combined effort one one program could have left much more time for heavily desired features, such as voice/video support.\n\nYet one is written for GTK and one is written for QT.\n\nThis waste of developer resources extends past one app into the entire spectrum of free software being written right now.  I'm not sure why someone would contend this isn't a serious problem that inherently degrades the quality of free software.\n\n\"A merge would mean writing a new toolkit from scratch, and I'm sure you have the skilled, ingenious, fast ...and braindead people at hand, who would get it on the Qt level regarding completeness, quality of API and code, stability, availability of professional support in practically no time.\"\n\nThat isn't necessarily the case.  I know some C and C++, and though I'll admit I'm a shoddy-hack of a programmer who only does it best as a hobby, I'm not completely ignorant of these matters.  Why write a completely new toolkit?\n\nThere are two simple solutions to the problem.\n\n1 - Extend an existing toolkit (preferably QT) to be able to produce the same results of other toolkits.  Given that QT already provides a rich base, you're not writing from the ground up.  The specifics of how GTK operates for an end-user, such as the GTK file dialog needs to be duplicated, but again, between Xorg and QT, you already have the capabilities to render the window and produce a file dialog.  You just need to tweak the behavior.\n\n2 - Develop a wrapper (this would be from the group up) that intercepts the calls to GTK and reinterprets them as calls to QT.  There is a project that somewhat offers this functionality now, and perhaps could be further developed.\n\nhttp://gtk-qt.ecs.soton.ac.uk/\n\nLook, I believe QT to be the superior toolkit, but it is also hard to deny with the growing popularity of Ubuntu, that more and more distros seem to be focusing on Gnome first.  And many apps are being developed with GTK in mind first.\n\nThusly, for all the brilliant effort that has gone into the core technologies of QT 4 and KDE 4, those features will be completely lost of many if not most users of free desktops.  Frankly, that is a shame.\n\nI know considerably less about Gnome as I haven't touched it in years.  But I'm sure they have a few core technologies that could be ported over into a new unified code-base.  Again, they'd have to be rewritten with QT and kdelibs in mind.\n\nBut again, isn't the entire point of the FreeDesktop.org project to unify many of the core technologies that are ubiquitous to every desktop user?  I closed my post asking that at the very least, continue that effort with an eye one or two new technologies that we can focus as being universal to all desktop users, such as trying to bring together one new standard for search (perhaps based off Strigi since it supports multi-backends and has such great performance).\n\nExtending that even further, shouldn't meta-data, component models, semantic desktops, and search all tie together into a universal model that we can all use?\n\nWhy is everyone here so immediately dismissive of a project that has already proven not only to be successful, but crucial to the development of both KDE and Gnome?\n\nI find myself extremely disappointed that it seems this is dismissed without actually acknowledging what I wrote, but rather no doubt immediately turning to previous discussions on the matter."
    author: "T. J. Brumfield"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "\"Why is everyone here so immediately dismissive of a project that has already proven not only to be successful, but crucial to the development of both KDE and Gnome?\"\n\nWe are not dismissive of FreeDesktop.  We are dismissive of *you* because you continue to ramble on endlessly about things you know nothing about.\n\nYour ideas are not novel.  They are just a mishmash of impossible and stupid suggestions -- \"rewrite qt or gtk or both into some massive new library\" -- tired old fanboy lamentation -- \"duplication of effort is always bad and killing free desktop!!\" -- misinformed and ignorant comments -- \"strigi and beagle should merge!\" -- and pie in the sky naivete -- \"GNOME should rewrite everything to use Qt!\"\n\nQuit rambling.  You are not suggesting anything new or novel.  That is what is being dismissed."
    author: "anonymous"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-05
    body: "Making personal attacks and flaming while posting anonymously is pretty bad form."
    author: "T. J. Brumfield"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "Honestly I haven't really read though your response. It's much air.\n\nSometimes you need an alternative with different design to prove something can be better. Qt4 graphics have a much better performance then Cairo. Strigi beats Beagle in performance and system load. HAL and DBus OTOH are really good, so KDE4 is adopting them.\n\nRegarding widgets: Windows doesn't have a good standard widget either!! The default widgets are the plain menu's you know from Windows 95, which Notepad and Wordpad still use. If you look closer you'll see Notepad, Windows Explorer, Visual Studio, Office all use different menu's and toolbar handles. They're all custom widgets, nothing standard.\n\nMost advanced widgets for Windows are part of a commercial widget toolkit you've chosen. This can be MFC, ComCtl, VLC (Borland), Borland OWL, Windows Forms (.Net), WPF (.Net3), Gtk or Qt. All those different frameworks do have something in common. Windows provides central settings for fonts and color schemes. This makes them all look the same, even though they aren't. That's something Linux should really improve."
    author: "Diederik van der Boor"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "I think I could have deduced that you didn't read my post given your response.  I'm not sure how the two posts really relate.\n\nMy desire is to see the best core technologies (whatever they may be) be available to all people.  However, we are tying many of these technologies into separate systems, which seems counter to the FreeDesktop.org's mission.\n\nI hate to repeat my response to the above two other posters.  I fully understand you can't simply throw GTK and QT together.  You can't just functions from one, and call them from another, or anything like that.\n\nOut of the box, KDE can easily be configured to look and operate functionally like OSX, right?\n\nOut of the box, KDE can easily be configured to look and operate functionally like Windows, right?\n\nWhy couldn't KDE out of the box be configured to also optionally look and operate functionally like Gnome?\n\nIf that were the case, then people who want a Gnome-like experience could have it, but also take advantage of the superior QT toolkit, and all the nifty new technologies of KDE 4.\n\nFor instance, is there any reason why the stack of HAL/DBus/Solid shouldn't act as the universal user-space interface for hardware?\n\nTo the end user, they want the ability to configure their desktop to work the way they want it, but underneath, it only makes sense for everyone to be operating on the same core technologies.\n\nFurthermore, as a developer, why should I be forced to either write two fully separate branches in my code to cater to both GTK and QT users?  If I only wrote for the one toolkit, and left it to the configuration of the desktop to decide how it appeared to the user, wouldn't that be vastly superior?"
    author: "T. J. Brumfield"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "> I'm not sure how the two posts really relate.\n\nWith my talk about widgets, I ment the different toolkits. Most users complain there is Gtk and Qt. What they don't know is the Windows world isn't much better. It's just less transparent.\n\n- When you code in the .Net IDE, your apps will likely use Windows Forms or WPF.\n- When you code in the Delphi IDE, your apps will likely use the Borland VCL.\n\nThe same applies to Linux:\n- When you code in the GNOME IDE, your apps will likely use Gtk.\n- When you code in the KDE IDE, your apps will likely use Qt.\n\nSo IMHO, the whole discussion about merging toolkits is not really necessary. Instead, we should focus on providing consistent looks by using the same color scheme settings and fonts. That's what Windows does, and causes everyone to believe Windows interfaces are consistent ..when they're not better then Linux at all. Apple has different toolkits too, with the recent WWDC they announced OS X 10.5 will ship with ..a consistent look across applications. Go figure. :-p"
    author: "Diederik van der Boor"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "I think I could summarize the thoughts of everyone here: NO."
    author: "Vide"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "Thanks for your considerate and well thought-out response."
    author: "T. J. Brumfield"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "Hi !\n\nI think he's right also I'm not sure if it's possible to merge these technologies. Especially his words \"[...]I'm not sure why someone would contend this isn't a serious problem that inherently degrades the quality of free software.\" seems to me very important. Many approaches to common problems have advantages because as a result you might get really exciting and innovative stuff but on the other hand the work has to be done several times and with additonal effort and this leads to lower quality of code. Because the resource developers is very limited and the number (or complexity) of projects that are develop is growing. So at the end you might get many possible solutions that try to solve a common problem but all of them doesn't work perfectly. \n\n"
    author: "Alex"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "i'm not going to go through your comment point by point as there are general answers that apply to various issues you raise, which are:\n\n- when we can and it makes sense, we do share implementations of libraries. you mentioned libxml, for instance. ditto for things like decibel ..\n\n- when the options are inferior to our requirements (which can be for various reasons, ranging from technical to project structure) we need to go elsewhere\n\n- we try and standardize things that can be so separate implementations behave consistently\n\n- there are many issues that are not apparent unless you are familiar with the technologies, such as real world utility, portability, maintainability, etc. you mention gstreamer but that's simply not an option for us to write all our applications to directly, so we have phonon which abstracts that 'detail' away from the vast majority of app developers and lets us use gstreamer where it is available and works and other solutions elsewhere.\n\n- a lot of the issues have arisen over time line issues, e.g. kde was often first on the scene and for various reasons other projects decided to reimplement what we had already done. soemtimes there were good reasons for this (in which case people shouldn't complain about it) and sometimes there weren't, but it is really hard to change historical timelines retroactively ;)\n\n- sometimes there are purely political problems that get in the way. very rarely this is an issue that we (kde) can fix.\n\nyou are right that you aren't the first to suggest such things. the reasons why things aren't more \"single stack\" as you note have to do with a myriad of details. please either just trust us that we are doing our best to arrive at optimal solutions or get involved and improve things.\n\nunfortunately, long diatribes tend to have the opposite effect that you want by frustrating developers and causing them to close themselves off into their own little worlds so they don't have to listen to vague ramblings from those without the context/background to speak with knowledge on the topics. this balkanization works against cooperation which requires openness and communication.\n\nso ... get involved or let the people who are do the best they can. we're usually aware of the issues far sooner than those who aren't involved are anyways =)"
    author: "Aaron J. Seigo"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "Thanks for the response.  I appreciate that you read what I wrote and didn't immediately jump to conclusions.\n\nI believe I poorly worded my post in an effort to seem diplomatic.  Honestly, a \"merging\" of the two worlds if it did occur would likely mean really just largely an open adoption of QT.  Given that benchmarks showed the same apps running of the supposed \"bloat\" of KDE 3 ran faster and used less memory than on Gnome/GTK 2, and considering the improvements in QT (I'm particularly interested to see real world performance with the multi-threading improvements), is there any good reason not to push for open adoption of QT 4?\n\nAgain, the three hurdles you'd likely run into are:\n\n1). Some people like the way GTK looks, but this is a misconception.  QT can look like GTK if you wanted it to, and GTK doesn't really have a look of it's own per se, given that it can be themed to look different ways really.\n\n2). I prefer C over C++, but again with the varied language bindings, and the new scripting platforms, language shouldn't be a barrier anymore.  You should be able to develop around QT with the various bindings in your language of choice as I understand it.\n\n3). It is too much work.  Well, this is subjective, and I'm not the one coding, but I would volunteer to test, log bugs, write documents, do web pages, whatever I would be capable of.  Again, I know some C and C++, but really I'd just get in the way of myself.  I'm a horrid coder.  However, I do personally believe the benefits are significant, and not just in performance increase, or letting the other guys utilize the new KDE-niftyness, but being able to develop for one toolkit and know that everyone can use your app.\n\nI think I'm going to try this again as a different, smaller suggestion.\n\nWhen KDE 4 launches (or a pretty usable RC launches) I'd like to see someone throw together a LiveCD or a decent proof-of-concept that you could take Clearlooks, and get KDE to look and operate at a basic level like a Gnome desktop.  Then utilize that proof-of-concept to demonstrate comparable performance with the existing Gnome desktop, and also demonstrate the advantages of the new technology.\n\nA tangible proof-of-concept should be enough to initiate a discussion of whether or not there is any worth in discussing such a project."
    author: "T. J. Brumfield"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "Look.  Seems all you are suggesting is that GNOME adopt Qt if the Trolls make a slightly reworked version that gives standard GTK dialogs for like the file dialog... then:\n\n1.  You've written a lot of hot air for such a simple proposal.  GNOME adopts Qt... that is what you are suggesting??\n\n2.  Please go very quickly over to #gnome or on the GNOME mailinglist and give your brilliant proposal.  I'm sure they will be amazed they'd never thought of this before and will quickly see the wisdom of your gentle counsel.\n\nme <-- ROTFLMAO"
    author: "anonymous"
  - subject: "More specific question"
    date: 2007-07-04
    body: "You seemed to suggest that if an existing implementation exists that you like, you use it.  This makes sense.\n\nHowever, it seems like some of the new stuff in KDE 4 is so universally useful, that perhaps it should have been pushed into a more neutral domain such as directly into Xorg, or into a shared FreeDesktop.org project.\n\nFor instance, people do mix and match GTK and QT apps in both desktops.\n\nWhen looking at things like Strigi, metadata, NEPOMUK, etc. that it increases in usefulness if every app is on the same page?  How aware will these be of data models from GTK apps?"
    author: "T. J. Brumfield"
  - subject: "Re: More specific question"
    date: 2007-07-04
    body: "you fail to see the real problem. gtk and qt are different because they WANT to be different. they are build by developers who think different. they simply can not agree on something as big as a main library like a gtk/qt merge would be, because they value things differently.\n\nyou want this library to be the jack of all trades, but thats just not possible. you have to make tradeoffs, and there are always people who value things differently.\n\nso to summarize this thread: its simply not possible. that may be hard to understand if you are no developer your self, buts thats just how it is. try reading the kde core dev list a few months, and you will see how hard it can be to get a consensus even with mostly like-minded people.\nthe deveoper world is just like any other world, its impossible to get everybody to agree on something on a large scale - just look at world politics ;)."
    author: "AC"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "\u00abCouldn't there be a universal library that is capable of operating in appearance and usability like both GTK and QT when it comes to widgets and visuals?\u00bb\n\nYep, instead of QStyle and GtkStyle, it would be great to have a common high-level drawing interface that would be then implemented by KDE/Qt and GTK+. It would also be great for other software (like OpenOffice) which resorts to some hacks to provide the same appearance of the desktop."
    author: "fast penguin"
  - subject: "Re: With the new language bindings..."
    date: 2007-07-04
    body: "May I suggest a concise, but polite, criticism of the proposal...\n\nhttp://www.bobcongdon.net/blog/2004/06/boil-ocean.html\n\nPhil"
    author: "Phil Thompson"
  - subject: "Re: With the new language bindings..."
    date: 2007-08-13
    body: "I don't think that this has been mentioned, but licensing is potentially an issue here.  From memory, Gtk+/GNOME use the LGPL, whereas Qt Free Edition uses the (viral) GPL.  I would agree with the other posters that the technical difficulties of this approach are unlikely to be overcome.  However, even if they were, those developers currently using the LGPL Gnome code may not wish to switch to using a Gnome like product built on the (GPL) Qt."
    author: "Jonathan Morgan"
  - subject: "Great job!"
    date: 2007-07-04
    body: "Thank you for another great release of QtRuby! I hope we will see a lot of C# developers joining KDE development very soon.\n"
    author: "Pau Garcia i Quiles"
  - subject: "Bindings"
    date: 2007-07-04
    body: "Stupid question, perhaps, but will any of the bindings be included by default with KDE 4.0? It would make writing in non-c++-languages more attractive, IMHO."
    author: "Plopp"
  - subject: "Re: Bindings"
    date: 2007-07-04
    body: "The QtRuby/Korundum and Qyoto/Kimono bindings (and hopefully PyQt/PyKDE too), along with Kross runtimes for various languages, will be in the kdebindings module for KDE4. It is up to a Linux distributor to decide what they want to package from kdebindings though."
    author: "Richard Dale"
---
After the recent final release of <a href="http://trolltech.com/products/qt/jambi/index">QtJambi</a>, Trolltech's Java bindings, I'm pleased to announce another new member of the Qt bindings family, the Qyoto C#/Mono bindings for Qt 4.3, which are available for download on the <a href="http://www.qyoto.org">Qyoto/Kimono site</a>, where there is also a help forum for your Qyoto programming questions. Big thanks to David Canar for setting up the site, and organizing the release. Read on for more details.

<!--break-->
<p>Additionally, I've just released QtRuby 1.4.9 on the <a href="http://rubyforge.org/projects/korundum/">Korundum Rubyforge site</a> with many improvements. Meanwhile Thomas Moenicke is working on another binding using the Smoke library - PHP-Qt - see last week's <a href="http://commit-digest.org/issues/2007-06-24/">Commit-Digest</a> for details. What with these bindings, and all the action with Kross scripting of KDE apps and Plasma (and PyQt/PyKDE of course), these are exciting times for fans of non-C++ languages in KDE!

<p>Arno Rehn and myself have given a presentation and demo of Qyoto at <a href="http://akademy2007.kde.org/">aKademy 2007</a>. We will show how you can quickly build user interfaces with Qt Designer/uics, easily communicate with other applications via the QtDBus framework, and how other CLR languages, such as IronPython, work with Qyoto.</p>

<p>Qyoto includes very complete coverage of the Qt classes, and optionally the QScintilla text editing framework, along with the 'uics' tool for compiling Qt Designer .ui files to C#, and 'csrcc' a resource compiler based on Qt's rcc. The code is based on QtRuby, which in turn was derived from PerlQt, and it uses the same language independent Smoke library as those bindings. That should mean that Qyoto will be quite solid and mature for a first release.</p>

<p>Now for a brief summary of how the Qt language features look in Qyoto. You define slots by marking methods in your class like this:</p>

<tt>[Q_SLOT]</tt><br />
<tt>public void MySlot(int arg) {</tt><br />
<tt>...</tt><br />

<p>Signals are defined in an interface associated with the class, with the signatures being marked with a 'Q_SIGNAL' attribute:</p>

<tt>public interface IMyButtonSignals : IQPushButtonSignals {</tt><br />
<tt>[Q_SIGNAL]</tt><br />
<tt>void Clicked(bool arg);</tt><br />

<p>To emit a signal you, use a special property called 'Emit' like this:</p>
<tt>Emit.Clicked(true);</tt><br />

<p>Note that Qyoto signals are typesafe and can be checked by the compiler because they must conform to the type signatures defined in your signals interface. Qt properties are mapped directly on C# properties, and so instead of setting the text of a button with a setText() method call:</p>

<tt>mybutton.setText("Hello World!");</tt><br />
<p>You set a 'Text' property like this:</p>
<tt>mybutton.Text = "Hello World!";</tt><br />

<p>If you wish properties to appear in the QMetaObject data for your class, so that you can export them over DBus perhaps, you can mark them like this:</p> <tt>[Q_PROPERTY]</tt><br />
<tt>public string Text {</tt><br />
<tt>...</tt><br />

<p>Connecting a signal to a slot is very similar to C++:</p>
<tt>Connect(quit, SIGNAL("clicked()"), app, SLOT("quit()"));</tt><br />
<p>But note the quotes round the signal/slot signatures.</p>

<p>Well that pretty much sums up the differences - the api is so similar to the C++ api that very little explanation should be needed. Although Qyoto has its own distinctive personality with method names beginning with capital letters and much use of getting/setting properties, the code examples should be very easy to follow, and getting up to speed straightforward for those familiar with Qt. The MonoDevelop IDE works well with Qyoto, and there is a MonoDevelop project file 'Qyoto.mpb' provided, which is a very useful way to browse the classes in the api.</p>

<p>Happy C#/Mono hacking with Qyoto!</p>

