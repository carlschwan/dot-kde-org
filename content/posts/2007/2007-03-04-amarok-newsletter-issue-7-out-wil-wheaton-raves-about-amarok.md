---
title: "Amarok Newsletter Issue 7 Out, Wil Wheaton raves about Amarok"
date:    2007-03-04
authors:
  - "Ljubomir"
slug:    amarok-newsletter-issue-7-out-wil-wheaton-raves-about-amarok
comments:
  - subject: "Get rid of unnecessary borders"
    date: 2007-03-04
    body: "I hope the final version of Amarok 2 will have removed the redundant borders that are present in the screenshots provided by this Amarok Newsletter. See for example the many borders around the left pane titled \"Building Collection Database...\" in this screenshot:\n\nhttp://amarok.kde.org/blog/uploads/amarok_on_mac.png\n\nIn general, I hope all KDE 4 apps will be audited to remove these unnecessary border. Robert Knight, the principal author of Konsole, has started doing just that (http://lists.kde.org/?t=117156103800002&r=1&w=2), but I would think this is a huge job. Perhaps the answer is to change the defaults in these widgets so border=0 unless otherwise specified..."
    author: "AC"
  - subject: "Re: Get rid of unnecessary borders"
    date: 2007-03-04
    body: "I should point out that I am not the \"principal author of Konsole\", just the person who has been maintaining it for the last six months or so.  I believe that most of the code was written by Lars and Waldo.\n\nI also agree with the other comments - it is too early to worry about this yet.  They are little ( but important ) details which can be dealt with in preparation for Beta releases.\n\nEspecially since Amarok looks like it is getting quite a dramatic facelift."
    author: "Robert Knight"
  - subject: "Re: Get rid of unnecessary borders"
    date: 2007-03-05
    body: "You are not \"just\" the person who has been maintaining it for the last six months or so. You are the official \"konsole dude\".\n\nRemember that."
    author: "Robert Scott"
  - subject: "Re: Get rid of unnecessary borders"
    date: 2007-03-05
    body: "I agree totally. I posted this sveral times to the marok lists, but they never seemed to understand - therefore, even if Amarok 2 gets some face lift one should point this out again and again... I still prefer JuK for its look."
    author: "Sebastian"
  - subject: "Amarok is not perfect"
    date: 2007-03-04
    body: "I depends on uses. when you e.g. have large audio files, say 3 h recordings from radio shows then you want to jump to a precise position. The current slider is totally unusable for that, it is not precise enough."
    author: "ben"
  - subject: "Re: Amarok is not perfect"
    date: 2007-03-04
    body: "I think this is more true for kaffeine. There you can't skip e.g. 5 seconds (variable skip; depending on length o/t movie)"
    author: "Mootjes"
  - subject: "Re: Amarok is not perfect"
    date: 2007-03-04
    body: "Yes, intrestingly with certain windows video files and the xine engine using the slider does not work at all."
    author: "ben"
  - subject: "Re: Amarok is not perfect"
    date: 2007-03-04
    body: "Scrolling on the Amarok progress bar appears to move forward and back by 10 seconds regardless of file length.  Just seemed worth pointing out."
    author: "nobody"
  - subject: "Re: Amarok is not perfect - position slider"
    date: 2007-03-07
    body: "I think too that Amarok position slider is way too small to be accurate.\nI like Kaffeine because i can make slider as own toolbar and then it is big and accurate. And i even like more kaffeine ctrl+J combo where user can just scroll with mouse wheel position what s/he likes to move.\n\n\nRight now Amarok position slider is just hint for user what part user is listening. By me, i would add more options for user, how to configure Amarok toolbars, now i cant move where is volume slider and where is search bar..\nIt's not 'a must' wish, but i know many person who would like that, and they really like Amarok!"
    author: "Fri13"
  - subject: "Re: Amarok is not perfect"
    date: 2008-10-21
    body: "Hi,\ncan somebody say me how I can change F/R duration in Amarok? The default value is\n10 seconds, but I want just 5. \n\nLarisa"
    author: "Larisa"
  - subject: "Some more cleanup"
    date: 2007-03-04
    body: "In my view, cleanup is necessary on the buttons themselves. Why on earth would one still have the \"Play\" button when a song is being played. This in my opinion should be automatically changed to \"Pause\". This will save on screen real estate."
    author: "cb"
  - subject: "...?"
    date: 2007-03-04
    body: "Play/Pause has been the default for a couple of years now."
    author: "Ian Monroe"
  - subject: "Re: Some more cleanup"
    date: 2007-03-04
    body: "Play/Pause has been a toggle button for a long time now.  It may be possible that you first used Amarok before the change happened and your tool bars had the individual 'Play' and 'Pause' buttons (if you go into configure toolbars you'll see that theres a 'play' 'pause and 'play/pause', you probably want to replace the 'play' and 'pause' on your toolbar with the 'play/pause' combo).  Or maybe you're just running an ancient version of Amarok?"
    author: "Sutoka"
  - subject: "Early state of development"
    date: 2007-03-04
    body: "Commenting on the the GUI \"cleanliness\" is very premature at this point. The port to KDE4/Qt4 started exactly one month ago and the GUI is currently changing on an almost daily basis as people try out new things. Also, the GUI has until now had fairly low priority as just getting the basics to work was plenty of work in itself\n\nCheer up, there are many months to 2.0 will be released an many things will change before that! :-)"
    author: "nhnFreespirit"
  - subject: "Yay, Phonon"
    date: 2007-03-04
    body: "Looks like Phonon is really doing well, first Noatun now Amarok support it."
    author: "Ben"
  - subject: "Re: Yay, Phonon"
    date: 2007-03-04
    body: "Yea, of the new KDE4 technologies, Phonon is one of the most complete."
    author: "Ian Monroe"
  - subject: "Re: Yay, Phonon"
    date: 2007-03-04
    body: "Well, it plays fine here, so I guess you're right."
    author: "superstoned"
  - subject: "New Layout"
    date: 2007-03-04
    body: "Why on EARTH is the playlist being relegated to second class when it comes to screen real estate? It's the single most important pane in the application!\n\nI hope this \"UI redesign\" screenshot is not reflective of 2.0's UI layout. If it is, please make sure there's an option to switch it back!\n\nI'm having a hard time finding things wrong with Amarok, except for the silly Magnatune integration that you can't compile out, and the obscene memory consumption.\n\nBut the UI, wow. It's flawless!"
    author: "Alistair John Strachan"
  - subject: "Re: New Layout"
    date: 2007-03-04
    body: "I think at this point they're just experimenting with various different layouts to try and see how they can improve the current one.\n\nAlso as for the memory consumption, you can always dig into the source yourself and try to track down where its being eaten, or maybe use valgrind to profile Amarok (or if you don't have the skills for that, find someone you know that does that has free time then pester them to do it for you)."
    author: "Sutoka"
  - subject: "Re: New Layout"
    date: 2007-03-05
    body: "My guess is that a lot of the memory consumption problems are due to the software reuse in the application (SQL backend, UI glitter, Xine engine) and can't easily be stripped down. Reuse is something that I would never say was a bad idea.\n\nDon't get me wrong, it's not as though it's a killer problem. 60-100MB RAM for a piece of software like Amarok is a small price to pay for the features. But it's still pretty heavy (compared to other KDE applications).\n"
    author: "Alistair John Strachan"
  - subject: "Re: New Layout"
    date: 2007-03-04
    body: "Keep in mind that the current state of the playlist is subject to change. We would like to experiment with a \"column-less\" playlist, which will use much less horizontal space."
    author: "Mark Kretschmann"
  - subject: "Re: New Layout"
    date: 2007-03-05
    body: "I look forward to seeing these experiments.\n\nAs a mere user, I just wanted to point out that I feel strongly that Amarok play to one of its greatest features -- the playlist -- and that this attribute is not lose its prominence in a later version of Amarok."
    author: "Alistair John Strachan"
  - subject: "Re: New Layout"
    date: 2007-03-05
    body: "I kind of agree. I'd rather have the context browser on the right, the services maybe on the left and the playlist in the middle.\n\nAs for the playlist, I'd love it to have an Outlook-message-list kind-of look and feel. What I mean is:\n\nTRACK NAME1 ..... 00:00 ..... STAR-RATING\nArtistName - Album\n\nTRACK NAME2 ..... 00:00 ..... STAR-RATING\nArtistName - Album\n\nArtistName and Album on a smaller font-size and maybe in italics.\n\nThat way I believe the playlist would be less crowded..."
    author: "NabLa"
  - subject: "ZOMG basic video support!"
    date: 2007-03-04
    body: "thank you amarok devs :)"
    author: "Patcito"
  - subject: "Re: ZOMG basic video support!"
    date: 2007-03-04
    body: "I compleatly missed the entire argument. Why do the amarok devs not want video? I'm sure there's a good reason but what is it?"
    author: "Ben"
  - subject: "Re: ZOMG basic video support!"
    date: 2007-03-04
    body: "cause Amarok is a music player. The video player feature is designed in mind for things that are music player-ish - eg for podcasts and music videos."
    author: "Ian Monroe"
  - subject: "Re: ZOMG basic video support!"
    date: 2007-03-04
    body: "Makes sense. I just assumed there was more to it than that."
    author: "Ben"
  - subject: "Re: ZOMG basic video support!"
    date: 2007-03-05
    body: "Finally an hope for a better support for 5gen ipod! I waited a lot of time for this feature. A way to manage, play and update video playlist/podcast in linux.\n\nThanks to the amarok team!"
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "preliminary splash screen"
    date: 2007-03-04
    body: "I simply LOVE that slash screen, hopefully it won't be too hard to switch to using that one after the 'real' splash screen replaces it in SVN (always nice to get a good laugh, and that would definitely give one every morning)."
    author: "Sutoka"
  - subject: "CPU consumption, GUI"
    date: 2007-03-04
    body: "What I can only dream of is a KDE music player that takes 0% of my CPU time during playback. This is the case with every \"normal\" player such as XMMS, Beep Media Player and of course any Windows player. Even Linux video players take 0% of CPU time during video playback - they can play an MPEG-4 video *with* MP3 audio together and CPU is at 0%. But just silly standalone MP3 audio playback (without video) in any KDE player, including Amarok, requires around 3-4%. This is really interesting and difficult to understand.\n\nOn the GUI front, the new screenshot looks only very slightly optimistic - the worst thing about the Amarok GUI is the ridiculously small slider used for navigation (seeking in the file), it is so incredibly awkward and difficult to use... In the new redesigned proposal, it is at least a little bit wider. But the layout is more chaotic and the info panel on the right is just way too narrow to be of any use."
    author: "J. M."
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-04
    body: "The only time when my CPU is at 0% is when my computer is switched off... You should really have a so powerful computer if, even for playing a MPEG4/MP3 stream your CPU is at 0%."
    author: "Plop"
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-05
    body: "No, it's just Celeron 2.4 GHz. Of course by 0% I mean \"less than 1%\" - my CPU meter shows only integers. And it's pretty normal when a low-res (320x240 or so) MPEG-4 or similar (WMV, RealVideo) video is played, with MP3 (WMA, RealAudio...) audio in it, the CPU meter shows 0%. But then when I play audio only, in various KDE players, CPU is at 2-20%. While in video players like MPlayer, RealPlayer etc., CPU stays at 0% during audio-only playback, too. So either the CPU meter doesn't show correct numbers, or there's something inherently inefficient about KDE players."
    author: "J. M."
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-05
    body: "if you minimise the application (so that it doesn't need to draw, you will find it drops to close to 0%). mplayer has no gui, so the comparison is totally worthless."
    author: "seb ruiz"
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-05
    body: "No. Firstly, it's not just MPlayer, and also, even if I close the Amarok window (so it's hidden in the tray and nothing is drawn to the screen, even if I turn off the frequency analyzer and the OSD), CPU is still at 2%. While programs such as RealPlayer or Beep Media Player take 0% of CPU even with the full GUI, with the frequency analyzer turned on..."
    author: "J. M."
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-05
    body: "(Of course I'm not saying 2-4% CPU consumption is a big deal - it isn't, I just find it interesting and strange, and wonder what could possibly cause the difference.)"
    author: "J. M."
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-05
    body: "In my case it makes no difference Amarok being the active window, minimised or iconised on the tray: it uses around 9% of my CPU when playin any MP3 file. This is on a Pentium M 1.7. I can see no difference of CPU utilisation when turing off visualisations and OSD.\n\nIsn't this a bit excessive for just playing MP3?"
    author: "NabLa"
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-05
    body: "Keep in mind with reguard to the ui changes--they were just added to trunk this week.  There is still a huge amount of work planned for 2.0; one of the things we really want to spend time on is a much narrower playlist.  Until this gets done, the ui is naturally going to look quite bad."
    author: "Dan"
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-05
    body: "OK."
    author: "J. M."
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-06
    body: "KPlayer is based solely on MPlayer, so it's the closest you are going to get to perfect when it comes to playing you music or movies *and* having a nice interface *and* having the lowest possible CPU usage. KPlayer 0.6.1 is now out BTW! Gotta try it out today."
    author: "plapsky"
  - subject: "Re: CPU consumption, GUI"
    date: 2007-03-06
    body: "Indeed - with KPlayer, CPU is at 0% during playback! Thanks for the tip, it's refreshing finding such an efficient player for KDE."
    author: "J. M."
  - subject: "gapless"
    date: 2007-03-05
    body: "Gapless has always been broken for me. MPD has been the only player aside from using the command line ogg123 that plays flawless gapless playback. I truly love Amarok and its interface and integration, but c'mon, using xine is delaying the advancements that I consider basic features of music. We need a good audio player to go with the already great interface."
    author: "scooter"
  - subject: "Re: gapless"
    date: 2007-03-05
    body: "For me, gapless Ogg Vorbis playback in Amarok sometimes works, sometimes it doesn't. I mean, sometimes there's a noticeable gap e.g. between track 2 and 3, but when I go back and play the section again, there's no gap between them. Or vice-versa. So yes, I agree it would be great if it worked flawlessly every time."
    author: "J. M."
  - subject: "Amarok bug"
    date: 2007-03-10
    body: "Amarok file menu is buggy. See me browsing the files of my Robert Johnson files using the \"down key\". Strange jumps.\n\n"
    author: "pod"
---
The <a href="http://ljubomir.simin.googlepages.com/awnissue7">seventh issue of the Amarok Newsletter is out</a>. We talk about Amarok's success in the LinuxQuestions.org yearly poll, new features in the upcoming Amarok 2, and continue to point out interesting related projects. Read on for some Amarok lovin' from Wil Wheaton.


<!--break-->
<p>
In the other news, Wil Wheaton from Star Trek <a href=http://suicidegirls.com/news/geek/20496/>reviews Amarok</a>. When you see quotes like <i>"Amarok is much more than just another music player or iTunes clone; in fact, it blows iTunes away. It is Kryptonite to iTunes Superman. It's the Death Star to iTunes' Alderaan."</i> and <i>"[...] it's like having Uncle Joe Benson and Rodney Bingenheimer sitting in your living room with you"</i>, you know you can't miss it!

