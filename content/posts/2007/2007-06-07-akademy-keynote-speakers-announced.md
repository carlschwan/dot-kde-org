---
title: "aKademy Keynote Speakers Announced"
date:    2007-06-07
authors:
  - "jriddell"
slug:    akademy-keynote-speakers-announced
---
The <a href="http://akademy2007.kde.org/">aKademy 2007</a> team is pleased to announce the keynote speakers for <a href="http://akademy2007.kde.org/conference/programme.php">this year's conference</a>.  The opening talk will be from Lars Knoll of Trolltech who will tell us about their plans for Qt 4.4 and their relationship with KDE.  <a href="http://www.markshuttleworth.com/">Mark Shuttleworth</a> of <a href="http://www.canonical.com">Canonical</a> will be talking on the 10 Challenges to Open Source.  On Sunday, Dan Kohn of <a href="http://www.linux-foundation.org/">The Linux Foundation</a> will talk on the state of Linux Standardisation on the Desktop.  Continuing the week the  <a href="http://akademy2007.kde.org/codingmarathon/schoolday.php">Edu and Schools Day</a> will be opened by Sulamita Garcia with a talk on <a href="http://www.intel.com/intel/worldahead/classmatepc/">Intel's Classmate PC</a>.
<!--break-->
<p>Once the talks are over you may be interested in refreshments, so on Saturday we have at the <a href="http://www.chateaugateau.co.uk/">Chateau</a>, an evening of art, music and food.  On the Monday we will be welcomed by Glasgow's <a href="http://en.wikipedia.org/wiki/Lord_Provost">Lord Provost</a> with a reception at <a href="http://www.glasgow.gov.uk/en/YourCouncil/Council_Committees/CityChambers/">the city chambers</a>.  The buffet at the reception is provided by our platinum sponsor <a href="http://www.trolltech.com">Trolltech</a>.  As previously announced, on the Thursday our <a href="http://akademy2007.kde.org/codingmarathon/day-trip.php">Day Trip to Loch Lomond</a> will have a barbecue on the beach.</p>

<p>Remember that while attendance is free, you must <a href="http://www.kde.org.uk/akademy/">register to come to aKademy</a> (including Edu day and the Text Summit).</p>















