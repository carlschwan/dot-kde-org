---
title: "People Behind KDE: Troy Unrau"
date:    2007-05-18
authors:
  - "dallen"
slug:    people-behind-kde-troy-unrau
comments:
  - subject: "I have waited long enough!"
    date: 2007-05-17
    body: "Please release KDE 4.1.1 today! While it is interesting to read about all the cool people and neat technologies behind the upcoming KDE 4, and while I appreciate all the hard work, please bear in mind that many people (such as myself) have put in a lot of waiting into the forthcoming release already!\n\nAt this point someone will usually point out that we should contribute something else than waiting: \"You don't have to be a coder\" etc. I would like to respond to this that we are all unpaid volunteers, and if waiting is the form of contribution we have chosen, you can't force us to do something else.\n\nAgain, please release KDE 4.1.1 today! I have come to understand that many of the new frameworks in the KDE 4 series will not be fully exploited by applications until 4.1 at the earliest, and I believe that one bugfix release will also be needed to make the new desktop all it can be.\n\nTo keep this on topic, Troy seems to be a really cool guy. Perhaps he can be the one to release KDE 4.1.1? Don't delay, Troy! Do it today.\n\nThank you all!\n- Edmund (a formerly patient fan)"
    author: "Edmund T. Roll"
  - subject: "Re: I have waited long enough!"
    date: 2007-05-17
    body: "just try the Live CD of the KDE 4 alpha, that is something you can do today \nits totaly not ready !!"
    author: "Pieter Vande Wyngaerde"
  - subject: "Re: I have waited long enough!"
    date: 2007-05-18
    body: "Errr... Thanks for sharing, Mr Roll."
    author: "Patentia A. Waite"
  - subject: "Re: I have waited long enough!"
    date: 2007-05-18
    body: "actually: Mr. T.Roll \n\nI do not think it is a coincidence ;-)"
    author: "MK"
  - subject: "Re: I have waited long enough!"
    date: 2007-05-19
    body: "Nor was the name I chose for my reply... :)"
    author: "Patentia A. Waite"
  - subject: "Troy Unrau"
    date: 2007-05-17
    body: "Troy Unrau, you are a cutie! (And this time I'm serious.)\n\nThanks for all your work on KDE!"
    author: "AC"
  - subject: "Thanks"
    date: 2007-05-18
    body: "A big Thanks for those great articles\nI Thnk i cant say anything more...\n (Hey!there were some talks about Kwin_composite Article, where are you Troy?!!!)\nIm waiting ;)"
    author: "Emil Sedgh"
  - subject: "Re: Thanks"
    date: 2007-05-18
    body: "It's a work in progress at the moment - I have already got the responses I needed from Seli - now I just need to get kdelibs building again after being gone for a few weeks so I can take the screenshots I need :)\n\nAlso in the works is a Konsole article - it's seen a lot of work :)"
    author: "Troy Unrau"
  - subject: "congrats"
    date: 2007-05-18
    body: "hey Troy, congrats on all your great articles, they've always been fun to read ;-)\n\nLet's hope you'll write a lot more!"
    author: "superstoned"
  - subject: "my personal qotd ;-)"
    date: 2007-05-18
    body: "> These people are the nicest people in the whole Marble widget--errr, world.\n\n*lol* \n\n"
    author: "Torsten Rahn"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we travel to North America for the first time this series to talk to an IRC veteran and the author of ground-shaking, in-depth promotional articles on the interesting road towards KDE 4 - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/troy/">Troy Unrau</a>.
<!--break-->
