---
title: "KDE 3.5.8 Release Fixes Hundreds of Bugs"
date:    2007-10-17
authors:
  - "sk\u00fcgler"
slug:    kde-358-release-fixes-hundreds-bugs
comments:
  - subject: "Thank you very much"
    date: 2007-10-16
    body: "Thanks for another release of KDE 3.5. I appreciate all work you do to make the existing version more stable and faster while we wait for KDE 4.\n\nBTW, the release announcement has just been Dugg a few minutes ago:\nhttp://digg.com/linux_unix/KDE_3_5_8_Released"
    author: "Boris"
  - subject: "*dances around*"
    date: 2007-10-17
    body: "KDE rocks my entire closet of socks! I updated to .8 today on gutsy and things are snappier than ever! Keep the great work up!"
    author: "A KDE Advocate"
  - subject: "Long live, KDE3!"
    date: 2007-10-17
    body: "I hope KDE 3.x will not be abandoned after KDE4 is out. The former is rock solid nowadays."
    author: "Artem S. Tashkinov"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-17
    body: "KDE 3.5 will probably be supported for years, but active development has (and also should have) shifted to KDE 4. That said, there are already some nice things in the pipeline for 3.5.9. :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-17
    body: "Thank you, I appreciate knowing that support will not cease.\n\nI find that there are some issues with supporting libraries that are going to need to be addressed if that is going to be a reality.  Such issues might be fixed by the maintainers of the libraries or the problems might continue to occur with other libraries.\n\nIt appears that the problems with aRts and Glib have been resolved which is good to see although I'm not clear on exactly how it was fixed.\n\nI note that I build from source so some of these problems may have been fixed by your distro.  I am still having multimedia issues (not all of them KDE).\n\nKaboodle and Noatun have simply quit working although the embedded audio player still works.  I haven't been able to figure out the cause.  This isn't a large issue since Kaffine and KPlayer work fine.\n\nKView and KPat won't open JPEG files.  This must be a specific issue with imlib since some other KDE apps have no problems.  I also don't seem to get new thumbnails of JPEG files which probably isn't an imlib issue.\n\nKDELibs won't build against the current version of LibArt_LGPL.  It is possible that this is only a header issue.\n\nYes, I need to write bugs for these as well as do further research to find the exact causes.\n\nIAC, continued support is appreciated."
    author: "James Richard Tyrer"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-17
    body: "\"Kaboodle and Noatun have simply quit working although the embedded audio player still works. I haven't been able to figure out the cause. This isn't a large issue since Kaffine and KPlayer work fine\"\n\nKaboodle and Noatun both uses aRts while Kaffine and KPlayer don't, so your aRts install is most likely not correct. Or something is preventing aRts from accessing the soundcard. \n\nWhat you mean by the embedded audio player, im not sure. But if it's something browser embedded, it's most likely handled by Kaffine and KPlayer. If it's system sounds, they may be configured to play directly with alsaplay or something going directly to your soundcard.\n\n"
    author: "Morty"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-20
    body: "The \"Test Sound\" button in the \"Sound System\" plays a sound, so I doubt that aRts is totally broken as you presume.  Also, the System Notifications work.\n\nHowever, this might be an aRts issue.  I am using the current SVN branch 1.5.\n\nActually the name is \"Embedded Media Player(Kaboodle Component)\".  This ships with KDE.  I think that the same code is used for the \"Navigation Panel\" \"Media Player\" and the \"Previews\".  However, I note that EMP(KC) only works for audio, trying to play a video results in Konqueror hanging.\n\nOriginally, all KDE sound went through aRts.  I don't know what currently uses aRts and what doesn't if KDE is built \"--with-arts\".  If this works for you, what aRts version are you using and where did you get it?"
    author: "James Richard Tyrer"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-20
    body: "Yes, you are right your aRts are not totally broken, but your KDE install clearly is. The full name of Embedded Media Player spells it out, Kaboodle Component or in technical terms the Kaboodle kpart. You have the kpart working, but not the standalone player using the kpart. Don't you find theat a little odd? Sounds like you have multiple versions lying around,linking issues, broken Sycoca database, old broken .desktop files or a combination of those issues.\n\nI don't think using Kaboodle(embedded or not) playing video will work very well, the video support in aRts newer reached it's potential. Not sure if it's even works/are enabled anymore. Try using Kaboodles video brother Codeine for that.\n\nAs for aRts usage, the programs in the kdemultimedia package uses it. Kaboodle  Noatun and JuK(JuK can alsouse direct output with the akode lib). Amarok  can use multiple backends, but usually defaults to Xine not aRts. All the video players(Codeine,Kaffeine, KPlayer, KMPlayer etc.) use Xine or MPlayer. One neat but not to commonly used trick is that Xine actually can use aRts as output for sound, and it solves the old problem of applications locking the sound device(I think more use of this would have lesend  some of aRts historic bad reputation).\n\nI have never actually had any major problems with aRts, either self compiled or as distribution packages all the wayfrom the KDE2 days. On this machine I'm currently using aRts 1.5.8 and KDE 3.5.8, from backports Suse 10.1."
    author: "Morty"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-23
    body: "You are using an aRts binary which was probably patched by SuSE.\n\nSo, it appears that the aRts issues have not been resolved and I will have to try going back to a patched copy.  If I can find a copy of an SRPM for the SuSE binary, I will try that."
    author: "James Richard Tyrer"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-29
    body: "I'm having this same problem with OpenSUSE.  Not all of my sounds work at the moment.  Amarok w/gstreamer is fine, but I don't have any system sounds, even tough the \"test\" buttong in the kde control center module works to play a sound.  Any fixes for this yet?"
    author: "Jake"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-17
    body: "The \"longer life\" of the 3.5 branch is indeed an excellent news. IMHO, and although the 4.x serie is very promising, this branch is actually the most complete and most convincing alternative to proprietary solutions, for home-users and corporate users.\n\nI want to thank you all KDE guys and girls and allow myself to stress how much KDEPIM  (and the synchronization part) is a decisive piece to improve for wider adoption in corporations (with Koffice of course) !\n\nMany thanks folks,\n\nJA"
    author: "Jean-Alexandre"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-17
    body: "That are very good news. I use KDE every single day. Thank you for work."
    author: "Yttrium"
  - subject: "Re: Long live, KDE3!"
    date: 2007-10-17
    body: "I think it unlikely that KDE 3.x will be abandoned til at earliest 4.1, when things settle down a little bit and 4 gets as solid as 3."
    author: "G2g591"
  - subject: "Already in Rawhide"
    date: 2007-10-17
    body: "It's already in Rawhide. :-) Updates for Fedora 7 and Fedora Core 6 are being prepared and should be ready in about a week."
    author: "Kevin Kofler"
  - subject: "KDE4 Beta 3"
    date: 2007-10-17
    body: "I wonder there is no KDE4 beta 3 released yet. Wasn\u00b4t that scheduled to October 15th?"
    author: "Duuuhk"
  - subject: "Re: KDE4 Beta 3"
    date: 2007-10-17
    body: "It will be released on Thursday due to some slight schedule changes."
    author: "Erunno"
  - subject: "Bugs resolved"
    date: 2007-10-17
    body: "These two bugs (almost the same) seem to be solved, at least for me:\n\nhttps://bugs.kde.org/show_bug.cgi?id=126286\nhttps://bugs.kde.org/show_bug.cgi?id=133606\n\nThey are related to Kopete, which shouldn't access tv card at startup.\n\nThanks!!"
    author: "Me"
  - subject: "Re: Bugs resolved"
    date: 2007-10-25
    body: "Ah yes, I ended up with several broken MythTV recordings due to this before I changed the permissions so that Kopete didn't have access to my TV card. "
    author: "makomk"
  - subject: "Thanks!"
    date: 2007-10-17
    body: "Holy /crap/, you guys outdid yourselves! So many bugs were fixed, this is most excellent. :) Thank you all for the great work!"
    author: "Balinares"
  - subject: "KMail"
    date: 2007-10-17
    body: "Hey, what is this \"favorite folders\" a la Outlook? :) Nice addition altough I have too many accounts and too little screen estate to enjoy it.\n\nBut is it only my impression or IMAP support has suffered a slow-down in respect to KDE 3.5.7? I'm on Kubuntu Gutsy (well, just dist-upgraded to benefit KDE 3.5.8 new packages)"
    author: "Vide"
  - subject: "Re: KMail"
    date: 2007-10-17
    body: "I found this favorite folder only on Ubuntu :-( \nI guess it comes from the enterprise version of kmail. Why is it not present in the normal version ?"
    author: "xavier"
  - subject: "Re: KMail"
    date: 2007-10-19
    body: "Yes, this comes from kdepim-enterprise. You'll see it on Fedora shortly (Fedora 7 updates, Fedora 8)."
    author: "Kevin Kofler"
  - subject: "finally!"
    date: 2007-10-17
    body: "> Bugs in handling HTTP connections have been fixed\n\nThank god, finally! Does dilbert.com work properly yet?"
    author: "anon"
  - subject: "Re: finally!"
    date: 2007-10-17
    body: "I've been reading dilbert.com daily since -- well, must've been KDE 1.12 -- with konqueror. No problems at all..."
    author: "Boudewijn Rempt"
  - subject: "Re: finally!"
    date: 2007-10-18
    body: "Yeah, but it doesn't work right. Some clunk with HTTP makes it insanely slow there."
    author: "anon"
  - subject: "Re: finally!"
    date: 2007-10-18
    body: "You're missing a word or two -- \"for\" and \"me\" -- because I don't doubt it's slow for you, but for me, konqueror loads www.dilbert.com right speedily."
    author: "Boudewijn Rempt"
  - subject: "Re: finally!"
    date: 2007-10-20
    body: "I'd say the same as Boudewijn, newer had any problems and the speed is good. \n\nOn the other hand I have seen \"speed problems\" and lots of timeout problems on some sites with Konqueror, while other browsers like Opera or Firefox are ok. But that has always been easily solved by disabling IPv6 support in KDE, still lot of equipment out there not handling it well."
    author: "Morty"
  - subject: "Update this page, please.!"
    date: 2007-10-17
    body: "Hy folks,\n\nI'm saving money to send another donation. KDE rocks.!! This release shows how KDE E.V. is professional and responsible with the users. KDE 4 is a huge chalange, but the bug hunting and developing of 3.5 is on the road.\n\nPlease, update this page. It stopped at kde 3.3.2 and display Conectiva Linux as a linux distribution with KDE.\n\nhttp://www.kde.org/download/distributions.php\n\n:-)"
    author: "from Brazil"
  - subject: "Konqueror profiles"
    date: 2007-10-17
    body: "KDE gets more awesome from release to release :-)\n\nReally, i cant stand all those other Desktops anymore... Whenever i try another Desktop (or operating system without KDE), i begin to miss several features after a short time and then i just feel the urge to throw it all away... Well, no bashing, but the conclusion that KDE is simply perfect for me :-)\n\nOne big thing that bothers me for a long time now is Konquerors profile management, and i still hope that this will be fixed somehow...\n\nIts nearly impossible to have independent konqueror profiles, like having different toolbars per profile, at least not without some \"hacking\" on the config files like described here: \n\nhttp://wiki.archlinux.org/index.php/KDE_Desktop_Tricks#Konqueror_With_Completely_Independent_Profiles\n\nThis \"hack\" works, but not for every setting... It would be optimal to have some sort of profile management like in KDE4's Konsole, where you can edit *every* setting _per profile_, like toolbars, the tabbar and various settings... \n\nTHIS is the ONLY reason why i do not use Konqueror for browsing..."
    author: "Jan"
  - subject: "Re: Konqueror profiles"
    date: 2007-10-17
    body: "One other thing that may be worth fixing is some kind of DNS cache built into the kio slaves. You can do this by running pdnsd externally or just using a proxy server, but that's additional work on the user side.\n\nWell I use konqui for browsing but the thing Jan describes is a bit enerving indeed."
    author: "Dunkelstern"
  - subject: "Konqueror 5.0?!"
    date: 2007-10-17
    body: "would you mind to tell me what \"Konqueror 5.0\" is? its marketshare is 0,02% for september 2007. \nlook at http://marketshare.hitslink.com/report.aspx?qprid=7&qpcustom=Konqueror+5.0"
    author: "twilightzone"
  - subject: "Re: Konqueror 5.0?!"
    date: 2007-10-17
    body: "Hy\n\nOpen your Konqueror's Config Dialog and look at How your browser show it's ID. My Konqueror is configured with this ID tag.\n\nMozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.7 (like Gecko)\n\n\n:-)"
    author: "from Brazil"
  - subject: "Re: Konqueror 5.0?!"
    date: 2007-10-17
    body: "mine does not identify itself. it's paranoid. ;-) \nbut yours says \"... Konqueror/3.5 ...\" \n3.5 != 5.0\n"
    author: "twilightzone"
  - subject: "Re: Konqueror 5.0?!"
    date: 2007-10-20
    body: "His also says:\n\nMozilla/5.0 (...) KHTML (like Gecko)\n\n5.0=5.0 :)\n\nAs was said before, it's buggy version check. Since the UA includes Gecko, it assumes that it's a Mozilla browser. I guess this was intentional (I rarely see a page that works in FF and not Konqueror), but has some unintentional results.\n"
    author: "riddle"
  - subject: "Re: Konqueror 5.0?!"
    date: 2007-10-17
    body: "My guess would be buggy method used to check the browser's version."
    author: "Sutoka"
  - subject: "Re: Konqueror 5.0?!"
    date: 2007-10-18
    body: "these stats are not perfect... they represent only an small sample of the internet population.\n\nlooking at browser stats for http://dot.kde.org/ or for http://slashdot.org/ would give totally different results. while those two I just mentionned would give interresting results I'd be happy to look at. The #1 interresting browser stats I'd like to check is those cumulated by the famous http://google.com/ domain ;)\n\n"
    author: "Mathieu Jobin"
  - subject: "KDM 4 ?"
    date: 2007-10-17
    body: "Has anyone information about whether KDM has been ported to qt4 ?"
    author: "Frank"
  - subject: "Re: KDM 4 ?"
    date: 2007-10-19
    body: "It has in the 4.0 betas (not in 3.5.8 of course), I'm not sure how well the KDE 4 KDM works by now though."
    author: "Kevin Kofler"
  - subject: "But System Settings is borked"
    date: 2007-10-30
    body: "Yes, I like the 3.5.8 KDE release. However, it broke System Settings, which when access via K Menu produces a Crash Handler. This problem has been reported to launchpad. This problem was not present until I upgraded to 3.5.8. It would be nice if the problem was identified and fixed ASAP, please????"
    author: "Paul Loughman"
---
The KDE community today 
  <a href="http://kde.org/announcements/announce-3.5.8.php">released KDE 3.5.8</a>.      While the developers' main focus lies on finishing KDE 4.0, the stable 3.5 series     remains the desktop of choice for the time being. It is proven, stable and well    supported. The 3.5.8 release with its literally hundreds of bugfixes has again    improved the users' experience. The <a href="http://www.kde.org/info/3.5.8.php">3.5.8 info page</a> lists how to get the sources and distribution packages.


<!--break-->
<p>The main focus of improvements for KDE 3.5.8 is</p>
        
    <ul>
        <li>
        Improvements in Konqueror and its web browsing component KHTML. Bugs in handling 
        HTTP connections have been fixed, KHTML has improved support
        of some CSS features for more standards compliance.
        </li>
        <li>
        In the kdegraphics package, lots of fixes in KDE's PDF viewer and Kolourpaint, a painting
        application, went into this release.
        </li>
        <li>
        The KDE PIM suite has, as usual, seen numerous stability fixes, covering KDE's email client
        KMail, the organizer application KOrganizer and various other bits and pieces.
        </li>
    </ul>
 
<p>
  For a more detailed list of improvements since the
  <a href="http://www.kde.org/announcements/announce-3.5.7.php">KDE 3.5.7 release</a>
  on the 22nd May 2007, please refer to the
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_7to3_5_8.php">KDE 3.5.8 Changelog</a>.</p>


