---
title: "KDE Commit-Digest for 18th February 2007"
date:    2007-02-19
authors:
  - "dallen"
slug:    kde-commit-digest-18th-february-2007
comments:
  - subject: "Thanks!"
    date: 2007-02-19
    body: "As usual thanks a lot to Danny for the commit-digest which helps reducing the waiting time for KDE 4 :-) \n\nBy the way: great to see that my favorite game (space duel) is ported to KDE 4 - thanks to the coder who makes this happen! "
    author: "MK"
  - subject: "Re: Thanks!"
    date: 2007-02-19
    body: "I concur, Danny.  You spend so much time on this thing every week, it really is a great piece...\n\n/me raises a glass..."
    author: "Troy Unrau"
  - subject: "KGeog - Plans for KDE4?"
    date: 2007-02-19
    body: "Any plans to move KGeography to vector or SVG map formats, possibly by using Marble?  They would be far easier and more flexible and offer opportunities for new features.  CIA Fact Book and Wikipedia integration might be good too.\n\nhttp://www.kdedevelopers.org/blog/551\nhttp://websvn.kde.org/trunk/playground/base/marble/\n\nJohn."
    author: "Odysseus"
  - subject: "Re: KGeog - Plans for KDE4?"
    date: 2007-02-19
    body: "yeah, that sounds reasonable"
    author: "matt"
  - subject: "Re: KGeog - Plans for KDE4?"
    date: 2007-02-20
    body: "AFAIK, Marble is pixel-based."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: KGeog - Plans for KDE4?"
    date: 2007-02-21
    body: "No, it's not. It's a combination of vectordata as well as pixel data. And Odysseus is right that it would indeed make much more sense to make use of that data (instead of \"drawing\" static maps that don't share a coordinate system by hand in a pretty non-standard format).\n\nTorsten - just started to work on Marble again ..."
    author: "Torsten Rahn"
  - subject: "pixieplus"
    date: 2007-02-19
    body: "Dirk Stoecker committed changes in /trunk/kdenonbeta/pixieplus/app:\n\nWasn't pixieplus mosfet's invention? I thought it had disappeared in just the same myterious way as mosfet himself. I sometimes wonder what he's doing today. \n\nFortunately, we now have a new graphics god named Zack :)"
    author: "anonymous"
  - subject: "Re: pixieplus"
    date: 2007-02-19
    body: "Yep, one and the same if you check the CVS.\n\nI do agree on the God-like properties of Zack on the graphics front. Very cool stuff comes out of his direction lately!"
    author: "Andr\u00e9"
  - subject: "Re: pixieplus"
    date: 2007-02-19
    body: "Speaking of which. Does anybody knows what's made of Mosfet this days. \nHe did some really cool stuff for kde. "
    author: "RandomOne"
  - subject: "Dolphin Question"
    date: 2007-02-19
    body: "2 questions about Dolphin: \n\n1. Is the purpose and scope of Dolphin written somewhere? For example, will it always be a simple(r) file manager, how does it expect to deal with possible added functionality, such as a shortcut for a distro's package installer. And could a \"webbrowser mode\" ever be added or not? Note: these are NOT feature requests of mine, just want to know which direction Dolphin is going exactly.\n\n2. Will it be possible to use Dolphin in a consistent way instead of Konqueror all/most of the time? For example, I'm using Dolphin on my desktop now (kicker shortcut to it), but most programs will still launch Konqueror when they need a file manager. So I find myself half of the time in Dolphin and half of the time in Konqueror when I actually want Dolphin.\n\nI love the Dolphin layout, but 10% of the time I do use Konqueror though, because of certain added functionality it has. So how does KDE intend to make its desktop more consistent? Another example is the Mac OSX-style taskbar, which only shows menu items for certain programs (I think only the KDE ones, e.g. Audacity and Firefox don't have a menu).\n\n"
    author: "Darkelve"
  - subject: "Re: Dolphin Question"
    date: 2007-02-19
    body: "Hi,\n\n> 1. Is the purpose and scope of Dolphin written somewhere?\n\nI think Aaron explains it quite well on his blog: http://aseigo.blogspot.com/2006/12/on-oxygen-on-dolphin.html. On the official Dolphin home page you can find some further informations: http://enzosworld.gmxhome.de. Please also have a look at the 'News' section, where I tried to summarize some things concerning the move to kdebase.\n\n> For example, will it always be a simple(r) file manager, how\n> does it expect to deal with possible added functionality,\n> such as a shortcut for a distro's package installer.\n\nDolphin will focus on being a file manager only, which is easy to use. But this does not mean that it will offer less features: if features are requested by users we are open to include them...\n\n> And could a \"webbrowser mode\" ever be added or not?\n\n... except for the feature of webbrowsing ;-) Seriously: Dolphin does not and cannot replace Konqueror. As Aaron explained on his blog both applications have a different target group of users.\n\n> 2. Will it be possible to use Dolphin in a consistent way\n> instead of Konqueror all/most of the time? For example,\n> I'm using Dolphin on my desktop now (kicker shortcut to it),\n> but most programs will still launch Konqueror when they need\n> a file manager. So I find myself half of the time in Dolphin\n> and half of the time in Konqueror when I actually want Dolphin.\n\nThis is already possible in the KDE3 version of Dolphin. Please check the FAQ section of the Dolphin manual how to configure this. For sure it will be also configurable in KDE4 whether Dolphin should be started as default or Konqueror.\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Dolphin Question"
    date: 2007-02-19
    body: "Well, as I said, these were not really feature requests. More like, I'm thinking about my own workflow, and Dolphin fits in there, but webbrowsing, I don't know.\n\nAnd about features, I understand you will not cut down on features for the sake of usability? I'm imagining over a dozen 'action' icons in the \"Information\" sidebar. Then again, the Dolphin developers probably are sensible enough to prevent those things :p \nOr, in yet *other* words, the reason I like Dolphin is because it's so clean and simple. I hope it stays that way :)\n\nAbout sharing code with Konqueror, I think I already witnessed that: I've installed mplayer-thumbs on my desktop, and in Dolphin they show as well. Which is very nice B) Personally I use that for my Video folder.\n\nOh, and thanks for the info."
    author: "Darkelve"
  - subject: "Re: Dolphin Question"
    date: 2007-02-19
    body: "these 'actions' depend mostly on installed .desktop files and services. So cleaning up those will help dolphin, but there isn't much dolphin can do when users install a 100th of these desktop files..."
    author: "superstoned"
  - subject: "Re: Dolphin Question"
    date: 2007-02-19
    body: "For a user (God I hate that word) it's his own decision. But I hope the KDE developers won't end up putting dozens of these actions (slight exaggeration) into Dolphin *by default*. Well, not unless they can keep Dolphin clean, simple and efficient."
    author: "Darkelve"
  - subject: "Re: Dolphin Question"
    date: 2007-02-19
    body: "Or distributions, for that matter."
    author: "Darkelve"
  - subject: "Re: Dolphin Question"
    date: 2008-04-22
    body: "i have a question on the whole recueing dolphins. do they go back into the wild?\n\n"
    author: "chelsy"
  - subject: "How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "Just wondering how do Khalkhi and decibel relate to each other.  Do they overlap, can they just live next to eacht other?\n\nFrom the article \"Most applications currently have their support for actions on persons and their state, like email or chat, hardcoded\", isn't this what decibel is about?\n"
    author: "Richard"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "As far as I understand, Khalkhi is a contacts database, while Decibel is a framework for communicating with someone else (typical use case, collaborative editing in kword/whatever between two distant computers), Decibel will (hopefully) retrieve contact information from Khalkhi."
    author: "Cyrille Berger"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "Khalkhi and Decibel are suppossed to work together :)\n\nFor KDE4 Khalkhi is going to have two aspects: The person data layer and the services. The person API layer is planned to become the successor of KABC, that is, a convenience layer around the data about persons stored in Akonadi, so one has in her code only to deal with classes like Persons, Groups, and lists of them. The services thing is what Khalkhi is basically for KDE 3, the option to offer all possible actions on a person or seeing all states there are plugins for, without needing to know anything about the semantics. So KAddressbook would offer chatting, without really needing to know about it, because the semantics do nothing to the logic of the program.\n\nDecibel would deal with both aspects, too. It has two kinds of data that are stored for Persons, statics like chat address, and dynamic/volatile like the presence status. These data might either be stored and retrieved through the Khalkhi layer, or directly in Akonadi. But that needs to be worked out, discussion is just starting.\n\nThen there should be Decibel service plugins for the Khalkhi services. Programs that need the semantics of chatting or other realtime data exchange would work with Decibel directly. As the Khalkhi service plugins have a \"DoNotShowIn=X\" parameter Decibel services for Khalkhi and Decibel should never conflict :)"
    author: "Frinring"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "still, this isn't clear to me. could you elaborate on this a bit more, maybe even with a graph and/or some examples?"
    author: "superstoned"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "Oha. So I will see to find some time to add some documentation. I was trying to concentrate on the code, because running code and programs should be the best description ;) Might take me up to three weeks, so please be patient.\n\nCould you help and say what is clear and what is hard to understand so far?\n\nPerhaps it also helps to read what got me started into Khalkhi:\nhttp://frinring.wordpress.com/2006/06/11/its-about-contacts/\n"
    author: "Frinring"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "Reading through your blog (I read it before), it sounds very similar to decibel, as far as I know it. Though decibel has a daemon and is more focused on cooperation, your framework seems more focused on managing and accessing the data and contacts, closer to Akonadi, perhaps. It seems to me what Khalkhi does could be done in several ways - it could be a like Khalkhi is now, a 'doorway', easy API around Akonadi, cooperating perhaps with Decibel. But it could also be integrated in Akonadi, or in Decibel, right?\n\nI'm not sure if I got this right, and even if I get it, I'm not going to argue for any of these three roads. Why? I think it's more important to have you on board for KDE 4 - developing stuff, adding ideas (which might be even more valuable) than to have two instead of three API's each dealing with contacts/presence/data/etc from a slightly different, perhaps sometimes overlapping, point of view.\n\nOf course, overlap means duplicate work, and you guys (not just you but also Thobias and the Akonadi dev's) should avoid that. But as it seems to me now, having these three, each focused differently, can only enhance innovation and new ideas. So I urge you to get in contact even more with KDE4, get your stuff in trunk, get it for review, and available for the KDE 4 developers. Working together will ensure possibilities are maximized, constraints are minimized, and more fun of course ;-)\n\nAnd are you going to FOSDEM? We could have a talk, set up an interview, get this on the dot as a news article or maybe part of the pillars of KDE 4 series (I don't make them but I'm sure Nathan wouldn't mind if I tried to help ;-))\n\nNo FOSDEM - still an article could be usefull. Explain the differences, advantages, disadvantages, possibilities, ideas..."
    author: "superstoned"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-20
    body: "No FOSDEM for me. And an article, once Khalkhi works and is far enough to get into any release, perhaps in the end of march. Until then it stays just one of the halfbaken things around, not ready for consumption ;)\n\nKhalkhi will/should be to Akonadi, what KABC is to the KABC resources. So being the layer around Akonadi for data of persons means integrating with Akonadi.\n\nThe difference to Decibel is that Decibel cares for the stuff that has the semantics of realtime (a)synchronous communication/data exchange, nothing more, nothing less (AFAIU). Khalkhi/Akonadi stores everything without looking at the semantics, the data are just blobs, interpreted by handler plugins, the services just actions.\n\nDecibel is kind of an engine that fills the data related to realtime communication with life. Something comparable from Khalkhi's abstract point of view would be the RSS framework Syndication in kdepimlibs. A Syndication demon could/can retrieve and buffer new blog entries from a person. The static data would be the person's blog feed urls, and the volatile/dynamic data the number of new, unread blog entries. There will be programs that make explicit usage of it and access Syndication directly, other will just use it implictly by some Syndication plugin services for Khalkhi (\"open latest blog entry\", \"number of unread\", ...) without knowing it, because they don't need to and just asked for all possible services. See the pattern?\n\nThere really shouldn't be any overlap between Khalkhi, Akonadi and Decibel. Code should use Khalkhi and Decibel in parallel, they would add to each other, Khalkhi delivering the general data (from the programs POV) and Decibel enriche all that what is needed for realtime communication. But that is future talk, let's see how and when it works... :)"
    author: "Frinring"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-20
    body: "but can't Akonadi tell you about new data from a person, new blogs etc? (I'm trying to understand it because I'd like to mention Khalkhi when giving my KDE 4 presentation at fosdem).\n\nMaybe you can send me an email, so we can discuss this more properly, maybe even using skype or something ;-)"
    author: "superstoned"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "I think I understood this, but I may be wrong, I think it works like this\n\nthere is two relivent types of data:\n\nPeople, including groups of people. This is stored in Akonadi \n\nServices, like MSN, ICQ. Probobly stored in Akonadi\n\nAnd they relate, a person has several services. \n\n\nKhalkhi is a wrapper around Akonadi, that filters out everything except filters and services. \n\nDecibel is framework that provides the data about people and services to other applications. It also provides the user's prefered way of talking to various people and real time information like, \"are they logged into ICQ\"? If an aplication wants to open a chat with a person Decibel can arrange it.\n\nDecibel may or may not use Khalkhi to get data from Akonadi. That still needs to be decided.\n\n\n\nWhat I don't know is, what happens to Khalkhi if Decibel goes straight to Akonadi? decibel provides the same data to anyone who asks.\n\n\nMan thats like another language :)"
    author: "Ben"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "edit, I read \nhttp://frinring.wordpress.com/2006/06/11/its-about-contacts/ and things are a bit clearer. \n\nKhalkhi has more data than phonon wants, so even if phonon goes straight to Akonadi Khalkhi will still be their to provide an easy wrapper for address books and such."
    author: "Ben"
  - subject: "Re: How do Khalkhi and decibel relate"
    date: 2007-02-19
    body: "I think you meant decibel, not phonon :)"
    author: "Patcito"
  - subject: "When will Sonnet pass the review?"
    date: 2007-02-20
    body: "As a Bachelor in languages, I'm very interested in Sonnet.\nDid it already pass the revue? If not, is it planned to be\nin the series sometime?"
    author: "Darkelve"
  - subject: "Re: When will Sonnet pass the review?"
    date: 2007-02-20
    body: "(and no, I'm not the same person as RideOut) :p "
    author: "Darkelve"
---
In <a href="http://commit-digest.org/issues/2007-02-18/">this week's KDE Commit-Digest</a>: The Dolphin file manager is moved into kdebase. Continued work in <a href="http://uml.sourceforge.net/">Umbrello</a> courtesy of the <a href="http://dot.kde.org/1165100724/">Student Mentoring program</a>. Graphical element representations start to be introduced in <a href="http://edu.kde.org/kalzium/">Kalzium</a>. More new country maps in <a href="http://edu.kde.org/kgeography/">KGeography</a>. KSpaceDuel begins the porting process to a scalable graphics interface, with further SVG integration work in KMines, KWin4, KNetWalk, KBlackBox and KMahjongg. <a href="http://kolourpaint.sourceforge.net/">KolourPaint</a> gains the ability to interface with image scanning hardware. Improved handling of the XPS document format in <a href="http://okular.org/">okular</a>. Lilypond export functionality in <a href="http://ktabedit.sourceforge.net/">KTabEdit</a>. More work in the KDE Fonts Manager. The KNewStuff2 framework reaches new milestones in its reworking for KDE 4.

<!--break-->
