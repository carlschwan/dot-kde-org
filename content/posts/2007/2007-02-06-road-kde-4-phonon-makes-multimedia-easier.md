---
title: "The Road to KDE 4: Phonon Makes Multimedia Easier"
date:    2007-02-06
authors:
  - "tunrau"
slug:    road-kde-4-phonon-makes-multimedia-easier
comments:
  - subject: "Digg this"
    date: 2007-02-06
    body: "The story is available for digging here:\nhttp://digg.com/linux_unix/The_Road_to_KDE_4_Phonon_Makes_Multimedia_Easier"
    author: "JLP"
  - subject: "Re: Digg this"
    date: 2007-02-06
    body: "Looks like the link of the article changed. So don't use the above link, use the new one with fixed link:\nhttp://digg.com/linux_unix/The_Road_to_KDE_4_Phonon_Makes_Multimedia_Easier_fixed_link"
    author: "JLP"
  - subject: "Re: Digg this"
    date: 2007-02-06
    body: "Please, STOP doing this. I suppose Digg users already read the Digg page, so you don't have to post links here. Same applies for del.icio.us, reddit, spurl, and the boring list of sites that do the same functionality."
    author: "Anonymous Coward"
  - subject: "Re: Digg this"
    date: 2007-02-07
    body: "Digg is a very high traffic website. Getting KDE news on its front page is graet for KDE."
    author: "aa"
  - subject: "Re: Digg this"
    date: 2007-02-07
    body: "Right, but you don't need to post on the dot linking to the digg story about the dot story. Everyone that reads it is already here, reading the actual story; they don't need to read a summary on another website."
    author: "dolio"
  - subject: "Re: Digg this"
    date: 2007-02-07
    body: "dolio,\n\nthe idea of asking people to \"digg this\" is not to have them go to digg.com and read the article *there*. The idea is to have those few friends of KDE (to be found *here*) who *do* frequent digg.com, to give the entry there a \"thumbs up\", so it may enter one of the front pages of digg for a while. \n\nVery often, Digg posts who get a dozen of diggs within their first hour (only a dozen!) do get a lot of attention in their second and third hour.... \n\nAnd once on a Digg front page, a *lot* more Digg readers will see it there, and a part of them will come over to the Dot and read the full article here.\n\nGot this?\n\nStill thinking it is a bad idea to tell people about a Digg entry?\n\nIf yes, let it be told to you, that your trying to censor friendly people who are commenting on Dot stories and your asking them to stop helping to promote KDE is also a ... bad idea. Got *that*?\n"
    author: "ac2"
  - subject: "jackd integration"
    date: 2007-02-06
    body: "It sounds interesting.\n\nOne question:\n\nWould jackd be one of the supported engines? Or would applications like rosegarden plug in phonon and phonon then manage the server choice?\n\nBest\n-.-\neeos communications support\nhttp://www.eeos.biz\nCompany No. 05765416"
    author: "eeos communications - support"
  - subject: "Re: jackd integration"
    date: 2007-02-06
    body: "Hi,\ncertainly, having jackd interfaced somehow into Phonon would be great for everybody who wants to make music on Linux.\nAs far as I understand, jackd itself is not an engine... It is the way to get into your soundcard/midi and stream the data from/to there. So it is roughly at the level of ALSA or OSS... \nOn the contrary, Phonon relies on \"hefty\" backends such as xine, capable of doing all the file-decoding for mpegs, oggs, etc; and it is xine that would actually need to talk to jackd (rather than to ALSA, OSS, or so)... \nSo: it might be a question to the XINE developers to support jackd...\n\nCould some of KDE/Phonon specialist tell if my thinking is right?\n\nAny bright ideas to have jackd integrated somehow anyway?"
    author: "piters"
  - subject: "Re: jackd integration"
    date: 2007-02-06
    body: "Correct. As far as I know xine supports audio output to jackd since version 1.1.3. As soon as xine tells Phonon it supports jack, Phonon will list jack in the list of output devices (where in the screenshot above you already see arts and esd as soundservers)."
    author: "Matthias Kretz"
  - subject: "Re: jackd integration"
    date: 2007-02-07
    body: "Is there any work ongoing to secure that , I as an below avearge sound user, can have an understandable way of configuring this stuff?\n\nPreferably one manner to configure everything regarding sound? Please! not alsa.conf for this and jack conf for that and Phonon conf .... \n\nI have had several realy nasty encounters with sound problems earlier....\n\nRegards Birger"
    author: "Birger Kollstrand"
  - subject: "Re: jackd integration"
    date: 2007-02-07
    body: "The idea is that Phonon will try to guess the correct defaults and give you the possibility to adjust those.\n\nRegarding soundcard setup I'll try to make it smart. For example it currently uses dmix automatically, if that doesn't work fall back to hw and if that doesn't work fall back to plughw. Now the next step is to first test whether hw does mixing already, then dmix might be unnecessary or even counterproductive.\n\nRegarding the soundservers I'd like to be able to help the user configure the soundserver correctly if he wants to use one. All that needs a lot of testing and information about different hardware and setups so that I will never be able to get it right on my own. It would be great if people can help out here."
    author: "Matthias Kretz"
  - subject: "Re: jackd integration"
    date: 2007-02-08
    body: "Dear friends,\n\njackd is not really at the same logical level as alsa.\n\nAs far as I know, it needs alsa to work properly, so it works on top. Or am I wrong?\n\nFrom the documentation:\n\nWhat is JACK?\n\nJACK is a low-latency audio server, written primarily for the GNU/Linux\noperating system. It can connect a number of different applications to an audio\ndevice, as well as allowing them to share audio between themselves. Its clients\ncan run in their own processes (ie. as normal applications), or can they can\nrun within the JACK server (ie. as a \"plugin\").\n\n<snip>\nJack has two sets of parameter options. The first part are specific to running\nthe jack server. The second part are run time options for how jack interfaces\nwith the sound driver - currently only ALSA.\n\nThe easiest way to start jack is to run this command:\n\njackd -d alsa -d hw:0\n\nOf course that gives you very little control over what jack does to the audio\nstream and which device you use. You can specify a card name by setting up an\n.asoundrc file. Visit the online ALSA docs for your card/device to get one.\n</snip>\n\n\nBest\n-.-\neeos communications support\nhttp://www.eeos.biz\nCompany No. 05765416"
    author: "eeos communications support"
  - subject: "Re: jackd integration"
    date: 2007-02-06
    body: "No.Rosegarden or other Audio/Video Editing software are not going to Use Phonon.Phonon is NOT Designed for them.Its Designed to support Basic functionalities that __ALL__ of its Engines support.For example Playing and Pausing Audio is supported by all engines.but more advanced things which are needed for Editing are not implemented in all engines.\nevery video/audio editing application (Usually) uses One Engine.\nsorry if im wrong.Im just a user and i know these by reading mailing lists/talks..."
    author: "Emil Sedgh"
  - subject: "Re: jackd integration"
    date: 2007-02-07
    body: "As a musician I use all the applications like Rosegarden, Ardour, ... directly with jackd, no need for Phonon. But therefore I have to connect the jackd deamon via Alsa to my prefered sound device.\nNow I would be realy glad if KDE via Phonon could make use of jackd. If not, I would have to connect KDE to a different sound device because the one used by jackd is already blocked. Right?"
    author: "Germel"
  - subject: "Re: jackd integration"
    date: 2007-02-07
    body: "Hey Germel,\n\n  Hopefully somebody will implement a jackd backend for phonon.  However the linux kernel has something called dmix which basically allows multiple 'apps' to open the sound device, so it won't be blocked.\n\nJohn"
    author: "JohnFlux"
  - subject: "Re: jackd integration"
    date: 2007-02-07
    body: "There can not be such a thing as a jackd backend for Phonon. A Phonon backend does decoding of arbitrary media formats and in addition to audio playback also does video.\n\nWhat is needed are jackd output plugins for the libs that are used by the backends. Those outputs are, AFAIK, already available for GStreamer and Xine."
    author: "Matthias Kretz"
  - subject: "Re: jackd integration"
    date: 2007-02-07
    body: "Attached you can see Phonon using libxine 1.1.3 which provides the jackd output plugin."
    author: "Matthias Kretz"
  - subject: "Re: jackd integration"
    date: 2007-02-07
    body: "Or not. Konqueror playing tricks on me?\n/me tries again"
    author: "Matthias Kretz"
  - subject: "Re: jackd integration"
    date: 2008-10-09
    body: "Do you perhaps mean xine lib 1.1.13  ( I installed 1.1.15 just now and I am now listening to amarok through my Jack only sound card - awesome)"
    author: "Danni Coy"
  - subject: "NMM"
    date: 2007-02-06
    body: "actually nmm is quite cool. if you could not compile it, there are packages and even a knoppix based live-cd at http://www.networkmultimedia.org/Download/index.html\n\nsome backgrounds to nmm can be viewed at http://graphics.cs.uni-sb.de/~slusallek/publications.html (Multimedia) or \nhttp://www.infosys.uni-sb.de/teaching/perspektiven0607/slides/slusallek.pdf\n\n"
    author: "Andreas"
  - subject: "Re: NMM"
    date: 2007-02-06
    body: "The precompiled packages won't work since you need to apply a patch first which lies in kdemultimedia/phonon-nmm/NMM-patches/ :(\n\nSo you have to compile NMM yourself (after patching) and then you can compile the backend.\n\nIt would be great if somebody could put more effort into the NMM backend. It sure is a great technology but currently nobody is working on it (I keep it compiling and running, but don't have time for more at the moment)."
    author: "Matthias Kretz"
  - subject: "Re: NMM"
    date: 2007-02-06
    body: "That's sad to hear, I hope more effort goes into it's development again. NMM is seriously cool, and it would provide KDE with functionality making it a level above other desktops in the multimedia area too. I think I would like NMM as the 'certified to work' engine."
    author: "Morty"
  - subject: "independent application volume control?"
    date: 2007-02-06
    body: "Hello, that was nice to read.\n\nI have one question: will there be a way to control the volume for a single (kde) application at a time? Say for instance that I am watching a dvd whose audio is quite low, so I want to push up the volume for the dvd player, but don't want to jump out of my seat or get deaf if I get a mail or some other event occurs, whose notification sound is comparatively louder.."
    author: "tendays"
  - subject: "Re: independent application volume control?"
    date: 2007-02-06
    body: "Ok never mind, I \"RTFWS\" and it looks like the answer is yes.\n\n:-)"
    author: "tendays"
  - subject: "Re: independent application volume control?"
    date: 2007-02-06
    body: "Would be really great ! And useful :)"
    author: "gaboo"
  - subject: "Re: independent application volume control?"
    date: 2007-02-11
    body: "well, it's already possible to turn down notification volume... :)"
    author: "Chani"
  - subject: "Use PulseAudio!"
    date: 2007-03-14
    body: "Use http://pulseaudio.org and you'll be able to do this already!\nMy Gentoo system is already configured to use PulseAudio for all audio output and it works great! No special application support needed. It works as an Alsa plugin, a fake esd and other things so all programs that support either alsa or esd will play through PulseAudio. Apps that only support OSS can be used with the padsp utility (like esddsp or aoss). Monty of Xiph (author of ogg/vorbis) has also written a oss2pulse daemon which create a fake /dev/dsp and route it to PA.\nFedora Core 7 will be 100% PulseAudio. It's the compiz/beryl/xgl of desktop audio.\n\nBTW, it does not try to compete with Phonon, GStreamer, Xine, Jack etc. It complements it."
    author: "Tuxie"
  - subject: "Per-App Settings?"
    date: 2007-02-06
    body: "Firstly, thanks as always to Troy for preparing this for the Hordes hungering for KDE4 news :)\n\nTo save everyone the trouble of rooting through the API, I have a couple of questions:\n\n1) I heard rumblings of the existence of per-application volume settings so that (e.g.) you don't have your eardrums blown out when a buddy signs in to MSN just as you're watching the quiet part of your film ;) How will this work, exactly? Any mockups of e.g. kmix?\n\n2) Would it be possible to, for an arbitrary piece of video supported by the currently used engine, extract frames (plus accompanying sound for that frame) one-by-one in some format (YUV+PCM, maybe), process them (maybe adding a watermark, or doing your own custom effects) and then send this altered audio and video frame into another Phonon-based encoder for encoding to, say, XVid?\n\n"
    author: "Anon"
  - subject: "Re: Per-App Settings?"
    date: 2007-02-06
    body: "3) Will it be possible to choose the output device per application? E.g. I have two soundcards and want KMplayer to always use the first and Amarok to always use the second."
    author: "Bakterie"
  - subject: "Re: Per-App Settings?"
    date: 2007-02-06
    body: "First of all (as you can see in the screenshot) device preference is set per category of application. The distinction between Music and Video probably needs a better name for the category, though.\n\nIf that isn't enough for your needs the application still can override the global setting per category. So if the application provides the switch (it's almost no effort to implement) then you can have that, too."
    author: "Matthias Kretz"
  - subject: "Re: Per-App Settings?"
    date: 2007-02-06
    body: "1) Every Phonon AudioOutput object can be told over dbus to change its volume. For now the only way to remote control the volume of an app is to use qdbus:\n% qdbus org.kde.knotify /AudioOutputs/0 Get org.kde.Phonon.AudioOutput volume\n1\n% qdbus org.kde.knotify /AudioOutputs/0 Set org.kde.Phonon.AudioOutput volume 0.2\n% qdbus org.kde.knotify /AudioOutputs/0 Get org.kde.Phonon.AudioOutput volume\n0.200000002980232\n\nThe value is stored as float in case you wonder about the 0.2.\n\n2) It's on the todo list. But it's not likely to be ready for 4.0 unless more people help out. The idea is to use AudioDataOutput and VideoDataOutput objects to capture the media data and then an AvWriter object to encode and write to a file."
    author: "Matthias Kretz"
  - subject: "Re: Per-App Settings?"
    date: 2007-02-06
    body: "Great stuff, thanks Matthias :)\n\nThe answer to 2) is a huge (and welcome!) surprise to me: it looks like you have a very rich and functional piece of software coming together.  Keep it up!"
    author: "Anon"
  - subject: "Video: Only sound on the right speaker"
    date: 2007-02-06
    body: "Same for you as well? Or is that a feature I just didn't notice in the video ;-)"
    author: "Carsten Niehaus"
  - subject: "Re: Video: Only sound on the right speaker"
    date: 2007-02-06
    body: "Sorry, that's a feature of recordmydesktop. It didn't record any sound if I told it I want to record only one channel. So I recorded two channels - one with mic, one silent."
    author: "Matthias Kretz"
  - subject: "Re: Video: Only sound on the right speaker"
    date: 2007-02-07
    body: "Ah, nice issue :) Great work, Matthias."
    author: "Carsten Niehaus"
  - subject: "Re: Video: Only sound on the right speaker"
    date: 2007-02-06
    body: "Yes, only sound in the right speaker.\n\nThanks for another excellent article about KDE 4!"
    author: "Joergen Ramskov"
  - subject: "gstremer builds now"
    date: 2007-02-06
    body: "Just a small update since I wrote this article - I discovered I was missing a package on my system to get the phonon-gstreamer engine to build.  After installing libgstreamer-plugins-base0.10-dev, it now builds... I had previously only the libgsreamer0.10-dev package and had assumed that it would be enough to build the engine, but tbscope and christoph4 in #phonon helped me to realize what I was missing.  \n\nCheers"
    author: "Troy Unrau"
  - subject: "Thank you Troy!"
    date: 2007-02-06
    body: "I really enjoy reading your \"The Road to KDE 4\" articles. Thank you very much for writing them!"
    author: "Michael Thaler"
  - subject: "The best part of KDE4"
    date: 2007-02-06
    body: "Phonon is (for me) the best part of KDE4.  Ever since I heard the proposal, I've been looking forward to KDE4.  I have a USB sound card for my laptop which I use with my speakers in one room, but if I take my laptop elsewhere there's no point having that sound card.  It's a nuisance to have to manually change the audio output each time (doubly so when said change needs to be done in a config file since xine_part doesn't like to load when it can't access the output).\n\nAs much as I'm looking forward to all the other cool stuff coming for KDE4, Phonon has to be the best one for me.\n\nHats off to the devs!"
    author: "mactalla"
  - subject: "Re: The best part of KDE4"
    date: 2007-02-07
    body: "i'm really looking forward to having music from my boxes while i play a game which outputs its sound to my headphones..."
    author: "superstoned"
  - subject: "Huge"
    date: 2007-02-06
    body: "Really impressive and interesting architecture. This rocks.\n\nThank you phonon developers.\n\nTroy, as always: huge."
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "KDE4"
    date: 2007-02-07
    body: "When is KDE4 going to be released? Its appearing that the release is getting pushed back as much as Vista has been. "
    author: "Steven"
  - subject: "Re: KDE4"
    date: 2007-02-07
    body: "Short answer:  There is no official time period for release yet.\n\nLong answer:\n\nThere has never been an official release period, so it cannot have been 'pushed back'.  Unlike Microsoft, KDE has not published a roadmap for its next major release.  The only previous answers to that question have been various people's personal thoughts on the issue, as opposed to a consensus amongst developers.\n\nKDE 4 consists of several parts.  The two principal parts are the platform ( the libraries which KDE applications use ), and the applications themselves.  The platform absolutely must be completed before KDE 4.0 is released.  The applications can be scaled up or down in terms of features for the release, depending on how much time the developers have.  A lot of work has been done on the platform, and as this article discusses, much of it has been rolled into the main development branch.  However there are still some items of work outstanding.  Some developers feel uncomfortable committing themselves to a release period until this work is completed.\n\nA release team has recently been formed, so a more concrete answer will probably be forthcoming in the next month or so."
    author: "Robert Knight"
  - subject: "Re: KDE4"
    date: 2007-02-07
    body: "Well fair enough but Microsoft did wait till the last minute to put a date on the release of Vista, but it (like many other stories have reported about KDE4) have said \"it will be released sometime around early/mid/late 2004-2006 now 2007. I'm a  fan of KDE (I use it primarily on my laptop) but KDE4 is looking like Netscape 6, and vista. Lots of pretty concept art and then a late schedule. \n\nIn no way am I trying to push on development on the backend ... its better to ship with a stable core than to release crap, espically in this community (the open source/free software)"
    author: "Steven"
  - subject: "Re: KDE4"
    date: 2007-02-07
    body: "Well, as anyone who has been around in the KDE 1.1.2 -> KDE 2.0 series transition, backend changes can mean a long development cycle.  In the KDE 2 series however, great KDE technologies like kio slaves, kparts, dcop, etc. were born, and it was worth the wait.\n\n(KDE 2->3 was less of a big deal, a lot more code cleanups, less huge structural changes...)\n\nKDE 4 is introducing many new great technologies, and like KDE 2.0 (or OS X 10.0), may lack a little polish when it's first released, but should be a precursor of great things to come.  The downside is that the 4.0 transition cycle will be quite a lot longer than 3.0's transition cycle.  Honestly, I think that we'd be lucky to see KDE 4.0 this year.  However I'd wager we'll see our first alpha released this July for Akademy.  As noted above, however, this is an opinion, and there is not an announced release schedule for 4.0 yet.\n\nNote also that 3.5.7 is likely also on its way, within 3 months if irc rumours are true."
    author: "Troy Unrau"
  - subject: "Re: KDE4"
    date: 2007-02-07
    body: "> be released sometime around early/mid/late 2004-2006 now 2007\n\nsorry, as far as I can recall 2004,2005 and 2006 were _never_ mentioned as possible release dates for KDE4. In the very beginning where QT4 came out and the libs were branched to work on the KDE4 target it was mid2007-end2007 and this still seems to be a good guess if you consider the process which has been made up to now. Really, the dates you mention have never ever been connected to a guessed KDE4 release-date.\n(duh, 2004/2005 .. how would that have been possible?)"
    author: "Thomas"
  - subject: "Re: KDE4"
    date: 2007-02-07
    body: "I think Steven is talking about the release date of Vista, not KDE, as an exemple..."
    author: "Nicolas"
  - subject: "Re: KDE4"
    date: 2007-02-07
    body: "You should also remember that KDE don't have thousands of fulltime developers working on KDE 4. Considering how relatively few developers that are actually working on KDE, it is amazing how cool a desktop they have created. \n\nI don't really see how KDE 4 can be compared to Vista? There has been no official comment about when KDE 4 will be released and there is a lot more than just concept art available. \n\nI am certainly looking forward to KDE 4 and these articles just makes it worse ;) but KDE 4 will be ready when it's done.\n\nBesides, KDE 3 is already a great desktop :)"
    author: "Joergen Ramskov"
  - subject: "Re: KDE4"
    date: 2007-02-07
    body: "But that means that KDE failed to build the industry coalition. Why? Totally unnecessary. Now KDE 4 has to be ready, 3.5 is an old plattform. The question is if KDE 4 will be there in time."
    author: "Wim"
  - subject: "Re: KDE4"
    date: 2007-02-08
    body: "Umm, it's free software, programmed by volunteers.  Since when have any of these volunteers ever needed an industry coalition in order to contribute?  These sorts of coalitions generally tend to be PR groups only.  The Linux kernel exists without some sort of coalition, and would continue to do so even if no one in industry cared."
    author: "Troy Unrau"
  - subject: "Re: KDE4"
    date: 2007-02-08
    body: "KDE 3.5 is an old platform? What are you talking about? KDE 3.5 was released november 29, 2005. That is just over a year old. It has seen six minor revisions since then, so it is actively maintained. All these releases have made the platform more stable, faster, and more polished. It is a very mature and stable platform, and probably the platform of choice for the less adventurous users for quite some time to come. \n\nInstead of bitching that KDE 4 is supposed to be ready, stop wasting time and actually start contributing to *getting* it ready."
    author: "Andre Somers"
  - subject: "Re: KDE4"
    date: 2007-02-12
    body: "\"Well fair enough but Microsoft did wait till the last minute to put a date on the release of Vista\"\n\nWell, they did wait untill the last minute for the exact date of release.\nBut after the release of WindowsXP they promised that the next version of Winodws would be released in 2002, 2003, 2004, 2005, 2006, late 2006, 2007..\nThat is a delay of 5 years between the first estimated release date and the exact release date.\nAnd for the successor of Vista (Vienna), Microsoft has given an estimated release date of autumn 2009..\nWell see if they will make it or if Vienna will be released 5 years later..\n\nAs for KDE4, KDE never mentioned an estimated release date...\n\n"
    author: "otherAC"
  - subject: "KDE3 compatibility"
    date: 2007-02-07
    body: "Is there any thought to supporting kde3 apps? Many depend on arts, so any kde3 apps will require an old unmaintained arts installed. Kde3 apps will be around for a long time yet.\n\nHow about a library in kde4 that looks like arts but calls phonon?\n\nCould be called pharts?\n\nDerek"
    author: "D Kite"
  - subject: "Re: KDE3 compatibility"
    date: 2007-02-07
    body: "pharts is a cool name! I want it. Even if its completely useless :)"
    author: "me"
  - subject: "Re: KDE3 compatibility"
    date: 2007-02-07
    body: "If there'll ever be a aRts backend to Phonon I want it to be called pharts. :)\n\nMy hope is that aRts will continue to work for those people that have to use it in KDE4 times. I'm not able to maintain aRts!\n\nAnyway aRts is able to use ALSA and dmix then does the rest to be able to use aRts and Phonon applications at the same time. There are still ways to break such a setup, but those are solvable.\n\nImplementing a lib that looks like aRts but calls Phonon is next to impossible."
    author: "Matthias Kretz"
  - subject: "Re: KDE3 compatibility"
    date: 2007-02-10
    body: "I suspect KDE3 compatibility will depend on the distributions.  Gentoo already has KDE slotted into /usr/kde/<slot>, where slot is x.y minor version, so for example 3.4.x and 3.5.x could exist beside each other for a time, and I'm sure that wouldn't be taken away for a change as big as KDE4.  KDE 3.5.x and KDE 4.0.x will therefore exist beside each other until the Gentoo sysadmin decides to unmerge 3.5.x.  When KDE 4.1 comes out, it'll be yet another slot.\n\nIt has been awhile, but I was back on Mandrake for the KDE 2.x -> 3.x upgrade, and while their arrangement was somewhat different (KDE files were distributed into appropriate directories directly under /usr, so in /usr/bin, /usr/share, and /usr/lib, for whatever version of KDE shipped with the distribution release), making it difficult to have but one \"main\" version installed at any point, early in the 3.x cycle they installed it to (IIRC) /opt (the as-shipped KDE default, AFAIK), so again, 2.x and 3.x could and did exist beside each other, for those admins wishing that it be so.  As 3.x matured to the point they could ship it as the \"main\" KDE version for a Mandrake release, they killed 2.x and moved 3.x into the main /usr dirs along with everything else.\n\nThere shouldn't be anything stopping the various KDE versions from running on the same system, the environments one at a time just as one can run KDE or GNOME, one at a time, on the same system, or indeed, various apps from one version running under the environment of the other, just as KDE apps can run on GNOME and GNOME apps on KDE, as long as your distribution arranges it that way.  If you grab the sources and direct compile your own, that you are of course creating your own distribution, so it would be up to you to configure them to install to different locations if you didn't want conflicts.\n\nAs for depending on aRts specifically, as long as aRts can be set to share the device (that's the problem, as it was designed before that was normally possible and it still doesn't like to share tho it's generally possible and most other apps now share), you should be able to run KDE3/aRts apps on a KDE4 desktop, just as you can now run KDE3/aRts apps on a GNOME desktop, as long as all the necessary dependencies remain installed.\n\nHowever, while many apps depend on aRts to be installed if compiled with that dependency (and a few require the dependency), fewer apps now depend on it actually /running/, as they (and the KNotify system as well) can be configured to play to ALSA or whatever directly instead of to aRts.  I no longer run aRts here, as I got tired of not being able to run anything else because it was hogging the sound devices, and of all the problems keeping aRts working reliably.  I still had to keep USE=arts in my USE flags (Gentoo), as disabling that disabled a bunch of other stuff one wouldn't intuitively think was related to aRts (it's likely some of that was Gentoo linking of KDE audio features to USE=arts even when it wasn't related to arts itself, however), even tho I no longer run aRts itself.\n\nThus, it's likely that with proper KDE3 configuration, you should be able to quit running aRts itself even if you have to keep it installed as part of your KDE3 dependency tree, and can pipe sound directly thru ALSA or whatever, just as I do now, and as KDE's photon will be doing indirectly thru xine/gstreamer/nmm/whatever.  Of course, you won't get the benefit of the per-category and individual app volume settings in your KDE3 apps, only in your KDE4 apps, but one wouldn't expect it, either, since they are still KDE3 and not KDE4.\n\nDuncan"
    author: "Duncan"
  - subject: "ooohh man!"
    date: 2007-02-07
    body: "i'm just thinking aloud but xmms2 backend + amarok = dream media player/server.\n\n\ni love having my music go no matter what my computer is doing (in and out of x, gui crashes mainly) and allowing all qt multimedia apps to take advantage of the convenience features of xmms2 is well, just ... *drool*"
    author: "Jonathan Dehan"
  - subject: "Re: ooohh man!"
    date: 2007-02-07
    body: "Support would depend on someone writing an engine for phonon that interfaces to xmms2.  At the moment, there isn't anyone working on this."
    author: "Troy Unrau"
  - subject: "Nice, a couple of feature suggestions"
    date: 2007-02-07
    body: "I love the idea of having different categories for sound, communications, music, etc. I'd like to suggest you add and extra category for music creation software. \n\nSecondly I'd like to suggest that maybe you can set it that if something in one category is playing it can mute or turn down the sound on another category. So if something in Communication turns on music will turn off.\n"
    author: "Ben"
  - subject: "Re: Nice, a couple of feature suggestions"
    date: 2007-02-07
    body: "Music creation software is probably beyond the scope of phonon and would want to use a lower level interface directly, like arts or jack.  Rosengarden, for instance, would not be one of the applications suggested to transition to phonon."
    author: "Troy Unrau"
  - subject: "Re: Nice, a couple of feature suggestions"
    date: 2007-02-07
    body: "1. suggestion)\nHow would you call the category? I'm reluctant to add another category. I think there are too many categories already.\n\n2. suggestion)\nYes, I want such a policy manager, too. Anybody who wants to work on it? I can certainly guide somebody how to do it."
    author: "Matthias Kretz"
  - subject: "MIDI"
    date: 2007-02-07
    body: "just wondering how Phonon will work with MIDI, anyone have any idea?"
    author: "Ben"
  - subject: "Re: MIDI"
    date: 2007-02-07
    body: "MIDI is orthogonal to Phonon. The \"Desktop\" doesn't have any need for MIDI. MIDI-Applications could use Phonon for audio (PCM) stuff, but they probably want a different lib or do it all themselves.\n\nI'm not saying you cannot make use of MIDI in general for the desktop, but it's such a special case that for now it's ignored completely in Phonon."
    author: "Matthias Kretz"
  - subject: "Re: MIDI"
    date: 2007-02-07
    body: "Thats a shame, I play quite a few MIDIs and would have liked to control the volume inside phonon with everything else."
    author: "Ben"
  - subject: "Re: MIDI"
    date: 2007-02-07
    body: "Two possiblities:\n- hardware synth: nothing for Phonon, you have to use the hardware mixer to control the volumes\n- software synth: either the software synth provides the same dbus interface as Phonon applications do, or it uses Phonon to do the audio output. The former is not standardized yet. The latter is still on the todo list."
    author: "Matthias Kretz"
  - subject: "Re: MIDI"
    date: 2007-02-07
    body: "I tend to disagree. Linux desktop is still waiting for decent karaoke players and Guitar Pro clones. This is \"fun stuff\", and its important. "
    author: "Ljubomir"
  - subject: "Re: MIDI"
    date: 2007-02-07
    body: "Did you tried ktabedit, a good fork from the un-maintained kguitart?"
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: MIDI"
    date: 2007-02-07
    body: "You have to consider that so far I've been designing and implementing Phonon mostly on my own. I should be learning for Uni instead. There's no way I can provide API for MIDI for KDE 4.0. I agree that it might make sense to have a Qt-style MIDI API, but it won't be me doing that."
    author: "Matthias Kretz"
  - subject: "Re: MIDI"
    date: 2007-02-13
    body: "MIDI is not general purpose, it's indeed a particulr use-case (amateur musicians in particular and a few others). So, I find perfectly normal a lower priority."
    author: "Davide Ferrari"
  - subject: "Latency"
    date: 2007-02-07
    body: "I don't know whether it's a problem with video/audio synchronization, but the response latency of the system to the issued commands (via the GUI) looks awful to me, it looks to be close to one second!"
    author: "Fabio A."
  - subject: "Re: Latency"
    date: 2007-02-07
    body: "Well, if you want to fix that you have to fix libxine. Other frameworks might be faster with switching, but libxine doesn't care that much and simply lets the buffer of the first soundcard run empty until the other soundcard starts playing. You could try and make it flush buffer of the first soundcard, but that's\n1) not to fix in Phonon, but in libxine\n2) very low priority: Why should I optimize for device switching which is a rather rare case?"
    author: "Matthias Kretz"
  - subject: "Re: Latency"
    date: 2007-02-07
    body: "Device switching is the least of issues, also lowering or highering the volume happens close to one second later than the actual manipulation of knob. Even hitting the stop button sorts its effect with that great delay."
    author: "Fabio A."
  - subject: "Re: Latency"
    date: 2007-02-07
    body: "Volume is handled by libxine too, and apparently the volume is applied to the data that's not in the audio buffer yet. It's very bad when using the OSS emulation of my Headset - that's like 4 seconds delay. Completely unusable. :(\n\nMy feeling for the stop delay is something ~100ms. If you see/hear such a great delay it's probably a/v out of sync for you. You really should try it yourself to judge.\n\nBtw, from clicking stop to the call to xine_stop it's really not far: as soon as the X event has reached the processEvents method in the main loop it's the clicked() signal and then Phonon is called. A few instructions later the stop command is send to the xine thread, which (if the command queue is empty - that's the normal case) calls xine_stop after a few asserts I added for debugging."
    author: "Matthias Kretz"
  - subject: "OpenAL?"
    date: 2007-02-07
    body: "Is the Phonon framework relevant for the technology known as OpenAL?\n\nhttp://www.openal.com/\n\n\"OpenAL is a cross-platform 3D audio API appropriate for use with gaming applications and many other types of audio\"\n\nOr am I way off base here?"
    author: "Darkelve"
  - subject: "Re: OpenAL?"
    date: 2007-02-07
    body: "This again something that has to be integrated on the backend side. If e.g. GStreamer supports to use OpenAL then the Phonon GST backend can be written to make use of it. If you're going to write a first person shooter game then you probably don't want to use Phonon anyway but rather OpenAL directly.\n\nPerhaps one day we'll see integration of OpenAL into a backend and then a GUI the define where you want to hear notifications and so on. :)"
    author: "Matthias Kretz"
  - subject: "Re: OpenAL?"
    date: 2007-02-07
    body: "Thanks for the explanation! (and for not making fun of me :p )"
    author: "Darkelve"
  - subject: "Phonon is just an enourmous fallacy"
    date: 2007-02-07
    body: "I think this is an enormous mistake because it puts KDE on the sideline rather than getting actively involved with Gstreamer and helping to make Gstreamer into a totally kick-ass framework and instead taking the cowardly \"let us see\" attitude that serves noone.\n\nPhonon is also built on a number of fallacies:\n1. It assumes that if Gstreamer changes it's API, only Phonon (not the apps) needs to be changed, without considering that Gstreamer may introduce new APIs that can't be handled by the current Phonon API, thus requiring an API change that requires change in applications as well.\n2. It assumes that Phonon is going to be more API stable than Gstreamer. Given that Gstreamer is approaching maturity, this is not at all proven.\n3. Phonon will never be able to support everything that the lower level systems can. For every new feature that the lower level systems get, Phonon will get there slower. \n4. Phonon is setting itself up for a Q & A nightmare, where apps will have different capabilities depending on what capabilities the subsystem has.\n5. Phonon will be no more capable and stable than the subsystem. If all the subsystems are half-baked, then Phonon will be as well. Instead KDE could have focused on getting in on helping out with one subsystem to make sure it will stay alive and well."
    author: "Bjarne Alderhaug"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-07
    body: "1) Seems like a perfectly reasonable assumption to me.\n\n2) Gstreamer is a lot bigger and complex than Phonon, I'd be very suprised if Gstreamer had a more Stable API.\n\n3) Welcome to UNIX, we've got years of history so you should do a bit of catching up, starting with 80/20 ;). 80% or more of Desktop applications only need: Play, rewind, fast forward, stop, pause and a progress bar. Phonon dose that and more. If you need more than Phonon can provide use a back end directly. \n\n4) I'll leave this one, I can't argue either way. \n\n5) http://aseigo.blogspot.com/2006/05/id-like-another-black-eye-please.html\n\n6) Why is Gstreamer better than NMM? Phonon makes it really easy for Kprograms to support both, giving users a choice. Sounds a lot better to me than plain Gstreamer support."
    author: "Ben"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-07
    body: "Gstreamer does not support all of KDE's platforms, it's API has been shifting enough to break previous KDE applications (like amarok) and we'd like to not repeat it.\n\nWe'd also not like to repeat our mistake by selecting one dedicated sound engine, like we did for arts.  So imaging Phonon as higher level KDE/Qt bindings for gstreamer for basic multimedia applications (playback, recording, simple effects) - but having been designed in such a way so as to be able to use other engines as well as necessary.  On a mac, this could be xine or QuickTime, since they play nice on that platform.  \n\nSo we aren't shunning gstreamer - we're providing a high-level API for gstreamer which happens to look the same as our high level audio API for all those other engines.  And the app won't care what engine is being used."
    author: "Troy Unrau"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-09
    body: "\"Gstreamer does not support all of KDE's platforms, it's API has been shifting enough to break previous KDE applications (like amarok) and we'd like to not repeat it.\"\n\nThen get more heavily involved with GStreamer. Make sure it supports all your platforms and if you are heavily involved, you can drive the development of the API."
    author: "Bjarne Alderhaug"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-09
    body: "Or mabey KDE can just intergrate with the local media framework? "
    author: "Ben"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "with phonon, kde can :)"
    author: "otherAC"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-08
    body: "Sounds familiar for me: last time KDE was blamed for not using CORBA/Bonobo... "
    author: "MK"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-08
    body: "Well can you explain to me why I should use GSteamer instead of libxine? Libxine just works for me."
    author: "Arnomane"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-09
    body: "Libxine is much more limited than GStreamer and can't provide the full feature set that GStreamer is providing.\n\nYes, Libxine may work better in some cases for video and music playback CURRENTLY, but GStreamer does the whole thing including advanced recording.\n\nThe KDE guys have a lot of skills that could really benefit the GStreamer project. We could have one really kick-ass stable system rather than 5 half-baked ones."
    author: "Bjarne Alderhaug"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "Gstreamer depends om Gnome technology (glib and gtk-docs), if the gstreamer wanted to make it an independent platform agnostic soundengine for both kde and gnome, they should not have used those dependencies..."
    author: "otherAC"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "I am quite sure that gtk-doc is for one just a build time requirement and additionally most likely optional.\n\nAnd while glib's origins are with the GIMP project and through GTK+ has found its way into GNOME's software stack, it is not a GNOME technology, just like Qt is not a KDE technology but an externally provided base dependency the respective projet builds upon.\n\nSaying a project should not use glib if it wants to be considered desktop independent is as ill-advised as saying you can't use QtCore and in both cases following such an advice would just lead to more bugs, in the worst case even security exploitable ones.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "True, but lets put it the other way round: if GStreamer uses the Qt equivalent of Glib, would the Gnome desktop adopt it as easily as they have done right now?\n"
    author: "otherAC"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "I think this question can't be answered on the example of Gstreamer, because the developers who created it are used to using glib and additionally are often associated with GNOME.\n\nWe could try to base it on an example of a glib based technology that has not been created by developers associated with GNOME but is still widely used there.\n\nHowever I don't have enough knowledge about technologies used by GNOME to find such an example."
    author: "Kevin Krammer"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "Well, there is nothing wrong with gstreamer using glib (i can't imagine a linux desktop without it and kde 3.3.5 has it as recommended dependency), but what bothers me is that since KDE decided to drop aRts, the GStreamer community is pushing hard and agressivly to make KDE adopt GStreamer in stead of something else like phonon (that can use gstreamer).\n\nIt makes me wonder what would happen if it was the other way round: let's say that developers associated with KDE/Qt started creating GStreamer with a dependency on the Qt equivalent of Glib andt GTK-docs and Gnome was looking for a ESD replacement, would they have adopted GStreamer?\n"
    author: "otherAC"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "Well, they didn't adopt aRts, which hadn't any Qt dependency and was the best solutions of its time.\n\nI'd say we see it when Akonadi becomes available :)"
    author: "Kevin Krammer"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "I think this little argument demonstrates *exactly* why phonon is so important. we don't want to get caught up in religious wars over backends; we just want the sound to WORK!"
    author: "Chani"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-11
    body: "Can't agree more :)"
    author: "otherAC"
  - subject: "Re: Phonon is just an enourmous fallacy"
    date: 2007-02-08
    body: "Why does everyone who thinks KDE should focus on a preexisting backend believe that it's gstreamer they should focus on?\n\nSheesh, be happy phonon-using apps will still in many cases be using gstreamer, without getting annoyed if gstreamer's API changes.\n\nMyself, I'll happily be using the xine backend.  I've used both, and prefer it and I'm sick of the gstreamer astroturfers trying to force something different on me."
    author: "?"
  - subject: "prefer to see in phonon direct support for ffmpeg"
    date: 2007-02-07
    body: "Why not to use ffmpeg (integrated in xine, mplayer, videolan, openwengo, gizmo, vivia, ..) - fastest multimedia engine used for decoding/encoding and grabbing media from DVB, Video4Linux, Fireware devices and CD/VCD/DVD or Internet resources and also used for streaming.\n\nIMO gstreamer is ugliest (stability, performance, documentation) engine build on top of ffmpeg. Does anybody try to use gstreamer-edtor, pitivi and other related projects higly tied to gstreamer?\n\n\n\n"
    author: "serge"
  - subject: "Re: prefer to see in phonon direct support for ffmpeg"
    date: 2007-02-07
    body: "The avkode engine is intended to pretty much be an interface to ffmpeg, (or other standard decoding libs where ffmpeg doesn't provide the codec).  Not really well developed, but definately lighter weight than the other backends will likely be."
    author: "Troy Unrau"
  - subject: "KMplayer needs both xine and mplayer engine"
    date: 2007-02-07
    body: "My KMplayer needs xine engine for listening to mp3 radio streams and it needs mplayer engine (embedded in konqueror) to show HD quicktime trailers.\nThe xine engine can't show those HD quicktime trailers from http://www.apple.com/trailers/ as it can't buffer enough cache of a big HD trailer (e.g. 150 MB).\nMplayer engine is not used for mp3 radio streams as it takes too long to fill the cache (I've set it to 12 MB), therefore the use of xine for mp3 radio streams.\n\nHow will Phonon deal with a problem like this?\nI want KMplayer to be able to play mp3 radio streams immediately and to buffer big HD trailers enough so the video doesn't stutter. \nFor me now both xine and mplayer engine need to be supported.\n(I know nothing about Phonon KDE multimedia technology, my apologies if my question is not relevant)"
    author: "vatbier"
  - subject: "Re: KMplayer needs both xine and mplayer engine"
    date: 2007-02-09
    body: "both are supported by phonon, and the app can tell phonon what it wants to use. so this has to be in kmplayer, not phonon, i think... not sure tough."
    author: "superstoned"
  - subject: "The road to KDE 4"
    date: 2007-02-07
    body: "The road to KDE 4? \n\nRelease early, release often. \n\nKDE 4 will take up when development releases are available. The question is how user will react when they need to wait more than 1.5 years for a new version of KDE.\n\nI don't understand why KDE does not branch: Stable version (3.5.x) and developmen  version with weekly snapshots."
    author: "Pim"
  - subject: "Re: The road to KDE 4"
    date: 2007-02-07
    body: "Quote:\n\"I don't understand why KDE does not branch: Stable version (3.5.x) and developmen version with weekly snapshots.\"\n\nIt does.\nAlthough weekly snapshots = your daily svn checkout.\n"
    author: "Tim Beaulen"
  - subject: "Re: The road to KDE 4"
    date: 2007-02-10
    body: "That's even better then weekly snapshots, you can get SVN snapshots every minute.\nHeck, even every second :o)\n"
    author: "otherAC"
  - subject: "Current state?"
    date: 2007-02-08
    body: "Does anybody know how far along phonon actually is? This is a great overview of what it is but it would also be nice to know the current state of the project. The roadmap on the website seems somewhat outdated (it currently says things that were supposed to be completed in Q2 2006 are in progress). Is there any news on how far along the developers are?"
    author: "reldruh"
  - subject: "Thanks / Question"
    date: 2007-02-08
    body: "Thank you very much Troy, it's always interesting to read \"The Road to KDE4\"-articles. You've done, as always, a great job.\n\nAs a user, it could be hard to follow the development. Lucky me that this kind o series exist. I want to know about so much about KDE4; Plasma, Konqueror, Krunner etc.\n\nHowever, I think there is an application which is mentioned very rarely when talking about KDE4: Get Hot New Stuff (don't know if this is the actual name). I think it is an application that is very good, but that would need some love from developers and artists.\n\nDoes anybody know the current state of Get Hot New Stuff, and how it is going to work in KDE4?"
    author: "Lans"
  - subject: "Re: Thanks / Question"
    date: 2007-02-08
    body: "GetHotNewStuff is a library shared between many apps. It has seen some work for KDE 4, however I haven't really looked at it.  I'll add it to my list of article topics to research for some day in the future.\n\nPlasma is not ready to be shown off.  \nKonqueror looks pretty much like Konq in 3.x, except the backend libs for rendering html and javascript have seen some new work.  Not much to show off there :)\nKrunner is visually just a run dialog, and hasn't really changed since I showed that screenshot a few weeks ago, except that it's now enabled by default.  It also has a few things that kdesktop used to have that have simply been ported over.  It now controls screensaver activation, for example.\n\nI'll write an article in a few weeks containing updates to old topics that'll touch on krunner some more."
    author: "Troy Unrau"
  - subject: "Re: Thanks / Question"
    date: 2007-02-08
    body: "Thanks for the answers. I look forward to read the next article!"
    author: "Lans"
  - subject: "Is it Coincidence !!!"
    date: 2007-02-08
    body: "Just to mention \n\nThe Pronunciation of phonon in Arabic means Arts\n"
    author: "RF"
  - subject: "Re: Is it Coincidence !!!"
    date: 2007-02-09
    body: "that's really cool ;-)"
    author: "superstoned"
  - subject: "Is current kmix going to die ?"
    date: 2007-02-09
    body: "As we're talking about sound, any news on kmix ? I hope KDE4 will not ship with a similar sound manager as I would like to install it without the users being lost with all the undocumented and obscure parameters"
    author: "Morreale Jean Roc"
  - subject: "Re: Is current kmix going to die ?"
    date: 2007-02-10
    body: "you could also try to document al those parameters to make sure your users will understand how kmix works"
    author: "otherAC"
  - subject: "Will this make my audio jump & my videos stutter?"
    date: 2007-02-10
    body: "layer upon layer upon layer upon layer upon layer = slow machine.\n\nand me who thought the point with audio was to be able to listen to it, and the point with video was to be able to view it.\n\nNOW."
    author: "kris"
  - subject: "Re: Will this make my audio jump & my videos stutter?"
    date: 2007-02-11
    body: "your assumption that layer upon layer is always slowing down is wrong ;)"
    author: "otherAC"
  - subject: "Re: Will this make my audio jump & my videos stutter?"
    date: 2007-02-11
    body: "Sometimes layer upon layer upon layer upon layer upon layer... is faster (and better) than just a single layer."
    author: "Corbin"
  - subject: "am I understanding correctly?"
    date: 2007-02-11
    body: "Did you say that once you switch backend in Phonon's config, it will use that backend and only that one?\n\nI thought it could be a preference list, like the KDE language settings. so that one is preferered and if it fails, it fallback onto the next one. wouldn't that be sweet? and non-working backend on a system should be automatically disable. for example on a fresh install gstreamer backend would be the default (for example), phonon finds out it fails, phonon suppose gstreamer is not installed and disable the backend. so it won't try and fail again.\n\nanother solution would be to mimic the file association config panel. mimi-type type of thing. where, I want mplayer or xine to handle video, and gstreamer for music. again, for example.\n\n\nkeep up the good work\n\nthanks"
    author: "somekool"
  - subject: "Phonon and VLC"
    date: 2007-02-11
    body: "I was wondering what happens to programs like VLC that have the codecs built in rather than useing an engine like xine. Will there some way of controlling VLC via Phonon? say by useing a special phonon-engine that just takes sound from VLC and sends it to ALSA or the Sound Server?"
    author: "Ben"
  - subject: "Re: Phonon and VLC"
    date: 2007-02-11
    body: "there is no need for vlc to use phonon for this, just like there is no need voor xine, mplayer or xmms to use phonon.\n"
    author: "otherAC"
  - subject: "Re: Phonon and VLC"
    date: 2007-02-11
    body: "Well there is no need for it to use phonon for decodeing media, but what about phonon's volume control, or its ability to chose diffrent pieces of hardware or soundservers?"
    author: "Ben"
  - subject: "Re: Phonon and VLC"
    date: 2007-02-11
    body: "if VLC sees advantage in supporting phonon, then they can put an option in the media player that uses phonon as backend.\nJust like xmms had an option to use aRts as backend.\n"
    author: "otherAC"
  - subject: "PulseAudio?"
    date: 2007-02-11
    body: "Hi, I like very much what I have read about Phonon, and I would like to ask if there will be support for PulseAudio (http://pulseadio.org) as it seems it's evolving and becoming more popular now.\n\nThanks."
    author: "Josep"
  - subject: "Re: PulseAudio?"
    date: 2007-02-11
    body: "Looks like competition for GStreamer :)\n\nI guess if someone writes a pulseaudio backend for phonon, then pulseaudio will be supported as wel.."
    author: "otherAC"
  - subject: "Re: PulseAudio?"
    date: 2007-02-11
    body: "I don't think that PulseAudio is a competitor of GStreamer, they have orthogonal feature sets.\n\nPulseAudio is very likely already a supported output option of GStreamer, in which case it is also supported by the GStreamer based Phonon backend.\n"
    author: "Kevin Krammer"
  - subject: "Re: PulseAudio?"
    date: 2007-02-11
    body: "Judging from the name PulseAudio, it seems to deal with audio only.\n\nBut indeed, GStreamer can play to a PulseAudio server.\n\nhttp://img201.imageshack.us/my.php?image=snapshot1br2.png\nhttp://img74.imageshack.us/my.php?image=snapshot2ko4.png"
    author: "Tim Beaulen"
  - subject: "Authoring APIs?"
    date: 2007-03-22
    body: "I'm just curios about authoring APIs. Something on par with QuickTime - check the example: http://developer.apple.com/documentation/QuickTime/RM/CreatingMovies/MTCreateMovies/B-Chapter/chapter_1000_section_5.html  \n\nThe goal of Phone is all nice and evolutionary. But IMHO problem now is not a playback - but creativity support. KDE (and Linux desktop) need decent multimedia creativity support to go forward. After all to play some multimedia content - it (the content) has to be first created.\n\nThere are only few applications - e.g. Cinelera, Kino, mencoder, transcode. As my friend said if you want to buy camcoder and use it under Linux - do NOT buy camcoder: video encoding at best flaky, video editing is non-existent."
    author: "phony"
---
Like the previously featured articles on new KDE 4 technologies for 
<a href="http://dot.kde.org/1169588301/">Job Processes</a> or <a 
href="http://dot.kde.org/1167723426/">SVG Widgets</a>, today we 
feature the shiny new multimedia technology <a 
href="http://phonon.kde.org/">Phonon</a>. Phonon is designed to take 
some of the complications out of writing multimedia applications in 
KDE 4, and ensure that these applications will work on a multitude 
of platforms and sound architectures. Unfortunately, writing about 
a sound technology produces very few snazzy screenshots, so instead 
this week has a few more technical details. Read on for the details.


<!--break-->
<p>Phonon is a new KDE technology that offers a consistent API to use audio or video within multimedia applications. The API is designed to be Qt-like, and as such, it offers KDE developers a familiar style of functionality (If you are interested in the Phonon API, have a look at the <a href="http://www.englishbreakfastnetwork.org/apidocs/apidox-kde-4.0/kdelibs-apidocs/phonon/html/index.html">online docs</a>, which may or may not be up to date at any given moment).</p>

<p>Firstly, it is important to state what Phonon is not: it is not a new sound server, and will not compete with <a href="http://xinehq.de/">xine</a>, <a href="http://gstreamer.freedesktop.org/">GStreamer</a>, ESD, <a href="http://www.arts-project.org/">aRts</a>, etc. Rather, due to the ever-shifting nature of multimedia programming, it offers a consistent API that wraps around these other multimedia technologies. Then, for example, if GStreamer decided to alter its API, only Phonon needs to be adjusted, instead of each KDE application individually.</p>

<p>Phonon is powered by what the developers call "engines" and there is one engine for each supported backend. Currently there are four engines in development: xine, <a href="http://www.networkmultimedia.org/">NMM</a>, GStreamer and <a href="http://websvn.kde.org/branches/work/avkode/">avKode</a> (the successor to <a href="http://carewolf.com/akode/index.html">aKode</a>). You may rest comfortably in the knowledge that aRts is now pretty much dead as a future sound server, and no aRts engine is likely to be developed. However, aRts itself may live on in another form outside of KDE. The goal for KDE 4.0 is to have one 'certified to work' engine, and a few additional optional engines.</p>

<p>Other engines that have been suggested include MPlayer, DirectShow (for the Windows platform), and QuickTime (for the Mac OS X platform). Development on these additional engines has not yet started, as the Phonon core developers are more concerned with making sure that the API is feature-complete before worrying about additional engines. If the Phonon developers attempt to maintain too many engines at once while the API is still in flux, the situation could become quite messy (If you would like to contribute by writing an engine, jump into the #phonon channel at irc.freenode.org).</p>

<p>When an engine is selected by the user or application, Phonon will use the selected engine to determine what file formats and codecs each backend supports, and will then dynamically allow the KDE application to play your media. As it currently exists in the KDE 3 series, the user would have to manually change engines in each application (Kaffeine, Amarok, JuK, etc.) rather than being able to select engines for use across KDE.</p>

<p>Once an engine is selected for Phonon, it allows the programs to do the standard multimedia operations for that engine. This includes the usual actions performed in a media player, like Play, Stop, Pause, Seek, etc. Support also exists in Phonon for higher-level functions, like defining how tracks fade into one another, so that applications can share this functionality instead of re-implementing it each time. Of course, some applications will want more control over their cross-fading, and so are still free to design their own implementation.</p>

<p>The engine with the greatest progress so far is xine, which I was able to set up and run on my system. I was unable to get the NMM (notoriously hard to compile/setup) or GStreamer engines to compile on my system, whilst avKode is currently disabled by default. I would show you a screenshot of Juk or Noatun playing audio with Phonon, but right now these applications look just like their KDE 3.x versions (only with a somewhat ugly/broken interface!). When they are getting polished for release, I will show them off in a later article.</p>

<p>Matthias Kretz offers a <a href="http://static.kdenews.org/danimo/phonon_device_switching.ogm">short video</a> which, if you turn your speakers on while watching, demonstrates device switching. Phonon lets you switch audio devices on the fly, and you can hear the specific moment when the music switches from his various outputs (headphones, speakers, etc.).</p>

<p>Matthias also submits the following screenshot of output device selection using Phonon's configuration module. This is also a work-in-progress, and so take it with a grain of usability salt.</p>

<p align="center"><img src="http://static.kdenews.org/danimo/vol6_4x_kcmphonon.png" alt="Phonon config from 4x devel"/></p>

<p>There are not many things that I can take a screenshot of which show Phonon in use (screenshots of an audio framework are notoriously difficult to compose!), but I can describe one of the neat side effects of using Phonon: <em>network transparency</em>. KDE has long used KIOSlaves to access files over the network as easily as if they were stored on your local computer. Multimedia apps like JuK or Amarok should be able to add files transparently over the network to their collections without having to be concerned about whether or not the back-end engine is aware of how to deal with ioslaves. This support is already partially implemented in KDE 4, and is most visible through audio thumbnails, which are working for many people over any KIO protocol, including sftp:// and fish:// - two popular protocols among KDE power users. They do not yet work for me due to some instability in the fish:// KIOSlave of my current compilation, but the developers in the #phonon IRC channel claim that it this functionality will be ready and working when fish:// is more stable.</p>

<p>So, Phonon, while still in development, is going to be a great pillar technology for KDE application programmers, making their job easier and removing the redundancy and instability caused by constantly-shifting back-end technologies, and (eventually) making support for other platforms a piece of cake. This means that those developers can spend more time working on other parts of their applications to ensure KDE Multimedia applications shine even more brightly than they currently do.</p>

<p>A couple of quickies here to note: Mark Kretschmann, lead developer for <a href="http://amarok.kde.org/">Amarok</a> has <a href="http://lists.kde.org/?l=kde-commits&amp;m=117041944607084&amp;w=2">officially opened up Amarok 2.0 development</a> this week, and seems to be quite interested in what Phonon can do for Amarok 2.0. He doesn't rule out keeping their own engine implementations, like they currently do in the Amarok 1.4 series. However, given its early stage of development, Phonon can likely be adjusted to ensure that it will do everything Amarok asks of it.</p>

<p>If you're looking for a way to help out with KDE and are not a programmer, Matthias Kretz, lead developer of Phonon (Vir on IRC) has requested some help in keeping the Phonon website up-to-date.</p>

<p>And lastly, a few translations of these articles have been popping up around the world in various languages. Sometimes more than one translation is happening for a specific language. If you are translating or plan to translate these articles, send me a message so that we can save everyone some work and avoid redundancy (lets keep the redundancy-reduction spirit of Phonon alive!).</p>

<p>Until next week...</p>

