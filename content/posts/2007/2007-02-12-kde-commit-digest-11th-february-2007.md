---
title: "KDE Commit-Digest for 11th February 2007"
date:    2007-02-12
authors:
  - "dallen"
slug:    kde-commit-digest-11th-february-2007
comments:
  - subject: "Kbabel replaced?"
    date: 2007-02-12
    body: "Wow, a replacement for kbabel?\n\nThat really implies the big step from kde 3 to kde 4: when whitching from kde 1 to kde 2, we whitched from ktranslator to kbabel..\n\nIt will be hard to say goodbye to kbabel, but if kaide is even better, then i'm looking forward to it nevertheless :)"
    author: "Rinse"
  - subject: "Re: Kbabel replaced?"
    date: 2007-02-12
    body: "you mean switched, not whitched :)"
    author: "Patcito"
  - subject: "Re: Kbabel replaced?"
    date: 2007-02-12
    body: "hmm, my keyboard fooled me :o)"
    author: "Rinse"
  - subject: "Re: Kbabel replaced?"
    date: 2007-02-12
    body: "Run it through babel and see. You will find that it is an old American dialect."
    author: "a.c."
  - subject: "Re: Kbabel replaced?"
    date: 2007-02-12
    body: "sounds promising:\n\"Some highlights of the current version:\n\nloads ru/messages/kdevelop/kdevelop.po (a 1 MB file) in approximately 1 second, while KBabel does this in 9 seconds! (I think this is because KBabel calls msgfmt, but i'll implement such checks in the background via threads).\n\nuses only 20 MB of RAM after this file has been opened (KBabel with Qt 4 uses 50 MB, KBabel with Qt 3 uses 70-90 MB) - I didn't subtract shared memory (Qt and other libs) from these numbers.\n\ndoesn't hang if you hold PgDown/PgUp/F3/... for a while (because CPU usage doesn't hit 100%, and indeed is less than 30% on my 1.8GHz processor).\""
    author: "some dude"
  - subject: "Re: Kbabel replaced?"
    date: 2007-02-12
    body: "Why is kbabel replaced ? It works quite nice the way it is, thank you..\nI mean, it's just the kind of program, that needs extra functionality, not lower requirements, you don't need it to run as a background process you know.. Almost forgot, it has a cool logo too :)"
    author: "viseztrance"
  - subject: "Re: Kbabel replaced?"
    date: 2007-02-13
    body: "Hope the new developer has some real experience with translation and works as closely with the folks on i18n-doc to make this really worthwhile. Without this, a lot of developers I talked to tend to miss how great KBabel really is from a practical translation point of view."
    author: "AC"
  - subject: "Top 10 Translations"
    date: 2007-02-12
    body: "Something seems fishy. Last week Danish and Swedish translations was almost at the top of the list, now they are not even represented.\n\nTake a look at this and tell me how they arrange this list:\nhttp://l10n.kde.org/stats/gui/trunk/toplist.php\n\nme no understand!"
    author: "pascal"
  - subject: "Re: Top 10 Translations"
    date: 2007-02-12
    body: "Just looked at it, and the list is messed up because of the migration to kde4 in another branch.\nSwedish translations for kde4 have been removed from trunk, but Dutch translations not yet.\nIn the toplist at l10n.kde.org  it now looks like that the Dutch team has far more translated than the Swedish team, while this is not the case in reality.\n"
    author: "Rinse"
  - subject: "only the real could be kept, sorry!"
    date: 2007-02-12
    body: ":)"
    author: "wii-user"
  - subject: "Plasma developement ?"
    date: 2007-02-12
    body: "First of all, lots of thanks to all the developers!\n\nWhat I noticed is that while you hear a lot about some apps (okular, koffice, etc.) and also about some frameworks (phonon, decibel, sonnett) you hear very little about what I consider to be the most critical part of KDE4: Plasma.\nAm I just not aware of any news about it or are they changing a lot under the hood but not making visible changes yet?\n\nPlease don't take this negative: I know all the KDE developers are doing a fantastic job and I want to thank them all for that, I'm just wondering why you hear so little about the most essential and visible part of KDE4...\n\nThanks in advantage for every response!\n"
    author: "wassi"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "Thanks in advantage = Thanks in advance\n\nSorry about that, english is not my native language..."
    author: "wassi"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "> I'm just wondering why you hear so little about the most \n> essential and visible part of KDE4...\n\nIt is fair enough to ask.  The reason is that there is very little Plasma-related activity happening in KDE's Subversion repository at present.\n\nDanny and Troy only write about stuff which is available in Subversion for everyone to download, build and try out themselves.  \n\nI am not working on this part of KDE myself, so I cannot give you the whys and wherefores.  \n\n"
    author: "Robert Knight"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "to add to the other comments, Krunner is part of plasma, a replacement for part of the functionality previously delivered by Kdesktop (run command dialog, screensaversettings/activation)."
    author: "superstoned"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "Plasma has had lib work, and kdesktop has been killed in favour of krunner, but this makes pretty lousy news, especially when I'm quite fond of screenshots myself...\n\nkicker is still running on KDE 4, but is not being fixed.  Plasma has a lot of libs and engines and stuff that have been flushed out already, but the actual interface is really only being programmed by one developer - and he's very active in other parts of KDE libraries work at the moment.  At this point, plasma is not yet an application, and mostly a collection of libraries. Honestly though, there are ways you can help it along though, as I'm sure Aaron could use the help."
    author: "Troy Unrau"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "So what you are saying is: \"At this time only Aaron is working on it.\" ?\n\nTo the best of my recollection (http://plasma.kde.org/cms/1084) Plasma was about \"pervasive, systemwide API for scripting\" + \"Enabling of new, glitsy visual effects\" + \"Java, Python, Ruby\"\n\nThe scope sounds a lot like the \"WinFS\" - the cool, magical file system extension Vista couldn't wait for any longer.\n\nIt's just an idea, but:\nMay be useful to scale down aspirations a bit for 4.0 in terms of full Plasma delivery. For the shebang factor it is possible to pass a brushed up SuperKaramba (add 1-3 \"4.0-enabled\" applets) for Plasma Lite. This way real plasma can trickle in in a form of scripting API for core and apps in point versions. This way noone will be yanking developer's chain asking \"are we there yet?\" \n\n\n"
    author: "Daniel"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "Well, plasma is the most anticipated of KDE 4 technologies by users, since it directly affects their user experience.  There are other things being worked on as well in KDE that will affect the KDE 4 experience that are much farther ahead than plasma that I've been showing off, but there is still quite a lot of time before KDE is released.  I'd say more like a year.\n\nThe KDE developers build dcop in a day.  A year should be plenty for a kicker+superkaramba replacement.  Just don't expect it to release with 100 applets.  And really, that is all that plasma is -- a kicker type program with applets.  The difference being that these applets don't necessarily have to live on the panel, they can live on the desktop too, like superkaramba.\n\nThe libraries to power these applets have already seen some work, there just isn't a nice binary to tie things together yet. We haven't even hit alpha yet :)\n\nThe stuff we're showing off for KDE 4 is the stuff that's received the most work.  Phonon is required to build KDE applications, plasma is not - so obviously it makes more sense to work on Phonon early than plasma.  The fact that some KDE 4 programs are stable enough to take some screenshots does not mean KDE 4 is anywhere near done :)  You could build the sources yourself if you want to take a look - it's kind of painful :)"
    author: "Troy Unrau"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "But why are there no KDE4-devel liveCDs featuring the old user interface components but which enable you to test the new ones.\n\nThe user interface comes last, okay. But then why is there no \"KDE4 unstable\" release with the old user interface. Nightly previews so to speak."
    author: "Bert"
  - subject: "Nightly releases"
    date: 2007-02-12
    body: "There is only it's not nightly, you can get it any time you want:\n\nsvn checkout svn://anonsvn.kde.org/home/kde/trunk/KDE/kdelibs\nsvn checkout svn://anonsvn.kde.org/home/kde/trunk/KDE/kdepimlibs\nsvn checkout svn://anonsvn.kde.org/home/kde/trunk/KDE/kdebase\n\netc... build according to instructions on developernew.kde.org\n\nOr alternately, use http://kdesvn-build.kde.org/ - a script that checks out and builds KDE 3.5 or 4.0 from SVN automatically.\n"
    author: "Troy Unrau"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "If the initial scope is just API and support centered on kicker functionality = that's manageable enough. \n\n\"but there is still quite a lot of time before KDE is released. I'd say more like a year.\"  Let's make sure the audience doesn't just start yawning and leaves. "
    author: "Daniel"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "<rant>\n\n\"Let's make sure the audience doesn't just start yawning and leaves.\" \n\nYou are complaining about lack of plasma news mostly.  Plasma is not ready to make the news yet.  I have a policy of only covering topics that have become defaults for KDE, this means no plasma articles yet.  However, you users have started to imagine plasma as this great technology that'll KO all competitors.  I've seen websites that claim that Plasma will be able to run OS X dashboard applets and other such nonsense.  Quit making things up and contributing to the hype.  Plasma will be a kicker replacement that's capable of running some superkaramba type widgets... nothing more.  The fact that it isn't done yet is not a big deal, since KDE 4 is not even in alpha quality shape yet.  Sure, some apps and some technologies are ahead of the pack and ready to have articles written about them, but KDE 4 as a whole is months and months away still.\n\nYou cannot rush volunteers.\n\nAnd as far as 'losing our audience'... well, the writers for dot.kde.org are all volunteers.  There's no adverts on this site, and we make no money from writing any of our articles.  In fact, some of us lose money, based on the amount of time we commit, and bandwidth costs.  If our audience 'yawned and left', as you put it, we'd actually probably save money.\n\nWe'll voluntarily write about plasma when the other KDE volunteers have in in a state where we can write about it.  KDE 4 will be released when the volunteer coders, artists, documentation writers and translators think it's ready.\n\nEven the relatively few developers that have some sort of corporate sponsorship from trolltech, novel, canonical, etc. still all get to choose what parts of KDE to which they contribute.  So like the volunteers, they pretty much work on the parts that are the most interesting to them.  Right now, the opportunity is there to make big architectural changes to the foundation of KDE, the libraries, so that's where the interesting work is happening.  Projects like plasma are pretty much last on this list, since they need a good solid foundation to build upon.  As you can tell from all the articles about KDE technologies, there's a lot of great work happening in these libraries that will translate to a better plasma down the road, but by working on the backend first, it'll also have a better kword, konqueror, amarok, kopete, etc..  If those applications sucked, who would care about how pretty plasma turned out. \n\nSo guys, worry less about plasma.  When it's done, you'll hear about it on the dot.\n\nAnd worry less about KDE 4's release date, just know that when it does eventually get released, it'll have a lot of great new stuff.\n\n</rant>\n\nSorry Daniel, that wasn't necessarily directed at you alone, but at users at large that are complaining about having to wait for plasma news, and the KDE 4 release.  \"I'd say more like a year\" is based on my personal impressions.  There is no release schedule yet, so that estimate could still shift forwards or backwards quite a bit.  You'll know it's getting close when the alpha and beta releases start to arrive... my personal prediction is that we'll get our first alpha just before the Akademy KDE developer conference in Glasgow."
    author: "Troy Unrau"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-13
    body: "Asking for KDE 4 all the times reminds me of a scene in a movie about Michelangelo:\n\nMichelangelo painted the frescos in the Sistine Chapel and spent several years with that endeavour. Everytime the pope came along (knowing that he would not live forever) he kept asking Michelangelo with an impatient voice: \"how much longer will it take?\" ... and Michelangelo replied everytime with the same words: \"I am done with it, when I am done with it\". This scene is repeated several times during the course of the movie.\n\nAt the end of the movie the frescos were finished and the pope was able to enjoy the beautiful paintings before he died...\n\nI am sure we all will be happy with the results of KDE 4. Whether it takes 6 months, a year or two actually does not matter too much... it is done when it is done :-)"
    author: "MK"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-13
    body: "Okay, but the problem is, the pope financed Michelangelo.\n\nFree software is no cathedral, it only gets improved and mature when it is used, at least by skilled developers.\n\nMozilla was such a cathedral. I became slowly usable but everyone took part in betatesting and you saw the progress. When it was was fully ready, it took off.\n\n3.5.x is the stable branch\nbut what about KDE 4 development versions. You cannot improve what you don't use. And KDE lacks a pope that finances Michelangelo for years."
    author: "Andre"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-14
    body: "Well, go ahead and start using kde4 :)\nBut keep in mind that betatesting usually happens with beta versions of the software, and kde4 has not even reached the alpha stage yet.\n\nyou can find kde4 packages at\nhttp://software.opensuse.org/download/KDE:/KDE4/openSUSE_10.2/\n\nor at the repository of your kde friendly distributor\n"
    author: "Rinse"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-13
    body: "Your rant misses the point. You have a mature plattform, that is KDE 3.5.x where app developers don't want to invest time because it will get replaced by KDE 4. This is the first problem. And KDE 4 is not existing, so app developers don't code for it either.\n\nKDE 4.0 will be \"immature\" while KDE 3.5.x will be rock-solid by then.\n\nThe question is whether by early 2008 anyone will be excited about the software anymore and whether everyone will use the software of a competitor. And all this is not about plasma. And if it was. Think of Suse's new KDE menu. Why not release a KDE 3.6 (aka KDE 3.5.8) with the new Suse menu, a different crystal skin and phonon. Users would be excited! \n\nThe second point is developers. True, KDE failed to get involved with the business community. How much can you rely on volunteers? They usually lack the ressources to do the boring parts. Successful Free software is usually developed in an evolutionary style. For KDE 4 you break it up, but lack the ressources to put it together again in a timely manner. Why does KDE fail to recruit volunteers? Maybe it is because contributing is too complicated. Or simply a hen-egg problem. \n\nIf you had that KDE 3.6 Suse Menu, new skin, phonon, then app developers could at least start to use phonon for their applications etc. and observe productive use.\n\nThen it is said: KDE 4.0 will only features a subset of what is planned in the KDE4 cycle. Reminds me of Vista. In the end KDE4 will be of little interest. While KDE 4.0 will be release next year and won't feature what was promised. I mean think about it. You don't have the time. App users will not code for KDE 3.5.x anymore. KDE 4 developers are yet scary. And users will get bored or try out a different plattform."
    author: "Randy"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-13
    body: "You have a very grim outlook for KDE 4.  I'll just hit a few points.\n\nPhonon is a Qt4 technology, so using it in KDE 3 is not realistic.  The Suse menu, which while new and interesting, does have usability issues, and will not likely become a default in KDE.  The main reason being, it takes more clicks in that menu to get what you want (if it's not frequently used).  Crystal has not been developed for a while, while the Oxygen icon set has grown by leaps and bounds for KDE 4.\n\nActually, this statement bugs me the most: \"True, KDE failed to get involved with the business community.\"  This isn't really true.  Redhat, Apple, Canonical, Trolltech, Linspire, IBM and many more are contributing to KDE in one way or another, be it sponsoring developers, bandwidth, conferences, and more.  And there are smaller contributions that help too, for instance the developer of k3b has received free optical drivers from LaCie in order to ensure that his application works flawlessly with their equipment.  This is really only one sample, but the list of companies supporting KDE is very long, but perhaps less published than you would like.  (/me notes that perhaps this is something I can personally remedie...)\n\nLastly, KDE 4.0 will include all of the technologies I'm publishing for the Road to KDE 4 articles, since I am taking care to only publish those things that I can confirm are not vapour, and are already set as the defaults for the KDE 4 source tree.  So, Phonon, Decibel, SVG, KJobs, Kalzium3D, flake (koffice), etc. are all polished to the point where I'm comfortable in publishing them.  That doesn't mean that they are bug free, or 100% feature complete (for example phonon devels say they are probably 75% done with their coding, but that doesn't count bug fixing) nor that they will change the world for KDE 4.0.\n\nBut this has happened before for KDE, back in the 2.0 release.  KDE 1.1.2 was a solid release (kind of like 3.5.6 is now), and the KDE 2.0 development series had huge goals.  Let me just explain: Konqueror was born, dcop was born, arts was born, kio_slaves was born, KDE moved all of it's artwork to hi-colour from a 40 colour palette, new themes were created, kparts was developed, kicker was born, konsole too, and much more.  This was a huge technology reworking of an already successful environment, but those KDE 2 technologies (except for arts maybe) have all been a hit, and have lived onwards to KDE 3.5.6.  \n\nKDE 4 is our chance to rework our libraries once again, making these far-reaching changes to our API.  We have solid, phonon, decibel, oxygen (icons, style, windeco), kwin_composite branch (the compiz/beryl killer), krunner, konsole_split_view branch (pretty much a rewrite), flake, sonnet, akonadi, kuiserver, Cmake transition (the build system doesn't make you want to take a nailgun to your head anymore), wholesale changes to the API to make in cleaner and more readable and even easier to use.\n\nThe stuff I've mentioned are only the things that have already reached a state of usability already.  There are other things happening, like the liveui dynamic XML driven user interfaces for applications (this won't make it for 4.0, so I haven't been talking about it), the dolphin experiemental file manager, and so forth.  I'm taking care not to mention these things in my articles so I don't build up hype of the same nature that plasma currently has.  This sort of hype is terrible for plasma, because no matter how good it is in KDE 4.0, some people will feel let down.  Like I said, people are publishing things saying plasma will run OS X dashboard widgets natively and other nonsense... don't believe the hype.  \n\nSo hold onto your panties :) KDE 4 is coming, and it'll be big.  \n\nIn the meantime, KDE 3.5.x will probably be releasing every 3 months or so until KDE 4 gets here.  Some great work is still happening there, and new applications are still emerging for that platform.  Have a look at basket.kde.org for a great new KDE 3.x application that is just hitting 1.0.  Or check out mailody, a new IMAP centred email client for KDE 3.x that tries to be a better KMail for IMAP.  These programs are not part of the official KDE packages, but they are developing for a stable KDE 3.5.x platform that will be around for quite a while, even after KDE 4 is released.  I expect most distros (except maybe gentoo) will be shipping the KDE 3.5.x libraries for at least a year after KDE 4.0 goes live, if not longer - mostly on account of the fact that there are so many great KDE 3.x applications out there that would need time to migrate.  \n\nSo if you're developing a new application for KDE and have to choose between KDE 4 and KDE 3.5.x, I'd go with KDE 3.5.x right now.  Porting to KDE 4 and Qt 4 later will pretty much be a search-and-replace type operation -- Amarok 2.0 (the KDE 4 version) just started last week.  They took the Amarok 1.4.5 code and ported it to KDE 4 over a single weekend.  And it was able to run and play audio using it's old backends.  Adding a phonon backend capable of playback apparently took them less than two hours to create (a testiment to how easy the Phonon API is perhaps).\n\nIn fact, perhaps I should write an article on this topic."
    author: "Troy Unrau"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-13
    body: "/* Actually, this statement bugs me the most: \"True, KDE failed to get involved with the business community.\" This isn't really true. Redhat, Apple, Canonical, Trolltech, Linspire, IBM and many more are contributing to KDE in one way or another, be it sponsoring developers, bandwidth, conferences, and more. */\n\nI know, these are the contributions. But its not really \"money\" so to speak. More small donations. Everybody gets this.\n\nWhen you are in a business environment, then you need to speak business. Build a joint alliance with them, give them some good press, get them on a board. Provide clean interfaces to them.\n\n/* And there are smaller contributions that help too, for instance the developer of k3b has received free optical drivers from LaCie in order to ensure that his application works flawlessly with their equipment. */\n\nIsn't that natural? Do you expect a news magazine to pay for its test equipment?\n\n/* This is really only one sample, but the list of companies supporting KDE is very long, but perhaps less published than you would like. (/me notes that perhaps this is something I can personally remedie...) */\n\nSure, sure.\n\nBut the fact is that KDE is seriously underfunded. So something needs to be done. Free Software is often not sustainable. It creates a precariate of exploited hackers, but the problem is just that there is no money but small money makes huge difference. So all that is needed is an environment where it is natural that you invest money into it, that you think in terms of investment. \n\nJust get a board of real business people to provide guidance. They will end up in financing KDE and turning KDE ndevelopment into a business. And the KDE distributors? I don't see how they are officially involved, on a top level basis."
    author: "Andre"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-14
    body: "I agree.  KDE tends to be by hackers for hackers.\n\nWhat we need is for IBM to support KDE and spend like $100M on it.\n\nI have been in business with my Father who was an accountant.  I make statements as to what will be more appealing to business customers and developers simply flame me."
    author: "James Richard Tyrer"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-14
    body: "I suspect the reason people have \"flamed\" you in the past is because of the way you tend to assert your opinions. It certainly isn't because KDE people are anti-business."
    author: "Paul Eggleton"
  - subject: "When business $ is good? not now."
    date: 2007-02-15
    body: "You raise a very good point about commercial funding and business involvement into KDE. Money is is what separates good product from mediocre one. But, that works perfectly only for products, not architecture / technology.\n\nKDE4 appears to be 2 things:\n- quantum leap in underlying tech\n- improvements in applications built on top of that leap.\n\nIf you look at Akonadi, as an example, you can imagine how many commercial interests would gladly massacre the tech to plug in their own in-house-friendly solution. Same IBM just showed off their cross-platform Notes solution a week ago - an almost direct competitor.\nSame goes for Strigi, (where Google, Novell etc are likely vultures) and other core tech. \n\nOnce the good core is delivered, then tin-foil-cap people can relax a bit and let the hacking fest in.\n\nThat's where my humble (non-core-developer) opinion comes in:\nBecause of diverse desire to cram as much as possible into a definition of KDE, things start to look Vista-like. \nDespite opium-induced-like enthusiasm for instantaneous release of beautiful Plasma fluff with the first release of KDE4, it's all that - enthusiasm.\nI am heavily involved in SuperKaramba scene and really good widgets take 1-2 years of tweaking and generations of API changes and fixes.\n\nSo, enthusiasm for the whole shebang is good, but what really works in the end is incremental improvement. \n\nI hope to see KDE4 come out sooner with absolutely necessary MINIMUM of features and start growing fur in point releases. If some real, practical set of what will be KDE4 Technology release is not established and rushed out, this will start to look more and more like Communist Society and Cold Fusion."
    author: "Daniel"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-14
    body: "\"/* Actually, this statement bugs me the most: \"True, KDE failed to get involved with the business community.\" This isn't really true. Redhat, Apple, Canonical, Trolltech, Linspire, IBM and many more are contributing to KDE in one way or another, be it sponsoring developers, bandwidth, conferences, and more. */\n \n I know, these are the contributions. But its not really \"money\" so to speak. More small donations. Everybody gets this.\"\n\nIt is money in the sense that they hire developers to work full time on kde development.\nKDE is niet 100% voluntarily, there are also payed developers involved.\nThat is not a small donation...\n"
    author: "otherAC"
  - subject: "Qt3 or Qt4?"
    date: 2007-02-14
    body: "\"I'd go with KDE 3.5.x right now.\"\n\nYou won't make friends with Aaron like this :)\nJust when one thought people could be convinced to start finally with a Qt4/KDE4 port to speed things up..\nDevelopment of a non-applet type of app will take it's time too, so why not just start with KDE4, when your app is releasable, KDE4 is not long away. Or start with Qt4 and do a Qt->KDE conversion later. Perhaps easier than KDE3 -> KDE4 if you start building logic stuff first, not the KDE integration bits.\n\nAnd the one weekend needed for Amarok conversion: well, it compiles now.. adapting it to the new API and technologies and getting it to look like before will take a little bit more than this, I fear."
    author: "Phase II"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-14
    body: "Are we a victim of our numbering system?\n\nIf we were to port KDE-3.6.0 to Qt-4 and make a totally new KDE 4.0 that also uses Qt-4, what should we call them?\n\nIs the lack of a proper slot for KDE-3.6.0 built against Qt-4 the reason that nobody even wants to think about it?  Or is it even possible since it would have to run KDE 3.x.y apps built against Qt-3.x.y as well as apps built against Qt-4"
    author: "James Richard Tyrer"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-14
    body: "> Is the lack of a proper slot for KDE-3.6.0 built against Qt-4 \n> the reason that nobody even wants to think about it? \n\nI'll say.  Porting KDE from one major version of Qt to the next is a major endeavour, not a few small changes here and there.  By \"major endeavour\", I mean a couple of years worth of work.\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-14
    body: "\"Then it is said: KDE 4.0 will only features a subset of what is planned in the KDE4 cycle. Reminds me of Vista. \n\nBig difference between kde4 and Vista is that the latters is not done by volunteers, but by the largest softwarecompany in the world with allmost unlimited resources.\n\nNow what does that tell us?\nIt tells us that corporate backup or a lot of money is no guarantee that development will speed up or that promised features will show up in the next release.\n\n\" While KDE 4.0 will be release next year and won't feature what was promised. \"\nMost 'promises' kom from non kde4 developers that try to get an idea about how kde4 should look like.\nIf you check the roadmap of kde4 and similar pages, there are no promises that kde4 cant keep.\nAnd official kde developers already warned that kde4 is not the same as kde4.0, just like kde2 was not the same as kde2.0 or kde3 as kde 3.0\nLike in all those versions, the follow-ups will start using the new technology to it's full amount, not the .0 release.\n\n\"\nI mean think about it. You don't have the time. App users will not code for KDE 3.5.x anymore.\"\n\nHaven't checked kde-apps.org lately i guess?\nEvery day there is new code for 3.5 announced on it.\n\n\"And users will get bored or try out a different plattform.\"\nIf kde 3.5 bores you, try using another theme or start fidling around with beryl and kde ;)\n"
    author: "otherAC"
  - subject: "Plasma Fact Correction"
    date: 2007-02-14
    body: "While I agree with your statement, this one is wrong:\n\"I've seen websites that claim that Plasma will be able to run OS X dashboard applets and other such nonsense.\"\n\nPeople state it because Zack Rusin announced it, resulting from work on khtml.\n\nhttp://www.kdedevelopers.org/node/1715\n"
    author: "Phase II"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "Please Do Not decide about Plasma, before you talk to Aaron or another developers.if making Plasmoids for Plasma is easy, like creating Karamba's, then the Library itself is important and making Plasmoids will not take much time.Im sure Aaron has plans for Plasma.\n\nP.S:(Danny, this weeks commit-digest was really intresting.I cant wait for next week's digest)\n\nP.S 2: Trol, Im waiting for Road To KDE4 next vol.last week you commited Phonon files into SVN, so I found what will you talk about, please commit something again!maybe you're going to talk about Solid?\n\nSorry for my bad English."
    author: "Emil Sedgh"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "You'll have to wait until Tuesday like everyone else. :)"
    author: "Troy Unrau"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-13
    body: "It's tuesday! ;-)\nI can't wait for the next episode =)."
    author: "Thomas"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-12
    body: "Thanks a lot for all your answers!"
    author: "wassi"
  - subject: "Re: Plasma developement ?"
    date: 2007-02-14
    body: "In my humble opinion, KDE4 should, and will, ship without Plasma on 4.0 version. You know, you don't need to trash all KDE3 applications at once, just port the old kdesktop and kicker to qt4, release it, then when Plasma is ready, ship it.\n\nI think plasma is just two years from now, if they plan to make what was promissed. It just there are too few people working on it, and it's very away from even having some libraries to start working abouve. I hope KDE4 don't is delayied just because one or two techonologies aren't done.\n\nI agree with people that think KDE should move soon to 4 version because it will make it's development faster, even more because it will run apps in Win32 and Mac."
    author: "Iuri Fiedoruk"
  - subject: "page scaling zoom!!!"
    date: 2007-02-12
    body: "Very cool, and fast as well. Doesn't feel a bit slower compared to the KDE 3.x zooming... And I'd say dump the 'scale only fonts', who would use that if you have real zoom?"
    author: "superstoned"
  - subject: "Re: page scaling zoom!!!"
    date: 2007-02-12
    body: "Because if you're just reading a page of text, and want to increase the size, you don't want the lost screen real-estate you get if all the irrelevant images increase as well.\n\nI haven't seen this particular implementation in action, but I know the IE7 implementation doesn't re-flow the page, simply scales it, adding horizontal scroll bars.  That's fundamentally wrong in my opinion - people increase the size of the page to increase its readability - causing horizontal scroll bars to be added _decreases_ the readability."
    author: "mabinogi"
  - subject: "Re: page scaling zoom!!!"
    date: 2007-02-12
    body: "\"I haven't seen this particular implementation in action, but I know the IE7 implementation doesn't...\"\n\nI did try it... and it really really sucks for particular uses. Scaling zoom distorts the images and pushes the page out of bounds, while all I want to do is make the text more readable. Blurry images started to be annoying very fast."
    author: "Daniel"
  - subject: "Re: page scaling zoom!!!"
    date: 2007-02-12
    body: "Those that want the scaling effect of HTML pages. Often, you don't want to blow up pages wholesale, but just make the text bigger. If you only increase the font size then, the page can still fit on your screen."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: page scaling zoom!!!"
    date: 2007-02-12
    body: "not necessarily... If you have 20\" or bigger monitors, you may like to get your wireless keyboard and sit back in an armchair to read online news and the like from 2-3m distance to the monitor. zooming the whole page with pictures is what you want. Horiz scrollbars are not a problem as you may have wide-screen anyways (e.g. 1920 x 1200)\n"
    author: "Thomas"
  - subject: "Re: page scaling zoom!!!"
    date: 2007-02-12
    body: "Thats why he said 'often'. Not 'always' ;)\n\nYour usecase is not as common as you might think, though."
    author: "Thomas Zander"
  - subject: "Zoom is good. All kinds of zoom."
    date: 2007-02-12
    body: "How, exactly, do you know that the use case is \"not as common\" as Thomas believes? Media PC:s are becoming very common. It's not like 14 inch screens should \"be enough for anyone\" these days. \n\nEven if you were right, why cripple the feature set on a \"tyranny of the 51%\" basis? Though actually this is more like the tyranny of \"the hypothetical ghost users.\""
    author: "the libertarians say \"hi\""
  - subject: "Re: Zoom is good. All kinds of zoom."
    date: 2007-02-12
    body: "if you have a huge monitor, then the website will be displayed huge as well in full screen.\nNo need to zoom anything in such a case.\n\nif you still can't see the pictures, try sitting closer to your tv screen or use another dpi-setting for the screen.\n"
    author: "Rinse"
  - subject: "Re: Zoom is good. All kinds of zoom."
    date: 2007-02-12
    body: "Which would be good if all webpages specified image sizes in dpi."
    author: "JohnFlux"
  - subject: "Re: Zoom is good. All kinds of zoom."
    date: 2007-02-12
    body: "wallpapers usually come in different sizes ;)\n\nAlso compiz/beryl has a nice zooming effect for people who tend to place the monitor several meters away from their chair.\nYou could try them out as well..\n\n"
    author: "Rinse"
  - subject: "Re: page scaling zoom!!!"
    date: 2007-02-13
    body: "Your post made me thinks that about the ways to do page scaling:\n- page scaling with or without reflowing (to remove the horizontal scrollbar).\n- page scaling font only or font and images.\n\nSo 2 options * 2 options = 4 possibilities, each totally sensible (and I would add that reflowing a webpage without zooming is useful too), the problem is: how is-it possible to provide that many choice to the user in a sensible way?\n\nMaybe:\n+,-: by default scale font and image\nshift+,shift-: only scale fonts.\n*: remove reflowing/scaling.\nF12: reflow webpage (in Opera it is F11 which suck because it is in the middle of a keygroup F12 being at the edge is better).\n\nJust a few random though.."
    author: "renoX"
  - subject: "Kmail and mht"
    date: 2007-02-12
    body: "I understand this is no help forum, but I have a question:\n\nI am subscribed at a mailing list and get my mails in digested form. The only problem emerges when someone sents an attachment along with his mails e.g a doc file. Then I have to look at the mailman archives and load the attachment as a an .mht-file from there.\n\nFF suggests me to import mht files with kmail but that import fails except the header. rtf attachments seem to be unsupported. However, the format looks very simple.\n\nhttp://en.wikipedia.org/wiki/MHTML\n\nWhat can I do to open these files and extract the rtf attachments? "
    author: "Andre"
  - subject: "Re: Kmail and mht"
    date: 2007-02-12
    body: "you could try http://kde-forums.org for questions like this one\n"
    author: "Rinse"
  - subject: "what do you mean by all those commits to branch "
    date: 2007-02-12
    body: "just a newbie question, when i saw all those commits to branch/kde 3.5 do you mean we will see kde 3.5.7 ?\n\nand thanks really Danny for this review, as the code begun to flow, we have now faith to see kde 4 sonner ( i mean at last after 5 months ;(\n\nfriendly "
    author: "djouallah mimoune"
  - subject: "Re: what do you mean by all those commits to branch "
    date: 2007-02-12
    body: "AIAFK there will be a 3.5.7 and probably also .8, .9, etc.\nAs long as kde4 is not on the horizon kde3 applications will continue to improve"
    author: "Rinse"
  - subject: "Re: what do you mean by all those commits to branch "
    date: 2007-02-12
    body: "See my rant above...\n\n3.5.7 is planned, but no release date has yet been set.  I've heard whisperings saying 3 months from now.\n\n4.0 is more than 5 months away yet.  To kind of get an example of how the release will work, check out this website: http://developer.kde.org/development-versions/release.html\n\nWe haven't hit step 2 yet, as the libraries are still very much a moving target.  I'm not sure if we have a release dude yet either.  And yes, this individual has usually been called the 'release dude' :P  This position is possibily one of the most demanding, high stress jobs that a KDE developer can take, so we don't always designate the same person.\n\nHope this helps..."
    author: "Troy Unrau"
  - subject: "Re: what do you mean by all those commits to branch "
    date: 2007-02-13
    body: "The release dude position has been \"killed\", it's now a release team to spread the stress on more people ;)"
    author: "Cyrille Berger"
---
In <a href="http://commit-digest.org/issues/2007-02-11/">this week's KDE Commit-Digest</a>: Much-requested "Page scaling" zoom mode introduced to <a href="http://www.khtml.info/">KHTML</a>. Work on the XPS document format backend, and intergration of a <a href="http://phonon.kde.org/">Phonon</a>-based audio player for embedded document sounds in <a href="http://okular.org/">okular</a>. More maps added to <a href="http://edu.kde.org/kgeography/">KGeography</a>. KMines becomes the latest game to move toward a scalable graphics interface, with continued work on KBlackBox and KGoldRunner. scuba and wmap datasource additions to <a href="http://kst.kde.org/">Kst</a>. A better fullscreen interface for <a href="http://www.digikam.org/">Digikam</a>. Continued improvement in the KDE Fonts Manager. <a href="http://amarok.kde.org/">Amarok</a> 2.0 development progresses at full speed. Initial import of version 2 of the <a href="http://gwenview.sourceforge.net/">Gwenview</a> image viewer, and a possible <a href="http://kbabel.kde.org/">KBabel</a> replacement, KAider, into KDE SVN. <a href="http://oxygen-icons.org/">Oxygen</a> icons become further integrated into the desktop, with renamings and the setting of the theme as the KDE default.
<!--break-->
