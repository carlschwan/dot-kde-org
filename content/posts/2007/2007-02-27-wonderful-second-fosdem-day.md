---
title: "A Wonderful Second FOSDEM Day"
date:    2007-02-27
authors:
  - "jmourik"
slug:    wonderful-second-fosdem-day
comments:
  - subject: "Thanks for the detailed report"
    date: 2007-02-27
    body: "And the slides already up for consumption, perfect :)"
    author: "Frinring"
  - subject: "wow @ slides"
    date: 2007-02-27
    body: "wow @ kde 4 slides !"
    author: "funnyfanny"
  - subject: "KDE 4 slides and Oxygen theme"
    date: 2007-02-27
    body: "Most of the screenshots in the KDE 4 slides are using Thomas' Oxygen style which is very much work-in-progress, with key widgets changing noticeably from week to week.  I think that was unwise myself - it makes the applications look more broken than they really are.  \n\nFor anyone else planning to present KDE 4's progress at shows like this in the near future, please don't use it until it is more mature. "
    author: "Robert Knight"
  - subject: "Re: KDE 4 slides and Oxygen theme"
    date: 2007-02-27
    body: "On one hand, you're right, on the other hand, I tried to finally show a diff between KDE 3 and KDE 4 ;-)\n\nMany ppl have been asking for screenshots from KDE 4, and I tried to give'em something.\n\nBtw, I explained the weird look and artifacts here and there during the presentation, but that's not really clear in just the slides. I figured it's not really giving much away, as the look will still change a lot, and the screenshots aren't that clear... Thanx to OO.o - what is it with PDF and Office apps? Koffice can't create pretty text, OO.o f*ks the pictures... ???"
    author: "superstoned"
  - subject: "The ogg"
    date: 2007-02-27
    body: "Why must FOSDEM take so long to upload the ogg from this presentation? "
    author: "Ben"
  - subject: "Re: The ogg"
    date: 2007-02-28
    body: "As far as I know there was no-one filming this talk, so no ogg on the fosdem website. Maybe it shows up on youtube, who knows.\n\nGreat talk Jos, congratulations\n\n\nStecchino"
    author: "Bart Cerneels"
  - subject: "Re: The ogg"
    date: 2007-02-28
    body: "Damm :("
    author: "Ben"
  - subject: "Edu follow up"
    date: 2007-02-28
    body: "A first draft on what happened during the workshop is available here\nhttp://wiki.kde.org/tiki-index.php?page=FOSDEM2007EduReport\nAnne-Marie"
    author: "annma"
---
The second day of <a href="http://fosdem.org/2007">FOSDEM 2007</a> was as busy, if not more, as the first day. Many face-to-face interactions, of great benefit to cooperation between developers and projects, and time spend on hacking on and promoting KDE. The KDE developer room was well used, first by an Educational workshop, well led by Anne-Marie Mahfouf, followed by some more talks. Topics included Krita's present and future by Bart Coppens, a KDE 4 talk by Jos Poortvliet and a KDE e.V. talk by Sebastian K&uuml;gler. Read on for a report on day two.





<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right; width: 240px">
<a href="http://static.kdenews.org/jr/fosdem-2007-edu.jpg"><img src="http://static.kdenews.org/jr/fosdem-2007-edu-wee.jpg" width="240" height="180" /></a><br />
The Educational Workshop brought together developers from several projects.
</div>
<h2>Education</h2>

<p>Everyone interested in free Educational applications gathered in the KDE developer room to listen to a talks. Many of those quickly turned
into discussions about cooperation between developers from the different applications, and ways of easily managing all the software and settings for teachers. Finally, the discussion led to the agreement there was more communication and cooperation needed, especially for creating
 bridges between the several management systems. Specifically mentioned was the Get Hot New Stuff framework in KDE, which is used by many of the KDE Edu applications to download and install data like dictionaries or additional lessons. Several ideas came up, asking local network support so the teacher can distribute lessons or other data to the students and upload integration in Get Hot New Stuff. Integration in KIOSK would make these much easier to manage for the teachers. Another problem is that the most used server for GHNS data, kde-files.org, isn't owned by
 KDE, and the Edu people don't have access to it. So when users upload useful data, for example for Khangman, but there is a small bug in the
file, it can't be fixed unless the submitter steps in - which isn't always the case. And of course, if the maintainer(s) of kde-files.org decides to move on to another hobby, the data should be preserved some way - and this can't be ensured right now. The conclusion was to ask the KDE
e.V. to step in here. They can probably help fund a new site for the data, or make an agreement with the current kde-files.org website owner.</p>

<h2>Cross Desktop</h2>
<p>During the educational track, we joined the Gnome Developer Room for  a <a href="http://fosdem.org/2007/schedule/devrooms/crossdesktop">Cross-Desktop</a> series of talks, where search engine Strigi, query standard Wasabi, instant messenging backend Telepathy and others where discussed. Many KDE developers  attended these talks and participated in the discussions. The technology behind Strigi, the tool delivering indexing, metadata and database functionality to KDE 4, was
discussed. Wasabi is a project to develop specifications and APIs for search and meta-data services. It has partly been initiated by the Strigi author, Jos van den Oever and intends to unify desktop search interfaces so users can have applications use whatever search tool they want. Finally, Telepathy is a set of specifications and API's designed for chat and VOIP, and will be used in Kopete and supported by Decibel in KDE 4.</p>

<h2>Krita, the Present and Future</h2>
<p>After the Educational and Cross-Desktop tracks, the first KDE talk was done by Bart Coppens about Krita, the image manipulation application of the <a href="http://www.koffice.org/">Koffice project</a>. He spoke about the present situation and highlighted the neat new functionality available since Krita 1.6, like tablet support, layer masks and smudging very well. Bart then told about the "Feature Thaw", which means the
 KOffice developers add features in the form of additional effects and plugins to the stable branch of Krita. The second part of his presentation was about the future of Krita and KOffice. The change to KDE 4 and the work going on in the internals of KOffice 2 prompted the Krita team
 to rethink, redesign and improve several core technologies to improve integration with the rest of KDE and KOffice.
Some of these changes include the use of the new core KDE 4 technologies like Phonon, Kross (scripting framework) and Flake, the new lightweight embeding framework of KOffice. Another proposal Bart mentioned was the implementation of the Open Raster file format, which other projects like the Gimp are also working on.</p>
<h2>KDE 4 Presentation</h2>
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left; width: 240px">
<a href="http://static.kdenews.org/jr/fosdem-2007-kde4-talk.jpg"><img src="http://static.kdenews.org/jr/fosdem-2007-kde4-talk-wee.jpg" width="240" height="180" /></a><br />
The KDE 4 Talk had the audience standing in the aisles.
</div>
<p>Jos Poortvliet entertained an overcrowded and enthusiastic devroom with a talk about KDE 4. Clearly, the community is curious to know about
 the new KDE release. First Jos summarised the current situation and achievements made so far, which have lead to a consistent and stable user
 experience. Next he elaborated on the development on the foundations of KDE 4. Jos showed dozens of very juicy screenshots, highlighted minor and bigger improvements - even though it's hardly possible to touch more than the surface of KDE 4 in a 45 minute talk. Still, focussing on
the pillars of Solid, Phonon and Plasma, he gave a nice overall picture of the articles on the Planet, the Dot and development itself. A few questions came, concerning cross-desktop cooperation, some features, and of course 'when will KDE 4 be released'.</p>
<h2> The KDE e.V.</h2>
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right; width: 240px">
<a href="http://static.kdenews.org/jr/fosdem-2007-stall.jpg"><img src="http://static.kdenews.org/jr/fosdem-2007-stall-wee.jpg" width="240" height="180" /></a><br />
The KDE stall sold out of merchandise.
</div>
<p>The last talk was by Sebastian K&uuml;gler, concerning KDE e.V. and the legal underpinnings of the KDE project. He explained the role and structure of the e.V., and the way it tries to support the KDE development without any interference in technical issues.</p>

<p>There was a question about the lack of a legal mailing list. The questionner explained the <a href="http://europa.eu.int/idabc/">IDABC</a> project has recently been set
up by the European Union which wants to ensure and safeguard interoperability and cooperation in the area of software development in Europe. Sebas and the other attendees agreed setting up a line of contact to this group would be a great thing, and will be brought up in the KDE e.V. board meeting. One of the attendees then brought up the existing (but not very alive) KDE licensing mailing list, which will be looked into as well. Also, a news story about IDABC will be written to explain its purpose and what it can do for the KDE project in the area of legal advice and support.</p>

<p>Outside the KDE room, Amarok promoter Sven Krohlas gave a lightning talk about the advanced features in Amarok including dynamic playlists and how suggested tracks works with last.fm integration.</p>

<p>All in all, the second FOSDEM day was productive and fun for the KDE people.  Many thanks to the team from KDE-NL who organised the schedule and staffed the busy stall for two days demonstrating KDE 4, Amarok 2 and selling out of merchandise.</p>

<p>Slides from all these talks can be found on the <a href="http://www.kde.org/kdeslides/">KDE slides page</a>.</p>


