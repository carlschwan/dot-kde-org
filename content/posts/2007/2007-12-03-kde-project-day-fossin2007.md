---
title: "KDE Project Day at FOSS.IN/2007"
date:    2007-12-03
authors:
  - "abhattacharya"
slug:    kde-project-day-fossin2007
comments:
  - subject: "rock the house"
    date: 2007-12-03
    body: "i know you guys will rock the house =) wish i could be there this year (nearly made it) but with the people you've got coming, it's going to be friggin' awesome. enjoy and i hope to hear all sorts of cool stuff come out of the full day sessions =)\n"
    author: "Aaron J. Seigo"
  - subject: "An advice"
    date: 2007-12-04
    body: "I am happy to hear about this event.As kubuntuforums consists a large number of people,  it would have been better if you(or who ever is concerned) would have posted in the forum about this event at least 10 days or a week ago.Now i find many people are missing this event and they all have to wait for the next year."
    author: "Srikar"
  - subject: "Hope KDE will come to China"
    date: 2007-12-04
    body: "I'm a Chinese user and I've used KDE for many years. I hope KDE will hold a party in Shanghai,China in the future."
    author: "Qian zheng"
  - subject: "Re: Hope KDE will come to China"
    date: 2007-12-04
    body: "hey, I'm in hangzhou. maybe we could have a delayed release party once exams are over ;)\nI don't know of anyone else nearby, though. there's someone up in beijing iirc... I'm so bad at remembering this stuff :)"
    author: "Chani"
  - subject: "Re: Hope KDE will come to China"
    date: 2007-12-10
    body: "Well I'm in Shanghai and for sure I'd like to have a KDE party here :) KDE vs. Shanghai WTC : which will be released first ? ;)"
    author: "Alban"
---
KDE will be participating at
<a href="http://foss.in/2007/">FOSS.IN/2007</a>, India's premier
FOSS event in Bangalore. KDE Project Day is this Wednesday 5th December.
Project Day will have a complete starter course for eager
contributors to jump into KDE. Speakers would showcase various avenues
of contribution to KDE - artwork, documentation, translation,
development, marketing et al, basics of Qt/KDE programming and the
various frameworks as well as the state and future of KDE 4 and more.  This is the biggest
ever representation and splash by KDE on Indian soil. Read on for details including the new KDE India website.








<!--break-->
<div style="float: right;">
<a href="http://static.kdenews.org/jr/foss-in-2007.jpg">
<img src="http://static.kdenews.org/jr/foss-in-2007-wee.jpg" border="0" /></a>
</div>

<p>KDE Project Day schedule is as follows:</p>
<ul>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=548">Till Adam - The Pillars of KDE 4</a>
</li>
<li><a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=362">Pradeepto K Bhattacharya - How to be a part of the gang?</a></li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=550">Pradeepto K Bhattacharya - KDE 4 development setup</a>
</li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=269">Kevin Ottens - Qt/KDE Concepts and Frameworks: Overview</a>
</li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=484">Girish Ramakrishnan - Styling Qt Using Style Sheets</a>
</li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=247">Piyush Verma - Kdevelop3/4: Beyond C++...extending the Language Base.</a>
</li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=268">Kevin Ottens - Test Driven Development with Qt and KDE</a>
</li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=430">Prashanth N Udupa - Component Software Design/Development</a>

</li>
</ul>

<p>
The conf's complete schedule is available at
<a href="http://foss.in/2007/schedules/">http://foss.in/2007/schedules/</a>. There are some more Qt and KDE talks and
tutorials by core KDE dudes happening during the main conference:
</p>
<ul>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=270">Kevin Ottens - KDE/ISI Student Projects</a>
</li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=275">Till Adam - Akonadi - Personal Information Unleashed</a>
</li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=281">Volker Krause - Developing PIM applications with Akonadi</a>
</li>
<li>
<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=414">Simon Hausmann - QtWebKit</a>

</li>
</ul>
<p>Also an old KDE contributor Holger Hans Peter Freyther is speaking on
"<a href="http://foss.in/2007/register/speakers/talkdetailspub.php?talkid=343">Using OpenEmbedded to power Open Devices</a>".  There are plans to have
a Qt/KDE hack centre and BoFs sessions as well.
</p>
<p>
The local KDE community (<a href="http://www.kde.in">KDE India</a>) team has been working on various
stuff for the KDE Project Day @ FOSS.IN/2007. We'll have a booth at
FOSS.IN. There will be KDE swag like stickers and <a href="http://www.flickr.com/photos/anuragp/sets/72157603248086487/">posters</a>. All artwork and printing work has been done by local volunteers for the first time. KDE and KDE-India T-shirts were kindly sponsored by Swati and Tarique Sani of <a href="http://www.sanisoft.com">SANIsoft</a>. People we
expect to attend the KDE Project day and the conference as a whole
range from young students aspiring to contribute to KDE and FOSS in
general to developers/artists/translators/integrators who are
enthusiastic about the vibrant community. There is a lot of everything
for everyone :)
</p>
<p>
The gang is planning to have lot of fun at the conference and eat a
lot of awesome <a href="http://en.wikipedia.org/wiki/Cuisine_of_Karnataka">local food</a>.
</p>
<p>
The KDE-India local community became two years old on 2nd December. It was
formed over a KDE BoF in <a href="http://foss.in/2005">Foss.in/2005</a>. Since then we have reached a long way and there still remain many more goals to achieve.
</p>

<p>
On its second anniversary, KDE India team is proud to announce its new
wikified website: <a href="http://www.kde.in">www.kde.in</a>. The site aims to showcase Getting
Involved tutorials, event participation, regular interviews of Indian
FOSS contributors and personalities, Qt/KDE application reviews, localisation efforts and much more. If you have written something about the KDE project, an informative Qt/KDE tutorial or an interesting review on Qt/KDE application that is free and open source lately, then this is the time to get them published.
</p>






