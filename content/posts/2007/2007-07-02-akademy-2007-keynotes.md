---
title: "aKademy 2007: The Keynotes"
date:    2007-07-02
authors:
  - "jallen"
slug:    akademy-2007-keynotes
comments:
  - subject: "Keynote?"
    date: 2007-07-02
    body: "Aaron's talk was not a keynote."
    author: "binner"
  - subject: "Re: Keynote?"
    date: 2007-07-02
    body: "we know, but it was a lot LIKE a keynote (more vision, less technical stuff), and the tracks article was long enough already... (yeah, the first tracks one is coming soon)"
    author: "jospoortvliet"
  - subject: "Small clarification"
    date: 2007-07-02
    body: "> He also asks KDE to stop locking in users and applications to our framework\n\nThere were some questions from people in the audience who did not understand the request either.\n\nOne concrete thing that was mentioned is making sure that a KDE application works well on a Gnome desktop.  Apparently there are still some fit-and-finish problems, Gnome users can probably expand on this.\n\nOther than that, Jim was not able to express exactly what the ISVs meant by this point, assuming that they are not asking for something unreasonable.  Perhaps there is someone from Adobe, Skype, IBM etc. reading this who can clarify?\n"
    author: "Robert Knight"
  - subject: "Re: Small clarification"
    date: 2007-07-02
    body: "I think it refer to the lack of use of freedesktop standards in KDE 3, while other desktop like XFCE implement more. And perhaps it happens because some of the freedesktop standards comes directly from Gnome.\nBut it doesn't seems to be the case for KDE 4 anymore."
    author: "Nuscly"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "> I think it refer to the lack of use of freedesktop standards in KDE 3\n\nSuch as?\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "Well I can think of two, both fixed in KDE4\n\nDBus, KDE 3 uses DCOP instead.\n\nKDE dosn't use the unified icon naming schemes. \n\n"
    author: "Ben"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "KDE3 is also using D-Bus for the parts where it made sense, right?\nFor example talking to system bus services.\n\nAnd D-Bus is not a freedesktop.org standard, it is rather a technology hosted at freedesktop.org intented to be used by a wide range of projects.\n\nI agree on the icon theme spec, but I'm not even sure that it is even finalized.\n\nFact is that KDE always implemented specs as soon as possible, icon naming and shared MIME database just weren't possible until yet, all other have not only been implemented, they also have actively been worked on by KDE developer, e.g. menu spec, desktop-entry spec, trash spec, base directory spec, etc.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "I'm not attacking KDE or anything ;)"
    author: "Ben"
  - subject: "Re: Small clarification"
    date: 2007-07-06
    body: "(Not) shared mime db would be another one.\n\nWhen looking at freedesktop.org documents, there're sometimes defaults missing, as well as tools to change settings cross-desktop-wide, which can be a source of irritating discrepancy in behaviour of (Gtk,QT,...) applications.\n\nAnnoying is that there isn't a common shortcut system desktops and applications obey to, instead comming with there own ones.\n"
    author: "Carlo"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "> He also asks KDE to stop locking in users and applications to our framework\n\nI still struggle to suppress my initial thoughts about him (a la: \"What a moron! Will he be the next one of the bunch of Linux Foundation fulltime officers/bureaucrats to hire at Microsoft??\")"
    author: "scratching my head"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "Moron = american NIH gnomester."
    author: "reihal"
  - subject: "Re: Small clarification"
    date: 2007-07-09
    body: "It's sad, but yes, he seemed to be that way. Though it could be he was just uninformed (deliberately?)."
    author: "jospoortvliet"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "I've heard that kde programs can use gnome filedialogs in gnome... when will gnome programs be able to use kde filedialogs in kde?"
    author: "Chani"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "Where have you heard that?\n"
    author: "Lubos Lunak"
  - subject: "Re: Small clarification"
    date: 2007-07-06
    body: "I'd think the comment of the above poster was meant ironic, given that there're e.g. the Gtk-Qt engine and KGtk, while on the Gnome side there is nothing similar."
    author: "Carlo"
  - subject: "Re: Small clarification"
    date: 2007-07-09
    body: "indeed. KDE has always done more to integrate Gnome apps in KDE than Gnome has to integrate KDE apps. KDE apps run better on XFCE than on Gnome - so it's a matter of the Gnome's not willing to spend time on it."
    author: "jospoortvliet"
  - subject: "Re: Small clarification"
    date: 2007-07-03
    body: "ISVs want to develop for the \"Linux Desktop\", they don't want to develop for \"Gnome\", \"KDE\", \"Redhat\", \"Novell\", or any combination of those.\n\nWhat does that man for KDE? Well, take KWallet for example, it's a great feature. Gnome has gnome-keyring which does essentially the same. Yet this really nice feature is extremely difficult to use for an ISV because they are so tied to specific desktop environments.\n\nIn the same way, users, if they are unlucky, are faced with two key management solutions on their system that do the same yet are blissfully unware of each other."
    author: "Waldo Bastian"
  - subject: "Re: Small clarification"
    date: 2007-07-05
    body: "\"ISVs want to develop for the \"Linux Desktop\", they don't want to develop for \"Gnome\", \"KDE\", \"Redhat\", \"Novell\", or any combination of those.\"\n\nThen ditch one or more of them. Putting a whole layer on top of it is just a daft idea."
    author: "Segedunum"
  - subject: "Re: Small clarification"
    date: 2007-07-05
    body: "It is not about running a \"whole layer on top\" of something.\n\nThe idea is to have a mapper for interface that currently have differences but aim at having the same interface (probably different implementations) in the long run.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Small clarification"
    date: 2007-08-31
    body: "I just figured out where he got that. From Gnomes, of course, who actually ARE doing this. Try to use epiphany, and download a file to another location than the desktop. It'll open nautilus to show it - and no way to choose another filemanager (like thunar, or konqi). they lock their users in to their desktop, and of course expected us to do the same (which we don't)."
    author: "jospoortvliet"
  - subject: "Predictable release schedule"
    date: 2007-07-02
    body: "I think it would be quite cool if KDE started doing 6 months releases like GNOME and other projects do, but I would like to hear what disadvantages people see in doing that?"
    author: "Joergen Ramskov"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-02
    body: "It is no real release then. When you write applications for KDE3 you want to use them on the whole KDE3 plattform. The Gnome release scheme means gnome3 will never emerge and compatibility breaks happen between the series releases. It is also no real release but just the biannual update cycle where it is added what is ready. Permanent release means no real releases. "
    author: "Rock"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "Why isn't it a real release? I don't see why you can't still keep the compatibility? Yes, the next release will feature what is ready at that time, but AFAIK, there has been new KDE3 releases relatively frequent. Will it make much of a different to say that a new release will happen each 6 months? What exactly do you base your conclusion on that Gnome 3 will never happen? Are you basing it on the same \"fact\" that Linux 2.8 will never happen? Is that really a problem?\n\nI believe the release manager would have an easier job if the release schedule was predictable. No, that's not what should decide whether the KDE project should go with a predictable release schedule."
    author: "Joergen Ramskov"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "I believe that could have happened if there ever had been any contributions from Denmark, \nbut there never is so it won't happen."
    author: "reihal"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "More releases, means more attention. Simple Marketing-Rule."
    author: "Chaoswind"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "This is FLOSS! Screw marketing rules and make the best software there is :)"
    author: "Ben"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "Marketing is unfortunaly an important cornerstone of an successful product."
    author: "Chaoswind"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "Good Code is the cornerstone of a successful product. Marketing comes 8th, right after carbon paper. "
    author: "Ben"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "kde will be able to follow 6m cycle when that rich man hire some 50 developers full-time (he could hire devs from badly developed countries like russia, ukraine, ... to pay them less)"
    author: "nick"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-02
    body: "I am assuming that a six monthly A+B+C release enables a lot to talk about at once - and so generates interest for discussion sites/magazines and so will help expand the user base. \n\nHowever, if you start adding in D+E+F (pick from Linux, X, KOffice, Amarok, Digikam, F-Spot, Evolution, whatever) and suddenly the six monthly date will start slipping as it becomes more difficult to coordinate a release.  \n\nDistros tend to have six month releases and my impression is that sometimes it is seen as too soon \"Ku-hat-ian-use 10.1 wasn't much of an improvement over 10.0\".  \n\nAnd then there is the \"release-early-release-often + \"many-eyes-makes-all-bugs-shallow\" which would seem to dissipate resources if big packages are all released at the same time\n\nI have no idea which approach is superior - I suspect they are meeting different needs. Would coordinated PR be better than coordinated releases? \n   \nFWIW"
    author: "Gerry"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "> Distros tend to have six month releases and my impression is that sometimes it is seen as too soon \"Ku-hat-ian-use 10.1 wasn't much of an improvement over 10.0\".\n\nDid you just say Debian releases \"too soon\"? Debian has an 18 month release cycle and its allways late because of extensive bugtesting. That's not too soon."
    author: "Ben"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-08
    body: "He/she was probably talking Ubuntu, not Debian"
    author: "Leen Besselink"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "On the other hand, that means you only speak of linux about twice a years, while having releases at different dates means you have noise around linux all the time. For instance, for KOffice releases we have, so far, make sure that they don't happen the same week as a KDE release, that allows us to have more visibility, and it increase the amount of time the press is speaking about KDE projects. And honestly, I don't believe a KOffice + KDE release will increase the press coverage of either projects."
    author: "Cyrille Berger"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "My immediate reaction to Marks comment was that it would actually lead to LESS publicity.  Imagine everyone releases within 2 weeks of each other every 6 months.  That leaves 5 1/2 months when there's no major publicity and M$ gets a chance to appear more 'innovative'.  Then what happens when we do all release at once?  We compete with each other for the limited press attention available, and he with the shiniest bling will win, so projects will have less incentive to do the hard stuff under the hood.\n\nI've worked for organisations that had regular time-driven release cycles, and ones that had feature-driven release cycles.  I'd say the customers were divided on the benefits, some liked having the predictable schedule that they could work into their business cycle, while others hated having to wait several months (or even releases) for some new feature their business really needed.\n\nI guess we need to decide who our customers are, the distros, or the users?  The former would love 6 monthly releases, the latter I'm not too sure on.  While users might like regular releases as they get new stuff more frequently, they also like the big new features that take longer to engineer.\n\nHow about we guarantee regular 6 monthly bug fix releases (3.6.x) to coincide with his release schedule so he has the latest bug fixes in, but choose for ourselves when a point release with new features (3.x) goes out the door.\n\nI think people were very miffed too that Mark had the impression we don't deliver on our release schedules.  I wonder who gave him that impression? :-)  Someone needs to send him a list of all the KDE3 releases comparing the initially announced schedule dates and the actual dates achieved.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-09
    body: "I don't think mark really meant having all FOSS projects release at about the same date. If KDE would pick a 8 or 9 month schedule, he'd be happy (and yes, he did specifically say that). It's mostly about the predictability and the short time, not the alignment with other projects."
    author: "jospoortvliet"
  - subject: "Re: Predictable release schedule"
    date: 2007-08-14
    body: "Acctually I think the ideal situation would be to have all major FOSS packages have a set periodic release schedule which breaks into units no longer than 2 years.  More specifically it would be nice if every major FOSS had a release within 6 months prior to august (LinuxWorld) at least every other year, and that their release cycle was consistent or semi-consistentbv.\n\nFor example KDE could have a one year release period planning to have mayday as the release day or something like that.  "
    author: "Shea Kauffman"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-02
    body: "I think Mark wants to streamline Opensource releases.\n\nA new feature every day doesn't impress the crowd very much. But a big release with many new features every 6 months get much more attraction from outside.\n\nI think he's right in this point.\n\nUbuntus 6 Month normal release and I think it is 2 year LTS release cycle iss a very good choice, in fact when you think about how many people try new ubuntu releases everytime there is a new release.\n\nThe big Problem ist that it forces you to get new features ready to a certain release date. So sometimes you have to release half baked Software ... look at Micr*s*oft/A**le, they blame themselves often enough by missing sheduled release dates.\n\nI think a good choice might be, when there are cool new features, we might wait until there is some big release of distributions like opensuse, fedora  or kubuntu to extend the wave the release makes."
    author: "Bernhard Rode"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-02
    body: "6 months cycles sounds like a great idea. It shouldn't be to difficult for KDE to adopt after the 4.0 release, because the building blocks and apis are fixed for several years.\n\nIt would make sense to include X and/or the window manager in the cycle.\n\nLook at Eclipse. A joint release simplifies marketing and deployment."
    author: "Martin"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-02
    body: "P.S.: Qt might be a problem. Trolltech won't change their release schedules 'just' because of KDE. IMHO Linux isn't their cash cow but Windows. "
    author: "Martin"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "Is it neccesary to get Trolltech to change their release schedules? I don't see why?"
    author: "Joergen Ramskov"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "If KDE can't make significative changes in a 6 month period then is in dip shit.\n\n"
    author: "aam"
  - subject: "Re: Predictable release schedule"
    date: 2007-07-03
    body: "It don't need significative changes, only enough bugfixes and new features to blow up the changelog ;)"
    author: "Chaoswind"
  - subject: "0.25s?"
    date: 2007-07-02
    body: "I wonder if 0.25s for animation wouldn't be too slow, I remember that there used to be a \"rule\" that anything taking over 0.1s would be noticed by the user..\n\nPersonally, on Windows I disable the progressive appearance of Windows, because I feel that it's too slow."
    author: "renoX"
  - subject: "Re: 0.25s?"
    date: 2007-07-03
    body: "If you're right the animation has to take longer than 0.1s to be noticed. 0.25s are good then."
    author: "EMP3ROR"
  - subject: "Re: 0.25s?"
    date: 2007-07-04
    body: "From a user perspective - please think about the purpose of the animation.  If it is intended to draw attention to the fact that something has changed, then yes, a fast animation is good.  OTOH, the last thing I want to see is a 'busy' desktop, with unnecessary animations.  They can be too distracting.  Of course, we can always switch them off - but in doing that maybe we miss something useful.  That is a choice between living with something you hate or crippling the desktop.  It must be possible to achieve a balance, adequate notification with a calm, productive environment.\n\nAnne"
    author: "Anne Wilson"
  - subject: "Re: 0.25s?"
    date: 2007-07-06
    body: "> the last thing I want to see is a 'busy' desktop, with unnecessary animations. \n\nCouldn't agree more."
    author: "Carlo"
  - subject: "Re: 0.25s?"
    date: 2007-07-09
    body: "don't worry, then, from what I've seen most anim's out there in the KDE 4 field really aim to increase usability."
    author: "jospoortvliet"
  - subject: "Govaritye pa Russki"
    date: 2007-07-03
    body: "what he meant when he suggested to speak russian?\n\nwhy not chinese?\n\nmy humble guess is that there are more brilliant people among the russians :)"
    author: "nick"
  - subject: "Re: Govaritye pa Russki"
    date: 2007-07-05
    body: "Mark speaks Russian, he dosn't speak Chinease. That's the reason ;) "
    author: "Ben"
  - subject: "Re: Govaritye pa Russki"
    date: 2007-07-09
    body: "He meant linux/FOSS apps are translated in many languages, and many proprietary apps don't support such a wide range. It is one of our advantages."
    author: "jospoortvliet"
  - subject: "Coincidence?"
    date: 2007-07-05
    body: "\"Jim Zemlin, Executive Director of the Linux Foundation starts by mentioning the Linux Desktop Architects meeting at Google's headquarters. Nobody from KDE was there, so it was a rather limited meeting - something clearly went wrong in the communication area.\"\n\n\"He also asks KDE to stop locking in users and applications to our framework. Partly due to time, we didn't really interpret and understand what he meant by this statement.\""
    author: "Segedunum"
  - subject: "Re: Coincidence?"
    date: 2007-07-05
    body: "I'm not seeing a link between those two statements.  Can you expand on this?"
    author: "Anon"
  - subject: "Re: Coincidence?"
    date: 2007-07-09
    body: "Yep, there is clearly a problem. Jim apparently doesn't know 60% of the linux desktops runs KDE, and he should talk to and work with us if he really wants to further the Linux Desktop. I can imagine what people he mostly talks to..."
    author: "jospoortvliet"
  - subject: "Re: Coincidence?"
    date: 2007-08-31
    body: "About the lock-in thing:\n\nI just figured out where he got that. From Gnomes, of course, who actually ARE doing this. Try to use epiphany, and download a file to another location than the desktop. It'll open nautilus to show it - and no way to choose another filemanager (like thunar, or konqi). they lock their users in to their desktop, and of course expected us to do the same (which we don't)."
    author: "jospoortvliet"
---
<a href="http://conference2007.kde.org/">aKademy 2007</a> has kicked off! The first weekend hosted our user conference, which brought many talks about various topics, ranging from very technical to more practically oriented, which were spread over <a href="https://dot.kde.org/1183388210/">two tracks</a>. The tracks were interweaved with keynote talks. Read on for the report of the aKademy 2007 keynotes.








<!--break-->
<h3>Trolltech</h3>
<p>Saturday opened with Lars Knoll, talking about KDE from the perspective of a <a href="http://trolltech.com/">troll</a>. Trolltech employs over 50 full-time developers on Qt itself, accompanied by an assortment of testers and support personnel. Following the ideas behind 'extreme programming', Qt employs extensive code reviews and an incremental design. Focusing strongly on the API's while letting the developers work in loosely defined projects or even fully releasing them on 'creative friday' - it all fits very well with Open Source ideals and methods of development. This way of working is bearing fruit, and Qt 4.4 will bring us work on multimedia, hardware integration, with Trolltech even working with Apple on integrating Webkit as a new, distinct module in Qt. Research is going on in the areas of resolution-independent interfaces, multi-threading and extensive IPC.</p>

<br><a href="http://static.kdenews.org/dannya/akademy_trolltech.jpg"><img src="http://static.kdenews.org/dannya/akademy_trolltech_small.jpg" alt="" title=""></a><br>

<p>Yet, Trolltech also has another strong focus: the community. The Trolls realize they need to cooperate more, and thus are trying to pursue the common interests. By introducing developer blogs, releasing early snapshots and having a community manager, they hope to increase communication with the community and encourage contributions. Until now, KDE developers often worked around limitations in Qt, but in the future they could send patches.</p>

<p>This work is bearing fruit, with co-operation with Pango and OpenOffice.org developers on a common textlayout engine, contributed by Trolltech. Of course, Trolltech is higly committed to KDE, showing this by sponsoring developers, events and code. They want more feedback from, co-operation with and contributions to the community - and these can only bring good things to the Free Software ecosystem.</p>

<br>

<h3>13 Lessons for the Free Desktop</h3>
<p>Mark Shuttleworth of Canonical (and Ubuntu) fame gave a keynote about his vision for Free Software. He presented the Top 13 challenges the Free Desktop faces from his perspective.</p>

<ol>
  <li>Pretty is a <i>Feature</i></li>
  <li>Consistent <i>Packaging</i></li>
  <li>Simplified <i>Licencing</i></li>
  <li>Pervasive <i>Presence</i></li>
  <li>Pervasive <i>Support</i></li>
  <li>Govaritye pa Russki</li>
  <li>Great Gadgets</li>
  <li>Sensory <i>Immersion</i></li>
  <li>Real-time Collaboration</li>
  <li>More Organised Community</li>
  <li>The Extra Dimension</li>
  <li>Granny's New Camera</li>
  <li>Keeping it <i>Free</i></li>
</ol>

<p><b>Consistent <i>Packaging</i></b><br>
Talking about Packaging, he urged developers to re-think their procedures and priorities. There are many areas where we create different and incompatible systems, like RPM and DEB. Long ago, in a time of great flux and fundamental innovation, these differences where meaningful and useful. Nowadays, they are just barriers to broader adoption of the Free Desktop, and lead to a lot of duplication and useless work.</p>

<p><b>Great Gadgets</b><br>
Another challenge lies in the rise of new gadgets. A new generation of mobile phones, powerful enough for traditional desktop software are emerging. Interoperability with the latest digital cameras and multimedia devices is becoming more and more important, and Mark feels we should do more to bring the two spheres of Asian hardware engineers and European and American software developers on board. Asia is where the digital innovation is going on, and we should be there.</p>

<p><b>Sensory <i>Immersion</i></b><br>
Also interesting is the merger between the digital and the real world. They become more and more connected, and Free Software can play a role here. Sound, according to Mark, is crucial here. He gives an example of the bush: if you really want to experience one, you should stand in one, and close your eyes - the environment will 'talk to you'. Not the smartest move in the Darwinian sense, but definitely the way to a cool sensory experience. The sounds of everything trying to not be eaten, and eating other things can be overwhelming yet.</p>

<p>Other challenges Mark mentions are <i>real-time co-operation</i>, the 'extra dimension' brought by the latest 3D technology on the desktop, and finally, the challenge of keeping Free Software really <i>'Free'</i>. Mark says that he is highly committed to this freedom, both in the 'gratis' and 'libre' sense, and we should be, too.</p>

<br><a href="http://static.kdenews.org/dannya/akademy_shuttleworth_crowd.jpg"><img src="http://static.kdenews.org/dannya/akademy_shuttleworth_crowd_small.jpg" alt="" title=""></a><br>

<p>An interesting talk which became even more so when Mark suggested that KDE move to a more predictable (preferably 6 monthly) schedule. Mark mentioned that if KDE, GNOME and OpenOffice.org could agree to a common, regular release period, the rhythm and beat of publicity would be a frightening prospect for proprietary competition. This certainly prompted heated discussion, which is still going on. When KDE 4 is released later this year, who knows what the exciting future will bring.<p>

<br><a href="http://static.kdenews.org/dannya/akademy_aaronvsmark.jpg"><img src="http://static.kdenews.org/dannya/akademy_aaronvsmark_small.jpg" alt="" title=""></a><br>

<br>

<h3>Beautiful Features</h3>
<p>Aaron Seigo held an energising talk named 'Beautiful Features'. Though not officially a keynote talk, any members of the audience will agree that it had the impact of one. Starting by mentioning the negative (yet undeserved) reputation KDE has in the area of the so-called 'bloat' and associated bad usability, he pointed to the many unexperienced users working with KDE without issue.</p>

<p>However, having a basically usable, complete desktop isn't enough. First, you need to make a great <i>First Impression</i>. And the current KDE would have given a good impression 5 years ago, but not now. We need to start bringing good eyecandy to the desktop, while at the same time increasing its usability. A good example are toolbars. Having many toolbar buttons doesn't just look bad, it also makes using the application less efficient. Developers are used to complexity, but most users aren't - they are just not 'wired' that way. Using images and ideas from Joga, Aaron told us with what mindset we must develop software.</p>

<p>Showing a picture of Charles Darwin, he started to talk about the connection between real-life and what happens on a computer. Using subtle animation, a computer can feel a lot more natural. Qt 4.3 offers great features in this area, and we should make use of it. Effective use, of course. One second animations make the interface feel slow, whilst short, 0.25 second animations become functional. Aaron's grand vision of the future, a KDE out-innovating all competitive desktops, brought with a lot of humor, inspired many questions, and even led to some quotes we won't mention here.</p>

<br>

<h3>Desktop Linux - The Next Phase</h3>

<p>Jim Zemlin, Executive Director of the Linux Foundation starts by mentioning the Linux Desktop Architects meeting at Google's headquarters. Nobody from KDE was there, so it was a rather limited meeting - something clearly went wrong in the communication area. We ask him why KDE didn't get any invitations - after all, we represent the majority of the Linux Desktop and are working on its future. Jim told us there was a serious screwup, and apologizes - next time, they promise to get it right. We discover later on that there were a few invitations sent, but only to individual developers. Jim is offered the address of the KDE e.V., our legal organization, which will ensure the message will get through next time.</p>

<p>Then Jim starts to talk about our proprietary competition, and its strengths (mostly a bunch of lawyers) and its weaknesses (less innovation). What is our situation? Firstly, we grow faster than our competition. And we innovate. And we have lower costs. All this is due to the way Free Software works, both by increasing competition and co-operation. The Free Software market is much more dynamic, and thus it offers a much stronger eco-system.</p>

<p>Where does the Linux Foundation come in? Well, aside from paying the bills for some high-profile kernel hackers (for example Linus Torvalds), they are here to defend us. Our biggest competitor has a habit of trying nasty things to continue its monopoly and sustain the huge income it has (during the time you read this article, several millions are brought in to Redmond). It is spreading <a href="http://en.wikipedia.org/wiki/Fear,_uncertainty_and_doubt">fear, uncertainty and doubt</a>, trying to get proprietary, non-interoperable standards everywhere, and even denying our existence (even though Linux is a multi-billion dollar business). The Linux Foundation tries to co-ordinate our defence, both in the marketing and the legal area. There are a lot other organizations working on these things, like the Software Freedom Law Center and the Free Software Foundation. The Linux Foundation mostly tries to focus on the long-term, building a defence, for example lobbying for a reform of the patent law.</p>

<p>Jim proceeds to talk about the Linux desktop. He begins with the image it has, where we're at in that area. He points out that most talk is about what's wrong with Linux, what's missing, while we're already very good. Not perfect, of course, but what is? We should focus more on our strengths, communicate them. He told us how easy it was to impress some journalists from big American papers like the Wall Street Journal with a Ubuntu live CD.</p>

<p>He finally talks about things that the Linux Foundation has done, like the Linux Standards Base (LSB) or the co-operation on Text Layout (failing to mention it was Trolltech who donated the bulk of the initial code). He also asks KDE to stop locking in users and applications to our framework. Partly due to time, we didn't really interpret and understand what he meant by this statement.</p>

<h3>Conclusion</h3>
<p>The keynotes were fun, interesting and thought-provoking. Thinking is good, and most of us like 
it, so we are grateful to the speakers for joining aKademy 2007, and sharing their perspectives.</p>







