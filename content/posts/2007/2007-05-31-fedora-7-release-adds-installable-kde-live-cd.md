---
title: "Fedora 7 Release Adds Installable KDE Live CD"
date:    2007-05-31
authors:
  - "kkofler"
slug:    fedora-7-release-adds-installable-kde-live-cd
comments:
  - subject: "RH"
    date: 2007-05-31
    body: "Knowing RH it must be one heck of a unthankful job for Than Ngo. Kudos man, you're the one keeping RH even remotely usable for the most of us ( while your company continues to overlook it's customers, us ).\n\nRH really had it's way. If Linux desktop is not completely dead it's really well delayed for good 5-10 years from what it could have been."
    author: "foo"
  - subject: "Re: RH"
    date: 2007-06-01
    body: "wow, no grudges, huh.."
    author: "pascal"
  - subject: "Re: RH"
    date: 2007-06-02
    body: "I was under the impression that Redhat had done mostly good for the linux community.  Could you explain to me or point me to a link that shows what they've done wrong?"
    author: "Philip"
  - subject: "KDE is ugly \"by default\""
    date: 2007-05-31
    body: "First impressions are always important and it strikes me that KDE based distro's always look ugly by default (imho of course), where the default look-and-feel of gnome based distro's look reasonably OK. KDE can be made to look beautiful too, but most distro's use KDE's default. First Impression matters! (ask any girl :-). Compare the default look-and-feel of the latest Kubuntu with the latest Ubuntu and you see what I mean. It's the same with OpenSUSE and now with Fedora 7. I still think the current KDE has better functionality and speed than the current Gnome, but all large distro's (Fedora, Suse, Ubuntu) seem to favour Gnome as their default and I for one don't think licensing issues are the only reason. Many people prefer the look-and-feel of Gnome saying it's \"clean\" and \"slick\". I have to agree with them. The default KDE look is far too flashy. With lot's of adjustments the look can be made much better, but most people don't seem to bother. If I look at KDE based screenshot's I tend to notice the same thing. Most peoples KDE desktop's look \"ugly\"."
    author: "cossidhon"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "hum, ugly and beatifull is a matter of personality.\nI personaly don't like the look ok gnome (in ubuntu or mandriva) and can't feel it.\n\nAt the look level, I like kde quite good, even if not perfect.\nAnyway, both are far better in both area than CDE which I discovered this year on an HP-UX station ^^"
    author: "kollum"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "I concur, your excellency. It always takes me a good 15 minutes to make it look nice after a fresh install."
    author: "Tim"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-01
    body: "> your excellency\n\n:-D\n\nI must say, KDE really looked ugly (IMHO) in all Fedoras inc. 6, fonts were too big, widget style was Curve something which I can't bare to look at, I guess they've setup Gnome to their liking and then just tried to make KDE look something like it, not being their \"first choice\", they don't care, but the user should, so I have no problem with spending some time fixing it for myself."
    author: "Dado"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-03
    body: "> widget style was Curve something which I can't bare to look at, I guess \n> they've setup Gnome to their liking and then just tried to make KDE look \n> something like it\n\nIt's a good thing Fedora 7 doesn't do that then, eh?  :)\n"
    author: "Rex Dieter"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-03
    body: "The \"Curve something\" would be Bluecurve, and believe it or not, there are people who actually _like_ Bluecurve, exactly because it's a common theme for everything. I'm personally still using Bluecurve in F7. But it's not the default anymore, neither in KDE nor in GNOME, so it's pointless to complain about it."
    author: "Kevin Kofler"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "I think it is the other way around. I have yet to find a gnome screenshot (or livecd) which looks good. Gnomes icons are realy ugly, and the general color scemes on most installations I have seen make me think of dirt or mud. \nKDE on the other hand looks OK by default and takes just a few tweeks to look good IMHO. \n\nBut maybe it is just that taste is different and as a german saying goes: \"There is no argueing about taste!\"\n\n\n"
    author: "Turiya"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-01
    body: "No kidding!\n\nUbuntu's default theme makes me think of feces... >_>"
    author: "Amy Rose"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-01
    body: "that's exactly what I was going to say. :)"
    author: "Chani"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "I'm a foaming KDE fan, and I agree completely.  In fact, it's not just the default *look* that sucks - it's *most* of KDE's defaults, full stop.  I've never - ever - heard anyone praise KDE or its applications default settings, and it would be nice if the Usability Group would give a special focus on these sometime through the KDE4 series - heck, even a user-driven \"Krap Default Hunting Season\" would be hugely beneficial, IMO."
    author: "Anon"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "I am a long time KDE fan, but I must agree that GNOME looks better. Some of my friends use GNOME mainly because it looks better, even though KDE is a much better desktop."
    author: "anonymous"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "And that's EXACLY what i mean!!\n\nI'm on a official CLP10 (SuSE Linux Enterprise Linux System 10) certification course right now, and everything was Gnome based. I asked the trainer about the weird KDE/Gnome issue at SuSE and his reaction was Novell choose Gnome because of a number of reasons, but one was the consistent and sober business look. Another was the easy way to lock it down using the Zenworks tooling, which sounds a bit strange to me, belease KDE also has excellent support for this thru kiosk mode. Another disturbing remark was that allthough Novell support KDE \"equally\" to Gnome, the amount of work on KDE is at an absolute minimum."
    author: "cossidhon"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "> but one was the consistent and sober business look. \n\nthis was probably a footnote at best in the decision.\n\n> Another was the easy way to lock it down using the Zenworks tooling, which\n> sounds a bit strange to me, belease KDE also has excellent support for this\n> thru kiosk mode\n\ngiven that Zenworks support was added after the fact, i doubt this had much to do with the initial decisions.\n\nif you want more probable answers, look at the acquisition timelines and who was promoted to various VP roles within Novell."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "Ok, that's all true. KDE is ugly by default. And Keramik was nightmare ;]\n\nBut now we can help:\nhttp://dot.kde.org/1178743323/\nhttp://dot.kde.org/1179568832/\nhttp://dot.kde.org/1180273052/\n\nWith better HIG and better look (I strongly believe in plasma&oxygen!) KDE will be much better. So lets just check this HIG ;]  (I will check few apps from beta1 [@developers, not to late?])"
    author: "Dolphin-fanatic :)"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-01
    body: "It always seemed like an odd combination to me - buying Ximian and then buying SuSE - something was going to have to give and on the corporate front it was KDE. openSUSE though seems to be heading in a sensible direction (now, mostly) despite Novell"
    author: "Simon"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-03
    body: "I saw one of the Novell people at AFCEA in germany Bad Godesberg. He was presenting GNOME to all people. After a small conversation with him. I asked him similar questions and the answer he gave was the same programmed ones:\n\n- You can run KDE apps on GNOME\n\nWhich brought me to the question, why not run KDE apps on KDE. He continued with the same programmed crap that I've been hearing for years. At the end of the conversation he told me that he is on Linux for about one year now and that he is trying to reach the enterprise people. I then told him about all the technical flaws found inside GNOME and if he is able to sleep well at nights, whenever he sells that stuff to people.\n\nWell don't blame those people at Novell. Most of them didn't even know about Linux until 1-2 years now and they are just filled up with programmed phrases. They have no real own knowledge about the facts that float around for 9 - 10 years now."
    author: "Sambora"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "Agreed. Gnome is nowhere near the quality and technical excellence of KDE, but usually it manages to look better, specially by default. "
    author: "Derek R."
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "I know that opinions tend to diverge -- but you canot be serious about that.\n(well, imho) Gnome has \n- ugly colours: brown, grey, khaki, muddy green\n- ugly icons: just look at the cancel, ok button, the open folder icons or the folder icon in general: dull, dreary, boring. And with cartoon-like black borders around them\n- and ugly, clunky, angular widgets with huge offsets (the black lines) which makes it look like it was released between Windows 3.1 and Windows 95\n\nOnly recently I've seen screenshots of decent looking Gnome Desktop/apps, ones with new icons, rounder widgets and orange window decorations. Strangely, this seems to be a distro specific thing and hasn't been incorporated into mainline Gnome (?). I don't know what's up with that, as obviously, Gnome can be made to look better. \n\nThen again, on the KDE side, I was disappointed when I saw the latest (or the one before that) Mandriva LiveCD that they still have this old, clunky window decoration with strong bevels which they have shipped for years since like KDE 3.3?\n\nMaybe to conclude, post screenshots when you declare something good- or even better-looking."
    author: "Anonymous"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "How about this, my first attempt to \"mimick\" the latest ubuntu with stuff from kde-look."
    author: "cossidhon"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "I completely agree about the KDE defaults.\n\nThe Gnome defaults are much cleaner and simpler; having a nice default appearance is really important. I suppose this is why the Linux Mint desktop is getting so much love. I think a more classic default look (PCLINUXOS is not a bad step in the right direction, OSX very nice) would be a good idea.\n\nThe thing I like so much about the KDE desktop is the immense customizeability and I always fool around to get an icon/style/theme I like. It does seem, however that the desktop screenshots I find myself favoring on sites like TCS are GNOME based. ( Of course,I can make them look nearly identical (and nice) but these are not the defaults. )\n\nI completely agree that out-of-the-box KDE defaults should look more clean/elegant.  \n\nKudos, though, to the growing KDE incorporation into Fedora, a wonderful distro. Great job fellahs."
    author: "zachcp"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "That's exactly what i think.\n\nMy actual KDE looks better than any gnome, for me at least ;-) But, the default (the clock is specially ugly) theme, is... you know what i mean"
    author: "Me"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "oh my, this again. =)\n\n- we're working on the default look and feel for kde4.\n- kde looks very nice in many distributions that take the opportunity to make it look nice\n- i've heard many people comment both positively about kde's look and negatively about gnomes; and vice versa. it really helps if one steps outside the enthusiast community, as well.\n- kde is found in more operating systems than any other free software desktop. period, full stop. we also seem to have more market demand.\n\ndefault look has a lot less to do with decisions than does politics, human networking, marketing and project priorities. almost infinitely less.\n\nbut here, for me is, the important bits:\n\nRed Hat is really stepping up here with their support for KDE. that's awesome. it's going to take time to see the best they have to offer. if you think what they are doing could be better, get involved and communicate. but please, do so in a *positive* tone. better yet, help them by pitching in.\n\nKDE is a great position right now: we have terrific technology. kde4 is only raising that bar even higher. but with kde4 we also have a renewed, or perhaps just \"new\", aspect to the community of participants around kde that is very conscious of appearance and usability issues. so far, i don't see any \"selling out\" of the KDE ideals and traditions coming as a result of this, but i do see a prettier KDE.\n\nalso, give us time. don't judge purely on 4.0. the HIG stuff is like many technologies in kdelibs: it's brand new and will take a release or three to find its way into all applications. 4.1 will likely be a hell of a lot nicer than 4.0 will be. and 4.0 is shaping up to be pretty nice.\n\nfinally, when you do start seeing these positive changes, be sure to remember your constructive criticism such as the ones you've offered here. remember them because when things improve you owe it to those who have done the work to say \"thanks\" in some manner, to recognize what has been done. imho, that's the responsibility part that comes with engaging constructively.\n\nand of course, feel free to participate more directly by helping improve kde's interfaces and artwork. to everyone that is already doing that: you people rock my world.\n\noh, and one more time: GO RED HAT KDE TEAM! =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "Aaron, thanx for you're, as always, informative comments. I also welcome the renewed KDE presence in Fedora very much, so, indeed, kudos to the REDHAT KDE team!!\n\nAbout the default for KDE 4.0. I understand that KDE 4.0 is \"only\" the first release in the KDE4 series, but nevertheless, expectations are building up hefty, so the default look of the first in the series IS important. Let's make KDE4 rock, both in functionality and in look-and-feel and let's try to make a great first impression!\n\nThat said, do you know what is the best place to get involved in a non technical (as in development) manner for these kind of things in KDE4?"
    author: "cossidhon"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "> do you know what is the best place to get involved in a non technical (as in\n> development) manner for these kind of things in KDE4?\n\nartwork and documentation are two areas that need all the person power they can get. each have mailing lists that you can use to get yourself started with the community (kde-artists and kde-doc-english, both @kde.org) ..\n\ntesting and big triage are also useful ways to contribute; helping with the KDE HIG weeks is another way to get involved ..."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "You mean, like the clock in kicker with the numbers rendered by black bars like in old alarm clock or calculators... Indeed, that's bad and I wonder why it is the default."
    author: "oliv"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "due to font inconsistency issues across OS's. we're getting a bit more aggressive with these issues, but really each OS team should take 10 minutes to make the panel look good on their system."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-02
    body: "Why is there a clock as default at all? I always remove it since I have lots IRL clocks.\nTraditionally KDE have showed off most options just to advertize their existence.\nThis have been used by the dark side in their propaganda as being \"bloated\".\nNo need for that anymore, everybody knows KDE can do everything and the defaults can be set to minimums.\n No clock as default, instead of show clock."
    author: "reihal"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-02
    body: "Interesting, I removed the IRL clock, because the computer has one. \n\nThat's saving me money I would need for a radio-enabled clock that would go as accurate. Not to speak of batteries of electricity and environmental effects of producing the clock.\n\nI also removed the radion-enabled mini weather station that I had. It wasn't working as good as KWeather does.\n\nWhat I would rather do, is to give users obvious ways to discover different sensible presets for different user types. But I guess, that's for the distributions to do.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-02
    body: "Heh, I have a radio-controlled alarmclock on top of my monitor.\nIt was dirt cheap. \n\nIt's a matter of taste, but I do think that the defaults should\nbe kept at a minimum so the user can add stuff instead of removing."
    author: "reihal"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-03
    body: "I really do think the panel should keep its clock. I have never seen a panel on _any_ OS that does not have a clock.\n\nThat being said, the default clock is ugly. I like the bold sans-serif clock, or the anolog one."
    author: "Schalken"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-03
    body: "The problem with the clock is that it's default now it tries to mix to different concept of how a clock should look. \n\nThere are basicly two ways you can reperesent a digital clock. One is plain, having a text showing the time on the taskbar preferably with a nice looking font, like the clock in Windows XP. The other way is to have something stand out on the taskbar, for instance looking more like a physicall clock.\n\nThe kicker clock is originally of the second type, but along the way people have been trying to convert it(rather unsuccessfully) to the other kind. As you noted, making the LCD like numbers look very out of place. To make it look like it's supposed to enable 'LCD Look' in the config dialog. It now look like you have embedded a small LCD clock in your panel, and the numbers does not look out of place. If you like how it looks is of course depending on personal taste and another matter entirely, but the look at least makes sense.\n\nOne problem with this tho, the LCD look does not work with transparent kicker. Making it look real horrible, and I guess that's one of the reasons it's not default."
    author: "Morty"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-05-31
    body: "Folks, for those who hate the default KDE look&feel, Bluecurve is still available in Fedora. And if you hate that too, well, download your favorite theme from kde-look.org. And if you hate those too, well, then Fedora is not your problem. ;-)"
    author: "Kevin Kofler"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-01
    body: "http://tldp.org/HOWTO/Encourage-Women-Linux-HOWTO/x168.html#AEN171"
    author: "Amy Rose"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-01
    body: "Oops, I thought i was making a compliment..."
    author: "cossidhon"
  - subject: "Re: KDE is ugly \"by default\""
    date: 2007-06-03
    body: "GNOME was ugly before Tango came along. Now everything is going Tango and it looks nice and clean. However, Oxygen makes Tango look like a toy.\n\nA great example of a sexy KDE desktop is that of OpenSUSE 10.0 (Then just \"SUSE\", it remains the best SUSE release). The lizard in the background and that cute little blue \"home\" icon just seemed to balance perfectly. :)\n\nFrom there SUSE's KDE only went downhill as they moved their efforts to the GNOME desktop as per Novell's (read: Ximian's) priorities. Their \"Kickstart\" menu and Kerry Beagle implementation is horribly unpolished! However, they do have their \"sysinfo\" KIO slave which I love. :)"
    author: "Schalken"
  - subject: "Ah finally!"
    date: 2007-05-31
    body: "I remember, my first linux installation redhat9, where using kde was a nightmare with x server crashing and requiring frequent reinstalls. I tried fc1 but got bugged up with that too for not having proper kde support. I was using fc4 a few months back but now changed completely to kubuntu(6.10) due to its wonderful kde support.\nBTW i forgot to mention, kde caught me in first sight itself even though i had gnome installed (my taste of course ;) )\nAnyway, a nice news for kde  && fedora fans. :p"
    author: "gopala"
  - subject: "default KDE"
    date: 2007-06-01
    body: "I don't know what the big deal is, I think KDE is really fine in it's default form. I mean how hard is it to change a wallpaper or theme? if it is such a problem, then maybe adding somthing to the first start setup wizard of KDE that allows you to download themes,wallpapers, pointers and icons before you get to the desktop. then you will never have to see the \"default\" desktop. it will be your setup that you see.\n\nblah.....\n\n  "
    author: "madpuppy"
  - subject: "KDE look and feel"
    date: 2007-06-01
    body: "I think plastik is quite acceptable. \n\nI think Baghira looks pretty nice and available on the repos on (K)ubuntu. Maybe KDE default can be switched to Baghira?\n"
    author: "Anand Vaidya"
  - subject: "Re: KDE look and feel"
    date: 2007-06-01
    body: "Yeah. Let's make a shitty OSX-clone theme the KDE default.\nYou gotta be kidding..."
    author: "insultcomicgeek"
  - subject: "Re: KDE look and feel"
    date: 2007-06-01
    body: "I would prefer Lipstik to be the default."
    author: "A.M."
  - subject: "Re: KDE look and feel"
    date: 2007-06-01
    body: "I also love lipstik. The point really is that appearance is something where personal preferences vary hugely and the defaults are never going to make everyone happy. I actually came to KDE becuase to me it looked better than Gnome; I've stayed with KDE becuase the technology is better"
    author: "Simon"
  - subject: "Re: KDE look and feel"
    date: 2007-06-01
    body: "Maybe we could organize a election of some sort (kde-look would be the perfect place). The winner will be the default, nrs. 2 and 3 will also be installed by default, and the rest is there to download.\n\n- or -\n\nWe install all 3 and let the user decide thru a startup wizard.\n\nO, and it's not just style. It's the whole theme package (cursors, colors, deco's, etc)\n\nAnother suggestion is that KDE4 will not get released until the HIG and artists people are totally happy with it.\n\n"
    author: "cossidhon"
  - subject: "Re: KDE look and feel"
    date: 2007-06-02
    body: "> I think Baghira looks pretty nice\n\nFrankly, Baghira is a collection of rendering errors more than a widget style."
    author: "Peter"
  - subject: "F7 on Inspiron 6400"
    date: 2007-06-06
    body: "There are lots of bug in laptop,can`t install video drivers,always crush.\nI must wait for the advanced version,or upgrade."
    author: "mybays"
---
The <A HREF="http://fedoraproject.org">Fedora Project</A> has <A HREF="http://www.redhat.com/archives/fedora-announce-list/2007-May/msg00009.html">announced</A> the immediate availability of their latest release, Fedora 7 (Moonshine) including, for the first time, a KDE live CD/DVD showcasing KDE and 
KDE applications, which can also be installed to the hard disk, resulting in 
a regular Fedora installation with KDE.  Along 
with other current software, Fedora 7 includes <A 
HREF="http://dot.kde.org/1169738902/">KDE 3.5.6</A>. Unfortunately, <A 
HREF="http://dot.kde.org/1179831426/">KDE 3.5.7</A> was released too late to 
be included in Fedora 7, however it will be made available as an update. The 32-bit version fits on a 700 MB 
CD, the 64-bit version needs DVD or special overlong CD media for space 
reasons. KDE is also included on the traditional installer DVD. KDE in Fedora 7 defaults to the default KDE look and feel, with Plastik as the widget theme and CrystalSVG as the icon theme.








<!--break-->
<div style="border: thin solid grey; width: 350px; margin: 1ex; padding: 1ex; float: right;">
<img src="http://static.kdenews.org/jr/fedora-7-wee.png" width="350" height="258" alt="fedora 7 screenshot" /><br />
Fedora 7, more screenshots at <a href="http://www.thecodingstudio.com/opensource/linux/screenshots/?linux_distribution_sm=Fedora%207%20%20RC2">The Coding Studio</a>
</div>

<P>A lot of work has been 
done to improve the KDE experience in this release of Fedora. Thanks to the 
merger of Core and Extras, the core packages are now open to non-Red Hat 
contributors, and Rex Dieter of <A 
HREF="http://kde-redhat.sourceforge.net">KDE-RedHat</A> fame is now 
co-maintaining the KDE packages with Red Hat's Than Ngo, bringing the 
packaging improvements from the KDE-RedHat project into the official Fedora 
packages. Work has also been done on KDM, including <A 
HREF="https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=228111">integration</A> 
with <A 
HREF="http://library.gnome.org/gdm/2.18/consolekit.html.en">ConsoleKit</A> 
and a new default theme, <A 
HREF="http://bugzilla.redhat.com/bugzilla/attachment.cgi?id=154550">Fedora 
Flying High</A>, with support for face browsing (i.e. the user list with user 
images), which is now enabled by default. A <A 
HREF="http://fedoraproject.org/wiki/SIGs/KDE">KDE Special Interest Group</A> 
has been created to work on KDE and KDE applications in Fedora and maintains a 
list of KDE-oriented packages currently considered for inclusion into Fedora. 
For the next release, a <A 
HREF="http://fedoraproject.org/wiki/KevinKofler/F8KDE4Plan">plan</A> to get 
KDE 4 integrated is currently under discussion.</P>

<P>You may have noticed that there is no "Core" in the name this time. This is 
not an omission. Starting from this release, Fedora is no longer split into 
separate Core and Extras repositories. Instead, there is a single collection 
of packages, the Fedora Collection, from which different "spins" are 
produced, both traditional installer-based spins and live CD/DVD spins. The 
following spins will be of interest to KDE users:</p>

<UL><LI>Fedora: the default installer-based spin, offering a package set 
comparable to the traditional Fedora Core.</LI>
<LI>KDE Live: a live CD/DVD spin based on KDE, using KDM as the display 
manager and KDE applications such as Konqueror, KMail and KOffice as the 
defaults. Alternatives such as OpenOffice.org or Firefox and other additional 
software available from the Fedora Collection can be added after installation 
to the hard disk.</LI>
<LI>Everything: not a real spin, but a Yum repository (also usable with 
APT-RPM or Smart) containing the entire Fedora Collection.</LI></UL>

<p>Note that only the installer-based spin can be used to upgrade from a previous version of Fedora (without reinstalling). Live CDs or DVDs can only be used for fresh installs (or reinstalls). Upgrades through Yum, APT-RPM or Smart are possible, but strongly recommended against.</P>

<p>Download the ISO through <a href="http://torrent.fedoraproject.org/">bittorrent</a> or the <a href="http://mirrors.fedoraproject.org/publiclist/Fedora/7/">download mirrors</a>.</p>

<p>Rex will be <a href="http://akademy2007.kde.org/conference/talks/07.php">talking about Fedora 7 and KDE</a> at <a href="http://akademy2007.kde.org/">aKademy</a> next month.</p>






