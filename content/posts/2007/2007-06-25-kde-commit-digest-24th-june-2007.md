---
title: "KDE Commit-Digest for 24th June 2007"
date:    2007-06-25
authors:
  - "dallen"
slug:    kde-commit-digest-24th-june-2007
comments:
  - subject: "Wow."
    date: 2007-06-25
    body: "Last week, when it was announced that KDict was going away, I mentioned that it may be cool to make it into a Plasmoid.  Little did I know that somebody with some coding skill (and new to KDE!) had the same idea, and within days that widget was born.  Awesome.  Good job, guys."
    author: "Louis"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "I had almost the exact same thought.  Seems kdict funtionality will be there with this plasmoid.  I must say that with each passing week I am starting to get more and more excited about KDE4 and Plasma (as well as Solid, Phonon, etc.)  I'm not a programmer, but it seems that it will be fairly simple for Plasmoids to be written.  I can't wait to see how it will be extenteded to applications and to the desktop as a whole.  If what I understand about the zooming capability is correct, then I envision objects (i.e. file cabinet) that when clicked will zoom out (or is it in?) to open a whole new desktop.  If that is the case, this can be very interesting indeed."
    author: "cirehawk"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "I had the same feeling after I read that digest and the comments so I decided it would be perfect for me to start contributing to KDE with.  The sad thing is, I won't be able to work on it for 4 weeks starting on Wednesday since I will be going on vacation, but I believe other Plasma hackers will continue adding small improvements.  But I will have a large amount of free time the end of July and all of August to continue working on it.\n\nP.S. Please post any suggestions/improvements you have for the dictionary plasmoid."
    author: "Thomas Georgiou"
  - subject: "Suggestions & Improvements"
    date: 2007-06-25
    body: "1. Unicode support (for bilingual dictionaries)\n2. Custom Dictionary list (edit database set as in kdict) for lookup in specific (user preferred) dictionaries.\n3. lookup clipboard/selection ablility (kdict -c will do the same) to look-up the \"selected\" word anywhere. (with a shortcut or a button/quicklauncher)"
    author: "fast_rizwaan"
  - subject: "Re: Suggestions & Improvements"
    date: 2007-06-26
    body: "what, it's not getting magical happy unicode support from qt already?\n\nif someone doesn't implement this list of stuff I'll add it to my list of things I wish I'd get done someday. ;) although there is KTranslator for looking up selected words in kde3 :)"
    author: "Chani"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "not a suggestion for the dictionary pone, but if you could make a translation plasmaoid, that would be great! :-) (just for single words, that would be enough)"
    author: "Beat Wolf"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "Very nice work, Thomas!\n\nMy suggestion: use a different icon. I never liked the lenses/binoculars as a methaphor for search. Why not simply use a dictionary icon? That should be easy to draw (maybe ask the oxygen guys to make one if they haven't already) and much more evocative!"
    author: "Benoit Jacob"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "It's just a visual suggestion : could you round the corners of the dictionnary area :)\n\nThanks for your great work !\n\n"
    author: "Marc"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "Do you mean round the corners of the actual lineedit?"
    author: "Thomas Georgiou"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "I think he means the results window.  I, too, thought that it would look better if the inner window had rounded corners to match the outer decoration.  Is it even possible?"
    author: "Louis"
  - subject: "One MORE request"
    date: 2007-06-25
    body: "4. Double clicking on a word inside the plasmoid should define the word as if we typed it.. Please :)"
    author: "fast_rizwaan"
  - subject: "Re: One MORE request"
    date: 2007-06-25
    body: "No double clicks in KDE please! Its especially not good for older users.\n\nBye\n\n  Thorsten\n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: One MORE request"
    date: 2007-06-25
    body: "Let\u0092s just agree that it should respect the clicking preference of the user.  I like my click for select, double click for action.  You like your way."
    author: "Henrik Pauli"
  - subject: "Re: One MORE request"
    date: 2007-06-26
    body: "Indeed. I've tried to get used to singleclick, forcing myself to use it for weeks. But really, I can't live with it. I even use triple-click very often... Singleclick just means you lose a lot of control, and I don't like that."
    author: "superstoned"
  - subject: "Re: One MORE request"
    date: 2007-06-27
    body: "if this is implemented, it should use single click because it is effectively acting like a web link.  All web links are single click."
    author: "Vlad Blanton"
  - subject: "Re: One MORE request"
    date: 2007-06-25
    body: "You mean to have all words behave like links do now?"
    author: "Thomas Georgiou"
  - subject: "Re: One MORE request"
    date: 2007-06-25
    body: "Please don't do this.  One of the big dictionary sites does this, and it drives me up the wall, because it makes it impossible to select a word by double clicking on it, which it is the default in every other text display.  Redefining the double click to mean \"look up word\" instead of \"select word\" without any user notification is really annoying.  Just as annoying as the old KDE wiki that liked to go into edit more on double clicks.  "
    author: "Leo S"
  - subject: "Re: One MORE request"
    date: 2007-06-25
    body: "I will still leave the related words provided by dict.org as blue links."
    author: "Thomas Georgiou"
  - subject: "Re: One MORE request"
    date: 2007-06-25
    body: "Yes, for sure.  I think the grandparent was suggesting that all words can be looked up in the dictionary by double clicking on them, not just links."
    author: "Leo S"
  - subject: "Re: One MORE request"
    date: 2007-06-25
    body: "Yes, for sure.  I think the grandparent was suggesting that all words can be looked up in the dictionary by double clicking on them, not just links.  "
    author: "Leo S"
  - subject: "Re: One MORE request"
    date: 2007-06-25
    body: "I'll just ask the usability guys which is better."
    author: "Thomas Georgiou"
  - subject: "Re: One MORE request"
    date: 2007-06-26
    body: "smart choice ;-)\n\nThey might have ideas. Like a mouse-over effect showing a small 'lookup word' button..."
    author: "superstoned"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "Great work, it looks very useful. To code plasmoids seems indeed to be a very good way to start contributing to KDE, I think I'll see if I can make something after my vacation (heh, also 4 weeks, starting om Thursday).\n\nSome suggestions:\n\n- A clear button\n- Agree with Benoit Jacob, another icon; I also suggest a dictionary\n- There are some visual things (to rounded corners in my opinion, for example), but I think it's too early to complain about. "
    author: "Lans"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "Edit: Oh yes, please don't show the about info if the user type \"about\" - imagine the confusion if you actually look for the work. ;)\n(I would suggest something like kdict:about or something similar)"
    author: "Lans"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "Right now we are thinking of showing the current about with the normal definition or only showing that about with /about."
    author: "Thomas Georgiou"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "I think something like this would be great\nfr:bonjour\nfr:en bonjour \n\nSo you can very easy and fast choose a language.\nFor this it needs different languages and translation of course.\nAnd so you don't have to type it always, make a default behaviour."
    author: "Thomas Coopman"
  - subject: "Re: Wow."
    date: 2007-06-26
    body: "This idea is awesome!"
    author: "Martin Stubenschrott"
  - subject: "Re: Wow."
    date: 2007-06-26
    body: "yes, yes it is."
    author: "Chani"
  - subject: "Re: Wow."
    date: 2007-06-25
    body: "This is a nice dictionary applet for mac os: http://en.wikipedia.org/wiki/Image:Dictionarywidget.png\n\nI think it may be inspiring for you."
    author: "Fhucho"
  - subject: "applet tutorial?"
    date: 2007-06-25
    body: "It would be cool to have a plasmoid tutorial on how to write one.\nI searched in techbase and plasma.kde.org but couldn't find anything.\n From what I know, it's only possible to write them in C++ and Javascript, right? I'm waiting for the ruby biding to come cause that's my preferred language though I wouldn't mind trying in C++.\n\nFor now, the best location to look for seems to be http://websvn.kde.org/trunk/KDE/kdebase/workspace/plasma/applets/ right?\n\nBy the way, where can I find the code for the dictionary, facebook and weather applets?\n\nthanx a lot\n\n\n"
    author: "Patcito"
  - subject: "Re: applet tutorial?"
    date: 2007-06-25
    body: "found the facebook one:\n\nhttp://websvn.kde.org/trunk/playground/pim/kfacebook/plasma/applet/\n"
    author: "Patcito"
  - subject: "Re: applet tutorial?"
    date: 2007-06-25
    body: "The dictionary appears to be here:\nhttp://websvn.kde.org/trunk/playground/base/plasma/applets/dict/"
    author: "Oscar"
  - subject: "Re: applet tutorial?"
    date: 2007-06-26
    body: "tutorials will be appearing at some point after akademy unless someone else beats me to it ... the api just isn't ready for such documentation imo."
    author: "Aaron Seigo"
  - subject: "Konqueror development activities"
    date: 2007-06-25
    body: "Maybe it didn't look through the last commit-digest issues thoroughly but I didn't spot any commits for Konqueror in a long time. Did I miss something?"
    author: "DITC"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "I asked about this from Danny.He said that Konqueror is just a shell around KDE Technologies.It hasnt much codebase.\nBut (I think) you will see much difference between Konqueror 3 and 4, Because those Technologies are changed a lot in KDE4, like the new KListView."
    author: "Emil Sedgh"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "This is for the file management part, right? And what's happening with the browser part?"
    author: "DITC"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "WebKit part, KHTML Improvements, Userscripts..."
    author: "Emil Sedgh"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "What DITC and I want know is, what is happening to the \n\"Konqueror is just a shell around KDE Technologies\" part.\n\nIf you put the webkit kpart i Dolphin you'll have a Dolphinor,\n in Kate a Kateror etc.\nBut what about the specific web browser shell Konqueror? "
    author: "reihal"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "You are totally right. I haven't read in months about any major development in the web browser part of Konqueror. Is the browser field left to Firefox?"
    author: "DITC"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "Frankly, I suspect that starting a dedicated, khtml/ webkit-based KDE browser would be more beneficial than working on Konqueror - the burdens of being a general-purpose KPart viewer optimised for Web Browsing *and* File Management simply make it really hard to work on and improve.  I remember dfaure on IRC discussing how to implement Undo Close Tab - an oft-requested feature that should be almost trivial, really - and how the need to take file management into account complicated things excessively.\n\nA \"Dolphin for the Web\" would be the best move, imho - leave Konqueror to the power users."
    author: "Anon"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "Are you mad? telling these things to reihal? his kde-ultra-\u00fcber-power-user ego could suffer a heart-attack if ther're going to kill Konqueror as the default web browser as they did with file-management ;D"
    author: "Vide"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "I'm working on the assumption that pushing Konqueror further from the mainstream will increase his e-penis bragging rights ;)"
    author: "Anon"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "I got it all out.\nThe Konqueror debate, I mean."
    author: "reihal"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "Yeah, I'm dead now. Hope you're satisfied."
    author: "reihal"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "Whats wrong with Konqueror as Web Browser?\nKonqueror is the most powerfull KApplication, its the flag of KDE, its one of the tools that do not exist on other platforms.Its what makes KDE so powerfull...so shiny and so so unique.\nand yep, its a shell over KIO/Slaves, Kparts and other Technologies.if they improve, Konqueror improves.\nKonqueror is improving while KDE is getting improved.its what you should understand...\nit even has Plugins structure, if you are missing Firefox Extensions, that because people are not creating plugins for Konqueror, but itself, supports it.\nsorry for bad English."
    author: "Emil Sedgh"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "\"Whats wrong with Konqueror as Web Browser?\"\n\nWhat's wrong with it is that it is also a full-featured file manager.  Why can I not set a separate home URL for its file manager and web browser mode? If I'm using it as a file manager, pressing \"Home\" should take me to ~, not Google, and vice versa.\n\nIf I'm using it as a web browser, why would mistyping a URL cause a scan of my harddrive for what I typed using kio_locate, instead of searching google? Why do I see tons of menu items that are useful *only* in a file manager? Why, when I right click on an http URL, am I given the option to Commit to a SVN Repository?\n\n\"Konqueror is the most powerfull KApplication, its the flag of KDE, its one of the tools that do not exist on other platforms.Its what makes KDE so powerfull...so shiny and so so unique.\nand yep, its a shell over KIO/Slaves, Kparts and other Technologies.if they improve, Konqueror improves.\"\n\nYes, I know all this - Konqueror is a great app for Power Users.  As a web browser, however, it is very, very confusing and incredibly slow to progress.  Whay is there no Undo Close Tab? Why no \"Protect Tab\"? The list of missing features one would expect to see in a modern browser shell goes on and on, and a large part of the lack of progress is due to the fact that spanning the worlds of file manager and web browser all at once is immensely difficult.\n\n\"Konqueror is improving while KDE is getting improved.its what you should understand...\"\n\nI understand completely - please stop talking to me as though I am an idiot who knows nothing of KDE :) \n\nThe KParts that it wraps are improving.  The Konqueror shell itself remains a mess of lacking features and features that are wholly appropriate when I'm using an instance solely as a web browser or as as file manager.\n\n\"it even has Plugins structure, if you are missing Firefox Extensions, that because people are not creating plugins for Konqueror, but itself, supports it.\"\n\nAnd why are they not? Two reasons, that I can think of:\n\n- Extensions must be native code - there's no easy way to get them unless you compile from source or they are pre-packaged.  Compare with Firefox where you can install one from a website at the click of a button.\n\n- Nowhere near as many hooks as Firefox.  If I want to intercept the html as it is downloaded and tweak the DOM tree before it is displayed, how can I do this? How can I change the tab navigation behaviour with a plugin? There are no hooks into the system!\n\nThe \"API\" that Konqueror offers to the would-be extension writer is simply too primitive to get anything interesting done.\n\n\"sorry for bad English.\"\n\nYour English is perfectably understandable :)"
    author: "Anon"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "Alhough I used to love Konqueror, I have to say that everything should be driven again to the Unix philosophy: one tool for one job.\nIt would be wonderful if someone step in to create a Konqueror NG, built from ground up to be *only* a web browser (with the old one left for the nostalgics).\nI know I could sound a little harsh, and I owe all the respect they deserve to the original Konqui's developers, but we are in a tunnel now, IMO. \nDolphin was the Right Decision (TM), a Konqueror NG (don't want to lose the brand) would be another.\n\nBut I'm just a comment in the Dot."
    author: "Vide"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "I'm not against a dedicated KDE browser in the vein of Dolphin but suggesting to wholy replace Konqueror is just daft. Konqueror is a great tool for people like me who like to have all documents related to a project (FTP shares, PDF files, websites, etc) open in one window as it seriously lessens the clutter on the taskbar and it's easier for me to scan the tab bar than to search n the taskbar for the window I'm looking for. Plus, I can arrange tabs in an order which suits my workflow.\n\nI'd like to see that Konqueror is rewritten as a multi-purpose KParts viewer which dynamically changes it's interface depending which KPart is viewed in a tab."
    author: "Erunno"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "so... you want konqueror rewritten to be like konqueror? why not keep the old one?"
    author: "ac"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "No, he'd like it rewritten to be like it is now *except that the interface changes depending on what's being viewed*.  You know, that whole thing about how filebrowsing menu entries are shown even when you are browsing the web and vice-versa.\n\nSort of like how in Windows Explorer if you enter a web URL hey presto suddenly it's \"internet explorer\" with different menus and such, even though it's the same program.\n\nI don't mind having Konqueror access files and the web, but I'd like it to look a bit different with different bookmarks and menu entries depending on which it is accessing."
    author: "MamiyaOtaru"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "\"Sort of like how in Windows Explorer if you enter a web URL hey presto suddenly it's \"internet explorer\" with different menus and such, even though it's the same program.\"\n \nWindows explorer is explorer.exe, and IE is iexplore.exe.  Not the same program."
    author: "Anon"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "Technically iexplore.exe is just a wrapper for the IE ActiveX control provided by shdocvw.dll (at least it was up to IE6, I assume the same is true for IE7).\n\nInterestingly I notice that with IE7, at least on my work system if you type a web address into Explorer it no longer mutates into IE as it did with IE6 (and the same applies if you type a local path into IE's address bar)."
    author: "Paul Eggleton"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-27
    body: "Oops, I forgot to add - instead of mutating the current window, it opens a new one."
    author: "Paul Eggleton"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "It almost sounds like you've never used konqi... It does change it's interface. menu's and toolbars are differend, depending on what kparts are loaded... It has been this way since what, forever?"
    author: "superstoned"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "Yes, I know it does. I would just like to see this support extended beyond what is implemented today. For instance, the configuration dialogue is pretty much geared toward web browsing / file management. How about redesigning it and making it possible to configure each KPart individually (pluggable configuration dialogues?). For instance, if I load a PDF file in a tab I can configure the behaviour via \"Configure KPDF\" but wouldn't it seem more natural to have it in one place?"
    author: "Erunno"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-27
    body: "Well, for a start, I disagree with having different bookmarks.\nYou see, to me one konqueror window is the same as another. When I need to access to the local filesystem, or to an ftp site, I just open a new tab...\nif the bookmarks were \"magically\" changed, depending on the \"mode\" (how would you decide the mode?) it would lose functionality for me."
    author: "Luciano"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "But Konqueror *works* according to the unix philosophy, only you look at it from the wrong angle: it's not a simple program, it's the shell.\n\nIn the *nix shell, there are small programs that do one thing well... these are KParts in KDE. And Konqueror let us combine those to make a more powerful application, just like the shell let us combine ls with more to paginate through a long directory listing.\n "
    author: "Luciano"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "I think this is pure sophism :) I mean, in the unix shell you don't have ls, awk, grep etc always in your face. In fact, since discoverability is completely different in a text-based interface, if you don't know about grep, grep almost doesn't exist for you. On the other hand, if in the View menu there is a \"Show as hexadecimal\" (or what's the description of the KHex Kpart) while browsing a web page, IT IS in your face. Or if I go in the Preferences panel while browsing a web page and I find at first sight options about file management IT IS in your face, and IT IS *highly* unusable, irritating and discouraging, even for power users.\n\nI would for sure welcome a more powerful profile management, but I think that the better choice is the radical one, as they already did with Dolphin."
    author: "Vide"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "Konqi already only shows what is usefull right now. OK, it might need some cleanup here and there, but imho the principle is great."
    author: "superstoned"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "\"On the other hand, if in the View menu there is a \"Show as hexadecimal\" (or what's the description of the KHex Kpart) while browsing a web page, IT IS in your face.\"\n\nThis happens because KHexEdit is associated with the mime-type that you're viewing (like for me, when I go to View -> View Mode -> I only have \"KHTML\" and \"Embedded Advanced Text Editor\" for this page).  The View menu point is invalid (since apparently on your system its associated with that filetype), though your Settings point is valid, Konqi's config dialog can appear daunting because of all the options for Web and File at the same time (I think maybe a tree view would be better, or that other widget... toolbox I think it was called? and then have one section for File Management, one for Web Browsing, and maybe a misc section as well for options that aren't really related to file/web exactly).\n\nKonqi does need a good bit of loving to clean up and restructure the code, or possibly even a complete rewrite depending on how much a mess the code is (one day I plan on attempting a rewrite)."
    author: "Sutoka"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "No, my point about the View menu is still valid, because my complaint is that Konqueror should act ONLY as a webbrowser, and not as a KPart swiss-knife that shows me every possibile kpart associated with the file I'm viewing.\nI know that sounds drastic, and in fact it could even be worth trying to spawn a new project, just as Dolphin did, but I recognize that it would loose brand recognition. But in this way Konqueror could still live in its current form (for the power-users) and the new program would be the default for web browsing in KDE. "
    author: "Vide"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-29
    body: "Other browsers (AFAIK Firefox, Opera, IE) have the feature of integrating other apps, e. g. Acrobat Reader. This is very simple in Konqueror, as if an embedded viewer is associated with pdf, it will open in Konqueror, using some kpart . And the possibility of changing the kpart is important: for example, I use kpdfpart by default (because it is fast and it is kde) and if a pdf does not display well in kpdf, I simply switch to nspluginwrapper which shows Adobe Reader inside Konqueror."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-27
    body: "Well, this means Konqueror is more discoverable than the shell. I can't see this as bad. How else would you invoke a new command in a \"graphical shell\", if not through toolbuttons or menu entries?\n\nAt the moment there is a way to control which extensions should be enabled. Probably it would be useful to offer the user a way to disable certain parts (or conversely, to enable rarely needed parts).\n\nYes, the CVS view is probably not needed by everybody, as the hex view.\n\nSo, it could be useful to have the parts enabled only if the user requires them. In the same way, it could be useful to enable only the simper file-management views, and let the user decide if they want more advanced views, or if they prefer different views altogether.\n\n"
    author: "Luciano"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-30
    body: "Exactly, this should be the principle for all of KDE:\n\nMinimum defaults; the user adds stuff.\n\nNot the other way around."
    author: "reihal"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-30
    body: "\"IT IS *highly* unusable, irritating and discouraging, even for power users.\"\n\nAfter you have first found a feature, you know where it is and you do not have to bother with the features you do not use at the moment. I think beginners use this \"method\" frequently, and mostly with success. (Except that sometimes they do not bother with the important features also. :) )"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-30
    body: "I agree with that person who wrote that Konqueror works accordingly to the Unix philosophy. One task is viewing a certain file type, or downloading (and in some cases also uploading) using a certain protocol. A typical \"web browser\" integrates the two almost unrelated task of downloading a file using http and displaying a html file. And what if I want to display a html file which is located on an sftp server?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-25
    body: "Hi\nMost of your problems are those can be fixed by extending and working on Konqueror's Profile tool.Yes I know that it currently doesnt work as excepted, but I think its easier to extend (or even rewrite) them than creating a whole new Browser.\n\n\nI mean improving Konquerors profiles and Plugins Structure is easier than creating whole new browser + new Extension API.\n\nand please...Konqueror is not just for power users.for example, think about when a normal user tries to open a picture.which is easier for him, a new application opens up, with new everything, or Konqueror just changes its menu items to it??\nI tried it, please! you should try it too! I tested it with my grandma ;)\n\nI cant understand whats the matter of konqueror with idiots ;)"
    author: "Emil Sedgh"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "It isn't likely to happen, but I'd love to see a branch of Firefox with KHTML/Unity as the renderer.\n\n1 - It would be true Web Unity, bringing together Firefox (and quite possibly Camino, Flock and all the Mozilla-based browsers), Safari, Konqueror together under one rendering engine.  Fast, compliant, standard rendering across every platform.\n\n2 - People keep asking for Firefox plugin support into Konqueror.  Either you rewrite a branch of Konqueror to be a dedicated web-browser that supports Firefox plugins, or you port KHTML into Firefox.  Given the two choices, I think injected QT and KHTML into Firefox is the one that is most likely to gain mass exposure and acceptance, especially when you'll be able to use the QT/KHTML branch on all major platforms.\n\n3 - If Trolltech and KDE want to show the entire world what they've got going with QT 4.x and the KDE 4 libraries, then grab a hold of one of the biggest OSS programs on the planet and introduce yourself to some new users, and thusly some new developers as well.  I know tons of programmers.  I've worked for several large IT departments, and even those that know Linux know nothing of QT.  That is sad indeed.\n\n4 - People are not only already familiar with Firefox, but you save yourself all the time, effort and heart-ache of designing the look, interface, etc. of the browser.  Invariably, all those decisions will upset people anyway, and the time investment in planning such a huge project is not to be underestimated.  So instead of making a new browser, just beef up the one that so many use and love.  As far as I know, there is already a long-abandoned QT branch in the Mozilla repositories that is just begging for some QT 4 love."
    author: "T. J. Brumfield"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "1. Yeah, it'd be lovely if firefox would adapt KHTML, and indeed KHTML is the better technology, but being better never brought KDE technology much good.\n\n2. Well, you could rewrite firefox' interface into Qt, I guess, but then you'd have the bad interface firefox has (fitting in nowhere with it's own look & feel, a typical windows app). Why would you want that, you can't use the name firefox anyway (see how debian has to use 'Iceweasel' just because they maintain their OWN security patches...)\n\n3. - firefox is controlled by the company/whatever behind it, you can't use it's name for anything else, nor influence the project much. Forget any KDE/Qt influence in Firefox: they focus on windows users mostly, and IF they look at the linux platform, it's mostly Gnome/GTK centric.\n\n4. Firefox might be loved, but that's not because it's any good. It has a mediocre interface (OK, it's main interface is OK, but the rest sucks - configuration, extensions behave weird, etc). It's not fast either (konqi has always been faster, and IE is also faster!). Firefox is only loved because ppl know it, it has a pretty widespread use and as such receives a lot of promotion and plugins. And we can't use these plugins (they aren't even usable between firefox versions, so forget about different apps, I guess they don't have a real plugin structure but just expose their internals like compiz and beryl do).\n\nI think having a seperate webbrowser for KDE wouldn't be bad, but I would love to keep konqi. I don't see myself use dolphin, as it can't do webbrowsing, and I always mix local and webbrowsing and filepreview (PDF) etc in a window. Any filemanager/webbrowser which can't do that sucks for me."
    author: "superstoned"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "1 - I wish honestly I had thought to suggest this a good year back.  From what I understand, the majority of the work on Firefox 3.0 has been developing a new/updated rendering engine, and all the while it has been getting slower and slower.  If their concern was putting in a better renderer after Firefox 2.0, they might have been much more open minded a year ago.\n\n2 - If I had to guess, I'd say Apache is the most widely used OSS program out there, but to the average user, Firefox is one of the biggest OSS programs on the planet.  They use it every day, and it keeps growing in popularity.  The interface can't be that horrible.  My wife installed Kubuntu just tonight, and tried Konqueror for about two hours before screaming to have Firefox back.\n\n3 - Mozilla controls Firefox, and they are pretty specific.  Clearly label your build \"Unofficial\" or don't use the name Firefox, and that is fine.  The Mozilla trunk has a branch for OSX widgets called Camino.  I'm not suggesting that we demand that Mozilla switch wholly to KHTML tomorrow, even though I think it would be a good idea.  I'm suggesting that some talented QT/KDE devs consider adding a KHTML based brank to the huge Mozilla trunk.  Do you know have many branches/splits they have?  Again, they already have a QT branch, it just needs (sorely) to be updated.\n\n4 - As far as speed, everyone says how incredible QT 4.x and the new KDELIBS are, so put them to the test.  Plug in this faster rendering engine, and fast widgets, and voila, you should get a faster Firefox.  I'm sure that will turn heads and bring attention to the masses what wonderful technology QT and KDE is.  That's somewhat my main point.\n\nAnd frankly, the massive collection of add-ons is a huge selling point.  I just use nightly-testing-tools, force compatibility, and all my plugins magically work, even though I use nightly tester builds of 3.0\n\nAnd KDE often has multiple apps for the same job.  I doubt Konqi will ever disappear, but it does have easily spotted flaws that come from trying to be all things to all people."
    author: "T. J. Brumfield"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "2. She's just used to firefox. I use it now and then, mostly at my job - IE 4/5/6 suck because they don't have tabbed browsing, but I have no problem with IE 7. Imho firefox isn't much better, at least for what I do. Konqi, on the other hand, IS much better. But maybe I'm just used to it...\n\nI like your idea of having firefox based on KDE/Qt technology, sure, I just think it won't happen. Unless they can totally lay off the NIH syndrome, but I don't expect it'll happen..."
    author: "superstoned"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-29
    body: "\"If I'm using it as a file manager, pressing \"Home\" should take me to ~, not Google, and vice versa.\"\n\nFor what do you need a home page button so much? In most browsers Home page has 2 functions: 1. The browser starts with it. In Konqueror this can be set in the profile. 2. It loads in when the user presses the Home button. But what about using bookmarks instead? Nevertheless, what you want could be simply solved within Konqueror: a home button which loads the start page of the current profile.\n\n\"If I'm using it as a web browser, why would mistyping a URL cause a scan of my harddrive for what I typed using kio_locate, instead of searching google?\"\n\nWell, I think when a mistyped URL starts a google search is equally annoying.\n\n\"Whay is there no Undo Close Tab? Why no \"Protect Tab\"? The list of missing features one would expect to see in a modern browser shell goes on and on, and a large part of the lack of progress is due to the fact that spanning the worlds of file manager and web browser all at once is immensely difficult.\"\n\nI do not see why these features would be more difficult to include within Konqueror than in a specialised web browser. (And it would also be useful in the filemanager part.)\n\nI agree in that Konqueror should provide a better intrface for extnsions."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-26
    body: "you know, I don't think there's anything stopping anyone from building a simple KBrowser app that just does khtml and nothing else. it could be a fun experiment. :)\n\nimho we definitely need konqueror for the flexible-kpart-viewer thing, though. it's really convenient to have a bunch of documents in one app, regardless of whether they're text or images or some weird crazy format and regardless of where they're stored. :)"
    author: "Chani"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-27
    body: "Actually, there is an application that is almost that: Akregator.\nWhile evidently it is a news aggregator, it could function well enough as a  browser... Even if it is missing a few features.\n\n"
    author: "Luciano"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-30
    body: "heh, true. all it's missing is javascript and an address bar :) ahh, the magic of kparts."
    author: "Chani"
  - subject: "Re: Konqueror development activities"
    date: 2007-07-01
    body: "It does have JS support."
    author: "Vide"
  - subject: "Re: Konqueror development activities"
    date: 2007-07-03
    body: "then why do all websites complain that there's no js when I open a page in akregator?"
    author: "Chani"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-30
    body: "There are many Konqueror specific things that could (and should) be improved:\n- Session handling (almost every other browser does this and it would also be usable in the filemanager)\n- Better profile handling (more settings stored in proiles, e. g. toolbars)\n- Better tab handling: rearranging tabs (currently dragging a tab to another place duplicates it), Undo close, etc.\n- Home button which reloads th start page of the current profile (for some people it is a major problem that the home button directs to the home directory when they browse the web)\n- Better arranged configuration area: a tiny central configration and then the configuration area of the filemanager (Dolphin), khtml, kpdf, whichever is currently used\n- File information sidebar. I know Dolphin does this, but it could also be implemented in KDE, as many people would continu to use Konqueror as a file manager. (See our debate )"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-30
    body: "(Sorry, duplicated)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror development activities"
    date: 2007-06-30
    body: "There are many Konqueror specific things that could (and should) be improved, including:\n- Session handling (almost every other browser does this and it would also be usable in the filemanager)\n- Better profile handling (more settings stored in proiles, e. g. toolbars)\n- Better tab handling: rearranging tabs (currently dragging a tab to another place duplicates it), Undo close, etc.\n- Home button which reloads th start page of the current profile (for some people it is a major problem that the home button directs to the home directory when they browse the web)\n- Better arranged configuration area: a tiny central configration and then the configuration area of the filemanager (Dolphin), khtml, kpdf, whichever is currently used\n- File information sidebar. I know Dolphin does this, but it could also be implemented in KDE, as many people would continu to use Konqueror as a file manager. (See our debate at http://dot.kde.org/1172721427/1172897220/1172915550/1172918074/ . So is the purpose of Dolphin to be really a subset of Konqueror?)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "what ever happened to \"Konqueror for a new exp\"?"
    date: 2007-06-27
    body: "Anyone remember this:\n\nhttp://konqueror4.linuxdevel.net/\n\n?\n\nKonqueror for a new experience!  It was the cool new Konqueror 4 vision discussed widely on the mailing list about a year ago.\n\nWhat ever happened to it?"
    author: "Vlad Blanton"
  - subject: "Re: what ever happened to \"Konqueror for a new exp"
    date: 2007-06-28
    body: "That is a brilliant mock-up.  Kudos.\n\nI would absolutely love to see a Konqueror like that.  Quite frankly, the mockup isn't really aimed at the web-browsing capabilities all that much.\n\nMost of the concepts of the mockup could be ported in Dolphin design.\n\nIf this Dolphin 4.1 or Konqueror 4.1, I would be a very, very happy man.\n\nSomeone please make this so."
    author: "T. J. Brumfield"
  - subject: "Re: what ever happened to \"Konqueror for a new exp"
    date: 2007-06-30
    body: "To me that looks mostly like SVN Dolphin with the Konqueror sidebar (and a few other modifications, but mostly how Dolphin already looks)."
    author: "Sutoka"
  - subject: "Dedicated web browser"
    date: 2007-06-25
    body: "All of you guys wishing for a \"dedicated\" KDE web browser are unaware that one has already existed for a long time, albeit unmaintained - Kafilah. Unlike Dolphin, Kafilah is focused on simplicity, and not usability. To be more exact, it's little more than a performance test.\nhttp://websvn.kde.org/trunk/kdenonbeta/kafilah/\nNot of much use but interesting nonetheless ;)"
    author: "logixoul"
  - subject: "Re: Dedicated web browser"
    date: 2007-06-25
    body: "oh, last commit was 2 years ago.seems to be a dead project."
    author: "Emil Sedgh"
  - subject: "blur"
    date: 2007-06-26
    body: "I just remembered something from the last commit-digest:\n\" updated printer icon to use blur shadow. tweaked to reduce svg file size of 50%.\"\nDoes that mean Qt supports blur now?"
    author: "EMP3ROR"
  - subject: "Bad link"
    date: 2007-06-26
    body: "Hi Danny, the link to \"Icon Cache\" points to the kopete implementation of MP15\n"
    author: "tobami"
  - subject: "Re: Bad link"
    date: 2007-06-28
    body: "Fixed.\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Why...?"
    date: 2007-06-29
    body: "Why didn't you announce KDE 4 alpha 2?"
    author: "Luis"
---
In <a href="http://commit-digest.org/issues/2007-06-24/">this week's KDE Commit-Digest</a>: Introductions of a Dictionary, Photoframe, and Facebook Plasmoids, and a Weather and <a href="http://solid.kde.org/">Solid</a> DataEngine in <a href="http://plasma.kde.org/">Plasma</a>. Usability improvements and optimisations in KListView, used for icon views in <a href="http://www.konqueror.org/">Konqueror</a> and <a href="http://enzosworld.gmxhome.de/">Dolphin</a>. The start of a shared, common location for vocabulary files across <a href="http://edu.kde.org/">KDE-Edu</a> applications, with initial implementation in <a href="http://edu.kde.org/kanagram/">Kanagram</a>. Support for application-specific caches in the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=F3389CEA023E0E9D">Icon Cache</a> implementation, and further progress in the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=5D66C1579098460C">KOrganizer Theming</a> and <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9064143E62AF5BA6">KRDC</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> projects. Better support for ALSA in KMix. <a href="http://uml.sourceforge.net/">Umbrello</a> gets support for SQL code generation. The start of enhanced animation support in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. Scripting interaction with Yahoo! web services to provide weather and stock quote information in <a href="http://www.koffice.org/kspread/">KSpread</a>. Advancements in the <a href="http://ktorrent.org/">KTorrent</a> port to KDE 4. The creation of the PopUpDropper, a context-sensitive drag-and-drop widget in <a href="http://amarok.kde.org/">Amarok</a>. Import of kollagame, a game development IDE. Systemsettings is moved to kdereview as a possible replacement for KControl in KDE 4.
<!--break-->
