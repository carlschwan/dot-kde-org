---
title: "Trolltech Hosting Phonon Backends in KDE Subversion Repository"
date:    2007-12-13
authors:
  - "jmitchell"
slug:    trolltech-hosting-phonon-backends-kde-subversion-repository
comments:
  - subject: "Great"
    date: 2007-12-13
    body: "This is a great move by trolltech. \n\nDoes anyone know if this will be in KDE-4.0? \n\nAnd does this mean that in the future since Qt already has phonon that although Phonon will be developped in KDE's subversion KDE will not release Phonon anymore?\n\nAnd does someone know in which Qt module (QtGui, etc) Phonon will be or if it will be a new module?\n\nThanks in advance for any answers :)"
    author: "leaves"
  - subject: "Re: Great"
    date: 2007-12-13
    body: "For more informations : \nhttp://aseigo.blogspot.com/2007/12/trolltech-phonon-and-open-processes.html\n\nWell, since the backends are in /trunk/KDE/kdebase I think they will be included in kde 4.0 :)"
    author: "shamaz"
  - subject: "Re: Great"
    date: 2007-12-13
    body: "Well the ones for the current OS anyway, I don't think KDE on Linux will require a windows phonon backend"
    author: "Ben"
  - subject: "Re: Great"
    date: 2007-12-14
    body: "Bonus geek points to someone who figures out how to use the Windows phonon backend with winelib on linux.\n\nHave fun."
    author: "Hank Miller"
  - subject: "Re: Great"
    date: 2007-12-14
    body: "Wow!"
    author: "Manuel"
  - subject: "Re: Great"
    date: 2007-12-13
    body: "> Does anyone know if this will be in KDE-4.0?\n\nIt already *is*. See http://websvn.kde.org/trunk/KDE/kdebase/runtime/phonon/ and http://websvn.kde.org/?view=rev&revision=747964\n\n> And does this mean that in the future since Qt already has phonon that\n> although Phonon will be developped in KDE's subversion KDE will not release\n> Phonon anymore?\n\nSorry, I don't understand what you mean.\n\nBut what can tell you is this: Phonon will be released along with each Qt release and each KDE release. This means we'll alternate version numbers. \n\nWhat could be a source of problems is if you have two sets of headers installed in your system. That could cause compiling errors. If that's the case, you can just disable the compilation of Phonon in Qt or in KDE.\n\n> And does someone know in which Qt module (QtGui, etc) Phonon will be or if\n> it will be a new module?\n\nNew module called \"phonon\". It's the exact same API as the one we have in KDE, so no surprises there.\n\nPhonon should be available in tonight's snapshots of Qt 4.4.0."
    author: "Thiago Macieira"
  - subject: "Re: Great"
    date: 2007-12-13
    body: "*awesome*"
    author: "sim0n"
  - subject: "Confusing"
    date: 2007-12-13
    body: "I see the benefits, but find it confusing. Take PyQT. Will QT's phonon be included in PyQT binding's and if so, will PyQT stay only dependent on QT? (hope so). Same question for PyKDE. The library separation must stay clear. Will there be a QT component _and_ a KDE variant based on it? (both residing in KDE's repository?)"
    author: "Fred"
  - subject: "Re: Confusing"
    date: 2007-12-13
    body: "It's the same library. It's just going to get released more often (possibly with minor or no changes between KDE and Qt releases, we'll see). As for dependencies: libphonon itself depends only on Qt libraries. So PyQt could (from the Qt 4.4 release on) contain the Phonon bindings."
    author: "Matthias Kretz"
  - subject: "Re: Confusing"
    date: 2007-12-13
    body: "Phonon is the same library. It doesn't matter where it comes from, does it?"
    author: "Thiago Macieira"
  - subject: "Re: Confusing"
    date: 2007-12-13
    body: "Presumably what it means is that Phonon will be in KDE only, until Qt4.4 is released and KDE moves to using it.  At that point, Phonon will be will be available at a lower layer, in Qt.  The only really KDEuser-visible advantage is that the few non-KDE, Qt-only apps that don't use the rest of the KDE libraries will also have good multimedia support.  As KDE becomes more and more cross-platform, I'm hoping there will be less and less Qt-only apps anyway, though."
    author: "Lee"
  - subject: "Re: Confusing"
    date: 2007-12-13
    body: "It means paid developers working on Phonon, less maintenance burden for KDE developers, commercial support available for Phonon. Trolltech's work on Phonon will assure good multimedia support on _all_ platforms for KDE.\n\nCongratulations to everyone involved on this great piece of collaboration!"
    author: "sebas"
  - subject: "Re: Confusing"
    date: 2007-12-14
    body: "I was under the impression the paid development was happening already, it just moved from Trolltech's repository to KDEs. Am I missing something?"
    author: "Ben"
  - subject: "nice one"
    date: 2007-12-13
    body: "++"
    author: "arno neemes"
  - subject: "Nice KDE description too"
    date: 2007-12-13
    body: "Great announcement!\n\nI also noticed the (new style?) KDE description at the end of the title. I really like this one:\n\n---------------\nKDE is an international technology team that creates integrated Free/Open Source Software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet and web applications, multimedia, entertainment, educational, graphics and software development. Building on the cross-platform capabilities of Trolltech\u00ae's Qt\u00ae, KDE4's full-featured applications run natively on Linux, BSD, Solaris, Windows and Mac OS X.\n---------------\n\nIt puts the \"team\" (aka community) in front, and defines the achievements really well. :-)"
    author: "Diederik van der Boor"
  - subject: "Re: Nice KDE description too"
    date: 2007-12-13
    body: "Thanks, we do too.  A slightly modified version of that text will be used in the foreseeable future for About KDE boilerplate text. "
    author: "Jeff Mitchell"
  - subject: "the trend continues"
    date: 2007-12-13
    body: "\"Let's hope that this trend continues in the future.\"\n\nlook at www.heise.de/newsticker/meldung/100517:\n\n\"Lars Knoll, die treibende Kraft hinter der Render-Engine KHTML des KDE-Browsers Konqueror, wird Entwicklungsleiter (Vice President of Engineering) beim norwegischen Software-Unternehmen Trolltech. [...]\"\n\nThis means that Lars Knoll will work for Trolltech as a Vice Predident of Engineering.\n\nCongratulations."
    author: "heisereader"
  - subject: "Re: the trend continues"
    date: 2007-12-13
    body: "For the announcement in English:\nhttp://trolltech.com/company/newsroom/announcements/press.2007-12-13.8494897112\n\nFor those confused: Lars has been working for Trolltech for 7 years now. The news is that he's now in a new phase of his life, as VP of Engineering. But, even though he's a VP now, he's still getting his hands dirty and coding. Last I saw him, he was deep in discussion about WebKit internals. ;-)"
    author: "Thiago Macieira"
  - subject: "Re: the trend continues"
    date: 2007-12-22
    body: "Konqueror is the first of the three killerfeatures of KDE. Then comes Kontact and last but not least Knotes. Because Knotes is one of those convenient little things that make life easier. "
    author: "love konqueror"
  - subject: "Re: the trend continues"
    date: 2007-12-27
    body: "I like the knotes plasmoid. I mostly use ti to write little notes for my wife and she to me or simple remind each other of important things. It's very convenient and nice."
    author: "Bobby"
  - subject: "Thanks Trolltech"
    date: 2007-12-13
    body: "That's very cool!"
    author: "Joergen Ramskov"
  - subject: "Great News!"
    date: 2007-12-14
    body: "Hey, very cool!  Thats on in the eye for all the knockers and doubters :-)\n\nJohn."
    author: "Odysseus"
  - subject: "Great news!"
    date: 2007-12-14
    body: "I presume this also means that there is an easily-accessed open repository with examples for other people wanting to write other back-ends as well."
    author: "T. J. Brumfield"
  - subject: "Re: Great news!"
    date: 2007-12-14
    body: "there's nothing easier to access than kdebase, is there? =)"
    author: "Aaron J. Seigo"
  - subject: "Gstreamer"
    date: 2007-12-14
    body: "This go for all those \"This phonon-thing suxx cause it's not using directly Gstreamer\""
    author: "Vide"
  - subject: "Re: Gstreamer"
    date: 2007-12-16
    body: "but i want nmm!\n\n(_really not to be taken seriously;\n have you ever seen the code of that *shudder* )"
    author: "Andreas"
---
<a href="http://trolltech.com/company/newsroom/announcements/press.2007-12-11.2263733764">Trolltech announced today</a> that the Phonon backends, which they have been developing for inclusion in Qt, are being <a href="http://websvn.kde.org/trunk/KDE/kdebase/runtime/phonon/">transferred into the KDE source code repository</a>.  <a href="http://phonon.kde.org">Phonon</a> is the KDE 4 API for multimedia and is also set to be part of Qt 4.4, scheduled for the end of Q1 2008.  You heard it right folks, a part of Qt will be officially hosted and developed inside KDE's very own Subversion repository, from whose loins Phonon first sprung, and be freely available to all under the LGPL.







<!--break-->
<img src="http://static.kdenews.org/jr/phonon.png" width="140" height="53" align="right" />

<p>The code being transferred is: Trolltech's GStreamer Phonon backend for Linux and UNIX platforms, their DirectShow 9 backend for Windows, as well as their Quicktime 7 backend for Mac OS X.  All future development on these backends, as well as future development by Trolltech on Phonon itself, will take place directly in the KDE Subversion repository.</p>

<p>Quoting from the press release:</p>

<blockquote>"By developing Phonon components within the globally accessible public KDE source repository, Phonon developers are able to watch and participate in the development of Trolltech's Phonon back-end code and library code contributions. This also allows the community to evaluate and provide input into the work being done by Trolltech&#8217;s internal development team. KDE, in turn, benefits by having Trolltech's employed developers contribute to the ongoing development and maintenance of Phonon, freeing KDE developers to work on other aspects of the desktop. This is a marked change from how open source libraries are usually integrated into commercial products, which has typically involved pulling snapshots of the open source code into the commercial product. Additionally, the Windows and Mac OS X backends will enable KDE4 applications running on these platforms to include multimedia functionality."</blockquote>

<p>This is a great move by <a href="http://trolltech.com/">Trolltech</a> and shows that it "gets" the benefits of collaboration between open source projects and industry.  Let's hope that this trend continues in the future.</p>









