---
title: "Qt Apps Community Sites Launched"
date:    2007-04-29
authors:
  - "liquidat"
slug:    qt-apps-community-sites-launched
comments:
  - subject: "Redundance.."
    date: 2007-04-29
    body: "Just to please FLOSS lovers and Non-Free users.. QT-apps.org and QT-prop.org is created..\n\nit should have been:\n\n1. qt-apps.org for the unbiased (who would like to see both prop. and free software)\n2. qt-free.org for the FLOSS crowd.\n3. qt-prop.org for the NON-Free crows ;)\n\nqt-apps.org is unsuitable just for free apps, because qt proprietary is a qt application too!"
    author: "Fast_Rizwaan"
  - subject: "Re: Redundance.."
    date: 2007-04-29
    body: "for me, i would like prefered Linux-app, what's the hell will all those qt, gtk, kdelibs, libgnome.\n\nwe want app, we don't care about the toolkit, and we don't care either if it is free or proprietary."
    author: "djouallah mimoune"
  - subject: "Re: Re: Redundance.."
    date: 2007-04-29
    body: "I second that. A bit like http://happypenguin.org would be nice. Additionally you could filter FLOSS/proprietary, Qt/GTK/FLTK/OpenGL/Motif/&c. and other stuff."
    author: "S\u00f8ren Hansen"
  - subject: "Re: Redundance.."
    date: 2007-04-29
    body: "I think its good as it is. New community sites do no harm. KDE-apps is a great success. I am however concerned that QT-apps integrate as bad into KDE as do Gnome apps. for instance the use of the top menu bar does not work with Scribus. For a DTP software that apple style menu on top bar really does make sense. I hope that someone would fork the QT-only apps and make KDE ones out of them."
    author: "andre"
  - subject: "Re: Redundance.."
    date: 2007-04-29
    body: "I would think it would be possible for Trolltech to make Qt-apps compile with kde-native support in the same way that they can be compiled with Linux, OSX and MSWindows support. \n\nHowever, they have probably thought of this already and decided against it. But I would have loved lyx and other qt-apps with proper kde-support. I might even have used Opera again :)."
    author: "Dragen"
  - subject: "Re: Redundance.."
    date: 2007-04-29
    body: "> for me, i would like prefered Linux-app,\n> what's the hell will all those qt, gtk, kdelibs, libgnome.\n\nFor me it matters. Being build on KDE means integrates with my desktop, use my other KDE services, reuse my KDE preferences, fonts, icons and color schemes. It means the file open dialog won't suck, help system looks what I know, features are not stripped to death and it'll reuse the kdelibs already loaded (instead of loading new libs). Until cross-desktop standards fix these issues I'll care.\n\nI actually don't like the Gtk/Gnome apps that much, so I prefer to filter. It could apply a filter to the website, or use a separate website. Which one, I don't care, as long as I can exclude some app types.. ;)\n\nJust my $0.02"
    author: "Diederik van der Boor"
  - subject: "Re: Redundance.."
    date: 2007-04-29
    body: "Of course we care about the toolkit. Not only about that, but we care about desktop integration and coherency. That's why I use KDE, and at what KDE excels."
    author: "Derek R."
  - subject: "Re: Redundance.."
    date: 2007-05-03
    body: "You're speaking for yourself, and as we've seen from a load of comments from you in the past, you aren't in agreement with a lot of users.  Lots of us do care about the toolkit.  Lots of us do care about proprietary.  For people who don't, wth are you doing using Linux?"
    author: "MamiyaOtaru"
  - subject: "Re: Redundance.."
    date: 2007-04-29
    body: "Personally, I disagree.  Some of us take the view that, by default, software should not limit your freedom."
    author: "Lee"
  - subject: "Re: Redundance.."
    date: 2007-04-29
    body: "weird thing is, there are Qt only and non-free apps on kde-apps.org. And i agree with the other guy here, too many websites, pretty useless imho. Just confusing..."
    author: "superstoned"
  - subject: "Re: Redundance.."
    date: 2007-04-30
    body: "They have been asked to move their software out of kde-apps.org\nAfter a short period, all non-kde apps that didn't move itself will be removed."
    author: "whatever noticed"
  - subject: "Re: Redundance.."
    date: 2007-04-30
    body: "Great. Now one has to search in three places instead of 1. :("
    author: "testerus"
  - subject: "Re: Redundance.."
    date: 2007-05-03
    body: "Or use Google (or preferred search engine) and include these terms in your search: (site:kde-apps.org OR site:qt-apps.org OR site:qt-prop.org)"
    author: "Matt"
  - subject: "Re: Redundance.."
    date: 2007-05-20
    body: "that sounds like effort.\n\nquick, someone set up a fourth site with one of those google search widgets, set to search the other three sites! ;)"
    author: "Chani"
  - subject: "so you like to play nice..."
    date: 2007-04-29
    body: "Though your suggestion makes sence (qt-apps could aggregate submissions to both sites.. to remove posting redundancy), I think that we should be discouraging proprietary software. Creating the two sites in that manner is doing exactly that.  In that line of thought, I wonder why create a qt-prop site at all?  Just have qt-apps.org open to FLOSS submissions and leave it at that.  I can't imagine qt-prop.org having the same community feel that qt-apps will have (and kde-apps has).  There is a fair amount of bug fixing, patches, and new owner maintanence happening in kde-apps that wouldn't be present in a proprietary site.\n\nVlad"
    author: "Vlad"
  - subject: "Re: so you like to play nice..."
    date: 2007-04-29
    body: "But let's not forget that (Qt) propietry appslications play a very important role - they provide a source of income for Trolltech! And this as to be a good thing for KDE :-D"
    author: "David"
  - subject: "Re: so you like to play nice..."
    date: 2007-04-30
    body: "KDE-look and GNOME-look both have proprietary wallpapers, so what's the big deal with qt-prop having proprietary apps?"
    author: "Brandybuck"
  - subject: "floss???"
    date: 2007-04-29
    body: "As in dental floss?  RMS is surely turning in his grave at that silly acronym."
    author: "KDE User"
  - subject: "Re: floss???"
    date: 2007-04-29
    body: "RMS is not dead"
    author: "claes"
  - subject: "Re: floss???"
    date: 2007-04-29
    body: "if he was, he could not turn in his grave ;)"
    author: "Andi"
  - subject: "Re: floss???"
    date: 2007-04-29
    body: "But what is he doing in his grave when he isn't dead? Test-lieing? That's a bit morbid."
    author: "panzi"
  - subject: "Re: floss???"
    date: 2007-04-30
    body: "touche"
    author: "Vlad"
  - subject: "say no to proprietry software"
    date: 2007-04-29
    body: "we should not waste money or bandwidth on non-free software!\n\nfirst thing we should ban non-free content on kde-apps (and for that matter also on kde-look).\n\nsplitting the community between qt-apps and kde-apps is not really a great idea i think (you could have marked qt-only-apps on kde-apps a different color...).  espiecially since kde-apps will be more portable soon...\n\nanyway getting rid of non-free content on our community sites would be much more important!"
    author: "hannes hauswedell"
  - subject: "Re: say no to proprietry software"
    date: 2007-04-29
    body: "why this software zelotisme."
    author: "andre"
  - subject: "Re: say no to proprietry software"
    date: 2007-05-03
    body: "It's kinda what sets Linux apart from easier (for most people) to use OSs like Windows and OS X."
    author: "MamiyaOtaru"
  - subject: "why split kde-apps.org?"
    date: 2007-04-29
    body: "KDE-Apps features Qt and KDE applications, proprietary and FLOSS. Why the split?"
    author: "testerus"
  - subject: "Re: why split kde-apps.org?"
    date: 2007-04-29
    body: "because they can do it ? that's all"
    author: "djouallah mimoune"
  - subject: "Wow"
    date: 2007-04-29
    body: "There are a lot of strong feeling about this idea. Some people are for it. Some people are against it. Some people seem to think RMS spends his time test-running coffins.\n\nI wish people could sit back and look at this discussion (and MANY MANY others) in a broader context.\n\nFreedom is important. It's very important. OTOH, I have a hard time with the obvious anger and passion of the hard-core free-software zealots. Look around the world. Put things into a broader perspective. I'm going to save my passion and vitriol for stopping the war in Iraq (Yes, I'm an American), global poverty, global warming, etc.\n\nIn the meantime, I will continue to contribute and participate in the Free Software movement. In time, free software will win because it's rules defy the traditional laws of economics. So relax, make a donation to one more cause, and let's write some kick-ass software.\n\nPeace"
    author: "Gunksta"
  - subject: "hi"
    date: 2007-04-29
    body: "hello liquidat\n\nsorry for your name coming up red.\n\nany way i am getting great mail.\n\nhope sepeck to you soon.\n\nnatalya.\n\n"
    author: "natalya"
  - subject: "Good Idea"
    date: 2007-05-01
    body: "This is a good idea since there are quite a few people out there who believe all QT apps require KDE to be installed. This is a good site to show QT-only apps -- just as we have GTK-only ones."
    author: "Kanwar"
  - subject: "Bikeshedding"
    date: 2007-05-05
    body: "To everyone: see http://bikeshed.org before posting. Thanks."
    author: "logixoul"
---
Some days ago the the community around the web sites <a href="http://www.kde-apps.org">KDE-Apps.org</a>, <a href="http://www.kde-look.org">KDE-Look.org</a>, etc <a href="http://kde-apps.org/news/index.php/New+Websites+for+Qt+Software?id=250&name=New+Websites+for+Qt+Software">launched</a> two new web sites: <a href="http://qt-apps.org/">Qt-Apps.org</a> and <a href="http://qt-prop.org/">Qt-Prop.org</a>.
Both sites will be a home for presenting Qt based applications like kde-apps.org already is for presenting KDE applications. The difference between both new sites is that Qt-Apps will be pure FLOSS only, while Qt-Prop is a home for proprietary software.  The launch was done in cooperation with Trolltech.


<!--break-->
