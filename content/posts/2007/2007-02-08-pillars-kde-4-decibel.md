---
title: "The Pillars of KDE 4: Decibel"
date:    2007-02-08
authors:
  - "nogden"
slug:    pillars-kde-4-decibel
comments:
  - subject: "Integration (and KDE4)"
    date: 2007-02-08
    body: "Personally, I think Decibel sounds a bit scary. I'm not big on integration myself, and that's what it sounds like to me. That said, KDE appears to have a much better approach than Microsoft (sorry, but I'm both a KDE and Windows fanatic ;-) ). For example, Microsoft chose to integrate MSN Spaces, Microsoft's blogging site, with MSN Messenger. Now, I absolutely despise MSN Spaces, and have no official way of removing the 'functionality' from their Messenger, or associating with a different blogging site.\n\nEither way, I'm still very interested to see it in action. You guys have proved to be capable of making some very cool stuff. I consider KDE 3.4 to already be superior to the Windows desktop environment, and that definitely says something to me (although some people might take it the wrong way).\n\nOn an unrelated note, how is KDE4 moving along, if I may ask? On Slashdot, I read a random commenter saying saying something along the lines of it being  mostly ideas, but little code. Are you still on track for a final release in the first half of this year? That's what I read, anyway. 2007 looks like it'll be a great year for Linux-based OSes!\n\n(Oh, and kudos to you for a commenting system that doesn't require an e-mail address. Nice CAPTCHA, too!)\n\n~ random Kubuntu n00b"
    author: "TMaster"
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-08
    body: "I see Decibel the same way as DirectX is: a framework that gives application writer the *possibility* to use certain technologies without having to deal with their details in each single application. Application writer may simply ignore some of Decibel, that would not harm.\n\nKDE4 is indeed a lot of ideas, with the subtle difference that you can check them out, compile and run them on your computer :-)"
    author: "PAF"
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-08
    body: "It's not gonna be like DirectX at all, do you even know what DirectX is? Think SDL, this is not a programming interface, it is indeed a service based integration. Just like Phonon is, you add Phonon to your application and your application will use any feature you define in Phonon, for example, you can use Xine engine or Gstreamer, or just like a demonstration in Phonon article in here that shows that we can change the sound devices on the fly.\n\nIf for example Kopete would implement Decibel, then you'll have different things to it, there's houston that's a connection manager, I.E: You add a plugin for a certain protocol, and Kopete will be able to connect to this protocol. From what I understand, it can already do that (Decibel)."
    author: "Serge"
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-08
    body: "In fact PAF was not completely wrong.\nFor example DirectShow allow any media player to use different filters (codec) without having to make specific code. And as AvKode is the phonon backend for FFMpeg, FFDShow is a DirectShow backend for FFMpeg.\nSo even if phonon has more features, it is comparable to DirectShow.\nSo please don't ask question like \"do you even know what DirectX is?\", if you don't know the answer.\nBtw, many video gamers confuse DirectX with Direct3D. Direct3D is only a part of DirectX."
    author: "shamaz"
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-08
    body: "> Think SDL, this is not a programming interface\n> If for example Kopete would implement Decibel\n\nHow can one implement something that is not even an interface ? What I meant is that the application writer needs to make the decision \"I want to use online presence in my app\", and then gets an API to program against. Now later, when new instant messaging protocols appear, this app will not have to change one iota to accomodate for that new service providing its own online presence.\n\nSame thinking goes for audio/video streaming, PIM info management, and already exists for GUI, kioslaves, kparts, and the list is long...\n\nI think I was not too off the mark :-)"
    author: "PAF"
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-08
    body: "It wasn't off the mark.  The guy obviously just had a problem with DirectX being a Microsoft product...so he'd rather choose SDL because it is Open Source.  It is your every day example of letting emotion manipulate your thoughts.\n\nFYI, I used to program DirectX...back in DirectX 4 or 5...last time I checked it was at like DirectX 9.   Man, I'm getting old.\n\n\n"
    author: "Henry S."
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-08
    body: "I don't think this will be the case for KDE4, they are not gonna really make it a part of every application, they are gonna make it an option you can either use or disable, most likely. I don't know though, but I still think Plasma, Phonon, Solid, Decibel, etc... sound really really good, and not overhyped like some thing.\n\nDecibel, it would be awesome for communication, for example, people could write plugins for new protocols and then it would be so easy to implement it in the application. I think we'll finally see some decent VoIP protocols in Kopete, such as real integration with Jingle (Google Talk) and SIP, and who knows, maybe by the time we have KDE4/Decibel out, maybe Skype will finally let people connect to it from other clients, so we'll have a plugin for decibel to connect to Skype. Then Plasma will also allow us to work with Kopete in ways we couldn't think before :) Oh my god... Ok, maybe I'm overhyping this a little."
    author: "Serge"
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-08
    body: "\"I'm not big on integration myself, and that's what it sounds like to me.\"\n\nUm, what's wrong with integration? Seriously? Should they just keep on re-inventing the wheel? Each and every app does teir own thing, and none of the apps would talk to each other? Is that what you want?"
    author: "Janne"
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-08
    body: "Wow, thanks for all the replies!\n\nAnd for why I often dislike integration? If you put a lot of not necessarily related functionality in one application (think browsing and BitTorrent), you might end up with a great browser, that has a lousy BitTorrent implementation, and you'll slowly end up with bloatware.\n\nAs long as you can still separate the parts, it'll probably not be as much of a problem. If I would've been able to change MSN Messenger's blogging provider as easily as I can change Internet Explorer's search engine, it would not have been a problem. Konqueror's file managing and browsing capabilities don't get in eachother's way either, but that's simply because I think it does a good job at the both of them.\n\nDecibel sounds like it will be a rather modular project, so I'd love to see what will become of it even more.\n\nAnother reason is that I like staying on top of certain things myself*. I don't think I'd like to have a list of my contacts, which would automatically select a way of communicating with the person. I like being able to do this manually: open Kopete, check if the person is online, if not, decide to postpone whatever it was I was doing - and not have the operating environment bring up KMail instead, because it would assume it would be the second best option.\n\n*) I am a GUI guy, though.\n\nI guess what I'm trying to say is that you have \"combined\" integration, like Konqueror, and the linking type of integration, like Internet Explorer's search engines. Both can be good, both can be bad - it has to be done in a non-intrusive way."
    author: "TMaster"
  - subject: "Re: Integration (and KDE4)"
    date: 2007-02-09
    body: "\"If you put a lot of not necessarily related functionality in one application (think browsing and BitTorrent), you might end up with a great browser, that has a lousy BitTorrent implementation, and you'll slowly end up with bloatware.\"\n\nUm, that's what would happen with less integration. With integration, you could have separate apps that talk to each other seamlessly. Without integration, every app would have to provide all possible functionality by itself.\n\nAnd what about likes of Kontact? It has lots of functionality. But that functionality is achieved by integrating separate apps in to one (the separate apps are also available independently). Without such integration, they would have had to write every bit of functionality from scratch. More wasted time, more bloat. Instead of having an app that merely integrates existing apps in to one, they would have had apps for email, calendar etc, but they would also have a PIM-suite that would offer it's similar, yet separate email-app, calendar etc. etc."
    author: "Janne"
  - subject: "Integration? => framework sharing"
    date: 2007-02-09
    body: "KDE is usually not integrating, but it is extending its framework and all apps then use that framework getting the functionality for free.\n\none can integrate some apps, let say:\n - email (kontact) and IM (kopete)\n - browser (konq) and torrent (ktorrent)\nthen the email app would be able to connect with the IM app, and the other way around (same for browser/torrent); or one of the apps completerly integrates the other.\n\nthe 'framework sharing' solution is that a framework is created that all apps share (like: anakondi, decibel, kio-slaves, khtml/kjs) then all apps can use the framework; they are really just views of the framework!\n\nso kontact just builds on anakondi and decibel, just like kopete does.\nand ktorretn and konqueror just use kio-slaves for sucking in torrents.\n\nthat's the kde approach.\n\n\"framework sharing\" (tm)\n\n\n"
    author: "cies breijs"
  - subject: "not like MS spaces & messenger"
    date: 2007-02-08
    body: "Well, if I understand the article correctly, this is an integration project only on service level, not application level, so programmers might make use of it to allow communication things to be seen in final application. For example, your amarok will still be that amarok and not become an application for chatting, you got kopete for that. But amarok can use decibel to easily program a new function that allows you to msg someone on MSN messenger as soon as a certain new song is playing for example.\nWell I hope I made my point that it's on service level and not application level."
    author: "Another Kubuntu n00b"
  - subject: "Re: not like MS spaces & messenger"
    date: 2007-02-08
    body: "> But amarok can use decibel to easily program a new function that allows you to msg someone\n> on MSN messenger as soon as a certain new song is playing for example.\n\nThat's something I'd welcome a lot! Currently Kopete and my client (KMess) need to poll the DCOP calls of Amarok/Kaffeine/Noatun/.. every 4 seconds to get the \"now listening to..\" information. If the media application can notify the IM-clients instead it would avoid a lot of unneeded DCOP calls (and in the end CPU usage)."
    author: "Diederik van der Boor"
  - subject: "Re: not like MS spaces & messenger"
    date: 2007-02-08
    body: "Couldn't Amarok/Kaffeine/Noatun/.. just emit a DCOP signal that you could connect to, and save all that polling?"
    author: "Richard Dale"
  - subject: "Re: not like MS spaces & messenger"
    date: 2007-02-08
    body: "If that was possible.. that would be really sweet! This is currently not the case. There is no way to register for some signal (Observer pattern). The DCOP interfaces of the various applications are not consistent either.\n\nBut taking this even further.. IM-clients shouldn't have to know Amarok/Kaffeine/... are media applications. What if an other media player joins the scene? Instead I hope Decibel can offer some generic centralized signal. So when MPlayer/Banshee/xmms/etc.. implement that it could automatically work too. \n\nCurrently Kopete and KMess have hard-coded list of calls to make for all different applications. The second part of this source file reveals that: http://kmess.cvs.sourceforge.net/kmess/kmess/kmess/nowlisteningclient.cpp?revision=1.1&view=markup"
    author: "Diederik van der Boor"
  - subject: "Re: not like MS spaces & messenger"
    date: 2007-02-08
    body: "\"There is no way to register for some signal\"\n\nThis is wrong, you can do this with DCOP. You can even register for a signal with a certain signature (like \"started playing of\") from any emitter. See http://api.kde.org/3.5-api/kdelibs-apidocs/dcop/html/classDCOPClient.html#a27\n\nOr did you mean that Amarok isn't emitting a proper one? Filed a bug/wish report? :)\n\nAnd a standard DCOP/D-bus interface for media players would be nice, agreed."
    author: "K"
  - subject: "Re: not like MS spaces & messenger"
    date: 2007-02-08
    body: "The media player should obey to the observer pattern, and just send a signal on it's state changes. It doesn't need to know that it is talking to IM programs :) Loose coupling, no hardcoding."
    author: "K"
  - subject: "Re: not like MS spaces & messenger"
    date: 2007-02-08
    body: "I think this should be already possible without polling. I only wrote a Lyrics plugin for amarok so far, but I think other plugins work similar. A plugin is a script/program that gets messages from stdin, e.g. the line 'trackChange' to indicate the start of a new track.\n\nSo your script reads lines from stdin and waits for that line. When it encounters that line it asks amarok the name and artist of the song by: 'dcop amarok player title' and 'dcop amarok player artist' (or 'dcop amarok player nowPlaying' if you prefer) and send the information to kopete by 'dcop kopete KopeteIface messageContact \"$user\" \"$song\"'.\n\nNow put this script into e.g.: ~/.kde/share/apps/amarok/scripts/<your-script-name>\nand make this file executable."
    author: "panzi"
  - subject: "Re: not like MS spaces & messenger"
    date: 2007-02-08
    body: "Instead of using KopeteIface you could use KIMIface and be able to adapt to a different IM app by changing the DCOP name (as long as the other application also implements KIMIface)\n"
    author: "Kevin Krammer"
  - subject: "So is Decibel Hal/DBUS for software?"
    date: 2007-02-08
    body: "So, basically, Decibel is for software what HAL/DBUS is for hardware?\n\nIs it only for audio or are there more applications for it?"
    author: "Darkelve"
  - subject: "Re: So is Decibel Hal/DBUS for software?"
    date: 2007-02-08
    body: "No, Decibel is a framework (API) for integrating Communication protocols with KDE applications. Instead of having to program the protocols for every application, you'd have one API that would use DBUS to talk to the Application and back so they can establish connections, start chats, etc....\n\nIf we're talking about Amarok here, another thing that would be cool with Decibel is sharing of music directly from amarok to instant messaging. Some sort of Social Network..."
    author: "Serge"
  - subject: "Re: So is Decibel Hal/DBUS for software?"
    date: 2007-02-08
    body: "\nI think the name \"Decibal\" is misleading you into thinking \"audio.\"  It seems to be more about instant messaging, e-mail, VoIP and things like that.  Probably a better name would be \"Kollab\" but I think that is taken.  Perhaps \"Communicate\" would be a good name....or should I say \"kommunicate\"?"
    author: "Henry S."
  - subject: "hmmm..."
    date: 2007-02-08
    body: "ok... i was generally absolutely for all of those changes in KDE4, they sounded ok and practical... but this time i just don't think this is gonna be ok for me. i strongly recommend allowing the user to totally turn off the usage of Decibel by apps.\n\ncheers!"
    author: "mikroos"
  - subject: "Re: hmmm..."
    date: 2007-02-08
    body: "Isn't it a bit early to be judging this?"
    author: "Joergen Ramskov"
  - subject: "Re: hmmm..."
    date: 2007-02-08
    body: "because?"
    author: "K"
  - subject: "Re: hmmm..."
    date: 2007-02-10
    body: "because i definitely prefer simplicity over all-in-one's. for example i don't like Superkaramba even though i am aware of its functionality - maybe it adds some functionality but intereferes with a native role of a desktop so i don't like it. for me it is easier to navigate in an environment where every program has its own and separate role and i expect my KDE to enable me to switch on/off each function easily. that's why, in a few words. i do not say that this is a bad technology (maybe i just don't realize its advantages) - what i do is just mentioning that i don't think it would be useful _for me personally_. however, it is of course a very early stage of development (or even less: just a description) and surely i will give it a try and then decide what to do. best wishes!"
    author: "mikroos"
  - subject: "Re: hmmm..."
    date: 2007-02-08
    body: "If I understand this right, Decibel is not much more than DCOP/D-BUS. Just a bit of a framework around D-BUS? And KDE3 already uses DCOP heavily, so if you don't want such a functionality, don't use KDE (and don't use GNOME, because those guys already use similar technology). "
    author: "panzi"
  - subject: "Re: hmmm..."
    date: 2007-02-08
    body: "decibel is more than the communication protocol, it has a daemon (houston) which stores information about on/offline presence and coordinates & communicates settings concerning communation between applications using decibel's protocols.\n\nalso, like phonon, decibel will offer some easy-to-use widgets to quickly enable applications to have embedded communicationtools.\n\ndecibel sounds simple, but don't underestimate it... :D"
    author: "superstoned"
  - subject: "The name \"Decibel\""
    date: 2007-02-08
    body: "I think this has been mentioned before, but I still find it odd: why \"Decibel\"?\nTo me it sounds like something to do with sound. Well, you might say that it is; you communicate by talking right? However, what I mean with \"sound\" is for example music.\n\nIf I've understood Decibel right, then something with both connection/communicating would be a more fitting name. Am I wrong?\n\nBy the way, thank you for the article."
    author: "Lans"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-08
    body: "It is a fitting name. From Wikipedia:\n\ndecibel is also considered to be a dimensionless quantity\n\nThe bel (symbol B)is mostly used in telecommunication, electronics, and acoustics. Invented by engineers of the Bell Telephone Laboratory to quantify the reduction in audio level over a 1 mile (1.6 km) length of standard telephone cable, it was originally called the transmission unit or TU, but was renamed in 1923 or 1924 in honor of the laboratory's founder and telecommunications pioneer Alexander Graham Bell.\n\nhttp://en.wikipedia.org/wiki/Decibel\n\n"
    author: "reihal"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-08
    body: "sorry to sound snarky, but if the feature is as \"fitting\" to KDE, I don't think I really want it :)\n\nAnyway, to me it sounds like Yet Another Communication Layer. What you describe here is an \"easier DCOP\", another interface that developers will have to add to their programs. Why not spending the effort making DCOP easier...?"
    author: "Giacomo"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-08
    body: "1) KDE4 will not use DCOP.\n\n2) DCOP (and D-Bus) is meant for programs on one computer to talk to each other. Decibel is about allowing users on different computers to communicate. That is a pretty different use case! I guess you would not want to communicate with your friends by using predefined \"method calls\" only;-)"
    author: "Tobias Hunger"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-08
    body: "D-BUS is designed to be able to communicate over any kind of socket. Why not make D-BUS able to communicate over a (SSL-)TCP/IP-socket (instead of just over a UNIX-socket)? Or is this what Decibel dose?"
    author: "panzi"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-08
    body: "decibel doesn't communicate, it uses chatprotocols like msn, irc, voip, email for that. the app just tells decibel 'i want to talk to this user', then decibel looks if that person has a phone, and is available on voip -> calls the user. no phone or not available? let's try chatting. no chatting? mail. etcetera.\n\nit can also tell you if a user is available so the apps don't have to try and figure that out themselves (compare this to phonon), and it can manage meta-settings (like phonon, which can set policies like 'when the phone goes, lower music volume')."
    author: "superstoned"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-08
    body: "> the app just tells decibel 'i want to talk to this user', then decibel looks if that person has a phone, and is available on voip -> calls the user. no phone or not available? let's try chatting. no chatting? mail. etcetera.\n\n\nI hope you mean giving the user the choice to choose between a number of options to contact someone else. The software can't know about the context. Depending on whom you want when to contact for what reason, you might try possible ways to do so in a different order or even decide to shove it to somewhat later.\n\nThere's a helluva lot of reasons to choose between asynchronus/syncronus and voice/video/textual contact methods. SMS has not become so popular for no reason.\n\nA software might well guess, but that's it about it."
    author: "Carlo"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-09
    body: "well, i think some sane defaults would be nice. the ability to have (configurable) policies like the ones i stated would be nice."
    author: "superstoned"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-09
    body: "If you really mean the software should decide for the user, I can tell you the software will be pita and you'll earn nothing but complaints."
    author: "Carlo"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-13
    body: "From a geek pov this is true, from the remaining 98% of users, this is not always true (I mean, that self-deciding software is a pita).\nAlthough I strongly believe that as in usual KDE tradition (almost) everything will be configurable in some way by the end user."
    author: "Davide Ferrari"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-11
    body: "SMS is actually an extremely popular medium in my country. I find it incredibly useful to send an SMS to someone and not interrupt him on what he was doing. He can text back with a reply when he is done.\n\nThat is a totally different use case. Same thing goes for IM and chat. Some people prefer an asynchronous communication. Some prefer synchronous ones to make sure they have total attention of the other party."
    author: "rafelbev"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-08
    body: "As already mentioned: DCOP & DBUS are NOT voice nor sound nor voip implementations...\n\nDbus & Dcop are message bus system for APPLICATIONS --> NOT FOR HUMANS!! xD.\n\nfrom what I unerstand the layers will be something likde this:\n\nKDE APP\n   |\n   |\n   V\nDECIBEL: Which is a service architecture to make chat and phone communication universally available to desktop applications. This part allows you to be creative with the final FUNCTIONALITY of your Product and stop missing time imagining how to do this or this.. in other words --> Think how to USE the PHONE not how to reinvent it.\n   |\n   |\n   V\nPhonon: Configurate your sound and multimedia devices easily. Decibel will use it to make the HUMAN <--> Computer interaction.\n   A\n   |\n   |\n   V\nDBUS for IPC. When programming your app. you might never touch or need to know how to program the Dbus API, everything will be easier and faster. Dbus communication will be Decibel job.\n\nIn resume, This framework / API's will accelerate the development of many many new high end class state of the art applications. xD\n\nThat's all.. I think xD\n\n\n\n\n"
    author: "gatuus"
  - subject: "Re: The name \"Decibel\""
    date: 2007-02-08
    body: "> What you describe here is an \"easier DCOP\", another interface that developers\n> will have to add to their programs. Why not spending the effort making DCOP easier...?\n\nThat's like saying.. protocols like SMTP / HTTP / FTP are just \"easier TCP\". Why not spending to effort making TCP easier?\n\nDBus is the communication channel, just like TCP in my example. Over this channel you need to define some message formats. That's one part of what Decibel is going here, defining the messages! Just like SMTP / HTTP / FTP define what messages to send over TCP. Just like Telepathy is doing, like KIMIface does."
    author: "Diederik van der Boor"
  - subject: "Just some musing on potential."
    date: 2007-02-08
    body: "Ok, so I think I understand the idea of Decibel, and I think I understand Phonon, so I'll lay out what I think would be a neat scenario, and people who know better than me can say if it will or won't work.\n\nLet's say Phonon and Decibel become relatively well integrated, with the idea that there is a coherent connection between your identity as someone who is communication, and the sounds you are listening to.\n\nCould you:\n- Use Decibel as an output for Phonon, allowing any audio application to stream music to any of your \"contacts\"?\n- Use Decibel as an input for Phonon, allowing your contacts to effect what you are listening to?\n\n(There is a gray area of authentication that would need to be addressed for these to become a reality)\n\nIn both of these cases, these are things that can be done today, but it seems to me that this would enable them to be done at a much higher level. So rather than audio applications having to deal with network protocols, or network applications having to deal with sound formats, you could have it all helpfully abstracted through Decibel and Phonon). If you wanted to have your audio networked, all you would need is this integration, rather than a plugin for your specific audio program.\n\nI'm sure this isn't the primary focus of these libraries, but I would be very interested to hear how possible this would be.\nThanks."
    author: "Alec Munro"
  - subject: "Re: Just some musing on potential."
    date: 2007-02-08
    body: "I don't think it will work quite like that. I think it would be more like\n\nFriend (sends MP3) --> Kopete --> Phonon --> backend of choice (say Gstreamer) --> ALSA --> speakers\n\nHowever Decibel Amarok Devs may be able to use Decible to give Amarok internal functions for streaming MP3's over VOIP or Instant messenging. I'm not sure about that though."
    author: "Ben"
  - subject: "Re: Just some musing on potential."
    date: 2007-02-09
    body: "So KDE4 will integrate firefly and other streaming servers?"
    author: "Max"
  - subject: "Re: Just some musing on potential."
    date: 2007-02-09
    body: "well, aside from decibel not really being input to phonon (it's more like it tells phonon to do stuff, and asks for stuff) i guess those things would be possible, yes.\n\nthe first example would go more like this (i think):\namarok uses decibel to ask for contacts it can stream music to. decibel only returns those who have the right technology (eg msn can't stream music, so msn-only contacts won't show up. and someone who is offline won't show up either...).\n\nremember phonon doesn't do anything, apps use it. the same with decibel.\n\nand the second:\namarok could have a plugin, which asks decibel to return the 'orders' from the contacts, and amarok will play it (through phonon, indeed)."
    author: "superstoned"
  - subject: "KDE4"
    date: 2007-02-09
    body: "When will development versions of the next KDE be available? "
    author: "Magar"
  - subject: "Re: KDE4"
    date: 2007-02-09
    body: "They are already.\n\nhttp://kubuntu.org/announcements/kde4-3.80.2.php\nhttp://en.opensuse.org/KDE\nhttp://forums.gentoo.org/viewtopic-t-530111-highlight-kde4.html\n\netc.\n\nhttp://developer.kde.org/source/anonsvn.html shows how to get the latest code.\n\nDerek"
    author: "D Kite"
  - subject: "Re: KDE4"
    date: 2007-02-09
    body: "I don't think he wants to know about 3.80.2, but rather about the NEXT release.  I think many want to know... besides a roadmap would be nice. \n\nA tip for the next articles about KDE4 applications/pillars/etc is to mention how far these applications are in the development process."
    author: "Ernst"
  - subject: "Telepathy"
    date: 2007-02-09
    body: "How is Decibel related to Telepathy. Do these technologies compete or can they use each other?"
    author: "Henning"
  - subject: "Re: Telepathy"
    date: 2007-02-09
    body: "Decibel uses Telepathy over Qt/DBUS interface."
    author: "Serge"
  - subject: "Re: Telepathy"
    date: 2007-02-09
    body: "Telepathy is pretty much a FD.org standard, and Decibel is an implementation (I think Telepathy also includes a simple reference implementation as well, like Tango is a spec for naming icons, but it theres also the Tango icon theme which is an implementation of the specification (Oxygen will follow the Tango naming spec, IIRC several of the people working on Oxygen also worked on Tango)."
    author: "Corbin"
  - subject: "Integration - how to do it right"
    date: 2007-02-09
    body: "Some commenters are concerned that Decibel will result in bloat and the forced integration of unwanted apps.  I'd like to clear that doubt up.\n\nThe distinguishing factor between bad and good integration is the degree of coupling.\n\nThe MSN integration mentioned above is an example of tight coupling.  Tight coupling is good when you want to get something done quickly and flexibility be damned, but doesn't leave you many choices afterward.\n\nThe coupling which Decibel offers, like KIMProxy before it in KDE 3, is loose coupling.  In this model no component is bound to a particular other component.  Decibel defines interfaces, that allow any component that implements that interface to be replaced with any other component.  \n\nFor example, with KIMProxy we were able to integrate Kopete and KAddressbook to show contact status in KAddressbook.  However this was no tight coupling.  As other applications implemented the KIMProxy interface, these could be used instead, for example, substitute Licq or Konversation for Kopete as the sources of contacts' online status.  This is loose coupling, and this is what Decibel provides."
    author: "Will Stephenson"
  - subject: "Compatibility"
    date: 2007-02-09
    body: "Maybe it's a bit offtopic - but are all the KDE projects Phonon, Decibel, ... compatible with Gnome technics?\n\nIn my point of view this is most important for Linux adoption on the desktop... choice is good but I think common specs for both Gnome and KDE are the MOST IMPORTANT thing.\n\nIt will be very bad if people cannot change colors, themes, audio backends for both environments in ONE configuration app... Hope that freedesktop will help in that direction.\n\nIt's too complicated at the moment - settings like themes, one/two mouse click behaviour, left/right hand mouse, fonts, colors etc. for Gnome apps in Gnome control center for KDE apps in KDE control center.\n\nToo complicated and time consuming for beginners! And why should someone who uses Ubuntu only use Gnome apps because of better look and feel? It's a big limitation if a user shall not use KDE applications in Ubuntu or vice versa... \n\nIntegration of both DE's is the most important thing. Portland project is a very good step in the right direction. But KDE and Gnome should use same things for storing configuration (like gconf or sth. else) etc."
    author: "FelixR"
  - subject: "Re: Compatibility"
    date: 2007-02-09
    body: "Hopefully Decimal will remove the requirement for QT, so any application can take take advantage of the framework."
    author: "Todd"
  - subject: "Re: Compatibility"
    date: 2007-02-10
    body: "All this is based on communication over D-Bus and there is no Qt dependency in D-Bus.\n\nSo, since there is no requirement for Qt, there is nothing to remove :)"
    author: "Kevin Krammer"
  - subject: "Re: Compatibility"
    date: 2007-02-10
    body: "And even if it did. How would \"any application\" be unable to use QT?"
    author: "Debian User"
  - subject: "Re: Compatibility"
    date: 2007-02-10
    body: "All power to standards, it's going to help us. Most new tech from KDE4 is not existant in Gnome. But that's only a matter of time I guess.\n\nBut can't we agree that \"beginners\" are best off using only one desktop and its integrated applications anyway? I personally hate the idea of letting \"beginners\" mess around with OpenOffice, FireFox and and rest-desktop.\n\nWith KDE 4.1 and KOffice 2.1 at latest, I would assume there will no reason at all anymore, to use anything from the other desktop. Sure Gnumeric will still be better than KSpread maybe, but why care? I personally use only KDE applications nowadays. Konqueror, KWrite, Krita, Kate, Eric3 (ok, at least QT), KDevelop, Digikam, Kmail, Kontact, Konsole, Amarok, etc... \n\nAnd that's because I find consistency of higher value than most everything. There is Firefox and OpenOffice for situations where my KDE environment currently is not sufficient for reasons outside KDE's responsibility, external broken formats, so I am very happy.\n\nI am very confident a \"beginner\" can be very happy with pure KDE 3.5.6 and applications already. For the next round, I would be certain.\n\nGreetings,\nKay\n\nPS: I would also sell all shared on Gnome, if there were any. Ah, gone those Novell shares ;)"
    author: "Debian User"
  - subject: "Re: Compatibility"
    date: 2007-02-11
    body: "The new tech in KDE4 is indeed exciting, though much of it is built on top of freedesktop.org (and other free software) standards that GNOME has been very instrumental in building.  Phonon, for example, is a high-level API that wraps lower-level APIs in a KDE friendly way; yet, it is not necessarily new tech in the sense that GNOME has access to the lower-level APIs, such as GStreamer, to which the GNOME developers contribute.  Similarly, Solid wraps HAL, which is developed (in part) by GNOME developers.  Decibel wraps Telepathy, which too is developed (in part) by GNOME developers.  On the other hand, I'm not sure GNOME has anything like Sonnet or Plasma.\n\nIt seems to me that over the last several years, GNOME developers became disillusioned over developing new technologies under the GNOME umbrella because they feared that other groups, such as KDE and Mozilla, would not use the new technologies but develop their own equivalents.  So, the GNOME developers began putting more and more energy in developing the technologies in independent places, such as freedesktop.org, where there was a better chance that KDE and others would use them and contribute to them.\n\nThese new technologies were often written in C, however, using GNOME-like coding conventions (and sometimes use GLib as well).  So, it makes a lot of sense for KDE developers to wrap these APIs in a way that suits Qt/KDE conventions.\n\nSo, based on all of this, I think it's inaccurate to say that GNOME doesn't already have much of what's coming in KDE4: they're just using the technology directly from freedesktop.org and other places without wrapping the APIs.  That said, the KDE4 technologies look very exciting, and it will be interesting to see how versatile the frameworks will be with the various back-end plug ins.  Good luck to KDE4.\n\n"
    author: "aigiskos"
  - subject: "Re: Compatibility"
    date: 2007-02-13
    body: "I don't think its because GNOME devs became \"disillusioned\", I think that Gnome and KDE had a bad fight in the beginning over QT licencing and over the last few years the devs _both_ groups have gotten over it and they are both working togeather. \n\nTake this quote\n\n\"Decibel wraps Telepathy, which too is developed (in part) by GNOME developers.\"\n\nTrue but what you don't mention is that KDE devs are also working in Freedesktop.org and probobly also worked on Telepathy. not \"disillusioned\" just working togeather."
    author: "Ben"
  - subject: "Re: Compatibility"
    date: 2007-02-11
    body: "> But can't we agree that \"beginners\" are best off using only one desktop and its integrated applications anyway? I personally hate the idea of letting \"beginners\" mess around with OpenOffice, FireFox and and rest-desktop.\n\nThis is what I meant with I do not want to restrict me in using software because of a toolkit which they use.\n\nHow shall I tell people they shouldn't use Gaim, Gimp, etc. under KDE? They are professional apps and not using them because working in KDE means not using great software.\n\nThat's why I think there must be a central configuration place: if you use KControl you should be able to set fonts, themes, colors for Gnome apps and vice versa. If there is a solution like that people are not forced in using only apps for Gnome OR KDE if they want to have a common look'n'feel.\n\nI wouldn't underestimate the importance of look'n'feel for many people. Otherwise they ask rightly \"why does Gaim look completely different than Konqueror\" etc.\n\nKDE and Gnome should even use common user interface guidelines and common keyboard shortcuts!! Why is settings dialog in Gnome apps under \"Edit\" and in KDE apps under \"Settings\" etc.? -> It's too confusing!"
    author: "FelixR"
  - subject: "Re: Compatibility"
    date: 2007-02-12
    body: "Interesting to mention Windows isn't consistent either with toolkits. Compare Borland / Delphi and .Net apps. Compare the menu's of Word, Visio, Visual Studio, Windows Explorer and Notepad. You can tell these applications are built with different toolkits.\n\nThe reason no one is annoyed by it is the consistent colors and fonts these applications all use. This makes a huge difference!"
    author: "Diederik van der Boor"
  - subject: "Re: Compatibility"
    date: 2007-02-13
    body: "\"This is what I meant with I do not want to restrict me in using software because of a toolkit which they use.\"\n\nNo one is restricting you but there are advantages of sticking to one, especially in the beginning. \n\n\"How shall I tell people they shouldn't use Gaim, Gimp, etc. under KDE? They are professional apps and not using them because working in KDE means not using great software.\"\n\nDon't, tell them to try Kopete and Krita, if they prefer Gaim, despite its lack of intergration, then let them.\n\n\"That's why I think there must be a central configuration place: if you use KControl you should be able to set fonts, themes, colors for Gnome apps and vice versa. If there is a solution like that people are not forced in using only apps for Gnome OR KDE if they want to have a common look'n'feel\"\n\nA common look and feal is a lot more than just Kcontrol, I havn't used Gnome but I'd be suprised if it didn't have a very diffrent menu structure from KDE. There is also things like the save file dialoge, there is one standard and very good one for KDE. There is probobly a standard Gnome one. They're both diffrent.\n\nOf course if you just want to do the basics just download a Gnome theme with an identical KDE counterpart and install both. "
    author: "Ben"
  - subject: "Re: Compatibility"
    date: 2007-02-13
    body: "I may be unique, but I like to switch around between Gnome and KDE from time to time.  (That's not the unique part - I know others do that)  But while I'm in each, I only use apps from that environment as much as possible. \n\neg\nKDE I use Kopete, Konqueror, amaroK, etc\nGnome I use Gaim, Firefox, banshee, etc\n\nWhat I'd like to see is a central place to keep settings, confs, etc.  For example, I should be able to easily access my bookmarks in both Konq and Firefox.  I should be able to read my mail in Kmail or Thunderbird without having to download the same messages twice.  \n\nWhy do through this hassle?  The author of this thread metioned on reason - visual consistency.  It really adds a lot when I have to spend hours in front of the cpu at work and home.  But there's also integration!  When I'm in Kopete and click a URL it launches Konq (or should - if it doesn't) so I'd like to be able to access my bookmarks.  Or email contact in Kopete should launch Kmail with all my settings from Thunderbird.\n\nPerhaps via freedesktop we can do something like this.  There will be some settings specific to each program and those can be in .program_name, but the common files could be in .messaging, .internet, etc\n\nAnyone else agree?"
    author: "Eric"
  - subject: "integration"
    date: 2007-02-10
    body: "I think that software should be about inteligent features not \"inteligent\" decisions. what I mean is that I don't want kopete to start kmail, but I love the smile in kmail as a notify for a user being online in icq/msn/jabber. a common communication history would be even more avesome. if a user is offline in icq maybe I would like to send her a email, but it shouldn't be automatic, a nice  little icon for voip/mail would be more than enogh. the first step should be to find out patterns of user behavior, then provide actions that are likely for the use case."
    author: "miro"
  - subject: "Integration should be more integrative ;-)"
    date: 2007-02-11
    body: "From my point of view, the whole thing goes to the right direction but not the whole way. Lets make a diffrent approach for application integration. I will call it \"aspect oriented applications\" (nothing to do with the Java thing). Instead of making the applications integrate theirselfs we should make document integration. In my environment a lot of small business companies have a lot of documents such as word excel access emails and a lot of other information. But the informations about its relation together is \"stored\" only in the brains of the employes. Who nows that the document Offer-2006-10-12-ProductA.swx has something to do with the email \"Thanks of the Offer\" replayed at 2006-10-20 sent to mr. Smith. And the spreadsheet \"Caluculation-2006-10-11.swx\" is the calulation for that offer. So, only the employe who was working on that business process knows the relation between all this informations. So if we want make a better application integration, we should find a efficient way to link all these informations together in a assotiative way such as a human does it. So lets think about a small and easy szenario: In the above case it whould be nice to have a central address store for all desktop and business apps. Attached at this address store a workflow engine should link all workflows (such as the above calulation, offer, acceptance of this offer etc) together with the address records of the customers, partners and supplirs etc. This is just one szenario, there might several others for developer companies, it integrators, universities and a lot more, but all have together that the assotiation of all those documents and informations can be stored in a defined and standard way. And at the end (as I hope so) we will no more work with files on a fileshare (nfs or smb or something else) we will rater have \"documents\" which are stored in assotiative database by which each of the attached workers (employes etc) can refind all infos to a worklow a other worker has made without having to use such helper programs as beagle or other search engines. All informations are pre se stored in a refindable way."
    author: "Roland Kaeser"
  - subject: "Re: Integration should be more integrative ;-)"
    date: 2007-02-11
    body: "Based on what I remember from aKademy2006, managing relations like this is part of the idea the Nepomuk project is working on.\n"
    author: "Kevin Krammer"
  - subject: "Re: Integration should be more integrative ;-)"
    date: 2007-02-12
    body: "I've read it. But is the same as a wrote above. It goes one step ahead but not the whole way. It isn't about meta data it is about the whole relation between the document data itself. I my vision the \"My Computer\" or \"Arbeitsplatz\" (in german) doesn't show files and mount points anymore. It rater will be a database frontend to search and create new document regardless which applications is used to create it.\nIt seems to go the same way as the most of the opensource projects goes, first everybody implements its own way, which brings a lot of diffrent approaches to solve the same problem, and after some time all of them see that a standardized  concept will be better and after that a central workgroup makes this standardization."
    author: "Roland Kaeser"
---
The KDE development team is working hard on the KDE 4 platform. KDE 4 will include many exciting new technologies which will greatly enhance the functionality of KDE. One of these new technologies is <a href="http://decibel.kde.org/">Decibel</a>. We would like to give you an idea of what Decibel is all about.



<!--break-->
<div style="float: right; margin: 2ex">
<img src="http://static.kdenews.org/dannya/decibel.png" border="0" width="250" height="94"/>
</div>

<p>In putting this article together, the KDE promotional community was able to get most of the information from the lead developer of Decibel, Tobias Hunger. Tobias lives in Germany and studied Computer Engineering at the University of Kaiserslautern. Upon graduating, he found a job as a consultant for a small company specializing in systems management. Currently, he is employed as a software developer for basysKom GmbH.</p>

<p>This article is part one of a four part series about Decibel. In part one, we would like to provide a general overview of Decibel. Part two will define several terms related to Decibel. Benefits for developers will be covered in part three. Finally, part four will discuss benefits for users.</p>

<p>People use their computers to communicate with others. Usually, they want to communicate as close to real-time as possible. Email, instant messaging, and Voice over IP (VoIP), are some of the different ways people communicate using their computers. Each of these has its strengths and weaknesses. Ironically, each of these means of communication do not talk very well with other means of communication.</p>

<p>This is where Decibel comes in. Decibel is a service, not an application. The goal of Decibel is to create a bridge between different communication technologies. Decibel will make it easy to integrate real-time communication technologies into applications, Tobias says. Decibel provides a central storage place for settings of real-time communications. This will allow one communication application (say, email) to talk to another communication application (say, instant messaging) without having to learn a new language.</p>

<p>However, Decibel is not going to become yet another isolated box dealing only with communication. There are at least two ways Decibel will be able to connect to technologies and applications not normally associated with communication. Because Decibel allows programs in general to talk to each other in a more streamlined manner, programs that are not related to communication can also take advantage of this technology. For instance, document editors (such as word processors or graphics editors) could use this technology to allow better collaborative editing.</p>

<p>Also, Decibel is being developed to integrate with other KDE technologies. For instance, <a href="http://phonon.kde.org/">Phonon</a> is a KDE technology that deals with integrating multimedia programs and services. It is possible for Decibel to work with Phonon in situations such as encoding and decoding voice data during a VoIP conversation.</p>

<p>In part two of this series on Decibel, we will define some terms to help further enhance your understanding of Decibel. In the final two sections, we describe potential benefits for both users and developers, and provide information on how to get involved with the Decibel project. The goals of the Decibel team are wide-ranging and forward-thinking. They have done an excellent job of specifying their vision. However, they are still in the beginning stages of reaching their goals. Much work needs to be done. The Decibel team would appreciate any help they can get.</p>


