---
title: "CA-Browser Forum Finalises Extended Validation Certificates"
date:    2007-06-20
authors:
  - "gstaikos"
slug:    ca-browser-forum-finalises-extended-validation-certificates
comments:
  - subject: "And thanks to you, George"
    date: 2007-06-20
    body: "Thanks for your efforts. I remember when this all started and when the forum was created. Needless to say we're glad to have you around."
    author: "Thiago Macieira"
  - subject: "\"Real life\" security would be better"
    date: 2007-06-20
    body: "Although I appreciate that there is an open process for defining security certifcates this won't improve the current situation with SSL in real life (Phishing etc).\n\nWhy that?\n\n1. Usability:\n\nCurrent web browsers inform you with a dialog box if you enter a web site with a valid certificate (\"This web site has a valid certificate signed by foobar. [OK] [Don't show this again]\"). There is also a dialog box on self signed certificates \"This web site doesn't provide a trusted certificate. [Continue] [Cancel]\")\n\nWhat does an average user \"see\" in both cases? \"Oh yawn yet another nasty dialog box with some harmless useless info text. I want to get my work done. I click on every OK/Continue without reading.\"\n\nSo please under any circumstances NEVER EVER show \"info dialog boxes\" in a browser unless the user explicitely requested one (for example only an explicit right click on the URL bar gives you some information about a certificate).\n\n2. \"Valid\" SSL certificates aren't widespread enough:\n\nHow many user want their private web site secured via SSL but simply don't want to spend a lot of money on Versign and friends for something they can create on their own?\n\nA lot. Probably everyone knows a web site of a friend or even pages of non-commercial authorities (university web sites, not-for-profit-organisations...) that provide self signed certificates which tell you: \"Hey I am trustworthy; Click on \"continue\" and everything is fine\". So in daily life you very often get the \"invalid certificate\" warning in case everything is fine. So when you click 100 times on continue, you automatically will do this (by routine) in the dangerous 101. case. EV certificates will improve NOTHING in that aspect.\n\nThere is an easy solution made from private people for the people: CA-Cert.\n\nBut not a single browser supports it. Why? Because CA-Cert cannot be made legally responsible for damage by harmfull people that tricked them and because SSL is confused with some imaginary Fort Knox security.\n\nPlease stop stop dreaming of imaginary Fort Knox security, start doing real world security. Supporting CA-Cert is something which will improve real world security on SSL pages dramatrically. And even furthermore: Importing CA-Certs root certificate by default into browsers will secure and strengthen the business of Versign and friends, because SSL in general will \"start working\" in daily life.\n"
    author: "Arnomane"
  - subject: "Re: \"Real life\" security would be better"
    date: 2007-06-20
    body: "they did say \"a great step forward\" not \"the final piece of the puzzle\".\n\nboth the problems you note are real; in fact, i think the solutions to each are linked:\n\nwith better UI (e.g. one this is simultaneously more expressive and less modal) we might be able to start to provide feedback on varying levels of security, allowing for \"less trusted but better than no signer\" groups like ca-cert to play in the game.\n\nthis is, btw, why microsoft has been moving to putting more information in the toolbar (harder to fake, easier to see, less in the way)\n\nstill, the new certs are an improvement of one aspect of things and thus should be accepted as such. it's very cool that we were able to have a part in this process and that we were perceived as an equal player at this level."
    author: "Aaron Seigo"
  - subject: "Re: \"Real life\" security would be better"
    date: 2007-06-21
    body: "I didn't criticize EV certificate advocates for claiming to have the final piece of the puzzle, I did criticize EV certificates for beeing wasted money if not a lot of other more important steps are done in advance.\n\nThe concepts for UI improvements with regard to security are luckily solved in a similar fashion by all common browsers and I'm thankful to your great UI work (technically and in meetings with different browser vendors).\n\nI interpretate your answer '\"less trusted but better than no signer\" groups like CA-Cert to play in the game' as being connected to EV certificates. I don't have a good feeling with the opinion that CA-Cert necessarily is less trustworthy than a company like Verisign and now as there is a better EV certificate available one can degrade the common SSL certificates and let the dirty CA-Certs join by default without harm. I bet a large group of Free Software people will disagree with that view.\n\nIMHO this is not about less trustworthy against more trustworthy: It is about two different security models: Central authority versus web of trust (CA-Cert brought web of trust to SSL, which originally was designed for central authority model only). Whom you trust more is highly depending on your personal beliefs and philosophy.\n\nSo I'd advocate for something more honest and less biased: \n* Per default neutral small UI differences for central auth and web of trust security (for example URL color) but do not make the one or the other more complicated to deal with by default (like extra dialog box or whatever).\n* User defineable UI schemes for different security models and different Root CAs (I bet quite some people will like to be able to configure the URL bar red in case they come across a Verisign certificate ;-). This will also help companies *alot* in order to customize browsers to their own internal web security guidelines.\n* And furthermore this is not only about browsers. Such a concept should also be considered for example at Email programs. There you also have the two security concepts (S/MIME vs. OpenPGP/MIME). And of course keyring applications like KGPG already show you a possibility of user customizable trust.\n"
    author: "Arnomane"
  - subject: "Re: \"Real life\" security would be better"
    date: 2007-06-20
    body: "Watch George Staikos's talk about browser security from Akademy last year. Web browser developers do know about the issues you are raising, but they aren't necessarily easy to solve."
    author: "Paul Eggleton"
  - subject: "Re: \"Real life\" security would be better"
    date: 2007-06-20
    body: "Here is a link to the video (193MB):\n\nhttp://home.kde.org/~akademy06/videos/aKademy06-Towards_A_More_Secure_Networked_Desktop_Environment_-_George_Staikos.ogg"
    author: "Paul Eggleton"
  - subject: "Will this get included in KDE 3.x?"
    date: 2007-06-21
    body: "It's great to see KDE involved in making decisions that will help shape the future of the Internet! I'm just wondering if there are any plans to incorporate this technology into KDE 3 or if we will have to wait for KDE 4 to be released. I know that 3.5.7 has been announced as the final update for KDE3, but will that mean Konqueror will be the last browser to support the new certificates or will the other browsers take just as long?"
    author: "Ian"
  - subject: "Explain Please"
    date: 2007-06-22
    body: "Can someone explain in brief what it means to a layman like me. I know how current SSL certificates work. What changes will the Extended Validation certificates make?"
    author: "anonymous"
  - subject: "Re: Explain Please"
    date: 2007-06-22
    body: "EV certificates aren't brand new technology, technically they are as secure as normal SSL certificates.\n\nThey biggest difference is that you can only get such a certificate after you have passed much more requirements than currently needed for obtaining signed SSL certificates from a central authority.\n\nSome technical differences are a special flag so that new browsers can distinguish EV certificates from other SSL certificates and some additional information embedded into the certificate.\n\nSee also: http://en.wikipedia.org/wiki/Extended_Validation_Certificate\n"
    author: "Arnomane"
---
The CA-Browser forum, a group of leading Certificate Authorities and web browser developers, has approved <a href="http://www.cabforum.org/">the first version of Extended Validation certificates</a> for use with web browser and other applications to certify a higher level of identity verification.  This is a great step forward for security and trust on the web, and KDE is proud to have been a part of the process from the beginning to the end.






<!--break-->
