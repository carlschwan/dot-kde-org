---
title: "Quickies: KDE e.V. Presidential Address, KHTML and WebKit, Qt4 Book, KDE4 on Mac Visuals"
date:    2007-07-24
authors:
  - "tunrau"
slug:    quickies-kde-ev-presidential-address-khtml-and-webkit-qt4-book-kde4-mac-visuals
comments:
  - subject: "KHTML and Webkit"
    date: 2007-07-23
    body: "Call me stupid, but I didn't really get the news about KHTML and Webkit.\n\nSooo...have they merged or what? When will KHTML get abandoned? If it won't, why not? Where do the SVN commits go to? You guys have your own Webkit tree?\n\nQuestions over questions..."
    author: "fish"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-23
    body: "Not a KHTML/webkit dev, but afaik several of the dev's are working with apple in the Webkit tree. Some things from KHTML are being ported over to Webkit. Most developers will abandon KHTML and work on Webkit, but it's a KDE 4.x thing I think. Of course, if Webkit becomes part of Qt 4.4, they can use that I guess.\n\nI said Most dev's will abandon KHTML, as last time I checked not all supported the move. This might have changed now."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-23
    body: "\"Sooo...have they merged or what?\"\n\nMany parts have merged.  Some things (like the article mentioned) have yet to merge and may not merge.\n\n\"When will KHTML get abandoned?\"\n\nMight be obvious, but when the last developer interested in it stops committing I guess.\n\n\"Where do the SVN commits go to?\"\n\nNew commits go here: http://webkit.org/coding/contributing.html\n\n\"You guys have your own Webkit tree?\"\n\nTrolltech and KDE developers have our own platform target for WebKit.  That platform target lives in WebKit SVN just like Apple's platform target and Nokia's platform target.  They are all actively developed in WebKit's svn.\n\nWhat has happened is that many KDE developers and Trolltech developers have decided to start working on WebKit proper.  Qt will be including WebKit in an upcoming release and there will be a KDE KPart which will allow Konqueror to use the very same WebKit rendering engine that the Safari browser uses."
    author: "anonymous"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "You must be super-smart, actually, since there are no news. \nNothing really has changed. You'll know when stuff is real \nwhen you see actual commits, done by actual KHTML developers.\nAnd, then, you'll know what's being attempted, not if it will \nwork out or not.\n\n-Maks\n"
    author: "SadEagle"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "\"...actual commits, done by actual KHTML developers...\"\n\nhttp://trac.webkit.org/projects/webkit/search?q=%40kde.org&changeset=on\nhttp://trac.webkit.org/projects/webkit/search?changeset=on&q=%40kde.org&page=2\nhttp://trac.webkit.org/projects/webkit/search?changeset=on&q=%40kde.org&page=3\nhttp://trac.webkit.org/projects/webkit/search?changeset=on&q=%40kde.org&page=4\nhttp://trac.webkit.org/projects/webkit/search?changeset=on&q=%40kde.org&page=5\nhttp://trac.webkit.org/projects/webkit/search?changeset=on&q=%40kde.org&page=6\nhttp://trac.webkit.org/projects/webkit/search?changeset=on&q=%40kde.org&page=7\nhttp://trac.webkit.org/projects/webkit/search?changeset=on&q=%40kde.org&page=8\n[...]\nhttp://trac.webkit.org/projects/webkit/search?changeset=on&q=%40kde.org&page=14\n\nDon't these count?\n"
    author: "manyoso"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "Did any of these people contribute anything to KHTML in the last 3 years or so? No (well, Rob and Niko did KSVG2 stuff, and George did stuff relevant to... merging with Apple). So how would they count? And even more so, one has to consider that a lot of them are done as subject to commercial contracts, and, as such, should IMHO not even be done with @kde.org addresses.\n\n"
    author: "SadEagle"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "... Besides, I was referring to commits in to KDE SVN, not some 3rd-party SVN."
    author: "SadEagle"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "So, what's your point? I mean, what do you think from your dev pov it's going to happen with the webkit/khtml topic? And what do you hope? You seem quite on the  \"don't touch our KHTML!\" side, am I wrong?"
    author: "Vide"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "My point is that there are 2 things that have actually happened:\n\n1. Some corporations and contractors somewhat involved in KDE, such as \nTrollTech, have interest in WebKit. Some of people working for them, \nwho have never been involved in KHTML development or have not been \ninvolved in it for many years have committed to WebKit. Now, I am sure \nthose people do think their actions also help KDE, but they also have not \nbeen the ones making thousands of changes over the last few years,\nor the ones trying to setup a decent and fair working relationship.\n\n2. Some of those people got together with some of the KHTML contributors \nat aKademy, and tried to convince them to abandon KHTML for the fork. \nAs a result of discussions, there were some ideas for a more sensible \npath that were proposed. There are hence some things that will be tried.\nMay be they'll work out, may be they will not. At any rate, any announcement \nis premature, and an announcement coming from people not involved in \nactual development is simply inappropriate.\n\nSo the short answer is that stuff might happen, and if it does, I am sure\nyou'll see it in the commit digest. Before that, it's all speculation.\n\nAnd as my side... I would love to work with Apple (and in fact we do\ncooperate with them very -losely- on KJS, especially since their JSC \ndevelopers have shown a lot more concern for our technical needs than their\n HTML ones). On the other hand, I have no interest in working (unpaid) for\n Apple; nor do I really have the time for diplomacy and grandiose projects.\nSo, well... I would really appreciate if the people who are all jumpy about\nWebKit, would, you know, actually help sort the difficult issues out and not\njust go around telling everyone how we should/will be using it. There are\nfinally signs that that may be happening, though. "
    author: "SadEagle"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "Lars created KHTML, but because he hasn't worked in KDE tree recently he no longer counts... ok."
    author: "manyoso"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "Developers join the KDE project and leave it again all the time. Only ones that that count are those currently willing to invest some of their time. Not much room for nostalgia here.\n\nI won't even mention has-KDE4-compiled-and-running as a criteria as that would rule out far too many ;) "
    author: "Harri"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-25
    body: "i think it's disingenuous to rule someone out because they start contributing again but to a related project that is still qt/kde related. in my books, working on a qt port and a kde kpart for webkit is worthy of not being dismissed.\n\ni also think it's pretty unfair to those who are certainly active kde developers working on this stuff to ignore them in these dismissals. finally, to paint those have managed to find a way to support themselves working on things close to their interests as somehow innately untrustworthy because of it is pretty lame.\n\ni'd also suggest that khtml and qtwebkit are in need of soft skills that many of the developers involved lack. chasing away those who can bring those to the table, particularly after they were invited in the first place, is not what i'd call big picture thinking. =)\n\ni think that pretty much sums up what i think of the dismissive and generally negative attitudes some cast upon the idea of qtwebkit."
    author: "Aaron Seigo"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-28
    body: "Maybe the negatives come from remembering how much fun it was to work on khtml before Apple did what they did.\n\nI actually wrote a blog titled \"How to Destroy a Free Software Project\" based on the khtml saga. I deleted it to not hurt the people involved. I really don't want to discourage anyone. This is a mess, years have been lost, and the choices are between bad and worse. I guess the most difficult is coming to grips with the idea that a fundamental building block of the KDE desktop is now under someone else' control, someone who's interests are that KDE not exist.\n\nI feel strongly that KDE has left itself wide open to irrelevance by thinking in this instance product not process. KDE wouldn't exist but for the process. I acknowledge my own contribution when I excitedly highlighted the patches coming from the safari codebase. It became clear that I was misled when the khtml developers either quit or only stayed on due to massive intestinal fortitude.\n\nDidn't Webkit as a process arise from Nokia and others wanting to work with Apple? (Gee, that made the IPhone browser alot easier didn't it) I suspect that they insisted on an appropriate process before getting involved. Somehow we (KDE) just went along. I think we got suckered. Back 3,4 years ago it should have been recognized for what it was, a hostile fork.\n\nDerek"
    author: "D Kite"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-29
    body: "You know, all (trademarked term) what happened, was that fame was being taken away from individual developers for their code. Like when they passed ACID test already, and KHTML didn't yet, and everybody said \"why don't you just take it from Apple\".\n\nI can imagine this has hurt those developers and deprived them of their motivation to work on that specific project. But I am at the same time very glad, that the influx of manpower into the rendering engine has happened and the potential has not been waisted.\n\nIf you or anybody envy Apple or Nokia or whoever for earning money from the LGPL code, I am afraid, you didn't understand why the LGPL type of license is chosen by KDE in the first place.\n\nAnd now it's happy end. A consortium of companies will in cooperation maintain the web rendering engine and make it the best suited one. They have different toolkits for which they implement backends (QT -> KDE from Trolltech, GTK from Nokia, Cocoa(?) from Apple) and that will push the quality of the abstraction to a new level.\n\nAnd KDE simply can stop working on it itself, for all boring stuff, and concentrate on e.g. Plasma. And how is that not a great thing please? You can't even start to imagine how much more me Konqueror-Browser user loves to see Epiphany use Webkit (well possible now), instead of Konqueror-Browser using Gecko (still near impossible to be done right).\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: KHTML and Webkit"
    date: 2007-09-09
    body: "It's still free software, you can always *fork back* anyway."
    author: "J. Pablo Fernandez"
  - subject: "Re: KHTML and Webkit"
    date: 2007-07-24
    body: "This is open source; focused development is rare :)  There are people putting effort in the better fork and there are people still afraid of change that are working on KHTML original.\n\nIts only fair to point this out, and its not hard to see that the most compliant and most developed-in tree will naturally be picked up by distros and end users.\n\nThat's the situation (as the article already said).  You make your own conclusions :-)"
    author: "me"
  - subject: "webkit and new license"
    date: 2007-07-23
    body: "Hi,\nI'll be so happy to see this unforking but an eventual passage of kde to gpl3 are ok for webkit (or viceversa)?\nThanks\n"
    author: "vincent"
  - subject: "Re: webkit and new license"
    date: 2007-07-24
    body: "the GPLv3 discussion is being had on the kde licensing mailing list. before that has come to a conclusion, it's premature to take a stand on GPLv3 and KDE one way or the other."
    author: "Aaron Seigo"
  - subject: "Re: webkit and new license"
    date: 2007-07-24
    body: "It's irrelevant: KHTML and WebKit is LGPL v2+"
    author: "Allan Sandfeld"
  - subject: "Re: webkit and new license"
    date: 2007-07-24
    body: "Actually 2.1+, since it's the license used in some Apple-originated files \n(render_layer.cpp and arena.cpp)\n"
    author: "SadEagle"
  - subject: "WYSIWYG"
    date: 2007-07-24
    body: "WYSIWYG Editing is one of those things that is really missing in KDE.\nWebkit has this ability, is this merged into KHTML?\nWill we at last see TinyMCE, FCKEditor and other editors running in Konqueror?"
    author: "Emil Sedgh"
  - subject: "Re: WYSIWYG"
    date: 2007-07-24
    body: "it's in webkit, though QtWebKit doesn't implement the widgets for it yet so you still can't see them in QtWebKit. the underlying bits are there, however. so this would be one of the advantages that would come with a completed QtWebKit."
    author: "Aaron Seigo"
  - subject: "Re: WYSIWYG"
    date: 2007-07-29
    body: "Will QT be able to base its old HTML implementation on QtWebKit, BTW?\n\nAnd if QT 4.4 contains QtWebKit, probably KDE 4.0.0 should require it, so this sort of cleanup can happen as soon as possible?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Any news about kdewebdev?"
    date: 2007-07-24
    body: "Not sure if my question is relevant to this topic. I am a daily user of quanta plus. I didn't see any news about quanta plus been ported to qt4 or is there any features planned in quanta plus for KDE 4. In Linux we miss a good HTML editor like dreamweaver. The only better solution I think available to Linux user is quanta plus. I would like to know the status of this cool software. Thanks  "
    author: "Swaroop"
  - subject: "Re: Any news about kdewebdev?"
    date: 2007-07-24
    body: "I think the development stuff (esp. kdewebdev) is targetting KDE 4.1."
    author: "Alex Merry"
  - subject: "Re: Any news about kdewebdev?"
    date: 2007-07-24
    body: "In fact, after a quick build of kdevplatform and kwebdev, I can tell you that Quanta... exists.  And runs.  But doesn't produce much in the way of a useful interface when it does run.\n\nBut, like I said, I believe the intention is to release it with 4.1."
    author: "Alex Merry"
  - subject: "Re: Any news about kdewebdev?"
    date: 2007-07-24
    body: "Yes, the plan is to have it ready around 4.1."
    author: "Andras Mantia"
  - subject: "Re: Any news about kdewebdev?"
    date: 2007-07-24
    body: "There have been a number of issues with kdewebdev. For one, using a common framework with KDevelop has meant we need that in place, and we've had to wait for KDE libraries to stabilize... Then there are the obvious questions about whether we will have an adequate framework for visual development in place, which has long been a question of what is happening with Webkit. In addition to this I have been buried in taking my business from seasonal swings to a dynamic expansion so I can allocate effort without jumping up to put out fires.\n\nWhile we do have some volunteer development most of our development is sponsored. Currently we have no developer working on VPL (a DTD accurate WYSIWYG alternative) and have totally burned out two volunteer developers due to the sheer difficulty of the task. The irony is that we have a huge user base and maybe two dozen sponsors. 50-100 sponsors at \"lunch money\" level contributions would empower us to have several developers full time on this. The efficiency is orders of magnitude higher when a developer works without interruption. I encourage our supporters to go to http://kdewebdev.org/donate.php."
    author: "Eric Laffoon"
  - subject: "Qt 4 Book"
    date: 2007-07-24
    body: "Congratulations to Daniel for another Qt 4 book, this time in English.\nI believe Johan Thelin also has a book, Foundations of Qt Development, which he said was going to get printed last week.\n\nUnfortunately I will have to wait for a year before these books will be available locally, which is how long it took for two other Qt 4 books (C++ GUI programming and Introduction to Design Patterns) to be available. Hopefully I'll get my copy next month. Yay!\n\n... now to wait for a KDE 4 Development book... :)"
    author: "Jucato"
  - subject: "no need to agree"
    date: 2007-07-24
    body: "The Mac OS X installer asks the user to agree to the GPL license. There is no need for the user to agree to the GPL license. It only grants you rights and takes none away from you.\n\nhttp://www.simplehelp.net/images/kdeosx/kdeosx5a.jpg"
    author: "Jos"
  - subject: "Re: no need to agree"
    date: 2007-07-24
    body: "The GPL it's a license like any other one, and like any other one you can disagree with it and refuse it (I'm just saying you can, not that you should do that ;) ). And like any other license you have to be aware of it, so yes, the installer question about the license is needed."
    author: "Pino Toscano"
  - subject: "Re: no need to agree"
    date: 2007-07-24
    body: "No it's not, and refusing the GPL means you are not allowed to distribute the application. It has no effect on using it."
    author: "Allan Sandfeld"
  - subject: "Re: no need to agree"
    date: 2007-07-24
    body: "I'm not a lawyer but I suspect that almost every EULA out there is about distributing (well, about NOT distributing ;)  the software you're installing."
    author: "Vide"
  - subject: "Re: no need to agree"
    date: 2007-07-24
    body: "No, that's not the case. EULAs typically also contain language on how you are allowed to use the software. E.g. the BitKeeper license contains a clause saying that you may not reverse engineer the code. There are also frequently clauses saying that you are not even allowed to modify the program files on your own computer. The GPL has none of these restrictions; there are no limitations at all on how you may _use_ the software.\n\n\n"
    author: "Jos"
  - subject: "Re: no need to agree"
    date: 2007-07-24
    body: "There are some details you are mixing up;\naccording to the GPL text; as soon as you use the software you are bound by the license (you accept the license as soon as you use it, or in other words, if you don't accept it you can't use it)\n\nThe part I guess you are confusing it with is changes you make to it have to be made public, but only if you actually distribute your version.  But this distribution thing has nothing to do with usage and acceptance of the license."
    author: "Thomas Zander"
  - subject: "Re: no need to agree"
    date: 2007-07-24
    body: "That's not true, in fact it explicitly says the opposite!\n\nFrom version 2:\n5. You are not required to accept this License, since you have not signed it. However, nothing else grants you permission to modify or distribute the Program or its derivative works. These actions are prohibited by law if you do not accept this License. Therefore, by modifying or distributing the Program (or any work based on the Program), you indicate your acceptance of this License to do so, and all its terms and conditions for copying, distributing or modifying the Program or works based on it.\n\nVersion 3 makes this even more clear:\n9. Acceptance Not Required for Having Copies.\nYou are not required to accept this License in order to receive or run a copy of the Program. Ancillary propagation of a covered work occurring solely as a consequence of using peer-to-peer transmission to receive a copy likewise does not require acceptance. However, nothing other than this License grants you permission to propagate or modify any covered work. These actions infringe copyright if you do not accept this License. Therefore, by modifying or propagating a covered work, you indicate your acceptance of this License to do so."
    author: "Kevin Kofler"
  - subject: "Re: no need to agree"
    date: 2007-07-26
    body: "> according to the GPL text; as soon as you use the software you are bound by the license\n\nAs Kevin already pointed out, this is the exact opposite of the truth.\n\n> if you don't accept it you can't use it\n\nIn the USA, even if a license explicitly forbade you from using it in any circumstances at all, you would still have the right to use it, because copyright does not give somebody the right to stop you.  Copying software to your hard drive during installation and copying software into memory in order to execute it are not covered by copyright."
    author: "Jim"
  - subject: "Re: no need to agree"
    date: 2007-07-25
    body: "Not sure I got your explanation, but your statement is true.\nGPL is a copyright license -- it applies to distribution -- there is no need to force the user to agree with it for the purpose of installing, using, or modifying the software -- he is only liable on redistribution (so just stick a COPYING file with the package). EULAs are shrink-wrap contracts, they have usage clauses -- the user can be liable for unauthorized usage -- that's why it's important to have the user to acknowledge it and accept the risk."
    author: "blacksheep"
  - subject: "Re: no need to agree"
    date: 2007-07-25
    body: "At least you must agree to the \"NO WARRANTY\" section (11. and 12. of GPL V2).\n\nIn case I'm a private person offering something on the Internet, everyone should be clear right from the beginning that I'm not giving any WARRANTY.\n\nBut in case companies like Apple offer something on the Internet, I at least can have the impression that it doesn't ruin my computer, deletes file, burn the neighbours dog or start another worldwar.\n\nSuch warranty is not given here and I understand that Apple or any other company needs to have this signed."
    author: "Philipp"
  - subject: "Re: no need to agree"
    date: 2007-07-25
    body: "Where's the legal backing for what you are saying?\n\nIf you do want to state that, just state it. Say \"KDE libraries will be installed. Bla, bla, bla. This software comes with no warranty. [Next]\" That is not a condition to use the software, so it's stupid to force the user to accept it.\n\nAnyway, this thread is about having the GPL to be accepted as an EULA contract."
    author: "fast penguin"
  - subject: "Re: no need to agree"
    date: 2007-07-25
    body: "From the GPL FAQ at http://www.gnu.org/licenses/gpl-faq.html#ClickThrough\n\n\"Q: Can software installers ask people to click to agree to the GPL? If I get some software under the GPL, do I have to agree to anything?\n\n\"A: Some software packaging systems have a place which requires you to click through or otherwise indicate assent to the terms of the GPL. This is neither required nor forbidden. With or without a click through, the GPL's rules remain the same. \n\"Merely agreeing to the GPL doesn't place any obligations on you. You are not required to agree to anything to merely use software which is licensed under the GPL. You only have obligations if you modify or distribute the software. If it really bothers you to click through the GPL, nothing stops you from hacking the GPLed software to bypass this.\""
    author: "Divide"
  - subject: "KHTML is in Windows"
    date: 2007-07-24
    body: "KHTML / Konqueror rules. It feels so much better already on Linux than Firefox. Maybe Webkit will give Linux the speed of KHTML, with the reliability of Firefox.\n\nI just got KHTML running on Windows:\nhttp://gerte.nietbelangrijk.nl/pic/khtml_win.png\nMaybe I'm a pure Webkit / KHTML user in a couple of months ;)"
    author: "Irondog"
  - subject: "Re: KHTML is in Windows"
    date: 2007-07-24
    body: "Awesome! I didn't know it was already this far along.  I guess now that I managed to switch to Linux for most of my time at work, it's no longer so important, but very cool nonetheless!"
    author: "Leo S"
  - subject: "Whats the big difference between Webkit and KHTML?"
    date: 2007-07-24
    body: "I know that Webkit is still similar to khtml, but I don't know whats happened besides Apple taking qt rendering out of it. I am excited to see four large groups (Trolltech, Apple, Nokia, and KDE) working on a single layout engine. While I don't think its really fair that Apple secretly forked the components that make up Webkit (khtml, kjs, ksvg I think). It would be a good thing for KDE to jump in the boat with everybody else. Such an amount of collaboration could really help create an excellent engine for unix-like desktops. If I'm not on Linux I use Firefox, but Konqueror will always be my favorite browser."
    author: "Eric"
  - subject: "Quickie?"
    date: 2007-07-24
    body: "It might just be me, but I think the \"We are going to WebKit\" news deserves it's own story, not a minor mention in a quickies summary."
    author: "Tim"
  - subject: "Re: Quickie?"
    date: 2007-07-24
    body: "As I said before, it deserves neither, as it's woefully inaccurate.\n"
    author: "SadEagle"
  - subject: "Re: Quickie?"
    date: 2007-07-24
    body: "I couldn't agree more and I had the very same feeling when I read that article (therefore my question at the very beginning) - this news is no news at all and should never have been made public. I will only result in ever more rumours about KHTML and Webkit...sigh."
    author: "fish"
  - subject: "Re: Quickie?"
    date: 2007-07-25
    body: "you saying its inaccurate doesn't magically make it inaccurate. you may not agree with it, you may not like it ... but it's a whole lot more accurate than you seem to be willing to accept. perhaps that's something to think about."
    author: "Aaron Seigo"
  - subject: "Re: Quickie?"
    date: 2007-07-25
    body: "Aaron, please... The khtml and kjs people who were \nin Glasgow talked to me, asked for my opinion \n(which is rather laissez-faire), and told me \nwhat their plans are and aren't. I know that if\nthey wanted to make an announcement, and if there \nwas something worth making an announcement about, \nthey would. Things aren't as simple as \"KDE will \nuse WebKit\". It's true that there is a renewed interest\nin merging things, but that has happened about a zillion\ntimes before. If something happens, the commits will \nshow. \n\nAnd, frankly, I am getting tired of you spreading disinformation \nabout the direction of KHTML. You keep making all those \ndefinite statements that seem to have at best a weak \nbasis in reality. I am afraid I can not interpret it as anything\nother than an attempt to influence the direction of a project you have not\ncontributed to, which goes strongly against KDE principles. \n(\"He who does the work decides\")\n\nIt's pretty clear, for example, that the article linked above was \nnot vetted by anyone involved in KHTML development, as the original \nversion contained clearly wrong technical claims, which were only corrected \nafter they were noticed by an another KDE contributor.\n"
    author: "SadEagle"
  - subject: "Re: Quickie?"
    date: 2007-07-25
    body: "> You keep making all those \n> definite statements that seem to have at best a weak \n> basis in reality\n\nhere's what i know:\n\n- people (some with and some without a profit motive, but all with a pro-kde viewpoint in life) are working on qtwebkit and a kpart for it\n- there is both community participation as well as financial investment going into this\n- the goal is to provide webkit as an alternative to khtml\n- when that is achieved the two code bases will compete on their merits\n- if feature and bug compat with other software products using webkit is maintained (meaning a larger user base, among other things), it's obvious which one will get picked up by those integrating kde software into their own products (think linux distros, third party software developers)\n- in the meantime, khtml has been decreasing in user support\n- having a larger community of developers would be a great thing, and webkit will be able to move ever quicker forward for it (meaning: we benefit from better tech and those working on it from kde probably will have a more rewarding time of it)\n- progress has been and is being made on the \"apple cooperation\" front, and at an increasing pace\n- there are those who want more recognition for the role kde developers played in khtml both historically as well as currently, yet some of these same people do their best to torpedo attempts to rectify that\n\nthe above is precisely what i've been saying. when someone asks me \"has a decision been made?\" or \"is webkit replacing khtml?\" i tell them honestly: it's not been decided yet and there's much to happen before that can happen. you can look over irc logs from yesterday in #kde-devel if you want to confirm that. \n\nnow i'm not sure which part over the points aboveyou feel are inaccurate, but to me it seems fairly obvious that if qtwebkit is successful, it will be a good thing (you seem to agree on that) and our users will pick qtwebkit over khtml if they are even remotely comparable. there is serious commitment to making that happen and huge strides forward. the most serious threat to success is stop energy from within.\n\nso ... why am i involved in this at all given that i am not a major contributor to khtml and that it allows me to deal with wonderfully negative retorts? because i was asked to by people who are involved with khtml and webkit. i accepted because this promises to have a huge impact on kde as a whole, something i happen to have a personal interest in. it's not lost on me that the campaign for silence on these matters is a great way to undermine those efforts. you can accuse me of whatever you wish, but i know where i stand, what is actually gong on and i will be sticking to that.\n\nif there are people wish work on khtml as a separate code base from here to eternity, that's obviously perfectly fine; it also doesn't change what happens with webkit. but denying the existence and implications of qtwebkit and the people who are putting their time and effort into it (which happens to include people who did small things like, you know, start the khtml project in the first place) is not what i'd describe as charitable."
    author: "Aaron J. Seigo"
  - subject: "Re: Quickie?"
    date: 2007-07-29
    body: "And I take, as a KDE President now, you are now entitled to express your view on KDE chose as its direction.\n\n"
    author: "Debian User"
  - subject: "Re: Quickie?"
    date: 2007-07-29
    body: "As Aaron has already stated, he's not \"KDE president\". He's president of the KDE e.v., which is not the same thing. See http://aseigo.blogspot.com/2007/07/presidential-address.html"
    author: "Paul Eggleton"
  - subject: "Re: Quickie?"
    date: 2007-07-30
    body: "Can we - please - have fun and call it \"KDE president\" ?!"
    author: "Debian User"
  - subject: "Re: Quickie?"
    date: 2007-07-25
    body: "> that the article linked above was \n> not vetted by anyone involved in KHTML development\n\nbtw, the easiest way to fix that is to not be insular but to engage. troy was approached by someone else in the qtwebkit project (who isn't paid by anyone to work on it, btw) with this story idea (love how i get to take the flak anyways ;). due to the silent nature of the khtml contributors, i don't think troy really knew who to ask. realizing that troy has been around the kde community for quite a while, it underlines the need for improvement there.  those he did send email to didn't respond in a timely fashion. how are we supposed to get the kde contributions and contributors recognition when they remain ghosts?\n\ntroy has, however, followed up since with those who did poke their heads up due to his article. and that's a good thing: maybe we'll actually get to hear more of *your* voices directly rather than having to deal with silence mixed with concern, confusion and quiet discontent around tables or backwater irc channels, modulo the occasional web board dissent."
    author: "Aaron J. Seigo"
  - subject: "Re: Quickie?"
    date: 2007-07-26
    body: ">i don't think troy really knew who to ask. realizing that troy has been around the kde >community for quite a while, it underlines the need for improvement there. those he did >send email to didn't respond in a timely fashion. how are we supposed to get the kde >contributions and contributors recognition when they remain ghosts?\n\nwell since you asked for ideas, how about adding a list of key people to the relivent part of Techbase? So next time troy wants to send an E-mail he could go to KHTML's techbase page and see a list of who to send it to. \n\nIt won't magically make the more quiet but deserving members of KDE stars overnight, but it makes it easier for people praising software to move up a step to praising the author by name."
    author: "Ben"
  - subject: "Re: Quickie?"
    date: 2007-07-26
    body: "that's an excellent idea, imho."
    author: "Aaron Seigo"
  - subject: "KOffice for Mac"
    date: 2007-07-25
    body: "So when is KOffice coming for Mac?\nWill it be available with KOffice 2, since it is based on Qt4?"
    author: "Acke"
  - subject: "Re: KOffice for Mac"
    date: 2007-07-25
    body: "(Unofficial) builds of KOffice for OS X have been available for years now."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice for Mac"
    date: 2007-07-25
    body: "Oh, and just to be clear about it: yes, KOffice 2.0 alpha packages are available, too, in the same way the tutorial describes, from the same place."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice for Mac"
    date: 2007-07-26
    body: "Thank you all who work on this for KDE and make it available on the Mac."
    author: "sc"
---
A number of KDE related news stories are floating about the interweb today, so here's a quick round-up.  Aaron Seigo writes his <a href="http://aseigo.blogspot.com/2007/07/presidential-address.html">KDE e.V. Presidential Address</a> on his blog in an effort to force the e.V. to be more transparent about their activities.  Over at Ars Technica, I have an article talking about the <a href="http://arstechnica.com/journals/linux.ars/2007/07/23/the-unforking-of-kdes-khtml-and-webkit">future of KHTML and WebKit</a>: you'll be happy to know that this seems to no longer be a real problem.  Daniel Molkentin has published a new book on coding for Qt 4.x which is now available for ordering at <a href="http://qt4-book.com/">qt4-book.com</a>.  Lastly, I've stumbled across a <a href="http://www.simplehelp.net/2007/07/22/how-to-install-kde-4-in-os-x/">short visual tutorial</a> for those Mac OS X users among us that are looking to help test the KDE/Mac snapshots.  Of course you can always go over to <a href="http://techbase.kde.org">TechBase</a> for the build instructions if you have some CPU cycles to spare.







<!--break-->
