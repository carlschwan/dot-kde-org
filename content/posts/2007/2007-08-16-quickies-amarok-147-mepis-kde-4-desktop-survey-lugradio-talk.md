---
title: "Quickies: Amarok 1.4.7, MEPIS KDE 4, Desktop Survey, Lugradio Talk"
date:    2007-08-16
authors:
  - "jriddell"
slug:    quickies-amarok-147-mepis-kde-4-desktop-survey-lugradio-talk
comments:
  - subject: "Biasing Survey Results"
    date: 2007-08-16
    body: "From the desktop survey:\n\n\"NOTE: Participants are asked to refrain from promoting or advertising the survey to mailing lists, or encouraging friends or co-workers to vote for specific software choices. This will ensure that the survey represents a broad sample of Linux desktop users rather than being used to advance a particular open-source software cause.\"\n\nBy posting a link on dot.kde.org, you are violating the spirit of the above request, and biasing the results in favour of the KDE project.\n\nThe PCLOS, Mandriva and Ubuntu forums have all had threads advertising the survey, and all have respected the wishes of the survey creators by deleting these threads.  Please do the right thing and remove the link and all mention of the survey.\n\nMany thanks in advance."
    author: "Anon"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "The page; http://www.desktoplinux.com/news/NS7943272425.html has a link to post the story on dig.  They *are* looking for more publicity.  Asking the dot to remove it sounds quite hypocritical with that fact in mind."
    author: "Thomas Zander"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "While digg is a general news site, a posting on the dot is more like a request to vote for KDE.\nWell i already voted anyway."
    author: "Frank"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "And I don't know what digg is and I'm not interested in. I'm happy that it was posted here because I wouldn't know that survey else."
    author: "tom"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "Digg has no affiliation towards any particular software project, though, so the exposure from Digg will give them useful results.  The same would hold for, say, slashdot if it hit their front page.\n\nThe exposure from dot.kde.org, however, which is obviously strongly affiliated with the KDE project, will taint the results, just as exposure from the PCLOS, Ubuntu, Mandriva forums (or from, say, gnome.org) would taint the results by over-representing their affiliated projects.  The three aforementioned forums recognised this and removed the link of their own volition.\n\nThere's no hypocrisy at all, whatsoever, here: the survey creators want a large set of results from open-source software users, but they want them to be *neutral* with regard to particular open source projects."
    author: "Anon"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "Digg has no affiliation toward any particular project ? that's why EVERY KDE NEWS ever posted on digg is FULL of flame against KDE. No criticism, pure flaming only. There's no arguments. Just insults and spitting. \n\n"
    author: "Anonymous Coward"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-18
    body: "\"The same would hold for, say, slashdot if it hit their front page.\"\n\nJust as the other poster to your comment stated about digg, I would have to say that slashdot is definitely anti-KDE biased.\n\nIf it has more buttons than GNOME, its too complicated and they hate it. Just like they hate MS compared to Apple. They think that everything has to remove all buttons and we should all move to buttonless interfaces.\n\nSeriously. Go read slashdot sometime and you will see for yourself.\n\n"
    author: "Forest"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "I don't see how simply telling a group of people (dot.kde.org readers) that the desktop survey is running is biasing the results.\n\nI, for one, didn't know the survey was on until reading about it here. I would also have submitted the exact same results regardless of where I had read about it."
    author: "John"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "\"I don't see how simply telling a group of people (dot.kde.org readers) that the desktop survey is running is biasing the results.\n \n I, for one, didn't know the survey was on until reading about it here. I would also have submitted the exact same results regardless of where I had read about it.\"\n\nThe logic is so simple that I'm astounded you cannot grasp it, but here it is, using your post as an example: because of the fact that dot.kde.org posted the survey here, KDE got one more vote (from you) than it would have done had it not been posted.  Now multiply this my the readership of dot.kde.org.  Even if you personally (contra to my assumptions) didn't vote for KDE, we can assume that a large majority of the readers who would not otherwise have voted at all if the link hadn't been posted here have now voted for KDE, biasing the results."
    author: "anonymous"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "Well, it doesn't prove in the slightest that having so-called neutral sites like Digg advertising the Survey and not the dot will bring you results closer to reality, since you don't have the faintest idea of the general profile of Digg readers.\nI'm also of the type of people who read only the dot and linuxfr.org, as far as OpenSource is concerned. I probably wouldn't have heard of this survey otherwise. Is my vote less interesting or valuable than any other Digg reader?\nMaybe the results will be biased because most KDE users comes to the dot, while XFCE users got to Digg or some other so-called \"neutral\" sites. You have no way to know, but you assume that it is and you will claim that these results are more representative. And that's just obfuscation, in my opinion.\nWhy not just admit that this kind of internet survey is basically worthless, instead of trying to give it any kind of credibility?"
    author: "Richard Van Den Boom"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "\"Maybe the results will be biased because most KDE users comes to the dot, while XFCE users got to Digg or some other so-called \"neutral\" sites. You have no way to know, but you assume that it is and you will claim that these results are more representative. And that's just obfuscation, in my opinion.\"\n\nAre you seriously trying to suggest that it's even *plausible* that the percentage of people who use KDE will not be higher amongst readers of dot.kde.org than readers of general news sites?\nI'd bet anything you'd care to wager that there's a higher percentage of KDE users here than reading digg."
    author: "Waywocket"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "No, what I say is that you have absolutely no idea if general news site is in anyway representative of general Linux usage as a desktop. Nor Inquirer, nor Slashdot. So limiting the advertising to these sites is also biasing the results.\nIn general, limiting the voters to a certain category (in this case the ones that read general news site, which is not the case of many here) will bias probably as much if not more than what this limitation is supposed to avoid."
    author: "Richard Van Den Boom"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "But are slashdot etc a _closer_ representative of general linux usage than dot.kde.org ?   Of course they are.\n\n\nI second this request to remove the link."
    author: "John Tapsell"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "You have no way to prove the \"of course\". Maybe no KDE user goes to slashdot because they come here for news. That would bias, if true, results **against** KDE. Claiming that the dot is KDE biased (I don't argue that) is not an argument to claim Slashdot is not.\nCan't you see that assuming big news site are representative of anything is already biasing results much more than letting all sites advertise this Survey?\nThat basically says : \"I say Slashdot readers are representative of the average Linux user. So I ask them what they use and then I claim the results are representative of all Linux users\". You're making an assumption, extract results from it then claim the results prove your assumpion.\nTalk about a snake biting its own tail.\n\nEvery site (Slashdot, Digg, Dot.KDE, Gnomewhatever, XFCEwhatever) should be allowed to advertise this. That's the closest you can get to unbiased, IMO."
    author: "Richard Van Den Boom"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "Well, KDE are losing to GNOME by a comfortable margin now, so I guess posting it here has had negligible effects, after all.  May as well just leave it up, I guess."
    author: "anon"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "Well, that's due to some script kiddie manipulating the poll.\nYesterday the poll was at about 23.000 votes with openSUSE at about 9.5 %. Now SUSE votes have increased to 16.5 percent. Do your math and you'll see that by a large margin most of the votes submitted since then must be openSUSE-Gnome votes. This is highly unrealistic given the fact that according to more scientific surveys done by SUSE KDE usage among openSUSE users is at about 75%."
    author: "Skewed"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "Indeed, there's a significant, improbably shift after 9000 more votes quite late in the poll. Since I also had a look yesterday, I can tell you the voted configuration of the vote manipulator:\nopenSUSE (from ~9,5 to 16,5)\nGnome (from ~37,5 to 42,5)\nFirefox or maybe none selected (from ~54/55 to 58,5)\nEvolution (from 14 to 21,5)\nNone -- I don't run Windows apps on Linux (from ~22 to 30)\n\nIf DektopLinux is smart and has a look at their logs (and the voter was dumb), they can deduct the forged votes.\nWhatever it turns out, you can trust the accuracy of these votes only in a limited way. The results are skewed not only in a way which subset of a group can be reached (e.g. emailing - used app), but also which general kind of persons find their way to such a poll."
    author: "Phase II"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "You make a compelling case why dot.kde, Ubuntu, Gnome and others *should* post these links. \n\nIf you have one kde user who reads about it here and goes to vote kde, that's one kde closer to the target of perfect accuracy (everybody votes, honestly). "
    author: "Ben"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "In statistics it's called Sampling Bias. To ensure the accuracy of a survey of a population, the people taking the survey must be randomly chosen from the entire population. The survey is analyzing the a population composed of all free software users, so they can't sample primarily from one subset of the population.\n\nFor example, if you wanted to determine the rates of teen sexuality, then taking the poll in a teen pregnancy centre would put a significant bias on the results.\n\nHere's a mathematical example: Say you have a population composed of\n15% XFCE users\n40% KDE users\n45% Gnome users\n\nYou survey the population to try and find out that distribution. A certain percentage of each group fills out the survey. (XFCE had more people know about it, because it was posted on their mailing list, but not Gnome or KDE)\n80%  of XFCE users\n30% of KDE users\n28% of gnome users\n\nYour final analysis would result in a population distribution of:\n33.8% XFCE\n33.8% KDE\n32.7% GNOME (Yes, there's a rounding error)\n\nAs you can see, the results of the survey are nothing like the actual population distribution."
    author: "Soap"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "I am a strong Gnome supporter and I read KDE dot news. The Linux Desktop is a common plattform these days."
    author: "bert"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "Indeed, I read the Gnome planet. BTW is there a gnome news site like the dot??? Can't find any..."
    author: "jospoortvliet"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "There's http://www.gnomedesktop.org/ ... \n\n"
    author: "cm"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "aah, yes, I found that one, but it's pretty low trafic. Not bad thing of course... At least the information is of good quality and actually about gnome, unlike many of the blogs on planet gnome."
    author: "jospoortvliet"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "Here you go:\n\n\nhttp://planet.gnome.org/news/"
    author: "David"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "it very interesting to read about sawfish maintainer resign.\n\n1. he said that sawfish is finished\n2. he said so because the code become hard to maintain"
    author: "nick"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "thank you!"
    author: "jospoortvliet"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "Maybe, but I'd say the majority of people that read KDE dot news would vote for KDE, which makes linking to the survey here skew the results (unless an identical link is made on a similarly-popular Gnome news site, provided there is such)"
    author: "LiquidFire"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "Might as well give up trying to balance it.  The results of this survey will be wildly inaccurate anyway, no matter what you try to do to control where the survey is posted.  With internet surveys like this, you have to realize that the results for each category probably have an uncertainty of +/- 20% or so.  So unless the results really show overwhelming preference towards one choice over the other, you can't come to any conclusions from the data."
    author: "Leo S"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "Indeed. the bias is already in who read the news at all, who are interested etc. It measures more the size of the community, I'd say."
    author: "jospoortvliet"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "You've clearly failed to prevent it being posted to *any* of the desktop-specific websites so why not go for the obvious alternative and post it to *all* of them?  Actually I think that would have been a better idea from the start."
    author: "Adrian Baugh"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "Yes, but would there be any disadvantage when the result would be biased? Or even, does the result of the survey mattars anything at all? \n\nI don't think so."
    author: "kde user"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-16
    body: "It clearly matters to the people who created it in the first place, and it clearly matters to JR who posted it here."
    author: "anonymous"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-17
    body: "as a Debian user, I found out about the survey on blog.thedebianuser.org, and I subsequently selected Debian as the distro I use.  Does that mean thedebianuser.org is responsible for a bias in favour of Debian in the survey results?  Not in my books - I use Debian/KDE, I voted for Debian/KDE.  Should I have not voted because I learned of the survey from a biased source?  It seems to me if DesktopLinux.org wanted to ensure no bias, they should have sent info on the survey to every community they included in it.\n\nIt's an internet survey (with no apparent mechanism for ensuring one person = one vote, beyond IP addresses, if that) - it is never going to be a balanced representation.  Putting something on the internet and then saying \"oh yeah, please don't link to this page\" is by far the dumbest thing I've ever heard.  You'd think as website operators they might have the foggiest clue about how the internet works..."
    author: "Geoff"
  - subject: "Re: Biasing Survey Results"
    date: 2007-08-21
    body: "> It's an internet survey (with no apparent mechanism for ensuring one person = one vote, beyond IP addresses, if that)\n\nNo, they do not seem to check IP addresses. They allow any number of votes per person."
    author: "Erik"
  - subject: "ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "just imagine, if both camp,had worked together in this time, just imagine how much effort has gone in reinventing the wheel, how much lines has been written in this stupid flame wars, each one pretending he is the real desktop Linux. \nshame on you, sometimes i feel like the developers has no idea of how people use  linux, there is no gnome or kde fanatics, we use both of them, i am kde users, but i use openoffice, firefox (gtk based) and openproj ( java ), as i imagine people who prefer gnome may use krita or amarok or what ever software that fulfills their needs, people  don't care about the framework used, we want applications well integrated that do the job that's all. \nit's a pity that we piss on ourself and forget the real challenge, windows has   95 % of market share.       "
    author: "djouallah mimoune"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "I disagree, and I think both KDE and Gnome 'camps' get it quite well, along with the Firefox, Open Office and Eclipse 'camps'. Multiple GUI toolkits/desktops are not a problem if the various projects cooperate on infra-structure, as they do via freedesktop.org and other common interest groups such as for printing or document formats. \n\nThe people who are directly contributing to the KDE and Gnome projects are happily cooperating as well as competing. I think the problem is more with the so called 'fans' of KDE who bore everyone senseless on forums like OS News with the same tired old arguments about GTK LGPL vs Qt GPL dual license, or how Mono is the devil's spawn, or how Gnome or KDE's language bindings are much easier to produce etc, etc. I think we really need some kind of FAQ where we can put the various claims and counter-claims, and then just refer to the FAQ, which would allow use to discuss less boring things. Such as whether the best colour for widgets is blue or brown.. "
    author: "Richard Dale"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "take for example khtml, did you think, that we needed the intervention of apple to make webkit which is simply a khtml without the qt dependency, why you( kde developers)  did not make the engine abstract in the beginning !! so others can used it !! \n\nas a user  when i used a (gtk, java, qt whatever) application i expect when i click on open file,or print, i'll find my kde dialogue ( and this true for gnome too), is this hard to implement, is this hard to make a fd.o specification !!! \n\n   "
    author: "djouallah mimoune"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "don't bother answering Richard, everytime djouallah is posting he needs to flame on something:\n\nhttp://www.googlesyndicatedsearch.com/u/dot?as_q=&as_epq=%22Also+by+djouallah+mimoune%22&as_oq=&as_eq=&num=100\n\nby the way, thanx for adding my ruby syntax suggestions to svn :-)\n\n"
    author: "Patcito"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "Patcito, me flaming !, if you found my post stupid, just don't respond, unfortunately we are not all that smart, this is life, so what! if you are not a super c++, qt hacker, you have not the right to write about kde !!!\nthe dot is a free forum, AFAIK ;-)\n\nfriendly  "
    author: "djouallah mimoune"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "But the problem is that you've put 'both camp don't get it ;-)' in the title of your question (albeit with a wink), which implies that you as a 'genius pundit' who somehow knows more than the Free Desktop developers do, and that if we only followed your advice all problems would melt away.\n\nOf course there are people are putting in real *work* to make dialogs in the various applications fit in with the desktop they're being used with - they aren't just sitting there in an armchair telling others what to do.\n\nKHTML was developed by very few people, and the reason that it wasn't developed without Qt dependencies initially, was that there was no need for that. And the extra effort would have meant that it would probably would never got out of the door in time for the KDE release it was aimed at. If you think your judgement of such matters is better than the likes of Lars Knoll, then I'm sure the KDE project would be happy to use your services as a release manager.\n"
    author: "Richard Dale"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "why you are so aggressive, i was just asking questions,is this a crime !! i did not say to people why they have to do! i did not make a rant or a judgment, simply because i don't have the skills. \n\nfor khtml, i am just sad, that apple take a credit for webkit.\n\n\"then I'm sure the KDE project would be happy to use your services as a release manager \" \n\nthat's bad, i did not expect that you treat users like that.( even if you think,they are stupid)\n\nsorry, but as a non native English speaker, my writing perhaps seems rude.  "
    author: "djouallah mimoune"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "\"i was just asking questions\"\n\nWell, no you weren't just asking questions:\n\n\"don't get it\"\n\"shame on you\"\n\"reinventing the wheel\"\n\"stupid flame wars\"\n\"each one pretending\"\n\"gnome or kde fanatics\"\n\"it's a pity that we piss on ourself and forget the real challenge\"\n\nYou think these are neutral phrases, even if you can be given some slack for not being an english speaker? I have no intention of being rude to kde users or developers, but you must be more careful with your use of loaded phrases if you prefer to get polite responses to your questions.\n"
    author: "Richard Dale"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "ok, take it easy, man, just take a look at the \"Biasing Survey Results\" thread, no comment !!   "
    author: "djouallah mimoune"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-17
    body: "I think you should take it easy and think twice before using such vocabulary such as \"stupid\" and \"pissing on ourself\". And don't use the \"I'm not a native English\" argument please because I invented that excuse :-) as I'm not a native either so I know how fake that excuse can be. By the way, not only is Richard an amazing programmer but he's also a very nice non-aggressive person ;-)"
    author: "Patcito"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-17
    body: "Well to answer your question, free software devs works on their projects for fun and are not being paid for it so they tend to only work on things that are fun to them.\nNow think about it:\nMaking KHTML independant of KDE= not fun, boring and time consuming\nMaking a cool HTML engine for your favorite desktop= lot of fun\n\nsee what I mean?\n\nWhen was the last time you did something not fun, boring, time consuming and difficult without being paid?\n\nOk, I guess you now understand why the KHTML guys didn't work on that first."
    author: "Patcito"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "that's just not true... Djouallah generally is very sensible."
    author: "jospoortvliet"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-16
    body: "yeah !!  thanks jos, i owe you a beer, oops, i mean, a coffee ;-)"
    author: "djouallah mimoune"
  - subject: "It's YOU who doesn't \"get it\""
    date: 2007-08-17
    body: "So we would be better off if we had just one team working on just one desktop? So, answer me these questions:\n\na) KDE-developers prefer C++ and Qt. Would they be as good in C and GTK?\n\nb) GNOME-developers prefer C. Would they be as good in C++ and Qt?\n\nc) Related to A and B: what language and toolkit should this unified desktop use? How would you convert the develpers of the \"other\" language and toolkit in to the new language and toolkit?\n\nd) Both KDE and GNOME have very different design-philosophies that are quite incompatible on many levels. How would you go about combining the two without alienating users and developers?\n\nIn short: Combining KDE and GNOME would be downright stupid, since the two are different, and they attract different users and developers. If they were combined, many developers would be unhappy and they would start their own desktop-project, and we would be right back where we started.\n\nRemember: open source is about scratching an itch. KDE does not scratch the itch GNOME-developers and users have, and vice versa.\n\nTo answer some individual comments you made:\n\n\"just imagine, if both camp,had worked together in this time\"\n\nyou mean something like freedesktop.org?\n\n\"just imagine how much effort has gone in reinventing the wheel\"\n\nsuch as?\n\n\"how much lines has been written in this stupid flame wars\"\n\n99% of the time those flamewars are fought between users, not developers.\n\n\"shame on you\"\n\nNo, shame on YOU, for coming here and dictating other what they should and should not do!\n\n\"people don't care about the framework used, we want applications well integrated that do the job that's all.\"\n\nSo this isn't about the desktop, this is about the apps? So why are you whining about the desktops then? And why are you whining since GNOME-apps work just fine in KDE and vice versa?\n\n\"it's a pity that we piss on ourself and forget the real challenge, windows has 95 % of market share.\"\n\nSince when has this been about taking away market-share from Windows? I thought this was about creating the best possible desktop-UI for computers?"
    author: "Janne"
  - subject: "Re: It's me who don't \"get it\""
    date: 2007-08-18
    body: "Janne, i know my English is a crap, but nevertheless i never said that.( did you read my posts !!)\n\nby working together i mean sharing the maximum of technology not writing the super unified desktop environment. \nthings like dcop( yes now we have dbus but after 10 years), khtml,shared mime entries could have been adopted earlier, did we needed to wait 10 years.\ntake for example solid, phonon( oops i am aware that people will treat me as a moron ;) api to make access to hardware and multimedia independent from the OS or the engine( gstreamer, xine), which is a great.\n\nthe problem is as the others ( gnome, xfce, etc) will not adopt those library, because it depends on QT. as a result they would probably  begin from scratch and create their \"real, free\" libraries, or in the case of phonon are saying gstreamer is the standard and we don't need another silly abstraction layer.    \n \ncan you imagine, totem using phonon, or nautilus using solid !!!\n"
    author: "djouallah mimoune"
  - subject: "Re: It's me who don't \"get it\""
    date: 2007-08-18
    body: "> by working together i mean sharing the maximum of technology not writing the super unified desktop environment.\n\nEven if it might not be that visible to people not associated with the project or maybe not even to associated non-coders, this is what is happing.\n\nSharing technology is not as easy as it sounds, because for most options there already exist subsystems and any new shared replacement will have to be able to replace all of them, basically offer a union of all features.\n\nAdditionally switching to a shared implementation can involve a lot of work which nobody currently has the resources for. Maintaining even a less then ideal but working solution is often less work.\n\nNot to forget that it depends on both old and new subsystem if the switching can be done without creating compatability issues.\n\nUnfortunately KDE seems to get a lot of critique for not immediately \"adopting\" some software somebody else has just uploaded to freedesktop.org (which by itself does not make it shared technology, but merely a candidate).\n\nIt is so easy to forget that quite some KDE developers or developers close to KDE have refrained from adding Qt dependencies to their respective project, resulting in additional work, because at that time somebody from outside KDE indicated interest. In the end this additional efforts have almost never lead to adoption by other projects, probably because people just want to assume without checking that anything originating from KDE is somehow KDE dependent. \n\nWe have seen this reaction for things like aRTs, DCOP and others, we are starting to see it for things like Strigi.\n\nOn another front, KDE gets critique for actually using shared techology.\nAs you mentioned it yourself, a good example is Solid. Why do people assume that application developers should be doing low-level D-Bus calls to HAL instead of using a nicely designed API?\n\nNot even the developers of the HAL project suggest one should be doing it, they even maintain and ship such a convenience API themselves. I am sorry but I fail to see why abstracting the D-Bus calls into a C-API would be OK and abtracting it into a C++-API wouldn't."
    author: "Kevin Krammer"
  - subject: "Re: It's me who don't \"get it\""
    date: 2007-08-18
    body: "It's not your English that's the problem in this case, but simple lack of knowledge regarding history and a little misunderstanding of the reason behind some of the thecnology. So the \"after 10 years\" comments are way off.\n\nLike DCOP, back when it was created nothing exsisted which was fit for the use KDE needed. Later when Gnome needed similar thecnology they went for a solution based on CORBA, which KDE had alredy found unfit for the task. And that GNOME newer got a IPC usage comparable to KDEs use of DCOP underscores that. Years later when efforts to intergrate the desktop better with Linux, HAL was started. The need for an IPC was again apparent, and DBUS was created. And it was even modeled after DCOP. Besides KDE has for years already used DBUS for the same tasks as GNOME. From KDE 3.4(or was it 3.3) DBUS/HAL have been used for media discovery and detection just like on GNOME.\n\nFor khtml it has always seemed more a lack of giving back from people interrested in the thecnology. From Apples recent fork into WebKit, developed for a year behind closed doors rather than cooperating. To GNOMEs gtkhtml, orginally forked from KoHTML(khtml's predecessor) a long time ago.\n\nRegarding the shared mime entries, it has not exsited for more than a few years and it has not been possible to incorporate in KDE earlier becouse of BIC issues. Had there been a mime standard 10 years ago, KDE would have adopted it like it does other standards that make sense. Like the menu entry standard, which KDE was able to support as soon as it was ready.\n\nSolid is not intendend to be a library for other desktops, its a abstractionn layer on top of stuff like HAL, to to make it easier to use foe KDE coders and intergrate it better in the desktop. The standard here is HAL, and it's the choice of the the other desktops like XFCE and GNOME how they want to interact with it.\n\nAs for multimedia, KDE already tried to cooporate and make a standard years ago with aRts. To acomodiate this, aRts was early changed from using the Qt event loop to be based on glib. But in the end it seems like NIH syndrome and personal interests was bigger, and gstreamer was created from scratch. In truth the open source desktop is not any closer to a standard multimedia framework today than it was 10 years ago, but KDE has benefitted from haveing a common solution all those years. So to keep KDE both platform independant and not tie itself to a multimedia backend that make fade and die becouse of politics or supperior alternatives, Phonon is created. And as a benefit it makes it much easier for the KDE coders to use, as the style of the API is familiar with what tehy already are used to in Qt.\n\n"
    author: "Morty"
  - subject: "Re: It's me who don't \"get it\""
    date: 2007-08-18
    body: "thanks really kevin and Morty for your explanation, so it's all about politics and NIH syndrome. i hope initiative like lsb and portland will make the next 10 years better.\n\nand sorry again for my language ( reading all those comments in digg, and slashdot  has made a bad influence on my words ;)    "
    author: "djouallah mimoune"
  - subject: "Re: It's me who don't \"get it\""
    date: 2007-08-18
    body: "It's not always about politics and NIH syndrome. Often there simply are no exsisting thecnology or standards or the ones that do are not possible to adopt to your needs(with reaosnable amount of work). And if that happens, it does not make it standard by declaring it to be so etiher. You have to look at the history of projects to see why things are done the way they are."
    author: "Morty"
  - subject: "Re: ten years later and both camp don't get it ;-)"
    date: 2007-08-17
    body: "Imaging the loss of development pace with the loss of competition. And yes, GNOME and KDE are their biggest competitors, as they can be compared directly (opposite to Windows and OSX).\nWithour GNOME, KDE's menus would still be clutters as they were years ago. Maybe no Kontact without Evolution. No ... without ...\nIf forces would have joined, progress wouldn't have doubled. I'd rather think progress would have been decreased.\nHaving a friendly competition pushes us forward to challenge even better with windows (which I doubt has a lot more than 90% marketshare left).\n"
    author: "Birdy"
  - subject: "why I must read digg to vote??"
    date: 2007-08-16
    body: "If I am a kde user since ever, why I must read digg or the site promoting the survey to know that there is a survey???\n\nI assume that if I use kde I want to vote on it. What's the trouble??\n"
    author: "Sergio fernandes"
  - subject: "10 years of gnome"
    date: 2007-08-16
    body: "I think it is interesting that Miguel did have using language bindings as one of the main tenets in the creation of Gnome. And they still can't make up their minds about it. :) Maybe they should have just stuck to the Scheme bindings and avoided much internecine conflict."
    author: "Ian Monroe"
  - subject: "Re: 10 years of gnome"
    date: 2007-08-16
    body: "Yes I agree Scheme is a great language I wonder why it didn't take off for Gnome programming. There are still Guile bindings being actively maintained: http://www.gnu.org/software/guile-gtk/\n\n\"Qt also forces the programmer to write his code in C++ or Python.\nGtk can be used in C, Scheme, Python, C++, Objective-C and Perl\"\n\nWell since 1997 we've had Qt bindings for all those languages except.. Scheme. Maybe Qt/KDE Scheme bindings would be KDE's long lost killer bindings app?\n\n\"We plan to export the GTK API through a procedural database\n(which will in fact be an object database) to allow easy\nintegration with scripting languages and modules written in\nother languages\"\n\nThis is interesting - I'd never heard of it before. It sounds a lot like the Smoke approach of a language independent dynamic introspection based binding of the complete static language C++ (or C for Gnome) api.\n\nThe magic bullet for bindings was even supposed to be CORBA at one time.\n\nMore recently it's been language independent virtual machines like the Java JVM or the .NET CLR (and Parrot - is that MIA?). Now we've got Qt bindings for both the Java JVM with QtJambi, and Mono bindings with Qyoto, I think we can have a lot of fun finding out whether that is a good idea. Simon Edwards, Arno Rehn, one of the QtJambi guys and myself, did a bit of playing around with that at this years aKademy and it does seem to 'nearly work', but isn't quite as easy as it sounds. I especially liked Arno's analog clock in VB.net although it apparently didn't quite work.\n\nThe latest Gnome bindings Great White Hope is Vala, which sounds pretty interesting to me and not a bad idea at all. But what if they'd just adopted the OpenStep based GNUstep instead, and radically improved that? I really think KDE would have much more trouble competing with a Cocoa clone, as there isn't a lot wrong with Cocoa/Objective-C even after all these years."
    author: "Richard Dale"
  - subject: "Re: 10 years of gnome"
    date: 2007-08-17
    body: "Vala seems to be mostly a \"Mono is yucky\" development. I've looked at the code for Banshee and it seems rather straight forward. Banshee itself is surprisingly responsive. Which really shouldn't be surprising, its just that I'm so used to the 10 years of experience of a VM meaning Java meaning AWT meaning clunky, memory hugging and ugly.\n\nThough the Gnome guys probably know more about Mono then we do, maybe its not a horse you should bet on."
    author: "Ian Monroe"
  - subject: "Re: 10 years of gnome"
    date: 2007-08-17
    body: "I can't really let your comment regarding Scheme go unnoticed, I just hate it so much. I had to use Scheme in a class, Lisp is such a horribly ugly language.\n\nGranted it was a programming language class that we learned it in, so I suppose I shouldn't be surprised that a such an avid student of programming languages as yourself would be a fan. ;)"
    author: "Ian Monroe"
  - subject: "Re: 10 years of gnome"
    date: 2007-08-17
    body: "When I was a student we used a language called 'pop-11' and Unix which was like lisp with a more Pascal like syntax: \n\nhttp://en.wikipedia.org/wiki/POP-11\n\nI think pop-11 was a better language to start learning programming than lisp (and ruby would be better today). My Philosophy professor, Aaron Sloman describes the differences like this:\n\n\"The closest comparable language with similar characteristics is Common Lisp, though many people (not all) find the Pascal-like syntax of Pop-11 easier to learn than Lisp's very terse syntax using very few syntax words. For the same reason some people find Pop-11 programs more maintainable. On several occasions I have met commercial programmers who were used to other languages, like Pascal, Fortran or C, and who had tried to learn Lisp and disliked its syntax immensely. By contrast when they tried to learn Pop-11 they found that it offered them a smooth transition from familiar programming constructs to more sophisticated AI programming. After that they found Lisp easier to learn. \n Not everyone agrees on which is easier to learn or use: so it is good that both Pop-11 and Lisp should be available.\"\n\nhttp://www.cs.bham.ac.uk/research/projects/poplog/primer\n\nI used to think lisp was ugly then. But learning Scheme from the book 'The Structure and Interpretation of Computer Programs' changed my mind as it must be one of the best computer science books ever written."
    author: "Richard Dale"
  - subject: "Re: 10 years of gnome"
    date: 2007-08-17
    body: "And even better, the complete text of the sicp is freely available: http://mitpress.mit.edu/sicp/."
    author: "Boudewijn Rempt"
  - subject: "Re: 10 years of gnome"
    date: 2007-08-18
    body: "The programming language class is an upper level class about the history, design and syntax of languages. So learning Lisp did make sense, Lisp is very simply defined (our text book had a Lisp compiler written in Lips) and in the 80s pioneered garbage collection and such. Java was written by a bunch of old Lisp folks.\n\nLearning it as a first language would be pretty crazy. :)"
    author: "Ian Monroe"
  - subject: "Re: 10 years of gnome"
    date: 2007-08-17
    body: "A big congratulations to everyone involved in GNOME development for ten years of excellent work.  The GNOME HIG may not suit everyone's needs but it has been a big step forward for Linux on the desktop.  Well done!  "
    author: "David"
  - subject: "Safari on Linux?"
    date: 2007-08-17
    body: "I wonder how Safari got 0.3%...\n\nDo people run Mac OS inside a virtual machine? Or do they run the Windows version in Wine, etc.?\n\nAnd why?\n"
    author: "Dima"
  - subject: "Re: Safari on Linux?"
    date: 2007-08-17
    body: "MacOnLinux? Or they compiled the WebKit sources with the Qt backend?"
    author: "hf"
  - subject: "Re: Safari on Linux?"
    date: 2007-08-17
    body: "In konqi, Go to tools->change browser ID->other->safari.\nWhy? Have to have fun, I guess. I know that I have ID konqi on a few sites as Mozilla or even MSIE so that I could view the site (home depot was HORRIBLE for this and they were just one that comes to mind; That attitude cost them about 20K from me; I shopped lowes because they at least worked). Don't have to anymore."
    author: "a.c."
  - subject: "Congrats GNOME!! (Btw, How did we celebrate ours?)"
    date: 2007-08-17
    body: "You have to hand it to the GNOME guys, to have been able to successfully create a good desktop environment. And while we all probably want everyone to use KDE (right? :P), GNOME's existence and presence has been really beneficial to everyone.\n\nI saw this link from a dev from Ubuntu Planet:\nhttp://www.gnome.org/press/releases/gnome10years.html\n\nSeems like their going to make a scrapbook wiki documenting their 10 years of existence. I can barely remember how we celebrated and commemorated our 10th birthday last year. Aside from the official announcements on the website or blog posts on the Planet, did we have anything special during that time?"
    author: "Jucato"
  - subject: "Re: Congrats GNOME!! (Btw, How did we celebrate ours?)"
    date: 2007-08-18
    body: "http://dot.kde.org/1160834616/"
    author: "Konqi"
  - subject: "Re: Congrats GNOME!! (Btw, How did we celebrate ours?)"
    date: 2007-09-08
    body: "Yeah, congratulations to the GNOME project, for sure.  They've come a long way since the early days.\n\nIt's nice to see that both projects seem to be getting back to their roots, at least to a certain level.  I mourn the loss of a scripting framework for GNOME, for example, and wonder how in the world they took a light, speedy toolkit like GTK+ and managed to bog it down.  OTOH, it's a decent enough desktop with usability in mind.\n\nAnd KDE!  Most of what I see about the upcoming KDE4 tells me that y'all are getting back to the usability roots.  Hallelujah!  I've missed the simplicity of the KDE 1.x days.  The first GOOD non-Netscape browser on *n?x was, imho, KFM, and Konqueror continues that trend.  I'm especially glad to hear that there's an effort underway to work WebKit into GNOME; glad to hear that, as they need to get that albatross...erm, gecko, off of their neck. :-D  Had Netscape chosen to make their browser GPL only, and Gecko LGPL, it would have been no big deal that the Foundation keeps changing their mind about how their various copyrights and trademarks need to be enforced; unfortunately for projects like Debian, it has meant forking Firefox.\n\nSo yes, kudos to both projects for being around for more than a decade, and congratulations to both for continuing to improve by leaps and bounds, and to continue to converge toward a certain level of interoperability.  May the progress continue; the true winners in the end will be the users. :-D"
    author: "regeya"
---
<a href="http://amarok.kde.org/en/node/243">Amarok 1.4.7 was released</a> with improved collection backend, new streams, altered icon and bugfixes. *** The annual <a href="http://www.desktoplinux.com/news/NS7943272425.html">Desktoplinux.org Survey</a> is under way</a>. *** MEPIS <a href="http://www.mepis.org/node/13929">released a KDE 4 Beta 1 live DVD</a> using <a href="http://kubuntu.org/announcements/kde4-beta1.php">packages from Kubuntu</a>. *** The <a href="http://2007.video.lugradio.org/">videos from Lugradio Live</a> are up including Ben Lambs' Conquering the Desktop with KDE 4. *** Finally, following the <a href="http://dot.kde.org/1160834616/">10th anniversary of the free desktop</a> last year, congratulations to another project which <a href="http://mail.gnome.org/archives/gtk-list/1997-August/msg00123.html">has gained double figures in age</a>, but whatever did happen to those Scheme applets?





<!--break-->
