---
title: "aKademy 2007: Text Layout Summit"
date:    2007-08-07
authors:
  - "jriddell"
slug:    akademy-2007-text-layout-summit
comments:
  - subject: "Good to see gnome fame sticking around."
    date: 2007-08-06
    body: "i read some where that perhaps the next GUADEC and Akademy will take place  at the same city, if it's true it will be fun. \n\n<kidding> Behdad Esfahbod caught drinking beer in glasgow </kidding> "
    author: "djouallah mimoune"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-06
    body: "cool idea !"
    author: "srettttt"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-06
    body: "That was an idea, however it's not likely to happen for this year, last I checked. One of the arguments against this was that GUADEC requires people to pay to attend and Akademy does not."
    author: "Troy Unrau"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-07
    body: "So what's the problem?  It just means more people will come to aKademy."
    author: "Anonymous"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-08
    body: "\nthere is a good post from a gnome fame, why gaudec and akademy in same city is a sane for desktop linux. \n\nhttp://pvanhoof.be/blog/index.php/2007/06/03/because-competing-is-necessary-and-fun"
    author: "djouallah mimoune"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-11
    body: "This idea makes a lot of sense really. Having separate but overlapping conferences that allows interactions while still allowing each conference to have its unique character would be ideal. "
    author: "Ian Monroe"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-07
    body: "Won't happen.  The communities are not ready for it. The differences between GUADEC and aKademy are huge. \n\nRemember GNOME is 90% developed by fulltime employees, while KDE is 90% developed by volunteers. This makes GUADEC a business event and aKademy a more grassroot style event.\n"
    author: "Allan Sandfeld"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-08
    body: "Are those percentages reasonably accurate? Its not that I doubt you, on the contrary. Its the thought of how development is spread between the two has never occurred to me. To see these numbers takes me by surprise."
    author: "taurnil.oronar"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-08
    body: "Sorry for getting offtopic, but just to clarify:\n\nNo, my numbers were not accurate at all (it's probably more like 99% vs 66% volunteers, but don't trust me on that). It was just an illustration of how different the two communities are. \n\nThe real difference is in who shows up for these events. I've been told by several GNOME developers now, that they find aKademy to be a very different event because everybody are volunteers; something they don't see at all at GUADEC. This might be because aKademy is free and even pays for the plane-tickets of some volunteers, where GUADEC takes a fee just for the event.\n\n"
    author: "Allan Sandfeld"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-08
    body: "So, you correct your made up statistics by making up some more...\nHere are some facts from the most recent GUADEC\n\nGUADEC paid for some volunteers accomodation and travel. The amount paid is not known yet as the accounts are not finalised, but \u00a330,000 (61,000USD) was budgeted for this.\n\nJust over 400 people attended, estimated at around 430 due to some people turning up and not registering and some registering and not turning up. The registration numbers break down as follows:\n~200 were professionals\n~150 were hobbiests\n~160 were marked as Other.\n\nThe Other category breaks down into\n~75 - Students/Concessions\n~75 - Speakers\n~10 - GUADEC Volunteers\n\nSo, basically the volunteer/professional percentages are 45% professionals, 55% are volunteers.\n\nI hope that helps."
    author: "iain"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-08
    body: "Oh, and of those marked as professionals, not all are employed to work directly on GNOME. They may work for a company that uses GNOME technologies and so the company paid for them to attend the conference."
    author: "iain"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-09
    body: "\"So, basically the volunteer/professional percentages are 45% professionals, 55% are volunteers.\"\n\nYou'd never guess from the presentations and the stuff that gets talked about. GUADEC just has a very different feel to it, and paying to attend doesn't help the feel really."
    author: "Segedunum"
  - subject: "Re: Good to see gnome fame sticking around."
    date: 2007-08-16
    body: "You are totally wrong.  First, I was to aKademy this year, and it's not that different from GNOME Boston Summit.  Sure it's different from GUADEC, but more because of size and organization, not because of culture or inherent differences of the communities.\n\nAs for company vs volunteers, it comes to me as a surprise that someone suggests that GNOME is driven by companies and KDE is almost-all volunteers.  Don't forget that Qt is for the most part driven by TrollTech.  Same is not true about Gtk+.  And yes, in GNOME, there are about 10 companies that actively pay people to work on GNOME, because they use GNOME technology.  I don't see how this can be bad though.  Most those people have been community members doing GNOME work and then hired by the companies, not the other way around.  And GUADEC is definitely a heck of a lot of fun and very personal.  Nothing like a business-driven conference.\n\nI don't think the GNOME and KDE communities will find each other that different, should we get to host the conferences together."
    author: "Behdad Esfahbod"
  - subject: "ODF support?"
    date: 2007-08-08
    body: "http://testsuite.opendocumentfellowship.org/summary.html"
    author: "bert"
---
aKademy 2007 hosted two mini-summits, one for <a href="http://dot.kde.org/1183817727/">Schools and Education</a> and one for Text Layout.  The <a href="http://www.freedesktop.org/wiki/TextLayout2007">Text Layout Summit</a> was a true cross platform event, and followed from the one last year at the Gnome Summit.  Text layout is a complex area requiring advanced knowledge of dozens of different writing methods.  With funding from The Linux Foundation they brought together people from Pango, Qt, IBM ICU (Intl. Components for Unicode), SIL Graphite, Unifont.org, m17n, W3C and DejaVu.  Getting the various widget sets to have the same high quality support for all scripts is a problem the summit hoped to solve. Read on for details of what they discussed.











<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 250px; float: right">
<img src="http://static.kdenews.org/jr/akademy-2007-text-summit.jpg" width="250" height="188" />
Busy in the Summit
</div>

<p>Much of the current work for standardising text layout in the free software world is going on in the <a href="http://www.freedesktop.org/wiki/Software/HarfBuzz">HarfBuzz</a> project, a joint effort with code from Qt and Pango.</p>

<p>Organiser  Ed Trager said of what had been discussed <em>"A number of brief updates and discussions were given (as outlined on the <a href="http://unifont.org/TextLayout2007/">summit wrapup page</a>), but the major focus of
discussion was
    on insuring that the HarfBuzz architecture and API will
comfortably support the
    needs that SIL Graphite presents.  The Open Source SIL Graphite engine
    is at the current time, from a features perspective, the most
    advanced Text Layout Engine available anywhere and was
    designed to support minority scripts as well as major complex
    world scripts like Burmese and Khmer.  Scripts not yet in
    Unicode are also supported.  There is an informed belief,
    especially from the Graphite community of course, that only a few key
    architectural features will need to be added or provided
    in HarfBuzz in order to support
    SIL Graphite integration.  (A similar feature set may be required
for Apple's AAT
    technology support, and Lars Knoll of Trolltech has already
stated that
    he intends for HarfBuzz to have AAT support).</em>"</p>

<p>"<em>Behdad Esfahbod
(Redhat, Pango)
    and Simon Hausmann (Trolltech, Qt) provided
    verbal agreement at the meeting that they would, together with the
SIL Graphite
    folks (Tim Eves and Sharon Correll, inter alia), provide the
    foundational infrastructure needed to support SIL Graphite in HarfBuzz.
    Eric Mader will also be looking to port his synthetic Arabic GSUB
table support
    for non-OpenType fonts from ICU into HarfBuzz.</em>"</p>

<p>Other talks included Arne Gotje talking about <a href="http://unifont.org/TextLayout2007/presentations/ArneGotjeCJKUnifontsProjectOutlookJuneJuly2007.pdf">CJK support</a>, the revival of <a href="http://www.openfontlibrary.org">openfontlibrary.org</a> a website to catalogue freely licenced fonts and <a href="http://eyegene.ophthy.med.umich.edu/interview/fontima.php">Fontima</a>, a library that can be used to make font selection widgets a better experience than just seeing one big list.  Sharon Correll <a href="http://www.unifont.org/TextLayout2007/presentations/SharonCorrellSILGraphiteNotes.pdf">presented Graphite</a> which aims to support many minority languages.</p>

<p>Promising as these developments are, there are still unsolved problems in the world of text layout, including the Pan-Unicode issue.  Should one font include every character, or should we fix our tools to make it easier to load characters from different fonts?  The issues were described in <a href="http://unifont.org/TextLayout2007/presentations/BenLaenenPanUnicodeFontsDiscussion.pdf">The Pan-Unicode Fonts talk</a> by Ben Laenen of DejaVu Fonts.</p>

<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 250px; float: left">
<img src="http://static.kdenews.org/jr/akademy-2007-text-summit-2.jpg" width="250" height="188" />
Relaxing in Glasgow
</div>

<p>Behdad Esfahbod of Pango said that "the true value of all this meetings and harfbuzz is the sharing
of OpenType engine and shapers between Pango, Qt, m17n and eventually ICU".  New work on HarfBuzz and Graphite appears to be continuing at
a good pace and although we don't know when it will all be completed, cross platform text layout bliss does seem a genuine prospect in the not too distant future.  Many thanks to Daniel Glassey and Ed Trager for organising the summit.  There are <a href="http://jasmine.19inch.net/~jr/away/2007-07-akademy-text-layout-summit-glasgow/">some photos of the meeting</a> and you can get the <a href="http://www.unifont.org/TextLayout2007/">presentation slides and an audio recording</a> of the summit, with video still to come.</p>












