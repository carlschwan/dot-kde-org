---
title: "The Road to KDE 4: Dolphin and Konqueror"
date:    2007-03-01
authors:
  - "tunrau"
slug:    road-kde-4-dolphin-and-konqueror
comments:
  - subject: "More configurable, maybe?"
    date: 2007-03-01
    body: "\"That doesn't mean it won't be powerful or configurable, only that Dolphin is being built for a single purpose.\"\n\nI think this bears repeating - something that is optimised for File Management and which can implement features without worrying how they will affect other functionality (e.g. as Troy mentioned, the \"breadcrumbs\" mode would not be a good fit for Konqueror as the mode is not useful for webbrowsing) could *easily* end up being a far more powerful and configurable tool than Konqueror, while avoiding the cascade of clutter when you open a config menu by virtue of its not being a Swiss-army knife.  The factoring out of useful code from Konqueror into shared libraries means that if Dolphin doesn't have this as its this goal, then writing a File Manager that does will not be such a Herculean task\n\nIt would be interesting to see if anyone steps up and makes a dedicated Khtml-based, KDE-integrated web-browser."
    author: "Anon"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "> It would be interesting to see if anyone steps up and makes a dedicated Khtml-based, KDE-integrated web-browser.\n\nThere's already, called Konqueror."
    author: "Pino Toscano"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "I think Anon's emphasis was on \"dedicated\".  Although in that sense it's still a weird thing..."
    author: "Ralesk"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "This could be an interesting idea. What I feel may be more useful and this may already be the direction used is kind of like a shell that detects what type of activity you are performing. Yes, Konq. already attempts to do this but let's take this into the near-do-al ways of Microsoft for a minute. If you have Windows Explorer open and attempt to navigate to a website by manually entering this into the location bar, just about everything from Windows Explorer (Exception, File Tree on left) will disappear and be replaced by the layout of Internet Explorer. Surely, with the fact that Konquorer already attempts to determine what the user is doing, this cannot be to difficult to do - just switch seamlessly switch from Konqueror to Dolphin and mid-shift.\n\nPlease, no flaming on this as this does seem to be the idea asked here and may already be a direction we're headed."
    author: "Drgan"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "Actually with IE7 installed, Explorer no longer switches layouts like that. It now just opens up the default web browser with that url. Note it uses the default web browser, not just IE7."
    author: "Kevin"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "\"as Troy mentioned, the \"breadcrumbs\" mode would not be a good fit for Konqueror as the mode is not useful for webbrowsing\"\n\nIt would be very easy to implement it so that Konqueror switches to using a breadcrumb interface when in filebrowsing mode. I hope this switch to Dolphin development doesn't mean the filesystem side of Konqueror is left as is. I don't see why anything currently implemented in dolphin couldn't be included in Konqueror's fileview mode.\n\n"
    author: "Evan Robert"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "I'd say that breadcrumbs aren't a good fit for file browsing either. The only place where breadcrumbs (sort of) work are in blogs -- and even there it's only useful because there isn't anything better, really."
    author: "Boudewijn Rempt"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "Any reason why you don't think they're a good fit for file browsing? The advantages are that you have one click access to to any ancestor directories AND (by holding down the button, moving your mouse a bit, and then releasing) access to direct descendents of directories in the ancestor directories.\nI can't see any real disadvantages."
    author: "Cerulean"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-03
    body: "Well, maybe you're not looking for them really.\n\nI see one clear disadvantage, and it is that with the \"breadwhatever\" you totally lose the sense of where you're standing inside the directory tree. There's no single way in this paradigm to eyeball what other places are there in the directory structure to find what you're looking for (an image, text, whatever).\n\nLet me say that I really dislike that paradigm and from my experience is something that is not by any means more usable than the tree structure, let alone that you're trying to change what 95% people already feel comfortable with as Richard stated.\n\nIf I was to ask something to Dolphin's developers as not to make this breadchunks the default way of looking at the file structure. Make them an option for people wanting to 'think different'.  ;)\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-03
    body: "> If I was to ask something to Dolphin's developers as not to make this breadchunks\n> the default way of looking at the file structure. Make them an option for people\n> wanting to 'think different'. ;)\n\nPlease, guys. Try dolphin before complaining about it. This is already an option.\n\nAnd since yesterday dolphin in svn also has a treeview. It's not finished, but it is there. Added within 24h(!!) after all this complaining started.\n"
    author: "infopipe"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-03
    body: "Fist of all, I can't try Dolphin, because I use Mandriva 2006 and don't have the chance to do it.\nBut I would like you to read more carefully before replying.\n\nI said what I said, not because I believed there won't be a tree view in Dolphin, but because, as I understand, currently the breadcrumbs seem to be the default option and I clearly don't like that fact.\nI hope I have made it clear this time.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-03
    body: "How are breadcrumbs in the location bar worse than text in the location bar?  With a single click the breadcrumbs will switch to the standard location bar as well.  Also there is a KDE3 version of Dolphin which should work in Mandriva 2006 no problem."
    author: "Sutoka"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-09
    body: "I don't mind having breadcrumbs in the location bar if I can have a tree view at the left side of the Dolphin window, but by all means I don't want to have it as a replacement for the tree view. This would be a step backwards.\n\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "I for one like and use the Up button on websites that are well structured, like the dot.\n\nAnd allowing websites to provide a tree-structure would be good too. Better than learning navigation over and over.\n\nKay"
    author: "Debian User"
  - subject: "Re: More configurable, maybe?"
    date: 2007-03-02
    body: "So do I :)"
    author: "kollum"
  - subject: "me, 2oooo..."
    date: 2007-03-04
    body: "..I sec.. third that! Moving up is one useful, the Google Toolbar has it also! But it got kinda broke by not being able anymore to move up from a subdomain. I filed a bug 2 1/2 years ago but it got ignored: http://bugs.kde.org/show_bug.cgi?id=95433\nI wished I could code QT so I could fix stuff like this myself... after the exams I'll try..."
    author: "eMPe"
  - subject: "Most important thing: Keyboard navigation?"
    date: 2007-03-02
    body: "Who else thinks that keyboard navigation is a very important topic where Konqueror (and Dolphin 0.x) have deficits?\nJust navigating between directories, deleting files (focus jumps to the beginning of the directory), creating folders (new folder is *not* selected),... today has too many (usability) bugs."
    author: "max"
  - subject: "Re: Most important thing: Keyboard navigation?"
    date: 2007-03-02
    body: "This could be a great way for you to contribute to the KDE project!  Not many people bother with keyboard navigation, so I guess is gets very little testing.  Once eveything stabilizes a bit more why don't you download a snapshot and give dolphin some thorougher keyboard only testing and submit bugs?  I'm sure it would be pretty easy to do and would make sure Dolphin is perfect for you in KDE 4.0!"
    author: "Paradox"
  - subject: "Re: Most important thing: Keyboard navigation?"
    date: 2007-03-02
    body: "I agree, there are way to many keyboard related bugs in kde, probably half of the 30some bugs i have reported to kde have been keyboard navigation related. \n\nE.g. hit alt+b (bookmarks menu), hit down until you have selected a folder, hit right and then left and finally the menu key (between alt gr and ctrl), then choose anything there, e.g. properties, notice how the dialog you get isn't the correct one, you are getting the dialog for the topmost item in the folder, not the folder itself."
    author: "Marius"
  - subject: "Re: Most important thing: Keyboard navigation?"
    date: 2007-03-02
    body: "i think that all [not object oriented] actions should be available through keyboard.\n\nfor example i've hidden kmail toolbar after learning all keys i need. (and i rarely use menubar as well)\nanother example: i execute apps by pressng Alt+F2, typing first few chars of the name and pressing enter (autocompletion can be enabled in the rmb-menu of any lineedit)\nand of course amarok global shortcuts rule"
    author: "nick"
  - subject: "'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "I use the up arrow when I'm browsing to get to the upper directory of the html file I'm viewing. For example this news addres is http://dot.kde.org/1172721427 , so when I press UP, Konqueror will open http://dot.kde.org/ . I like it !\nThe bookmark are store as KIO://address and I also like it!\n\n"
    author: "zvonsully"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "Agree. The \"UP\" arrow is one of the nicer things about konqueror. "
    author: "lll"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "Yeah, +1 saving the up arrow in Konq."
    author: "Pete"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "I didn't say it's going away either :) I was just using it as an example of a button that is less than intuitive for many websites.  What does it do when using gmail? it breaks the website (last I checked).  Works great on dot.kde.org though... I use it after I post a comment to go back to the article quickly :)"
    author: "Troy Unrau"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "The back button breaks a lot of sites too. But that is caused by (ab)using the browser as an application platform rather than a hypertext viewer, so the back button is not really to blame. (I like web apps, but they are definitely something web browsers were not designed for.)\n\nI find the up arrow very useful: if you come in via a search you often find yourself somewhere deep down in the tree structure of a site and on many sites navigation controls are either missing or not obvious, making the up arrow a quicker alternative."
    author: "Maarten ter Huurne"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "+1 for up button"
    author: "Re: 'Up' arrow whe webgrowsing "
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "Me too,\n\nThe up arrow is a great feature for web browsing, something I immediately miss when using firefox.\n\nYes it does have limitations, but it's very useful to getting to a site's toplevel page after landing deep inside it having followed a websearch result."
    author: "Paul"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-03
    body: "Firefox has a plugin called konqefox or similar, based on the knonqueror functionality. Some other plugins also provide this. And check out the buttons in FFox 2! \n\nKonqueror definately lead the way with this button, and I consider browsers without it to be broken, especially when you consider the social bookmarking sites that link to deep structures, and you want to have a nosey round the site :)"
    author: "Glenn"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-04
    body: "Please check again, up-button works fine in gmail, it brings you to your overall view of your mailbox. In my personal experience there are very few websites that really break with the up-button."
    author: "LB"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-03
    body: "Mark me down for the \"up\" arrow (and the \"clear\" / \"enter\" clickable buttons by the url, they save a boat-load of time for me...)"
    author: "Brandon"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "Ditto that. I like it too. I only wish it would skip cgi parts. But I can live with that."
    author: "Mads Bondo Dydensborg"
  - subject: "'Up' arrow in file browser, too?"
    date: 2007-03-02
    body: "Troy, thanks for the terrific articles. And especially to all the kde4 developers!\n\nI recently had a chance to use MacOSX, after being 99.9% KDE and 0.1% Windows for several years. Given OSX's reputation I was excited to give it a spin, but I confess that I really did not like the Finder. One of the biggest usability problems for me: it took me 10 minutes to figure out how to go \"up\" one level of the directory hierarchy. For me, the problem was that it looked like all the functionality should be available from the buttons in the finder window (so much of it was), but in fact one had to navigate into a menu in order to find \"up\". Even now that I know what to do, it still seems like a really horrible usability design mistake.\n\nNow, perhaps the breadcrumbs (which look nice!) basically eliminate the need for an up button? But will an up button be available as an option? (In particular, do recent versions of Windows still have an up button? One could imagine this being something set by an initial \"configure KDE to look like: 1. KDE, 2. MacOSX, 3. Windows\" wizard.)\n\nAlso, what will the breadcrumbs code do when the file hierarchy gets so deep that it can't all be displayed in the window? Presumably there has already been thought given to this, but I'll offer my 2 cents (for what it's worth):\n1. Having all of the breadcrumbs get more and more abbreviated (sort of like the tabs in konsole) might not be great, because then they really won't convey any useful information---a single character probably won't help most users figure out which directory is which.\n2. One might want to prefer showing the directories that are immediately above the current location, if necessary at the expense of ones even higher up. After all, one can always click \"home\" to get there.\n3. Alternatively, the algorithmist in me likes a \"fully display every nth directory\" (or perhaps choose the ones to display on a log scale, oooo!) because it insures that any higher directory is not many clicks away. But I fear that could be rather confusing to most users, so I don't really suggest it seriously...\n"
    author: "Tim"
  - subject: "Re: 'Up' arrow in file browser, too?"
    date: 2007-03-02
    body: "> But will an up button be available as an option?\n\nIn the KDE 3.5 Dolphin release, you can add an Up button by configuring the toolbar."
    author: "Jucato"
  - subject: "Re: 'Up' arrow in file browser, too?"
    date: 2007-03-05
    body: "I would prefer your option 2 combined with Left/Right buttons similarly to the Left/Right buttons that appear when you have too much open tabs in Konqueror.\n\nSpeaking of tabs in Konqueror, I would like to have an option for setting the minimal width of the tab title (if you have many open tabs, the title currently reduces to \"...\" which is not very informative)."
    author: "Vlad"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "As a basic KDE user guy, I'm crazy about the 'Up' arrow when using the browser.  It's one of the things I miss every time I am forced to use Firefox.  Keep it handy, please."
    author: "TimZ"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-02
    body: "Cool! I was missing the Up arrow while using Dolphin and I just realized that it can be easily added by right clicking on the toolbar and clicking \"Configure Toolbars\". KDE rocks. :)"
    author: "Elijah Lofgren"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-04
    body: "Just another +1 for \"keep the up-button as part of the default buttons\". I think this button is really useful when browsing websites. It would be a shame that handy buttons are removed in the \"quest for an empty toolbar\". Who makes these decisions?"
    author: "LB"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2007-03-04
    body: "Forgot to mention that gmail works just fine with the up-button, it takes you to the beginning of your mailbox."
    author: "LB"
  - subject: "Re: 'Up' arrow whe webgrowsing"
    date: 2008-06-20
    body: "+1 for the Up button.\nThis thread just tells the writer of this article how ignorent he is."
    author: "konqueror lover"
  - subject: "KPart of Dolphin"
    date: 2007-03-02
    body: "How about to KPart Dolphin and Konqueror just to embed it when viewing files just as KPDF is embedded for viewing pdf? This part could also be used by the \"Open\" dialogs."
    author: "zvonsully"
  - subject: "Re: KPart of Dolphin"
    date: 2007-03-02
    body: "Well, the icon view from dolphin is being made into a KPart for just this purpose, although it's probably too heavy for a file dialog :)"
    author: "Troy Unrau"
  - subject: "Re: KPart of Dolphin"
    date: 2007-03-02
    body: "But you're on the right track. Dolphin, Konqueror and kfile (the File|Open dialog) share code already.\n\nAny and all usability improvements made to the listing of directories should appear in all three. But obviously, kfile will be a lot more limited in features than Dolphin: it is not a file manager."
    author: "Thiago Macieira"
  - subject: "Correeection"
    date: 2007-03-02
    body: "> it now implements CSS 3, including the highly-anticipated 'opacity' tags.\n\nNot everything remotely related to web development is a \"tag\" you know.  You are referring to the opacity *property*.\n"
    author: "Jim"
  - subject: "Re: Correeection"
    date: 2007-03-02
    body: "Yeah, and they aren't called \"tags\" either; they're called \"elements\" according to XML and DOM."
    author: "Matt"
  - subject: "Re: Correeection"
    date: 2007-03-03
    body: "Tags exist, and so do elements, but they are not the same. For example:\n\n<b>Hello there</b>\n\nThis is one element, and two tags (a start tag, and an end tag)."
    author: "AC"
  - subject: "Re: Correeection"
    date: 2007-04-09
    body: "Not even that was correct ;)\n\n<b>Hello there</b>\n\nIs actually two elements, and two tags. The tags are [<b>, </b>] and the elements are [the b-tags, the text]. Text inside a start- and end-tag is an element by itself. A \"text element\"."
    author: "Gustaf"
  - subject: "Re: Correeection"
    date: 2007-03-02
    body: "My apologies :) thing semantics over syntax, I'm not a professional writer or anything :P"
    author: "Troy Unrau"
  - subject: "Re: Correeection"
    date: 2007-03-03
    body: "Also, it's not strictly correct to say Konq implements CSS3, as many modules of CSS3 aren't even complete yet. The linked article refers to the CSS3 Selectors module plus one other propere"
    author: "joe lorem"
  - subject: "Re: Correeection"
    date: 2007-03-03
    body: "Also, it's not strictly correct to say Konq implements CSS3, as many modules of CSS3 aren't even complete yet. The linked article refers to the CSS3 Selectors module plus one other property"
    author: "joe lorem"
  - subject: "Konqueror"
    date: 2007-03-02
    body: "Use whatever you want, but I don't think switching to \"Dolphin\" as the default file manager for KDE will be a good thing.  One of the biggest complaints I see from Gnome (and even some OSX/WinXP) users is that their file managers are not as capable as Konqueror.  If it really bothered people so much that Konqueror also did file management; then why not the wholesale conversion to Krusader?  or Nautilus?  or the other 8 dozen file managers?  Because Konqueror is easily the best file manager available on any platform...  The users haven't wanted a switch from Konqueror.  In fact the only users I hear complain about Konqueror currently is users who have no intention of EVER user it, because it is a KDE app.\n\nRemember when Gnome switch to the \"new\" filemanager design in v2.0?  Most users assumed that the limited feature set meant that it was somehow broken.  I really don't want users to come to KDE and think it's file manager is broken.\n\nThe other question I have is; if it is SO important to move away from Konqueror as the default file manager, then why not simply use Krusader?  It is already there, it is fast, it is actively developed, and it has a signle focus.\n\nBobby"
    author: "Bobby"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "Well, the way it will be is that Dolphin is the default.  Konqueror and Krusader are alternates which you can use, and set as default.  I know that there are a lot of people out there that really love Konqueror (I'm one of them) who will love Konqueror all throughout the KDE 4 series.  The fact that Dolphin will be listed in the K Menu will not harm your user experience in any way."
    author: "Troy Unrau"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "\"I know that there are a lot of people out there that really love Konqueror.(I'm one of them)\"\n\nLoL! What is puzzling is I still cannot find a real user who would say \"I need my file manager crippled, and thus I prefer Dolphin.\" People moan when Kubuntu hides some folders, imagine what moaning you will get when suddenly smb:// ftp:// or other non-local paths stop working in the default file/resource browser.\n\nIs there ANYONE out there who can truly say \"By default, I (focus is on I) need a browser that puts tight limits on what is possible to do with files.\"?\n\nFor a lot of web-enabled people, there is NO separation between remote and local resources. Once we get to metadata / tag / DB enabled filesystems, there will no longer be a need for \"file manager\" I just hope I get my search-enabled interface to my data: files, contact info, messages etc. This way Dolphin will hopefully be the last thing I would have to use on my system."
    author: "Daniel"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "Dolphin will do smb and ftp, worry not :)"
    author: "Troy Unrau"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "Seeing as KIO is integral to KDE's file handling there's no way it would just stop working.\n\n> By default, I (focus is on I) need a browser that puts tight \n> limits on what is possible to do with files.\n\nBut that's not what Dolphin is supposed to do - in fact, by not having to worry about also being a web browser, it can offer an interface better suited to file management."
    author: "Paul Eggleton"
  - subject: "Re: Konqueror"
    date: 2008-03-18
    body: ">But that's not what Dolphin is supposed to do - in fact, by not having to worry about also being a web browser, it can offer an interface better suited to file management.\n\n Please explain how Dolphin is better suited to file management? What about it makes it better? Be Specific."
    author: "Steve"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "1. Dolphin already does ftp:/ and sftp:/ (I don't have smb:/ to test). It also does everything in remote:/, which includes Bluetooth stuff and Samba shares.\n\n2. Dolphin doesn't put tight limits on what you can do with files. It only sort of limits what it, the application, presents to the user, that is, limiting the UI to file management functions. Whatever file *management* (emphasis on the management) functions you need, they are there. Management doesn't directly involve embedded viewing. It's just an added bonus in Konqueror, thanks to KParts.\n\n3. \"Web-enabled people\" is a bit vague. Does this term refer to people who can surf the Web? Some of these people don't even know remote vs. local. In fact, they don't even know about remote files. Now, if you're talking about people who knows about servers, remote locations, FTP's, etc., then you're talking about advanced users, people who can configure their system, who can change the default file manager from Dolphin to Konqueror.\n\nPeople don't really say \"I want a crippled file manager.\" Some do say that they want a file manager that is separate from the web browser, but still shares in the benefits of KDE (KParts and KIO). Dolphin is for these people. Dolphin answers almost all the basic file managing needs. If you want more, Konqueror will always be there.\n\n(And I do want more!!)"
    author: "Jucato"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "Well 95% of people using a computer are used to a a file manager that does web browsing and have no issue with that, are used to have an explorer-like windows on the left side and are OK with that, are used to have no crumbread thing or whatever but a path field and are OK with that.\nWhether Gnome and OSX have switched to a different way of doing file management does not mean it's a good thing, I know a good deal of people, computer-saavy or not, who find it irritating at best. I don't think there has been any serious study showing that's actually better way of doing it and the fact is that most people actually do not use such stuff.\nSo making it the default seems a strange idea to me if it's supposed to bring new users, it doesn't seem to have really brought any new user to either Gnome or OSX. It actually make things a bit more different for people coming from Windows, and that's usually enough to have them back.\nAnd if the answer is \"well you can personnalize Dolphin to look like Konqueror/Explorer\", what's the point making Dolphin the default then?\nPeople will probably tell me that Konqueror will not go away, that I'll still be able to do whatever I've done with it in the past just the same, etc.\nOK, fine. \nBut I don't think than setting the file manager that looks like something about 5% of computer users are used to is such a well thought-out decision, and the arguments have been provided by proponents of this idea(\"simpler is better\", some people want a separate app\") look more like gut feeling than based on any solid poll, statistics or study.\nWhy not a wizard at first start of KDE asking if you prefer a OSX-like file manager or a Windows-like as default? This way everybody can have what he's used to."
    author: "Richard Van Den Boom"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "> Well 95% of people using a computer are used to a a file manager that does web browsing and have no issue with that\n\nGranted you are referring to Windows, how many times does windows open a link from an external application inside Windows Explorer? While Internet/Windows Explorer are much like Konqueror, in that they both do file management and web browsing, the distinction between the two is clear and persistent. Things don't get mixed together. Konqueror's Profiles are not as strong.\n\n> And if the answer is \"well you can personnalize Dolphin to look like Konqueror/Explorer\", what's the point making Dolphin the default then?\n\nI think no one said this (might be wrong, though). It was just said that Dolphin will be equally configurable as an KDE app is. Of course, within the limits of it being a file manager.\n\nDolphin was never meant to be a clone of some other file manager. Its aim is to be a \"file manager for KDE focusing on usability\". If it incorporated some features that are similar to other file managers, it doesn't do so for the sake of imitation, but because of their usability."
    author: "Jucato"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "OK, I agree that Konqueror's profiles would need work, and I also know that it may not be as easy as it sounds, depending on how the app was coded in the first place. I understand pretty well that sometimes, starting again from scratch is often the best way to improve a software, so I will never complain about people trying to do something different, especially in open source.\nOn top of that, I'm perfectly able to change the default file manager if the option exists, so I don't expect to be lost.\nThe thing is : the little \"usability\" I can sort out from screenshots (I don't have the possibility to test Dolphin right now) seems to imply a heavy influence from Gnome/MacOS. It may not be the only source of inspiration, I don't know, but the general look does. It may seem more usable to some, but there's probably as many people, if not more, that will prefer a standard Explorer-like window and Konqueror is a better match for them.\n\nTo answer your question, every FTP request is actually opening an Explorer-like window, in which you can drag and drop from local explorer windows, just like Konqueror. \n\nWhat I mean about all this is : for all the examples of people I know who would prefer a Konqueror-like file manager, you will probably find as many who will prefer Dolphin. I'm perfectly OK with that, Open Source is about choice after all. But I think imposing Dolphin as default leaves as many people \"in the dust\"  and not just power-users. Providing the choice between the two styles of file management at first start would be the option I would choose personnally.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "Okay, compromise time.  Dolphin is the default file manager.  It was also clearly stated (and even shown in a picture) that it can be switched to resemble the current status quo (toolbar, path bar, file pane, sidebar).  I think there's significant weight to the point that you want to cater to the majority while still expanding their horizons- they can switch when they're ready, if ever.  To wit, make the \"classic view\" the Dolphin default, and allow people to customize from there.  That way, everyone is happy.\n\nIt should be noted, however, that I like tabs.  I like a path.  I like embedded document viewers.  They all help my workflow.  And really, why would you spend all that time making Okular if you can't use it with Kparts to show a pdf in a file manager tab?  While I appreciate the Unix philosophy of \"one application per task,\" it only works when a user can create a positive environment to work in with it.  This, I find, is accomplised through developments like Kparts, Phonon, and Solid.  That's a damn good question, actually- how will Kparts be extended and improved in KDE4?"
    author: "Wyatt Epp"
  - subject: "Re: Konqueror"
    date: 2007-03-03
    body: "I think I am in a mid place here. I somewhat like the integration between the file manager and the web browser, while at the same time I feel in the case of Konquieror this integration is not completely well done.\nI agree with the fact that Konqueror's profiles are not extensive (they don't affect application behaviour as much as they should).\n\nI like the fact that when I'm browsing the internet I could see a PDF just in the browser window, but I really dislike the fact that when I'm looking for an image in the file manager and double click over an image name, the image opens in my file manager window. The problem in this respect with Konqueror is that you can't define distinct behaviours for file types for file management and web browsing modes.\n\nThe other complaints I have with Konqueror is with it's tree view sidebar when in file management mode, because it lacks some basic functionality.\nThere are really two things that I don't like:\n1) not beaing able to rename a directory in the tree view\n2) the tree view don't always follow the directory showing on the right side (hidden ones, etc.)\n\nI think Dolphin could be a welcome addition to KDE if done right (make it simple and coherent with what has been file management inside KDE until now), but at the same time I tend to think that there were so little things that separated Konqueror from being the perfect file manager/web browser that it's a pitty hackers hasn't been able/willing to correct or add them for KDE 4.\n\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Konqueror"
    date: 2008-06-20
    body: ">>> I like the fact that when I'm browsing the internet I could see a PDF just in the browser window, but I really dislike the fact that when I'm looking for an image in the file manager and double click over an image name, the image opens in my file manager window. The problem in this respect with Konqueror is that you can't define distinct behaviours for file types for file management and web browsing modes.\n\n\n1. I guess most people want to view a image file really fast and quick instead of open it for editing in GIMP.\n\n2. If you really prefer defaulting to open an image file in an seperate application, just configure it in Konq:\n  Right-Click on the file name to open the context menu, select 'Property' and press the 'troque' icon to open the MIME configuration dialogue. Here, select the 'Embeding view' tab and change the default behaviour to 'open in seperate app.'. \n  You can even have different setting for different type of image file, is it sweet?\n\n\n"
    author: "konqueror lover"
  - subject: "Re: Konqueror"
    date: 2008-06-20
    body: ">>> The other complaints I have with Konqueror is with it's tree view sidebar when in file management mode, because it lacks some basic functionality.\n There are really two things that I don't like:\n 1) not beaing able to rename a directory in the tree view\n 2) the tree view don't always follow the directory showing on the right side (hidden ones, etc.)\n\n\nFor your 1) complaint. It actually support 'renaming' but you must bring up the context menu and select 'rename' to do it. Pressing 'F2' or Press&Hold left button don't work as they should.  A minor bug."
    author: "konqueror lover"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "thats actually not true at all. most windows users don't even know that the explorer is the same thing as the internet explorer. and when you look at it as a user, it really isn't. \n\ninternet explorer and explorer look completely diffrent. they don't share toolbars, the don't share options. they don't share anything besides the same executable.\n\nthe only way to notice that these are the same is when entering a web url in your normal explorer view. though what you get is en explorer suddenly shifting into internet explorer - not an explorer displaying a webpage. also the explorer doesn't show files, even if you click on a local html side it starts a new internet explorer instance.\n\nalso, when you click on \"my computer\", and thats what most new users use, because its more or less the only filesystem related thing by default on the desktop windows starts a explorer without the treeview. you only get the treeview when you search for the \"real\" Explorer in your start menu or use a special shortkey/rightclick on the startmenu - something normal windows users wont do.\n\nthough actually i don't care what windows users expect. there should be reasoning to do something like you do. not just because everyone else does it this way.\n\ni don't think viewing documents(including webpages...) has anything to do with managing your filesystem. i never created new \"things\", or move \"things\" around, while reading the dot. my webpages don't resemble a graph or tree. so i don't think i should use the same program for both... it just doesn't make any sense....."
    author: "ac"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "Well I have both of them opened right now on my Windows desktop here at work :\n\nYou have exactly the same menus names, exactly the same position of tool bar, path, and OK button, the same previous and next icon at the same place.\nYou basically have several icons added to the browser version but that's about it.\nThere is a Favorites menu in the file manager window, with all the links to websites I recorded.\nI don't know for you but most people I know actually switch to treeview once they know about it. I perfectly agree that my experience is just one data point but nobody seems to actually be able to provide anything else here anyway.\nMoreover, when they get to FTP sites, seemlessly from their browser, people get a file manager view and usually like it, that one of the complains I hear most often against Firefox.\n\nThe argument of taking into account Windows users is an argument against the \"simple is better\" one : people not always prefer what is simple but what they are used to, especially non-power users. I've seen many Windows users being baffled by OSX file manager and finding it not practical at all. So if you plan to make things easier for people, their vision should be taken into account too.\n\nGoing on the web is not just watching web pages but also sending and receiving files nowadays. Having the possibility to do file management through the web thus makes perfect sense. And in that view, having two different apps doesn't.\n\nIt just come down to what you're doing and how you want to do it. Having both solutions is OK to me, but I think you consider that many people, and not just power users, are used to browse their files in a certain way and that KDE should provide a way to do it in a straightforward manner, not through hidden options."
    author: "Richard Van Den Boom"
  - subject: "Re: Konqueror"
    date: 2007-03-03
    body: "Don't worry. Vista acts exactly as OSX and Gnome (and as KDE will do). So 99% of user in 3-4 years will be experiencing separate file/web browsing"
    author: "Vide"
  - subject: "Re: Konqueror"
    date: 2007-03-07
    body: "Let Vista acts as it wants. But WHY should KDE copy Vista (or OSX, or GNOME, or whatsoever else)? Why? Many people like KDE as it is. Don't turn it to another bad copy of Mac, like gnome already is. Let there be at least one _normal_ DE, please.\n"
    author: "vicza"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "_I_ would like a separate file manager. I like konqueror very much but still. I certainly prefer external file viewers (pdf _really_ annoys me when opened in konqueror- when I mistakenly use the left mouse click). \nI also do not like having my Web bookmarks in my file browser."
    author: "pizorn"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "You would like a separate file manager just for a configurable setting you don't like?"
    author: "Pino Toscano"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "When I heard the news about a simplified file manager instead of Konqueror the first thing I thought was : \"Oh no ! I moved out from Gnome and they gonna do me the same stalinian treatment, soon, it will be our way or no way, hidden editable paths, reversed buttons, configurability to what *we* think it is best for you..\"\n\nBut I tried Dolphin and I understood immediately the benefits ! \n\nI consider Konqueror the best web browser ever, but it is really confusing for me, for example :\n- you grab a window you never know in which \"profile\" it is so trying to fit a profile to a purpose is useless\n- what is home ? I have 2 homes, my google home page and my /home/user\n- what is the default page, should I put one in the profile ?\n- you have the focus on a specific file in the file browsing mode, shift+down arrow then you have a \"really selected one\" video reversed and a \"current one\" just with a border.. you do a ctrl-C, which ones are copied ?\n\nWith dolphin, everything is clear and that's what I need when I move around important files !\n\nFor whose who are sceptical like me, just try it ! associate it as your default file handler for directories and try it a bit, in no way it is a gnome like regression.\n\nGuillaume\n"
    author: "Guillaume BINET"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "I've never seen the point of having a \"home\" webpage. Just enable the bookmark toolbar and put your frequently used webpages in the root directory.\n\nIt is true that the profile system in Konqueror is a bit messed up but this could be fixed, I hope the Dolphin project doesn't stop this from happening.\n\n"
    author: "Evan Robert"
  - subject: "Re: Konqueror"
    date: 2007-03-03
    body: "Well, I for one work for an Internet website and I find it useful in having a default webpage opened whenever I launch my browser, instead of having to type it in or click on a bookmark..."
    author: "Francesco"
  - subject: "Re: Konqueror"
    date: 2007-03-04
    body: "Konqueror already does this. Open the page or directory you want to be the default page and then save that profile. Everytime you launch that Konqueror profile, you get that page.\n\nThe problem starts when you try to return to that page when you try to press the Home button. Unless you set it to a different location in Konqueror's settings, it goes to the user's home directory. While this is ideal in the file management profile, it's a bit unnatural in a web browsing profile."
    author: "Jucato"
  - subject: "Re: Konqueror"
    date: 2007-03-03
    body: "You brought me to an idea that could be a killer feature for Konqueror of KDE 4:\n\nHow about modifying the target of the home button that way that it displays a nice HTML page with your bookmarks/favourite places? This could be easily done with a (local) KIO-Slave called \"bookmarks\" that delivers a HTML page.\n\nIf you look around in the internet many web sites want to be your \"home\". They are portals that you can personalize in order to make them your \"base camp\". The downside is: You are locked in, reveal a lot of your personal data (habits, interests, whatever) for data mining and you have to accept that these sites want to promote their stuff which is not necessarily the stuff you want.\n\nNow imagine a \"home\" which is feed by you own browser history and browser bookmarks. For example boxes representing your bookmark folders, screen space weighted by a tag cloud mechanism (taking the number of your views of these bookmarks as calculation basis) and of course if you like, tweaking this \"home\" page to your own needs by yourself...\n\nThis thing would provide *much* more than Windows Live and friends would ever be able to provide to you while being at the same time:\n* Much more intuitive and usable, cause the page is directly feed by your browsing habits and without needing to create yet another strange account somewhere on the web.\n* Best possible privacy. Your personal browsing data never leave your computer in aggregated form. You don't need to trust anybody but you that they don't do data mining on these data... And of course this local home page is not restricted by privacy laws. ;-) You can do personal data mining on your own as much as you like in order to dynamically customize your home to your needs.\n* No customization - site provider tradeoffs. You don't need to accept ads, preselected search engines, pre selected whatever pages the site provider wants to promote in your customized home.\n\nHow about that idea?"
    author: "Arnomane"
  - subject: "Re: Konqueror"
    date: 2007-03-04
    body: "ooooh, shiny."
    author: "Chani"
  - subject: "Re: Konqueror"
    date: 2007-03-04
    body: "\"The fact that Dolphin will be listed in the K Menu will not harm your user experience in any way.\"\n\nIt's not the fact that it is listed in the file manager that bothers me.  It's that (for most users) they will believe that the \"file manager\" for KDE is just as broken as it is in Gnome.  When did we start to confuse a lack of functionality as a feature?\n\nI have heard almost NO ONE (at least no one who is not already totally convinced to use Gnome regardless of what we do) who thinks that Konqueror web integration was a bad thing.  Why are we changing this?  Why?  \n\n0h, and just as an FYI.. someone type /home into your firefox browser... or open an html page in Nautilus... OMG web and file integration!\n\nBobby"
    author: "Bobby"
  - subject: "Re: Konqueror"
    date: 2007-03-04
    body: "I have to agree.  Dolphin seems like a solution in search of a problem.  Every single Konqueror complaint I've read in this thread seems more easily addressable by changing the respective KParts than by creating a separate file manager.\n\nMaybe I'll change my mind once I try it, but from what I've read so far, Dolphin seems to be quite a big step in the wrong direction.  Konqueror is a fairly capable web browser (though it has at least two severe rendering problems: bad CSS and major slowness), but it seriously rocks as a file manager.  I'll try to keep an open mind, but my gut instinct is that Dolphin is going to cause more problems than it solves.\n"
    author: "Tony O'Bryan"
  - subject: "Re: Konqueror"
    date: 2007-03-04
    body: "Actually, I can't say that the CSS problems I have are due to Konqueror (KHTML).  It's entirely possible that the problem is with web sites that have bad CSS.  However, Firefox renders those sites correctly.\n"
    author: "Tony O'Bryan"
  - subject: "Re: Konqueror"
    date: 2008-01-14
    body: "I agree with all the positives that have been said for Konqueror and love the integration and file viewing flexibility. When I am doing this and need to include viewing web documents it is perfect. When I just want to do some web browsing I use Firefox.\n\nPlease don't hide this easy to use and wonderfully flexible piece of software from new users. If anything, it should be treated as a flagship.\n\nolaf"
    author: "neal"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "> that their file managers are not as capable as Konqueror\n\nwhat capabilities are you concerned will be left behind? \n\nyou see, the goal is not to create a crappy file manager. it's to create a file manager that has just that one purpose in life. that means it needs to be capable and include the best features kde makes available to it.\n\n> only users I hear complain about Konqueror\n\nthis isn't in response to complaints. it's in response to the usability of konqueror as tested with real people.\n\n> then why not simply use Krusader\n\nkrusader is an interesting tool with a very dedicated following. however, to make it usable for the general user base and make it feel modern would require some massive changes. i don't think that's fair to the project's users or developers. they know what they want."
    author: "Aaron J. Seigo"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "> what capabilities are you concerned will be left behind? \n\ndirectory tree."
    author: "?"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "It seems that the missing directory tree is one of the most requested features of Dolphin. I'll start implementing a prototype during the next days, so that Dolphin offers a dock for such a tree. It's also planned to integrate Qt4.3'\ns ColumnView into Dolphin (see Icefox' blog http://www.kdedevelopers.org/node/2701 for details).\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "> It's also planned to integrate Qt4.3's ColumnView into Dolphin (see Icefox' blog\n> http://www.kdedevelopers.org/node/2701 for details).\n\nNice. I'm eager to see this in dolphin!\n"
    author: "infopipe"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "nice! I must say I did expect you to do this, as it's a pretty basic filemanager feature. And I have all the confidence in you and the other dolphin hackers to expect dolphin to be very cool. I might even use it, though that's not very likely as i often deliberately mix webbrowsing and filemanagement (drag'n'drop a local or ftp or fish file into a html input field like gmail's using split-window for example).\n\nAnyway, I think this will benefit KDE enormously."
    author: "superstoned"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "I suppose some people will love QColumnView, but you really really must include the traditional tree view as well. "
    author: "Ben"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "\"It seems that the missing directory tree is one of the most requested features of Dolphin. I'll start implementing a prototype during the next days, so that Dolphin offers a dock for such a tree.\"\n\nEverybody claiming that the incorporation of Dolphin is a sign of the \"GNOME-ificiation\" of KDE would do well to read this post.  There's no reason - none at all - why Dolphin shouldn't be a very powerful file manager, perhaps surpassing Konqueror itself, which must sometimes make concessions due to its dual-nature and resulting large code-base."
    author: "Anon"
  - subject: "Re: Konqueror"
    date: 2008-01-31
    body: "Posting one year later:\n\nThe dirtree in Dolphin sucks.  No horizontal scrolling (bad when you start to get far down a tree), no way to choose the root of the tree (it changes it for you depending on where you go in the filesystem.. brilliant?), and the root of the tree *isn't even shown*.  No copy and paste on the directories shows in the dirtree.  It is inferior in every way.  *Nautilus*' dirtree is better.  \n\nIt's clear the dirtree was added just to get those of us who complained about it's absence to shut up, and was promptly left to rot.  It was added by people who obviously don't care about it or use things like it (since they initially left it out) and it shows."
    author: "AS"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "embedded console part"
    author: "FH"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "It is planned that this will be supported in the final version for Dolphin for KDE 4.0.\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "I personally am surprised that so many people fear that Dolphin will be worse file browser in any way than Konqueror.\n\nMy fear is that once Dolphin gets so good like it already looks, that my beloved Konqueror gets neglected in the file management domain.\n\nTo me it makes sense to e.g. have a file manager in one tab and certain web sites that relate to these files there. I like \"Copy to location\" menu from the context, etc.\n\nWhat I think people don't want to miss is what Kontact is to Kmail and Knode, with regards to Konqueror and Dolphin. The one thing that allows them to be tightly bundled. What surprises me is that instead of Dolphin, there wasn't a web browser spin off done, or both at the same time, with a unification plan.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "> what capabilities are you concerned will be left behind?\n\ntabs.\n\nSee my post below:\nhttp://dot.kde.org/1172721427/1172845277/1172856948/"
    author: "Phase II"
  - subject: "Re: Konqueror"
    date: 2007-03-04
    body: "I second that. Tabs should not get lost."
    author: "WPosche"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "\"If it really bothered people so much that Konqueror also did file management; then why not the wholesale conversion to Krusader?\"\n\nYeah, I was wondering that as well...Krusader is way underrated!"
    author: "fish"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "Ok hold on guys, if we really want this thing called dolphine, why should i use kde??? there is one already called nautilus which we all think SUCKS! so why copy it to KDE. KONQI is now the B E S T file manager ever... do not waste time on this fishy thing...\n\n\nThanks, \nkonqi "
    author: "edi"
  - subject: "Re: Konqueror"
    date: 2007-03-02
    body: "Nautilus sucks? Why? I never really used. So can you tell me?\n\nIs Konqueror the only thing you like of KDE? What about KIO-slaves, k3b, Amarok, Kontact, KDevelop, digiKam, krusader, KOffice, ...\nKDE is much much more than the \"simple\" kpart shell called Konqueror."
    author: "Birdy"
  - subject: "Nautilus"
    date: 2007-03-03
    body: "I think that the reason why nautilus isn't a good file manager is more than the lack of options.  gnome-vfs isn't really implemented in all GNOME apps, which makes opening remote files a hit-and-miss, and it isn't snappy enough to really justify using it all of the time."
    author: "Chris Parker"
  - subject: "I'm glad!"
    date: 2007-11-12
    body: "I use Firefox, so I'm happy to see Dolphin. It means I don't need to separately install Krusader (which is great, but a little slow at times).\n\nI hate it when distros get too far away from \"The Unix Way\". I don't want one massive utility shoved at me that does 500 things. My file manager should be a file manager. My web browser should be a web browser. They can interoperate, but I should be able to decide what tool I want for which purpose. I mean, why have Kaffeine or Amarok? Why not just play and view everything directly in Konqueror? Why not use it as my programming IDE, too?\n\nIf I used Konquror as my browser, I might feel different. I don't really know. Konqueror as a web browser seems to be weak in a lot of areas that I rely on \n\nAs far as file management, I'm not sure what Konqueror ever offered that Krusader/Dolphin do not."
    author: "no"
  - subject: "Re: I'm glad!"
    date: 2008-06-20
    body: "If you don't use Konq., then you will never understand why every of us call it the most powerfull file manager in all plateforms. If fact, I think Konq. is actually being used as an plateform itself, somewhat like emacs. \n\nNote this. I'm now in Koqn running in Gnome running in Ubuntu Linux running in VMWare Fusion running in OS-X 10.5. I tried to love Mac Finder and Nautilus but eventually retreated to my loved Konq.\n\nBut I do envy the Nautilus way of serching/locate file by just pressing the first few characters of the file name. Nautilus also has better decoding for CJK file name than Konq (It just works in Nautilus but needs some installation and configuration of proper font in Konq)."
    author: "konqueror lover"
  - subject: "Re: Konqueror"
    date: 2008-12-26
    body: "Bobby, I just moved to OpenSuSE 11.1  and it has been a nightmare. The main reason, I do not like at all the Dolphin file manager. I removed it and guess what!? Konqueror is broken! well, as you said, it is not broken but that is how it feels when all the good things that made it the best file manager, are gone. Until finding this forum, I thought Konqueror was broken, but now I know that somebody got the idea of dismantling it.\nA web browser? not that much. On KDE3+, Konqueror had so many problems to present websites properly... My main web browser? not at all. Firefox solved all the problems. But for FTP managing, konqueror was great, for file-managing, the best ever. \nSo I see as a total mistake to push konqueror to become just a webbrowser instead of using it for the best it has been for; a file manager.\n\nDolphin is so limited!!! Very often, I use more than 5 tabs in konqueror as a filemanager. Dolphins cannot. Konqueror's previews helped so much. Dolphin just wastes space on the screen, where Konqueror provided tools and valuable information. The integration with Digikam, so use to it. Fast and reliable. Specially for those that use tons of photo files to manage. But Dolphin cannot do it. So, I tried to manage Konqueror4 to look like in KDE3+ but no way... not even the view modes are available to be set back... Then do not even try to install the package for file size view mode.. and the device notifier widget still pushes for Dolphin, despite I removed, to open the devices and I see no section where to select the file manager. A nightmare... \nAll the problems that were presented in this introduction, I never had them as \"problems\" and I am also one of those loving the up button's behavior. And everything else like \"unwanted buttons\" on the bars, was able to be managed through the profiles instead of removing the power to the best file manager ever. Now I have an endless list of problems...\n\nIn my opinion, the KDE guys are moving Konqueror to the wrong direction. They are pushing it to its weaker side as a web browser instead of celebrating its best side as a file manager. Konqueror as file manager and extended capabilities   was the reason why I kept KDE instead of GNOME.\n\nSorry guys I am very upset. I should never had moved to KDE4."
    author: "polo"
  - subject: "We need a \"real\" browser"
    date: 2007-03-02
    body: "IMHO Dolphin is a great file manager that has just enough power for most users.\nKonqueror on the other hand is the ultimate tool the power-user can dream of; and a good browser too...\n\nBut, I see one problem with this setting. There isn't an easy to use browser for KDE. One that takes away many of the less used functions but offers a better usability.\n\n\nThis is (more or less) similar to Mozilla/Seamonkey vs. Firefox for that matter."
    author: "JAT"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "Firefox may have started as a 'lightweight gecko browser' but it definitely is not lightweight anymore :)  If we do the same thing to KHTML, it'll eventually end up as heavy as Konq is anyway."
    author: "Troy Unrau"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "Right, people will continuously ask for new little features until it's not a lightweight browser anymore, and if new features aren't added for the sake of staying \"light\", the browser will be criticized for not having sexy features.  It's a no-win for developers.  The combination of Konq and Dolphin should satisfy everyone just fine."
    author: "Louis"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "So the light browser of yesterday grows heavy, creating an opportunity for a new light browser to start. The browser renews itself like a Phoenix ;)"
    author: "Maarten ter Huurne"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "...then changes it's name - at least twice ;)"
    author: "mabinogi"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "You could just ship official pluggins that are disabled by default, letting people chose their weight."
    author: "Ben"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "\"But, I see one problem with this setting. There isn't an easy to use browser for KDE. One that takes away many of the less used functions but offers a better usability.\"\n \nKonqueror _is_ easy to use. Its usability is great in simple web browsing. I did put it under some \"powerless users\" (because if there is power users, then there is powerless ones) hands and they were very successfull with it."
    author: "Kleag"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "One tool for one job. I like this idea. That's why I do have two konqueror windows open. One for web-browsing, and one for file management (maybe I should switch over to dolphin or krusader). I nerly never mixed those two modes in one window. And I always start separate programs (kpdf, gwenview, ...) for viewing filecontents. That's why I'm very pleased with the decision of having a dedicated file manager.\n\nSo having a dedicated web browser would be a logical step. I would really appreciate such a program. And it should use webkit (well - a webkit/khtml unity). Always synchonising webkit and khtml is a waste of resources. One codebase would be a real benefit.\nAn easy way to start such a progam could be to streamline konqueror (web-mode). My first starting point would be the settings dialog (removing all file management elements)."
    author: "Birdy"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "I'm not sure I agree...\n\nIf by \"real browser\" you mean HTML renderer only, I can't agree. There are many pages out there that have links to FTP sites. And when you click them, what do you expect to see?\n\n1) a new window gets opened and you see your file manager\n\n2) you see an HTML rendering of the contents, with very little that you can do there\n\n3) you see the FTP contents like your file manager, but in the same window\n\nMy personal preference is #3. I don't see why Konqueror shouldn't load the file listing parts to display FTP."
    author: "Thiago Macieira"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "My personal preference would be #1\n\n#2 is very limited (can be seen with firefox)\n#3 desires a full file management. Ok, it currently exists and is nice. But at the costs of bloat of konqueror. And yes, konqueror is (after several trimming attempts) still bloatet (best example is the settings dialog)."
    author: "Birdy"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-02
    body: "Well, in KDE 4, you can have #1 (default) and who wants it can have #3. Isn't KDE great?"
    author: "superstoned"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-03
    body: "I think there are two very different usage patterns for FTP:\nA) to download files from (read-only access)\nB) to manage files on a remote server (read/write access)\n\nFor type B I agree a file manager is more useful than a web browser. But for type A switching to a file manager interface is not useful, since the user is not planning to manage files, only view or download them."
    author: "Maarten ter Huurne"
  - subject: "Re: We need a \"real\" browser"
    date: 2008-06-20
    body: "What make ftp so much different from NFS, SMB or AppleTalk? \n\nAren't they all invented to access remote files? Why bothered at the first place to develop NFS to make remote files feel and look like local files?\n\nAnd please think again why we now have 'fuse' to mount wdav, ftp, ssh, tarball and even svn so we can access these contents in our familiar file manager?\n"
    author: "konqueror lover"
  - subject: "Re: We need a \"real\" browser"
    date: 2007-03-03
    body: "#1 is how it is handled on Mac OS X: you click an FTP URL in Safari, it is added as a location/mount and a Finder window is opened. It works, but the switch to a different application feels a bit weird to me.\n\nSince both Konqueror and Dolphin would be using KIO and thus have to ability to handle FTP, why not simply open FTP URLs in the same application in which they are activated? So if you click an FTP hyperlink in Konqueror, the site is opened in Konqueror (#3), but if you type an FTP URL in Dolphin, it is opened in Dolphin."
    author: "Maarten ter Huurne"
  - subject: "Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "Is a left-pane Tree View planned? I think a Tree View is the #1 requirement for a file manager -- it makes orienting through the filesystem hierarchy and moving files around so much easier."
    author: "Vlad"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "I'm looking for this feature as well. The breadcrumb widget is good for navigation, and the split view for moving files from one directory to another. But I don't think it's a power user thing to be able to drag a file/directory directly to another directory, right?\n\nAs Dolphin still isn't in its final form, I'm hoping to see more changes soon."
    author: "Jucato"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "Hm, how about allowing the user to drag into the breadcrumb?\n\nOf course it would also need a way to navigate into sub-folders while dragging like Konqueror works today.\n"
    author: "Olaf Schmidt"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "Also, it should be possible to drag from any part of the breadcrumb."
    author: "Olaf Schmidt"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "Nice idea's, I hope the dolphin dev's pick it up. Anyway, treeview is coming ;-)"
    author: "superstoned"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "I've never used treeview in all my life in a file manager and I'm quite happy with it. So your #1 requirement is not mine.\n\nThere is a reason if in every file manager treeview panel is hidden by default (if present at all)"
    author: "Davide Ferrari"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "Yes, it is planned to support a tree view in Dolphin (an additional dock will be offered). It seems to be one of the most requested features :-)\n\nPeter"
    author: "ppenz"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "I hope that Dolphin's treeview will recognize the main view such that if I change the directory in the address bar, the tree view autoamtically changes as well. \n\nThis is the only thing bugging me on konqueror which does not recognize anything."
    author: "Sebastian"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "I can't reproduce this behavior in Konqueor. If I click on a folder in the main view, the tree-list view changes to the appropriate folder as well. Same thing if I type in the directory manually in the location field.\n\nKDE 3.5.6 Kubuntu 6.10"
    author: "Jucato"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "It does not work:\n\nOk,  Home-Folder Tree view seems to work, but not: \no switching to upper directories does not switch to \"root folder view\".\no switching KIO-slaves does not update the tree views (for examples in the service->media tree on \"media:/...path/\")"
    author: "Sebastian"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "I can reproduce the problem in Konqueror where changing the location in the URL field doesn't update the tree view on the left. I think part of it has to do with the \"Home\" sidebar being selected, as opposed to the \"Root\" sidebar.  For example, /tmp is not under \"Home\", so there is no /tmp node to highlight.\n\nI think this whole problem can be resolved if there is _NO SIDEBAR_, but instead just a Tree View with a root node at \"/\". Kind of like MS File Explorer does it."
    author: "AC"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2008-01-31
    body: "One year on:\n\nThanks a lot for this you jerk.  Dolphin's dirtree now changes it's (undisplayed) root depending on where you are in the filesystem.  So it's basically impossible to use it to copy things from one device to another.\n\nOld way: Be wherever in the filesystem.  Type in a location on a different device.  File pane goes there but dirtree doesn't react so you can copy a file from the file pane and paste into a folder in the old location.\n\nImpossible now.  Typing in a location isn't available by default.  The dirtree changes its (non displayed) root so you can't paste to the old location, but that's ok since you can't paste things in the dirtree anyway.\n\nDolphin is ass."
    author: "AS"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-03
    body: "Also when you browse to a hidden directory, Konqueror's tree view can't follow you.\n\nAnother missing thing from Konqueror's tree view is the ability to rename a folder pressing 'F2'. It's often annoying to have to click in the right view just to rename a folder when you already have it selected in the left (tree) view.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "> Yes, it is planned to support a tree view in Dolphin (an additional dock will be offered). It seems to be one of the most requested features :-)\n\nGreat to know! I don't think there's a need for multiple hierarchical trees like the \"Home\" and \"Network\" in Konqueror. But I definitely like Simon Edward's idea __and patch__ for how to integrate KIO slaves into a single hierarchical tree:\n\nhttp://www.kdedevelopers.org/node/2231\n\nCould this be used in Dolphin?"
    author: "AC"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2008-01-31
    body: "This is basically what Nautilus does.  Not Dolphin though."
    author: "AS"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-02
    body: "I use the TreeView a lot too. I would really miss it in a file manager like dolphin. The only way I don't miss it, is a MidnightCommander/Krusader like interface.\nHaving a TreeView should definitely be an option."
    author: "Birdy"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-04
    body: "I'm in the other camp.  I keep the left pane tree view turned off 100% of the time, as I consider it a nuisance and of limited use.  Konqueror's split panels are many times more useful and powerful.  Split panes give me a much bigger target when moving files around, while the tree view is more like trying to hit the head of a needle from the roof of a two story building.\n"
    author: "Tony O'Bryan"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-03-19
    body: "For me file manager is one of the center key item of an os and I could not imagine working with files without a tree view. Konqueror tree view is great and as I was reading that Dolphin was to become next default file manager of KDE I had to try the current version and I was shocked to find out that there was no tree-view.\n\nThen I searched some and found somebody mention here that tree-view is under construction in Dolphin - what a great news! Otherwise Dolphin seems to be a very lightweight and promising tool for managing files and I'm sure it will get alot of improvement before we will see it with KDE4."
    author: "Miika Koskinen"
  - subject: "Good Lord this is ESSENTIAL!!!"
    date: 2007-03-23
    body: "I have literally *combed* the internet looking a file manager I could use on Linux that has a tree view.\n\nHaving come from windows (read explorer) years ago I was looking for something that had the same functionality, tree view to the left that had Local folders, network folders, hot plug and devices all at the top level and drillable. I was *amazed* that of the *dozens* of file managers *none* seemed to do this. Then I finally discoverd that with configuration Konqueror could at least almost do this , but in a fragmented fashion, arrrghh! wtf. How can there be two dozen fms that do twin pane view but no *decent* tree views?!!? \n\nTo date I have found nautilus's tree view (ugh), Xandros done well but only on their system (ugh),thunar now on the scene, is the closest thing to a tree view that roots a top level for the main areas, home folder, system folders, and media.\n\nFor the love of all that is holy, *i* *am* *begging* you, please make your tree view a 1st class citizen.\n\nIt makes me *shudder* to cite anything windows as a positive example, but if you will make a tree view as functional as Explorer or Xandros fm, I will wash your car for the rest of your life, I will pay cold hard cash, seriously name a bounty and I will attempt to raise it. You need anyones legs broken I'm your man.\n\nJust name it.\n\nAnd to the people that feel a need to post to this thread to say that they don't want/need a tree view because a twin pain view is sooo much superior, I want to reach right through my monitor and strangle you all.\n\nYOU ALL ALLREADY HAVE *DOZENS* OF FMS TO CHOOSE FROM! STOP TRYING TO DISCOURAGE THIS!!!\n\nSorry for the shouting, my doctor says I am making progress.  :)\n\nBut seriously, I am begging and I will raise a bounty.\n\njust say the word."
    author: "Clifton Hyatt"
  - subject: "Re: Good Lord this is ESSENTIAL!!!"
    date: 2007-07-01
    body: "Will you really...:) "
    author: "riddle"
  - subject: "Re: Good Lord this is ESSENTIAL!!!"
    date: 2008-07-31
    body: "Well, there's always Konqueror! It still has its file manager part working in KDE 4.1. And guess what, just like in KDE 3.5, it STILL has tree view in KDE 4.1!\n\nHere's how to get the tree view in Konqueror:\n\n1) go to file manager mode (not web browser)\n2) in settings menu, enable navigation panel\n3) click on the red \"root\" icon\n4) tadaa! tree view\n\nI'm using Konqueror instead of Dolphin now, I mean, I didn't find the same in Dolphin, and this is near august 2008 instead of the 2007 where this thread started...\n\nKonqueror still rocks!"
    author: "K"
  - subject: "Re: Tree View is a must for any filemanager"
    date: 2007-07-01
    body: "It ***IS*** available in the current svn. Just right-click on the top of the 'Places' bar and choose 'Folders'."
    author: "riddle"
  - subject: "AppArmor & systrace"
    date: 2007-03-02
    body: "Finally! :-) The problem with just one tool for different purposes is that it's extremely difficult to apply security mechanisms such as AppArmor or systrace. I really want to be able to restrict konqueror with some good profile in systrace or AppArmor but in that case it becomes useless as filemanager. Now Dolphin can take over that part! Great work!\n\nkind regards,\nTobias W."
    author: "Tobias Weisserth"
  - subject: "Re: AppArmor & systrace"
    date: 2007-03-02
    body: "But you'll still need abilities like \"Save Image As...\" and printing to PDF in a web browser, so it should not be completely isolated from the local file system. Maybe it could use some kind of internal privilege separation though."
    author: "Maarten ter Huurne"
  - subject: "Re: AppArmor & systrace"
    date: 2007-03-02
    body: "The solution klik and zeroinstall might use, where the file save dialog is actually another application granting access to the file the user selects is imho pretty smart. Find more info on http://plash.beasts.org/"
    author: "superstoned"
  - subject: "Metadata editor"
    date: 2007-03-02
    body: "I think Dolphin could add file metadata editor or color tag feature for usability."
    author: "Ranger"
  - subject: "Re: Metadata editor"
    date: 2007-03-02
    body: "A whole Nepomuk project working on it ;-)"
    author: "superstoned"
  - subject: "Webkit"
    date: 2007-03-02
    body: "Speaking of khtml.. what's the status of merging webkit?"
    author: "Andi"
  - subject: "Re: Webkit"
    date: 2007-03-02
    body: "Webkit and KHTML share code back and forth.  Basically, Webkit is KHTML but with all the Qt and KDE library calls being abstracted away.  So, other than that code, the rest is shared pretty easily between the two projects.  As improvements and fixes hit Webkit, they make their way into KHTML as well.  And vice versa.\n\nSo, Konq using webkit would actually be a disadvantage, since we'd basically be abstracting away our own Qt and KDE library calls.  Using KHTML makes more sense, as we can then do things like the above mentioned trick, letting you put ftp or fish urls into an HTTP upload form.  Using webkit, this would probably vanish.\n\nRendering components are almost entirely shared.  Many thanks to Apple, Nokia and other webkit using organizations for remaining true to the open source community."
    author: "Troy Unrau"
  - subject: "Re: Webkit"
    date: 2007-03-02
    body: "> Webkit and KHTML share code back and forth\n\nyes. they also don't share a lot of code back and forth.\n\n> As improvements and fixes hit Webkit, they make their way into KHTML as well.\n> And vice versa.\n\nthis simply isn't true. bug compatibility is another big issue as the diversity there makes it harder for web developers to produce sites that work with khtml.\n\n> we'd basically be abstracting away our own Qt and KDE library calls\n\nthe overhead of this is negligible and is dwarfed compared to the benefits gained from a single code base. e.g. more developers, a larger user base, more websites tested against our browser, etc...\n\n> Using webkit, this would probably vanish.\n\nwhat?\n \n> Rendering components are almost entirely shared\n\nit's not that close."
    author: "Aaron J. Seigo"
  - subject: "Re: Webkit"
    date: 2007-03-02
    body: "I stand corrected :)"
    author: "Troy Unrau"
  - subject: "Re: Webkit"
    date: 2007-03-02
    body: ">> Webkit and KHTML share code back and forth\n> yes. they also don't share a lot of code back and forth.\n\nBoth statements are wrong.\nCode is routinely merged back from WebKit to KHTML.\n\nIn contrast, code is never officially merged from KHTML to WebKit, because of the strong corporate NIH syndroma that dominates the WebKit project.\n\nWebKit people do take code from KHTML tree from time to time, \nsadly they just never give proper attribution as we do.\n\n>the overhead of this is negligible and is dwarfed compared to the benefits\n>gained from a single code base.\n\nThis is a silly dellusion.\nA *single* code based is not a *shared* code base.\n\nApple owns WebKit.\n\n"
    author: "Germain"
  - subject: "Re: Webkit"
    date: 2007-03-02
    body: "Wrong.  There are a number of very well known KDE/Qt developer's with full commit and review access to WebKit.\n\nFull commit and review\n----------------------\nLars Knoll (original creator of KHTML)\nNikolas Zimmermann \n\nFull commit and review to Qt port\n---------------------------------\nGeorge Staikos\nZack Rusin\n\nThis can only improve if more KHTML developers get involved with WebKit!"
    author: "anon"
  - subject: "Re: Webkit"
    date: 2007-03-02
    body: "don't get things all mixed up.\n\nHaving Qt be the only portable backend for WebKit (as the win port and the gdk port are basically dead now) is attractive for Trolltech.\n\nSo of course they are going to have a few people hanging around.\nThey do want WebKit to work with Qt.\nThey even opened  a 'Trolltech lab' page just for it.\n\nHowever those commiters, from their own admission,  have absolutely no bearing on the project. They can't decide anything, just like Nokia.\nEverything is owned and steered by Apple.\n\nPeople should just understand once and for all that WebKit is an external project that has no relation with KDE whatsoever.\n\nThey might produce a great browser working in KDE4, and that'd be swell and all.\n\nBut in no way does it invalidate the necessity for KDE to ship it's own independant engine.\n"
    author: "Germain"
  - subject: "Re: Webkit"
    date: 2007-03-03
    body: "\"Having Qt be the only portable backend for WebKit [...] is attractive for Trolltech.\"\n\nAnd for KDE as well.  And make no mistake, the Trolltech folks are just as interested in the benefits to KDE as well as Trolltech.  Look in the svn repository and you'll find a __WEBKITKPART__!!\n\n\"However those commiters, from their own admission, have absolutely no bearing on the project.\"\n\nNot so.  Lars Knoll has full commit and review access.  And the views and insight of these developers IS being heard and addressed.  See the recent thread on webkit-dev for instance.\n\n\"People should just understand once and for all that WebKit is an external project that has no relation with KDE whatsoever.\"\n\nThen what on earth is the WebKit KPart doing sitting in Apple's svn repository?  And why are _KDE_ developers actively working on it?\n\n\"But in no way does it invalidate the necessity for KDE to ship it's own independant engine.\"\n\nMaybe not right now, but who knows what the future will hold..."
    author: "anon"
  - subject: "Re: Webkit"
    date: 2007-03-03
    body: "> See the recent thread on webkit-dev for instance.\n\nnothing new there.\n\n> who knows what the future will hold...\n\nmy thought exactly."
    author: "Germain"
  - subject: "Re: Webkit"
    date: 2007-03-03
    body: "> code is never officially merged from KHTML to WebKit,\n\npot, meet kettle. how many times have we seen webkit patches rejected because they aren't \"good enough\" and then reformatted / rewritten? it goes both ways and both projects have their own internally consistent reasons for doing so.\n\n> WebKit people do take code from KHTML tree from time to time, \n> sadly they just never give proper attribution as we do.\n\ni hope you're not claiming they strip copyrights. that said, instead of trying to find one place everyone can work together you're using it as a reason to keep the non-functioning status quo. which seems amazingly .. odd.\n\n> A *single* code based is not a *shared* code base.\n\nyou're right. but it can be. unfortunately:\n \n> Apple owns WebKit.\n\nthat attitude will prevent you from being part of that solution. it's not that apple doesn't \"own\" webkit, it's that using that as a final destination description is defeatist and unnecessary.\n\nwhen those working on unifying things actually arrive with their working solution after all that effort, i really hope we are all able to recognize it."
    author: "Aaron J. Seigo"
  - subject: "Re: Webkit"
    date: 2007-03-03
    body: "> how many times have we seen webkit patches rejected because \n> they aren't \"good enough\" and then reformatted / rewritten?\n\nAre you serious?\n\nEach time we have taken the pain to merge even so little as a heavily elaborated-upon *one liner*, proper attribution of the initial work has been given. Always.\n\nBeing disrespectful to people's work is just not our way at all, and I find it very offensive to see such a statement in the mouth of a fellow KDE contributor.\n\n> i hope you're not claiming they strip copyrights.\n\nIn my book \"giving proper attribution\" is not about stripping copyright notices.\n\n> that attitude [...] it's not that apple doesn't \"own\" webkit\n\nHow could I argue on that irrational, head-in-sand \"let's have a positive attitude\" couplet?\n\nyes indeed, I have principles of probity, freedom and humanism that I'm not ready to whipe my bottom with for the sake of \"market penetration\".\n\nSorry 'bout that.\n\n"
    author: "Germain"
  - subject: "Re: Webkit"
    date: 2007-03-03
    body: "Really like to know what you are talking about...\n\nWhat, _precisely_ do you consider 'giving proper attribution'??\n\nMust Apple provide a paragraph of blustery thanks to the KDE community every time it gives a press release on WebKit?\n\nReally, what is this you are talking about??"
    author: "anon"
  - subject: "Re: Webkit"
    date: 2007-03-04
    body: "Nope, just that the few times they have adopted patches from KHTML, it would be nice if the changelog said: \"patch adopted from KHTML\" or \"Patch originally by Germain Garand ported for WebKit by WK-Fanboy1\" instead of \"Patch by WK-Fanboy1 approved by Hyatt\"\n\nAnother example could be instead of writing: \"The code for generated counters in KHTML is not good enough, so we have to start from scratch\" and then port all my code with some variable renames. They could write \"The code in KHTML is good enough, but we renamed some variables for clarity\""
    author: "Carewolf"
  - subject: "Just so you know"
    date: 2007-03-02
    body: "\"Konqueror is so advanced that you can enter an FTP URL into a HTML upload form and it just works as you would logically expect it to (as far as I know, it is the only browser which allows this).\"\n\nJust so you know, FireFox has an add-on called FireFTP which is pretty nice too. And you are right, it is a little different. It has to be added and it has its own address field. See http://fireftp.mozdev.org/"
    author: "Abe"
  - subject: "Re: Just so you know"
    date: 2007-03-02
    body: "you missed the point. here's the use case:\n\nyou're at a website and filling in a form. it has a location for uploading a file. you can put any url in there and it will fetch that url and upload it. it could be another http address, fish://, ftp:// whatever.\n\nit's not about being an ftp client, it's about being network transparent even when it comes to things like web form upload fields."
    author: "Aaron J. Seigo"
  - subject: "Re: Just so you know"
    date: 2007-03-02
    body: "even cooler, try this:\n\nyou have a html upload file. now split-screen (on the statusbar, rightmouseclick -> split screen hor/ver) and go in the one screen to your fish or samba files share or an ftp site. now just drag'n'drop the files into the input location, and hit 'submit' or whatever... things will just work. Now, you can't even do that with a LOCAL file in windows (try drag'n'drop a file to a attachment bar like under the response message you could write to me, won't work)..."
    author: "superstoned"
  - subject: "Re: Just so you know"
    date: 2007-03-02
    body: "Even better, in the kde file dialog you can chose a ftp bookmark an save your image thru FTP!\n\nI'm using this often when downloading photos from my digital camera, resise them in a batch and upload the results to my blog without having to do a local copy....\n\nOlivier.\n"
    author: "olahaye74"
  - subject: "Bread Crumbs"
    date: 2007-03-02
    body: "\"clicking and holding a breadcrumb item displays a list of directories that are at the same level as the one clicked\"\n\nJudging by the screenshot, you are wrong. It seems to display contents of the item you clicked... Yeah, confusing. The breadcrumbs is a dandy idea but we managed to copy the most brain-dead implementation to KDE.\n\nThe closest analogy to how one would expect this to work would probably be a pull-down-list HTML widget. In Dolphin, you click on path element, and instead of choosing a replacement for the item clicked, you add to the path as result... "
    author: "Daniel"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-02
    body: "please test this with users. i wasn't sure which way to go so i camped out at a coffee shop one day (aaah! caffeine!) and coerced people to try various things out. this was one of them. i'm not so much interested in arm chair theorizing, but if you have real data i'm interested in looking at it.\n\nbtw, this also works nicer with, say, $HOME since it showing other homes would be a bit odd and unexpected. as i found out at the coffee shop."
    author: "Aaron J. Seigo"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-02
    body: "Interesting new way of doing cheap usability studies :-)"
    author: "Thiago Macieira"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-02
    body: "Usability studies>  Probably time for me to leave then.  One of those produced the koffice startup/new file/file open dialog: the sole reason I still use koffice 1.4, and something that causes wonderful first impressions everywhere:\n\n\"Krita's startup window is downright annoying: I'm not interested in selecting RGB or CMYK or Grayscale at predefined dimensions or just a custom Document.\"\n\n\"The only thing IMO GIMP's UI does better for me, is creating a new image, their dialog for that purpose is a lot easier then krita's.\"\n\nboth not me, from http://osnews.com/comment.php?news_id=17366&offset=0&rows=15&threshold=-5"
    author: "?"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-02
    body: "well, then, it just shows people differ. I love the new startup window: you can immediately select previous files you have been working on, or create a new one, easy as pie. Love it, really. I wish this kind of startup dialog became the default in ALL kde apps (codeine has something similair, exept in a seperate window)."
    author: "superstoned"
  - subject: "KOffice/Krita startup dialog (was: Bread Crumbs)"
    date: 2007-03-03
    body: "Indeed the dialog is no improvement at all. Why do they place functions all over the place? Have they ever heard the principle to group functions?\n\nRight now it looks like game: hunt the button (featuring Krita).\n\nUse Case 1: just give me a new document\nquestion: where is the button?\nanswer: on the right side somewhere in the middle of the same window\n\nUse Case 2: open existing document\nquestion: where is the button?\nanswer: on the left side at the bottom and the bottom right side in the new window with the dialog\n\nUse Case 3: open recent document\nquestion: where is the button?\nanswer: recent documents are in the top left corner and the button is on the right side somewhere in the middle of the top half of the screen of the same window\n\nBTW The corresponding bug report is here:\nhttp://bugs.kde.org/show_bug.cgi?id=121233"
    author: "testerus"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-02
    body: "I hate Bread Crumbs and this is one reason my I don't use gnome. The file dialog needs tons of clicks to go to a place. Not speaking about entering directories starting with dot.\n\nBTW, how doeas it manage the devices:/ or media:/ copared to the file:/ in a \"Bread Crumbs\" way?\n\n- Who conducted the usability study?\n- Did non computer Science people participated?\n- did Windows users participated to the study?\n- Where are the results?\n- Is it a compilation of wishes from computer skilled reporters in a bugzilla database?\n\nHave read all the thread, no answer at all about this \"usability survey\"\n<troll>\nI though I was reading a gnome thread :-D\nAm I lost? surely a konqueror usability desing problem.... ;-)\n</troll>\n"
    author: "olahaye74"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-02
    body: "Looks like you introduced that <troll> tag too late in your post. It should have been at the top.\n\nI guess you forgot."
    author: "Chicken Blood Machine"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-03
    body: "> how doeas it manage the devices:/ or media:/ copared to the\n> file:/ in a \"Bread Crumbs\" way?\n\nget ready to gasp in delight: it shows the protocol in a drop down combo box at the front as if it were part of the breadcrumb! *gasp*!\n\nyou could've found that out by trying it, but i'm sure that would've been less fun, right? ;)\n\n> Who conducted the usability study?\n\ni did. why?\n\n> Did non computer Science people participated?\n\nat the coffee shop, none of them were comp sci people. i also tested it at home on some friends in the industry, just to make sure i also hit some people from the geek crowd.\n\n> did Windows users participated to the study?\n\nand macos users too!\n\n> Where are the results?\n\nnotes in a notepad. i also constructed experiments and fixes on the fly and had people try out the changes.\n\n> Is it a compilation of wishes from computer skilled reporters \n> in a bugzilla database?\n\nno, but keep digging. i'm sure you'll find a reason to discount the idea. it's obviously what you want to do.\n\noh, wait, i know! i did it on a tuesday. and usaiblity studies done on tuesdays  don't count in canada. totally forgot about that! *smacks the forehead*\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-03
    body: "I don't like \"breadcrumbs\"(*)\nWith this system you don't get a clear panoramic view of where you are in the file system and what other ramifications are there in it.\nI feel trapped, blind, when forced to use this paradigm in GIMPs open dialog.\nBesides that, you often end clicking many times just to select a folder (firstly to open menus and see if what you want is inside there, then when you find it, another time to select it), I think this isn't a progress in usability, but the other way round.\n\nPlease just don't make it the default option in Dolphin or in any other KDE part.\n\n\n(*) I'm curious, what a name! Can someone explaint from where it derives? :)\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-03
    body: "Basically it means that instead of being able to eat the whole bread, you only get to eat the bread scraps...  Some people like bread crumbs, but as you pointed out you don't get the full thing."
    author: "Anonymous"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-05
    body: "Interesting. I always thought it has to do with the tale of Hansel and Grettel, where the kids would leave bread crumbs behind them while walking through the forest, so they could find their way back later on. The widget does something similar, showing you the path you followed to arrive where you are, so it sounded like the same thing to me."
    author: "Fede"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-09
    body: "Hey! Hansel and Grettel? If I recall well, it was another tale called Pulgarcito (in spanish as I knew it in my infancy, don't know it's name in english, sorry) that left bread crumbs to mark it way.  :)\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-03
    body: "\"With this system you don't get a clear panoramic view of where you are in the file system and what other ramifications are there in it.\"\n\nShould be no different than the location bar in that respect.\n\n\"Besides that, you often end clicking many times just to select a folder (firstly to open menus and see if what you want is inside there, then when you find it, another time to select it), I think this isn't a progress in usability, but the other way round.\"\n\nAnd with the current location bar you CANT even do that... Whats your point?  \"Breadcrumbs\" are an alternative view the the KLineEdit currently used (which you can instantly turn into the KLineEdit if you want to rewrite parts of the path and not use the breadcrumb method!).\n\nAlso just so you know, the 'breadcrumb' feature in Dolphin is already much more advanced than the version in the gtk file open dialog."
    author: "Sutoka"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-09
    body: "Well, yes. You're right. The text location bar, doesn't give any panoramic view of the file system at all. I was saying that because I feared someones were proposing the \"breadcrumbs\" system as a replacement to the tree view system we already have. So in this respect it made sense to comment that.\nI think I wouldn't mind having the breadcrumbs AND the tree view together in the screen either.\nSo go ahead with the breadcrumbs if you maintain the tree view also!  :)\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-05
    body: "> get ready to gasp in delight: it shows the protocol in a drop down combo box at the front as if it were part of the breadcrumb! *gasp*!\n\nwhahoo, awesome, so if I missclick on media:/ when www.google.com is in the path, what is the result?\n\n> you could've found that out by trying it, but i'm sure that would've been less fun, right? ;)\nDidn't tested it because I wasn't aware of this move. More over, according to posts, dolphin is far from having most of its functionalities, so why testing a thing were many changes are already planed?\n\nRegarding the survey you've certainly forgotten the <joke> tag right? Do you realy think that your coffe shop represents the whole effective and potential KDE userbase?\n\nAs for Bread Crumbs (the initial subject of this thread) I still don't have answers to some basic usability questions:\n- How do you browse automounted entries that are not yet displayed?\n- How do you browse a directory if the parent directory is 711 chmoded?\n  (typicaly /home so a user can't see other home directories and permissions)\n- How do you browse directories that begins with a dot?\n- How do you copy/past a path?\n- What if the number of dirs exeeds the height of the screen in the dropdown menu?\n"
    author: "olahaye74"
  - subject: "Re: Bread Crumbs"
    date: 2007-03-03
    body: "> but if you have real data i'm interested in looking at it.\n\nSorry, someone had to ask, but: where is *your* real data? Doing some (informal) tests is nice but pretty much useless unless you document it... Who was tested, what was the test design, how did you proceed, where are the problems? Maybe there is another design that works even better than the ones tested. Unfortunately without the data we can't tell (and can't redo the test with alternative designs)."
    author: "."
  - subject: "Re: Bread Crumbs"
    date: 2007-03-02
    body: "It dosn't seem too bad to me. Then again I'm never leaving the good old keyboard url bar"
    author: "Ben"
  - subject: "The right direction!"
    date: 2007-03-02
    body: "I think this is a very wise and well-considered direction to go. I have used Konqueror as my default web and file browser after years of experimentation and I'm very happy with it.\n\nI think Konqueror can only benefit from having less burden placed on it to perform two usually different, but occasionally overlapping roles. And a dedicate file browser can focus on UI issues not present in a web browser.\n\nYet when you enable konqueror and dolphin to enjoy the same core technologies, you get a system that is powerful *and* makes sense.\n\nNow if Cervesia and the svn support in Dolphin could find similar accordance, it would be VERY impressive and more importantly, very useful. But the way KDE4 is being designed, this looks likely.\n\nI LOVE fish BTW. It's very productive for me - secure, drag-n-drop, network-independent file operations. FTW.\n\nThe ultimate lesson is, sometimes you need a complete, featureful interface for a given KIO. But convenient, integrated operations in other or more general purpose programs are incredibly powerful. Eg. shouldn't \"extract to\" be a context-click operation from a zip file in a browser?\n\nKDE4 looks very promising and capable of competing with any proprietary OSen in this regard. Architecture and framework enables all.\n"
    author: "JT"
  - subject: "fish://"
    date: 2007-03-02
    body: "I'm curious: is there are reason to use fish:// rather than sftp://? I know fish:// can work with telnet too, but every non-Windows host supports SSH nowadays."
    author: "Maarten ter Huurne"
  - subject: "Re: fish://"
    date: 2007-03-02
    body: "fish:// is useful if the remote host's SSH server doesn't have SFTP enabled.\n\nFor example, my Nokia N800's mini-SSH server."
    author: "Thiago Macieira"
  - subject: "Re: fish://"
    date: 2007-03-02
    body: "After I found fish, I NEVER bothered with any other method of sharing files (like samba, nfs, sftp or whatever). Fish is THE most usefull and easiest of all for me ;-)\n\nI mean, how simple could it be? Enable SSH (mostly very very easy, apt-get install ssh on debian and it gets enabled as well, or add it to rc.conf on arch or whatever) and then just fish://user@host [enter] and you're there..."
    author: "superstoned"
  - subject: "Re: fish://"
    date: 2007-03-02
    body: "I should have added: fish:// doesn't work if you don't have a shell on the remote machine. But sftp might."
    author: "Thiago Macieira"
  - subject: "Re: fish://"
    date: 2007-03-02
    body: "> I'm curious: is there are reason to use fish:// rather than sftp://?\n\nEven stranger, fish is really slow. I used FileLight on a remote system. With sftp:// it's done pretty quick, with fish:// it's so much slower. Examining the tree took a few hours instead of 15 minutes with sftp://"
    author: "Diederik van der Boor"
  - subject: "Re: fish://"
    date: 2007-03-02
    body: "Wow, how slow must the gnome fish-variant be, then... I read a blog by a gnome guy complaining it was way slower than fish:// in KDE ;-)"
    author: "superstoned"
  - subject: "Re: fish://"
    date: 2007-03-02
    body: "FISH uses the remote system's shell, while SFTP doesn't (it uses special features of SSH) so SFTP is faster (I think only if you're doing lots of things at once, like what file light would do).  I generally use SFTP but sometimes it just buggers out (I really need to try and figure out why sometime) so then I can fall back to fish which just always works.\n\nFish is will work pretty much everywhere that you have a shell, sftp requires the SSH to have sftp enabled in the configuration (which is pretty common from my experience).  So its really a compatibility vs speed trade off (and the nice thing is you get both for free in KDE)."
    author: "Sutoka"
  - subject: "Re: fish://"
    date: 2007-03-03
    body: "sftp is far from being fast or even quick:\nBug 129331: sftp very slow compared to commandline sftp client\nhttp://bugs.kde.org/show_bug.cgi?id=129331"
    author: "testerus"
  - subject: "Profiles"
    date: 2007-03-02
    body: "Personally I would like to see profiles to be able to save different toolbar configurations, I think that would solve almost anyones complaints about konqueror as a file manager.\nunless of course you get those that think the profiles are per tab(or a I'm browsing local why is it still web browsing?) instead of an application instance wide setting.\nWhile I'm happy for the change. I don't really see any compelling reason to even use dolphin. currently I have 6 tabs, 3 local, 3 websites. I've never found konq's ability to browse my files to be lacking in anyway. and have always loved the ability to have a web page up in the same window.\nI daily need both local and web up at the same time and would rather have them together than appart. as they are very related. (1 local is for my media, 1 web for my media database, 1 local for the website's source code, 1 web for the php api.)\nI cringed when I saw the 'breadcrumb' bit but was very relieved that it had the option to use the standard."
    author: "Stephen"
  - subject: "Re: Profiles"
    date: 2007-03-02
    body: "ehm... profiles DO save toolbar configurations. There is the simple browser profile there by default, with a kubuntu-like default all-in-one toolbar..."
    author: "superstoned"
  - subject: "You've Convinced Me"
    date: 2007-03-02
    body: "Like many I was skeptical about Dolphin when it was first announced to be the default file manager.  When told I could set konqueror as the default I wasn't worried, and that it would be my first task to do once I had KDE 4.0 installed.\n\nThis article convinced me to try dolphin, that screenshot shows a lot of useful ways to navigate through directories and files, it just might be useful.  I'm going home tonight and trying out dolphin!\n\nSunil"
    author: "Sunil Khiatani"
  - subject: "Re: You've Convinced Me"
    date: 2007-03-02
    body: "Please keep in mind that the version of Dolphin you are getting from SVN is still very much a work in progress.  Please don't judge it entirely on it's current state, as it will be much improved by KDE 4.0.  So for instance, the config dialog won't be as tall :P"
    author: "Troy Unrau"
  - subject: "Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "Why is KDE making this kind of decisions? The more KDE tries to copy half-baked Gnome applications, the worse for KDE users. Konqueror was just perfect as it was in KDE3. \n\nAnd please do not tell me \"you can still use Konqueror as your file manager in KDE4\" as people are not going to do that: 99% of people just use defaults, therefore 99% of work goes to improve whatever is default. Manpower is scarce and Konqueror was already lacking developer time; this Dolphin is not exactly going to improve the situation.\n"
    author: "Pau Garcia i Quiles"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "> Why is KDE making this kind of decisions\n\nbecause kde is for more than you and me. it's for everyone. we're trying to cater to a broader audience. sort of like what we've done with every release of kde every made compared to the one before it.\n\n> The more KDE tries to copy half-baked Gnome applications\n\n*sigh* you can think that's what you're doing if you want. you're also completely wrong. the goal is not to copy gnome applications. it's to take good ideas where they exist and bring them together with our own and produce something even better.\n\ni know it's *astounding* but works for you or for me may not actually work for everyone. we can stick our head in the sand if we wish, but that doesn't change reality (whatever that might be).\n\nthat said i've used nautilus in the last few months while doing various bits of research for dolphin. it's a pretty poor file manager, imo. so with that in mind, read what you wrote again and think about it.\n\n> Konqueror was just perfect as it was in KDE3.\n\ni'm very happy you feel this way. konqueror will still be there in kde4 as it was in kde3; hopefully even better actually. but your assessment of it as the \"right tool for the job\" is not universal.\n\nbut tell you what, as soon as we start making the Pau Desktop Environment we'll make konqueror not just the default, but the only file manager you're even allowed to run. how's that? ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "> because kde is for more than you and me. it's for everyone.\n\nYeah, the typical 'users are idiots' canard.  Linus unleashed a crapstorm by expressing his opinion of that design philosophy in Gnome, but I happen to agree.  \n\nEven if one believes that the average user's brain explodes when he sees his filesystem represented as a tree (wtf) that doesn't explain why Dolphin is set up to prevent people from doing so if they please.  Sure I can use Konqueror, but once there is a \"usable\" filebrowser at some point the the simplification brigade will be pushing for filebrowsing functions to be removed from konqueror to make it a better webbrowser, and given your own comments on the unworkability of improving konqueror's handling of different profiles things will have to move in that direction, leaving me with a non dirtree filebrowser.\n\nEven if that doesn't happen, do you really expect me to believe that work on filebrowsing won't be concentrated in the default filebrowser?  Konqueror is being left out in the cold vis a vis file browsing, saying anything else is disingenuous, so telling me to continue using it is sadistic.  I'm given a choice between a neglected, unwanted (by the devs anyway: they replaced it) filebrowser, or the braindead app that manages to be \"simple\" by taking away how many people are used to doing things, and adding a new way (like that won't confuse anyone).\n\nFor trying to woo the mythical masses of idiots waiting to jump into a complicated powerful Unix like OS and needing to dumb things down there is a one word response: Explorer.  If anyone thinks Midnight Commander is something more familiar to all the Windows users I dare say he/she is mistaken.  Even if the dirtree isn't visible at times, it is always a click away.\n\nSorry this has me so steamed.  The dumbing down of Deus Ex 2 (apologies if you aren't a gamer) had me worked up in a similar way.  The sequel to the complicated (and loved, and oft regarded as one of the best games ever) Deus Ex was \"simplified\" (dumbed down) to appeal to a console audience.  It flopped, and the legacy of the original was tarnished.  Using a single ammo type for every weapon, removing skill points etc didn't attract an audience, and it helped alienate those who liked such features in the original.\n\nRemoving dirtree (as an option even) will not attract an audience.  It can do the opposite.\n\nI'm a long time KDE user, but the longer goofy bugs are ignored, the more usability studies inspire retarded changes and the more I am written off in a quixotic attempt to appeal to the mythical Joe Sixpack who is interested in using a powerful, complicated Unix like system, the more I am pushed away.  I'm just one person, but such an emotional, intensely negative response from me probably isn't an isolated incident.\n\nAdding dirtree would assuage me, but that simple thing will never be done, as you know better, and this is indeed typical of the Gnome modus operandi you claim not to be emulating.  I'm sure you're sick of hearing this from me, but soon you won't have to."
    author: "MamiyaOtaru"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "> Adding dirtree would assuage me, but that simple thing will never be done, as you know better, and this is indeed typical of the Gnome modus operandi you claim not to be emulating. I'm sure you're sick of hearing this from me, but soon you won't have to.\n\nRead up a bit. You will be assuaged.\n\nDolphin is not yet in it's finished for. That has been mentioned in the article. It still lacks a few features, that may (or may not) be added. Nothing is set in stone yet, except that it will be the default file manager for KDE 4.x."
    author: "Jucato"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "+1\n\nEven the Amiga had a multiview. But KDE seems to choose the Atari TOS way;\n\nBe carefull, there are hidden anciant trolls ;-)\n\nMore seriously:\n\n- Dolphin lacks features not written yet\n- Despite that it will be the default browser\n- Not widely tested\n\nBetter make sure it is rock stable\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "Relax! \nPeter Penz already stated in the comments, that a dirtree will be added to dolphin."
    author: "infopipe"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "\"I'm given a choice between  ... , or the braindead app that manages to be \"simple\" by taking away ...\"\n\nYou seem to think, that dolphin is about \"simplification\". I guess that's wrong. Dolphin is about \"concentration\". It concentrates on being a program for file management. It will (or should be) as powerful as konqueror. It will (should) offer nearly everything that konqueror offers for file management.\nBut dolphin won't mess the user with UI-elements originated from file viewing or web browsing (have a look at the settings dialog).\n\nI'm pretty confident, that dolphin will be even more powerful on file management than konqueror. Simply because neither programmers nor designers will have to make any concessions to non file management tasks."
    author: "Birdy"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "I really hope you're right on that. "
    author: "Ben"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "Don't sit in the corner hoping. Go do something about it.\n\nTest Dolphin, test Konqueror. Make sure they are on par. Make sure they are rock stable. And if features are missing, volunteer to write them or convince a developer to do it.\n\nKDE developers are not totalitarian power-hungry fascists. Quite to the contrary: we're very open to new developers. We're receiving a nice influx of new blood due to the KDE 4 development process.\n\nSo, why not you too?\n\n(this reply is directed at everyone, not specifically at the parent poster)"
    author: "Thiago Macieira"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "Speaking of testing Dolphin, I recently grabbed v0.8.2 from FreeBSD Ports and gave it a spin in KDE 3.5.5.  It seems much more lightweight and capable of doing what I want than Konqueror.  I also like the sidebar where I can view Network Shares and trash:/ -- which reminds me. I've tried to find a Bug Tracker for Dolphin to submit a feature request for the ability to Empty Trash (maybe from the right-click menu inside the window pane like Konqueror has, or even just an Edit action under the main menu) from within Dolphin.  I cannot seem to find one on the Dolphin homepage, nor is it listed in the KDE Bugzilla drop-down form for applications when you go to create a new query. In fact, searching for the word \"Dolphin\" only produces 12 tracker results, 1 of which could actually be considered relevant.  Is there some other place I could pitch this idea or would I be wasting my time? Because while I was at it I also wanted to mention in case nobody had yet noticed that when the mouse is over a file with a really long description the text wraps to a second line but the bottom half of the second line is cut off by the window border--regardless of whether you resize the window or not.  I don't really ever read those little descriptions so it doesn't bother me, but I can see how it might make Dolphin be viewed as \"not ready for primetime.\""
    author: "Michel Boto"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: ">Test Dolphin, test Konqueror. Make sure they are on par. Make sure they are rock >stable. And if features are missing, volunteer to write them or convince a >developer to do it.\n\nI'm doing all that, I've been playing around with dolphin and I did post the missing featues I noticed\nhttp://dot.kde.org/1172721427/1172833833/ \n\nHeck I'm playing around with Dolphin right now, I'll be posting a new thread right after this post with a couple of problems I found. \n\nI also plan to test alpha releases of KDE4 (not dev snapshots though)\n"
    author: "Ben"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "Ben, please do not think your dot comments will get noticed by the Dolphin developers.  This is not the appropriate forum to post such things if you want to have any chance someone who can act on it will hear you.\n\nGo post on the list."
    author: "anon"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-03
    body: "They did get noticed ;)"
    author: "Ben"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "Now please go and re-read this whole thread. Konqi is a thin shell, just like dolphin - and they are sharing almost every filebrowsing function they have, if not just plain all of it. So this scenarion you describe is pretty much impossible...\n\nAaron didn't just say 'It's for everone' but he meant it. KDE is for you, and Dolpin is just there for those who aren't like you and do prefer something simpler. But it's for everyone, so it's still for you - and nobody will even start thinking about dropping konqueror. OK?"
    author: "superstoned"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-03
    body: "> the typical 'users are idiots' canard.\n\nuhm. no. try: different people work in different ways, and there are certain ways greater numbers of people work than others. we're trying to cater to as many of those ways as is reasonable, and that includes the power user konqueror fan.\n\n> Linus unleashed a crapstorm by expressing his opinion of that design\n> philosophy in Gnome, but I happen to agree. \n\nyou evidently didn't understand what Linus was saying then, because what he said isn't applicable to what we're doing here.\n\n> why Dolphin is set up to prevent people from doing so if they please.\n\na) there's always konqueror.\nb) a tree can be added to dolphin and looks like it will be an option\n\n> Sure I can use Konqueror, but once there is a \"usable\" filebrowser at some\n> point the the simplification brigade will be pushing for filebrowsing\n> functions to be removed from konqueror to make it a better webbrowser, \n\nyes, we've all been lieing to you this entire time! we're not keeping konqueror as the tool it is, we've just been telling you that! mwuashahahaahaha! you've seen right through us!</sarcasm>\n\ni'm glad you think were idiots though. because that's what it would take to do what you're suggesting.\n\n> and given your own comments on the unworkability of improving konqueror's\n> handling of different profiles things will have to move in that direction,\n> leaving me with a non dirtree filebrowser.\n \ni'm sorry for confusing you with technical details that reflect the facts. but please don't try to build conclusions you are incapable of erecting given your inability to grasp those technical details. because your little quip here makes no sense.\n\nno, you know what, i'm going to try to explain it a bit better for you: profile handling in konqueror is just great for what it is meant for. trying to twist that into creating completely separate and efficient interface styles to different use cases and modes of operation is not what it was meant for nor can it be made into such a thing without huge collateral damage in the form of insanely complex code paths internally and rediculous configuration interfaces.\n\nin other words, keeping the profiles as they is good. trying to make them do what dolphin is bringing us would not be. i'm not sure why you'd prefer us to screw over konqueror, but that's essentially what you're whinging for.\n\n> do you really expect me to believe that work on filebrowsing won't be\n> concentrated in the default filebrowser?\n\ni don't expect you to believe anything. i expect reality to remove the need for belief and replace it with fact. you've shown yourself to be resistant to rational persuasion, and that's ok. believe whatever you want.\n\nwhat you'll find is that between the code sharing and the number of people who love and use konqueror, it's not going to slip through the cracks.\n\nthough i must say, to quote darth vader, i find your lack of faith disturbing.\n\n> I'm sure you're sick of hearing this from me,\n\nno, i'm just tired of you not listening and comprehending.\n\n> but soon you won't have to.\n\nah, because you're going to try it out and realize how thankfully misguided your concerns were? great!"
    author: "Aaron J. Seigo"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: ">  it's to take good ideas where they exist and bring them together with our own and produce something even better.\n\nWhat makes you think it's a good idea at all? Is your feeling based of facts? studies?\n95% of peaople on earth are using explorer that acts just like konqueror without troubles. But trust me, in my company, most users leave konq because of its ppor support of javascript. They also leave kmail because of its lack of exchange support. Finaly they leave kopete because of its bad MSN support and lack of features in jabber.\nStrangly, they keep KDE desktop because of its full featured file browser: konqueror. Even windows users are using konqueror+filelight on Linux to cleanup their account on filers.\n\nAs I wrote a few tread above, there are tons of lack in KDE that are IMHO more important to fix that this sort of rewrite. (javascript in konqueror, exchange support, msn support, ...)\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "Why don't you try to convince your company to give you a few hours a week to work on these elements then, or sponsor a developer to work on them.  KDE is programmed mostly by volunteers... if there is a feature missing or lacking, it's usually because no one finds it interesting enough to work on.  This is most evident when it comes to things like Documentation, which are required for a good environment, but a PITA to write."
    author: "Troy Unrau"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-03
    body: "> What makes you think it's a good idea at all?\n> Is your feeling based of facts? studies?\n\nfacts, studies, research and a better personal track record with these things than most of the people in this discussion combined.\n\n> 95% of peaople on earth are using explorer that acts just\n> like konqueror without troubles.\n\nheh... wasn't it just you who was going on about feelings vs facts? explorer is not an easy tool, and konqueror even less so. before explorer, 95% of computer users got along just fine with split pane managers. in the days of mac-is-the-only-commercial-ui it was spatial. according to your thoughts, just because people manager to limp along we shouldn't bother fixing it.\n\nand for the record (you, know, similar to \"facts\") many people have a horrendous time with konqueror. i hear it on bugzilla, on irc and i've seen it many, many times in person.\n\ni have done usability testing with real live people on konqueror, including those who manage just fine with windows explorer. and for a significant % of people it presents challenges and problems.\n\nbut konqueror also works really, really well for a bunch of other people too. so we are keeping both. they compliment each other and serve different parts of our user base.\n\nnot sure why that bothers you. perhaps you'd be more satisfied if we just ignored people who weren't you.\n\n> But trust me, in my company, most users leave konq because of its ppor\n> support of javascript.\n\nc.f. the webkit/qt possibilities.\n\n> They also leave kmail because of its lack of exchange support.\n\nthis is an issue with exchange itself: it's a proprietary undocumented shifting set of protocols.\n\n> Finaly they leave kopete because of its bad MSN support and lack of features\n> in jabber\n\ni'd love to see more features in kopete. and you know what, the kopete team is working on providing just that. kde does more than one thing at once.\n\nof course, none of this has anything to do with file management.\n\n> there are tons of lack in KDE that are IMHO more important to \n> fix that this sort of rewrite\n\na) this is not a rewrite.\nb) it is benefiting other parts of kde, too; see dfaure's work on the model/view split for listings, frederikh's work on the view delegates, etc.\nc) the people working on dolphin were already working on it prior to us adopting the project\nd) those same people don't work on javascript, exchange support or msn support. we are an open source project, we don't get to dictate what people work on.\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-04
    body: "\"facts, studies, research and a better personal track record with these things than most of the people in this discussion combined.\"\n\nGood,good. Now please tell me exactly what those facts are.\nPlease show me the studies.\nPlease direct me to the research.\n\nI demand to scrutinize your data. (I'm seriously curious.)\n\nI am sure all of us thinks that Dolphin is an excellent addition, it's the\ndefault replacement we are so scared of. We suspect that Konqueror is going to\nsuffer, but if you say thats not's going to happen we will believe you. Ok?"
    author: "reihal"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: "\"may not actually work for everyone\"\n\nEXACTLY who are those people? Where and how did you find them?\nIn a coffe shop full of gnomesters?\n\nOk, you can play with the dolphin, but if you mess with Konqueror......\n"
    author: "reihal"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: ">> Konqueror was just perfect as it was in KDE3.\n\n>i'm very happy you feel this way. konqueror will still be there in kde4 as it was >in kde3; hopefully even better actually. but your assessment of it as the \"right >tool for the job\" is not universal.\n\nKonqueror IS a mess actually, it's powerful, but actually it's not for everyone, and need some usability love.\nFor kde 4 I hope to see a better dolphin, maybe a standalone web browser, and other apps that can be embedded as kpart ONLY when needed, and I hope to see a jack-of-all-trades konqueror that can be tweaked and filled with kparts at the user need, this make kde good for \"dumb\" and power users"
    author: "ra1n"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-02
    body: ">because kde is for more than you and me. it's for everyone. we're trying to cater >to a broader audience. sort of like what we've done with every release of kde >every made compared to the one before it.\n\nI don't think thats the best sort of philosphy. On Linux there is KDE, Gnome, XFCE, IceWM  and more. I think that each one should design according to its own philosophy and not chase marketshare, then let users chose the one that fits them.\n\nOn the other hand dolphin dosn't seem like an idea, having a dedicated file browser app has its advantages, such as a cleaner interface, more file browser featues (like breadcrumbs), speed and stabiltiy since its got no web browser code. \n\nAnd if I am honset the concept of a combined file/web browser was offputting to me before I tried KDE. but that didn't last long after I actually used it."
    author: "Ben"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-03
    body: "> I think that each one should design according to \n> its own philosophy\n\nwe are.\n\n> and not chase marketshare\n\na \"broader audience\" means a more broadly capable desktop. that was the original intention of kde if you go back to the first announcement of the project by ettrich.\n\nour user base has grown significantly and we are not servicing many of those people as well as we could or should. hell, many kde developers themselves use konq for file management sparingly as it doesn't provide a compelling story over the command line for them. there are so many groups of people who use kde right now that konqueror serves well, and those it doesn't.\n\ni also don't see the inherent conflict with wanting more users and therefore accommodating them while at the same time also keeping true to the philosophy and design we've always held.\n\ni know it's a radical thought, but one can grow and be true to themselves ... at the same time."
    author: "Aaron J. Seigo"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-03
    body: ">> I think that each one should design according to\n>> its own philosophy\n\n> we are.\n\nNice to here :)\n\n>our user base has grown significantly and we are not servicing many of those \n>people as well as we could or should. hell, many kde developers themselves use \n>konq for file management sparingly as it doesn't provide a compelling story over >the command line for them\n\nI too use the command line a lot, in my case its not konquoror but GUI's in general.\n\n\n>i also don't see the inherent conflict with wanting more users and therefore \n>accommodating them while at the same time also keeping true to the philosophy \n>and design we've always held.\n\nI don't see any inherent conflict either, and currently I'm pro dolphin, it was more a comment about Linux design philosophies in general, and the benifits of speciliseation, than a comment about anything happening in KDE development today.\n\n>i know it's a radical thought, but one can grow and be true to themselves ... \n>at the same time.\n\nnothing radical about that at all :) I was just saying that I think being true to yourself, not growth, makes a better priority. "
    author: "Ben"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-03-04
    body: "Just curious: what didn't you like about Nautilus?  I'm sure the Gnome developers would be interested to hear what you found frustrating.  It might help them make some improvements."
    author: "coulamac"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-11-28
    body: "I'm about ready to start working on the Pau Desktop environment.  Thanks for the source code.\n\n>because kde is for more than you and me. it's for everyone. we're trying to cater to a broader audience. sort of like what we've done with every release of kde every made compared to the one before it.\n\nIn other words, screw the actual users, we're designing KDE for the people who AREN'T using it.  That makes sense, doesn't it?\n\n>konqueror will still be there in kde4 as it was in kde3; hopefully even better actually. \n\nHopefully, but I'm not exactly feeling reassured.  You seem to be saying that the idea is that Konqueror  is going to be \"adjusted\" so that the file management won't get in the way of the web browsing.  That's like Marlon Brando cutting back on the acting to concentrate on his singing career.  Whatever Konqueror might have been intended for, it's the world's greatest file manager, and a mediocre web browser.  That's why every kde-based live CD distro comes with firefox. If Dolphin was a web browser, reading this article would not have made me sick to my stomach.\n\n>Konqueror is not going to disappear for KDE 4, although its user interface may yet see some adjustments as its primary utility will not as the default file manager.  \n\nSeriously, do you expect people to NOT freak out when they read this?  \n"
    author: "blackbelt_jones"
  - subject: "Why not ..."
    date: 2007-03-02
    body: "making a konqueror rework indeatd of implementing a new project ? \nKonqueror has a few flaws ... but i think if we rework some usability issues there it could be THE KDE App that it was (from KDE 2-3).\nOk the bread... navigation (from mac osX) is a nice thing but couldn't be that a nice extension for konqueror ? Ohh i hope that i wouldnt get too sentimental ... about that things. But the kfm thing would die if dolphin took his (k)part. "
    author: "Scherf"
  - subject: "Re: Dolphin worse than Konqueror"
    date: 2007-09-02
    body: "I don't agree. For me Dolphin is simply useless.\nKDE4 with Dolphine as default file-manager will get the applause from Gnome-users, but it is and remain a really nonsense.\n\nSorry for my english.\nI find this decision the worst one KDE peoples have ever token."
    author: "mcz"
  - subject: "mc rules"
    date: 2007-03-02
    body: "Call me weird, but I still use the Midnight Commander... It's just so easy: Everything works with keys, same keys as in the good old times, hm..."
    author: "ac"
  - subject: "Re: mc rules"
    date: 2007-03-02
    body: "Try Krusader. It's like a GUI version of Midnight Commander, for KDE of course. ;-) And there is complete keyboard navigation, along with the familiar function key shortcuts. http://krusader.sourceforge.net/ And no, I'm not involved with Krusader's development, I'm just a happy user of it."
    author: "Kevin Kofler"
  - subject: "Re: mc rules"
    date: 2007-03-07
    body: "mc forever ;-)"
    author: "andrea"
  - subject: "duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "I've already posted that on Arun's blog, but I think this needs to be here as well with few missleading fixes.\n\nIt's realy sad to see duplicate effort. How many hours of coding lost in developping Dolphin that could have been used to fix konqueror (CSS3 is fine in konqueror, but javascript is far behind and needs lots of improvement IMHO)\nEventhough there is code re-use, this is time loss in writing Doplphin that could be better used.\n\nkonqueror has profiles for file browsing, wonderfull filelight interface for cleaning directories, and much much more. Why not improve profiles then?\n\nPlease stop Dolphin and leave dupplicate effort challenges to Gnome project.\nAnd please, one for all, don't suppose users are Dumb! Tree view is in Windows explorer since ages, nobody complains about that and it represents 95% of the users around the glob! Your direction is toward 5% of users.\nMore over, the danger is that it's a new, not widely tested app set as default. You'd better make sure it's rock stable or many users will run away!\n\nKDE is a wonderfull clear, concise desktop environment, please don't cripple it with dupplicate applications. This is the most confusing thing a new user could experience: what app to chose?\nRight now, we have single app for each tasks\n- one media browser\n- one media burner\n- one desktop suite\n- one photo management whichg could be enhanced by a video library management and DVD authoring side (why? because when you author a DVD, each software has a library which is a duplicate effort as well)\n...\nI see dolphin as the beginning of several apps for each tasks.\n- Several file browsers\n- Several images viewers\n- Several duplicate apps.\nDon't you aim at a k3b simplified concurent, a simplified koffice and such?\n\nIMHO, the oposite direction should be taken instead:\nI think that one day, amarok and kaffeine should merge. My music library is half mp3/ogg and half mp2-video (tv recording), thus I cannot have common playlist...You see the point of merging?\n\nIMHO, there are big lacks in KDE that are incredibely more important than recreating something that already exists. These are:\n- Decent javascript support in konqueror\n- Native Exchange support in kmail\n- Kmail mail subjects automatic styles (color, ...)\n- Better multimedia integration (ManDVD is fine but lack many features; could be integrated to digikam to create a media management system IMHO; digital camera already merge video and photos...)\n- Better kopete components (mainly webcam supports, msn wizz and such, upnp support in msn and others, unified proxy, white board in jabber, voice in jabber, ....)\n- More logic in TV apps (analog is done with kdetv and digital is thru kaffeine).\n- Improving kdewallet by having password groups (when you use the same password on many places (single sign on for example in a company), you have to update your new password in kdewallet in all login forms.\n\nIf you want to call KDE4 a \"revolution\", that's not in rewriting already existing apps. It's in writing things that lack in KDE right now.\n\n/me: not happy with new directions....\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "Just some quickies:\n\n1. KHTML (the HTML rendering engine) development is a bit separate from Konqueror (the app) development. The KHTML devs aren't losing time due to Dolphin.\n\n2. Dolphin is not a duplicate. The efforts that are put into Dolphin will not be wasted. The changes made to underlying technologies and libraries that have come about due to work on Dolphin will affect Konqueror as well (as the article and comments have already stated).\n\n3. Presuming users are dumb is probably wrong. But presuming that everybody loves Konqueror the way it is also probably wrong. The thing is we now actually have *a choice*. People who want a separate file manager can use Dolphin. People who want to keep on using Konqueror will still continue using Konqueror. It's a win-win situation.\n\n4. Dolphin will be the default file manager. Konqueror is still the default Web Browser. Single app for a single purpose. Besides, realistically speaking, we already have multiple apps for single puproses anyway. There's Amarok, K3b, Digikam, Kaffeine, etc.\n\n5. You presume that everyone is working on Dolphin and just Dolphin alone. JS and CSS are for KHTML. KMail is for KDE PIM. So on and so forth. \n\n"
    author: "Jucato"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "1) Dolphin developpers are loosing their time on Dolphin instead of helping other far more important areas in KDE.\n\n2) Change in underlying technology would/could have occured in konqueror file browsing enhancement.\n\n3) The wildcard argument *choice*.... It's bulshit (sorry for the word).\nDo you call choice  when:\n- Without study, the default file browser is changed (thus more used by users that always use default, thus konqueror file browser will die as said by someone else above)\n- you have to use evolution to connect ot exchange server natively?\n- you have to use firefox to browse websites that make extensive javascript use?\n- you have to use gaim for IM stability and features?\n- you have to use vlc for video straming/recording (triple play access)\n\nChoice as you mean (filebrowsing) was already available: krusader, Nautilus and many others. Better spend taskforce on areas were nothing exists at all.\n\n4) Replacing one default app by 2 default app is just plain stupid as menu will differ, and users will have to learn 2 behaviours, configure new gestures (if at least dolphin can handle gestures (hopefully as it is integrated to kde))\nMore over it's very dangerous as this is a new and not widely tested app. If there is a bug/problem is the \"main\" desktop app, it will give the whole image of the desktop.... Realy dangerous\n\n5) People working on Dolphin would be far more usefull at helping the projects you mention. JS, PIM and OFFICE teams needs more resources and are incredibely more important that a file browser in the day2day use of a Desktop!\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "> 5) People working on Dolphin would be far more usefull at helping the\n> projects you mention. JS, PIM and OFFICE teams needs more resources and\n> are incredibely more important that a file browser in the day2day use\n> of a Desktop!\n\nIt would be stupid to believe that developer resources can be allocated like that. Someone who works on file management is very likely not to have interest in working on other (even similar or more important) things instead. In software companies, people can be commanded what to work on, but this is open source, so we better accept help where it's offered.\n\nAlso, there _are_ people working on JS, KDE-PIM and KOffice. Just because you find those more important doesn't mean that file management does not need man power just as badly."
    author: "Jakob Petsovits"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "1. This is Free and Open Source Software. Developers cannot be told to do what they don't want to do or work on something they don't like. Everyone is a volunteer. Unless you are being paid by a company to do work on it, of course.\n\n2. True. But they have decided to use something that was built on cleaner code. From what I remember, the current Konqueror code is messy. It was much easier to start clean and build on clean code than to take apart Konqueror.\n\n3. Let's focus only on the issue at hand. Let's not drag kdepim, koffice, khtml, kdenetwork, etc. into this. \n- You presume the decision was made on a whim.\n- Krusader is a twin-panel file manager. Not everybody will want to have that. Nautilus, Thunar, ROX-Filer, etc.? We are talking about a KDE file manager, right?\n\n4. There will only be 1 default app for file management: Dolphin. There will only be one default app for web browsing: Konqueror. Those who want to use Konqueror as the default file manager as well can change to that. User will not *have* to learn. If they switch to Konqueror, then that means they already know about Konqueror in the first place. Besides, aside from some new UI enhancements, the basic functions of file managing are the same.\n\nA \"not widely tested app\"? Why not help test it? An app can only be \"widely tested\" if there are users concerned enough to test it. There's a KDE 3.x version of Dolphin.\n\n5. See reply #1, and the other reply to you."
    author: "Jucato"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "First lets tell you that I am a KDEONLY USER  and a \"kind of\" developer ;)\n\n\n>> Developers cannot be told to do what they don't want to do or work on something they don't like. Everyone is a volunteer.\n\nAccording to me that is the main problem of the Open Source Society and Software .... :(\nEverybody thinks he makes (can make etc.) a great software, a great job etc. - a fact that maybe is true, if it was not the reality that there are \"hundreds\" of file managers, \"hundreds\" of IM clients etc. and they all look alike and do the same things... - that is called VANITY... :)\n\nMy opinion is that developers should concentrate on improving existent software instead of branching and changing default applications drastically.\n\nI really don't see why someone loses efforts in developing Dolphin.... and more important question is WHY and WHO decides that this Dolphin should replace Konqueror in file browsing....That is called autocracy\n\nWell... we all know who and we all know why....\n\n\nAnd such behaviour is the main reason for Microsoft's announcements of increasing profits year after year....\nAt least they do users' surveys .... ;) \n\n>> 4......\nYou don't sound too convinced yourself. I have a question....\nWill the file browsing features of Konqueror evolve and improve when it will not be the default file manager....\nI am sure that the answer is NO.\n\nSo my voice is against the Dolphin thing... sorry.... "
    author: "PhobosK"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "\"My opinion is that developers should concentrate on improving existent software instead of branching and changing default applications drastically.\"\n\nIf developers only worked on improving things that already exist KDE would have never even been started.  Developers work on what they are interested, developers working on things they AREN'T interested in won't be close to as productive (and they may not have any experience in where they 'should' be working.\n\n\"Will the file browsing features of Konqueror evolve and improve when it will not be the default file manager....\n I am sure that the answer is NO.\"\n\nThe answer is Yes.  Since Dolphin and Konqueror will be sharing pretty much all of the file management code Konqueror will be getting all of Dolphin's improvements for free, plus more likely than not many developers will still prefer Konqueror over Dolphin so there will be interest in maintaining the non-shared code (plus Konqueror WILL still be a default app, just not default for file management)."
    author: "Sutoka"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-03
    body: "> You don't sound too convinced yourself. \n\nI am convinced. My #4 paragraph doesn't state otherwise.\n\n> Will the file browsing features of Konqueror evolve and improve when it will not be the default file manager....\n> I am sure that the answer is NO.\n\nThe answer is YES. It has already been said, over and over again to the point of being redundant, by people *who are involved in developing for KDE*. Now if you don't believe them, I don't know who you will believe (besides yourself)."
    author: "Jucato"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "> I think that one day, amarok and kaffeine should merge. My music library is\n> half mp3/ogg and half mp2-video (tv recording), thus I cannot have common\n> playlist...You see the point of merging?\n\nLast time I tried, Amarok played video files like Ogg Theora or some DivX/MP3 .avi flawlessly. The only thing it doesn't do is showing the video, but if you're just in for the music there's no need to keep the playlists seperate. (And of course, Kaffeine also plays both types, just that it's not as good an audio player as Amarok is.)\n\nIn rather unrelated news, I would LOVE to see Kaffeine's current playlist widget replaced with Amarok's. Maybe there's a way to shove this _very_ advanced playlist widget into a separate library so that Amarok isn't the only app able to use it?"
    author: "Jakob Petsovits"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "Then it could be combined with VLC after it finishes moveing to QT 4...\n\nA guy can dream"
    author: "Ben"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "Please re-read the thread. All your concerns aren't valid (anymore). And work on a new SHELL just for filebrowsing kparts won't influence the KHTML kpart in any way - not positively, not negatively.\n\nKonqi won't loose any features, it uses the same kparts.\n\nAnd on a more personal note: please don't ask for merging video and audio?!? I don't even see how they relate. You don't want kword to be able to view websites, now do you? Well, it can, but that's not what I meant ;-)\n\nUse an audioplayer for your audio, and a video player for your video and a picture viewer for your pictures etcetera. Merging all those will just create a monstrous application which can't handle any of them well."
    author: "superstoned"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "\nyou leave in the past...\n\nAll video / photo and audio app are converging.\n\n- Look all those media centers\n- Look those ipods\n- Look thoses archos\n- Look to your digital camera\n- Look at your multimedia phone.\n\nTestcase 1:\nYou whant to download your digital camera content to your computer:\nthere are videos, photos and audio contents (comments)\nDo you realy mean that you'll use a video app to download video then use another app to download your photos and then use an audio tool to download and manage you photo comments? how will you keep sync?\nSorry to disapoint you, but, aside peer to peer divx collections and peer to peer music collections, there are real life use of a computer...\n\nTestcase 2:\nYou want to create a presentation on a DVD of a wedding: you want to make a morph between a video1, a photo or 2 a video2 and an audio comment in a photo. do you realy have benefit of having:\n- a separate photo database (digikam or whatever)\n- a separate video processing software\n- a separate authoring software that has its own media database\n- a separate audio processing software that has its own media database\nand swap between apps with temporary results, directory structures, 3 media databases and such?\n\n\n"
    author: "olahaye74"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "your view of reality is completely broken.\n\nwhat the hell should an vidio, audio, graphics hybrid app look like!? shouldn't it do pdf too? i mean, pdfs are allmost like graphics... and than we allready have a text engine, so it should also be my word processor. but wait! i still would need to open my email application, press a button und search my stuff on the filesystem when i want to mail something! his has to be changed - this thing needs to mail stuff by it self!\n\nwhat you want is a total usability nightmare.\n\ndo you know with how many \"testcase\" users could come up with? combining stuff into one application just because you do one after the other is a total horrible idea...\n\nyou want to organize audio, video and image files? what about text? use your filesystem! thats what its for. sticking everything together in one database brings you exactly NOTHING.\n\n\nso, sorry to disapoint you... but there are many people who just want to listen to music, watch videos or manage their filesystem. those people want be happy with your videoauthoring-musicplaying-dvdburning-database-monster..."
    author: "ac"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "No. The future is to have separate and specialized applications but have those working together.\n\n\"Look all those media centers\"\nWhat about them? They use separate applications for each media type.\n\n\"Look those ipods\"\nWhat about them? They are great for listening to music. I don't know anyone using photo or video features of these.\n\n\"Look at your multimedia phone\"\nBest example. The UI of these is disgusting.\nOnly iPhone does not seem to do too bad. You know why? Because it morphs it's UI radically according to it's use (something konqueror is pretty bad doing it).\n"
    author: "Birdy"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-02
    body: "Please, note that Konqueror is a pain as file manager for common users. This is just a truth that I've realized looking at many newbies using Konqueror.\n\nManaging files is a big part of the heart of a desktop enviroment, and Konqueror makes it really annoying because it's not optimized for that and contains too much options not related to file managing.\n\nI agree with you when say that there are areas that need a lot of help and improvements in KDE (PIM, web browsing, IM, video...) but I really think that a good and intuitive file manager is a need for KDE4. It's not a duplicate effort, it's in fact something that didn't exist.  "
    author: "I\u00f1aki Baz"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-03
    body: "my name is Aaron, not Arun. and the rest of your message just goes downhill from there in accuracy."
    author: "Aaron J. Seigo"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-03
    body: "Hahaha!  I think it's awesome that you care enough to respond to so much of the crap that being thrown around here.  You and Peter have shown a lot of class in your responses.  You guys keep up the good work.  I'll reserve my judgement for when I actually have KDE 4.0 running on my computer.  I'm sure I won't be disappointed."
    author: "Louis"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-03
    body: "Yeah. I agree. \nDon't let the loud crowd dampen your visions.\nI'm quite sure most of them will cheer you, when they see dolphin in action."
    author: "infopipe"
  - subject: "Re: duplicate efforts when other areas miss code"
    date: 2007-03-05
    body: "lol. nice."
    author: "illogic-al"
  - subject: "Krusader all the way!"
    date: 2007-03-02
    body: "There's no better filemanager on any OS than Krusader!\n\nAnyway, Dolphin should use the F-Keys and make it obvious. When there's a split-window-view, at the bottom a bar should appear with the obvious tasks...\n\nF1 Copy\nF2 Move\nF3 Delete\nF4 Make Dir\n\nand so on...\n\n*THIS* would be ease-of-use and good visible integration of functions/keys into a filemanager..."
    author: "fish"
  - subject: "Re: Krusader all the way!"
    date: 2007-03-02
    body: "F2 is \"rename\" in almost all if not all app on earth from cells in excel on windows to CD volume name in k3b!\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: Krusader all the way!"
    date: 2007-03-02
    body: "Yeah, I know those keys are \"wrong\" - it's just an example...\n\nAnyway, my point is that this should be VISIBLE just like it is in Krusader, at the bottom, or, whereever it makes sense. Also, it should NOT be visible when you don't operate in a split-view. \n\nF-Keybindings just make sense for filemanagement and shouldn't be hidden..."
    author: "fish"
  - subject: "Re: Krusader all the way!"
    date: 2007-03-02
    body: "I really must try Krusader one day. Dose it work with KIO?"
    author: "Ben"
  - subject: "Re: Krusader all the way!"
    date: 2007-03-02
    body: "Yes, it supports all common KIO slaves. And adds its own, like the archive handling through krarc.\n"
    author: "dek"
  - subject: "Re: Krusader all the way!"
    date: 2007-03-02
    body: "of course it does ;-)"
    author: "Jonas"
  - subject: "Re: Krusader all the way!"
    date: 2007-03-03
    body: "Sounds like:\nNorton Commander\nMidnight Commander\nWindows Commander.\n\nYou sound like:\nnoob\nzealot"
    author: "Anonymous"
  - subject: "Re: Krusader all the way!"
    date: 2007-11-12
    body: "Sounds like:\n\nNever bothered to look at Krusader.\n\nKrusader is a little like the above mentioned applications, but those are console based. Krusader is an actual kde application. And it is fantastic. Dolphin is clearly heading toward duplicating much of its functionality and presentation.\n\nKonqueror is a terrible way of handling things. Individually, parts might be great. But this is Unix. You're supposed to have a tool that does something. If you need four things done, you pick the four best tools. You don't get one giant tool crammed down your throat and then told \"if you want to do the first thing a certain way, we're going to force you to do everything else our way, too!\"."
    author: "no"
  - subject: "Konqueror 4: Is it time already?"
    date: 2007-03-02
    body: "I guess this would an opportune time and place to ask about this and throw out some ideas. We've all heard about progress in Dolphin. But since Konqueror will still be in KDE 4.x, although not the default file manager, I guess it's only natural to wonder, \"How is Konqueror?\" While work being done on Dolphin is amazing, I am wondering what work is being done on Konqueror. What are the plans, aside from the usual porting to Qt4? When will they begin? I'm not asking about the KHTML part, but more on Konqueror as an app.\n\nI think it is also time for people who love Konqueror, who think Konqueror is the best, to divert their attention from complaining about Dolphin vs. Konqueror, and to focus on helping improve Konqueror. Konqueror is far from perfect, otherwise we wouldn't even be having this problem at all.  If Konqueror needed some changes/improvement, I think the time is now.\n\nI have a few ideas, based on my own experience, and those of others I've come in contact (IRC, forums, mailing lists, etc.). Since I'm not a programmer, I leave it up to those who can code to see if these are feasible\n\n1. Profile management. There probably wouldn't be a need for a separate file manager app, if Konqueror had a better profile system. The UI should be able to adjust depending on the profile, not just the content/current view. Currently, only the Navigation Panel seems to have a per-profile setting.\n\n2. UI Revamp. We have a KDE HIG working group now. Perhaps it's time to see which stuff should go, which stuff should be put where, and which stuff should be added. I think Dolphin got some UI stuff right: a location bar/breadcrumb bar per split view, a single button to split/un-split views, clear indication of active/inactive views, no side tabs (navigation panels/side bars are nice, but side tabs are probably a waste of space). The UI for the settings also needs some changes (but perhaps more under KControl's area of responsibility).\n \n3. Changing Konqueror's description. This is probably the only non-technical change. You might ask why. Simple. Konqueror simply can't be defined in a simple way. It's a file manager plus web browser plus plus plus. After a while, they all add up, and we tend to lose sight of what something really is. This change maybe artificial and sort of bikeshedding in some ways, probably. But I think now is a perfect oppurtunity to redifine what Konqueror really is, what it is not, and what it will become."
    author: "Jucato"
  - subject: "Extra! Extra! Konqueror 4 - the test!"
    date: 2007-03-02
    body: "Seeing where this goes, a test of sort :)\n\n1. Now that KDE has a \"user friendly\" file manager, what will happen to Konqueror?\n\na) Nearly 100% of people will just switch to Konqueror as default and life will go on normal again.\n\nb) Some dude will properly port BreadCrumbs part to Konqueror and for that 100% of people the life will go on normal again.\n\nc) With an excuse \"Since Dolphin now supports file-management for idiots\", Konqueror devs will overload Konqueror with buttons and features and will make it an unusable monster, resulting in a situation where users will not use either and switch to some other desktop.\n\nd) The person who wrote the \"usability study\" as part of their university degree and strongly suggested that \"monkeys prefer file manager and browser be split,\" will get the accolades and go on working as a rubber-stamper for post office."
    author: "Daniel"
  - subject: "Re: Extra! Extra! Konqueror 4 - the test!"
    date: 2007-03-05
    body: "As I am wont to do whenever the correct answer to a multiple choice test wasn't available; The correct answer is:\ne) None of the above"
    author: "illogic-al"
  - subject: "Re: Extra! Extra! Konqueror 4 - the test!"
    date: 2007-07-01
    body: "Actually:\n\ne) Konqueror will just be a web browser and Dolphin just a file manager, since Dolphin will implement all of the feature requests (read, \"complaints\") running around here.\n\nf) None of the above\n"
    author: "riddle"
  - subject: "Sounds not like a good idea"
    date: 2007-03-02
    body: "AFAIK there are still a lot of things to do to finish KDE 4.\n\nReplacing an established, good working filemanager with a \"gnome style\" one is not what I want to see on that list. Seems to me like KDE development lost a bit of it's focus and now repairs planless things that aren't broken.\n\nIt's a pity, my high hopes get more and more curbed: I expected new concepts and visions to become slowly more clear, but beside a lot of buzzwords about plasma & co, this is the first real visible change, and I don't like it.\n\nSorry, but IMHO this is a step in the wrong direction at the wrong time. And yes, I regulary check out svn versions and try them."
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "Sorry for replying top myself:\n\nAfter reading more comments, it looks like some KDE users have problems seeing dolphin become the default browser.\n\nSo, how about voting?\n\nEven if Dolphin would lose maybe because some users simply don't know it, this would be IMHO the best solution, and if KDE 4.0 is out and Dolphin shows that it's really the better alternative one could still change the default settings later, as this would just affect new installed 4.x versions and all other users would keep their used environment. But please let Dolphin show how good it works in real installations, before making it the default, if it looks like a lot of users have objections.\n\nWouldn't that be the better way, instead of deciding for all users and simply ignoring those who don't like that decision? Please leave that way of development to the \"other\" desktop environment..."
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "wouldn't it be the better way to let the ones who do the work, decide what they do!? how does it come that most of the dot readers seem to think the kde developers are working for THEM??\n\nif you want to see a better konqueror with more users in the future, go ahead and work on it.\n\n\ndespite from that: voting doesn't work for such things. people don't like new stuff. people complain more often then praise. and last - people can't decide on something they don't know anything about - you can't vote on an application you never tried.\n"
    author: "ac"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "\"how does it come that most of the dot readers seem to think the kde developers are working for THEM??\"\n\nI guess they work to build the best desktop environment they can do. For themselves, and for me and you.\n\n\"if you want to see a better konqueror with more users in the future, go ahead and work on it.\"\n\nWith that argument you can kill every discussion about everything. Do you really think that a fruitfull discussion about the concepts or asking the users what the want aren't usefull contributions to KDE's development? Wouldn't you think that KDE developers were disappointed if users complain about KDE 4?"
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "i don't see the question for a poll to remove a new application many kde developers have agreed upon as the new standard as fruitfull discussion, no...\n\n\ndo you want to know what i think this is? its rude. nothing more.\n\ndon't you think the kde developers thought about that allready? do you really think they changed something just because it was monday and they needed to break the svn version again?\n\ni don't see a fruitfull discusion at all. just some people whining because their beloved application will not be the default anymore. they are not even questioning if there are people that don't like konqueror like it is now. they just want their default back..."
    author: "ac"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "Who asks to remove dolphin? It's just about making dolphin the new standard. And you should read the reply of Thiago Macieiras, who is a developer: \"Users are fully entitled to complain about developer decisions.\" Beside that I just see one person here who is rude, and that's you.\n\nEND OF THREAD."
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-04
    body: "Many kde developers agreed upon this?  When?"
    author: "John Tapsell"
  - subject: "Re: Proposition"
    date: 2007-03-04
    body: "Make a Poll."
    author: "funnyfanny"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "\"Replacing an established, good working filemanager with a \"gnome style\" one is not what I want to see on that list.\"\n\nWhy \"gnome style\"?!? Only because the file manager can't browse the web and cook coffe?\nDolphin is progessing quite nicely. And I doubt it will be less powerfull than konqueror as file manager.\n\n\"Seems to me like KDE development lost a bit of it's focus and now repairs planless things that aren't broken.\"\n\nNo, the opposite! KDE is focusing on proviving one tool for one task. While it may be nice to have a knife, one can use as hammer too. Tim Taylor would prefer to have a separate knife and hammer ;-)"
    author: "Birdy"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "Dolphin is just going to the old GEM times.\n\n- My directories have index.html to represent important directories and virtual views, this will vanish with Dolphin.\n- I'm happy to click on a PDF file and have it in my browser (I avoids clicking the alternative html link in google).\n- Provided I'm in a PDF file in the current view, I'm happy to click on a hypertext link to continue my surf.\n\nThis was a realy seamless surf that will vanish with Dolphin.\n\nI realy though that KDE4 would though about future and be the 3rd millenium way of accessing data in a flawlesly and seamlessly manner. This move proves the contrary were there will be a specific app for a specific datatype.\n\ntoo bad..."
    author: "olahaye74"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "Continue using Konqueror then.\n\nAnd please accept that some people are not you."
    author: "Thiago Macieira"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "This is not the point.\n\nthe point is that konqueror could have been enhanced to have a profile that behave like the current dolphin project in IMHO far less work (no need to rewrite the shell from scratch).\n\nBTW, the konqueror current behavior is the behavior that 95% of users around the world are used to (windows explorer represents 95% of desktop users) why putting default on 5% of users?"
    author: "olahaye74"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "I know the Konqueror source code. And I am not convinced it would have been easier.\n\nMore to the point: NO ONE was taking care of the file manager part of Konqueror. I repeat: NO ONE. It has been left to bit rot for quite some time now. Aside from bug fixes here and there, there hasn't been any major development in it.\n\nThen there's this guy who comes along and proposes to do the work. In a new shell as well as the KonqParts. Who are we to refuse that? We'd be hypocrites for saying \"No, we don't accept your contributions. Go work on Konqueror instead, something we haven't been doing.\" The way I see it, that's what Linus was complaining about in the GNOME development model: they don't accept contributions and they don't fix the problem themselves either.\n\nOne more argument: split file manager and web browser has been one of the most requested features of Konqueror. We know we can't please Greeks and Trojans, so we didn't split Konqueror. We added this new tool.\n\nFinally, while Aaron and the others have been saying that Dolphin will be the default file manager, KDE 4 hasn't been released. Between now and then, a lot can still change. I don't think it's likely that the Dolphin decision will be reverted, but it's not set on stone either.\n\nAnd I don't accept your Windows Explorer/Internet Explorer argument."
    author: "Thiago Macieira"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "Disclaimer: I am not saying I agree with Linus. I know nothing about the GNOME development model to agree or disagree.\n\nThat above was just my interpretation of the recent facts."
    author: "Thiago Macieira"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-04
    body: "\"Finally, while Aaron and the others have been saying that Dolphin will be the default file manager, KDE 4 hasn't been released. Between now and then, a lot can still change. I don't think it's likely that the Dolphin decision will be reverted, but it's not set on stone either.\"\n\nWhy have a default at all? Let the users chose. And isn't this set by the distributions? Slackware will have Konqueror and Novell will have Dolpin as default?\nPostpone the decision to set a default."
    author: "reihal"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "\"BTW, the konqueror current behavior is the behavior that 95% of users around the world are used to (windows explorer represents 95% of desktop users) why putting default on 5% of users?\"\n\nAnd to 99% of those 95% of users Windows Explorer and IE are two different applications that just both have 'explorer' in their name.  In fact they ARE.  I've never actually seen a person open up windows explorer and type a URL into the location bar, or open up IE and type a file path into the location bar.  I've done it a couple times, and you end up with an app that looks like an ugly hybrid of the two explorers (everything is sized wrong and you have odd menu entries and toolbar buttons).  Also explorer is a different app than iexplorer (you can see by looking in task manager even, they have different executables!), in the olden days Microsoft tried to merge them together to but never were able to create one app that did both.\n\nBTW, Konqueror currently behaves the same in KDE SVN, and will continue to behave the same."
    author: "Sutoka"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "At least by the huge amount of replies to this posting on the dot (over hundert by now) you should see that there seem to be more discussion neccessary then just \"please accept that\".\n\nOf course the dev's can do what they want, and with the same right the users can think that a decision is wrong or right. If that's the end of the dialog for you ... I'm not sure if that's the right way to make KDE the best desktop environment."
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "No, of course not. Users are fully entitled to complain about developer decisions. And developers making very bad decisions should get a nice smack on the head and learn from their mistakes.\n\nThe message I am trying to get through is that \"users\" isn't \"you\". Your preferences do not necessarily reflect the majority of the users -- or even of a minority. Mine don't either, obviously. Nor do Aaron's or Peter's.\n\nI am not saying that we'll \"dumb down\" things so that the smallest majority can use. I hope we don't forget about all of the minorities either. What I am saying is that people coming here saying \"this is the best; you developers are wrong\" are wrong.\n\nAlso, this discussion is not a good sample. That's also the reason why we don't take polls in dot.kde.org about such design decisions. Dot readers are nowhere near representative of the user base (we have no idea what the user base really is...). And, of course, complainers always make a louder noise than praisers.\n"
    author: "Thiago Macieira"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "Of course you're right, this discussion is not about my preferences.\n\nBut the large amount of replies shows that this topic causes a lot of reactions, and that not all are happy with dolphin becoming the default file manager, the dot is representative enough to state that, wouldn't you agree? Did you expect such a strong reaction?\n\nAs far as I see, the integration of dolphin itself into KDE isn't a problem for most of the authors here, it's just making it the default, that some interpret as a (major?) shift in concept of KDE. Not even distributors like the kubuntu people, who care a lot about usabilty for newbies, changed konqueror as default file browser, so I just don't understand the necessity of making dolphin the default.\n\nIn your other post you said that it's also about having a new developer who cares abut the filemanager code. OK, that makes thing sound much more reasonable to me. But would this guy immediately stop the development if dolphin wouldn't become the default file manager? I still think that letting dolphin first prove that it's the better manager and then making it the default would be the better way. "
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-03
    body: "> the dot is representative enough to state that\n\nthe readership of theDot is about as representative of our global user base as i am of nba stars.\n\nthere are some canadians in the nba. and spud webb was pretty short too. but if you have ever seen me, you'd have to admit that even if i do hit the court once in a while i'm not representative of people in the nba.\n\nor perhaps a more apt anology is that the people here are like people who play in the nba: they are not representative of all basketball players. most people who play basketball are shorter, have less talent and certainly less experience.\n\nit is astounding to me how many people fail to grasp the idea that we are not our users. we are some of our users. and a small \"some\" at that.\n\n> Did you expect such a strong reaction?\n \nyes, actually. i'm very aware of how loud people in this community can be. sometimes to the point of being obnoxious, particularly when anyone dares to do something progressive that disturbs the status quo.\n\nbut being loud does not make you right (or smart, or reasonable). it just makes you loud.\n\n> But would this guy immediately stop the development if dolphin wouldn't\n> become the default file manager? \n\nno, because it had already existed for quite some time prior to this.\n\n> I still think that letting dolphin first prove that it's the better manager\n> and then making it the default would be the better way.\n\ngood idea. maybe that's why we picked an existing application with an active maintainer who was interested in kicking it up a notch. see, we do agree on some things!"
    author: "Aaron J. Seigo"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-03
    body: "> see, we do agree on some things!\n\nOh, I think we'ld agree on a lot of other things, too, but unfortunatly not on which default file manager KDE 4 should ship with, which is by an almost malicious draw of fate the topic of this discussion ;)\n\nWell, you're the developer, I'm the user, so I trust in your experience that dolphin as a default file manager is the right decision, although I'm not convinced.\n\n> but being loud does not make you right (or smart, or reasonable). it just\n> makes you loud.\n\nBut even loud minorities are not obviously wrong and just afraid of changes of the status quo. Where, if not here, should KDE users state their objections? So, thanks for listening!\n\nMaybe the whole problem is that for some users the reason haven't become clear: I'm sure that I could reconfigure my desktop to have konqueror as default file manager, so why all that worry? I can't speak for others, but it looks like switching to dolphin as default takes away some of the power and elegance of KDE.\n\nMaybe these people are afraid of a change in paradigms and a regression in KDE's flexibility.\n\nMaybe you shouldn't think of these users not as a \"loud, conservative\" minority, but as enthusiastic KDE 3 users. Not everything new is automatically better, and are these people who call konqueror as a file browser \"bloated\", embedded viewing inconvenient a \"better\" loud minority? ;)"
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "\"Only because the file manager can't browse the web and cook coffe?\"\n\nCome on, that's not the way to discuss that (although I must admit that my first posting was also a bit pathetic ;) ) The point is that konqueror is the heart of KDE, and was intentionally developed as file *and* web browser. AFAIK users were happy with that and konqueror was never a source of steady discussion, as Nautilus or the Gnomes \"The-user-don't-need-to-know\" oversimplifications.\n\nMaybe I messed something, but I never heard complaints about konquerors file browsing abilities, or that it would be to complicated to use. The opposite is true: Other DEs envied KDE for this level of integration. So if it's not broken, don't fix it.\n\nWhen Dolphin will evolve you surely will see more and more sophistcated previews for different file formats, and when html preview will become integrated ... well, we already have that application: It's called konqueror.\n\n\"KDE is focusing on proviving one tool for one task\"\n\nThe good old unix philosophy ... Do just one thing and do that right ;)\n\nYes, but you forgot one thing: These \"little\" tools are meant to construct powerful commandlines by *combining* them. So in this terms that means Konqueror needs a good file manager part, a good html rendering part, a good image preview part, ... and combining it under one konqueror shell gives you a mighty tool. And that is the concept behind konqueror and it's IMHO still superior to the one separate application with different GUIs, strength and weaknesses per task.\n\nKonqueror has a level of abstraction that is the logical continuation of another philosophy that Unix like operating systems made that good: Everything is a file. In Konqueror everything is a url. And that's an elegant and easy to understand concept, that you shouldn't give up for some questionable GUI improvements, at least nor without asking the users. I think the parallels to the gnome development and it's problems are obvious, and I really wonder why KDE wants to make the same mistakes, especially at such a central point and where KDE has long time proven to have the better concept. Ask gnome users which KDE application they'ld most like to see in gnome, and I'll guarantee you it will be k3b, amarok and *konqueror*.\n"
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "\"The point is that konqueror is the heart of KDE\"\n\nNot for me. For me the heart of KDE is kdelibs. So it's KIO-slaves, kpart, DCOP, ...\nKonqueror is only the application that uses most of these at once.\n\n\"AFAIK users were happy with that and konqueror was never a source of steady discussion\"\n\nThere where a lot of discussions. Main criticism: konqueror is bloatet and messed up. And it still is a valid argument imho (profiles would need much more improvement).\nWhile it may not be that konqueror is \"broken\", it definitely can be improved a lot.\n\n\"When Dolphin will evolve you surely will see more and more sophistcated previews for different file formats, and when html preview will become integrated ... well, we already have that application: It's called konqueror.\"\n\nPreviews are different to full views! For example for a HTML preview no bookmarks (and all it's GUI) is needed. KHTML is more or less enough. No need for a full browser...\n\n\"and combining it under one konqueror shell gives you a mighty tool\"\n\nYes, konqueror is mighty. But it is bloated/messy too :-(\n\"My\" killer-feature of konqueror are the KIO-slaves. But they can be used with dolphin, krusader and every filedialog too. I don't need a full browser for that in addition...\nBut that's only my preferences.\n\n\"In Konqueror everything is a url\"\n\nHmmmm - I don't get what you mean here. can you expailn this a bit further please?\n\n\"Ask gnome users which KDE application they'ld most like to see in gnome, and I'll guarantee you it will be k3b, amarok and *konqueror*.\"\n\nK3b: One tool for one task. Burning CDs in konqueror could be done quite nicely. But users prever the specialiced application?\n\nAmarok: One tool for one task. Ask the Amarok developers, why they don't support video.\n\nkonqueror: Are you sure? (won't it be kdevelop?) I doubt...\n"
    author: "Birdy"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "\"Not for me. For me the heart of KDE is kdelibs. So it's KIO-slaves, kpart, DCOP, ... Konqueror is only the application that uses most of these at once.\"\n\nWell, it's the shell for all the nice things you mentioned. That where a user sees kdelibs in action.\n\n\"konqueror is bloatet and messed up\"\n\nYou may see it that way, but I don't see so much bloat. Just if you criticize the concept of a shell for different parts, it's bloated. But that's like accusing wikipedia that users can edit the articles. You may like it or not, but it's the concept.\n\n\"it definitely can be improved a lot\"\n\nSure, and IMHO that's the way to go, that's what this whole discussion is about ;)\n\n\"For example for a HTML preview no bookmarks (and all it's GUI) is needed\"\n\nBut isn't it an advantage to have konqueror's bookmarks transparent and uniform, wether it's a local pdf or a remote html file? Why should I want to use two different bookmark systems?\n\n\"I don't get what you mean here. can you expailn this a bit further please?\"\n\nWith the integration of the kparts, I can type any url into konquerors address bar, with the nice kio-slaves even things like fish://, imap://, ... and of course also http:// I still can decide with a left or middle click if I want to see it embedded or in en externel viewer. Dolphin is mainly just for one type of urls: file://, or did I miss anything about the concept of dolphin?\n\nOf course, CD writing and music collection managment are highly specific tasks which are better done in separate applications (well, before Amarok, konqueror wasn't that bad to nurse my mp3 collection). Writing documents, programming, ... are also such tasks, where you need a special \"IDE\". But file-management is a task where konqueror was designed to be the \"natural\" IDE, and I don't see where it does it that bad that we need something different as default.  "
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "\"Well, it's the shell for all the nice things you mentioned.\"\n\nPutting it all together in one place does not necessarily make it a good thing.\n\n\"Just if you criticize the concept of a shell for different parts\"\n\nThat's what I'm doing...\n\n\"\"it definitely can be improved a lot\"\nSure, and IMHO that's the way to go, that's what this whole discussion is about ;)\"\n\nI think it's much easier to start it from a ned point, than to fix it over and over again (last attempts all didn't work out too good).\n\n\"Why should I want to use two different bookmark systems?\"\n\nBecause bookmarks can be used in different ways:\n* for file management (bookmark \"~/work/papers\")\n* for information management (bookmark \"dot.kde.org\")\n\n\"Dolphin is mainly just for one type of urls: file://, or did I miss anything about the concept of dolphin?\"\n\nDolphin handles KIO-slaves just as nice as konqueror, krusader oder any KDE file dialog. Dolphin is not able to embed other views, but is only intended to manage files (locally or remote). I'm don't able to see the benefit of embeding other views, but see some downsides.\n\nKonqueror surely is doing nice in managing files. But every time I use konqueror as file browser, I can see the bloat of \"konqueror the browser\". And as soon as I use konqueror as file manager, I can see some glimpses of the file manager bloating the browser UI.\n"
    author: "Birdy"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "Once again, please read before you comment.  I've said this like 6 times already in the comments...\n\nDolphin can do ftp, fish, etc."
    author: "Troy Unrau"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "Please also read again ;)\n\n\"With the integration of the kparts, I can type any url into konquerors address bar, with the nice kio-slaves even things like fish://, imap://, ... and of course also http:// I still can decide with a left or middle click if I want to see it embedded or in en externel viewer. Dolphin is mainly just for one type of urls: file://, or did I miss anything about the concept of dolphin?\"\n\n\"mainly\" and \"type of\" are the keywords. All kio-slaves that dolphin can handle are treated in a way that a file manager can do. That's the idea, isn't it? \n \n"
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-02
    body: "\"All kio-slaves that dolphin can handle are treated in a way that a file manager can do. That's the idea, isn't it?\"\n\nYes. And that's what KIO-slaves are about. I think you mess up KIO-slaves and kparts.\nSo to sum it up:\nDolphin is able to browse (remote) via ftp://, imaps://, sftp://, ipod://, ...\nDolphin isn't/won't be able to show pdf/jpg/... files embeded.\n\nBut it is possible to browse \"sftp://some.host/home/hamster/data/\" click on \"file.pdf\" there, and okular pops up, showing the document.\n"
    author: "Birdy"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-05
    body: "From your post:\n\"Amarok: One tool for one task. Ask the Amarok developers, why they don't support video.\"\n \nFrom the KDE Commit-Digest for 4th March 2007, http://dot.kde.org/1173081506/\n\"Initial video support in Amarok, with heavy interface redevelopment underway for version 2.0.\"\n\nSorry, couldn't resist, but this surely won't help us in this discussion. I'm a bit disappointed, that most of the criticism was shout down as \"those who criticise don't understand\" by a \"loud, obnoxious minority\" that is \"just afraid of changes\". This certainly also didn't in this discussion.\n\nHe, who codes, decides: That are the rules. And if a selfmade usabilty-test in a coffee shop is representative, but none of the 400+ postings here ... it looks like the decision has been done, and the discussion here was just wasted time.\n\nI just feel sorry for Peter Penz: None of my critics was meant to discourage him. Even if I have objections seeing his file manager as default, I do respect his work."
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-03
    body: "> In Konqueror everything is a url\n\nas it is in dolphin.\n\nthe only difference is when it comes to embedding files. and if you look at it, you'll discover that embedding in konqueror has been completely screwed up because people were trying to make it into a file manager rather than let it be a web and file *browser*.\n\ni actually hope konqueror will return to sanity in its default settings when it comes to embedding, e.g. text documents.\n\n> Ask gnome users which KDE application they'ld most like to see in gnome,\n\ngood thing we're still providing all of those apps you mention then! =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-03
    body: "Now I'm a bit confused ;)\n\nOf course, embedding files is one thing dolphin does not, as it's a file browser \"by design\". So you say the only views that konqueror should embed by default are files and web pages? OK, that should be no problem. So the difference to dolphin would be konqueror could open an embedded view and dolphin can't.\n\nI can't see why a problem that could be solved by \"sane\" (for who?) default settings of an existing application should be solved by hard-coding them into a new application.\n\nWhat's wrong with people who want to view their pdfs embedded? So konqueror as a web browser shows them embedded (as a lot of users like it, and with a simple middle click also in an external viewer), and after the download of the pdf dolphin as a filebrowser shows it in an external viewer and click with the middle mouse button does something completely different? What about the pdf laying on an ftp server, which is linked by a web site? konqueror opening dolphin opening kpdf? Let's hope that that pdf doesn't have any links to web sites ... ;) Three different menu structures, GUIs, ... at the same time on the screen.\n\nI'm not sure if that's more \"sane\", at least it's confusing for newbies. I helped a lot of people (and some are old) with their first steps with Linux/KDE: Believe me, that is calling for troubles! If konqueror has usabilty issues, they are certainly not solved by another application ;). One of Ubuntu's first steps to make Linux systems more newbie friendly was: One application per task.\n\nWell, I wouldn't care, if that would be the only problem. By making it the new default I see another problem: This is weakening KDEs concept of kparts: modularity and integration. Maybe this is wanted by the developers, but then you should announce it that way.\n\nThat's just my $0.02, but looking at the immense amount of replies to this posting, I hope you rethink the decision to make dolphin the default. Seems like this is a very controversial topic."
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-03
    body: "*************Birdy:\nNo, the opposite! KDE is focusing on proviving one tool for one task. While it may be nice to have a knife, one can use as hammer too. Tim Taylor would prefer to have a separate knife and hammer ;-)\n*********************\n\nYou are wrong about konqueror, kde and unix.\nKonqueror is for kde as the *console terminal* is for \nunix. In unix you\nhave one tool for one task integrated by redirections\nand pipes. Kde without a powerfull Konqueror is not\na kde anymore. Konqueror is not *one* tool, is the bench\nwhere the tools are. If you cannot put the Dolphin into\nkonqueror then Dolphin is not a good kde tool. \n\nI want to see a movie, manage files,  play a music and *more*\nwithout to get out of the *bench* (konqueror),  wheres all\ntools for these tasks are available.  \nDolphin does not follow this gold unix rule.\nKde tools are *graphical* tools that need a super taskbar.\nThat powerfull taskbar is Konqueror. Without a more powerfull\nkonqueror,  kde4 is not going further in\ndirection of the good gold unix standard. \n\nOda\n\n\n"
    author: "oda"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-04
    body: "Your \"bench\" may be konqueror. I use \"the desktop\" (kwin/kicker/...) as \"bench\".\nYour taskbar is konqueror. My taskbar is inside of kicker.\n\nI have a powerful desktop which acts superb in handling several applications. So I favor to use this one, than to squeeze several applications into one other.\n\nAnd as I stated once before: KDE is much much more than only konqueror!"
    author: "birdy"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-04
    body: "Maybe this is the core of this whole conflict: There are ethusiastic \"kdelibs users\" and ethusiastic \"konqueror users\". Who is right? kdelibs show (partially?) up everywhere, but konqueror is the place where all these concepts come together and form a mighty application.\n\nSo this huge outcry is probably a sign that the switch to dolphin was badly communicated to the \"konqueror users\". Nobody wanted to discourage Peter (at least not me), but if there are any problems with konqueror I'ld like to hear about them first before I'ld welcome changes in a part of KDE which could affect in the long term my everyday work.\n\nThis step seems more and more like a PR debacle: Thiago implied a bit that konqueror's code basis as a file manager has some problems as a reason for the change, Aaron disqualified partially criticism as a \"loud, unprogressive minority\". This gave understandably the \"konqueror users\" the feeling of being victims of politics or lack of manpower, but not a reasonable, comprehensible decision. (Well, lack of manpower is very reasonable ... but then tell it, if that's the reason)\n\nMaybe next time the dev's make such a predictable controversial decision they could inform the users a bit more about the reasons and earlier about rising problems with established applications. Just reading in a blog that your favourite application gets \"degraded\" and you should be happy with that seems to cause at least some heated discussions."
    author: "furanku"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-04
    body: "amen\n"
    author: "John Tapsell"
  - subject: "Re: Sounds not like a good idea"
    date: 2007-03-04
    body: "Birdy, \n\nkonqueror is for kde as bash, cshell\nare for unix. Without the shell \nthe unix kernel became useless.\n\n\nKonqueror is the heart of kde\nas a *productive*  desktop, it is the shell\nwhere all the good specific tools  \nmay be available by context\nand by the flow of the mind of the user, \nunlike ktaskbar, kwin,  kicker\nand karamba. \n\nDolphin is good for kde if its advances can\nbe embedded in Konqueror. If you ignite a \nDolphin x Konqueror war,  Kde4 and its users\nwill lose. \nDolphin *must must* not try to kill\nthe file  capabilities of konqueror.\nDolphin could be seen as a training camp\nto improve konqueror capabilities\nbut this is not what I understood.\nI get the wrong message?\n\nMy opinion as a long time kde *user*.\n\nOda"
    author: "Oda "
  - subject: "Competitor"
    date: 2007-03-02
    body: "\"Dolphin is not intended to be a competitor to Konqueror, the default KDE file manager: Konqueror acts as universal viewer being able to show HTML pages, text documents, directories and a lot more, whereas Dolphin focuses on being only a file manager. This approach allows to optimize the user interface for the task of file management.\" (Dolphin Web Site)\n\ni dont know who made this decision, but did they ever asked himself  \"Why people dont like this decision?? maybe we are wrong??\"\n\n"
    author: "Emil Sedgh"
  - subject: "Re: Competitor"
    date: 2007-03-02
    body: "Yes. We haven't concluded that we are wrong (yet).\n"
    author: "Thiago Macieira"
  - subject: "Re: Competitor"
    date: 2007-03-02
    body: "\"Why people dont like this decision??\"\n\nI like this decision a looooooot."
    author: "Birdy"
  - subject: "Re: Competitor"
    date: 2007-03-02
    body: "but many others doooooooont, about 180 comments (until now), most of them fighting on this!"
    author: "Emil Sedgh"
  - subject: "Re: Competitor"
    date: 2007-03-02
    body: "so how many users do you think kde has? i hope its more than 180 just in my town..."
    author: "ac"
  - subject: "Re: Competitor"
    date: 2007-03-03
    body: "To be true. Most people didn't understand the basic things and jump to conclusions.\n\nThis is all only in-fighting with 3 kinds of people.\n\na) Kind A: \"Don't waist your time doing something I wouldn't want.\" These people have no say in the Free Software world. My reply #1 to those: Without good competition, and fresh ideas, Konqueror is going to stagnate too much. Without competition there is no progress. My reply #2 to those: You have no right to demand or wish that people shouldn't do what they find to be fun. They are Free to do this.\nb) Kind B: \"A tool from Gnome was mentioned, Nautilus. Not even a good one, PANIC!\". These people do not trust KDE developers, and they do not respect Gnome maybe. And they didn't understand. The new nav is optional and may be great. My resonse to those: Didn't you\nc) Kind C: \"KDE should decide everything democratic, developer deciding it, oh no!\". Those people trust democrazy, which is good. But actually KDE is a meritocrazy. You got to earn your voice, or you have none. My cycnic response to those: Make a fork of KDE, where you decide everything in a poll and then find people to abide those polls. Maybe you will find that you have to pay them to do that. And then you will end up with people having to contribute money, which is not bad. But some will give more and want to say more. And then it will be like it is now, those who invest the most, the developers, get the final say. It doesn't mean, they don't care what people say, actually they want to be popular after all. But it doesn't mean, they will abide majority, esp. not when the majority doesn't even know what they vote about. My nice response: Look, KDE is really good at making no decisions. Everything is already configurable. That's how KDE achieves to please the majority. This time will be no exception.\n\nSaying that many concerned posts indicate that something is wrong, is not true. And all you are going to achieve is letting developers think, well, why do we tell them before it's ready and they can understand by using it? Your at best only going to close the minds of those that were open to you. And your going to spoil their expectations about how good they can explain things to you. \n\nNo KDE developer seems willing to let anybody's concern here become a reality. So why do you assume so, people?\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "A clear list of features that will be added"
    date: 2007-03-02
    body: "Is there a list of featues that arn't in KDE3Dolphin but planed for the KDE4 version? Currently Dolphin is missing some pritty good file browsing featues found in Konquorer.\n\nI know embedded konsole and Tree view are in the works (http://dot.kde.org/1172721427/1172794484/1172821938/1172826380/ and http://dot.kde.org/1172721427/1172796957/1172827469/)\n\nbut what about file-size view, spliting the screen multiple times or fine grained control over what you display preivews for? And can you enter into the url bar /home/me/*.jpg?"
    author: "Ben"
  - subject: "Re: A clear list of features that will be added"
    date: 2007-03-02
    body: "> Is there a list of featues that arn't in KDE3Dolphin but planed\n> for the KDE4 version? Currently Dolphin is missing some pritty\n> good file browsing featues found in Konquorer.\n\nThere is no official list yet, as we don't want to promise too much. But the capabilities of Dolphin in KDE 4.0 will be > than the KDE 3 version of Dolphin. Also Dolphin will be improved after KDE 4.0 :-)\n\n> but what about file-size view,\n\nIs not planned yet, but is worth to consider IMHO.\n\n> spliting the screen multiple times\n\nNot planned yet, depends on the input from the users. Technically it would no be a big problem to offer this.\n\n> or fine grained control over what you display preivews for?\n\nis planned\n\n> And can you enter into the url bar /home/me/*.jpg?\n\n... also planned\n\nBest regards,\nPeter\n\n"
    author: "ppenz"
  - subject: "Re: A clear list of features that will be added"
    date: 2007-03-02
    body: "Thanks for the quick response full of good news :)\n\n>> spliting the screen multiple times\n\n>Not planned yet, depends on the input from the users. Technically it would no be >a big problem to offer this.\n\nWell my personal user imput would be to do it, its very usefull for quickly cleaning a mess, say splitting one unorganised folder into a bunch of folders. And it will be one less missing feature people could use to say KDE is turning into Gnome. "
    author: "Ben"
  - subject: "Re: A clear list of features that will be added"
    date: 2007-03-02
    body: "IMHO, splitting in multiple views is a geeky feature. You can always open 3 instances of Dolphin, position the windows on your screen and drag&drop.\n\nMore important is that we will have at some point in KDE4 an infrastructure (Nepomuk, Strigi) to tag and search quickly files. It will certainly have an influence on the evolution of Dolphin. "
    author: "cmiramon"
  - subject: "Re: A clear list of features that will be added"
    date: 2007-03-03
    body: "IMHO, since Open Source is developed mostly by geeks, as volenteers, its important for an Open Source project to attract the geek market to maintain a supply of developers. So geeky features are good. \n\n"
    author: "Ben"
  - subject: "Re: A clear list of features that will be added"
    date: 2007-03-03
    body: "And let's be honest, not only the developers are mostly geeks, also most of the users are. But I agree with geek-only functionality this won't change. (Well I wouldn't care.) As long I don't loose my loved geek functions I will be satisfied."
    author: "panzi"
  - subject: "Re: A clear list of features that will be added"
    date: 2007-03-03
    body: "Having to resize four different windows and then navigate them from my home directory to where I'm organizing things is time consuming and tedious.  With split panes, it's also a thing of the past.  Beside that, I don't like having twelve Konqueror windows open- I like twelve tabs at different locations.  Why would I suddenly want to spawn extra windows?\n\nI'm curious, what happens if you enter a web address in Dolphin?  Does it shift that tab to konqueror via Kparts and go about its merry way?  That right there would dispell a lot of resistance I have to it."
    author: "Wyatt"
  - subject: "Re: A clear list of features that will be added"
    date: 2007-03-03
    body: "> it will be one less missing feature people could use to say \n> KDE is turning into Gnome.\n\nyeah, because that'll make us change our mind! seriously, how old do you think we are?"
    author: "Aaron J. Seigo"
  - subject: "A comment from a long time user..."
    date: 2007-03-02
    body: "I've been using KDE from the beginning and I've always preferred it comparing to GNOME not only by the added functions but also by what I perceived as well thought and integrated decisions on his architecture.\n\nHowever I don't understand this decision of splitting Dolphin and Konqueror and in my opinion is a clear \"shot on the foot\".\nI don't understand why the decision is not to improve Konqueror as a file browser, making it more flexible and correcting the shortcomings found now, and instead to add \"noise\" with another file browser.\nMore, as a user in my daily work, I frequently need the full power of Konqueror both as a file browser, web browser, ftp browser, etc... AT THE SAME TIME (most of the time splitting it in multiple windows). Also is quite convenient (and faster than open a dedicated application) to look at a file directly on Konqueror.\nFrom what I understand Dolphin will be \"fancy\" but won't support that. At same time I feel that, with time, Konqueror will lag behind as a file browser.\nProbably, if this happens, I will switch to a browser that will still give me all that (maybe in GNOME?).\nAnother consequence of this (bad) decision is that a good number of users will switch from Konqueror (as an all-browser) to Dolphin + Mozilla and drop Konqueror! In the medium term this may slow down the development of Konqueror.\nKonqueror does need some improvements (KHTML is not yet able to see some pages - in this case I still need Mozilla). I.e. I would like to remove Mozilla from my disk if possible (one less fat software). But I don't think this is the good way of doing it...\n\n\"Just my two cents\"\nNP\n"
    author: "Nuno Pinh\u00e3o"
  - subject: "Re: A comment from a long time user..."
    date: 2007-03-02
    body: "\"I frequently need the full power of Konqueror both as a file browser, web browser, ftp browser, etc... AT THE SAME TIME (most of the time splitting it in multiple windows)\"\n\nAnd what if those windows all have different names?\nI mean I too need the full power. But I don't need all the power from one single application. I prefer separate applications, concentrating on theris specific task. And have them working together.\n\nIf you really want konqueror to do everything, why not extend it to be a music manager too? And another kpart/profile for photo management? And CD buring is a must for a \"do-it-all-konqueror\". And shouldn't konqueror be able to act as email client too? What about calendaring? Calculator? Games?\nWhy not rebuild KDE to consist of only one application (konqueror) with hundreds of profiles?\n\nBrowsing the web and managing files are completely different tasks (from a user's point of view). And every task should be best done by an application concentrating on this task (one job, one app).\nKDE's benefit would be to provide superb integration between these applications.\n\n\"(and faster than open a dedicated application)\"\n\nStarting the KPart takes more or less the same time. That's my experience...\n\n\"Dolphin + Mozilla and drop Konqueror\"\n\nI think most people like this, already use konqueror as file manager, and Mozilla as browser. I doubt konqueror will loos users.\n"
    author: "Birdy"
  - subject: "Re: A comment from a long time user..."
    date: 2007-03-02
    body: "Part of the problem is that Tabs are much better at keeping your workspace organised than the taskbar, both visually and in terms of usability. Therefore it is advantageous to have all your workflow views within one application than in separate ones.\n\n"
    author: "Evan Robert"
  - subject: "Re: A comment from a long time user..."
    date: 2007-03-03
    body: "Wait...Dolphin won't have tabs?  I hope I'm just reading into that the wrong way..."
    author: "Wyatt"
  - subject: "Re: A comment from a long time user..."
    date: 2007-03-02
    body: "I totaly agree.\nwhen I update my blog, I have 3 views at the same time:\n- a web view for the ticket\n- an ftp views for content like images/videos/flash players/...\n- a local filesystem view\n\nI drag local files to the ftp and ftp files to the blog ticket so I have the URL immediateley.\n\nHaving profile according to directories would be even better! That would be the kick ass feature!\n- enter the Picture directory and you have the kio_digikam\n- enter the Music directory and you have the kio_amarok\n- enter the PVR directory and you have the kio_kaffein\n- enter simple directory and you have your files\nOf course, the profile would not change according to dirnames, but according to a .directory file or such. some default created directorys could contain preset profiles like ~/Music ~/Video ~/Photos, but it shouldn't be hard-coded.\n\nThis is a seamless way to represent data. end users want to access data and don't want to bother what app is apropriate for that!\n\nIf only I had coding skills......."
    author: "olahaye74"
  - subject: "Re: A comment from a long time user..."
    date: 2007-03-02
    body: "\"End users want to access data\"\n\nI totally agree!\n\n\"and don't want to bother what app is apropriate for that!\"\n\nI totally disagree!\nThey want music - they start Amarok and don't care where the files are.\nThey want photos - they start digiKam and don't care where the files are.\n\nUsers care about data, and they associate one application with each data type. But users don't care about where the data is as long as they can easily access them. Having all your music in a database? Would be fine too, as long as Amarok can access/handle it.\n\nStarting Konqueror and going to \"~/Mail\" for reading mails? No way! Simply start KMail/Kontact.\n\nHaving only one application (konqueror) and lots of kparts seems absurd for me.\nBut ok - some prever to have all their application to be in the web browser (web applications). Maybe it's me who is wrong.\n"
    author: "Birdy"
  - subject: "Re: A comment from a long time user..."
    date: 2007-03-02
    body: "> They want music - they start Amarok and don't care where the files are.\n> They want photos - they start digiKam and don't care where the files are.\n\nI disagree on the following point: having this philosophy leads to tons of redundant non linked databases.\nDocument indexing then becomes a nightmare.\n\nAs for reading mail, IMHO, it's better to see mails than seeing useless amount of files..."
    author: "olahaye74"
  - subject: "Re: A comment from a long time user..."
    date: 2007-03-02
    body: "\"Document indexing then becomes a nightmare.\"\n\nWhy? Is there any difference if your photos are stored in \"~/photos\", \"~/documents/images\" or \"~/.photos/albums\" ?\n\n\"it's better to see mails than seeing useless amount of files...\"\n\nIt's better to see photos than seeing useless amount of files...\nGot it?"
    author: "Birdy"
  - subject: "Dolphin- wishes "
    date: 2007-03-02
    body: "I think that Dolphin could replace Konqueror in file management operations only if\nit will support all remote kioslaves  (ftp/sftp/fish/svn etc.) and integrate archive management available in ark (maybe both project should be merged )."
    author: "terran"
  - subject: "Re: Dolphin- wishes "
    date: 2007-03-02
    body: "Dolphiin will support kioslaves, and archive management in konquorer is done by kioslaves."
    author: "Ben"
  - subject: "Re: Dolphin- wishes "
    date: 2007-03-04
    body: "an other wish is the possibility to place MANUALLY the icons , independent from alfabetic orders, and the possibility to remember theyr position of course and theyr dimension and so on, and the same focus policy of windows too would be a great wish"
    author: "pierluigi"
  - subject: "Thanks Troy!"
    date: 2007-03-02
    body: "Thanks for your great articles,\nand especially for this one about Dolphin,\none of the KDE4 changes I'm really looking\nforward to!"
    author: "Darkelve"
  - subject: "Fileselectorbox?"
    date: 2007-03-02
    body: "Hi,\n\nwhat about fileselectorboxes when you do file->open or file->save? Do we got dolphin style or current style or is this a config option?\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Fileselectorbox?"
    date: 2007-03-02
    body: "Those are neither Konqueror nor Dolphin. The technical name for them is \"kfile\" (named after the KDE 2 library libkfile, which doesn't exist as a separate library anymore).\n\nAll of this thread has been pointing out again and again that Dolphin and Konqueror share the same filebrowsing backends. They are just different shells for the same thing. Like Kate & KWrite, as well as KDevelop, Quanta, Kile, etc. (all of them use the KatePart).\n\nBut no one has mentioned yet that the sharing is done three-way: kfile also shares code with both apps. The only difference is that your File|Open dialog is not meant to be a file manager or much less a web browser :-)"
    author: "Thiago Macieira"
  - subject: "Re: Fileselectorbox?"
    date: 2007-03-02
    body: "\"But no one has mentioned yet that the sharing is done three-way: kfile also shares code with both apps. The only difference is that your File|Open dialog is not meant to be a file manager <snip>\"\n\nWhy not?"
    author: "Darkelve"
  - subject: "Re: Fileselectorbox?"
    date: 2007-03-02
    body: "Because it's a File|Open dialog, not a file manager. If you want a file manager, you start your file manager.\n\nThat is not to say that the dialog will not support *some* file management tasks. I find it handy that I can rename a file inside the File|Open dialog.\n\nIt should also support an easy \"open the file manager here\" option."
    author: "Thiago Macieira"
  - subject: "Re: Fileselectorbox?"
    date: 2007-03-02
    body: "There isn't that much difference between both... as I said in a comment further down, it would be a good idea IMO to replace the file selector with a dolphin instance."
    author: "andi"
  - subject: "Re: Fileselectorbox?"
    date: 2007-03-05
    body: "I am also a big fan of the \"open the file manager here\" option.  I needed it already many times.\n\nI would also like to see such an option in the \"File\" menu of applications such as Okular, KWord, Krita, Kate, etc.  This option should open the file manager in the directory in which the currently viewed/edited file is located."
    author: "Vlad"
  - subject: "Load files externally"
    date: 2007-03-02
    body: "[quote]\nKonqueror will still be mostly present (except that Dolphin will necessarily load files externally instead of using embedding viewers).\n[/quote]\n\nHow come Dolphin has to load files externally instead of using embedded viewers?\nThat's what I liked so much about Konqueror!\nIsn't there any way to embed viewers? \nIs is technically or just policy?"
    author: "Gunter Schelfhout"
  - subject: "Re: Load files externally"
    date: 2007-03-02
    body: "I don't know, I think it may be policy. \n\nThat said if it isn't technically maby they'll put an option in the config menu.\n"
    author: "Ben"
  - subject: "Re: Load files externally"
    date: 2007-03-02
    body: "That's the whole point of having Dolphin: loading stuff externally. It's also IMHO the only line we can draw between a file manager and the web browser.\n\nIf you embed HTML files, you soon end up with a web browser: you're going to click links and expect them to open in the same window you are right now.\n\nThat said, if you find that feature interesting, keep using it. I reserve the right to not like it: opening text files, PDFs, images, etc. in a powerful viewer is better for me.\n\nBTW, Hint: if you middle-click a file in Konqueror's file view, it opens externally, much as clicking a link on an HTML page opens it a new tab/window."
    author: "Thiago Macieira"
  - subject: "Re: Load files externally"
    date: 2007-03-04
    body: "> That's the whole point of having Dolphin: loading stuff externally.\n\nI thought the point of having Dolphin was to have a different user interface to file management. However, if loading files externally is the only purpose of Dolphin, then Dolphin is entirely unnecessary, since Konqueror already can load files externally.\n\n> if you find that feature interesting, keep using it.\n\nBut why shouldn't I be able to use that feature _in Dolphin_?\n"
    author: "A.M."
  - subject: "Re: Load files externally"
    date: 2007-03-05
    body: "> > if you find that feature interesting, keep using it.\n> \n> But why shouldn't I be able to use that feature _in Dolphin_?\n\nI agree.  If I want to do file management tasks, I often want to *quickly* check the contents of a file before I move/copy/delete it so that I am sure that I do this action with the correct file.  Waiting until some external application loads and shows the file is not quick, loading the file in a kpart inside the file manager is.\n\nBTW, I do not claim that the efforts done on Dolphin are useless.  On the contrary, I think it is useful to have an app that is focused on file management (and not on webbrowsing).  Viewing the files in a kpart inside Dolphin is not in conflict with the focus on file management: the purpose of viewing a file in a file manager is *previewing*, not browsing.  Therefore the file manager should not show my (web) bookmarks and other stuff that is specific for web browsing (such as web browsing specific options in the configuration dialog).  To be more clear: in the file manager, the khtml kpart should be considered as one of many kparts, i.e. equal to the kpdf, kghostview, ... kpart.\nOn the other hand, Konqueror can be the Universal Browsing Application, i.e. it is a web browser, but if it encounters a \"file://...\" URL then it can also *browse* your local files.  The file management stuff (move/copy/delete/rename) is of less importance here.  In this context, the bookmarks and the web browsing configuration options should still be available, even if you are browsing local files."
    author: "Vlad"
  - subject: "Dolphin and usability"
    date: 2007-03-02
    body: "I have not yet tried Dolphin and I was wondering how it responds to the usability problems we have with Konqueror as a file manager.\n\n- Selecting files with the mouse was always difficult. Aaron had a demo some times ago of making it possible when you hover over a file to have a small menu to appear with several contextual actions, like selecting. Is this idea tested in Dolphin ?\n\n- Looking at the screenshots, I see that in the splitview the little green light that we have in Konqueror to switch panes has gone. This little green light is clearly a usability nightmare. How is it replaced in Dolphin ? I'm wondering also if in a splitview, fading a little bit the inactive pane would not be a good idea. Splitview is also often used to copy something from a local to a remote place. Having a visual clue, maybe with a different background, showing what is the local pane and what is the remote pane would be nice."
    author: "cmiramon"
  - subject: "Re: Dolphin and usability"
    date: 2007-03-02
    body: "> Selecting files with the mouse was always difficult.\n> Aaron had a demo some times ago of making it possible\n> when you hover over a file to have a small menu to appear\n> with several contextual actions, like selecting. Is this idea\n> tested in Dolphin ?\n\nIt has not been tested yet, but we discussed this internally already. It's to early now for giving a final statement, but we're aware about the problems in KDE3 regarding selections and plan to improve this.\n\n> Looking at the screenshots, I see that in the splitview the little green\n> light that we have in Konqueror to switch panes has gone. This little\n> green light is clearly a usability nightmare. How is it replaced in\n> Dolphin ? I'm wondering also if in a splitview, fading a little\n> bit the inactive pane would not be a good idea. Splitview\n> is also often used to copy something from a local to a remote place.\n> Having a visual clue, maybe with a different background, showing\n> what is the local pane and what is the remote pane would be nice.\n\nIn the KDE3 version of Dolphin the inactive view uses slightly darker colors and a gray breadcrumb view as indication. In the KDE4 version of Dolphin I think we'll indicate this in a similar way, but we'll use the benefits we got by Qt4 to do it even nicer.\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Dolphin and usability"
    date: 2007-03-02
    body: "If Dolphin supports per window backgrounds why not make it default to having a large picture in the background of two networked computers?\n\nYou could diferentiate between a root opened session of Dolphin and a normal one by putting a large bomb in the root session."
    author: "Ben"
  - subject: "Selecting files via mouse"
    date: 2007-03-02
    body: "An idea for file selection: By clicking the middle button (or left and right button together or whatever) on a empty section of the dolphin window, we switch to file selection mode: The cursor image changes appropriately and you can [de-]select items with the left button. Same button combination lets you leave selection mode again."
    author: "andi"
  - subject: "Re: Selecting files via mouse"
    date: 2007-03-05
    body: "That is a good idea. But it may be better then to have a \"Browse\" and \"Selection\" button, similarly as in KPDF."
    author: "Vlad"
  - subject: "Getting rid of the \"open file\" dialog"
    date: 2007-03-02
    body: "The file selection dialog basically is a dumbed-down file manager plus filter options plus \"open file\" button.\nSo when I heard about Dolphin, I had this (rather obvious) idea: Get rid of the \"open file\" dialog altogether and replace it with a Dolphin instance. I don't think it would cost much in terms of resources and it would certainly help overall interface consistency, as two interfaces almost identical in purpose yet different in design would be replaced by a single one.\nAnd as an added bonus, all the goodies of the file manager would automatically be available to the file selector.\n\nWhat do you think of this?"
    author: "andi"
  - subject: "Re: Getting rid of the \"open file\" dialog"
    date: 2007-03-02
    body: "Rubbish\n\nIt means that if I double click on the file I want, exactly what will happen?"
    author: "kolla"
  - subject: "Re: Getting rid of the \"open file\" dialog"
    date: 2007-03-02
    body: "Thank you for your constructive criticism. If you double-click the file you want, the Dolphin window will close and the application you use loads that file."
    author: "andi"
  - subject: "Re: Getting rid of the \"open file\" dialog"
    date: 2007-03-02
    body: "That would be very confusing for pretty much all users (if I didn't know about that before hand and I was given an application that did that I'd just close the dolphin window and wonder where the kfile window was (and probably just go to file -> open again and repeat until I figure that its causing dolphin to open, at which point I'd assume that something had just bugged out).  It really doesn't make much sense that double clicking sometimes opens the file in its default app, and sometimes closes dolphin and opens it in some other app, it would be quite confusing for I think all users actually."
    author: "Sutoka"
  - subject: "Re: Getting rid of the \"open file\" dialog"
    date: 2007-03-02
    body: "Well that could be easiliy remedied by setting the window title to \"Open file...\", showing a large icon of the calling application in the lower left corner where you can drag a file to open, and perhaps by adding a great big flashing \"Please select a file NOW\" sign that causes seizures. Just think of it as a filemanager window with a different default application set for all files.\nOnce users get used to it, I expect they will not have a problem with selecting files via the filemanager interface. We shouldn't be afraid to confuse first-time users a bit (only not too much mind you), or else we wouldn't be able to change any aspect of the UI. Ever."
    author: "andi"
  - subject: "Digg"
    date: 2007-03-02
    body: "I don't usually do this, but somewhere has written up a Digg article here and I'd quite like to see it bumped to the front page:\n\nhttp://digg.com/linux_unix/The_Road_to_KDE_4_Dolphin_and_Konqueror\n\nThe reason I make an exception in this case is that this article is not merely promotional, and it would be good to get some opinions on the Dolphin As Default issue from people who *don't* necessarily frequent dot.kde.org.\n"
    author: "Anon"
  - subject: "Why not improving Konqueror?"
    date: 2007-03-02
    body: "[quote]Dolphin is a whole different animal. It is a 'real' file manager - it's interface has a lot of elements which are specific to a file manager and cannot really be justified in a browser. This is best demonstrated with a screenshot.\nDolphin in KDE 4.x showing breadcrumbsNotice the implemention of a 'breadcrumb'-style directory selector, which works well for file management in a lot of cases, but is totally useless if you need to enter a URL when using a browser, and so becomes the sort of widget which is only useful when dealing with file hierarchies.[/quote]\n\nI think all of these extra filemanagement functions could be implemented in Konqueror when it displays a directory. Take a look at these mockups: \n<a href=\"http://www.kde-look.org/content/show.php?content=39993\">Konqueror 4.0</a>\n<a href=\"http://www.kde-look.org/content/show.php?content=40135\">\"Navigation Bar Reloaded\"</a>\n<a href=\"http://www.kde-look.org/content/show.php?content=35637\"Clickable slashes in path bar(filemanag)</a>\nI definitely like the solution of clickable and editable navigation bar in these mockups."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Why not improving Konqueror?"
    date: 2007-03-02
    body: "Totaly agree, THIS is the way to go!\n\nSeamless access to data what ever the datatype is or the storage location.\n\nMany people think that a file and a picture are 2 different things. Having a separate file browser and a picture manager won't help ;-)\n"
    author: "olahaye74"
  - subject: "Re: Why not improving Konqueror?"
    date: 2007-03-02
    body: "\"Many people think that a file and a picture are 2 different things.\"\n\nMaybe because they are?!?\nA file is only the place, the picture is stored in. Pictures could also be stored in databases for example. A picture manager could access the picture both ways. But a file browser - well ...\n\nIf a user wants to handle pictures, she doesn't care about files at all. All that matters is to have an easy way to view/edit/manage/burn/... the picture/data. If it's a file(-system), a database or whatever that stores the data is totally unimportant.\n\nHave a look at iTunes, Amarok or digiKam. Why are they so succesful? I tell you: Because they hide the file system from the user, so he can focus on the actual data and tasks.\n"
    author: "Birdy"
  - subject: "Re: Why not improving Konqueror?"
    date: 2007-03-02
    body: "That is was I was trying to tell but you misunderstood me.\n\nUser doesn't matter how the data is stored, thus I wonder why splitting konqueror to manage data that is stored in a file just because it is stored in a file.\n\nTake the example of a photo album.\nyou can have a web based copy\na file based copy that you browse via konqueror\na file based copy that you browse via digikam\n\nNow imagines you have bookmarks for your photo albums and forgot wich versions lack a specific photy. You have to start 3 software to find out were is the missing photo.\n\nThat's why I also wonder why people want to see pdf files in a specific window? what is the benefit from konqueror windows? both windows provides zoom, navigation, print and such ... so what is the problem??? Wors, in the separate view, if you click on an URL, either it does nothing or it opens a new web browser.\n"
    author: "olahaye74"
  - subject: "Tabbed filemanagement"
    date: 2007-03-02
    body: "[quote]You can tell from Konqueror's default configuration of using tabs, and various other related interface choices that Konqueror is now mostly a web browser that also happens to do file management.[/quote]\n\nI think tabs are very useful for file management also even if it is more common in web browsers."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Tabbed filemanagement"
    date: 2007-03-02
    body: "I have to agree there I find myself hitting Ctrl+T in windows explorer and then sighing as I realise that I have sold my soul for hardware support to print of an essay :'(\n\nWell konqueror is the best file manager/web browser I have ever used, period.\nThis is my opinion so I am free to change.\n\nI like the fact that Dolphin does what it does so well, and I can see dolphin and konqueror sharing a very large code base for file management, and of course solid :)\n\nThe main pro's with dolphin imho is the fact that it's configurable for file management. As I have heard voiced konqueror 'could' have had a configuration for web browsing and file management that are completely separate however this would probably cause my favourite app to become split.\n\nI like konqueror the way it is, it could be customised to do file management but if I did require a dedicated session for file management on a more complex level I would welcomely give Dolphin a try (as long as it has tabbing or tabbing equivalent ;P)\n\nThe idea of a serious app for file management that couldn't allow tabs or better for moving between 2 directories that are far apart i.e. ftp://your.site.here and /home, scares me. Moving files between these 2 directories seems best done with tabs to me or something equivalent.\nThe nearest comparison to this is using the directory bar, which I have no problem with, just I would prefer to have both directories open in the same program in the same window.\n\n\nInteresting Idea but could a dolphin window be used to manage files within Konqueror as a tab option like a page can be displayed in explorer in a new tab in windows firefox?\n(Probably not but I recon that this could go down an interesting road)"
    author: "Bob"
  - subject: "Re: Tabbed filemanagement and reasoning"
    date: 2007-03-02
    body: "I als think that tabs are an essential element of a file manager nowadays, introduced by (if I'm guessing correctly) Konqueror itself. Taking these out again would take away the ease of file management, not making it less complex or easier to use. Imho it is not a \"pro\"- feature that needs to be hidden from an average user who is operating with files.\n\nReasons for tabs:\n\n- it greatly helps unclutter the task bar. Just like it helped collecting the dozen web sites in a single entry for browsers, it is not uncommon to have half a dozen different folders or more opened, which don't need to be spread on the whole taskbar. Also the folders don't need to be accessed via the taskbar (which is at least one additional step), but can be accessed via the already openend window.\n- the only other mechanism for handling of multiple places I can see is the bookmarks area. Tabs cannot reasonably be replaced by these. First, they are not as fast and uncomplicated to navigate e.g. when you want to move files compared to a tabbed view, and second, often the folders which you want open concurrently aren't necessarily ones that you deem important enough to dedicate an own bookmark to.\n\nI think tabs are recognized as what they are and included into the workflow during file management through the widespread introduction in web browsers, now that even Internet Explorers has these.\n\nExample: You have the following opened windows: a window with your photo collection and the the folder of your camera in tabs. In another window (to have a logical separation), you have your download folder with new files, a document folder (perhaps you want to move files here) and a working folder with some more/other data and whatnot opened with which you want to work.\nThe importance of the folders as you have them opened is non-permanent, it is gone once your tasks are done. So bookmarking doesn't make sense.\nAlso, you want them them accessible exclusive in a normal view (no splitview) without hassle, so what do you do without tabs? Have this functionality cluttered and less useful/accessible in the taskbar? Search for the correct folders and first have to arrange them next to each other? Searching for the window title of the other window again to drag this one around? Surely you don't want to navigate from within one window to the other folders. What if you have separated out files? This work is gone when you leave the folder. Also I don't want to leave that folder simply because it takes time and effort to navigate back to it.\nNow that other documents (pictures, pdfs, etc.) are opened in own viewers (-> entry in taskbar), I think it wise to group the folders together, which is quite like the base from which you operate.\nAlso think of notebooks without mouse. Do I really have to do _window_ management if I want to have more than only one folder opened? I want to do _file_ management!\n\nThe only hurdle I can see is a fitting integration of this element into the user interface which which is neither visually disturbing nor doesn't get in your way if you don't use it.\n"
    author: "Phase II"
  - subject: "Re: Tabbed filemanagement"
    date: 2007-03-03
    body: "Krusader is a file manager (not a dual-use file manager and web browser like Konqueror) and it also supports tabs. (There are independent tabs for each of the 2 panels.)"
    author: "Kevin Kofler"
  - subject: "Re: Tabbed filemanagement"
    date: 2007-03-03
    body: "Krusader is a dual-pane file manager. Dual panes are not tabs. Those are two different things.\n\nThat said, tabbed file managing is something I definitely miss in Dolphin. The single split view feature is just not enough sometimes, specially if you want to keep a certain \"view\" (like Media or FTP) active at all times."
    author: "Jucato"
  - subject: "Re: Tabbed filemanagement"
    date: 2007-03-04
    body: "Read my message again, you can have tabs WITHIN each of the 2 panes! For example, you can open 5 tabs on the left and 3 on the right, and switch tabs on the left and right independently."
    author: "Kevin Kofler"
  - subject: "STOP DON'T DO IT! KONQI IS THE BEST!"
    date: 2007-03-02
    body: "Ok hold on guys, if we really want this thing called dolphine, why should i use kde??? there is one already called nautilus which we all think SUCKS! so why copy it to KDE. KONQI is now the B E S T file manager ever... do not waste time on this fishy thing...\n \n \n Thanks, \n konqi"
    author: "edi"
  - subject: "Re: STOP DON'T DO IT! KONQI IS THE BEST!"
    date: 2007-03-02
    body: "Don't you think that the people that have developed konqueror are capable of creating something even better?\n\nI think that people with your conservative attitude should have some more trust in the developers. (Or just keep using konqueror, of course. ;-))"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: STOP DON'T DO IT! KONQI IS THE BEST!"
    date: 2007-03-02
    body: "What I see so far, is that this is an attempt to copy gnome attitude in, instead of streamlining konqi and making it easy to use, or is this a too big challenge!!!\n\nby the way, if why not copying the multi panel of finder? if copying is the game, just lets copy the original not the cheap and unsuccessfull imitation. \nby the way, I think Dolphine developers as well as nautillus ones should go back and watch nerd.Tv eposode with Andy hetzfeld. he will tell you what happened to nautillus and what is it worth, also what they wanted it to be is exactly what came out of konqui, so all I am saying is that konqi is a peak, and it will be hard to get higher!\n"
    author: "edi"
  - subject: "Re: STOP DON'T DO IT! KONQI IS THE BEST!"
    date: 2007-03-02
    body: "Do you think Nautilus has ZERO good features?  I'm not a nautilus or gnome fan, but I'm sure it has at least ONE good feature.  The breadcrumb location bar seems to be a very good feature (at least for some use cases), so why not copy it?  Also the Dolphin devs are planning on adding the multipanel finder style view (thanks to a new class in Qt 4.3 it'll be easy for them).\n\nWhen ever you get to a peak, you are only at a new bottom, you're never actually to the top.  I'm most likely going to stick with Konqueror, but when KDE 4 comes out I'm definitely going to give Dolphin a shot because it has some interesting features.\n\nFYI, the reason Konqueror wasn't simply 'streamlined' is because the people familiar with Konqueror's codebase knew that there was a lot of bitrot in it and it would be easier to improve Dolphin than to make Konqueror's profiles actually work right."
    author: "Sutoka"
  - subject: "Re: STOP DON'T DO IT! KONQI IS THE BEST!"
    date: 2007-03-02
    body: "breadcrumb navigation is the worst thing ever created. Concept seems sexy, but which of its fans dit use it intensively on a daily basis?\n\nWith this type of navigation:\n- when there are lots of sub directories: you have a giant drop down menu while with predictive type like konqueror, the more you type, le smaller it get. It's just like replacing autocompletion in the shell by a curses based interface you controle via arrow keys\n- what about directories that starts with a dot?\n- what about directories you d'ont have permission to navigate? (HOW do you go throu chmod 711???)\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: STOP DON'T DO IT! KONQI IS THE BEST!"
    date: 2007-03-03
    body: "You react as if the breadcrumb bar is the only way to navigate in Dolphin (or in Nautilus for that matter). \n\n- when there are lots of sub directories: most of people w/ this kind of setup  type in the direct location anyway.\n- directories that starts with a dot: Indeed this is a feature worth asking about. For once, you have an actually rational question.\n- directories you don't have permission to navigate: Then you can't navigate into them. Why, can you navigate into them in Konqueror, without running Konqueror as root or changing the directory's permission first?"
    author: "Jucato"
  - subject: "Re: STOP DON'T DO IT! KONQI IS THE BEST!"
    date: 2007-03-05
    body: "Point 1) So you admit that breadcrumb is unable to address some situations that were adressed by the previous solution.\n\nPoint 2) It's maybe because I'm a real life user? For once, I recieve an answer different from \"dot dirs shouldn't be accessed by browser as they are often system directories\".\n\nPoint 3) /home is chmod 711. This means you can cd in that directory, but you can't readdir you must know the file/directory name you want to use. thus as a user, you don't need to browse /home, but you may be interrested in browsing /home/username.\n\nI should have stated directory you can't browse but can navidate thru.\n\nAnyway: question still there: How do you navigate using breadcrumb method to a path were some upper directory has 711 permission and you're not the owner?\n"
    author: "olahaye74"
  - subject: "Dolphin +/-"
    date: 2007-03-02
    body: "Dolphin <> Konqueror\nI have now set-up Dolphin as default file manager, and have following observations: -\n+ Is fast\n+ A bit lighter than Konqueror\n+ 'breadcrumb'-style directory selector\n+ Very simple\n- Missing embedded views > I disliked them one they were made a part of Konqueror. I still have disabled embedded views for all uses except viewing tar/zip files.\n- Tree view > Actually i did not noticed it after usage of 2-3 days, only came to know about it on this page itself!\n\nAbout Konqueror: -\n+ You all know ;)\n- 'breadcrumb'-style directory selector\n- Profile Management\n\nI'll use Dolphin if it replaces its -s with +s, otherwise continue with Konqueror."
    author: "Yogesh M"
  - subject: "Re: Dolphin +/-"
    date: 2007-03-02
    body: "dolphin is easy and fast beacuse it lacks all advanced features :>"
    author: "lukilak@we"
  - subject: "Re: Dolphin +/-"
    date: 2007-03-05
    body: "rubbish. I could go on to point out all the advanced features, like kioslave support, split views, intelligent file sorting, an,d so on that it has. but seeing as you probably wouldn't listen anyways it would be a waste of my time. "
    author: "illogic-al"
  - subject: "We do not want a second GNOME!"
    date: 2007-03-02
    body: "Well, in my opinion Dolphin is a bad idea. Why do we need another file manager. If I want to use a \"real\" file management tool, I will take Krusader. Dolphin is useless and looks just like a copy of Nautilus. The universality of Konqueror was the only reason why I have taken KDE as my desktop environment on Debian. "
    author: "Ian Yaacov Levenfus"
  - subject: "Re: We do not want a second GNOME!"
    date: 2007-03-02
    body: "Good thing Konqueror isn't being removed, only not the default for file management (still the default for web browsing)."
    author: "Sutoka"
  - subject: "Re: We do not want a second GNOME!"
    date: 2007-03-02
    body: "Yes! Improve Konqueror! Don't split our resources on two projects when all the Dolphin features could be implemented also in Konqueror but not vice-versa!"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: We do not want a second GNOME!"
    date: 2007-03-05
    body: "Point is, you can't implement one important feature from dolphin in konqueror: it's simplicity. Now I won't be using dolpin a lot, I love konqi. But I think it can be helpfull for many users out there, so why not? All other OSses and DE's have a sperate filemanager and a seperate browser (yes, MS since IE 7) and I guess there are reasons for that ;-)"
    author: "superstoned"
  - subject: "FUD ;)"
    date: 2007-03-02
    body: ">For example: the \"Up\" arrow is still available on the toolbar even when browsing\n>Google Maps, but it is totally irrelevant in this context; another is having a web\n>bookmarks toolbar visible while sorting icons in your /home folder.\n\nThis is because your konqueror is badly configured ;)\n\ngnumdk@flanders:~/.kde/share/apps/konqueror/profiles$ grep XML *\nfilemanagement:XMLUIFile=konqueror.rc\nwebbrowsing:XMLUIFile=web.rc\ngnumdk@flanders:~/.kde/share/apps/konqueror/profiles$\n\nAnd here my file manager:\nhttp://hibbert.univ-lille3.fr/~cbellegarde/konq_file.png\n\nhere my web browser:\nhttp://hibbert.univ-lille3.fr/~cbellegarde/konq_web.png"
    author: "gnumdk"
  - subject: "Re: FUD ;)"
    date: 2007-03-02
    body: "Huh, I use the up button almost exclusevely for webbrowsing, where it makes perfect sense for me - it's one the really cool things in Konqueror that I miss immidiately in any other web browser."
    author: "kolla"
  - subject: "Re: FUD ;)"
    date: 2007-03-02
    body: "Did you accomplish this with just the profile functionality?  I have tried to do similar things with no success.  It seems that any change made to the toolbar or menu is inherited by all profiles.  So what am I doing wrong?"
    author: "Tim"
  - subject: "Re: FUD ;)"
    date: 2007-03-10
    body: "Try e.g. to have different icons sizes for different profiles.\nToolbar saving is broken (design flaw obviously) and has to be fixed."
    author: "Kray"
  - subject: "Makes me worried..."
    date: 2007-03-02
    body: "...that the baby may be thrown out with the bathwater.\nThe integration found in Konqueror was the most important factor when I choosed KDE (and that I kept on using it).\nThere is a real risk that newcomers will use Dolphin and Firefox instead of Konqueror, just because \"usability experts\" say that Konqueror is too complicated.\nThere sure are things that can be improved - like profiles, menus just to name a few - but I just can't see the point of dividing the best part of KDE. Make it better instead.\n"
    author: "G\u00f6ran J"
  - subject: "Dolphin + Konqueror"
    date: 2007-03-02
    body: "After reading all comments (180+ comments, WOW!!!) and watching some KDE-GNOME wars, in my opinion why people keep calling Konqueror \"bloated\" is because there is no clear separation between webbrowser + filebrowser, e.g: why do you need \"configure spellchecking\" and \"print\" in a filemanager? (some of them are \"greyed\", but still there). But besides that, many people like Konqueror for universal viewing application / swiss army tool. Anyway, I have two ideas in my brain:\n\n1. Improve profiles handling in Konqueror (like many others have suggested).\n2. Set Dolphin as file manager, and make sure that it will have all killer features of Konqueror and set Konqueror as full featured Webbrowser.\n\nPersonally, I prefer Konqueror like in 3.x series (but I wish they had better profiles handling), however, given that there are people who wish to actively maintain Dolphin, I 100% agree with this decision. I also really love the KParts embedded things, as I really like to \"torture\" my Konqueror ;), for example if I'm given few links to pdf files, I just use my middle click mouse for every links and few seconds later I have all pdf documents ready :D\n\nFrom a KDE freak"
    author: "freds"
  - subject: "Re: Dolphin + Konqueror"
    date: 2007-03-02
    body: "\"1. Improve profiles handling in Konqueror\"\n\nWhich is an extremely hard task, which no one seems to be willing to to.\n\n\"2. Set Dolphin as file manager, and make sure that it will have all killer features of Konqueror and set Konqueror as full featured Webbrowser.\"\n\nWhich is rather the way it's going to be (it seems). The only feature dolphin will  miss compared to konqueror will be to embed other views/kparts/applications.\n"
    author: "Birdy"
  - subject: "Re: Dolphin + Konqueror"
    date: 2007-03-02
    body: "\"Which is an extremely hard task, which no one seems to be willing to to.\"\n\nDid you forget the \":)\"?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Dolphin + Konqueror"
    date: 2007-03-02
    body: "\"Improve profiles handling in Konqueror (like many others have suggested).\"\n\nI agree. And session management would also be neccessary for powerful use."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Dolphin + Konqueror"
    date: 2007-03-03
    body: "> 1. Improve profiles handling in Konqueror (like many others have suggested).\n\njust because lots of people who are completely unfamiliar with the code and probably lack the expertise to write complex software in the first place say something doesn't make it true. it just makes them loud.\n\nthe suggestion sounds easy to the innocent ear, but it's anything but. in fact, it would, imho, actually make konqueror worse once all was said and done.\n\n> I just use my middle click mouse for every links and few seconds \n> later I have all pdf documents ready \n\nyeah, i do this too. love it =) konqueror will maintain this feature, of course."
    author: "Aaron J. Seigo"
  - subject: "I like konq"
    date: 2007-03-02
    body: "I don't use lonqueror as my default browser, there are better more secure and often more convenient options, but it is at least a decent browser and if it can be trusted not to 'give away the farm' I don't see a real advantage to an app that simply removes a convenient functionality that is almost trivial to it's \n\nI like the up button on websites - should be (an option) in all browsers.\nGranted, up one level may often get you a forbidden message, but up again may take you to somewhere interesting. Naturally you could select the branch of an address by hand, backspacing to the  slash, but the up button is very convenient here.\n\nI love the x button of konqueror's location bar.  Another option that all navigators (file/web) should have, but don't (save for a firefox plugin) - especially with a linux-like clipboard system (middle click to paste selected would fail if the location bar couldn't be cleared without selecting, right?).\n\nI hate bread crumbs.  Too slow compared to konq's predictive entry or cut and paste.  I'd rather navigate in a bash shell than use bread crumb buttons.\nI am thankful that many apps that use \"breadcrumb\" navigation offer a location bar alternative by pressing \"Ctrl-L\" (a nearly universal key-combo the Gimp! an most media players.) \n\n\n"
    author: "strongheart"
  - subject: "Make Dolphin an Option, Not Default"
    date: 2007-03-02
    body: "As most people like to keep Konqueror and some hve said that DOlphin will be the Filesmanager of Kde, i suggest du opposite :\n\nKeep Konqueror and the Power and let the User switch to Doplhin if he wants..\n\nyou can profile the konqueror default to look like doplhin though"
    author: "lukilak@we"
  - subject: "Re: Make Dolphin an Option, Not Default"
    date: 2007-03-03
    body: "That's definitely not my opinion. The power user, if he or she is a real power user, will be capable of switching to konqueror if he wants to do so. The \"normal\" user often isn't.\n\nInstead I'd like to suggest Dolphin as default, with Dolphin having a menu command\n\nOpen in Konqueror, \n\nand Konqueror having a menu command\n\nOpen in Dolphin,\n\nwhich both just close the current app, opens the other, asking if one would like to make it the new default.\n"
    author: "MM"
  - subject: "icon size"
    date: 2007-03-02
    body: "A big problem for me are dolphin's icon sizes. I either can use 64 (too small for my display/my eyes) or 128 (too big, takes up too much space). There are a lot icon themes out there with icon sizes 96x96 and 72x72. Konqueror can show those sizes. Why not dolphin? I hope this will be corrected in the next/KDE4 version."
    author: "JaKi"
  - subject: "Re: icon size"
    date: 2007-03-05
    body: "I hope they have all icons as SVG, and you can zoom pixel by pixel... :D"
    author: "superstoned"
  - subject: "view profiles"
    date: 2007-03-02
    body: "I think the development of Dolphin as a dedicated file manager is a good idea. However, with Konqueror as it is now, one can easily configure different interface appearances for different uses and save them as separate view profiles. One then just opens the view profile suitable for the purpose to which one is going to put Konqueror."
    author: "tom"
  - subject: "Some suggestions"
    date: 2007-03-02
    body: "I'm continueing to play with dolphin and since I can't find any official bug tracker I'm going to post a some suggestions.\n\n1) Bookmarks, there are a few things that could be better with dolphins implimentation of bookmarks:\n\nFirstly you can only access bookmarks from bredcrumb mode, you should have the ability to use bookmarks from the editable url bar. In this mode useing a bookmark should copy the full url of the bookmark to the address bar.\n\nSecondly in bredcrumb mode bookmarks hide the file tree below them, if you browse into /home/me the bredcrumbs will say \"home\" and nothing but \"home\" this is annoying if you want to go back up to another part of the filesystem, and IMHO it will slow down a new user from fully learning his way around the Linux filesystem. I think it would be better if you show the full path from / instead\n\nthridly the folder / is called root. That dosn't feel right to me, and again it may slow a user down in learning his way around the filesystem\n\nFinally there is no way, that I found, of makeing a new bookmark without going into configure dolphin. A simple \"bookmark this folder\" should be found under tools, as well as hotkey and optional toolbar button for the same.\n\n2) file/folder names sometimes cut to a new line in the middle of a word, this is horrid (sorry to be so blunt but it really is), perhaps you could copy konquorers code for this, konquoror dose get it right\n\n3) I think the breadcrumbs could be implimented a little better, \n\nfirstly you have folder > folder, wouldn't it make more sense, and fit in better with Liunx to have folder / folder?\n\nand the current system of clicking on a folder then seeing everything in that folder in a drop down menu is nice, but I think if you click a folder it should cut to that folder, and if you click the arrow (or the slash) then you get the drop down menu.\n\n4) Just a small suggestion, there is two seprate hotkeys for switching to bredcrumbs/editable url. There should be one hotkey that switches to whatever one you're not useing.\n\n\nNote: If you replace the arrows with slashes, and rename root to /, then you'll end up with two slashes in a row, a solution could be to have no name at all for root and then let the following / stand in for the name. "
    author: "Ben"
  - subject: "Re: Some suggestions"
    date: 2007-03-02
    body: "First thanks for your constructive input!\n\n> 1) Bookmarks, there are a few things that could be better\n> with dolphins implimentation of bookmarks:\n\nThe bookmarks handling in Dolphin will be completely reworked for KDE 4.\n\n> Secondly in bredcrumb mode bookmarks hide the file tree below them\n\nWe plan to make this configurable, as there are a lot of pro and cons on both sides, whether a hiding of the physical file hierarchy should be done or not.\n\n> thridly the folder / is called root. That dosn't feel right\n> to me, and again it may slow a user down in learning his way\n> around the filesystem\n\nWe'll discuss this.\n\n> Finally there is no way, that I found, of makeing a new\n> bookmark without going into configure dolphin.\n\nCurrently it's possible by using the context menu, but I think we'll introduce a \"Bookmark\" menu to stay consistent with other applications using bookmarks.\n\n> 2) file/folder names sometimes cut to a new line in the middle of a word,\n> this is horrid (sorry to be so blunt but it really is)\n\nYou're right, it is horrid :-) Will be solved for sure in KDE4, the code for this is shared by Dolphin and Konqi.\n\n> 3) I think the breadcrumbs could be implimented a little better,\n> firstly you have folder > folder, wouldn't it make more sense,\n> and fit in better with Liunx to have folder / folder?\n\nThe look of the breadcrumb will be improved anyway. I'm not sure whether it's better to use \"/\" instead of \">\", but let's see :-)\n\n> and the current system of clicking on a folder then seeing\n> everything in that folder in a drop down menu is nice, but\n> I think if you click a folder it should cut to that folder,\n> and if you click the arrow (or the slash) then you get the\n> drop down menu.\n\nThere had been some usability tests concerning this point and the outcome was that arrows as drop down have not been perceived very well by the users (it was not clear for the people which directory was shown: the directory before the arrow or after the arrow; also accidentally clicking on the arrows happened).\n\nWe are not sure whether the current approach is the best, but we're working on it :-)\n\n\n> 4) Just a small suggestion, there is two seprate hotkeys\n> for switching to bredcrumbs/editable url. There should be\n> one hotkey that switches to whatever one you're not useing.\n\nWe'll discuss this. I personally also would like a toggle key for this.\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Some suggestions"
    date: 2007-03-02
    body: "Thanks for replying :) again you're post is filled with good news and I look forward to seeing the results of you're discussions in KDE4-alpha.\n\nTwo points I'd like to respond to directly\n\n>> 2) file/folder names sometimes cut to a new line in the middle of a word,\n>> this is horrid (sorry to be so blunt but it really is)\n\n>You're right, it is horrid :-) Will be solved for sure in KDE4, the code for >this is shared by Dolphin and Konqi.\n\nThank goodness :) I'm delighted to hear it.\n\n>There had been some usability tests concerning this point and the outcome was \n>that arrows as drop down have not been perceived very well by the users (it was \n>not clear for the people which directory was shown: the directory before the \n>arrow or after the arrow; also accidentally clicking on the arrows happened).\n\nHow long were users exposed to dolphin before the results were recorded? I ask because the confusion of what directory was shown should clear up with a little practice, suggesting that they didn't use dolphin for long. And IMO the real usibiltiy test is not first impressions, but how useable is it after you've had it for a while and learnt you're way around.\n\n"
    author: "Ben"
  - subject: "Re: Some suggestions"
    date: 2007-03-02
    body: "> How long were users exposed to dolphin before the results were recorded?\n> I ask because the confusion of what directory was shown should clear up\n> with a little practice, suggesting that they didn't use dolphin for long.\n> And IMO the real usibiltiy test is not first impressions, but how useable\n> is it after you've had it for a while and learnt you're way around.\n\nIn this test Dolphin was not involved at all. The test has been done at Akademy last year and the result has been posted to kfm-devel: http://lists.kde.org/?l=kfm-devel&m=116377940524940&w=2\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Some suggestions"
    date: 2007-03-03
    body: "Thanks, I'll read up on that"
    author: "Ben"
  - subject: "Re: Some suggestions"
    date: 2007-03-03
    body: "> should clear up with a little practice\n\nit's a matter of human visual cognition and limitations on fine motor skills. in other words, for most people it would take upgrades to the software in our heads."
    author: "Aaron J. Seigo"
  - subject: "Re: Some suggestions"
    date: 2007-03-03
    body: "Did you mean the accidental cliks? Because I can't see how how being confused as to weather the directory shown was before the arrow or after the arrow is related to fine motor skills.\n\n"
    author: "Ben"
  - subject: "Seamless data access"
    date: 2007-03-02
    body: "\nWe're in the 3rd millenium and it's time to think about future of computing and what is the purpose of computers.\n\nMany people seems to make confusion between \"computing science is an end itself\" and \"computer science is a mean to reach its ends\".\n\nMost people, hopefully use their computer as a way to achieve a result no matter the technology behind.\nI mean, no matter how data is stored (files, database, remotely, localy, ...), no matter what application will manage data (one multi-profile, several dedicated)...\n\nBut one thing is sure, data is converging toward multimedia in a seamless access way. trying to spearate datatype access is just adding difficulties...\n\nExample are digital camera: they host videos, photos and audio comment. How do you keep track (and link to the photo) of audio comments with a photo-only mangement software?\nHow do you see photos related to an audio comment in a audio only software?\n\nThe way to go is to render seamless the access and the management of the data just like multiview did in the past on Amiga but with new technology.\n\nA file manager like dolphin is useless when entering a maildir directory (you see useless files, you can't know what to drop, you can't move directories, ...) while a data browser like konqueror could manage the content, move sub-parts and such provided the correct profile and kio is there. (yes kio will exist in dolphin, but it is not designed for such philosophy).\n\nWhat would be great benefit and great step forward in KDE would be to have a more intuitive seamless data management like the following:\n\nYou could have .directory (at the root of specific directories) containing specific profile skinning the browser for optimised browsing according to the content of the directory it resides in. By default, when a new homedir is created, the ~/Video directory would be setup for video browsing, the ~/Mail directory would be setup for Mail browsing, the ~/Photo directory would be setup for photo viewing and so on...\nThe the data browser would have ONE UNIQUE database used for linking related content (which is impossible with separated apps).\nExample, you recieve a mail with a attached photo, the attachment is moved to the Photo library and linked to the original mail (right now, you save the attachment and keep the mail having 2 copies of the attachment in 2 locations and 2 different formats (yes some non kde apps can drop attachment, but the link is lost; fowarding suc a mail will miss the original attachment)).\n\n- Pluging a digital camera would allow to download content in apropriate directories keeping link of audio comments to associated photos.\n  Later, starting amarok (a profile of the data browser) and seeing updates in the audio directory (subsection comments), would allow to see associated photo for the comment (just like the album photo is associated with a music file).\n  Later, starting digikam (another profile of the data browser) would allow to hear comment in the photo.\n  Then how about sending an e-mail with some photos. and the magic includes (at your wish) the related audio comments).\n\n- What about representing maildirs as a file system. A mail (the leafs) would be represented as directories. Entering the directory would display the mail, but staying above could alow to drop files in the virtual folder which is the mail; adding an attachment.\n\nYou need to make room on you filesystem...\nWith the above representation and filelight view, no need to start multiple applications to make space. just remove data whatever it is (a mail subfolder that is obsolete, a file, ...)\n\nYou want to remove all records (whatever they are) that are older than a year (problem many companies have regarding the  Sarbanes-Oxley law).\nNo need to start many apps anymore (like file browser, mail browser, exif browser, archive browsers ...)\n\nJust look at the iPhone GUI. did you heard of any file browser? no, there is a media browser.\nSpeaking of file sohuld raise no more often than speaking of kernel modules. Reserved to Computer Science, not Computer users.\nFailing to see this evolution will lead to fall behin future Mac OS-X and Vista. (yes winfs and related app will raise one day)\n\nPS: Having a database based filesystem would help but is not a showstopper. \n\nThink that Microsoft (and far before, Be Inc.) thought about such data representation and the winfs project is not dropped....\n\nThink also about google web based applications that hide the data storage model too.\n\nProgrammers are not end users, but they create software they do not use to achieve a task, but to achieve a goal. Gimp vs Krita is perfect example. Gimp was desing by Skilled Graphic computer science programmers: you need to be computer science skilled in order to find how to draw a strait line. Krita was designed by Graphists that have computer science skills: that makes a big difference!\n\nIs KDE4 leading toward skilled computer science programmers leaving out computer end users?\n\nJust my 2 cents.\n"
    author: "olahaye74"
  - subject: "Re: Seamless data access"
    date: 2007-03-03
    body: "http://nepomuk-kde.semanticdesktop.org\n\nThere is no need for a \"data browser\" and a conversion of all applications into profiles/kparts."
    author: "birdy"
  - subject: "Re: Seamless data access"
    date: 2007-03-05
    body: "Seems realy cool project, but while it will (seems to) address the problem of keeping audio comment of photos downloaded from a digital camera I don't see how it could address a common database for media management.\n\nI mean:\n- digikam will manage photos\n- amarok will manage musics\n- kaffeine will manage videos and other multimedia contents\n\nThen, If I use an authoring software to create a DVD, this software will have two choices for media library:\n- Have its own library (just like all windows authoring software I know): very likely to happen, and thus some code to write and maintain, plus inconcistencies to manage.\n- Have to link with all the above software API (if possible) and manage the different concept of thoses APIs for a similare representation: unikely to happen.\n\nMaybe I didn't read enough though"
    author: "olahaye74"
  - subject: "KDE should make konqueror an OPTION"
    date: 2007-03-02
    body: "I've been using GNOME for a long time and recently formated and reinstall a just-KDE Debian Etch to tryout KDE. I'm one who uses both Windows and Linux depending on what I'm doing.\n\nNow, since I switch between two desktop, I won't be interest in spending any extra time to look at diff applications on different platforms. The result is, I uses Azureus for BT, Firefox for browsing, Thunderbird for mail client, etc.\n\nThe best thing that KDE users think about KDE is its tight integration (I think, at least). But you should also realise that there're a big group of people who prefer a more \"object oriented\" approach.\n\nI don't mind to have the library that Konqueror bases on to sit on my computer if it's meant to share code with some other programs (e.g. file manager). But I just do not need Konqueror itself since I uses Firefox and I really don't want Konqueror to be on my computer.\n\nMay be KDE should consider not having Konqueror a dependency, but a default option? That way, we who uses *other stuff* can have *one application for each task*. I know KDE4 is aiming for *one app for each task*. And I like the same thing. Doesn't hurt to give us another option huh."
    author: "Paul"
  - subject: "Re: KDE should make konqueror an OPTION"
    date: 2007-03-02
    body: "You will be able to use Konqueror in KDE 4 on Windows, I think. :)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Tree view"
    date: 2007-03-02
    body: "Will Dolphin in KDE4 include a directory tree side panel?\nI tried the last KDE3 version and the panel only has info and bookmark views, and no tree view, that I need often...\nI can't help but wonder why so many filemanagers completely ditch proper tree views..."
    author: "Gespenst mk.II-S"
  - subject: "Re: Tree view"
    date: 2007-03-02
    body: "> I can't help but wonder why so many filemanagers completely ditch proper tree views...\n\nBecause programmers are using shell completion as a daily basis and only use their development for testing..."
    author: "olahaye74"
  - subject: "Re: Tree view"
    date: 2007-03-02
    body: "I'm 100% with you on the necesity of a Tree View. I personally don't care for any other sidebar..."
    author: "AC"
  - subject: "Re: Tree view"
    date: 2007-03-03
    body: "> Will Dolphin in KDE4 include a directory tree side panel?\n\nYes, a directory tree side panel will be supported. I committed an initial version just some hours ago...\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Tree view"
    date: 2007-03-03
    body: "Awesome! I hope it works better than konqueror's!"
    author: "Tim"
  - subject: "Konqueror should split into two apps!"
    date: 2007-03-02
    body: "Konqueror is an awesome file manager. It's got the best thumbnail support, the largest number of views (file size view, tree view, etc), and support for an infinite number of panes, for instance. I would like to see a uber-breadcrumb widget address bar, though.\n\nKonqueror is a mediocre web browser. It's competing with Opera (my 1st choice) and Firefox (a close second). I'd like to see it catch up to Opera's level, however, because I'd like my browser of choice to be open source.\n\nI honestly think Konqueror should be split into a separate File Browser and Web Browser, because they are two completely separate functions. For emphasis, let me emphasize:\n ___________________________________\n|*\n|* FILE MANAGING and WEB BROWSING are two \n|* COMPLETELY SEPARATE FUNCTIONS! \n|*\n ___________________________________\n\nSplitting Konqueror into two apps would help developers refine these two functions. Having a \"web\" profile and a \"file\" profile is a hack. Rather than have two faces for the same app, let there be two apps!\n\nOn a separate note, isn't it wonderful how we have as many different proposed solutions as we have people? We can dump Dolphin, use Dolphin, use Krusader, use Konqueror, and so on. That's the great thing about open source: we have so many choices!"
    author: "kwilliam"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-03
    body: "> FILE MANAGING and WEB BROWSING are two\n> COMPLETELY SEPARATE FUNCTIONS! \n\nI always used to believe this too, but it isn't really true. A web-browser is just a file-viewer which works over http. (Or you could say that a file-viewer is a web browser for local documents!).\n\nIn both cases, we have a window, some navigation buttons, a URL-bar, and (usually) multiple tabs. No need for multiple applications, mainly duplicate code, and wasted RAM.\n\nHowever, when konqui changes context (web/file etc), this ought to change the widget layout automatically. \n\nFor example, the [home] button should change context (and probably the icon too) to distinguish /home/me  from http://myhomepage.com.\n\n\nIn my view, the \"Profiles\" thing should be split up into 2 functions:\n\na)Automatic context switching between GUI and File manager. \n\nb)Something for advanced users, who might actually want multiple home-pages, or file-management profiles.\n\nCurrently, it mainly works as (b), but is most commonly used as (a).\n"
    author: "Richard Neill"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-03
    body: ">> FILE MANAGING and WEB BROWSING are two\n >> COMPLETELY SEPARATE FUNCTIONS! \n \n >I always used to believe this too, but it isn't really true. A web-browser is >just a file-viewer which works over http. (Or you could say that a file-viewer >is a web browser for local documents!).\n\nExactly my point. I've always thought that this was the point of Konq. \nAnd I love it - if I wanted it different, I would use Firefox and Krusader (you always have to have Firefox around anyway, since there are webpages that Konq can't handle, and Krusader is a great file manager)."
    author: "G\u00f6ran J"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-03
    body: "\"A web-browser is just a file-viewer which works over http.\"\n\nFor example GMail is something like file viewing?!?\nCan you explaim me the similarities of the _use_cases_ of \"file management/browsing\" and \"browsing/commenting dot.kde.org\"?\n\nFace it. The days were web browsing was (mostly) just viewing static HTML are over. Welcome to the world of Web 2.0!"
    author: "birdy"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-03
    body: "Yes, GMail is just slightly fancier than a series of static HTML pages.  Konqueror is a universal viewer that doesn't care about protocols.  If Konqueror didn't embed other kparts then you'd have a point that embeded khtml didn't make much sense, but you can view PDFs, videos, text documents, html files (regardless of how much java script they have) over ANY kio slave, whether thats file, HTTP, sftp, nfs, smb, etc!\n\nKonqueror isn't JUST a file browser like Nautilus, Explorer, and Finder are.  It's much closer to an object viewer, regardless where the object is located (in a database, on the local system, or on an http server), or what type of object it is (text, picture, video, audio, html, etc).\n\nSo again, being able to view view all of those other formats regardless of format and protocol but NOT html over http wouldn't make any more sense than being able to only 'browse' files and view html over http."
    author: "Sutoka"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-04
    body: "Being able to to view documents regardless to the protocol is a feature of KIO-slaves. So it's not only konqueror, but any application (and kpart) that can do this. Simply try it with any open/save filedialog.\n\nIt may be nice for you, that konqueror can display most objects. For me it's more convenient when konqueror delegates the viewing to other applications. Having konqueror always morphing parts of it's interface is simply disturbing me..."
    author: "birdy"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-03
    body: "and file viewing since when is file-viewing file-managing? dolphin can't view files. thats the point. most websites are like documents, not like a filesystem. a tabbar, navigation buttons and a url input are nothing major that needs to be duplicated. in fact because of different meanings of these elements many need different implementations for any mode anyway. \n\nyou end up with many functions only usefull in one mode. you don't rename websites. you don't move them. you can't view websites like a tree. you don't create new websites in a browser. you don't open thins in a webbrowser and you don't have multiple actions for a website.\n\nweb browsing and filemanagment are not related at all. in the end webbrowsing isn't related to anything anymore. because of the direction the web is heading, it isn't simple document-viewing-with-hyperlinks like it used to be. many websites today are web-applications."
    author: "ac"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-03
    body: "\"you end up with many functions only usefull in one mode. you don't rename websites. you don't move them. you can't view websites like a tree. you don't create new websites in a browser. you don't open thins in a webbrowser and you don't have multiple actions for a website.\"\n\nThats mostly just a limitation of the HTTP protocol.  If the websites were over any other protocol, you could without any problems.  Web Browsing and File Viewing+Management ARE related, web browsing is simply a limited sub set of the functionality."
    author: "Sutoka"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-04
    body: "\"Web Browsing and File Viewing+Management ARE related, web browsing is simply a limited sub set of the functionality\"\n\nWhich subset?\nSorry, but I don't see the similarity. For me those are completely different use cases."
    author: "birdy"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-04
    body: "viewing and managing are two things! viewers have \"save as\" to rename the current document. managers don't, cause they don't _have_ a document. isn't that different enough?\n\n"
    author: "ac"
  - subject: "Re: Konqueror should split into two apps!"
    date: 2007-03-05
    body: "\nYou can do HTTP PUT\n\nUsing roxen web server and konqueror, I can upload pages by drag and dropping pages from my local file-system to the web server.\n\n"
    author: "olahaye74"
  - subject: "I Hate Everything About This"
    date: 2007-03-03
    body: "I'm sorry but you want to remove my precious Up button from Konqueror? Yes, I use it and I consider it a very useful feature for web-browsing.  You want to turn the default file manager into some crippled Nautilus clone?  You want to turn KDE into GNOME?\n\nPlease, don't do this.  I'm begging you.  If I wanted GNOME, I would use GNOME.\n"
    author: "KDE User"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "The button will probably be disabled for web browsing, but you can bring it back.\n\nWe don't want to clone Nautilus; we don't want to cripple anything and we don't want to turn KDE into GNOME.\n\nI find it offensive that you posted your message to the news without bothering to read any of the comments. Don't be obnoxious."
    author: "Thiago Macieira"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "I haven't read all the comments yet (only around half of them), but I'm tired of \"Dolphin is a Nautilus clone\", or \"No, don't turn KDE into GNOME!\".\n\nAs far as I understand, the most remarkable missing features in Dolphin is the web browsing part. At the moment, I prefer Konqueror, but Dolphin hasn't had the time to mature yet; I'm sure we'll lots of improvements for KDE4.\n\nAnd I don't really care about this Dolphin vs Konqueror, as long as I can use which one I like as my default file manager. And I'm not even sure it will be Konqueror in the future.\n\nThank you for all your hard work. And thank you Troy for writing about all these interesting things!  "
    author: "Lans"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "\"And I don't really care about this Dolphin vs Konqueror, as long as I can use which one I like as my default file manager.\"\n\nBut file management functions will be improved in the default file manager. If it's going to be Dolphin, it will have extra functions over Konqueror. If it's going to be Konqueror, these improvements will be included in Konqueror. The more mainstream Dolphin is going to be, the mode developers will work on Dolphin instead of Konqueror."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "I don't think that's right. Usually, the number of developers attracted are a function of the itch that needs to be scratched. Another one might be documentation, size of the team (\"This thing sorely needs someone to take care of it\"). It is also often the case that the mainstream thing scares away people (\"I'm not good enough to work on the default option\", \"I want some freedom in what I'm working on\", or even \"I don't want to adhere to the release cycle\" (the latter not being an option for something that's included by default of course)).\n\nAnd then, most of the code will be shared, of course. Furthermore, the final decision of what is default will be made by the distro people or system integrators.\n"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "\"And then, most of the code will be shared, of course.\"\nI know. But if everyfeature of Dolphin becomes realized in Konqueror, there is no need for Dolphin as its feature set becomes a subset (a proper subset) of Konqueror's. If not, that means several things that could be included in Konqueror are included in Dolphin only - as all the current extra features of Dolphin could be implemented in Konqueror.\n\nThere are very nice mockups for Konqueror 4 on http://kde-look.org, e.g. http://www.kde-look.org/content/show.php?content=39993. Will these (basiaclly file management related) improvements (basiaclly file management related) written and included in Konqueror as I (and probably many others) would want to continue using of Konqueror for file management because of its range of functionality, e.g. kparts integration?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-04
    body: "you don't understand.\n\ndolphins purpose IS to be a subset of konqueror. dolphin doesn't show files or websites. so even if the filesystem managing functions are completely the same, there is still a need for dolphin for many users. same functions doesn't mean same look/usability. removing all those viewer functions from the managing functions alone makes dolphine way easier to use..."
    author: "ac"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-04
    body: "If Dolphin truly was a subset, then nobody would use it. Its purpose is to be better in file management, isn't it?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-04
    body: "what about reading my post? the problem of konqueror are not its file managing functions. the problem with konqeror as a filemanager are all the other functions. if you remove them you allready got a way more useable application. now put some more filesystem oriented ui ontop and ou got dolphin...\n\n\nmost of the filemanaging stuff is inside the kio-stuff and the filemanager kpart. as i see it, the plan is that konqeror and dolphin are using the same code for this in kde4, so they will have mostly the same filemanager features."
    author: "ac"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-05
    body: ">>Will these (basiaclly file management related) improvements (basiaclly file management related) written and included in Konqueror as I (and probably many others) would want to continue using of Konqueror for file management because of its range of functionality, e.g. kparts integration?\n\n\nI don't think konqueror is wel suited for the looks in those mockups.\nThat is one of the reasons why kde wants to use a different filemanager.."
    author: "whatever noticed"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "I find it offensive that the Up button will be disabled for web browsing making me do *work* to get my KDE experience back.  I find it offensive that you remove the location bar from the the file manager, making me do *work* to get my KDE experience back.  Why not switch to firefox while I am at it.  I can bring the Up button back there too.  Why not switch to Nautilus while I am at it.  I can bring the location bar back there too."
    author: "Anonymous"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "Indeed, why not?  Heaven forbid you might have to do *work* or sprain your wrist moving your mouse.  God, to think you might even hurt your poor little finger clicking on that monstrously hard mouse button.  So much damn WORK!"
    author: "anon"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "Wow, KDE is all about configurability and you're whining that the default is going to be SLIGHTLY different from your ideal situation?  I find it offensive that you apparently assume that if the default isn't how YOU like it, its a personal attack on you."
    author: "Sutoka"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-04
    body: "use F11(fullscreen mode) and alt+Up"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "In what way do you see Dolphin being exactly the same as Nautlius?\nIn what ways is Nautlius so baldy crippled?"
    author: "birdy"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-05
    body: ">>In what way do you see Dolphin being exactly the same as Nautlius?\n\nthe first screenshot of dolphin liks a bit like nautilus.\nAnd you know how those critics are: they just look at one screenshot and start to screem!!\n\n\n>> In what ways is Nautlius so baldy crippled?\n\nThat's a retorical question?\n"
    author: "whatever noticed"
  - subject: "Re: I Hate Everything About This"
    date: 2007-03-03
    body: "I am with you bro!\n\nI smell some external influence here... maybe its Mike... I mena shulttelworth ..."
    author: "kamel"
  - subject: "Dolphin will rock!"
    date: 2007-03-03
    body: "After reading through all those 300 comments to this dot story, two things become evident:\n\n- There's lots of armchair developers around who do not understand nor seem to want to understand the difference between simplification and optimization for a certain task\n- Dolphin will rock your socks of like previous KDE applications did. It's not like we replaced KDE developers with monkeys that aren't capable of creating highly flexible and configurable applications that are a pleasure to work with.\n\nKeep rocking! :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Dolphin will rock!"
    date: 2007-03-03
    body: "Thanks for insulting your users/developers.  That surely clears everything up."
    author: "Anonymous"
  - subject: "Re: Dolphin will rock!"
    date: 2007-03-05
    body: "I am not insulting *actual* developers, only those that know better without really bothering trying to understand why this change will be made.\n\nSure, it's scary if people seem to move away from an application you really seem to like, but that should be all the more a reason to digg into it and set aside the fear.\n\nThe question I'm posing is why the very same people that created excellent applications in the past should now suddenly start to design crippled applications that no one wants to use. The filemanager is important enough that the community *will* take care of it in a way that it'll become/stay a really great applications, and one of the cornerstone of a great KDE 4 series.\n\nWe gotta trust this community."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Dolphin will rock!"
    date: 2007-03-04
    body: "Nice one coolo.  Anyone that doesn't like the decision \"doesn't understand it\"."
    author: "John Tapsell"
  - subject: "Re: Dolphin will rock!"
    date: 2007-03-05
    body: "Yep, that wasn't too hard to understand, was it?\n:o)"
    author: "whatever noticed"
  - subject: "Re: Dolphin will rock!"
    date: 2007-03-05
    body: "Uhm, look at how often simplification and optimisation for a specific task are confused in the comments, then you see what I mean.\n\nOw, and I happen to not be coolo :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Great decision"
    date: 2007-03-03
    body: "I've also tried to read most of the comments but I got too fed up with the same arguments appearing again and again.\n\nI'd still like to add my two cents though:\nI think that this has been a great decision. I've tried KDE4 and Dolphin a while ago and really liked it. Also I think that just because Konqueror has done and will continue doing a great job doesn't mean that there isn't a way of improving the user-experience when file-browsing. Another great thing is that Konqueror and Dolphin (as far as I understand it) are going to share a lot of code.\n\nPeter Penz and all the other developers working on Dolphin: Keep up the great work!"
    author: "mata_svada"
  - subject: "Up Button"
    date: 2007-03-03
    body: "I hope the Up button will be available while browsing on web pages. I use it a lot. E.g. It is faster for me to press Alt+Up two times, than going to the Location bar to remove \"1172721427/addPostingForm\" or klicking somewhere. This is really a important feature and also correct behavior as web pages not only have a forward/back dimension.\n\nOf course it would be nice, if you could have a webpage dimension description in html, so the browser knows what up means in e.g. a dynamic page (something for 2040 I guess ;))\n\nAlso nice would be if up doesn't stop on dot.kde.org but wents to kde.org (or www.kde.org if kde.org has no HTTP server listening).\n\nSo why removing something usefull which is _not_ in IE and Moz ?\n\n\nGreetings\nFelix"
    author: "Felix"
  - subject: "Re: Up Button"
    date: 2007-03-03
    body: "\"Of course it would be nice, if you could have a webpage dimension description in html, so the browser knows what up means in e.g. a dynamic page\"\n\n<link rel=\"up\" href=\"upper.html\">\n\nNot standard but works in Opera."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Up Button"
    date: 2007-03-04
    body: "Wow, nice, I guess I can also have a onclick instead of a href ? But as long as it is not a standard, not very usefull."
    author: "Felix"
  - subject: "Re: Up Button"
    date: 2007-03-04
    body: "No. Opera has a Navigation toolbar on which Home, Contents, Index ... and Up buttons are aviable if the page set them in a link tag. Many of these are standard but unfortunately Up isn't. See also: http://www.w3.org/TR/html4/struct/links.html#edef-LINK"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Up Button"
    date: 2007-03-05
    body: "Konqueror has this too. Enable the extension under settings -> configure extensions and then access it under tools."
    author: "superstoned"
  - subject: "Re: Up Button"
    date: 2007-03-05
    body: "Good to know... it can also be accessed in a separate toolbar."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: ":)"
    date: 2007-03-03
    body: "not too much to say, Dolphin simply looks good and reasonable :) I hope its quality will be as good as the visual impression it makes!"
    author: "mikroos"
  - subject: "Comments"
    date: 2007-03-03
    body: "338 comments.. wait, 339 =)\nIs this the most commented story on the dot so far? [/curious]"
    author: "Anonymous"
  - subject: "history lesson"
    date: 2007-03-03
    body: "People don't want to hear this, but it's important that this be said because this is exactly what happened to GNOME.  \n\nA bunch of so-called and self-appointed usability experts took over GNOME (lead by a guy named Havoc).  These experts felt and feel that they know everything about usability and they made all the decisions.  They completely ignored all the protests and input from the common user, not only because they were experts but also because they had other motivations.  They were leaders and they lead.\n\nThe old GNOME users were not important to them, they had bigger contracts and deals they were interested in.  They even tell their users to go use KDE if they are not happy with GNOME. So the experts went ahead with their plans.  GNOME got a lot of money, contracts and enterprise suport, and yes lost the old users it didn't care about anyway but money was most important so GNOME did the right thing.\n\nThis is what is happening today.  Finally some leadership is emerging in KDE and big decisions is to be made.  This is about the enterprise.  We need to leave the old users behind to get the money.  GNOME led the way, there is no shame in following them.  If this means simplifying the File Dialog and File Manager to breadcrumbs for the enterprise, then so be it.  If this means making Konqueror more like Firefox by removing Up button, then so be it.\n\nLinux is free, if you don't like it you can reconfigure it and if you still don't like it you have the source code."
    author: "Learning From History"
  - subject: "Re: history lesson"
    date: 2007-03-03
    body: "I don't know why gnome is more popular in enterprise distros (maybe because of the less restrictive linence) why is this only about the enterprise? This approach is that I hate in gnome. Why should we follow them?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: history lesson"
    date: 2007-03-04
    body: "1 - RedHat uses gnome since the beginning\n2 - Novell bought Ximian, a Gnome company, before bying SUSE, a Linux distro with KDE and lots of other stuff\n3 - dunno any other enterprise distributions :)\n4 - RedHat ships with kde as alternative, so does novell suse enterprise. Opensuse has no default desktop.\n5 - Ximian made a deal with SUN about replacing CDE with Gnome.\nAFAIK SUN recently decided to ship KDE as alternative desktop.\n6 - although redhat and novell tend to use gnome as default, their customers don't allway agree. Take for example the Birmingham Library, that chose KDE over Gnome on SUSE, the Dutch weather service, using KDE on RedHat, the record store chain FreeRecordShop, using novell enterprise with kde, etc. etc..\n\n"
    author: "whatever noticed"
  - subject: "Re: history lesson"
    date: 2007-03-04
    body: "Xandros is designed for an enterprise desktop in mind, KDE.\nwhat about Canonical? Gnome is clearnly their default, although KDE is an option. \n\nAnd dose these companies put the same effort into KDE as gnome?"
    author: "Ben"
  - subject: "Re: history lesson"
    date: 2007-03-04
    body: "canonical only offers support for 3 years on the desktop.\nNot really an enterprise mentality...."
    author: "whatever noticed"
  - subject: "Re: history lesson"
    date: 2007-03-03
    body: "As intresting as history is, that dosn't quite fit in with the present. If you read comments posted by Dolphin devs they are planning to add more features for the KDE4 version (continueing past 4.0), the much requested tree view was just comitted in the other day for example. http://aseigo.blogspot.com/2007/03/dolphin-gets-treeview-krunner-gets.html\n\nthe plan is to have a similar level of file browseing power as konquorer, but without web-browseing. \n\nThis follows the UNIX philosophy of \"do one thing and do it well\" Weather you agree with this is a diffrent matter, but the devs have promised konqoror is going to be their for those who like it. I see no reason not to trust the devs when thay say dolphin won't have a detremental effect on Konqoror."
    author: "Ben"
  - subject: "Re: history lesson"
    date: 2008-03-16
    body: "  If they can make it a useable tool then I'll use it. I'll look at the Dolphin bundled in KDE 4.0 and evaluate it... again. \n  The Dolphin I've seen so far should not have been released as it is clearly not ready. Because it is not ready there is alot of negativity around it.. and so there is alot of resistance, not to mention bad feelings towards those who foisted this incomplete poorly functioning tool onto the KDE users at large.\n  Just thinking of having to use Dolphin as it is now makes my hair hurt. Thinking of having to uninstall and reset the default file manager to Konqueror for each KDE using distro is enough to make me select UBUNTU and not KUBUNTU.\n  Now having said all that, if... (and thats a big 'if') Dolphin becomes a useable tool hen all of those dreads in the above paragraph will instantly vanish... but from where I stand now it sure as heck looks like someone is  trying to make a silk purse out of a sows ear."
    author: "Steve"
  - subject: "Re: history lesson"
    date: 2007-03-03
    body: "me again, this time I'd just like to ask where can I read about the Havoc period of Gnome's history?"
    author: "Ben"
  - subject: "Re: history lesson"
    date: 2007-03-04
    body: "http://uncyclopedia.org/wiki/GNOME"
    author: "whatever noticed"
  - subject: "Re: history lesson"
    date: 2007-03-04
    body: "hehe uncyclopedia rocks, but it was a serious question. "
    author: "Ben"
  - subject: "Re: history lesson"
    date: 2007-03-04
    body: "It's not all-100% Havoc, but check out the archives around those times, lots of good discussions, and with 5 years hindsight, we see that those good intentions led to the GNOME which is quite inadequate for my uses:\n\nhttp://www.arcknowledge.com/gmane.comp.gnome.usability/2002-05/msg00148.html\n\n\nI do fear the excessive simplification of KDE4, but I also have great confidence in the KDE devs, so I'll just wait and see.\n\nHope you'll find these archives interesting.\nKig"
    author: "Kigrwik"
  - subject: "Re: history lesson"
    date: 2007-03-04
    body: "Thanks for the link\n\n>I do fear the excessive simplification of KDE4, but I also have great confidence \n>in the KDE devs, so I'll just wait and see.\n\nI've got zero problem with simplificaiton, on the other hand, I can't stand the delusion that you can't simplify without removeing features.\n\nThat said I havn't seen any evidence to say the KDE devs are following Gnome's mad philosophy."
    author: "Ben"
  - subject: "Re: history lesson"
    date: 2007-03-05
    body: "I think that simplification in itself is a good thing. I wouldn't want to make anything less simple than what's reasonable. (The computer is a tool, after all.) The point is not to dumb down the interface, and not to limit the user.\n\nAt the same time, lots of people seem to fear oversimplification and that is being confused with specialisation. Dolphin is all about specialisation, so this tool can be optimised for a certain task, and that was one of the things that proved to incredibly hard with konqueror in the 3.x series.\n\nKonqueror is much more a shell you can run anything in, a bit like the whole desktop that is the shell for all applications. I think stating that having dolphin as a filemanager will draw away resources from the shell konqueror is a bit of a vague assumption, because they're very different tools, and the most important parts of konqueror-the-shell are kio, kparts  and the filemanager part in particular. None of those are likely to get less attention in the future, simply because they're core KDE technologies and used all over the place."
    author: "Sebastian K\u00fcgler"
  - subject: "As  long as"
    date: 2007-03-04
    body: "As long as konqueror doesent get lower prioity, or phased out im happy. But i really really like konqueror as it is, and thus wouldnt want it to go"
    author: "redeeman"
  - subject: "Embedding the viewers"
    date: 2007-03-04
    body: "For me, it is THE feature of Konqueror (kio-slaves make close second). This is what makes KDE so usable for me. I really, really like the ability to, say,  browse the Internet looking for some scientific articles and then open the pdf-files in separate tabs in the same application, together with some ps-files and web pages. As users are interested in data, not in formats, it's so easier to have all the needed information of the same type in one place, and not separating it because of different formats. \nBack when I was a Windows user and used IE (ah, sins of the past) I was thrilled when I saw the tabs in Opera. (That's actually how I began to look for the \"alternative\" software.) It really made sense to me, and the concept of Konqueror even more so. \nSure, it doesn't always work perfect, but the prospect of having to open 10 instances of kpdf/okular, 5 instances of kghostview and what else not while gathering some information on the net isn't very compelling. Talk about bloat of the desktop ...\nThat obviously makes me non-user of Dolphin. And of course, I will still use my beloved Konqi. \nBut I think you are throwing out a great concept, which really made the qualitative difference, and in the end you're making harder to switch to KDE, since you're offering one great idea less.\nAnd if my opinion isn't enough to change the minds of KDE developers, let me add that my wife thinks the same. If you knew my wife you would change your mind immediately :) \n"
    author: "semi"
  - subject: "360!!"
    date: 2007-03-04
    body: "Wow, 360 comments in just 4 days time!!\n\nDid we break a record with this amount?\n\n"
    author: "whatever noticed"
  - subject: "my two cents"
    date: 2007-03-04
    body: "I've never really bothered managing my files. personally, I think what I want is more of a file *viewer*. I love being able to open anything in konq, but when I've got a dozen pdf's from school and I'm searching for the one that has the info I need, I get tired of hitting the back button. it's not as bad as having a bajillion windows open, but it's still annoying. I think what I need is some way to have the folder contents shown on one side, and the contents of the selected file on the other side. this would make flipping through pdfs much easier. I've no idea where I would find such behaviour, though... there's an image view thingy for konq that does this, but it's only for images; I want to be able to view any file type!\n\n...now that I think about it, it can't be too hard to extend that image view to show anything... if only I had the time..."
    author: "Chani"
  - subject: "Re: my two cents"
    date: 2007-03-04
    body: "In konqueror you can \n- split the view left/right, \n- link the two views by clicking in the right empty square in the status bar\n- lock the left view to its current location using the status bar context menu\nWhen you click on a pdf file (or any kind of file) in the left view it opens in the right view.\n\nI use this all the time and find it very convenient...\n\n"
    author: "Aris"
  - subject: "Re: my two cents"
    date: 2007-03-05
    body: "interesting.. I never understood what \"link view\" meant.\nI think this is exactly what I wanted. thanks! :)"
    author: "Chani"
  - subject: "Firefox"
    date: 2007-03-04
    body: "What really annoys me is the bad intergration of Firefox into KDE. I don't understand why in the free software world interaction and interoperability do not perform properly:\na) why don't files open as KPDF\nb) the ugly download context window, it's okay for most of us\nc) the open dialogue.\n\nI want to use Firefox but under KDE it is a real usability pain. Konqueror could be an alternative. the point is I prefer to use firefox and I don't see why the mozilla guy make KDE the worst firefox plattform."
    author: "ben"
  - subject: "Re: Firefox"
    date: 2007-03-04
    body: "See also\n\nhttps://blueprints.launchpad.net/ubuntu/+spec/firefox-kde-support"
    author: "ben"
  - subject: "Re: Firefox"
    date: 2007-11-12
    body: "It is one of the things I actually hate about KDE.\n\nThe Unix Way is a well crafted tool for a single purpose. Not a half-assed bundle of crap to address 400 functions. In the attempt to make an uber-combo of applications that address email, contacts, browsing, file management, etc - other stand-alone apps get shafted.\n\nDolphin may not be as full featured as some of the other managers and may have a number of things that should be added to it still (trees and tabs, for example) - but the fact that they have finally acknowledged that sometimes having one well built tool for a specific purpose is enough of a reason to encourage me with regard to the future of KDE."
    author: "no"
  - subject: "Re: Firefox"
    date: 2007-11-12
    body: "Actually, you couldn't be more wrong. Konqueror is your shell, comparable to bash. The various parts for viewing documents or managing files or browsing the webs are small, lean components that in many cases can also be used stand-alone."
    author: "Boudewijn Rempt"
  - subject: "Re: Firefox"
    date: 2007-11-12
    body: "Exactly.  Fun fact: The codebase of Konqueror is only approximately 40k lines of code.  In comparison, Nautilus is about 95k, Epiphany also about 95k, and \"lightweight\" Thunar appears to be (rather oddly - maybe sloccount is acting up?) about 77k."
    author: "Anon"
  - subject: "Re: Firefox"
    date: 2007-11-13
    body: "Your Unix Way is less complete than My Unix Way.\n\nMy Unix Way does have the same well crafted tools that yours has. But on top, My Unix Way is able to combine those tools in new creative ways.\n\nIn a shell I can use pipes, re-directions, loops, functions that include the different of those tools.\n\nKonqueror helps me to use KParts, KIOslaves, DCOP, bookmarks and more KDE building blocks to achieve new functionality. In the FileOpen and SaveAs dialogs I can do the same.\n\nYou don't see the advantages for you? Your bad. Keep doing like you do, but let me keep doing like I do, please."
    author: "Kurt Pfeifle"
  - subject: "Branch KDE !"
    date: 2007-03-04
    body: "hehe :> i am certainly joking, but if you dont add your userbase to such desicions you will get it. Like xfree86. I know he who codes decides, i dont think thats the best thing for kde.\n\nWe need Guidance and clear and democratic descicions."
    author: "funnyfanny"
  - subject: "Re: Branch KDE !"
    date: 2007-03-05
    body: ">>democratic descicions.\n\nYup, and in a democracy, only the official citizens of a country decide.\nSo in the case of KDE, i guess that would be the active members of the project.\nAnd that would be the developers..\n\nSo the question is, did a majority of the developers decide to put dolphin in kdebase?\nI guess they did...\n"
    author: "whatever noticed"
  - subject: "Re: Branch KDE !"
    date: 2007-03-05
    body: "I don't agree.\n\nDeveloppers could compare to people that are elected.\nUsers could compare to people that vote.\n\nPeople not using KDE could compare to \"non official\" citizens.\n\nNobody voted, is it a dictature?\n"
    author: "olahaye74"
  - subject: "Votes?"
    date: 2007-03-04
    body: "I think it would be fine to let the user base decide on the default settings. Same goes for the other programs that do the same thing (ex: Okular/Ligature). Personally, it would be nice to fix Konqueror's problem within Konqueror, not by inventing a new manager."
    author: "Youssef"
  - subject: "Re: Votes?"
    date: 2007-03-05
    body: "so we need to prerelease kde4 with all options open, like a RC-something, then let the user base use all applications for a while, lets say 6 months, and after that everyone gets to vote wich application stays in and wich application gets out..\n\nHmm, that would delay the release of kde to summer 2008 i guess...\n\n"
    author: "whatever noticed"
  - subject: "Poll"
    date: 2007-03-04
    body: "Poll on kde-forum:\nhttp://www.kde-forum.org/thread.php?threadid=16354"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Poll"
    date: 2007-03-04
    body: "This requires registering. A more accessible poll would be better."
    author: "Youssef"
  - subject: "Re: Poll"
    date: 2007-03-05
    body: "in most democracys, to vote means to register..\n"
    author: "whatever noticed"
  - subject: "How many people did actually try Dolphin out?"
    date: 2007-03-04
    body: "The question comes to mind, after seeing all the outrage. I played a bit with the KDE 3 version and it looks like a nice file manager. Before saying things like \"Don't take Konqueror away\" or the like, you should at least wait until KDE 4 is released to see if you like/dislike Dolphin as default, IMO. In this case a stand \"by principle\" is not the best course of action.\n\nAs Konqueror is not going away anyway, I see it as a non-issue, really. Even if Dolphin will be the default, it will ultimately be up to distros to make the choice of default file manager.\n\nAgain, I think people are overreacting.\n\nDisclaimer: I use neither Dolphin nor Konqueror for my daily work, and it's not because I don't like them, but because I'm a command line person."
    author: "Luca Beltrame"
  - subject: "Re: How many people did actually try Dolphin out?"
    date: 2007-03-04
    body: "Just to add something, don't judge dolphin based on the KDE3 version, loads of features are planed for KDE4 and you won't do it justice if you judge now."
    author: "Ben"
  - subject: "Always the same"
    date: 2007-03-04
    body: "its not just dolphin, i always hate it when problems are not fixed where they arise but a new project is created because someone does not want/or can read the orginal code.\n\nto the developer from doplhin: Please keep up the work, but i think you know yourself, working on the core element of kde is a core discussion :> so just prove everybody wrong :)"
    author: "chri"
  - subject: "No thank you Dolphin"
    date: 2007-03-05
    body: "Developers, please get a clue.\nWe, the users of KDE, didn't ask for this infantile abortion to be pushed on us.\nTake the hint before we are forced to walk with our feet and take our goodwill with us."
    author: "Mr. Angry"
  - subject: "Re: No thank you Dolphin"
    date: 2007-03-05
    body: "Wow, you make a show of constructive criticism. </sarcasm>"
    author: "Luca Beltrame"
  - subject: "Re: No thank you Dolphin"
    date: 2007-03-05
    body: "It's kinda funny:\n\nkicker gets removed: everyone cheared..\nkdesktop gets replaced: everyone cheared...\nDCOP leaves KDE: everyone cheared...\nOther componements removed from KDE: ditto....\nKonqueror no longer default (but stays in kde): Everyone gets angry!!\n\nI guess everyone has fallen in love with this application.\nIf we look at the past, even small changes to Konqueror (like cleaning up the interface) have lead to large discussions on the dot.\nSo for most KDE users, this change is Huge, even more huge than the removal of kdesktop/kicker/dcop..."
    author: "whatever noticed"
  - subject: "Re: No thank you Dolphin"
    date: 2007-10-22
    body: "Well, I use konqueror as my file manager all day every day. Yes, it's that important to some of us."
    author: "Ian MacGregor"
  - subject: "Re: No thank you Dolphin"
    date: 2007-11-28
    body: ">Developers, please get a clue.\nWe, the users of KDE, didn't ask for this infantile abortion to be pushed on us.\nTake the hint before we are forced to walk with our feet and take our goodwill with us.\n\nSOURCE CODE! Don't forget to take the SOURCE CODE! "
    author: "blackbelt_jones"
  - subject: "Settings menu"
    date: 2007-03-05
    body: "Can somebody tell me why all KDE programs has to have a Settings menu? I'm not an expert, but from my point of view most people change the setting once or twice until they find their preferred settings. After this is done you leave them alone, and then the Settings menu just clutters the UI.\n\nIn a \"simple\" program like Dolphin I guess most people would be happy with a \"settings\" menu item under Edit or something.\n\nIs there a setting for removing the settings menu?"
    author: "Lars"
  - subject: "Re: Settings menu"
    date: 2007-03-05
    body: "I think that the Configuration menu is a good thing for the following reason:\n\n- All application have their settings in the same place: you don't need to search if setup is done in the File menu, in the Edit menu, in the tools menu or in the Help menu. Also you don't need to check if settings are called options, setup settings or whatever as if any setting exists, its within the Configuration menu.\n\n- Same goes for the Help menu\n\nNote that the Edit menu is bad IMHO as many applications thare are only viewers don't have an Edit menu (pdv viewers, image viewers, ...)\n\nYou must be a Firefox user under Linux. If you look at Firefox under Windows, you'll notice that the preferences are under the Tools menu. Thus its even worse as it's inconsistent between the 2 OSes...\n"
    author: "olahaye74"
  - subject: "dolphin embedded in konqueror"
    date: 2007-03-05
    body: "I followed the entire discussion here and what I don't understand is: Why can we not embed dolphin into konqueror, just like how kpdf can be an embedded viewer? That way konqueror becomes truly a base from which it calls up the parts that it needs. Or is this a totally crap idea?"
    author: "KDE user"
  - subject: "Re: dolphin embedded in konqueror"
    date: 2007-03-05
    body: "Best solution imho.\nAnd let's do the same with the web-part of konqueror."
    author: "birdy"
  - subject: "Re: dolphin embedded in konqueror"
    date: 2007-03-06
    body: "There will be only one filemanagement part, which will be the same that is used for Dolphin. So embed it into konqueror, you get the konqueror filemanager, embed it into the dolphin frame, you get dolphin."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: dolphin embedded in konqueror"
    date: 2007-11-12
    body: "We don't need more crap jammed into konq. I dont' use konq for my web browser. I don't use it for my file management. I don't use it for viewing anything. I just pretend it doesn't exist.\n\nI mean, why not embed our email app into konq? And a bit torrent client? And krdc? And an IDE for developers?"
    author: "no"
  - subject: "Dolphin or Golphin?"
    date: 2007-03-05
    body: "After reading this endless thread I'm pretty sure that the name of the new best of the best 'gnumerish' file manager is just misspelled! \nIt must be called 'Golphin'!"
    author: "A_KDE_User"
  - subject: "Is it that bad?"
    date: 2007-03-06
    body: "Guys, why the big fuss?\nIs dolphin THAT bad? It's basicly a konqueror tab, just in a new window.\nI think we should be more thankfull of the KDE devs."
    author: "T1m"
  - subject: "Is it that bad?"
    date: 2007-03-06
    body: "Guys, why the big fuss?\nIs dolphin THAT bad? It's basicly a konqueror tab, just in a new window.\nI think we should be more thankfull of the KDE devs, and if you want to use Konq for file browsing, do so."
    author: "T1m"
  - subject: "KDE developers need to listen end users "
    date: 2007-03-08
    body: "I am a fan of Konqueror and I am depressed to see that KDE developers instead of focussing their efforts in improving what is already working and proven software, they are trying to re-invent the wheel. I don't want any foreign Dolphin stuff in my machine. I don't need it! I didn't ask for it! Maybe they live in a parallel world from which we common users cannot have access. I believe the problem is in lack of interaction-feedback between developers and end users. I appreciate developers efforts a lot, but I prefer them not to waste their time in duplicating or complicating further KDE. For instance, what we really need is a fast responsive system that works in say, 5-7 years old hardware. Or are they again copying the Vista model, which can run only with newest fastest hardware? C'mon guys, Improve what is already there. Do not re-invent the wheel. One example is: the KcD player. It is soooo slow to start working upon inserting a music cd, that you can hardly believe it is part of KDE. This kind of situation is embarrassing since Windows XP is so responsive. I believe that the root of the problem lies in the lack of a strong LEADER in KDE. Yeah, KDE lacks of a Leader with the caliber and experience of  Linus Torvalds. Too bad. Hope that Gnome will learn from KDE mistakes..."
    author: "Maxei"
  - subject: "Re: KDE developers need to listen end users "
    date: 2007-03-08
    body: "Not only a troll, but a silly troll as well, aren't you :)"
    author: "Jonh K."
  - subject: "Re: KDE developers need to listen end users "
    date: 2007-03-23
    body: "Oh, come on... He does have a point. That Dolphin screenshot only made me think about Vista's new Explorer. By the way - I don't like either. There should've been a \"List view\" option in Windows Explorer but Microsoft thought the users never used it or something. Now we see the best (probably) desktop environment going down Microsoft's road. And not only trying to have an application that's more like its latest competitor but the worst that it happens is copying the concept and pretty much of its layout. How can that be original. Reading of Dolphin got me excited thinking about a KDE application that will simply rock local file management. Seeing the screenshot only got me in this position, complaining about high resemblance with M$ software and backing up a guy someone in here named him a troll. Why copy?? KDE is quite good as it is. Making it faster by use of QT4 and more development might be every user's dream. But copying sucks. Sorry. No thumbs up for Dolphin so far."
    author: "Nick"
  - subject: "Konqueror"
    date: 2007-03-08
    body: "KDE does not have enough man power to fix all the Konqueror bugs, so starting a whole new project that does nothing but duplicate a little bit of existing functionality is a total waste.\n\nFix Konqueror bugs, make it more flexible, take care of all the profile management mess once and for all by separating web browsing from file management, change all the eye candy you want, let the user choose whether to have the Up button, etc. etc.\n\nThere is plenty of work to do. How about fixing file selection using keyboard for example? It is obvious no Konqueror developer ever tried to select files in it using keyboard. Konqueror is the only file manager in existence that has totally broken and useless keyboard selection and navigation. Do we have man power to fix that? It seems we don't. (And I beg you please *please* don't come with the usual crap like \"have you opened a bug?\". Just try to use it and you'll see how badly it's broken and has been forever.)\n\nSo if we have people who don't care to improve an existing program that is already the best of all but needs work to make it better, that's fine, let them work on a new project or do whatever else they want or just leave, KDE doesn't need you. KDE needs people who can finish a good work that was started ten years ago, that is what we all need."
    author: "ipspe&#322;"
  - subject: "Konq"
    date: 2007-03-08
    body: "Apt quote at the bottom of the post:\n\n\"No fake - I'm a big fan of konqueror, and I use it for everything.\" -- Linus Torvalds\n\nDolphin will provide a much simpler interface that is comparable to Nautilus in GNOME. This seems to have some KDE users worried that development on Konqueror's file management will cease. Linux is about options and this will just give the average user what many believe to be a less intimidating experience. Meanwhile power users will still have all the flexibility of Konqueror at their disposal."
    author: "Jonathan Zeppettini"
  - subject: "Re: Konq"
    date: 2008-03-16
    body: "\"Linux is about options and this will just give the average user what many believe to be a less intimidating experience\"\n\n I've installed Kubuntu 7.04 on many PCs that used to be Windows machines. Not one of those users found Konqueror an \"intimidating experience\".\n\n Konqueror is one of those apps that is deceptively simple on the surface. As you use Konqueror you discover it's an amazingly powerful tool. Dolphin is the exact opposite, is looks like it might be a powerful tool, but my experience with it is that it is all smoke and mirrors.\n\n"
    author: "Steve"
  - subject: "Another one against this"
    date: 2007-03-13
    body: "I am a huge fan of KDE and of Konqueror in particular, and I am, like many in this thread, disappointed that things are being taken in this direction.  Konqueror is a truly new paradigm for how to use the desktop.  It finally realizes well the longstanding goal of mixing the web with the local machine. I really love having tabs that mix directories, files, and web pages.  If it's split apart into separate apps, as is proposed here, it will be a step backwards.  (And, like others, I find the up botton isn't a problem when browsing, and it's sometimes an advantage.)\n\nFrankly, as a web browser, Konqueror is still not up to the level of Firefox.  It is slower and doesn't render as well. Konqueror is still unstable (it crashes pretty often when I right click on things, and it gives the \"Can't connect DCOP\" popup most of the time when it doesn't crash).  But I use it anyway because it's really a lot better way to work.  If the developers think of it simply as a browser, they'll stop improving the other parts, and split their efforts.\n\nKonqueror is one of the greatest applications of the last 10 years; I hope it stays at the center of KDE.\n"
    author: "Mitch Golden"
  - subject: "Re: Another one against this"
    date: 2007-04-12
    body: "I agree with Mitch. I don't see a need for yet another file manager when the effort would be better spent to improve the stability of Konqueror, my only gripe. Konqueror is I believe uniquely the most powerful \"browse and manage everything\" tool, yet doesn't even show up in most browser statistics. I feel the effort spent in promoting Dolphin would be better spent in promoting Konqueror, which seems heavily under-publicised considering its merits.\n\nSee the attachment for a screenshot with a website, local home, local folder, and a remote folder in one tab (and 4 websites and a server folder in further tabs). I suspect Konqueror is the only application which can do this."
    author: "Theo Schmidt"
  - subject: "Leave it alone!"
    date: 2007-06-29
    body: "Dolphin is too much like Gnome. I don't use Gnome because of its lack of options, and this really makes me sad. What is wrong with embedded PDF and HTML viewing? I think  this is a really bad idea, and Konqueror should be left alone. I tried Dolphin and it irritates the heck out of me. How is this supposed to be more efficient when you have to open an external viewer just to view a file? It slows things down in my opinion. What about the split view terminal mode. I use that all the time, and I don't see it in Dolphin. Instead it opens an external Konsole. Linux still requires the use of a terminal quite often, and I just don't see how this is better. To me the only way you can make Dolphin usable is to give it the embedded capabilities, and advanced configuration options Konqueror has, and then you just have Konqueror. Just leave it alone.  "
    author: "siege2050"
  - subject: "Re: Leave it alone!"
    date: 2007-06-29
    body: "I really don't see what there is to complain about here. If you don't like it then nobody's forcing you to use it - Konqueror will still be there and will still be fully functional for file management in KDE 4.\n\nBy the way, you should bear in mind that many new Linux users will not use the console, nor should they be expected to even know it exists. We are already at the stage where for day to day use the console is no longer necessary - but of course it's still there for power users such as yourself (and myself). The small subset of those power users who want Konsole embedded in the file manager can still have that in Konqueror in KDE 4 - nothing is going away."
    author: "Paul Eggleton"
  - subject: "Re: Leave it alone!"
    date: 2007-06-29
    body: "\"I really don't see what there is to complain about here. If you don't like it then nobody's forcing you to use it - Konqueror will still be there and will still be fully functional for file management in KDE 4.\"\n\nPeople's IQs seem to drop sharply when discussing Konqueror and Dolphin, and they are unable to grasp simple facts like the one above for any length of time."
    author: "Anon"
  - subject: "Re: Leave it alone!"
    date: 2007-07-01
    body: "It ***IS*** there. Just choose view->panels->terminal, and you will have it. Check to see if the needed features are there before you complain!"
    author: "riddle"
  - subject: "Konqueror is the best"
    date: 2007-06-30
    body: "Why is it bad to mix file browsing, web browsing and document view browsing : this is the most powerfull functionnality. Internet is everywhere and you need it all the time. All the time my konqueror is full of web tab mixed with file management tab and I can switch easily from one to one. I can browse my dowloaded files while reading the documentation of how to use them in a splitted view.\nI can bookmark my directory (but this is a web feature this should not be good for file managment isn't it ?)\nWith konqueror there is no barrier between my desktop and the web and that's what I like !\n\nbreadcrumb feature seems very good but I want it in konqueror and there are ways to know if the control is relevent or not. This is already done with the search field which is either google while in a webpage or a filter while in a file managment page.\n\nAnd said in previous post I would prefer that efforts are done in improving Konqueror that killing it !"
    author: "Cedric"
  - subject: "Hey ! Don't Re-Invent the Wheel..."
    date: 2007-10-19
    body: "Heard of the expression. one step forward (two step back)?\n\nKonqueror, has been in Kde for years.... Dolphin just this year.\n\nAs you know: Konqueror is a web browser, file manager and file viewer,\nConsole, Kio-Slaves and much more... and Dolphin is not\n\nMy Point being, since Konqueror is HERE in the NOW, why develop a new application for a \"File Manager\" Since we are have other File Managers\nlike: \"Krusader\" or \"Midnight Commander\" and of course Konqueror.\n\nKonqueror does need improvements in Web Browsing, Java, Ajax, Meta-data,\nand several other areas... BUT, it works very good for the most part, and its integrates well into KDE... plus, it works with other apps in KIO-Slave aspect.\n\nSo You say, You got very little for time, for development... Why not put your energy into Konqueror, instead of making something new, that is REALLY Not Needed... This could save more time, in the future.\n\nI read, WHOM is going to do the work...Where the Time...etc\nif KDE.org could break down, into parts, of WHAT Wrong with Konqueror,\nand start to works on those problem areas, say a block at a time, \nThen in a short period of time, to say in KDE 4.5, Konqueror will kick butt.\n\nSO, if Dolphin comes up short, with some bugs in the future... are they going to come out with another file manager. ??? (case in point)\n\nDon't Re-Invent the Wheel, Just fix it.\n\nCheer's\n\n-R\n"
    author: "Rich"
  - subject: "Why?"
    date: 2007-10-22
    body: "I have been using kde for years and I love konqueror as a file manager. I now use Kubuntu 7.10 (Gutsy Gibbon) and it has Dolphin as the default file manager. Now, I have tried Dolphin and I think someone made a huge mistake.\n\nDolphin lacks a tabbed UI. Any file manager worth it's salt will have a tabbed UI. If it doesn't, I can't use it.\n\nDolphin lacks a tree view (with buttons) on the right. Any file manager worth it's salt will have a tree view (and hopefully buttons). If it doesn't, I can't use it.\n\nKonqueror, however, already has a tabbed UI and a tree view. So, I set the file association for inode/directory back to Konqueror and uninstalled Dolphin.\n\nI can't understand why anyone would replace a feature-rich file manager with a file manager that lacks so many features. Is the kde dev team going to continue to develop Dolphin to bring it's feature set up to the equivalent of konquerors' feature set? If so, how do you justify wasting all that time and energy when you could have kept Konqeuror as the default file manager and spent that time and energy on something more important? Did the kde team start using gnome developers?\n\nI won't use Dolphin, it's a piece of junk in my opinion, and a bad move to make it the default file manager. If konqueror disappears from kde, I'll either compile it myself or use/integrate Krusader.. but I won't use Dolphin. KDE is awesome, mainly because it isn't dumbed-down. But, this move to use Dolphin as the default file manager appears to be \"Dumb-down: Step 1\"\n\nOh, wait.. I have a good idea. If you want an excellent file browser that is feature-rich and integrates very well with KDE, why not use Konqueror as the default file browser? ;)"
    author: "Ian MacGregor"
  - subject: "old thread but still important..."
    date: 2007-11-19
    body: "here is what i would rather then create a fully new file manager:\n\n1. make the profiles linked to protocol type. that is, make it so that when you have a profile set to web browsing, it will show up if i enter a url that starts with http://. similar for ftp, smb and all the others.\n\n2. give the profiles a option to store its last state on exist. kinda like how firefox can save its tabs when you close it, and even recover them on a crash.\n\n3. create a kpart or whatever that can show data about the file in a konqueror region, and turn the photobook one into a preview of the highlighted file (if any). maybe the data kpart can allow for editing of metadata for when one encounter a media file of some sort.\n\n4. modify the location bar into one that can show a breadcrumb style path, if the user wants it to. but still allow him to type in one by adding a button to one end of the bar that turn it into the classical location bar. same should happen if the location hotkey is entered (F6, or ctrl+l on some browsers).\n\n5. remove the home page part of the configuration window. instead, make it simple to set a home page pr profile (without the need to save the whole profile over again). hell, lets make the profiles fully editable without having to do the save thing (if one wants to adjust one setting without having to worry about at the same time overwriting some other that one wants to keep). and the home page button should bring one to the home page of the currently used profile (if it have one). so if im browsing the web (http) then i should be brought to the home page of the web browsing profile (those home page changing javascript codes and similar should alter the home page of the current in use profile btw). same with be doing some local file management, then i should be brought to my user directory.\n\nall this and you basically have dolphin, as a konqueror profile. and can better avoid konqueror the file manager stepping on the toes of konqueror the web browser."
    author: "turn_self_off"
  - subject: "sudo apt-get remove dolphin"
    date: 2007-11-26
    body: "Currently, when I open a terminal window from dolphin, it opens to ~ , instead of the present working directory.  That's all I need to know.  Please don't make this the default file manager."
    author: "blackbelt_jones"
  - subject: "Just don't change Konqueror"
    date: 2007-11-26
    body: "QUOTE\n\nAnd Konqueror will benefit from Dolphin as well. Konqueror is not going to disappear for KDE 4, although its user interface may yet see some adjustments as its primary utility will not as the default file manager. Of course, Konqueror will still be available for file management tasks as it has been in the past - there will be no changes in this regard. Changes made to KDE's icon view parts through the development of Dolphin will also help to improve Konqueror's icon views, as they both share these libraries. As stated before, Konqueror loads all of these icon views as pluggable libraries called KParts - improvements to the underlying KParts automatically benefit all users.\n\nEND OF QUOTE\n\nLook, fine, make whatever piece of crap you want the default file manager, but don't try to \"integrate\" Konqueror into some grand scheme.  \n\nNews flash: Konqueror is not a particularly good web browser. That's why every kde-based live CD distro carries firefox (or ice weasel.)  Nobody is going to want to use dolphin as a file manager and konqueror as a Web browser. They're going to use firefox.  Konqueror is a terrific file manager which happens to function as a web browser.  This enables all kind of cool tricks with html, (you can open applications, directories, almost anytghing with an html link) but for surfing, and streaming media, firefox is faster and has the cool plugins.  It is as a file manager that konqueror lives or dies, and I think you'rfe crazy if you think large number of people are going to use both Konqueror AND Dolphin.  They're going to chose sides.\n\nOkay, I don't really have any profound knowledge about how other people use Konqueror, but, neither, clearly, do you. "
    author: "blackbelt_jones"
  - subject: "Re: sudo apt-get remove dolphin"
    date: 2007-11-27
    body: "Okay, so the open terminal window seems to be working now, maybe I imagined the whole thing, who knows?   I'm having a hard time figuring out just how horrified I should be by this."
    author: "blackbelt_jones"
  - subject: "Look, Dolphin is a great idea, but..."
    date: 2007-11-27
    body: "I tried Dolphin, and I can see where a new user might find Dolphin a lot more hospitable than Konqueror , and that's very important. I want to support you in this, \n\nBut Konqueror is the biggest, most comprehensive power-user desktop application ever.  After five years, I'm still finding new ways to use it.  I use it in fluxbox, where it is almost a desktop environment onto itself. I created a special Konqueror homepage where html links can be used to open applications, directories, webpages, even shell scripts.\n\nSo when you do your \"adjustments\", please don't assume that people are going to be using both Konqueror and Dolphin.  \n\nKonqueror is the most powerful, most awesome desktop application, ever.  Please respect the awesomeness.  Don't curtail Konqueror's awesomeness to fit into an \"integrated Desktop\".\n\nAlso, even if Dolphin is the default file manager for KDE, please don't make it the default manager for Konqueror.  In other words, when I open a file in Konqueror, I don't want to see a dolphin window.  This kept happening again and again, until I was forced to uninstall dolphin.   "
    author: "blackbelt_jones"
  - subject: "The more I read this, the more ominous it seems."
    date: 2007-11-28
    body: "\"Konqueror will ship as the default web browser, and will still be usable as a file manager to those that prefer the historical lifestyle.\"\n\n\"HISTORICAL LIFESTYLE?\"\n\nHow did I miss that before?  F*** this, I'm archiving all the KDE 3.5 source code I can get my hands on!"
    author: "blackbelt_jones"
  - subject: "Dump Dolphin"
    date: 2007-12-01
    body: "I came into this discussion a bit late. I had no idea what Dolphin was until I installed the 7.10 version of Kubuntu. I hated it immediately. After finding that Konqueror was thankfully still included, I opened up Adept and removed Dolphin from my copy of Kubuntu. Whoever thought including a half finished piece of S**t like Dolphin would be a good idea should be forced to go back and use Windows Vista FOR THE REST OF THEIR LIVES. Its horrible. \nI agree with the many other posters here that Dolphin is a bad idea. Fine, include an option to use Dolphin for those who wish to, but don't make it the default file manager.\nBTW, the FIRST thing I do now after loading up a copy of Kubuntu? \nRemove Dolphin."
    author: "lpbbear"
  - subject: "Let's keep it simple..."
    date: 2007-12-12
    body: "I firmly believe one of the most important pieces of software ever developed for the operating system has been Konqueror. I classify it as the only real Universal Resource MANAGER. There are small issues with Konqueror that could easily be fixed rather than relegating KDE to even greater complexity.\n\nI would like to see more effort into streamlining KDE and clarifying its interface. There is no sense in having two editors, a host of PIM hooks, and far too many controls and subcontrols. And now there will be more than one way to drag an .mp3 from one folder to another. BLOAT!!!"
    author: "Mark"
  - subject: "dolphin waste of dev resources"
    date: 2008-01-18
    body: "the beauty of open source world - \"being free\" - is also its curse :))\nIn my opinion dolphin is nothing just resource waisting... better idea is to improve konqueror!!!! not by get rid of unused features but providing a very simpler profile (with less options available for those overwhelmed)....\n\nthe only direction for kde is to provide as much customization as possible in most logical way...\n\nkde should be reliable project with no revolution in user space only stable improvement...\n\neg I liked feature of ioslave /devices:, after that was a disaster of /media: because it didn't behave in the some way and it took a lot of time to restore functionality... and now they disappear in kde4... because its dolphin for files!!!!\n\nif some users like simpler file manager do it but why destroying thing liked by others???? \n\ndolphin doesn't allow for multiply windows view (only vertical)... a lot of people use it in horizontal mode"
    author: "genesiss"
  - subject: "Loose Dolphin... PLEASE...."
    date: 2008-03-16
    body: "  A cluttered interface with icons that are far too huge, I really don't like Dolphin, deep six it.\n"
    author: "Steve"
  - subject: "Re: Loose Dolphin... PLEASE...."
    date: 2008-03-16
    body: "Here's an idea that will blow your mind - how about *you* don't use it, and the KDE guys don't \"deep six\" it so the many people who do like it can continue to use it?\n\nOh, and I suspect you meant \"lose\", not \"loose\"."
    author: "Anon"
  - subject: "Re: Loose Dolphin... PLEASE...."
    date: 2008-03-18
    body: "  See the thing is my opinion on this will change if Dolphin actually becomes a useful tool. As far as pwole liking it, well, people like windows too, and there are some that even like Vista. Even if the dev team does stop working on Dolphin *you* can continue to use it, the same way *I* can continue to use Konqueror, or whatever other better file manager can be found.\n  Good call on the \"lose\" vs \"loose\". Thank you."
    author: "Steve"
  - subject: "What to do about it"
    date: 2008-03-31
    body: "I've given KDE4 a chance more than once, I've tried it when it was beta, RC, 4.0.1 and I ran trunk a couple of times, last time was yesterday. Still I can't coerce myself to use it daily even though I really want to, not because of the bugs in Plasma or the lack of apps but because of Dolphin and the Dolphin KPart that has RUINED Konqueror.\nI think Dolphin is Complete And Utter Crap (TM) and the mentality behind Dolphin is the cancer that's killing KDE, and I see that many agree with me at least partially.\nIs anybody working on a Konqueror fork or maybe a port to KDE4 of the old Konqueror file KPart from KDE3?\nDamnit I will pay gold and bleed C++/Qt from my fingers day and night just give me a real file manager on KDE4 not this gnomish krap."
    author: "Teo"
  - subject: "Re: What to do about it"
    date: 2008-03-31
    body: "This kind of rant isn't really worth responding to, but for any more civil people who are interested: both Peter and the Konqueror devs have stated on numerous occasions that any features that have disappeared from Konqueror in KDE4 as a direct result of the use of the Dolphin KPart are to be treated as bugs, and patches are welcome: Konqueror is still the \"power user tool\", and they have no interest in crippling it (largely because they are probably power-users themselves).  \n\n\"Damnit I will pay gold and bleed C++/Qt from my fingers day and night just give me a real file manager on KDE4\"\n\nGreat - go for it!\n\n"
    author: "Anon"
  - subject: "Konqueror's multiple functions are fine!"
    date: 2008-04-23
    body: "I am sorry that it has been decided to \"abandon\" Konqueror. Its multiple personalities were the prime reason for me to keep using it. Nice things such as the UP (URL) arrow and CLEAR (URL) icon gave it an enormous advantage over its competitors, in both web an file browsing.\nTroy Unrau gave _no_ valid reason for \"abandoning\" Konqueror.\nEven a newbie could make good use of it. And he/she would not be confused: he/she could use different applications for \"fileing\" and \"webing\", if he/she wanted to!\nWhat should be done, was to improve it, not substitute it!\nI am very sad for KDE's developers decision!\nJos\u00e9"
    author: "Jos\u00e9 M. Cruz"
  - subject: "Another not intereted in Dolphin"
    date: 2008-04-27
    body: "I am a power user and find that dolphin has slowed down my functional ability to work significantly. My number one complaint is the lack of a tree view as I often use this to move files and directories around the system.\n\nCheers"
    author: "cthulhu"
  - subject: "Konqueror rox!"
    date: 2008-06-04
    body: "Dolphin vs Konqueror is like Tux Paint vs. The GIMP, or Windows' Command prompt compared to a Linux bash shell. Dolphin does an acceptable job as a file manager. Konqueror does a phenomenal job as a file manager.\n\nThe effort should be to improve Konqueror. Many suggestions have been made, such as allowing toolbars and sidebars to be saved with the profile. Dolphin should ABSOLUTELY NOT be the default file manager."
    author: "Ramon Casha"
  - subject: "Re: Konqueror rox!"
    date: 2008-06-04
    body: "Konqueror is dead. Really. It uses the same part as Dolphin use, so all the context menus it had and things like spring loaded folders (when you drag a file over a folder without letting it go, it opens the folder!) are not in KDE4 Konqueror because Konqueror shares the same shitty code as Dolphin has. \nDolphin and Konqueror in KDE 4 are useless. I can't even select multiple files by holding down ctrl which is a PITA if there are files I don't want to select with the mouse.\n\nWell, the whole KDE is dead for me anyway. Next to useless plasmoids and dolphinism. "
    author: "Anon"
  - subject: "Konqueror will live."
    date: 2008-07-26
    body: "<b>Konqueror is dead. Really. It uses the same part as Dolphin use, so all the context menus it had and things like spring loaded folders (when you drag a file over a folder without letting it go, it opens the folder!) are not in KDE4 Konqueror because Konqueror shares the same shitty code as Dolphin has.\nDolphin and Konqueror in KDE 4 are useless. I can't even select multiple files by holding down ctrl which is a PITA if there are files I don't want to select with the mouse.</b>\n\nIt's bad all right, but I bet it turns out okay in the end.  KDE 4.1 RC2 only reinforced my bad feelings about KDE4, but Konqueror will live in the end.  I've heard that it's supposed to b easier to write extensions for Konqueror 4, so maybe an extension is all that's required to fix konqueror.   I'm prepared to keep using KDE3 until KDE5 comes along.  \n\n  "
    author: "blackbelt_jones"
  - subject: "Re: Konqueror will live."
    date: 2008-07-26
    body: "The fact that people think that \n\na) KDE4 will not see any major improvement until sometime past 2013; and\n\nb) That KDE5.0.0, when it arrives, will be strictly better than the last entry in the KDE4 series\n\nabsolutely boggles my mind.\n\nAnyway, I don't see any reason why Konqueror 4 should be easier to write extensions for - do you have a source/ justification for that? \n\n\"Fixing\" Konqueror - whatever that means - simply requires that people step up and help.  Of the two examples your parent gave, one is simply false (you can select multiples files by holding CTRL, using either the mouse or the keyboard) and for the other, a complete newcomer submitted a 5 line patch (excluding comments) that restores spring-loaded folders a couple of weeks ago which Peter says he will incorporate once it is modified a little and he is done fixing bugs for 4.1. Just 5 lines, and yet the parent seems to think that this is some massively intractable problem that is either incredibly hard to fix or that the developers don't want added.  \n\nIt's sad that people are so pessimistic, as it makes potential contributors think that either fixing their pet bug/ restoring their pet feature is too hard or that it won't be accepted by devs."
    author: "Anon"
  - subject: "Useless Konqueror and Dolphin"
    date: 2008-07-30
    body: "Times ago, it was hard to accept Konqueror in KDE, mc had been the tool of choice.\nBut it took no long time to find out how to connect these two tools.\nWorking with the mouse, it was less time consuming to go to the path, taking a look on the files, an with F4 (Extras-Terminal) I got the Konsole and the bash.\nIn this enviroment now I took the mc and made the task on the keyboard without a mouse, without the overhead of konqueror. Trying commands in the Konsole have been possible, but now all the goodies are lost (Forget the included terminal, forget the Konsole in KDE4, with is not capable fixing the window-size permanently, .. ). \nFirefox is the webbrowser of my choice, but useless for working on local html-files. There was konqueror the tool of choice, because it was a browser and filemanger. \nThe Konqueror is now useless, moving files to directories on left tree is impossible, Dolphin is totaly useless, MC is now my only tool, back to the roots? Worth to think about fvwm again?\nMaybe, it seems to me that kde4 will get a windows clone: always hoping to get the thinks I need, but always getting gimnicks not needed.\n\nWhat will I use: \nFirefox - no need for konqueror\nmc - No need for dolphin\n\nPlease think about the further way, explorer is now much better than these two tools. I'm really sad.\n\n\n"
    author: "Thomas"
  - subject: "Re: Useless Konqueror and Dolphin"
    date: 2008-07-30
    body: "Amidst all your whining, you forget to mention the only relevant details that will get your concerns addressed: what you feel is lacking from Konqueror and Dolphin.\n\nAbout the only information you gave on its drawbacks (apart from a thoroughly worthless \"it's useless\") is that \"moving files to directories on left tree is impossible\" (can anyone verify if this is the case?).\n\nWhen are people going to learn that writing a paragraph of whining *without giving any specific details on what the problems actually are* isn't going to help their situation? Talented though the KDE devs are, I'm fairly sure that they aren't mind-readers :)\n\nSo, for everyone who truly wants KDE software to improve (rather than just whining on forums which, while I'm sure it is cathartic for you, is of absolutely no practical worth to anybody.  Including yourself.):\n\n- Cut the Livejournal crap and the \"KDE == teh GNOME/ Windows/ whatever\" droning; and\n- Give us specific criticisms that we can actually work on!"
    author: "Anon"
  - subject: "Re: Useless Konqueror and Dolphin"
    date: 2008-07-30
    body: "\"About the only information you gave on its drawbacks (apart from a thoroughly worthless \"it's useless\") is that \"moving files to directories on left tree is impossible\" (can anyone verify if this is the case?).\"\n\nNah, it's not true. Dropping files on the treeview is perfectly possible. Dropping files on the Places icons works fine, too. Just tested it with Dolphin on Windows, at work, where I don't have access to Linux. (Btw -- starting Dolphin 4.1 on Windows is much snappier than with the last RC.)"
    author: "Boudewijn Rempt"
  - subject: "Re: Useless Konqueror and Dolphin"
    date: 2008-07-30
    body: "You're probably feeling a little deja vu, then? ;)\n\nhttp://dot.kde.org/1179921215/1214618001/"
    author: "Anon"
  - subject: "Comment on a Good Feature"
    date: 2008-10-22
    body: "Just a comment: this article mentions the \"up\" button on the Konquerer interface as if it's a bad thing in a web context: however that feature is one you have to write a plug-in for in browsers like firefox: it's great if you want to move upwards in a directory in contexts that might resist you just changing the url. \n\nIn my humble opinion, and forgive me for now knowing a better place to post this, I would keep that feature."
    author: "John"
  - subject: "Dolphin almost makes it but Konq is now crippled."
    date: 2008-11-26
    body: "By way of introducing this I'll say that I have given Dolphin a chance and it does some, a few things, very well.\n\nIt is better than I feared but it isn't really all that good.  In the process it's crippled Konq and both a file manager, in my experience, as a browser.\n\nFirst let me address the problems with Dolphin.  If, as it seems here, the inspiration for Dolphin as a file manager has largely been from Nautilus and Explorer there has been every reason to fear that this was going to be a dumbed down file manager.  For what it's worth Nautilus and Explorer are worst-of-breed not best-of-breed file managers.  Nor is Dolphin in any way comparable to Midnight Commander which is best-of-breed.\n\nThe first and major problem is Views.  Views must be set, individually for each folder instead of default.  If you want folders view in each folder you get that by default on first start.  And heaven forbid that you want something else because the user cannot click View, select Details and easily make it the default.  Nope, it's hidden down at the bottom of the menu as Adjust View Properties. (-3 for awkwardness and -1 for hiding the default choice so well.)\n\nNice but it's not, as near as I could find, mentioned in the Dolphin Handbook. (-1 for it seems you don't want people to know that.)\n\n(Just why folder view is so popular in file managers of late is one of the biggest mysteries of my life, btw.  It tells me nothing at all about them, rents up far too much screen real estate and, in the end, doesn't even have a lot of visual appeal.)\n\nNow for Details view, in case you missed it, the one I find (found) most useful.  Details reveals a wealth of information hidden by a Folder view at a glance.  Add some details columns and you can see a lot of information there.  A far better way of managing files and folders that a screen full of folders.  \n\nAlso a far better way of seeing if something has gone haywire. Date stamps are wonderful things.\n\nOne HUGE problem, by design it seems, is that while you can sort on the additional information columns once they are displayed you can't move them around to see what you want or need to see in the order you want or need.  They're frozen there in arbitrary order.  (Mostly, it seems, the order you selected them.)  C'mon guys even Explorer does that! (-5 for usability crippling).\n\nColumns is an interesting concept in that it provides a visual drill down of a folder into subfolders and so on.  Drilling down using the mouse is fairly quick but going back is kinda slow.  Still, I can see a use for it once it's refined and made a little quicker.  (+2)\n\nThe Information column.  Is it really necessary to have the folder icon so huge?  Look, I'm near sighted but I can even see it clearly across the room with my glasses off.  So it's too big!\n\nThe concept is good as is the rating and goes some way towards building a semantic desktop but it really needs thinking through.  The icon does not need to be so huge nor should it be.  The tagging ability is nice but as long as I'm in Details view the rest of it is redundant and shouldn't appear at all.  In fact under Details tags should be an Additional information column and yes, along with the rest of them freely rotateable. (+1)\n\nIf you are to borrow from anything for tagging and such do have a look at how Adobe Bridge handles it.\n\nThe items in the right column (Places) are done right and work well.\n\nThe split function works well though it is the mirror image of the functionality of Konq veterans which takes some getting used to.  It's certainly nothing to boast about.  It's existed, as you point out, for all practical purposes, forever in KDE.\n\nIs Dolphin a KDE replacement?  Emphatically no.  Conceptually it's a giant step back to KFM.  Is it capable?  Yes.  Does it rank as a default. No.  Not unless you truly want to emulate broken file managers like Nautilus or Explorer and I don't think that was the intent but that is the road Dolphin is on.\n\nKFM was capable too, remember.  Outstanding it was not.  Dolphin is not outstanding.\n\nKonq, on the other had has been the swiss army knife of GUI file management almost since it's inception.  The fact that it's also been a very capable browser only added to its luster as a file manager.\n\nAs it shares the file management code of Dolphin, along with the assorted strong points and drawbacks I'll skip that except to note that the code runs noticeably faster in Dolphin for some strange reason.\n\nI'll mention, too, that on the Places menu in Dolphin if one plugs in a USB key or drive it instantly appears though there is no such capability for Konq.  (-10 because is more than annoys me and shouldn't have happened.)\n\nKonq retains most of it's swiss army knife characteristics that have made it a power users delight.  For that, I guess, we should be thankful.  It is noticably slower in some things and crashes more than I'd like.  (In fairness Dolphin is crash prone too and both crash in similar circumstances.)  The speed differences are there in both 32 and 64 bit builds.\n\nAs a browser it's taken a few steps backward in that it no longer renders a number of sites at all or as well as it did in it's previous incarnations.  Behind a router forget it.  So the wonderful advantage of being able to look for solutions on the Web to something I found within the file manager part without leaving the central tool is gone, at least for now.\n\nAll in all, then, in spite of Troy's otherwise good, if misguided, in my opinion, PR in the previous post in tracing the evolution of file management in KDE from KFM to Dolphin Konqueror remains the best of breed file manager and Dolphin does nothing to change that.  As I hinted above it's really a prettier evolutionary throwback to the concepts behind KFM.\n\nDolphin isn't a worst-of-breed file manager such as the sad default excuses for file management in Windows and GNOME.  It's not best-of-breed either.  It's kinda like the kid that gets through school with straight C's.  Capable but uninsipiring and not likely to be remembered or amount to much.\n\nFinally, what triggered this response was this.  Troy finished his post with the following statement. \"...Konqueror will ship as the default web browser, and will still be usable as a file manager to those that prefer the historical lifestyle. Users of KDE will have the ability to set the default file browser...\"\n\nThe remark is offensive, uncalled for and insulting and deliberately expressed to be so.  What it indicates is that in Troy's perspective, is that both the file management part of Konq and the browser part are in the not to distant future to become abandonware and no amount of asking, praying and encouraging that neither happen will change that.  It'll be like pissing into the wind of a Force 10 Gale.\n\nThere is no other way to read that remark and, now, no other way to read the often angry and irrational responses by some, a minority to be sure, defenders of Dolphin.\n\nA file manager based on any part of Nautilus is nothing to be proud of and it is not now and never has been something to emulate as it emulates the worst file manager of them all -- Explorer.\n\nTroy, if I wanted to use GNOME, I'd be using it.  I use KDE because it is a far better desktop manager and continues to be in KDE 4.2 in spite of mistakes such as Dolphin and disasters such as the Kickstart confuser.\n\nKonqueror given the chance was and is the future not the past.  The fact that for, whatever reason, KDE developers chose the past with Dolphin (and Kickstart for that matter) mystifies me but that's not your decision not mine.\n\nttfn\n\nJohn (a now insulted and very angry Konq user.)\n\n"
    author: "TtfnJohn"
  - subject: "A vote for konqueror"
    date: 2008-12-07
    body: "While it would have been nice to have had a vote concerning the switch to dolphin as the default file manager, of course the day to day users of KDE were not consulted in the decision to replace konquerer as the flagship application of the project. I have been using KDE since KDE 1 days, even compiling it from source code uncountable times (how many remember the initial koffice versions with icons that looked like lego bricks?). \n\nWhile I have flirted with gnome from time to time as it developed in parallel, KDE was always the first choice in large part because of konquerer. I have been using the various (broken) releases of KDE 4 for some months now and must say that it has not (yet) fullfilled its promise. Konquereor simply saved me time. I am a professional educator, scientist and archaeologist. The ability of konquerer to facilitate multiple views of information in one place --- web resources, sftp connections, pdf files, and local file views among many other resources --- is a stupendous attraction. It simiply saves me time. I can open one \"container,\" choose a profile and get to work. With dolphin and firefox (I can not use konqueror in KDE 4 because it is broken), I estimate that I loose several minutes each time I must switch around and open multiple programs to get started. \n\nThe average user --- whoever that is --- wants to work productively and efficiently. Please, please focus on konqueror and mending its deficencies. You might start by facilitating the function of firefox plugins like zotero on konqueror. You might also facilitate the switching of button bars in sync with the displayed plugin. If the file browser needs fixing, then facilitate its development as a plugin to konqueror's universal container. A much better use of time would have been the development of a mechanism to access and manipulate metadata notes for files much like Gnome's Nautilus has. A universal metadata note taking system to store notes about files, sftp files, web resources locally would be a much more useful addition to KDE.\n\nThose of you who have worked hard on the long term development of konquereor as an application deserve to be pround of your accomplishment. I noticed in today's NYT a full page add for Apple's G3 iPhone. Prominent among the icons displayed on it's \"desktop\" is the icon for safari, a fork of konqueror. While konqueor is downplayed and allowed to remain broken in KDE 4, the many, many users of the iPhone browse the internet with a tool that is a descendent of konqueror!\n\nWhile we all understand the attraction of starting a new project like dolphin rather than emending and extending an existing one like konqueror, the real long term success of KDE lies in improving and streamlining the product. As a commentator above pointied out, why spend time on several applications like multiple multimedia tools when the user wants a single tool that works well with all the kinds of data they may encounter. Embrace, develop and extend a single audio media tool, a single video media tool and the single universal resource container, konqueror.\n\nChris\n\n\n"
    author: "Dr Christopher von Nagy"
  - subject: "Re: A vote for konqueror"
    date: 2008-12-08
    body: "\"I can not use konqueror in KDE 4 because it is broken\"\n\nCan you point out exactly what is broken in Konqueror from KDE4?"
    author: "Yves"
---
As some of you who monitor the KDE news sphere may have noticed, there has been a recent addition to the kdebase module. The <a href="http://enzosworld.gmxhome.de/">Dolphin File Manager</a> has been added to complement Konqueror's browsing capabilities. Read on for more information about this new File Manager and its relationship to Konqueror and the rest of KDE.




<!--break-->
<p>A brief history lesson so you can get an overview of how file management has evolved with KDE: In KDE 1.x, KFM (the KDE File Manager) was born. It was a very rudimentary, very basic file manager with limited web browsing capabilities. Below is a shot of KFM browsing files (from the kde.org screenshot archive) so you get an idea of how it operated.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol10_1x_kfm.png"><img src="http://static.kdenews.org/dannya/vol10_1x_kfm_thumb.png" alt="KFM in KDE 1.x" /></a></p>

<p>While it's obvious that KDE has come a long way since KDE 1.x, it is still easy to see which parts of KFM have inspired Konqueror's contemporary design, which was introduced as part of KDE 2.0. KParts technology revolutionized the way we used our File Manager application, turning Konqueror into a full fledged web-browser, and more. Here's a shot of Konqueror from KDE 3.5.6, and you can see that while the user interface is much improved, the same basic concepts remain visible from the KFM days.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol10_356_konq.png"><img src="http://static.kdenews.org/dannya/vol10_356_konq_thumb.png" alt="Konqueror in KDE 3.5.6" /></a></p>

<p>Konqueror really shines as a beacon of KDE technologies in the KDE 2.x and 3.x series, showcasing the best parts of KDE technologies. Konqueror showcased the power of KDE's IO slaves, allowing true network transparency when managing your files over FTP, fish (SSH), HTTP, and much more. Konqueror is so advanced that you can enter an FTP URL into a HTML upload form and it just works as you would logically expect it to (as far as I know, it is the only browser which allows this). It also featured KParts, which allowed it to embed just about any sort of viewer required, directly into the interface, embedding things like <a href="http://kpdf.kde.org/">KPDF</a>, <a href="http://koffice.kde.org/kword/">KWord</a>, image viewers, and most importantly, the ever-improving <a href="http://www.khtml.info/">KHTML</a> page renderer. This is important, since even Konqueror's icon views were implemented as pluggable parts, making just about any kind of icon view possible.</p>

<p>So, Konqueror is a really powerful tool that can do just about everything you and your system can possibly want, and with this power comes unlimited configurability and extensibility through control modules and plugins. However, what often happens in Konqueror when you are browsing the internet is that Konqueror still wants to behave as a file manager and not a web browser. This split behavior is easily noticed through elements such as toolbar buttons. For example: the "Up" arrow is still available on the toolbar even when browsing Google Maps, but it is totally irrelevant in this context; another is having a web bookmarks toolbar visible while sorting icons in your /home folder.</p>

<p>Introducing <a href="http://enzosworld.gmxhome.de/">Dolphin</a>: Dolphin is a new File Manager for KDE 4 which is dedicated 100% to file management, and is not intended to be a one-size-fits-all tool as Konqueror currently attempts. It is intended to optimize your file management related tasks, and present an easy to use file manager for casual KDE use. That doesn't mean it won't be powerful or configurable, only that Dolphin is being built for a single purpose.</p>

<p>Dolphin isn't a total rewrite however, and is not intended to compete with Konqueror, rather the two applications will be complimentary. Dolphin uses the already existing IO slave facilities of the KDE platform to perform remote or local file management, meaning that it will be capable of doing all of the 'remote management' type activities that Konqueror has already matured. Dolphin just won't show web pages or PDF files embedded as Konqueror does.</p>

<p>And Konqueror will benefit from Dolphin as well. Konqueror is not going to disappear for KDE 4, although its user interface may yet see some adjustments as its primary utility will not as the default file manager. Of course, Konqueror will still be available for file management tasks as it has been in the past - there will be no changes in this regard. Changes made to KDE's icon view parts through the development of Dolphin will also help to improve Konqueror's icon views, as they both share these libraries. As stated before, Konqueror loads all of these icon views as pluggable libraries called KParts - improvements to the underlying KParts automatically benefit all users.</p>

<p>So lets take a look at Dolphin and Konqueror as they currently exist in <a href="http://websvn.kde.org/">KDE's Subversion repository</a>. Please keep in mind that these snapshots represent developer work-in-progress builds and, while publicly available, are not representative of the final appearance or intended functionality of either applications, nor are they recommended for everyday use.</p>

<p>Konqueror currently looks something like this, and the icon views only half work. The problem is that these file views are simply direct ports of the KDE 3 codebase. Konqueror will eventually receive the same fileviews that Dolphin is currently using.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol10_4x_konq.png"><img src="http://static.kdenews.org/dannya/vol10_4x_konq_thumb.png" alt="konq in kde 4x" /></a></p>

<p>You can tell from Konqueror's default configuration of using tabs, and various other related interface choices that Konqueror is now mostly a web browser that also happens to do file management. While Konqueror's roots are truly derive from file management, it is more frequently operated as a browser these days by many KDE users. Konqueror does a great job as a web browser, underpinned by the fact it <a href="http://www.css3.info/khtml-356-is-the-most-css3-compliant-of-all/">now implements CSS 3</a>, including the highly-anticipated 'opacity' tags.</p>

<p>So while Konqueror continues to improve as a browser, it will continue to maintain KDE 3.x file management standards, providing a baseline functionality, and will be improved as code is shared between itself and Dolphin.</p>

<p>Dolphin is a whole different animal. It is a 'real' file manager - it's interface has a lot of elements which are specific to a file manager and cannot really be justified in a browser. This is best demonstrated with a screenshot.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol10_4x_dolphin_breadcrumb.png"><img src="http://static.kdenews.org/dannya/vol10_4x_dolphin_breadcrumb_thumb.png" alt="Dolphin in KDE 4.x showing breadcrumbs" /></a></p>

<p>Notice the implemention of a 'breadcrumb'-style directory selector, which works well for file management in a lot of cases, but is totally useless if you need to enter a URL when using a browser, and so becomes the sort of widget which is only useful when dealing with file hierarchies. Breadcrumb widgets may be familiar to anyone who has used OS X's Finder or GNOME's Nautilus. Another comment about the above screenshot: clicking and holding a breadcrumb item displays a list of directories that are at the same level as the one clicked, allowing for more efficient navigation.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol10_4x_dolphin_alt.png"><img src="http://static.kdenews.org/dannya/vol10_4x_dolphin_alt_thumb.png" alt="dolphin in KDE 4.x showing other configurations" /></a></p>

<p>However, using the breadcrumb widget is not essential, and if you are more comfortable with a Konqueror-style location bar, this mode of operation is easily configurable, as seen above. In fact, much of Dolphin is configurable, illustrated below.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol10_4x_dolphin_options.png"><img src="http://static.kdenews.org/dannya/vol10_4x_dolphin_options_thumb.png" alt="dolphin in KDE 4.x showing configuration dialog" /></a></p>

<p>This screenshot evidences the amount of effort KDE is spending trying to make configuration layouts sane while still providing as many options as reason allows. Also note the improved appearance of the configuration dialogs in KDE 4. Of course, this is going to be revisited somewhat as the dialog is too tall for some screens at the moment. After the Oxygen visual components go live, this dialog will be even easier on the eyes.</p>

<p>So, Dolphin's functionality is not entirely new, other than it presents itself in a new way. It can be seen as a hybrid between the power of Konqueror and the structure of Nautilus. Dolphin still builds on a strong KDE base, reusing existing technologies like KIO slaves and so forth. Right-click actions that were available in Konqueror will still be mostly present (except that Dolphin will necessarily load files externally instead of using embedding viewers). And Konqueror can now improve its web browsing experience even more, doing so without losing the file browsing support that has been there since KDE 2.0.</p>

<p>When KDE 4 is released, Dolphin will be configured as the default application for the local file:/ protocol, as well as the default file manager listed in the applications menu. Konqueror will ship as the default web browser, and will still be usable as a file manager to those that prefer the historical lifestyle. Users of KDE will have the ability to set the default file browser, much like how KDE 3.x can use third-party applications such as Krusader as the default file manager. Stay tuned for more information as Dolphin and KDE evolve towards 4.0.</p>



