---
title: "Ars Technica: A First Look At Dolphin"
date:    2007-04-09
authors:
  - "tunrau"
slug:    ars-technica-first-look-dolphin
comments:
  - subject: "Nice Article"
    date: 2007-04-09
    body: "I like the article!  I defiantly think that the switch from Konqueror to Dolphin is a good thing.  I can't wait for KDE 4, or Troy Unrau's next \"Road to KDE 4\" article!  One of the goals for KDE 4 should be to take the place of gnome and become the DEFAULT desktop for Ubuntu and Suse."
    author: "batonac"
  - subject: "Re: Nice Article"
    date: 2007-04-09
    body: "> I defiantly think that the switch from Konqueror to Dolphin is a good thing.\n\nWhat a cute typo. How appropriate. :-)"
    author: "Anonymous Coward"
  - subject: "Re: Nice Article"
    date: 2007-04-09
    body: "You can use dolphin with KDE 3.5 now. Dolphin is no reason to wait for KDE4\n\ne.g. using Mandriva as root\nurpmi dolphin\n\nSo what was my impression? \n\nWow!\n\nDolphin makes it much more convenient. Some features are missing but you really don't care.\n\nWhat you really would like to have with Dolphin is a MAC OS X style menu bar on top. That is a very nice option for Dolphin and its possible with KDE.Unfortunately non-KDE killer applications as OpenOffice don't play well with the KDE topbar setting. The real question is of course: why do I want the pull-down menus on top. And the reason is: Dolphin is nice but the menu bars take too much screen and are not of so much use. \n\nSo What I would suggest for Dolphin is to get rid off the whole pull-down menues. They are really not needed. And when you don't have them on top MACOSX style for small windows they will consumer 3 lines with small windows. See screenshot. - you can apply the options in context menues\n\nFile - edit - view -- these three can be made obsolete.\n- Go to - you can leave out this one as well.\n\nExtra - Open console: That looks very important to me and needs to be integrated at an appropriate intelligent place. Maybe that should be just another \"view\".\n\nSearch-file --- that one is ugly and bloathed: We want google style simplicity. \n"
    author: "bert"
  - subject: "Re: Nice Article"
    date: 2007-04-10
    body: "> You can use dolphin with KDE 3.5 now. Dolphin is no reason to wait for KDE4\n\nNo, the KDE 4 dolphin version becomes more different to the KDE 3 dolphin version every day. Don't review the KDE 3 version and state you know how KDE 4 will look like!"
    author: "Anonymous"
  - subject: "Re: Nice Article"
    date: 2007-04-10
    body: "Nobody knows how KDE4 will look like. But for KDE 3.5.x Dolphin already makes a real difference."
    author: "bert"
  - subject: "Re: Nice Article"
    date: 2007-04-10
    body: "I just hide the menubar by default for Konqueror. \n\n\"ctrl-m\"\n\nSadly this doesn't work with the current KDE 3 Version of Dolphin. "
    author: "Arne Babenhauserheide"
  - subject: "This article, just like Dolphin itself, is garbage"
    date: 2007-04-09
    body: "Normally I don't get worked up over what's going on in the KDE world but recently I had a chance to try KDE 4 and I almost lost my mind when I saw Dolphin.  From the article:\n\n\"Konqueror's elaborate profile system and support for KParts-based document viewing add complexity to file management and intimidate users who are accustomed to less sophisticated file managers.\"\n\nWho has been intimidated by konqueror?  I want names.  This crappy nautilus wannabe is catering to a nonexistent audience while simultaneously solving a problem that noone has.\n\nSo how does this fine piece of tech journalism end?  \n\n\"Although some Konqueror enthusiasts are skeptical about the potential benefits of the transition to Dolphin, I think it's important to keep an open mind and wait until Dolphin is complete before passing judgment.\"\n\nYeah, who cares if it has half the features and will become the default filemanger?  Let's just wait it out and hope (with an open mind) that Konqueror development isn't totally destroyed or subverted into just a web browser.\n\nWhoever is in charge of KDE 4 release decisions needs to squash Dolphin as the default IMMEDIATELY.  If you want to a konqueror profile suitable for a three year old or a gnome developer, fine.  Go ahead and include it as the default view if you want to.  But DO NOT remove as default the one app that differentiates KDE from every other desktop unless you want an even nastier version of the gnome/nautilus debacle (which, by the way, brought me over to KDE in the first place).\n\nAnyone who is a current KDE user needs to burn and run one of the KDE 4 live DVDs to see just what a horror Dolphin is.  I promise you will be shocked."
    author: "Two year KDE user"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-09
    body: "I agree, one of the major reasons I use KDE is for Konqueror, which is excellent.\n\nOf course, just like they say: If it looks too good to be true, it probably is. That is to say, since Dolphin is going to take center stage, I just may have to go back to Gnome, even if it means missing out on KDE4. I've been a KDE user for several years and I'm sad to hear of this news regarding Dolphin."
    author: "Joseph"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-09
    body: "Konqueror is a _terrible_ browser, and I'm glad it's getting scrapped (well, not really, but it's no longer the default).\n\nIt is extremely buggy. I don't crash it as often these days, at least, but it has all kinds of weird and wonderful quirks. For example, keyboard navigation is terrible. You hit the left or right keys and you have no idea where the cursor is going to jump, except that it will generally be in the approximate proper direction. It often \"forgets\" history. That is, if you hit back, it goes back two. If you then hit forward it goes forward two. There's no sign of the intermediate step.\n\nIt is slow. Have you ever browsed a filesystem with Windows explorer? There is no perceivable delay between clicking on a directory and its contents appearing, unless there are many thousands of files in that directory. With konqueror, if there's more than a couple of dozen files, you can watch them appear in batches.\n\nFinally, as much as people might like them, kparts are a nuisance. Do you realize how much of a pain it is to click on something, then hit close, or Ctrl+W, only to lose your entire file browser because you forgot that this particular action (unlike actions on various other files) opens in a kpart in the browser.\n\nKonqueror is terrible. If you people can't see that, KDE 4 is doomed and I'm switching to Gnome. Fortunately, I think that common sense is finally prevailing and maybe soon, this monstrosity will die."
    author: "Anonymous Coward"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "\"Have you ever browsed a filesystem with Windows explorer? There is no perceivable delay between clicking on a directory and its contents appearing, unless there are many thousands of files in that directory.\"\n\nI don't care much for the rest of your statement, I neither love nor hate Konqueror, but this remark is just silly. Yes, I use Windows all the time and yes, even om my brand new dual core system there are often noticeable delays when clicking on directories! Especially if they have a lot or large .zip files in them for example.\n\nAt other times it's because there is a drive letter referring to some inaccessible server.\n\nYou might think that's normal but why the heck does it need to hang the entire program?? And if you don't have the option enabled to give each explorer window it's own process ALL of the explorer windows are unresponsive!\n\nThat's a problem I've never had with KDE and I'm always glad that whenever I accidentally enter \"that\" folder that contains several thousands of files that I can still hit back or up without waiting for KDE to list all those files."
    author: "Quintesse"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-12
    body: "> I neither love nor hate Konqueror, but this remark is just silly.\n\nI can browse files on my Linux box faster over SMB using Windows than with Konqueror. This is taking into account various things like the dentry and inode caches. Call it silly all you want, but it's true."
    author: "Anonymous Coward"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "> For example, keyboard navigation is terrible.\n\nMight be. Does matter for some 0.x percent of the current user base maybe, not to speak about the masses caring even less.\n\n\nKonqueror is quite nice, but has a few problems:\n\n- missing dns cache (and no, that you can use a small server like dnsmasq isn't an argument for joe user)\n- very slow javascript\n- no plugin ecosystem like firefox"
    author: "Carlo"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "- no plugin ecosystem like firefox\n\nThere is a plugin system, it only lacks the audience creating plugins.\nPerhaps kde4 will change that as konqueror gets a larger potential usergroup including windows and mac users.\n"
    author: "whatever noticed"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "> There is a plugin system, it only lacks the audience creating plugins.\n\nIt's not the /audience/, but the missing effort to build such a community and maybe the entry level is to high for a number of people, having to code them in C++. That's why it's no _eco_system. Honestly said, I haven't even looked what you can do with it, because even the builtin stuff (cookie handling, ad filtering, conditional javascript usage) doesn't hold a candle to the extensions available for Firefox - neither functionality- nor usability-wise."
    author: "Carlo"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-12
    body: "> Might be. Does matter for some 0.x percent of the current user base maybe,\n> not to speak about the masses caring even less.\n\nMaybe, but Windows gets it right. There's a big difference between saying that something doesn't need to be added because nobody will use it and saying that something doesn't need to be fixed because nobody notices. People do notice, and it's been around for a very long time. Also, people have been working to fix it as it _has_ been improving, albeit slowly."
    author: "Anonymous Coward"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "Have you ever browsed a filesystem with Nautilus? On my computer it takes 15 seconds to display the contents of /usr/bin (+/- 2000 files) and the screen is blank until the list is completely loaded (the same applies when you want to select another application in Firefox than the default one for opening some mimetypes and you have to select the app in the GNOME file dialog that pops up; what's even worse is that if you type e.g. /usr/bin/kpdf and press Enter then the file dialog first loads the contents of /usr/bin before closing).  In Konqueror it takes 2 or 3 seconds (for the same directory of course) and you can start browsing immediately since the list is built up incrementally.\n\nKParts is not a problem at all: if you don't like them, go to the \"File Associations\" tab in the Konqueror Options and disable embedding for all mimetypes."
    author: "Vlad2"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-12
    body: "Maybe Nautilus sucks, too. That's not the point. You also can't start browsing immediately because things are moving too quickly.\n\nAs for KParts, you can turn them off, but then you have an uglier Dolphin, no?"
    author: "Anonymous Coward"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "Fortunately I haven't run into these bugs... but if Konqueror is boggy or have useless navigation then that should be fixed instead of writing a completely new application."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-12
    body: "It should be, but all these bugs have been there for years, and some of my complaints (e.g. KParts) were about the fundamental design of Konqueror. I think it is time to move on."
    author: "Anonymous Coward"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "That's a very odd reason to go back to gnome :o)\nAs stated in the article, you can use konqueror as default filemanager if you wish.\n\nAnd besides that, i think you should use the final version of dolphin before you can judge if kde does a step forward or backward by including dolphin as default filemanager.\n"
    author: "whatever noticed"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "Well, that's weird. I'm using KDE on FreeBSD and I prefer Konqueror as a browser to any other (although it has some issues with native flash on FreeBSD).\n\nI think its file management capabilities are very satisfactory (besides the missing split view), but just to try out, I installed Dolphin and use it for common tasks now. I miss some things there, but overall it's fully usable. There's nothing wrong with it."
    author: "Silver"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: ">the missing split view\n\nUhh...\n\n/me coughs"
    author: "Wyatt"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "Man that's an ugly setup.\n\nBut yes, Konqueror has split view and yes, Dolphin is a terrible idea."
    author: "foo"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "and yes, you can use konqueror as default filemanager if you want to"
    author: "whatever noticed"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-11
    body: "Oh yes, sorry. Konqueror really has the split view :)"
    author: "Silver"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-11
    body: "Konqueror was one of main reasons why I use KDE. I think many do.. Why to replays it? to sophisticated? Nonsense. I think its wrong turn of KDE team..\nHope you will realised that soon..."
    author: "Dumas"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-09
    body: "though i oppose the capitalization, i share your point. having the same app that is the file manager serving also as web browser is great, and so is konqueror.\ni'd be interested to learn in which process it was decided to make dolphin the default file manager. anyone with the enlightning-rod around?\nknowing it s too late anyways, out of curiosity i'd like to see how a vote on this would turn out. \n "
    author: "somecoward"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-09
    body: "I'm sure there will be an option to make konq a default filemanager with one click, it's not like somebody removing konqueror without any possibility of recovery. All your rant is about just a couple of .desktop files, that can be easily changed to start any program you want. Heck, I would add a dialog that would pop at first Dolphin launch to ask whether you want konqueror to be the default. Same thing can be added to KPersonalizer, KControl module, etc. As a power user of KDE with 9 years of experience I want to try Dolphin, and I'm confident that I can reconfigure the desktop to my old defaults under <5 min."
    author: "Sergey"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-09
    body: "I second your thoughts!\n\nkde is unique because of konqueror. i like to have both usecases in 1!!! app!"
    author: "funnyfanny"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: "> Who has been intimidated by konqueror?\n\ni don't suppose you care, but this is the result of actual user feedback augmented by user testing to see what the feedback has been about.\n\n> who cares if it has half the features and will become \n> the default filemanger? \n\nyou and i want/need all those features. turns out a huge # of people don't, including a good % of our current user base. but hey, screw them just so we get what -we- want, right? no point in trying to have something for everyone, right?\n\nseriously, come off your pedestal and realize that kde is not only for you, it's for everyone. if i can do it, as one of the people helping make the damn thing, i'm sure you can too.\n\n> Let's just wait it out and hope (with an open mind) that Konqueror\n> development isn't totally destroyed or subverted into just a web browser.\n\nnothing quite like fear, is there? ;)\n\nlook at the change in konqueror as a file manager between 3.0 and 3.5. very little changed. so let's not go inventing some silly story that konqueror development was ripping along.\n\nand how has it been doing for 4.0? there have been at least as many improvements for 4.0 than did between 3.0 and 3.5, such as the move to model/view based views and tweaks to the gui.\n\nso you don't need to guess or hope, you can see it happening right now.\n\nfurthermore, we've (the developers) stated from day 1 that we are not looking to or wanting to strip down konqueror to a webbrowser-only (i'd rather see a dolphinesque web browser first before that ever happened). konqueror is remaining in kdebase where it always has been (well, since 2.0 anyways =) and stays as the power tool of choice.\n\n> But DO NOT remove as default the one app that differentiates KDE from every\n> other desktop unless you want an even nastier version of the gnome/nautilus\n> debacle (which, by the way, brought me over to KDE in the first place).\n\nah, now we get to the heart of the issue: you feel you were screwed over once by completely different group of people with an app that looks similar (not even identical) in screenshots. i can empathize: once bitten, twice shy.\n\nplease realize that kde is not gnome: we do not have their agenda or their way of approaching either technology or our user base. which approach is better, well, everyone can decide for themselves. but the approaches are different. please don't paint us and our efforts by the outcome of theirs.\n\ni'd also hold off on passing judgement on dolphin for another couple of months until 4.0 starts to settle down. it's not going to be a nautilus.\n\nand finally, this is kde: you get to choose. want to use konqueror rather than dolphin for your file manager? great! nobody is stopping you. simply make the switch, something you are far more capable of doing than those who benefit from dolphin's simplicity; and we won't be hiding these options beneath a registry tree somewhere either."
    author: "Aaron J. Seigo"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: "++"
    author: "Troy Unrau"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "+++ !"
    author: "Askrates"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: "Aaron,\n\n  Is this user study online somewhere?  Did the users actually say they want those features removed?\n\n"
    author: "John Tapsell"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: "I don't know where the studies are now, but they have been posted on dot or kde-core-devel at various times. \n\nI think the biggest problem with the usability studies is that all make the same mistake, and only study the rare and elusive first time user (sometimes first time KDE users, sometimes first time PC users). \n\nI have yet to read a usability study that focus on everyday day use, and test how useable KDE is a too a user who has used it at least half a year. They are the ones that are our users, and they are the ones we SHOULD be catering to.\n\nIt seems usability people consider previous experience a complication in their studies they just can't or won't handle."
    author: "Allan Sandfeld"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: "thats makes absolutly no sense. how do you want to determine they \"easyness\" of a task when all participants allready learnt how to do the task?\n\nthe point is, that a task should be done with as little extra learning as possible.\n\nmaybe you should look up what usability is about..."
    author: "ac"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: "Maybe get a user that used kde in general for half a year, then get them to do tasks that they haven't done before or tasks that they don't do very often.\n\nYou could even get them to do a task that they do fairly often, and see if they still make mistakes or take the long route round etc.  See how well short cuts are discovered/used."
    author: "John Tapsell"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: "Hmm, for the basic tasks, you only learn them once, but do them over and over again. Wouldn't it make sense to optimize for efficient _usage_ rather than for efficient _learning to use_? And for what etymology's worth, the word 'useability' would seem to point to easy 'usage' rather than easy 'learning to use'. And of course, I am not against easy to understand interfaces, only for me it seems that it is not as important --- I am willing spend some time learninng to use the tools I do my work with, if it's worth the effort.\n\nJust my 2c.\n\n\n"
    author: "Jonathan Verner"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-11
    body: "vi has pretty efficient usage, but terrible \"learning to use\".  As a result hardly anyone uses it and even those that do (like me) probably don't use all the features they could benefit from.  OK, vi is a bit of an extreme case but whatever tool we talk about tasks certainly have to be easy to learn to do.  People won't get the manual out to learn how to do something, they'll just get an easier-to-use tool even if, in the long run, it's less efficient.  The real trick to design is to cover both aspects, ease of learning and long term efficiency."
    author: "Adrian Baugh"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-11
    body: "There's a difference to efficient usage and terrible learning curve and efficient usage and some need to learn. \n\nThe problem with vi as well as emacs is, that their shortcuts are (more or less) only useful within the single application. Staying in the context, the problem with KDE (or the whole desktop - no matter which application toolkit in use) is, that there's no consistent profile of actions and behaviour.\n\nAn example: The Konqueror side bar can be shown/hidden with f9, when I use KPDF it is ctrl+l. Moreso, when I open a pdf file in Konqueror, the KPDF kpart is opened, but its navigation widget doesn't embed into Konqueror sidebar as you'd expect it to. When I want to see a pdf in fullscreen, I use ctrl+shift+f. Contrary to Konqueror, there's no button shown to get back to normal view mode; That I have to press ctrl+shift+f again (allowing esc as well wouldn't be bad) is something I have to know. There're lots of these inconsistencies making it hard for new users. And it shouldn't be up to single application developers to define shortcuts and implement widgets with similar functionality in different ways. Have a look at your KDE application. There are many with sort of a sidebar and all of them \"feel\" different.\n\nThe problem becomes even more blatant, if you extend it to the whole desktop: Why has the user to know that e.g. Firefox and Gimp use F11 as shortcut for fullscreen functionality, while KDE applications use ctrl+shift+f? Why can't the unix desktop world come together, define abstract actions, implemented as profiles and all applications would react to the user chosen profile shortcuts, instead defining their own?"
    author: "Carlo"
  - subject: "Re: This article, just like Dolphin?"
    date: 2007-04-11
    body: "Well, that is one (a very wrong) way to look at it.\n\nStudies (they were mentioned in the AAAS journal Science) have shown that no matter what the task there are two types of users which are (surprise :-)) new users and experienced users.  There is, or should be, a difference of kind, not just degree in the way these two types of users use something.  \n\nThis is the source of one of the most valid criticisms of the Mac interface.  It is very easy for first time users, but experienced users gain little -- the learning curve doesn't descend very far.\n\nSo, applying the tautology that most users of a product have been using it for a while, it is a valid question to determine how experienced users use the product and also to determine how rapidly users become experienced users.\n\nYou can also study metrics (no actual users needed) based on the presumption that the user knows how to do the task.  That is, how much does the user need to do to accomplish a task.  This paradigm easily shows the gross stupidity of not having things on the toolbars.  Is it really better, and easier, to click through three levels of menus than to click on an icon on a toolbar to do something?"
    author: "James Richard Tyrer"
  - subject: "Re: This article, just like Dolphin?"
    date: 2007-04-11
    body: "but experienced users are allready part of the kde development. they provide their wishes through the bugtracker, or even write the software ;).\n\n\nso studies with new users are far more important for kde.\n\nalso it depends on how you use studies. as i see it, the kde team uses them to determine how to implement something, not to determine what to implement. because they know very well what they want. so again, testing with experienced users isn't realy needed, at leased not as much as testing new users..."
    author: "ac"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-11
    body: "Hmm.\n\nIn my trade, we can tell inexperienced mechanics from the tools they choose. The most useful and quickest tool, used to diagnose almost everything, requires knowledge and experience. Without the knowledge and experience, it is confusing and useless.\n\nBut once a person has the knowledge and experience, all the extra 'user friendly' stuff in the other tools get in the way of doing the job.\n\nWhen I see usability studies suggesting removing features to appeal to inexperienced users, I literally lose interest and walk away. Inevitably the buttons and features I use regularly disappear. I don't mean to insult anyone, but this has been my experience. Obviously the intended target is someone else.\n\nBTW, the first thing i noticed about Dolphin (for 3.x, which may not apply to the final product) is the file preview required one click. I deleted it. I hope it's changed since then. It seemed to be geared to esthetics as opposed to actually being useful.\n\nDerek\n\nps. Konqueror was getting very close to replacing the command prompt for file management. I'm quite happy to see continued work on Konsole, since I (and probably many others) will need it."
    author: "D Kite"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-11
    body: "or you could just keep using konqueror. I suppose expert mechanics aren't necessarily too good at reading tfa though."
    author: "Adrian Baugh"
  - subject: "Re: This article, just like Dolphin itself, is ..."
    date: 2007-04-10
    body: "thats makes absolutly no sense. how do you want to determine they \"easyness\" of a task when all participants allready learnt how to do the task?\n\nthe point is, that a task should be done with as little extra learning as possible.\n\nmaybe you should look up what usability is about..."
    author: "ac"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-04-10
    body: "It's not worth bitching about it. There are people who like it simple, who follow the hype, and there are people, who know what's Konqueror worth, both as a file manager and (well, a bit less) as a browser."
    author: "Carlo"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: ">>Who has been intimidated by konqueror?  I want names.\n\nWell, i did, but i'd rather stay anonymous. Dunno if i would get even more intimidated by you :o)"
    author: "whatever noticed"
  - subject: "Re: This article, just like Dolphin itself, is garbage"
    date: 2007-04-10
    body: "Me too. It has way too many 'things'.\n\nDolphin looks a lot cleaner, and makes more sense too. Most of the web browsing tools don't apply to file browsing and vice versa.\n\nFor example: The history (although konquerors sucks anyway)\nHome page\nThe 'metabar'\nFile tree."
    author: "Tim"
  - subject: "Re: This article, just like Dolphin itself, is gar"
    date: 2007-08-09
    body: "I am also shocked that Dolphin is going to replace Konqueror as the default file manager in KDE 4. Why doesn't KDE simply create a simple \"file manager view\" if people complain about konqueror being to complicated?\n\nKonqueror is a fantastic, flexible, feature rich file manager. As a browser it is not yet very good (slow, instable). I am pretty sure that many people would agree.\n\nSo a reasonable person would think that KDE would keep the good stuff and improve the    weak parts. But what do the KDE folks do? They say: \"Well, let's keep the bad stuff and replace the good stuff with bad stuff. \"\n\nWhy do they do this?"
    author: "Oliver"
  - subject: "I am glad."
    date: 2007-04-09
    body: "I am glad that you think you're the %100 of the KDE user base on your own self \"two year\", Myself, I'm pretty happy to have both dolphin and Konqueror, I use Konqueror now only for very specific file tasks, a few complicated ones. For the rest I happily use Dolphin (in Kde 3.5.6) and I am very happy with it, Personally I find I am much more confortable, and faster working on my daily file tasks in Dolphin's clean, simple interface than I ever was with Konqueror.\n\nRather than a \"Nautilus wannabe\", I think Dolphin is a good tool designed by people who understand ussability and have learned from other great systems, including Apple's Finder.\n\nNow please I beg you to stop talking on my stead (proclaiming there is no need for Dolphin, and nobody wants it), Because all you're doing is demoting your comment from usefull feedback to useless trolling."
    author: "Lars"
  - subject: "Re: I am glad."
    date: 2007-04-09
    body: "++\n\nThe incessant whinging by people who seem to want us to look up to them as \"power users\" when apparently they are too inept to spend 20 seconds changing the default file manager from Dolphin to Konqueror is becoming most tiresome.  \n\nPeter, and everyone else behind the decision - ignore the naysayers! Your determination to use Dolphin as default in spite of the barrage of flames and threats from people such as TYKU sends a clear signal that you have listened to the criticisms of Konqueror and have a clear focus on usability for KDE4 while - even better - the fact that you have left it so that the decision can be easily reversed shows that you have not turned your back on some of KDE's most devoted fans (which includes myself: Dolphin looks great, but I personally won't use it unless it has tabs :)).\n\nWhining about something so trivially changed is like screaming at the top of your voice about how much you hate the dark when you have a candle and a match in your hands, and frankly is the kind of tantrum I would expect from a two-year-old.  It's simply embarrassing to watch."
    author: "Anon"
  - subject: "Re: I am glad."
    date: 2007-04-09
    body: "When I first heard Konqueror wasn't going to be the default file manager I was very upset. However after trying Dolphin I see why it has been chosen as the new default. It is very simple to use and will be great for new users. I will continue using Konqueror personally because it is so powerful and I am so used to it but Dolphin is a fine file manager and I am sure both projects will become even better by the time KDE 4.0 is released.\n\nBTW why doesn't someone from the Dolphin project get interviewed for KDE://Radio? or is that podcast pretty much dead?"
    author: "Devon"
  - subject: "Re: I am glad."
    date: 2007-04-10
    body: "KDE://Radio: it's not so much dead as i simply don't have time atm. another person was set to continue it a couple months back, but that hasn't worked out. if anyone is willing and able to step up, please email me."
    author: "Aaron J. Seigo"
  - subject: "Re: I am glad."
    date: 2007-04-10
    body: "Radio == support for PLS files?"
    author: "bert"
  - subject: "Re: I am glad."
    date: 2007-04-09
    body: "I agree.  I've been using Dolphin for a little while now and I really enjoy it.  It's clean, easy to understand, and fast.  Konqueror was fine for me but far to buggy because of all the features I rarely used being present.  My wife hated Konqueror with a passion because she couldn't understand it and refused to use it.  And for the guy above who wanted name, her name is Amanda (I won't give her last name for obvious reasons.)  I showed her Dolphin and she's much happier using linux now.  I have no problem with changing the default file manager to konqueror from dolphin in kcontrol.  And I think if you do have a problem with this you should reconsider your position as a \"power user.\"    Having Dolphin available now allows us to reach the average computer user as an audience.  And having Konqueror still available to be made default allows us to keep our power user audience.  It's  a win-win.  Keep up the good work guys.  "
    author: "Carl"
  - subject: "Re: I am glad."
    date: 2007-04-10
    body: "Up to now I didn't really care about all that much about dolphin. But having people compare it to nautilus or apple's finder has alerted me. To put it simply: I find those two to be utterly unusable. And I must agree with the grandparent poster that the greatest piece of software in KDE is konqueror.\n\nNow that's only my personal and uninformed opinion and I don't want to add more oil to the fire. But if it's going to take more than 2 or 3 clicks to restore konqueror as default file manager, I fear the result could be disastrous for KDE.\n\nI guess I'll have to check out a live CD really soon."
    author: "anonymous coward"
  - subject: "Re: I am glad."
    date: 2007-04-11
    body: "\"But having people compare it to nautilus or apple's finder has alerted me. To put it simply: I find those two to be utterly unusable.\"\n\nI agree completely. I have to say that this fad-like talk of \"usability\" at the moment is rather baffling. Every piece of software I've tried to use that claims to have \"good usability\" just seems to me to make assumptions about what I want to do with it - assumptions which are often wrong, and mean that I have to spend longer trying to find out-of-the-way options to achieve what I could do before much more easily.\n\nPlease don't go down this road."
    author: "Peter Lewis"
  - subject: "I read this article last night."
    date: 2007-04-09
    body: "As soon as I read the comparisons to Nautilus, I said to myself, \"Here we go again.\"  Dolphin meat must taste delicious to hungry trolls."
    author: "Louis"
  - subject: "Had a little test"
    date: 2007-04-09
    body: "I just was a little curious and installed dolphin with KDE 3.5 . In general I agree that although browser and filemanager in konqui are very powerfull, the usability of both components is not very good and it did not happen very much  concerning that in the past.\nNow seeing dolphin which seems very polished, but still lacks a lot of features, I am glad to see something is happening. But honestly I would rather see improvements in konqui than having a new and separate filemanager. Of course is will be now problem for me to change the default to konqui. But will konqui will be still see improvements in usability of the filemanager component and the interaction between filemanager/ browser if there is another default filemanager? I fear that wont be the case and that would be really a shame. So why not have an experimental branch of konqui for testing/ improving usability instead of a new filemanager? Then good thing would easier find their way back."
    author: "Mark"
  - subject: "Re: Had a little test"
    date: 2007-04-09
    body: "And of course Dolphin has changed a lot since the KDE 3.5 release. The KDE4 version is being actively developed with new features being added and improved all the time."
    author: "Matt Williams"
  - subject: "Re: Had a little test"
    date: 2007-04-10
    body: "I was gonna say...\n\nI just tried Dolphin in 3.5.6 again just now and...it makes me go \"Hmm...\"\n\nFor example, I put it in detailed mode and it has filename, size, and what appears to be mtime. I don't even have the <i>option</i> to sort things by mimetype (or other metadata- something I'd really like to see in the future).  I found that you can change the navigation back to a path, but it's wonky and changes its mind occasionally, and it doesn't have hover-preview at all, it seems.  I'll grant that it has potential (an easy button for split pane, for example is nice, though it doesn't let me split more than once and only vertcally).  I'm not overwhelmingly negative about this, but Dolphin 0.8.2-r1 definitely needs some time to bake."
    author: "Wyatt"
  - subject: "Re: Had a little test"
    date: 2007-04-10
    body: "Why wouldn't konqui see improvements brought about through work on dolphin?  Come on, this isn't the 1980s any more: we have shared libraries.  One of KDE's biggest strengths is its aggressive re-use of code between projects.  So all the bits the will be useful for konqui and dolphin (who knows, maybe other projects too) will eventually get moved into kdelibs.\nI was firmly in the konqui-only camp until I actually tried dolphin: I've tried dual-pane file managers before and hated most of them, but dolphin is different.  Sure, it's not exactly polished yet, but I find myself using it more and more for file management (including over network protocols). And I have to agree with the previous poster's comment that kparts within konqueror are just annoying.  Not only does it make it easy to close the window, forgetting that you're closing the file manager window not just a helper application, but it gets in the way of clicking on a file to open it, then clicking on another file to open that too - you can't click on the second file because your konqui window has been co-opted to display the first file you clicked on."
    author: "Adrian Baugh"
  - subject: "Re: Had a little test"
    date: 2007-04-10
    body: "heck, good bits from dolphin are even finding their way into the file dialog.. e.g. the sidebar speedbar/bookmarks area was just replaced with a common implementation from dolphin that was polished in beauty by kevin ottens and moved to kdelibs."
    author: "Aaron J. Seigo"
  - subject: "Reuse of Dolphin features in Konqueror"
    date: 2007-04-09
    body: "I'm using Konqueror for all of my non-konsole file management, and most of my web browsing.\n\nSometimes I'm missing features that mc has (like browsing into RPMs out of the box), and I also think that it could be better in split-window mode. E.g. you only see the path of the active window, not both at the same time.\n\nBut in total I like it very much. I'll give Dolphin a try, and the improvements I've heard of sound interesting. What I am wondering about though is whether it wouldn't be a good idea to integrate all the new Dolpin features into Konqueror, too. Why shouldn't Konqueror also use the new path feature with the drop-downs for quick navigation to subfolders?\n\nI don't like the whole concept of separate applications very much - the document- or data-centric approach, combined with \"browsing\" (i.e. keeping a back/forward history) makes much more sense in most cases, and I think that to a large extent KDE improved the situation with its very modular approach and direct links between apps (like being able to open apps in an editor directly from the preview). So I'm curious if Dolphin will be an improvement for me or not."
    author: "Joachim Werner"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-10
    body: "> wouldn't be a good idea to integrate all the \n> new Dolpin features into Konqueror, too.\n\nwe are integrating many of the features that makes sense given konqueror's reason for existing. we're not going to make konqi something it isn't, that'd be even worse than introducing a new app.\n\nhonestly, i'm surprised that the people concerned about dolphin don't realize that this allows us to keep konqueror for what it is as opposed to ruin it by turning it into something else.\n\n> Why shouldn't Konqueror also use the new path feature with\n> the drop-downs for quick navigation to subfolders?\n\nperhaps; it would need to default to the editable version, as the breadcrumbs really don't make much sense for browsing, particularly on the web.\n\n> So I'm curious if Dolphin will be an improvement for me or not.\n\nhonestly, it might not be. and that's the beauty of the situation: you can choose to use what works for you. the default is being picked with the greatest % of people in mind so that fewer people have to take action and configure things."
    author: "Aaron J. Seigo"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-10
    body: "\nBreadcrumb is a non-sense:\n\n- It cannot traverse directories with 711 permission.\nexample: /home can be 711 chmoded to prevent one user to see that another one mad a mistake. You are in /, how to you go to you home dir with breadcrumbe?\n- not easy to enter directories starting with a dot. (while with classical view, no problem.\n- users comming from windows are already familiar with classical path\n- if path is too long, how is it handleled?\n- if current directory has let say 150 directories, how do you traverse? Do you scroll the breadcrumb dropdown menu for hours until you find the entry?\n\nFor sure, it can be called \"usability\"!"
    author: "olahaye74"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-11
    body: "I like your comment a lot because it's constructive, so let me address it point by point (as I now use dolphin as my file manager in 3.5). Also, you should note that there is always the classic text box entry available in dolphin; a button switches between the two, and you can press ctrl+L to put focus on it.\n\nSpeaking of Dolphin specifically...\n\n711 permission:\nYour example doesn't work well, because the Bookmarks represent the root of the breadcrumb. To get to your home directory, you just choose it at the left hand of the widget.\n*You are correct that without the bookmarks in place it wouldn't work*\n\nHidden directories:\nTo access them with bread crumbs, you must show hidden files (Alt+.) or press Ctrl+L to show the text entry (which conveniently puts the cursor at the end of the line)\n\nUsers coming from windows familiarity:\nSkipping the whole debate of catering to windows users, when I first switched to linux, I wasn't familiar with linux's path system. That was enough to throw me off a bit at first. Anyway, I think this is your weakest argument.\n\nPath is too long:\nTo test this, I made a recursively linked home directory. It took 9 six(6) character directories to fill up my bar for my window size. After the bar was filled, each breadcrumb scaled, much like the tabs do in Opera 9 or Firefox 1.5. I got in more than 24 deep, and still found it usable. NB that the tooltip for each crumb shows that directory's name.\n\nMany directories in current directory:\nFirst, I'd hope you use the main browser window for that, as that functionality in the breadcumb just provides an alternate method. The way it works in Dolphin 0.8.2 isn't scrolling, but a large menu that adds columns as necessary. My 1600x1200 resolution, with 8pt font, gives me one column for every 59 directories (tested with 102 directories. I did find about a .5-.75s lag for the menu to come up.\n\nIn conclusion, dolphin has a pretty solid implementation of breadcrumbs, and allows you to bypass the weaknesses of the widget by having a quickly accessible text entry field.\n---\nWhile creating this post, I found some other problems/bugs. Note I'm using Dolphin 0.8.2 in KDE 3.5.5, so they may be fixed already.\n\n1. I cannot select the first directory in the popdown menu. The menu goes away, but dolphin does not navigate to the directory.\n2. I had to restart Dolphin for the bookmarks to update in the pull down on the breadcrumb widget, when I added a new one. Using the bookmarks in the side panel (which I don't normally use) worked fine.\n3. Some right click functionality would be nice. I think showing the dropdown menu without navigating to the directory first would be nice.\n"
    author: "Soap"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-11
    body: "Thanks for you comment, this is the 1st time I recieve positive feedback instead for \"garbage\".\n\nalthough, regarding argument, My whole impression is the following:\n\n- When when it's not possible / not easy with breadcrumbs, then you can use the classic way. (IMHO, this demonstrate pretty well the weakbness of the breadcrumb usability: some impossibilities without any (IMHO) real advantages over the classic method).\n\n- for Hidden directories, it's the same as for gtk apps: right click / show hidden files, but it's not obvious IMHO.\n\n- User comming from windows: maybe the weakest argument, but honestly, do you think breadcrumb is something that would reduce the feeling that unix is different from windows?\n\n- Path too long: IMHO, scaling is not the solution. when you go deep into diretory tree, the last directoy names are more important than the 1st ones. when you scale, even the current directory name is sqwized. Having the whole widget scroll would help (classic way does this, and IMHO, it's more user friendly.).\n\n- Many directories: using classic way allows autocompletion, while using breadcrumb requires your brain to find the directory and point to it. once again, IMHO, not a progress in usability...\n\nAs for the point you raised, I think these are not critical and will be sorted out soon if not already in kde 3.80\n\nPersonaly I'm realy disapointed by this move (Dolphin , ...) and this is the 1st time since I'm using KDE (I started with KDE 0.4 long ago if my memory is correct). Unfortunately developpers have the power to decide and apparently, there is no way to help them change their mind.\n\nI can't test KDE4, but Dolphin in kde 3.5.6 is not of any use to me: no way to split view in 3 pans: one for my blog preview, one for my ftp content upload to my blog and one for my local files to drag&drop to the ftp upload pan.\n\nOlivier.\n"
    author: "olahaye74"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-12
    body: "> I started with KDE 0.4 long ago if my memory is correct\nWell, I don't believe there ever was a KDE 0.4 :)  KDE 1.0 had alphas and betas, but no version numbers lower than 1.0 as far as I can recall... that was a long time ago already...\n\n/me recalls trying to get X working in 1997 or 98 to test a KDE 1.0 beta on Redhat only to find his video card was not supported by any X drivers..."
    author: "Troy Unrau"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-10
    body: "> perhaps; it would need to default to the editable version, as the breadcrumbs \n> really don't make much sense for browsing, particularly on the web.\n\nYes, default to the editable version, although this makes little difference if it is configurable.\n\nIt has nothing to do with browsing -- \"browsing\" your local file system is still browsing!  Breadcrumbs are usefull for some protocols and not for others.  Like other features and toolbar items, it can appear only on those protocols for which it is useful (HTTP probably isn't one of these, OTOH it would be useful for FTP [Konqueror *does* browse when using the FTP protocol]).\n\nBTW, isn't FTP a \"File Management\" function that should be included in Dolphin?"
    author: "James Richard Tyrer"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-10
    body: "> BTW, isn't FTP a \"File Management\" function that should be included in Dolphin?\n\nIt is included already. Dolphin uses the KIO slaves like Konqueror and hence can access all common protocols like ftp:/, home:/, file:/, system:/, media:/, remote:/, applications:/, sftp:/, fish:/ and smb:/\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-10
    body: "But not http:/ ?"
    author: "reihal"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-11
    body: "My bad. :-(  I don't know why my first attempt didn't work.\n\nHowever, it probably could use a few additional features to be a good FTP client.  \n\nIntegration with KGet.\n\nSome way to store a long list of commonly used FTP sites.\n\nWith those two features, I would probably use it as my FTP app.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-12
    body: "Aaron said:\n\"perhaps; it would need to default to the editable version, as the breadcrumbs really don't make much sense for browsing, particularly on the web.\"\n\nI have to disagree on this point.  I use the up arrow in Konqi all the time when browsing the web.  The up arrow is essentially \"breadcrumbs in a menu\".  For me, it is a killer feature.  So, I submit that breadcrumbs do make sense for web browsing, and I'm willing to give a bona-fide breadcrumb widget a try in Konqi.\n\nThat said, I'm dubious about dolphin, but I will reserve judgment until KDE4.  \"No wine before its time\" (Gallo, if memory serves)\n\nRegards,\n\nMark\n\n"
    author: "Mark Taff"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-10
    body: "\"I don't like the whole concept of separate applications very much - the document- or data-centric approach\"\n\nThe approach taken in Konqueror, although convenient in some ways, just hasn't worked out. The use cases for file browsing, web browsing and other forms of browsing are just too different to have one universal browser. Does a file manager need to be able to manage cookies?\n\nAlthough opening documents within Konqueror itself, without a new application opening, was sometimes convenient, it caused issues in other ways. Namely, Konqueror had to become even more of a universal browser for all different types of docouments."
    author: "Segedunum"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-10
    body: "The not being able to browse into RPM's should be relatively easy to fix. Provide a KIO slave that can do it, and Konqueror can behave just like it does for other archives now.\n"
    author: "Andr\u00e9"
  - subject: "Reuse of Dolphin features in Konqueror"
    date: 2007-04-09
    body: "I'm using Konqueror for all of my non-konsole file management, and most of my web browsing.\n\nSometimes I'm missing features that mc has (like browsing into RPMs out of the box), and I also think that it could be better in split-window mode. E.g. you only see the path of the active window, not both at the same time.\n\nBut in total I like it very much. I'll give Dolphin a try, and the improvements I've heard of sound interesting. What I am wondering about though is whether it wouldn't be a good idea to integrate all the new Dolpin features into Konqueror, too. Why shouldn't Konqueror also use the new path feature with the drop-downs for quick navigation to subfolders?\n\nI don't like the whole concept of separate applications very much - the document- or data-centric approach, combined with \"browsing\" (i.e. keeping a back/forward history) makes much more sense in most cases, and I think that to a large extent KDE improved the situation with its very modular approach and direct links between apps (like being able to open apps in an editor directly from the preview). So I'm curious if Dolphin will be an improvement for me or not."
    author: "Joachim Werner"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-09
    body: "\"What I am wondering about though is whether it wouldn't be a good idea to integrate all the new Dolpin features into Konqueror, too. Why shouldn't Konqueror also use the new path feature with the drop-downs for quick navigation to subfolders?\"\n\nI'm pretty sure that someone (Kevin Ottens?) very recently moved the \"breadcrumb\" widget somewhere where it is more universally accessible so that e.g. the Open/ Save file dialogs could use it, so there's no reason why not - massive code re-use is one of KDE's defining traits, after all :).  Konqueror's web/ file management duality probably complicates the direct use of this widget, though (which is partly why, of course, Dolphin exists: A *dedicated* file manager can be a *better* file manager because there are less concerns about whether the addition of a new feature will conflict with all of the other existing baggage), but I'm betting it's not something that can't be overcome :)\n\nThere is in fact some talk of a Dolphin KPart that could be re-used in Konqueror, but I have no idea what form this would take or what the visible effects would be."
    author: "SSJ"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-09
    body: "they share the same codebase !"
    author: "pietervandewyngaerde"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-10
    body: "But will Dolphin have a Krusader kpart?!"
    author: "agc"
  - subject: "Re: Reuse of Dolphin features in Konqueror"
    date: 2007-04-09
    body: "I believe there are plans to use Dolphin as a KPart in Konqueror for filebrowsing. This was you can keep all the nice features of Konqueror such as tabs and opening documents inline without code duplication."
    author: "Matt Williams"
  - subject: "Isn't it enough that Dolphin is already around?"
    date: 2007-04-09
    body: "If you're capable of installing KDE, then your also probably capable of chosing which file manager you use. So, how come Dolphin has to be the default? KDE users today have the choice to download Dolphin and use it if they want. I don't see why it has to affect Konqueror.\n\nWhat's next? A new \"clean\" (*sigh*) window manager? So \"power users\" can set Kwin as default by hand? :)\n\nI'm on a P4 and I don't really experience any speed differences between Konqueror and Dolphin. It's the interface that is different. I remember about a year back, when I first tried Dolphin. There were some good ideas, but nothing more at the time. Since then I can imagine lots of time have been spent to develop Dolphin. I'm not going to tell people what to do with their coding time but wouldn't it have been better to actually develop Konqueror in a desirable direction instead? (You know, KDE's killer application? :) )\n\nI'd like to see many of the features of Dolphin implemented in Konqueror. Better profile management for Konqueror is a must. So I hope that code can easily be shared between the two, as some KDE developers have claimed.\n\nI guess there's no real point in discussing this. The decision has already been made, but I think the KDE team was wrong to make this decision. It would have been much better if the different distros did this kind of thing. E.g. Kubuntu could set Dolphin as default filemanager and the rest us could set it as default if we *wanted*.\n\nThe last thing I want to say, is for the Konqueror developers. I often read forum posts with people claiming that it's wrong to combine file management and web browsing. I absolutely love that Konqueror is both a great file manager and a great web browser. Contrary to what certain \"usability nuts\" (*sigh*) claim file management and web browsing are closely related. You're doing a great job!"
    author: "mirshafie"
  - subject: "Re: Isn't it enough that Dolphin is already around?"
    date: 2007-04-10
    body: "> What's next? A new \"clean\" (*sigh*) window manager? So \"power \n> users\" can set Kwin as default by hand? :)\n\nunlikely to the point that i feel confident in saying: \"no\". the window manager features are easily ignored and almost never confronted by users for whom the features are too advanced / difficult to manage. the file manager, however, confronts these same people on a daily basis.\n\n> It would have been much better if the different distros did this kind of\n> thing. \n\nthey still can (e.g. by making konqueror the default if they wish). but this way we ensure a good quality file manager in dolphin as well as a good konqueror. which means we have better options for them to choose from and a default optimized for the majority of people (of which i'm not one of =)\n\n> E.g. Kubuntu could set Dolphin as default filemanager and the rest us could\n> set it as default if we *wanted*.\n\nthat's possible even with KDE upstream doing it. in fact, us doing it -ensures- that such configurability is possible. often (usually?) distros botch up some things and don't make it configurable at all. think about it.\n\n> Contrary to what certain \"usability nuts\"\n\nbetter them than those idiot users. oh, i'm sorry, that was probably offensive. my bad. maybe we should be more careful about how we talk about other people, particularly when we evidently don't know who they are. what do you think?\n\n> file management and web browsing are closely related\n\nthis is quite true for certain people. the point you are missing is that for most people they aren't remotely the same, and it has nothing to do with remote vs local, or http vs file:/// ... it's about management vs browsing.\n\nthere's been actual research and testing done behind a lot of this."
    author: "Aaron J. Seigo"
  - subject: "Re: Isn't it enough that Dolphin is already around?"
    date: 2007-04-10
    body: "I agree with you that it's great that KDE is getting more configurable. If people want it \"clean\" and without lots of features that they don't use, they should be able to make their desktop environment work that way. But I stand by my statement that the development should focus on making Konqueror more configurable. E.g. many of those features that make Dolphin easy to use should have been implemented in Konqueror instead.\n\nAnd for the record, I didn't actually mean that the people that make this kind of decisions are nuts. I usually speak that way with people that I meet IRL because they know what I really mean, but you're right that one should be careful with language on the internet.\n\nBtw I want to thank you for always taking the time to answer comments!"
    author: "mirshafie"
  - subject: "Re: Isn't it enough that Dolphin is already around?"
    date: 2007-04-10
    body: "Maybe power users will probably get to set Kmail as default by hand as well!\n_____\n\"it's about management vs browsing.\" - Aseigo\n\nIs it really?  How many people actually manage files?  I think fewer people do so nowadays as the trend is to let iTunes/iPhoto like apps organize your files instead of creating directory trees to hold stuff from different artists/albums whatever.  I just don't see a lot of people spending time moving files around a s opposed to looking at them or using them.  Of course, they'd be using iTunes/iPhoto like apps to do so, so I might have answered myself.  \n\n(responded to two people, wasn't sure where to put the response)\n"
    author: "Cliffton"
  - subject: "Bring on Dolphin"
    date: 2007-04-09
    body: "I accept everyone's arguments in favour of Konqueror, but the one thing that makes it for Dolphin for me is its speed. I just installed it under KDE 3 and it starts instantly, compared to that slight delay for Konqueror. I know that the time involved is not a lot, but given they both do the basic things I want done 90% of the time, the feeling of responsiveness from Dolphin is something Konqueror cannot match."
    author: "DJM"
  - subject: "Very important Remark"
    date: 2007-04-09
    body: "I am generally surprised by posts that include categoric point of view of the kind of \"I hate totally dolphin or I like it very much\", or also \"konqueror versus dolphin\" or also \"gnome versus kde\".\n we need to be more objective, and help the developers with our remarks concerning usability, suggestions for improvements and always keep in mind that the applications are made to satisfy a whole community and not a particular person.\n\nI think the categoric type of comments we generally read are the main cause why developers are now afraid of showing screenshots of work in progress. \n\n"
    author: "landolsi"
  - subject: "Re: Very important Remark"
    date: 2007-04-10
    body: "KDE rules, gnome sucks.\nvi rules, emacs sucks.\nLinux rules, Microsoft sucks.\nC rules, Fortran sucks.\nMULTICS rules, CP/M sucks.\n\nI don't see why a Dolphin / Konqueror flamewar should <i>surprise</i> you..."
    author: "Adrian Baugh"
  - subject: "Re: Very important Remark"
    date: 2007-04-10
    body: "BTW, WTF happened 2 KVim? It was a cool idea, shame it never went off :(."
    author: "slacker"
  - subject: "Re: Very important Remark"
    date: 2007-04-11
    body: "Have you seen the new version of Fortran?  It is a great improvement over C.\n\nYes, Fortran IV sucks but it is a bit out of date by now.\n\nWhere is the new version of C?"
    author: "James Richard Tyrer"
  - subject: "Re: Very important Remark"
    date: 2007-04-10
    body: "> I think the categoric type of comments we generally read are the main cause\n> why developers are now afraid of showing screenshots of work in progress.\n\nI feel the same way, I think it's holding developers off."
    author: "Diederik van der Boor"
  - subject: "You have to be kidding"
    date: 2007-04-09
    body: "Alright, I just had a look at Dolphin.  It is a decidedly mixed bag for me.\n\nPROS:\n- It is very minimalist.  \n- Some of the information that it conveys about files is definitely nice.\n\nCONS:\n- It is very minimalist.\n- Breadcrumbs.  Seriously folks, anyone with a filesystem of any complexity realizes that breadcrumbs are the antithesis of productivity when you want to get a quick grasp of information that might be spread over a number of folders.  Fine for web apps, breadcrumbs are a miserable solution in a file browsing application.\n\nI guess the bottom line is that Dolphin doesn't offer anything compelling to a user like myself.  It is simplistic and feels light weight, but I could run circles around it with the technology I've been using for years.  Even without all of the bickering about whether a file browser should be able to be a web browser as well (I don't know why people have trouble with this), Konqueror is the more useful tool to such a degree that Dolphin can't even stand in its shadow.\n\nRegards,\n\n\nSteve"
    author: "SteveT"
  - subject: "Re: You have to be kidding"
    date: 2007-04-09
    body: "I think the same was said regarding the Norton Commander philosophy. I was very sceptical and thought nothing could beat this philosophy. Now, you use the same arguments pro Konqueror as a file manager I used pro Krusader.\n\nKonqueror compares to Mozilla, Dolphin to Firefox. \n\nWhen things get overcomplex you need some refactoring. \n\nAnd Konqueror will be shipped with KDE 4. So what is the problem.\n\nToday I would say: Dolphin  + Console."
    author: "bert"
  - subject: "Re: You have to be kidding"
    date: 2007-04-10
    body: "I think you meant:\n\nKonqueror compares to Mozilla SeaMonkey, Dolphin to Mozilla Firefox :-D"
    author: "Patcito"
  - subject: "Re: You have to be kidding"
    date: 2007-04-10
    body: "Seamonkey is a later naming scheme for the mozilla webbrowser\n\n\n"
    author: "bert"
  - subject: "Re: You have to be kidding"
    date: 2007-04-10
    body: "it's been years already since they decided to call it seamonkey, the mozilla webbrowser doesn't mean anything today. It's either firefox or seamonkey etc..."
    author: "Patcito"
  - subject: "Not hard"
    date: 2007-04-10
    body: "like 90% of the people use windows\ni don't think people have big problem to use a software who do web browsing and  file manager... IE do \n\nlike we can see on this web page, there are many GUI in kde and windows who are similar\nhttp://www.laboiteaprog.com/article-kde_vs_windows-85-5\n"
    author: "Marc Collin"
  - subject: "Re: Not hard"
    date: 2007-04-10
    body: "just because the apps are both called \"explorer\" doesn't make them the same UI. windows explorer and internet explorer behave rather differently."
    author: "Aaron J. Seigo"
  - subject: "Re: Not hard"
    date: 2007-04-10
    body: "...and that's one point that I have to (painfully) admit that M$ did better than KDE..."
    author: "slacker"
  - subject: "Re: Not hard"
    date: 2007-04-10
    body: "Sorry Aaron, I guess you have a long time without touching a Windows box.\n\nSimple test:\n1. Open a \"Windows Explorer\" window.\n2. Type http://www.kde.org in the addressbar.\n3. Voil\u00e0! The KDE home page appears where the file icons were. No new window. No different menus.\n\nKDE2 architects took a thing Microsoft did mostly as a excuse to crush Netscape, and make a central part of the vision of their design. Konqueror, while not a great browser at that time, was terrific in its concept of universal browser, and most old school KDE users learned to love that concept.\n\nI really don't see that killing such a brilliant idea with things that looks like the things we had before Konqui is an advancement, but jumping back to 1999. \n\n"
    author: "Shulai"
  - subject: "Re: Not hard"
    date: 2007-04-10
    body: "> I really don't see that killing such a brilliant idea\n\nkilling such a brilliant idea would be not to ship konqueror. that's not what's happening.\n\nso i'm not sure what you and others are moaning about.\n\nas for making universal components part of the central vision of things, that hasn't changed either."
    author: "Aaron J. Seigo"
  - subject: "Re: Not hard"
    date: 2007-04-10
    body: "when did you do this the last time?\n\nthe toolbar icons and the contents of the menus changes drastically. and thats just for windows xp and older. in vista its even more different...\n\n\nso why should you write a software that can morph into a completely different app with a completely different usecase.\n\n\nwhen i use a filemanager i want to kopy or rename files. i want to organize stuff. i never do this while browsing the web...."
    author: "ac"
  - subject: "Re: Not hard"
    date: 2007-04-10
    body: "> when i use a filemanager i want to kopy or rename files.\n> i want to organize stuff. i never do this while browsing the web....\n\nYes, those are really completely different.\nFilemanagement:\n* Copy/move/delete files/directories\n* New directory\n* Open files for viewing the content (external or embedded)\n* Show preview of files\n* Show metadata of files\n\nWebbrowsing:\n* Following links\n* HTML-renderer\n* Javascript\n* Flashplayer\n* Editing web-forms\n* Open files for viewing the content? (external or embedded) This is used rather rarely\n\nOn the other side, one may ask why konqueror doesn't do a punch of ofther stuff?\nWhy not manage/browse music? Who needs amarok anyway...\nWhy not manage/browse photos? Why is there digiKam or kphotoalbum developed?\nWhy is there any other application, when konqueror can be morphed to anything other?\n\nI honestly don't know why some people think webbrowsing and filemanagement is similar. Even in times of \"Web 1.0\" they where different, \"web 2.0\" makes them completely different.\n"
    author: "Birdy"
  - subject: "Re: Not hard"
    date: 2007-04-10
    body: "you're right, but on the other hand, konqi CAN be used to listen music and view pictures. And I'd love to keep it that way.\n\nI often mix webbrowsing and local filebrowsing: go to sourceforge, download a zip (klik open) and extract the files (yeah, embedded view). Ctrl-T (or even split-screen and drag'n'drop), ctrl-Home -> goto Downloads, manage the files. I wouldn't like to have to open a seperate app for this last step. Same with pictures and music, I manage & preview them in konqi. But I have no problem having a seperate pic viewer, nor a seperate music player, nor a seperate filebrowser. I'd even use them, as long as konqi doesn't lose functionality.\n\nSo, like Aaron, I don't understand this flamewar. There will be a new app, a filebrowser in KDE 4. As it shares (this is, after all, KDE) a lot of code with konqi, konqi will get better as result, AND it will be able to focus itself a bit more on being a BROWSER instead of a manager (but browsing both files, embedded docs and webpages). It'll only get better."
    author: "superstoned"
  - subject: "Re: Not hard"
    date: 2007-04-10
    body: "When did you touch a Windows box last time? ;-)\n\nThis is correct for Windows XP. But no longer for Windows Vista (as far as I know - I still didn't receive my upgrade yet).\nI just did what you said. And the result is that FileExplorer changes into InternetExplorer 6. But I do have IE7 installed!\n\nSo Microsoft already did what KDE is going to do. Splitting file-management and web-browsing."
    author: "Birdy"
  - subject: "Golphin again??..."
    date: 2007-04-10
    body: "I'm just wondering where the 'UI' [G/K]nights will take KDE to?\nto the 'G'DE perhaps.. or maybe they'll even surpass the 'gnumerish'\nnoncense.. :( \n"
    author: "A_KDE_User"
  - subject: "Re: Golphin again??..."
    date: 2007-04-10
    body: "i've already said it once in this thread, but i think it bears repeating: where do you get off judging us (kde) by the efforts of others (gnome) who are in this case unrelated to us?\n\nyou perceive them to have failed in this regard; fair enough. that's completely your prerogative (i'll keep my opinions on that to myself here =) ... but try and see what we are doing for what we are actually doing rather than through the tinted lenses of someone else's efforts.\n\nthank you."
    author: "Aaron J. Seigo"
  - subject: "Re: Golphin again??..."
    date: 2007-04-10
    body: "In my earlier, more sarcastic, post, I noted that the reviewer in the story made direct comparisons to Nautilus.  I know that's out of the control of the developers, and comparisons are inevitable when features are similar.  But such comparisons have the unfortunate effect of bringing out all the Gnome-haters to, once again, cry that Dolphin is somehow Gnome-ifying KDE.  I wouldn't let it bother me too much if I were you.  For every one making noise there are dozens withholding judgement for the actual finished product, and some that offer constructive suggestions."
    author: "Louis"
  - subject: "Re: Golphin again??..."
    date: 2007-04-10
    body: "And the funny part: Many here complain Nautilus being a bad filemanager. But no one could tell me what Nautilus is doing wrong.\nI guess the only answer is: \"it's GNOME\" ;-)\n"
    author: "Birdy"
  - subject: "Re: Golphin again??..."
    date: 2007-04-10
    body: "well, Nautilus is (or maybe was, didn't try it lately) a little too barebones for most of us. Compared to Nautilus, Explorer was/is a powerfull filemanager, so imagine the difference between Konqueror and Nautilus. I guess ppl where afraid KDE would loose konqi, the powerfull filemanager, and go for something dumbed down. I can see that, but it has been stated over and over and over and over again: Konqi won't go anywhere but in KDEbase. So the flamers really don't know how to read..."
    author: "superstoned"
  - subject: "Re: Golphin again??..."
    date: 2007-04-10
    body: "Funny how having redundant apps is such a bad thing when it's text editors.  Yeah Konqueror isn't going anywhere, but now there are two file browsers.  This is somehow good now.  People who apt-get kde instead of picking and choosing parts are going to have one more redundant app to complain about.\n\nLeaving that aside, Dolphin was created in response to problems with Konqueror.  By creating Dolphin, they've ensured the problems in Konqueror will never be fixed *in Konqueror*.  To then turn around and tell us we can continue using Konqueror if we don't like Dolphin?  Thanks for nothing.  We have the choice of putting up wioth the same old problems that we can guarantee will now never be fixed, or using an app we don't like."
    author: "Cliffton"
  - subject: "Re: Golphin again??..."
    date: 2007-04-10
    body: "> Funny how having redundant apps is such a bad thing when it's text editors.\n\nThat situation was somewhat different - KEdit was around purely because the editing component used in KWrite and Kate didn't support bi-directional text. That's now fixed in KDE4, and thus KEdit is gone. Kate, the editor with more multi-file workspace type features, was moved to kdesdk (IIRC) since it's more of a tool for software developers, thus moving it out of the way of general users.\n\n> Leaving that aside, Dolphin was created in response to problems with Konqueror. \n\nActually, a specific problem, which is new users are uncomfortable with it. As Aaron has mentioned several times it was a response to *actual user testing* and research. They aren't just doing this on a whim.\n\n> By creating Dolphin, they've ensured the problems in Konqueror will never be \n> fixed *in Konqueror*.\n\nRubbish. Seeing as Konqueror and Dolphin share a lot of code there are going to be improvements that affect both applications, so that's patently false.\n\n> To then turn around and tell us we can continue using Konqueror if we don't \n> like Dolphin? Thanks for nothing. \n\nThey're giving you a choice. Use more or less what you currently use, or try out the new thing. Hell, you can even do both at once. How can that be bad?\n\n> We have the choice of putting up wioth the same old problems that we can \n> guarantee will now never be fixed, or using an app we don't like.\n\nHang on a second, do you actually like Konqueror or not? Perhaps you are just trolling. With all the vitriol in your post it's hard to tell."
    author: "Paul Eggleton"
  - subject: "?????"
    date: 2007-04-10
    body: "I guess that I just don't get it.  Is there anything in Dolphin that isn't just the KFM part in Konqueror with a lot of features removed?\n\nThe answer is probably yes.  If these are useful features:\n\n  The bookmark and information sidebar.\n\n  Preview mode. \n\nBut I still don't get it.  Does removing features and reducing usability really represent an improvement?\n\nAlso, this is going to further confuse the issue of what is a browser vs. what is a file manager since Dolphin is a stand alone file manager that -- at first glance -- looks like a file manager running IN Konqueror."
    author: "James Richard Tyrer"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "Geez. Who said about removing features; Dolphin wil just lose the ability of a web browser, but as a file manager it isn't just Konqueror Light. "
    author: "Lans"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "I see a lot of missing features -- lots of stuff missing from the menus.  \n \nWasn't that the point?  Less features that \"confuse\" users. :-(\n\nAs I have said many times, there is no such thing as a web (only) browser.  What Dolphin doesn't have is the ability to function as a browser -- it won't open files as a browser does -- you have to open stuff in a separate app which means that there is no way to view some files (except to open a browser!!).\n\nIt would be nice to be able to set the background.  I am going snow-blind just trying it."
    author: "James Richard Tyrer"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "> Wasn't that the point? Less features that \"confuse\" users. :-(\n\nNo, \"less features\" never was a goal of Dolphin. Please just check the following links for a little bit more background information:\nhttp://enzosworld.gmxhome.de\nhttp://aseigo.blogspot.com/2006/12/on-oxygen-on-dolphin.html\nhttp://aseigo.blogspot.com/2007/02/konqueror-not-vanishing-news-at-11.html\nhttp://aseigo.blogspot.com/2007/03/dolphin-gets-treeview-krunner-gets.html\n\n> It would be nice to be able to set the background. I am going snow-blind just trying it.\n\nWe'll do our best to prevent you from getting snow-blind and might add the capability to set a background. Hope you did not get blind already by reading this reply because of the white background of the dot ;-)\n\nCheers,\nPeter\n\n"
    author: "ppenz"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "> Hope you did not get blind already by reading this reply because of the white \n> background of the dot ;-)\n\nYes, the background is White, but the text is NOT Black (it is 333333)."
    author: "James Richard Tyrer"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "Also. please remember that Dolphin isn't complete, features will be added.\nPeople complained that Dolphin didn't have tree view - well, read Peter's last link.\n\nI think one of the factors to the flamewars is this misunderstanding. \"Oh no, less features to confuse user - we don't want another Gnome\". Or something along these lines.\n\nI still prefer Konqueror, but have tried Dolphin under KDE 3.5.x. It's nice, and seeing screenshots from the development versions, I can just say that it's getting better and better (no surprise, eh?). \n\nI think the devs deserve much more than all the rant om the dot; if you don't like something, at least try to come with constructive criticism."
    author: "Lans"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "> if you don't like something, at least try to come [up] with constructive \n> criticism.\n\nI said that I didn't think that removing features was an improvement.  That was a constructive criticism.\n\nGiven that Dolphin will only support file management without browsing and won't have tabs, why does it improve it to remove relevant Konqueror features?"
    author: "James Richard Tyrer"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "> I said that I didn't think that removing features was an improvement. \n> That was a constructive criticism.\n\nWell, it would start being one if you said which features that should be in a file manager have been removed..."
    author: "ac"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "Sorry if I was unclear. That wasn't directed at you, I am just tired of the Konqueror vs Dolphin flamewars.\n\nIt would also be good if dissatisfied persons tell others what they miss, not only \"Dolphin lacks so many features\". You mentioned one thing - tabs - and who knows, maybe they will be implanted."
    author: "Lans"
  - subject: "Re: ?????"
    date: 2007-04-11
    body: "> I am just tired of the Konqueror vs Dolphin flamewars.\n\nI agree.  The whole \"Dolphin is garbage\" thread is useless.  What we need is objective statements of what people like and what they don't like.  Even if you don't like an app, you can offer an objective analysis of it.\n\nI must admit that \"lacks many features\" was my first impression and a bit subjective.  I will e-mail Peter with my actual thoughts on this since it will be a list of minor features with comments.\n\nOne thing that I really missed is: \"Folders First\".  Sometimes I want to turn this off.  Unix types may miss the two types of alphabetical sorts."
    author: "James Richard Tyrer"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "\"I see a lot of missing features -- lots of stuff missing from the menus.\"\n\nGood, because quite honestly, menus in Konqueror are an utter mess. I mean, really. There are too many menus, and those menus contain way too much stuff. Also, UI-wise, Konqueror is not pretty. It's functional, but it's not pretty. Maybe it could have been fixed, but I haven't seen it get fixed in several years, so maybe it's better to start over from scratch?\n\nBy \"not pretty\" I mean stuff like borders and lines everywhere, toolbars, sidebars, humungous context-menus etc. etc. Some improvements have been made (thank you AJS!), but a lot remains to be done. and, for some reason, they do not get done.\n\nBut hey, don't like Dolphin? Then don't use it. The point is that users are presented by an simple app by default, The newbies can handle that app just fine, and so can power-users. But if power-users want something more powerul, they can alwys switch to Konqueror. Why should we present an overtly complex app to users, and the expect newbies to either\n\na) learn to cope with Konqueror\nb) figure out how to switch to Dolphin\n\nPower-users can manage things like A and B (well, replace \"Dolphin\" with \"Konqueror\" in B), newbies cannot. Why do you want to make newbies suffer for the sake of your own comfort? Especially since you could get that comfort back through 10 seconds of work? Why make newbies do all the work, while powerusers can just sit on their laurels?"
    author: "Janne"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "+1"
    author: "superstoned"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "> Good, because quite honestly, menus in Konqueror are an utter mess.\n\nMy comment was only about the menus on the MenuBar.  If the items were removed from those menus, then the feature was removed.\n\n> By \"not pretty\" I mean stuff like borders and lines everywhere, toolbars,\n> sidebars ...\n\nActually, you can reduce Konqueror to the same ToolBars as Dolphin and then it looks almost identical except for the MenuBar.\n\n> But hey, don't like Dolphin? \n\nThis isn't about not liking Dolphin.  I didn't say that I don't like it.  What I said was that I didn't think that removing features from Konqueror was a great improvement.\n\nThat said, I agree that there are things in Konqueror that need improvement and I have suggested several improvements.  I notice that something very similar to one of my suggestions is a feature of Dolphin.  Why can't Konqueror be improved?  What I ran into was an irrational reactionary response to my suggestion.  This is unfortunate since this problem doesn't just apply to Konqueror.  This mindset is the antithesis of good software engineering. \n\nI see some good new features in Dolphin that should be included in Konqueror."
    author: "James Richard Tyrer"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "And now they never will be since they are in Dolphin.  And people wonder why Konqueror users don't like the idea of Dolphin?\n\nIn response to people saying Dolphin lacked critical stuff (tree view) some of it is added, bringing it closer to Konqueror.  Konqueror can be simplified, bringing it closer to Dolphin.  Why aren't people aiming for something in the middle instead of having two filebrowsers, one too complex and the other two simple?"
    author: "Cliffton"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "Well, features will be added, thus making it aim for the middle position? \nAnd \"simple\" doesn't have to mean less (file manager) features."
    author: "Lans"
  - subject: "Re: ?????"
    date: 2007-04-11
    body: "why KDE doesn't go for the middle? because that ensures nobody is happy. Konqi-middle will lack features for powerusers, and still be confusing for average users. Now we will have a dedicated filemanager (Dolphin) and a (web) browser (Konqueror) which also happens to be able to browse filesystems, documents, music etcetera (like it does now).\n\nThat's the difference between Dolphin and Konqi: Dolphin is a Manager, Konqi a Browser. Both can do file, but Konqi, as a browser, can also browse documents (embedded Okular), pictures (Gwenview), websites (Khtml), Video's (kaffeine, Kmplayer) etcetera."
    author: "superstoned"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "\"My comment was only about the menus on the MenuBar.\"\n\nAnd my comment was about the menus in the menubar. They are a total disaster.\n\n\"If the items were removed from those menus, then the feature was removed.\"\n\nIn that case Konqueror is filled with next to useless features that do nothing but make the UI a complete and utter mess. What if we don't talk about features? I'm not at a Linux-machine right now so I can't test it, but IIRC the \"Settings\"-menu in Konqueror is a total mess as well. There were entries titled \"Configure Konqueror\", \"Configure Shortcuts\", \"Configure Shortcuts\", \"Configure View Profiles\" etc. In short: A confusing mess. Instead of that disaster, there should just be \"Configure Konqueror\".\n\n\"Actually, you can reduce Konqueror to the same ToolBars as Dolphin and then it looks almost identical except for the MenuBar.\"\n\nI'm not talking about just the toolbar. I'm talking about frames around different views and such.\n\n\"Why can't Konqueror be improved?\"\n\nWho says it coudn't be? But fact is that Konqueror is a certain type of app. And no matter how much it's tweaked, it wont change the basic premise of the app. If it did, then it wouldn't be Konqueror anymore. Many people want a straightforward and easy app to use. Instead of trying to make Konqueror fit both roles, we might as well create two separate apps. Having a \"Simple Konqueror\" and \"Advanced Konqueror\" would just make things even more confusing. And if those two modes of one app would have different menubars (simple menubar and complex menubar), different toolbars (simple and advanced), different layouts, different context-menu's... why not simple create two separate apps? If we didn't, we would just confuse people. It's like changing settings with Windows XP. Have you tried talking someone through that on a phone? You are looking at the \"Classic View\" on your computer, and the other person is using the \"Category View\", and it all gets very confusing very fast."
    author: "Janne"
  - subject: "Re: ?????"
    date: 2007-04-11
    body: "You're so right. cleaning up konqi would make it less attractive to many users, for many usecases - and why would we want that?"
    author: "superstoned"
  - subject: "Re: ?????"
    date: 2007-04-11
    body: "\"Why do you want to make newbies suffer for the sake of your own comfort?\"\n\nWell, I don't really think that it's \"suffering\", as you put it, but the real answer is that because KDE (like any free software project) is for its users and developers. We're not in the business of trying to sell things to people because they represent a larger target market than our current user-base (whom we can therefore afford to lose).\n\nYou don't make all your friends from the Horror Movie Club go to watch Disney because you might make new friends.\n\n"
    author: "Peter Lewis"
  - subject: "Re: ?????"
    date: 2007-04-11
    body: "\"Well, I don't really think that it's \"suffering\",\"\n\nYes it is. The system is unsuitable for them, and they are required to do something that they do not want to do\n\n\"but the real answer is that because KDE (like any free software project) is for its users and developers.\"\n\nNewbies are users as well. People with limited computer-skills are users as well. And the point is that power-users CAN change things in the desktop to suit them, newbies cannot. I can change the windecs to suit my needs, I can arrange the taskbar till I drop, I can tweak the window-specific settings to my hearts content. My wife can't. She just wants to use the desktop, and she wants it to be idiot-proof, straightforward and pretty. She's not dumb by any stretch of the imagination, she just doesn't have the time, skills or the interest to go around tweaking her desktop. Powerusers (like myself) actually enjoy doing that. So why do we have this the other way around? We cater to the powerusers so they wouldn't have to change things (and yet they do), while making the system so complex that either we force newbies to learn the ins and outs of the system or we expect them to change things to suit them. And they have no interest in doing either of those things.\n\nMany moons ago I wrote a text about this issue. By default, the desktop should be as easy, clean and straightforward as possible. For newbies, that kind of system would be ideal. Their needs are not as extensive as powerusers needs are. they would feel comfortable with such a system since it wouldn't overload them with it's complexity and power. What about powerusers? They could change that simple foundation to suit their needs. They are comfortable at doing that, and they like changing things. This way both groups would be happy. But as things are right now, we are trying to make the powerusers happy (and failing at it somewhat, since they still tweak things to their liking), while throwing newbies to the wolves.\n\nIn short: why do we insist on taking advantage of the weakness of the newbies (their lack of skills/interest in changing the system), instead of trying to cater to that weakness, while taking advantage of the strength of powerusers (their willingness to tweak the system)? Why do we insist that newbies must adapt to overtly complex system, instead of simply providing them with an easy and simple system that they can actually use comfortably, while making it possible for powerusers to change that system to suit their more advanced needs?\n\nIf KDE is primarily meant for it's developers, then we can just drop this crap about KDE being \"easy to use\". Developers are powerusers, their wants and needs are not similar to regural users. Fact is that if we want more people to use free software and free operating systems, we need to drop this crap about \"free software is primarily for it's developers\". With that way of thinking we will never make Joe Sixpack see the light on this issue."
    author: "Janne"
  - subject: "Re: ?????"
    date: 2007-04-10
    body: "I've tried Dolphin, and at first I thought \"I've set Konqueror to work exactly how I want it, I'll never use Dolphin very much\". \n\nI ended with using Dolphin exclusively for files, because it's simply better to use. \n\nIt is mostly a different GUI to functionality from Konqueror, and they use the same backends, so it isn't competition to Konqueror, but rather a complement, which is better to use for some people (like me). \n\nOther users don't lose functionality, and everyone cand ecide not to go with the decision of the KDE team (and distros can do the same), but I think the KDE Team is right in choosing Dolphin as default File-Manager. \n\nAnd in my opinion, the KDE team should choose defaults themselves. Distros can still change them, but they shouldn't have to change them to get teh system which the KDE team thinks best. "
    author: "Arne Babenhauserheide"
  - subject: "dolphin rocks"
    date: 2007-04-10
    body: "i consider myself a poweruser and have never really liked konqueror (the file-manager): the easy stuff is hard, while the hard stuff is easier in the console.\n\ni for one, welcome our new animal overlords.\n\ngo PP, AS and the rest of the bunch."
    author: "anonymous"
  - subject: "Dolphin"
    date: 2007-04-10
    body: "Well, usually I tend to love new applications and like to try them out first-hand. New applications that make everything simpler and better? Great!\nSo, recently, I tried out Dolphin instead of Konqueror. But I found that it doesnt really live up to the hype. It's interesting nevertheless because now I know what I really miss about Konq and why it's generally a good idea to accept half a second more start-up time to have a decent application. The only thing I really missed about Konq all the time was separate bookmarks for filemanagement and web-browsing. Dolphin gives me that but brings LOTS of bad things with it at the same time. The #1 issue: You cannot click right next to a file in order to select it without opening it. So no more click+F2 to rename, no more click+Del to delete, no more click+Ctrl+C to copy and so on. Virtually all keyboard commands become worthless. The only option is to right click and then close the context menu with Esc. Even more annoying: When you try to insert a file by pressing Ctrl+V and sth. happens to be already selected Dolphin always wants to overwrite the selected file instead. So right now, I cant really see what advantage all this brings. There might be some one day but I fear right now that really needed improvements for Konq just wont happen because all the major development goes into Dolphin. So Konq stays with its problems forever and Dolphin just isnt good enough because it has lots of bugs, missing functionality, etc. It's like KDE 1.0 all over again.\n\n\n\n\n\n"
    author: "Michael"
  - subject: "Re: Dolphin"
    date: 2007-04-10
    body: "Konqi will partly profit from the work on Dolphin, as they share a lot of code. Second, I can imagine Dolphin will get some improvements in the following months. And last, Konq can't really be improved in the filebrowsing area if you don't want to touch the webbrowsing part. A big part of usability is, after all, not having useless stuff in the interface. Konqi already has a interface which changes all the time, and the only way to improve one part is to change the interface even more, OR remove stuff from another part. Both things would suck. For many powerusers (like me, and probably you) the overloaded interface in konqi isn't much of a problem, and we love the features. But for some, the features just make it harder to use (and they don't use those features anyway). The dev's try to keep us all happy by introducing dolphin..."
    author: "superstoned"
  - subject: "Dockable Dolphin"
    date: 2007-04-10
    body: "I would like to see a dockable Dolphin which can be shown and hidden with a simple shortcut (e.g. F11), similarly to Yakuake (which is a dockable konsole).  Then, no matter in which desktop you are working and no matter how many windows are open, you press F11 and a file management program pops up. This would be much easier than first closing all windows and pressing that \"Home\" icon on the desktop or selecting \"Home\" in the System menu on the kicker. Also it would avoid to have to search the file manager in your Alt-Tab list or on the taskbar when it loses the focus: whenever you want the file manager back in front, just press F11."
    author: "Vlad2"
  - subject: "Re: Dockable Dolphin"
    date: 2007-04-10
    body: "++"
    author: "anon"
  - subject: "Re: Dockable Dolphin"
    date: 2007-04-10
    body: "Err... Sounds like something you can easily do with a little DCOP magic and configuration. Binding F11 to show and hide a Konq window should be easy enough. Still, there are already easy ways to get yourself a file manager. I for one just click the button next to the start button."
    author: "Andr\u00e9"
  - subject: "Re: Dockable Dolphin"
    date: 2007-04-10
    body: "There were some talk about a general application based on Yakuake for this. An application that works like a container for other apps. That would be super sweet. :)\n\nI wouldn't use it for file management though. I've always got a system:/ tab open in Konqueror, and Konqi is also the only application that shows up in my taskbar except dialogs. I've set the rest to skip the taskbar and show up in the systray. It works very good."
    author: "mirshafie"
  - subject: "Open File Manager on desktop-click/drag ?"
    date: 2007-04-10
    body: "I was wondering if in KDE4, it may be possible to click on an empty area of the desktop and the default file manager shows up? Or draw a rectangle with your mouse on an empty space, and the file manager launches with the dimensions of that rectangle.\n\nOr you draw you click your left mouse and draw a line and depending on the lenth of the line and perhaps the shape, a a file manager window shows up.\n\nFor example: \n- the shape \\ (upper left->bottom right) starts drawing the window in the upper left corner, growing while you keep dragging (making the virtual line 'longer')\n- the shape / (bottom left->upper right) starts drawing the window in the bottom left corner, growing while you keep dragging.\n- etc. for the other 2 directions\n- if you draw a circle/spiral, the window starts centered and grows while you keep drawing the circle/spiral\n\nThis has the advantage that you do not need a lot of empty space in order to control your windows precisely. The disadvantage is, that perhaps it's more difficult to perform for some people (especially RSI etc I guess).\n"
    author: "Darkelve"
  - subject: "Re: Open File Manager on desktop-click/drag ?"
    date: 2007-04-10
    body: "Those are features of Directory Opus, something that is slightly amiga-related. Hence KDE will never support it. Just like multiselect in menues. I also would like another DOpus feature, being able to mark out rectangles on the desktop where I want certain types of icons to be grouped. Cancelling drag&drop actions from mouse would also be nice (now I have to hit esc - why not also rmb). SuperKaramba \"applets\" should be in the same z-level as desktop icons (so that icons dont vanish under them) - just like app-icons on amiga, so I guess it wont happen.\n\nIn general, KDE wont support anything new, unless Windows does it first, so that one can bring up the \"most users\" argument."
    author: "kolla"
  - subject: "Re: Open File Manager on desktop-click/drag ?"
    date: 2007-04-11
    body: "That is, unless you write it for inlcusion in kde.\n"
    author: "whatever noticed"
  - subject: "Re: Open File Manager on desktop-click/drag ?"
    date: 2007-04-11
    body: "\"That is, unless you write it for inlcusion in kde.\"\n\nThe most advanced application I ever programmed is a Hello World program in C.\n\nWell actually, it was a Carmen San Diego clone in Q-basic and so chock-full of spaghetti code that I ended up not understanding what much of the code did myself. \n\nBut hey, it worked and it even had a bit of sound and graphics! :p"
    author: "Darkelve"
  - subject: "Off-topic: Currently open files and directories"
    date: 2007-04-10
    body: "Does anyone know of a kernel function or something that returns a list of all files a process has opened since it has been started?\n\nAFAIK, the Linux lsof command displays the files that are currently open, but most programs close files once they have read them.\n\nBut if it would be possible to retrieve all files that has been read by a process it would be possible to make a Dolphin/Konqueror/... sidebar that displays a list of files that are open (or have been open recently), or highlighting those files and maybe even \"directories\" (if Konqueror/Dolphin sessions can be \"seen\"), when the list is filtered for those files that are assigned to the application (for kword only kword-compatible files are shown but not the configuration files etc)\n\nIt would be something like a system-wide \"open files\" list like in e.g. Kate (that works for Kate files only)\n\nHm, writing that, it may be hard to activate the right window when you have the pid only, especially with MD-Interfaces. Well, maybe someone has an idea to get around this...\n"
    author: "MM"
  - subject: "Re: Off-topic: Currently open files and directorie"
    date: 2007-04-10
    body: "Like recent documents? Yes, it's desktop specific, but I don't think the kernel provides something like what you're looking for. To be more precise, I'm pretty sure the kernel doesn't. Any idea how big such a list could become, over time? The kernel dev's aren't really happy to include everything and the kitchensink, you know, they're quite the purists ;-)"
    author: "superstoned"
  - subject: "Re: Off-topic: Currently open files and directorie"
    date: 2007-04-10
    body: "I get your point regarding the size of the list. Maybe it would be more useful as an option, with some configuration (file name patterns to be stored). But you are right, I have not heard of such a thing, except that some security solutions (SELinux?) provide access control lists by process, but I don't know if they have some kind of logging.\n\nIMHO, the problem with recent documents is not that it's desktop specific, but iirc it only works when opening files from the desktop or konqueror and does not work when a document is dragged and dropped from konqueror to an already open instance of e.g. kword. There's also only the information that the document has been opened, but not, if it has been closed (well, that problem would persist with a kernel function if the document is just closed but the app not).\n"
    author: "MM"
  - subject: "Will Dolphin allow you to set a default view?"
    date: 2007-04-10
    body: "If Dolphin will allow me to set the default view to detail view, then I'm ready to switch now.\n\nI like Konqueror for file management a lot, but this one thing drives me nuts. I hate opening a new tab, and then having to switch the view mode.\n\nArawak"
    author: "Arawak"
  - subject: "Re: Will Dolphin allow you to set a default view?"
    date: 2007-04-10
    body: "Yes, it's possible to set the default view in Dolphin. For the KDE 3 version of Dolphin select Settings -> Configure Dolphin... and select the default view mode under the 'General' settings.\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "All those files..."
    date: 2007-04-10
    body: "I can't, for the life of me, understand what people want with all this file management that makes a new, simpler, more specialised file manager neccessary.\nThe only file management I do is usually done with a few clicks: opening a window here, another window (or a new pane) there, mark a few files here and drag them there. Heck, even my wife (who, at 57, is hard to learn any new tricks), has gotten good at this!\n\nBut then, we don't do much file handling. I have my little music collection with hardly 21,000 songs, and a diminutive picture collection of less than 10,000 pics. And, of course, a few thousand text and pdf documents that we've collected during the nearly 20 years we've used computers at home (I am, sadly, one of those people who can't throw anything away).\n\nI've always thought that Konqueror was eminently capable of handling this - besides being a fairly good browser. But now it seems I've been wrong the whole time, and that one actually needs ANOTHER, more specialised, tool. \nI guess I could handle that, if you give me a few years to getting used to it. But my wife - who supposedly is one of the poor user who supposedly needs this new, specialised tool even more than me, is crying already...\n"
    author: "Goran Jartin"
  - subject: "Re: All those files..."
    date: 2007-04-10
    body: "KAudiocreator is also a good candidate for improvement. Lots can be learned from iTunes which does it all right.\n\nGoran, try Dolphin and you will see that this tool does it all right."
    author: "bert"
  - subject: "Re: All those files..."
    date: 2007-04-10
    body: "I've tried Dolphin; several times. And I'm not impressed :-)\nSure, I haven't seen the final version, but has anyone?\n\nI agree with you re KAudiocreator. Sure, it works, but it's - boring. It's just not fun to work with. I think the guy who did the development got bored, too :-)\n(But I must add that that I'm very thankful to him; I use the tool frequently)."
    author: "Goran Jartin"
  - subject: "Why"
    date: 2007-04-10
    body: "Why is the KDE team planning to replace (as default) an established, popular, fundamental program with a program that is not even finished?  Sure, the KDE4 launch is months away, but how do we know that Dolphin will improve at all in that time?"
    author: "Nate"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "Maybe a remnant of the kfmviewxx that was used in the KDE 1.0 days ?\n\ndont repeat the\nthe mistake"
    author: "bah"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "Or more to the point.  Why do we keep starting new apps rather than improve (and complete) existing ones.  The same thing is happening to KView unless there is a last minute reprieve."
    author: "James Richard Tyrer"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "\"Why do we keep starting new apps rather than improve (and complete) existing ones.\"\n\nBecause improving Konqueror wont change the basic premise of the app. if it did, then it wouldn't be Konqueror anymore, and we might as well have a brand-new app in any case."
    author: "Janne"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "\"Why is the KDE team planning to replace (as default) an established, popular, fundamental program with a program that is not even finished?\"\n\nBecause it will be \"finished\" when they release it? By same logic: Why is KDE-team going to replace KDE3 with KDE4, since KDE4 isn't even finished!\n\n\"how do we know that Dolphin will improve at all in that time?\"\n\nHow do you know it will NOT improve? You are looking at a beta-version of the app, and proclaiming that\n\na) it's crap\n\nand\n\nb) it's not going to get any better"
    author: "Janne"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "> How do you know it will NOT improve? You are looking at a beta-version of the\n> app, and proclaiming that\n> a) it's crap\n> and\n> b) it's not going to get any better\n\nFair enough, but those arguments are hardly strong enough to change the *default* file manager. And that's what we're actually talking about."
    author: "Goran Jartin"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "\"Fair enough, but those arguments are hardly strong enough to change the *default* file manager. And that's what we're actually talking about.\"\n\nWhy not? Are you suggesting that Dolphin will be incapable of handling filemanagement when KDE4 is released?"
    author: "Janne"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "> Why not? Are you suggesting that Dolphin will be incapable \n> of handling filemanagement when KDE4 is released?\n\nNo. What I'm saying is that the fact that noone has proved that Dolphin is bad, is not proof that it is good. Not in my book, anyway.\nThat's why I think that the decision to make Dolphin *default* might have been a bit hasty."
    author: "Goran Jartin"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "what should be proved!?\n\ni even like the kde-3 version of dolphin more than konqueror. doesn't that prove dolphin is better?\n \nhow would you even compare konqueror and dolphin? they don't do the same thing - and thats the point! the argument for dolphin is that it doesn't do everything konqueror does."
    author: "ac"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "> i even like the kde-3 version of dolphin more than konqueror. \n> doesn't that prove dolphin is better?\n\nNo. But it's nice that you like it :-)\n\n> the argument for dolphin is that it doesn't do everything konqueror does.\n\n? I'd say that that would be an argument for Konqueror, - as least as *default*.\n\nLook - I have nothing against Dolphin, other than that i find Konqueror better, for now. In the future, who knows?\nI don't say \"scrap Dolphin\" - on the contrary, I think people should be allowed to choose. Maybe there shouldn't be a default at all, but just a choice - do you want a integrated KDE file manager / browser, or a simplified one, with a choice of browsers?\n\nIf you make Dolphin the default, people will start to use Firefox as a browser, because it is (at the moment) slightly better (at some things).\nThat *will* end up in fewer people using Konqueror, and thus less interest in developing Konqueror. \nThis is not hypothetical, it's just pure marketing effects."
    author: "Goran Jartin"
  - subject: "Re: Why"
    date: 2007-04-12
    body: "you don't understand...\n\nkonqueror and dolphin have completely different concepts.\n\nthe developers of dolphin think that the concept of konqueror failed. thats why they write a new application.\n\nso its pretty simple: you can't replace konqueror with dolphin alone, thus you can't really compare them. you just can't say \"well, its time, finally dolphin is better than konqueror - make the switch!\".\n\n\nbesides, there are other problems. the default filemanager has to be integrated into the desktop. so the best time to change the default is a new major kde version.\nthats why the default was decided before dolphin was ready. and because its currently under heavy development, in contrary to konqueror for the last years, it is most likely that dolphin will be ready for the release.\nit won't be perfect, but at leased it will not have the same problems konqueror had. and with luck, it may be a better filemanager even for most powerusers."
    author: "ac"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "a) \"the KDE team\"\nThere hardly is \"THE team\". KDE is made from a losely coupled group of individuals. And many of them join and leave this group.\nThere is no \"masterplan\" that developers have to follow sticktly. That's OSS!\n\nThat's the reason why some importand KDE applications aren't developed currently. It's sad, but that's the way of OSS.\n\nb) \"a program that is not even finished\"\nBecause konqueror's filemanagement development of the last years nearly stalled. But dolphin has a very active grup of developers at the moment. Thanks to their work konqueror filemanager saw more improvements in the last months than in the whole last years.\nWithout dolphin, konqueror would have hardly seen any improvement at all!\n\nc) \"how do we know that Dolphin will improve\"\nThe list of active deolphin developers ist long compared to konqueror's filemanagement list of developers.\nHacking on dolphin is much easier than hacking on konqueror, simply because konqueror is much more complex.\n\nI think chances that dolphin sees much more development than konqueror's filemanagement is pretty high ;-)\nBut maybe I'm wrong. Maybe some developers start improving konqueror quite a lot. And maybe dolphin's development will stall.\nThen chances are good, that konqueror becomes the default filemanager again.\n"
    author: "Birdy"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "I'm glad to hear that Dolphin has an active development team.  Let's hope it turns out to be a good program.\n\nI just think that it is hasty to decide that an unfinished program should be the default file manager.  I would prefer Dolphin to mature for a little while first."
    author: "Nate"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "> I would prefer Dolphin to mature for a little while first.\n\nKDE 4 is a platform for proving lots of new technologies and applications. If you want something already mature, you're better off sticking with KDE 3.x until 4.x has been out for a while."
    author: "Paul Eggleton"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "Um, no. KDE 4 is the next iteration of KDE.  Which I expect to be bigger and better than the previous iteration of KDE, KDE3.  It's not about proving random technologies and applications."
    author: "KDE User"
  - subject: "Re: Why"
    date: 2007-04-12
    body: "Um, yes. If you look at the number of new technologies being introduced in KDE 4 (none of which are random, by the way) then you'll see that I am correct. Not to mention that KDE 3.x is planned to be be kept around under maintenance even after KDE 4.0 is released for that exact reason."
    author: "Paul Eggleton"
  - subject: "Re: Why"
    date: 2007-04-11
    body: "> Because konqueror's filemanagement development of the last years nearly\n> stalled. But dolphin has a very active grup of developers at the moment.\n> Thanks to their work konqueror filemanager saw more improvements in the \n> last months than in the whole last years.\n\nIf this is correct (and I have no reason to doubt it), it's actually the first solid argument I've heard for choosing Dolphin! (I still don't think it's a very *strong* argument for changing the default, though)."
    author: "Goran Jartin"
  - subject: "No Crystal Ball"
    date: 2007-04-11
    body: "I don't know if I'll like Dolphin in KDE 4 or Konqueror in KDE 4 better.\n\nI'm glad I'll get to choose for myself between them when they actually exist."
    author: "Evan \"JabberWokky\" E."
  - subject: "Hey! Where is the tree view??"
    date: 2007-04-11
    body: "Hey! Where is the tree view?? How should I navigate directories? Imagine deep directory structure and you need to go just about its bottom... it would be awfull clicking folder icons and \"back/forward\". I agree, Konqueror is a bit overfunctional (for a 2 minute session of just finding and opening some doc), but file manager is not a file manager for me, if it hasn't a simple (and fast building) tree view."
    author: "Knuckles"
  - subject: "Re: Hey! Where is the tree view??"
    date: 2007-04-11
    body: "The KDE 4 version of Dolphin has a tree view.\n\nBest regards,\nPeter"
    author: "ppenz"
  - subject: "look this mockup"
    date: 2007-04-11
    body: "Hi! I love konqueror but at this moment i don't like dolphin (i don't know if i'll like it when it be finished). But i've seen this mockup and it's very interesting and nice: http://www.kde-look.org/content/show.php/Dolphin+mockup%21?content=56040\nIt would be nice if developers take a look at this mockup. The only thing that i've missed it's tabs, they are essential for me and many people."
    author: "Fenix-TX"
  - subject: "Usability tests on Dolphin"
    date: 2007-04-11
    body: "I think it is a great idea to make Linux more user friendly, and Konqueror could be easier to use; however, maybe it's just me, but I found Dolphin harder to use than Konqueror.\n\nDoes any one know how many usability tests the developers have done on Dolphin?"
    author: "Nate"
  - subject: "Why not hide the confusing features of konqueror?"
    date: 2007-04-11
    body: "I still do not understand why it wasn\u00b4t possible to simply add a Beginners/Advanced-Option to Konqueror and hide most of the functionality in beginner-mode.\nThat would probably only take a week of work."
    author: "Jens H."
  - subject: "Re: Why not hide the confusing features of konquer"
    date: 2007-04-11
    body: "Because it's also about to have a simple (and once again, simple doesn't have to mean that it lacks features) file manager.\n\nFrom http://www.konqueror.org/:\n\"Konqueror - Web Browser, File Manager - and more!\"\n\nAlso, you can read some thoughts about user levels here:\nhttp://lists.kde.org/?l=kde-core-devel&m=98944039129759&w=2\n\nSure, you only suggested Beginner/Advanced, but it could still be interesting reading as some applies to two levels too I think."
    author: "Lans"
  - subject: "Re: Why not hide the confusing features of konqueror?"
    date: 2007-04-12
    body: "Dolphin is not about being simpler, it's about being specialized.\n\nAnd I doubt it would have been a week of work. As konqueror is very complex because of being a manager and browser, it's much harder to work on.\nSimply have a look at the konqueror's settings. They are a mess. Browser and manager related settings are mixed up - surely not userfriendly. And not fixable in one week...\n"
    author: "Birdy"
  - subject: "Inspiration from Xandros?"
    date: 2007-04-12
    body: "One thing I'd love to see in Dolphin is a left bar identical or at least similar to the sidebar in Xandros' File Manager (you can see it here at http://en.wikipedia.org/wiki/Image:Xfm.png).\nIt looks reasonably ergonomic and contains links to various important parts of the system. Obviously in some ways Dolphin already looks better, e.g. better icon theme and breadcrumb support. However, let's take the best features from the Xandros offering and use them to enhance this manager even further."
    author: "Darryl Ramm"
  - subject: "Re: Inspiration from Xandros?"
    date: 2007-04-13
    body: "It's a joke, dosen't it?."
    author: "Luis"
  - subject: "bad ideas"
    date: 2007-04-12
    body: "I think from this whole discussion and from the previous one it is quite obvious that making Dolphin the default was and is a seriously bad idea. Not because it's like Nautilus or whatnot, not because it may or may not have been conceived by \"usability experts\" that seem to always end up ruining usability, and not because Konqueror is so much better - we all know it's teeming with ridiculous bugs and needs tons of work to make it usable.\n\nIt's because making Dolphin the default brings a great risk of splitting the community and ruining the traditional KDE spirit of progress, unity, and collaboration. It is so much easier to ruin a community than to build it. Remember what happened to Kollaboration? It worked wonderfully until one day someone decided to \"improve\" it by moving the whole thing to some different place and making other changes? The result? The whole thing is *gone*, as in *totally destroyed*, and that new project with shiny new forums has exactly two messages posted over the last year, without a single reply.\n\nSo Aaron and all of you who make decisions, please think it a hundred times over before doing something like that again."
    author: "5yr KDE user"
  - subject: "Don't be so hasty"
    date: 2007-04-13
    body: "Let the project take its course. There's no reason why the best attributes of Konqueror can't be implemented into Dolphin, and the hard work of the developer(s) suggests a willingness to produce high-quality code, which could quite feasibly have few, mainly minor bugs by the KDE 4 release."
    author: "Albus Snape"
  - subject: "Re: Don't be so hasty"
    date: 2007-04-13
    body: "I'm with you 100%. Let Dolphin take its time to develop and be a great KDE program.\n\nThe bad idea I talked about is making it the default, at this time at least.\n\nThat's a mistake, and I hope the powers that be realize it sooner rather than later."
    author: "5yr KDE user"
  - subject: "why i love dolphin"
    date: 2007-04-14
    body: "1. view memory. the single biggest reason i dislike konqueror. who on earth though it would be a good idea *not* to remember view settings in directories? if i set the contents to preview mode and sort it by date, i want it that way permanently. thank god dolphin does this. if konqueror does it, you can mark it down for usability cos i've been using the thing for a year and not found it yet...\n\n2. bookmarks. very easily accessible bookmarks. and very easy to make new ones too.\n\n3. file info. highlight a file and it's on the left (or right or wherever they've put it now). i don't have to hover over anything and wait for it to pop up in a tiny image.\n\n4. open as root. the amount of times i've had to go through the start menu to open a whole new konqueror window just so i can get root privaleges to edit something... highlight a folder in dolphin and there's a button in the sidebar to allow oppening that folder as root. i like the \"open this location in terminal\" button too.\n\n5. no horrible tree view for files. never like the things. up, back, forward, and a few bookmarks, that's how i like my navigation."
    author: "pepsi_max2k"
  - subject: "Re: why i love dolphin"
    date: 2007-04-15
    body: "5. Bad news: there will be a tree view ;-)"
    author: "MM"
  - subject: "right way, but wrong direction."
    date: 2007-04-15
    body: "Are you sick, KDE4 developers?\nIn my opinion, Konqueror is the only serious reason for the KDE project still to exist: The genuine all-in-one idea has been, as Konqueror has been the first in it and is still the only, the key to KDE's success. When you need a web browser, take Konqueror, and it does a really good job as file manager and basic FTP client, as well. Any file can be viewed \\- but hasn't to. No one and nothing forces the user to view videos with KMPlayer KPart, there are also Kaffeine and xmms-mplayer and so on. For browsing, Firefox or Opera or w3m or whatever also do a good job. KWord is an excellent _and_ lightweight word processor not only in a Konqueror tab; nothing plays your music more comfortably than Amarok. But to provide all these facilities combinedly, Konqueror is the only choice!\nKDE lives from being integrated, it's even advertises with that. Why should you want to destroy that image forcibly? What would then be the difference between KDE and all the other faceless \\\"mosaicked\" DEs? Why should one install blown-up Qt4 and kdelibs just for another Thunar or nautilus clone (they _exactly_ do the same restricted job and _look_ even the same)? There is not even a lack of standalone Qt-based file managers! Not even amateurish ones!\n\nAnd to the point that Konqueror would stay available: Did you realise at all, that Konqueror is KDE's most charismatic application? One must squeeze man into their luck. And that Konqueror isn't that customisable that it looks and acts like all the mid-class Gtk-FMs does be luck.\nPeople changing from any proprietary OS to linux want to discover new functions. But when there are none, why should they stay using linuxoids? As I switched from Windoof to GNU/Linux (KDE 3.3 on SuSE 9.2, good ol' times...), I was deeply amazed by all of Konqueror's features, and no time even the idea of frightened about over-complexity, that's not the problem. Konqueror just was a good app (and furthermore self-confident enough for showing its name), a very good app.\nSome months, I still used Firefox as a web browser (of habit), until Konqueror's Adblock-functionality obsoleted it within KDE 3.5 . Even though one can argue if Konqui's browsing capabilities are superior of Geckos' ones (btw: don't throw KHTML away when integrating Gecko into the KDE4 browser, not only Apple would be concerned...), but Konqueror offered one-in-all functionality \\- an entierly new experience! Is it sensible to hide that between the dozens of other applikations of a KDE box?\n\nImagine a Windoof user trying a try-out-Linux-GUI live CD, browsing his HDD with Dolphin and the web with Konqueror, and then switching to Gnome, seeing the same with Nautilus and Epiphany, and then to XFce, with Thunar and Firefox. He undoes the CD and asks why to switch away from his Exploiter/IE desktop when there is nothing new than a few other-looking widgets, a bit more security, exchanged for a bit few WMV and Adobe Flash.\nIs that the impression you want to give, KDE4 team? To be replaceable? (Yes, you might say Konqueror still continues existing, but the expression will come up, if you don't force all people use it). If yes, you should also port QtCurve as fast as possible to Qt4. Or sink.\n\nIs there actually a reason to throw Konqueror as file manager? Konqueror (and the view profiles technology is awesome as you can see from the minor spread of Krusader and similar) is as simple or as advanced as you need it for file management! Yes, annoyingly Metabar, the preview and filelinfo function is not as weakly hidden as in M$ Exploiter or Nautilus, for scripting you will have to cope with a programming language, and yes, the directory tree sidebar does not follow the current path properly. But those are no existential problems or design errors, but solvable issues. Why not integrating some useful how-to pages into documentation accessible over the portal page? Why not coding some lines instead of writing an entirely new programme? And OOOARGH! They KILLED the URL bar! Why not as here: http://www.kde-look.org/content/show.php?content=35637 ? Why to flatten everything onto Gtk-format, only because Canonical has a smarter and more successful marketing management than Bernhard Rosenkr\u00e4nzer?\nDoesn't a free community live from freedom to choose, to choose also between a Konqueror-ish file manager and cd ls mv ?\nKDE can't wait for masses' curiosity of computing issues for promotion its uniqueness!\n\nActually, I want to give some advise to both the KDE4 developers and, mainly, to dolphin's: Do you know Filelight? http://www.kde-apps.org/content/show.php?content=9887 It's unique, it's tiny, and it's KPart. Running \\\"filelight\" from a Run dialog, or out of the K menu, Filelight starts as a standalone device filling grade overview, you can browse into your dirs, search'n'delete huge files or enjoy all the nice colours of the filesize diagrams. But clicking the little yellowish-orange button in Konqueror's main toolbar, the same applies to the current directory. KPart. It works.\nI acknowledge every line of free source code and don't want to fight against diversity of software. But wouldn't it thus be the finest to transform Dolphin, which might really have its advantages, into a KPart? And then, as the default Konqueror plugin for directory browsing, I don't mind. Dolphin (what a sin \\- not a single K inside the name, how should people then realise that it is a KDE programme?) would stay optionally as a standalone, basical file browser, and those who want to continue using the high-quality file manager Konqueror (as me, for instance), enjoy another growth of versatility. Why not?"
    author: "ertua"
  - subject: "Re: right way, but wrong direction."
    date: 2007-04-15
    body: "> Are you sick, KDE4 developers?\n\nNo, but we are open for any constructive input. Starting your input like this does not speak very much for you, honestly speaking ;-) Still I'd like to reply to some of your points.\n\n> Did you realise at all, that Konqueror is KDE's most charismatic\n> application? One must squeeze man into their luck.\n\nNo, we don't want to squeeze anybody into their luck. That's why Konqueror will stay as it is. We don't want to replace Konqueror by Dolphin. Please check http://aseigo.blogspot.com/search/label/dolphin for some background infos.\n\n> Is there actually a reason to throw Konqueror as file manager?\n\nKonqueror will exist as file manager as always. Nothing will be thrown away.\n\n> Why not coding some lines instead of writing an entirely new programme? \n\nDot comments like yours are one reason why developers fear to change anything in Konqueror: as soon as something will be changed a lot of people raise there voice \"don't change this and that, you kill the soul of Konqueror\". Also Dolphin is not a complete new application, it uses all Konqueror commands and the KDE library for the file management tasks. Dolphin is not much more than a thin shell.\n\n> And OOOARGH! They KILLED the URL bar!\n\nThat's not true: you can switch back without any problems and the setting will be remembered. Please inform yourself a little bit and spend 5 minutes reading some information at http://enzosworld.gmxhome.de.\n\n> Actually, I want to give some advise to both the KDE4 developers\n> and, mainly, to dolphin's\n\nThanks, but as you called us developers \"crazy\" I'll just ignore (at least your) advice :-)\n\n> But wouldn't it thus be the finest to transform Dolphin,\n> which might really have its advantages, into a KPart?\n\nDolphin will offer a KPart for Konqueror.\n\nBest regards,\nPeter\n\n"
    author: "ppenz"
  - subject: "Re: right way, but wrong direction."
    date: 2007-04-15
    body: "I just wanted to say, Peter, that I admire your patience and willingness to listen to input (even if most of the input from these threads is ... a little misinformed, to say the least :))\n\nYou're doing a great job with Dolphin, and I'm sure it will go from strength to strength.  As one of the handful of KDE fans on the UbuntuForums, I pay special attention to the complaints about KDE, and one of the foremost is the clutter and complexity of Konqueror that stems from its browser-manager duality.  Making Dolphin the default is, in my opinion, the Right Move, and keeping Konqueror around for the power users makes it even Righter :)"
    author: "Anon"
---
Ryan Paul over at Ars Technica has a <a href="http://arstechnica.com/news.ars/post/20070405-afirst-look-at-dolphin-the-kde-4-file-manager.html">short article</a> talking about Dolphin and KDE 4. <i>"The Linux-based <a href="http://enzosworld.gmxhome.de/">Dolphin file manager</a> is now scheduled for official inclusion in KDE 4, the next major release of the KDE desktop environment. Dolphin includes several unique usability enhancements that aren't available in Konqueror, KDE's current file manager..."</i>



<!--break-->
