---
title: "Key KOffice Developers Talk About KOffice 2 and Open Standards"
date:    2007-11-02
authors:
  - "sk\u00fcgler"
slug:    key-koffice-developers-talk-about-koffice-2-and-open-standards
comments:
  - subject: "It makes no sense"
    date: 2007-11-03
    body: "First you say \"we listen to our users and hear that they want to exchange documents without enforcing their customers, teachers or peers to use the exactly same application in the exactly same version.\", but later you say \"If OfficeOpen XML becomes an ISO standard, we will, in all likely hood, still not spend time on supporting it.\"\n\nI'm sorry, but get a reality check. MS Office has about 90% market share. OpenOffice is in a large part so successful, because it supports MS Office formats very well. People don't want to use office software that frags the documents they recieve. No supporting MS file formats does not weaken MS's position, but makes people stick with MS Office.\nThere is already a very good FOSS converter for OOXML documents. It converts OOXML files to ODF files and vice versa. There's no need to work through the big OOXML specs and write your own converter. Just make an *optional* KOffice module that talks to the CLI odf-converter app."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: It makes no sense"
    date: 2007-11-03
    body: "\"still not spend time on supporting it.\" it means we won't spend time developing our own OfficeOpen XML support. One of the KOffice hacker allready made it possible to use OpenOffice to silently convert files to odf to then open them in KOffice.\n\nBut if there is a good OOXML to ODF converter, we will of course be able to leverage it and use that. I would also like to point out, that OOXML is not yet an ISO standard, and a lot of changes are needed to be made to the standard before it can become one, so support for OOXML is clearly not a priority for the next release."
    author: "Cyrille Berger"
  - subject: "Re: It makes no sense"
    date: 2007-11-03
    body: "I think the koffice people need to implement at least basic microsoft document support if they expect people to use it.  I love kde to the end of the earth and use kde apps everywhere I can, except office apps, there I use openoffice;  The fact of the matter is I'm a college student in America.  I can't turn in reports in odf, because my school uses ms word.  I can't force people to use odf, but if kword supported doc it would be much easier"
    author: "Level 1"
  - subject: "Re: It makes no sense"
    date: 2007-11-03
    body: "You clearly haven't found the \"Save As\" option (or are trolling...)\n\nFile -> Save As; select Microsoft Word document.  It's not rocket science..."
    author: "Adrian Baugh"
  - subject: "Re: It makes no sense - compatibilty with msformat"
    date: 2007-11-03
    body: "I don not think  he/she is a troll. Compatibility with Ms-office is crucial in spreading koffice.\nCan one export kword files containing tables and pictures to the ms-doc format?\nCan one _import_ ms-doc formatted documents containing tables and pictures?\n\nUntil last year, as far as i remember, that was not possible.\n\nAre developers making progress in that direction?\n\n "
    author: "Pol"
  - subject: "MS formats are history"
    date: 2007-11-03
    body: "The onus is on M$ to become compatible.  However, that'll never happen voluntarily.  So, we have for the time being Sun's ODF plugin for MS Word:\n  http://www.sun.com/software/star/odf_plugin/\n\nMS formats are history.  Sure several of the .doc formats are around now, but not a single one of the specifications has been published.  That means as soon as a universal format gets a critical mass, people will leave.  \n\nKOffice is making the right move to turn to open standards.  \n\n "
    author: "Right direction"
  - subject: "Re: MS formats are history"
    date: 2007-11-04
    body: "I don't think anybody ever seriously considered/proposed making MS Office formats the default formats in KOffice. ODF should stay the default file format in KOffice.\nBut good reading support for MS Office formats is crucial.\n\nI have a Linux PC and an iBook with Mac OS X. Thus I'm also visiting Mac user forums. You probably don't imagine it how big the resistance against the upcoming MS Office 2008 is. That release will display most existing MS Office documents just fine, but it won't support VBA scripting. If killing a feature that only a minority uses causes so much uproar, just think about how much resistance an office suite will get (on platforms where MS Office is available) that can't even open many MS Office file remotely correct."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: It makes no sense"
    date: 2007-11-03
    body: "OpenOffice is bloated and hard to use because instead of improving the apps they are always following Microsoft. There's no need for another app to go that way.\n\nFor example kpresenter is different from ms powerpoint, but IMHO it's better and easier to use (although some features are missing).\n\nThe bottom line is: we don't need another ms office!"
    author: "AC"
  - subject: "Re: It makes no sense"
    date: 2007-11-04
    body: "It isn't bloated for copying MS Office, and it certainly departs from MS Office in many ways.  It is bloated because of a large, perhaps poorly written code base.\n\nMS Office is perhaps the best piece of software MS has put out.  It is very feature rich, stable, and loads very quickly.  It is well integrated, and really a good product.  If it were multiplatform, I might just use it on Linux believe it or not.  Because it isn't multiplatform, I use OOo on both."
    author: "T. J. Brumfield"
  - subject: "Re: It makes no sense"
    date: 2007-11-04
    body: "Yeah, Office is truly excellent.  Openoffice is ok for basic needs, but for anything more advanced I have to turn to MS Office (especially excel is leaps and bounds ahead of calc).\n\nTrying to beat microsoft at their own game is impossible.  KOffice needs to be innovative and take a different approach, which I think they are already doing very well.  I look forward to 2.0."
    author: "Leo S"
  - subject: "Re: It makes no sense"
    date: 2007-11-06
    body: "You should turn in reports in PDF, not .doc or .odt.\n"
    author: "Reply"
  - subject: "Re: It makes no sense"
    date: 2007-11-04
    body: "\"But if there is a good OOXML to ODF converter, we will of course be able to leverage it and use that.\"\n\nMy point is that you don't want to do it. How hard is it for a C++ developer like you to launch an external executable from KOffice? If it's a piece of cake then just check if odf-converter is installed in the system's PATH, run it to convert an OOXML file to ODF, and open the resulting ODF file. AFAIK Novell already does this in their OpenOffice built.\nWhen I mentioned odf-converter the last time, I just received hatred, because the converter is Mono-based. So what? I don't want to become Mono a requirement for KOffice either and that's why I just propose to make it optional."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: It makes no sense"
    date: 2007-11-05
    body: "Yes and we all know about the Microsoft/Novell deal too. Sure Novell can use mono, and OOOXML and whatever Microsoft technology it wants to, they've signed a deal with Microsoft that allows them to do so.  \n\nOpen standards work for everyone, Microsoft standards only work for those whom Microsoft allows them to work for."
    author: "Steve Brennan"
  - subject: "Re: It makes no sense"
    date: 2007-11-03
    body: "Office 2007 doesn't have the huge market share that Office 2003 does, because most businesses aren't making that jump yet.  MS Office does have a huge market share lead, but a bigger priority should be on importing and exporting Office 2003 .doc and .xls formats first, before worrying about the huge mess that is OOXML.\n\nFurthermore, now that there are .odf converters for Office 2007, you can convince Office 2007 users to send documents in .odf format to ensure compatibility with them.  Office 2007 users who want to share documents likely have to \"Save As...\" in another format anyway, since not many people are using Office 2007 anyway.\n\nOpenOffice is good, but not perfect.  It is a little slow, has political issues with Sun going on, features bloat from an old code base, and has a poor UI.\n\nPersonally, I'd either like to see a combined effort from developers to make OOo the best it could be, or see a better product replace it.  KOffice couldn't compete with OOo because it wasn't a multiplatform product, but it is about to be.\n\nThere has been some discussion in the OOo crowd to remove some older portions of its clunky codebase, and replace it with other existing libraries and products, such as page rendering and the like.  Neither OOo nor KOffice is going away, so we're going to get developers split between the two products, and we have to accept this at some point.\n\nHowever, I have to wonder if the OOo and KOffice devs couldn't work together on some joint libraries between the two products, such as MS import filters, ODF libraries, etc, and libraries for both products to best display natively on all three major platforms.\n\nA codeveloped ODF library doesn't just benefit the KDE team, it benefits all developers, and the further adoption of the ODF format."
    author: "T. J. Brumfield"
  - subject: "Re: It makes no sense"
    date: 2007-11-04
    body: "See, very good support can't be done within a day, a week, a month or even a year. It takes just very long. Now the situation is, that it's guaranteed that ISO OpenDocument will be the default file-format for a bunch of applications for a pretty long time. That means, if we invest today time, it will pay out tomorrow and even the day after tomorrow we still support a widly used format. Now try to compare that to the MSOffice2003-Format. 2003 is _NOT_ compatible with 2007. Don't believe there the MS food, they are pretty much different file-formats what means, that already today the MS2003-format is already expired and out-of-date. If you look back a few years you will note, that this was very often the case and the old MSOffice-formats got replaced each few years with a new format. Now we have those blending MSOffice2007-Format. And guess what? Its guaranteed that the format will be replaced with a new one as soon as there is a new MSOffice-suite out. We are already able to see this with the large VML-related parts defined in the MSOOXML2007-specification and down the road we can expect that the next MSOffice-format will also introduce more dependencies to .NET && .Windows .\nSo, rather then starting to work on import+export _filters_ it just makes more sense to put all energy into a format that is guaranteed to stay real backward-compatible, even in 2008, 2009, and so on... A format that will improve in small steps rather then beeing replaced behind closed doors in one huge rush with a new MSOffice-version.\n"
    author: "Sebastian Sauer"
  - subject: "Re: It makes no sense"
    date: 2007-11-04
    body: "\"Office 2007 doesn't have the huge market share that Office 2003 does, because most businesses aren't making that jump yet. MS Office does have a huge market share lead, but a bigger priority should be on importing and exporting Office 2003 .doc and .xls formats first, before worrying about the huge mess that is OOXML.\"\n\nYes, you are right, but fully supporting old MS Office formats is hard. For OOXML files there already is a stand-alone converter that converts OOXML files to ODF files. Just executing this converter shouldn't be so hard.\n\n---\n\n\"Furthermore, now that there are .odf converters for Office 2007, you can convince Office 2007 users to send documents in .odf format to ensure compatibility with them.\"\n\nUm, it's sadly a bit more complicated than that. As Windows users don't have a nice package management system, they have to resolve all dependencies by themselves, because they have to install the Office Compatibility Pack, install the .NET Framework, activate .NET Programmability Support, and then install the ODF converter. And that's only when they want to send their documents to someone else. What if those Windows users like to switch to KOffice 2? Why should they first need to convert all existing files they have on the hard drive? If KOffice supported them right away it would be a lot easier for them.\n\n---\n\n\"KOffice couldn't compete with OOo because it wasn't a multiplatform product, but it is about to be.\"\n\nAbiWord has always been multi-platform. It boots faster and uses less resources while having all features common users want to have. Yet almost no Windows or Mac user uses AbiWord. Why? Because its MS Office file supports sucks as much as KOffice's."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: It makes no sense"
    date: 2007-11-03
    body: "> [about OOXML] I'm sorry, but get a reality check. MS Office has about 90% market share.\n\nNo, this is a reality check. OOXML has *NO* market share today. There are more ODF documents at the web then OOXML documents. The .doc and .xsl formats do have market share. If KOffice and OpenOffice support OOXML now it will help Microsoft to get that marketshare.\n\nIt's a classical Microsoft trick: provide documentation so everyone jumps in the boat, and stop providing information once they got market share. They did the same with early MS-Office formats, the MSN protocol, and even the SMB protocol was documented partially once to kill Netware (MS even invited developers from Samba). The community shouldn't fall for this trick with OOXML."
    author: "Diederik van der Boor"
  - subject: "Fragmented at best"
    date: 2007-11-03
    body: "Of those 90% of users who use .doc, how many use Office 95 .doc, or Office 97 .doc, or Office 2000 .doc, or Office XP .doc, or Office 2003 .doc?\n\nMost today probably fall within the last 3, but I work for a newspaper, and there aren't many vendors who specialize in software for the newspaper industry, but th Harris software we use (and hopefully we get away from) sits as a wrapper on top of Word 97, so our newsroom is forced to stick with Word 97, ten years later.\n\nGoogle was once a nobody 10 years ago.  IE once had an insurmountable lead in market share.  Educating users, providing a better product, and good marketing can overcome huge market shares."
    author: "T. J. Brumfield"
  - subject: "Re: It makes no sense"
    date: 2007-11-03
    body: "Ten years ago, most people used VCRs and VHS videotapes, to rent movies and record off TV.\n\nNow, they use DVD players and ISO-standard DVDs.\n\nYou cannot put a VHS videotape into a DVD player. Either you use a conversion service, convert it yourself, or leave it inaccessible on VHS.\n\nIt's pointless making a DVD player able to read from and write to VHS videotapes. You want a VCR for that job.\n\nNew stuff goes on ISO-standard DVDs. \n\nGood work, 'koffice' crew. Keep it up !"
    author: "Chris Ward"
  - subject: "Re: It makes no sense"
    date: 2007-11-04
    body: "Document archiving is somewhat more important than watching a 20 year old copy of \"Romancing the Stone\".  That is a big reason why ODF is so important."
    author: "T. J. Brumfield"
  - subject: "Re: It makes no sense"
    date: 2007-12-11
    body: "For document archiving, the chances are that you <em>don't</em> have to be able to read your old files into your new office suite.  You might want to <em>view</em> an old letter you sent 10 years ago, but you sure as heck don't need to <em>edit</em> it  (in fact, editing it could be a Very Bad Thing in some circumstances).  So, all you need do is get a Microsoft Windows box with a version of Office that can open all your old files, and a PostScript printer driver.  Then \"print\" all your documents to PS files.  They can be viewed or printed as required.\n\nIf you do ever need to edit an old file, any Open Source office suite should be able to have at least a brave go at opening it.  Most files will need a few minor tweaks, but in rare cases it may work out easier and quicker just to retype the whole lot from scratch.  At any rate, you can always call up the PostScript version as a reference rendering."
    author: "AJS"
  - subject: "Re: It makes no sense"
    date: 2007-11-03
    body: "The refusal to consider OOXML even if supported as a standard does seem hypocritical at first glance but only a little digging shows it's the only possible choice. \n\nTo begin with, Microsoft does not intend to support its own standard. The reason given by Microsoft was that once a standard, OOXML would necessarily have a governing board which is not Microsoft, and Microsoft refuses to guarantee that it will follow decisions imposed by a standards organization. \n\nWhen you have 90% market share you can get away with this. Microsoft then has the rest of the world scrambling to keep up with OOXML which is broadly compatible with its own products, but is not itself obligated to follow inconvenient decisions. The technical committee for the standard will then be in the position of forever trying to catch up to changes by MS which are never properly documented. \n\nSecond, neither Sun nor KOffice nor the ODF developers are trying to supplant MS Office formats. Market share is not the immediate objective. The main reason why ODF is gaining traction is that it allows for archival formats which will forever be readable by future word processors, etc. This is why librarians and archivists the world over have been clamouring for such a standard. Since ODF is a slim, technically complete standard available right now which does the job, neither KOffice nor OOo nor Abiword has any particular reason to support MS-OOXML. \n\nThird, the OOXML converters you mention have a huge problem. They do not (and can not, because OOXML is not completely specified) perfectly convert between ODF and OOXML. Nor will they ever be able to, since OOXML contains 'binary blobs' which are not properly understood at this time even by Microsoft. This is one reason why the French ISO representatives at one point suggested that the proper designation of OOXML was not a standard, but an ISO TS (technical specification). A TS does not have to meet the very high consistency and completeness criteria of a standard. \n\nRegards, TC\n"
    author: "Terry Cole"
  - subject: "Re: It makes no sense"
    date: 2007-11-04
    body: "\"Third, the OOXML converters you mention have a huge problem. They do not (and can not, because OOXML is not completely specified) perfectly convert between ODF and OOXML.\"\n\nWho wrote something about perfect support? I just wrote about not fragging documents. I did some experimenting with the CLI odf-converter and from what I saw the conversation is quite good."
    author: "yxxcvsdfbnfgnds"
  - subject: "Nobody support Ecma OOXML, not even Microsoft"
    date: 2007-11-05
    body: "* yxxcvsdfbnfgnds writes: \n> I'm sorry, but get a reality check. MS Office has about 90% market share.\n\nYes, Microsoft Office 1997 document formats got 80-90% marked share. Ecma OOXML has zero marked share. Please don't mix together the product name Microsoft uses on their office suite and the realities concerning the different file format supported in Microsofts products. Microsoft has in two attempts made what they describes as an open format, one in used with MS Office 2003 and a new one with  MS Office 2007. Unfortunately Microsoft just partly support OpenDocument Format. Microsoft don't use the Ecma OOXML specification they suggested them self. Microsoft has made a special non documented version of OOXML they follow them self. Almost no businesses uses OOXML, everyone uses the 12 year old MS Office 1997 format. \n\n>  People don't want to use office software that frags the documents they recieve.\n\nI agree with you. Unfortunately Microsoft them self are betting on that people will change to their OOXML non-Ecma edition, leaving MS Office 1997 format behind. \n\nDifferent national ISO-members has committed thousands of objections that has to be addressed before accepting Ecma OOXML. I will name three objections against supporting Ecma OOXML: \n\n* Microsoft does not uses Ecma OOXML them self, but a special version of that format. If someone should be folish to implement Ecmas OOXML, they never know if it's really is implemented in real life as in MS Office 2007. This is a moving target, and you can end up with supporting 6000 pages with Ecma OOXML and has to reverse engineer Microsofts interpretation of OOXML. To implement 6000 pages of document format is a huge undertaking and a lot bigger than the around 600 pages OpenDocumnt Format spesification. But you must also support what Microsoft add and retract from the Ecma OOXML spesification. You don't get time left to make an usable office suite when betting on Ecma OOXML. \n\n* Ecma OOXML is illegal to use for individual developers according The Danish Standardization body. Individual developers are prohibited from using Ecma OOXML. Most of the KOffice developers are individual developers and will be banded from implementing Ecma OOXML if they will contribute. \n\n* Ecma OOXML consist of non documented features like binary only storage elements and non documented functions. To get access to _some_ of the feauture, not all, but some: Your company has to sign a patent deal with Microsoft to get access to some of the not documented features in Ecma OOXML. Microsoft then demand that they shall get right to use all your intellectual property. This kind of legal binding to Ecma OOXML shows that the format is really an Microsoft format, not controlled by a standardization body. Similar legal wording has made Japans competitive authorities to do a rassia against Microsoft for breaking the competitive law. \n\n> No supporting MS file formats does not weaken MS's position, \n> but makes people stick with MS Office.\n\nI think you are slightly out of topic. Maybe because you are somewhat confused about the many formats Microsofts has implemented as a part of MS Office, and which formats they don't support where Ecma OOXML is not supported. \n\nYou are spot on when it comes to supporting the old MS Office 1997 format. But as mention above, Microsoft them self don't support Ecma OOXML. And Microsoft prohibits individual developers to use Ecma OOXML. ISO comities around the world voted against standardization of OOXML in its current form. Thousands of issues has to be addressed if OOXML gonna represent a transition format from old MS Office documents to a \"modern\" one. This is the main goal for Microsoft, but they have not delivered on promised, as the ISO standardization process shows. "
    author: "Knut Yrvin"
  - subject: "Re: Nobody support Ecma OOXML, not even Microsoft"
    date: 2007-11-05
    body: "\"Ecma OOXML has zero marked share.\"\n\nOh, where did you get you data from? I'm eager to see that nobody ever bought Office 2007 and nobody pirated Office 2007.\n\n---\n\n\"Almost no businesses uses OOXML\"\n\nThe world consists of more than just business. I'm fully aware that there are the OOXML file formats and older MS Office file formats. OOXML has the advantage that there is already a working, stand-alone converter.\n\n---\n\n\"I think you are slightly out of topic.\"\n\nWho is off-topic? You wrote about ECMA OOXML and MS OOXML and so on. Honestly: I don't care. Maybe YOU are confused by all that political stuff. Let me try to explain again:\nThere is a working OOXML to ODF converter. It's stand-alone and it's free software that is already integrated into Novell's OpenOffice builds. I tried the converter with a few OOXML documents I found on the net (all created with MS Office 2007) and the results where quite good.\nThere's no need for any KDE/KOffice developer to write a new OOXML converter.\nJust take the existing converter, wrap a GUI (=the open/save window) around it and have instant access to more file formats."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Nobody support Ecma OOXML, not even Microsoft"
    date: 2007-11-05
    body: "Knut Yrvin wrote: \n\n\"Ecma OOXML has zero marked share.\"\n\nyxxcvsdfbnfgnds wrote: \n\n> Oh, where did you get you data from? I'm eager to see that nobody \n> ever bought Office 2007 and nobody pirated Office 2007.\n[...]\n\nSo you don't know the difference between the OOXML specification Microsoft committed to Ecma and their own deviated OOXML-version used in MS Office 2007? This emphasizes my point. You believe things works as long as you can read a label with a five letter acronym from a big cooperation. You don't care if it means to fit a driver seat from BMW in a Volvo, without any specification available. As long as Microsoft tells you that the BMW driver seat should fit in in a Volvo, you believe them. You don't care about the people who makes the solution you want, even if it involves legal risks or a huge pile of work.  \n\n> You wrote about ECMA OOXML and MS OOXML and so on. Honestly: \n> I don't care. \n\nYou don't care ...\n\nFree software developers cares about the software they write. They care about licenses and boring legal stuff. Unfortunately Microsoft makes it illegal to make OOXML integration for individual developers. When Sun makes an OOXML converter to ODF, they are a company and got the legal team to handle the licensing issues. I suggest you yourself, since you don't care of legal risks, tries to implement Suns OOXML->ODF in KOffice based on the work from Sun Microsystems.  But it seems that you don't know about this things in depth to do it your self. If that true, I think you should put your money where your month is. Instead of letting others live with the legal risks implementing OOXML, you should pay any legal cost for the developers who supports MS OOXML as you suggests. Or don't you care about that either? "
    author: "Knut Yrvin"
  - subject: "Re: Nobody support Ecma OOXML, not even Microsoft"
    date: 2008-01-11
    body: "I would be interested to know more about the source of information on Ecma OOXML being different from MS Office 2007. How does one validate this piece of information ?"
    author: "Indian User"
  - subject: "Re: It makes no sense"
    date: 2007-11-05
    body: "It makes perfect sense.  What you're missing is the other factor: how crap and deliberately unimplementable MS's format is."
    author: "Lee"
  - subject: "Re: It makes no sense"
    date: 2007-11-05
    body: "\"What you're missing is the other factor: how crap and deliberately unimplementable MS's format is.\"\n\nBullsh*t. There is already a working converter. KOffice just needs to execute it. If KOffice's ODF support is good enough (the converter does OOXML->ODF), most OOXML files will open just fine in KOffice."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: It makes no sense"
    date: 2007-11-05
    body: "This converter is using Mono which many people don't like. So it won't be included with KOffice."
    author: "AC"
  - subject: "Re: It makes no sense"
    date: 2007-11-05
    body: "That's why I wrote \"optional\". Nobody should be forced to install it, but people should have the option to do so. Learn to read."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: It makes no sense"
    date: 2007-11-05
    body: "yxxcvsdfbnfgnds writes: \n\n> That's why I wrote \"optional\". Nobody should be forced to install it, but \n> people should have the option to do so. Learn to read.\n\nMicrosoft makes it illegal for individual developers to implement the OOXML specification. So pleas pay the legal risks and cost of implementing a OOXML<->ODF converter. But you probably don't care about legal risks as you wrote in an earlier statement. Put your money where your month is, or are you just another Troll?: \n\nhttp://en.wikipedia.org/wiki/Internet_troll"
    author: "Knut Yrvin"
  - subject: "Great to hear!"
    date: 2007-11-03
    body: "Good to see that koffice is making progress!\nI have always used it on my machines for years now and always felt using openoffice a real burden (kind of like the feeling when using firefox although that isnt as bad ooo ;) )...\nThe only problems i had with koffice where related to ODF support which seemd to have been improved and tables (I could never get KWord to make proper tables with different size cells that dont crash KWord when being merged or split...).\n\nOther than that its a great light-weight Office Suite that just integrates well with my favorite desktop ;)"
    author: "hannes hauswedell"
  - subject: "hmmm"
    date: 2007-11-03
    body: "To be honest I just want an office suit that is just that, an office suit, word processor, etc.  And now that OpenOffice 3 seems to be looking at bundling Thunderbird in as an email client to tackle outlook, I am looking more seriously at KOffice.  So I am looking forward to see KOffice 2"
    author: "Richard"
  - subject: "Re: hmmm"
    date: 2007-11-03
    body: "Appears unlikely to be your reason as even if it is \"bundled\" you very likely won't have to install it just as you can choose which OOo apps to install at this point."
    author: "Impression"
  - subject: "Re: hmmm"
    date: 2007-11-03
    body: "Well, according to business users, an office suite usually does include PIM software.  This is why MS Office also comes with Outlook.  On the KDE side, to get similar functionality, you need both KOffice and Kontact (with KMail, KAddressBook, KOrganizer, etc.)."
    author: "Matt"
  - subject: "Re: hmmm"
    date: 2007-11-03
    body: "I think you have cause and effect mixed up.\n\nMS Office comes with Outlook, as an added value over the pre-installed stand-alone Outlook Express, therefore business users think an office suite usually contains PIM software.\n\nOn desktop systems with an open 3rd-party market one can have PIM suite and office suite from different software vendors, probably each a specialist in its field.\nIs is often even possible to switch either later one, due to the power of almost all players using open standards."
    author: "Kevin Krammer"
  - subject: "Re: hmmm"
    date: 2007-11-04
    body: "As it has been stated by other people, you can install exactly what you need and leave out the rest.  You have the option of just installed Base or Writer, and ignoring the other apps in the suite.  Thunderbird shouldn't make or break the decision."
    author: "T. J. Brumfield"
  - subject: "wating for KOffice 2"
    date: 2007-11-03
    body: "I'm eagerly awaiting KOffice 2. We're using OO atm in our business (we switched from MS Office to SO/OO already 6 years ago). Most of our documents are in .odf-format anyway, and 99% are simple letters with our corporate logo on top, simple layout and lots of text. I already did a test to open some in Koffice betas. After the GSOC KOffice got it 90% correct, only the Logo disappeared somehow. KOffice starts up instantly is fast and lightweight with a clean UI.\n\nWe still recieve .ppt/.doc/.xls files by mail. I still don't know how to handle these. OO will stay for sure, but for our own documents I'd really like to take KOffice.\n\nSo far: way to go KOffice team!\n"
    author: "Thomas"
  - subject: "Re: wating for KOffice 2"
    date: 2007-11-04
    body: "That is the biggest thing for me.  OOo is slow, but it handles importing Microsoft documents for me."
    author: "T. J. Brumfield"
  - subject: "Try and erase"
    date: 2007-11-03
    body: "Filter status for KOffice-2.0 is missing\n( http://www.koffice.org/filters/ )\n\nI'll recheck KOffice as fast as I'll be sure it gives some improvements\nwhere it was really lacking."
    author: "nae"
  - subject: "Amazing"
    date: 2007-11-03
    body: "It is amazing what great application suite such a small team of developers has created, and even more so what is happening now with koffice 2. \n\nIn my daily work koffice 1 is already perfectly usable, doing the job, and fast and fun to use. \n\nKeep the pace up, guys, I can't wait to see koffice 2 !"
    author: "Matt"
  - subject: "DTA - Kimalcorp.org"
    date: 2007-11-03
    body: "I offer my strong support for the KDE and Koffice teams in your efforts to provide us computer users with a much better PC technology experience.  Your reasons for supporting the ODF format are quite valid and understandable for anyone who has followed and understands the \"reality\" issues especially ooncerning OOXML.\n\nUnfortunately many people here in the USA are totally ignorant of the facts\nregarding ISO standards and certifications, and will continue to make stupid and completely (factually) false statements in support of Microsoft's disingenuous efforts at International open standards, simply because Bill Gates/Microsoft are an American entity and much of the KDE/Koffice and OpenOffice/ODF matter is European/Universal.\n\nKeep up the good work, and do not be discouraged by having to constantly and time consumingly correct and educate people, and refute the nonsense like that which I have read in comments section."
    author: "W. Anderson"
  - subject: "IBM Lotus Suite"
    date: 2007-11-04
    body: "This response is so late I wonder if anyone will even see it.\n\nThere are some developers who are a bit disenchanted with having to sign over copyrights to Sun and such, and haven't had their code included in OOo.  IBM is now committing to becoming a major contributor to OOo, but what parts?  I'm assuming much of the Lotus Suite's improvements will remain in IBM's fork so they can maintain copyright.\n\nLotus Suite is currently a fork of OOo 1, so they still have a long way to go to develop their fork.  I wonder if IBM would instead consider contributing to and partnering with KOffice 2.  The Lotus suite UI looked fairly sharp, and they seem to have a good idea for design.  Collaborating with the KOffice devs (KOffice also has a better UI than OOo), I'm sure you guys could knock out incredible future versions of KOffice.\n\nBig commercial support like that would be HUGE."
    author: "T. J. Brumfield"
  - subject: "Re: IBM Lotus Suite"
    date: 2007-11-05
    body: "This is a brilliant idea. Someone should get the Koffice devs together with Aaron Seigo as our head honcho or a PR person from our side to talk to IBM about it. KOffice would be much easier for them to work with than the bloated, outdated (1.0) version of Openoffice.org they're currently using for their suite."
    author: "Darryl Wheatley"
  - subject: "Re: IBM Lotus Suite"
    date: 2007-11-05
    body: "I also think this is a brilliant idea.  You could compare lines of code (~1 million for the entirety of KOffice vs ~ *5 million* for OO.o) and the number of paid developers working on each (*1* for KOffice IIRC (and he works exclusively on Kexi) vs ~50 on OO.o).\n\nAs far as I'm concerned, it's a no-brainer for IBM: KOffice simply gives a much better return on investment in terms of developer costs and has a vastly superior architecture."
    author: "Anon"
  - subject: "Re: IBM Lotus Suite"
    date: 2007-11-05
    body: "I also agree.  They should definitely be approached. A few of us have already spoken to Thomas Zander about some of the things from the *older* Lotus Smart Suite that we would like in KOffice.  If the new IBM Symphony/Lotus actually joined forces with Koffice to bring this about, we could really start the ball rolling on something huge."
    author: "The Vicar"
  - subject: "OpenDocument Foundation shifting support to CDF"
    date: 2007-11-04
    body: "How is the OpenDocument Foundation shift in support to the W3C Compound Document Format going to affect things (if at all)?"
    author: "Pumpkin Pie"
  - subject: "Re: OpenDocument Foundation shifting support to CDF"
    date: 2007-11-04
    body: "My guess is it will have no effect whatsoever. The \"OpenDocument Foundation\" has an official-sounding name but has nothing to do with actually directing ODF at all. See this blog posting for some discussion of this issue: http://www.robweir.com/blog/2007/10/cracks-in-foundation.html"
    author: "Paul Eggleton"
  - subject: "Re: OpenDocument Foundation shifting support to CDF"
    date: 2007-11-04
    body: "plus http://www.valdyas.org/fading/index.cgi/politics/pattern.html and as usual Boudewijn is right :)"
    author: "Sebastian Sauer"
  - subject: "Re: OpenDocument Foundation shifting support to CDF"
    date: 2007-11-04
    body: "Open-Document-Foundation is a Microsoft trademark."
    author: "nae"
  - subject: "Excellent"
    date: 2007-11-24
    body: "The KDE projects values are simply outstanding. Focusing on really open and free formats are the right thing to do, and KDE is at the front of the action.\n\ntruly excellent."
    author: "redeeman"
  - subject: "Re: Excellent"
    date: 2007-11-26
    body: "Agreed.  Attempting to support Microsoft's patent-encumbered Uh-Oh-XML file formats is both counterproductive and legally dangerous.  Microsoft could have avoided this whole mess by simply adopting the encumbrance-free ODF.  But they want strife, because they want to control what we do.\n\nI left Microsoft Windows for good several years ago, because I became very concerned about what I see as their ultimate goal--power over others at virtually any cost.\n\nRemember, Microsoft is acting just like a drug dealer who says, \"the first hit is free.\"  Don't believe me?  I quote:\n\n  http://www.news.com/2100-1023-212942.html\n\nNo thanks, Microsoft.  I'll stick with Freedom.  Every time.\n\n--SYG"
    author: "Sum Yung Gai"
  - subject: "Go guys go!"
    date: 2007-12-12
    body: "I really dislike that a company who has fallen so far off the beam as MS has is the company to whom many people look to for standards. It's time for global, universally accepted standards that are independent of companies who monopolistic interests are served by their own rules being adopted. MS had a role at one time in setting standards but that time has passed.\n\nAnd yeah, as you can tell from the tone of my writing, whenever I'm forced to use  a MS product I generally go 'ick!' and end up getting mad from the design errors."
    author: "Pete Dixon"
---
<a href="http://www.koffice.org">KOffice</a>, the office suite built on KDE technology and in the KDE Communtiy has <a href="http://www.linuxworld.com.au/index.php?id=1596080362&amp;rid=-50">recently</a>
gotten a lot of press, but is still often underrepresented. In this interview, some key KOffice developers tell us about the recent progress of KDE's Office suite, about Open Standards and how KOffice plays an active role in bringing Freedom to users. We have talked to Boudewijn Rempt, developer of Krita, core KOffice contributor and KOffice release manager, as well as to David Faure who has been taking part in the <a href="http://www.oasis-open.org">OASIS</a>, the organization that is responsible for advancing the OpenDocument (ODF) standard.






<!--break-->
<p>David Faure says:
<blockquote>We're taking part in the OASIS because we truly believe in Open Standards. KOffice standardises on OpenDocument. Free Software and open standards are a perfect match and the way to move forward for a society to ensure vendor-independent access to its data. We're actively participating in the OASIS since it matches our value, and we believe that one strong standard is in the best interest of our users.</blockquote></p>

<p>Boudewijn Rempt gives us an overview over what's happening in the KOffice community.</p>

<h4>How is KOffice 2 progressing?</h4>
<blockquote>We had a bit of a slow start -- porting to KDE4 took longer than we thought. Maybe we started our port a little too early. Large parts of most applications have been completely rewritten. We're making really good progress now, across the board of applications. The Google Summer of Code project to improve OpenDocument support in KWord has been a big success: we're still not completely done, but confidently expect KOffice 2.0 to have improved support for OpenDocument in key areas such as spreadsheet and word processing.</blockquote>

<h4>What will be the main features for KOffice 2?</h4>
<blockquote>We've taken integration to the next level. Applications now offer an interface optimized for a particular task, but they all use the same small-grained objects to compose documents from. That means richer documents, more consistency in the user interface and excellent expandability.</blockquote>

<h4>What target users do you have in mind for KOffice?</h4>
<blockquote>Right now, home users, students and people running small businesses. Additionally, KOffice offers a very flexible and rich platform for implementing office-type applications for specific markets, such as education or vertical markets.</blockquote>

<h4>Can you explain some platform aspects of KOffice 2(.x)?</h4>
<blockquote>OpenDocument is totally native for us. KOffice has had a large part in establishing the standard and continues innovating within the standard and contributing to new versions of the OpenDocument standard. OpenDocument really is the platform we are building on, it informs many of our design decisions, without forcing us to implement an OpenOffice clone at all.</blockquote>

<h4>KOffice has some time ago switched to OpenDocument as default file format. What is the motivation for this move?</h4>
<blockquote>Cooperation has always been in the forefront of KOffice development. We developed the libwv2 .doc library together with Abiword, and the libwpd WordPerfect library together with OpenOffice. Developing our own, underspecified file format just didn't make any sense when there is a chance to cooperate on a widely used, rigorously specified file format. Besides, we listen to our users and hear that they want to exchange documents without enforcing their customers, teachers or peers to use the exactly same application in the exactly same version.</blockquote>

<h4>Why do you think OpenDocument is the right thing to embrace for KOffice?</h4>
<blockquote>It's been beneficial in two ways: the KOffice involvement has kept OpenDocument from becoming the memory dump of OpenOffice internals detractors so often allege it to be. And KOffice has gained recognition, compatibility and also quite a few features in the process.</blockquote>

<h4>There is a Windows version of some of the KOffice applications coming up. In how far does this new platform influence the way the KOffice community works?</h4>
<blockquote>Well, it's actually *all* of the KOffice applications that will be available on all three platforms: Unix/X11, OS X and Windows. Not that that changes much for us: we have a very clear vision of what we want to achieve, and we're making good progress in that direction. We are ready to welcome the influx of users the wider availability of KOffice means -- and actually, we're already get quite a few inquiries about where people can download our software.</blockquote>

<h4>Recently, the ISO standardisation process of OfficeOpenXML ("OOXML") has gained a lot of public attention. What are the implications of OpenXML as ISO standard next to ODF for Free Software applications?</h4>
<blockquote><p>The standardisation process of OfficeOpenXML has turned sour, not in the least because Microsoft couldn't resist the temptation to cheat. Right now we're seeing evidence of a concerted campaign at discrediting OpenDocument vis-a-vis OfficeOpen XML. That's unfortunate, to say the least.</p>

<p>If OfficeOpen XML becomes an ISO standard, we will, in all likely hood, still not spend time on supporting it. The standard is enormous, very complex and to a large extent so badly specified that a full implementation is probably even harder than implementing the old Microsoft binary file formats. Add to that patent encumbrances and problems with copyrighted elements -- and our conclusion is that we prefer to concentrate on making KOffice a great set of applications that are satisfying to use and satisfying to develop.</p></blockquote>

<h4>There were rumors about an ODF library in KDE. Can you update us on the
progress there?</h4>
<blockquote>We're committed to developing such a library. Only today, some first steps have been set in the direction of that goal. Still baby steps, and the ODF library won't be available with KDE 4.0 or even KDE 4.1, but we are keeping that goal in mind when developing KOffice.</blockquote>

<h4>Recently, more and more governmental agencies and public bodies embrace ODF as their default document standard. Can you give us reasons for that?</h4>
<blockquote>There are many good reasons for governments and public bodies to support OpenDocument, and no good reasons not to support it. OpenDocument documents can be read and edited on every computing platform, present and future. Wide availability is good for citizens, a solidly specified, unencumbered standard is good for archiving, and the very implementability means a free market with genuine competition, which is good for the budget.</blockquote>

<p>Thank you very much. I'm personally really looking forward to KOffice 2.0. I think it's the office suite with the greatest potential in the market right now.</p>