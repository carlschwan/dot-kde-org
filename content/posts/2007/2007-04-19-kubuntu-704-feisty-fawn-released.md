---
title: "Kubuntu 7.04 (Feisty Fawn) Released"
date:    2007-04-19
authors:
  - "imbrandon"
slug:    kubuntu-704-feisty-fawn-released
comments:
  - subject: "Thank You very much Kubuntu Team!"
    date: 2007-04-19
    body: "Nothing else to say."
    author: "David"
  - subject: "Mandriva?"
    date: 2007-04-19
    body: "What's special about Kubuntu that such story with uninformative marketing blob gets posted but no one about Mandriva Spring?"
    author: "Anonymous"
  - subject: "Re: Mandriva?"
    date: 2007-04-19
    body: "Nobody has submitted a story.  Give us a story with a KDE angle and we'll publish (note I didn't publish the above story).\n"
    author: "Jonathan Riddell"
  - subject: "Re: i love opensuse ?"
    date: 2007-04-20
    body: "ok i am a little astonished, i thought the issue of posting distribution news was already solved :)\n\nanyway, i would like to thank opensuse fame for paying attention for people who still don't have a direct net connexion in home, and the organisation of the build service is a gift for us, i can download the latest kde4 svn packages from work( under windows xp) go home and just fire yast and see the glory of koffice in action. even amarok kind of work now.\n\nso thanks opensuse, thanks kde for all those great applications, koffice and  specially kplato team.\n\nfriendly mimoune.\n\nps; i use xfce4, all those stories about the gnome vs kde vs (?)DE is a nosense it is all about applications !!!!!!!"
    author: "djouallah mimoune"
  - subject: "Re: Mandriva?"
    date: 2007-04-19
    body: "Ubuntu is one of the most hyped distributions nowadays. (and its good and getting better and better I think).\n\nKubuntu is the KDE-promoting flavor of Ubuntu so a part of the attention that Ubuntu receives (and it receives a lot) is always going into Kubuntu and KDE as well.\n\nThat's a good thing, isn't it?\n\nMandriva should be promoted as well here at the dot, since it's shipping with KDE as default desktop."
    author: "David"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "Mandriva is the best distribution out there. urpmi is fantastic. I also like the default folders for music and video, that should get standardised.\n\nUbuntu is a Gnomification trojan horse. KDE needs more attention. Kubuntu is nost satisfactory, maybe SimplyMepis is."
    author: "andre"
  - subject: "Re: Mandriva?"
    date: 2007-04-21
    body: "Mandriva.... is.... sluggish.... \n(at least on my machine, compared to kubuntu)"
    author: "Humberto Massa"
  - subject: "Re: Mandriva?"
    date: 2007-04-24
    body: "Kubuntu... is... sluggish...\n(at least on my machine, compared to ArkLinux)\n\nor even better yet:\n\nrandom($distributions)... is... sluggish...\n(at least on my machine, compared to random($distributions))\n\n;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Mandriva?"
    date: 2007-04-22
    body: "Normally I would think you were being a bit paranoid, but after\nwatching a critical, lost-all-my-data bug remain in\nEdgy for a whole *six* months before being fixed, I have come\nto the conclusion that Kubuntu is indeed a second class citizen\nin the Ubuntu community.  (I am talking of course about the\n\"Safely Remove\" option removing the mounted USB disk icon\nfrom the Desktop *before* the drive was actually unmounted.\nThis was a very popular bug in Launchpad...)\n"
    author: "C.S."
  - subject: "Re: Mandriva?"
    date: 2007-04-22
    body: "Heh, I hadn't lost any data yet, and that's more than I can say for OS X 10.4. :->  I was just delighted that I could actually *unmount* removable media under Edgy.  That's more than I could say about Dapper, and it's the *stable* release!\n\nKubuntu is definitely a second-class citizen.  I don't remember if it was Edgy, Dapper, or which release, but one of the releases included kubuntu-desktop packages that were *not* the final release, and therefore came loaded with bugs.  No such problem with ubuntu-desktop, though!  \n\nAnd you should have seen the wailing and moaning when Kubuntu was announced.  People actually talked about dumping Ubuntu if it was going to have the taint of KDE around it!  Oh, the horror! :-D\n\nAnyway, I'm a happy KDE user; I haven't been this happy with a KDE-centric distribution since the first Mandrake release.  It has rough edges (I haven't upgraded to Feisty, so I may change my tune in a day or two) but even though the Kubuntu team has modified KDE to be more Ubuntu-friendly it's still one of the least adulterated KDE desktops I've seen outside of Slack and Gentoo."
    author: "regeya"
  - subject: "Re: Mandriva?"
    date: 2007-04-23
    body: "In edgy, some of the core libraries were buggered in such a way that for some people, including myself, random crashes would occur in certain X applications, such as Firefox, Amarok and (more annoyingly) kwin, kdesktop, etc. Bugs that I haven't experienced anymore since I switched to Feisty. Edgy was very buggy in this respect, whereas Dapper, although less flashy on technologies, was way more stable."
    author: "NabLa"
  - subject: "Re: Mandriva?"
    date: 2007-04-19
    body: "http://dot.kde.org/addPostingForm"
    author: "Marc"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "yes Mandriva ++! I cannot submit Dot articles as I use Mandriva for everything except for my KDEs which I compile from KDE svn (3.5.6 and 4). \nI used Spring to upgrade to a new kernel so that my sound now works properly and it all went swiftly witht he new kernel listed at boot start and no mess like the one I do when I compile a kernel.\nAlso thanks to the Mandriva in the IRC channel as each time I ask something I always get an answer. The one time I overwrote my gcc with icecc for example they got me fixed quickly without a hundred of Mb packages to reinstall.\nMaybe next time the writer should enquire about other distros and do a Distro Point. \nWe already had the German book promoted and such news are of interest to only a minority if users. \nI wish a distro like Mandriva would go be interested in Edu and ship an edu-oriented edu distro based on KDE."
    author: "annma"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "A distro point ? That would make a good dot article ! :)\nso which distribution are kde centric ?\n- Mandriva (and PCLinuxOS)\n- Slackware (and Slax)\n- Pardus\n- Knoppix\n- Sidux\n- freespire\n- SimplyMEPIS (not sure)\n- Belenix (opensolaris based)\n- desktopBSD (freebsd based)\n\nOk that's just a short list, but it's a good indicator that KDE is simply great :)\nThe interesting thing is that I see a new trend among distributions: releasing a special kde edition. For example :\n- (K)ubuntu\n- Fedora core (\"kde spin\" scheduled for version 7)\n- Mint (version 2.2)\n- Vector SOHO edition\n\nI'd prefer to see article like \"Distro XYZ v1.23 released\" on distrowatch.com instead of here :\\\n(no offense meant to the author)\n"
    author: "shamaz"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "You forgot Sabayon Linux, one of the best and fastest-growing of the newer distros, and which has KDE as default desktop (though the DVD offers GNOME, Enlightenment, Xfce, Fluxbox...)."
    author: "Francesco"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "Special about Kubuntu?\nI wish someone french-speaking could report on those 1250 Kubuntu workstations for the French governement."
    author: "reihal"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "hmmm well, I'm french. But here is my vision about the event you are refering :\nThe french parliament (Assembl\u00e9e Nationale) decided to switch to Linux. THIS is the great thing. In the end, kubuntu has been chosen instead of Mandriva because Linagora offered a cheaper maintenance for Kubuntu than the bundle proposed by Mandriva.\nSo does this REALLY  make kubuntu more special than Mandriva ?\nno. It was just a matter of money :\\"
    author: "shamaz"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "Can you research it a little and write an article for the dot? It will be appreciated, it's about KDE after all.\n"
    author: "reihal"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "This is quite old imho, and it has already been advertized on the web \n( http://linux.slashdot.org/article.pl?sid=07/03/12/0257212 ) and on planetkde.\n"
    author: "shamaz"
  - subject: "Re: Mandriva?"
    date: 2007-04-20
    body: "That's about Ubuntu, not Kubuntu. Slashdot doesn't count.\nAnd it's still news, I haven't read an article about it yet.\nGreat material for the dot."
    author: "reihal"
  - subject: "Re: Mandriva?"
    date: 2007-04-21
    body: "The article on slashdot talk about ubuntu, but it is really Kubuntu."
    author: "shamaz"
  - subject: "Good timing"
    date: 2007-04-20
    body: "Just last weekend I had tested out Edgy Eft in a virtual machine to test out how well Kubuntu worked, and I was left rather unsatisfied (some problems, like random junk somehow getting thrown into the middle of entries in /etc/passwd).  I was planning on waiting till Feisty came out before I'd switch my laptop over (from SuSE 10.3... The package manager taking all day to open is a complete PITA, as well as several other annoyances).  I'm gonna seriously consider switch my laptop over to Feisty (I like that they use NetworkManager by default now, I'll probably switch over to using KPowerSave as well depending on how well the Guidance power manager works, which didn't even start at all on my previous tests).\n\nAs a side note, I personally like hearing about new releases of distributions that include KDE, especially when the announcement includes some KDE centric details (versions and stuff is very useful as well)."
    author: "Sutoka"
  - subject: "Re: Good timing"
    date: 2007-04-20
    body: "The Guidance power manager has got many improvements. It's simpeler than KPowerSave, but powerfull enough for my needs.\n\nI think it's good to announce releases of KDE centric distributions on the dot. I don't understand that every time there is a distro release, some people are complaining. KDE has to be installed somewhere and why not promote a linux distribution/BSD/... that uses it as main DE."
    author: "stivani"
  - subject: "Name"
    date: 2007-04-20
    body: "I think it's very confusing to have multiple names for one distro (Kubuntu, Xubuntu etc.). They should forget all those and use just Ubuntu."
    author: "Petteri"
  - subject: "Re: Name"
    date: 2007-04-20
    body: "Actually, I think it helps clarify it.  When I describe Linux to others, I usually ask one question:  Which do you feel more comfortable with: Mac OS X or Windows?  I then point them toward either Ubuntu or Kubuntu respectively.  The name keeps the two distributions mostly seperate in both my mind and the mind of the person I'm talking to since most people don't really understand the difference between OS (Kernel) and Windows Manager.  The seperate names help to accentuate that."
    author: "Rob Oakes"
  - subject: "Re: Name"
    date: 2007-04-20
    body: "If I remember correctly Ubuntu first started as a Gnome distro only (means Gnome was chosen as default DE to focus resources). The Kubuntu project started later to provide KDE as default DE, and wasn't part of Ubuntu then.\nI also installed Kubuntu Feisty last week on a desktop and install and setup was mostly smooth and very easy (can't say that about opensuse-10.2 which I also tried, the install was o.k. but the additional setup needed scary).\nI might chose Kubuntu for my laptop the next time I hose up my Gentoo system on it."
    author: "Tlaloc"
  - subject: "Re: Name"
    date: 2007-04-23
    body: "I agree. Ubuntu - Gnome Edition and Ubuntu - KDE Edition would be better. (this wasn't my idea, read it on a blog somewhere)."
    author: "Ian Monroe"
  - subject: "kolab dependancy broken"
    date: 2007-04-20
    body: "hey guys,\n\nthe kolab depandacy is still broken... bug reports exist, no one seems to care.\n\n...muesli"
    author: "muesli"
  - subject: "Re: kolab dependancy broken"
    date: 2007-04-23
    body: "Ask Jonathan riddel or some other dev on amarok-devel on IRC, they might be able to help..."
    author: "superstoned"
  - subject: "System configuration"
    date: 2007-04-20
    body: "I would wish Kubuntu to help to unify system configuration tools across distributions."
    author: "andre"
  - subject: "OSX-like effect on icon activation"
    date: 2007-04-20
    body: "Jonathan (or whoever), can you tell me which is the name of the patch of this effect? I mean, then when you activate (double-click) an icon you get a rapid \"zoomed\" icon that suddenly disappears to show you have really activated that icon. It's really nice and a good usability improvement1 it should be in KDE default branch!"
    author: "Vide"
  - subject: "Re: OSX-like effect on icon activation"
    date: 2007-04-21
    body: "Icon execute feedback:\n\nhttp://www.kde-look.org/content/show.php/Icon+execute+feedback?content=52994"
    author: "kowal"
  - subject: "Dapper"
    date: 2007-04-21
    body: "And how to upgrade from dapper?"
    author: "bert"
  - subject: "Re: Dapper"
    date: 2007-04-21
    body: "First you need to upgrade to Edgy and download all the updates with that.  Then you'll want to upgrade to Feisty.\n\nIf you have experience with aptitude or apt-get and using its dist-upgrade feature, you can simply edit your /etc/apt/sources.list file by replacing all occurrences of \"dapper\" with \"edgy\", do a dist-upgrade, then change \"edgy\" to \"feisty\" and upgrade again.\n\nAnother easy way to upgrade like that would be to just reinstall it completely from a Feisty CD."
    author: "Matt"
  - subject: "CD vs DVD"
    date: 2007-04-21
    body: "I'm wondering what's in the DVD version that's not in the CD version. I mean, those 3+ Gigabytes of data have to contain SOMETHING extra.\nAlso, the link to the torrent of the DVD version of Feisty Fawn doesn't seem to work or else my KTorrent-program is malfunctioning (again).\nWhen browsing through the download sites I can see 7.04 daily builds, however, they're dated April 18, not 19. Has there been any change between the daily build of April 18 and the release on April 19? if not, I can just download a DVD iso from one of those sites."
    author: "Bob"
  - subject: "Re: CD vs DVD"
    date: 2007-04-21
    body: "The DVD has the whole main repository and has ubuntu/kubuntu/xubuntu"
    author: "Anonymous"
  - subject: "whoeeee"
    date: 2007-04-22
    body: "i used suse 10.0 as my os. i've tried ubuntu 5.10 before but it was hard to install. i dont have a plan to change to any os because my suse is work perfectly and its very easy to set up.how about kubuntu? is kubuntu coming with nvidia driver? or dvd player and any basic needed? especially for me."
    author: "boyke"
  - subject: "Re: whoeeee"
    date: 2007-04-23
    body: "wait to the next version for nvidia, but it's easy to install with the new proprietary sofware management tools (eg amarok downloads your mp3codec if it needs it)."
    author: "superstoned"
  - subject: "Guidance cannot be configured"
    date: 2007-05-08
    body: "This is more a problem then a comment, but as nobody posted at http://ubuntuforums.org/showthread.php?t=432945 I'm asking some help here.\nI am using Kubuntu 7.04, upgrade from 6.10, all packages updated, on HP nx6110 laptop. I tried Klaptop, Guidance and KPowersave and each has a problem. Only Klaptop detects the power/battery. \nGuidance and KPowersave cannot be configured (option are grayed out). They don't detect power/battery status nor they display remaining battery time. Only after I installed uswsusp hibernation started to work and after I configured CPU Module for  p4_clockmod Guidance shows CPU frequency. But the battery remains the problem so I use Klaptop.\nThere are too many power management packages in the repository. How can I solve this?"
    author: "timur"
---
<a href="http://kubuntu.org/announcements/7.04-release.php">Kubuntu 7.04</a>, development codename "The Feisty Fawn", was released today. Free CDs are available through <a href="https://shipit.kubuntu.org/">ShipIt</a>.  Kubuntu prides itself on working towards the perfect KDE GNU/Linux solution, and with this latest release the development team worked harder than ever to do just this. 7.04 includes <a href="https://wiki.kubuntu.org/FeistyFawn/Beta/Kubuntu">many updates, new features, and the latest releases of your favorite applications</a>. KDE 3.5.6 is of course the desktop of choice, with K3b up to the milestone 1.0. The development team has worked hard to incorporate the best usability and accessibility features, to tame the edginess, and to provide a stable and secure computing environment for everyone. So stand with us and congratulate the Kubuntu development team and its many users in a job well done.



<!--break-->
