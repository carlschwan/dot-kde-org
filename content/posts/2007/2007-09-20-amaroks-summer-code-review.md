---
title: "Amarok's Summer of Code Review"
date:    2007-09-20
authors:
  - "sruiz"
slug:    amaroks-summer-code-review
comments:
  - subject: "When I thought KDE4 couldn't get any cooler"
    date: 2007-09-20
    body: "Amarok 2 looks to be a jaw dropping improvement over the previous version. Will the Plasma used in the context view be able to use data engines and visuals on the desktop and vice versa?\n\n"
    author: "Skeith"
  - subject: "Re: When I thought KDE4 couldn't get any cooler"
    date: 2007-09-20
    body: "It actually would make some sense to allow Amarok plasmoids to be plasmoids on the desktop. This might happen at some point, but it is not a priority.\n\nBeing able to put desktop plasmoids in the Amarok context view would be kind of silly IMO. :)"
    author: "Ian Monroe"
  - subject: "Re: When I thought KDE4 couldn't get any cooler"
    date: 2007-09-20
    body: "For me it sounds just logical that a plasmoid can be used technicly anywehre you want to. It doesn't metter if you put it on your desktop into your Deskbar (fka kicker) or into Amarok. I think that is the idea behind Plasma that it can be used anywhere if you implement it.\n\nAnd maybe you don't want to put Desktop Plasmoids into Amarok but maybe anybody else. It could be usefull to put the clock into Amarok or something like that.\n\nI gueas that every plasmoid can be used on either one Desktop and Amarok."
    author: "baumhaus"
  - subject: "Re: When I thought KDE4 couldn't get any cooler"
    date: 2007-09-21
    body: "You are absolutely right! Silly would be not to be able use desktop plasmoids in Amarok CV and Amarok plasmoids on the desktop because that would imply two different and incompatible implementations of basically the same framework/functionality.\nThe fact that most people doesn't want to have desktop plasmoids in Amarok is entirely irrelevant. "
    author: "Fredrik Larsson"
  - subject: "Re: When I thought KDE4 couldn't get any cooler"
    date: 2007-09-21
    body: "It's Leo's baby, but I know Amarok plasmoids are in-process to Amarok. Libplasma was originally designed with only the plasma process in mind. It just happens that Amarok needed to solve similar problems as libplasma so now Amarok uses it."
    author: "Ian Monroe"
  - subject: "Re: When I thought KDE4 couldn't get any cooler"
    date: 2007-09-23
    body: "exactly. isn't there a media center form factor? not that I am saying amarok should and must use that form factor, but that they were dreamed up to accommodate for this very sort of thing"
    author: "Mike Wyatt"
  - subject: "Playlist: Sorting and Information"
    date: 2007-09-20
    body: "Quick question about the playlist. While I do like the new fancy layout of the playlist, I'm wondering about two things:\n\n1. In the previous column-based playlist, users can add/remove columns of information. With this new playlist, are we only limited to those 4 pieces of information in that screenshot?\n\n2. One of the advantages of the column-based playlist is being able to sort the list in ascending/descending order, based on title, artist, album, time, etc. Will that still be possible in the new playlist?\n\nOther than that, I like it! I used to doubts as to the placement of the playlist (moving from \"playlist-centric\" to \"context-centric\") but as I observed by Amarok usage recently, they don't really affect it. Keep up the great work!"
    author: "Jucato"
  - subject: "Re: Playlist: Sorting and Information"
    date: 2007-09-20
    body: "Yep we'll figure out sorting and customizing what info is visible, no worries."
    author: "Ian Monroe"
  - subject: "Re: Playlist: Sorting and Information"
    date: 2007-09-20
    body: "That's great news then. More power to you! :)\n\n(Can't wait for the first beta that I would be able to run in my KDE 4 beta)"
    author: "Jucato"
  - subject: "Re: Playlist: Sorting and Information"
    date: 2007-09-24
    body: "In current column-based playlist I always have some difficulty to organise it, as some tacks has very long names, others long album name, others two or three artists, so I could not see it all.\nNew playlist, it seems, solve this in rather elegant way, very good work.\nMy only concern, sorting aside, is in-place tag editing, which is very handy sometimes. But it is solvable too, isn't it? "
    author: "pilpilon"
  - subject: "Playlist Time Alignment"
    date: 2007-09-20
    body: "No, not another comment bashing the new playlist. ;)\n\nJust a comment about the alignment of the track length display. Wouldn't it be better to have it right aligned rather than left aligned with what I think is the album title?\n\nIn the screen shot, the track name of the second item is being cut short purely because the album title is long. The whitespace to the right of the track length is essentially being wasted. Not having the track lengths aligned (with each other) also makes it difficult to quickly scan the track lengths of those that are visible too as the eyes have to keep jumping to left and right as the scan down (or up).\n\nI believe it's not a difficult thing to change which tends to make me think that the current layout was chosen based on some merit. I'd be interested to hear what that merit is, but would likely still lean toward right aligned."
    author: "Jason Stubbs"
  - subject: "Re: Playlist Time Alignment"
    date: 2007-09-20
    body: "Nah, it wasn't chosen on some merit, it's just how I first implemented it."
    author: "Ian Monroe"
  - subject: "Re: Playlist Time Alignment"
    date: 2007-09-20
    body: "May I propose dropping the two-coumn layout of a playlist item in favor of just a line-based solution (like currently OSD shows its information)? Since the width of the playlist is probably going to be cut by >50% compared to Amarok 1.x, I'd say it's really important to not waste any more horizontal space than absolutely necessary..\n\nI know it's nitpicking, I really appreciate the work being put into Amarok 2, and am really looking forward to using it myself!"
    author: "bsander"
  - subject: "Re: Playlist Time Alignment"
    date: 2007-09-20
    body: "No thats not nitpicking, thats saying it should be completely different. You would have to make a mockup or something to show what your trying to say."
    author: "Ian Monroe"
  - subject: "Re: Playlist Time Alignment"
    date: 2007-09-20
    body: "Like I said, just as Amarok 1 does with OSD. I found a screenshot at http://www.linuxos.sk/media/IMG/amarok/osd.jpg\n\nBasically it's just two lines of text, so no extra vertical alignments of (as in the screenshot) for instance the album and the track length. I think it'll save horizontal space that way :)"
    author: "bsander"
  - subject: "Re: Playlist Time Alignment"
    date: 2007-09-21
    body: "Ah ok, I see what your saying. Yea that is nitpicking. :) Sure we'll play around with it."
    author: "Ian Monroe"
  - subject: "Re: Playlist Time Alignment"
    date: 2007-09-20
    body: "The article does mention being able to customize the information displayed in the playlist cells. It sounds like you might already be able to \"deselect\" some of the information and implicitly make it one column."
    author: "Justin Noel"
  - subject: "Complements to the chef"
    date: 2007-09-20
    body: "Great job Ian & Leo.. I was going to post about sorting as well but that question's already been answered. Best of luck with your future work, and I'm looking forward to the beta too :D *nudge nudge* "
    author: "Rahul"
  - subject: "Amarok Plasmoid ideas"
    date: 2007-09-20
    body: "Well, i've got some idea's for additional Amarok plasmoids:\n\n1. A full screen \"now playing\" plasmoid replacing AmarokFS\n2. no.1 idea implemented as an animated wallpaper :-)\n3. Visualization plasmoid(s)"
    author: "Fred"
  - subject: "Re: Amarok Plasmoid ideas"
    date: 2007-09-20
    body: "Well, that would be plasmoids for the desktop, right? It's cool that Amarok2 uses plasmoids too, but AFAIK there's some \"restrictions\" (maybe modifications would be a better word). One example is how the positioning of plasmoids.\n\nI've seen many great ideas of plasmoids, and I'm sure we'll see lot of cool them  in the near future. :)"
    author: "Hans"
  - subject: "Re: Amarok Plasmoid ideas"
    date: 2007-09-20
    body: "Indeed, those sounds like plasma plasmoids, not Amarok plasmoids. :)\n\nAmarok will have a full DBus interface of course, so please make cool plasmoids for the desktop. :)"
    author: "Ian Monroe"
  - subject: "Re: Amarok Plasmoid ideas"
    date: 2007-09-20
    body: "I second Eean :-)\n\nAnd even better, since we have dropped support for the old player window, it would be really cool if someone were to write a cool desktop plasmoid for taking over some of its functions"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Amarok Plasmoid ideas"
    date: 2007-09-20
    body: "< Edit to my post: remove a \"how\" >\n\nAbout the player window, isn't that what leinir is investigating? Oh. Maybe we'll see a THEMEABLE player window - now that would reduce one of the feature requests we often see."
    author: "Hans"
  - subject: "Amarok - Plasma portability"
    date: 2007-09-21
    body: "Awesome work!!\n\nI got a question about Amarok's Windows port. AFAIK plasma will not be ported to non-X platforms, so what will happen to Amarok on those platforms?\n\nThanks in advance. :)"
    author: "Lucianolev"
  - subject: "Re: Amarok - Plasma portability"
    date: 2007-09-21
    body: "I think the plasma library (libplasma?) that Amarok will be using will mostly be completely portable to whatever platforms QGraphicsView supports.  The only problems (that I heard of) were that some X11 specific code is/was in 'libplasma' so the Amarok devs were helping move it out (or at least thats how I remember what I think maybe was happening... possibly!)"
    author: "Sutoka"
  - subject: "amarok and podcasts"
    date: 2007-09-23
    body: "Hi,\n\nPlease allow me to give a some feedback on  amarok 1.4.6-1 in KDE 3.5.7 (Debian testing):\n\nFirst, I noticed that amarok is not associated to itpc:// protocol, which is the protocol related to podcast in iTunes (I presume).\n\nAdditionally, it would be confortable if amarok could harvest the podcasts and media from a URL given by the user.\nFor example, this URL contains tens of podcasts, and I did not find a fastest way than to open each of them individually in amarok :\nhttp://www.radiofrance.fr/services/rfmobiles/podcast/index.php?channel=5&g=EMI&cdid=\n\nFinally, once all these podcasts are imported in the playlist panel, it would be handy to be able to move and sort them. That is not possible in amarok 1.4.6.\n\nThanks for your jobs, and i have good hope that amarok 2 will especially focus on ergonomy improvement.\n\nThanks and Regards,\nDavid"
    author: "David"
  - subject: "Re: amarok and podcasts"
    date: 2007-09-23
    body: "oops : it is possible to move and sort podcasts in 1.4.6. Sorry."
    author: "David"
---
  This year, Amarok had two summer of code projects under the KDE umbrella. Both of these projects have finished while remaining in continued
  development and were extremely successful. Read on to learn about two
  innovative additions to the Amarok project.





<!--break-->
<h3>Ian Monroe, Model/View Implementation and Usability Improvements for
  the Playlist</h3>

<img src="http://static.kdenews.org/jr/amarok-soc-1-wee.jpg" style="margin:10px; width:288px; height:439px; float:right"/>

<p>
  Amarok 2.0 is going to provide users with a new interface - one which is more
  refined, intelligent and with a greater emphasis on usability. Ian's job was
  to use the superior Qt 4 model-view architecture to create an effective and
  efficient playlist for listening. As a second time Summer of Code student, Ian
  got straight to work on his project. Conventional playlists have always been
  displayed as a typical column based list view, with a dense amount of
  information in a rather unreadable format. Ian's interpretation involves a
  customisable view for the display of playlist items, allowing for more complex
  displays of track information. For example, displaying album artwork for a
  track or groupings on an album basis could be possible. A little more
  under-the-hood, Ian focused on optimising the playlist to be capable of
  efficiently handling many thousands of tracks in the playlist. This is
  something which was not possible in Amarok 1.x due to
  technical constraints.
</p>

<br clear="all" />

<h3>Leo Franchi, Web Services Integration</h3>
<img src="http://static.kdenews.org/jr/amarok-soc-2-wee.png" style="width:300px; height:242px; float: left; margin: 1ex"/>

<p>
  The Amarok project has always striven to provide users with the best
  experience when playing music, allowing them to <i>rediscover their
  music</i>. Leo was a first time Summer of Coder and his project was to
  integrate various web related projects into Amarok. Originally, ideas included
  upcoming concerts, artist trivia knowledge and more - however, it soon became
  clear that there were much larger obstacles in achieving this aim. There was
  simply no adequate place to display the information. Thus, the Context View
  was born, a prettier, more powerful upgrade to the Context browser seen in the
  Amarok 1 releases. Rightfully so, Leo decided that the Context View was more
  central to the features of Amarok 2, and proceeded to import the KDE 4
  Plasma workspace as the backend of the new Context framework. After
  weeks of hard work, there is a wonderful vector graphics based Context View
  which shows context sensitive information relating the the currently playing
  song. In line with KDE 4's Plasma, custom applets can be created as
  plug-ins to display virtually any information (eg, guitar tables). There
  are a few applets already implemented, such as track information and lyrics
  fetching.
</p>





