---
title: "KDE 4.0 Release Schedule Finalised"
date:    2007-03-21
authors:
  - "sk\u00fcgler"
slug:    kde-40-release-schedule-finalised
comments:
  - subject: "KDE 4 "
    date: 2007-03-21
    body: "It seams KDE4 is getting closer and closer :) I'm just curious though how many changes we'll see in the applications. A freeze in June is really close, but I'm happy to see there is such a long time to stabilize KDE4.\n\nI guess it's the difference between \"KDE4\" and \"KDE 4.0\". It's been emphasized before (can't find the blog entry anymore), and I think it's important to mention it again.\n\nI guess you can say KDE 4.0 brings cool stuff in the libraries, and some cool new apps. In KDE 4.1, more and more apps start taking advantage of the new libraries. This also happened with KDE 3.1, or Mac OS X 10.0."
    author: "Diederik van der Boor"
  - subject: "Re: KDE 4 "
    date: 2007-03-21
    body: "Sorry two corrections:\n- first I like to thank very much for KDE4, my post seams a bit harsh.\n- \"KDE 3.1\" should be \"KDE 3.0\" off course. KDE 3.1 and Mac OS 10.1 / 10.2 showed more features in the libraries."
    author: "Diederik van der Boor"
  - subject: "Re: KDE 4 "
    date: 2007-03-21
    body: "it's even more like the situation with 2.0 than 3.0. 3.0 had few new library features and didn't introduce any really large subsystems. 2.0 had a large number of new infrastructure pieces, new apps (file manager, desktp and panels =) and took a long time to get out the door ...\n\nbut yes, you're right that we've been down this path before and it took a couple releases for apps to \"catch up\" with the library changes.\n\npersonally, i can't wait to get back to straight app devel ;)"
    author: "Aaron Seigo"
  - subject: "Re: KDE 4 "
    date: 2007-03-21
    body: "> It's been emphasized before (can't find the blog entry anymore)\n\nhttp://www.kdedevelopers.org/node/2600 ?"
    author: "Anonymous"
  - subject: "Re: KDE 4 "
    date: 2007-03-22
    body: "I say push it back 8 days and make it October 31 Haloween!! I know stupid commnent but a haloween release would be interesting..."
    author: "lostson"
  - subject: "Thanks!"
    date: 2007-03-21
    body: "I'm very happy with this announcement and would like to thank the release team, core developers, and everybody involved writing this release plan. Hopefully it's going to be easy to stick to it, and that on the 23th of October the bottles of champaign can be opened!\n\nAs Diederik already did, I wan't to emphatize that 4.0 is mostly the core, and the new foundation, not the whole \"city\" of apps.\n\nCongratulations!"
    author: "Niels van Mourik"
  - subject: "Awwww Yeah!"
    date: 2007-03-21
    body: "Great to hear guys!\n\n"
    author: "Ben"
  - subject: "It\u0092s alive !!"
    date: 2007-03-21
    body: "\nYeah, it is almost here, the idea that I can install my beloved kde application in my work, and show them the glory of open source is really exciting, software like kplato, and koffice is a wonderful tools for our daily job, so thanks all the men and women who make it possible. Of course some open source fanatic will say, using free software on closed OS is immoral and bad, so my friend be prepared for an avalanche of new happy  users.\n\nCheers, \n\n\n  \n"
    author: "djouallah mimoune"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-21
    body: "will kde 4.0 be able to install on other systems like mac os.... this because you say you want to install in your work ????\n"
    author: "geoback"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-21
    body: "geo take a look at http://ranger.users.finkproject.org/kde/index.php/Home\n\nby the way in work i use windows ;)\n\nfriendly "
    author: "djouallah mimoune"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-21
    body: "in work?\nI think you meant \"at work\" :)"
    author: "Patcito"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-21
    body: "KDE, the desktop will most probably not run on Windows (well, unless someone wants to and makes it run). kdelibs however is actively developed also for Windows and Mac OSX and will run natively on those platforms. (Development version already do.)\n\nThat means that you'll be able to use your favourite KDE applications on other operating systems, too. (Of course as long as they care about portability in their own code.)\n\nWhether or not kdelibs on Windows and Mac OSX will be sufficiently usable by the time KDE 4.0 comes out (or earlier) is another question. This is not directly related to this release schedule since we're talking about the whole desktop here.\n\nI've heard from the Amarok people that they really want to have Amarok running on Windows, and personally, I'm sure other app developers think the same way. kdelibs should not stand in the way of this."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-21
    body: "> the desktop will most probably not run on Windows\n\ns,most probably,,\n\n> (well, unless someone wants to and makes it run)\n\nif anyone does, they will have to fork it. i have precisely zero interest in accommodating other platforms in the workspace. the only reason to port the workspace to windows or mac is \"because we can\". there is no benefit to users (they already have window management, desktop icons, etc) and a number of negatives to the kde workspace, both technological and marketing.\n\n> kdelibs should not stand in the way of this.\n\nindeed =) \n\n\"your apps where i want them\" is not the primary goal here, of course. that's a cute by-product. the real wins will be when this garners more developers and helps spread open standards."
    author: "Aaron Seigo"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-22
    body: "So windows users will be able to use Amarok.\n\nPS: i personally don't care about what options windows users have."
    author: "anonymous"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-22
    body: "cool, you don't care about 90 % of computer users"
    author: "djouallah mimoune"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-22
    body: "Well, I use linux wherever I can. Now that 'wherever I can' is unfortunately limited to private use - my current job doesn't allow me linux. Having at least some usable (KDE) apps to make using windows a bit less annoying would be great."
    author: "superstoned"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-23
    body: "Yes, I don't care about them. Call me selfish, but why should I. The windows world (including both hardware and software vendors) never cared about us."
    author: "anonymous"
  - subject: "Re: It&#8217;s alive !!"
    date: 2007-03-26
    body: "That's right."
    author: "MamiyaOtaru"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-22
    body: "Hello Aaron,\n\nhow about finishing Plasma for your one platform first, before trying to dictate what others are allowed to do. \n\nAre you really in a position to state this that way? When did KDE start to accept political reasons for technical decisions? I doubt it ever did.\n\nI can very much agree that the porting of full KDE 4.0.0 is no way nearly useful and desired, esp. with this tight schedule, there is little resources to waste. But for 4.1 release I do not see why people wouldn't do that.\n\nPersonally I look forward to what KDE on Windows will do the market. I would assume, it will give FLOSS a huge boost in public awareness, which will be absolutely worth it. And think of it. He who boots his Windows machine into KDE desktop, won't he also use Koffice and more, all KDE things, simply because he likes how KDE behaves more than how lousy Windows apps behave and integrate?\n\nIf I personally were a Windows user today, which I am only for gaming, I would find that Linux supports my hardware better. My wireless battery powered optical mouse stops working on Windows many weeks before it does on Linux. It's such a waste to swap the batteries earlier than needed. And then my sound card on board stuff, has a driver last updated 2003, earlier than when I bought the machine, and crashes to power off on Windows with modern games. These play better on Linux now. And then there is the issue with the wireless LAN chip set from RaLink. The drivers under Windows present problems so often. Many times I need to boot again, because no way ever a connection will happen, but after reboot it will. I will get frequent disconnections when playing MMORPGs and looking at things, my packets are corrupted. Under Windows only, latest drivers even made things worse. Under Linux never had any trouble. Not to speak of how lousy the SATA driver on Windows looks like for my storage driver, but I luckily don't use it. Not to speak of my TV card which is practically useless under Windows due to the many ways the only software that can use it works. I was so shocked about how bad it is. Even Kaffeine is nicer to use.\n\nEven if I were a Windows lover. Installing Kubuntu with Wine on my machine beats Windows any day.\n\nSo why are people so scared of KDE users with a closed source kernel suddenly?\n\nDo you deep in your heart feel that Windows is magically better and we can't compete on every level at the same time?!\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-26
    body: "No, it's pretty simple. Porting Plasma to windows is a huge amount of work, and would make maintaining it much harder. Why waste your time when users on those platforms already have a desktop?\n\nAnd there is indeed the point that Aaron prefers Free Software over Non-Free Software. The 'why' must be clear by their names."
    author: "superstoned"
  - subject: "Re: It\u0092s alive !!"
    date: 2007-03-30
    body: "Why waste your time when users on the current platform already have a desktop as well? The argument doesn't make much sense."
    author: "bob"
  - subject: "openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: "...to be released October 23rd. If they're now planning to release in September they could just as well wait another month and include KDE 4.0. That would be the best thing to have the new KDE already pre-installed.\n\nAnyway, already looking forward to October. Thank you for your great work!!"
    author: "WPosche"
  - subject: "Re: openSUSE should adjust release schedule for 10"
    date: 2007-03-21
    body: "I hope it too :-) "
    author: "jake"
  - subject: "Re: openSUSE should adjust release schedule for 10"
    date: 2007-03-21
    body: "heheheheh, fiesty+1 will have it, wonder if Riddell can confirm it"
    author: "djouallah mimoune"
  - subject: "Re: openSUSE should adjust release schedule for 10"
    date: 2007-03-21
    body: "ooops i mean feisty, where they get those name !!!!"
    author: "djouallah mimoune"
  - subject: "Re: openSUSE should adjust release schedule for 10"
    date: 2007-03-26
    body: "Nope, most probably it won't: http://www.kubuntu-de.org/interview-mit-riddell-zu-kde4-englisch"
    author: "chris42"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: "Hopefully Kubuntu can be delayed long enough to put in KDE 4.0 as well.  That, or they release a 7.10-kde4 or something."
    author: "Matt"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: "Kubuntu will release with Ubuntu.Kubuntu Feisty will not include KDE 4 as default."
    author: "Emil Sedgh"
  - subject: "Re: openSUSE should adjust release schedule for 10"
    date: 2007-03-21
    body: "Especially because Feisty will be released in april. You mean feisty+1"
    author: "pascal"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: "The planning for Kubuntu is to release a KDE4-based version really soon after 4.0 is out. This will probably not be supported officially, though. Read https://wiki.kubuntu.org/KubuntuFeistyKde4Plan for the details about this plan."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: "That would be awesome! KDE 4 on openSUSE. They should make it the default desktop environment :D"
    author: "sup"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: "You don't need to wait.  You can it install it now:\nhttp://ftp-1.gwdg.de/pub/opensuse/repositories/KDE:/KDE4/\n"
    author: "Richard"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: "May i ask if I install this alongside KDE 3 and how would I choose it?"
    author: "Gerry"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-22
    body: "Yes, it installs into /usr. And install kdebase-session and you can choose in kdm a \"KDE 4\" session."
    author: "Anonymous"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: "Right. Then you remember that it's an estimated release date. It could happen sooner or later."
    author: "Thiago Macieira"
  - subject: "Re: openSUSE should adjust release schedule for 10.3"
    date: 2007-03-21
    body: ">  openSUSE to be released October 23rd. If they're now planning to release\n> in September they could just as well wait another month and include\n> KDE 4.0. That would be the best thing to have the new\n> KDE already pre-installed.\n\nThat sounds like a really bad idea. By that time they're had the alpha/beta releases, and RC's are coming up. At that point it's seriously stupid to change the entire desktop the users will see.\n\nI rather hope they get a rock solid 10.3 release out with KDE 3.5.7, so they can focus all their resources on getting KDE4 running smoothly with 10.4 or 11.0."
    author: "Diederik van der Boor"
  - subject: "Re: openSUSE should adjust release schedule for 10"
    date: 2007-03-22
    body: "Seconded."
    author: "Darkelve"
  - subject: "Re: openSUSE should adjust release schedule for 10"
    date: 2007-03-24
    body: "Thwarted."
    author: "illogic-al"
  - subject: "nice"
    date: 2007-03-21
    body: "thanks to all kde community"
    author: "vincent"
  - subject: "and don't forget to digg it :)"
    date: 2007-03-21
    body: "http://digg.com/software/KDE_4_0_Release_Cycle_Finalized"
    author: "Patcito"
  - subject: "Birthday"
    date: 2007-03-21
    body: "YAY KDE4 is out on my birthday! What a great present!\nThats if it goes out on target anyway..\nI've been working with Qt4 recently and it's a nice API, good luck porting everything and keep up the great work\n~Fred"
    author: "AdolescentFred"
  - subject: "Re: Birthday"
    date: 2007-03-21
    body: "Well, I hope they will delay 2 day's, then it will be released on my birthday. Actually, I hope it will be delayed 3 day's, or my birthday party will be without me :-)"
    author: "Cossidhon"
  - subject: "October 23 ftw"
    date: 2007-03-21
    body: "October 23 is also my birthday, so this should be a nice gift.\n\nI also remember KDE 2.0 fondly, because it was also released on October 23, in the year 2000 -- see http://dot.kde.org/972331966/ . I wonder if this is a coincidence? Or could the whole KDE team repeatedly try to give me nice birthday presents?"
    author: "Haakon Nilsen"
  - subject: "seriously"
    date: 2007-03-21
    body: "I hope it will be delayed seven months, then it will be released on MY birthday! :-)"
    author: "mg"
  - subject: "Akonadi?"
    date: 2007-03-21
    body: "With how much delay? ;)\n\nSeriously, what's about akonadi? Is it delayed for 4.1 release? cause it's not in feature list."
    author: "Julien"
  - subject: "Re: Akonadi?"
    date: 2007-03-21
    body: "it's currently a wait-and-see; it is likely that akonadi will not be ready for full integration into the apps until 4.1. there is already runnable working code in svn, it's just a matter of whether all features get implemented in time and the quality is there.\n\neven if it does hit 4.0, though, i'd expect kontact et al not to have proper akonadi support before 4.1.\n\nthat's still somewhat speculative, however, as we are still waiting to see how this all pans out."
    author: "Aaron Seigo"
  - subject: "TRAC"
    date: 2007-03-22
    body: "I think that setting up a system like TRAC with milestones and small things that needs geting done will speed up development and releaseing betas\n\nMany people would like to contribute, but they usualy do not have time to white 1000 line piece of code. They can add small improvements or fixes if they know what needs to be fixed"
    author: "mag"
  - subject: "Re: TRAC -> tips on what to do and how"
    date: 2007-03-22
    body: "We already have several tools for people who would like to help with KDE 4. Those tools tell you exactly what needs to be fixed. It can be code fixes or doc fixes for non-developers. Not to mention doc writing ... We have the EBN \nhttp://www.englishbreakfastnetwork.org/krazy/?component=kde-4.0\nand the dashboard\nhttp://ktown.kde.org/~dirk/dashboard/4.0/\nPick up an application and fixe all warnings found by those tools. Then please send the patch to the kde-devel mailing list. That's as simple as that!\n\n"
    author: "annma"
  - subject: "Good news"
    date: 2007-03-22
    body: "Great to hear a schedule is available, a little something to look forward to testing...\nhttp://www.comnetslash.com"
    author: "Johno"
  - subject: "Feature freeze by june??!!"
    date: 2007-03-22
    body: "What about theming (KDE4 themes: see the wonderfull mockups people made!) and fonts? Can we see some working plasmoids? Does decibel integrates with kopete allready?\n\nThis is the kind of questions people ask, but they are never answered. Can all of this be done before june 1st?"
    author: "Dennie"
  - subject: "Re: Feature freeze by june??!!"
    date: 2007-03-22
    body: "The theme is being worked on (I have it running) but not ready. And it doesn't have to be ready for June the first, too.\n\nPlasmoids - no, not jet. They don't have to be ready very soon, the libraries behind them are still being worked on (I do recall they had superkaramba compatibility almost ready a week ago, so we're getting there).\n\nDecibel integration in Kopete is currently experimental, but yes, it's there."
    author: "superstoned"
  - subject: "Please, take Domino by default!"
    date: 2007-03-22
    body: "I'd like to see some Domino based default theme. I think, it already runs rather stable and is far beyond all other known styles (if you consider smoothness and consistency). \n\nFor the basic theme:\nhttp://www.kde-look.org/content/show.php/Domino?content=42804\n\nFor Domino themes:\nMurrina theme creator:\nhttp://www.kde-look.org/content/show.php/Domino-Murrina?content=51597\nKore shiny theming suite:\nhttp://www.kde-look.org/content/show.php/Kore?content=54701\n\nOther themes:\nhttp://www.kde-look.org/content/show.php/LightGrey+for+Domino?content=52721\nhttp://www.kde-look.org/content/show.php/GreyforDomino?content=52307\nhttp://www.kde-look.org/content/show.php/Domino+Blue+Milk?content=52632\nhttp://www.kde-look.org/content/show.php/Domino+DarkShine?content=53707\n"
    author: "Sebastian"
  - subject: "Re: Please, take Domino by default!"
    date: 2007-06-04
    body: "right! i like that style!\nits really something else than usuall styles, and really good looking"
    author: "nescius"
  - subject: "Re: Please, take Domino by default!"
    date: 2007-07-01
    body: "I completely agree KDE's best themes have never been the default and I think Domino is an exceptionally pleasant well rounded theme. Can't think of a better choice for the default."
    author: "Rainier Sephr"
  - subject: "So what\u0092s about webkit !!!!"
    date: 2007-03-22
    body: "Where is webkit, I thought it was supposed to be another pillar ok kde4, perhaps we are missing a great opportunity specially now, as it has been adopted by major IT names, just look at  adobe with Apollo thingy.\nBut why bother, we pushed dcop and now dbus is here, and eh where is arts. \n\n  \n\n"
    author: "djouallah mimoune"
  - subject: "Re: So what\u0092s about webkit !!!!"
    date: 2007-03-22
    body: "DBus is based on lessons learned from DCOP. Basically, it's a \"better\" DCOP, designed from the beginning to be more flexible and independent from the windowing system. Arts was a mistake from the beginning... it's being replaced by the new and shiny Phonon. WebKit is based on KHTML, WTF do we need to abandon our own version, if it isn't any worse?"
    author: "slacker"
  - subject: "Re: So what&#8217;s about webkit !!!!"
    date: 2007-03-26
    body: "Especially when webkit is controlled by a company that doesn't necssarily have KDEs interest at heart.  \n\nThere's also the PR side.  Now it's nice to say Webkit is based on KHTML, it's something that KDE started and Apple decided was worth picking up.  If KDE starts using Webkit, any indication that it originally came from KDE kinda disappears.  Now KDE is using Apple tech, but really, it came from KDE first!  No srsly!\n\nNow, being in control of your own destiny is a more important reason to stay with KHTML, so don't get hung up on my worry about appearances."
    author: "MamiyaOtaru"
  - subject: "KDE Astrology"
    date: 2007-03-22
    body: "Congratulations.\n\nAll the dates will be one day before or after the full moon.\nThis is great because you got all the community at full intellectual activities.\n\nSeptember 23 is the equinox, this means autumn at North and spring at South.\nThe autumn will bring to KDE an introspective development phase, something to improve at the northern community and a expansion at the user base at the southern community. The KDE will freeze when the nature start the introspective phase.\n\nOctober 23 we will have a conjunction with the sun and mercury. A stormy day.  "
    author: "KDE Astrology "
  - subject: "Re: KDE Astrology"
    date: 2007-03-22
    body: "The KDE will freeze when the northern community and the nature starts an introspective phase. At the southern community this means the point where the nature blossom is at the main point, the blossom of new people and new values.\n\nThe European guys will stay at the office developing and improving, the seed at the soil, the potential working in silence at the winter. The south America guys will spread this new tool around, make noise and get new users."
    author: "KDE Astrology "
  - subject: "Re: KDE Astrology"
    date: 2007-03-22
    body: "Sounds like you could be the right person to write a KAstrology program!"
    author: "Bille"
  - subject: "Re: KDE Astrology"
    date: 2007-03-27
    body: "Or perhaps the right person to help pick important dates! :)\n\nVlad"
    author: "Vlad"
  - subject: "Re: KDE Astrology"
    date: 2007-03-26
    body: "Ah! Another KDE astrologer.\n\nCould you tell me which (if any) programs you use to do your charts?  I have begun looking into software that runs on linux and preferably using kde and haven't found any that actually work :( Kastrolog looked promising but the project seemed to have ended a long while ago and using plain Astrolog is mind boggling.\n\nVlad"
    author: "Vlad"
  - subject: "Re: KDE Astrology"
    date: 2007-03-27
    body: "Not to mention that there was a revolution in Hungary 23rd October 1956. Is 23rd October 2007 going to be a revolution?\n\nHave a nice day\n\nZoltan"
    author: "Zoltan Bartko"
---
The KDE Community and the release team have put together a release plan for the long anticipated version 4.0, which is planned to be released in October 2007.  KDE 4.0 will be a major milestone for the Free Desktop, as it offers a new foundation and set of frameworks that will shape the desktop user experience for years to come. Users will benefit from improved speed through Qt 4, integration of hardware through <a href="http://solid.kde.org">Solid</a>, multimedia performance via <a href="http://phonon.kde.org">Phonon</a>, usability enhancements by close collaboration with <a href="http://www.openusability.org">OpenUsability</a>, new real-time communication options with <a href="http://decibel.kde.org">Decibel</a>, spell-checking with Sonnet, comprehensive desktop search through <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> and <a href="http://nepomuk.semanticdesktop.org/">Nepomuk</a>, a new desktop metaphor through <a href="http://plasma.kde.org">Plasma</a> and, last but not least, a completely new artwork experience called <a href="http://www.oxygen-icons.org">Oxygen</a>.


<!--break-->
<p>KDE 4.0 will likely contain initial versions of all the major subsystems that have been described in recent Dot articles.  These "Pillars" of KDE set the stage for desktop and application growth and maturity over the life of the KDE 4 series. The release schedule contains a number of milestones and dates, the most important of those being:</p>

<ul>
	<li> <strong>April 1, 2007:  Subsystem Freeze </strong><br />
		From this date forward, no new KDE subsystem or major changes can be committed to kdelibs.  </li>
	<li> <strong>May 1, 2007: Alpha Release + kdelibs soft API Freeze </strong><br />
		Alpha will be a source-only release without translations. The kdelibs API is "soft-frozen", meaning that changes can be made but only with the consent of the core developers. </li>
	<li> <strong>June 1, 2007: trunk/KDE is feature frozen </strong><br />
		Trunk is frozen for feature commits. Internationalised string changes are allowed. A list of main modules that will be included in the final release will be made. </li>
	<li> <strong> June 25, 2007: Beta1 </strong><br />
		Beta 1 is prepared and released after some initial testing. The incoming bugs will be reviewed for their severity. After this release, a new Beta version will be released every month. </li>
	<li> <strong>September 23, 2007: Total Release Freeze </strong><br />
		This is the very last date for committing anything that isn't reviewed on the development lists. </li>
	<li> <strong>September 25, 2007: Release Candidate 1 </strong><br />
		Targetted date for first release candidate. Only regressions (breakage caused by the KDE 4 port) or grave bugs can be fixed. Starting with this Release Candidate, a new Release Candidate will be put out every two weeks until the codebase is sufficiently stable and all showstopper bugs have been fixed. </li>
	<li> <strong>October 23, 2007: Targetted Release Date </strong><br /></li>
</ul>	

<p>As always, the KDE community welcomes involvement with translations, documentation, testing and promotions.  Developers who want to contribute to make KDE 4 even more rocking should check out <a href="http://techbase.kde.org">techbase.kde.org</a>, the new platform for all development-related information about KDE and its technologies.</p>

<p>A more detailed version of the release roadmap can be found on <a href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Roadmap">techbase</a>.  More and updated information about the release schedule can be found on the <a href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Schedule">Release Schedule webpage</a>.</p>






