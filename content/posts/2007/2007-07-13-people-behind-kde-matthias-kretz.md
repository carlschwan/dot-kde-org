---
title: "People Behind KDE: Matthias Kretz"
date:    2007-07-13
authors:
  - "dallen"
slug:    people-behind-kde-matthias-kretz
comments:
  - subject: "openSUSE Ardour packages"
    date: 2007-07-13
    body: "Aww, cmon Vir:\n\nhttp://benjiweber.co.uk:8080/webpin/index.jsp?searchTerm=ardour"
    author: "Will Stephenson"
  - subject: "Re: openSUSE Ardour packages"
    date: 2007-07-13
    body: "I've been using apt since many years now and had about 4h to get a working openSUSE system with Ardour. The openSUSE tools, the openSUSE webpage and Google couldn't help me to get there."
    author: "Matthias Kretz"
  - subject: "Re: openSUSE Ardour packages"
    date: 2007-07-13
    body: "Just for reference, http://opensuse-community.org/Package_Sources is a pretty definitive guide for setting up your repositories. On the command line this consists of pasting ~5 commands, and then you'll have all the recommended repositories (some of which are not included by default for legal reasons)."
    author: "apokryphos"
  - subject: "Re: openSUSE Ardour packages"
    date: 2007-07-13
    body: "On Fedora, it's as easy as:\napt-get install ardour\nIt's in the default repository. :-p And yes, apt (apt-rpm) is available from the default repository too."
    author: "Kevin Kofler"
  - subject: "Re: openSUSE Ardour packages"
    date: 2007-07-13
    body: "on pure debian..."
    author: "nick"
  - subject: "Re: openSUSE Ardour packages"
    date: 2007-07-15
    body: "Honestly? You tried google? When I search for <opensuse ardour rpm> I get packman's site as second search-result. I guess that's all that there is to say about that issue and opensuse being the cause."
    author: "Sven"
  - subject: "Phonon Post 4.0"
    date: 2007-07-13
    body: "\"Also my next plans for the frontend API for Phonon (post-4.0) are to add low-level audio I/O and look into an abstraction for the hardware mixer.\"\n\nHave you ever looked at the Kronos group for inspiration?  And great work on Phonon."
    author: "am"
  - subject: "Re: Phonon Post 4.0"
    date: 2007-07-13
    body: "I've been on www.khronos.org before but have not been able to learn anything of much use. Their work might be useful to implement a backend, but I've not seen anything I could adopt in the Phonon API."
    author: "Matthias Kretz"
  - subject: "Re: Phonon Post 4.0"
    date: 2007-07-14
    body: "I'm interested in what low-level audio I/O actually means. As far as I have understood, Phonon wanted to abstract the low-level stuff and those who needed specialized stuff should program directly for their chosen backend. Does it mean additional \"pro-audio\" support with the ease of the Phonon libs for those who need it?\nA bit unfortunate (regarding look and feel e.g.) is that almost all pro-audio programs (that use Qt) use Qt only but not KDE. Or is that because most are done with Qt4 while KDE4 is not out and stable yet (so too much hassle to already add support for)?\nNot counting Krecord, which doesn't count as pro-audio anyway ;-)\n"
    author: "Phase II"
  - subject: "Re: Phonon Post 4.0"
    date: 2007-07-14
    body: "For the applications using Qt4, the lack of KDE 4 may be a reason for not being a KDE app. For others it may be as simple as the authors don't feel they need the extra stuff they get by being a KDE app. And it seem the audio programs in general love to do their own solutions, like the way they do with all those self made widgets. \n\nHope you count Rosegarden(http://www.rosegardenmusic.com/) as one of those KDE applications."
    author: "Morty"
  - subject: "Re: Phonon Post 4.0"
    date: 2007-07-14
    body: "Yes, I saw that Rosegarden was surprisingly the only app which makes use of KDE. Which is Qt3/KDE3 actually, which again probably points into the direction of KDE4 not yet being ready.\nThe custom widgets and knobs thing is true, perhaps coupled with the feeling that Qt4 gives almost everything needed. At least I've read much enthusiasm on several projects about how Qt 4 already boosts the development.\nIt'll be interesting to see how much pull and appeal KDE4 can create to offer an incentive to do a porting -- which they can't refuse ;-) (though I'm still interested what the further Phonon plans are/mean)"
    author: "Phase II"
  - subject: "Great response"
    date: 2007-07-13
    body: "Q: If you could be any part of the KDE platform, what would you be? Why?\nA: Neither Phonon nor KHTML or so - I'd get to see/hear too many things I wouldn't want to see/hear.\n\nNow, that's what I call a thoughtful answer :-)"
    author: "G\u00f6ran Jartin"
  - subject: "kview"
    date: 2007-07-13
    body: "\"...instead I got involved with KView, which had just lost its maintainer\"\n\nReports of my death have been greatly exaggerated.\n\n(Thanks Vir, for picking up where I left off! ;)"
    author: "taj"
  - subject: "Re: kview"
    date: 2007-07-13
    body: "Thanks to you I learned quite a bit. School was boring so I printed out the whole KView sourcecode and read it in class. Obviously with the KDE2 libs KView should have looked a bit different, but perhaps because of that I learned a few things about C++ that I wouldn't have learned otherwise."
    author: "Matthias Kretz"
  - subject: "Reduntant borders again and again in KDE"
    date: 2007-07-13
    body: "The window titled \"Tutorial 4\" in Matthias' desktop screenshot has a redundant border. Why not make these borders 0 by default so that application developers (like Matthias) don't need to remember to remove them one by one and so that desktop real estate isn't wasted?"
    author: "AC"
  - subject: "Re: Reduntant borders again and again in KDE"
    date: 2007-07-13
    body: "fixed.\n\nDo you want the border of QFrame default to 0?\n\nYou can't make it automatic in QFrame AFAICS since the QFrame can not know for certain that it will be a toplevel window.\n\nIf you care enough you could write a style like scheck that will find redundant frames automatically and report them."
    author: "Matthias Kretz"
  - subject: "Re: Reduntant borders again and again in KDE"
    date: 2007-07-14
    body: "> Do you want the border of QFrame default to 0?\n\nThat's the general idea :)\n\n> You can't make it automatic in QFrame AFAICS since the QFrame can not know for certain that it will be a toplevel window.\n\nRedundant borders don't just happen if the frame is a toplevel window. They can happen even in nested frames.\n\n> If you care enough you could write a style like scheck that will find redundant frames automatically and report them.\n\nThat's a great idea, I'll look into it!"
    author: "AC"
  - subject: "Matthias++"
    date: 2007-07-13
    body: "Phonon is awesome. Way to go, dude :)"
    author: "Mark Kretschmann"
  - subject: "His prompt under Konsole"
    date: 2007-07-14
    body: "Look at his desktop screenshot. In Konsole, how does he get the current directory at the right end of the command line? Is it possible under Bash?"
    author: "Hobbes"
  - subject: "Re: His prompt under Konsole"
    date: 2007-07-14
    body: "It's probably ZSH. That's how I do it here."
    author: "dolio"
  - subject: "Re: His prompt under Konsole"
    date: 2007-07-14
    body: "Yes, via a fancy PS1 or PROMPT_COMMAND. I used to have a status bar of sorts that got refreshed with the prompt that way."
    author: "Peter"
  - subject: "just my thoughts"
    date: 2007-07-14
    body: "I feel Matthias needs to lay off on the whole bible preachy stuff.."
    author: "Roger"
  - subject: "Re: just my thoughts"
    date: 2007-07-14
    body: "And I feel that your comment is very inappropriate."
    author: "Carsten Niehaus"
  - subject: "Re: just my thoughts"
    date: 2007-07-14
    body: "you're just jealous 'cos you weren't abused as a child! "
    author: "x0"
  - subject: "Re: just my thoughts"
    date: 2007-07-14
    body: "Preachy?  He was asked questions and answered them.  Would you prefer he lied?"
    author: "mactalla"
  - subject: "Re: just my thoughts"
    date: 2007-07-15
    body: "Right, because two vague mentions of his faith is really preachy.\n\nIf you don't like Christianity just say that.  There's no need to go through the evasion of turning simple answers to simple questions into a fault of Matthias.\n\nPersonally, as a kid, I was very influenced by Santa Claus.  You believing or not believing in Santa Claus doesn't change the fact that the influence, because of my belief, was real.\n\nPeople of KDE is about finding out about the real people behind KDE; I may find Taj not trimming his beard, Coolo liking bad euro-pop or Matthias making ascii art fish in his email signature strange, but in literary terms it's what gives depth to their characters and that seems to be the point of this interview series."
    author: "Scott"
  - subject: "Re: just my thoughts"
    date: 2007-07-16
    body: "Very bad examples!"
    author: "Coolo"
  - subject: "Re: just my thoughts"
    date: 2007-07-15
    body: "What do you expect?  I agree with the above...he was answering \"Personal Questions.\"  The answers to these types of questions should reflect the individual. \n\nI only saw religion mentioned in two questions (and only as part of an answer), out of 36.  "
    author: "ayro"
  - subject: "Re: just my thoughts"
    date: 2007-07-16
    body: "While I despise religions too, I find your remark uncalled for: he answered personal questions and he gave honest answer.\n\n"
    author: "renoX"
  - subject: "PulseAudio"
    date: 2007-07-14
    body: "Can someone summarize the relation between http://www.pulseaudio.org/ and Phonon. Is there overlap in function? Collaboration? Greatness waiting?\n\nSorry for not just googling for the answer myself."
    author: "Anders"
  - subject: "Re: PulseAudio"
    date: 2007-07-14
    body: "PulseAudio is a sound server. It's designed to be flexible, supporting network transparency, providing a drop-in replacement to dmix (ALSA plugin), ESD and JACK (protocol emulation), and allowing you to manually connect producers to sinks or set volumes for individual producers if needed (like aRts did). However, as far as I can tell, it does NOT do any decoding: unlike aRts, one can't send a Vorbis, FLAC, MP3 etc. file directly to PulseAudio, it wants an audio stream. Video is also outside of its scope.\n\nPhonon provides a backend-independent API for multimedia, the application programmer can give it an audio or video file and it will play it. As I explained above, PulseAudio doesn't do decoding or video, so Phonon won't use it directly as a backend. Instead, using Phonon with PulseAudio will work like this:\n* Your application uses Phonon.\n* Phonon uses Xine to do the decoding.\n* Xine then outputs the video over something like Xv and the audio over something like PulseAudio.\n(There are other Phonon backends in the work, for example a GStreamer backend, but that will work essentially the same way with GStreamer instead of Xine.)\nThe \"something like\" means there's configurability here, distributions will pick a sensible default, and users will be able to set them.\n\nThe gain over KDE 3 here is flexibility through separation of concerns: in KDE 3, the application sent a multimedia file to aRts and aRts did everything else. Sounds simple, but this means all of aRts is needed: its codec infrastructure, its sound server etc. If you want to use a different sound server or none at all (e.g. because your sound card supports ALSA hardware mixing or because you value absence of latency over concurrent soundcard access), you're out of luck. In KDE 4, the application sends the multimedia file to Phonon instead, decoding is taken care of by xine-lib (code reuse!), and the output can then be sent to one of the many output plugins xine-lib supports. And xine-lib won't be a hard requirement either, a GStreamer backend and native backends for Win32 and OS X are in varying states of planning/development.\n\n(I hope everything I explained is correct, if a developer of the mentioned components notices anything wrong there, feel free to correct me. :-) )"
    author: "Kevin Kofler"
  - subject: "Re: PulseAudio"
    date: 2007-07-15
    body: "I'm all but sure that Pulse is not intedended as a drop in replacement for JACK or aRts. \n\naRts dose decodeing and stuff, pulse dosn't so while pulse is ment to replace aRts its not quite \"drop in\"\n\nas for JACK, lack is a low latency sound server designed for music production, pulse is designed for playing music. "
    author: "Ben"
  - subject: "Re: PulseAudio"
    date: 2007-07-15
    body: "> I'm all but sure that Pulse is not intedended as a drop in replacement for JACK or aRts.\n\nI never claimed it was a drop-in replacement for aRts, in fact I explicitly highlighted the differences! If you want to use PulseAudio while still having aRts-using apps, you'll have to run both. One possible setup is to have aRts set to output to ESD using PulseAudio's ESD emulation. (WARNING: When I tried this, I had to turn off full duplex in aRts. If you don't, you'll get an extremely unresponsive system and aRts doesn't work.)\n\nFor JACK, it looks like I was indeed wrong when I said it's a drop-in replacement:\nhttp://pulseaudio.org/wiki/Modules#JACKConnectivity\nSo you will need to have JACK running if your apps use JACK, however PulseAudio can work together with it (i.e. JACK output will be forwarded to PulseAudio)."
    author: "Kevin Kofler"
  - subject: "Re: PulseAudio"
    date: 2007-07-16
    body: "Hi,\n\nThanks for the explanation. Makes sense now."
    author: "Anders"
  - subject: "Qt vs KDE app"
    date: 2007-07-15
    body: "I vaguely remember an effort that was meant to make it easier to let a program use KDE libs if they were available, but fall back to the pure Qt libs otherwise. Does anyone have any update on this?"
    author: "ac"
  - subject: "Re: Qt vs KDE app"
    date: 2007-07-16
    body: "Well, it might be usefull for Qt apps which want to make limited use of KDE, like only the file-open dialog. But if you really want all the added benefits of the KDE libs, like the KIO slaves, KParts, toolbar stuff, shortcut editor, Kwrite text input field, Solid, Phonon, Decibel, Sonnet and all other infrastructure - there is no 'optional' or you'll just have to do everything twice."
    author: "jospoortvliet"
  - subject: "Re: Qt vs KDE app"
    date: 2007-07-16
    body: "It would still be nice for the whole looknfeel. It is not just the file open dialog, it is the menu bar on top, the styles, the toolbars looking and behaving different."
    author: "Sebastian"
  - subject: "Why Vir works on multimedia"
    date: 2007-07-16
    body: "Why Vir works on Linux multimedia:\n\"Hard problems. Easy problems are boring. :-)\""
    author: "Ian Monroe"
---
After a short break, we return to the next interview in the <a href="http://behindkde.org/">People Behind KDE</a> series, travelling back to Germany to talk to a developer who wants to make things as simple as possible - for both users and developers. The recent winner of an aKademy Award for Best Non-Application for his work on Phonon - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/mkretz/">Matthias Kretz</a>.

<!--break-->
