---
title: "Interview with Lars Knoll, creator of KHTML"
date:    2007-06-13
authors:
  - "tunrau"
slug:    interview-lars-knoll-creator-khtml
comments:
  - subject: "This is not an interview"
    date: 2007-06-13
    body: "I hopped on the arstechnica page to read the interview. The whole article has hardly a line from Knoll - a warning for all those planning to read the interview. "
    author: "Sarath"
  - subject: "Re: This is not an interview"
    date: 2007-06-13
    body: "You're right - but I simply borrowed the title from the original article.  A more suitable title might be \"Reflections on KHTML and Webkit as dicussed with KHTML creator Lars Knoll\" or similar.  That said, there are still long term benefits to KDE from this announcement."
    author: "Troy Unrau"
  - subject: "Re: This is not an interview"
    date: 2007-06-14
    body: "In what way?\n\nHas the development situation improved since the last time I looked?\n\nDerek\n\n\n"
    author: "D Kite"
  - subject: "Re: This is not an interview"
    date: 2007-06-15
    body: "I don't know when you last looked, but the development situation has improved for KDE developers.  We now have a number of TT/KDE developers with commit and review access to WebKit svn.  As Lars said in the interview, WebKit is now nearly indistinguishable from a fully fledged open source project.  Commit and review access are acquired through the same process as KDE employs.  Submit bug reports, patches and generally demonstrate that you are responsible and apply for svn access rights.\n\nThere is a small but dedicated group of Trolltech and KDE developers who are working well with the WebKit community right now.  Many of the same people who brought you the original KHTML.  Simon is even working on a QtWebKit KPart that will be a drop in replacement for KHTML in Konqueror when finished.  Lots of interesting things are happening with QtWebKit.  Stay tuned!"
    author: "anonymous"
  - subject: "Re: This is not an interview"
    date: 2007-06-16
    body: "let me fix that PR speak for you.\n\n> situation has improved for KDE developers. \n\nsituation is identical, but we don't want the souffl\u00e9 to puff down, so let's post one more of those silly retired-since-four-years-khtml-developer-think-webkit-is-great article and have fake \"anonymous\" people sing the praise.\n\n> a number of TT/KDE developers \n\nTrolltech is doing all the work.\nIt is dying to see Qt adopted as the main portability platform for WebKit.\n\nIt knows WebKitQt is architecturally limited on X11 because of widgets being sub-windows, but all the same, let's posture as if platform parity is a reachable goal. It's not as if Linux matters in the big picture anyway.\n\n> WebKit is now nearly indistinguishable from a fully fledged open source project.\n\n\"Open source\" WebKit is a complete marketing stunt.\n\nApple won't share a bit of control so Nokia forked, Adobe forked, and all Trolltech wants is having fresh and naive KDE developers maintaining that free advertisement for them, so they can have their developers back.\n\nWant a peak into the future? have a look here:\n\n    http://www.synack.net/~bbraun/writing/osfail.html\n\namazing how history repeats itself when profit is in sight!\n"
    author: "anonymous"
  - subject: "Re: This is not an interview"
    date: 2007-06-18
    body: "\"It knows WebKitQt is architecturally limited on X11 because of widgets being sub-windows\" ...  \"Open source\" WebKit is a complete marketing stunt.\"\n\nYou have absolutely no idea what you are babbling about, but don't let that stop you."
    author: "anonymous"
  - subject: "Re: This is not an interview"
    date: 2007-06-19
    body: "\"You have absolutely no idea what you are babbling about\"\n\nactually *you* seem to have no idea what I'm talking about,\nand I'm not surprised given people working on WebKitQt seem to just care about building a backend and have the engine maintained by \"other\" people, whoever that would be.\n\nLet me check. Since more than a year that you have been unleashing propaganda on the dot and blogs, the only contribution you made to the CSS/HTML/ECMA engine has been \n    http://bugs.webkit.org/show_bug.cgi?id=11544 \na ten line patch that is hopelessly wrong.\n \nNo engine on earth, not even MSIE's, is broken enough to render this piece:\n   <fieldset><legend style=\"text-align:right\">foo</legend></fieldset>\nwith a right aligned legend except WebKit after your patch. \n\nWell, thanks guys. Great reputation you are making to KDE.\n\nBut hey, sorry for the \"babbling\". Hope you can soon figure what limitation I was talking about, anyway."
    author: "anonymous"
  - subject: "Re: This is not an interview"
    date: 2007-06-19
    body: "That's really funny that you think that's me posting the comment."
    author: "George Staikos"
  - subject: "Re: This is not an interview"
    date: 2007-06-24
    body: "what a strange idea. I'm not sure why you'd think that.\nThe antecedent of 'you' in the bottom half of the message is of course 'people working on WebKitQt', as should be hinted by the latter plural on 'guys'.\n\n"
    author: "anonymous"
  - subject: "Safari"
    date: 2007-06-13
    body: "Users: Don't expect any revolutionary improvement if and when Konqueror switches to Webkit. When I got a Mac from my employer, I expected pretty much every site to just work in the Webkit-based Safari. I was surprised to find out that about the same fraction of web sites have problems in Safari as in Konqueror. Not necessarily the same sites, but roughly the same percentage.\n\nI switched to Firefox on the Mac, but I'm still using Konqueror as my primary browser on Linux machines."
    author: "Martin"
  - subject: "Re: Safari"
    date: 2007-06-13
    body: "If as you say, the set of sites that work not well in safari is not the same as the set of sites that work not well in konqueror, then it will be a good thing for both mac users as kde users!\n\n"
    author: "ac"
  - subject: "Re: Safari"
    date: 2007-06-16
    body: "except most sites that work better in Safari compared to Konqueror do so because they sniff the exact browser you are using, and decide to specifically support it or not, no matter what's the underlying engine.\n\nYour \"WebKit\" browser is not identified as \"Safari\" on an \"Apple\" platform?\nBe gone, you meaningless < .2% marketshare freak!\n\nHave you ever heard of \"Epiphany\" in the DHTML development community?\n\nNo. Not at all. Not a single word. It doesn't matter that they have full Gecko engine compatibility and are the flagship browser of the GNOME desktop environment, they are just an insignificant also-ran.\n\nCompare that to KHTML, which has full, explicit support from a dozen different DHTML menu providers.\n\nhttp://www.milonic.com/\nhttp://www.hiermenuscentral.com/\nhttp://deluxe-menu.com/browsers-info.html\nhttp://dhtml-menu.com/tree-examples/tree-menuxp.html\n\nThis is what's at stake. Significance in the desktop war.\n"
    author: "anonymous"
  - subject: "Re: Safari"
    date: 2007-06-14
    body: "The advantage could be to free ressources."
    author: "Andre"
  - subject: "Re: Safari"
    date: 2007-06-14
    body: "> Don't expect any revolutionary improvement if and when Konqueror switches to Webkit\n\nI thought that Konqueror was not going to switch."
    author: "Petteri"
  - subject: "Re: Safari"
    date: 2007-06-14
    body: "If WebKit is KPartified, it would be quite easy to switch. No explicit support from Konqueror needed, AFAIK."
    author: "Andr\u00e9"
  - subject: "Re: Safari"
    date: 2007-06-16
    body: "Webkit is being kparted right now: playground/libs/webkitkde\n\nStick webkit in Kate and you have a tabbed web browser you can call Katerer.\n."
    author: "reihal"
  - subject: "Hmm"
    date: 2007-06-13
    body: "The strange thing is how much exposure Apple gets for porting it to windows."
    author: "Andre"
  - subject: "Re: Hmm"
    date: 2007-06-13
    body: "Exposure?  As in: http://larholm.com/2007/06/12/safari-for-windows-0day-exploit-in-2-hours/\n\n]:-)\n\n"
    author: "cm"
  - subject: "Re: Hmm"
    date: 2007-06-14
    body: "Yep, it has amazed me the last two days as the tech \"media\" has slammed Apple for the bugs in Safari for Windows, even though Apple clearly states, at the point of download, that it is a public beta.  Looks like computer-related media would understand what 'beta' means and the purpose of releasing a beta.  Great job, nameless security pros!  You've uncovered flaws in unfinished software!  What a discovery!  Seriously, I'm sure Apple thanks you.  Sorry for being off topic."
    author: "Louis"
  - subject: "Re: Hmm"
    date: 2007-06-14
    body: "The carries for both the Windows beta version and the latest release version for OS-X (Safari 2.0.4 apparently).\n\nFor that matter, the professional in question is David Maynor."
    author: "strider"
  - subject: "Re: Hmm"
    date: 2007-06-14
    body: "> even though Apple clearly states,\n> at the point of download, that it is a public beta\n\nWell I've installed the thing, but about every button causes the program to crash. All bold fonts don't render. These are hardly bugs which should be in a beta. It looks more like a pre-alpha release (or random svn snapshot), and Apple is giving Windows users a really bad first impression about Safari.\n\nAfter 1 minute of clicking around I determined it's totally unusable right now."
    author: "Diederik van der Boor"
  - subject: "Re: Hmm"
    date: 2007-06-14
    body: "> Well I've installed the thing, but about every button causes the program to \n> crash. All bold fonts don't render. These are hardly bugs which should be in a \n> beta. It looks more like a pre-alpha release (or random svn snapshot), and Apple \n> is giving Windows users a really bad first impression about Safari.\n\nI think this only happens on Non-English Windows installations. On my Windows XP PC at work Safari runs quite stable (I have not encountered a single crash) and also all headlines show up as expected"
    author: "Michael"
  - subject: "Re: Hmm"
    date: 2007-06-14
    body: "\"Beta\" doesn't mean \"pwn me please\"..."
    author: "Kevin Kofler"
  - subject: "Re: Hmm"
    date: 2007-06-17
    body: "I'm sure someone at Apple will apologise to you personally for having deliberately left the security bugs in.\n\nA bug is a bug, the only way to be sure that a release does not contain security bugs is to have already discovered all the bugs - and since the point of a Beta is to uncover undiscovered bugs, then it should be no surprise that some of those bugs are security related.\n"
    author: "mabinogi"
  - subject: "Gnustep"
    date: 2007-06-13
    body: "Can webkit and Gnustep be used to create a mac compatibility layer."
    author: "Andre"
  - subject: "Re: Gnustep"
    date: 2007-06-14
    body: "WebKit is a fork from KHTML. They are HTML renderers. WebKit is platform independent; it already works on Qt, but integration to KDE4 techs is being worked on."
    author: "fast penguin"
  - subject: "Re: Gnustep"
    date: 2007-06-15
    body: "...be used in order to achieve mac compatibility? when webkit is free the logical next step would be *step integration, no?"
    author: "semsem"
  - subject: "Re: Gnustep"
    date: 2007-06-15
    body: "Its a HTML renderer... I want what you guys are smoking! ;)"
    author: "fast penguin"
  - subject: "CoreAnimation"
    date: 2007-06-13
    body: "What is the Qt project which is similar to CoreAnimation?"
    author: "EMP3ROR"
  - subject: "Re: CoreAnimation"
    date: 2007-06-14
    body: "I guess the QTimeLine, QGraphicsView classes and new transformation algorithms (Zack-foo) are the first steps towards it."
    author: "Diederik van der Boor"
  - subject: "Re: CoreAnimation"
    date: 2007-06-14
    body: "Off course I meant Zack-Fu: http://zrusin.blogspot.com/ ;-)"
    author: "Diederik van der Boor"
  - subject: "KHTML is notoriously underrated"
    date: 2007-06-14
    body: "As a professional web consultant I am used to work with all browsers out there. I always start development with Konqueror and check FF and IE later. From that perspective I can only say that the _html_ rendering engine of Konqueror, KHTML, is doing a better job than FF and IE. And I am very serious about that. \nThese are the little things that KHTML simply got sorted out properly where FF and IE suddenly come up quirky. Konqueror does however have some problems in the javascript area which have be ironed out step by step. Things steadily improved and I would very much appreciate going forward on this route instead of throwing out the baby with the bath water. Webkit and Safari is only getting more PR than Konqueror but is not necessarily better."
    author: "Michael Daum"
  - subject: "Re: KHTML is notoriously underrated"
    date: 2007-06-14
    body: "Your site looks professional indeed but my W3C-checkers tell me that there are some mistakes. Of course you never claimed it to be \"valid\" (that's what I said or assumed). But is there a reason for keeping it that way (I mean not 100% valid, though perfectly renderable via Konqueror)?\n\nI do not intend to offend! If you got the impression, sorry for that.\n\nHTML\nhttp://validator.w3.org/check?uri=http%3A%2F%2Fwikiring.de%2FPublic%2FWebHome\n\nLinks\nhttp://validator.w3.org/checklink?uri=http%3A%2F%2Fwikiring.de%2FPublic%2FWebHome&hide_type=all&depth=&check=Check#results1\n\nCCS\nhttp://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fwikiring.de%2FPublic%2FWebHome&warning=1&profile=css21&usermedium=all"
    author: "mean_checker_;)"
  - subject: "Re: KHTML is notoriously underrated"
    date: 2007-06-14
    body: "I haven't looked into the specifics, but it's very well possible (some of) these 'bugs' are needed to get IE working with the site?!?"
    author: "superstoned"
  - subject: "Re: KHTML is notoriously underrated"
    date: 2007-06-14
    body: "Sounds reasonable. I do only basic web pages (from templates: www.oswd.org & www.openwebdesign.org ), therefore I have only little knowledge but comments like this are good to learn something. Let's see."
    author: "mean_checker_;)"
  - subject: "Re: KHTML is notoriously underrated"
    date: 2007-06-15
    body: "> I haven't looked into the specifics, but it's very\n> well possible (some of) these 'bugs' are needed to\n> get IE working with the site?!?\n\nThe underscore hack is quite tricky, any browser with parsing errors could pick it up too. He could have used a validating hack instead:\n\n* html <selector here> { ... }\n\nBecause IE6 actually thinks there is something above <html> (perhaps the 'document' or <!DOCTYPE>, who knows :) ) Most hacks are simple stuff to attach \"hasLayout\" to an element. This is a optimization that causes IE's rendering algorithm to skip elements which have no markup. Unfortunately they didn't take some variants into account like padding on a parent element. Often a simple \"position: relative\" or \"min-height: 0\" is enough to fix this.\n\nMicrosoft fixed these parsing bugs in IE7. They also fixed the \"* html\" parsing bug, which is not a problem since it renders much more like Firefox. Fortunately for us, there is yet another hack that works:\n*+html <selector here> { min-height: 0; }\n\n:-p"
    author: "Diederik van der Boor"
  - subject: "Re: KHTML is notoriously underrated"
    date: 2007-06-15
    body: "or use conditional comments"
    author: "blade"
  - subject: "Will Webkit make Konqueror compitable with Gmail?"
    date: 2007-06-19
    body: "Since Safari is working with Gmail, does this mean, when Webkit is integrated into Konqueror, that Konqueror will also work with Gmail?\n\nOh by the way: When can we expect that Webkit is integrated into Konqueror?"
    author: "Samir"
---
Hot on the heels of Apple's announcement of Safari for Windows, Clint Ecker has published a short <a href="http://arstechnica.com/journals/apple.ars/2007/06/12/ars-at-wwdc-interview-with-lars-knoll-creator-of-khtml">discussion with Lars Knoll</a>, one of the original coders behind KHTML. The article focuses on the connection between KHTML and Apple's Webkit, and in turn shows how Apple's announcement of Safari for Windows can benefit KDE and Qt. Safari on Windows will help KDE in two additional ways: improved website compatibility as more coders can now check site rendering when on Windows; and mindshare for KDE technologies for cross platform applications.
<!--break-->
