---
title: "KDE Commit-Digest for 8th July 2007"
date:    2007-07-10
authors:
  - "dallen"
slug:    kde-commit-digest-8th-july-2007
comments:
  - subject: "Remoing Kio-Slaves"
    date: 2007-07-10
    body: "\"system:/ and home:/ KIOSlaves removed, with preparations to remove media:/ in the near future.\"\n\nIf those kioslaves are going away whats being added to replace them?  I always found media to be really useful (quick and easy access to the USB devices or my CD I just put in... I guess I could use /media but that doesn't have the option to eject or set the special properties and the like which I found very useful).\n\nOn a side note Kollagame looks very nice, theres quite a lack of simple and easy to use game engines available in the open source world.  Hopefully the scripting language(s) used are relatively simple and easy to use."
    author: "Sutoka"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "I have no problem with getting rid of home:/, but why the other two? I dont use system too often, but I still like it. Hopefully you can replace it with sysinfo instead, which is probably more useful and looks cooler.\n\nAs for media, unless it is being replaced by solid:/ or something like that, i would really not like to see it go. I use it often and it does a lot of things that you cant do by just going into /media."
    author: "DR"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "Afaik, Kubuntu has patches which allow you to use /media with all features media:// had - so that might be the reason why they remove it ;-)"
    author: "superstoned"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "And KDE was already quite adept at handling mounting and unmounting of removable media back in the KDE2 days. So media:// did not really solve any problems, it just introduced a different way to do it. Besides it was mostly different Linux distributions ways of doing automounting that caused the problems for the old way of handling it. With the introduction of HAL those issues has been mostly resolved."
    author: "Morty"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "Yes... and we're having some problems about those now... we're still waiting for the lead maintainer of that patch to get back to us, but as far as I can see, the /media change has brought its own share of problems, perhaps more than the problems it tried to solve."
    author: "Jucato"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "\"system:/ and home:/ KIOSlaves removed, with preparations to remove media:/ in the near future.\"\n\nFinally, not a day to soon. They where supposed to bring some kind of usability improvement, which clearly they didn't. So it's good they get removed. Unfortunately it does not bring back all those wasted hours fixing bugs they caused.\n\n\n\"If those kioslaves are going away whats being added to replace them?\"\n\nNothing would be the best option IMHO, since they reimplement existing functionality already present in the filesystem. "
    author: "Morty"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "> They where supposed to bring some kind of usability improvement, which clearly they didn't.\n\nMostly wrong. For me they work far better than any other solution."
    author: "Chaoswind"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "> Mostly wrong. For me they work\n\nMe != 90% of the users. Saying it \"works for me\" doesn't prove the argument 'wrong'.\n\nThe problems with media://\n* IO slaves can't be nested, so with media:// it's impossible to browse a ZIP archive at a CD-rom.\n* Applications which only knew UNIX filepaths (e.g. The Gimp) can't access files on media://\n* Applications can't recognize /media/xyz and media://xyz point to the same file.\n* KDE applications which weren't patched for media:// thought they were network paths, and limited abilities to browse/play the files, or copied them to a local folder first.\n\nThese are all problems that /media doesn't have, because it's just a UNIX path!!  Using media:// is IMHO the wrong solution for the problem:\n* I can imagine you're using media:// to hide the other UNIX root folders (dev, etc, usr, ..). I've seen patches somewhere where Konqueror only showed /home and /media. Mac OS X does something similar. This offered real usability improvements to users.\n* I can imagine you're using media:// to have some mounting shortcuts in the context menu. These can be added to normal /media too. In fact Konqueror already recognizes SMB folders to treat them as special.\n\nBottom line: media:// introduces more problems then it solves, and the stuff it solves can be implemented in a better way."
    author: "Diederik van der Boor"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "> Me != 90% of the users.\n\nThat's unimportant, for this.\n\n> * IO slaves can't be nested, so with media:// it's impossible \n> to browse a ZIP archive at a CD-rom.\n\nIs'nt there coming support for nestet slaves with strigi?\n\nBesides, /media exists.\n\n> Applications which only knew UNIX filepaths (e.g. The Gimp)\n> can't access files on media://\n\n/media exists.\n\n> Applications can't recognize /media/xyz and media://xyz point \n> to the same file.\n\nWhy should?\n\n> I can imagine you're using media:// to hide the other UNIX root folders\n\nNo."
    author: "Chaoswind"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "> > Applications which only knew UNIX filepaths (e.g. The Gimp)\n> > can't access files on media://\n \n> /media exists.\n\nPlease, don't be so freak. If a common user is browising him documents stored in a CD (so KDE opened a konqueror in media://cdrom) and there is a image, ths common user CAN'T open it with Gimp. Should he write /media/cdrom in Konqueror URL? do you really think so? so you know what usability is? do you imagine common users knowing which apps can work over kisolaves and which ones no?\n\n\n> > Applications can't recognize /media/xyz and media://xyz point \n> > to the same file.\n \n> Why should?\n\nBecause they are in fact THE SAME ??? please...\n\n\nPlease, give reasons of why is convenient to use media:// kioslave instead of be opposite to any good ghange."
    author: "I\u00f1aki Baz"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "> If a common user is browising him documents stored in a CD (so KDE opened a\n> konqueror in media://cdrom) and there is a image, ths common user CAN'T open it > with Gimp\n\nUnter Kubuntu, this works. The URL is then rewriten to /media.\n\n> Because they are in fact THE SAME ??? please...\n\nDoes'nt answer the question.\n\n> Please, give reasons of why is convenient to use media:// kioslave\n\nmedia:// supports audio-cd://\nmedia:// shows any existing drive, regardless whether they are mountet or not.\nmedia:// is a kio-slave, not a tool anywhere in in kicker."
    author: "Chaoswind"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "> Under Kubuntu, this works. The URL is then rewriten to /media.\n\nYes, I use Kubuntu. Now it works but in earlier version it didn't.\n\n\n> > Because they are in fact THE SAME ??? please...\n \n> Does'nt answer the question.\n\nNo? what do you need? /media/cdrom/file and media:/cdrom/file are exactly the same file. How do you say they aren't?"
    author: "I\u00f1aki Baz"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "I want a clear answer, why it should bother me, that an application can't understand that media://* and /media/* point to the same file. I had never a problem with tha, nor can i imagine a real situation, where this could become a problem."
    author: "Chaoswind"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "The question is: give us a reason why there should be two DIFFERENT ways to reach the same path, one of which it isnt precisely posix-compatible (media:/)"
    author: "Vide"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "because media:// is easier??? if there will be no substitution, I dunno where to look if I want to mount a disk, sorry. Or do I need to go back to the console for that?\n\nAre system:/ and remote:/ gonna go as well? what about trash:/? Sorry, but I use all those, and a lot - I'd miss them. And I'm very well at home at the commandline. These KIOslaves can't be missed by average users, as they don't know any other way to do the things these slaves did.\n\nIf there will be no alternative, I think it's very bad to remove them. /media can't be used, I don't see my dvd drive there, and I can't rename media, nor mount and unmount them. There are folders there for drives which aren't connected at all, etcetera... So we'll at least need the Kubuntu patches for /media (but apparently those have problems as well) or keep media://"
    author: "superstoned"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "YOU DON'T NEED MEDIA:/. Fullstop.\nYou need a place where you can find all the devices you have in your system (not only discs), which is a bridge to /media/cdrom0 if you've inserted a data cdrom, which is a bridge to audio:/ or kaudiocdcreator (or whatever) if you've inserted and audio-cd and so on. But you don't need to have another layer which is not only \"KDE only\", it's \"compatible KDE apps only\"."
    author: "Vide"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "No, I don't need media:/, I agree, we need something else which offers the same functionality. Which doesn't exist, and media:/ does, so I'd rather keep it...\n\nkind'a not-so-smart argument you got there."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-12
    body: "Well, just be a little more patient, man! :)\nYou know all the pillars are there, it will be a few hours/days hacking to get the thing we all need. I trust in KDE devs :)\n(an d man, it was the same Kevin who created the media:/ slave that deleted it, so I think there are good reasons behind it)"
    author: "Vide"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "Reasons were given. And don't bug a user of an envirmonet, based on extrem usage of virtual filesystems, with posix-nonsens."
    author: "Chaoswind"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "\"media:// supports audio-cd://\nmedia:// shows any existing drive, regardless whether they are mountet or not.\"\n\nThese are probably two of the best examples, which the Kubuntu patch doesn't do anything about AFAIK.  audiocd:/ kio-slave is one of the killer kio-slaves, but without media:/ most people wouldn't find out about it, and it wouldn't show up in /media either.\n\nAnd the argument that the Gimp can't open the file is pretty bad since the gimp wouldn't have less problems opening a file via fish, http, or smb.  Saying that a feature should be removed because \"a luser will be confused!!!\" is a horrible argument which is the cause of the feature-hate that appears in the GNOME camp that so many KDE users have ran to KDE to seek refuge from."
    author: "Sutoka"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "I'm checking the behavior right now, and I guess, if currently (at least) Kubuntu's KDE is smart enough to realise the CD I just inserted is an audio CD (as it is, because offered opening KAudioCreator to me), it can just be smart enough to offer opening audiocd:/ instead of /media/cdrom0.\n\nBTW, I always hated media:/ and specially home:/, so this is a welcomed change. I only used media:/ on Slackware because it had the \"Unplug safely\" option. system:/ on the other hand is handy sometimes and I guess is a kind of known starting point for Windows migrants.\n\nOther ioslaves lying around are applications:/ and settings:/ While those are interesting I'm not sure if anybody (or anything) uses them, besides settings:/ is a nice way to access KPanel functionality in a Windows-like manner (Yet I prefer KPanel's tree, and BTW \"System Preferences\" sucks).\n\nOn the other hand, I miss (not because of being really useful, but because of the coolness factor) a packages:/ ioslave, allowing Mac-style drag&drop package management. "
    author: "Shulai"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "KAudioCreator isn't close to as easy to use as audiocd:/.\n\n/media does NOT solve the problem of mounting un-mounted file systems, or accessing redbook (i.e. audiocd) filesystems in an EASY manner.  home:/ never really served a purpose (since you could get to any home directory already by typing ~USERNAME (and it would autocomplete for you!)).\n\nSo really, whats the solution to mounting filesystems that aren't mounted?  How do you remount a file system if you unmount it (without removing the device/cd and then reinserting it)?  To me nothing else provides the functionality in an easy to use manner that'd make removing media:/ a good idea (though I like the idea the person had to make it so media:/ was symlinks to the mounted directory when they're mounted, or to audiocd:/ for CDs, that seems like it'd fix the 'gimp won't work' problem to me)."
    author: "Sutoka"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "I totally agree with you. I acknowledge the problems brought to the linux world by introducing abstractions like media:/, but currently media:/ offers functionality users are dependend upon and which is hard to get somewhere else. So we can't remove it without alternative, imho."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "I didn't said you should use KAudioKreator. audiocd:// is more Unixy indeed :-)\n\nI said that I guess making KDE able to open audiocd:// instead of KAudioCreator is  trivial (I even bet is just a service menu file), so it is no reason for keeping media:/ ioslave around."
    author: "Shulai"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: ">Besides, /media exists.\n\nExactly, and by that you summed up the main reason why media:// should be removed. It was supposed to be a usability improvement, and it clearly shows it's not. And nobody ever gave any reasonable explanation why media://cdrom should be more usable than /media/cdrom. "
    author: "Morty"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "> It was supposed to be a usability improvement, and it clearly shows it's not. \n\nAnd that's nonsens. On Kubuntu, it clearly shows it is!\n\nAs long, as there is'nt an equal solution, the decision to remove media:// is only stupid. KDE is'nt GNOME. Who cares if only 5% used media://. Remove it as default-handler, but don't remove the code and bother people with personal problems. That's not kde'ish."
    author: "Chaoswind"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "You always can pick the code and mantain it yourself. IIRC KDE is modular enough to allow installing extra ioslaves on a default distro build (e.g. you don't have to compile your own KDE to keep using media, just the ioslave)."
    author: "Shulai"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "And if the cdrom is an audio cd?  It WONT show up in /media in that case!"
    author: "Sutoka"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "My dvd drive doesn't show up either, and my harddisks can't be renamed. media:// simply has usable features ppl need, which /media can't fully provide. Fix that first, before removing a usable feature."
    author: "superstoned"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "ehm... it has a unmount option?!? And besides, I don't SEE a /media/cdrom. The contents of /media here are just a few folders, named things like 'sdc1' and t1 and t2 and t3 etc. - pretty useless for an average folder. Media://, on the other hand, shows my harddisks (I've renamed them as well) and my DVD which I inserted. A rightmouseclick gives options to eject, mount, preview, open with... In /media a rightmouseclick gives the same options all other folders give, which doesn't make sense.\n\nmedia:// is even for me, and I'm pretty unix-adepty, very important."
    author: "superstoned"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: ">The problems with media://\n> * IO slaves can't be nested, so with media:// it's impossible to browse a ZIP archive at a CD-rom.\n\nWhat's bad with symlinking? this is just because when you click a device in media:// you'll find yourself at media://device instead than at the real path.\n\n * Applications which only knew UNIX filepaths (e.g. The Gimp) can't access files on media://\n\nOh well, since when the technological advancement should be blocked by\nprehistoric age software (and gimp-core is, GEGL is programmed since year 2k\nand still isn't there....)\n\n * Applications can't recognize /media/xyz and media://xyz point to the same file.\n\nLook above, the easy solution is for media:// to be only a collector of\nsymlink (a la kio-sysinfo) : anything converted in the real path when clicked.\n\n * KDE applications which weren't patched for media:// thought they were network paths, and limited abilities to browse/play the files, or copied them to a local folder first.\n\nStill read first answer. Having all the storage periphals in the same place\nwas something really usable and comfortable, one of the pro of kde against\nit's main competitor...\n\nPS: I never used any stupid patch to hide anything, just /media isn't enough:\nfor example it doesn't detect at all removable drives, resulting in a single\nfixed folder (with uncomfortable name) which may be empty exactly as /mnt..."
    author: "mat"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "[My keyboard's somewhat broken so sorry if I missed a few letters now and then]\n\n\"I've seen patches somewhere where Konqueror only showed /home and /media. Mac OS X does something similar. This offered real usability improvements to users\"\n\nHiding expected functionality is never a usability improvement. If something's right there, then show it; not doing so is a solid indicator that the implementor is suffering from Microsoft Brain Damage. MBD is a serious and contagious disease which has destroyed my ability to use software such as Windows and Gnome without becoming terminally frustrated at its confusing illogic, and unfortunately its effects appear to be manifesting bit-by-bit in Kubuntu.\n\nHell, even my mother (who usually doesn't care about computers and tends not to feel emotions related to them at all) was somewhat angry to discover the amount of files and file information (like the extension) hidden by Winows Explorer (can't remember what she was trying to do now).\n\nPervasive hiding of useful, relevant, and important information destroys discoverability, hence preventing any user from learning more than whatever subset of things they already know, and it frustrates experienced users by pulling the rug from under their feet *again* in the name of paying lip-service to usability, when real usability improvements are few and far between (eg. I can't, off the top of my head, think of any Kubuntu changes to KDE which made it easier to use, but I can think of several which made it harder, by removing features or making the user jump through hoops to use them).\n\n<was going to write more here by typing on this keyboard is uncomfortable>\n\nI think from now on my motto is going to be \"Usability through Obscurity: Just Say NO!\"\n\n"
    author: "Waywocket"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "What're you talking about?  No one is hiding useful, relevant, or important information here.  If you had a konqueror view right now that showed only /media and /home/user it would be very useful (just like the home view right now), because that's where you (probably) are spending 99% of your time.  If you want to see the whole filesystem, then click the root view.  Nothing is being hidden.   "
    author: "Leo S"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "Are you using Kubuntu? In Kubuntu's version of Konqueror, most of the entries under / are hidden and I don't know how to disable that behaviour (I don't know what you mean by 'root view', sorry). If you want to go to one of the hidden directories, you just have to know it's there so you can type it into the address bar.\nThat said, if anybody *can* tell me how to disable this, I'd be grateful."
    author: "Waywocket"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "sudo rm /.hidden"
    author: "Cyrille Berger"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "Ah, thanks a lot."
    author: "Waywocket"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "I think that \"show hidden files\" is even quicker ;) (AFAIK it should do the job)"
    author: "Vide"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "It's stupid anyway. And confusing. The expected meaning for \"hidden files\" is \"dot-files\", and that is what vanilla Konqueror (file:/) shows when you check the option.\n"
    author: "Shulai"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "Iff you are a unix-weenie, then the expected meaning is \"dot-files\". The rest of humanity doesn't carry that expectation after all. I'm not convinced that a distribution like Kubuntu should be targeting unix-weenies. The hardcore users are already well catered for by things like the *BSDs, Slackware etc.\n\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "Because you're a geek. So, since you have more computer knowledge than the rest of people, just add this tiny little piece of knowledge to your KB.\n\nI hope .hidden is going to be some kind of \"standard\" (at least a freedesktop draft) because it's the best way to handle this."
    author: "Vide"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "Oh, sorry.  I remembered incorrectly how it worked on Kubuntu.  I thought that the \"home\" view only showed media and home, not the root view.  Indeed, hiding directories in the root view is a mistake like you say."
    author: "Leo S"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "They added that patch for one release, and immediately removed it after the release as everybody complained."
    author: "superstoned"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "Oh, yes, the best for the usability is the bare console without KDE, right?"
    author: "mat"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-10
    body: "For some tasks, it actually is, yes. For others it isn't, of course."
    author: "Andre"
  - subject: "Re: Remoing Kio-Slaves"
    date: 2007-07-11
    body: "Romeving media:/ is BAD.\nI use it a lot, and just going to /media ISN'T nearly as easy and usefull. :("
    author: "Iuri Fiedoruk"
  - subject: "media:/"
    date: 2007-07-10
    body: "It seems to be a new trend of commit-digest saying \"what is being removed\" without telling \"What will be the alternative of the removed feature!\" causing much hue and cry of kde users ;)\n\nI presume that KDE4 will get a \"My Computer\" like interface/place where things are automagically (using dbus+hal+solid) and seemlessly accessible/mounted/unmounted."
    author: "Fast_Rizwaan"
  - subject: "Re: media:/"
    date: 2007-07-10
    body: "> It seems to be a new trend of commit-digest saying \"what is being removed\"\n\nThe commit digest just reports what developers write in the messages attached to their code changes.  They are primarily written for other developers to read so that they can follow changes in the code base.    They are also written in the context of various mailing list and IRC discussions going on, so they might not make complete sense without them.\n\nThe commit digest doesn't show every commit either, that would make it very long and probably very boring.  Somewhere along the way there seems to be a juiciness filter which extracts the most interesting commits.  \n\nThe price you pay for getting this bleeding-edge news is that it isn't checked, reviewed and polished like a full press release."
    author: "Robert Knight"
  - subject: "Re: media:/"
    date: 2007-07-11
    body: "That juiciness filter is DANNY. Danny goes through more than 3500 commit messages each week, and filters out those 100 interesting ones. He spends many hours on this, each and every week again..."
    author: "superstoned"
  - subject: "Re: media:/"
    date: 2007-07-11
    body: "Danny is my hero!"
    author: "Oscar"
  - subject: "Re: media:/"
    date: 2007-07-11
    body: "and that's deserved. I've seen him do it, at aKademy. And he does a lot more, like the interviews, artwork etcetera... That guy really is a machine, you know. He goes on and on and on..."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: media:/"
    date: 2007-07-12
    body: "Heh, you're quite an effective content creator yourself, Jos ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Nice work on Kollagame!"
    date: 2007-07-10
    body: "It's an interesting gaming niche and one can be surprised to find how many of these small-time RPGs are written (mostly, if not all with proprietary game makers for Windows). Within a short time one will come up with dozens of exciting new ideas how to make that an even better project. It's also one of the apps that will surely benefit hugely from a Windows port, exposing itself to a big new crowd of potential game makers.\n\nIt's also something I proposed in the KDE games survey.. if someone ever bothered to read that ;-)"
    author: "Phase II"
  - subject: "Re: Nice work on Kollagame!"
    date: 2007-07-12
    body: "There's a really nice java based tool that should be coming out soon that's like this... http://www.stencyl.com\nIt looks a lot more polished than what Kollagame looks like.  \nGood work still!"
    author: "Jordan K"
  - subject: "rant.  please ignore."
    date: 2007-07-10
    body: "\"Dolphin embedded as the file management view in Konqueror\"\n\nTo me the whole message about the purpose of Konqueror and Dolphin is a mess.  In response to criticism of local and internet bookmarks and other things being mixed together, Dolphin was born to be a second filebrowsing app.  Of course, Konqueror had to retain its filebrowsing capabilities for those who didn't like how pared down, sparse or dumbed down Dolphin was.  Then, as the reality of Dolphin being the default sank in, Dolphin gained more and more features (Dolphin can use Konqueror service menus, option to have Delete instead of Move to Trash in context menu, treeview).  So much for simplicity.  It just can't be done while satisfying everyone who will be using it.  \n\nAnd now, Konqueror will be Dolphin while filebrowsing?  IIRC Dolphin will open a lot of stuff in external viewers no?  Isn't that what made it a \"file browser\" instead of a \"universal viewer?\"  Whatever the differences, If Konqueror will now behave as Dolphin in filebrowsing mode I don't see why it should even bother.\n\nAll this effort to simplify things, and KDE can't even decide whether or not to draw a distinction between viewing the internet and viewing local files.  Separate apps, or the same app for both?  How about both options?  We'll have both separate apps *and* an app that does both!  Brilliant!  All we need now is a dedicated webbrowsing app.  Perhaps Konqueror can embed it for viewing webpages.  Perhaps Konqueror can simply become \"the desktop\".  \n\n*spirals further into ranting*\n\nWith the kmail and mailody strikes me as similarly odd.  All of this \"we must make things simpler\" is useless without real changes.  Digikam for example: having to add a camera?  Having to figure out what model it is?  Having to choose the right entry when you have several?  Retarded, especially in the case that the USB system conspires to give it a different number each time necessitating a dance along the lines of Remove Camera, Auto Scan, Add each time you wish to use it so that it accesses the right port (what my grandfather has to do, and can never remember how to do).  If AutoScan can pick it up every time, it shouldn't have to be explicitly invoked. Just do it and let the user access whatever camera(s) may be attached at the moment.\n\nThen there's Koffice.  File->New brings up a mess of a dialog that in addition to letting you choose from a bunch of templates also duplicates the functionality of File->Open and File->Open Recent: things that obviously fit with the task of creating a new document.  And those responsible actually think this makes sense, and that the layout (with columns and buttons sprayed all over the place) is sensible.  Recent Documents is grouped with the templates, while Open Existing (for all those documents that don't happen to be recent) is its own solitary button off someplace else, and the Use This Template button (for opening a blank document, as people tend to want to do when starting koffice from its icon istead of doubleclicking a koffice document in one of two filebrowsers) is in some nebulous location in the middle of the dialog.  WTH.  \n\nKaffeine has gone from a videoplayer that does what it needs to do with important controls well laid out to an awful tabbed monstrosity with more complex keybindings and the all important ability to put the progress bar wherever you want.  Ktorrent feels that *IDEA* is a great interface for the task of downloading things.  Amarok has a playlist that mutated ungracefully into the main window with an alphabet soup of widgets.  Cervisia provides a way to integrate CVS into Konqueror even though almost everyone uses SVN or Git or something else now.  One has to have kghostview installed and cluttering up the kmenu for print preview to work in anything.  Kget doesn't actually resume timed out downloads on its own at all.  Kcontrol is about to be replaced by an OSX knockoff that still manages to add tabs.  It's impossible to make a simple pie chart in Kchart (a pie chart with 3 values needs a 4x4 table with the values reading left to right in row 2 starting with column 2 and their corresponding labels entered *top to bottom* in column 1, starting with row 2.  Makes a lot of sense).\n\nThere are just so many things that actually need fixing that it frustrates me to see so much effort churning into duplicating functionality with new applications.  \n\nNo responses needed or really wanted.  I'm just ranting; I know nothing will change.  I've been watching for years what makes KDE and Linux unusable day to day for ordinary people and stuff like Dolphin, Mailody, Plasma etc. doesn't address it.  It makes me irritable.  I can't recommend this to anyone and I no longer do. "
    author: "MamiyaOtaru"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-10
    body: "Drink a beer (or two) and relax!"
    author: "Sepp"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-11
    body: "> Dolphin embedded as the file management view in Konqueror.\n\nWell, I don't drink beer (food allergy), but since I have no idea what the above means, I am going to wait till I find out before commenting.\n\nHowever, I do have some questions.\n\nKonqueror is a file browser (dolphin isn't -- unless you count looking at the previews as browsing).  So, does this mean that if I am using the Dolphin KPart in Konqueror that it won't be possible to browse files?\n\nDolphin has a better Location Bar, but how could this work in a KPart?\n\nKonqueror currently uses KFM for file management and directory based file browsing.  IIUC, KFM is really two different file managers/browsers: list & icon.  Will these still exist?\n\nPerhaps some screen shots would help. \n\nIAC, what I would like to see would be to have the Dolphin Location Bar available in Konqueror.  Having the bookmarks available in a sidebar rather than the toolbar would be nice too."
    author: "James Richard Tyrer"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-10
    body: "\"No responses needed or really wanted. I'm just ranting; I know nothing will change. I've been watching for years what makes KDE and Linux unusable day to day for ordinary people and stuff like Dolphin, Mailody, Plasma etc. doesn't address it. It makes me irritable. I can't recommend this to anyone and I no longer do. \"\n\nNeat - an excellent way of spreading FUD without being held accountable for it, or having to clarify exactly what it is your are criticising so people can try and sort it out (\"I know nothing will change\" - things have and are and will change, so I have no idea what you are trying to convey here.  Do you think the KDE guys just sit around with their thumbs up their arses all day?).  Your rants on Konqueror and Dolphin, for example, are so muddled that they are of no use to anyone, and it's hard to tell whether you even understand the goals of Dolphin (it is not intended to be \"simple\" - it is intended to be *usable*.  Not including service menus and a tree view *detracts from usability*, so it's no wonder that they were added - and yet you're trying to spin this as a bad thing that makes a mockery of Dolphin's stated goals!) or the usage of KParts (no, Konqueror \"will not be Dolphin\" when it is filebrowsing, but will merely embed the KPart.  I'd imagine that it is up to Konqueror whether it opens any URL in an external app or internally - just as is the case now) but of course, since you have stated that this is just a quick cut'n'run post, there's no point us asking, is there?\n\nI'm not even going to touch half of the other stuff - you harp on about Cervisia while the world now uses SVN, apparently ignoring the continued development of kdesvn.  You pretend that KGhostview isn't being killed off for KDE4.  You ignore the large amount of work that has gone into KGet in the make-kget-cool branch.  But no, nothing ever changes - except, of course, for all of the things that do and which render some of your complaints obsolete, but we'll ignore them, eh? And what is this \"so much effort churning into duplicating functionality with new applications\"? Are you referring to Dolphin which absorbs the massive resources of one man working in his spare time? \n\nIf you're not going to stay around and clarify or defend your rants, then please just keep them to yourself - you're doing nothing but sowing doubts about the competence and commitment of the KDE devs and you're doing it in such a way that it's hard to mount a defence of their actions (when they are defensible, of course)."
    author: "Anon"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-11
    body: "> Dolphin which absorbs the massive resources of one man working in \n> his spare time? \n\nNot to mention that AFAIK he's a fairly recent addition to the KDE team, injecting some welcome new life into KDE file management. Thank you Peter, just ignore all this negativity! :)"
    author: "Paul Eggleton"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-10
    body: "Do you actually know that ranting and not providing constructive criticism will not yield any result?\nAs a simple user, I'm getting tired of negative posts *every single digest issue*. I don't think this gives more motivations to the developers."
    author: "Luca Beltrame"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-10
    body: "> And now, Konqueror will be Dolphin while filebrowsing?\n\nNo, Konqueror will use its custom context menus and will also show files embedded as in KDE 3. Users won't notice that a Dolphin part is used for displaying the content. No feature from Konqueror offered in KDE 3 will be skipped in KDE 4...\n\n> IIRC Dolphin will open a lot of stuff in external viewers no?\n\nYes, Dolphin opens files in external viewers.\n\n> Isn't that what made it a \"file browser\" instead of\n> a \"universal viewer?\" Whatever the differences,\n> If Konqueror will now behave as Dolphin in filebrowsing mode\n> I don't see why it should even bother.\n\nThen please just don't bother... We are open for constructive input, but as you declared your post as 'rant' yourself, I see no need to investigate any time in giving some more background informations..."
    author: "ppenz"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-11
    body: "\"> And now, Konqueror will be Dolphin while filebrowsing?\n\nNo, Konqueror will use its custom context menus and will also show files embedded as in KDE 3. Users won't notice that a Dolphin part is used for displaying the content. No feature from Konqueror offered in KDE 3 will be skipped in KDE 4...\"\n\nAt last, some real information. Thank you.\n"
    author: "reihal"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-10
    body: "Ok, aftr the rant, the answer you deserve and that you looked for with the rant.\n\nYou're just a moron, useless to KDE, FLOSS and IT community in general because you criticize without even understanding basic concepts.\n\nDo you feel happier now?"
    author: "Vide"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-11
    body: "Some points are right. \n\nThe Digikam one, for example. Digikam is great, but it needs some serious usability work. \n\nI disagree on KOffice, I love the startup page, would want it in more apps. \n\nAnd yes, Kaffeine, I dumped it when it wanted to become a 'mediaplayer (retarded concept, if you ask me) instead of just a good videoplayer. And KTorrent IS becoming a bit complex, though we have KGet for the simple stuff."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-11
    body: "Digikam is a power-user app, and its UI is in some cases a mess. But in this case I think that the best solution is a tiny, little new app to do the dirty work that the vast majority needs while dealing with cameras.\n\nI'm planning tu implement one if I can pass the initial barrier to become a KDE dev :)"
    author: "Vide"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-11
    body: "well, I don't think digikam is a poweruser-app. Most ppl NEED a lot of the functionality it provides, and several apps do provide most of it in a much more usable way. There is also kphotoalbum, which offers an even more complex interface for the same thing, though with its advantages and disadvantages. I'd rather see you work on digikam, then, maybe you can persuade some usability person to give digikam a review? promising you'd implement it?\n\nand you can do a lot yourself, with the HIG and looking at how other apps (apple, windows) do things. Take the best from there, add own ideas - you'll get a long way in making it more usable..."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-12
    body: "I hope the digikam people have a good luck at Picasa (now it even runs on Linux :))... to me it's album management done right. Simple retouching is super fast - for complex stuff I can open gimp or krita or whatever, but seriously all I want to do is some cropping, sharpening, rotating etc. In Digikam right now there's not even a rotating button I think, and rotating brings up a very annoying progress window. But I'm sure it will rock for 4.0. Looking forward to it!"
    author: "Stian Haklev"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-12
    body: ">I hope the digikam people have a good luck at Picasa (now it even runs on >Linux :))\n\nIf Picasa become the standard photo-management program under Linux, there is no reason to do an open source photo-management program..."
    author: "RobertC"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-12
    body: "Please explain.\nI mean, a Picasa with complete KDE integration would be wonderful, and maybe one day Digikam could become this software"
    author: "Vide"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-13
    body: "Why ? digiKam is not currently integrated into KDE ???"
    author: "RobertC"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-13
    body: "Oh please, don't make digikam into somthing like picassa.\n\nI used picassa a week before I get back to digikam.\nWhile on windows, I used ACDSee.\nI had a look at KphotoAlbum.\n\nAnd ____For_Me____  Digikam is _by_far_ the best of them all."
    author: "kollum"
  - subject: "Re: rant.  please ignore."
    date: 2007-07-12
    body: "> I'm planning tu implement one if I can pass the initial barrier to become a KDE dev :)\n\nGood luck...\n\nWe working on digiKam project since 2002. I hope than you can have the same experience than us in this domain...\n\nGilles Caulier"
    author: "Caulier Gilles"
  - subject: "Requesting for details regarding the kioslaves"
    date: 2007-07-10
    body: "I'm just extremely anxious and curious about the removal of system:/ and media:/ kioslaves. I am aware that their current implementations are not even close to perfect. I'm just wondering whether alternatives to these have been considered.\n\nIf there's a mailing list thread I can refer to, or a proper place to ask this question, please do tell.\n\n(Because I hate to see the Dot become a ranting ground :P)"
    author: "Jucato"
  - subject: "Re: Requesting for details regarding the kioslaves"
    date: 2007-07-10
    body: "the replacement is simple. we just need to have a similar kio like solid:/ or so, which just have symlinks to the real folder: clicking on solid:/cdrom actually resolves to /media/cdrom and not to solid:/cdrom/whatever, how it was in kde3. that was not so good as argumented earlier, and thus it was removed.\nwhat we need is just a plain summary kio which just symlinks to the real paths. nothing more. period."
    author: "litb"
  - subject: "But is konqueror itself getting any love?"
    date: 2007-07-10
    body: "But is konqueror itself getting any love? It seemed alot worse of than dolphin in alpha 2 so i hope there are plans to really get it up to speed soon, it's my favourite kde application.\nI know that alot of code is shared between them and so on but i'd love to see some more work go into konq itself."
    author: "Marius"
  - subject: "Re: But is konqueror itself getting any love?"
    date: 2007-07-10
    body: "The file browsing kpart is getting love via dolphin, and KHTML is still being worked on, so both of those will get love, but the actual Konqueror application I don't think has been getting all that much attention (the code is supposedly a mess, which is part of the reason Dolphin was created instead of Konqueror being improved)."
    author: "Sutoka"
  - subject: "Konqueror improvement suggestions"
    date: 2007-07-10
    body: "Two weeks ago there was a thread about that there were no Konqueror development activities for a long time. Emil Sedgh asked Danny about this and he answered that 'Konqueror is just a shell around KDE Technologies.It hasnt much codebase.'\n\nThen I suggested a non-exhaustive list of improvements that could and should be made on Konqueror the shell as it is still an important KDE application and the default web browser but I was late so no answers came.\n\nSo here is my list:\n- Session handling (almost every other browser does this and it would also be usable in the file manager)\n- Better profile handling (more settings stored in profiles, e. g. toolbars)\n- Better tab handling: rearranging tabs (currently dragging a tab to an other place duplicates it), Undo close, etc.\n- Home button which reloads th start page of the current profile (for some people it is a major problem that the home button directs to the home directory when they browse the web)\n- Better arranged configuration area: a tiny central configuration and the configuration area of the filemanager (Dolphin), khtml, kpdf, whichever is currently used\n- File information sidebar. I know Dolphin has this, but it could also be implemented in KDE, as many people would continue to use Konqueror as a file manager. (See our debate at http://dot.kde.org/1172721427/1172897220/1172915550/1172918074/ . So is the purpose of Dolphin to be really a subset of Konqueror?)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Correction: Konqueror improvement suggestions"
    date: 2007-07-10
    body: "In the last suggestion I should have written Konqueror instead of KDE, sorry."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror improvement suggestions"
    date: 2007-07-10
    body: "\"Session handling (almost every other browser does this and it would also be usable in the file manager)\"\n\nKonqueror supports desktop session, i.e. if you log out with Konqueror open, when you log back in Konqueror will return to its previous state (I don't recall if back/foward history is restored with it though).\n\n\"Better profile handling (more settings stored in profiles, e. g. toolbars)\"\n\nFrom what I've heard/read the profile handling is a large part of the reason Konqueror's code base is a mess.\n\n\"Better tab handling: rearranging tabs (currently dragging a tab to an other place duplicates it), Undo close, etc.\"\n\nRearranging has long been possible, use middle mouse click and drag.  Undo close currently would require a lot of work from what I've read.\n\n\"Home button which reloads th start page of the current profile (for some people it is a major problem that the home button directs to the home directory when they browse the web)\"\n\nThis would be an interesting idea, though I personally think the profile system is a broken design (it shouldn't matter which button you click on, maybe profile is set based on what you're currently looking at, now what link you used to open Konqueror would work better?).\n\nIn some of my spare time I've been working on trying to rewrite Konqueror in a way that'd be more flexible and easier to maintain (though I don't have much experience using KParts or KIO in development so I'm having to learn about all that before I'm able to get much done)."
    author: "Sutoka"
  - subject: "Re: Konqueror improvement suggestions"
    date: 2007-07-10
    body: "\" From what I've heard/read the profile handling is a large part of the reason Konqueror's code base is a mess.\"\n\nSo it shuld be rewritten. It is not a good strategy to start writing a new program if the existing becames an unmaintainable mess, instead of restructuring the existing application.\n\n\"Konqueror supports desktop session, i.e. if you log out with Konqueror open, when you log back in Konqueror will return to its previous state (I don't recall if back/foward history is restored with it though).\"\n\nIt is good but I think real per-app session handling is still missing. One can use different sessions only by workarounding with profiles. And if Konqueror crashes (which, to be honest, not very rare), my session is gone. (Opera restores it, including history.)\n\n\"Rearranging has long been possible, use middle mouse click and drag.\"\n\nThank you, I didn't try this.\n\n\"Undo close currently would require a lot of work from what I've read.\"\n\nThen it should be really a mess. :-("
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror improvement suggestions"
    date: 2007-07-10
    body: "\"It is not a good strategy to start writing a new program if the existing becames an unmaintainable mess, instead of restructuring the existing application.\"\n\nSometimes an application becomes such a mess as to make anyone familiar with the code base pretend it doesn't exist, and scare away any potential developers, which is I believe pretty much the situation thats developed with Konqueror.\n\n\"And if Konqueror crashes (which, to be honest, not very rare), my session is gone. (Opera restores it, including history.)\"\n\nTheres actually a plugin to help with this situation.  Go to Settings -> Configure Extensions -> Tools tab -> and enable Crashes Monitor (might be enabled by default), then go to Tools -> Crashes and \"All pages of this crash\" for the right crash."
    author: "Sutoka"
  - subject: "Re: Konqueror improvement suggestions"
    date: 2007-07-10
    body: "\"Undo close currently would require a lot of work from what I've read.\"\n\nNot sure if it's in SVN yet, but it sounds like most of the work is done.\nhttp://lists.kde.org/?l=kfm-devel&m=117563613117290&w=2"
    author: "Morty"
  - subject: "Re: Konqueror improvement suggestions"
    date: 2007-07-11
    body: "For file information bar, see my Cbar Konqueror sidebar: http://www.kde-apps.org/content/show.php/Cbar?content=46391\n\nIt is easily extensible with Python scripts even :)\n"
    author: "Krzysztof Lichota"
  - subject: "Re: Konqueror improvement suggestions"
    date: 2007-07-11
    body: "It looks good although it could be a bit nicer. If it is well integrated with KDE, it should be distributed with Konqueror."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Koffice and open document"
    date: 2007-07-11
    body: "I was wondering if anyone could clarify what commits like this one for kspread\n\nOpenDocument Loading/saving.\n\nI thought kspread was already using odf. I'm extra curious what this means in the context of flake since I've also seen mention of flake rendering or saving in odf. For things like kword and kspread that already use opendocument, what do these commits mean?\n\nThanks for all your work!"
    author: "Christian"
  - subject: "Re: Koffice and open document"
    date: 2007-07-11
    body: "Hi Christian,\n\nin KSpread things got improved. You may also like to take a look at the bidi-thread in the koffice-devel mailinglist for a more concrete example.\n\nKWord got in rather large parts refactored and partly rewritten (e.g. the text-engine got replaced and is now build up on Qt4 scribe). So, there things really changed drastically and while they still stays a long way to go, we already made huge progress. Let me say thanks to google here for sponsoring us (see http://techbase.kde.org/Projects/Summer_of_Code/2007/Projects/KWordOpenDoc ) and for sure thanks to Pinaraf! :) You may also like to look at http://wiki.koffice.org/index.php?title=KOffice2/Goals#KoText (well, probably not really that complete but at least it provides a better overview then I am able to write down here).\n\nIn the context of flake it does mean, that things are now much better integrated and it feels really good to take a look at the codebase and to realize what was already done. While each flake-component does provide load/save odf functionality it may not direct related to the KSpread OpenDoc-loading/saving here as long as you don't integrate flake-components into KSpread or use KSpread itself as flake-component."
    author: "Sebastian Sauer"
  - subject: "Re: Koffice and open document"
    date: 2007-07-11
    body: "after sending the reply I even did take a look at the commit you are refering too and seems we both did misunderstood it since if we follow the link to what was actualy committed, it's clear, that the \"initial opendoc loading/saving\" was only related to auto-filters. So, auto-filters will be handled too now :)\n"
    author: "Sebastian Sauer"
  - subject: "Dolphin becoming a Whale"
    date: 2007-07-11
    body: "\"Dolphin embedded as the file management view in Konqueror\"\n\nI thought we were going to have a choice on which to use. If I don't like Dolphin, and Dolphin powers the file management in Konqueror, how do I end up having a choice?"
    author: "Nach"
  - subject: "Re: Dolphin becoming a Whale"
    date: 2007-07-11
    body: "Konqueror will be just like Konqueror of KDE 3 (whit improvements of course), for and end-user kike there isn't difference.\n\nIt's just using Dolphin Kpart for browse folders (which is good, Dolphin is unmesurable faster than Konqueror), for open a PDF inside it will use another Kpart, etc.\n\nSorry for my english."
    author: "Luis"
  - subject: "Re: Dolphin becoming a Whale"
    date: 2007-07-11
    body: "for and end-user there isn't difference*\n\nSorry xD."
    author: "Luis"
  - subject: "Re: Dolphin becoming a Whale"
    date: 2007-07-13
    body: "Dolphin *can* be embedded in konqueror; like KWord can be.  Doesn't mean you have to, you can start it without konqueror just fine.\n\nIn other words; its an extra feature, not relevant for people that don't want to use it."
    author: "Thomas Zander"
---
In <a href="http://commit-digest.org/issues/2007-07-08/">this week's KDE Commit-Digest</a>: <a href="http://conference2007.kde.org/">Akademy 2007</a> draws to a close. <a href="http://enzosworld.gmxhome.de/">Dolphin</a> embedded as the file management view in <a href="http://www.konqueror.org/">Konqueror</a>. <a href="http://plasma.kde.org/">Plasma</a> continues to mature, with new data engines for Tasks and Bluetooth, and <a href="http://englishbreakfastnetwork.org/">EBN</a> and Task Manager Plasmoids making an introduction. Further progress in Javascript bindings through QtScript; import of Kimono (C#) classes. More basic functionality added to Kollagame, a game development IDE. Initial work in the <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=FBBF1AFF3A34E55B">KWin/Xinerama</a> and <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=DCE4DBD4A0509DC7">2d Projection for Marble</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> projects, with continued progress in the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=1EF6392A4C8AEADD">Icon Cache</a>, <a href="http://code.google.com/soc/kde/appinfo.html?csaid=5D66C1579098460C">KOrganizer Theming</a>, <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9064143E62AF5BA6">KRDC</a> and <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=9B5F3C70D02AA396">Music Notation</a> projects. KListView gets support for keyboard navigation, and a new, more descriptive name: KCategorizedView. KIconCache renamed to KPixmapCache to reflect its wider benefits to graphics across KDE. Paint mixing improvements and general speed optimisations in <a href="http://www.koffice.org/krita/">Krita</a>. <a href="http://kontact.kde.org/kmail/">KMail</a> and <a href="http://www.mailody.net/">Mailody</a> now share account identities. Support for more digital camera models in <a href="http://www.digikam.org/">Digikam</a> libraries, with the porting of many image plugins to KDE 4. More interface and collection management work towards <a href="http://amarok.kde.org/">Amarok</a> 2. More effects for kwin_composite. <a href="http://decibel.kde.org/">Decibel</a> is moved to playground/pim. system:/ and home:/ KIOSlaves removed, with preparations to remove media:/ in the near future.
<!--break-->
