---
title: "Qt Centre Programming Contest"
date:    2007-01-05
authors:
  - "wwysota"
slug:    qt-centre-programming-contest
comments:
  - subject: "Hmmmm"
    date: 2007-01-06
    body: "So you have to do a lot of work just to enter the contest (write up of what you want to do, with algorithms that you'll use, mockups, API, and so on).\n\nThen they can accept or reject you.\n\nIf you get accepted, then that just means you can enter the contest.\n\nAfter entering, you then code it up, and put it under a software license that they allow.\n\nNot only does it have to be fully coded (no missing features at all), but you have to get it fully working on 3 different OS's and quite a few other strict requirements.\n\nAt the end of this, they _may_ chose some winners.  They may not however.\nAnd finally, for winning, you receive 'a prize'  (note that very carefully do not say that even if you win first prize in a category that you will win anything particularly exciting)\n\n\n\nPlease someone tell me I'm misreading this."
    author: "John Tapsell"
  - subject: "Re: Hmmmm"
    date: 2007-01-06
    body: "> So you have to do a lot of work just to enter the contest (write up of what \n> you want to do, with algorithms that you'll use, mockups, API, and so on).\n\nIf you read closely, you'll see that it isn't that hard. It's clearly written that it should be \"without details\" and it \"is only meant to be a general description\". If you have any doubts, check the example (http://contest.qtcentre.org/example.pdf).\n \n> Then they can accept or reject you.\n\nWe will reject only those submissions which don't follow the rules (http://contest.qtcentre.org/rules).\n \n> put it under a software license that they allow.\n\nYes, but in fact we allow *all* Open Source licences.\n \n> Not only does it have to be fully coded (no missing features at all) [...] \n> and quite a few other strict requirements.\n\nNot quite, these aren't strict requirements. Just the more requirements your project meets the bigger chance you have for a prize.\n\n> have to get it fully working on 3 different OS's \n\nNot 3, but 2 and with Qt that's really not a problem.\n\n> At the end of this, they _may_ chose some winners. They may not however.\n> And finally, for winning, you receive 'a prize' (note that very carefully do \n> not say that even if you win first prize in a category that you will win\n> anything particularly exciting)\n\nYes, we have a secret plan to keep all this stuff (http://contest.qtcentre.org/prizes) to ourselves. ;)\n \n> Please someone tell me I'm misreading this.\n\nYes, you are.\n"
    author: "Jacek Piotrowski"
  - subject: "Re: Hmmmm"
    date: 2007-01-07
    body: "Fair enough.  I over-reacted somewhat - I'm sorry about that.\nMust have woke up the wrong side of the bed today ;-)"
    author: "John Tapsell"
  - subject: "Re: Hmmmm"
    date: 2007-01-07
    body: "Well, i too thought it looked like a nice idea, but all the extra formal work means I'll probably not enter. Testing required? Come one... Testing UI applications automatically is a major pain, and can at best only test absolutely trivial stuff.\n\nAnd as the world is anyway full of much better developers than me, it would be a waste of time anyway. :)\n"
    author: "Chakie"
  - subject: "Re: Hmmmm"
    date: 2007-01-07
    body: "if you were to write a KDE application, you could probably use the freely available KDE edition of the Froglogic Squish GUI testing tool. check\nhttp://www.froglogic.com/pg?id=Products&category=squish&sub=editions&subsub=kde"
    author: "tester"
  - subject: "Re: Hmmmm"
    date: 2007-01-08
    body: "> Testing required? Come one... Testing UI applications automatically is a major pain, and can at best only test absolutely trivial stuff.\n\nNobody said you have to test the UI... And I think you should always test the logic of the application, don't you? And I'm sure you already do that by injecting tests directly into the application and disabling them when you don't need them anymore. Your application (and you too) can only benefit from performing tests.\n\nBelieve it or not - Trolltech performs thousands of automated tests on Qt itself everyday. They even have dedicated machines for that.\n\nThe pain is only to write the test for the first time, not to execute it once and again when you already have it. And using test libs (like, but not limited to, QtTestLib) makes it even easier and produces better production code. Not performing regressive tests can be fatal to the development of any system that is bigger than a thousand LOC.\n\nAnd as Brandybuck already said - prizes are worth the effort.\n\nBesides, the testing is only mentioned as a hint if you want to increase your chances of winning :) It's not a strict requirement - read the rules of the contest (http://contest.qtcentre.org/rules).\n\nCheers :)"
    author: "Witold Wysota"
  - subject: "Re: Hmmmm"
    date: 2007-01-08
    body: "> Nobody said you have to test the UI... \n\nActually they give three suggestions for how to test your app.  I didn't recognise the first one, but the other 2 were definitely  UI testing apps. (There was an article on squish a few months ago)."
    author: "John Tapsell"
  - subject: "Re: Hmmmm"
    date: 2007-01-07
    body: "* Write a one page submission. That isn't difficult. See the example.\n\n* All submissions that meet the rules will be accepted.\n\n* You may use any Open Source license.\n\n* The software needs to work. Is that so onerous?\n\n* It only needs to work on two different systems.\n\n* Personally I would consider a new MacBook or iPod to be an exiting prize.\n\n* You are misreading this."
    author: "Brandybuck"
  - subject: "You forgot"
    date: 2007-01-06
    body: "to mention that large projects are preferred.\nSo if your project is the lucky one, you may share your award among a couple of devs.\nPretty lousy! :)"
    author: "katakombi"
  - subject: "Re: You forgot"
    date: 2007-01-06
    body: "> You forgot to mention that large projects are preferred.\n\nHu?  Read again:\n\n\"In case of doubts when declaring the winner, this requirement may be used to resolve the issue in favour of the smaller team.\"\n\n"
    author: "cm"
  - subject: "No Jambi"
    date: 2007-01-10
    body: "The rules state you can use QtJambi; but as there still is no final release of Jambi it is impossible to comply with the open source requirement. As you are not allowed to release the code written against Jambi while it is in testing.\n\nOn the mailinglist they stated that next month another beta will be released of Jambi (so still not usable) and no indication when the final version is out."
    author: "Thomas Zander"
---
Qt community site <a href="http://www.qtcentre.org">Qt Centre</a> is celebrating its first anniversary with a <a href="http://contest.qtcentre.org/">programming contest</a>. The contest is open until the end of May and the winning entries will be the best Qt 4 or Qtopia application or component, each selected from one of several categories. Great prizes to be won include a MacBook and two <a href="http://www.trolltech.com/products/qtopia/greenphone">Qtopia Greenphones</a>.  The contest is sponsored by <a href="http://www.basyskom.com/">Basyskom</a>, <a href="http://www.froglogic.com/">froglogic</a>, <a href="http://www.ics.com/">ICS</a>, <a href="http://www.kdab.net/">KDAB</a> and <a href="http://www.trolltech.com/">Trolltech</a>.



<!--break-->
