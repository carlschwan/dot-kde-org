---
title: "Report from the Decibel Hackathon"
date:    2007-03-23
authors:
  - "thunger"
slug:    report-decibel-hackathon
comments:
  - subject: "Great! "
    date: 2007-03-25
    body: "This seems to be promising.\n\nThe idea of such little meetings is really good since it increases links between developers. And I guess it's easier to exchange ideas from man to man rather than on IRC ?"
    author: "Marc"
  - subject: "I hope"
    date: 2007-03-27
    body: "this means that all these fabulous new contact handling services, storage systems, frameworks and what have you not will actually be merged from a userapp POV: One person, one contact entry, have a nice weekend.\n\nDon't get me wrong, all of them seem to have really good ideas. I just want to see it all come together on the desktop.\n\n/Bent\n"
    author: "Bent Terp"
---
At the Decibel Hackathon sponsored by <a
href="http://nlnet.nl/">NLnet</a> and <a
href="http://www.basyskom.de/">basysKom GmbH</a> in <a
href="http://en.wikipedia.org/wiki/Darmstadt">Darmstadt, Germany</a> last
weekend, hackers from the <a
href="http://www.kde.org/">KDE</a> community met to discuss the handling
of contact data in KDE 4. Read on for a summary of the event. 











<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex">
<a href="http://static.kdenews.org/danimo/decibel-groupphoto.jpg">
<img src="http://static.kdenews.org/danimo/decibel-groupphoto_small.jpg" border="0" width="300" height="199" /></a><br />
<a href="http://static.kdenews.org/danimo/decibel-groupphoto.jpg">
<small>From left to right: Duncan Mac-Vicar Prett,<br />Friedrich Kossebau, Will Stephenson, Tobias<br /> Hunger, Volker Krause, Stanislav Karchebny,<br /> Dominik Haumann</small>
</a>
</div>

<p>First Tobias Hunger (<a href="http://decibel.kde.org/">Decibel</a>) and
Will Stephenson (<a href="http://kopete.kde.org/">Kopete</a>) described
the needs of their projects for data storage. Then Volker Krause from <a
href="http://pim.kde.org/akonadi/">Akonadi</a> gave an introduction to
their data storage system, followed by a presentation on the <a
href="http://kde-apps.org/content/show.php/Khalkhi+framework?content=54450">Khalkhi</a>
framework done by Friedrich Kossebau. With these presentations forming a
basis for the further discussions we were able to define workflows to
handle contact data across the new frameworks involved in KDE 4. Dominik
Haumann (<a href="http://kate-editor.org/">Kate</a>), Duncan Mac-Vicar
(Kopete), Geert Vlastuin (not involved with open source projects before)
and Stanislav Karchebny (<a href="http://www.skype.com/">Skype</a>)
provided valuable input, addressing needs they forsee from their own
perspective.</p>

<p>
More tangible results of the meeting include updates to Decibel's as well
as QtTapioca's build system to work on Decibel to make it optionally a KDE component as well as a pure Qt application, fixes for several warnings found, as well as stability improvements. Further details can be found in the blog posts by <a
href="http://www.kdedevelopers.org/node/2728">Will</a>, <a
href="http://duncan.mac-vicar.com/blog/archives/197">Duncan</a>, <a
href="http://nodalpoint.madfire.net/articles/2007/03/19/sounds-of-the-shuttle">Stanislav</a>
and <a
href="http://basysblog.org/index.php/archives/decibel-hackathon">Tobias</a>.
</p>

<p>The event was a success, fostering the integration of different KDE
technologies and improving the interaction between developers on a
personal level over some beer and pizza on Saturday evening.</p>

















