---
title: "KDE Commit-Digest for 16th September 2007"
date:    2007-09-17
authors:
  - "dallen"
slug:    kde-commit-digest-16th-september-2007
comments:
  - subject: "impressed"
    date: 2007-09-17
    body: "Wow, I'm really, _really_ impressed by the music support for koffice, it looks extremely good and I'm sure will be very usefull! Is this all written by just Marijn Kruisselbrink in just the summer of code? Impressive!\n\nDo you know if there also will be a little program specialised in writing music like kformula is for formules?\n\nI think KOffice is really going to rock when it's released!"
    author: "david"
  - subject: "Re: impressed"
    date: 2007-09-17
    body: "Owh and I had another question, do you think musicxml can be added in the future to the odt standart like I think mathml is too?\n\nThanks,\nDavid"
    author: "david"
  - subject: "Re: impressed"
    date: 2007-09-17
    body: "First take your go 'n kill the Open XML standard"
    author: "Martin"
  - subject: "Re: impressed"
    date: 2007-09-17
    body: "One way to kill that standard is to make ODF useful enough that people use it.   Open XML gets attention because a lot of people use Microsoft Office.   The more people we can get using something else, the better chance we have of killing the idea of making Open XML a standard.\n\nTo that end, adding things like MusicXML to the standard makes ODF useful to people like musicians.   (Particularly if we can get someone from Open Office to add support for it, but also getting support into the programs they use, Finaly for instance)\n\nWe can't move everyone to ODF at once, but move small groups over, and those small groups together have power to force the rest of the world."
    author: "Hank Miller"
  - subject: "Re: impressed"
    date: 2007-09-17
    body: "Let others fight _against_ it and we fight _for_ improving the usability and feature set, so that there are *good* and solid alternatives available"
    author: "she"
  - subject: "KDE 4.0"
    date: 2007-09-17
    body: "KDE 4.0 is going to be great. Guys, keep it up."
    author: "neurondev"
  - subject: "kplayer rocks"
    date: 2007-09-17
    body: "Glad that kplayer has been ported to KDE 4.  That is the best front-end to Mplayer that handles video in general and videocds, audiocds and video dvds in particular flawlessly.  The way it has been designed to work with discs is especially brilliant.  Congrats to the developers"
    author: "Ask"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "anybody knows how it compares to smplayer ? it also seems to be a very nice frontend (it worked when plain mplayer didn't for me...) and is moving to qt4."
    author: "richlv"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "Thing is, why don't smplayer/kmplayer/kplayer developers collaborate together? Do they have different goals for their players or do they enjoy competing to see who can produce the best? As a bystander, it seems like needless duplication of effort but of course I could be wrong."
    author: "Alan Denton"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "It's already getting better, the whole player engine is being abstracted away by Phonon. The simple players will really be the same, and I think these will disappear. Only the more complex ones will differentiate enough to survive. Give it some time..."
    author: "jospoortvliet"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "Do you know what's the status of Video Player, Codeine's KDE4 version?\nThat's really the one video/DVD player I want, since I finally switched to Amarok for my music."
    author: "Richard Van Den Boom"
  - subject: "smplayer is better ;-)"
    date: 2007-09-17
    body: "SMPlayer is by far the best, no?\n\nIt is based on QT4 and Mplayer.\nFeature wise it is hard to beat!\nCheck : http://smplayer.sourceforge.net/en/linux/index.php"
    author: "veton"
  - subject: "Re: smplayer is better ;-)"
    date: 2007-09-18
    body: "smplayer has lots of features I can see from the screenshot. It has like 25 buttons, too busy for my taste. Video Player will have like a button and two sliders by default. Anyways it appears they fill different niches in the video player ecosystem.\n\nHowever I read that smplayer remembers settings for video files, this was always the killer feature for Codeine for me. So that is pretty awesome. :) Every video player should have this IMO."
    author: "Ian Monroe"
  - subject: "Re: smplayer is better ;-)"
    date: 2007-09-18
    body: "Absolutely, for 95% of my video watching needs, codeine is the absolute best choice out there.  Only when I want to watch certain wmv files I use VLC, and only because xine can't decode them properly (lots of artifacts)."
    author: "Leo S"
  - subject: "Re: smplayer is better ;-)"
    date: 2007-09-18
    body: "see this entry on Xine's bugtracker: http://sourceforge.net/tracker/index.php?func=detail&aid=1795720&group_id=9655&atid=109655\n\ncertain videos work with ffmpeg (ffplay), mplayer (ffmpeg), mplayer (win32) and xine (win32, you can force it to use the dlls) but are broken in xine (ffmpeg).  Really curious.\n\nAnyway, my favourite frontend remains kaffeine 0.4.3.  Just the buttons I need , simple keybindings, and I like having language settings right there, and a vertical volume slider makes me happy  http://img64.imageshack.us/my.php?image=kaffeine7gu.png"
    author: "MamiyaOtaru"
  - subject: "Re: smplayer is better ;-)"
    date: 2007-09-18
    body: "That's one thing I would like to have in Codeine / Video Player : the ability to select language for sound and subtitles through a right click option.\nVery nice when the video is set full screen, you don't have to reduce it to access a menu entry."
    author: "Richard Van Den Boom"
  - subject: "Re: smplayer is better ;-)"
    date: 2007-09-18
    body: "Exactly. Just one button, one or two sliders, starts almost instantly when I double click on a video file, remembers settings for each video file AND no system tray.\nAs far as I am concerned, I prefer xine support than mplayer, since I'm running Slackware and xine is provided with the Slack.\nSo yes, bring in Codeine / Video Player as it is, that's all I want.\nI understood you had your hands full with Amarok, but do you think Video Player will be part of KDE 4.0 or do you plan to release it outside KDE?"
    author: "Richard Van Den Boom"
  - subject: "Re: smplayer is better ;-)"
    date: 2007-09-18
    body: "KPlayer has even more features, they are just less intrusive and not visible in the initial install, but are very easy to find and turn on.\n\nKPlayer is more complete compared to SMPlayer and everything else. The multimedia library and the device handling are the killer features. Playlist handling is very nice as well."
    author: "hmmm"
  - subject: "Re: smplayer is better ;-)"
    date: 2007-10-02
    body: "Definitely too busy. I wrote my own \"ideal video frontend\" and it's just a PyGTK XEmbed socket in a frame (so MPlayer can't un-minimize against my wishes when it changes files) and some Python scripting to keep track of which anime episodes I've already watched.\n\nIt looks and acts just like the non-GTK mplayer binary... just a bit more intelligent. The TODO list is still fairly large but it's usable and you can pull the bzr repo at https://launchpad.net/animu-player/ if you're curious.\n\nOh, in case anyone is still reading, here are some of the ideas I had for future features:\n- higher-resolution tracking of where I left off in a file which will facilitate...\n- ...a mode which creates virtual channels on-the-fly using a heuristic guesser and allows channel surfing with IR remote support via PyLIRC (so I can comfortably browse anime from my bed\n- A simple menu/dialog with HAL support so I can stick a DVD+R in the drive and resume where I left off with two clicks and no more. (Or two button-presses on the IR remote)\n- etc. etc. etc.\n\nYeah... I'm *very* lazy."
    author: "Stephan Sokolow"
  - subject: "Re: kplayer rocks"
    date: 2007-09-18
    body: "Vir made it compile recently, but it crashes on startup. Amarok 2 is taking up my coding time currently."
    author: "Ian Monroe"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "I wonder if there will be a plasmoid for being able to reproduce a video in a small window and always in foreground. Sometimes I like to watch videos while browsing :)"
    author: "David"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "These multitask people.... ;-)"
    author: "No one"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "Why do you need a plasmoid for that? Any window in KDE can be made to stay above other windows, so you can already do what you want *now*. "
    author: "Andr\u00e9"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "...because plasmoids are the latest hype today. I think people would even implement start menus and mail servers as plasmoids these days..."
    author: "me"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "well, work on a startmenu is going on, I dunno about mail servers - but I'm pretty sure you'll be able to contact them using aKonadi ;-)"
    author: "jospoortvliet"
  - subject: "Re: kplayer rocks"
    date: 2007-09-17
    body: "of course, it won't be complete until there is an irc client and a text editor. =P"
    author: "Aaron Seigo"
  - subject: "Re: kplayer rocks"
    date: 2007-09-18
    body: "Great text editor, could use a desktop though. ;)"
    author: "Sutoka"
  - subject: "Re: kplayer rocks"
    date: 2007-09-18
    body: "Maybe someone can make an emacs plasmoid and get all that at once ;)"
    author: "MamiyaOtaru"
  - subject: "Re: kplayer rocks"
    date: 2007-09-18
    body: "For that, we would just embed a KPart in a Plasmoid, no? :-)"
    author: "Andr\u00e9"
  - subject: "Re: kplayer rocks"
    date: 2007-09-18
    body: "\nWould be so cool to have Kaffeine and Amarok in a single multimedia app.\n\nI have a lot of Music titles stored as video clips, but not all. And I'd like to have playlists that mix video clips and mp3 files.\n\nWhen friends are at home, I'm using a video projector to display video clips with kaffeine, and I'd be happy to have mp3 files played within video clips (displaying visual effects). But AFAIK, it's not possible for now (while it is using winamp on windows).\n\nEach apps would benefit the other app great features. Multimedia desktops like MythTV and such are only regrouping multimedia applications and thus does not permit (AFAIK) the above.\n\nThough that's only my personal feeling...."
    author: "olahaye74"
  - subject: "Re: kplayer rocks"
    date: 2007-09-18
    body: "That's a cool idea - a \"Media Konqueror\" of sorts. But seeing as how the swiss army knife approach is being abandoned in KDE 4 (Konqueror->Dolphin as default file manager), such a program would probably only be an optional extra..."
    author: "Darryl Wheatley"
  - subject: "Re: kplayer rocks"
    date: 2007-09-18
    body: "You just described KPlayer precisely. Go try it, you'll like it."
    author: "aha"
  - subject: "KDE-Edu Polishing Day"
    date: 2007-09-17
    body: "The commit digest does not mention that on Saturday the KDE-Edu team had a debug day (Polishing Day) and fixed several bugs. GUI also was improved following users feedback (KHangMan, Marble, KGeography and blinKen). Very often us developers know our application so well that we are not able to see it the way the user does. \nThanks to all people who participated. Special thanks to Albert \"tsdgeos\" who fixed a lot of code!\nWe now need to set up a Polishing Day for more KDE core components!\n"
    author: "annma"
  - subject: "Re: KDE-Edu Polishing Day"
    date: 2007-09-18
    body: "This is an important point.  In fact, I think it's so important that you should write a separate article about it.  Lots of pics of 'before' and 'after'. Kudos to the participants.  Descriptions of the adorations of the masses.  That sort of thing."
    author: "Inge Wallin"
  - subject: "Akonadi"
    date: 2007-09-17
    body: "Its really nice to hear from Akonadi\nkeep up the good work PIM dudes..."
    author: "Emil Sedgh"
  - subject: "KTeaTime!"
    date: 2007-09-17
    body: "Woot!  Great app, small, simple and to the point.  Easy to forget when coding....  :)"
    author: "am"
  - subject: "Re: KTeaTime!"
    date: 2007-09-17
    body: "Without KTeaTime, my pizza would have burnt in the oven several times."
    author: "Stefan"
  - subject: "lots of data engines"
    date: 2007-09-17
    body: "These plasma data engines will definitely be usefull, but a question keeps nagging at me : aren't we reimplementing a read-only, kde-centric version of DBUS ? I have a vague feeling that plasma engines are more ressource-efficient, but that's the only advantage I can think of.\n\nWhat's the advantages of plasma engines over DBUS and vice-versa ? When should we use one over the other ? Does it make sense to write \"DBUS plasma engine\" and \"plasma DBUS service\" bridges ?"
    author: "Vincent de Phily"
  - subject: "Re: lots of data engines"
    date: 2007-09-17
    body: "dbus is a communication protocol. plasma data engines GENERATE the data, eg download & parse data from several weathersites and expose it to the plasmoids (that last part could be dbus, maybe it even is, I dunno)."
    author: "jospoortvliet"
  - subject: "Re: lots of data engines"
    date: 2007-09-17
    body: "The plasmoids and data engines are all in the same process and so there is no need to use dbus. The data engines just emits a signal with a hash of names and their values, and a slot in the plasmoid picks that data up."
    author: "Richard Dale"
  - subject: "Re: lots of data engines"
    date: 2007-09-17
    body: "the point of DataEngines is to provide a uniform API that is data-centric to a variety of sets of information. the implications of this are many, including the ability to write generic visualizations (meters, text displays, etc) that can be connected to any engine or the ability to easily access a wide variety of data from scripting languages. the other goal is to keep data presentation *simple* as opposed to complex custom data structures.\n\nit's a very specific (to the use cases of plasma) model/view implementation. dbus is inter-process communication. the two have nothing to do with each other, other than the fact that some engines use dbus internally."
    author: "Aaron Seigo"
  - subject: "I don't really know where to write this so..."
    date: 2007-09-17
    body: "... I would just want to suggest to KWord developpers to have a look at Lotus Word Pro. I don't think it's still developped by IBM but even the 2000 versions would do.\nThere are a lot of wonderful ideas in this soft, like a tab for chapters, tabs that you can reorganize by drag/drop (with automatic adjustments of indexes and tables of contents), automatic formatting, contextual attribute box. I've never find any word processor, free or not, to be as easy and powerful as this particular one. Since a lot of work is made on KOffice, I wonder if it's not the good moment to suggest to adapt some of these features."
    author: "Richard Van Den Boom"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-18
    body: "Hear, hear!  I absolutely agree with this.  Lotus Word Pro is still the most elegant Word Processor I have every used, especially with its lovely modeless, \"floating palette\" interface.  The only piece of software I miss from my Windows days.  I hope that Open Office or KOffice can take a look at this and seriously begin implementing its features, especially the beautiful GUI."
    author: "The Vicar"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-19
    body: "you mean the Lotus that IBM just put up as a free download, for both windows and linux? :) http://symphony.lotus.com/software/lotus/symphony/home.jspa "
    author: "Chani"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-19
    body: "I've read that it was based on OpenOffice, so I don't expect to have Word Pro in it.\nWell, I'm downloading it anyway, so I can see what it is.\nI'd prefer an Open Source office suite, though."
    author: "Richard Van Den Boom"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-19
    body: "I'll gladly borrow good ideas; but I don't have the sofware. Please spent some time explaining them. Maybe a long mail to the mailinglist?  Or a couple of wishlist items on bugs.kde.org?"
    author: "Thomas Zander"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-19
    body: "OK, I'm going to send a mail to the mailing list.\nI still have one version somewhere, I can probably install one to take some screenshots.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-19
    body: "Hello Thomas,\n\n\nI raised the issue with the Abi people quite some time ago.  If you look at this report in their bugzilla, you will see many screenshots of the Word Pro interface and a great deal of information about how it all works:\n\nhttp://bugzilla.abisource.com/show_bug.cgi?id=7283\n\n\nI hope you will seriously consider this as most people who have used Word Pro have (anecdotally) reported it to be the most intuitive and elegant word processor that they have used.  The same floating palette-tab system was used throughout the suite.  "
    author: "The Vicar"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-20
    body: "I don't know if my mail went through to the Koffice mailing list, Gmail seems weird in the sense that I don't seem to get back the mails I sent to ML in general.\nHere's a copy, sorry for double posting :\n\n***\n\nHi,\n\nfirst, thanks a lot for the wonderful work done on Koffice, I've been using it\nquite often for years now and am really impressed by how far it has come.\n\nFollowing a bit of a stupid post I made on the Dot, Thomas Zander suggested me\nto post it on the Koffice mailing list, so here we go.\n\nLotus Word Pro is definitely the most confortable, powerful, well designed\nword processor that I've used for the last ten years. It had (I use the\npassed time because it seems to have been killed with Smartsuite by IBM) many\nfeatures I've never seen in any word processor and which I found using it\nextremely useful.\nSince Koffice is having a large overal right now, I though maybe it could be a\ngood idea to point out these features, if some of them could perchance be\nimplemented easily :\n\n***\n1/ Usage of styles :\nWord Pro used styles for everything : paragraphs, characters, frames, tables\nlike many now, but also headers, footers, page layout. You could create a\nstyle for every single object you can put in a document. And of course, you\ncould import and export styles. That made creating page layout almost as\npowerful as a PAO software.\n\n2/ A single, contextual, attribute box :\nThere was only one box displaying styles and attributes. The display was\ncontext sensitive, meaning the displayed styles and attributes were dependent\non the selected object. Any change in the attribute box was reflected on the\nselected object dynamically. This made formatting much quicker than having a\npop-up box, select your options then validate/apply. And it reduced quite a\nlot the crowing of the interface. This is a bit what Scribus has now, but the\nbox was small, with many tabs, instead of these rows of options that I don't\nfind very practical in Scribus. There was a drop-down menu also allowing to\nselect what type of attributes you wanted to view, in case the selected\nobject allowed for various settings.\n\n3/ Organize your docs in tabs\nYou could organize your documents in several divisions, each one appearing as\na tab. Each division could have a completely specific style of page layout,\nnumbering, styles. It was completely as a separate document, to the point\nthat you could import an external file as a division. The interest is that\npage numbering, table of contents and indexes could be generated on the whole\ndocument. You could reorganize tabs ( like chapters) by simply drag and\ndropping, and page numbering was automatically reflected (though I think that\nToC and Index needed to be updated). This was really great to deal with large\ndocuments with many chapters.\nNote that this generalize basically the notion of Sheets in Spreadsheets.\n\n4/ Quick format\nThere was an option that came a lot later to Word, which is quick formatting.\nBasically, it allow you select part of the text, click on the icon, and then\nevery part of text that you select will be formatted the same way, until you\nclick again on quick formatting. Word Pro offered an added goodie, in the\nsense that is allowed either to apply the actual formatting of the initially\nselected text, or the style. The later made changes so much quicker.\n\n5/ Research / replace styles\nIt was possible to look for occurence of a style and replace it with another.\nBelieve me, it was wonderful.\n\n***\n\nWell, that's all that comes to my mind, right now. I would need to work with\nit again to remember other useful features. But the ones above are obviously\nthose I miss the most.\nPlease let me know if you want some screenshots or more explanations.\n\nAgain, thank you all for all your work.\nBest regards,\n\n       Richard Van Den Boom"
    author: "Richard Van Den Boom"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-21
    body: "Richard,\n\n\nI have also emailed Thomas directly with some specific detailed information relating to the \"single, contextual, attribute box\" which was the feature I most desire in a Linux word processor.  He was very gracious in his reply and promised he would look into it."
    author: "The Vicar"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-21
    body: "Yes, he also replied to me on the ML (which means my mail did go through) and had a very positive attitude, and that's really a strong quality, as I suppose a lot of people make requests of specific features they would appreciate in Koffice. \n"
    author: "Richard Van Den Boom"
  - subject: "Re: I don't really know where to write this so..."
    date: 2007-09-21
    body: "Yes, its great.  Hopefully something will emerge from this.  :-)\n\nOn places like OSNews, there have been a number of people in the past who have been very wistful for a Lotus approach to one of the Linux wordprocessors, so if KWord takes these requests onboard, it should please quite a few users."
    author: "The Vicar"
  - subject: "where is the work in Plasma?"
    date: 2007-09-17
    body: "two weeks ago under the Digest, there was a discussion about Plasma improvements, I said that I do not see any and it was explained to me that they are in \"playground\" and that they should be moved to kdebase soon ... a few days later, after rebuild from svn, I experienced some of the Plasma-stuff working\n\nnow there is mentioned desktop switcher applet and other Plasma things, but I do not see anything like that (I am just finishing fresh rebuild from svn, kdebase already done, I would not expect it within kdegames which are not finished yet) - time for another complaint?"
    author: "kavol"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-17
    body: "\"time for another complaint?\"\n\nFor God's sake, no.\n\nJust build trunk/playground/plasma."
    author: "Anon"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-17
    body: "Isn't it trunk/playground/base/plasma?\nWell, wherever it is, complaining won't help at all."
    author: "Hans"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-18
    body: "> \"time for another complaint?\"\n> For God's sake, no.\n\ndo not take me wrong - I do not want to say that KDE developers are not doing good, but ...\n\n> Just build trunk/playground/plasma.\n\n... last time it was said something like that things in the playground are not meant to be used (and tested) by others; hey, we are after second beta, features should be frozen, and the users are told that there is much more than within the \"standard\" packages, and they still should not report bugs? - I smell something bad in it\n\n(I repeat once again: I think that the developers are doing a good job, they are progressing nicely, but they are not meeting the user expectations based on what is told)"
    author: "kavol"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-20
    body: "While playground is indeed a place for things that are not yet \"ready\" for the main trunk, no one ever said that stuff in there is not meant to be tested. Testing and constructive feedback is just what these things need (apart from developer's time, of course)."
    author: "Frando"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-17
    body: "theDot is like an automated TODO list where the next or current steps that are on the list are relayed here in the complaints, er, comments every week for me. yay! and here i was keeping my own TODO list.. pffft! ;)"
    author: "Aaron Seigo"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-18
    body: "It's more efficient to let others do it for you anyway.  Distributed processing and such, it's the future."
    author: "Leo S"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-18
    body: "Yeah, your TODO list has been gridified!!"
    author: "scroogie"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-18
    body: "So in true swarm mentality, I suggest we help each other by typing one character each!\nI start with...:\nM"
    author: "martin"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-18
    body: "Mo"
    author: "Martin"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-18
    body: "Moa"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-18
    body: "Moab"
    author: "jospoortvliet"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-18
    body: "There is only one word in my vocabulary starting with \"Moab\", and that is \"Moabit\", a district of Berlin."
    author: "Stefan"
  - subject: "Re: where is the work in Plasma?"
    date: 2007-09-19
    body: "See? The mob is stupid! :)\n\nBut, the people have decided: Please welcome \"Moabit\" as one of the new pillars of KDE. What does it do?"
    author: "anonymous coward"
---
In <a href="http://commit-digest.org/issues/2007-09-16/">this week's KDE Commit-Digest</a>: Continued work in <a href="http://plasma.kde.org/">Plasma</a>, including a KMLDonkey data engine, a RSS data engine and news feed applet, and a Virtual Desktop switcher applet. More interface work for <a href="http://amarok.kde.org/">Amarok</a> 2.0, with progress on alternate music service integration. Support for webseeding in <a href="http://ktorrent.org/">KTorrent</a>. Support for network access of colour palettes in <a href="http://kolourpaint.sourceforge.net/">KolourPaint</a>. An <a href="http://pim.kde.org/akonadi/">Akonadi</a> resource for the del.icio.us bookmarking service. CMake support for PyKDE4 applications. Wider logging support in KSystemLog. SVG caching optimises usage, resulting in speed gains in many applications. KTeaTime rewritten for KDE 4, <a href="http://kplayer.sourceforge.net/">KPlayer</a> ported to KDE 4. New game based on "Deal or No Deal" arrives in playground/games. More code reorganisation in KDE SVN. KAider translation utility moves to kdereview.

<!--break-->
