---
title: "Amarok Weekly News Issue 5 Released"
date:    2007-01-18
authors:
  - "Ljubomir"
slug:    amarok-weekly-news-issue-5-released
comments:
  - subject: "filters"
    date: 2007-01-18
    body: "yay - filters are really cool!!"
    author: "nidi"
  - subject: "Re: filters"
    date: 2007-01-18
    body: "Hey, this is similar to something I suggested a while ago.\n\nI wonder if the devs say my pray, or if two geniuses has come up with the same idea. ;)\nWould imho be nice if the [...] is replaced with a nice icon. Maybe the filter icon (in e.g. Konqueror)."
    author: "Mogger"
  - subject: "Re: filters"
    date: 2007-01-18
    body: "I recall similar idea on the forums, and as soon as I saw the editor I though about that post. That said, I couldn't dig it up, so if you provide me a link to forum, I'll give you a credit. Anyways, filter editor is nice, but I'm thinking about some usability improvements.\n\nPS. Feature request are best when posted on bugs.kde.org. It's easy to track them down and maintain."
    author: "Ljubomir"
  - subject: "Re: filters"
    date: 2007-01-18
    body: "No credits needed, I just wondered if it was a coincide that this feature were implanted.\n\nWell, I'm happy with this feature, and I enjoyed reading the Amarok Weekly News. Thanks."
    author: "Mogger"
  - subject: "dcopquit"
    date: 2007-01-18
    body: "in the article it says:\n\n\"One of the harder ones to spot is a call for quitting application. To quit Amarok, call:\n\ndcop amarok MainApplication-Interface quit\"\n\nactually, it's not that hard to find ...if you know about the dcopquit command. you can do \"dcopquit <dcop appname>\" of just about any kde app and it'll quit for you. so \"dcopquit amarok\" is good enough.\n\nin kde4 the name of the command has been changed due to the dcop->dbus change. it's now the slightly more generically named \"kquitapp\"."
    author: "Aaron Seigo"
  - subject: "Re: dcopquit"
    date: 2007-01-18
    body: "One learns all the time. :)"
    author: "Ljubomir"
  - subject: "Re: dcopquit"
    date: 2007-01-18
    body: "Indeed!\n\nI consider myself an expert on KDE commandline tools, but I didn't know that one."
    author: "Kevin Krammer"
  - subject: "Re: dcopquit"
    date: 2007-01-19
    body: "indeed; all these weekly articles help keep me up to date with all kinds of things. really helps keep my reading load down. so a big thanks to you and the others authoring similar periodicals."
    author: "Aaron Seigo"
  - subject: "Re: dcopquit"
    date: 2007-01-20
    body: "I never use the CLI to quit amarok, only when it freeze I do so and generely the only thing that works in that case is good old 'pkill -9 amarokapp' and variants."
    author: "Patcito"
  - subject: "what is that lovely style?"
    date: 2007-01-18
    body: "What is that style featured in the screenshots? Perhaps qtcurve or polyester? It looks really nice."
    author: "Greg"
  - subject: "Re: what is that lovely style?"
    date: 2007-01-18
    body: "It's QtCurve 0.35"
    author: "Ljubomir"
  - subject: "DJing with Amarok"
    date: 2007-01-19
    body: "I think it would be pretty cool if future versions of Amarok would be able to use the fine grained output speed modifiers, which at least the xine engine seems to provide via XINE_PARAM_FINE_SPEED. Together with a nice and little plugin, one could adapt the output speed of Amarok to use it for DJing and mixing purposes.\n\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: DJing with Amarok"
    date: 2007-01-19
    body: "Have you already taken a look at Mixxx?  \nhttp://mixxx.sourceforge.net/\nhttp://www.kde-apps.org/content/show.php?content=17238\n\nIt has an interface to prokyon3 for music management.  Not to Amarok though ... \nhttp://prokyon3.sourceforge.net/\nhttp://www.kde-apps.org/content/show.php?content=10010\n\nIt's a plain-Qt combo.\n\n"
    author: "cm"
  - subject: "Re: DJing with Amarok"
    date: 2007-01-19
    body: "It won't happen, sorry. It's way beyond the scope of the application."
    author: "Mark Kretschmann"
  - subject: "Re: DJing with Amarok"
    date: 2007-01-19
    body: "Why? Supporting these fine grained speed control _actually present_ in libxine and others and adapting Amaroks API to it, so a simple plugin could take control of Amaroks output speed, isn't that mixing special. :)\n\nNote: I didn't request for mixing stuff, bassline editing or whatever, I would just need a possibility to control Amaroks output speed in a more or less stepless manner. Is that way beyond Amaroks scope?"
    author: "Erik"
  - subject: "Re: DJing with Amarok"
    date: 2007-01-19
    body: "Because Amarok isn't a DJ application. It's a music player, and we try hard keep its focus there.\n\nYou see, it may appear that Amarok has a lot of features, but we also reject many ideas that don't seem appropriate. And we're especially anal about adding new options. Any new option adds bloat to the interface, and it actually means that we have to constantly test and maintain it.\n\nBefore we add a new feature, we ask ourselves: \"Is this useful to the majority of our users, in a common use case?\" For example, we sometimes have been asked to add \"Guitar Tabs\" support to Amarok. How many of our users are guitar players? Probably less than 10%, I would estimate. Maybe a lot less. For everyone else this would just make GUI more complex, and it would make the program code more complex. The same is true for DJing capabilities.\n\nThere are programs that strive to be ultra flexible. I hear Foobar2000 is such a program; you can do virtually anything with it. Amarok has a different design philosophy: It's not intended to be extremely flexible, but rather work as well as possible for the use case that we define.\n\nHope this helped to explain our design philosophy a bit :)"
    author: "Mark Kretschmann"
  - subject: "Re: DJing with Amarok"
    date: 2007-01-19
    body: "That's exactly hitting the point of my last posting: I don't want a DJing tool or DJing capabilities, I would just need the possibility to adjust the output speed. Since I actually own a set of turntables and mixers there is no need for any further.\n\nI can neither believe that there is no other use for a somewhat stepless speed adjustment, nor I think it is not typical for a music player application to adjust the output speed of the currently playing song or to just give an interface to the speed adjustments. :)\n\nFollowing your explanations, one could say \"Oh, we don't need any volume adjustments, just turn the knob on your amplifier\". :)\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: DJing with Amarok"
    date: 2007-01-19
    body: "Its called the 90% rule (a feature probably shouldn't be implemented if its only interesting to 10%). Volume adjustment clearly qualifies under the 90% rule... making music go fast does not. \n\nI suppose that feature of xine libs would be handy if we wanted to implement a \"fast forward\" button. I can't imagine why we would though. :)"
    author: "Ian Monroe"
  - subject: "Re: DJing with Amarok"
    date: 2007-01-20
    body: "So you're telling me that wikipedia integration, k3b support, cover management and iTunes-support are in 90% of users' wishes? I totally agree that every feature of these is useful, but I don't see the point of this 90% rule and why these features fulfil it.\n\nIt's no problem to me if anyone thinks that fine regulation of output speed is not useful for her and many others and it's absolutely no problem to me, too, if there's noone begging me for implementing it, but an \"explanation\" like this mysterious 90% rule is quite disappointing, I'm sorry. :(\n\nlg\nErik\n"
    author: "Erik"
  - subject: "Re: DJing with Amarok"
    date: 2007-03-13
    body: "But why I need in amarok a volume button?\n\nEvery desktop brings a volume-button byself, but not a pitch-contol?\n\nThis will a very good feature for amarok!\nIt is the only additional I need.\n\nBut it is your choice...\n\nGregor"
    author: "grefabu"
  - subject: "Re: DJing with Amarok"
    date: 2007-06-25
    body: "I have to admit, a pitch control would be especially useful to me. I do love Amarok but I tend to DJ a lot, and whilst i don't use the laptop for actual performance its handy hearing how a song will sound pitched up on the deck when im planning my sets.\n\nSo many friends of mine do like their music that little bit faster too.. Practically anyone who's into any dance music scene will find this a nice feature."
    author: "Dan haworth"
  - subject: "Show cover on desktop"
    date: 2007-01-19
    body: "Will this ever be a default feature of Amarok? Or is it \"beyond the scope of the application\" as well?! Uhh...\n\nhttps://bugs.kde.org/show_bug.cgi?id=122223\nhttp://amarok.kde.org/forum/index.php/topic,13538.msg17039.html#msg17039\n\nLots of players have this as a default *option* - why do you refuse to add this?\n\nIt wouldn't even clutter the GUI..."
    author: "fish"
  - subject: "Re: Show cover on desktop"
    date: 2007-01-19
    body: "It should be handled by a script."
    author: "Mark Kretschmann"
  - subject: "Re: Show cover on desktop"
    date: 2007-01-19
    body: "Why?"
    author: "fish"
  - subject: "Re: Show cover on desktop"
    date: 2007-01-19
    body: "why not?\nit works fine with the available script, so why do you need this implemented as default in amarok?\nJust put the script on and it is default on your computer ;)\n"
    author: "AC"
  - subject: "Re: Show cover on desktop"
    date: 2007-01-19
    body: "which one of the 3 scripts do you use?\n\nI'm not happy with any of them..."
    author: "fish"
  - subject: "Re: Show cover on desktop"
    date: 2007-01-19
    body: "Report the bugs/wishs to their authors then. Help them improve."
    author: "Alexandre"
  - subject: "Streamtuner"
    date: 2007-01-21
    body: "Do you know whether there is a tool as Streamtuner for KDE/amarok to find appropriate radio streams?\nhttp://www.nongnu.org/streamtuner/\n\nIt's wonderful but a Gnome app.\n\nI feel that the Radio-Stream access of Amarok is not very convenient. In my Mandriva version of Amarok the French Radio ogg streams died from the server side. I thought it was a codec or library issue but the server was put down and no ogg streams are offered any more. "
    author: "Bert"
  - subject: "Re: Streamtuner"
    date: 2007-01-21
    body: "No longer needed. Amarok 1.4.5 has a built-in Shoutcast stream browser."
    author: "Mark Kretschmann"
---
The <a href="http://ljubomir.simin.googlepages.com/awnissue5">next issue of the weekly Amarok newsletter</a> is out. This issue covers the fine tuning of the scoring algorithm, new fadeout options, configurable playlist color, as well as new handy tool for editing filters. Traditionally, with tips included.


<!--break-->
