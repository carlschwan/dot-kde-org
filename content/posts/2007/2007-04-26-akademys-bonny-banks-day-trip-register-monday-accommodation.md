---
title: "aKademy's Bonny Banks Day Trip; Register By Monday for Accommodation"
date:    2007-04-26
authors:
  - "jriddell"
slug:    akademys-bonny-banks-day-trip-register-monday-accommodation
comments:
  - subject: "done as in?"
    date: 2007-04-26
    body: "\"Payment for accommodation needs to be done by Monday\"\nmeans\na) reached your account\nb) told the bank to do the transfer\n?"
    author: "a"
  - subject: "Re: done as in?"
    date: 2007-04-26
    body: "Paypal is preferred and that's generally instant.  For bank transfers make sure you send me an e-mail so I can mark it as being on the way.\n"
    author: "Jonathan Riddell"
  - subject: "Re: done as in?"
    date: 2007-04-26
    body: "paypal sucks beacuse i heard it's not a bank"
    author: "srettttt"
  - subject: "Re: done as in?"
    date: 2007-04-27
    body: "Paypal is an electronic money institution, regulated by the Financial Services Authority. They have to obey the same rules as any other bank in this country, and have to follow these FSA rules in all of Europe as well.\n\nPlease see https://www.paypal.com/uk/cgi-bin/webscr?cmd=p/gen/fsa-outside for more details."
    author: "Kyle Gordon"
  - subject: "Re: done as in?"
    date: 2007-04-27
    body: "I don't use paypal because it's evil"
    author: "Allan Sandfeld"
  - subject: "Re: done as in?"
    date: 2007-04-27
    body: "Not terribly explanatory.  When it comes to bank transfers, UK banks are also bad (either very slow or very expensive).  Using either for akademy is fine, just let me know if you're doing a bank transfer.\n"
    author: "Jonathan Riddell"
  - subject: "Beautiful!"
    date: 2007-04-26
    body: "That sounds really nice. I need to start contributing to KDE right away, so I can go see the Loch!\n\nWhat is going on in the middle of the top (very blue) picture? A boat? A splash? A small island? Someone in the water?"
    author: "E"
  - subject: "Re: Beautiful!"
    date: 2007-04-26
    body: "Since it's a Scottish Loch, I believe it's a monster. I hear they have one in each loch."
    author: "Thiago Macieira"
  - subject: "Re: Beautiful!"
    date: 2007-04-26
    body: "Actually I think it is even a requirement to call it a Loch.\n\nIf your loch's monster vanishes/dies, your title is revoke until you get a replacement.\n\nThe state of such titles was kept on \"lochcards\", later renamed to \"punch cards\" to obfusicate the origins, only the German translation \"Lochkarte\" still given hints about this, but is often mistaken to originate from the German work \"Loch\" (en: hole)\n"
    author: "Kevin Krammer"
  - subject: "Re: Beautiful!"
    date: 2007-04-27
    body: "+1 funny! :-)"
    author: "Andre"
  - subject: "Akademy->aKademy"
    date: 2007-04-26
    body: "Heh, every dot article that mentions Akademy later gets changed to aKademy. Shows up twice in the RSS feeds. Is there some internal capitalisation war raging?\n\nSpeaking of RSS, why do we only get the headline in the feed? It would be nice to read the article in the feed too."
    author: "Tim"
  - subject: "Re: Akademy->aKademy"
    date: 2007-04-27
    body: "Configure your Akregator (Edit News Source, Advanced)\u00a0to load the website when displaying the feed."
    author: "Thiago Macieira"
---
Thursday at aKademy 2007 will be our <a href="http://akademy2007.kde.org/codingmarathon/day-trip.php">Bonny Banks Day Trip</a>.  A chance to get out from infront of your computer, we will be taking the short journey to Loch Lomond. Use this as a chance to socialise and discuss KDE matters without the distraction of the internet.  Take your pick from walking along the shores or in the forest, taking the ferry out to expore the island or just sitting about having a coffee in the village, but be back in time for the barbeque on the beach. Remember to <a href="http://www.kde.org.uk/akademy/">register for aKademy</a>.  Payment for accommodation needs to be done by Monday if you want us to book for you.










<!--break-->
<p><a href="http://akademy2007.kde.org/images/loch-lomond.jpg"><img src="http://akademy2007.kde.org/images/loch-lomond-wee.jpg" width="500" height="370" alt="Photo of Loch Lomond" border="0"
 /></a></p>

<p><a href="http://akademy2007.kde.org/images/loch-lomond2.jpg"><img src="http://akademy2007.kde.org/images/loch-lomond2-wee.jpg" width="500" height="370" alt="Photo of Loch Lomond" border="
0" /></a></p>







