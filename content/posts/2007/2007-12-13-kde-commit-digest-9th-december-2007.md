---
title: "KDE Commit-Digest for 9th December 2007"
date:    2007-12-13
authors:
  - "dallen"
slug:    kde-commit-digest-9th-december-2007
comments:
  - subject: "Thanks!"
    date: 2007-12-13
    body: "Thanks, Danny!"
    author: "mactalla"
  - subject: "Re: Thanks!"
    date: 2007-12-13
    body: "+1"
    author: "Emil Sedgh"
  - subject: "Re: Thanks!"
    date: 2007-12-13
    body: "+1  \\o/"
    author: "shamaz"
  - subject: "Re: Thanks!"
    date: 2007-12-13
    body: "Fourthded"
    author: "Leo S"
  - subject: "Re: Thanks!"
    date: 2007-12-13
    body: "5th'd!\n\nMore interesting news: people have tried KDE4 RC 2 on the EEE, and it runs very nicely - incredible as the KDE team probably haven't even gotten to the main optimisation phase yet! Check it out:\n\nhttp://digg.com/linux_unix/KDE4_on_the_Asus_EeePC"
    author: "Anon"
  - subject: "Re: Thanks!"
    date: 2007-12-13
    body: "Amazing! I just ordered my Eee a few days ago and the shipping date in Germany will probably be January 10th. Well, I don't have to tell you the release schedule for KDE 4.0, so I guess it's just perfect timing."
    author: "WPosche"
  - subject: "Re: Thanks!"
    date: 2007-12-13
    body: "Very much so!"
    author: "peter"
  - subject: "Re: Thanks!"
    date: 2007-12-13
    body: "Yes, thank you very, very much. The commit digest is appreciated as much as the development, translation, usability, artist, promotion and all the other great stuff that is happening in the KDE atmosphere."
    author: "LB"
  - subject: "Great news about nepomuk"
    date: 2007-12-13
    body: "I'm glad to hear about the progress for nepomuk and I'm even glader to see that it seems to be in a runnable, usable state. This I hope can be a very usable \"thingie\"/feature in the kde4 desktop and I'm looking forward to try it out. Is this available in the RC2 or did I just miss it somehow?\n\nI want to thank all the developers for all the hard work, the semantic desktop is coming closer each day;-)"
    author: "Peppe Bergqvist"
  - subject: "Re: Great news about nepomuk"
    date: 2007-12-13
    body: "Disclaimer: Question not criticism!\n\nI agree it all sounds very interesting. I was just wondering whether desktop search using strigi is already available in kde4. I've been playing with betas and RC's and I haven't been able to find a way to use it. I've also googled around but couldn't find anything. Is there a strigi applet in kde4 or something similar?"
    author: "Federico Gherardini"
  - subject: "Re: Great news about nepomuk"
    date: 2007-12-13
    body: "\"Disclaimer: Question not criticism!\"\n\nRelax: Devs can tell honest questions from unconstructive criticisms :)\n\n\"I agree it all sounds very interesting. I was just wondering whether desktop search using strigi is already available in kde4. I've been playing with betas and RC's and I haven't been able to find a way to use it. I've also googled around but couldn't find anything. Is there a strigi applet in kde4 or something similar?\"\n\nEnsure that strigidaemon is running.  There is a very primitive GUI called strigiclient which can be used to interact with the daemon and perform searches."
    author: "Anon"
  - subject: "Re: Great news about nepomuk"
    date: 2007-12-14
    body: "Indeed!\n\nThis is one of the features I like most as it maybe will help me to keep my files a little bit more organized.\nOk, not really, but it will help me to find my unorganized files. :D"
    author: "mat69"
  - subject: "integration of directshow backend in amarok?"
    date: 2007-12-13
    body: "I'm afraid this is going to be another post that sounds like criticism, when it's mostly just curiosity.\n\nFrom the post, it sounds like work is being done on amarok to make it use the phonon directshow backend?  Surely the whole point is that phonon is used in an app once, and then backends can be added without changing the apps?  Has there been an API change or something?\n\nOn another note... the talk of phonon implementing subtitle streams and multiple audio streams is making me think more and more of gstreamer.  Don't get me wrong: those are necessary features, but doesn't it seem to be leading to the same inevitable reproduction of features to anyone else?\n\nAgain, no criticism intended; just asking questions because I'm sure someone has good answers."
    author: "Lee"
  - subject: "Re: integration of directshow backend in amarok?"
    date: 2007-12-13
    body: "Well, Phonon needs applications having use-cases to determine a sensible API and get everything to work flawlessly. I guess one of the developers of Amarok is trying to build it with Phonon on Windows and is coworking with the Phonon maintainer to make this work.\nAbout the subtitle and multiple audio streams, I guess Phonon implements these only if all the backends support or otherwise implements some kind of capability detection. Phonon still adapts, so there is no reproduction of features. \nIm just guessing though, im neither connected to phonon nor to amarok development. "
    author: "scroogie"
  - subject: "Re: integration of directshow backend in amarok?"
    date: 2007-12-13
    body: "Well, one key difference is that phonon would be able to support a bunch of backends. A gstreamer backend for phonon should eventually be available.\n\nAs for Amarok, my guess is that some Amarok developers are helping to create the directshow backend."
    author: "Soap"
  - subject: "Re: integration of directshow backend in amarok?"
    date: 2007-12-13
    body: "> post that sounds like criticism\n\nnah .. at least to me it sounded like curiosity and honest questions.\n\n> work is being done on amarok to make it use the phonon directshow backend\n\nno; rather a simple phonon backend was developed to allow amarok to work at its most basic level on windows. it stayed in amarok because it's a temporary solution, as the amarok people didn't want to wait on a full featured DirectShow backend emerging, which is understandable.\n\n> the talk of phonon implementing subtitle streams and multiple audio streams\n> is making me think more and more of gstreamer.\n\nwhat phonon lacks is the high level abstraction APIs. these will be coming in future releases and will use the underlying capabilities of the given media frameworks as much as possible.  that's why right now Video Player needs to access the xine backend directly: there is no high level API for it.\n\nso it's not that phonon is going to implement ripping subtitle streams from dvd's directly, but it will provide an API for it that the phonon backends will then implement the support for; xine/gstreamer/quicktime/directshow/whatever will still be doing the heavy lifting there wherever possible. so in future Video Player (and others) should be able to use this high level API and not worry about the backend.\n\nhth."
    author: "Aaron J. Seigo"
  - subject: "Re: integration of directshow backend in amarok?"
    date: 2007-12-13
    body: "Just to add to what Aaron was saying, the APIs for subtitle access is already there and commented it out. It only adds a couple methods, its not a big deal. Which is partly why I feel OK about adding the Xine dependency back to Video Player, since it is a very slight dependency and will be easy to switch to 100% phonon with KDE 4.1."
    author: "Ian Monroe"
  - subject: "Re: integration of directshow backend in amarok?"
    date: 2007-12-14
    body: "This Commit Digest is apparently already out-of-date. Trolltech has released gstreamer and DirectShow Phonon backends already! http://dot.kde.org/1197535003"
    author: "kwilliam"
  - subject: "Re: integration of directshow backend in amarok?"
    date: 2007-12-19
    body: ">On another note... the talk of phonon implementing subtitle streams and \n>multiple audio streams is making me think more and more of gstreamer. Don't \n>get me wrong: those are necessary features, but doesn't it seem to be leading \n>to the same inevitable reproduction of features to anyone else?\n\nWell, phonon is realy an duplicated layer, but is there for an good reason. Gstreamer people could not guarantee API and binary compatibility (this will come only after the 1.0 release, probably) for the entire KDE4 life-cicle (planed for 5 years), so an abstraction layer is needed to protect KDE aplications from those changes.\n\nThere is also the possible problem of mantainability. If, for some reason, Gstreamer development is stoped in comming years, it would be impossible to maitain +30k lines of alien C code, and the story of Arts will repeat. With Phonon, we can automaticaly migrate to the next greater mm backend, and not remain stuck with an unmanteined GStreamer."
    author: "Manabu"
  - subject: "Thanks"
    date: 2007-12-13
    body: "Thanks for the commit-digest! Theres a lot of interesting stuff this time! Just two little things: in the second paragraph you confused Robert Knight and Eike Beer (headline of kgpg stuff) and the link to kgpg40rc1.png doesnt work.\n\nAbout Nepomuk: awesome project! i hope you get all performance problems under control. Will there be a possibility of defining own vocabularies? or connecting files with arbitrary predicates? There are so many possibilities with RDF!"
    author: "scroogie"
  - subject: "Re: Thanks"
    date: 2007-12-13
    body: "Fixed, thanks (that's what I get for moving stuff around ;))!\n\nDanny"
    author: "Danny Allen"
  - subject: "I want my bragging rights!"
    date: 2007-12-13
    body: "Hi,\n\nI had closed on the order of 40 bugs the week ending Sunday, and b.k.o showed me as top bug-killer (still defeating my position). However, I'm not even listed in the bug-killer ranking in the commit digest. I want my bragging rights! (I already sent you a mail about this, but I just can't wait...)\n\nThanks for the otherwise great digest.\n\nRegards\nThomas"
    author: "tfry"
  - subject: "Re: I want my bragging rights!"
    date: 2007-12-13
    body: "If you're thomas friedrichsmeier, your stats now show 72 -- way cool!"
    author: "Boudewijn Rempt"
  - subject: "Re: I want my bragging rights!"
    date: 2007-12-13
    body: "Yes, that's me. I'm afraid the number will soon start to decline, however, as I'm running out of easy-to-close reports in kate..."
    author: "tfry"
  - subject: "Re: I want my bragging rights!"
    date: 2007-12-14
    body: "Hey Thomas,\nSorry! There was an issue with the script which extracts the statistics from the bug page where it only looked for email addresses up to 40 characters in length!\n\nFixed now (and for future achievements ;))!\n\nThanks,\nDanny\n"
    author: "Danny Allen"
  - subject: "Re: I want my bragging rights!"
    date: 2007-12-14
    body: "Thanks!\n\nThomas"
    author: "tfry"
  - subject: "sopranocmd"
    date: 2007-12-13
    body: "I just realised that soprano (or at least sopranocmd) is in the kubuntu KDE4 rc4 packages I installed yesterday! That's GREAT; I'm going to have to play with that real soon :D\n\nOne thing though... it dumps its help to stderr, rather than stdout, which is strange and evil ;)"
    author: "Lee"
  - subject: "Re: sopranocmd"
    date: 2007-12-13
    body: "Hm, you are right. I should write the help to stdout. Well, let me fix that..."
    author: "Sebastian Trueg"
  - subject: "VideoPlayer"
    date: 2007-12-13
    body: "Thanks for the Digest, Danny.\nI'm 100% in favor of VideoPlayer integration in kdemultimedia. It's the perfect fast-loading media player you use to check a video or a music by double-clicking on the file or when downloading it from the web.\nIt is the perfect complement to apps like Amarok, which I use to organize my collections of music but which is not exactly suitable for fast view of media file.\nA similar app to Amarok but for videos (allowing to organize collections and view them) would be great. I currently somewhat use kaffeine for that, but it's playlists should be improved. In fact, reusing somewhat the way Amarok works, but for videos, would be great.\nAnyway, thank you all for your time and efforts, I'm really looking forward to KDE4.\n\nBTW, one small question : is there any \"migration\" tool planed? Something allowing to recover many KDE3 settings when moving to KDE4, like passwords in kwallet, for instance. Or maybe it won't be needed?"
    author: "Richard Van Den Boom"
  - subject: "Re: VideoPlayer"
    date: 2007-12-13
    body: "Indeed great news about the menus.\nSometimes it's good to have multiple choices and let them eat each others ideas so a better one will prevail in people's hearts for the better of them all :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: VideoPlayer"
    date: 2007-12-13
    body: "Glad you like Video Player. :)\n\nNot sure about it being the default for music files, its not really optimized for that. At aKademy this year, we were thinking about having adding a 'quick start' mode to Amarok where it would load a basic window and start playing the file immediately before loading the database and such. This way we can support the use case of a user quickly wanting to listen to a file, as well as the user who already has Amarok open or who wants to use its various features when launching a file from Dolphin or Konqueror.\n\nFor Amarok 2.0 we're working really hard just to get feature parity with Amarok 1.4, but I expect this feature will make an appearance somewhere in Amarok 2.x, where x is > 0. "
    author: "Ian Monroe"
  - subject: "Re: VideoPlayer"
    date: 2007-12-13
    body: "Video Player (well ok, I've only tried codeine) absolutely rocks.  Best damn video player out there.  The only time I ever use VLC is when I need to use the slow motion buttons, or if xine can't handle a particularly tricky wmv file.  \n\nStarts fast, super simple to operate, minimal UI, and the position saving rocks!"
    author: "Leo S"
  - subject: "Re: VideoPlayer"
    date: 2007-12-13
    body: "\"A similar app to Amarok but for videos (allowing to organize collections and view them) would be great. I currently somewhat use kaffeine for that, but it's playlists should be improved. In fact, reusing somewhat the way Amarok works, but for videos, would be great.\"\n\nI'd love something like that (like Amarok or KPhotoalbum) : you can tag your videos with custom tags, and view not only simple thumbnails but small \"movie strip\" (a few frames to have an overview of the entire video file)\n\nMaybe something wich use the strigi data (video size...) too and you can search something like : \"I want all my family video with a size superior to 100 mo and created before xx/xx/xxxx\"... \n\n"
    author: "DanaKil"
  - subject: "Kmenu and Kclock"
    date: 2007-12-13
    body: "First of all thanks to you Danny for putting so much energy and time in the Commit-Digest, to give us a view of what is going on behind the scene.\nWell at last I hope that we will get an end to the everlasting Kmenu discussions seeing that the KDE team has been listening to it's users. Big thanks and compliment to you guys!\n\nConcerning the Kclock, I have just one question: There is the possibility to configure  the present clock in 3.5x so that one can see the time in different countries when one moves the mouse over the clock, will this feature be in the upcoming version of KDE? I would rather have that than the train station clock that so many people are crazy for :)\nOnce again thanks and keep up the good work!"
    author: "Bobby"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "You could add these train-style-clocks to you panel and desktop as many as you want and change the configurations and timezone of each of them.\n"
    author: "Emil Sedgh"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "You are right but I think that the present KDE 3.5x style is more clean, clearly arranged and more practical. Instead of having a dozen clocks for a dozen countries I have one clock and with a mouse hover it gives me a popup menu which shows me the time in a dozen countries. I love this because I sometimes call foreign countries and instead of searching for their local time on the net all I need to do is move my mouse over the clock (of course I have to configure the time zones before)."
    author: "Bobby"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "+1"
    author: "anonymous"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-14
    body: "do you know who implemented the features you talk about, such as the popup showing all the timezones at once? me. know why? because i damn well needed it ;) so don't worry, it'll all be there eventually because i still need all that stuff =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-14
    body: "Thanks very much Aaron, that put a big smile on my face :)"
    author: "Bobby"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "Ok, these kind of questions keep on coming up, so let's try for a generic answer - hopefully I'm not mis-representing Plasma and Aaron's plans for it, here.\n\n\"will this [panel] feature be in the upcoming version of KDE?\"\n\nIn KDE3, kicker's design made it rather difficult and tedious for a programmer to implement their own taskbar, systray, menu etc, so it rarely happened: thus, most panel features came from the small team of kicker developers.  If you wanted a new panel feature, you had to ask them and, if they didn't have time for it or flat-out vetoed it (as has happened with at least one request: many users hate that mouse-wheeling over the taskbar switches the currently focused app as it is easy to accidentally trigger it on a laptop touchpad, but requests for an option to configure this away have been denied), you were largely out of luck.  Thus, pre-Plasma, this kind of petitioning the core devs with a \"will you implement this?\" makes sense.\n\nPlasma, however, aims to make both the creation *and* the deployment and installation of new panel functionality (be they more exotic Plasmoids such as the Fifteen Piece Puzzle plasmoid, or generic ones such as a new, uber-configurable taskbar) much, much easier, so we no longer have to rely on the Plasma team to approve and grant requests.  Think of it in similar lines to Firefox: people no longer have to petition the Firefox devs to add, say Undo Close Tab support to Firefox, or mouse-gestures, or a feature that makes all Dot comments flourescent pink: the framework allows such extensions to be easily created and installed, and this is precisely what has happened: all manner of handy and interesting extensions have been created, and the users and devs are much happier: users no longer have to depend on the whims of a core group of developers to implement their wishes, and the devs are free to worry about other things than balancing a mountain of conflicting user requests.\n\nIn short, Firefox's extension system has Democratised the Web, in that users can easily customise their web experience to their heart's content without having to depend on a small, harassed and potentially stubborn team of developers.  Similarly, Plasma should Democratise the Desktop.  I don't want to over-emphasise the similarity between Plasma and Firefox, here, as it smacks of false advertising, but to me personally, it seems that some of the parallels are very striking indeed.\n\nSo, in a post-Plasma world, you *could* ask if Plasma's clock with have feature $X, but there's really no need anymore; if there is a popular feature but they don't want to add it, then it's very probable that someone else *will*, and you'll be able to effortlessly install it regardless of technical skill via GHNS.  Accusations of GNOME-ifying KDE (which for the record I think are groundless anyway for reasons that have been repeated ad-nauseum: Plasma is a from-scratch re-write of the desktop, and the devs simply haven't had time to re-implement the old features yet) become meaningless: Firefox presents a very simple set of features and configuration options by default, yet it's actual configurability as a web browser is, IMO, completely unsurpassed: if the default functionality and configurability does not meet your requirements, simply take the few seconds required to add more!\n\nHopefully this will put some fears to rest.  Oh, and this shouldn't be taken as a statement that the Plasma team will be deliberately and lazily stripping out features just because more functional replacements can be added so easily: given time, I'm sure that the default Plasma interface will be just as rich as the KDE3 one :)"
    author: "Anon"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "+1 insightfull"
    author: "Andre"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "Thanks very much for your simple, transparent and understandable explanation Anon. I think that I now have a clearer picture of plasma. It sounds like a very good, innovative concept. I was a bit curious for the simple reason that these implementations and configuration possibilities are still absent in the present release but I will exercise patience until the pieces of the puzzle are put together ;)"
    author: "Bobby"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "Thanks for your understanding :)"
    author: "Anon"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-14
    body: "things are absent because it's very hard to get all the pieces together to make what we are working towards possible. it took kicker, kdesktop and minicli 7 years to get where they are. we've had ~18 months to get the replacements in the form of plasma and krunner to where they are, and we're doing a number of rather more advanced things.\n\nalso, while kicker/kdesktop just lived with the warts and what not of Qt3, with plasma/krunner i've made sure that when things sucked we've dived into Qt4 and fixed things (many thanks to all the understanding TT devs out there =)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "\"will this [panel] feature be in the upcoming version of KDE?\" \n\n\"In KDE3, kicker's design made it rather difficult and tedious for a programmer to implement their own taskbar, systray, menu etc, so it rarely happened: thus, most panel features came from the small team of kicker developers. If you wanted a new panel feature, you had to ask them and, if they didn't have time for it or flat-out vetoed it (as has happened with at least one request: many users hate that mouse-wheeling over the taskbar switches the currently focused app as it is easy to accidentally trigger it on a laptop touchpad, but requests for an option to configure this away have been denied), you were largely out of luck. Thus, pre-Plasma, this kind of petitioning the core devs with a \"will you implement this?\" makes sense.\n \n Plasma, however, aims to make both the creation *and* the deployment and installation of new panel functionality (be they more exotic Plasmoids such as the Fifteen Piece Puzzle plasmoid, or generic ones such as a new, uber-configurable taskbar) much, much easier, so we no longer have to rely on the Plasma team to approve and grant requests. Think of it in similar lines to Firefox: people no longer have to petition the Firefox devs to add, say Undo Close Tab support to Firefox, or mouse-gestures, or a feature that makes all Dot comments flourescent pink: the framework allows such extensions to be easily created and installed, and this is precisely what has happened: all manner of handy and interesting extensions have been created, and the users and devs are much happier: users no longer have to depend on the whims of a core group of developers to implement their wishes, and the devs are free to worry about other things than balancing a mountain of conflicting user requests.\n \n In short, Firefox's extension system has Democratised the Web, in that users can easily customise their web experience to their heart's content without having to depend on a small, harassed and potentially stubborn team of developers. Similarly, Plasma should Democratise the Desktop. I don't want to over-emphasise the similarity between Plasma and Firefox, here, as it smacks of false advertising, but to me personally, it seems that some of the parallels are very striking indeed.\n \n So, in a post-Plasma world, you *could* ask if Plasma's clock with have feature $X, but there's really no need anymore; if there is a popular feature but they don't want to add it, then it's very probable that someone else *will*, and you'll be able to effortlessly install it regardless of technical skill via GHNS. Accusations of GNOME-ifying KDE (which for the record I think are groundless anyway for reasons that have been repeated ad-nauseum: Plasma is a from-scratch re-write of the desktop, and the devs simply haven't had time to re-implement the old features yet) become meaningless: Firefox presents a very simple set of features and configuration options by default, yet it's actual configurability as a web browser is, IMO, completely unsurpassed: if the default functionality and configurability does not meet your requirements, simply take the few seconds required to add more!\n \n Hopefully this will put some fears to rest. Oh, and this shouldn't be taken as a statement that the Plasma team will be deliberately and lazily stripping out features just because more functional replacements can be added so easily: given time, I'm sure that the default Plasma interface will be just as rich as the KDE3 one :)\"\n\n--------------------------\n\nWOW, that's allot of delicate, but informatively intended words, even with concepts such as Democracy, GNOME<>KDE and easing fears added to them.\n\n\"will this [panel] feature be in the upcoming version of KDE?\"\n\nNo, not likely, also looking at how delicate the responses are. Very likely, when there is a new bug reported about this, it will be supported again.\n\nThis is mostly a question of available time and priorities at the moment I think. This rather effective bug reporting process is how it used to work and how it continues to work so far. Now with the concepts and new realities of plasma, I bet your mother we can expect so many more cool things on the desktop, even when it comes to 'mere' clocks. Still, the concept of report bug -> arrive at feature, still very much is the same. The only thing that is changing is how larger and larger groups of people with different interests communicate on the Dot itself."
    author: "ac"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-13
    body: "Thanks a lot, Anon, for your insightful explanations. I'm really looking forward to cool things to come. Looking at what cool stuff the user community has already been creating for KDE 3.x (see www.kde-look.org), and combine this potential with the possibilities of Plasma, the future of KDE looks more than bright. \nOn the other hand, I don't think I'll recommend any average user to use KDE 4.0 as soon as it's release. It will be a great playground for enthusiasts and contributors, but most probably won't be anywhere near to ready for, say, my grandma. I think you should consider this in your marketing strategy, or else many people might get disappointed due to false expectations."
    author: "chris.w"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-14
    body: "Thanks for stating the obvious. It's a bit light on the buzzword side, but still rather helpful. ;-)"
    author: "sebas"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-14
    body: "This reply is truly interesting. I recently proposed a Plasma FAQ and I was wondering if I could use these bits of information in the document (also properly citing you as well)."
    author: "Luca Beltrame"
  - subject: "Re: Kmenu and Kclock"
    date: 2007-12-14
    body: "Please clear it with Aaron, first - I don't want to make promises he can't keep :) Feel free to use any quotations you want from the post.  I generally prefer to remain anonymous, so you'll have to take my word for it that I'm the Anon you are responding to ;)"
    author: "Anon"
  - subject: "nice commit and thnkz"
    date: 2007-12-13
    body: "\"As every detail of KDE 4.0 seems to need a water-tight disclaimer these days, i'll just state that the green colour scheme shown in the screenshots is not the default of KDE 4.0!\"\n\nthis is true, cut this message and  past every site where a screen of kde4 appears.\n\nand the integration of soprano is really nice, and thins like this http://www.kde-look.org/content/show.php/smart+file+browse?content=71417 could be possible ???"
    author: "Rudolhp"
  - subject: "What about Kasbar ?"
    date: 2007-12-13
    body: "I wonder if anyone has planned to do a plasmoid for the kasbar of kde3 ?\nI think you will be my hero !\n\nBy the way, I do not know how easy it could be to do that. Is there a data engine for providing those informations ?\nDo you think that I (know only some bits of qt and nothing about kde) could implement a plasmoid when I will have time ?\nWhat do you think about the skills needed ?\n\nThanks in advance and very good job for all kde devs\n"
    author: "phD student that should stop reading the dot"
  - subject: "Re: What about Kasbar ?"
    date: 2007-12-13
    body: "\"Do you think that I (know only some bits of qt and nothing about kde) could implement a plasmoid when I will have time ?\"\n\nSome of the current Plasmoid devs had to prior experience even with Qt, so that puts you ahead of the game :) I say - go for it!"
    author: "Anon"
  - subject: "Re: What about Kasbar ?"
    date: 2007-12-13
    body: "There is indeed a data engine that lists the tasks that should be showing up on a taskbar already, which could be re-used in a kasbar type taskbar. I'm not sure about some of the other things, like showing the thumbnails of the windows, which might require the creation of a thumbnail engine that queries kwin (or other window manager) for this information...\n\nCheers"
    author: "Troy Unrau"
  - subject: "Simple menu"
    date: 2007-12-13
    body: "How can I add the simple menu to the taskbar? Adding it to the desktop and moving it on the taskbar does not seem to work for me (svn from today)."
    author: "Michael"
  - subject: "Re: Simple menu"
    date: 2007-12-14
    body: "right now you have to drag it directly from the add widget dialog to the panel. I have no idea if we will be able to drag plasmaoids from the desktop to the panel later. (i hope so, would be logic)"
    author: "Beat Wolf"
  - subject: "Re: Simple menu"
    date: 2007-12-14
    body: "and functional. imagine adding an rss reader configured for 20 sites to your task bar, then adding it to the desktop. copying and pasting 20 URLs is a pain. but what is more important is this: you can't let an applet crash the desktop. it's happened to me in some of the recent builds. one of the applets was fixed almost immediately after I reported it, but that's not the point. the point is, if an applet crashes, it should crash alone. that's what Linux prides itself on, right? modularization, or something similar."
    author: "yman"
  - subject: "Re: Simple menu"
    date: 2007-12-14
    body: "No you can't add it to the taskbar (at least the default one), but dragging from desktop to a _panel_ will be possible in the future. ;)\n\nBoth of your questions are answered here: http://aseigo.blogspot.com/2007/12/birthing-plasmoids.html"
    author: "Hans"
  - subject: "Re: Simple menu"
    date: 2007-12-14
    body: "I already read that blog post. and I wasn't talking about the default RSS reader, but a hypothetical one."
    author: "yman"
  - subject: "Re: Simple menu"
    date: 2007-12-14
    body: "I was referring to your statement \"if an applet crashes, it should crash alone\", not the RSS widget.On the page I linked to (in the comments, sorry for not being clear about that) you can read:\n\n\"No, Plasmoids are all executed in process, so we will always have the situation where a buggy (native code) Plasmoid brings down the entire desktop. Plasmoids written in Javascript/ Python/ Ruby/ whatever are immune to this, though, and also have the advantage that they are significantly easier to download and install.\"\n-- Anonymous\n\nAnd\n\n\"the anonymous answer is correct; and the reason for things being in process is that out-of-process means a LOT more overhead (a LOT) and makes visual effects and harmony pretty much impossible. it also isn't a cure all for stability problems, as the systray can attest to.\"\n-- Aaron J. Seigo\n\nAs for the question in the first post, the answer is also in the comments:\n\n\"> Will plasmoids eventually be draggable\n> to/from the panels and desktop?\n\nthat's the idea.\"\n-- Aaron J. Seigo\n\nOnce again, I apologize for not being clear in the first comment. :)"
    author: "Hans"
  - subject: "Concerning KWin"
    date: 2007-12-13
    body: "This may be quite offtopic, but: Does anyone know whether KPowersave is already being ported to KDE 4 (including, most notably, Solid)? I could imagine that KPowersave could automatically turn off desktop effects when in battery or powersaving mode, that would be very cool."
    author: "Stefan"
  - subject: "Re: Concerning KWin"
    date: 2007-12-14
    body: "And preventing strigi to start a full indexing of harddrive, and replace the plasma animator with a simple one, and stoping wifi cards when network is down for more than one minute.\n\nYay, but I think we will have to wait."
    author: "kollum"
  - subject: "Re: Concerning KWin"
    date: 2007-12-14
    body: "http://websvn.kde.org/trunk/KDE/kdelibs/solid/solid/powermanagement.h?view=markup\n\n       SOLID_EXPORT bool appShouldConserveResources();\n\nThis is already present, but apps need to make use of it."
    author: "Bille"
  - subject: "Panel idea"
    date: 2007-12-15
    body: "Thanks Danny for another riveting Commit Digest. I especially like the written features at the beginning of each digest where developers talk about and show how their work is progressing.\n\nI have a suggestion for the panel. It looks beautiful and sleek at the moment, but the icons on the bottom bar are hard to distinguish in all the blackness. My idea is to have the tabs in inverted colors (image attached). What are your opinions on this? This ain't a biggie, but it's just a little thing that I suddenly though of.\n\nThe KDE 4 series looks more exciting and promising by the commit, great work guys!"
    author: "Darryl Wheatley"
  - subject: "Re: Panel idea"
    date: 2007-12-15
    body: "Grr the attachment didn't work! Here it is (hopefully)"
    author: "Darryl Wheatley"
  - subject: "Re: Panel idea"
    date: 2007-12-15
    body: "I hope not, that's real ugly :)"
    author: "Anon"
  - subject: "Re: Panel idea"
    date: 2007-12-16
    body: "Well fair enough. It doesn't fit the overall theme very well, but I think that the current settings make the different windows on the panel a bit indistinguishable. So I've tried to provide a constructive suggestion. Still, this is such a minor niggle and I have great faith in the KDE team that I know there's nothing to worry about."
    author: "Darryl Wheatley"
---
In <a href="http://commit-digest.org/issues/2007-12-09/">this week's KDE Commit-Digest</a>: The "simple menu" (similar to the menu found in the KDE 3 series) becomes usable. The clock receives a popup-based calendar widget, with KRunner becoming multi-threaded in <a href="http://plasma.kde.org/">Plasma</a>. Work continues the long-awaited update of KBugBuster, with important development milestones reached. Version Control and other general work in <a href="http://www.kdevelop.org/">KDevelop</a>. Start of a DirectShow (for Windows) backend for <a href="http://phonon.kde.org/">Phonon</a>, and the integration of this backend in <a href="http://amarok.kde.org/">Amarok</a> 2.0. Continued work on the BitTorrent plugin for <a href="http://kget.sourceforge.net/">KGet</a>. <a href="http://kblogger.pwsp.net/">KBlogger</a> gets KWallet integration. The beginnings of a simple vector text shape with support for text-on-path and exact positioning, and the start of another painting framework in <a href="http://www.koffice.org/">KOffice</a>. A bitmap (BMP) export filter for <a href="http://www.koffice.org/krita/">Krita</a>. Support for SVG animations in KDM. Important work on the KNewStuff2 framework, through the work of a new maintainer. Adjustments in colour schemes intended for KDE 4.0, more work on adapting icons to the FreeDesktop.org icon naming standard. <a href="http://purinchu.net/abakus/">Abakus</a>, a calculator application, begins to be ported to KDE 4. KDE 4.0 Release Candidate 2 is tagged for release. <a href="http://commit-digest.org/issues/2007-12-09/">Read the rest of the Digest here</a>.

<!--break-->
