---
title: "Two Weeks with Kubuntu Gutsy Gibbon (7.10)"
date:    2007-11-06
authors:
  - "mrogers"
slug:    two-weeks-kubuntu-gutsy-gibbon-710
comments:
  - subject: "Good post"
    date: 2007-11-06
    body: "Canllaith's post is interesting and well-written, and unfortunately I think it highlights the fact that Kubuntu is not getting the love it deserves. To be fair, I think Jon Riddell and the many volunteers who help out do a fantastic job, but there just isn't enough manpower to keep Kubuntu ahead of the pack. Take these examples:\n*Adept is still behind Synaptic, both in features and user interface. It would be good to have a Qt version of Synaptic and Ubuntu's Add/Remove Programs, because they are easier and more intuitive, as well as having more features. Better still, it would be great to see one good KDE tool for package management used across all KDE distros;\n*The default theme is a beautiful blue that, although more aesthetically pleasing than previous purple defaults, hurts the eyes after a period of time (I have no idea why, it's just too intense?);\n*Strigi's interface needs refinement. When you have the search window open, the background is a very similar eye-popping blue as the wallpaper (which can be changed I guess), meaning it bleeds into the desktop (http://kubuntu.org/images/gutsy_strigi.png);\n*Features like Bulletproof X are not available because of lack of a KDM failsafe server\n\nOf course, it's easy to moan and hard to help, but I hope these comments will encourage Canonical and the community to seriously pursue KubuntuHardyCatchup (https://wiki.kubuntu.org/KubuntuHardyCatchup). Ideally they should make neutral backends for their new features and simply create GNOME and KDE frontends at the same time (I hear this is happening with Kleansweep, which will one day be both Buntus' system cleanout tool). This will prevent the endless cycle of moving targets and disappointed KDE fans, as Kubuntu valiantly tries to catch the receding wave that its GNOME cousin is already surfing.\n"
    author: "Darryl Wheatley"
  - subject: "Re: Good post"
    date: 2007-11-06
    body: "There is also a big problem with Kubuntu: it's not fully internationalized by\ndefault. Not only the Live CD doesn't embed the localisations that can be\nselected at boot time, but also the installation process doesn't even download\nthe necessary files to have Kubuntu in your own language. And THIS is a big\nshow stopped."
    author: "Thomas"
  - subject: "Re: Good post"
    date: 2007-11-06
    body: "Why a showstopper???? If you are installing on \na machine without internet, you're already supposed\nto use the DVD version (which has internationalization),\nas with the CD you would lose too much anyway....."
    author: "mark"
  - subject: "Re: Good post"
    date: 2007-11-06
    body: "I personally like Adept over Synaptic (although it is far from perfect), but I especially like the apt:/ kio-slave. The interface is simple, clean, and easy to use. Adept should learn from apt:/"
    author: "Severed Head"
  - subject: "Re: Good post"
    date: 2007-11-06
    body: "I mostly use suse with kde. love the (guru's version) smart package manager and gui. Smart's command line is similar to apt-get and the gui is similar to synaptic. Only both have more options. I've run k-x-ubuntu and several other distros enough to be fairly familiar with their package managers. I just recently noticed that on the \"about\" dialog of the smart gui that smart has been a canonical funded project since 2005. I also remember about a year ago Shuttleworth saying in an interview ubuntu may switch to smart. What's the hold-up?"
    author: "Stu"
  - subject: "Re: Good post"
    date: 2007-11-07
    body: "* The default theme is a beautiful blue that, although more aesthetically pleasing than previous purple defaults, hurts the eyes after a period of time (I have no idea why, it's just too intense?);\n\nI agree, the color theme is way too bright, unless you've got full ligthing on the room you're working in, the eyes hurt after a while. Specially with things like the file manager being maximised etc."
    author: "NabLa"
  - subject: "Wrong Site?"
    date: 2007-11-06
    body: "Is this now distroreview.com?"
    author: "Anonymous"
  - subject: "Re: Wrong Site?"
    date: 2007-11-06
    body: "Nah, but I'd say an article reviewing the integration of KDE into a distro fits well here."
    author: "Jonathan"
  - subject: "Re: Wrong Site?"
    date: 2007-11-06
    body: "No. It's a site about all news KDE related (including distro's) where everybody get's to burb his/her opinion about the article and the preceding burbs. Including you and me, of course."
    author: "Andr\u00e9"
  - subject: "LEAP Support"
    date: 2007-11-06
    body: "I work in IT, and I read every tech blog I can in a day, and I've never head of this LEAP wireless protocol.  She says she is disappointed that KDE hasn't caught up with Gnome, because they support a rare, insecure wireless protocol?\n\nLet's not go tit-for-tat comparing KDE and Gnome features and then suggest one needs to catch up to the other."
    author: "T. J. Brumfield"
  - subject: "Re: LEAP Support"
    date: 2007-11-07
    body: "> She says she is disappointed that KDE hasn't caught up with Gnome, because they support a rare, insecure wireless protocol?\n\n\nNo, she says she is disappointed that the KDE frontend does not yet support the full range of capabilities of the backend, while the GNOME frontend does.\n\nHowever, actually having the KDE frontend is already a bonus for some other distributions, e.g. Fedora decided that their KDE users should not have that convenience any more but rather have to add their network credentials again into a temporary frontend and, if the have any new networks in the mean time, again when the KDE frontend is eventually restored in an update later on.\n\nUsers of reasonable distribution can be assured that the developer working on the KDE frontend is working hard to get up-to-date with the backend's changes, who's authors should probably think about either keeping compatability and providing additional extensions or making sure at least their main frontend developers had a chance to update their implementations accordingly.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: LEAP Support"
    date: 2007-11-07
    body: "Tee hee! We wouldn't want to complicate things for anyone by providing a feature that they in their unilluminated mind need. Like everyone can upgrade their infrastructure to modern standards and everything would be so simple.\n\nI have the privilege of working with stuff that runs on dos and 16 bit windows. I need things like qemu, wine, and really appreciate the slick ways that the desktop sees for example an application installed in wine. I can open an image file with qemu from the graphical file manager. I actually changed distributions from one that supported externally and incidentally these things to one that supports them.\n\nNot having support for something you need is a flaw.\n\nDerek"
    author: "D Kite"
  - subject: "Disgruntlements"
    date: 2007-11-06
    body: "I put Kubuntu on my laptop a year ago because I needed a Linux (and the obligatory proprietary drivers). I had no time for a source based distro, or one that took a lot of configuration and RTFMing. So I picked Kubuntu. One year later and I want to get rid of Kubuntu. 7.10 just enhances this desire. When this job finishes and I have time to reformat and use something else, I'll dump it.\n\nI like KDE. I like vanilla from-the-box KDE, with no distro provided \"improvements\". I'm not getting that from Kubuntu. I had to remove several (required) addons to get back to decent KDE. But the underlying system itself is flawed.\n\nI should be able to switch to something else next month. If it weren't for the laptop's need for proprietary drivers I'd use FreeBSD. But that's not the case, and I need to stick with a semi-free Linux system for the time being. I'm thinking of Slackware. Maybe Gentoo. Something basic, slim, with no frills. And a real KDE straight from the source."
    author: "David Johnson"
  - subject: "Re: Disgruntlements"
    date: 2007-11-06
    body: "> I like KDE. I like vanilla from-the-box KDE, with no distro provided\n> \"improvements\". I'm not getting that from Kubuntu.\n\nThen why the @#$% did you install Kubuntu in the first place?? Kubuntu has\nbeen modifying KDE for quite a long time already. It has never claimed to\ncome with a pure \"straight from the source\" version of KDE.\n\n\n(Almost posting from KDE4. Submitting the form didn't work. dang!)\n\n--\nSimon\n\n\n"
    author: "Simon Edwards"
  - subject: "Re: Disgruntlements"
    date: 2007-11-06
    body: "Maybe he didn't know? *I* didn't when I first tried Kubuntu. Of course it never made the \"straight from the source\" claim, but I can't recall a \"comes with a KDE fork\" (I'm exaggerating here, I know) claim either."
    author: "Just me"
  - subject: "Re: Disgruntlements"
    date: 2007-11-07
    body: "You are correct. I just didn't know. I'm a FreeBSD guy, and hadn't been keeping up with the find nuances between Linux distros. I had heard Kubuntu was a good distro for KDE, so I chose it. In its defense, it is a damned quick and easy install."
    author: "David Johnson"
  - subject: "Re: Disgruntlements"
    date: 2007-11-06
    body: "Well, I can tell you why _I_ \"@#$%\" installed Kubuntu in the first place: I tried Dapper Drake and I liked most of it. However, subsequent releases pushed me further away from a \"normal\" KDE working environment.\n\nIn Edgy the Kubuntu team felt the need to remove the display of any directory under /, except for /home and /media, YAY!\n\nEdgy users were mostly unamused, so Feisty saw the feature ripped out again, leaving the people that used it to their advantage (the .hidden file in konqueror could be used to hide any file) fuming. No migration path, no warning, no option to turn it back on... YAY!\n\nYesterday I finally tracked down why konqueror would not let me drill down into tar archives anymore: kubuntu-default-settings wouldn't let me, so another workaround was in order. YAY!\n\nNow let me be clear: These \"features\" are not bad all by themselves, they are bad because:\n1) There are lots of them in each release.\n2) They are not _bugs_ (hey, I can live with bugs), but _experiments_ that lack a direction.\n3) I do not know who to complain/whine to. Is it Kubuntu's fault? Or KDE's? I am getting tired of sifting through megabytes of kubuntu diffs.\n\nTo quote  the original article:\n\"The KDM login screen now sports a user selector that can be configured to show a custom image for each user account - a pleasing effect, and great usability for a family shared computer.\"\nYeah, great. Too bad that the _original_ kdm (the one in KDE-proper) was already capable of doing that a long time ago. Until kubuntu decided to change its default.\n\n"
    author: "timri"
  - subject: "Re: Disgruntlements"
    date: 2007-11-07
    body: "I use Kubuntu and Gentoo and really appreciate the extra polish Kubuntu puts on KDE. Really I kind of see it as the distros job.\n\nAnd it really is just polish, they mostly just tweak the default settings."
    author: "Ian Monroe"
  - subject: "Re: Disgruntlements"
    date: 2007-11-06
    body: "If I may suggest anything - try Arch Linux (http://www.archlinux.org). That's apparently what you're looking for."
    author: "Luke"
  - subject: "Re: Disgruntlements"
    date: 2007-11-07
    body: "Arch certainly isn't vanilla KDE.  They openly advertise how much they've altered their KDE.\n\nHowever, Arch and SuSe both offer great KDE experiences.  I do recommend them over Kubuntu, where KDE certainly feels second-citizen.\n\nhttp://kdemod.ath.cx/"
    author: "T. J. Brumfield"
  - subject: "Re: Disgruntlements"
    date: 2007-11-07
    body: "Standard KDE in Arch is as vanilla as it gets (save for a custom default wallpaper and perhaps splash). KDEmod is an alternate KDE installation for Arch users who fancy splitted packages and whatnot. It's all about choice, really."
    author: "Luke"
  - subject: "Re: Disgruntlements"
    date: 2007-11-07
    body: "Archs standard KDE is completely vanilla, they just added a Wallpaper and a new splashscreen. The only patches applied are bugfixes, thats the philosophy of K.I.S.S. :-)\n\nKDEmod (which is an _unofficial_ Arch _community_ project) has, in contrast, the most patches/extra stuff applied that you will ever see in a KDE distro. I know that, because i am one of the Devs/Packagers :-) We also provide a highly customized theme and are writing new patches on our own etc but, and thats the point, you can remove the theme and disable all extra stuff to get a, lets say, 99% vanilla KDE but with splitted packages...\n\nHowever, the underlying base (=the distro) is another point, and i can say about me that i will never switch back to another distro - Arch is just perfect in all terms... Its clean and simple, always up to date and very customizable, so you get exactly the system you want without fiddling around for ages... And the biggest point for me is: You can contribute very easily. Making packages is a breeze (hey, we are only 2 people who did a complete splitted KDE in almost no time :-), just try to do that on another distro) and you can share them easily through Archs AUR.\n\nI can just suggest everyone who has some understanding of Linux and dont fears the terminal to try Arch, you will not be disappointed :-) And doing some stuff in a terminal does not mean that it doesnt \"just work\"...\n\n"
    author: "Jan"
  - subject: "Re: Disgruntlements"
    date: 2007-11-08
    body: "Having tried KDEmod via Sabayon, I must say I'm impressed.\n\nI wonder how many of the KDEmod improvements will get ported as patches to KDE 4?"
    author: "T. J. Brumfield"
  - subject: "Re: Disgruntlements"
    date: 2007-11-08
    body: "Sabayon? Well, unless you fetched our packages and extracted them manually, you cant have used it on Sabayon :-)\n\nMaybe Sabayon uses the splitted Gentoo KDE packages and you just mixed it up. KDEmod shares some stuff with them in terms of patches (we started with their KIP patchset), but you cant really compare these two... The Gentoo people have splitted KDE completely (every lib etc) where we follow another path and only split things where its useful -  we dont split kdebase and kdelibs for example, as its not necessary in our opinion..."
    author: "Jan"
  - subject: "Re: Disgruntlements"
    date: 2007-11-07
    body: "I suggest to use Mandriva, it has get good reputation from KDE devels because Mandriva devels work together with KDE devels. And Mandriva dont add any patch to KDE what isn't on KDE itself. (this i have understand from KDE devels, and that there is one another distro too what use mainstream KDE what Arch and Kubuntu does not)."
    author: "Fri13"
  - subject: "Re: Disgruntlements"
    date: 2007-11-10
    body: "to be honest, I get bored of these threads where people suggest others try another distro.  People are using the distro of their choice.\n\nI get utterly tired of people who go from distro site to distro site disrespecting that distro and promoting the one they use.  It really has to be the ugliest part about linux.  And I would go even further as say that it most likely puts some people off trying linux because of the very real hostility that some users have towards other distro's.  Don't believe me, go read some of news.opensuse.org, there is always at least one tosser that goes in, disrespects the distro and uses the site to promote the one they use\n\nTo those in the linux community that do that, grow the hell up and stop being a bunch of idiotic trolls.  You are the dark ugly side of linux.\n\nI don't use ubuntu products, I have the distro I like, just as many ubuntu people use the distro they like, so if you don't like the distro talked about take a running jump and leave your negative, and try this distro comments in your mind where they belong, not in the linux community making newbies wonder what a bunch of hostile people you are.\n\nI try to convince people to try linux, I even give out the opensuse live cd, and suggest they try all distro's before they settle on one, and it is really a pain in the royal ass when they turn down moving to linux because they go to distro pages, go to KDE and see people constantly dissing distro's and suggesting they try another.  \n\n"
    author: "Richard"
  - subject: "Re: Disgruntlements"
    date: 2007-11-12
    body: "While I understand what you are coming from, please realize the suggestions in the context of the thread. The parent post that began this thread was voicing his dissatisfaction with kubuntu, and what has happened so far is a civilized thread discussing other KDE distros and theirs pros and cons. There's no flaming, no religious wars, and no bad attitudes here.\n\nI have found the entire thread so far to be quite educational as the reason I am even reading this news article is I am looking to try a new kde based distro as my main system. There's a reply saying \"try mandriva, I hear it's pretty good.\" There's a reply saying \"beware of arch, they also heavily modify their KDE\", and a counter by an arch dev telling how to get an unmodified KDE if you want it.\n\nThis thread has been informative and well mannered. Please save your distaste for the posts that deserve it, and I know precisely what you are talking about. It usually comes in the form of a person asking about a specific feature of a specific distro and some jackass replying with \"X distro sucks! Use Y distro!\""
    author: "MrGrim"
  - subject: "Re: Disgruntlements"
    date: 2007-11-07
    body: "After trying (K) (X) Ubuntu versions 6.06 to 7.04 I installed PCLinuxOS 2007 that natively features KDE on my main computer and laptop, and couldn't be happier. No proprietary issues, and it's slick and stable. you may want to look into it."
    author: "Frank S"
  - subject: "Re: Disgruntlements"
    date: 2007-11-08
    body: "Debian's KDE is pretty true to the distribution, and works fine, though updates are slow, even on unstable. \n\nJust don't install a desktop system [ ]. For some reason, Debian equals that with Gnome, and installing all that Gnome just to remove it again is no fun.\n\nAs a bonus, it's pretty close to what Kubuntu does. And the Debian mailinglists offers as many flames as you can take, and then some :o)\n"
    author: "Esben Mose Hansen"
  - subject: "Re: Disgruntlements"
    date: 2007-11-09
    body: "> though updates are slow, even on unstable. \n\nSlow on SID?\nKDE updates are usually available when the release announcement hits the web, some of the packages appear even during the week after tagging.\n\n> Just don't install a desktop system \n\nActually just use the first CD iso named $version-kde-CD-1-iso\nThere is also such a CD 1 for XFCE, the one for GNOME is missing.\n\nThe generic CD 1 has a bug in its desktop task item, i.e. it fails to ask the question which desktop to install and just uses one of the choices. It is possible to set this automatic choice by a boot prompt parameter though.\nGiven the high quality work usually available in Debian this bug will likely be fixed soon.\n\nOne can get further information about KDE on Debian Etch here:\nhttp://pkg-kde.alioth.debian.org/installkdedebian.html"
    author: "Kevin Krammer"
  - subject: "IT Pro - now relaxing"
    date: 2007-11-13
    body: "Hey Guys, I have to say that I agree about the blogs being hostile out there and how one person prefers and pushes a particular distro over another. I have tried many distros, from the meek to the over-glorified (no names given, but I'm sure most of you are aware :-). One thing I have to say is, if you happen to be a newby, then Ubuntu and it's several flavors, is DEFINITELY a good starting point - and for that purpose alone, it's hard to find fault in the distro... Hey, most of us responding to this blog are experienced Linux users. To us, we find fault because the distro doesn't support one or another feature. We each have our faves and anyone else (or, perhaps, even ourselves in the future) will find some fault with it. One could just as easily go on and on about our choice in desktops - or even operating systems, for that matter. I prefer to see each person's choice for exactly that - their choice. If we continue to down (or even push, for that matter) any other distro/desktop we DO confuse any potential new users out there. When we should simply be saying: \"Hey, here's a free disc. Let me know if you need any help and I'll be more than happy to oblige.\" Once the new user gets up to speed, they can decide for themselves if it supports what they need and you can guide them from that point.\nHappy computing;\nJim\n"
    author: "Jim"
  - subject: "Distribution DOES matter--esp. w/ newbies"
    date: 2007-11-21
    body: "Ubuntu is a decent distribution.  No doubt.  Of course, this thread is about Kubuntu.  \n\nIt very well may be a good starting distro, but remember that there are other good starting distros out there.  \n\nI happen to like openSUSE quite a bit, part of which is how easy it is to install (although I second the notion of using Smart rather than Yast for package management) and run.  KDE is well supported via the various package repositories, including KDE 4, backports, community, etc.\n\nIt also has pretty good hardware support (with the exception that you do need to go to the NVIDIA and ATI maintained binaries on your own)--I have had zero problems with an IBM ThinkPad T42.\n\nI think that the notion that you can just give someone new to Linux any old distro and let them decide is a bit silly.  I *tried* Red Hat (before there was even a Fedora) as my first and it was painful.  I switched to SUSE (before there was openSUSE/Novell SUSE) & it made a huge difference in my motivation to stick with Linux.  \n\nI think the choice of a distribution makes a tremendous difference in many areas.  I think that even new users need to understand that there are different distributions for a reason (so they can focus on specific aspects, etc.), as well as different GUIs, different windows managers, different XXX, and that is one of the best things about Linux these days.  There are certainly distributions that non-techies/non-masochists should stay away from--and people new to Linux should be told this!\n\nSo, pick a humane distribution for new folks.  Ubuntu is reasonable, Kubuntu is probably getting there, but openSUSE (and others) already has great KDE support.  OpenSUSE probably does need a little more handholding, since it offers a CHOICE of KDE, Gnome, minimal GUI, text mode, etc.  (Can add Blackbox, XFce, etc. later as well) as well as a ton of end-user applications (probably too many choices in media players and too few for XML editors IMHO).\n\n"
    author: "DrK"
  - subject: "Re: Distribution DOES matter--esp. w/ newbies"
    date: 2008-02-11
    body: "I am running Win98 at home and WinXP at work.  I am looking for a Linux that I can learn on since Vista is a little scary. (Those iMac attack ads seem to work)\n\nI have tried Puppy Linux and had a good experience.  I am currently trying Kubuntu on VMWare Player and having a more difficult time.   Most of the difficulty comes from software installation and directory privileges.  I guess that is the price to pay for moving to a more secure multi user system from a single user system.\n\nYears ago I had just gotten used to setting up DOS 2.11 systems when Win3.0 came along and then Win3.11 and then Win95 and then Win98 (Win32).  I paid full price for two of these, with the others being OEM and upgrades.  I can't bear the thought of having to upgrade hardware every time more eye candy comes out.\n\nAll distros have their good and bad points but for newbies like myself the big determinant is informative and easy to follow documentation and tutorials. Well implemented installers would also be a big help.\n\nMy problem is that I like to have the new OS's but I haven' got the skills yet to be able to install Flash Player, Firefox, etcetera.  This being said I wonder if the distros are being made available a little too soon in their life cycle without their contents being described as Alpha or Beta versions.  Maybe they should list the distro's contents so we newbies can pick the one that requires the least tweaking?\n\n<B>Bravo Open Source<B> for giving enthusiasts around the world a great adventure."
    author: "Engineer John"
---
KDE's intrepid FAQ maintainer, Jes Hall (aka canllaith), spent some time getting cozy with the latest <a href="http://www.kubuntu.org/">Kubuntu</a> release and wrote a nice summary for our enjoyment. Get her thoughts on Gutsy Gibbon (Kubuntu 7.10) at <a href="http://canllaith.org/?page_id=48">her blog</a>.


<!--break-->
