---
title: "aKademy 2007: The Tracks"
date:    2007-07-02
authors:
  - "jventuri"
slug:    akademy-2007-tracks
comments:
  - subject: "you rock"
    date: 2007-07-02
    body: "Thanks for this great summary guys!!"
    author: "DITC"
  - subject: "Re: you rock"
    date: 2007-07-02
    body: "Agreed!\nThank you all, impressive work!"
    author: "AC"
  - subject: "Re: you rock"
    date: 2007-07-09
    body: "My pleasure, and I'm sure Danny enjoys it as well ;-)"
    author: "jospoortvliet"
  - subject: "Scripting"
    date: 2007-07-02
    body: "I am still confused about KDE's vision for what kind of scripting interface an application should use. There were talks about KJSEmbed and QtScript, could someone explain how those relate to Kross, which I thought would be the new, language-agnostic scripting interface?"
    author: "ac"
  - subject: "Re: Scripting"
    date: 2007-07-03
    body: "Since the functionality of applications vary, what they need from the scripting interface will also vary. Thus the KDE vision for scripting interface is flexible. \n\nThe lanuguage-agnostc interface are prefered to be used where heavy use of scripting are expected, giving users the ability to use their preffered scripting language. And among several languages, Kross already support KJSEmbed.\n\nIf the application has simpler needs regarding scripting, it should use QtScript/KJSEmbed. But the decission of wich(or both) solutions to use in KDE 4.0 are not made yet, according to Richard Moore's talk. http://akademy.kde.org/conference/talks/36.php\n\n"
    author: "Morty"
  - subject: "KHTML as a Qt Widget!"
    date: 2007-07-02
    body: "that rocks!\nI also hope that webkit will finally get into KDE and it sure will as a Qt widget :)"
    author: "Patcito"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-02
    body: "yeah, really looking forward to that bloat fest.\n\n 1900932 jui  2 22:39 libQtCore.so.4.3.0*\n 9238672 jui  2 22:39 libQtGui.so.4.3.0*\n\n15170192 jui  2 22:38 libQtWebKit.so.1.0.0\n\nThat's right. WebKit alone is already much bigger than all of Qt Core+Gui and duplicates most of'em. Yay for code reuse.\n\nAnd you've seen nothing yet as browser vendors are just about to start the HTML5/CSS3 feature war and push even more useless crap down our throats. Madness."
    author: "paul"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-02
    body: "That's some might excellent compiling you've done.  My WebKit is 3MB."
    author: "George Staikos"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-03
    body: "> My WebKit is 3MB\n\nin its bz2-compressed archive? Yes, whatever.\n\nyou know, you can spin it all the way, cripple your build into uselessness and remove every feature there exist a switch for, that won't change the simple and unavoidable fact that a great deal of WebKit is about reimplementing Qt.\n\nOh wait, you did not denied that anyway."
    author: "paul"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-04
    body: "WebKit: 15MB\nAverage hard drive: 100000MB\n\nI think we'll cope. The other risks and rewards of WebKit are of a much higher order of significance than a mere 15MB. And much of this code reimplementing Qt which you take exception with is a requirement for WebKit's wide portability -- how else could Apple use it in their Cocoa-based Safari browser, and at the same time, KDE in their Qt-based applications? There are GTK implementations, as well. This portability has a very concrete and significant benefit: web developers are far more likely to test and optimise their code for rendering engines with higher marketshares."
    author: "illissius"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-03
    body: "I guess you forgot to strip the debug symbols..."
    author: "Henning"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-03
    body: "Of course not. All those libraries are built with comparable options and stripped.\nIt's possible to make them all somewhat smaller (though the size george states is pure bullshit), but that won't change the size ratio.\n"
    author: "paul"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-03
    body: "Paul,\n\nKDE is actually a friendly community and respecting each other is the basis for working together well. \n\nIt's highly disrespectful to say that one of our core contributors is 'talking bullshit', and I'd like to ask you to show the due respect to the people who work on Free Software -- often those are volunteers and are being put off by comments as yours. Besides that, your points are very weakened by the way you're putting them forward.\n\nSo please show the due respect or just stop posting. Also, an excuse for your rude behaviour towards George is in place."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-03
    body: "well I think very highly both of friendship and of respecting each other.\n\nRespect is first and foremost about not ruthlessly steping accross some virtual lines that define the limits of trust and honesty between well behaved individuals.\n\nWhen some people consistently confuse Public Relation Spin and outright deception, distorting facts for the benefit of a specific agenda, they are doing a great disservice to the community and showing complete disrespect to people who argue with real world technical evidence.\n\nOnce you've crossed that border a great many times, you can't expect people to show much respect for your arguments in return."
    author: "paul"
  - subject: "Re: KHTML as a Qt Widget!"
    date: 2007-07-04
    body: "I agree George could've responded better, eg go into the duplicating thing you where talking about. Still, I think you where very rude, and I agree with Sebas here - saying sorry wouldn't hurt anybody.\n\nOf course, it's up to you, and luckily most of us can handle some rudeness (after all, geeks aren't known for their social skills). And this is an interesting topic you got there - I'm no hacker, so I don't know much about this. So I'd love to hear how Webkit is duplicating stuff in Qt. And if it is, doesn't that go for every html engine, or is it webkit specific (due to it being crossplatform or something?). And can something be done about that? (after all, if so, they might be working on that already)."
    author: "jospoortvliet"
  - subject: "Jacob Rideout"
    date: 2007-07-02
    body: "Hi there,\n\nsome weeks ago there was a post on planetkde.org (http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/429-Tidbits-Jacob;-Amsterdam;-Holland-Open.html) about the vanashing of Jacob Rideout (Sonnet developer). Is there any news about this or even a rumor?\n\nRegards\n\nHope I don't make waves here.\n"
    author: "oliv\u00e9"
  - subject: "Re: Jacob Rideout"
    date: 2007-07-03
    body: "He didn't register or show up, so there's not even a rumour. The university which I contacted did not reply -- I can imagine there are privacy concerns there, so I don't blame them."
    author: "Adriaan de Groot"
  - subject: "Re: Jacob Rideout"
    date: 2007-07-04
    body: "okay but sad. thx!"
    author: "oliv\u00e9"
  - subject: "pyqt4 rocks"
    date: 2007-07-03
    body: "I'm really starting to like python. it's a nice little language. with python, pyqt4, and qt designer, I can make stuff really quickly and easily :)\n\n..however, I wouldn't use it for something that had to run all the time. \nlike, say, a battery monitor. :P"
    author: "Chani"
  - subject: "Re: pyqt4 rocks"
    date: 2007-07-04
    body: "yeah, they've been discussing this a lot. If someone improves the 'competition', they might switch, I think. But they're also working on lowering the resource usage."
    author: "jospoortvliet"
  - subject: "Scripts security"
    date: 2007-07-03
    body: "We hope that applications scripts are so high level that can not be used to spread \"malware\" between users.\nI hope they can not be installed from any source, just by the user when it has requested it.\nI hope they can not read and write files and communicate over the network directly, but throught the application read and write capabilities and networking application capabilities, ensuring that only correct data is readed and writed.\nI hope we do not have to have an antivirus program just for kde scripts.\n\nBest regards."
    author: "Anonymous"
  - subject: "Videos!"
    date: 2007-07-03
    body: "Wow - videos of the talks *already*? I remember with '06 that it took until Feb '07 to get them all uploaded!\n\nWould anyone mind if I torrented them somewhere and Digg'd the torrent? Might ease the strain on your servers, somewhat ..."
    author: "Anon"
  - subject: "Re: Videos!"
    date: 2007-07-03
    body: "how do you play them ?\n\ni tried vlc, mplayer [-forceidx] and kaffeine, but always with glitches"
    author: "pvandewyngaerde"
  - subject: "Re: Videos!"
    date: 2007-07-04
    body: "unfortunately the sound quality is not too good - speakers are very difficult to understand as they are far away from the microphone..."
    author: "MK"
  - subject: "Re: Videos!"
    date: 2007-07-04
    body: "It's a mystery that not even nerds can learn the very basics of sound recording.\nGoogling is easy, here is an example:\nhttp://www.google.com/search?hl=en&q=microphone+recording&btnG=Google+Search\n\nhttp://www.ehow.com/how_15152_microphone-record-speech.html\nhttp://www.epinions.com/content_2034933892\nhttp://www.hrelp.org/archive/advice/microphones.html"
    author: "reihal"
  - subject: "Re: Videos!"
    date: 2007-07-09
    body: "assuming they do know the basics, I'd not bet the average geek got some spare Neumann U87's hanging out of their pockets, as well as all the semi-pro gear needed to drive them.\nMore likely, all they got is an el'cheapo electret bundled in whatever recording device they are using.\nGood luck getting any decent result with that, even if you do know what you are doing."
    author: "richard"
  - subject: "Re: Videos!"
    date: 2007-07-04
    body: "Somebody else uploaded them.  Story here:\n\nhttp://digg.com/linux_unix/KDE_s_Akademy_07_Videos"
    author: "Anon"
---
On Saturday, <a href="http://conference2007.kde.org/">aKademy 2007</a> kicked off with a keynote by Lars Knoll of Trolltech, with <a href="http://dot.kde.org/1183385741/">two further keynotes</a> throughout the day by Mark Shuttleworth ('13 Lessons for the Free Desktop') and Aaron Seigo ('Beautiful Features'). After Lars' talk, two separate tracks started. Read on for coverage of these tracks.






<!--break-->
<h2>KDE 4 Pillars</h2>

<h3>Strigi</h3>
<p>Jos van den Oever gave an overview of the history of search in KDE. The original implementation began with KFind (1996), followed by KFileMetaInfo (2001) and Kat (in 2005). Last year, at aKademy 2006, <a href="http://nepomuk.semanticdesktop.org/">Nepomuk</a> and <a href="http://strigi.sourceforge.net/">Strigi</a> were introduced. Nepomuk uses sematic storage technology, following a new <a href="http://freedesktop.org/">FreeDesktop.org</a> technology (Xesam). Strigi consists of data extraction, indexing and search elements.</p>

<p>These two new KDE tecnologies form part of the Pillars of KDE 4. A problem with existing desktop search was the difficulty of managing many file formats, many tools and many interfaces. KIO (for KDE), and VFS (for GNOME), doesn't work in all cases (such as when 'chained' in recursive directories) and they are not a complete solution for desktop search.</p>

<p>Strigi uses one stream of information that is processed by a pipeline, producing different levels of analysis, with the result produced at the end. Testing has highlighted that Strigi appears to be much faster</a> then Beagle, jindex, and Tracker (indexing 10,000 files, 168 MB in total). One of the KDE Summer of Code projects enables the indexing of 18 different chemical formats using Strigi.</p>

<h3>Flake</h3>
<p>Boudewijn Rempt presented the new KOffice 2 tecnology, <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a>. This lets you compound documents. Flake composes your document from individual objects: text frames, images, vector graphics, tables, connectors. Boudewijn illustrated the controller, the model, and the view. The KoTool create a KoShape and the KoShape create the KoCanvas. Each application has a specific canvas. Shapes can be created by factories, from templates, can be nested, grouped and aligned. Shapes know how to draw themselves, and load/save other shapes. Colour is a problem because of the subtly-different results produced by different devices, such as scanners, cameras and displays. Flake is not complete for all shapes. It is currently missing a table shape, video shape and a map shape. One of the Summer of Code projects will see the creation of a music shape to author and represent sheet music.</p>

<h3>ThreadWeaver</h3>
<p>ThreadWeaver simplifies the life of the developer in the age of multi-core processors, and introduces new performance and functionality improvements. Mirko Boehm refers to ThreadWeaver as "walking and chewing gum at the same time". ThreadWeaver enables both complete utilisation of all available CPU resources, and also of the temporary suspension of tasks, for instance, indexing updates of Strigi. To take advantage of ThreadWeaver, an application only has to implement a job class, put it in the (singleton) queue and wait for the job to end. Illustrating the promise of ThreadWeaver in practise, Boudewijn Rempt revealed that Krita has implemented visual filter previews using ThreadWeaver, to prevent GUI lockups.</p>

<h3>Sonnet</h3>
<p>KSpell1 was the original attempt to solve the complex technical issue of spell checking in KDE. Completely tied to the ispell backend, significant problems were soon noted, often arising from incompatible changes in the backend. After several major releases, KSpell1 stopped working completely. Realising this terrible state of affairs and wanting to prevent a future repeat experience, Zack Rusin created KSpell2, a backend-independent solution for KDE 3.2 - ispell was immediately depreciated in favour of the aspell backend. Unfortunately, this second implementation features an extremely complicated and over-designed API.</p>

<p>Sonnet marks the third KDE approach at the spell checking problem. Developed with the twin aims of a simplified API (the previous solutions required 7 components to spell, Sonnet consists of a single element), performance (KSpell used separate backend processes even for concurrent checks) and wider language support. A primary issue for spell checking certain languages, such as Thai and Japanese, is boundary detection and determining meaning which changes based on context. Sonnet currently includes English, French, German and Polish. The much-requested correction suggestion menu will also make an appearance in the Sonnet implementation.</p>

<h3>Akonadi</h3>
<p><a href="http://pim.kde.org/akonadi/">Akonadi</a> is a new framework for the <a href="http://pim.kde.org/">KDE-PIM</a> Project. Akonadi is based on an asyncronous library, and has a KJob-based API, which lets you adjust monitoring tasks, including notification filters. This enables several new features, not least simpler mail support. Rather than write custom connectors for each PIM application, developers can now simply write a serialisation plugin and an agent, and retrieve data through this channel. Agents react on changes (like when an item is added or removed from the Akonadi storage structure). Till presented a simple Plasma applet showing notifications of new email and how the Akonadi console displays the messages.</p>

<h3>Quasar</h3>
<p>After the impromptu appearance of Zack in the Sonnet talk, the second talk from Zack introduced Quasar, a framework for graphic effects. Mathematically and graphically accurate, Quasar enables the developer to chain filters together to produce the desired effect without requiring any knowledge of the maths behind the scenes. The current focus within Quasar development includes improving the performance when processing large images (+30,000 x 30,000 pixels) and combining Phonon to allow real-time manipulation of video imagery.</p>

<h3>Decibel</h3>
<p>Tobias Hunger began his talk by explaining to us why he thought <a href="http://decibel.kde.org/">Decibel</a> was needed. He showed us a sheet with a lot of different applications and protocols, each meant for communication, but rarely actually communicating with each other. This posed a problem for end-users, as they must use several different chat applications and tools to be able to communicate.</p>

<p>For developers, the situation isn't much different. Applications want to support as many protocols as possible, but this leads to a huge amount of work. Not just the protocols themselves (often proprietary and hard to get right), but you also need to work on UI, provide configuration tools and a lot of protocol-specific things.</p>

<p>Decibel separates the protocols (with a plugin structure), based on Telepathy, from the basic GUI and configuration. It manages, stores and retrieves presence, user management and contacts. The most interesting thing is that Decibel is based on a component manager. A component is an interface, using and/or presenting the Decibel functionality. Decibel supports profiles for these components, so you can, for example, ensure that during your working hours, a SIP call leads to the opening of a phone window, while when you are away, the 'out of office' profile ensures calls are answered by the component providing an answering machine.</p>

<p>Currently, there still is quite some work to do on Decibel, but Tobias ensured us he would have been able to show a working phone connection if a component on his laptop correctly resumed after the time spent in suspend mode. Yet, he was able to present us with a compelling vision of easy communication in every application which requires it. Decibel communicates using D-BUS, and the Telepathy system supports making remote D-BUS connections between peers, thus allowing for inter-application communication over a network. Implementation of functionality such as real-time co-operation would be eased by Decibel.</p>

<h3>WebKit</h3>
<p>Lars Knoll held another talk on Saturday, this time about Webkit within KDE. Lars started with a historic overview of <a href="http://www.khtml.info/">KHTML</a>. KDE started with a simple filemanager, KFM, but quickly felt the need for a HTML engine. In time, demands grew, and the HTML engine went through several big refactorings, adding a DOM, CSS support and scripting. KHTML was most successful in the 2000-2003 period, when it was the first web browser supporting right-to-left languages, and enjoyed a majority marketshare on Linux/UNIX.</p>

<p>In the Summer of 2001, Apple silently forked KHTML, and after 2 years of work on it they announced Safari. Though the community was proud that Apple recognised the quality of KHTML, the developers were less happy. Apple initially didn't play well with the community, releasing source code by dropping huge monolithic tarballs at a time. The frustration grew, and came to a climax with a blog by Zack Rusin about the lack of co-operation from Apple. Apple responded, and started the Webkit project, slowly opening their development. Finally, in 2006 a research project was initiated by some KDE developers to bring Webkit back into the arms of KDE. KHTML's marketshare has diminished, while Webkit has a 5% marketshare through Safari, and even more with the help of the Nokia S60, which sold millions. Several companies are working on Webkit besides Apple and Nokia, such as Adobe and Trolltech.</p>

<p>Trolltech, searching for a cross-platfrom, high-quality HTML renderer, is working on integrating Webkit in Qt 4.4 as a widget. They are also committed to developing a KPart with full KIO and KWallet support, thus integrating it into KDE. Discussion about replacing KHTML with Webkit in KDE is still ongoing, but according to Lars, unifying Webkit and KHTML would be the best option. Despite his initial scepticism, he found Apple to be very supportive to Trolltech's efforts integrating Webkit in Qt product and in KDE, and Apple engineers have even contributed considerable amounts of code. Working together increases the marketshare and thus mindshare Webkit has, and could benefit all parties.</p>

<h3>Bindings</h3>

<p>Meanwhile, Simon Edwards kicked off the Bindings track (which ran alongside the KDE 4 Pillars track, often resulting in a difficult choice for attendees!), describing the process of creating KDE applications in Python. He started with Python and its features, but as many attending the talk were Python veterans, he quickly continued to talk about his own involvement. Simon worked with C++ for a long time, but never was too happy with it. When he discovered Python, he immediately fell in love. For him, the binding of Qt/KDE and Python is a 'match made in heaven'. He started developing system configuration tools for Kubuntu. He showed how similar the Python-Qt-code is to C++. The bindings form a glue between the typical C++ syntax and the Python one, yet preserving many of the cool Qt features. Yet it is very efficient to program, and the performance overhead is very small due to the use of the Qt and kdelibs for the heavy lifting. Painting is delegated to the Qt libs, yet it is possible to subclass a C++ routine, and modify it. The biggest disadvantage is the memory usage of the python interpreter, but this can be shared between all Python processes.</p>

<br><a href="http://static.kdenews.org/dannya/akademy_bindings.jpg"><img src="http://static.kdenews.org/dannya/akademy_bindings_small.jpg" alt="" title=""></a>

<p>Next, Simon progressed to the 'cool stuff', showing how to embed a Python application in the C++ based KControl. He expects KDE 4 Python bindings to keep up with KDE development, releasing a beta alongside KDE. Then he talked about the nice side-effects the Python work brings, like faster development, better debugging information and a lower barrier to contribution. According to Simon, PyQT and PyKDE bindings bring the fun back into development.</p>

<p>After the talk by Simon, one our youngest members paired with one of our more seasoned hackers to give a talk about their work on the C# bindings. We will cover thisthis in a later article (an interview is planned) so you'll hear about it!</p>

<h3>KJSEmbed and QtScript</h3>
<p>After the coffee break, Richard Moore talked about KJSEmbed and QtScript. These scripting tools give several new possibilities to applications. First, you can have parts of applications in a higher-level, dynamic scripting language. Second, you can extend applications later on, by shipping the scripts separately. And lastly, your users can use scripts to automate tasks or extend the applications themselves. After an overview of scripting in user applications like emacs, Dreamweaver and Paintshop Pro, Richard continues discussing the features every scripting framework should offer. The ability to extend functionality early, at a high level, coupled with being able to stick components together and automate existing functionality are key components of a scripting framework. KJS was written for web browsers, while QtScript is specifically written to script Qt based applications.</p>

<p>Kent Hanson's talk had a strong focus on examples and really showed what was possible with QtScript. He showed how the scripting system used signals and slots and gave a quick overview of the API.</p>

<br><br>

<h2>Further talks</h2>
<p>There where other talks about okular, cryptography, Kexi, Krita and a series of short talks about the KDE Games (like 4 minutes each). Some of these talks where to technical, or we where unable to cover them due to a lack of manpower (it's just two of us...) so if you want to know more, you can have a look at <a href="http://akademy2007.kde.org">the aKademy site</a>, where slides and even video's will appear.

<p>We heard many interesting talks, and enjoyed writing about them, so we hope you did enjoy reading about it. As said, video's and slides will appear on <a href="http://akademy2007.kde.org">the aKademy site</a>, and we're sure the authors of the talks will happily answer questions. And of course, Sunday brought another bunch of talks, which will be covered in the coming days, together with hopefully many of the BOF's and some interviews.</p>

