---
title: "New KDE Distro Releases: Mandriva 2008 and Kubuntu 7.10"
date:    2007-10-24
authors:
  - "rjohnson"
slug:    new-kde-distro-releases-mandriva-2008-and-kubuntu-710
comments:
  - subject: "Memory requirements"
    date: 2007-10-24
    body: "What I was wondering about were the new memory requirements of 320mb. Would you recommend me to rather try Xubuntu?"
    author: "benja"
  - subject: "Re: Memory requirements"
    date: 2007-10-24
    body: "The 320 MB memory requirement is for running the live-CD."
    author: "."
  - subject: "Re: Memory requirements"
    date: 2007-10-24
    body: "Why does the xubuntu CD then take much less mem"
    author: "benja"
  - subject: "Re: Memory requirements"
    date: 2007-10-24
    body: "Probably the best place to ask this would be on ubuntuforums.com."
    author: "kwilliam"
  - subject: "Re: Memory requirements"
    date: 2007-10-25
    body: "As it was stated above me, that is for the LiveCD, since everything is in memory for the LiveCD.  Installing and running it is another story."
    author: "T. J. Brumfield"
  - subject: "Mandriva 2008 works great"
    date: 2007-10-24
    body: "I installed Mandriva 2008 (PowerPack with KDE) a couple of days ago and so far it works really well. More works out of the box than in Kubuntu and it is also easier to enable Compiz Fusion and install printer. In general the configuration tools in Mandriva Control Center are very good. And it looks like the KDE in Mandriva 2008 is more close to version 3.5.8 (it already has the new menu to change application language for example). I haven't tested how their KDE4 packages work yet. Anyways, I highly recommend you check out Mandriva. It's a very solid KDE distribution. Too bad it doesn't get as much attention from users as it deserves."
    author: "Neja"
  - subject: "Re: Mandriva 2008 works great"
    date: 2007-10-24
    body: "Yep, Mandriva is my current favorite too."
    author: "reihal"
  - subject: "Re: Mandriva 2008 works great: even with laptop(s)"
    date: 2007-10-25
    body: "Yes, and for the first time everything works great in my laptop: energy handling, suspend to ram/disk, bi-screen, etc.\n\nThis is finally quite perfect !"
    author: "Kleag"
  - subject: "Re: Mandriva 2008 works great: even with laptop(s)"
    date: 2007-10-25
    body: "Ben using mandriva for 7 years now, its simpli my favorite distro. Tried several others but always ended up coming back to my mandriva/mandrake.\nThis new mandriva is like all other before juts another step in the right direction. "
    author: "nuno pinheiro"
  - subject: "Re: Mandriva 2008 works great: even with laptop(s)"
    date: 2007-10-29
    body: "Same experience here with my HP notebook computer.  I tried Mandriva 2008 on my desktop test system and was so happy, I decided to do an install on the notebook.  Everything was detected and worked great.  The real surprise was wireless.  My HP uses the Broadcom 4318 wireless.  With OpenSUSE and Fedora, it's always a two day challenge to get wireless working with ndiswrapper...and that's with forum help.  It seems that the procedure is always a bit different between versions and a real headache.  With Mandriva 2008, I downloaded the recommended firmware....about three clicks and less than a minute later, my wireless was working flawlessly!  I was impressed enough to remove my new OpenSUSE 10.3 installs on my other computers in the house...and install Mandriva 2008.  Think I'll be staying with Mandriva!"
    author: "zenarcher"
  - subject: "Customer service is the worst out there"
    date: 2007-11-16
    body: "Bought 2008 Powerpack DVD. Waited 10 days. Received an e-mail from Mandriva saying that 'their supplier' cannot send within (another) 10 days. Ooooh right - I forgot that we're dealing with Mandriva here. They took my money right away (about 50 euros) and then they SIT on it for 20 days (and maybe more) before shipping it.\n\nThat reminded me instantly of my experiences when buying two earlier Mandriva versions on DVD. I hoped they would have learned by now. They won't. They despise customers.\n\nOther irritations:\n- hopelessly confusing website\n- UTTERLY TERRIBLE CUSTOMER SERVICE: I put in 2 service tickets WHILE BEING ENTITLED TO SUPPORT last year, and they didn't bother to reply. You see - they already have your money. They SHIT on paying customers. Bah."
    author: "User2"
  - subject: "Re: Mandriva 2008 sucks"
    date: 2008-03-14
    body: "Last night I installed Mandriva 2008 Powerpack on my notebook. I have some weird problems after installation. I am unable to pay video dvds on lindvd. What's worse, I can't browse the video dvds though I have no problem browsing data dvds.\nI have also installed libdvdcss still nothing to my avail.\n\nPreviously I had none of the problems with mandriva 2008 free. I had a great computing experience after installing some non-free stuffs. But how come such weird things happen with powerpack.\n\nI think it's the most hurried or improvised work than a polished one.\n\nVisit this thread for more: http://mandrivausers.org/index.php?showtopic=52704&hl="
    author: "manmath sahu"
  - subject: "mandriva is an excellent distribution"
    date: 2007-10-24
    body: "2008 PP works great, plug and play of hardware works wonderfully printers for instance, I have a HP laserjet 4P, I would allways have to play around because cups \"laserjet4p\" wouldn't work but, \"laserjet\" would I would ALLWAYS have to change this with mandrake/mandriva. my sansa e250r works great and amarok sees it \nperfectly. as well as usb cd/dvd burners.\n\nmandriva, in my opinion is a premier KDE distro and really integrates with it well. "
    author: "madpuppy"
  - subject: "Re: mandriva is an excellent distribution"
    date: 2007-10-29
    body: "I agree about Madriva and printers.  I hadn't tried Mandriva for about three years, so decided to try Mandriva 2008.  My HP Officejet 6210 was automatically recognized and installed, as I would expect.  What I did find, and did not expect, was that the internal fax for the multi-function printer was also found and properly set up, as well.  No other distro I've used has done this.  I'm going to be staying with Mandriva."
    author: "zenarcher"
  - subject: "What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "This is a genuine question, not a provocative flame...\n\nCould someone who uses it please explain the appeal of Kubuntu? Why do they prefer it over other distros? I used it early on and simply was not impressed for reasons I will outline below but it seems more and more people, even technically knowledgeable hardcore users are adopting it and I would like to find out what its appeal is.\n\n\nMy main problems with it are due to its poor implementation of KDE.  I would have prefered an installation with the default settings intact and a complete set of KDE applications on the CD. Even things like the absence of some view profiles in Konqueror is frustrating.  I know it is aimed towards the home market and that it is relatively easy t return Konqueror to default but it is frustrating that KDE's power has been curtailed.\n\nHardware support is terrible.  Fedora still supports more of the hardware on my personal machine than Kubuntu does.\n\nBeing limited to a single disk is frustrating for those of us without high speed internet connections.  For those of us who have to order via Shipit or obtain the system from magazine covers due to extremely slow networks, the lack of applications is a real disadvantage.  If Shuttleworth can afford to send people multiple copies of the OS install disk, why not compile a second disk of the most popular applications from the repositories for those who request it.  It hink the one CD barrier will really bite them soon as more and more high quality Linux applications emerge and existing applications become larger.\n\nFinally, the default theme is terrible.  I know this is only cosmetic and can easily be changed but it seems to show a lack of care and thought on the behalf of the developers.  I cannot believe that Ubuntu's brown Human theme attracts so much criticism when the blue Kubuntu theme is so much worse.  It is not just a matter of subjective taste; I actually find that the theme is literally painful to look at.  The \"blue on blue\" hurts my eyes and I am sure that I am not the only person.\n\nAgain, all of these thoughts are based on earlier versions, so if hardware support has improved in the last year or the theme is better, please bear that in mind.  However, I would definitely like to hear from experienced KDE hackers to find out what they find so appealing about Kubuntu compared to other KDE distros."
    author: "The Vicar"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "Its easy: Kde + Debian foundation + 0.5yr release schedule"
    author: "Anon Ymous"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "Thanks for replying. Yes, those are the things about it that I find alluring too, but the issues I raised above outweigh those benefits at the moment, for me.\n\nI personally use Fedora at the moment:  KDE support is improving and it has the 0.5 year release cycle. Hardware support has always been better for me, the theme is better and there is a more comprehensive range of packages available on the install disk(s) and it also has a repository available for more packages.\n\nThat really leaves the factor of Kubuntu having a Debian base as the main area of differentiation between the two distros."
    author: "The Vicar"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-26
    body: "> That really leaves the factor of Kubuntu having a Debian base as the main area \n> of differentiation between the two distros.\n\nWell, If you are comparing fedora and ubuntu, then i cannot disagree.\nAs for me, I just got used to all that .deb infrastructure. I am using RHES 4.5 at work now and unfortunately rpm does not give me any functional tools for installing RH packages. rpm -qa , ctrl-f in package repo page is a joke, Yum and all GUI tools are to slow, unintuitive for me. \n\nIn debian/ubuntu you have aptitude - which IMHO is almost perfect for package management. (both server and workstation)\n\n"
    author: "Anon Ymous"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "I've noticed most KOffice developers use Kubuntu. And so do I. The reasons:\n\n* easy to install, works with all my hardware out of the box\n* easy to update\n* easy to get a development environment up and running\n* Jonathan Riddell is very responsive\n\nI have never seen the things Kubuntu does to KDE outside the install environment because I'm taking my home dir wherever I go, and that's got settings carried over from my fvwm1 days, let alone years of KDE customization."
    author: "Boudewijn Rempt"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "Thanks for responding with your thoughts. It is really interesting to know most of the Koffice people are using it.\n\nCheers."
    author: "The Vicar"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "One word: apt."
    author: "Jonathan Verner"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-26
    body: "Two words: apt-rpm. The days where only Debian had apt are long past. Not to mention there are plenty of other dependency solvers with similar features nowadays."
    author: "Kevin Kofler"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "Easy: the Debian factor.  Debian is an extremely well organised distro, and its repositories are so comprehensive it is difficult to find an application or library that is not a part of it.\n\nSo why not just run Debian then? -- you might ask.  Because out-of-the-box, Debian still requires some tweaking before you get a usable, good-looking desktop.  Moreover, if you want to run a stable system, you are often stuck with very old versions of packages (though in practice, \"Debian unstable\" is actually pretty stable).  Kubuntu just takes a fairly recent snapshot of Debian unstable, makes sure that everything is running fairly smoothly, and adds a bit of pixie magic so it all looks shiny.  It's essentially the \"convenient Debian\".\n\nNow, I would like to have said that Kubuntu gets the same attention as (Gnome) Ubuntu.  Unfortunately, that's not quite true.  There are the occasional annoyances that make Kubuntu users feel like 2nd class citizens, but overall it is still a good choice for us KDE people.\n\nTalking of annoyances, one the major ones I can remember was bug #61946 in Kubuntu Edgy, where \"safely remove\" wasn't particularly safe and could easily lead to data loss.  You would think this sort of bug would have been rated \"top priority\", but unfortunately we had to wait six whole months -- till the next release -- before it was fixed!...\n\nThe Launchpad page for the bug #61946: (very interesting read)\nhttps://bugs.launchpad.net/ubuntu/+source/kdebase/+bug/61946"
    author: "Dario"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "Do you think the \"2nd class citizen\" feel is reducing in more recent releases of Kubuntu?  Are the *Buntu people starting to put as much effort into polishing the KDE version as they are the GNOME version?\n\nWith so many newbies attracted to Ubuntu, I wonder how many even know of the existence of the KDE variant? Perhaps things will chnage after a few releases of KDE 4 and things begin to stabilise.  It might become the more popular distro of the two..."
    author: "The Vicar"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "<i>Do you think the \"2nd class citizen\" feel is reducing in more recent releases of Kubuntu?</i>\n\nI would say so, yes.  Kubuntu Gutsy is looking pretty sweet...\n"
    author: "Dario"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-24
    body: "I've read that bug report that your provided a link to.  Yes, any data loss issue should always be of the highest priority in my opinion! At work I've dealt with commercial software vendors who don't seem to have that attitude, though.  :-("
    author: "The Vicar"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "This is an upstream bug from KDE (which we have now fixed).\n"
    author: "Jonathan Riddell"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "Hello Jonathan,\n\nAs per the complaints I raised in my original post, probably the most annoying for me is the default theme of Kubuntu.  I am serious when I say that it is painful for me to look at.  Perhaps I just have sensitive eyes but have other people complained about the \"blue on blue\" as well.  Do you have plans to change the theme to something less harsh when KDE 4 is released?\n\n\nWhy not use a version of Human on KDE?  I know that some people apparently find Human aesthetically unpleasing but given you use it on Ubuntu, would this not unify your branding?\n\nAlso, does the *Buntu community ever plan to release a two disk version?  It would make sense to have the basic distro, DE and OpenOffice on a disk (as you do currently) and then have an optional second disk based on the most popular downloads from the repository. This would help to showcase the most useful open source applications. It would help those of us without access to fast internet connections.  I happen to live in a first world country that still has poor network infrastructure in many regions so it is painfully slow to download software. If desktop Linux wants to gain a foothold in Third World nations also, this would undoubtedly help. If Mark Shuttleworth can ship people multiple install CDs free of charge, surely he could provide a second disk covering a wide range of useful applications too?\n\nI hope these suggestions are helpful."
    author: "The Vicar"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "personally I don't mind the blueness of the kubuntu theme itself (although I change it to green/purple when I get the chance).\nwhat concerns me is the blueness of the icon theme - a lot of the apps I use have icons that look more and more like little blue blobs. I have a small taskbar because I have a small screen, and sometimes it really is hard to tell them apart. konq, kontact and amarok are all essentially round and blue. kopete at least has a different shape, for now.\n\nmaybe the oxygen guys could keep this in mind when creating icons for kde4? :)"
    author: "Chani"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "Hello Riddel,\n\nIf you are reading this, I would like to second Vicar on this.\n\n(please don't take this as flame, its a sort of a rant because of how I get irritated with the looks of Kubuntu... please have patience and bear with me)\n\nPlease do try to take this as constructive criticism.\n\n[...]\n\nI've been using Kubuntu for ? 1 or 2 years now, it is a great distro, I like it a lot, but i have to say that I find it terrifyingly ugly.\n\nNot just normal ugly. *Very* ugly.\n\nI don't understand why it has to have its own art work, and not just have a consistent look with Ubuntu. Seriously. This looks like a typical case of NIH to me: that artwork was not invented here.\n\nFirst, I don't think that using an artwork invented elsewhere is a problem in any way.\n\nSecond, if you really want to roll on your own artwork, you should be able to dedicate enough resources to it, as to achieve a good result. Kubuntu looks, and color choices seem to me to be \"blue on bright white with blue, on the top of more blue\" hacked in a hurry with horrible results.\n\nKubuntu looks are bad within an hour, and dead horrible after a day's work.\n\n[...]\n\nRemoving all this blue is not easy just changing color theme and icons, don't solve it:\n\nthe tango icons set distributed with Ubuntu is not consistent with KDE, and too many icons are lacking.\n\nKopete will still have blue everywhere;\n\nKonqueror will still have blue on the Web Profile, no mater what...\n\nKSystemGuard will keep displaying a eye-hurting color sample despite the system colors...\n\nOh, well... you get the idea...\n\nSorry if I got carried away.\n\nKind regards,\nFrancisco\n"
    author: "Francisco Borges"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "This bug affected all Kubuntu users and many non-geek users I know of lost data. The problem is the time it took to fix it. It was only solved in the next stable Kubuntu release, six months later. That's way too long.\n\nIn my opinion, bugs that make people loose data should be fixed in the current stable distribution. Possibly via a dirty hack. A clean solution could then be provided in the next stable version.\n\n"
    author: "tuxo"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "www.mepis.org is a tweaked debian, and you can use original repos for updating the system.\n\nthey usually make CDs and DVDs"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "Excellent Kubuntu guys like Mark Shuttleworth and Jonathan Riddell make the brand one you can trust. I like the philosophy of basing Ubuntu/Kubuntu on a Free Software community, and using the Debian which is also community based as a foundation. Then they add commercial support services on top if you want them, without ramming them down your throat or offering inferior software for non paying users.\n\nIn my opinion, communities are the most important thing to consider in choosing software for the long term, and out of all the distributions I believe Ubuntu understand community dynamics the best."
    author: "Richard Dale"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "I would have answered in the same vein, only with worse wording. (K)Ubuntu (and Debian) being a community centered distribution that I can trust not to pull off some Microsoft patent deals and that puts independent contributors on the same level as Canonical employees (well, nearly) is something that I can rely to last for the near future at least, and do the right thing.\n\nMark Shuttleworth doesn't only understand Free Software, he also acts in a way that preserves the core values while broadening the target audience. While it's a bit unfortunate that Kubuntu is always falling behind the \"main\" distribution somehow, this doesn't change the fact that (K)Ubuntu can be trusted to do the right thing. For the foreseeable future, at least. And that's worth far more than the perfect KDE incarnation that other distros might offer."
    author: "Jakob Petsovits"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "opensuse is a community operation too.\nthe opensuse communtity made no patent deal with microsoft.\nnovell (sled) has a patent deal, but that's a gnome desktop.\n*buntu incorporates proprietary modules into the kernel by default (nvidia). But not everyone would think that's the right thing.\nAnd \"trust is more important than the perfect kde desktop\" is easy to say if you're happy using a gnome desktop.\n"
    author: "Stu"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-26
    body: "\"Fedora being a community centered distribution that I can trust not to pull off some Microsoft patent deals and that puts independent contributors on the same level as Red Hat employees (well, nearly) is something that I can rely to last for the near future at least, and do the right thing.\" - Sounds familiar? ;-)\n\n> Mark Shuttleworth doesn't only understand Free Software, he also acts in a way that preserves the core values\n\nHow's shipping the NVidia binary driver \"preserv[ing] the core values\" of Free Software?"
    author: "Kevin Kofler"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-29
    body: "Simply because you can choose to download a spin that doesn't use it and because the Ubuntu guys have said that this is an intermediary step until we reach wider availability of free drivers."
    author: "Anonimos Anonimos"
  - subject: "Re: What is the Appeal of Kubuntu? (or not)"
    date: 2007-10-25
    body: "Mmmm... for me, the appeal of Kubuntu was wealth of packages available in ?ubuntu repositories. But in the end - I needed latest compiled mplayer svn, ffmpeg svn, avidemux svn and shedload of othern latest svn/cvs software. For me .deb building is too much complicated. For building a software package one must go through hoops and loops to get it done on debian environment.\n<rant mode on>\nNow I run Archlinux. I do not have to get separate *-dev packages to compile stuff against some particular package. When I install glibc, for example - it is installed as a base package -, I get all development headers/libs, I do not have to get separate glibc-dev package. And making software package for Arch is much more simpler. All I have to do is write out some description about package - it is puerly informative - install steps: most cases ./configure && make -j3 && make install. Then I can install that package and if I need to update it with newer version I can be sure package management cleans up old version and installs new version. One can argue that it is not important to mage package of every little software but I find it easyer to maintain my system that way. Otherwise I could install Slackware and be done with it ;)\n<rant mode off>\nSo as you can see, package management is important for me. In Kubuntu it is OK as long as you use default repositories. Learning how to build your own .deb's can be painful.\nAbove is mention that most KOffice developers use Kubuntu as their choice of OS and reading my rant you might get a wrong idea. You can always install software to your ~username/<something>/<bin> but you do not share it system wide. I have done so with separate user for KDE4. All other users continue to use kde3.5.8 and only kde-devel user can use kde4."
    author: "arand"
  - subject: "Re: What is the Appeal of Kubuntu? (or not)"
    date: 2007-10-25
    body: "Have you tried checkinstall utility ? If not you should definitely give it a try !"
    author: "Anonymous"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "I don't really think there is an appeal.  Everyone one who has used it that I know, once trying opensuse, they move to opensuse, considering it to be a more finished, polished, and complete package.  Kubuntu lasted but an hour on my machine, I felt that it was a rather poorly made distro and went back to opensuse."
    author: "Richard"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "\"Could someone who uses it please explain the appeal of Kubuntu? Why do they prefer it over other distros?\"\n\nBecause it is free, I like Kubuntu, the disadvantage of the whole platform is \n- no pentium packages, just i386\n- slow startup\n- KDE integration of Firefox is bad\n- \n\nI agree that they need to spent more capacity on KDE.\n\n"
    author: "bull"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "Hmm, Kubuntu pioneered the System Settings replacement for KControl, which is being ported to KDE4. I generally like the distro, but here are the main problems I see:\n*Adept lags behind Synaptic, both interface-wise and feature-wise. Why couldn't they simply use the synaptic backend and provide a cute qt face?\n*Bulletproof X is unavailable in Kubuntu, since KDM doesn't have a failsafe server\n*Compared to distros like Opensuse, Kubuntu has less manpower which results in less available time for tweaking and polishing\n\nApart from that, it seems good enough."
    author: "Josh Riddley"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "I started using Ubuntu when it was at the Breezer version, because then it was starting to get hyped and I decided to give it a try.  I quickly got sick of Gnome, so I moved to Kubuntu.  But my experience is that Kubuntu gets worse and Worse.  When I upgraded to Gutsy (final release), the upgrader crashed before the cleanup (which by itself is unacceptable for a final release, but it wasn't the worst that happened).  This caused the packages evms* not to be uninstalled, as a consequence the new kernel (2.6.22-14) was extremely slow (actually it did not even boot until I reinstalled the kernel).  After googling a lot I discovered that I had to uninstall evms* which solved the problem.  The next problem was that the usplash screen did not appear and that tty1-6 were missing.  After googling a lot I discovered that I had to add fbcon and vesafb to /etc/initramfs-tools/modules and remove vesafb from /etc/modprobe.d/blacklist-framebuffer.  The next problem was that KDE seemed very slow, which was solved by uninstalling xserver-xgl.  The next problem (which I haven't solved yet) is that they removed support for .hidden files in Konqueror (in the previous versions of Kubuntu you could add filenames to the .hidden files, and then these files would not show up in Konqueror, making my Konqueror-view less cluttered).  If anyone knows how I can reactivate .hidden support, please tell.  This is only what happened during the last upgrade, the previous upgrades were not much better.  The only painless upgrade that I ever had, was the upgrade to Dapper.  I was also disappointed by the fact that the list of changes in Ubuntu was much larger than in Kubuntu.  The move to D3lphin in Kubuntu is also a horribly bad idea, it is very limited in comparison to Konqueror and to what Dolphin in KDE4 will be (according to the descriptions of Dolphin in KDE4 that I read on the web).\n\nWhat I would like to know is: what is the best KDE distro around there?"
    author: "anonymous"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "> What I would like to know is: what is the best KDE distro around there?\n\nMandriva ?\n\nWell, ofcourse, that's a mater of taste, and of hardware.\nMany would answer OpenSuse, other Mepis, Ark linux, or other I never heard about.\n\nAt least for me and my old Thinkpad R40, Mandriva is the best (kde) distro."
    author: "kollum"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "> What I would like to know is: what is the best KDE distro around there?\n\nhttp://www.kdedevelopers.org/node/3016"
    author: "Anonymous"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "It is simple. Really. \n\nI have been using Linux since a single floppy (i.e. in mid 1992). All that time, I have had NO real issues using it. But I have tried to get others to use it over the years. What I have found is that ALL Linux distros had about the same hardware support. So your gripe about hardware holds no water. But the real issue was that I found that others could not grasp how to install. Once it was installed, and set up they did great. But ask them to install a new app, and man, the issues would start. For the last 10 years, I have been helping my parents to move to Linux. My father likes it, but he finds all sorts of issues, that to me are trivial, but to him are difficult to overcome. Worse, he has a SMALL community to turn to; myself and now my brother. I started my father on mandrake, then moved him to Suse, and now we are on Kubuntu. Now that my twin is into Linux, he has tried to steer my father into other distros esp. ubuntu, but kubuntu is still the easiest for my father. Other than a tv card's IR setup (the one place that mandrake excelled at) AND a video card, he has had NO issues with hardware.\n\nQuite simply, Kubuntu is about the same ease of use as other distros due to KDE, but it makes distro and app install SO easy, that my father now makes heavy use of it. He still had windows for quicken, but for everything else, it is Linux. If the other distros will work on making install and setting easier, then they will win out.\n"
    author: "a.c."
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "\"What I have found is that ALL Linux distros had about the same hardware support. So your gripe about hardware holds no water.\"\n\n\nWell, it does on my box!  As I mentioned, I am in an area where I am restricted to dialup internet.  Fedora can recognise my USB external modem out of the box when I point kppp to the correct port but Kubuntu cannot.  I would have thought that the level of support for kernel drivers would be the same.  It must be a free driver or Fedora would not support it.  It has worked in Fedora since at least Core 3.  I probably should investigate that issue some more."
    author: "The Vicar"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "I've been a Kubuntu user for a year or so; had been a Mandrake/Mandriva user since about 2001, others before that.  Mandriva took a few stumbles, and caused me to look at Kubuntu.  Now it's on all my boxen.  Mandriva's done lots of good things lately, but I'm becoming accustomed to K's Debian way of doing things, and apt-get, etc.  That's very nice!  (Note to self to look at Mandriva again...)\n\nAlthough The Vicar needs more cd's due to his location, I love the live cd, and the one-install cd of Kubuntu.  I think the package selection on the cd works well for most folks.  Maybe not so if you need something different without a fast connection.\n\nI like the blue stuff and certainly find it better than Ubuntu brown, although I have more of a customized desktop look anyhow.\n\nKubuntu, and especially KDE, work for me.  (However, if Jonathan is still reading, I can't understand why the network applet on the taskbar does not see my wired or wireless connection, even though I'm networked fine..)\n\nLooking with anticipation to KDE4.   Keep up the great, great work, and Thanks!"
    author: "TimZ"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "One thing missing from the kubuntu version of the konq file manager is the 'open terminal' option right at the top of the tools menu. I use that frequently in opensuse, which is my kde distro of choice. Is there some extension that can be installed to add this feature? Does pressing F4 do the same thing? (I couldn't remember what the keyboard shortcut was, and since it's not it the menu....).The other thing that bugged me was that the kmenu was a mess compared to the way opensuse is organised. And there's numerous little things that just seem to be missing. Other than that, it's very user-friendly,everything works out of the box, loaded repositories, great forum support etc. Just leaves me wishing that with all the strong points *buntu has, that they couldn't put a bit more work into the k version. Some people, like myself, just hate the gnome desktop."
    author: "Stu"
  - subject: "Re: What is the Appeal of Kubuntu?"
    date: 2007-10-25
    body: "The bottom line is that amongst the developers who attend Akademy, Kubuntu seems to be the most popular choice.  OpenSuSE probably comes second, but gets bonus points for the healthy number of KDE developers employed by Novell.\n\n* It comes on one CD which is both a live CD and installable.  I think this is very important from the point of view of testing it and running it in the first place.  \n* Lots of software\n* Regular schedule and relatively painless upgrades, though I have yet to see a perfectly smooth upgrade which didn't break anything and which didn't risk leaving the machine in a completely messed up state if anything went wrong half-way through.  Both Edgy -> Feisty and Feisty -> Gutsy required some manual tweaks before the upgrade would proceed (mostly related to modified config files on disk) but the system came up with everything important working as required afterwards.\n\nFinally, one point I haven't seen mentioned elsewhere:\n\n* Kubuntu has a clear vision and goals.  I know what it stands for and I like where it is going.\n\nSupposedly we should all be running Fedora which aims to be good for developers.  Unfortunately it doesn't actually address any of the distribution-level issues which I care about better than any other distribution.  Having slightly newer versions of various libraries is not as important as some would like to think in this respect.  Usually I find that either the Kubuntu-supplied library is new enough or I need something bleeding edge which was released less than a week ago or is still unreleased in cvs/svn/git.    "
    author: "Robert Knight"
  - subject: "umm wrong facts?"
    date: 2007-10-25
    body: "\"making it the first distribution to release the 8th maintenance update to the KDE 3.5 branch.\"\n\nThis comment is false, it was released through opensuse the same day of the announcement from KDE"
    author: "Richard"
  - subject: "Re: umm wrong facts?"
    date: 2007-10-25
    body: ".. released as in the repositories (or whatever they're called in opensuse) or included in the day's opensuse new version which isn't mentioned? in kubuntu gutsy (at the time in freeze state if I recall correctly) all the packages were available and includable the very same day even very early - a great job, by the way. so yes, first distribution is correct."
    author: "andrea"
  - subject: "Re: umm wrong facts?"
    date: 2007-10-25
    body: "openSUSE's repositories include up-to-date packages and snapshots of quite a few projects. And the KDE packages - both 3 and 4 - are pretty up to date.\n\nSo I really don't care if the packages are out a few hours earlier and later - but they are there.\n\nIMHO openSUSE has made a great step forward. They had a very, very disappointing time (that was, coincidentally, the time when all the Ex-Gnome guys seemed to make decisions at Novell), resulting in a f**ked up release. \n\nBut with the current versions they seem to have re-gained their old strength, coupled with the advantages of a community supported distro - which includes the numerous repositories, official and unofficial, where you can find anything... "
    author: "Peter Thompson"
  - subject: "Kubuntu web site"
    date: 2007-10-25
    body: "First of all, I really like kubuntu distribution and use it for long time. Now a little bit of critics. First take a look at ubuntu.com. Then switch to kubuntu.org - do you feel the difference ? The latter looks much worse. Now scroll down a bit to see that funny photos in RC and BETA announcements. Yes, probably they are really funny for developers as they depict some well-known (for developers) faces and funny moments. But they are very purely done (non-sharp, under-exposured, not very good composition), purely (if ever) post-processed, and (the most important) they means nothing for non-developers ! It adds a very non-professional look to the whole site.\n\nBut the official website is really important and should look good. Just guess what an averange ubuntu user will do when someone told him about kubuntu and he wants to try it ? It is very likely that he will go to kubuntu.org ! Please, pay more attention to that site, its really important to get new users !\n\nAgain, thanks for everyone who work on this wonderfull distribution !"
    author: "Anonymous"
  - subject: "Re: Kubuntu web site"
    date: 2007-10-27
    body: "why would you post that here? this site is not kubuntu.org is it?"
    author: "mike"
  - subject: "Mandriva is the first distro with QtJambi?"
    date: 2007-10-29
    body: "\"Mandriva is the first distribution to include QtJambi [...]\"\n\nThis is not correct. Gentoo has qtjambi-4.3.0 in the Portage tree since 07 June 2007."
    author: "a Gentoo Linux user"
---
Within the past couple of weeks two Linux distributions came out with new releases, featuring the K Desktop Environment. On October 9th, <a href="http://www.mandriva.com">Mandriva Linux 2008</a> released their latest version to the masses. KDE 3.5.7 and Compiz Fusion 0.5.2 are just a couple of the updates with this <a href="http://club.mandriva.com/xwiki/bin/view/Main/2008_released">latest release</a>. Head on over and take a <a href="http://wiki.mandriva.com/en/Releases/Mandriva/2008.0/Tour">Mandriva Linux 2008 Tour</a>. On October 18th, <a href="http://www.kubuntu.org">Kubuntu 7.10</a> was released, marking its 6th major release. Kubuntu includes the last KDE 3.5.8, making it the first distribution to release the 8th maintenance update to the KDE 3.5 branch. There were many other updates in this <a href="http://kubuntu.org/announcements/7.10-release.php">latest release</a> as well.  Mandriva is the first distribution to include QtJambi, while Kubuntu adds Qyoto C# bindings.




<!--break-->
