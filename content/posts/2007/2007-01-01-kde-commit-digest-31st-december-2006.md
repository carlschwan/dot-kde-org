---
title: "KDE Commit-Digest for 31st December 2006"
date:    2007-01-01
authors:
  - "dallen"
slug:    kde-commit-digest-31st-december-2006
comments:
  - subject: "Thanks!"
    date: 2007-01-01
    body: "Thanks, and a happy new year!\n\n2007 will be a great year for KDE =)"
    author: "[Knuckles]"
  - subject: "Re: Thanks!"
    date: 2007-01-01
    body: "11:16pm local time and I just walked in the door from a restaurant and checked for the digest.\n\nIs it crazier that I'm checking right before New Year's or that Danny's release is like clockwork?\n"
    author: "Wade Olson"
  - subject: "Happy New year"
    date: 2007-01-01
    body: "Well done, KDE team! Looking forward to 2007!"
    author: "Roy Schestowitz"
  - subject: "KSysguard"
    date: 2007-01-01
    body: "Hey all,\n\n If anyone is interested, I have a screenshot for the new 'fill' for ksysguard:\nhttp://img139.imageshack.us/img139/3906/sensorload22oo5.png\n\n  (please no comments on text colour or svg.  The artist hasn't committed his changes just yet.)\n\n  I'm hoping that the semi-transparency will make it clearer that the lines are stacked on top of each other (so the height of the second line, is the value of the first sensor plus the value of the second sensor)"
    author: "John Tapsell"
  - subject: "Re: KSysguard"
    date: 2007-01-01
    body: "Nice :-)"
    author: "Anony Moose"
  - subject: "Re: KSysguard"
    date: 2007-01-01
    body: "Looks great! :)"
    author: "E"
  - subject: "Re: KSysguard"
    date: 2007-01-01
    body: "Are the colors blended in the lower regions."
    author: "Antonio"
  - subject: "Re: KSysguard"
    date: 2007-01-01
    body: "How do you expect to get the color yellow if you blend red with blue? :o)"
    author: "otherAC"
  - subject: "Re: KSysguard"
    date: 2007-01-02
    body: "I suppose now would be a time to point out that the color side of my brain sucks ;-) Also, apparently, the part that decides whether to put a question mark on the tail end of a sentence.\n\nAnyway, I think blending the colors would make it particularly evident that the graphs are stacked."
    author: "Antonio"
  - subject: "Re: KSysguard"
    date: 2007-01-02
    body: "IMHO it would look even better if the line separators b/n each graph is completely removed. There is no need to have a separator that says things like \"CPU Load\" when the graph is clearly labeled with the same wording. Just my 2 cents worth YMMV..."
    author: "DA"
  - subject: "Re: KSysguard"
    date: 2007-01-02
    body: "Hi John!\n\nThe screenshot looks great.\n\n- The semi-transparency does make it a lot clearer that the lines are stacked\n- I agree with above poster: No need to put a label above the graphs if the graphs already have they own labelling.\n- The CPU load graph needs a label 100% above the 80% one for the left axis.\n\nGreat work (and as the rumors go you just solved some bad instability issues? :)\n\n\nPS: The colours and the svg are alright, by the way :-)"
    author: "tobami"
  - subject: "Re: KSysguard"
    date: 2007-01-02
    body: "Hey, \n\n Thanks :-)\n\n  There are other ways to see the information though - for example there's a LCD-number type view etc.  Only the graph has the label.\n  Also it looks a bit strange not having the labels or anything.  Also what about when the graph isn't wide enough to be able to show the label in the small place?\n\n\n  Hmm, I'm not sure where exactly I'd put a 100% label.  It's already getting complicated to the layouting of the axis labels heh.\n  Maybe putting the labels outside of the graph would be better.  It would give me more room to do that if the instead of having the graphs in a 2x2 grid, they were in a 1x4 grid.  The I'd have horizontal room to have the labelling outside. Just a thought."
    author: "John Tapsell"
  - subject: "Re: KSysguard"
    date: 2007-01-02
    body: "Mmmm. Those are points worth exploring, the labelling inside the graph looks so much better though... \n\nBut yes, it isn't obvious where to put the 100% label. But being able to see 0% and 100% is more important than the other labels(20%,40%...) IMHO, as then you can quickly visually interpolate.\n\n How do other efforts solve the problem?. Let's see:\n\nEither they don't show labels on the axis at all, \n- Windows: http://www.worldstart.com/tips/screenshots/performance.jpg\n- OSX http://www.apple.com/server/macosx/images/cpuusage06212003.gif\n\nor they put them outside the graph, as you propose:\nhttp://static.flickr.com/34/91507931_92f7a939d7_m.jpg\n\nmore innovative approaches:\n- meter: http://www.widgetgallery.com/view.php?widget=36564\n\n- combo: http://www.apple.com/downloads/dashboard/status/qcpu.html\n\nI hope that inspires you :-)\n"
    author: "tobami"
  - subject: "KGet support for Metalinks"
    date: 2007-01-01
    body: "Also in KGet appears to be the beginning for support of the awesome Metalinks (http://metalinker.org). No more need for aria2; just insanely fast downloads straight through .metalink files in kget, hopefully. :)"
    author: "apokryphos"
  - subject: "Much kudos"
    date: 2007-01-01
    body: "to Martin Koller - this bugs in Konq were small but irritating.\n\nAnd of course for Danny Allen :)"
    author: "m."
  - subject: "some front-reports about 3.5 issue closure?"
    date: 2007-01-01
    body: "hello there,\n\nfirst i want to thank you for your outstanding efforts. thanks a lot for the last seven years of outstanding desktop experiences.\ncan someone make up a summary or assumptions on how many of the open issues for the 3.5 branch will be closed in 3.5.6? since all issues are in progress they could be closed through fixes. or might there follow a 3.6 and 3.7 branch?\nto put the question short: when will the 3.5 development be switched to maintenance status?"
    author: "engel"
  - subject: "Re: some front-reports about 3.5 issue closure?"
    date: 2007-01-01
    body: "3.5 development is in maintenace status since 3.5.0 ;-) Only that we are allowing \"small and well tested\" features in the 3.5 branch since KDE 4 is/was a bit far."
    author: "Albert Astals Cid"
  - subject: "Re: some front-reports about 3.5 issue closure?"
    date: 2007-01-01
    body: "There probably will be a 3.5.7 and 3.5.8, but not a 3.6 or 3.7 version of the kde3-series.\n"
    author: "otherAC"
  - subject: "So, finally we know kde 4.0 will be finish in 2007"
    date: 2007-01-02
    body: "This is a huge step forward... i was really starting to think that kde 4.0 was going to be released in 2008..."
    author: "Erne"
  - subject: "Re: So, finally we know kde 4.0 will be finish in "
    date: 2007-01-03
    body: "Well, you should not assume anything. KDE4 will be released when it is ready. Personally I hope it will be in 2007 but it's always better to release when it is done than to stress the release to get it out early."
    author: "Ephracis"
  - subject: "Re: So, finally we know kde 4.0 will be finish in "
    date: 2007-01-03
    body: "I'm not assuming anything. It is explicitly mentioned in the digest that kde4 is going to be out in 2007"
    author: "Erne"
  - subject: "Re: So, finally we know kde 4.0 will be finish in "
    date: 2007-01-03
    body: "also dont mix up KDE4 and KDE 4.0 : http://www.kdedevelopers.org/node/2600"
    author: "ReTyPE"
  - subject: "Re: So, finally we know kde 4.0 will be finish in "
    date: 2007-01-04
    body: "What I ask myself is:\n\nWhy Don't you ship \"KDE4 core\" early and KDE4 with all applications later.\n\nWho cares about Majong: these are just applications for the plattform."
    author: "gerd"
  - subject: "Re: So, finally we know kde 4.0 will be finish in "
    date: 2007-01-04
    body: "Then I really wonder why KDE 4.0 is not out now?\n\nI mean, do we really need \"KDE Games\" for a KDE 4.0 release?\n\nThe point is you first need a working and used plattform. That can be a plattform for few developers or a plattform for users which then start to ask: Oh, where is my favourite game? Let us port it from KDE 3.5.5.\n\nRelease early, release often. Plasma may follow later.\n\n"
    author: "gerd"
  - subject: "About the fonts"
    date: 2007-01-02
    body: "Thomas Zander says 'And most important, our fonts just look and print great now.', while I agree that this is an important goal, in the image that he linked(*), I disagree that the font look great.\nFor example, in the 'suscipit' word in the horizontal text,the 's' and the 'c' are too close to each other (kerning issue), and the inside of the closed loop in the 'e' letter is grey (this one is maybe because I'm watching the png on a LCD screen, I don't know).\n\n* at:\nhttp://commit-digest.org/issues/2006-12-31/files/koffice-text_runaround_thumb.png"
    author: "renoX"
  - subject: "Re: About the fonts"
    date: 2007-01-02
    body: "To clarify as it was may be a bit harsh, I don't mean that it looks ugly, it looks quite good, but 'great' no, I don't think so.\nAt least not in this png.\n\nBest regards,\nrenoX"
    author: "renoX"
  - subject: "Re: About the fonts"
    date: 2007-01-02
    body: "I agree, the font kerning in the image is not perfect at all.\nI got the idea that the image is placed in the wrong context, since it shows how text can be wrapped around images, not how the font kerning behaves..\n"
    author: "otherAC"
  - subject: "Re: About the fonts"
    date: 2007-01-02
    body: "Go and have a look to this screen shot, whitch show the same thing but with an other font :\nhttp://members.home.nl/zander/images/200612-Text%20runaround.png"
    author: "Kollum"
  - subject: "Re: About the fonts"
    date: 2007-01-02
    body: "Not better at all. Is it \"feu giat\" or \"feugiat\" to the right of the red arrow? Is it \"vero\" or \"ver o\"?"
    author: "ben"
  - subject: "Re: About the fonts"
    date: 2007-01-04
    body: "> Not better at all.\n\nLOL!\nThat's _exactly_ the same screenshot as this article supplies :)\n\nThe font issues you noted have been reported and should be fixes in time for the final release."
    author: "Thomas Zander"
---
In <a href="http://commit-digest.org/issues/2006-12-31/">this week's KDE Commit-Digest</a>: The KDE Commit-Digest 2006 retrospective. <a href="http://edu.kde.org/blinken/">blinKen</a> and KNetWalk become the latest applications to move to scalable graphics. KSquares further develops, with an AI player implemented. More maps and a more sophisticated divisions and capitals implementation in <a href="http://edu.kde.org/kgeography/">KGeography</a>. Support for password-protected RAR archives in the kio_rar interface. Work to support drag-and-drop of transfers in <a href="http://kget.sourceforge.net/">KGet</a>. Import of "koregressions" test suite for <a href="http://koffice.org/">KOffice</a>. Longstanding KWeather and <a href="http://www.khtml.info/">KHTML</a> bugs fixed. Major refactoring in the "sonnet" natural language checker. Version 1.0 of <a href="http://eigen.tuxfamily.org/">Eigen</a>, the library for vector and matrix math, is released.
<!--break-->
