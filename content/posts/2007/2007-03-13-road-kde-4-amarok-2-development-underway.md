---
title: "The Road to KDE 4: Amarok 2 Development is Underway"
date:    2007-03-13
authors:
  - "tunrau"
slug:    road-kde-4-amarok-2-development-underway
comments:
  - subject: "New GUI sucks"
    date: 2007-03-13
    body: "Isn't just ugly, It's unusable.\nEven the concept art look bad, I am quietly sure most user will prefer the OLD GUI style, because  it's simpler and more useful, BTW, the amarok 1.4 screenshot it's soooooooooooooooooooooooooooooooo manipulated, it's just don't look that way, maybe by default, and whit any cover, but I dosen't look that way for most user, common!"
    author: "Luis"
  - subject: "Re: New GUI sucks"
    date: 2007-03-13
    body: "The gui doesn't look right because it resembles itunes so well. Amarok was never such a clone. I really like the big playlist search field that the current amarok has.."
    author: "viseztrance"
  - subject: "Re: New GUI sucks"
    date: 2007-03-13
    body: "Mockups looks ugly. Don't make clone of something. Amarok now looks different, so why do you want to change it. Try to improve it."
    author: "No"
  - subject: "Re: New GUI is much better than the old one!"
    date: 2007-03-13
    body: "My opinion: the mockup is much much better than the older gui in 1.4.x. It is more tidy and more presentable. It only shows what you want to see when a playlist is active and playing. All those other features to manipulate, sort and so on are hidden in the background. It's simply beautiful! \n\nThe problem with my beloved KDE is that many applications are overloaded with controls and such. Keep it more simple and think about what the user normally wants to see on the screen! Enhanced editing and manipulation features can be hidden in the background. Think about it like \"tidyness like gnome\" but \"feature-rich and configurable like KDE!\" (so far for a brief excursion to KDE  in general)\n\nI am lookking forward to Amarok 2 and I hope it will look like the mockup that is presented in this article. Thanks for the amarok-team!"
    author: "Bert Speckels"
  - subject: "Re: New GUI is much better than the old one!"
    date: 2007-03-13
    body: "I second that!\n\nThe old GUI is soon @ the graveyard!\n"
    author: "David"
  - subject: "Re: New GUI is much better than the old one!"
    date: 2007-03-13
    body: "Just my opinion.\nThe current GUI is working, but it's not really informative.\n\nThe sidebar is annoying when looking at your playlist, and too small when seeking some information.\nNow, Amarok's more a database-viewer which can play music.\n\nIt should be the other way round, and this mockup seems to fulfill this."
    author: "Mo"
  - subject: "Re: New GUI sucks"
    date: 2007-03-13
    body: "Imagine seeing a construction wharf around a building, and people start screaming \"is that going to stay there? That sucks and besides the building looks ugly\". (hope construction wharf is the correct English translation).\n\nPlease understand a *mockup* is not a definitive version. It's a way for developers to show the new abilities of the program. The next step is polishing it so users also love it. So take part of this enthusiasm, don't demolish it (e.g. not providing good arguments) because people loose their motivation this way."
    author: "Diederik van der Boor"
  - subject: "Re: New GUI sucks"
    date: 2007-03-13
    body: "I think the UK-english word you want is scaffolding. "
    author: "Ben"
  - subject: "Re: New GUI sucks"
    date: 2007-03-13
    body: "Thanks, judging from Wikipedia and Google images it's exactly what I ment :)"
    author: "Diederik van der Boor"
  - subject: "You are a moron..."
    date: 2007-03-13
    body: "And I really hate to flame...\nBut...\n\n\"Also featured prominently in this screenshot is the middle pane. This pane is the focus of Amarok 2, as they try to give you more information about your current track, and allowing you to \"Rediscover Your Music\" as their motto goes. The leftmost column will function much as it used to, except with the \"Context\" information moved to the centre. Of course, as is KDE tradition, much of this will be configurable.\"\n\nRead... Please...\nDon't complain about what isn't done...\nDon't complain about what you can change...\nJust... don't complain when you can easily be proven wrong...\nOtherwise you look like and idiot, and a jerk.\nGR"
    author: "Gene"
  - subject: "Re: You are a moron..."
    date: 2007-03-13
    body: "Agreed.\n\nSo many whiners.\n\nThe instinct behind the change is right - just too many darn columns. Yes, you can configure them, but I often run out of horizontal space, and then I discover one I  hadn't used before, like \"last played\", ... No Room!\n\nThe challenge will be to keep good abilities to sort playlists and do tag editing, particularly bulk-retagging.\n\nBut I would be very surprised if the Amarok developers forgot that.\n\np.s. i used to use juk but initially came to amarok for reasons of stability, and stayed for the excellent features.\n"
    author: "JT"
  - subject: "Re: New GUI sucks"
    date: 2007-03-13
    body: "Well... As long as we can choose I don't mind having multiple options. But honestly, iTunes UI is very good, so it would pretty stupid to exclude it just because \"Amarok is not a clone\". Getting the best from other projects is not cloning, is non reinventing the wheel!"
    author: "Rico"
  - subject: "Re: I love it"
    date: 2007-03-13
    body: "Personally, I love the mock up. At the first look, it looks clean and simple and at the second look you discover all the useful features. It is not overloaded. Things must change and this is a good way. \n\nlokking foreward to KDE 4.0\n\ngreat work!!!"
    author: "jagga"
  - subject: "Re: New GUI sucks"
    date: 2007-03-14
    body: "Yes, it sucks, I agree. :(\n\nBut, listen, may be that's a global phenomena? I mean, look at the car design of today - the shapes are made so streamline, that they have become dull and ugly: huge fenders, sharp edges, really huuuge lights (both front and rear), etc. I, for example, don't want to have all the objects around me looking like a dzen garden. I like eye candy and ... well, I can't explain it. But for me \"new\" does not always mean \"better\". ;)"
    author: "Gruffy"
  - subject: "Re: New GUI sucks"
    date: 2007-03-14
    body: "i think its great. You are free to prefer the old guy."
    author: "benk"
  - subject: "No!"
    date: 2007-03-13
    body: "No no no. This mockup looks bad! Really bad.\nDon't try to invent a bike. Take current amarok gui, and try to improve it.\nThat's what we need, not some iTunes/WMP clone. Amarok now looks different, and that's good."
    author: "No"
  - subject: "lol?"
    date: 2007-03-13
    body: "It is the current GUI, but improved. The mockup looks nothing like, and is nothing like iTunes or WMP, so I spose your amazingly irrational response only serves to make you look foolish because fortunately you are wrong."
    author: "mxcl"
  - subject: "Re: lol?"
    date: 2007-03-13
    body: "Indeed.\n\nhttp://www.microsoft.com/windows/windowsmedia/images/international/windowsvista/v_album_view.jpg\nhttp://static.kdenews.org/content/the-road-to-kde-4/vol12_mockup.png\n\nThose two look alike?!?!?\n\nI'd pick the amarok mockup over anything, anytime. It's wonderfull, and imho the way to go for amarok 2.0!"
    author: "superstoned"
  - subject: "It Is the Current GUI Improved"
    date: 2007-03-14
    body: "I believe it is the natural progression of the old/current GUI.\n\nWinAmp, WMP, iTunes and Amarok each have a very unique look about them.  The problem is that content is buried in the current GUI.\n\nThe new GUI better frames the content.\n\nI may get flamed for this, but honestly I've always loved WMP9/10.  WMP11 makes for a pretty screenshot, but kills usability.\n\nThe layout for \"Now Playing\" is incredibly different from moving through your library and building your playlist.\n\nI think the solution is to follow this approach.\n\nWhen building/managing your playlist, something closer to the old GUI is better suited, because that is where the focus is.\n\nAs media is playing, the current GUI still gives you full control over navigation, while placing the central focus on the content that is actually playing.\n\nAmarok 2.0 looks to be the sexiest media player on the planet.\n\nKeep up the good work!"
    author: "T. J, Brumfield"
  - subject: "Amarok 2"
    date: 2007-03-13
    body: "Well, I think the new UI looks very nice. And since I have used Magnatune long before I discovered Amarok that is one feature I really like. The new version looks very interesting."
    author: "Bill Leeper"
  - subject: "Old fashion look"
    date: 2007-03-13
    body: "I really like the old fashion look Amarok 1.4.5 presents. It's very usable with playlist on the right side with headers under columns displying relevant information. I can simply find tracks sorting them by size, lenght, bitrate and especially type and directory. It's gaining power from it's simplicity, that's why people like Winamp UI, they don't have to think too much to find how to use GUI. To much sophistiated interface could overwhelm new users coming from Windows and MacOS camp. For the time being my female cousin Paula really like Amarok and she is not techncian. So, don't make another app for nerds and geeks, when you must go with learning curve to just play few songs.\nBest regards to all Amarok developers, you really do great job:)   \n\n"
    author: "Konrad"
  - subject: "Re: Old fashion look"
    date: 2007-03-13
    body: "I'm surprised, that you call the old playlist simple, and the new one sophisticated.\nI see just the other way round.\nI think I like the new interface. Why? Because I like to go through my collection. And I like the context tab. With the current amarok, I can't use them at the same time. With the new interface I can. I'm waiting to see it in practice.\n\nAnd the new playlist looks much cleaner and simpler. The \"old\" one is very \"technical\". And although I'm a technician, I like amarok mostly for the reason it hides most of the technique. This helps me to focus on the music, thus \"rediscover my music\" :-)\n\nIt's strange to see so many people crying because of the new interface. I think it's mostly an automatic reflex, caused by the fear of changes.\n"
    author: "Birdy"
  - subject: "Re: Old fashion look"
    date: 2007-03-13
    body: "Both comments are true :\n- I want to have a quick view on every bitrates in a playlist, on year and so on. In the mockup design, the new playlist is rather useless (the rating thing is quite useless : if you don't like a track, just delete it).\n\n- It was unpleasant to switch between \"collection\" and \"context\" panels. I am pleased it was noticed, and it is being addressed in next version.\n\nNow is there really no way to keep collection, context and playlist visible in the same time?? I definitely think there is, and the current mockup is not addressing this.\n\n"
    author: "David"
  - subject: "Re: Old fashion look"
    date: 2007-03-14
    body: "The old playlist is simple.  You have a database in a familiar and easily-parsed format with metadata in columns that can be arranged at the user's pleasure in any manner desired.  If you want ANY particular piece of that metadata instantly, you begin typing and it narrows itself down to what you want automatically.  No questions asked- it just works.  Queue up what you were looking for and go about your merry way; it will go back to your playlist or random play when it's done.\n\nRediscovering my music doesn't necessitate knowing who made it and what the album art was- I know who the artists and composers of most of the pieces in my collection are already, and can look at the appropriate column if I've forgotten.  This new interface seems to keep me from my arbitrary metadata in a most unpleasant way and makes-more-prominent information I could easily find by looking in wikipedia, either in Amarok itself or in a browser, in the event that I was curious (for the artists that actually have articles- quite a few don't in my case)\n\nI understand that it will be configurable, but I truly don't understand the rationale of this design decision and feel that it can only harm one of the shining stars of the open-source community.  Amarok has been almost unique in it's ability to allow me to manage a playlist of 4-5000 items as easily as one with 20-40 with only the metadata already in my files.  The last player that did that effectively was dBpowerAMP on Windows (though I have functionality issues with it, after having used Amarok).\n\nI think could actually go on at greater length about my issues with what I've seen so far, but if what I say here sees favourable response it may be a point to take up with the developers directly."
    author: "Wyatt"
  - subject: "Re: Old fashion look"
    date: 2007-03-14
    body: "Agreed.  What they're essentially doing is putting the spotlight on the context browser, instead of the actual music that's being played.\n\nWhile the context browser is a nice feature, I have quite a lot of obscure music and often what's displayed in that bar to the left is no more than what's written in the playlist anyway.\n\nI use Amarok to listen to music, not check out album art, artist details, and so on - that's what Wikipedia's for.  Keep the focus on the music please, not its metadata."
    author: "Tom"
  - subject: "Usability"
    date: 2007-03-14
    body: "I greatly disagree.\n\nThe new look is cleaner.  You still have navigation where you naturally expect it to be, on the sides.  The center is best suited to focus on what you have selected/playing.\n\nWMP took a huge step backwards in usability simply to look better at a glance, or in a screenshot with WMP11.\n\nI think anyone migrating from WMP11 to Amarok 2 will find it much easier and more intuitive.  As they also learn that it is more powerful, they will only fall in love that much more."
    author: "T. J, Brumfield"
  - subject: "Re: Old fashion look"
    date: 2007-03-18
    body: "Columns are useful, as you say. for sorting by certain criteria.  People know about clicking on column headers to sort from file managers.  There is no reason to ditch them.\n\nNot that I care what Amarok does with columns: I'll never be able to use it due to the cluttered GUI from hell (this from someone who thinks Konqueror's default is just fine and doesn't need to be pared down)."
    author: "MamiyaOtaru"
  - subject: "Re: Old fashion look"
    date: 2007-03-18
    body: "Oh, I do like the new mockup, as it eases back on the clutter (though I'm not sold on artist info in the middle so much, I have too many untagged tracks to care).  I might actually use Amarok if it looks like that, which is why I decry loss of columns.  Something that looks like the mockup, *with columns* would attract me."
    author: "MamiyaOtaru"
  - subject: "Great work!"
    date: 2007-03-13
    body: "Developing this fast is impressive. In this very early state, it's already looking very good. And also quite innovative, I like that. Ofcourse I can't say anything about usability yet. But I'll trust that is in save hands by the Amarok team :)"
    author: "frodoontop"
  - subject: "Re: Great work!"
    date: 2007-03-13
    body: "Thank you for your trust. Thats all we can ask for at this point. :)"
    author: "Ian Monroe"
  - subject: "yeah right.."
    date: 2007-03-13
    body: "isn't that what Hitler asked for in 1933? And look what it worked out to in 1939!\n...\nehh, just kidding there. I don't use amarok (except for fetching podcasts) right now because I don't really like the interface.. I mean I haven't had a closer look and all, but it somehow deters me from trying.. but that is only out of irrational reasoning. Anyways, the problem I recognize in the first screenshot that the filename does not seem to count as a valued information resource here, which is wrong as it - atleast in my collection - contains the most correct information in many cases. I also want to be able to tag files without changing a bit of them; what I am not so sure about is where to save those tags.. I'd certainly like an implementation independent from available database backends or filesystem attributes, f.e. XML metadata files stored either in the same directory or a directory above, that would allow transfering a folder hierarchy to a different place or using the same metadata from linux and windows...\nanyways, thx for your hard work up to know and for any outstanding. And, let's rock them windows system hard, too ;=====P"
    author: "eMPee"
  - subject: "Re: yeah right.."
    date: 2007-03-14
    body: " >  isn't that what Hitler asked for in 1933? And look what it worked out to in 1939!\n >...\n> ehh, just kidding there.\n\nthat's gotta be the lamest joke ever."
    author: "Patcito"
  - subject: "new GUI"
    date: 2007-03-13
    body: "I really love the new GUI (Mockup).thats really usable.I think the current Context Browser has a big problem: Its tabs.I hate those tabs (Music/Lyrics/Wikipedia).everytime im in one of them, I miss the others.I really want to see them all merged.(A brief wikipedia text and image that could be extended by a collapsing 'Hide'Show' link + lyrics in a HTML Frame (should have scroll bars) + Similliar artists/Favourite albums and... below that)\n\nbut since everyone wants his own things, i was thinking about Templating that (think about Smarty in PHP, something simmiliar that everyone creates his own 'Contex Bar Themes'.not only CSS)"
    author: "Emil Sedgh"
  - subject: "Re: new GUI"
    date: 2007-03-13
    body: "@ all flamebaits around :\n1) Don't flame developpers or do it yourselves\n2) Don't flame developpers on less that a month of _free_ time work, or prove that you can do better (you're not linus :D)\n3) respect people, at least, if you don't like the way they are going say it with respect.\n\nBy the way, I liked the parent idea much. \n@devel-team :\n- continue your great work. I like it. \n- have a look to what kdevelop4 team is working on for GUI\n\n(ps : english is far from my natural language, sorry)"
    author: "sebastien"
  - subject: "The New UI"
    date: 2007-03-13
    body: "Please don't make the new UI the default. Its very cumbersome, and non intuitive. The current UI doesn't have much quirks imho, maybe you can just fold the tabs together or something like that - but definitely, not this way."
    author: "Sarath"
  - subject: "Re: The New UI"
    date: 2007-03-13
    body: "How can you know it isn't usable if you haven't used it?"
    author: "Seb Ruiz"
  - subject: "Re: The New UI"
    date: 2007-03-13
    body: "I guess he tried to click on the controls in the mockup :o)\n"
    author: "whatever noticed"
  - subject: "Re: The New UI"
    date: 2007-04-17
    body: "Well, maybe I use cvs..."
    author: "Luis"
  - subject: "tag cloud"
    date: 2007-03-13
    body: "Maybe a good idea to have an option to display a tag cloud (like on Flickr) on the middle pane to select music. Another comment I would like to make: Why not use (dynamic) treeviews more to cut down on the number of tabs.\n\nKeep up the good work!"
    author: "cossidhon"
  - subject: "Keep going..."
    date: 2007-03-13
    body: "Keep going where you're going, amarok devs. Regardless of people's disapproval (at least until you're finished). You've created good things in the past, and I hope everyone can trust you to do so again. "
    author: "Steve"
  - subject: "CoverFlow"
    date: 2007-03-13
    body: "Zack Rusin has written a CoverFlow clone for Qt4. If you Amarok developers want to integrate that:\nhttp://zrusin.blogspot.com/2007/03/reflections.html\n\nLike some other people, I'm not that excited about the new playlist."
    author: "KAMiKAZOW"
  - subject: "Re: CoverFlow"
    date: 2007-03-14
    body: "Actually... We were discussing this a little sporadically last night, and we actually came up with an idea that might work with this... None of us have ever really thought of CoverFlow as something that's useful - pretty yes, but not useful really... So... How about a CD Stack in stead, which looks like CoverFlow, but where the CD that's focused is shown with all the tracks listed underneath in a list?\n\nThe reasoning behind this is fairly simple - we don't have the horizontal space to show the CoverFlow in any pleasant way, however we have a lot of vertical space... So a stack of CDs, where one is shown as open with a track list suddenly flopped into my mind, i suggested it and well... The few people who were looking at the channel at that moment liked the idea.\n\ni'm just tossing it out here to show that we're not ignoring people and that we're really thinking about the usefulness of any eye-candy we include too ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "nice job"
    date: 2007-03-13
    body: "I like the look of the mockup. Really, I don't care if it's inspired by something else, and I think complaining about it is pointless. It displays all the information the traditional playlist but in a more elegant and compact fashion. Keep up the great work!"
    author: "chris"
  - subject: "I like it but..."
    date: 2007-03-13
    body: "I really like the new Amarok but... I'm much more focusing on my collection than on the context browser. When the Amarok window is shown on my desktop, it's because I want to change the content of my playlist. Otherwise it's closed in the tray. I really think you're focusing on the wrong thing. Of course the context browser is really usefull and I use it, but not as much as the collection pane, which is, let's be honest, crap... I like to go through my collection the same way as I go through my shelves... looking at the covers. I must admit, even if I'm going to risk my life ;-), that the WMP Collection Manager is really well done, usefull, easy to use... At least it shows the covers in a configurable size next to the tracks. And that's the only thing I'm looking at, I don't want to bother reading the tags if I'm not looking for a song in particular. And it shows the track number and the length of the songs as well, in case I need them, which is not even implemented in Amarok. Every back cover of a CD has that information but not the collection manager, that's... weird isn't it ? The TreeView widget is definitely not a good choice for displaying a collection IMO.\n\n\nSo please focus on the collection manager, and make it more useable. Make it feel like wood... as a normal shelf, with real CD's on it."
    author: "Cypher"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "I fully agree.\nYou said everything.\n\nThe two most important things are the collection browser and the playlist.\nI want to browse my collection with covers, track number, album year...\nI want to \"see\" my playlist with such rich informations as well (the mockup is a start in that direction but need a lot of polishments to make it useful, compact and beautiful)."
    author: "S\u00e9bastien Lao\u00fbt"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "i agree fully.\n\n1. there's nothing wrong in comparing and borrowing features from other media players. ignoring good ideas just because it's itunes or wmp is plain stupid.\n\n2. some people focus mostly on their playlists, sorting and arranging them. other focus mostly on their collection, navigating it looking for new stuff to play. i'm in the second camp, but that's not really the point. the point is:\n\namarok 2.0 should allow the user to \"set up\" their inerface the way they want (within limits). so please, allow us to completely hide or resize any view we choose. that way the user can control the focus.\n\nthanks\n\n"
    author: "helmudt"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "Your last sentence is gold.\n\nOne of the things that's always annoyed me about Amarok compared to other KDE applications is the lack of customisability. Why can't you just allow users to decide how big the playlist/collection browser/context menu is?\nWhy can't you make the old playlist an option?\nWhy can't you allow things like the track status scroller to fit the whole width of the screen?\n\nI just don't see why you can't see that different users want different things, and rather than forcing everyone down one path, it makes sense to allow people to change their player.\n\nPersonally, I frequently use playlists of hundreds of tracks and I can't see ANY way the 2.0 mockup will handle this.\n\nIs any actual usability analysis being done of the interface or are the developers just storming ahead with what they think? \n\nLittle hint, want to make a better interface? Consult HCI specialists. THAT IS THEIR JOB!"
    author: "Mike Arthur"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "Man, what EXACTLY are you whining about?  The article has mockups, not actual screenshots of the new software, so you don't know what you're going to get (yet).  And it clearly says that much of the interface will be configurable, but here you are sounding like a crying baby that had his candy taken away!  And over free software at that!  \n\nIt's one thing to ask for features, but you are practically insulting the developers.  \n\nHere's a clue for you - if you get the new Amarok and it ends up being something you don't like...downgrade.  \n\n"
    author: "James"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "Yes. The collection browser has been the thing that truly annoyed me about Amarok from the very start. Take a look at the competition:\nWinamp, Itunes, Songbird and others. \n\nTime and time again the original(winamp) is copied. Why? Because the multi-pane view separating artist album (and optionally genre) from songs is pretty much the ultimate way to quickly and flexibly find what you are looking for.\n\nI type \"someword\" in the filter box on top, and I get this view:\n\nsomeword\n---------------------------\nartist1       |      album1            \nartist2       |      album2\n---------------------------\nsong1 \nsong2\n\nany artist album or song that matches someword is displayed. \nany song that has a matching artist or album is displayed.\nIf i select an artist or album the song list, and all other lists, is further filtered by that criteria.\n\nThis is simply orders of magnitude faster and more convenient than the Amarok way of browsing your collection via a tree structure.I've been using amarok for years now, and I'm still frustrated by this on a daily basis. Amarok is superior to winamp in every other way I can think of, but this single feature would have me using winamp instead of Amarok if it only worked right under wine.\n\nI'm begging here; please bring this functionality to amarok, it would make a world of difference. \n\nAnd of course, since I'm already begging favors, make it ultimately configurable so that the user can set it up with any filter criteria they wish in any GUI layout they wish.With the flexibility of QT interfaces(drag and drop interface parts), what's stopping you from making it so configurable that users could make the filtering look like this if they choose?\n\nsomewords\n--------------------\nalbum1 | artist1\nalbum2 ! artist2\n--------------------\ngenre1 | year1\ngenre2 | year2\n--------------------\nsong1\nsong2\n\nUser/Newbie friendly defaults, and ultimate configurability. That's what Linux should be all about in my opinion. Please bring Amarok closer to that ideal!"
    author: "Smoked"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "Try the flat view.  Its been in Amarok forever and does exactly what you are asking."
    author: "Dan"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "No it doesn't. It comes nowhere near to providing the functionality common to winamp, itunes and songbird. If you think it is comparable, I can only assume that you have never really used any of those applications.\n\nAmarok is great in many ways, but claiming that a flat single-filter-view is the same as a very flexible multiple-step hierarchical-filter-view is just factually wrong.\n\nPlease try one of the mentioned programs out. You should get the idea almost immediately. You get the songs you want into the playlist in a fraction of the time using a fraction of the number of clicks. Not to mention that the overview is far superior to either a flat view or a tree view."
    author: "Smoked"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "The drawback of the \"multiple-step hierarchical-filter-view\" is, that it takes away quite a lot of sceenspace.\nFor just searching names of artists/albums/songnames, the flatview is quite nice."
    author: "Birdy"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "Yes, it does take a bit of screenspace if you use the old way of displaying lists. However, the new playlist is an example of an excellent way to remedy this. Using that type of view for the file/album/artists lists in the browser you would not need to use more horizontal space than the current flat view requires to show artist | album | song. If you take a look at Winamp you'll find that it manages to do exactly this. The current interface is very wasteful of horizontal screen real estate, there are heaps of white space around. \n\nAmarok 2 appears poised to fix that! :) "
    author: "Smoked"
  - subject: "Re: I like it but..."
    date: 2007-03-13
    body: "Expanding on what I said in the first reply:\n\nWhat puts this type of filter view head and shoulders above either the flat view is that it combines all the practical advantages of both those views into one view. In fact, it actually manages to combine the advantages of any tree arrangement of the filters in question.\n\nTo understand what I'm talking about; Consider having the tree view Artist->Album->Song:\n\nTo select a song you need three clicks.\nTo see any song you need two clicks.\nTo select an album you need two clicks.\nTo see any album you need one click.\nOnly artist is directly accessible and visible without clicks.\n\nThe flat view has direct access to individual songs, but complete albums and artists are a hassle select. \n\nThe multi pane view on the other hand allows selection of any of these with one click, and they are all visible for excellent overview. This is the great advantage to this view.\n\nHaving used all these views quite a bit, I consider the multi pane view to be vastly superior to either of the other views. I am not at all surprised that the most used media players in the world use that layout. I am surprised that Amarok does not."
    author: "Smoked"
  - subject: "Re: I like it but... (file type browse/filter)"
    date: 2007-06-28
    body: "My collection is both flac (for home listening) and mp3 (for mobile listening). I therefore would like to browse/filter according to file type. Does anyone have any clever ways of dealing with this? Currently, I populate a playlist, then sort according to type, then select and remove from playlist. Not very clever, indeed! "
    author: "Alan"
  - subject: "Re: I like it but... (file type browse/filter)"
    date: 2007-06-28
    body: "Try \"type:flac\" or \"type:mp3\" in the search bar. Works like a charm!\nI like amarok more and more every day! Thanks."
    author: "Alan"
  - subject: "Looking good!"
    date: 2007-03-13
    body: "I really like the mockup. I've always been annoyed with having  a 1440px widescreen and still not being able to see all the information I'd like to (I always have to resize the left pane if I use it), so I really like the way you guys are heading :)\n\nI do hope that the playlist will still be one-click sortable on for example artist or rating, since I didn't see that functionality in the mockup and I kind of use that a lot.\n\nI also agree with Emil Sedgh that tabs in the new middle pane should be avoided at all costs, I hope it'll be possible to present all the information in one view without cluttering the pane up too much.\n\nAgain, great work (and great article, too)!"
    author: "bsander"
  - subject: "Re: Looking good!"
    date: 2007-03-13
    body: "The tabs in the context view are a leftover from 1.x. We do intend to get rid of them :)"
    author: "Mark Kretschmann"
  - subject: "Re: Looking good!"
    date: 2007-03-13
    body: "The way I envisaged the playlist, you would still be able to sort by any data you  like. Prolly a button or right click. We'll find out during implementation I expect."
    author: "mxcl"
  - subject: "Re: Looking good!"
    date: 2007-03-13
    body: "great! I think this interface is really the way to go. \nFirst, because the context browser wasn't wide enough (esp for long-line lyrics and wikipedia), so you had to resize it - but then it would be too wide for the collection/playlist/etc browsers. Seperating those is a good thing.\nSecond, screens are going to be widescreen. And this new design takes advantage of that!!!\n\nAnd the compact playlist view - lovely..."
    author: "superstoned"
  - subject: "Re: Looking good!"
    date: 2007-03-13
    body: "but please remember that some of us still have small screens! I assume I'll be able to configure amarok so that it's not too cluttered on my poor tiny laptop - nearly every program I use has to be fullscreen and still feels too small."
    author: "Chani"
  - subject: "Re: Looking good!"
    date: 2007-03-14
    body: "Well... How about a horizontal list of the sort order items, which you could then drag around to define in which order you want them to be sorted. To define ascending or descending order, you could simply do as now and click on each item to toggle that."
    author: "Dan Leinir Turthra Jensen"
  - subject: "What about podcasts?"
    date: 2007-03-13
    body: "I'd like a little more supprt for podcasts. Now it is still a pain in the ass. You have to scroll down the playlists to the podcasts, refresh the podcasts and download them - then you can hear to them or transfer the files to your mp3-player.\n\nWould it be possible to do it a little more like mypodder from podcastready.com? Every time I connect my MP3-Player the programm loads the most recent - or maybe the three most recent - podcast episodes to the device. "
    author: "Torsten"
  - subject: "Re: What about podcasts?"
    date: 2007-03-13
    body: "Well, works for me in amarok 1.4.5... It automatically downloads the podcasts, and put's new podcasts in the que when I connect my audioplayer... Check your settings."
    author: "superstoned"
  - subject: "Re: What about podcasts?"
    date: 2007-03-13
    body: "I'm planning to use the center pane (now called the context view) for detailed podcast info. In 1.4 this was not possible because the browsers where in the same place.\nI have used only Amarok for uploading my podcast to my phone for a long time now. I'm no longer convinced that using a GUI program from one single computer is the best way. http://commonideas.blogspot.com/2006/12/podcast-appliance.html might offer a easier way for transferring podcasts to your MP3-player. Guess that behaves a little like mypodder. There is however a possibility to integrate this with Amarok 2.\n"
    author: "Bart Cerneels"
  - subject: "Drag and Drop"
    date: 2007-03-13
    body: "I do like the the look of the proposed interface, however, as shown, there is one obvious showstopper for me. When populating the Playlist from the Collection panel, you will have to drag across the central panel. This is much worse than the present arrangement.  "
    author: "Stuart Neill"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "This is indeed a problem that we're discussing. So far we don't have a good solution. Some ideas:\n\n* Allow dropping on the context view (adds to playlist)\n* When starting a drag, extend the playlist view to the left\n* Rely on double clicking / context menu\n\n\nAny ideas are welcome :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "One more for you're list :)\n\n* Move the playlist to the center and put the context view on the side. Really you should be able to reorganise them as you want though.\n\nI really think you shouldn't have the playlist window snapping left and right while you add stuff, but doubleclicking to add to a playlist is fine."
    author: "Ben"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "Nope, that's a no-go. I'm convinced that the context view needs to be in the center. It's the context information that we want to emphasize in 2.0. The playlist is just a tool.\n\nAlso it simply wouldn't look as good."
    author: "Mark Kretschmann"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "Indeed. This problem of drag'n'drop was the first thing I thought about when I saw your mockup a few weeks ago. And, like you guys, I have no idea how to solve it, but I agree moving it to the left is NOT the way to go."
    author: "superstoned"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "I'm sorry but I think you're getting it wrong... For you, the context is more important, but I'm not sure (no source) it's the case for a majority... Programs are made for end users, arent't they ? For instance, having the context as a toolbox, the way it is now, is perfect, I don't want to get my screen covered by information I don't even read or use most of the time.\n\nSo please think about it, the playlist is more than a tool... well it is the central part of a player actually."
    author: "Cypher"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "I'm sorry but I think you're getting it wrong... For you, the playlist is more important, but I'm not sure (no source) it's the case for a majority... Programs are made for end users, arent't they ? For instance, having the playlist as a toolbox, the way it is now, is perfect, I don't want to get my screen covered by information I don't even read or use most of the time.\n\nSo please think about it, the context is more than a tool... well it is the central part of a player actually.\n\n-----\n\nSee a pattern? :) One word: customizability (within limits of course)"
    author: "helmudt"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: ">  I'm convinced that the context view needs to be in the center. \n> It's the context information that we want to emphasize in 2.0.\n\nWhy?  Something which Troy's article does not discuss in depth is the rationale for this direction.\n\nFrom my perspective, the 'contextual information' is an interesting extra to peruse if I need a distraction.  The core functionality of Amarok is an audio player, the essential tasks of which I see as:\n\nA) Music selection  \nB) Playback\n\n(A) means finding music to play and choosing the order in which it is played.  (B) means actually playing the audio\n\n> The playlist is just a tool.\n\nI disagree.  I think the playlist is an essential part of task (A) relating to selection and playback order of the music.\n\n"
    author: "Robert Knight"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "I never use the context view. It contains information I never need or want to see. The only thing I ever do with the Amarok GUI is move songs between my collection and playlist(s). I'm fine with the layout in the mockup being the default, but if there's no way to configure 2.0 to look and behave like 1.x, I will be very annoyed and go find something else to play my music."
    author: "Noah"
  - subject: "Re: Drag and Drop"
    date: 2007-03-14
    body: "> if there's no way to configure 2.0 to look and behave like 1.x, I will be very annoyed and go find something else to play my music.\n\nlol well do so, go find another player then. Do you think amarok devs are gonna lose some market share or that amarok share holders are gonna sell their stocks all in one? "
    author: "Patcito"
  - subject: "Re: Drag and Drop"
    date: 2007-03-19
    body: "That was a counterpoint, not a threat. Amarok is a product meant to be used by other people, and so the developers should at least consider the opinions of those people before making radical, non-configurable changes.\n\nAlso, why is it that so many people push others to use Free Software, and then rather than offer assistance when somebody encounters something they don't like, tell them to go away? It's incredibly irritating and a great way to drive people away from all this cool software we've got."
    author: "Noah"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "I have to agree with the other responses. The context view is an interesting distraction, but it's hardly the central feature of Amarok. After I've found something I want from my collection, Amarok goes in the tray unless I want to find the lyrics. Except for the vertical text on the tabs, I like the current interface quite a lot.\n\nPersonally, I'd like to see improved collection management: improve mass-tagging features, and make Amarok a full MusicBrainz client with an interface that doesn't suck (unlike Picard and the original MB Tagger)."
    author: "Till Eulenspiegel"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "\"The context view is an interesting distraction, but it's hardly the central feature of Amarok.\"\n\nThat depends on what you use Amarok for. Since the context info matured with support for last.fm, I use Amarok as much for find and exploring new music as for listening to the music that I already own. The context view is very much the central feature in this usage scenario.\n\nI think the answer is configurability. QT supports dragging and dropping interface elements to different positions. That seems like the perfect solution to this problem to me.It should make both of us happy.\n\n"
    author: "Smoked"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "\"Since the context info matured with support for last.fm, I use Amarok as much for find and exploring new music as for listening to the music that I already own.\"\n\nTrue; if it were a little better, I think I'd use it to (eg, I want to see tags for the album + artist in the context view, not just the track).\n\n\"I think the answer is configurability.\"\n\nI don't know. Configurability as an excuse for bad defaults is no good. As I said, I kind of like the current system of tabs for the left side. If the context pane is going to become another way of exploring your collection, it should be either/or, not both on the screen at the same time.\n\nAnyway, I'm sure this will get worked out in the end, after there are some betas to comment on."
    author: "Till Eulenspiegel"
  - subject: "Re: Drag and Drop"
    date: 2007-03-19
    body: "> Configurability as an excuse for bad defaults is no good.\n\nAnd good defaults as an excuse for lack of configurability is even worse. That's one of the big reasons that many of us, myself included, use KDE and not GNOME.\n\n"
    author: "Noah"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "I do not agree.\n\nHaving the playlist in the centre is not the \"ultimate\" solution, but it solves the drag'n'drop problem much better than the other proposals.\n\nTo be honest, I really disliked the mockup the first it. It is just too.. different.\nBut now, I think it looks fairly good, and has lots of potential.\n\nSomeone mentioned coverflow, and I think it would be a great addition to the collection browser; not only eye candy, I think it is a very logical way to browse your collection. My thought is to have a horizontal coverflow in the collection:\n[]\n[_]\n[__]\n[_]\n[]\n(Something like that)\nYou can, of course, add the album with drag'n'drop. A list at the top/bottom shows the 'current' album's information (the album in the middle), like Year, Artist etc., and also the tracks. Of course drag'n'drop would work here too.\n\nOK, there are many problems here, but something like this would be really cool."
    author: "Mogger"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "What about having a basket/button in the left pannel allowing you to drag there the collection?"
    author: "Daniel G\u00f3mez"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "This solution has also been discussed. The drawback is that you it would only add to the end of the playlist and you would loose control of where the new content was inserted"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "But that is the case of allowing drop on context or double clicking too.\n\nThat could be a shortcut, and if you want more control you drag all over the window."
    author: "Daniel G\u00f3mez"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "If you really wont be moved in terms of the context not being in the centre then I would say that allowing to drop on the context menu is the best solution.\n\nHow I am imagining this works is that when you drag outside the confines of the left panel then a line will extend across the central panel with an arrow pointing the position within the playlist where the tracks will be dropped."
    author: "Matt"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "I think the last point is a good idea. Kind of allowing the drop on the context panel, but interpreting its position like a drop in the playlist. (Of course a good visualization for the position in the playlist is needed.)\n\nBeside that: I think that a separate Contextpanel is a very good idea and I would love it centered, but it might be a good idea to make the positions of the three views customisable.\n\nAnd last: Your work is great!"
    author: "Sebastian"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "That seems like it would make the most sense in terms of dragging from the play list, but one of the things that could be awesome is dragging things like tags out of the context pane that bring associated songs with them. Do they get lines into the playlist as well? I feel like that could get really confusing."
    author: "Ryan"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "I was thinking the central pane should be a special drop target, it doesn't look like it should normally receive drops as its just an info pane. Drags from it are a different matter, they should work as normal so the tags thing you mention would be fine. My arrow suggestion was for dropping stuff from left onto right (or right onto left) using the central pane as a special case drop target. If you dragged from the left to inside the boundaries of the right pane the arrow should disappear, its only while dropping on the central pane it should be visible. "
    author: "Matt"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "What about moving the playlist or the content to the top, and leaving a large square-ish area for the content? This would make the drag and drop easier (it wouldn't have to cross the whole content), while leaving the content as the main area."
    author: "Pablo Barros"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "How about both collection and playlist on the left, and context on right (occupying center and right columns in current mockup).  This takes away space from the collection/playlist, but that can be countered by automatic resizing of the left column.  Split the space for collection and playlist 50/50 when mouse not over either, but give majority (80%?) to one when moused over.\n\nDrag to playlist flow:\n1. Collection/Playlist are 50/50, user mouses over collection and it automatically resizes to 80/20.\n2. User selects songs to drag and drags them downward to playlist.  When the mouse crosses from collection to playlist it automatically resizes to 20/80\n3. User selects where to place the songs and releases them, populating the playlist.  User moves mouse off of playlist and it resizes to 50/50.\n\nBenefits:\n-Saves on screen real estate.  If I want a lot of information about my collection or playlist, I move my mouse over it.  Then when I'm done I recover the space.\n-No gap between collection and playlist.\n-Leaves \"drag to context\" free for other actions, plus \"drag to context\" doesn't intuitively imply \"add to playlist\" (perhaps drag to context could do something like Pandora and recommend songs similar to the ones you dragged?  That would be pretty cool :))\n-Automatic resizing = eyecandy opportunity :)\n\nAnyway, that's my 2c.  Let me know what you think!"
    author: "Matt"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "Oh, and this is a different Matt from the other reply :-P"
    author: "Matt"
  - subject: "Re: Drag and Drop"
    date: 2007-03-13
    body: "An idea that popped into my head while reading other replys to this comment:\n\nExtending the playlist was already mentionned, but I imagine that would feel like a rather intrusive change in the GUI while performing the action.\n\nWhat - with all the talk about transparency and 3D going on - about having a semi transparent copy of the playlist appear right next to where the mouse was positionned? Maybe enlarge it a bit in the process.\n\nI envision this sort of like, as soon as the 'drag' starts, the mouse takes off from the program level, and that additional playlist level appears between 'the cursor' and the background. Like in a movie, where the camera moves away from an object while at the same time adjusting the zoom - and another object comes into view ...\n\nAs usual I have no idea whether or not it's possible or even if this makes sense.\n"
    author: "Richard"
  - subject: "Re: Drag and Drop"
    date: 2007-03-14
    body: "As you have mentioned possibly extending the use of \"double clicking\" may I make the suggestion that Amarok2 respects the users' preference with regard to single vs. double clicking as chosen in the KDE mouse configuration. While undoubtedly \"double clickers\" are in the majority, there is surely a significant number of \"single clickers\" for whom the instructions on the current Playlist rankle. As far as I can see, Amarok stands alone among the major KDE applications in unnecessarily prescribing this behaviour. I would hope therefore that the developers take the opportunity which Amarok2 presents to make a more integrated desktop for many users.  "
    author: "Stuart Neill"
  - subject: "A good direction"
    date: 2007-03-13
    body: "I like the mockup and I am happy to see the progress in amarok2 to resamble the ideas of the mockup. \n\nWhat I disliked the most on \"old\" (alreday great) amarok: sidebar + tabs and not fitting that great on a widescreen display.\n\nWhat I liked the most on the mockup: no tabs and sidebar ;-) Besides this, the items of the UI are very sensible grouped in the mockup. You have the \"control area\" on top that sticks on ones eyes because of the colored background (hope this will find it's way into the code some day). You have your collection to the left, the context info at the center (\"show lyrics\" and such would be nice to have here as \"links\" cause we don't want any tabs, right?). The playlist to the right is great as well - it is beautiful, informative and needs much less horizontal space as the \"classic\" playlist. In general I like the \"flat\" \"htmlish\" look of the mockup (well, that might be because it's a mockup). I hope you guys manage to resamble that look with qt4.\n\nWhat came into my mind when I read the artice: the new mockup ideal is IMHO _much_ better then the experimental context-browser/playlist merge that was introduced in an amarok-1.X-beta (and then delayed). So please don't say amarok devs don't care about there users. This mockup seems to be very well though of.\n\nKeep on the great work!\nSero  "
    author: "Sero"
  - subject: "Re: A good direction"
    date: 2007-03-13
    body: "Regarding the experimental layout from 1.4-beta: I've come to the conclusion that a top/bottom ordering of main widgets always looks messy and cluttered.\n\nAs you can see on the new mockup for Amarok2, there is a natural beauty in ordering the views strictly horizontally: Left, Middle, Right. Also this layout scales great with widescreen displays, which are becoming increasingly common."
    author: "Mark Kretschmann"
  - subject: "Re: A good direction"
    date: 2007-03-13
    body: "I agree this is a good idea to split the GUI in 3 horizontal parts.\nThe new GUI is really beautiful.\n\nIt seems like there are a lot of always-unhappy-users who would prefer the old gui. I think it might be a solution to make the layout a bit customizable (ie possibility to switch the right and the middle part, possibility to hide one of the 3 parts, etc).\nI have not tested amarok2 yet, and I don't really know Qt... but I'm under the impression this could be easily feasible.\n\nKeep up the good work, and try to keep the new GUI as simple as it is now ! :)\nDon't let conservative users destroy your innovative spirit :)\n"
    author: "shamaz"
  - subject: "visual appearence of mockup"
    date: 2007-03-13
    body: "I think the mockup for Ammarok 2.0 looks rather smart.  Its clear easy to follow.  Hopefully each of the panes will be reszable so that the playlist pane can be made wider.\n\nWhat I dislike about the mockup is the controls and visualisation along the top.  Firstly they seem out of reach along thwe top compared to there possition with the 1.x series.  Secondly they donot fit in properly in terms of visualr look.  The shole background shifting colour looks out of place and rather windows vista to me.  If they were to remove the colour blending and have the background the same colour as the rest of the application back ground (in the same way as the current 1.x series does), or even just not bother changing appearence of the controls section (they work perfectly fine at the moment and are visually pleasing) then the mockup would be perfect.\n\nIf the controls section were to change to how it currently looks then the mockup would be perfect.  I would certaintly use it."
    author: "tuxedup"
  - subject: "Re: visual appearence of mockup"
    date: 2007-03-13
    body: "The coloured background was me messing around. I doubt it'll end up in the final product. Although I kinda like it still :)"
    author: "mxcl"
  - subject: "Re: visual appearence of mockup"
    date: 2007-03-18
    body: "Maybe instead of the tabs, there should be three folding frames in the middle instead. Then the user can unfold the frame he wants to see.\n\nThe play bar on the top seems reasonable. It's good to have nice large controls for the user. Of course these should be customizable though..\n\nCheers\nBen"
    author: "Ben"
  - subject: "What the users *really* want..."
    date: 2007-03-13
    body: "- Tabbed playlists\n- A fullscreen mode\n- Multiple collections\n- Volume normalization without a need for scripts\n- Recording / capturing streaming media to disk\n- A better use of covers (maybe similar to Cower Flow)\n- More data from last.fm integrated into Amarok\n\nThis is what the users want, and this is what should be added to Amarok 2.0.\n\nNobody asked for a GUI refactoring, but everybody asks for the above features...\n\nSo which of those features will make it in Amarok 2.0? That's the really important question here, not the GUI..."
    author: "fish"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "Thank you for telling me what I really want!  You are right, to be aggressive with the developers is the solution !  \nListen to me Amarok devs : don't forget that everybody is paying you for this software!"
    author: "shamaz"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "http://bugs.kde.org/buglist.cgi?product=amarok&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED\n\nread and learn..."
    author: "fish"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "two things: your response hardly went any ways towards addressing the OP's point about manners being something that should be included when hoping to sway people who are working for free.\n\nSecondly, having had a quick (by no means comprehensive) look at the link you provided I'd hardly characterize the number of votes for the wish list items you listed as an overwhelming mandate of user opinion. I'm not saying that some of these items may or may not have merit, but the number of votes the various wishes have received certainly don't warrant the strident demands you seem to feel entitled to make on the devs"
    author: "borker"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "I agree with you...\nI think that the new GUI is step backwards."
    author: "franta"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "I never said the new GUI is a step backwards.\n\nWhat I'm saying is that there are much more important things than the GUI..."
    author: "fish"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "I also think, it's a step backwards. Main advantage of amarok is playlist creation and in 2.0 is playlist area moved to the little right pane with almost none information about tracks.\n\nAnd think about 1024x768 users, which don't want amarok to take all the screen area. And I'm not primary interested in artist info (which takes the most area).\n\nBut the mockup is not only bad. I like the top bar and little redesigned collection pane. The rest should not be changed..."
    author: "Petr"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "Well I'm a user and I don't want any of those features you listed.  However I do like the new GUI and look forward to using it.  So don't make sweeping generalizations that are obviously wrong.  "
    author: "Leo S"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "Is that why all those features are the top wishes in the Amarok bugtracking and why there are tons of threads on the Amarok board about tabbed playlists and all that?"
    author: "fish"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "How do you figure those are at the top of the wish lists?  I read the link you posted before and I certainly didn't get that. They may be the ones YOU posted there and there may be some other folks who asked for the same thing, but just because you SAY they're the top items doesn't make it so.  "
    author: "James"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "I for one have been waiting and hoping for exactly what this mockup is showing. If it is combined with an improved winamp/itunes style collection browser I'll be in heaven. \n\nDifferent strokes......"
    author: "Smoked"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "- Tabbed Playlists.\nI have yet to hear a convincing arguement for tabbed playlists.  I thought one day about coding them, then I realized that I could not think of a use, so I didn't.  They look ugly, they do not accomplish anything, and they have no buisness in Amarok.\n- A full screen mode.\nI've thought about this a little.  amarokFS already exists, if the developer of that wants to email the list and further explain what they need in order to make amarokFS better, then thats probably the best way to go.\n-Multiple Collections\nMaybe :)\n-Volume normalization without a need for scripts\nshow us the code.  None of the developers have expressed much interest in doing this, I'm sure if you coded it it would be added.\n-Recording/capturing streaming media to disk\nBeyond the scope of Amarok\n-A better use of covers\nWe'll see :)\n-More data from last.fm integrated into Amarok\nWe have big plans for the context in 2.0.. wait till you see them.\n\nNow I take big offense to \"This is what users want.\"  That is what you want. Most of that I could care less about.  The fun thing about open source software is taht you are free to add whatever you want--so do it.\n\nI am the person who has done most of the gruntwork behind the gui refactoring thus far (working off of mxcl's mockup).  I did this soley because I liked the new look, as did other developers, as did other users.  The interest has been there for a new look for quite a while.  \n"
    author: "Dan"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "\"I have yet to hear a convincing arguement for tabbed playlists. I thought one day about coding them, then I realized that I could not think of a use, so I didn't. They look ugly, they do not accomplish anything, and they have no buisness in Amarok.\"\n\nBut your USERS *want* and *love* them!\n\nThere's tons of threads about tabbed playlists on the Amarok board and people keep on asking about them on the IRC. Also, it's the #1 wish (almost) in the bugtracking system. You can, of course, ignore all that, but not adding tabs to Amarok 2.0 will disappoint a whole lot of users and they will prolly go elsewhere, that's for sure...\n\n\"amarokFS already exists\"\n\nSure, but adding it by default would be better.\n\nBut we had this discussion before on the mailinglist...\n\n\"code. None of the developers have expressed much interest in doing this, I'm sure if you coded it it would be added.\"\n\nThere's a script already that does it, so why don't you take a look at this code?!\n\n\"Beyond the scope of Amarok\"\n\nWhy?!\n\n\"Now I take big offense to \"This is what users want.\" That is what you want.\"\n\nWRONG!\n\nRead the Amarok SMF board and check the Amarok bugtracking system.\n\nThose are the top wishes and it's exactly what LOTS of users would like to see in Amarok 2.0...\n\n\"Most of that I could care less about.\"\n\nIt seems you also could care less about all the Amarok users out there.\n\nIgnoring all the requests is the easiest, eh? Yeah, why bother...\n\n\"I am the person who has done most of the gruntwork behind the gui refactoring thus far (working off of mxcl's mockup). I did this soley because I liked the new look, as did other developers, as did other users. The interest has been there for a new look for quite a while.\"\n\nOkay, yes...and I NEVER criticized the new look. Actually, the opposite - I really like it. \n\nHowever, just changing the look will disappoint lots and lots of users...\n\nIt's not what most of them are waiting for - the mentioned features is what lots of them would really love to see in Amarok 2.0 and not \"just\" a GUI fefactoring.\n\nBut yeah, keep on ignoring - you'll see where it leads to...  \n\n\n\n\n"
    author: "fish"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "If it leads to make users like you switch to another audio player (banshee or whatever), then it might be a good thing.\nI think you don't understand what is an open source project. The devs are working on amarok for free, and because it's fun. You can't FORCE them to make something that don't interest them.\nMoreover, what's the problem of having a feature in a plugin ? Why should it be better to add it by default ? (except adding build dependencies...)\nDoes Firefox bundles adblock, flashgot, fireftp, [insert your prefered plugin] by default ? No. Does it affect firefox popularity ? No. You see ?\n\nBy the way, I hope there are plan to integrate kghns2 into amarok2 for managing plugins :)\n"
    author: "shamaz"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "my problem with plugins is that I'm too damn lazy to install them ;)\nhowever, ghns does solve that in some cases..."
    author: "Chani"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "\"However, just changing the look will disappoint lots and lots of users...\"\n\nJust think about how long it will take until amarok 2.0 is release...\n\n\n\nSo there is quite a long time left to implement more features. But many of those features should be done _after_ the GUI refactoring.\nLet's see what the amarok 2.0 will bring. I bet many of your mentioned features will be (more or less) be included."
    author: "Birdy"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "I also would use tabbed playlists.\n\nBut you need to keep in mind, people ask for stuff that isn't always a good idea.\n\nEven if a lot of people want it, doesn't mean they are onto something."
    author: "mxcl"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "Yeah, you guys are sooo not going to make any money on this version.  No one is going to pay for this upgrade.\n\n:-P\n\n\nBret."
    author: "Bret Baptist"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-13
    body: "> -Recording/capturing streaming media to disk\n> Beyond the scope of Amarok\n\nIn Amarok 1.4 there is a menu-item \"Burn to CD\" which opens k3b when selected.  I think that there should equally be a menu-item \"Rip stream\" which opens a stream ripping program.\n\nBy the way, Amarok is a great app and your new GUI is cool!  I agree with a lot of other posters that the GUI should be customizable."
    author: "Vlad"
  - subject: "Re: What the users *really* want..."
    date: 2007-03-14
    body: "Maybe you should take a look at, http://www.kde-apps.org/content/show.php/FALF+Player?content=43961"
    author: "Peppelorum"
  - subject: "JACK support yet ?"
    date: 2007-03-13
    body: "I like Amarok, and I've briefly reviewed it in my blog for the Linux Journal at http://www.linuxjournal.com/node/1000181. For me, its most glaring lack is stable support for JACK. Gstreamer's JACK back-end is in need of maintenance (last time I checked), and I'm not sure what xine's current status is re: JACK. Give Amarok clean JACK audio support and it'll be the bee's kness for me. :)"
    author: "Dave Phillips"
  - subject: "Re: JACK support yet ?"
    date: 2007-03-13
    body: "If you want jack support you need to grab libxine from cvs (or is it svn) and compile it yourself.... (or wait until the next release)\n\nJack suddenly becomes an option in Amarok. Which is great because my good soundcard does not have alsa drivers."
    author: "Danni Coy"
  - subject: "Re: JACK support yet ?"
    date: 2007-03-13
    body: "That makes no sense. I think you would be better served asking the audio backends to improve JACK support. Amarok has no contact with JACK what-so-ever and never will."
    author: "Allan Sandfeld"
  - subject: "Re: JACK support yet ?"
    date: 2007-03-13
    body: "That would prob be because pretty much no one uses jack"
    author: "easytiger"
  - subject: "Re: JACK support yet ?"
    date: 2007-04-19
    body: "sorry but ardour/rosegarden/rezound/hydrogen users i.e. musicians doing music with Linux use Jack...  Jack support would be very much appreciated.\n"
    author: "lucus"
  - subject: "gui"
    date: 2007-03-13
    body: "The new gui doesn't look good. There (on the mockup) the tracks rows height is big, so you can't see many of them on one sreen.\n\nCan you please leave the old style as an option?"
    author: "CHX"
  - subject: "I like the old look better...."
    date: 2007-03-13
    body: "First of all: you guys are doing a great job, keep up the good work.\n\nOne of the reasons I like the old GUI better is: I think it's a bad idea to use 3 columns. It's just too much. Think of it: every professional program/website has 2 columns...\n\nML"
    author: "Meister Lampe"
  - subject: "Re: I like the old look better...."
    date: 2007-03-13
    body: "Simply not true. \nSince getting a widescreen monitor, three pane interfaces is a huge productivity boost. Among the programs that would be far less usable without it that I use are: kdevelop, eclipse, visual studio, outlook and thunderbird. And that's just an off the top of my head 20 second list. "
    author: "Smoked"
  - subject: "new appeareance..."
    date: 2007-03-13
    body: "I think that, on one hand, there are some great innovations with news GUI, but on the other hand, other innovations are just trying to change the user interface because of changing it : well, it's not the good way.\n\nThere should have been some example mockup releases before to begin the development. As some others, I think, for example, that the context menu, shouldn't be there, in the middle of the window. Because, yes, the context menu is good looking in himself, but I don't look at it so much when I'm workin'.\nAnd I think that, the common use of Amarok is, listening music when you're working. You just want to see Amarok icon in the system tray. When the playlist is finished, you click on it, and drag&drop some other files in the playlist. \n\nThere is something I like yet, it's that the playlist informations are not in columns anymore. The informations are integrated in the file line (I hope you see what I mean...) : author, time, notation etc.. (see mockup).\n\nI also think that, the collection should take more place, and should more use CD-covers. The collection is one of the things that made I decided to migrate to Linux & KDE : I didn't find this on Xmms. \n\nAnd to finish this message, I'll say that :  Yeah ! the menu, with navigation buttons (play, pause, next, previous etc...), is great. It's large, usable, with great graphics...but it should be on the bottom, not on top."
    author: "Jean-Baptiste"
  - subject: "I like the current layout"
    date: 2007-03-13
    body: "Please tell me there will be an option to make Amarok use the current main window layout? I don't think the new one looks as good (yet), and also it doesn't really leave enough space for people like me who want to see 7 or 8 columns of metadata for their playlist. More customisability of layout wouldn't be bad. Being stuck with a layout like the mockup would be bad for a lot of people who really like the current GUI.\n\nIMHO, Amarok's current GUI is very good, certainly the best of any existing audio player (being easier to use than iTunes is pretty impressive, given that iTunes is supposed to be built for simplicity and Amarok is purposefully feature-rich). Phonon support, scriptable services and all that are cool additions but it would be a shame if the stuff which is already almost perfect were changed just because it's The 2.0 Release."
    author: "Ben Morris"
  - subject: "Well..."
    date: 2007-03-13
    body: "Ladies and Gentlemen, Amarok 2 is progressing very rapidly. To quote Mark Kretschmann, Amarok's lead developer: \"If development continues at this pace, we'll be done with Amarok 3 by the time KDE 4 is out ;)\" Be prepared for something awesome, as you've come to expect from the Amarok team.\n\n--------------------------------------------------\n\nAccording to the comments I've read so far, they've got enough work ahead to match users' needs instead of implementing what they thought was the best ;)\n\nKeep it up team, we're not asking for Amarok3, just a really good Amarok2 will be enough ;-)"
    author: "Cypher"
  - subject: "I am looking foward "
    date: 2007-03-13
    body: "I am looking forward for it, I like the mockup and I don;t think that it looks too much like itunes. I really like that the middle pane with information is taking special attention since I listen to a bunch of podcasts and its a pint to go trough the play list and then having to click on the bottom button  (show more information)and maximizing an minimizing the info just to see what the podcast includes. I am also glad that you have added basic video support( I hope you keep it basic) and I am looking forward to see what other features are implemented. "
    author: "tikal26"
  - subject: "added features"
    date: 2007-03-13
    body: "I would love to see a skin optimized for small touch screens.  I have a car pc with 7\" screen and so far you either get a gui that's easier to use with features like giant buttons, but doesn't navigate through music collections well or great navigation but buttons and title info just a bit too small."
    author: "donniedarko"
  - subject: "bad, just bad"
    date: 2007-03-13
    body: "GUI looks terrible. Just terrible. Kinda looks like the alignment has 100 different rules and nothing is following it. Images look really basic, like something from windows 3.1."
    author: "anon"
  - subject: "Re: bad, just bad"
    date: 2007-03-13
    body: "Whoa, easy there!\n\nPlease remember that this version was ported to KDE4/Qt4 just one month ago, and the new layout was introduced even later than that. Considering that there are several months to go before Amarok 2.0 will be released, MUCH will change before that!\n\nIf you think it looks bad now, you should have seen it a few weeks ago! :-)\n\nAs others have said, have a little faith, this a PREVIEW article about the current state of development! This is how software in (very) active development, far from a release, trying out a completely new GUI looks!"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: bad, just bad"
    date: 2007-03-13
    body: "It does look very messy, so thanks for reassuring us its not permanent :p\n\nAmarok is great. One question: will we be able to choose horizontal panes instead of vertical ones (I still hate vertical panes with vertical text on them... :( )"
    author: "Darkelve"
  - subject: "Hmmmm"
    date: 2007-03-13
    body: "I do allot of copying from the files tab to the playlist.  With the new layout, one will have to drag over the context menu, and then into the appropriate place in the playlist.  I think that is the main downside to the new proposed GUI.  The other downside being that if I want all the current info that I like using (Artist, length, album, score, stars) and have the context menu open.... would require allot of width. \n\nI would have to agree that if you do go with this GUI, the old Gui should be an option.  \n\n"
    author: "UA=42"
  - subject: "Looks great"
    date: 2007-03-13
    body: "I like the new look.  Can't wait to use it.  \n\nTwo things I'd like to see in Amarok 2\n - Fetching context information for internet streams (Context info doesn't work for radioparadise.com for example, and since context is such a central focus of the app, it should always work)\n\n - I know its not easy to do with the current toolbar approach to buttons, but it is very nice when the important buttons are bigger than the less important ones.  Like Play is bigger than skip..  Windows media player does this and it works quite well.   http://www.winsupersite.com/images/showcase/wmp11_xp_15.jpg\nNot that you should copy that layout, but I like the idea of making the play button more visually prominent than the others."
    author: "Leo S"
  - subject: "Re: Looks great"
    date: 2007-03-13
    body: "Not so sure about this. I know that for each play (to start playing an album) I usually press skip a number of times (to move to another track). "
    author: "RandomOne"
  - subject: "Re: Looks great"
    date: 2007-03-13
    body: "Well, how could you get any relevant information from radioparadise? It's not obvious to me."
    author: "Mark Kretschmann"
  - subject: "Re: Looks great"
    date: 2007-03-14
    body: "I mean just loading the song info correctly.  I don't know the right term for it, since it is probably different than ID3 tags, but radio streams like radioparadise do send the author - song name when the song switches.\nThis is what shows up in the metadata history.  \n\nHowever, in the current Amarok, when I click Lyrics it doesn't show them to me.  Although right now it says:\n\nLyrics for track not found, here are some suggestions:\n\n  Squirrel Nut Zippers - Put a Lid on It\n  Squirrel Nut Zippers - Bad Businessman\n  Squirrel Nut Zippers - It Aint You\n\nThe track I'm playing is the first in that list, worded exactly like that, so I don't know why it didn't find it.  If I click on the Wikipedia tab, it says: \n\nWikipedia Information \nSquirrel Nut Zippers - Put A Lid On It\nJump to: navigation, search\nWikipedia does not have an article with this exact name.\n\nI assume that the stream sends the author and name as one string, and then amarok uses that to search.  So all I really want is for Amarok to take a stab at parsing the string it gets back.  They are always formatted like \"Author - Song name\" (on radioparadise streams) so it shouldn't be too hard."
    author: "Leo S"
  - subject: "Looking forward amarok2 and suggestions..."
    date: 2007-03-13
    body: "I really can't wait to have amarok2.0 but I would like to suggest a pair of things to devs.\nI think amarok is going the right way with new functions and gui but is important to remember features we're waiting amarok2.0 for.\nConcerning gui I think an implementation for amarokfs cuold be great; to use it as screen-saver or anti-geek.\nAbout collection imho a full implementation of what regards bug 119539 or 90095 could be great.\nhttp://bugs.kde.org/buglist.cgi?product=amarok&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED\nAnother thing about collection is that it's not necessary to show it when playing something; a better way to show all we want to see in it is it to have collection screen-wide (and so not together with playlist and contest browser).\nI also think that in the middle of the screen it has to be what a user uses mostly, not what is thougth to be important (eg. I prefer having infos, which I read quite often, on one side most likely than in the center).\nAnyway amrok is great and for what I've seen about it will continue to be so."
    author: "Dareus"
  - subject: "middle pane"
    date: 2007-03-13
    body: "i think that middle pane is useless and it takes a lot of usable space in the window. i use amarok for playing and listening to the music, not for reading some information about artist, which i am not interested in. yes, it could be an optional feature, but i think it shouln't be there by default. try to replace it with something useful."
    author: "nardew"
  - subject: "I would rather see KPlayer ported to KDE 4!"
    date: 2007-03-13
    body: "I would very much rather see KPlayer ported to KDE 4. I really like its multimedia library far better than all the Amarok clutter.\n\nIn KPlayer everything is very clean, only the really necessary stuff is shown by default. And the multimedia library is so easy to use, and never gets in your way.\n\nI just set up a few playlists, and then closed the library window because I don't need it anymore. All the playlists I made are on the File menu, very easy to click and play.\n\nI wish all KDE programs were that simple, flexible and powerful at the same time. Oh, and KPlayer will never ever pop up any stupid flashy window right in your face when you are in the middle of something else."
    author: "Give me KPlayer"
  - subject: "Re: I would rather see KPlayer ported to KDE 4!"
    date: 2007-03-13
    body: "Thanks for pointing out KPlayer. I cannot believe that I've completely missed it's existence for so long. I've just found my new default video player :) \n\nI'll definitely be keeping Amarok for my music though. "
    author: "Smoked"
  - subject: "UI"
    date: 2007-03-13
    body: "Man, what are you people bashing the UI for? I like the new UI. It looks really cool! It's probably not done yet, so just wait and maybe they'll improve it to your liking. :)\n\nAnd to the Amarok devs, perhaps an option as to whether to use the old UI or new UI is in order."
    author: "The Digital Pioneer"
  - subject: "Re: UI"
    date: 2007-03-13
    body: "I don't like the new GUI because it marginalizes what I consider to be the most important function of Amarok.\n\nI like Amarok so much because it does a very good job of keeping my music organized.  It has tag editing, \"Organize Files\", a rating feature, \"Suggested Songs\" ... and (unlike JuK, the last time I tried it) an actual playlist.\n\nSure, sometimes I want to read lyrics, and the cover art is a nice touch.  But those are *secondary features*.  They are tangential to the primary job of organizing and playing music.\n\nMost of the time, I have Amarok going in the background while I'm at work.  That's my primary use case.  I'm rarely (if ever) interested in staring at cover art or reading artists' Wikipedia entries while listening to music.\n\nI'm either *listening to music* and completely focused on it, or I've got some music playing in the background and I'm doing *other things*.  Neither of those two scenarios involve digging deeper into the artist/track that's playing, which is what the new interface seems to be focused on.\n\nNow, don't get me wrong -- I think it's a perfectly good idea to have those features.  But I don't think they should become the central focus of the player.  If they were to become so, it would greatly reduce Amarok's usefulness to me because I'd be constantly trying to navigate around things about which I don't care.\n\nIt's not a question of \"improving\" the new UI.  It's an issue of taking Amarok in a completely different direction, one that I'm not happy with.  (It sounds like many others feel the same way.)  I honestly can't think of a way to \"improve\" the new UI that doesn't involve moving back toward the old one.\n\nWe'll see.  Amarok developers have done a kickass job so far, so I'm willing to give them the benefit of the doubt.  But it seems to me they have a lot of work ahead of them."
    author: "Des"
  - subject: "Re: UI"
    date: 2007-03-14
    body: "JuK does have playlists, you can drag your collection around, or drag and drop back to where your collections are listed and it creates a new playlist.\n\nI think JuK and amaroK complement each other nicely.  I use JuK for my purposes (mostly recorded radio and listening to complete albums), and can easily see a music playlist obsessed person preferring amaroK."
    author: "Evan \"JabberWokky\" E."
  - subject: "Second screenshot"
    date: 2007-03-13
    body: "I love the concept of the second screenshot. It's realy cool to be able to get information about the band that's currently playing, and to get it in a readable way. \nI wasn't convinced about the playlist but thinking about it, it can actually be quite cool.\n\nOne thing it would be cool to have is to be able to get information about another band than the one currently playing. I don't how that should be handle. But when you're with friends and trying to remember the name of that guitarist in whatever band, it would be great to go the collection and be able to right click (for instance) and get information about the selected band displayed in the center panel."
    author: "Francois"
  - subject: "Windows version"
    date: 2007-03-13
    body: "If they manage to finish Amarok 2.0 before KDE 4.0 is released they could already release the Windows version. Some of my Windows friends really like Amarok when they see it on my PC but are just not yet ready to switch to Linux just for one app. If they could already enjoy Amarok before the final release of KDE 4.0, that would be awesome. "
    author: "WPosche"
  - subject: "I love Amarok"
    date: 2007-03-13
    body: "...and I really trust in its developers. They have proven again and again that they know what they do. Keep going!\n\nIf I were to ask something for Amarok, be it 2.0 or 1.x, is that I'd like it to start quickier when double clicking files in Konqueror.\nI still miss good old Winamp for that reason, currently Amarok takes too long to load a song, even when it's already running.\n\nThx!\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "maybe, maybe not"
    date: 2007-03-13
    body: "That mockup doesent really look so good to me, but ofcourse i havent tried it, and the amarok developers has previously created awesome stuff, so it cant really be decided untill the product is on the table, and at that time i will give it a full go as it deserves, and i probably will end up like it, as with the current amarok."
    author: "redeeman"
  - subject: "Syncing with MP3 players"
    date: 2007-03-13
    body: "Developers: Don't listen to all the GUI whiners! Amarok is the sweetest music player I've ever encountered, and I fully trust you guys/gals will do a great job with Amarok 2.\n\nMy only request is that you keep improving the ability to sync with portable media devices. I love the on-the-fly file type conversion, and would like to have more control over that. My MP3 player is only 1 GB, and since I mostly listen to it while I exercise, I'd like it if Amarok could re-encode the files to smaller file sizes when it transfers them to my music player. The treadmill lowers the sound quality anyway, and I could definately do with fitting more songs on it!"
    author: "kwilliam"
  - subject: "Re: Syncing with MP3 players"
    date: 2007-03-13
    body: "Now what exactly do you want? Amarok already can transcode files (install and run the transcode script, then check the settings for devices)."
    author: "superstoned"
  - subject: "On a positive note"
    date: 2007-03-13
    body: "I'm only taking the time to post because I see so much negative feedback. I liked the mockup, but moreover I like the pace and enthusiasm with which Amarok has been developed. You haven't dissapointed me so far so I'm more than happy to put my trust in you :D"
    author: "bigtomrodney"
  - subject: "Re: On a positive note"
    date: 2007-03-14
    body: "I second this!"
    author: "mactalla"
  - subject: "Two groups"
    date: 2007-03-13
    body: "It sounds like there are 2 main groups of people here commenting.  \n\nOne uses the context browser and loves the new UI.\n\nThe other doesn't use it and hates the new UI.\n\nI think just making the panes collapsible would get rid of a lot of the complaints."
    author: "Matt"
  - subject: "Re: Two groups"
    date: 2007-03-14
    body: "So what you're saying is have it exactly how it is now?\n\nAmarok, in my opinion, is a good player cause it's simply bloody good at playing music.  A simple idea that other music players don't seem to get.  The playlist is the music, the screenshot of the album cover isn't the music.  I don't get why the context menu (which doesn't really have all that much to do with choosing the music to play at all) would get precedence over the playlist.  People who are glowing over the new idea seem to be too concentrated on having it look good and are forgetting that Amarok should be centred around one function - playing music."
    author: "strider"
  - subject: "Double-edged sword"
    date: 2007-03-13
    body: "While I really love this \"Road to...\" series, it sure does stir up a lot of premature discontent.  It seems that each week, the writers add more and more preemptive \"this is pre-alpha\" warnings, but the negative comments just keep piling up.  I hope nobody gets discouraged by all the negativity.  I love being able to read these great articles to get insight into the development process, but the negative responses are overwhelming.  I wouldn't start belly-aching until release is a little closer."
    author: "Louis"
  - subject: "Re: Double-edged sword"
    date: 2007-03-13
    body: "I think people are reacting like that because of that particuler comment :\n\n-------\nNope, that's a no-go. I'm convinced that the context view needs to be in the center. It's the context information that we want to emphasize in 2.0. The playlist is just a tool.\n\nAlso it simply wouldn't look as good.\n-------\n\nComing from a team member, it sounds scary. It could be interpreted as (beware, troll inside) a really GNOME-ish attitude : I think it is, I make the stuff, you just follow.\n\nBut don't get people wrong. If they are reacting like that, it's just because they love Amarok and they want it to fit their needs. And as said above, it appears that they are two groups of people. And none of them wants to be left behind because of the implementation. It's not a flamewar, it's just people trying to defend their point of view, and debates are always good. It's better to react now while it's still alpha than later when it's final version, don't you think ?\n\nI think the developers got the point of that debate : adaptability. Amarok should definitely be able to be configured at will. Let's see now what's gonna happen. I trust them, they'll do their best, as usual."
    author: "Cypher"
  - subject: "Re: Double-edged sword"
    date: 2007-03-14
    body: "You make a good point, but voicing a concern is much different than blasting out derrogatory remarks.  I'm sure coherent, meaningful debate is appreciated to some extent, but words like \"sucks\" and \"hate\" will not help anyone.  There are way too many of the latter."
    author: "Louis"
  - subject: "Re: Double-edged sword"
    date: 2007-03-14
    body: "It tells the developers that there is a want among the users for being able to customise the GUI, potentially back to the 1.x version. Usefull information no?"
    author: "Ben"
  - subject: "Re: Double-edged sword"
    date: 2007-03-14
    body: "no. that information is not useful.  With most of the changes we plan to make it would require maintaining two sets of code to give the option.  We are changing the way the context works and acts, the way the playlist works and acts.  To give an option of switching back would require we maintain the current code as well as the new code, and no one wants that headache."
    author: "Dan"
  - subject: "Re: Double-edged sword"
    date: 2007-04-06
    body: "The 'two sets of code' you're talking about should be rather minimal presuming you're using good OO techniques and abstract your GUI from the rest of Amarok.  Actually if its done right you could probably swap with any theme you wanted to presuming they adhered to the api you defined, and these could be designed and maintained by any user with an understanding of C++, QT, and Amarok."
    author: "TheCycoONE"
  - subject: "Stability first?"
    date: 2007-03-13
    body: "Might I suggest spending a little time focusing on stability? Unfortunately, I've always found amarok to be one of the most unstable programs I have - it almost never runs for longer than a day, and often crashes within an hour. This is and has been true in various releases over the last 2 years, including the latest. It's a shame, because it's the best music library interface available - but I wish I could use something like sox or xmms to actually handle the playback."
    author: "Richard Neill"
  - subject: "Re: Stability first?"
    date: 2007-03-13
    body: "Never seen amarok crashing, and i use amarok for several years now.\n\nDid you file bug reports about the crashes?\n"
    author: "whatever noticed"
  - subject: "Re: Stability first?"
    date: 2007-03-14
    body: "I don't get it either. I have heard many people complain about the stability of Amarok, but even if I used SVN the most time it crashed very rarely here."
    author: "Lans"
  - subject: "Re: Stability first?"
    date: 2007-03-14
    body: "I had stability issues on some old release (1.2? 1.3?)  But it's been quite a long time since I've seen Amarok die on me.  So long I had forgotten about it :)"
    author: "mactalla"
  - subject: "Re: Stability first?"
    date: 2007-03-22
    body: "Unfortunately I have to agree.\nIt used to be very stable for me, but, IIRC, from about mid through the 1.3 branch, stability has gone out the window.\n\nThe new features and general usability have all been fantastic. No other player comes close in those regards, IMHO.\nBut increased instability and little bugs like killing the podcast download before it was finished and thereby cropping the last few seconds of the podcast, eroded my confidence in Amarok (and, I'm afraid, also in the development team itself) to such a degree I simply stopped using it.\n\nIf the bugs were ironed out, I'd never use another PC music player.\n\nI've never found another player I liked as much, so I may end up writing my own player with the features I want.\nThat does seem like a waste of time, but debugging a codebase the size of, and with as many apparent bugs as Amarok's, and one I'm not familiar with at that, would likely take longer than I'd be prepared to invest."
    author: "Kim Rasmussen"
  - subject: "Re: Stability first?"
    date: 2007-03-22
    body: "I've not noticed any major stability problems with 1.4.x, but perhaps we're using different audio backends. I'm using the xine backend, which one are you using? Have you reported the bugs you have found?\n\n> I may end up writing my own player with the features I want.\n> That does seem like a waste of time, but debugging a codebase \n> the size of, and with as many apparent bugs as Amarok's, and \n> one I'm not familiar with at that, would likely take longer \n> than I'd be prepared to invest.\n\nIf you're thinking that you can create your own player with Amarok's capabilities from scratch in less time than you could fix a few issues in pre-existing code, then you're probably fooling yourself."
    author: "Paul Eggleton"
  - subject: "Re: Stability first?"
    date: 2007-03-23
    body: "I use Xine too. And, IIRC, I did let it send off a few crash reports.\n\nIf I thought I could develop my own player from scratch with all Amarok's features in less time than I could debug Amarok itself, I probably would be fooling myself.\n\nBut I wouldn't be developing it in a vacuum. I could rip the visual layout ideas from Amarok, and I could take code snippets from all over the place (Amarok, Juk, etc.) and stick them into my own framework.\nBesides, I'd only want to replicate a relatively small subset of Amarok's features. For things like handling my iPod, GtkPod (that and Gimp are the only Gtk programs I use) is much better anyway. It's just that the subset of Amarok's features I do use, I can't find as well implemented, from a usability perspective, anywhere else.\n\nAlso, when I read what the problem was that had caused the ends of podcasts to be cropped, I couldn't help thinking it sounded like some of Amarok's internal design was rather flawed. Or at least totally incompatible with how I would want to do it.\nI might well have been mistaken, but it made me not even want to bother taking a look. Working with a codebase that would have me grinding my teeth would not speed up my efforts. :-)\n\nNot that Amarok are in any way unique in rushing to stick more and more features in, and skimping on basic quality assurance along the way. The obvious bugs I came across in the Linux kernel's atkbd.c driver a couple of years ago, gave me one hell of a fright. That was worse than anything I expect to find in Amarok. I seriously considered switching to some BSD variant after that. It certainly tainted the development process of the 2.6 tree in my eyes."
    author: "Kim Rasmussen"
  - subject: "Great Work!"
    date: 2007-03-13
    body: "The new Amarok release is looking excellent, and I really like that mockup! Great work- Amarok is one of the best apps I've ever used, and I'm sure it'll get even better with the 2.0 release."
    author: "Melon"
  - subject: "we didnt like it!!!!"
    date: 2007-03-13
    body: "Seriously, we didnt like it.\n\nLets see if the developers of amarok will be sensible enough to NOT go into this path...\n\nAnd if its already changed, please do like when they tried to chaneg the browser position to horizontal (wtf) and they've reverted some versions back in the svn.... PLEASE, I beg ya\n\nhappy (until now) amarok user"
    author: "meh"
  - subject: "Re: we didnt like it!!!!"
    date: 2007-03-14
    body: "We dooon't like it, my preccioussss...\n\nWho are \"we\"? Sure, there are many that don't like the mockup, but how can you tell that you don't like the actual product when you haven't even tried it - or so I suppose?\n\nAnd instead of just complaining about how \"we\" don't like it, why not point out which parts of the mockup and maybe how to improve it? Or is the old way the only way to go? "
    author: "Lans"
  - subject: "Plain brilliant."
    date: 2007-03-13
    body: "Plain brilliant.\n\nFor the GUI I would say, looks lovely. I am glad to hear that Phonon works out so good for you already. Lets hope Amarokers will push the limit of Phonon-xine to what they had in Amarok-xine already, that would cover one of the backends already quite fine.\n\nAlso, reading this stuff, to me this reads like Amarok could kill DRM only shops by the way of making us get to know fairly traded music. That would be your even higher achievement. So yes, push ahead, get Windows versions out there, go to the masses and liberate them of those who hold them hostage with Britney Spears remakes.\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Re: Plain brilliant."
    date: 2007-03-14
    body: "Hey what's wrong with Britney Spears remakes ?!\n;)"
    author: "shamaz"
  - subject: "i DONT LIKE new GUI"
    date: 2007-03-14
    body: "What for i need \"now playing\" http://static.kdenews.org/content/the-road-to-kde-4/vol12_4x_amarok.png \nat the center of the screen that occupies 70% of the screen?! Do you think users are so stupid that cannot understand that highlighted line in playlist is playing?!\n\nAlso why did you move buttons to the up left? Is it for mac users? Dont you think amarok is mostly linux player. \n\nUsers need to listen the music and not to \"rediscover\" how to disable unused features."
    author: "ref"
  - subject: "Re: i DONT LIKE new GUI"
    date: 2007-03-14
    body: "its a mockup :)"
    author: "whatever noticed"
  - subject: "Re: i DONT LIKE new GUI"
    date: 2007-03-14
    body: "No, its not a mockup.  The only mockup in the images is the one that looks completely different (the blue toolbar and redone playlist.) The rest are actual screenshots of what Amarok looks like right now."
    author: "Dan"
  - subject: "Re: i DONT LIKE new GUI"
    date: 2007-03-14
    body: "Really? Even if the devs are amazing, I don't think they've come this far yet..."
    author: "Lans"
  - subject: "Re: i DONT LIKE new GUI"
    date: 2007-03-14
    body: "yes really.  "
    author: "Dan"
  - subject: "Re: i DONT LIKE new GUI"
    date: 2007-03-14
    body: "And you can thank Dan for it all..."
    author: "shash"
  - subject: "broken web-page layout"
    date: 2007-03-14
    body: "Could somebody fix the layout of this page (and any similar pages) so it doesn't require a super-wide display?   Right now a horizontal scrollbar appears when I narrow the Konqueror window to less than 1200 pixels, which is ridiculous!  The main article body lines get truncated at about 1000 pixels.  If I enlarge the font, it is even worse!"
    author: "Per Bothner"
  - subject: "Keep the old UI!"
    date: 2007-03-14
    body: "Please keep the old UI! At least make it an option among different layout styles in user preferences.\n\nThis article puts forward the idea that Amarok merely copies the playlist style of XMMS/Winamp. That's true in the one-song-per-line sense, but Amarok goes beyond that by displaying way more information than the default XMMS/Winamp playlists ever did: Amarok gives you something more like a spreadsheet where you manipulate the widgets to get the playlist to display to your liking, including column order, sort order, and my favorite feature from recent releases, the highlighting of recently-added tracks. I LOVE that one. So I would say there's nothing \"old\" or \"boring\" or \"due for change\" in Amarok's playlist display. In fact, I think the current playlist format has quite a high information density, assuming all your music files have tag information. \n\nAs some others seemed be suggesting in not-so-many words, I agree that Amarok should be playlist-centric. I find three panes annoying, and they seem to stray from being playlist-centric. In the proposed layout, the amount of application real-estate the playlist takes up goes way down. "
    author: "Ben"
  - subject: "Re: Keep the old UI!"
    date: 2007-03-14
    body: "Totally agree. To compare Amarok's playlist with XMMS's playlist functionality is borderline despicable.\n\nAs I said in another thread, I think it's critical that Amarok play to its strengths, and an intuitive playlist (which is already has) is most definitely a strength."
    author: "Alistair John Strachan"
  - subject: "True gapless playing"
    date: 2007-03-14
    body: "As long as Amarok will not have a true gapless playing engine (as is available with the Crossfade plugin in XMMS), I will not be using it. \n\nAll the gizmos in Amarok are sure fine, but if it cannot do the most important thing - playing music and going track to trac without interruption, like is available on a CD player - then it's all for nothing,"
    author: "Guillaume"
  - subject: "Re: True gapless playing"
    date: 2007-03-14
    body: "it's had crossfade support for a long time."
    author: "Aaron J. Seigo"
  - subject: "Re: True gapless playing"
    date: 2007-03-20
    body: "The issue with crossfading it, Amarok doesn't detect, if tracks intertwine (proper english?). When I have an opus represented by N audio files, and Amarok puts gaps between them or crossfades, even though the tracks should be played directly one after the other, it sucks. I admit, while nice, such a detection isn't quite simple and the bug is more this information can't be embedded in id3 tags, to be read by the player, but still..."
    author: "Carlo"
  - subject: "Re: True gapless playing"
    date: 2007-03-14
    body: "Gapless playing is a feature of the engine, not Amarok itself. If the engine supports it, we have it. So, what engine are you using?"
    author: "shash"
  - subject: "Fix tags before the new GUI, please"
    date: 2007-03-14
    body: "It looks like there's a lot of commentary about the GUI.  Well, count me on the \"it's not broken, don't fix it\" side.  Really, I rarely if ever look at the GUI in Amarok.  I interact with it using the media buttons on my keyboard.  I *like* the iTunes-esque (not winamp-esque) interface.  \n\nIf there's any GUI changes to make to Amarok, the article already points out what they are.  For tag management, Juk currently blows Amarok away.  I use Amarok as my player but still switch to Juk to do heavy duty refiling and updating.  Simple things like the MusicBrainz integration in Juk are far better than Amarok's, which can waste time and lose data more often than not.  Please, consider that a UI bug to fix before reinventing the interface."
    author: "Larry"
  - subject: "XBMC integration"
    date: 2007-03-14
    body: "Would it be possible to have Amarok 2 integrated with XBMC?\n\nI mean:\n\n- controlling an XBOX MEDIA CENTER ala foxytunes\n- streaming an MP3 to an XBMC\n\nthanks,\nTimur\n\n"
    author: "Timur"
  - subject: "sound!"
    date: 2007-03-14
    body: "amarok is very good player, exept one - the sound! it sounds ugly, even with equalizers! do better sounding, i dont know how, and it will be the best!"
    author: "Konstantin"
  - subject: "Re: sound!"
    date: 2007-03-14
    body: "In your engine configuration dialogue, make sure you are using the Xine engine, and have the Speaker Arrangement set to Passthrough. Also, the equaliser has been improved a LOT in 1.4.5 (so i've heard, don't use it myself), so if you're running 1.4.3 or earlier - please upgrade now! :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Wishlist"
    date: 2007-03-14
    body: "Are there any plans for a \"real\" plugin infrastructure, not just dcop calls? Maybe with language bindings for Ruby/Python/Whateva?"
    author: "alterego"
  - subject: "Re: Wishlist"
    date: 2007-03-14
    body: "you can already write scripts with ruby/python to create amarok plugins"
    author: "AC"
  - subject: "Re: Wishlist"
    date: 2007-03-14
    body: "No, we don't plan to implement an interface for external binary plugins. Binary plugins are only used internally. This is because with binaries you end up in binary incompatiblity hell when you're going cross platform. And it's already a problem on Linux with 32bit and 64bit issues.\n\nAlso, binary plugins that run in-process are always a stability risk. A bad plugin can crash the whole application.\n\nTherefore we will only allow external plugins with scripting languages (Ruby and Python), just like it is now, but probably with more powerful interfaces.\n"
    author: "Mark Kretschmann"
  - subject: "I like !"
    date: 2007-03-14
    body: "I like amarok very much and this new version looks promising ;))\n\nKeep up the good work ..."
    author: "messner"
  - subject: "Playlist Overhaul is Much Welcomed"
    date: 2007-03-14
    body: "Like the quest for the \"perfect\" browser, I've long been seeking a powerful, flexible and consistant media player and organizer.\n\nHaving been told that Amarok is the best player around, I tried it, and fell in love with about 90% of it.\n\nHowever, the single thing that made Amarok unusable for me was the \"wonky\" playlist setup. Creating a play list is simple enough, there's a few logical ways to do it. Where my issues come in with Amarok is that the player seems to believe when I click a song it should then be added to the playlist, rather than played in PLACE of the playlist.\n\nThis is so counter intuitive that I've pushed Amarok pretty far down on my list of \"decent\" players, instead resorting to Rhythmbox or (ug) Banshee.\n\nHopefully, with the rework of Amarok for KDE4, it will claim it's rightful place on my desktop. That is, if playlists make sense by then."
    author: "Kevin Dean"
  - subject: "Re: Playlist Overhaul is Much Welcomed"
    date: 2007-03-15
    body: "I like that feature, it makes it simple to create/extend playlists.\nIf i want to replace the whole playlist with one song, i rightclick on it and select that option."
    author: "whatever noticed"
  - subject: "Re: Playlist Overhaul is Much Welcomed"
    date: 2007-04-03
    body: "I'm sorry but the functionality you propose is itself so counter-intuitive it is unreal.\n\nLook at the word playlist. It's a _list_ of songs to _play_. If you want a player which will just play the song you click on automatically, erasing the rest of the current playlist in the process, then the whole point of a playlist is rendered incredibly moot.\n\nAmarok, as well as \"Rediscovering your music\" through the use of the context panel, is very much playlist centered, it's one of it's strongest points, and thus the default actions for clicking on a song are very much justified."
    author: "Matt Harker"
  - subject: "Context Info Idea"
    date: 2007-03-14
    body: "First off, great work putting this stuff together in a semi-functional state so quickly.  I love how it makes the context stand out and much more useful.\n\nHowever, I always thought that the context would be most useful under the playlist, maybe with a 50/50 vertical layout.  It would imply a very nice \"Master/Details\" relationship between them. It would also provide a nice fat area to display the context so one could fit a good album pic or lastfm style album collage.  I usually have fairly short playlists and even if I have a long one, I'm only really conerned with the next 5 songs or so. Maybe some customization could be in order for window placement...\n\nGranted, maybe I've just been working with too many database apps recently which have vertical master->detail relationships, but this screenshot is the first I've seen where the details are to the left of the master... usually for hoizontal layout it's to the right.\n\nMy $0.03,\nGood work so far,\nThanks again,\n--Justin\n"
    author: "Justin Noel"
  - subject: "New GUI vs Old GUI"
    date: 2007-03-14
    body: "http://static.kdenews.org/content/the-road-to-kde-4/vol12_4x_amarok.png On this GUI, i think there is wrong direction.\n\nUser likes to search music what they have and add it to playlist. And then reads more info about artist or lyrics if s/he has time. Most time user just listen music and does other things same time.\n\nThat's why it should be easy to search music and add it to playlist and possible save them or add them to mediaplayer etc etc...\n\n-On this new gui, user needs first search music from left side and then if using drag, drag it over whole application to right side panel to get it added. \n\n-Position slider is still way too small and it should be more like kaffeine has, using whole space what can have.\n\n-Playlist search bar should be more closer it and collection search bar\n\n-I dont like to use that \"graphic\" list when something is playing and bars go up/down etc. I would suggest that if user takes it away, it will disapear. And it position could be much better.\n\n-Play/Pause, Stop, next, etc. Buttons are on left upper corner when playlist is on right side and it most control buttons are under it. What about making playlist + action buttons more as player by moving play/stop... buttons to over playlist if you like to have three panel view?\n\n-Playlist save, undo, redo etc, looks good\n\n-It's nice that Collection and Information panel now can be seen together. That info panel should not take so much space, user likes to see own collection and have playlist infos like name, artist, album, time and someone likes have rating too.\n\n-How about possibility to remove that status bar from down from taking space? Now there is info about playlist playtime and position slider.\n\n-Volyme slider could be near play/stop... buttons and i would like to see theme about that too...\n\n-More configuration options for user about panels, like usually KDE apps have. Right click > Edit toolbars etc... and option to move them...\n\nJust my opinions... (ps. sorry about bad english) "
    author: "Fri13"
  - subject: "Re: New GUI vs Old GUI"
    date: 2007-03-17
    body: "Hello.\n\nFirst off, The context pane is going to see some gigantic enhancements in 2.0--We have big plans for it.\n\n\"-On this new gui, user needs first search music from left side and then if using drag, drag it over whole application to right side panel to get it added.\"\n\nThis is a problem we have been trying to figure out the best solution to--rest assured we know that it is a problem and are planning on doing something, its just a matter of testing out possibilities until we see what works best.\n\n\"-Position slider is still way too small and it should be more like kaffeine has, using whole space what can have.\"\nI don't really agree with this.  We are planning on moving the position slider, but I don't think that it should take up the entire horizontal space--Would a jump to dialog work for more precise seeking instead?\n\n\"-Playlist search bar should be more closer it and collection search bar\"\nI think its obvious that that search belongs to the playlist but maybe I'm wrong :)\n\n\"-I dont like to use that \"graphic\" list when something is playing and bars go up/down etc. I would suggest that if user takes it away, it will disapear. And it position could be much better.\" \n\nThe plan is for all of the toolbars to be fully configurable as they were in 1.x\n\n\"-Play/Pause, Stop, next, etc. Buttons are on left upper corner when playlist is on right side and it most control buttons are under it. What about making playlist + action buttons more as player by moving play/stop... buttons to over playlist if you like to have three panel view?\"\n\nThe top left corner is a fairly important corner of the app--it tends to grab attention and be the one where important things go.  For us that is playback control.  Don't forget we have keyboard shortcuts for power users :)\n\n\"-Playlist save, undo, redo etc, looks good\" \nGreat!\n\n\"-It's nice that Collection and Information panel now can be seen together. That info panel should not take so much space, user likes to see own collection and have playlist infos like name, artist, album, time and someone likes have rating too.\"\n\nIt may look bad now, but we are planning on making the information panel much much cooler!\n\n\"-How about possibility to remove that status bar from down from taking space? Now there is info about playlist playtime and position slider.\"\n\nIts on my todo list :) I want it to go away, its just a matter of finding a home for the information it still holds.\n\n\"-Volyme slider could be near play/stop... buttons and i would like to see theme about that too...\"\n\nSwitching volume/analyzer might not be a bad idea.. i'll try it and see how it looks.\n\n\"-More configuration options for user about panels, like usually KDE apps have. Right click > Edit toolbars etc... and option to move them...\"\n\nWe have decided that we would rather not make everything configurable--The toolbars will allow you to decide what are on them, but as for repositioning parts of the app, its probably not going to happen."
    author: "Dan"
  - subject: "future of kde !!!"
    date: 2007-03-14
    body: " \" Amarok on these platforms will single-handedly do more for awareness of KDE as a multi-OS development platform than any other application \"\n\n i am a happy guy now, i am less concerned about the future of kde project, if if gnome win, kde is always a very wonderful tool to make multi platform software.\n\nit is really funy now to see that the only part of kde which is linux specific is kdebase/workspace \n\ngreat keep the good job"
    author: "djouallah mimoune"
  - subject: "Re: future of kde !!!"
    date: 2007-03-14
    body: "gnome can't \"win\", because both desktop attract different people with different ideals. :)"
    author: "Mark Hannessen"
  - subject: "The new UI is awful"
    date: 2007-03-15
    body: "The best approach is to take the nice things about the mock up gui and integrate them into the old ui or modify aspects of it.  The particularly annoying feature is that to add specific songs from specific albums is a serious mouse moving and clicking exercise.  What I would like is integration of a video manager whose collection can be maintained separately but whose contents can be added to any playlist with e.g music.  Implementing this with the new phonon et al stuff is more than enough to justify a 2.0 however if you would like to go over the top you can add support for jack plugins whenever the underlying backends provide access to jack.  Now you don't have to reinvent the wheel and you can actually produce a product that can be simple for the casual user, yet powerful with even per song or per album effects and filters.  Come to thing of it I have not seen anything like it unless I run a menagerie of applications that require constant attention.  Now tack on my linux-input-layer rf remote and my computer can do what no known affordable multimedia equipment dare tackle :)"
    author: "Coward"
  - subject: "thanks"
    date: 2007-03-16
    body: "Unfortantly no matter what those building this do, there will always be people who hate it, or like it.  Frankly I don't care what it looks like as long as it plays my music.\n\nThanks for the work all do on amarok.  "
    author: "R J"
  - subject: "Suggestions"
    date: 2007-03-16
    body: "I think, as other people said, that it may be good to focus on collection management.\n\nI suggest to allow user to manage collection overriding tags. For example, I have a lot of single songs from different artists and albums on a directory that I call \"various\". So, I would like to have all those songs in the collection udner a \"Various\" section (I could edit all tags from those files ant put \"Various\" as artist, but I'll loose artis real information).\n\nI also have a large directory with albums and songs that I haven't listened yet, or I want to examine with more detail until decide to keep them or drop. It would be nice to be able to tag all thos files (or just a directory) with a \"testing\" label or something like that. Sometimes I want to hear new music, and that directory is the best place, but I can't recognize those files in the collection view.\n\nIt may be useful to be able to treat albums as songs in, for example, aleatory playback o suggested playback. Amarok would suggest to the user a whole album, instead single songs. Sometimes I want to hear a complete album, without the dinamic changes that I feel when playlist is jumping over different songs and music styles.\n\n\nAnd, of ocurse, a tag system for taggins songs and albums, wich could be used to generate user playlists.\n\n\nThanks for this great audio player (also video player soon), it's really outstanding.\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    author: "Git"
  - subject: "JuK"
    date: 2007-03-17
    body: "what about juk?\n\nwill there be new versions? i don't need an online store, wikipedia stories, cover art, lyrics are any of that other stuff.\n\ni just want a kde-media-player with good playlist handling and a powerul tag editor. the last versions of juk already sucked very much (the cover art stuff, broken engines, high memory usage...).\nit seems juk has been completely neglected since the amarok hype :("
    author: "Hannes Hauswedell"
  - subject: "Amarok 2.0 look"
    date: 2007-03-28
    body: "I like a lot when it comes song controls. amaroks 1.* had controls below the playlist, but I am glad that they moves to top, as my eyes look controls always from top of the programs. \n\nWhat I hope:\n\nI use only the playlist. So I need configuration to amarok 2 that can hide those 2 panels on the left and middle. It is simple as this: I just listen music amarok hided. When I open it, I'll just change a song in 10 000 song playlist and add some priority to some songs and close amarok again to continue work.\n\nWhat I dont like:\n\nMostly that the new look feels like WMP, thats why I used winamp in windows. Winamp offers just the playlist, with needed funcionality arround playlist.\nThis is more the reason why I dont like itunes. I dont like that information about song just explodes to my eyes when I open the program.\n"
    author: "Jeroi"
  - subject: "Some people.."
    date: 2007-04-21
    body: "Some people are so afraid of change it's scary.\n\nI like then new look thus far.\n\nIt's interesting, unique, and fresh.  My only suggestion would be to work on the playlist more...make it easier to navigate through.  Just keep in mind, the playlist is very important, and users need to be able to use it and work with it simply.  It also needs to be flexible. \n\nI like the progression though.  Good work."
    author: "Raven"
  - subject: "too cluttered"
    date: 2007-05-27
    body: "IMO, it's too cluttered. 3 columns takes up way too much space. What I love about the current interface is that the one column to the left houses so many sections. So you have space for your playlist while still being given a ton of features. Look at how scrunched the playlist is (you can't even read the column names!) I like to have, title, artist, time, album and genre displayed, how would I fit all of this without it looking hideous? I know this is far from a final design, but it was posted in order to receive feedback, and I have to say I don't like it much.\n\nBut I trust the Amarok devs will come up with something inventive and user-friendly in the end, they are a great bunch."
    author: "axis"
  - subject: "The new UI made me hesitate wether I would..."
    date: 2007-07-15
    body: "choose Amarok or not. I want a simple overlook of my music where I in one glance can see title, artist, rating, genre and own comments on a list of songs. Not at one song at a time. It is the overlook that helps me like for example when I am in charge playing music at a party. I then need to see as many songs as possible in a simple list. Don\u00b4t have time to click on every other song when planing. I also need to be able to have a column with my own comments about like pace, good for dancing etc etc.\n\nIf I get the above and if Amarok comes for Windows, then it will be easier for me to change to Linux one day, which I for sure want to. I see no future in a monopolistic Windows."
    author: "Herwet"
  - subject: "the new ui isn't good..."
    date: 2008-02-16
    body: "i don't like the mock up. the central pane is irrelevant. it only only degrades its usability...\n\nkeep it up!"
    author: "adredz"
  - subject: "I love Amarok but....."
    date: 2008-05-28
    body: "Amarok is sweet to use... but are u all afraid to really redesign the GUI??\ni dont see much difference between the two.... all i see is just windows and tabs being re-arranged. if u going to make remake Amarok why not make a new and easy to use GUI.. that would be great and it would compliment KDE4"
    author: "andykriss"
---
This week we'll take a brief look at some of the many features that are making their way into Amarok 2, which is the development branch for <a href="http://amarok.kde.org/">Amarok</a> in KDE 4. The features discussed are all in-progress features which have reached varying stages of completion. Read on for information about Amarok's engines (including <a href="http://phonon.kde.org/">Phonon</a>), UI changes, changes to the Magnatune music store, OS X support, and more.



<!--break-->
<p>A few weeks ago, The Road to KDE 4 featured an article on <a href="http://dot.kde.org/1170773239/">Phonon</a>. When that article was published, work on Amarok 2 had not yet started, but the Phonon developers were keeping Amarok very much in mind as they designed the Phonon libraries.</p>

<p>You see, in Amarok 1.x, the developers had to maintain separate playback engines for xine, gstreamer, aKode, etc. This was somewhat of a hassle to keep all of these engines up to date, and in some cases, they were very much a moving target, forcing them to focus on only a single engine, xine. And other programs, like Noatun had to re-implement these backends so there was much duplication of efforts. For KDE 4, the Phonon interface is designed so that programs like Amarok don't have to worry so much about the backends anymore and can concentrate on the other parts of the application. According to the Amarok developers, implementing a Phonon backend took all of 90 minutes before it was usable, and a few extra hours before it was fully implemented. When using Phonon, applications can playfiles over network protocols thanks to KIO, so we'll see what happens as the Amarok team explores this feature in more detail.</p>

<p>So Phonon is now working in Amarok 2. The old engines were also ported over, and in particular, the old xine engine is still being actively developed and they haven't decided to drop their old engines. Since Amarok 2 has only been in development for a few weeks, it is too early to decide to drop their existing engines such as xine, which has served Amarok very well in the past.</p>

<p>One of the side effects of using Phonon is that Amarok can access the video playback functions of the underlying engines where they exist, such as in the phonon-xine engine. They have added rudimentary support for video playback in Amarok, but it's designed to complement audio playback, rather than supplant more involved video players like Kaffeine. The idea is that if you have a music video in your collection, and want to use Amarok to play music from that video, then the video stream would be considered as a Visualization for that music. Having video support will in no way hinder your Amarok audio experience in version 2. According to Dan Meltzer, adding initial video support was a whole seven lines of code using Phonon.</p>

<p>Of course, thanks to KDE 4's newfound portability, Amarok is able to run on other platforms, not just Unix/X11. The development version has already made its first appearance on OS X thanks to the work of Benjamin Reed. Porting to Windows as well is underway, although I don't have a screenshot of that progress.</p>

<p align="center"><a href="http://static.kdenews.org/content/the-road-to-kde-4/vol12_4x_mac.png"><img src="http://static.kdenews.org/content/the-road-to-kde-4/vol12_4x_mac_thumb.png" alt="Amarok 2 on Mac" /></a> </p>

<p>I personally think that having Amarok on these platforms will single-handedly do more for awareness of KDE as a multi-OS development platform than any other application that currently exists for KDE, since the program is head and shoulders above so many other media players out there.</p>

<p>But they wouldn't have a good reason to call it 2.0 unless there were some really big changes happening, and there are.</p>

<p>Amarok is in many ways inspired by XMMS, which in turn bears a lot of resemblance to Winamp. Basically, it's a music player with a multi-column playlist that displays information from tags contained within the media files. Now, these multi-column playlists have not really changed much over the last decade, except that different applications have added interesting ways to sort, filter, and edit tags. Amarok is particularly good at sorting and filtering, and half decent at tag editing (see <a href="http://developer.kde.org/~wheeler/juk.html">JuK</a> as an example of a player with an amazing tag editor). But none of these functions really require Amarok to present its playlist in a rigid column format, except for traditional reasons. So as part of the UI reworking for Amarok 2, the playlist is seeing a rebirth of sorts. While it still lists Tracks, Titles, and other tags, it will no longer be constrained by the old fashioned playlist column format.</p>

<p> This requires a picture to properly describe, so here is a mockup showing the concept in action.</p>

<p align="center"><a href="http://static.kdenews.org/content/the-road-to-kde-4/vol12_mockup.png"><img src="http://static.kdenews.org/content/the-road-to-kde-4/vol12_mockup_thumb.png" alt="Amarok 2 Mockup" /></a></p>

<p>You may first wonder "Where is the playlist?" as I and a few of my peers on IRC initially wondered, but if you look closely, the list on the right is actually the playlist, only it's broken free from its old format. So now, when your file is missing some tags, the playlist will simply adjust around those missing tags, smartly displaying what information it does have.</p>

<p>Also featured prominently in this screenshot is the middle pane. This pane is the focus of Amarok 2, as they try to give you more information about your current track, and allowing you to "Rediscover Your Music" as their motto goes. The leftmost column will function much as it used to, except with the "Context" information moved to the centre. Of course, as is KDE tradition, much of this will be configurable.</p>

<p>Here is a shot of Amarok 1.4.5 showing its current layout for comparison as well as a shot of the development branch of Amarok showing off some of the progress they've made in moving user interface elements around. The mockup above is the targeted UI they wish to achieve, but there will be bumps and changes along the way as they find what does and does not work well.</p>

<p align="center"><a href="http://static.kdenews.org/content/the-road-to-kde-4/vol12_356_amarok.png"><img src="http://static.kdenews.org/content/the-road-to-kde-4/vol12_356_amarok_thumb.png" alt="Amarok 1.4.5 in KDE 3.5.6" /></a></p> 

<p>And now another screen the Amarok 2.0 development version, this time on Linux. Keep in mind that Amarok 2 has only been in development for a single month, and work is still ongoing.</p>

<p align="center"><a href="http://static.kdenews.org/content/the-road-to-kde-4/vol12_4x_amarok.png"><img src="http://static.kdenews.org/content/the-road-to-kde-4/vol12_4x_amarok_thumb.png" alt="Amarok 2 in KDE 4 on linux" /></a></p> 

<p>One of the most promising of Amarok's features is <a href="http://magnatune.com/">Magnatune</a> store integration. According to Wikipedia: Magnatune is an independent record label which aims at treating both its musicians and its customers fairly. Users can stream and download music in MP3 format without charge before making a decision whether tobuy or not. Music files sold by Magnatune do not use any form of Digital Rights Management to prevent customers from making copies of music files they have purchased; and actually encourage buyers to share up to three copies with friends.</p>

<p>Amarok debuted with Magnatune support (for both audio streaming and purchase) in version 1.4.4. Since then, the team has received many emails from other stores interested in being added to Amarok as well. But in Amarok 1.4, the developers have been busy polishing up Magnatune support and had no manpower to start bigger tasks. With Amarok 1.4.5, the second version of the Magnatune store was released and developers are really pleased with that version. It works well, and generates a small but growing number of sales for Magnatune.</p>

<p>So it is time to move on. Nikolaj Hald Nielsen, the main developer of Magnatune store, is planning to move things up a higher level by providing a service framework for all streaming music sources. This service framework could, and is in part intended to, be used as a starting point for adding other stores, and will provide a large amount of basic functionality with regards to adding previews to the playlist, displaying data about selected items, and similar "read only" functionality. As a prof of concept, a service that can be controlled using scripts have been implemented on top of this framework. Currently there is no intent to make a framework for the more store specific functionality (purchasing, parsing of info from websites), simply because each store has their own unique way of handling these things. Nevertheless, it's a great step forward for unified streamed music support in Amarok, and in fact, the <a href="http://musicsojourn.com/Cat/Streams/index.htm">CoolStreams</a> service has been already ported to this new framework (as a rubyscript), and a Shoutcast browser is coming along.</p>

<p>Now, with the Magnatune store well received and with the possibility of using the service framework, it might be time to contact some of these interested music stores again.</p>

<p>Here is a screenshot showing the experimental "Cool Streams" ruby script running on top of the scriptable service:</p>

<p align="center"><a href="http://static.kdenews.org/content/the-road-to-kde-4/vol12_4x_services.png"><img src="http://static.kdenews.org/content/the-road-to-kde-4/vol12_4x_services_thumb.png" alt="Services browser in amarok 2" /></a></p> 

<p>If you'd like to get involved in the Amarok 2 development process, you will need to set up a KDE 4 development environment. There are instructions for using SVN available at the <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">KDE TechBase website</a>, or use the <a href="http://kdesvn-build.kde.org/">kdesvn-build</a> program to automate the whole thing. The Amarok developers accept patches, and will hook you up with SVN access if you require it. They also need help from artists, testers, and people to offer user support via the #amarok IRC channel on the freenode network.</p>

<p>Ladies and Gentlemen, Amarok 2 is progressing very rapidly. To quote Mark Kretschmann, Amarok's lead developer: "If development continues at this pace, we'll be done with Amarok 3 by the time KDE 4 is out ;)" Be prepared for something awesome, as you've come to expect from the Amarok team.</p>

<p>To stay up to date with Amarok news, check out the <a href="http://ljubomir.simin.googlepages.com/awn">Amarok Newsletter</a> published by Ljubomir Simin. Special thanks for his contributions to this article. It's my first experience co-authoring, and it went very smoothly. :)</p>
