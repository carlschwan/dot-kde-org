---
title: "KDE 3.5.7 Improves PIM and More"
date:    2007-05-22
authors:
  - "sk\u00fcgler"
slug:    kde-357-improves-pim-and-more
comments:
  - subject: "\"KMail\" links to bad page"
    date: 2007-05-22
    body: "The \"KMail\" link in the announcement links to a page that is both broken and marked deprecated. http://kontact.kde.org/kmail/ looks much better."
    author: "Adhominem"
  - subject: "Re: \"KMail\" links to bad page"
    date: 2007-05-22
    body: "Thanks for pointing that out. I've removed the broken content and made stuff redirect automatically. "
    author: "Adriaan de Groot"
  - subject: "Blackberry"
    date: 2007-05-22
    body: "Cool, but, when will I be able to sync my blackberry to the K suite?!?"
    author: "EMoShunz"
  - subject: "Re: Blackberry"
    date: 2007-05-22
    body: "I think not: http://www.opensync.org/wiki/DeviceCompatibilityList"
    author: "Martin"
  - subject: "Re: Blackberry"
    date: 2007-05-22
    body: "OK -- your question was not \"if\" but \"when\".\n\nThat's easy. As soon as you implement it. Giving a blackberry to a developer *might* also do the trick."
    author: "Martin"
  - subject: "Re: Blackberry"
    date: 2007-05-22
    body: "hmm...\nI may be able to gain access to a blackberry I could give to a developer, but I find it hard to believe that not a single linux hacker out there has a blackberry..."
    author: "EMoShunz"
  - subject: "Re: Blackberry"
    date: 2007-05-22
    body: "Well, restrict that further to \"a single KDE hacker with spare time\" and you might have your answer :)"
    author: "Paul Eggleton"
  - subject: "Re: Blackberry"
    date: 2007-05-23
    body: "I think you could do that already if you are willing to try the alpha opensync plugin that is part of barry since the latest v0.7 release. I have not tried it yet...\n\nBarry is a project to allow syncing of blackberries. \nSee: http://sourceforge.net/projects/barry\n\n\nYou can also use barry's bcharge to charge a blackberry (already merged into the newest kernel, not required there anymore).\n\nGreetings\n\nMoritz"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Blackberry"
    date: 2007-05-23
    body: "That is some great information, thanks!\nbtw...which kernel has the upgrade? I am running kubuntu 7.04 and I can not charge."
    author: "EMoShunz"
  - subject: "Re: Blackberry"
    date: 2007-05-28
    body: "http://kernelnewbies.org/Linux_2_6_21\n\nsearch for \"charge\" - so, the answer is, \"2.6.21\" :)"
    author: "richlv"
  - subject: "Highs and Lows"
    date: 2007-05-22
    body: "Thanks for another release and kudos to all the maintainers for, well, maintaining KDE 3.5 despite the effort to get KDE 4 out of the door :-) \n\nHopefully KDE 3.5 will receive further updates in the future as I suspect that business owners and governments will hesitate to move to KDE 4 for some time and with an almost feature-freeze the current branch is shaping up to be a rock-solid working environment.\n\nI was a little disappointed that kmail still does not support web.de email accounts via IMAP. With all the announced fixes I was hoping that this one would be among them. Ah well, one can still hope that one day I'll get rid of the behemoth that is Thunderbird ;-)\n\nCheers !"
    author: "Matthias"
  - subject: "Re: Highs and Lows"
    date: 2007-05-22
    body: "KMail + web.de email accounts + IMAP works fine for me since ages. Sometimes there are some timeouts, but normally it's works like a charm."
    author: "Dunedan"
  - subject: "Re: Highs and Lows"
    date: 2007-05-22
    body: "The same is right for me. I also use KMail and Web.de since this year and had no problems."
    author: "mathias (one t)"
  - subject: "Re: Highs and Lows"
    date: 2007-05-22
    body: "Ah well, must be something wrong with my configuration then. The authentification always fails and I never really found out why. I blamed KMail since Thunderbird and M2 work like a charm."
    author: "Matthias"
  - subject: "IMAP for web.de"
    date: 2007-05-22
    body: "In case that the \"it works for me since ages\" comments are not sufficiently helpful:\nUse server imap.web.de on on port 993 with the following security options\nencryption: SSL,  Authentication: text\n\n"
    author: "Stefan"
  - subject: "fish://"
    date: 2007-05-22
    body: "Does anyone know if fish:// and sftp:// are still broke in 3.5.7?  This broke just this weekend (with a Gentoo update invorporating KDE \"fixes\" from upstream).\n\nSee:\nhttps://bugs.kde.org/show_bug.cgi?id=145123\n\nand:\nhttp://forums.gentoo.org/viewtopic-t-556818-highlight-.html\n\nThanx!\nM.\n"
    author: "Xanadu"
  - subject: "Re: fish://"
    date: 2007-05-22
    body: "(I guess I should've finished reading the Changelog(s) before posting...)\n\nThere's no mention of \"fish\" or \"ssh\" in the short & pretty Changelog or the longer text one.  I guess fish/sftp has not been fixed in 3.5.7?\n"
    author: "Xanadu"
  - subject: "Re: fish://"
    date: 2007-05-22
    body: "I think it is a problem specific to Gentoo.\n\nI'm running KDE on Debian and fish:/ works perfect.\n\nI've attached a snapshot"
    author: "Ritesh Raj Sarraf"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "Why do you specify port 22? Is this to seem cool?"
    author: "Joe"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "Whats wrong with specifying a port in a url?"
    author: "whatever noticed"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "It's a bit unnecessary."
    author: "Tim"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "In other words: Nothing is wrong.\n"
    author: "cm"
  - subject: "Re: fish://"
    date: 2007-05-22
    body: "Gentoo specific problem, as it works for me on other distros.  Bug your packager or check if gentoo has a KDE forum someplace.  I think they have an irc channel on freenode, #gentoo-kde or somesuch, but I could be wrong."
    author: "Troy Unrau"
  - subject: "Re: fish://"
    date: 2007-05-22
    body: "If you would have had a look at the bug report, you'd have noticed that it's not Gentoo specific."
    author: "Carlo"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "I actually RTFBR and it's specific to 64-bit. That might be mentionable. Stops all the whining."
    author: "Joe"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "I have only 32-bit machines, as is also mentioned in the kde bug page and the gentoo page I linked to.  The problem is there too.  I hate to be a jerk, but:\n\nRTFBR.\n\n:-)\n\n\n\n\n"
    author: "Xanadu"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "I'm reading the bug report now, and see nothing saying it's not 64bit specific.  But it looks like a beast to track down, so it may be some time before you see a fix of any sort.\n\nAnd it works perfectly here, 64bit."
    author: "Nicholas Robbins"
  - subject: "Re: fish://"
    date: 2007-05-22
    body: "sftp:// works for me both in KDE 3.5.6 and 3.5.7."
    author: "Mark"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "file:/\nremote:/\n\netc. does not work anymore with Dolphin but I havent's rebooted yet."
    author: "hag"
  - subject: "Re: fish://"
    date: 2007-05-23
    body: "I have kubuntu feisty 64bit edition and I could not replicate this bug under 3.5.6 or 3.5.7. Just wanted to give you that as a datapoint. I wonder why some have this as a 64bit only issue but I am definitely running a 64bit compiled kde under a 64bit linux system without these issues. Got to love tracking down weird bugs. Just so you know I tested fish and sftp to another linux server that I have."
    author: "Kosh"
  - subject: "Re: fish://"
    date: 2007-05-25
    body: "If you (or anyone reading) check back with the bug report(s), I updated it with this finding:\n\nIf you're running kernel 2.6.21 it's broke.  With .19 it's fine.  I didn't have .20 on any of my machines so I can't comment on that version.\n\n"
    author: "Xanadu"
  - subject: "KAddressBook, KOrganizer and KAlarm received atten"
    date: 2007-05-22
    body: "KAddressBook is still an applications with many many bugzilla entries and the question is why? The applications looks very simple.\n\nIs it really well designed?"
    author: "Ben"
  - subject: "Re: KAddressBook, KOrganizer and KAlarm received atten"
    date: 2007-05-23
    body: "Why don't you go find out instead of asking nebulous questions in forums?"
    author: "Joe"
  - subject: "Composing HTML messages"
    date: 2007-05-23
    body: "Is the philosophy against composing HTML messages changing at all with the new version?  I would really like to be able to add proper hyperlinks to my messages.\n\nhttps://bugs.kde.org/show_bug.cgi?id=102924"
    author: "Axiom"
  - subject: "Re: Composing HTML messages"
    date: 2007-05-23
    body: "  I would pleased if Kmail would allow me to forward an HTML mail.  Right now, the only way I can find to do so is to forward a received HTML email is to do so as an attachment.  But that retains the originating senders address.  I don't want that, I want to respect their privacy.  We have enough #@&! spam already without needlessly exposing people's email addresses.\n\n  If forwarding as an attachment stripped the headers, perhaps as a separate option, that would suffice.\n\n--\nCheers,\nRob"
    author: "Rob Fargher"
  - subject: "SuSE 9.3"
    date: 2007-05-23
    body: "No SuSE 9.3 rpms available anymore :(\nSeems like it's time for me to upgrade my system... \n"
    author: "someone"
  - subject: "Re: SuSE 9.3"
    date: 2007-05-23
    body: "You better do, there is also no security fixes support anymore for it."
    author: "Anonymous"
  - subject: "New window links"
    date: 2007-05-23
    body: "\u00abA new and interesting usability feature in KHTML makes the mouse pointer indicate if a link wants to open a new browser window or not.\u00bb\n\nCool, though I would be more interested in KHTML ignoring the open in a new window  links and let me decide."
    author: "blacksheep"
  - subject: "Re: New window links"
    date: 2007-05-23
    body: "Agreed, it's an ill-thought feature.."
    author: "renoX"
  - subject: "Re: New window links"
    date: 2007-05-24
    body: "I changed everything to open in new tabs. much nicer, imho."
    author: "Chani"
  - subject: "Re: New window links"
    date: 2007-05-24
    body: "That, plus you can choose \"Open in this window\" in the context menu for links that would normally open in a new window or tab. Very handy, sometimes."
    author: "Jakob Petsovits"
---
The <a href="http://www.kde.org/">KDE
  project</a> today <a href="http://kde.org/announcements/announce-3.5.7.php">announced the immediate availability of KDE 3.5.7</a>,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. This release brings a renewed focus to <a href="http://pim.kde.org/">KDE-PIM</a> applications.
  <a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a>,
  <a href="http://kontact.kde.org/korganizer/">KOrganizer</a> and
  <a href="http://pim.kde.org/components/kalarm.php">KAlarm</a> received
  attention with bugfixes, while
  <a href="http://kontact.kde.org/kmail/">KMail</a> additionally
  witnessed the addition of new features and improvements with both interface work and
  IMAP handling: it can manage IMAP quotas, and copy and move all folders. Read on for more details.



<!--break-->
<p>
KDE now
  supports <a href="http://l10n.kde.org/stats/gui/stable/">65 languages</a>,
  making it available to more people than most non-Free
  software and can be easily extended to support more languages by communities who wish
  to contribute to the Open Source project.
</p>

<p>Several other applications have seen feature improvements:</p>
<ul>
<li>
  <a href="http://kpdf.kde.org/">KPDF</a> shows tooltips for links when
  hovering over them, correctly displays more complex PDF files (like this
  <a href="http://kpdf.kde.org/stuff/nytimes-firefox-final.pdf">Firefox
  advertisement</a>) and reacts to commands for opening the Table of Contents pane.
</li>

<li>
  <a href="http://uml.sourceforge.net/">Umbrello</a> now can generate and
  export C# Code and has added Java 5 generics support.
</li>

<li>
  <a href="http://www.kdevelop.org/">KDevelop</a> gets a major version upgrade
  to version 3.4.1. New features include much-improved Code Completion and
  Navigation, a more reliable debugger interface, Qt 4 support and better
  Ruby and KDE 4 development support.
</li>
</ul>

<p>In addition to the new features there were many bug fixes across the board,
  especially in the <a href="http://edu.kde.org/">Edutainment</a>
  and <a href="http://games.kde.org/">Games</a> packages and
  <a href="http://kopete.kde.org/">Kopete</a>. Kopete also received a major performance improvement for its chat windows.</p>

<p>
  As KDE users have come to expect, this new release includes continued work
  on KHTML and KJS, KDE's HTML and Javascript engines. A new and interesting
  usability feature in KHTML makes the mouse pointer indicate if a link wants
  to open a new browser window or not.</p>

<p>Packages are available for <a href="ftp://ftp.archlinux.org/extra/os/i686">Arch Linux</a>, <a href="http://pkg-kde.alioth.debian.org">Debian Unstable</a>, <a href="http://kubuntu.org/announcements/kde-357.php">Kubuntu Feisty</a>, <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.7/Mandriva/">Mandriva 2007.1</a>, <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.7/Pardus/">Pardus</a> and <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/">openSUSE</a>.  You can also <a href="http://kde.org/info/3.5.7.php">get the source directly</a> or compile with <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>.</p>

<p>For a more detailed list of improvements since the
  <a href="http://www.kde.org/announcements/announce-3.5.6.php">KDE 3.5.6 release</a>
  on the 25th January 2007, please refer to the
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_6to3_5_7.php">KDE 3.5.7 Changelog</a>.</p>


