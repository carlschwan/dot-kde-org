---
title: "KDE-EDU 4.0 Polishing on Saturday"
date:    2007-09-11
authors:
  - "acid"
slug:    kde-edu-40-polishing-saturday
comments:
  - subject: "Maybe wrong priorities ?"
    date: 2007-09-11
    body: "Don't take this as bitching, but ... who cares about KDE-EDU ? I just found out what KGeography is. Anyways, cheers to all ! :)"
    author: "Nick L."
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "perhaps all the people who use those apps? or the developers who work hard on them? the parents with kids who use Free Software and would like some educational tools to use? \n\nhave you even seen kalzium or kstars or marble or kiten any of the other kdeedu apps? they are pretty amazing apps.\n\nremember that this is open source software where people are scratching their itches and having fun. it isn't all about working towards some imaginary Single Vision Approach with a Central Product and Hard Customer Goals. people can and should work on what they want and we, as a community around it, should applaud everyone's efforts.\n\nand if you really, really must find a pragmatic reason for these apps consider that they help make kdelibs better which benefits other apps, introduce free software to young people so that we're not trying to teach them about it when they are adults and allow us to compete it one more software category.\n\nstill, i don't think we need a pragmatic reason to do this other than \"because the edu developers want to\".\n\npeace =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Don't get me wrong. I have no doubt all these are useful applications with a certain target audience and that kdelibs benefits from them. I also agree that the developers of each application can do what they like :D It just seems odd to me that kde 4.0 will lack a few important things, but luckily not state of the art kgeometry and ksudoku :o Still, this is just an observation and not meant to be taken in a bad way. It's just late and I need some company :)"
    author: "Nick L."
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Instead of replying to yet-another silly posting I'll simply say:\n\nThanks kdeedu folks.  You are great and you create useful, fun, quality, important, neat, educational (not just for kids)... well, simply wonderful software.\n\nHugs, Allen\n"
    author: "Allen"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "In what way is what I am saying \"silly\" ? And what position do you hold to judge it and indirectly characterize me in such a way ? Maybe that of \"God\" ? :/ I won't recycle my opinion, it's right on the above post. If you want to misunderstand it, please do so. The fact is : basic functional requirements > toys & more, but everything should be well accepted. Peace."
    author: "Nick L."
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Let's analize why your comment is bad and silly.\n\n1 - You say \"who cares about KDE-EDU\" and then you say \"Anyways, cheers to all\", saying something rude and then something \"nice\" does not make people forgot the rudeness.\n\n2 - You assume that KDE-EDU developers could be \"magically\" reassigned to \"more important parts\", as you call them. But actually i am almost sure all KDE-EDU developers are non-paid developers, so you can't just reassign them because they work on what they like.\n\n3 - Your rudeness analized in point 1 will probably make non-paid developers of point 2 sad and maybe they'll loose motivation, that would mean less developers on KDE-EDU and no new developers on \"mode important parts\", congrats you just made us lose developers!\n\n4 - You assume that KDE-EDU developers are some kind of wizard programmers that can just go to \"more important parts\", read it's non trivial code they have never seen before and fix the code without any problem. That's not true.\n\nSo your comment is bad because of 3 and silly because of 4.\n\nPeace."
    author: "Albert Astals Cid"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "1. You are right here, I'm sorry if I offended anyone though I didn't mean to.\n\n2. Here you are wrong. I do not assume anything, you make false assumptions about things I didn't say. I just made what is called an \"observation\", you obviously interpreted it as a \"suggestion\" or \"trying to boss around\". Your mistake then.\n\n3. Wow. I can't express my opinion because when I do KDE loses developers ? Lame way of thinking. Although you are right again I should choose my words more carefully.\n\n4. Again, I do not assume and I do not assign people to projects. Everyone is part of this only because of his free will and anything he does is enough.\n\nSo, my initial comment expressed a valid OBSERVATION (note the word) in a rude way, for which I apologize. If you choose to look at the finger and miss the moon help yourself ..."
    author: "Nick L."
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: ">So, my initial comment expressed a valid OBSERVATION (note the word) in a rude\n>way, for which I apologize. If you choose to look at the finger and miss the moon\n>help yourself ...\n\nhttp://en.wikipedia.org/wiki/Communication\n\nAn observation with which purpose? Sorry, but IMHO it's worth nothing."
    author: "Long H."
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "> basic functional requirements \n\nand for some people, these apps approach being a basic functional requirement."
    author: "Aaron J. Seigo"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "I disagree. These are not the basic functional requirements of a windowing system. They are extras, just like a word processor or an image manipulation application is extra."
    author: "Nick L."
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Fortunately KDE is not a windowing system, but a desktop environment, and they might very well be basic functional requirements of a desktop environment for some users."
    author: "Marijn Kruisselbrink"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "That's what I actually wanted to say. This board lacks an edit post feature :D Of course KDE is a desktop environment and a variety of applications are supposed to be integrated with it. This doesn't mean it doesn't consist of major and minor components. However, in a distributed development model such as the one deployed by the free software community, putting priorities in every task is a bit hard since developers are not paid workers, but hobbyists. That's why I'm not expecting or demanding things, just pointing out what strikes me as odd :)"
    author: "Nick L."
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-13
    body: "> just pointing out what strikes me as odd :)\nDoing what you want is far from being odd. Sharing it with others appreciating and  using it is very even.\n\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Well do you think it would be the right priority focussing entirely on yet another text editor, yet another terminal emulator, yet another...? Every desktop has its own functional apps for these purposes. For sure Kate is a great editor and Konsole a great terminal (although I now use Yakuake which in turn is based on konsolepart thanks to KDE's clever design) but do you think I can convince e.g. a Textpad (cute Windows editor) user or a die hard xterm friend using KDE just because of Kate or Konsole?\n\nNo. I can only convince people (that don't care about digital freedom initially) using KDE if I can show them _unique_ KDE features.\n\nThere is no other free desktop having such a broad range of educational applications (even not third party apps basing on that desktop). Ever wondered why Edubuntu features KDE-Edu apps as their key component although it is based on GNOME? You can be sure that Edubuntu people would have never made KDE-Edu their key component if there would be something remotely comparable to KDE-Edu.\n\nAnd I am sure the OLPC people will realize sooner or later that there is no way without KDE-Edu if they don't want to provide a really cute hardware with a crappy software. OLPC people are currently stuck with their view that KDE (GNOME as well) is too big, too demanding and too complex for OLPC and thus made the failure not evaluating KDE-Edu. I am sure someday they will realize that KDE is anything else but a big monolitical BLOB you cannot modularize and that it is easier \"sugarizing\" KDE-Edu than rebuilding everything with their own PyGTK based Sugar environment.\n\nEducational software is not your priority but it definitely is for a huge amount of completly new users. KDE is the world leader in free edu software. So do not understimate KDE-Edu's potential for the KDE world domination (TM).\n"
    author: "Arnomane"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Hy...\n\nSome apps from KDE-EDU are used at some brazilian educational projects.\n\nThere is some great tools for spanish learning, I think is missing more apps, but this is under responsability of regional governments make port to local language and demands.\n\nMaybe you don\u00b4t like KDE-EDU due it\u00b4s narrow target and this lack of apps, but it may have a briliant future if the community put big efforts at development of this nice edu tool."
    author: "from Brazil"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "i like kalzium alot\n\nalso i like that KDE as whole concentrates ALSO on education\n\nits a lot nicer to work somewhat in an integrated manner"
    author: "she"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Kalzium was pretty handy last semester when I took Chemistry."
    author: "Ian Monroe"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Tanks. We are always open for suggestions :)"
    author: "Carsten Niehaus"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Panther :)"
    author: "Erwin"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "Why the trolls? WHYYYYYY!?!!!????????\n\nKeep up the good work by the way!"
    author: "Jordan K"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "I use KWordQuiz as an important part of my language studies.......which I should get back to instead of websurfing!\n\nI haven't seen KDE4 yet so don't know whether KWordQuiz will be available...but I assume some vocab tool will be, and I'll be using it.  So I care!\n"
    author: "another language student"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "it saves me a lot of grief too. :) Not in the traditional sense of language learning, but similar - I use it to learn the latin counterparts of taxons. There is no way to learn this good than with constant grind and the random both-way multiple choice is perfect for this."
    author: "sorbusaria"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-12
    body: "yay stupid people and their screwed up priorities?\n\nHow about \"KDE-EDU is a part of KDE from what... 2003 or so? And what is wrong with making applications better? KDElibs are mportant, as well as kdebase, but why should be everything else left behind, huh? Dropping everything else is a good idea? Oh, I know! Let's forget about kdegames, kdenetwork and kdemultimedia. Let's forget about Amarok and all the other nifty gadgets that make KDE what it is today. But let's polish only kdelibs/kdebase. Yes. That sounds like a good idea.\"\nYay for sarcasm? Why don't you step in and help KDE instead of whacking the nonsense around? Like becoming a KDE developer and stop wasting developer's efforts to answer your crap? Don't you think that's a *better* idea?"
    author: "canine"
  - subject: "Re: Maybe wrong priorities ?"
    date: 2007-09-14
    body: "This comment brought out some sensitivity :)  I sort of see where you are coming from though.  I initially read the headline as \"KDE 4 polishing\" and when I read further and saw it was limited to KDE-EDU I was a little disappointed.  That's my fault for misreading the headline of course!  \n\nI certainly wouldn't want to marginalize KDE-EDU.  My disappointment wasn't that it was about KDE-EDU, but that it was _only_ about KDE-EDU (there is a distinction there).  This is a good idea and will hopefully be a great benefit for KDE-EDU.  If it works out well, perhaps we'll see something like it for other KDE components."
    author: "MamiyaOtaru"
  - subject: "Please, port tuxmath to KDE 4.0 :-)"
    date: 2007-09-12
    body: "I am not a developer, that\u00b4s why I don\u00b4t do it by myself.\n\nTuxmath is crazy, I love this apps.\n\nI saw o little 9 y.o. girl screaming and asking for help, struggling to solve that math problems. This edu-game is funny and it must be at KDE-EDU. Maybe the same structure could be used with a similar game for keyboard typing, instead of math problems, letters and words falling down."
    author: "from Brazil"
  - subject: "Re: Please, port tuxmath to KDE 4.0 :-)"
    date: 2007-09-12
    body: "If tuxmath is good, whats the point in porting it? Just run tuxmath. :)\n\nA decent Maxima KDE frontend would be nice, since Maxima is a good alternative to Mathematica but its GUI isn't all that great. Or some sort of computer algebra system anyways."
    author: "Ian Monroe"
  - subject: "Re: Please, port tuxmath to KDE 4.0 :-)"
    date: 2007-09-13
    body: ">>Maybe the same structure could be used with a similar game for keyboard \n>>typing, instead of math problems, letters and words falling down.\n\nWell... you are describing TuxTyping, isn't? The website is a bit confuse and unmantained, bu the program is generaly good, at least for english.\n\nhttp://tuxtype.sourceforge.net/screens/\n\n:D\n\nAnd don't forget to check out Ktouch: http://ktouch.sourceforge.net/\nThat is the best free typing program that I ever seen. One of the reasons that I instaled linux again. Part of KDE-EDU."
    author: "Manabu"
  - subject: "Re: Please, port tuxmath to KDE 4.0 :-)"
    date: 2007-09-13
    body: ":-D hhhuuaauu great... thanks for TuxTyping link.\nI was missing something like that... :-D\nThis is THE game for toeddlers.... \nAny other tip ;-)\n\n\n\nMy wife used Ktouch for typing training when she had a typing test for a public job. Ktouck is great."
    author: "from Brazil"
  - subject: "Re: Please, port tuxmath to KDE 4.0 :-)"
    date: 2007-09-13
    body: "yeah, KTouch rocks."
    author: "jospoortvliet"
  - subject: "Re: Please, port tuxmath to KDE 4.0 :-)"
    date: 2007-09-14
    body: "The best free typing program that I ever saw is called hypertype (www.hypertype.org)\n\nIt needs some more developers, so HELP MAKE IT POPULAR."
    author: "BATONAC"
  - subject: "Re: Please, port tuxmath to KDE 4.0 :-)"
    date: 2007-09-13
    body: "Both tuxmath and tuxtype use SDL, so they are independent of the KDE version.\n\nGlad to hear you like tuxmath. You should also know that most distros seem to include pretty old versions of the program, and there have been a whole host of improvements over the last year. The most current versions of both tuxmath and tuxtype are at http://alioth.debian.org/projects/tux4kids/.\n\n"
    author: "Tim Holy"
  - subject: "Never used KDE-EDU"
    date: 2007-09-12
    body: "I've never used the apps personally, but I've always loved the notion that KDE has committed to education software, which is really important.  As a parent, I look forward to building my daughter her own KDE-EDU box when she is old enough.\n\nAnd apps like Marble, Kalzium and Kiten should make inroads into schools for Linux."
    author: "T. J. Brumfield"
  - subject: "parley"
    date: 2007-09-12
    body: "i would like to say that the first thing, that my little brother has used when he touched kde was all those app in edutainement,( of course beside nexuiz ;)\nif i have a one complain about kde-education and kde applications in general is the naming scheme, that's why i was very happy by the new name of parley.\nso please guys try to be imaginative, surprise us with new sexy names like parley, kde-applications is wider then kde, even wider then linux"
    author: "djouallah mimoune"
  - subject: "KDE EDU"
    date: 2007-09-12
    body: "I understand that KDE Edu packages are there. This is a type of software that is coded, software that is sold and requested on the market. But is it really used? I mean the children are supposed to work with these games but educational stuff is boring and how will you lock down that children don't do something else with KDE. I mean as a ten year old I coded programs. Given the overcomplex task of entering programming today, with all the svn, complex documentation and toolkits I wonder what you would recommend a 11 year old today. How can he or she code KDE?\n\nWhy do you assume that children like simple stuff? Simple memory games with no real learning factor. What is your educational concept behind it?\n\nMy impression always was that Education software is like tomes. You want it but you don't use it."
    author: "Martin"
  - subject: "Re: KDE EDU"
    date: 2007-09-13
    body: "* You might not be typical - not all 10 year olds want to code programs.\n\n* It's not just \"simple stuff\" for children.\n\n* \"is it really used?\" - did you even read the rest of the comments here? \"Some apps from KDE-EDU are used at some brazilian educational projects.\" \"Kalzium was pretty handy last semester when I took Chemistry.\" \"I use KWordQuiz as an important part of my language studies.\"  Again, it's not (only) children using this stuff, and it's not (only) simple stuff involved.\n\nWhat's your \"educational concept\" behind saying that \"simple memory games\" do no good?  Obviously there's more to learning a language (for instance) than memorizing vocab, but I'm sure you can agree that that's a big part of it, and one of the more boring parts at that.  Isn't it nice that someone else has taken the trouble to write software to help with that, and gives it away freely?\n\n\"I wonder what you would recommend a 11 year old today\" - OK, Mr \"I coded programs as a ten year old\", what would *you* recommend?  Nothing says that 10 year olds are going to be \"coding KDE\", but there's more tools now than ever to introduce kids to programming.\n\nYet another example of a \"is this really any good\" whiner... if you've got useful feedback on the \"educational concepts\" that could improve specific software, then improve it!  If you've got ideas for something new, implement them!\n"
    author: "Anonymous"
  - subject: "Re: KDE EDU"
    date: 2007-09-13
    body: "Difficult questions, I mean the point is that we learned a lesson. In the past some actions were perceived as very difficult such as calculation, algebra, logic. But then came AI and we understood that the difficult and complex thing is what we perceive as easy. No wonder, as we are not designed for these tasks. Same for memorizing. You can't avoid it and the real important task it to forget what you don't need. Computers can do \"the other stuff\". Or go to Africa and find out how much you took for granted and what makes the difference.\n\nI wrote several learning programs but then I found out that the pedagogical concept behind flashcards was bad. And I didn't want to use them myself. Or think of the 70th programmed instructions. KDE stuff performs pretty well in the eLearning landscape. It is possible to learn a language in 3 month but not with education software.\n\nOf course educational software programs are important for schools in a kind of procurement checklist fashion. However, it does not make sense to use them and children will find them boring. Of course, in many nations of the south learning means memorizing.\n\nGood concepts from my perspective are tools which assist children to learn. Tools children want to use. Programming in 1988 was simple. You sit down and write your Hello World in minutes. Programming today is overcomplex."
    author: "Martin"
  - subject: "Re: KDE EDU"
    date: 2007-09-13
    body: "First work with script languages - Perl, Python, Ruby, etc. (C64 Basic anyone?).\nThen work up to more complicated & compiled languages (eg. c/c++)\nAdd in make-files as things get more complicated.\nAs you go, learn whatever graphics/sound/data-management stuff you want to.\n\nUnless you want to contribute to a group project, cvs/svn/git, and a bunch of stuff that might make programming harder to get into, isn't necessary.\n\nMuch of learning IS memorizing. \nBasic arithmetic -> 2+2=4, \nvocabulary -> canard=duck, \nspelling/phonics -> \"ce\" makes a \"s\" sound, there/they're/their\ngeography -> Canada is north of USA\nhistory -> Hitler is responsible for some aweful stuff\nPhysics -> F=ma\nmathematics -> all those axioms\ngrammar -> conjugation of verbs, pronouns, types of words...\nsports -> you're not allowed to touch a soccer ball with your hand (in most cases)\n...\nEven in cases where memorizing isn't necessary, it saves a lot of time.\n\nThere are also other apps in KEDU:\nKalzium - chemistry, impressive molecular modelling\nMarble - a globe with satelite imagery (recently added 2D models)\nStep - a 2D physics simulator, impressive stuff\nKig - interactive geometry, excellent for visualizing complex 2D geometry.\n..."
    author: "Soap"
  - subject: "Re: KDE EDU"
    date: 2007-09-13
    body: "I use KStars on a regular basis (beside a very small pocket book of stars). \n\nWhy do I use such an \"irrelevant\" application? Well I like astronomy and I love the dark night sky with the milky way across my head. It is a shame that most people even don't know how the night sky looks like cause of light pollution.\n\nEver wanted to fetch the stars from the sky for a girl? ;-) Previous to your next date with a girl run Kstars and look for some romantic things you can show her later...\n\nSo how can you think that Kstars is a children-only application, how can you think that none uses it seriously?\n\nAh and thee are some much more edu-software. A quick function plotter such KmPlot (provided by KDE-Edu) can safe your day if you have to do some math (but don't have expensive complicated expert apps like Maple/Matematica around).\n\nOr the vocabulary trainer or or...\n\nI'd even say many of the apps are educational apps primarily designed for adult persons and not for kids, because they don't have a silly \"kindergarden user interface\".\n\nSo go and actually test these apps. You will be surprised that they're totally different from your expectations."
    author: "Arnomane"
  - subject: "Re: KDE EDU"
    date: 2007-09-13
    body: ":-o\n\nok..... 10 years old,.... coding.. C++\nand whith 20 y.o. building a rocket to Mars\nand with 30 y.o. discovering the world outside his (or her) house.\n\nMaybe Quake4 could fill this educational special needs.\nhuuunnnnn... why not a Kuake4, the first KDE fsp.. :-)"
    author: "from Brazil"
  - subject: "Re: KDE EDU"
    date: 2007-09-13
    body: "This software is coded mainly by parents or teachers or students themselves. All of it was started to fullfill a need. I started KLettres to get my 4 years old prepared to read for example. Since then I have heard that illettrate adults also use it. Are you in the above category (parents or teachers or students)?\nAnother point: not sure about your background but what most European and American people easily forget is that KDE is world wide and Education is more needed in some countries. I happen to be involved in a charity which motto is about education.\nOur programs are learning helping tools or educational games (hmm, isn't that concept of educational games or mind-training games heavily used for the Wii?)\nAbout making kids code aged 11: well this is not our objective. We have KTurtle which is based on Logo, some teachers use it but that's it. Of course if you coded aged 11 I can see why you see our tools uninteresting...\n"
    author: "Anne-Marie Mahfouf"
  - subject: "Re: KDE EDU"
    date: 2007-09-13
    body: "Very interesting, I didn't know that KLettres was used by illiterate adults.\n\nOf course, being myself a proud contributor to KDE-Edu, I disagree with Martin when he says that education apps are useless. Both the apps for children (KLettres...), and the apps for students (Kig...), are very useful IMO.\n"
    author: "Benoit Jacob"
  - subject: "Re: KDE EDU"
    date: 2008-03-16
    body: "Wow, what a bunch of bunk!!\n\nWhat is learning if not repetition of known factors using variable inputs?  (didn't understand that one?  figure it out, it's meant for you)\n\nI am running KDE Unbuntu (Gutsy Gibbon) on a 1996 Intel 32 bit motherboard with a 550MHz CPU.  Sure, it's a little slow, but is way more friendly and stable than any of microsofts distro's ever were or could be. (Kudos to the worldwide Linux software production family!!)\n\nI have KStars, G-Comprise, KStitch, KTechLab, ChildsPlay, LinuxLetters&Numbers, TuxPaint, and a multitude of other games, videos, and internet links for youth entertainment and education.  All of which are enjoyed and loved by my three nieces and two year old nephew.\n\nNobody can put a price on !CLEAN! education!  The packages released through the KDE distro's are the highest quality, most friendly, and use masterable skills and concepts suited for !EVERYBODY!!\n\nTo all involved in releasing such high quality material I give my deepest gratitude and respect.  The world will never be able to repay you for the things you do to make computing and learning a better experience for everyone!!  And I wait with baited breath for the wonders that are yet to be.  8-)"
    author: "MelonDrift"
  - subject: "Missing Project"
    date: 2007-09-13
    body: "KDE-EDU have a missing project. a timetabling programm is missing, but a very good already is avalible. view http://www.lalescu.ro/liviu/fet/\n"
    author: "me"
  - subject: "Re: Missing Project"
    date: 2007-09-13
    body: "Oh, I've wanted something (but not really searching for it) like that for very long. Thanks for the link, will try it out.\n\nAbout the whole \"Is KDE-EDU useful or not\"? I would answer: No, if you don't use it. But there are people who do, even if you don't belong to that group, it's wrong calling the applications useless or saying that the devs should start hacking on another project.\n\nI use the edu apps very seldom, but when I do, they're really life savers."
    author: "Hans"
  - subject: "Re: Missing Project"
    date: 2007-09-23
    body: "Even when you don't use them, they're useful. They bring in new contributors, who in time might work on other parts of KDE. And they find and fix bugs, and as they are technologically often ahead of the rest of KDE, they help stability and functionality all over the place."
    author: "jospoortvliet"
  - subject: "USELESS RESPONSE"
    date: 2007-09-14
    body: "Don't people have anything better to do then deliver useless criticism?\nAnd I reckon from people outside the user-community.\nI can only take my hat off to you developers, I find your work highly constructive and positive in creating opensource \"FREE\" learning opportunities.\n\nThank You !!!"
    author: "P. Philipse"
  - subject: "thanks to kde-edu developers"
    date: 2007-09-14
    body: "kde-edu apps are really great. thanks to all developers.\nit's these things that get people interested intially in kde :)\n\nbtw, what's the status of kworldclock in kde4 ? is it being ported, and maybe it is being finally moved out of games->toys ? :)\n\nit is a realy nice application, and one that attracts people to kde. we had computer available two years ago in the new years eve, and we had it open all the time. \"hey, it's new year in kamchatka ! raise your glass !\" ;)\n\nand last time people remembered about the nice world map, and asked \"hey, is that cool app still there ?\" =)"
    author: "Richlv"
  - subject: "Off topic whinge about the dot's comment system"
    date: 2007-09-15
    body: "Another article on the dot, another set of comments. And yet again, because of a troll first post, the entire discussion gets hijacked in responding to a troll who we shouldn't be bothering to humour in the first place. It happens on a very high proportion of stories now.\n\nThere are many things I like about the dot, including the way I don't have to have yet another user account for a discussion forum and how you seem to have solved the problem of spam without requiring annoying captcha-solving. But the problem of trolls on here is rapidly making the comments pages not worth reading. Moderation systems always seem to be controversial and get emotional responses but I do wonder whether the dot needs to consider something like that, just to keep the forum worth reading.\n\nAnyway, kudos to all the kdeedu (and other kde) developers who over the years have provided us all with such an excellent desktop."
    author: "Adrian Baugh"
  - subject: "Re: Off topic whinge about the dot's comment system"
    date: 2007-09-15
    body: "Agreed. However there's now little chance that your comment will be read by many people, as this story is days old. You should write to some mailing list, though I don't know quite which. Maybe kde-www ?\n\n"
    author: "Benoit Jacob"
  - subject: "I love kgeography!"
    date: 2007-09-23
    body: "Hehe I wonder what most of the people who say kde-edu is useless would score on the kgeography-\"click division in world map\" test :D\nAnyway big thanks to all kde-edu developers!"
    author: "John"
  - subject: "Flamings are fun"
    date: 2007-09-24
    body: "Hello,\n\nI missed this blog entry and the first answers. The Jos entry did take my attention and then I read all the reactions. \n\nWell, please KDE EDU people take the previous reactions as what they are: funny things based on false assumptions of various types. These people need some education :-) Somebody to write a netiquette of free software philosiphy learning app ?\n\nAnyway, I currently not have much use of the kde-edu apps, but my children advancing in age, I hope that your apps will continue to be so great :-)\n\nContinue you great work, please!"
    author: "Kleag"
---
This Saturday (15.09.) will see the first <a href="http://edu.kde.org">KDE-EDU</a> 4.0 Polishing Day. The aim is to allow direct communication between users and developers. Issues, doubts and new ideas can be discussed, solved and coded in real time. For this purpose, a meeting will be held in <a href="irc://irc.kde.org/#kde-polishing">#kde-polishing</a> from 8:00 to 15:00 UTC. <a href="http://edu.kde.org/khangman/">KHangMan</a>, <a href="http://edu.kde.org/kgeography/">KGeography</a> and <a href="http://edu.kde.org/blinken/">blinKen</a> will be this first meetings subjects. In order to participate, KDE 4.0 Beta 2 or newer is required, but using the <a href="http://www.kde.org/info/3.93.php">precompiled KDE 4.0 Beta 2 packages</a> for Kubuntu, Mandriva and openSUSE or the "<a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a>" CD is fine. Join in &#8212; you can make a difference!





<!--break-->

