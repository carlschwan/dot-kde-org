---
title: "KDE to Have Room at FrOSCon"
date:    2007-08-23
authors:
  - "jriddell"
slug:    kde-have-room-froscon
comments:
  - subject: "Deutschland ..."
    date: 2007-08-22
    body: "Deutschland = germany (in german) for those who don't know..."
    author: "wassi"
  - subject: "Re: Deutschland ..."
    date: 2007-08-23
    body: "...are you serious? LOL! Well, for my part I can remember all C++, PHP, Java and Perl statements and have no problems discussing DBUS in KDE4 but what the heck do the words \"France\", \"Italia\" and \"Espana\" mean again?\n"
    author: "Michael"
  - subject: "Re: Deutschland ..."
    date: 2007-08-23
    body: "you are not average american then :)"
    author: "ac"
  - subject: "Re: Deutschland ..."
    date: 2007-08-26
    body: "Espana? I have no idea what that means.... I do know about Espa\u00f1a though..."
    author: "PuercoPop"
  - subject: "Re: Deutschland ..."
    date: 2007-08-27
    body: "LOL. I just knew someone would bring this up the moment I typed it. Please rememember that not everyone has got a key for this at his fingertips. Have fun entering Beijing in Chinese on your keyboard ;-)\n"
    author: "Michael"
  - subject: "Re: Deutschland ..."
    date: 2007-08-23
    body: "They mean the 'Federal Republic of Germany'. "
    author: "bert"
  - subject: "Re: Deutschland ..."
    date: 2007-08-24
    body: "-- Bundesrepublik Deutschland"
    author: "x"
  - subject: "Re: Deutschland ..."
    date: 2007-08-24
    body: "Well it was nice to learn about vienna being called wien, wenen too...\n\nAnd the first time getting to know about a city called Genf located around the same place a Geneva...."
    author: "boemer"
---
<a href="http://www.froscon.org/">FrOSCon</a> is due to start this weekend, and KDE will have our own room and give several talks.  The <a href="http://wiki.froscon.de/wiki/KDE_room">KDE room</a> will be shared with developers from <a href="http://amarok.kde.org">Amarok</a> and <a href="http://www.kubuntu-de.org/">Kubuntu DE</a>.  There will be talks on Marble, Amarok 2, Kubuntu in Deutschland and in the main track one on <a href="http://programm.froscon.org/2007/track/Desktop/43.en.html">KDE 4</a>.  If you are in the Bonn area, drop by Sankt Augustin this weekend and meet the developers and community.


<!--break-->
