---
title: "KDE 4.0 Release Candidate 2 Out Now, Codename \"Coenig\""
date:    2007-12-12
authors:
  - "sk\u00fcgler"
slug:    kde-40-release-candidate-2-out-now-codename-coenig
comments:
  - subject: "Window button arrangement wastes space"
    date: 2007-12-12
    body: "The gaping hole between the close button (X) and the minimize/maximize buttons needlessly expands application dialogs, especially if these have long titles. This weird spacing is a failed attempt at making it harder to accidentally close a window when in fact you only wanted to minimize it.\n\nThe better solution (and one advocated by Aaron Seigo) is to move the minimize/maximize buttons to the left and bring the title text and application icon to the center. Unfortunately, KWin maintainer Lubos Lunak decided introduce this ugly, wasteful space as a stopgap measure. Now I have the utmost respect for Lubos and his work, but I humbly disagree with this decision on aesthetic and practical concerns. Please move the max/min buttons to the left and bring the title text/icon to the center in the final KDE 4.0 release!\n\nThe thread where this was discussed is:\nhttp://lists.kde.org/?t=119508170500002&r=1&w=2"
    author: "Tray"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "With all due respect (not that much but still) putting the minimize/maximize icons on the left would be really ugly and completely unexpected.  Everyone who has ever used kde or windows is expecting the buttons to be on the right, moving them just to be different (because you can't convince me that its better usability to put them away from the close button) is, imo, not worth it at this point in time.  There are much more pressing bikesheds to paint."
    author: "Dan"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "I actually use the suggestion from aaron, and found it good.\nI think it is a matter of personal taste, but it could make the default value for kde 4.0 as it is not that annoying. Keep in mind that Apple has all buttons at left, windows at right. So people switching still may have to change their habits, however the few people I know who have switched windows -> mac os had no problem."
    author: "phD student that should stop reading the dot"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "Agreed, I was very skeptical at first, but after using the button arrangement Aaron suggested I must say it just works that much better."
    author: "slougi"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "It's not ugly, and it is excellent for mousing efficiency (since the minimize button in maximized apps is right in the corner of the screen).  I've had this setup for years.\n\nI agree it would be a bad idea as a default though.  New users would complain that it's different than windows to no end.  \n\nHowever, I hope that the setting to allow moving of maximized windows is finally off by default in kwin.  I don't know why it's on by default in kde 3, because it completely disregards fitts law and wastes screen space by putting a border around maximized apps.  The window buttons are way harder to hit that way.  Please turn this off by default if it isn't already.\n"
    author: "Leo S"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "> New users would complain that it's different than windows to no end.\n\nThis is KDE 4. I think users expect novelty, and will accept this change because it is apparent that a dangerous button like Close should not be anywhere near the other buttons. I personally haven't heard of a single OS X user complaining that the buttons are on the left side. And in OS X's situation they really don't have a good excuse to make the change beside wanting to be different from Windows. In this proposal we actually have a valid reason for splitting the buttons up."
    author: "AC"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "<blockquote>\nI think users expect novelty\n</blockquote>\n\nSomething can't be expected and novel at the same time.\n"
    author: "Velvet Elvis"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "You're changing his words and you know it.\n\n(Hint: he doesn't say \"Users want expected novelty\")"
    author: "Quintesse"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "Mac OS has always had the close button on the left.\n\nAs did CDE and other early X-Window environments. And Windows 3.x\n\nActually, I think it was Windows 4.0 (aka Cairo, aka Win95) first introduced the close button on the right.\n\nSince KDE has always had the close button on the right, though, I think it's good to leave it there, especially since this is the expected behaviour on most systems nowadays. It used to be configurable for people who want it on the left, and as long as the option is there, I'm fine with the current setup."
    author: "KOffice fan"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "Don't forget about the beloved Amiga! :)\nWorkbench UI also had the \"Close\" button on the left, and \"Minimize\"/\"Maximize\" on the right side.\nAs well as all Mac OS versions prior to OS X, now that I remember.\nIt was THE standard window button layout these days. And it was a good one, I don't know exactly why it was changed.\n\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "The problem with the \"Close\" button on the left is, that it is very near the menue and can accidently be clickt. I used this settings for a few months and after I accidently closed the app some times while I just wanted to access the menu I changed the \"Close\" button to the right and all other buttons to the left and since then I never accidently closed an app."
    author: "hias"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "> The problem with the \"Close\" button on the left is, that it is very near the menue\n\nUnder my proposal the menu (application icon) is moved to the center along with the title text. So the menu and the \"Close\" button are actually far apart."
    author: "Tray"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-13
    body: "sorry, I'n not a native speaker. I didn't meant the menu icon but the menu toolbar with its menues like \"File\", \"Edit\" ... "
    author: "hias"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "Dear all,\n\nfirst of all, thank you to all of you. KDE rocks and I'm excited about KDE4. Please consider my comment below as an alternative opinion for the discussion.\n\nI'm using windows in the office and have problems with changing environments. I'd prefer to have those functions that are executed without thinking consistent even over different OS.\n\nI'm still trying to save/close notepad.exe with ZZ (yes, I'm a VIM freak)\n\nI'm using ctrl-c ctrl-v in Linux instead of the middle mouse button for C&P.\n\nI'm typing 'ls' instead of 'dir' and 'cp' instead of 'copy' when working in a command shell on windows.\n\nPlease, allow me to keep the minimize button in the same place, anything else will drive me creazy (I know, it is configurable, but most people will never know)\n\nReemi"
    author: "Reemi"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "i think that \"put all the buttons in one corner thing\" really came over us in 1995, and i think it is a bad idea. it neither makes the ui simpler, nor prettier nor more efficient, nor more aestethically pleasing. i am definitely for splitting them up.\n\njust my oppinion"
    author: "e_arni"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-14
    body: "The default setup is intended to suit the needs of most users, who are used to the buttons on the right.  Having the close button separated from the two others actually represents a move forward in usability, there's a bit of buffer space so that if you are 2 pixels to the right of the maximize button you won't accidentally close the window, and the majority of KDE users (new users coming from Windows, and previous users) won't have to get used to a complete change in position.   \n\nSecondly, if you do not like the default setup, KDE is and has always been fully configurable.  It takes less than 5 minutes to go into the appearance dialog and drag your close button to the left side. "
    author: "f00fbug"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-16
    body: "all in favour of forever ignoring Dan say \"Aye\"!\n\n\"With all due respect (not that much but still)\"\n\nhow is that necessary?"
    author: "mike"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "This can easily be changed in systemsettings, just tick 'use custom titlebar button positions' in Appearances->Windows->Buttons  and drag out the two spacers.  I suspect the distropackagers will adapt the default style to fix these issues for their users.\n\nBy the way can anyone else confirm that the embedded terminal in konqueror gives no prompt (unlike in kate where it works well)?  I am using the RC2 packages of openSuse released today."
    author: "bobo"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "Thanks for the info, now that I know you can edit it easy I'm happy for any crazy experiment the devs wish to do :)\n\n"
    author: "Ben"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "Isn't this still controlled by the window manager settings, the way it is in KDE3?  In which case, each user can configure it the way they want it?\n\nI manually insert a few spaces in there, for just that reason.  Tried with them on the left, but didn't like it.  The nice thing (at least in KDE3, haven't tried configuring things in KDE4 yet) is that each user can set this according to their preferences."
    author: "Freddie"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "I have been using the 'aaron' method for a few weeks now since the \"try it before you dis it\" argument came up and it's fine except that sometimes when you click for a menu you miss and your window goes where you don't want it (smaller or bigger). The one advantage of them being on the right is that you rarely have menu items under the buttons to the chance of missing your target doesn't result in the wrong action happening.\n\nOn a mac the window buttons are on the left but you also don't have the menu nearby usually either.\n\nYour argument about the space between the close and max/restore button wasting space isn't a very strong one either unless you're on a really low res display where any button would be an issue. ;)\n\nI think the biggest issue here comes from the inconsistency of closing windows. Sometimes clicking the close button quits the apps, sometimes it puts it to the sys tray, and sometimes it leaves a copy running but reset so it launches faster next time. Then there's duplication of the app 'Menu' and a title bar right click and also a 'Close' in both of those. Close should be just that, close the window but don't quit the app.\n"
    author: "fuegofoto"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: ">> Close should be just that, close the window but don't quit the app.\n\nI hate that behaviour on OS X.  Sure it's more consistent, but for most apps it just doesn't make any sense.  I don't want to keep my web browser running when no windows are open.  There's no point to that.  An app like Ktorrent is fundamentally different.  99% of the time it's running in the background, and making it keep on running when you close the window is fine."
    author: "Leo S"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "Sometimes it's nice to have a browser open in a systray like opera does, to stay logged on, all the time i.e."
    author: "Terracotta"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "I put my close button on the left side. This way I never accidently close a windows instead of mini/maximizing it. It takes a little while to get used to, but I'm completely happy with it."
    author: "CAPSLOCK2000"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "As I wrote above, I had this settings a few months and it happened that I closed the app while I just wanted to access the menu, therefore I think it is better to put the \"Close\" button on the right, because there is nothing near and all other buttons on the left, so the worst that could happen is that the window get mimimized/maximized."
    author: "hias"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "I've removed the maximize and menu buttons\nswapped the order of the minimize and close\nand finally added a spacer in between the 2.\n\nWhy? I've never use the maximize I always double click the titlebar which I've set as maximize. I never use the menu button.\nAs for why I swapped the order, When you own an optical mouse and a cat.. you tend to realize quick how jumpy a cursor can be. Anytime I hair would get under the mouse it would ever-so-often jump to a corner of the screen.. a few of those times it would jump up into the upper right hand, right as I was clicking.. This would close the window. Swapping the order made it so the worst that would ever happen was it just minimized. The Space was added to avoid accidentally closing the window instead of minimizing.\n\nI've now ran with this for over a year and I'll never go back. Absolutely love this setup."
    author: "Stephen"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-12
    body: "What's all the fuss about, isn't this what KDE Personaliser is for?"
    author: "Rob"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-13
    body: "What I really like about OSX is the titlebar-button to show/hide the menu/toolbar. Now that's a space saver!! How difficult would it be to introduce a button for menubar rollup/down and one for toolbar rollup/down (heck, this could even be combined into one button. LMB on it will do a menubar show/hide, RMB will do a toolbar show/hide. Finish it all of with a session default configuration and I would be very happy!!"
    author: "Fred"
  - subject: "Re: Window button arrangement wastes space"
    date: 2007-12-14
    body: "IE7, WMP, and many new Windows apps now autohide the menu bar to make the interface look clean.  Press Alt and it magically appears.\n\nAt first I didn't care for it, but it has been growing on me, and the apps do look nicer."
    author: "T. J. Brumfield"
  - subject: "Kickoff questions "
    date: 2007-12-12
    body: "Will the following usability improvements make it until KDE 4.0 final?\n\n1. A resizable Kickoff like in KDE 3.5.X.\n2. Clicking on Applications button/tab will return to the Top-Level of the Application menu.\n\nOther than that I really like Kickoff. If only it would be a little bit more configurable..."
    author: "goran"
  - subject: "Re: Kickoff questions "
    date: 2007-12-12
    body: "depends if patches (by my hands, yours or someone else's) appear in time."
    author: "Aaron J. Seigo"
  - subject: "Re: Kickoff questions "
    date: 2007-12-12
    body: "It would be nice to be able to resize it or make the default size a bit bigger.  On a 800 high display and 100dpi the default size still makes you scroll once you hit the Applications area(tab?). I wouldn't even consider it an issue except that it's the 'not-a-menu launcher' so if it's visible you're using it and not anything else so the screen space it takes up shouldn't be a major issue as long as it fits on the screen. Kind of an organized desktop full of icons... ;)\n\n\n"
    author: "fuegofoto"
  - subject: "Re: Kickoff questions "
    date: 2007-12-13
    body: "oh, i agree. and it will eventually be resizeable at some point. how soon is the question =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Kickoff questions "
    date: 2007-12-12
    body: "http://cofundos.org/\n\nHave a look at it. You can donate money to have someone write the patches for you. Perhaps you can alter Aarons priorites by donating a large enough sum of money. :-)\n\nI've got some money waiting for the release of KDE4 and when that happens I'll donate to KDEev instead. I have no specific requests, I'll let them distribute it as they see fit.\n\nWouldn't it be cool to have a \"donate money on release day\"-day for the devs who work hard and do all this wonderful stuff for us?"
    author: "Oscar"
  - subject: "Re: Kickoff questions "
    date: 2007-12-14
    body: "Have you seen the Kickoff menu by Linuxmint?\n\nI really really like it the way it replaces the 3rd level every time you select a 2nd level item.\n\nIn the new Kickoff menu, I don't particularly like the way the 3rd, 4th, etc... levels push the previous level list to the left. It is dizzying and confusing. Replacing is more elegant, appealing and seems faster. \n\nPlease make the Linuxmint style the default if current behavior is configurable.\n\n"
    author: "Abe"
  - subject: "Debian packages - not there "
    date: 2007-12-12
    body: "Even though the release anouncement claims KDE4rc2 packages are in Debian experimental, thus us clearly not the case (for 64 bit AMD64) at least. So now again, I am using icewm until I find the time to reinstall kde3...\n\nAnnoying. Anyone know of a complete working set of packages for Debian? Something except for the non-working experimental packages?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Debian packages - not there "
    date: 2007-12-12
    body: "If you have a fairly speedy computer, or a lot of time on your hands, you can use kdesvn-build (google it) to get a trunk build of KDE4. I'm using it on Kubuntu Gutsy (x64) and it works well here."
    author: "Andrew"
  - subject: "Re: Debian packages - not there "
    date: 2007-12-12
    body: "im using archlinux and compiles kdelibs and kdebase direct from svn, and works fine, now kwin feels more fast, specially under heavy load, but stills have some lags.\n\nin my system compiles really fast(amd64@3000, 1gbram), about 20/30mins for kdebase and the same for kdelibs."
    author: "rudolph"
  - subject: "Re: Debian packages - not there "
    date: 2007-12-12
    body: "Does anyone know how to get the i386 packages from experimental?  I just have a standard line for experimental and it doesn't have RC2 packages yet.  Someone said that this is because it is for all architectures, and if you get the i386 branch the packages will arrive faster.  However I haven't figured out what to put in my sources.list to achieve this."
    author: "Leo S"
  - subject: "Re: Debian packages - not there "
    date: 2007-12-12
    body: "They are in incoming. Just wait for one day or pick them there\n\nOn the other hand I wonder why there is only amd64, but I may misunderstand something ..."
    author: "phD student that should stop reading the dot"
  - subject: "Re: Debian packages - not there "
    date: 2007-12-14
    body: "Yes, KDE 4 works for me the first time with Debian/experimental now. I think the key package is kdebase-workspace or something like that.\n\nHowever, I agree with other peoples' impression, that the technical platform is impressive, but some design decisions need tweaking. (I managed to ruin the panel in 10 minutes by adding the log-out widget, which couldnot be removed , I deleted the config file to be able to restore the previous situation). Also kmail from KDE3 stopped working.\n\nI look forward to the tweaked versions in the future..."
    author: "Moritz Moeller-Herrmann"
  - subject: "Better default font"
    date: 2007-12-12
    body: "Nice improvements to RC1, speed is really impressing. I'm using the live CD within a Parallels VM and it's not noticeable slower than my native KDE 3.5 installment. Wow. Good job!\n\nBut what I'd really like to see is a nice default font. Vera Sans or DejaVu Sans look quite dated nowadays. I know, I know... it's a distribution thing. But some kind of a recommendation for distributors would be cool.\n\nI use DejaVu Sans Condensed e.g. - looks much more modern than standard DejaVu Sans. And it works without any anti aliasing as good as with subpixel hinting."
    author: "Ascay"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "+ 1\n\nI have quite poor eyesight, and I think the fonts on the Betas and RCS are quite hard to read. One of the first things I do after installing KDE is change the default font. In the spirit of breaking new ground, are there any plans to change the default font to something prettier/more readable?"
    author: "Askrates"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "Oh, and Asian fonts tend to render poorly in default KDE as well :("
    author: "Askrates"
  - subject: "Disable font hinting"
    date: 2007-12-12
    body: "Disabling font hinting when using antialias produces a much smoother font rendering much like OSX. Even Vera can look good without hinting.\nHinting can be disable in the KDE font settings or in your ~/.fonts.conf\n\nI think distributions and KDE itself should consider this. It's almost 2008 and those lines made of a single pixel look like the 80s."
    author: "Flavio"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "I don't have the best eyesight either. That's why I always change the default font and it's size. I like and use Bitstream Vera Sans (font size 12) which seems to be fine for me. However Bitstream is not installed by default on openSuse. One has to install the package from the DVD/CD.\nIt would be nice to see better, more beautiful and readable fonts in the default setting."
    author: "Bobby"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "what about Liberation fonts from Redhat?"
    author: "Vladislav"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "Well, I have some gripes with Liberation, as compared with DejaVu.\n\n- at 7pt bad kerning is apparent\n- it's designed to be metric-compatible with fonts from the Windows 98 era... which were changed in Windows 2000, and then again in Vista, becoming closer and closer to today's DejaVu (except for the mono variant). So, well, Liberation brings back bad memories ;)\n- it has an all-around weirdness, like how the capital R is super wide, or how diagonal strokes are blurry and disproportionally fat... see attached\n\nAll of this is with antialiasing on (smoothness), subpixel hinting off (clarity), and hinting style full (sharpness).\nI've heard stuff about uncrippling my freetype, but this never worked..."
    author: "logixoul"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "That's the hinting screwing everything up. (IMHO the Liberation fonts all look awful though, especially the Mono font.. completely unusable)\nThe solution for better font rendering lies in display technology. Hinting will always destroy the way how the font is actually supposed to look.\nI've seen the XO-1 in grayscale mode (200 dpi apparently) and fonts look awesome on that machine.\nRight now I run my Desktop at 1600x1200 (@100HZ :-) on a 19\" CRT without hinting and it looks alright. I'm having a hard time to find a LCD replacement for this Monitor though. A 20\" screen at a resolution of 1920x1200 would be nice but I guess that's not going to happen (soon and at an affordable price).\n\nSo the magic words for \"good\" looking fonts are DPI, PPI, pixel density, dot pitch and so on ..but NOT hinting. :-)"
    author: "nobody a.k.a. idiot"
  - subject: "Re: Better default font"
    date: 2007-12-13
    body: "Amen."
    author: "Flavio"
  - subject: "Re: Better default font"
    date: 2007-12-13
    body: "A-ha. Thank you."
    author: "logixoul"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "I'm quite fond of the DejaVu font family, actually.\nWhat's \"dated\" about it? The latest version has been released a couple of days ago... and I find it quite good as a dektop font.\n\n\"Fraktur\" can be said to be dated, but most sans, not-too-fancy fonts could be used on the desktop with decent results. \n\nWhat's good with DejaVu is the Unicode coverage -- so it can be a good default font for a large number of languages.\n\nI used to use the condensed version for places where space was an issue, the problem is that it got impossible to select it recently, due to Qt/fontconfig issues, I think."
    author: "Luciano"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "> What's \"dated\" about it?\n\nJust the look.\n\nThe letters are very wide. New OS like Leopard (LucidaGrande font), Vista (Segoe font) and some devices like cell phones and media centers are using more narrow letters.\n\nIt's just a matter of fashion. And KDE 4.0 is a big improvement in that direction with Oxygen icons and theme, little animations, stylish wallpapers etc.\nThe default font looks a bit out of place with it's mid-90s charm."
    author: "Ascay"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "Well, I didn't realize font fashion changed so rapidly.\nActually, I think fonts are chosen quite conservatively -- \"Times New Roman\" seem to still be a popular font, for example.\n\nCell phones are a special case, where horizontal space is not to be wasted (they tend to have tall and narrow screens). \n\nAnyway, this is something KDE can't do much about -- I think it asks for a \"sans\" font, which the distros maps to their preferred font for the role.\n\nAnd there are not many free fonts that can be used anyway.\n\nThe Liberation family does not even have hints (at the moment, anyway), and its Unicode coverage is not comparable with DejaVu.\n"
    author: "Luciano"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "> Well, I didn't realize font fashion changed so rapidly.\n\nWell, you can't argue much about fashion and style. For most people it's just a matter of acclimatization and not taste.\n\nNevertheless, two examples to compare:\n\nKickoff with wide letters:\nhttp://www.kde.org/announcements/announce_4.0-rc2/konqueror.jpg\n\nLancelot with narrow letters (Ivan &#268;uki&#263; has generally a nice taste concerning GUIs imho btw):\nhttp://ivan.fomentgroup.org/blog/wp-content/uploads/2007/10/lancelot-2nd-shot.png\n\nIn my opinion the font in the Lancelot screenshot looks much more elegant and modern.\n\n> Actually, I think fonts are chosen quite conservatively -- \n> \"Times New Roman\" seem to still be a popular font, for example.\n\nYou are talking about font usage in general, I'm talking about fonts for GUIs. Who uses Times New Roman in menus e.g.? Nobody I hope. :)\n\nTimes New Roman is quite ugly actually. It's only used so much because it is the  default font in widespread applications like Word. Or for compatibility because every Windows and OSX installation has it."
    author: "Ascay"
  - subject: "Re: Better default font"
    date: 2007-12-13
    body: "Actually I like the Kickoff screenshot more because it's easier to read for me - more space between the characters.\n\nPrinted Times New Roman is not that bad, though I prefer the standard LaTeX font."
    author: "mat69"
  - subject: "Re: Better default font"
    date: 2007-12-17
    body: "Hello, can you please tell how you got kde to look like this:\n\nhttp://ivan.fomentgroup.org/blog/wp-content/uploads/2007/10/lancelot-2nd-shot.png\n\nMaybe a small step-by-step how to do that?? It looks very cool and I think more people would like that"
    author: "Peter V"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "I do not think DejaVu fonts look dated at all. On the contrary they are one of the most modern Free fonts to me. But all this is a matter of taste of course.\n\nTo address your point, there is a variant of DejaVu called DejaVu Condensed which is significantly narrower. It should make you happy.\n\nRegards,\n\nMed"
    author: "Med"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "> But all this is a matter of taste of course.\n\nMore a matter of fashion or trend. Wide fonts like DejaVu (Standard) or Verdana and Tahoma came up in the late 90s. Nowadays they are quite rarely used on modern user interfaces and look therefore a bit dated.\n\nI didn't say that Vera Bitstream or DejaVu look ugly. On the contrary, I prefer them for longer texts like on webpages (Verdana is quite present there). But on a brand new desktop environment like KDE4 they are a bit out of place in 2008.\n\nJust look at OSX Leopard, Vista, cell phones, consoles (Wii, Xbox 360, PS3), DVD menus, audio players... narrow letters everywhere. People get used to such trends, if you ignore it, it will look dated.\n\nThe icons and theme from KDE3 (Krystal, Plastik) look very dated today and got replaced to something much more modern. Great! Why not doing the same with the default font?\n\n> To address your point, there is a variant of DejaVu \n> called DejaVu Condensed which is significantly \n> narrower. It should make you happy.\n\nI suggested that font in my initial post. :)\n\nBut it's not about me. I can change the font settings myself. It's more about a modern look that will appear on screenshots in magazines, on websites etc."
    author: "Ascay"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "Narrower fonts only make sense in large hi-resolution devices (e.g., 1080p HDTVs, 20+ inch LCDs, etc.).  Just try looking at those \"modern\" fonts on an old-school 1990's SDTV and tell me that narrow fonts are universally good.  You can't read shit when the characters are using lines that are thinner than possibly representable on a 480i display..."
    author: "Matt"
  - subject: "Re: Better default font"
    date: 2007-12-17
    body: "Nobody cares about your old SDTV anymore. "
    author: "Anon"
  - subject: "Re: Better default font"
    date: 2007-12-12
    body: "> The icons and theme from KDE3 (Krystal, Plastik) look very dated today and\n> got replaced to something much more modern. Great! Why not doing the same\n> with the default font?\n\nFonts aren't something you can make easily. There goes a lot of work in that and that's the reason why there are very few fonts which are usable for screen display. DejaVu is probably the only real OSS font project with a community and with a font usable for screen, and even we didn't design the main glyphs...\n\nAnyway, for a font, the biggest concern should be legibility, not fashion. And legibility for small font sizes was one of the main concerns when vera was designed.\n\nSure, if you have perfect eysesight, you may prefer something thinner, but I don't think my grandma would be happy with such defaults, unless you also make the default font size bigger.\n\nAnother note: DejaVu Condensed fonts aren't hinted (doing that would take a few months full time work, but if someone has a lot of time, we welcome him), so it's not possible to use those as default now. Furthermore, Qt cannot handle the condensed family, which isn't exactly helpful to make them default either...\n\nFinally, the font settings are made by the distro, not KDE. Although there have been talks about adding dependencies on certain fonts in KDE, it'll still be the the distros who will make the defaults... But you can always make a slick preview distro with the funky fonts of course :-)\n\nGreetings\nBen, DejaVu maintainer"
    author: "Ben"
  - subject: "KPackage"
    date: 2007-12-12
    body: "Kpackage has been unuasbly slow for my for the complete length of the release cycle (using the openSUSE packages). E.g. scrolling the list of packagews is so slow that it takes half a minute to scroll by one package (on a AMD X2 4600+).\n\nI'd like to be able to try out kpackage on my system (as it seems to be a frontend for smart, and I'd really like to get rid of smart's ugly and unfunctional GTK-GUI).\n\nWhat's the reason for this behaviour?"
    author: "Peter Thompson"
  - subject: "Re: KPackage"
    date: 2007-12-12
    body: "Not sure how you installed the packages but if you go here: http://download.opensuse.org/repositories/KDE:/KDE4/openSUSE_10.3/ and install the KDE4-BASIS.ymp and the KDE4-Default.ymp then it should work but you should uninstall present packages first."
    author: "Bobby"
  - subject: "Re: KPackage"
    date: 2007-12-12
    body: "> What's the reason for this behaviour?\n\n\nMostly nobody working on it :("
    author: "jospoortvliet"
  - subject: "What are the plans for 3.5.x?"
    date: 2007-12-12
    body: "How long until 3.5 is EOLed?  I'm assuming debian and the like will keep it around for a few years regardless.\n\nIf KDE4 doesn't learn to treat screen real estate with respect I'll end up back with fluxbox.   A lot of the new libraries rock out hard, but I've been severely underwhelmed with desktop functionality.  "
    author: "Velvet Elvis"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-12
    body: "KDE 3.5.x updates should continue for some time, according to announcements, AFAIK at least until 4.1, as many intended features and applications (especially those not in KDE-core) simply didn't make it for 4.0 and the announced plan is to allow parallel installs of KDE 3.5.x and 4.0.x so people can continue using their KDE 3.5 apps until they get migrated to 4.x, which will be 4.1 time for many of them.\n\nThis is also why there has been a heavy emphasis on making KDE 4.0 apps behave well with KDE 3.5 and vise versa, 3.5 apps with 4.0.  In fact, IIANM, some of the updates in the 3.5.7 thru (the upcoming) 3.5.9 are specifically that, helping certain 3.5.x apps to work more smoothly under the 4.0 kwin and etc."
    author: "Duncan"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-12
    body: "Not too many people will tell you this, because it's politically incorrect, but KDE 4.0 is not meant for general use by the masses. You will probably have to wait until 4.1 for something that's up to par with 3.5."
    author: "David Johnson"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-12
    body: "The biggest question is who cares enough to maintain 3.5?\n\nIf many companies stay on 3.5 for years, updates can continue for years.   Linux still sees releases of the 2.0 kernel once in a while (I haven't kept up, but I believe there was one last year, and may be more in the future), and that hasn't been the current up to date stable kernel for 10 years.  (some linux distributions support releases for 5 years, so they may be forced to do some 3.5 work for several years yet just to fill customers needs)\n\nKDE 3.5 will likely remain unlocked in SVN forever.  However doing a release is a lot of work.  The current release team will want to stop doing releases as soon as they can.  However if someone else steps forward to do the work for more releases of 3.5 we may see them.  \n\nThis is open source, in large part what happens depends on who is willing to do what."
    author: "Hank Miller"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-13
    body: "> This is open source, in large part what happens depends on who is willing to do what.\n\nExactly. This is where most community driven projects unfortunately suck. And I do not mean to accuse floss or kde.org folks, stating the obvious. It all comes down to organisational and human aspects, ensuring proper maintenance over the lifetime of a \"product\" and accepting that its lifetime doesn't end, when the next iteration of your product is released.\n\n\nHaving the human ressources and the will to do at least two or three years of higher-level maintenance for KDE 3 than just crash and security fixes - no new features of course - an occassional bugfix release inclusive from now on is the point where I'd consider kde.org's organisation mature."
    author: "Carlo"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-13
    body: "> If KDE4 doesn't learn to treat screen real estate with respect\n\nspecifics, man, specifics. this tells us exactly nothing ...\n\n> I'll end up back with fluxbox.\n\nthe silence you hear is all the people rushing to prevent this from happening. ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-13
    body: "The default icon sizes all around seem to be larger.  The plasma wrench in the corner of the screen is huge.  The oxygen kwin decoration and style have excess space pretty much everywhere, though it is much improved as of late.  The panel is huge.  Kickoff is huge.  Many of these items are not resizable.\n\nWill KDE 4 provide a workable desktop?  In the strictest sense, at launch, yes it will.\n\nWill it for me?  No.  I can't stand to see so much wasted space.  I like maximizing my work area, and giving the lion's share of my desktop space to content.\n\nI'll check back when there is more flexibility to define the desktop exactly how I want it, which in my opinion, has always been perhaps the greatest strength of KDE as a desktop."
    author: "T. J. Brumfield"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-13
    body: "it's called \"white space\", and most people are actually more efficient when it exists.\n\nthere's also the issue of higher average screen resolutions. i mean, it's not a 640x480 world anymore.\n\n> The panel is huge.\n\nactually, it's not. it's precisely 2px taller than kicker's default. so why is it so big? the svg that's being used has these gigantic borders. why? because it's the same one we use for the applets because the artists haven't given me anything proper to use yet.\n\ni don't expect you to have known this, but.. yeah.. doesn't make complaining about it any more fun.\n\n> Kickoff is huge.\n\nlol. ok, so.. tell me this:\n\na) why does this matter when it's something that only appears when you use it?\nb) you can have the old, cramped application menu. you don't need to complain about kickoff.\nc) the #2 complaint about kickoff right now? it isn't big enough. yeesh.\n\n> Many of these items are not resizable.\n\napparently many people apparently refuse to read comments to the same story they are replying to. ;)\n\n> The plasma wrench in the corner of the screen is huge.\n\nhow big is your screen? it's a quarter circle, diameter 48px. that's not particularly ... huge.\n\nand what is it interfering with, precisely?"
    author: "Aaron J. Seigo"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-14
    body: "First off let me say, that you have been getting more criticism than praise.  So when I say I don't care for something, also let me say, that I do appreciate all work going into the project.  I'm just expressing an opinion.\n\nKickoff isn't resizable at all that I know of.  Some applets, such as the new panel, are only resizable by reading the dot and finding which files to edit.  At a glance, they appear to not be resizable.  When I try a new distro, one of the first things I do is configure the panels in KDE, and I usually go with a smaller panel.  The new panel is slightly larger than the old default, but I didn't care for how large the old default was either.  I'm not suggesting development should pander to me.\n\nQuite the opposite.  When the desktop is finished, and it is easier to customize it to make everyone happy, then I'll switch.  A big part of the appeal of KDE to me is the power of flexibility.  I like having my desktop exactly how I like it, and neither Gnome, OS X, nor Windows allows me to do that.\n\nMany of the complaints of KDE 4 aren't really faulting the work of KDE 4, but really they just speak volumes of what a great product KDE 3 is.  When you do a major refactor, it takes a while to fully match your existing product.  I'd be proud that the bar you set for yourself is so high, and it seems like you are close to meeting that bar.\n\nI'm exciting for the future of KDE 4, but I don't want to use it just yet."
    author: "T. J. Brumfield"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-13
    body: "Exercise a little patience. Wait until the eggs are laid before you start judging what the chicken will look like. As far as I have read and understood KDE4 will be much more configurable than the present 3.5x so yes it will also be a desktop for you :)\n\nThe most of your critics are actually minor issues. you will be able to use a new theme and icon-set if you don't like oxygen, the same applies to window decoration and Kickoff, which you will be able to replace with Lancelot, Raptor or the simple KDE 3.5x menu that so many people are clinging to.\nJust wait and see Mr. Brumfield..."
    author: "Bobby"
  - subject: "Re: What are the plans for 3.5.x?"
    date: 2007-12-13
    body: "I think you will find Novell will keep updating 3.5 for a few years as a lot of their businesses they work with will continue using that version if they use KDE\n\nBut I tend to view kde 4 in the light of windows xp.  Using XP was a habit, and moving from that piece of shit to linux was a learning experience, but one full of rewards once I got the hang of it.  KDE 4 appears to me the same.  It looks like a monster (no offence).  so many different and new aspects to it, that it will once again be a learning curve to get used to and fall back in love with it.  Of course it is better than moving from XP to Vista, which is like taking a computer and wrecking it, lol\n\nI'm looking forward to the release so I can update to it, I decided after RC1 to wait until the final release.  Now if someone here can just hack into Aaron's computer, and change his calender to saying Jan 11, then maybe we can trick them into releasing it now ;)"
    author: "Richard"
  - subject: "kicker edges"
    date: 2007-12-12
    body: "In the screenshot, kicker (or whatever it's now called) doesn't stretch out to the left and right edges of the screen. Can someone confirm whether this can be changed? If I can't throw the mouse pointer down to the bottom edges and have it 'hit' the kicker, my head will explode."
    author: "foo"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "Queue head exploding then.\n\nThe panel is still in really sorry state at this point--I'm fairly sure its going to be one of the bigger disappointments of 4.0, it should be much better for 4.1 however."
    author: "Dan"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "Ehm, Aaron said it can cover all...\n\nIt's just showing round borders as far as I remember, even more important, it's 100% themeable."
    author: "Luis"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "Uhe most disappointed thing about it is that it can't be configured. I am getting no response when I click on it with my right mouse button. Not typical for KDE."
    author: "Bobby"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "Apparently much of the configuration is there; it's even got some stuff kicker is missing, but maybe not everything kicker had. Unforunately, there's no gui to configure it yet."
    author: "Soap"
  - subject: "Re: kicker edges"
    date: 2007-12-13
    body: "that's because i'm trying to piss you off. and i do mean you. specifically, you, \"Bobby\". i hope that makes you feel special."
    author: "Aaron J. Seigo"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "It's not the final plasma theme (or maybe just not the final panel theme, I don't remember what has been said about it)\n\nBasically they just need (and from what I understand, they will) replace the SVG used for the background and border.\n\nI also believe that they will make it easily user-configurable in the future but maybe not in 4.0."
    author: "Antoine Chavasse"
  - subject: "Re: kicker edges"
    date: 2007-12-17
    body: "i think its strange that kde4 is released with the notion that basicly one of the most important aspects of the desktop (kicker) will suck."
    author: "Peter V"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "I threw the mouse to the bottom edge of my system, and ended up with a shattered mouse and cracked monitor.  Maybe we should stop throwing mice...mouses."
    author: "Henry S."
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "What a pain this kicker is... I use kde4live image, and it is very slow to use. It's making me go crazy. It also uses like 1/4th of a high-res monitor. Its 50pixel-wide edges are about the size of the old panel. \n\nAll the other stuff is cool, but this is such a show-stopper. Give me back the old panel (with all of its errors) and I will switch to 4.0 as soon as it arrives.\n\nCheers,\n\nMate"
    author: "Another PhD student"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "I second that request! I want to have the kicker looks like the old KDE 3 one! Same size (small!), changable, full row (so start is in the bottom left corner and you don't have to search for it with the mouse)."
    author: "Michael"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "I third this! On my Laptop I've only a 1280x800 screen resolution and the Panel wastes a lot of space! But Aaron already mentioned that the Kicker Applet is higly configurable. The question is: Is it highly configurable for Applet Designers or also for the normal user with a neat configuration dialog?\n\nBut anyway: Plasma rocks!!"
    author: "kiwiki4e"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "guys, give it some time...SOME TIME!!!!"
    author: "Emil Sedgh"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "Don't you think that two weeks before the final release of kde4.0 is a bit past the point where \"give it some time\" is valid?"
    author: "Dan"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "Two weeks? Isn't KDE 4.0 tagged on Jan 4th?"
    author: "Luca Beltrame"
  - subject: "Re: kicker edges"
    date: 2007-12-12
    body: "ehm\nthe support for resising, multiple panel, etc is there, it just needs UI to do it.\nalso, please do not forget that KDE 4.0.0 is just, just the begining."
    author: "Emil Sedgh"
  - subject: "Re: kicker edges"
    date: 2007-12-13
    body: "Why so?\n\nA release is simply a snapshot in development time. This is the state of KDE4 as it is right now.\n\nDerek"
    author: "D Kite"
  - subject: "Re: kicker edges"
    date: 2007-12-13
    body: "> What a pain this kicker is\n\nit's not kicker.\n\n> Give me back the old panel (with all of its errors) and \n> I will switch to 4.0 as soon as it arrives.\n\nhere's how to do it:\n\n1) kquitapp plasma\n2) open $KDEHOME/share/config/plasma-appletsrc\n3) look for a [Containments] group (probably [1]) that has plugin=panel\n4) delete that group\n5) save the file\n6) start plasma\n7) start kicker.\n\nhappy?"
    author: "Aaron J. Seigo"
  - subject: "Re: kicker edges"
    date: 2007-12-13
    body: "Dear Aaron,\n\nFirst off, thanks for the reply. Secondly, please do not get upset: the plasma kickoff-replacement is probably good for many people, simply, it is not made for me (or some other geek-minded people). As for the solution for starting the old kicker, it's a nice one - I would be really happy to have it as an option in system settings so that I(and many others) don't need use a text editor to do it.\n\nCheers,\n\nMate"
    author: "Another PhD student"
  - subject: "Re: kicker edges"
    date: 2007-12-15
    body: "Any geek-minded person would not mind opening a text editor to change a config option"
    author: "JackieBrown"
  - subject: "Re: kicker edges"
    date: 2007-12-16
    body: "\"Any\" I suppose you've talked to all geek-minded people to allow you to throw a blanket statement out there. Careful when you say 'any', 'every', 'all', etc.."
    author: "mike"
  - subject: "Relax guys!  It'll be OK."
    date: 2007-12-12
    body: "All you guys, relax! You obviously haven't been following Segio's blog.\n\nThe panel code is much more powerful and configurable than the old Kicker code. It's just being written at the last minute, so Segio hasn't gotten any configuration dialogs made yet. When he's got the panel mostly working, it'll only take a day or two at most for him to write the config dialogs. The current theme is also not the final one; he is just reusing the one used by the applets for testing purposes. I am confident that in a month, the Plasma Panel will have all the functionality and configurability that Kicker did, plus more."
    author: "kwilliam"
  - subject: "Re: Relax guys!  It'll be OK."
    date: 2007-12-13
    body: "Thank God."
    author: "Bobby"
  - subject: "Re: Relax guys!  It'll be OK."
    date: 2007-12-15
    body: "Only xy days until the dream becomes a nightmare. :-D\n\nI really doubt that Plasma can be fixed in less than 30 days."
    author: "KSU"
  - subject: "Re: kicker edges"
    date: 2007-12-13
    body: "> Can someone confirm whether this can be changed?\n\nfor the Nth *frigging* time: yes.\n\ni honestly feel like most of you don't deserve my attention since you apparently don't pay attention when i offer it."
    author: "Aaron J. Seigo"
  - subject: "Re: kicker edges"
    date: 2007-12-13
    body: "Sorry mate. I haven't had a chance to look at KDE4 much, although I did try a livecd of it some time back, and I don't remember the 'kicker' looking like it did in the screenshot. I appreciate your work, don't get me wrong."
    author: "foo"
  - subject: "Re: kicker edges"
    date: 2007-12-13
    body: "Aaron,\n\nI symphathize with you're, eh, disapointment, on the continuing critizism on the panel. I know it will change, you know it will change, but it seems lot's of people still don't. I think the reason behind this is that info on development, info on roadmap, info on planning, info on desing decesion, etc are very scattered. Some is on the dot, some is in blogs (a lot in yours :-) some in the mailing lists, some on IRC, etc. Easy to miss stuff. I like to follow all this, so i'm pretty infromed, but not all of us are. Maybe it would be a good idea to channel al this stuff thru one medium and one medium only. All other stuff can than be dismissed as work in progress/developers own current default."
    author: "Fred"
  - subject: "Re: kicker edges"
    date: 2007-12-13
    body: "Don't be angry Aaron. I don't think that anyone here doubt yours and the team's skill but as you know it's human nature to be impatient ;)"
    author: "Bobby"
  - subject: "Desktop icons"
    date: 2007-12-12
    body: "I'm still missing desktop icons and application starter icons in the panel.\n\nAre there any plans to integrate this features?"
    author: "raman"
  - subject: "Re: Desktop icons"
    date: 2007-12-12
    body: "As far as desktop icons go, there's a patch in panel-devel already."
    author: "Luca Beltrame"
  - subject: "Re: Desktop icons"
    date: 2007-12-12
    body: "Icons work fine now, i checked yesterday, the support is back after few month of left border edge only. The name is their (on 1 line) if you resize the icon. The small button in icon to open with different application is gone/broken again, but i hope it will be back in time, it was great, i also hope it will be in dolphin soon. The main problem is the icon title, it is insode of the border, so we have to resize the icon to see it, and it is only on 1 line...\n\nIcon are actually plasmoid only, they are not files in the desktop folder yet, but for application, it is not a problem anyway so it is fine for now."
    author: "Emmanuel Lepage Vallee"
  - subject: "Re: Desktop icons"
    date: 2007-12-13
    body: "> I'm still missing desktop icons \n\nif you mean displaying the contents of  ~/Desktop, that patch was approved for committing today. it'll be optional and i'm very tempted to turn it off by default because it is, quite honestly, one of the most stupid ideas. the only reason people keep asking for it is because they are used to it, not because it's actually any good.\n\nif you mean files and applications in general, that's been there for some time.\n\n> and application starter icons in the panel.\n\nalready been there for a while as well. we need to make it more obvious how to add things to the panel and how to configure icons to be associated with specific applications (rather than files or urls)"
    author: "Aaron J. Seigo"
  - subject: "Re: Desktop icons"
    date: 2007-12-13
    body: "\"one of the most stupid ideas. the only reason people keep asking for it is because they are used to it, not because it's actually any good.\"\n\nAhem... who says that this is stupid? Another of those usability studies? And isn't it legitimate to expect functionality of a desktop simply because you are used to the fact that a tool has a certain functionality?\n\nAs for me, I put all my important apps and documents as links onto th desktop. If I want to start one of them, I press the \"show desktop\" button and then the icon I want. Voila. Faster than any stupid Kickoff- or even the traditional menu.\n\nAnd this is nowadays considered stupid and deprecated? I think this \"we know better than the users what is good for them\"-attitude should be left to Gnome..."
    author: "Peter Thomson"
  - subject: "Re: Desktop icons"
    date: 2007-12-13
    body: "that's exactly the point. I can access all important applications and documents very fast. This is even more necessary since kickoff is the standard application menu."
    author: "raman"
  - subject: "Re: Desktop icons"
    date: 2007-12-13
    body: "let's take stock here: you mock usability studies, throw \"you're like gnomers\" crap, evidently don't get the the tie in between \"people keep asking for it because they are used to it\" and \"legitimate to expect functionality of a desktop simply because you are used to the fact that a tool has certain functionality\" (hint: that's what i bloody well wrote! hello?!)\n\nbut here's the real winner:\n\n\"I put all my important apps and documents as links onto th desktop. If I want to start one of them, I press the \"show desktop\" button and then the icon I want\"\n\nwhy is it the winner? because you have been able to do precisely what you describe here for, oh, i dunno, 6+ months now? \n\nlet's re-read exactly what i wrote: \"displaying the contents of ~/Desktop\"\n\nnote that i didn't say \"showing icons that represent applications or files\". no, i wrote: \"displaying the contents of ~/Desktop\". those aren't the same things. not even close. the latter is a stupid idea because assuming that all you care about exists in a single literal directory on disk is a bit on the naive side. it also makes it nearly impossible to easily swap around layouts, share settings, etc.\n\nwouldn't you say?\n\nso .. back it up, turn around and try again. i'm trying to offer you something that works better (collections of items from pretty much anywhere you wish) and instead you reply like this.\n\ni really don't need the abuse just because you fail to read closely enough or comprehend (not sure which it was) what i wrote. if you don't agree with something, ask questions or provide comments from a place of sincerity (because maybe, just maybe, you don't understand it yet) but save the sly insults and ridiculous attempts at wit. not needed, not wanted and in the end you've only made yourself look foolish for arguing against something that's not even a problem.\n\n> Faster than any stupid Kickoff\n\nwell, at least you managed to get a swipe in something else along the way, right?"
    author: "Aaron J. Seigo"
  - subject: "Re: Desktop icons"
    date: 2007-12-13
    body: "Hey Aaron, please take it easy on the guy. I've also misunderstood what you've meant with ~/Desktop when I first read your post, so maybe you were not that clear in your first explanation. But now I think you've clarified a bit. \n\nSo in KDE 3.5 everything was in ~/Desktop, and there you've had files and links to other apps. If I understand correctly, in KDE 4 the Desktop has a buil-in list of links to files? I've never found confusing the idea of having a ~/Desktop, but since I don't quite understand the new solution I can't judge what is better.\n\nBut I trust that whatever you guys do will have a reason. I think it is silly for people to complain at this stage of development.\n\nI think that given the short timeframe, you guys should just stop trying to make everyone happy. Focus on your goals and release a stable product, even if some things are not configurable to everyone's taste. Then gather all comments from users, and polish everything for 4.1\n\nThanks for all the hard work.\nJulian"
    author: "Julian"
  - subject: "Re: Desktop icons"
    date: 2007-12-14
    body: "Your \"collections of items from pretty much anywhere you wish\" sounds a little like \"a directory full of links\" that could perhaps reside in ~/Desktop? I figured links were the Right Way of dealing with places where you needed to get a bunch of not-entirely-related things in one play, and having things as files in directories has to be the proper unix thing to do.\n\nIn fact, I'm a little cheesed that (on whatever I'm using now, not KDE4) I have device icons on my desktop that don't show up in ~/Desktop for some reason."
    author: "Matthew"
  - subject: "Re: Desktop icons"
    date: 2007-12-14
    body: "\"the latter is a stupid idea because assuming that all you care about exists in a single literal directory on disk is a bit on the naive side.\"\n\nThings I care about (and wish to display on the desktop) exist in a single directory *because I put them there or link them to there*.  What's so wrong with having an actual directory that corresponds 1 to 1 with what is displayed on the desktop?  \n\nMaybe I would like to browse through all those important files and links with Dolphin/Konqueror/Konsole.  How am I going to do that if the desktop is a bunch of files from multiple places in the filesystem (\"collections of items from pretty much anywhere you wish\")?  What was wrong with using links to do just that: placing items from various places in the filesystem on the desktop?  It worked, and didn't break the ability to browse the desktop with Dolphin/Konqueror/Konsole.\n\nThe desktop as a container for important apps and documents that need attention (or links thereto) is a paradigm that's been around a long time.  If by default this container is not viewable in its entirety (at the same time) within the standard filebrowsing tools it's a step backwards, especially since the supposed benefits were already achievable with links.\n\nSeriously, I take issue with \"displaying the contents of ~/Desktop ... is a stupid idea\".  Removing the ability to browse the desktop so one can achieve through nebulous different means exactly what \"ln *someFile* ~/Desktop\" already did is incredible.  \n\nAnd if the desktop is still somehow browsable then how on earth is that any different from an end user perspective from having a ~/Desktop that matches with the desktop?  Either you've got a desktop container visible when all windows are out of the way and browsable with Dolphin with an ioslave or something (in which case it's functionally no different from the old method at all so what's the point) or the desktop isn't browsable.  The latter, while not the end of the world can't be called \"good\".\n\nSo I've mentioned something ~/Desktop was good for: accessibility with Dolphin and all the accompanying sorting possibilities incumbent in the detailed list view.  What is one single thing the new method does that couldn't be done with links?  Some pretty plasma gui for \"adding random item to the desktop from someplace in the filesystem that isn't ~/Desktop\"?  Whatever the method is, is there some reason it can't be a frontend for linking said items to ~/Desktop?  \n\nThere's plenty of understandable annoyance coming from you, but I don't see it accompanied by any convincing arguments that the current desktop isn't a step backwards.  Obviously that can just as well mean that I don't understand the convincing arguments; I'm not as close to this as you.  Elaborate on \"sharing settings\" perhaps?  I'm not sure I'd prefer being able to share my personal layout of widgets to being able to find the newest item on the desktop with Konqueror."
    author: "MamiyaOtaru"
  - subject: "Re: Desktop icons"
    date: 2007-12-14
    body: "I think you may have missed a couple of things here:\n\n1) The basic idea of having the desktop as an area for files (and files alone) most often leads to the desktop becoming a general dumping ground. I have seen this on the desktops of many, many users (including my own). When it becomes too cluttered with files it is simply a pain to manage. The very fact that you want to use a file manager to also look at the contents of the desktop tells me that you have a lot of files there, and it is likely that some of those files would be better placed and more easily organised elsewhere. The logical conclusion of all of this surely is that \"the desktop as just a directory of files\" scheme doesn't really work very well (for most people anyway).\n\n2) It sounds like you are seeing Plasma as only about showing files and other similar items on the desktop - it's a lot more than that. We're talking quite powerful and in some cases even interactive applets here. Plasma allows for much more flexibility, and I would argue that it will enable you to have items on your desktop that are actually more useful to you in your regular desktop usage.\n\nAs I see it, the ability to have icons on your desktop through Plasma is simply a stopgap to allow those people who insist on working the old way to continue to do so.\n\nIf you would like to explore this further, perhaps you could mention the types of items you currently have on your desktop, how often you use them, etc."
    author: "Paul Eggleton"
  - subject: "Re: Desktop icons"
    date: 2007-12-14
    body: "(by the way, I am not being facetious in that last comment - I think it would be worth examining how you use your desktop to see if it could be done more efficiently with Plasma)."
    author: "Paul Eggleton"
  - subject: "Re: Desktop icons"
    date: 2007-12-17
    body: "A desktop being used as a dumping ground ? that's exactly what a desktop is for.\n\nMy real desk, where my computer screen lies, is also a \"stack\" where things get pushed and pop in a random way. That beats all your fucked up usability studies anyday. "
    author: "Anon"
  - subject: "Re: Desktop icons"
    date: 2007-12-23
    body: "I never use icons on desktop because for most part it makes me mimize all the multi windows i have open to run another app..  But I do extensivly use icons on panel. And only place ones there I use repetitively...kde 4.0 does not allow this from what I seen..If it does..I not figured out how to go about it.  So I am left with click kicker...then click from a favorite list.  I prefer one click on panel besides..its much easier to see and read which ones i want to launch on panel.  I do like the favorites kicker..but I would never solely depend on it when I done experienced icons one click from a panel  Also when list on favorites in kde 4 becomes to long you have to scroll as well.  I have found no way to stretch the kicker menu.   Nice ideal I dissagree with it being the sole way to access your apps.  I would aggree with some other post...freedom feels bit restricted to one person point of view in KDE 4.0.  Now on to the file manager.  This is by far the most smartest beautiful file manager I have ever had the pleasure of using.  My hat is off to the functionality of it. The clock.  I not sure the reason why cannot set clock from military time to 12 hour.  or if you can I am not finding it.  But to me this seems to be a push to force others into a standard.  Majority of america is on 12 hour.  just removing it from a desktop to change way things is done will only hamper in the growth of KDE as a world desktop.  It does not reside on ever computer..windows does for most part..so I would delay this logic at time being.  This is My Opinion.  right or wrong.  Its still about Freedom of choice..not Whos smarter or Right way or Wrong way.  graphics in KDE are top class.  Best of luck with KDE 4.0  I still have my reserves but will use it until I know where it goes after release.  KDE 3.58=amazing  KDE 4.00 Not judging until release.\n"
    author: "Sigra"
  - subject: "Coenig = CDE?"
    date: 2007-12-12
    body: "\"Coenig\"? Is that nerdy humour? Will KDE become CDE? Where ist the \"K\"? ;-)"
    author: "K\u00f6nig"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "If you note the code names for some of the previous RCs and betas, they too had Cs instead of Ks.  I haven't seen anything official on it, but I did see speculation by a user (much as I'm now speculating as a user) that they are intentionally doing this for the betas/rcs, but will return to \"K\" for the official 4.0 and beyond releases.\n\nDuncan"
    author: "Duncan"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "Yeah, it's mostly Sebas behing funny here ;-)"
    author: "jospoortvliet"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "Coenig sounds like K\u00f6nig in german which means King!\nIf no \u00f6 is available one can use oe instead."
    author: "Eddi"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "They did use the oe but somebody decided to verarscht the German language but we are going to get back at them by the time the final version is ready ;)"
    author: "Bobby"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "http://en.wikipedia.org/wiki/Andrew_Koenig_(programmer)\n\nSo, yes. Nerdy humor."
    author: "sebas"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "Oh, it's Andrew? I thoug was http://en.wikipedia.org/wiki/D\u00e9nes_K&#337;nig !"
    author: "ZeD"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "damn unicode names!!!\n\nhttp://en.wikipedia.org/wiki/Denes_Konig "
    author: "ZeD"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "Oh, I thought it was http://en.wikipedia.org/wiki/Walter_Koenig"
    author: "Karellen"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-12
    body: "i guess we'll return to K names for stable releases"
    author: "nick_s"
  - subject: "Re: Coenig = CDE?"
    date: 2007-12-13
    body: "I am 100% sure, that KDE will from then on use \"G\" names, until the very last person in the universe is going to notice, that letters are only letters. And nothing beats the fun of confusing people. :)\n\nYours,\nKay "
    author: "Debian User"
  - subject: "Embedded Konsole"
    date: 2007-12-12
    body: "Why does the konqueror in the screenshot have an embedded console? Seriously now, the middle information thing in konqueror looks like a second window on top of the konqueror window. The new oxygene theme has not a lot of contrast (which works ok for me) but why on earth do we need THAT amount of contrast in such prominent a place? Everytime I open up konqi I think it opened konsole inside."
    author: "Anon"
  - subject: "Have to agree"
    date: 2007-12-12
    body: "I really don't want to criticise KDE4; mostly I'm just looking forward to it now.  But I have to agree on this one.  Theming in general still needs some work, imho.  I guess it's all still planned though."
    author: "Lee"
  - subject: "Re: Embedded Konsole"
    date: 2007-12-12
    body: "Completely agree with this ! The new welcome page for konqueror is too contrast."
    author: "Anonymous"
  - subject: "Re: Embedded Konsole"
    date: 2007-12-13
    body: "open the svg/pngs in an image editor, make some changes, submit them. they are found in kdelibs/kdeui/about/ and kdebase/apps/konqueror/about \n\np.s. congrats on being the Nth group of people to complain about something trivial without mentioning anything you find interesting or positive.\n\nlove, aseigo."
    author: "Aaron J. Seigo"
  - subject: "don't get so upset..."
    date: 2007-12-13
    body: "I think the main frustration from users is that this is named \"RC\", when under the feeling of many of us this is more an alpha or beta release. For this reason , lots of people are panicking and think \"oh, my God, please don't let this come to release in a month\". People usually expect a RC to be a full-working product, with just bug squashing. I know, this is not your fault, I'm just trying to share some of the feelings coming from RC-tester users. People get a sweet-sour taste when the kick-off menu is still morphing, looking for the right direction.\n\nBut no one will fail to notice that KDE 4 is a major improvement over 3. I just think that people should take this easier, thinking of KDE4 release as kind of like a beta and version 4.1 as the final release. I'm sure it will kick ass.\n\nPeople have thrown kudos for the speed improvements vs RC1. Some people don't know how to canalyze their frustration, when their expectation to have a RC being full-featured revolutionizing product dissolves. And sometimes this becomes too loud. Just don't get unmotivated about this, the whole OSS community is still betting very strong on KDE4 because we know it will rock. Thanks for offering your time and hard work to this."
    author: "Julian"
  - subject: "Re: Embedded Konsole"
    date: 2007-12-17
    body: "Watching you having your period is truly entertaining. "
    author: "Anon"
  - subject: "RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "Hey you guys, the RC2 rocks! As for today i switched to KDE4 on a day per day basis. I have to do that with compiz though, because on my Geforce 5200 kwin is really slow. I sure hope this will change over time.\n\nSingle other thing that bugs me is the appereance of the digital clock... in a screencast Aaron once did there was this design with a horizontal line with an animated train station-like letter change. Now this was really kind of fresh and new, why is now this boring and clunky clock in the panel? Please, put the old model back in as default. Think about the first impression... you wanted to 'wow' the users, didn't you?"
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "\"Now this was really kind of fresh and new, why is now this boring and clunky clock in the panel?\"\n\nBecause there was an avalanche of complaints about it."
    author: "Anon"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "...aw, come on... don't let a bunch of small minded gnomes dictate the default use of a clunky clock! \n\nThe train station clock was awesome and cool, and the big clunky one isn't. It's just that easy. I understand that the compromise with the progress bars, the tabs and the menus is due to lack of time, but DELIBERATELY replacing a perfectly good animated and awesome clock with a clunky one is just not cool. Seriously, reconsider this."
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "It might not be cool but if that means winning over the Gnomes then I think it deserves a try ;)"
    author: "Bobby"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "I'm aware of the fact, that there are desktop users who appreciate boring things and can't stand innovative ideas. But there are many others, namely KDE users, who are a bit disappointed if their DE takes steps back just to please those who are small minded. \n\nDon't forget, that KDE and Gnome mark different approaches. You guys decided to make a huge version step towards 4.0 for example, and against the small steps that the gnome side loves so much. So please, if you find something cool, and your users do, just let it friggin be that way."
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "A few reasons why I choose KDE over Gnome (not saying that Gnome is not good) are: it's richness, flexibility, that it's so highly configurable, that it gives it's users total freedom and control but still remain simple and easy enough for anyone to learn quickly. \nI have tried Gnome time and time again but kept coming back to KDE because of the above mentioned and I would be very sad should KDE lose those features. I believe that the developers are listening but there are two many vioces speaking at the same time so I would advise them to follow their path of success, the path of innovation and freedom."
    author: "Bobby"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-13
    body: "I'm 100% agree with you. I'm try to use gnome time to time with it drives me crazy in only 2 hours. I can't live without the flexibility of changing everything on my  DE like I can do on KDE. \n\nBy the way is it possible to have a proposed layout with two panels like gnome ? "
    author: "Vincent Mialon"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-13
    body: "\"By the way is it possible to have a proposed layout with two panels like gnome ?\"\n\nAre you reffering to the OSX-like panel at the top in KDE 3.5x? I think that I read somewhere that it will be available later but not sure if it will be in 4.0. I guess there is someone here who can answer this question better than me."
    author: "Bobby"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-13
    body: "The Dot is turning into a kindergarden, part 1.\n-----------------------------------------------\n\n> > users who appreciate boring things\n\nI like boring things.\n\n> > and can't stand innovative ideas\n\nI can't stand innovative things.\n\n> > just to please those who are small minded\n\nI am small minded.\n\n> > don't let a bunch of small minded gnomes dictate the default use of a clunky clock\n\nI dictate. Also, I now am a gnome.\n\n> > The train station clock was awesome and cool, and the big clunky one isn't.\n\nNo, I declare cool. Naaaa na na naaaa naaaaaaaa.\n\n> > For those who like the old fashioned,ugly, clunky and boring clock (like it is now) choose classic\n\nI am old fashioned, ugly, clunky, boring.\n\n\nStay tuned, next week we are going to the theme park and Mike is going to get his birthday present, a cool, innovative wrist watch."
    author: "ac"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-13
    body: "Your last cited sentence is a product of your imagination. Besides, your short term memory for names (Marc!=Mike) seems not to be the best."
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-13
    body: "My daddy is better then your daddy. This corner of the sandbox is taken and if you don't like it, I am going to tell mommy.\n\nAgain, welcome to the Dot kindergarden, part 2.\n\nYou know, this 'Mike' is actually a little kid who is going to the themepark going to get a wrist watch - not YOU, are you so _dense_??? I didn't even know a name in this **amazing** clock thread.\n\nWhy do I think the Dot is turning into a kindergarden sandbox? Well, your short term memory is better then my short turn memory etc. etc. You even insist on projecting a Marc into something, I had no idea who Marc was (well, now I see, you used that name) You imply I confuse names. Little Mike has got nothing to do with any Marc here, unless you imply to insist yourself.\n\n\"Your last cited sentence is a product of your imagination.\"\n\nThis WHOLE freaking clock talk is a product of our imagination - the way we play in this AMAZING clock sandbox, is something else.\n\nSo, there you have it, my clock is better then your clock. (sticks out tongue)\n\nAnyway, I like both clocks, I really do. What I like even better, is our ability to choose between them.\n\n..and I'm telling mom!"
    author: "ac"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-13
    body: "that just leaves to say *plonk*"
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "As far as i understand the clock is a plasmoid, so any number of clocks can be easily created. Just drag the one you like onto the panel. :)"
    author: "YS"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-13
    body: "> > As far as i understand the clock is a plasmoid, so any number of clocks can be easily created. Just drag the one you like onto the panel. :)\n\nHalleluja."
    author: "ac"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-13
    body: "Don't be dramatic. Better do a test the RC2 instead, you'll notice that the trainstation clock plasmoid isn't there anymore."
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "I agree with you... I think it should be set as an option, like they did with the battery applet (classic theme and oxygen theme). For those who like the old fashioned,ugly, clunky and boring clock (like it is now) choose classic, for those who like the beautifully amazing animated train station-like clock, choose Oxygen style."
    author: "SVG crazy"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "yeah.. i'm of the same opinion.. the normal clock looks just sooo boring.\n\nplz bring the trainstation clock back! .. at least make it available with KHotNewStuff2"
    author: "Bernhard"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "We'll probably revive the fancy trainstation clock, but as a different plasmoid. It might take some time until it's in, and it's not critical to 4.0. If you care enough for it, show up on the panel-devel mailinglist and state that you'd like to help. Ruphy and I will be happily providing guidance how you can get it back. \n\nHope that addresses your issues with being bored by a clock. :-)\n\nAnd what is that, everyone being totally obsessed with clocks in KDE4, I like it because it's kind of strange and geeky, but sometimes I ask myself where this comes from ... :>"
    author: "sebas"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "It was removed?!?!\n\nI loved the train-station clock!"
    author: "T. J. Brumfield"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "But i don't understand... it was there, and you killed it, and now i should bring it back? Ahm, can't i not just have it?"
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "But i don't understand... it was there, and you killed it, and now i should bring it back? ;-( Ahm, can't i not just have it?\n\nAbout your question with the clock obsession: The love for clocks was spread by people like aseigo and the whole bunch of plasma guys, always using clocks as demonstration for about everything. Don't say clocks wouldn't matter now..."
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-14
    body: "The train station model and the current one sat in the same plasmoid for a bit. We've transitioned the first into the latter, then the first had to go because we wanted something really simple. Getting the train station model back is a matter of pulling it out of SVN from some weeks ago, renaming it so it becomes another plasmoid and backporting a bunch of fixes that went into the plain clock and are also applicable.\n\nInstead follow steps above, send patch to panel-devel, address comment and import it into extragear after approval.\n\nWait, let me start over, trying to go at the same level as your post:\n\n\"No, be patient, kid.\""
    author: "sebas"
  - subject: "Another Digital Clock issue eh?"
    date: 2007-12-12
    body: "Oh God, another Clock issue.\n\nIf I had a nickel for every time I've read \"Clock Applet\" mentioned in the same line as Plasma or the KDE4 Desktop, I'd be a rich man.  It's as if these clocks are what everyone is most impressed or disappointed with in KDE4.\n\n\n- Roey"
    author: "Roey Katz"
  - subject: "Re: Another Digital Clock issue eh?"
    date: 2007-12-12
    body: "It's because WE LOVE CLOCKs, especially the animated train station-like kinds. :)"
    author: "SVG crazy"
  - subject: "Re: Another Digital Clock issue eh?"
    date: 2007-12-12
    body: "I want a true digital clock with red LEDs."
    author: "reihal"
  - subject: "Re: Another Digital Clock issue eh?"
    date: 2007-12-13
    body: "> > Oh God, another Clock issue.\n\nIt's the prophecy of another trumpet, right on time."
    author: "ac"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "\"in a screencast Aaron once did there was this design with a horizontal line with an animated train station-like letter change. Now this was really kind of fresh and new\"\n-- \n\nNot at all a good default, as many - including me - *hated* that clock. It was horrendously ugly and gimmicky. Good call changing it, I say..."
    author: "you're a bummer"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "The majority of the posters around liked it. So obviously YOU are the narrow minded gnome, who pushed the poor developers into killing the trainstation design? ;-/ I'd say it should be default again, and you change your settings one time."
    author: "Marc"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "The majority of the posters around disliked it. So obviously YOU are the narrow minded gnome, who pushed the poor developers into adding the trainstation design? ;-/ I'd say it should be default again, and you change your settings one time."
    author: "Anon"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "* \"shouldn't be default again\", of course :)"
    author: "Anon"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "The horizontal line at train station displays is a design problem of the technology and not a feature. Why should we, especially by default, imitate it with great technology when even train stations are switching to electronic displays? The purpose of a clock is to show the time/date and not to be interesting. After some time I find the horizontal line annoying and that it makes interpreting the time a bit slower because I usually see numbers without horizontal lines."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: RC2 rocks, but the digital clock?"
    date: 2007-12-12
    body: "There are people who appreciate a hint of retro design, i'm sure you heard about the concept? And imitating something physical and functional as those clocks with great technology IS the cool thing about it. And it's what many love about kde, what sets it apart. \nBecause of that i want this as default, for the cool first impression. Should you really be troubled by that, you could easily change the settings."
    author: "Marc"
  - subject: "Koenigsegg of Desktops?"
    date: 2007-12-12
    body: "sorry, just had to. ;-)"
    author: "anon"
  - subject: "Re: Koenigsegg of Desktops?"
    date: 2007-12-12
    body: "Style matching wallpaper:\nhttp://www.koenigsegg.nu/Pic-11.html"
    author: "reihal"
  - subject: "No Luck with RC2"
    date: 2007-12-12
    body: "I have installed the openSuse packages but I am having no luck with this release. It starts quite fast and seems to be more responsive than previous versions but my kde session crashes and automatically logs out after about 2 minutes each time so I can't really test this version.\nAnother thing is that Lancelot is not working. All I get when I click on it's start button is the dark shadow of a menu. I don't know if anyone else is having these problems or if it's the Suse packages or my specific installation."
    author: "Bobby"
  - subject: "Re: No Luck with RC2"
    date: 2007-12-15
    body: "Lancelot is not part of KDE 4.0 release because it's immature."
    author: "Anonymous"
  - subject: "Oxygen Gradient"
    date: 2007-12-12
    body: "What happened to the whole-window gradient in oxygen? That one was really nice.\nThis way, it just looks... plain. Using the openSUSE version btw."
    author: "Knome"
  - subject: "Re: Oxygen Gradient"
    date: 2007-12-12
    body: "I think it accidentally got knocked out when the style devs were optimising memory consumption, or something like that.  I imagine it will be back soon."
    author: "Anon"
  - subject: "Re: Oxygen Gradient"
    date: 2007-12-12
    body: "I would like to see the simple gradient also back, the current one is hardly recognizable, if at all and the slowdown not worth.\n"
    author: "anon"
  - subject: "Re: Oxygen Gradient"
    date: 2007-12-12
    body: "well...\nThe fact is that there is a gradient. It's just too light to be visible.\nTry taking a snapshot of a window, then increasing the contrast of the image with your preferred image manipulation package.\n\nAs it is, that just wastes pixmap memory.\n\nThere should be a way to turn it off anyway, if the style is to be of any use over the network."
    author: "Luciano"
  - subject: "Error installing on Kubuntu"
    date: 2007-12-12
    body: "...\n..\nUnpacking kdebase-runtime-bin-kde4 (from .../kdebase-runtime-bin-kde4_4%3a3.97.0-1ubuntu3~gutsy1~ppa2_all.deb) ...\ndpkg: error processing /var/cache/apt/archives/kdebase-runtime-bin-kde4_4%3a3.97.0-1ubuntu3~gutsy1~ppa2_all.deb (--unpack):\n trying to overwrite `/usr/bin/kwriteconfig', which is also in package kdebase-bin\nErrors were encountered while processing:\n /var/cache/apt/archives/kdebase-runtime-bin-kde4_4%3a3.97.0-1ubuntu3~gutsy1~ppa2_all.deb\nE: Sub-process /usr/bin/dpkg returned an error code (1)\n\nA fix anyone?"
    author: "Andreas"
  - subject: "Re: Error installing on Kubuntu"
    date: 2007-12-12
    body: "Did you try\nsudo apt-get -f dist-upgrade\nsudo apt-get -f install\n?\n\nI had the same problem and it worked for me."
    author: "stormwind"
  - subject: "Re: Error installing on Kubuntu"
    date: 2007-12-12
    body: "I used dpkg --force-overwrite -i /var/cache/apt/name-of-package.deb\nfollowing that, I dist-upgraded and it all worked smoothly. It seems to happen if you have the rc1 packages installed, in which case, apt tries to upgrade them before upgrading kdebase."
    author: "lexxonnet"
  - subject: "Re: Error installing on Kubuntu"
    date: 2007-12-12
    body: "I do have the same error :-("
    author: "Birdy"
  - subject: "Re: Error installing on Kubuntu"
    date: 2007-12-12
    body: "I had the same problem as well last night. Gave up in the end. Looking for a nice easy workaround :)"
    author: "dave"
  - subject: "Looks cool!"
    date: 2007-12-12
    body: "I do think it lacks some contrast between the toolbars and the content window (speaking of the konqueror window).\n\nI'm really looking forward to using KDE4 as my main desktop!"
    author: "Joergen Ramskov"
  - subject: "Kwin effects"
    date: 2007-12-12
    body: "This is my first install of kde4 that I actually was able to get working, probably more due to kubuntu and my lack of knowledge.\n\nThe first impression is that it is a bit slow and that some effects are put in place just to show effects rather than having them used as a mean of help/indication what is happening.\n\nI.e. the effect used to minimise a window in kwin 4 rc2 is well... confusing, the vista aero effect is IMHO one of the only good implementations out there for such an action, which is just a composited version of what happens in kwin3 when minimising a window. (the aqua effect, and compiz lookalike are way to flashy distractive if you ask me)\nThis effect used in kwin 4 suits better with exiting a program than minising, just as that the opposite effect is IMHO better used to open a program, rather than being used for unminimising a window. Just as the fade away effect (now used for exiting a program) would suite better with closing a program which has an icon in the system stray.\n\nKeep up the good work though and don't let people like me bug you too much :D."
    author: "Terracotta"
  - subject: "Pager"
    date: 2007-12-12
    body: "Can anyone tell me how to integrate the pager plasmoid into the application-pannel? I still have it flowting on the desktop :-)"
    author: "Enrico"
  - subject: "Re: Pager"
    date: 2007-12-12
    body: "yeah, nothing is as useless as a pager in the background.\nstill, i didn't figure out how to add the pager to the panel.\nactually i didn't figure out how to change _anything_ in  the panel setup.\nusing kubuntu packages..."
    author: "rick"
  - subject: "Re: Pager"
    date: 2007-12-12
    body: "Currently, you will have to manually edit the plasma-appletsrc file.  Located in ~/.kde4/share/config/\n\nAdd the following at the end of the file\n\n[Containments][2][Applets][8]\ngeometry=1202,23,58,47\nplugin=pager\n\nYou may need to change \"8\" depending on how many applets you have in containment 2.\n\nHope that helps!"
    author: "rdw4h"
  - subject: "Re: Pager"
    date: 2007-12-12
    body: "A blackbox way to do it is drag the pager widget onto the *border* of the panel.  This then messes it up the panel totally (it shrinks in my case) and the pager is not displayed properly, but if you now logout then back in it will appear on the far-right of screen.  Only prob is that the left border of it is not drawn, clearly there are one or two things to iron out by the overworked plasma devs.  We cannot expect something like a mattise (netbeans) drag and drop experience just yet I think."
    author: "conan"
  - subject: "kickoff"
    date: 2007-12-12
    body: "kickoff is crap and will be until it it possible to go to a sub-menu without having the top-lever disapearing!"
    author: "dajomu"
  - subject: "Re: kickoff"
    date: 2007-12-12
    body: "Kickoff is quite usable for me but hey! why don't you try Lancelot? At least it's an alternative to Kickoff."
    author: "Bobby"
  - subject: "Re: kickoff"
    date: 2007-12-13
    body: "use the simple menu plasmoid, be happy and (i can only hope, dear $DEITY above) be quiet."
    author: "Aaron J. Seigo"
  - subject: "Re: kickoff"
    date: 2007-12-13
    body: "Your answer would make no sense... if it were indeed an answer."
    author: "Griobhtha"
  - subject: "Re: kickoff"
    date: 2007-12-13
    body: "*sigh* if you don't like kickoff, you can use the simple menu. it looks just like the kmenu in kde3.\n\nwas that clear enough?"
    author: "Aaron J. Seigo"
  - subject: "Re: kickoff"
    date: 2007-12-13
    body: "Thanks to whomever was kind enough to create said simple menu alternative."
    author: "T. J. Brumfield"
  - subject: "How to change Plasma Theme"
    date: 2007-12-12
    body: "Hi, \n\nFirst of all, I am a very big fan of plasma... Congratulations to all the plasma developers for their excellent work.\n\nSecond, I heard and read a lot about plasma being completely themeable, but I never found in documentation how to do it. Please, I am so excited about plasma and have good SVG skill, I would love to know HOW to make a plasma theme myself, and create a very very very gorgeous theme for the panel. "
    author: "SVG crazy"
  - subject: "Re: How to change Plasma Theme"
    date: 2007-12-12
    body: "many things will only come available after the 4.0 release, and I'm sure documentation about theming will be one of them. A lot of the GUI work which needs to be in plasma is not there yet. I think Aaron's plan of doing regular (monthly) plasma releases after 4.0 is out is a good idea ;-)"
    author: "jospoortvliet"
  - subject: "Re: How to change Plasma Theme"
    date: 2007-12-13
    body: "Since Plasma is the centerpiece of KDE (like Aero is to Vista!), wouldn't it be a much better idea to finalize Plasma issues before releasing a Broken Knome interface package that offers less in control and customization than its predecessor (or its semantic namesake)?"
    author: "Griobhtha"
  - subject: "Re: How to change Plasma Theme"
    date: 2007-12-12
    body: "I actually found this when browsing the Techbase Documentation. I stumbled upon it when reading the simple plasmiod tutorial.\n\nHere's the info:\nhttp://techbase.kde.org/Projects/Plasma/Theme"
    author: "Soap"
  - subject: "Re: How to change Plasma Theme"
    date: 2007-12-12
    body: "WOW, thank a lot for the link :D"
    author: "SVG Crazy"
  - subject: "Icons on desktop?"
    date: 2007-12-12
    body: "Where are icons on desktop? It's a rc2 and there isn't no support to icons on the desktop.. when it will be reintroduced?"
    author: "Marco"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-12
    body: "Drag'n'drop an icon to your desktop and watch it appear. Add the 'Desktop' plasmoid and see the icons in ~/Desktop...\n\nThe reason those are not there by default is because we think it sucked, and we want to make the desktop actually useful instead of a dumping ground for files, folders and applications..."
    author: "jospoortvliet"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-12
    body: "word!\ni appreciate that!"
    author: "xapient"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-13
    body: "so its more useful to display a pretty picture on the desktop than it is to display items that the user explicitly put there?"
    author: "Dan"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-12
    body: "and what about animated icons ???, what is the state of this, will be for 4.1, the same for the icons with quick acces, to avoid the clicks.\n"
    author: "rudolph_"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-12
    body: "\"animated icons ???\"\n\nWhat do you mean, precisely? "
    author: "Anon"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-13
    body: "sorry, i was confused... ;)\n\nwhat is the state of icons with litle quick access button."
    author: "rudolph"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-13
    body: "support for the buttons are still there and they work, but the actions have been disabled for the time being. it needs a way to pull actions based on mimetype, essentially like servicemenus but a bit more focused."
    author: "Aaron J. Seigo"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-13
    body: "CLOSED: Dupe. (see above where this has already been asked and answered)"
    author: "Aaron J. Seigo"
  - subject: "White screen with compositing enabled"
    date: 2007-12-12
    body: "On both my i915 desktop and my i945 laptop when I enable the effects in the kwin config-module, I get a completely white screen and the only exit is CTRL-ALT-BACKSPACE.\n\nIt seems that clicks are still getting through to the desktop, because sometimes my random click seem to open another (white) popup menu.\n\nDoes anyone see the same?\n"
    author: "yves"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-12
    body: "It's an X11 driver issue. I had the same problem with Kubuntu, but the OpenSUSE worked fine. "
    author: "Boudewijn Rempt"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-12
    body: "Are you using xorg or xgl? Did you edit the xorg.conf file (if you are using xorg) to use composite? KDE4 doesn't seem to like xgl. I couldn't get compositing work with xgl."
    author: "Bobby"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-12
    body: "I have the same problem on my kubuntu laptop."
    author: "Anonymous"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-12
    body: "I use plain xorg with aiglx and compositing enabled.\nNote that compiz and komposer run on these boxes without trouble, so does xfce4.\n\nIIRC I also tried with suse on the i945 laptop, and kwin complained that composite and xdamage were not available, even they clearly were. (That was with packages from opensuse, ca. 10 days old)\n"
    author: "yves"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-12
    body: "s/komposer/kompmgr"
    author: "yves"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-12
    body: "I have the same problem if I choose OpenGL in the \"advanced options\" box, but it works fine (but slow) with Xrender.\n\nAlso, there is a tool wich switch the backend and asks if every thing is OK. If after 10 seconds nothing has been answered, it return to previous config. So I had no problem with it if I did from the system setings window."
    author: "kollum"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-13
    body: "I also tried Xrender and it worked fine. I then switched back to OpenGL and the problem was back so the problem is obviously with OpenGL. One doesn't get all that amount of fancy effects with Xrender though and it's a bit slow like you pointed out."
    author: "Bobby"
  - subject: "Working OpenGL i945 xorg.conf for openSUSE/Debian"
    date: 2007-12-13
    body: "http://www.pastebin.ca/814396 works fine on Debian and openSUSE 10.3 on an X60 with intel. \n\nHTH"
    author: "Bille"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-12
    body: "I had the same issue (ATI r300 chipset w/ 6.7.192 opensource drivers, xorg-server v1.4, mesa v7.02, aiglx, on Gentoo). I changed my config from using XAA rendering to EXA rendering, and it became useable.\n\nUnfortunately, the shadows don't draw cleanly, the effects are slow, and for some reason, the effects aren't turned off when I disable them from system settings. I'm chalking it up to not a wide enough test-base at the moment."
    author: "Soap"
  - subject: "Re: White screen with compositing enabled"
    date: 2007-12-12
    body: "I've got the same issue with an r350(9600 Pro) and the DRI drivers. I haven't tried EXA because it becomes unusable for me. However, with XAA and everything else Compiz/Beryl/Kcompmgr(kde3) work just fine, while Kwin-composite in KDE4 doesn't."
    author: "lexxonnet"
  - subject: "Re: White screen with compositing enabled"
    date: 2008-07-30
    body: "Hi,\nI have the same problem with the Intel Graphics Card,\nunfortunatly the pastbin file has been deleted:\nhttp://www.pastebin.ca/814396\n\nCan someone send the solution in for me?\n\nThanks in advance"
    author: "Eshat"
  - subject: "Re: White screen with compositing enabled"
    date: 2008-10-20
    body: "I have exactyl the same issue. Using archlinux and KDE 4.1. I can render with xRender but not with OpenGL :("
    author: "Sagattarii"
  - subject: "Always show \"plasma\" background"
    date: 2007-12-12
    body: "I really like the idea of having the choice of hide/show the background of plasma for the applets, just like was made for the battery applet on its configuration dialog. Wouldn't be great if we have that option on all/almost all applets?"
    author: "SVG crazy"
  - subject: "Re: Always show \"plasma\" background"
    date: 2007-12-12
    body: "yeah.. would be really great to have this feature.. hope it will be ready for 4.0"
    author: "Bernhard"
  - subject: "Re: Always show \"plasma\" background"
    date: 2007-12-13
    body: "i think you'll find this renders a number of plasmoids not particularly usable. and turning this on/off for every object individually seems a bit on the absurd side. \n\nbut hey, i could be wrong. you could try a patch that adds this somewhere in applet (context menu? applet handle?) and send it to panel-devel at kde dot org where everyone could try it out."
    author: "Aaron J. Seigo"
  - subject: "Two or more lines on taskbar"
    date: 2007-12-12
    body: "I was wondering if it will be possible on KDE 4.0 to config the number of lines on the taskbar. Today, there's just one line with big icons.\n\nBy the way, kde 4.0 is going to ROCK!"
    author: "Somebody from Brazil"
  - subject: "Re: Two or more lines on taskbar"
    date: 2007-12-12
    body: "No."
    author: "Dan"
  - subject: "plasma panel"
    date: 2007-12-12
    body: "what happened with functionality of panel. How to resize it, how to move appets, how to add apets, how to show realy cool clouds (like KDE 3.5), how to config window list (2 rows instend one) ... Could I remove (or something else) panel? \n\nWhat about removing right top corner - REMOVE CONFIG BUTTON!\n\nI'm useing Suse live cd\n\nI will use kicker in KDE4, waiting for 4.1 Raptor menu"
    author: "mihas"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: "Raptor is going to replace kickoff, not the panel."
    author: "Dan"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: "that's not sure, btw, there are currently 5 things which could happen:\n- we keep kickoff (not as unlikely as you think, once used to it many ppl like it)\n- we go back to an old-style menu (I don't think that's very likely, but maybe...)\n- we use Raptor (once it's finished it might have a good chance)\n- we use Lancelot (again, seems like a pretty good option)\n- or something else pops up... Not unlikely, given the architecture Plasma offers ;-)"
    author: "jospoortvliet"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: "In KDE3 you could bring the menu up by pressing a hotkey and\nthen you could navigate the menu with cursor keys. The kickoff\nmenu seems to be entirely mouse-driven and this is not good.\n\nIf a new menu system will replace kickoff in the future, hopefully\nit will be something you can navigate with keyboard.\n "
    author: "mipmip"
  - subject: "Re: plasma panel"
    date: 2007-12-13
    body: "> The kickoff menu seems to be\n> entirely mouse-driven and this is not good.\n\nthere is no hot key currently to bring it up, but once visible it is indeed completely keyboard navigable."
    author: "Aaron J. Seigo"
  - subject: "Re: plasma panel"
    date: 2007-12-14
    body: "OK, you're right.\n\nTab to jump between the menu and the search prompt. Enter to go into a submenu; Esc to go back to a parent menu. Left and right cursor keys to swith between different main menus (like Favourites and Applications).\n\nStill, I'm glad to see that the latest KDE commit-digest shows work being done on a KDE3-style \"simple menu\" for the new panel, too.  :-)"
    author: "mipmip"
  - subject: "Re: plasma panel"
    date: 2007-12-13
    body: "By replace I mean its going to do the same thing as kickoff, not the same thing as the panel.  I assumed the reader would be smart enough to follow the conversation and do appropriate interpretations in his/her head.  I can see I was wrong."
    author: "Dan"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: "ok I think same :-)\n\n"
    author: "mihas"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: "I would like to know the same... if the new panel will be configurable. It's kinda awkward for people like me who are used to Kicker. Right click with the mouse doesn't give one any configuration options. I hope that these little issues will be solved before KDE4 is released, otherwise the devs should not be afraid to postpone the release date until everything is working the way it should. We would be very disappointed to have a KDE4 with less functionality than 3.5x.\n\nI would like to say a big thanks and praise to the devs for the tremendous work that they have done and ask them please not to let the public or anyone pressure them into releasing KDE4 before it's finished!"
    author: "Bobby"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: "Kicker is dead, Plasma is new - many things available in kicker have gone, and will only re-appear in later KDE releases. Luckily, the bugginess of some areas of kicker and all the hard-to-maintain code is also gone ;-)\n\nBut yeah, you can still use kicker if you want. And some of the things you mention will appear before 4.0 is out."
    author: "jospoortvliet"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: " >> How to resize it ?\nopen kwrite or an other text editor. open you ~/.kde4/share/config/plasma-appletsrc and edit it (hum, trying to understant it may be usefull to ). Save. Restart plasma.\n\n>> how to move appets ?\nSame as above.\n\n>> how to add apets ?\nDrag them from the \"add widgets\" window to the panel.\n\n>> >> how to add a panel ?\nSee first point."
    author: "kollum"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: ">> Drag them from the \"add widgets\" window to the panel.\nDidn't works. \n"
    author: "mihas"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: "So it has to be a distrib related bug. Always worked fine here since RC1 I think."
    author: "kollum"
  - subject: "Re: plasma panel"
    date: 2007-12-12
    body: "Do not drag on widgets, drag them on the border of the panel.it should really work, but still many widgets will break it..."
    author: "Emil Sedgh"
  - subject: "Re: plasma panel"
    date: 2007-12-13
    body: "> How to resize it,\n\ncoming\n\n> how to move appets, \n\nditto\n\n> how to add apets, \n\nsame\n\n> how to show realy cool clouds (like KDE 3.5)\n\ni have no clue what you are talking about.\n\n> how to config window list (2 rows instend one)\n\neventually."
    author: "Aaron J. Seigo"
  - subject: "Re: plasma panel"
    date: 2007-12-13
    body: "> > how to show realy cool clouds (like KDE 3.5)\n \n> i have no clue what you are talking about.\n\nIf you run kweatherapplet and get really close to the screen those cloudy pixels get quite trippy :)"
    author: "Bille"
  - subject: "Re: plasma panel"
    date: 2007-12-24
    body: "I am a longtime KDE user and one of the things keeping me away from GNOME was the possibility of configuring almost anything easily and faster. Now, in KDE 4 we have plasma. I can't argue about the improvement that plasma is, but if the panel stays that way, many users will migrate from KDE to GNOME or other window managers. \n\nFor example I use kicker with height of 38px, which on 96 DPI is the minimum for having two rows of apps at 1280x1024 resolution. In KDE 3.5.8 all I have do do is to click several times and enter the desired values. In KDE 4 RC2 - still no success about this and plasma takes too much space on my screen! Also about changing the size of kickoff - in KDE 3.5.8 just drag the top right corner of the menu, in KDE 4 RC2 - no chance to do it. Hope this will change before KDE 5 (whenever it is).\n\nAnd finally what means \"eventually\" in:\n\n             >>how to config window list (2 rows instend one)\n             >eventually.\n\nwhen this is one of the best features of kicker?"
    author: "Hedron"
  - subject: "Re: plasma panel"
    date: 2007-12-24
    body: "\"but if the panel stays that way, many users will migrate from KDE to GNOME or other window managers.\"\n\nIt won't stay that way.  Why are people simply assuming that it will? Did KDE gain no features or configuration options at all between 3.0 and 3.5? No.  So why do people assume this will be the case for KDE4?\n\n\"Also about changing the size of kickoff - in KDE 3.5.8 just drag the top right corner of the menu, in KDE 4 RC2 - no chance to do it.\"\n\nThere have been a few discussions about adding this functionality, but so far no one has stepped up to do it.  When someone does, you'll have it.\n\n\"Hope this will change before KDE 5 (whenever it is).\"\n\nSince KDE5 won't likely even be started until 2012, I think you're setting your sights too low :)\n\n\"And finally what means 'eventually' in:\n>how to config window list (2 rows instend one)\"\n\nhttp://dictionary.reference.com/browse/eventually ;)\n\nIf you want a non-snarky answer, then it will be done when someone codes the functionality and submits the patch; simple as that."
    author: "Anon"
  - subject: "Gentoo packages"
    date: 2007-12-12
    body: "There are also packages for Gentoo, although the page is a bit outdated, the packages are up-to-date:\n\nhttp://genkdesvn.sourceforge.net/\n\nWould be nice if you could add this to your announcement."
    author: "Hanno"
  - subject: "App stability"
    date: 2007-12-12
    body: "I've stopped using the full KDE4 session. I give it a try now and then, but the desktop experience is, despite some eye candy, not ready for me concerning usability and productivity. Reasons for this have been discussed over and over here and elsewhere (Kickoff, new Panel, missing configuration options etc.) and need not to be discussed again, as nobody seems to acknowledge this.\n\nHowever, I continue trying the KDE4 apps on a KDE3 desktop. \n\nWith the current RC, I once again tried some (IMHO) key apps of KDE4 in daily use. However, even here I'm a bit disappointed with the current state - giving that we are talking about RC2. Many bugs have been around for as lang as I try the KDE4 betas!\n\nKMail: Unusable because I'm afraid of losing data. Especially with IMAP nothing really works. kioslave begins to eat up CPU time and temporarily messes up the account.\nKonqueror: Definitely getting better every release, however still not even up to Konqueror3 concerning the amount of sites rendered correctly????!?!?! I sincerely put my hopes in WebKit some time...\nCrypto-Stuff: Everything in connection with Crypto/Certificates is kind of messed up. KMail and Konqueror don't always remember certificates, and if you want to manage this stuff with Kmail/Konquerors settings module, only strange letters come up for this certificates.\nKonsole: YES! The one and only applicatioin which runs since the first betas and is really an improvement in comparision to the already great KDE3-konsole!\n\nI know that probably all these apps are not considered as part of the core KDE, however... there are still quite some things to get done in KDE4! And yes, I know, most programmers develop KDE for free and all that, but nevertheless..."
    author: "John Sawyer"
  - subject: "Re: App stability"
    date: 2007-12-12
    body: "\"Reasons for this have been discussed over and over here and elsewhere (Kickoff, new Panel, missing configuration options etc.) and need not to be discussed again, as nobody seems to acknowledge this.\"\n\nIt's been acknowledged several times and - guess what - devs are working on all of the things you mentioned as we speak!\n\nWhat on Earth gave you the impression that no one was acknowledging these complaints?"
    author: "Anon"
  - subject: "Re: App stability"
    date: 2007-12-12
    body: "OK, let's put it another way: It has been in some aspects acknowledged, but not been given priority concerning the RCs.\n\nHowever, my point was something else: I consider the state of some of the core apps as not up to a RC.\n\nKonqueror messes up to much. Something with the cookie management is broken (e.g. Ebay sometimes has hickups when logging in. Certificate management for https:/ seems to be broken. Konqueror renders less pages correctly as previous versions. Some settings are not saved.\nMaybe I'm old-fashioned, but I don't like Dolphin and Firefox, but for me Konqueror (and the embedded technologies) always were the prime example for KDEs superiority. Konqueror is for me still the main app on the desktop, and it's not yet up to daily usage because of bugs that have been around for quite some time.\n\nIt's the same with some other apps like KMail, which still is, at least for me, completely unusable.\n\nI know, the word is out that KDE4 is only about the library stability, but I'd rather have a set of working and seemlessly integrated core apps, too."
    author: "John Sawyer"
  - subject: "Re: App stability"
    date: 2007-12-13
    body: "PIM just won't be released with KDE 4.0. Divide the number of people working on it (with time) by the number of applications and you'll understand why. Especially for data-critical apps like PIM. To some extent we *have* to rely on users wanting to scratch the PIM itch enough to become developers here - there aren't enough people on the PIM project otherwise.\n\nMore information on PIM -- and even people posting \"how can I help?\" in blog comments sections -- may be found on <A href=\"http://pim.kde.org/development/\">the internet</A>."
    author: "Adriaan de Groot"
  - subject: "Re: App stability"
    date: 2007-12-13
    body: "> but not been given priority concerning the RCs.\n\nlet me acknowledge it right now then: these are known issues, will be done for 4.0.\n\ni hope that now *you* acknowledge it, as i've said this over and over. how do you think that makes me feel? hint: like you and the rest of your tribe aren't paying attention.\n\nyou at least seem to care enough to provide some feedback. that's something, i suppose."
    author: "Aaron J. Seigo"
  - subject: "Re: App stability"
    date: 2007-12-13
    body: "Aaron, I continue to be amazed about how patient you are. Most of us who thinks things are moving along nicely and don't have any major gripes stay silent. That doesn't mean that we don't appriciate your (and the other devs) work."
    author: "Oscar"
  - subject: "Re: App stability"
    date: 2007-12-13
    body: "Thanks very much for that wonderful imput. I guess some of us unconsciously try to but the devs under pressure but that's the last thing that they should have now. In spite of that I personally believe that everyone here (even those who criticize) greatly appreciate the invaluable work that the KDE team is doing. \nI once tried to program a very simple calculator and that was a heck of a job (with a thousand bugs of course). Imagine these guys are programming a whole desktop!"
    author: "Bobby"
  - subject: "It doesnt seem to start for me under kubuntu gutsy"
    date: 2007-12-12
    body: "Well, I downloaded and installed the packages as per the instructions on the kubuntu site, but no dice. I can run some kde4 apps from my normal kde3 session, but when I try to start a Kde4 session from kdm nothing happens. I get a blank screen for a few seconds, then the kdm login screen re-appears as if nothing had happened.. hmm."
    author: "Meh"
  - subject: "Re: It doesnt seem to start for me under kubuntu g"
    date: 2007-12-13
    body: "put\n\nexport KDEHOME=~/.kde4\n\nto\n\n/usr/lib/kde4/bin/startkde\n\nand it should work"
    author: "hias"
  - subject: "Re: It doesnt seem to start for me under kubuntu gutsy"
    date: 2007-12-13
    body: "I can top that.  I installed the Kubuntu packages exactly as the instructions specified, and it uninstalled my entire KDE3!  I was able to test drive KDE4 OK.  From  a console, I reinstalled KDE using apt-get, and I had my old faithful back.  No real harm, just had to click out of all the first-run dialogs again.  Good news is that I now have 3 and 4 working.  Sweet."
    author: "Louis"
  - subject: "Yuck!"
    date: 2007-12-13
    body: "I've always been a big fan of KDE, but 4.0rc2 seems to be totally not KDE. One of the great things about KDE was the ability to configure it however you wanted, but now you can't configure anything anymore. Its a total nightmare -- like the Gnome developers kidnapped and replaced all of the original KDE staff. All I can say is I won't be switching to KDE 4 unless it goes through some major changes.\n\nPlease, please, please bring back the KDE philosophy that the user should be able to configure their GUI how they like. Eye-candy is nice but giving me the power to choose is really what I thought KDE and FOSS was all about.\n\nThanks to the KDE 3 people you've done great, and I love KDE. Sadly my KDE is gone."
    author: "Sam"
  - subject: "Re: Yuck!"
    date: 2007-12-13
    body: "I totally agree with you Sam.  KDE 4 has become more Gnome than Gnome.  They should change the name to BKE for Broken Knome Enamel.  The Plasma interface is (to paraphrase an old proverb) the look of honey with the taste of ash.  What's really astounding are the statements that this is \"Release Candidate\" quality material and that it is complete for final release except for minor tweaking.  RC2 is a beginning alpha at best.  There is nothing there!  Behind the candyass look there are NO serious controls and NO configurations.  This is a bad turn for KDE."
    author: "Griobhtha"
  - subject: "Re: Yuck!"
    date: 2008-01-08
    body: "It's not done yet. Stop whining."
    author: "Coolvibe"
  - subject: "Re: Yuck!"
    date: 2007-12-13
    body: "amen\n\nIt's like the whole desktop was designed after listening to gnome users complain about KDE on /.\n\nThe only criticism I ever agreed with was KDE's fondness for distracting eye candy but that never bother me because you could turn it all off. By all I mean all.  No shadows, no heavy themes, no shadows or compositing, no animations, none of it.  If I want something that's nice to look at, I'll go to an art museum.   Now it seems like the eye candy has been made the whole point.\n\nHello.  Part of the point of running linux is that forced hardware obsolescence is stupid.  I should be able to use the same machine until it dies and not be forced to get a new one in order to upgrade my OS shell.  "
    author: "Velvet Elvis"
  - subject: "Re: Yuck!"
    date: 2007-12-13
    body: "Another possibility is that Havoc Pennington left a bunch of seed pods at a KDE developers conference somewhere"
    author: "Velvet Elvis"
  - subject: "Re: Yuck!"
    date: 2007-12-13
    body: "With plasma you'll be able to totally swap out anything and replace it with another plasmoid. Personally I don't really notice what options your talking about (I still see plenty of configuration dialogs), but regardless in the long run (probably not even that long) there will be many many choices you can make. And not just little piddly things."
    author: "Ian Monroe"
  - subject: "Bunch of drama queens"
    date: 2007-12-13
    body: "The Plasma config is just not done yet.  No konspiracy."
    author: "Bille"
  - subject: "Re: Yuck!"
    date: 2007-12-13
    body: "LOL, it seems like Gnome is the greatest nightmare for KDE users. Anyway don't worry, by the time the next RC is ready you will realize that the Gnomies haven't managed to kidnap our heroes ;)\nKDE4 will be more powerful that all the versions before."
    author: "Bobby"
  - subject: "O, come on!"
    date: 2007-12-13
    body: "Stop whining! By now everybode must know that:\n\n1. This is an RC for a techonology preview version (called v4.0) in the KDE4 series.\n2. It's a work in progress where plasma and the panels are still in heavy development. Please check out all the other greatness.\n3. It's called RC. OK, maybe that's a bit confusing.\n\n"
    author: "Fred"
  - subject: "Might I suggest"
    date: 2007-12-13
    body: "Using the same terminology as, say, the rest of the world rather than calling dogs cats and cats dogs?\n\nIf you call something a release candidate, you'll get people installing it expecting something that's feature complete and of release quality, because that what release candidate.  If you call it RC, it means that to the best of your knowledge it's ready to release as a stable version but needs more testing to be sure.\n\nAt best this makes the KDE developers look out of touch with the rest of the software ecosystem and does not build confidence.\n\nIt was my impression that in this case it was being used to mean that core features are presumed stable but bells and whistles still need tweaking because that would at least be in the same ballpark as how the rest of the world uses the term.   No such luck.\n\nNewsflash.  This is a buggy beta release at best.\n"
    author: "Velvet Elvis"
  - subject: "Re: Might I suggest"
    date: 2007-12-13
    body: "Newsflash: missing features != bugs."
    author: "Paul Eggleton"
  - subject: "Re: Might I suggest"
    date: 2007-12-14
    body: "news flash\n\nrelease candiates are NEVER FEATURE COMPLETE\n\nexample microsoft has just release vista sp1 RC to the general public, 300 bug fixes, and they have stated this is not the finished product and there is much more to come in it.\n\nFinal releases are feature complete, not beta's not RC's.  And if you knew the reasoning behind kde4 you will know it wont be feature complete until around 4.1 that 4 is being released so that people can port their software to it.\n\ngezzzz, I can't count the amount of times I have read kde people explain that to you people and still you don't get it, and cry"
    author: "Richard"
  - subject: "Re: Might I suggest"
    date: 2007-12-14
    body: "Don't tell me, tell the parent poster - I agree with you :)"
    author: "Paul Eggleton"
  - subject: "Re: Might I suggest"
    date: 2007-12-14
    body: "\"Microsoft breaks it too!\" should not be an excuse.\n\nThe way I understand it is that once you're get all the redefined vocabulary out of the way, things aren't going to be out of beta until 4.1.\n\nThe reason I'm hacked off is because I was tricked into beta testing on my work machine and lost the better part of a day's work as a result.\n\nOne more time.  \"Release candidates\" = \"this software has the potential to be bit for bit identical to the final stable and feature complete release.\"\n\n\n"
    author: "Velvet Elvis"
  - subject: "About version numbers"
    date: 2007-12-14
    body: "> 1. This is an RC for a technology preview version (called v4.0) in the KDE4 series.\n\nThank you. \"Technology preview\". That's the thing all those people who keep whining don't seem to understand...\n\nI don't expect the actual release to be totally feature complete and totally bugfree. I don't even expect it to contain all features found in 3.5.x.\n\nThe 4 in 4.0.0 means it is using new APIs and new technologies etc, breaking continuity from the 3.x series.\n\nThe .0.0 means it is the very first \"release\" version (as opposed to alpha, beta, rc), which means those technologies and their applications have hardly had time to mature, and libraries have hardly had time to get applications.\n\nComparing to Windows is misleading, because Windows has had punctual releases for a while now (95, 98, etc) which people buy, which are supposed to be stable, and get patches now and then.\n\nKDE, and most open source software, on the other hand, has a continuous development - we will have 4.0.0, 4.0.1, 4.0.2, 4.1.0, etc.\n\nIf you want absolute stability and \"full-featuredness\" and configuration dialogs for every single detail, and all kinds of plasmoids, just wait for a few extra releases!\n\nAnd yes, it is called \"whining\". Calling things \"crap\", etc, is not exactly constructive criticism. Saying this and that should be configurable just shows you haven't been following the development AT ALL."
    author: "tendays"
  - subject: "Re: About version numbers"
    date: 2007-12-14
    body: "(by the way when I say \"you\" I mean whoever is whining that way - not the GP in particular)"
    author: "tendays"
  - subject: "Re: Yuck!"
    date: 2007-12-14
    body: "C'mon, this is ALREADY the most configurable KDE version EVER! The technology for many configurations is already there, but there are some configuration dialogs missing on RC2. I bet many of them will come for KDE 4.0, for instance, the configuration dialog for the panels. They are not there yet because the developers had got to worry about the most important thing first (the technology, which makes things actually work), since there aren't sooo many developers, they had to have priorities...\n\nBut don't worry, you will be able to configure things and you will see KDE 4.0 and 4.X series will be a dream coming true. Make configurations dialogs and menus for configuration is the easiest part."
    author: "SVG Crazy"
  - subject: "Re: Yuck!"
    date: 2007-12-16
    body: "I've always tought peoplke complaining about the betas where exagerating a bit.\nSo today I've tested KDE 4.0.0 on Ubuntu Gusty and man... what a BAD first impression!!\nSpeed isn't nice but that was expected from a debug build, it really uses a lot of I/O to write debug messages so it's ok and probally final version will be even faster than KDE3 series.\n\nVisual is terrible! A nighmare!! Oxygen style is crappy! All constructive critics made during months here and there whent to deaf ears. BUT (big but) I've never used KDE default stile for more than a day, so I'll just use other thing. Sorry for people who stick with the default :-(\nAnother problem is plasmoids visual, it just dosen't mix with Oxygen style. It's like having two toolkits in the same DE, like a mix of ghnome and KDE.\n\nTalking about plasmoids... man! It's way too late! They just ... exist! No configurations, no answer for right click in most cases... I was lost when clocking on clock didn't call calendar! OK, you can tell me it's a work in progress, it will be fixed in 4.1... but it shouldn't! It should be very good already.\n\nI was planning to keep KDE 3.X until version 4.1, but now I'm seriously thinking about 4.2 or even 4.3... Blah! I'm installing fluxbox right now! KDE4.. sorry, double sorry, sucks :-("
    author: "Iuri Fiedoruk"
  - subject: "I'm going to say this only once"
    date: 2007-12-13
    body: "To all those naysayers, just let us code. It is KDE 4.0.0. It's just the beginning. There is no possible way for us to get KDE 4.0.0 completely polished now. You could extend the release date another 1 year but then who would begin porting their apps to KDE 4? \n\nI'm just going to ignore all the negative comments and then wait for those to eat their words once things are polished up. Then you can beg for forgiveness, and maybe we'll accept your apologies :P\n\nTrust me, when you look back at this whole affair everyone will say how cool KDE 4.x is and that they should've believed the developers in their vision.\n\nShawn."
    author: "Shawn Starr"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-14
    body: "I think if you read just the positive comments you will miss out on a lot of good points and discussions.  You may not like the negative comments, but remember they are coming from the kde users, do ignore what they say because you don't like the comments and focus on the positive is how you end up creating microsoft software ;)\n\nplus you need to understand that the 3.5 release was manily to linux people, now with the release of 4, you have a lot of windows people joining the kde and linux community who have no idea, through no fault of their own, how things work with kde releases.  Be kind to windows people, for they know nothing but buggy operating systems that are released buggy and end their life buggy, lol"
    author: "Richard"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-14
    body: "True, There's a a mixture of good and negative comments. Sure, but our KDE users need to understand, Rome wasn't built in one day, nor will KDE 4.0.0 be. If we have more developers things can move faster, so, Help us! :)\n\nShawn."
    author: "Shawn Starr"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-14
    body: "agree, but remember to window's users they never go through any of this.  They buy their XP, or Vista, etc, and are lead to believe that is the final version, until a few weeks latter bug fixes come out, a few months latter service packs come out.  What I would like to see from dot is to show people how KDE is developed.  Then maybe people will realise, that KDE is not a weird piece of software like windows, and that it continues to grow and develop with each release.\n\nI mean lets face it, I've read comments in here claiming that Release candidates are meant to be feature complete.  God only knows where that comes from.  Perhaps they should read about Vista SP RC that was just released to the public and how it was explained that it is not complete and still a lot more to come.  OR they could read the opensuse RC's and see how they were also not feature complete, yet KDE is meant to buck the trend and be feature complete in RC's, go figure, lol\n\nI just think a nice article, dumbing everything down to explain to people that hey, RC's are your way of testing, and improving, finding bugs, etc, etc.  That KDE 4 is your way of releasing so people can port their software to KDE, and that KDE in it's essence is something that is never finished and will always grow and change, it is the nature of it.  Does that make sense?  KDE 4 has a shit load of new users who simply do not understand how it is made and released, etc.  I think dot needs to do an article on that.  It's the windows effect,t hey come to kde expecting it's done like that.  I don't even know if people realise you all do this for the love of it."
    author: "Richard"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-14
    body: ">>God only knows where that comes from.\n\nFrom years of software development standard language:\n\nhttp://en.wikipedia.org/wiki/Release_candidate#Release_candidate\n\nGranted, the release team is free to use whatever language they like for their releases, and they have explained that they are allowing some leeway for UI stuff, but that doesn't mean they should get pissed when people are confused by it.  On the flipside, there's no reason for commenters to be dicks to the developers; that's not gonna have positive results. "
    author: "Louis"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-14
    body: "It's up to the developers to decide if their candidate is a RC.  For people to say it has to be feature finished is incorrect, and naive\n\nJust recently Vista 1 SP1  RC released today XP SP3 RC released, and both microsoft has openly stated these are not feature complete, and there is more to come in them.  so perhaps you would like to go and tell them also that they can't be release candidates because they don't go by what you believe an RC should be\n\nIt is solely the decision of the developers to what they term a release candidate.\n\nLook at the linux community.  Opensuse RC, they were not feature complete, It is anal for people to argue with the people releasing their own software on the terms of what they call it.  From what I have read the developers have every right to get pissed because the people constantly moaning have not read any of the explanations of how they are releasing, and keep moaning and complaining about the  same thing without listening.  \n\n"
    author: "Richard"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-14
    body: "\"It is solely the decision of the developers to what they term a release candidate.\"\n\nTrue. Especially considering the fact that many are doing this in their free time. But nevertheless they shouldn't be surprised by negative feedback if they choose a different definition than anybody else ;).\n\nThe times whern KDE could be a considered a toy just for geeks are over, that's good. But this also means that you have to play by the same rules than the competitors. Microsoft gets negative comments when releasing a crippled version as final version of their OS despite much marketing hype, and nowadays the same goes for KDE.\n\nI would suggest - with no authority but that of a reader of the dot - that everybody cools down. And for the next release sit back and learn from the experiences the KDE4 release has brought. All of this mess could have been avoided by simple steps like replacing \"beta\" with \"alpha\" or \"final release\" with \"technology preview\" etc.\n\nNobody would have get hurt and really everybody would be happy with the great bunch of code and possibilities KDE4 is even now. So why not mark this simple naming problem as \"leasons learnt\" for the next release cycle and leave it be?"
    author: "Anon"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-15
    body: "True. Especially considering the fact that many are doing this in their free time. But nevertheless they shouldn't be surprised by negative feedback if they choose a different definition than anybody else ;).\n\n\nWhy use microsoft? because like it or not they are the largest player in the market, and the fact that they, as well as many other developers are releasing RC's that are not feature complete shows that the meaning of the word has progressed and changed.  And the way that the meaning has changed has shown anon that their definition is not different to others, but that the market and release cycles are changing.  As was evident with Vista RC1 in 06, which blew all the arguments up that because it still needed a lot of work, and was not feature complete it wasn't an RC.  And a year latter KDE has followed track, Opensuse releases RC's that are not feature complete.  The meaning has changed.  \n\nI think also this will continue into KDE 4 release, people will be complaining that all the software isn't ported over and claim it isn't a release, in all honesty, who cares.  All I care about is getting my hands on it, lol"
    author: "Richard"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-14
    body: "I'm rather surprised to see you use a Microsoft practice as a justification for calling the current KDE4 a Release Candidate.  Look at the meaning of the words.  Something labelled as such is commonly understood to be a _Candidate for Release_.  That is to say, it is something that is believed to be done, something that can and will be released barring the discovery of Release Critical bugs.  It is by definition feature complete.  There is no excuse for labelling the current state of things as a Release Candidate.\n\nNow doing so is not a cardinal sin.  Fixing, adding and coding things is far more important than dickering over semantics.  I'm not going to reject KDE4 because of a terminology screwup.  But a spade is still a spade, and it will have negative repercussions among those who understand the common definition of Release Candidate and are expecting something a little more complete.  Then again, the same people will likely be disappointed with what will eventually be labelled \"final\" and will need to have the concept of *.0 software explained to them ;)\n\nBottom line: RC is a terrible choice of label, but I totally wasn't stuck on it until people started defending it.  Illogicality is what actually set me off ;)"
    author: "MamiyaOtaru"
  - subject: "Re: I'm going to say this only once"
    date: 2007-12-14
    body: "Nicely put...\n\nI'm looking forward to KDE 4 and appreciate the coders hacking on it, really.\n\nBut the way any criticism is nowadays handled as evil flaming coming from stupid users really is disgusting.\n\nAs must criticisms are repeated from different sides, why cannot anybody say \"OK, maybe it takes a bit longer, maybe the naming was uncommon and therefore a bit misleading, we'll think of that next time. But look at this and that, we are really getting there. This and that is not yet there, but wait, it will come.\"\n\nBut, on the contrary, anything not praising the new KDE4 here on the Dot is getting flamed. That's a sad development. After all, it's the users that count if you programm a desktop.\n\nIn fact, I have to admit that the way negative response to KDE4 is handled here on the Dot, even by developers, instead of professionelly taking it into account as user feedback by supporters of KDE, is for me more strange than any naming problems."
    author: "Anon"
  - subject: "Autostart"
    date: 2007-12-15
    body: "I was wondering how you select apps for Autostart."
    author: "LTV"
  - subject: "Buggzzz"
    date: 2007-12-16
    body: "I'm sure it's gonna be as buggy as every new release. The older I get, the less I care about eye candy."
    author: "szlam"
  - subject: "bleh"
    date: 2007-12-19
    body: "Where's the taskbar?? That shit down there is exact oposite of usability... "
    author: "bleh"
  - subject: "Support for old style menu"
    date: 2007-12-21
    body: "It will be great if kde4 will support switching to old style menu.\nIn my opinion new menu is worse then one from kde 3 and it is not so useful."
    author: "kirill"
  - subject: "Re: Support for old style menu"
    date: 2007-12-21
    body: "http://aseigo.blogspot.com/2007/12/rockstar-of-day.html"
    author: "Anon"
---
The KDE Community is happy to announce the immediate availability of <a
href="http://www.kde.org/announcements/announce-4.0-rc2.php">the
second release candidate for KDE 4.0</a>. This release candidate marks the last
mile on the road to KDE 4.0.  This release sees increasing participation from distributions, you can <a href="http://kde.org/info/3.97.php">download packages</a> for Debian, Kubuntu, Mandriva, openSUSE &amp; Fedora or grab the live CDs from Kubuntu &amp; openSUSE.









<!--break-->
<div style="float: right; margin: 1ex; padding: 1ex; border: thin solid grey; float: right">
    <a href="http://www.kde.org/announcements/announce_4.0-rc2/konqueror.jpg">
        <img width="400" height="300" border="0" src="http://www.kde.org/announcements/announce_4.0-rc2/konqueror_thumb.jpg" />
    </a><br />
    <em>KDE's webbrowser Konqueror</em>
</div>

<p>KOffice has also put out a <a href="http://download.kde.org/unstable/koffice-1.9.95.1/src/koffice-1.9.95.1.tar.bz2">sixth alpha release</a>, released separately.</p>

<p>
While progress on the quality and completeness of what is to become the KDE 4.0 desktop
has been great, the KDE Community decided to have another release candidate before releasing
KDE 4.0 on January 11th. The codebase is now feature-complete. Some work is still being done
to put the icing on the KDE 4.0 cake. This includes fixing bugs,
finishing off artwork and smoothing out the user experience.</p>






