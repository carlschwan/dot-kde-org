---
title: "The Road to KDE 4: Konsole Gets an Overhaul"
date:    2007-05-23
authors:
  - "tunrau"
slug:    road-kde-4-konsole-gets-overhaul
comments:
  - subject: "you had me there"
    date: 2007-05-23
    body: "I was so going to write a flame about the placement of the tabs after looking at those screenshots, but I am much happier now that I've read the full article. \n\nNice work again, Troy, thanks! :)"
    author: "KDE User"
  - subject: "Re: you had me there"
    date: 2007-05-23
    body: "I was so happy to see the tabs to be moved by default to a sensible position, but after reading the full article I have to write a flame. Why is KDE keeping this utterly nonsense setting as a default? Almost every other app (including Konq!) have the tabs on top, and users have come to expect the tabs being on top. Having them below the thing they are switching is confusing to a new user of the application."
    author: "other KDE user"
  - subject: "Re: you had me there"
    date: 2007-05-23
    body: "Well, one reason for doing so could be Fitt's Law, which has things to say (amongst other things) about why pixels closer to the screen's edge are easier to hit with the mouse. Since Konsole has no status bar, the bottom edge of the window lacks anything to click, and as such it might as well be used for switching between views. This is just me hypothesising, of course, i have not spoken with anyone about it."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: you had me there"
    date: 2007-05-23
    body: "I do not know where the tabs should be. But on the bottom, it makes sense for terminals. In a terminal, most of the time, you are working right at the bottom, since this is where you type new commands and where the commands outputs arise.\n\nThe tabs are located where your eyes are monitoring. :-)"
    author: "Hobbes"
  - subject: "Re: you had me there"
    date: 2007-05-23
    body: "+1, right on spot.\n\nI use Konsole a lot, and everything that happens is at the bottom of it: the last line you type in the terminal, and switching tabs. I seldom use the menu, and everything which is located at the top of the terminal window is stuff I typed some time ago (ie: my history), which I also seldom want to access.\n\nSo by having the tabs at the bottom, all the action in a terminal is happening in the bottom part of the window, and the rarely used stuff is \"stored\" in the top part of the window.\n\nIn Konqueror, the tabs are in the top part because the stuff you do with it involve: using the menu (more often than in a terminal), typing something in the adress bar, clicking on an icon in your toolbar, or a link in your bookmarks toolbar, which are all elements located at the top of the window. So this is where the tabs should go as well.\n\nWackou."
    author: "Wackou"
  - subject: "Re: you had me there"
    date: 2007-05-24
    body: "Even though my eyes might be on the bottom, my mouse is on top of the window, where it mostly is. I am used to go to the top to scroll tabs, like in Konqueror, so I prefer it the same in Konsole. But hey, it's configurable, I don't really care for the default in this case as I think neither one is really worse."
    author: "superstoned"
  - subject: "Re: you had me there"
    date: 2007-05-24
    body: "you use a mouse for changing tabs?\nI just set konsole to use ctrl-, and ctrl-. like all my other tabbed programs."
    author: "Chani"
  - subject: "Re: you had me there"
    date: 2007-05-28
    body: "btw, konsole has default shortcuts shift+cursor keys to cycle through tabs (and shift+ctrl+cursor keys to move them).\nhandy."
    author: "richlv"
  - subject: "Re: you had me there"
    date: 2007-05-23
    body: "I totally agree with you on this one! It absolutely makes sence to have it at the bottom(for me at least), your eyes will be there when you're working and so it's easier to switch them."
    author: "alltime konsole user"
  - subject: "Re: you had me there"
    date: 2007-05-24
    body: "Good reason.  Aesthetically I like them on the bottom to balance out the menu.  Having both at the top leaves the window looking top heavy, with the bottom looking somehow bare.  Of course, all the action happening at the bottom is a better reason ;)"
    author: "MamiyaOtaru"
  - subject: "Re: you had me there"
    date: 2007-05-25
    body: "Note that by this logic, tabs should be also at the bottom for web browsers: usually you change tab when you're finished reading a webpage..\n\nBah, it doesn't really matter as it's easy to change.."
    author: "renoX"
  - subject: "Re: you had me there"
    date: 2007-05-23
    body: "> Having them below the thing they are switching is confusing \n> to a new user of the application.\n\nMost of the time when working in the terminal, your eyes are at the bottom of the screen reading the most recent output.  It therefore makes sense to place the tabs there as well so that it isn't necessary to shift your gaze up and down the screen continually.  The same logic applies to Kopete where new messages appear at the bottom of the window.\n\nA web page however is different since the most interesting content, which means new articles, news, product information etc. appears at the top of the screen.  Power users will have their bookmarks there as well.  In that case it makes sense to put the tabs at the top, as Konqueror/Firefox/Internet Explorer do.\n\nI can understand why it might seem conceptually better to have every application's tabs work the same way, but it doesn't take into account differences in how users work with those applications.\n"
    author: "Robert Knight"
  - subject: "Re: you had me there"
    date: 2007-05-23
    body: "Agreed, coherency is *good*."
    author: "renoX"
  - subject: "Re: you had me there"
    date: 2007-05-24
    body: "It depends\nI think Konsole is a thing where you have your eyes usually on the last lines of the output and the very last line very you enter your command (Unlike a Web-Browser, etc..). So basically having the tabs near that area makes sense to me and i like it that way.\n"
    author: "Worf"
  - subject: "Re: you had me there"
    date: 2007-05-25
    body: "Totally agree."
    author: "devicerandom"
  - subject: "Re: you had me there"
    date: 2008-10-15
    body: "Man, all this talk about *where* the tabs are is good for one thing...\n\nproving that the ability to change settings is a good thing. I use the keys to navigate through tabs as well but if I don't like where they are located, I just *change* their position. I mean the author *did* spend a lot of time asking us where to put them *and* how the settings and configuration screens should work!\n\nI say \"STOP CRYING AND USE THE TOOLS KONSOLE GIVES YOU... PUT THEM WHERE YOU LIKE\"!"
    author: "DDreggors"
  - subject: "Great Work"
    date: 2007-05-23
    body: "It looks just awesome! :)\n\nReally looking forward for the first Beta release.\n\nBye."
    author: "Luciano"
  - subject: "Screenshot"
    date: 2007-05-23
    body: "Once again, great job Troy!\nCould you please fix the first and third screenshot? The first one says \"The requested URL /dannya/vol14_1x_konsole.png was not found on this server.\" when clicked, and the third one links to the second one (vol14_356_konsole.png).\n\nI did the survey even if I'm not an active Konsole user - I prefer more 'lightweighted' terminals (I do use Yakuake for irssi though). But it's always fun when you can make your voice heard and help supporting KDE.\n\nI have to say that the improvements look very nice, specially the new interface. This was not mentioned (I think) in the article, but true transparency is something I look forward to see (Yakuake + true transparency = love)"
    author: "Lans"
  - subject: "Re: Screenshot"
    date: 2007-05-23
    body: "True transparency for 4.0 has been coded, however it is currently disabled by default as it breaks on machines that do not have proper composite support.  This seems to be an issue encountered by quite a number of programs, with each program having to independently implement tests for composite support.  Perhaps something should get factored into a library so that this problem goes away.\n\nAbout the broken links - Danny will probably have to fix them as I do not have editor access to dot content.  I'm just submitting my articles using the contribute link on the side."
    author: "Troy Unrau"
  - subject: "Re: Screenshot"
    date: 2007-05-23
    body: "Found the reason for one of the broken links: it needs a .jpg extension instead of dot .png.  Still not certain of the other one."
    author: "Troy Unrau"
  - subject: "Re: Screenshot"
    date: 2007-05-24
    body: "> with each program having to independently implement tests \n> for composite support\n\nbool KWindowSystem::compositingActive();\n\nso that excuse is gone =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Screenshot"
    date: 2007-05-24
    body: "Could you communicate this directly to Troy in case he doesn't read these comments again? [/irony for using the comments to make this request]"
    author: "MamiyaOtaru"
  - subject: "Re: Screenshot"
    date: 2007-05-24
    body: "Fortunately I do read these comments, and if Robert hasn't already made the change by the time I'm home from the uni, then I'll do it myself :)\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Screenshot"
    date: 2007-05-23
    body: "KDE 3 + compiz = Yakuake + true transparency ;).\nYou can have it right here right now! (I also love the effect, especially cool with a video window in the background :)) Would love to see the compiz coolness reimplemented in a compositing version of KWin :)."
    author: "zonk"
  - subject: "Re: Screenshot"
    date: 2007-05-23
    body: "You have your wish.  This is already implemented in KWin for KDE 4.  I have an article in progress on this topic."
    author: "Troy Unrau"
  - subject: "One mistake"
    date: 2007-05-23
    body: "Mistake: in the title blurb, it should read KDE 1.0, not 2.0.  I have it correct in the body.  This should probably be corrected before other sites start to pick up the header.  My fault, sorry."
    author: "Troy Unrau"
  - subject: "Links"
    date: 2007-05-23
    body: "Please, could you check and fix the links (on the images). Some are not working (404) and some leads to the wrong pictures :( ."
    author: "Plop"
  - subject: "Now I have reason to wait for KDE4 :)"
    date: 2007-05-23
    body: "Well, at last something interesting for real\nusers in KDE4 instead of useless eyecandy :P\n\nThanks Robert :)"
    author: "m."
  - subject: "Re: Now I have reason to wait for KDE4 :)"
    date: 2007-05-23
    body: "So... Solid, Phonon, Akonadi and Nepomuk are now classed as eyecandy, and even the useless sort... *giggles* i must say i find this an intriguing concept ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Now I have reason to wait for KDE4 :)"
    date: 2007-05-23
    body: "And just to clarify the importance of these to your \"real users\": All of these make it considerably simpler and more consistent to handle hardware, multimedia, PIM and data respectively. So, it is all stuff that works behind the scenes, but that does not mean it's not good for \"real users\"."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Now I have reason to wait for KDE4 :)"
    date: 2007-05-23
    body: "I am waiting to see some USEFUL eyecandy being implemented, for the benefit of real users :P. Per-window settable transparency, compizey Alt+Tab switcher, and MacOS-Expose-alike feature - those are things I would like to see in compositing KWin!"
    author: "zonk"
  - subject: "Re: Now I have reason to wait for KDE4 :)"
    date: 2007-05-23
    body: "Your wish is, uhm... well, not my command... But it seems Lubos' ;) http://www.kdedevelopers.org/node/2787"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Per-window configurable transparency"
    date: 2007-05-24
    body: "... is already available in KDE 3.5.6, and has been for several microversions at least.  Admittedly, the UI could be made rather more intuitive, but it works, at least for those using a good composite-active xorg and xorg graphics driver.\n\n1.  Make sure your xorg graphics driver has composite support and that you have composite turned on in your xorg.conf.\n\n2.  In the kcontrol Window behavior applet, on the Translucency tab, activate the Use translucency and shadows checkbox and OK thru the warning if you get one.  On the Opacity sub-tab, set your system-wide translucency default levels for the various window types (Active, Inactive, Moving, Dock, and the \"keep above\" as active checkbox).  OK out and restart KDE if necessary (from my experience, it appears if it starts with translucency enabled you can turn it on and off as desired, but if it starts off, you have to restart KDE to turn it on).\n\nThat activates it in general.  To change specific types of windows (window classes) from the defaults:\n\n3.  In the Window specific settings applet (reachable from the control panel, the setting menu, or the the window menu for a specific window), create a new config for your specific window type as necessary.  Then on the Preferences tab, set the Active and Inactive opacity as desired.  (There's no window class default for moving windows, that's a system-wide default only, AFAIK.)\n\nThat sets the general defaults for specific classes/categories of windows, and/or specific apps, depending on how strictly you set the matching for the specific window settings.  For individual windows:\n\n4.  In the window menu for each window, there's an Opacity option.  If you want a specific window set to something other than the system or window class defaults, set this.  Since this is for a specific window, unlike the general system-wide and window class defaults, it applies only as long as the window exists. It also appears to apply to the /active/ state only, since the window is by definition activated (at least with the settings I have here) when you activate its menu, so it's the active window settings that apply and are adjusted.  Unfortunately, I know of no way to set inactive window transparency for a specific window, only at the system and window class levels.  Still, the window class settings are likely to suffice in most cases.\n\nBTW, there are still occasional bugs where KWin apparently gets mixed up and forgets to set a specific window to its usual state.  I most often see this when a window says at \"inactive\" transparency even after it has been activated.  Opening the window menu and clicking on the button under Opacity returns it to the usual defaults, thus fixing the transparency issue for that window temporarily.  I don't have to do this often, but it does happen occasionally.\n\nSo yes, there's definitely per-window transparency available in current KDE, with active/moving/inactive defaults configurable at the system level, active/inactive defaults for specific window classes configurable as exceptions, and individual active window settings configurable as exceptions to the class or system defaults.\n\nFWIW, I'm using an ATI Radeon 9200, as it's one of the last cards ATI actually cooperated with the X community on, releasing specs and even sponsoring some open development.  Naturally, I'm running the native freedomware xorg driver, with exa rendering (MUCH more CPU efficient than xaa), and yes, it supports composite quite well. =8^)\n\nIt's getting a bit dated now, but I've a screenshot of my dual monitor desktop, taken back when I was running KDE 3.5.1 and xorg 7.0, with composite and transparency enabled.  (You can see in the screenshot how much CPU it was chewing, far left sysguard graph, 100%.  Things are far better now, with it generally using ~10% of a single CPU, same Opteron 242s.)\n\nhttp://members.cox.net/pu61ic.1inux.dunc4n/pix/screenshots/index.html"
    author: "Duncan"
  - subject: "Re: Per-window configurable transparency"
    date: 2007-05-26
    body: "Yeah, I know about all this. The problem is, I want to simply change the transparency at will, without going thorugh 10 dialogs... In compiz (default config) you simply move the cursor over the window, and use Alt-Mousewheel..."
    author: "zonk"
  - subject: "Re: Per-window configurable transparency"
    date: 2007-05-26
    body: "In Kwin for KDE 4, if you right click on the titlebar, there is an opacity option in the menu that lets you quickly select some of the more common values very rapidly.  Alt+Mousewheel makes sense though, since the window manager already captures most ALT+MouseKey type actions for resize, move, etc...  "
    author: "Troy Unrau"
  - subject: "Re: Now I have reason to wait for KDE4 :)"
    date: 2007-05-24
    body: "> compizey Alt+Tab switcher, and MacOS-Expose-alike feature\n\nThis isn't useful, but eyecandy."
    author: "Carlo"
  - subject: "Re: Now I have reason to wait for KDE4 :)"
    date: 2007-05-24
    body: "I think Expos\u00e9 is one of those eyecandy things that can also be classified as rather useful ;)  "
    author: "MamiyaOtaru"
  - subject: "Re: Now I have reason to wait for KDE4 :)"
    date: 2007-05-26
    body: "You obviously didn't use them, otherwise you would know how wrong you are :). Both make finding the right window you want to switch to MUCH faster. Suppose you have four Konsoles open, one of them make'ing some app. Suppose you want to Alt-Tab switch to it. What is faster, finding the item with the right text on the list, or simply choosing that item which has constantly scrolling output in the live thumbnail? :)"
    author: "zonk"
  - subject: "How about transparency"
    date: 2007-05-23
    body: "I know it's in there now, but it's \"fake\" transparency. It just shows the background, no matter what. It also repaints a bit slow when moving the konsole window. "
    author: "cossidhon"
  - subject: "Re: How about transparency"
    date: 2007-05-23
    body: "See\n\nhttp://dot.kde.org/1179921215/1179939880/1179940099/"
    author: "Anon"
  - subject: "Re: How about transparency"
    date: 2007-05-23
    body: "Real transparency is available in Konsole for KDE 4.0, it will require a desktop which supports transparency ( eg. Compiz or KDE 4's KWin ).  Currently disabled until I fix some problems on desktops which don't support it.\n\nKDE 3's fake transparency and image backgrounds are not included.\n\nAdditionally proper transparency is available in KDE 3.5.7 via a command-line argument ( --real-tranparency ) when starting Konsole, though that again causes glitches if enabled on desktops that don't support transparency."
    author: "Robert Knight"
  - subject: "Re: How about transparency"
    date: 2007-05-23
    body: "> KDE 3's fake transparency and image backgrounds are not included.\n\nDoes this mean I can't use my favorite paper background ?\n"
    author: "Anonymous"
  - subject: "Making tea and kio-slaves.."
    date: 2007-05-23
    body: "I wonder if it would be possible to implement (an optional) support for commandline use of kioslaves before implementing the tea feature? \n\nIn the sense that there would be on option: \"Turn on transparent kio-slave interpretation\" (or something) and then on the command line one could type something like \"cat http://www.kde.org/\" and konsole would use http-kioslave to retrieve the file and then feed it to bash (or whatever) as cat's parameter...\n\nOh well, down to submit a bug report :-)"
    author: "outolumo"
  - subject: "Re: Making tea and kio-slaves.."
    date: 2007-05-23
    body: "kioexec cat http://www.kde.org\n\nIt'll even offer to upload back to the server if you modify the file. (works with fish, sftp, ftp, webdav, etc.)"
    author: "Thiago Macieira"
  - subject: "Re: Making tea and kio-slaves.."
    date: 2007-05-23
    body: "How about integration by konsole sniffing for addresses from shell prompt and redirecting them transparenntly to kioexec? \n\nAfter all, konsole is the layer between user and the shell... ;-)"
    author: "outolumo"
  - subject: "Re: Making tea and kio-slaves.."
    date: 2007-05-23
    body: "how should this handle \"wget http:\\\\dot.kde.org\" ?"
    author: "AC"
  - subject: "This can already be done with fuse_kio"
    date: 2007-05-24
    body: "http://kde.ground.cz/tiki-index.php?page=KIO+Fuse+Gateway\n\nHowever, why is this still in kdenonbeta? Why isn't it in, for example, extragear?"
    author: "A.M."
  - subject: "Re: This can already be done with fuse_kio"
    date: 2007-05-25
    body: "Hmm, doesn't work properly:\n\nfuse_kio /home/user/test fish://user@fishsomewhere\nfuse_kio: fuse_opt.c:67: fuse_opt_insert_arg: Assertion `pos <= args->argc' failed."
    author: "whatever noticed"
  - subject: "Popup menu"
    date: 2007-05-23
    body: "Will everything still be configurable in the popup menu when the menu bar is hidden? I find it useful.\n\nI don't think it is a good idea to put Encoding setting in some configuration dialog. I use it frequently and it is also in the menu in Kate and katepart based applications."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Popup menu"
    date: 2007-05-23
    body: "It's still there, under the View menu I believe.  It's been reorganized to be sane as well.  :)"
    author: "Troy Unrau"
  - subject: "Re: Popup menu"
    date: 2007-05-23
    body: "That's really nice to hear - thanks for your reply!"
    author: "stormwind"
  - subject: "Re: Popup menu"
    date: 2007-05-23
    body: "I agree. The only option of konsole i use at all is changing the encoding - at my work i need it almost all the time - it would be awkward if it's hidden in a menu.\n\nstormwind"
    author: "stormwind"
  - subject: "enhancements"
    date: 2007-05-23
    body: "great to hear the konsole gets enhancements for managing the configuration, it's necessary - but let's get back to earth - how often does the normal user change the configuration of konsole?\n\nIMHO a first time run wizard should/could handle the most wanted features\n* location of tabs top/bottom\n* color scheme\n* keyboard layout\n* font \n* encoding\n"
    author: "ferdinand"
  - subject: "Re: enhancements"
    date: 2007-05-23
    body: "I disagree with wizards as a rule.  If a person has not used a program before, they have no experience to draw upon to make reasonable choices in the wizard, and can end up with some totally bizarre configuration.  I'd rather see a sane default (which is not difficult to do for a terminal program), and a logical way to make tweaks, should the user require some changes.  This way, they at least know that they have a preference, before asked to set their preferences.  Or am I totally off my rocker on this one..."
    author: "Troy Unrau"
  - subject: "Re: enhancements"
    date: 2007-05-23
    body: "Dunno how it is \"usability wise\" since I'm just a normal user, but what you said sounds very reasonably.\n\nI too love to make tweaks. However there are applications which I just use the default settings. Photoshop (yes, I use it instead of Gimp etc). I think a really good default setting should be the goal of all KDE apps, even if you may tweak it if you want.\n\nTo take it further, I think it applies to toolbars and such too. So the toolbar should IMHO be \"locked\" by default."
    author: "Lans"
  - subject: "Re: enhancements"
    date: 2007-05-24
    body: "I *still* don't know what my preferences are for konsole :) every few months I go and tweak a setting or two to get it a bit closer to what I feel like I want at the time..."
    author: "Chani"
  - subject: "Re: enhancements"
    date: 2007-05-23
    body: "First-time wizards suck, because normally on public computers (at my uni, for example) a new user profile is created each time someone logs in, so you would have to put up with the wizard every time you started the app."
    author: "AC"
  - subject: "Re: enhancements"
    date: 2007-05-24
    body: "A normal user doesn't use konsole :-)"
    author: "Waldo Bastian"
  - subject: "Re: enhancements"
    date: 2007-05-24
    body: "The only reason you need a first run wizard in an app is for apps that can't run without configuration - Kopete, KPPP, KMail are examples of this.  Konsole, conversely, is perfectly runnable without any configuration."
    author: "Will Stephenson"
  - subject: "New Tabs"
    date: 2007-05-23
    body: "In Konsole, does anyone know how to open a new tab (as with ctrl+alt+n) that would bring you to the same directory as the current tab? I have never found the way to do that.\n\nAs far as I remember, it is even the default behavior in gnome-terminal.\n\nIf it is not possible, I would suggest to put an option in the session(s) configuration (well, profile definition, if I got the changes right), and to enable distinct shortcuts for the sessions/profiles. The latter would be a very interesting feature by itself. :-)\n\nAnyway, thanks for your work. Konsole is extensively used by many of us!"
    author: "Hobbes"
  - subject: "Re: New Tabs"
    date: 2007-05-23
    body: "> I have never found the way to do that.\n\nAs far as I know, there isn't a simple and easy way to do that.  Konsole/KDE 4 doesn't provide that option yet either, but I do plan to add it at some point.\n\n> and to enable distinct shortcuts for the sessions/profiles.\n\nThe facility to define custom shortcuts for each profile and use them to create new tabs is already implemented."
    author: "Robert Knight"
  - subject: "Re: New Tabs"
    date: 2007-05-24
    body: "Couldn't you use a custom KHotKeys shell command with a few dcop calls ?"
    author: "Khotkeys ?"
  - subject: "Re: New Tabs"
    date: 2007-05-26
    body: "That would be a really kludgy way to implement such a useful feature ;P. And horribly slow, also. I have already tried to make a KHotKeys command to open current folder from active Konq window in Yakuake, as a new tab. It worked, but took quite a time after the keypress ;P. Of course, you couldn't change the active window during that time xPPPPPPPPPP."
    author: "zonk"
  - subject: "Re: New Tabs"
    date: 2007-05-25
    body: "Bookmarks. Session->New Shell at Bookmark ->\n\nDerek"
    author: "D Kite"
  - subject: "Re: New Tabs"
    date: 2007-08-02
    body: "This will be a very useful feature. Since konsole lacks this feature , I am using gnome-terminal even though I did not like to use it. This is a feature for which everyone is looking after."
    author: "Gvs"
  - subject: "New search bar"
    date: 2007-05-23
    body: "It would be nice to cover also the new search bar in kosnole:\nhttp://kdemonkey.blogspot.com/2007/03/konsole-progress-searching-history.html\n\n:)"
    author: "liquidat"
  - subject: "Configure different colors for stdin and stdout?"
    date: 2007-05-23
    body: "My #1 wish for Konsole (and Yakuake) is the ability to make standard input a different color from standard output. When I scroll upwards, it is hard to find where I issued a given command, because it is lost among all the output. For example, \n\nuser@host:~$ sudo foo\n...zillion lines of ouput...\nuser@host:~$ grep bar\n...zillion lines of very similar output...\n\nCurrently, all the text is the same color. If the \"user@host:~$ foobar\" lines were a different color, they would be much easier to find."
    author: "kwilliam"
  - subject: "Re: Configure different colors for stdin and stdout?"
    date: 2007-05-23
    body: "> make standard input a different color from standard output.\n\nEverything Konsole prints on screen is from the standard output.  It just so happens that often the terminal echos the characters you enter via the standard input straight back to the output.\n\nChanging the colour of the prompt is what you are after, and to do that you need to change the contents of the shell variable which produce the prompt.\n\nA Google search for \"bash change prompt color\" (I take it that bash is the shell you are using) should turn up some helpful results.  "
    author: "Robert Knight"
  - subject: "Re: Configure different colors for stdin and stdout?"
    date: 2007-05-24
    body: "Sweet! Thanks. That will make life so much better. (I'll have to learn more about bash I guess.) Hopefully I can make that change automatic."
    author: "kwilliam"
  - subject: "Re: Configure different colors for stdin and stdout?"
    date: 2007-05-24
    body: "Cool. My new color scheme in ~/.bashrc.\nPS1='\\e[0;35m[\\e[m\\e[0;32m\\u@\\h\\e[m\\e[0;35m:\\e[m\\e[0;32m\\W\\e[m\\e[0;35m]\\e[m '\n\nI must say though, that is about as crazy and convoluted a syntax I've ever seen. (sigh) Definately not newbie friendly. ;-)"
    author: "kwilliam"
  - subject: "I change my mind.  Please add this feature!"
    date: 2007-05-24
    body: "So... don't trust everything you read on the internet. The code I mentioned doesn't work well, and caused Bash to do crazy things. I looked on google _again_.  The first website had used \"\\e[0;xxm\" syntax. Everyone else appears to be using \"\\[\\033[1;xxm\\]\" syntax. I must say, this seemingly simple task has proved is incredibly difficult. I consider myself a moderate Linux geek, but this syntax makes editing xorg.conf look like a piece of cake. I finally ended up with:\nGREEN='\\[\\033[1;32m\\]'\nPURPLE='\\[\\033[1;35m\\]'\nRESET='\\[\\033[00m\\]'\nPS1=\"${PURPLE}[${GREEN}\\u@\\h${PURPLE}:${GREEN}\\W${PURPLE}]${RESET} \"\n\nI still think that Konsole is a logical place to adjust Bash prompt colors, although I understand if you think that is beyond the realm of Konsole. (Perhaps a stand-alone GUI is needed to help users configure .bashrc.) Regardless, I think I finally have this working, so I'm not too worried about it. Thanks again for the tip."
    author: "kwilliam"
  - subject: "Re: I change my mind.  Please add this feature!"
    date: 2007-05-24
    body: "o.0\nmine's been coloured for as long as I remember. dunno whether it came with kubuntu or I copied it from an older comp or what, but I found this line in my bashrc:\nPS1='${debian_chroot:+($debian_chroot)}\\[\\033[01;32m\\]\\u@\\h\\[\\033[00m\\]:\\[\\033[01;34m\\]\\w\\[\\033[00m\\]\\$ '\n\nand I agree that the syntax is frigging scary. "
    author: "Chani"
  - subject: "Re: I change my mind.  Please add this feature!"
    date: 2007-05-24
    body: "oh, and for gentoo, I have a bright red prompt if I go root. very useful reminder to not do risky things at that prompt... \n\nbut in kubuntu I just use sudo :)"
    author: "Chani"
  - subject: "Re: I change my mind.  Please add this feature!"
    date: 2007-05-27
    body: "This discussion was very useful. Thanks for posting this issue and solutions!"
    author: "abcd"
  - subject: "Re: I change my mind.  Please add this feature!"
    date: 2007-05-24
    body: "The syntax is slightly better (and more portable to other terminals) if you use the tput command to generate the escape sequences.  The trick is going through the terminfo(5) man page and determining what to give tput.\n\nBash's prompt syntax (at least for what I do) was one of the things that made me take so long to switch my shell from tcsh to bash.\n\nIn bash, I have:  PS1=\"\\[$(tput smso)\\]\\h \\@\\[$(tput rmso)\\] \\w\\\\$ \"\nIn tcsh, I have (with a couple more minor features):\n  set prompt = \"%S%m %t%s %c03%# %L\"\n\nOf course, I'm just inverting fg/bg colors rather than changing to purple or green.  But it works well for me."
    author: "Rob Funk"
  - subject: "principle"
    date: 2007-05-24
    body: "- console settings have to be editable by console, either by editing config files or console commands\n- you don't need n different settings for open windows and don't change them twice a day.\n\nConclusion: concole configuration can be left to an external graphical config tool, e.g. a KcontrolCenter Module. It is also better to make a program Konsoleprofile which generates konsolethemes and you as a user can only select from 5-12 predefined good themes for the konsole. You shouldn't invent them yourself and if so that is the task of another program, not for everyday settings. powerusers will edit the config files.\n\nThe settings-codes menu is obsolete.\n\nAnd the Konsole handbook is absolutely not helpful for beginners. It includes everything you don't want to know.\n\nRemove KDE 2.0 style usability hells like the marmor background prefab setting "
    author: "Ben"
  - subject: "Re: principle"
    date: 2007-05-24
    body: "Please?"
    author: "superstoned"
  - subject: "Re: principle"
    date: 2007-05-24
    body: ">-console settings have to be editable by console, either by editing config files or console commands\n\nLast I talked to Robert, this is definitely planned.  I quote from email: \"Something which isn't there yet but I plan to add is a command-line\ntool to manage profiles ( saved terminal setups ).  The dialogs are\nuseful when visual previews/feedback are desired, or to browse\navailable options.  For people who know what they are doing though,\nthe command-line is a much faster way to get things done - hence the\nreason why people still use terminals.\"\n\nI didn't add that to the article because of length concerns more than anything else, but there you go :)"
    author: "Troy Unrau"
  - subject: "Re: principle"
    date: 2007-05-24
    body: "dbus?"
    author: "Aaron J. Seigo"
  - subject: "Thank you"
    date: 2007-05-24
    body: "Thank you Troy for this nice report and thank you Robert for this very promising konsole. I use it all day long and i really appreciate your work improving it."
    author: "Med"
  - subject: "Incompatible copy/paste shortcuts"
    date: 2007-05-24
    body: "Please, please, do something with the copy/paste shortcuts in Konsole (standard is CTRL+C/CTRL+V)."
    author: "JS"
  - subject: "Re: Incompatible copy/paste shortcuts"
    date: 2007-05-24
    body: "I'm afraid that would be a very ill-advised move. AFAK CTRL-C is the standard means of killing a running application in a terminal. You'd risk challenging muscle memory of command line users obtained from several decades of use to accomodate comparatively more recent copy/paste shortcut usage.\n\nBesides, most solely GUI based users wouldn't tinker with the command line anyway and it's far better not to alienate people accustomed to using a shell."
    author: "Command line User"
  - subject: "Re: Incompatible copy/paste shortcuts"
    date: 2007-05-24
    body: "Ctrl+C, Ctrl+V are reserved for use by terminal applications, so the closest alternatives ( Ctrl+Shift+C and Ctrl+Shift+V ) are used."
    author: "Robert Knight"
  - subject: "Re: Incompatible copy/paste shortcuts"
    date: 2007-05-28
    body: "i'm glad to hear this, as i am setting ctrl+shift+c as 'copy' whenever i use konsole for longer than a couple of commands ;)\ni still use shift+ins for pasting, though.\n"
    author: "richlv"
  - subject: "Re: Incompatible copy/paste shortcuts"
    date: 2007-05-24
    body: "Ummm... and break Ctrl+C being the kill signal to processes in the shell?\n\nCtrl+C isn't Copy for a very good reason.\n\nAnd Ctrl+V also has special meaning to many console programs."
    author: "StuP"
  - subject: "No"
    date: 2007-05-24
    body: "The standard behaviour of Ctrl+C pretty much anywhere, even on MS Windows, is to send a break signal. I don't know if Ctrl+V has a standard behaviour, but it appears to echo codes for special keys (on bash/rxvt at least).\n\nThe most widely accepted shortcuts for copy/paste seem to be Ctrl+Ins and Shift+Ins - again, even on Windows. A number GUI programs do offer Ctrl+C/V as an alternative to Ctrl/Shift+Ins, but I doubt that you'll ever see that in a mainstream console program."
    author: "Daniel"
  - subject: "yay! :)"
    date: 2007-05-24
    body: "I never understood konsole settings - even the control centre is easy compared to that thing ;)\n\nmy favourite improvement: qt4 handles fonts much better, so I can actually see *all* the chinese characters in konsole and not just a fraction of them. :)"
    author: "Chani"
  - subject: "Re: yay! :)"
    date: 2007-05-24
    body: "Well, I would say that is quite an improvement :-)\n"
    author: "The Vigilant"
  - subject: "Re: yay! :)"
    date: 2007-05-26
    body: "I wish /anything/ from KDE4 alpha worked /at all/ on my computer :P  I know it&#8217;s far from being done...\n\nYeah, the chinese characters issue in Qt3 is really annoying.  I can&#8217;t read Chinese, but I prefer seeing characters instead of little bullets or boxes :)"
    author: "Ralesk"
  - subject: "Tab buttons?"
    date: 2007-05-24
    body: "I use tab close and open buttons. Where they gone?"
    author: "sin"
  - subject: "Re: Tab buttons?"
    date: 2007-05-26
    body: "I also like to know, what about the \"new tab\"-button on the left beside the tabs."
    author: "Volker"
  - subject: "Re: Tab buttons?"
    date: 2007-12-12
    body: "Also, where are the right click menus on the tabs?  I use that to enable \"Send Input To All\" and it feels rather clunky now to have to 1 - Make sure I'm on tab I want to use as the input rather than simply right click on the tab I want, then 2 - Have to open the edit menu to select the \"Send Input...\"\n\nOther than that, it's a very nice cleanup.\n\n--Beemer"
    author: "Beemer"
  - subject: "Don't bash KDE3"
    date: 2007-05-24
    body: "Nice article all in all. But a bit of an emotional rollercoaster, I think the bashing of KDE3 Konsole goes too far. But all's well that ends well I guess.\n\nSure it's very nice that it's gotten some extra polish, but it wasn't that bad, neither is the rest of KDE3. It seems that a lot of people have bought into the GNOME \"FUD\" about KDE usability.\n\nDon't believe it, as Will Stephenson writes here:\nhttp://www.kdedevelopers.org/node/2816"
    author: "cb400f"
  - subject: "Re: Don't bash KDE3"
    date: 2007-05-24
    body: "Emotional roller coaster! What do you mean! *stomps feet* (just kidding man).\n\nI see your point, but I will suggest that it wasn't my intention to bash previous KDE versions which I have used and enjoyed for almost a decade now.  What I did intend was to show progress, and without showing screenshots that compare the two, that is quite difficult for an application such as konsole, since well... the main functionality of the application has been there since KDE 1.0.  That said, I still do believe that it had one of the worst configuration dialogs in all of KDE... but I don't believe in the gnome way of dealing with the problem.  In that scenario, options would have been removed, but we have elected to reorganize them rather than remove them, preserving the power and flexibility of the application, without losing users in a poorly organized dialog.  I hope you understand the subtle difference between the two philosophies.  I feel it is really important not to hide or remove options, especially in a power user application like Konsole, and I hope you did not get that impression from the article.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: Don't bash KDE3"
    date: 2007-05-29
    body: "\"In that scenario, options would have been removed, but we have elected to reorganize them rather than remove them, preserving the power and flexibility of the application, without losing users in a poorly organized dialog. I hope you understand the subtle difference between the two philosophies. I feel it is really important not to hide or remove options...\"\n\nIt is not a subtle difference, it is a major and crucial one. It is what makes KDE the best DE ever. And I hope the rest of KDE4 team shares your vision."
    author: "aha"
  - subject: "tab titles"
    date: 2007-05-24
    body: "Do the tab improvements include optionally getting (part of) the tab title from the xterm window title that can be set from the shell?\n\nI have this in my .bashrc:\n    case $TERM in\n    xterm*)\n        PROMPT_COMMAND='echo -ne \"\\033]0;${USER}@${HOSTNAME%%.*}\\007\"'\n        ;;\n    esac\nSo before every prompt, bash sets my window title to username@hostname.  Sometimes this feature is used to show the current directory.  It's always bugged my that this information doesn't appear on the tab title.\n"
    author: "Rob Funk"
  - subject: "Re: tab titles"
    date: 2007-05-24
    body: "In KDE 3 you can set the tab titles to match the window title via the \"Set tab title to match window title\" option in the General page of the settings dialog.\n\nIn KDE 4, you can include the window title set by the shell in addition to other text or dynamic elements.\nThe default tab title formats are \"directory : program\" for shells running 'local' commands and \"host (user)\" when you run ssh to connect to another computer ( the host/user information in that case is taken from the command-line arguments to ssh, so it may or may not be helpful depending on the way you work )"
    author: "Robert Knight"
  - subject: "Re: tab titles"
    date: 2007-05-29
    body: "I was wondering if it is also possible to assign very short names to the tabs to be able to have as many opened as the width of the screen allows. I am compiling lots of software in virtual machines and across the network so I need as many tabs opened as possible and the names need to be meaningful but short.\n\nIs there or will there be any provision to set the names on the tabs to something like \"Sxx\" instead of \"Shell no. xx\"? Otherwise I'm fond of Konsole and have been using it for years and I'm looking forward to more nice things to come in KDE 4."
    author: "Sunil Janki"
  - subject: "Konsole a replacement for VT200 terminal emulator "
    date: 2007-05-24
    body: "Just wondering if one could use Konsole for VT200-sessions instead of e.g. Reflections.\nWould mean a lot to our production environment.... "
    author: "Frits"
  - subject: "Border in fullscreen"
    date: 2007-05-24
    body: "Thanks for the improved Konsole! :)\n\nAlso, could you remove (or make optional) the extra border in KDE 3.5.5 that makes a true full-screen console impossible?\n\nthanks"
    author: "Helmudt"
  - subject: "Re: Border in fullscreen"
    date: 2007-05-25
    body: "As far as I know this is a \"bug\" in QTabBar and is fixed in Qt4 series. Perhaps someone with more knowledge about the issue can correct me if I'm dead wrong in this... :)"
    author: "tpr"
  - subject: "Re: Border in fullscreen"
    date: 2007-05-25
    body: "Close.  It is a 'bug' in QTabWidget, which (still) does not provide a means to turn the frame on or off.  \n\nKonsole currently uses a replacement tab widget I wrote using a QTabBar (that is just the widget which draws the tabs and handles interaction with them) and a widget stack.  The main motivation was to get rid of the border, but it also has other benefits.  Adding new tabs is smoother and slightly quicker.  Tabs are also allowed more of the window width by default which provides more room if you have long tab titles.\n\n"
    author: "Robert Knight"
  - subject: "Re: Border in fullscreen"
    date: 2007-05-25
    body: "Can't you send this as a patch to the trolls or something, to get it in 4.3!!"
    author: "superstoned"
  - subject: "Re: Border in fullscreen"
    date: 2007-05-25
    body: "As far as I know, it's a little late to get things included in 4.3, except bugfixes."
    author: "Troy Unrau"
  - subject: "I Really Like the Existing Konsole.."
    date: 2007-05-24
    body: "Frankly, I find the settings menu and dialog to be very intuitive.  Although I use it every day, I was able to find what I am looking for the first time I ever tried.  It was easier than in kwrite, kword, kspread--and god forbid konversation that doesn't even have a copy/paste option.  But I will wait and see, before I judge the new setup.\n\nAlso, I really like the tabs along the bottom of the window and would prefer that over having them up top, near the menu.  I might accidentally click on a menu.\n\nThe split-screen option could be VERY useful.  I am happy to see that.\n\nAnother feature that could be very useful, is to have a toggle button handy to switch between modes of capturing or not capturing CTL-Z, X, C, and V.  Because it really sucks having to use the edit menu.. and the middle-mouse button doesn't always give what I copied.. It gives whatever I last selected--which is sometimes different.\n\nMatthew\n"
    author: "Matthew C. Tedder"
  - subject: "Re: I Really Like the Existing Konsole.."
    date: 2007-05-24
    body: "Yakuake is nice but I never managed to set it up so the window pops up from below the screen."
    author: "semsem"
  - subject: "Re: I Really Like the Existing Konsole.."
    date: 2007-05-25
    body: ">> Another feature that could be very useful, is to have a toggle button handy to switch between modes of capturing or not capturing CTL-Z, X, C, and V. Because it really sucks having to use the edit menu.. and the middle-mouse button doesn't always give what I copied.. It gives whatever I last selected--which is sometimes different.\n\nHuh? Just use an alternative shortcut for paste etc (ctrl+alt+v for example). About middle mouse - try Klipper."
    author: "Lans"
  - subject: "Re: I Really Like the Existing Konsole.."
    date: 2007-05-26
    body: "^Z &#8212; what (and how) would you Undo in a terminal? o_O\n\n^X/^C/^V &#8212; how about Ctrl-Del, Ctrl-Ins, Shift-Ins?"
    author: "Ralesk"
  - subject: "please"
    date: 2007-05-24
    body: "Please allow the konsole look to be as it currently is in kde 3.5, with tab bar at bottom, and the button to create new tab. I really dont like the look put forth with tabs at top, though i think that can be configured, since it is KDE after all :D\n\ni love the split functionality though."
    author: "redeeman"
  - subject: "Re: please"
    date: 2007-05-24
    body: "/me points up and says \"Read the article again.\" :)"
    author: "Troy Unrau"
  - subject: "Re: please"
    date: 2007-05-24
    body: "So the tabs are at the bottom (yay), but what about the new tab/close tab buttons?  I tend to doubleclick the tab bar to make a new tab, but I wonder if this will be more difficult with the newly really wide tabs (filling up the tab bar faster).  Even with narrower tabs, when it gets full I'd rather click a new tab button on the tab bar than session->new shell.  And close tab is simply nice to have in place of or in addition to session->close session.\n\nI'd say make it an option, but I see no reason not to have them.  my 2\u00a2"
    author: "MamiyaOtaru"
  - subject: "xterm like option add"
    date: 2007-05-24
    body: "Hi.\nI discovered xterm at scool because we use a cad suit whitch has been designed to work with xterm's third button action.\n\nThe thing is that when you press the right button, it will past ... the_selected_text to the prompt, and an alias ...='/path/to/scipt.sh' make it quite nice.\nfor example : enter ls -l\nit print every file and directory. then double click one dir name, and press right button. The script detects that $1 is a directory, and cd to it, the call ls -l  so you can go throught dirs the way you would in konqueror ! (ok, you can't get preview of files there :)\nBut even further : if the directory is one from the cad (ie it contains files named as suposed to be ) it will open in the designer software.\ndo it whith a postscrip file, it open it in ghostview   etc...\n\nSo my whish for konsole in that regard would be an inteligent past. I mean, when clicking the middle moose button, AND IF THE PROMPT IS EMPTY, launch a user defined script. Comming whith a good one may turn konsole use much better.\n\nKind regards.\nKollum"
    author: "kollum"
  - subject: "worst menu..."
    date: 2007-05-24
    body: "> Possibly the worst settings menu of any KDE application in KDE 3.5.6 follows:\n\nMade me think about what's the worst menu in KDE. I think it's Kate's \"Extras\" menu. Second is its \"Document\" menu (when having so much files open that it is multiple columns it's really annonying) on my list. Other candidates!?"
    author: "Carlo"
  - subject: "No clickable links?"
    date: 2007-05-24
    body: "Yeah, Konsole has an awful settings setup, but messing with it is a waste when there are more important issues to take care of. You only change Konsole's setting once, when its lack of clickable links matters nearly every hour of every day.  GNOME-Terminal has it, XFCE's Terminal has it, Mac OS X's Terminal.app has it (CMD+double click)... but Konsole lacks it.  You may as well be using xterm.\n\nIt's been discussed before, where the Konsole people want to make it more general than just URLs.  But what does that entail?  Wasted time, that's what."
    author: "mX"
  - subject: "Re: No clickable links?"
    date: 2007-05-25
    body: "i never encounter links in konsole output :)\n\nBut you can double click on it, go to konqueror en middle click somewhere in konqueror or on the black-white cross of the location bar to open the link"
    author: "whatever noticed"
  - subject: "Re: No clickable links?"
    date: 2007-05-25
    body: "Some people may encounter links if they're using Konsole for IRCing perhaps via Irssi or some other cli IRC client. I've got used to double click the link and the middle mouse button paste in konqi window, though some new comers may not see this as a pro for Konsole when comparing Konsole to Gnome-Terminal for example... Me neither would mind if Konsole would support clickable links :)"
    author: "tpr"
  - subject: "Re: No clickable links?"
    date: 2007-05-25
    body: "well, you can get far with Klipper, which can have automatic actions and stuff like that. you'd have to doubleclick an url, and klipper will automatically launch Konqueror...\n\nI generally doubleclick and middleclick on a tab or the empty space in konqi to open a link. having a clickable link doesn't make a huge difference if you don't use it often. But yeah, I can imagine if you use it often it will make a diff. So be happy, Konsole 4.0 DOES support it."
    author: "superstoned"
  - subject: "Re: No clickable links?"
    date: 2007-05-25
    body: "Thanks for the info, tested it on my checkout and it indeed works quite fine. I'm used to doubleclick the URL and then using middle mouse button in clean tab after that."
    author: "tpr"
  - subject: "Re: No clickable links?"
    date: 2007-05-25
    body: "> when its lack of clickable links matters \n> nearly every hour of every day\n\nI think your comment is hyperbole, but to answer the question, links and email addresses are now clickable.\n\n> where the Konsole people want to make it \n> more general than just URLs\n\nI don't understand what you mean by \"the Konsole people\".  My guess is that you saw a reply to a bug report or perhaps a comment on the dot where one person stated that this was their opinion on the matter.  Please do not misrepresent this as a consensus between everyone who is involved with development of the terminal."
    author: "Robert Knight"
  - subject: "Re: No clickable links?"
    date: 2007-05-25
    body: "my frustration with links comes from the failure of double-click to magically select the entire link. any nontrivial url tends to get only partially selected, and then I have to go click-and-drag with my silly little laptop mouse...\nI'd be very happy if double-click selection could be made smarter by default in kde4.\n\nI also have a feeling that there's a way for me to improve this right now..."
    author: "Chani"
  - subject: "Re: No clickable links?"
    date: 2007-05-28
    body: "settings->configure konsole.\nsee the last option.\ni haven't tested it much myself, but i think i am adding \"?\" to that line ;)"
    author: "richlv"
  - subject: "Re: Yes, we really need clickable links!"
    date: 2007-10-25
    body: "\"lack of clickable links matters nearly every hour of every day.\"\n\nI totally hear you! This would be such an obvious enhancement of Konsole, and i can't find a solution anywhere on google (except loads of people suggesting this feature.)\n\nIf anyway comes up with a solution / patch / plugin / whatever solution that will make URLs clickable, i'm buying beer."
    author: "JoaCHIP"
  - subject: "set mouse=a"
    date: 2007-05-25
    body: "konsole doesn't work fine with vim when the option set mouse=a is on, when you select text it doesn't give the right feedback.. (compare it to xterm) it was something related to mouse protocol 2 or something like that..  and considering there is no vim client for kde.. this kind of sucks, can this feature be implemented"
    author: "hacosta"
  - subject: "Re: set mouse=a"
    date: 2007-05-25
    body: "Yes, I implemented this some time ago.  Konsole doesn't quite provide all the mouse facilities that xterm has, but I have added the most important one which fixes behaviour with \"set mouse=a\" in Vim.  The main benefit is that you can now drag the splitter bar up and down with the mouse as in other terminals."
    author: "Robert Knight"
  - subject: "Re: set mouse=a"
    date: 2007-05-25
    body: "so this will be in kde4.. great! thanks"
    author: "hacosta"
  - subject: "Re: set mouse=a"
    date: 2007-05-25
    body: "oooh, cool :)"
    author: "Chani"
  - subject: "Re: set mouse=a"
    date: 2007-05-25
    body: "I hope I didn't forget to mention in the article that you are amazing..."
    author: "Troy Unrau"
  - subject: "Re: set mouse=a"
    date: 2007-05-26
    body: "Use gvim. Really, Vim is not an application where you use GUI to do stuff (as you should know already :)). It doesn't matter what toolkit does it use, if the first thing you usually do with your .gvimrc is putting a command to turn the menu and toolbar off (so they don't waste screen estate) :). Still, I would like to see a KVim. I believe someone once tried to make it... though the project was quickly abandoned. The uselessness of switching a toolkit for a GUI that either way is non-existant looks like a good reason :). Though cutting memory usage for a Qt-only desktop would be nice."
    author: "zonk"
  - subject: "Konsole looking good!!"
    date: 2007-05-25
    body: "Looks great, hope that we have some flexibility in setting the titlebar text; my biggest annoyance with Konsole atm!\n\nKeep up the good work, it's really appreciated :)\n\nz."
    author: "effzee"
  - subject: "Tea, finally!"
    date: 2007-05-27
    body: "It's nice to finally see those hard-hearted developers dealing with the severe beverage usability issues in KDE. My old Apple II used to make tea for me when I first woke up, and then it fetched the paper and walked the dog for me. It's 2007 now people, and KDE is only _now_ catching up. Jeez!\n\nOn a more serious note, I'm grateful to Robert for the progress with konsole! I didn't realize I was missing some of these features, but they will be nice to have. Thanks!"
    author: "faketroll"
  - subject: "Wow, a gnome-terminal clone"
    date: 2007-05-29
    body: "The third screenshot from the bottom is almost identical to gnome-terminal. In fact, most of the changes seem to be... \"borrowed\" from the aforementioned. There's nothing wrong with that, gnome-terminal is a good terminal emulator (way better than the old Konsole which was horrific) but this is just like the similarities between Naut and.. what did they call it.. Dolphin? \n\nAh well, I never did like KDE so perhaps this subtle GNOMEification en-route to KDE4 will make it somewhat better. Certainly beats KDE's usual copying of OSX and Windows with their horrible design sense. C'mon people, the proprietary OSes don't actually have all the answers and - regardless of what Shuttleworth says - Linux should not try to be Windows plus Mac, it already has so much more to offer."
    author: "Flax"
  - subject: "Wow, a new troll record"
    date: 2007-05-30
    body: "I will certainly propose you for the troll-of-the-month-award! And I'd say your chances to win are actually quite high."
    author: "wassi"
  - subject: "Re: Wow, a gnome-terminal clone"
    date: 2007-05-31
    body: "lol, KDE doesent copy osx nor winblows. it creates whats best, and has done that for a long time. Ideas may come from seeing other applications, as with all, but it neither acts or looks as osx or winblows."
    author: "redeeman"
  - subject: "KDE4 Konsole - the road to Gnome"
    date: 2008-06-28
    body: "I use console ALOT, and while have no problems with the simplification of a convoluted and complex menu structure i do have a problem with the reduction of useabilty and configuration options. I see no particular advantage to a menu in my console, as i am using a command line because i need to type commands. One of the most usefull things about the old tab bar was the button for a New Console, as thats were the mouse was (at the bottom near all the tabs) the menu was turned off (just extra clutter) and it's alot easier than ctrl-shift-N. There are many examples of this type of thing in KDE4, I will keep using KDE3.59 untill it no longer ships, then i will switch to gnome, which although i find more anoying to use, works KDE4 seems broken even when its running as it should.\nVince \n(KDE user since version 1.x)"
    author: "vince"
  - subject: "Re: KDE4 Konsole - the road to Gnome"
    date: 2008-06-28
    body: "I love how you simply assume that all of the features that haven't been ported yet will never be, and how you completely ignore any features in KDE4 and its apps that weren't in KDE3, as if the whole exercise of the creation of KDE4 was in removing features.\n\nAnyway, sounds like you've made your choice regardless - have fun with GNOME! :)"
    author: "Anon"
  - subject: "Re: KDE4 Konsole - the road to Gnome"
    date: 2008-06-28
    body: "So, basically, Vince, you're telling us that you miss two things in Konsole for KDE4?\n\n* hide the menu\n* have a new tab button in the row of tabs\n\nHave you actually looked at konsole in kde4 at all? Right-clickon on your konsole window. Click on the menu option that's labeled \"show menu bar\". Now the  menu bar is hidden.\n\nThen, right click on your konsole window. Choose \"edit current profile\". Go to the second tab, labeled \"Tabs\". Check the checkbox labeled \"Show new tab and close tab buttons in tab bar\". Now you've got your buttons.\n\nAnything else you didn't bother to actually check out before complaining that you need help with?"
    author: "Boudewijn Rempt"
  - subject: "Re: KDE4 Konsole - the road to Gnome"
    date: 2008-07-08
    body: "Well...  I followed your most excellent instructions for the \"new tab\" button and guess what, there's no checkbox on my \"Tabs\" tab.\n\nI have 2 text boxes under \"Tab Titles\" and two combo boxes under \"Tab Options\".\n\nNo check boxes.\n\nKDE 4.0.5 on Fedora 9\n\nAnywhere else I should be checking before I complain? :) :)\n\nPat"
    author: "Pat"
  - subject: "Re: KDE4 Konsole - the road to Gnome"
    date: 2008-07-08
    body: "Yes - a more recent version of KDE4 ;)"
    author: "Anon"
  - subject: "Re: KDE4 Konsole - the road to Gnome"
    date: 2008-07-08
    body: "Hah!  Talk to the nice folks over at Fedora, maybe you can motivate them... :)\n\nStill waiting for my 4.1 packages..."
    author: "Pat"
  - subject: "Re: KDE4 Konsole - the road to Gnome"
    date: 2008-07-08
    body: "Oh well - here's a little screenie to tide you over :)\n\nhttp://etotheipiplusone.com/kde4-konsole-buttons.png"
    author: "SSJ"
---
Again, after a delay brought on by a bout <a href="http://behindkde.org/people/troy/">Real Life (tm)</a>, we return to bring you updates on the state of Konsole, KDE's UNIX terminal program.  Konsole has been a staple of KDE since KDE 1.0, as has been showing signs of a little bit of clutter and wear. So, Robert Knight has stepped in to clean up the program's code, and more than anything else, fix a cluttered and difficult interface. Read on for the details.





<!--break-->
<p>The goal of Konsole itself is pretty simple: provide a window to run a command prompt and command-line applications from. In fact, it has evolved from the very simple 'kvt' terminal program in the pre-KDE 1.0 days. An older KDE 1.x version of Konsole is pictured below:</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_1x_konsole.jpg"><img src="http://static.kdenews.org/dannya/vol14_1x_konsole_thumb.jpg" alt="The Konsole of versions past" title="The Konsole of versions past" /></a> <br /><i>Image courtesy of a (very old) book about Linux at <a href="http://linuxbook.orbdesigns.com/">linuxbook.orbdesigns.com</a> licensed under the (also very old) Open Content License v1.</i></p>

<p>The version of Konsole from that era was very simple, but it wouldn't stay that way, as more and more features were added. Konsole users wanted transparency (shiny!), support for more text encoding schemes, ways to control every feature possible: Konsole ballooned into a monster. By KDE 3.x, it can best described as a highly-functional mess.</p>

<p>As an example of just how bad Konsole had become for KDE 3.x, I present the following screenshots, the first showing a normal Konsole window from KDE 3.5.6:</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_356_konsole.png"><img src="http://static.kdenews.org/dannya/vol14_356_konsole_thumb.png" alt="Konsole's main window, KDE version 3.5.6" title="Konsole's main window, KDE version 3.5.6" /></a></p>

<p>Possibly the worst settings menu of any KDE application in KDE 3.5.6 follows:</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_356_konsole_settingsmenu.png"><img src="http://static.kdenews.org/dannya/vol14_356_konsole_settingsmenu_thumb.png" alt="Konsole's settings menu, KDE version 3.5.6" title="Konsole's settings menu, KDE version 3.5.6" /></a></p>

<p>And if that isn't enough, actually going to the settings dialog makes the situation even worse.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_356_konsole_settings.png"><img src="http://static.kdenews.org/dannya/vol14_356_konsole_settings_thumb.png" alt="Konsole's settings, KDE version 3.5.6" title="Konsole's settings, KDE version 3.5.6" /></a></p>

<p>If this isn't the most user-friendly theme dialog you've ever seen, then demand a refund. (I joke! It's actually terrible for many reasons, but the seemingly random nature of it is probably the best reason to dislike it).

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_356_konsole_schema.png"><img src="http://static.kdenews.org/dannya/vol14_356_konsole_schema_thumb.png" alt="Konsole's schema configuration, KDE version 3.5.6" title="Konsole's schema configuration, KDE version 3.5.6" /></a></p>

<p>The worst offenders are the settings menus, as you can see. An overly complex settings menu leads itself to an ugly settings dialog.</p>

<p>When the KDE 4 development series commenced, Robert Knight took over maintainership of Konsole. He decided that he would focus on bug reports and feature requests filed via the KDE Bugtracker, <a href="http://bugs.kde.org/">bugs.kde.org</a>. Ever aware of the fact that users can be pretty picky about what features an application like Konsole needs to have, he created a few online surveys to help with the determination of the most common as well as fringe use cases. <a href="http://kdemonkey.blogspot.com/2007/04/easter-holiday-terminal-update.html">This feedback has driven much of the work.</a></p>

<p>The end result is a Konsole for KDE 4 that is visually very similar, functionally improved and with a settings system you can actually stomach. The screenshot below shows that the main window has not changed too much. The tabs are shown at the top in this screenshot, however Robert tells me that they have been defaulted to the bottom of the window as this article went to press. Additionally, you'll notice that the text on the tabs contains more helpful information. This is configurable is a friendly manner - see three shots down.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_4x_konsole.png"><img src="http://static.kdenews.org/dannya/vol14_4x_konsole_thumb.png" alt="Konsole's main window, KDE version 4.0" title="Konsole's main window, KDE version 4.0" /></a></p>

<p>The once-intimidating settings menu now becomes very simple. It may look like the configuration options are gone, but they are still all available in a sanely organized fashion.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_4x_konsole_settingsmenu.png"><img src="http://static.kdenews.org/dannya/vol14_4x_konsole_settingsmenu_thumb.png" alt="Konsole's settings menu, KDE version 4.0" title="Konsole's settings menu, KDE version 4.0" /></a></p>

<p>As you see below, the settings menu leads to a Profile selector, under which all the settings are kept separated. They are much more organized now, rather than an odd collection of random check boxes.

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_4x_konsole_settings.png"><img src="http://static.kdenews.org/dannya/vol14_4x_konsole_settings_thumb.png" alt="Konsole's settings, KDE version 4.0" title="Konsole's settings, KDE version 4.0" /></a></p>

<p>And lastly, the appearance people will appreciate this dialog: its implementation is effective and its use becomes obvious. Additionally, Robert has implemented style previews in an intuitive manner. As you mouse over the style, the Konsole window in the background automatically applies that style in an active preview. So you can very rapidly look through and appreciate the styles just by hovering over the list.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_4x_konsole_appearance.png"><img src="http://static.kdenews.org/dannya/vol14_4x_konsole_appearance_thumb.png" alt="Konsole's appearance configuration, KDE version 4.0" title="Konsole's appearance configuration, KDE version 4.0" /></a></p>

<p>Side-by-side comparisons aside, Konsole also offers a number of other improvements. Among them, split-view mode, faster scrolling (thanks to a smarter line redrawing scheme), hotkeys and more.</p>

<p>Much of the inspiration for these improvements comes from analysing other programs. For example, the split-view mode, pictured below, is inspired by GNU Screen. It is a console output cloning tool so that you can see two views of the same scroll buffer. For example, if you are a developer, and you need to compile something really big (like say, KDE), then you can read through the scroll at your own pace on one side, while still monitor the output progress simultaneously. This is not a multi-panel model like Konqueror, so much as it is a cloning mode that lets you see more than one thing at a time within the same buffer. In this shot, the two sides are displaying the same output, just scrolled to different points.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol14_4x_konsole_splitview.png"><img src="http://static.kdenews.org/dannya/vol14_4x_konsole_splitview_thumb.png" alt="Konsole's split view mode, KDE version 4.0" title="Konsole's split view mode, KDE version 4.0" /></a></p>

<p>There is friendly interaction between Konsole and some of its biggest users. In particular, Yakuake just recently implemented a <a href="http://www.kde-apps.org/content/show.php?content=29153">Split View</a> mode like the one listed above. When I asked <a href="http://behindkde.org/people/hein/">Eike Hein</a> about the relationship between the two projects, he said "I think Yakuake is beneficial to Konsole in KDE 4 in so much as Yakuake is a more demanding user of the Konsole KPart than most applications, so developing Yakuake has resulted in finding out a few things about how the KPart's interface can be improved :)"</p>

<p>Konsole has benefited from a few features that were requested by the Yakuake users. For example, there is a new hotkey that pops the terminal up and down quickly. This was in response to requests from the Yakuake users when Robert did his interface survey. Of course, like all things KDE, it is configurable. With Robert at the helm, it is even more user-friendly to configure it.</p>

<p>Future plans for Konsole include, among other things, ideas such as: tear off tabs, a commandline configuration interface, and making tea. I asked Robert if it would one day make coffee, but he's British and much prefers it to make tea it seems. Perhaps when it obtains beverage-making abilities, this argument will resurface once again. :)</p>

<p>On a side note, this is the first time I have attempted to write my article from within KDE 4 itself. While a few applications were not stable enough to use, including Kicker (which is dying anyway), the experience was good enough that I will probably do the same henceforth. Now that the libraries have mostly settled down (with a few exceptions), the changes are becoming more apparent in the applications. I'll be keeping an eye out for more things to feature in this series, but the next topic should be the KWin window manager, barring any major problems. Cheers.</p>



