---
title: "aKademy 2007: Edu & School Day"
date:    2007-04-24
authors:
  - "amahfouf"
slug:    akademy-2007-edu-school-day
comments:
  - subject: "Merci  "
    date: 2007-04-25
    body: "Anne-Marie, j\u0092aimerai bien vous remercier pour votre article, je crois que les licences pay\u00e9es par l\u0092\u00e9tat pour acheter des logiciels propri\u00e9taires dans les \u00e9coles, doivent \u00eatre mieux utilis\u00e9 pour autre choses.\n\nAmicalement   \n"
    author: "djouallah mimoune"
  - subject: "one friendly appeal"
    date: 2007-04-26
    body: "could you please stop making the headers so silly? we really dont care which letter is big and which small, its not cool anymore.\n\nor at last stop duplicating it in rss channel.."
    author: "nescius"
  - subject: "Re: one friendly appeal"
    date: 2007-04-26
    body: "I am not sure what you mean here. If you mean that the first letters are capitalized in the title, well, this is the English way of writing titles. The aKademy and the mEDUXa capitalizations are not mine. \nAbout the rss channel: not sure what you mean either.\nIt's a bit disappointing for me that you pick that up in my article while it occurs very often on the Dot that a typo fixe is made and it's probably akregator that should be fixed. It looks negative which is not nice, considering all the work I and the aKademy team are putting into that.\n"
    author: "Anne-Marie Mahfouf"
---
You are invited to <a href="http://akademy2007.kde.org/codingmarathon/schoolday.php">aKademy Edu &amp; School Day</a> on Tuesday 3rd July.  This day will focus on installing and running free educational software in schools, presenting software as well as getting feedback from teachers and community people. Read on for details.











<!--break-->
<p>Highlights include:</p>

<ul>
<li><a href="http://wiki.debian.org/DebianEdu">Skolelinux</a> and <a href="http://www.grupocpd.com/archivos_documentos/info_meduxa/meduxa_project_released/PloneArticle_view"> mEDUXa</a> in schools</li>
<li>Feedback from a teacher using KDE at school 
<li>The <a href="http://gcompris.net/">GCompris suite</a> (on OLPC and associated with <a href="http://www.tuxisalive.com/">Tux Droid</a>)</li>
<li>New <a href="http://edu.kde.org">KDE-Edu</a> programs: <a href="http://edu.kde.org/marble/index.php">Marble</a> and <a href="http://kalgebra.berlios.de/">KAlgebra</a></li>
</ul>

<p>The day is freely open to everyone interested in Free educational Software. If you want to attend the conference, <a href="http://www.kde.org.uk/akademy/">register for aKademy</a>.  Make sure you sign up before 30th April to register for accommodation.</p>

<p>If you use free educational software in your daily work and are interested in giving a talk, please <a href="mailto:annma AT kde DOT org">let me know</a>!</p>

<p>For more information see the <a href="http://wiki.kde.org/tiki-index.php?page=Edu+%26+School+Day+%40+aKademy+2007"> aKademy Edu &amp; School Day wiki page</a>. Help spread the word using this <a href="http://edu.kde.org/akademy2007/flyer.php">flyer</a>!</p>







