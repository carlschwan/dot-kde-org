---
title: "The Road to KDE 4: Oxygen Artwork and Icons"
date:    2007-03-08
authors:
  - "tunrau"
slug:    road-kde-4-oxygen-artwork-and-icons
comments:
  - subject: "The Screenshot"
    date: 2007-03-08
    body: "On the screenshot there is a padlock on the PNGs for cystal, but why not for oxygen?"
    author: "Ben"
  - subject: "Re: The Screenshot"
    date: 2007-03-08
    body: "never mind I found the awnser. No emblem for locked yet."
    author: "Ben"
  - subject: "Comments about oxygen"
    date: 2007-03-08
    body: "I've been browseing around the web-svn, thanks for the link Troy, I couldn't find it yesterday, and I have some comments. I'll may have more later.\n\nI like the folder-music, folder-video, etc. but folder-tar dosn't look so good. I know the tar is supposed to be a cardboard box, but at that angle its more like a brown square, or a brown doghouse. Change the angle a bit, or maby use an actual ball of tar.\nBTW how dose that setup work? is it that a user defines a folder to use one of those, or some alogrithum.\n\nAnd on the subject of folders, I prefer the angle used by cystal to that of Oxygen. I don't suppose there is any plan to create an Oxygen style folder set drawn from the same angle as cystal?\n\n\n\nApps: I can't comment as a whole, since their so varied, but the ones I checked were lovely. Although why dosn't Kmail have an @ on the stamp?\n\n\n\nMimetypes, I'm afraide I don't like this set, the source code files just have the letters \"C\" or \"py\" on a standard file. It should be a very styleised C and a python respectively. As for the archives (tar tgz zip rpm deb), why are they an icon inside a file? surely they should look big and chunky to represent the fact they're often many files in one. \n\nThat said most of the actual files, like the image or midi are quite nice.\n\n\nA more general coment, I noticed that you've put the word oxygen around the place, such as Oxygen is the address to Kmail, and Oxygen times is on the news file. Gave me a little chuckle :)"
    author: "Ben"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-08
    body: "\"or maby use an actual ball of tar.\"\n\nEhm... what would a ball of tar look like?\nI mean, I know about tar, some kind of greasy dirty liquid, \nbut I'm not familiar with the concept of a \"ball of tar\" \nand I can't visualize that. So the association would\nprobably be lost on me."
    author: "Darkelve"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-08
    body: "You know I didn't actually think of that,\n\nI suppose the closest you could get would be a ball of playdo, with the imperfections http://cookingmonster.files.wordpress.com/2006/04/playdo.jpg\n\nonly make it black. and stick a big zipper on the front if its a compressed tarball.\n\n\n\n\n\n"
    author: "Ben"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-08
    body: "Or a tar baby, though that might be seen as racist I suppose.\n\nA barrel of tar?"
    author: "jacob"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-08
    body: "How about a tared and feadered person then?\n\n"
    author: "Debian Troll"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-08
    body: "I don't think an actual picture of tar is a good idea. For those unfamiliar with what a tarball is they would be clueless as to what it means but most people will recognize a box or package as a extracted zip or something similar which is more what they are going for. Obviously this is just my opinion though.\n\nI really love how the icons are coming, keep up the great work."
    author: "Devon"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-08
    body: "Then how about a box of tar? ;p"
    author: "Matt"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-08
    body: "Indeed, I think it's a bad idea. I am not a native English speaker and before reading this thread I didn't even know that tar was something else than an archive type. Let alone people who don't speak English at all.\nAs a general rule I think it is good to avoid icons based on a pun on the file type, since it is highly language dependant.\n\nAnd as a user I usually don't care about what type an archive is when I see one. I think it is better to keep all the archive icons under the same format and for example adding the letters of the extension on it."
    author: "Nanaky"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-09
    body: "a katamari ball! ;)\n\n...but really, um, I dunno. the comment about language dependence made some very good points. maybe the box could use a little decoration... boxes sent through the mail always seem to end up with lots of junk stuck to them :)"
    author: "Chani"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-09
    body: "Or just a diffrent angle so it looks like a box and not a doghouse"
    author: "Ben"
  - subject: "Re: Comments about oxygen"
    date: 2007-03-09
    body: "I have to agree that the Crystal folder angle is much better than Oxygen's.  I suspect part of the reason this angle hasn't been used is that it is viewed as too extreme, which is an understandable viewpoint.  Perhaps a sort of compromise could be made?  Rather than making Oxygen's folder a totally straight-on view, it could be turned a bit on the Y axis so that one side appears \"closer\" to the viewer than the other.  This would give the appearance of depth while also maintaining the square shape (as opposed to the somewhat diamond shape Crystal folders have, since they appear to also rotate along the Z axis).  \n\nI also agree with the above poster regarding the archive icons.  Rather than having an icon inside a file, like other mimetypes, they should be something different to represent the fact that they are a collection of files.  Perhaps a base \"package\" icon could be used with a smaller image on top of that to represent the type of archive it is.\n\nI hope these ideas have been expressed clearly.  If not, and you're interested in either of them, feel free to respond here or e-mail me and I'd be happy to try and clarify.  I'm just an end-user, but I think we can all appreciate good looking desktops :)"
    author: "Brian"
  - subject: "Re: Comments about oxygen"
    date: 2007-04-13
    body: "In the past (on windows ;( ) I saw an icon represending a folder with a zipper running across it. I allways liked that one."
    author: "Wilco"
  - subject: "Coherence"
    date: 2007-03-08
    body: "What I seem to notice is that the new Oxygen style misses the coherence among the icons like Crystal had.\n\nIs this intended?\nOr is Oxygen still about to change in that regard?"
    author: "Evil Eddie"
  - subject: "Re: Coherence"
    date: 2007-03-08
    body: "> What I seem to notice is that the new Oxygen style misses \n> the coherence among the icons like Crystal had.\n\nWell keep in mind that the actual release is very likely about 6-8 months away. Given that and given the huge amount of work these guys have accomplished it looks pretty coherent already. I'm pretty sure it will be even more coherent than Crystal in the end. One of the reasons is that Oxygen is using a well-defined color palette."
    author: "Torsten Rahn"
  - subject: "Re: Coherence"
    date: 2007-03-09
    body: "if you're basing that on the screenshot in the article, note that a number of those icons are actually crystal icons and not oxygen icons.\n\nif it is what's actually in svn, ther are some sets of icons that are still finding their place in life such as the back/forward/home buttons. but the final icons are pretty consistent.\n\nthere are differences between types of icons, however, and that's intentional. device icons are recognizable as device icons; filesystem icons as filesystem icons; action icons as action icons; etc... the design principles for each group has been selected for the given use case each fulfills and to allow them to be easily discernable."
    author: "Aaron Seigo"
  - subject: "Easy Decoration and Widget Style creation"
    date: 2007-03-08
    body: "Hi all\nI think KDE needs more and more Widget Style and WinDecos, but currently creating them is hard, needs code and compiling and...\nI was thinking that its a dream that we make Widget Style/WinDecos by easy XML files and SVG artwork.is that really possible?\nI know Dekorator is there, but currently:\n--Everybody who wants to use Dekorator, needs to install it (its not default)\n--Installing themes for it is not easy (yes, i know thats just download and copy, i mean it should use KHotNewStuff)\n--Selecting themes in different Theme Engines makes some problems, there just be one list, windecos list, currently you have to choose Engine and select your theme in the Engines list.\n\nKHotNewStuff is really needed for Windeco selection, icon part and Style widgets part.\n\nPS:why didnt you talk about the Oxygen Sound Theme??"
    author: "Emil Sedgh"
  - subject: "Re: Easy Decoration and Widget Style creation"
    date: 2007-03-08
    body: "There are some plans in this area, so it might be possible in KDE 4. But nobody is really working on this yet, as far as I know. Currently, themes are C++ code, apparently because of performance reasons..."
    author: "superstoned"
  - subject: "Re: Easy Decoration and Widget Style creation"
    date: 2007-03-08
    body: "At one point there was work on a SVG-based widgetstyle engine for KDE 4, CoKoon. Then the author lost interest I think.\nThere is also Detente for pixmap-based widgetstyles in KDE 3."
    author: "logixoul"
  - subject: "Re: Easy Decoration and Widget Style creation"
    date: 2007-03-11
    body: "Qt 4 has introduced the possibility to do styles using CSS some syntax. There was even an article about that at this site. Dunno how that works for doing gradients, and other drawing effects...\n\nAnyway, I have just started a project for a KDE style that uses GTK+ for the rendering (the opposite of gtk-qt-engine) [1]. Saying this cause I have done this QSimpleStyle layer which simplifies doing a C++ style using QStyle. It handles the placement of widgets parts, their sizes and etc, as well as highlighting, for you. You just need to paint primitives (like the step button of a scroll bar) and set the size of some attributes (stuff like how much pixels the label of a button should be displaced when pressed).\n\nI will be doing a simple Qt style with it as an example... Anyway, if someone is interested, drop me an email.\n\n[1] http://gtk4qt.sourceforge.net/"
    author: "blacksheep"
  - subject: "Re: Easy Decoration and Widget Style creation"
    date: 2007-11-13
    body: "I was wondering if theres a web site that has widget styles to download in rpm form. i use FC7 and im kinda sick of kemerik style. its one of the style i use. i love it but i want to have a veriety to choose from. that give a 3d look to buttons and all that great stuff.  thanks for a reply. i am also using the crystal theme. i also have dekorator, and its not as hard to install theme&#347; as you think."
    author: "ed"
  - subject: "Dolphin"
    date: 2007-03-08
    body: "I recently installed Dolphin 0.8.2 to get a little glimpse of what to expect of KDE4. Well, Dolphin is certainly a step forward. It solved the long-standing problem for me to have separate bookmarks for web and file browsing. Some things are a bit confusing though IMHO. For instance in Konqueror, when in the detail view, I could select a file without opening it, by not clicking on the filename but somewhere to the right i.e. on the date or filesize column. This doesnt work anymore in Dolphin and I really, really miss it. The new filter is great. It works much better than Konqueror's and doesnt forget about filter settings when changing folders which makes it much more useful. Storing folder views is still very problematic here. But it has been in Konqueror for a long time as well. So, not worse but no improvement either here. I tried in vain to once a for all store detail view for all folders. Still, Konq and Dolphin come up with icon view folders for some reason. This seems to happen particularly often on FISH:// folders but perhaps that's just my impression... All in all - I can see a promising direction and I do appreciate it.\n"
    author: "Michael"
  - subject: "Re: Dolphin"
    date: 2007-03-09
    body: "Hello... \n\njust to put some emphasis on this:\n> when in the detail view, I could select a file without\n> opening it, by not clicking on the filename but \n> somewhere to the right\n\nYes! definitly missing this feature! The first click I made within Dolphin disapointed me, beacause it throw me into a folder, but i just wanted to select it.\n\nHow is it at all possible to select just one file for eg copy? The two options i found: right click and then close the menue (by rightclick again) or pressing ctrl and clicking.\nBoth options are very cumbersome and nervewrecking... so how is it indeed planned to select one file?!\n\nBest regards \nDaniel"
    author: "Daniel W"
  - subject: "Re: Dolphin"
    date: 2007-03-10
    body: "If you want to copy it, just right-click on it and choose copy. If you want meta-info just hover over the file.\nIf you really just want to select the file, you can draw a frame around just one file, that's easy and fast to do in icon view but a tad cumbersome in list view. You could also use keyboard navigation.\n\nIs there any reason to select just one file, unless you want to perform an action on it (->right-click), d&d it (just do it =) or get file-specific information (hover)?\n\nNow one thing I hate about the debian community more than anything else is the \"why'd you want to do that\" attitude whenever I have a question, so please note I'm not dismissing your question, but I think it would help me find better suggestions or join you in demanding that feature if I knew why you want it.\n\n"
    author: "ac"
  - subject: "Re: Dolphin"
    date: 2007-03-10
    body: "Some reasons:\n1) Sometimes if lots of files are selected, I'd like to click somewhere to clear the selection and get a plain view on the files again and not have a whole screen draped in blue. And as ridiculous as it sounds: It somehow gives me a safe fuzzy feeling that not so many files are selected and could be deleted or whatever just by the touch of a button...\n2) If you like to use Ctrl+C and Ctrl+X/V to move and copy files or you like to use the Delete key instead of the context menu, you can't use that if you cannot simply select a file. Right now I'm opening the context menu and press \"Escape\" but I also find that somewhat cumbersome. Not that I'm in favour of copying every feature of Konq. It's ok to go different ways. But this is really a basic feature for me and the solution found in Konqueror is so simple anyway.\n"
    author: "Michael"
  - subject: "Re: Dolphin"
    date: 2007-03-10
    body: "1. At least with my Dolphin version (0.8.2 - Debian Unstable) clicking to the right of the files in list view deselects everything.\n\n2. Just found out that a middle-click *does* select a file without triggering an action. Hadn't tried that till now because I'm used to Konqi's \"middle-click opens in background tab\" behavior. =)\n"
    author: "ac"
  - subject: "Re: Dolphin"
    date: 2007-03-10
    body: "Jeah, Michael already named two of the main usecases for selecting a file by clicking. \n\nSome other which would come to my mind:\n\n*) sometimes if you show someone something on your computer (if you sit together in front of it or over a VNC-Session) \"Just take that file [click and select] and....\"\n\n*) for setting a marker, if you have to to something with multiple files, and you have to go to another program, select the next-to-handle file.\n\n*) mostly, if i select multiple files, id like to select the first one, without pressing ctrl, to be sure, that there are no other (unwanted) selected files anymore (which might be out of scroll)\n\n*) and so on...\n\nThe nice thing about that feature is, it wouldnt annoy you, if you dont want to use it. or?\n\ncheers,\nDaniel"
    author: "Daniel W"
  - subject: "Nice looking, but..."
    date: 2007-03-08
    body: "\"They are a team of developers and artists that are dedicated to making things look beautiful.\"\n\nWhat about that big ugly empty space to the right of the menu and the toolbar buttons?\nThe first thing I do in Windows is to make one bar ot the top of explorer and set the scrollbar to minimum width.\nI do this with Firefox too.\nHow  can I do this in KDE 4?\n"
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "I mean like this:\n"
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "Do you mean, like this?"
    author: "lqtys"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "Close, but one or two bars too many."
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "Exactly.  Yours is a little cluttered for my taste though :)  All those buttons have keyboard shortcuts, and you can search google and any other search engines right from the address bar.  The only thing you need is the back and forward buttons, because there is no easy way to skip back multiple pages using the keyboard.  This gives plenty of space for the address bar.\n\n"
    author: "Leo S"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "Yes, but see how much empty real estate there is on the grab-bar.\nIt's ready for developement."
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-31
    body: "<ALT>-<left> and <ALT>-<right> correspond to back and forwards. Easy enough on most keyboards, and amazingly easy on some laptops, particularly the smaller ones. Works in dang near every browser, too."
    author: "Alex E"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "I agree.  Believe it or not that's the *only* reason I use Firefox instead of Konqueror as a web browser."
    author: "strider"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "http://bugs.kde.org/show_bug.cgi?id=35795"
    author: "logixoul"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "Pending more than 6 years. \nIf it doesn't happen with KDE 4, it will never happen.\n\nThe Oxygen team must look into this, it's both about usability and estetics."
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "This goes a lot deeper than Oxygen and even Usability.\n\nIt might require changes in Qt. I am not sure."
    author: "Thiago Macieira"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "that's actually not a domain within which the oxygen project works.\n\nwhile i'm all for simplified toolbars that highlight just the most used and critical items, shoving icons and menus into one big line is perhaps generally questionable =)\n\nit could be one of those configurable things, i suppose, but it would require a fair amount of hacking. for how much benefit? *shrug*"
    author: "Aaron Seigo"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "For the benefit of at least 3 users on this thread alone.\nYou could do it in KDE 1, look at this:"
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "For the benefit of at least 3 users on this thread alone.\n\nnow 4, \nPersonally i found menu bar as a great place for bookmark folders."
    author: "Karol"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "At least 5 people.\n\nAt least with certain screen dimensions I like the MacOS style of having the menu bar at the top of the screen, but even then I have it on the same bar as the system tray, pager, shortcuts, etc.\n\nGiven a different screen size (here at work) I would like to toss the menu and toolbar on the same row.\n\nVertical screen real estate is in high demand on these wide screen displays..."
    author: "mactalla"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-10
    body: "at least one on the other hand doesn't find that this is something even close to being useful. 2, counting aseigo (I suppose :)"
    author: "Vide"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-10
    body: "asiego counts as an infinite number, the cause is lost, lost...."
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-10
    body: "heh ... not infinite; comensurate to participation, perhaps, and certainly within a reasonable distance to the origin. the biggest problem with me is i try and get rational argument and bit of big picture thinking into the mix. that sucks. it's much easier when you can line up 5 like minded people and bend the will of a project ;)"
    author: "Aaron Seigo"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-11
    body: "Actually this is about reading pages, documents, on screen like it was on paper.\nI don't want any unnecessary bars or borders to obstruct my reading enjoyment.\n\nPreferably I also would like to be able to move the window (paper) around on the\n desktop by grabbing it anywhere on the page, holding down a mouse button.\nAlso move the page inside the window by holding down another mouse button. (Or chord)\n\nWho would want a frigging stereo on top of his document? That's like having a\n frigging laser on top of a shark, and who would want that? Dr Who? No! Dr No?\nNo, Dr Evil, thats who!\n"
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-10
    body: "Fortunately for you both, it already provides you with all you need/want, then.  I'm happy for you.\n\nI suppose it's similar to me finding the keyboard switching globally or per application not even close to useful.  But thankfully I do have the option for per-window.  So I'm happy, and so is anyone who prefers a different configuration.  (That option in the Keyboard Tool was, incidently, the deciding factor for me to choose KDE over Gnome when I first switched to Linux).\n\nWe all have our preferences, and by permitting one thing we don't need to limit another.  So whether or not we ever get our wish, will in no way harm you or your preferred setup. :)"
    author: "mactalla"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "I also agree, wanted to post this as a wish to bugs.kde.org, but then I think it's a Qt limitation, which they really can't workaround. But it would definitly be very usefull to save this wasted space."
    author: "Martin Stubenschrott"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "What you are perceiving as ugly is not perceived as ugly by most people. \nIt's a common concept called \"white space\" or \"negative space\".\n\nhttp://en.wikipedia.org/wiki/White_space_(visual_arts)"
    author: "Torsten Rahn"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "No.\nThis is about noise and information. The empty space here is annoying,\nit acts as noise to my information gathering. It could instead be filled\nwith information, adding to the information content."
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "If you want to save even screenspace of menu bars in all of your (KDE-) apps, enable the Mac-like menu bar on top in KControl."
    author: "Arnomane"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "Best thing would be to have all bars autohide like Kicker."
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "toolbars are useles for people who know about 'Configure shortcuts' dialog."
    author: "nick"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "You didn't read the article, did you? That empty space is what you need in some places to make stuff look good and _not_cluttered_ . I'm not opposing to make it configurable in some ways. But please acknowledge common usability findings and decades if not hundreds of years of experience that designers have gathered.\nWhat you are aiming for is obviously a setup for somebody who wants to squeeze the last bit of efficiency out of each pixel. That's a good thing for you as a person, but keep in mind that it might not be the best for everyone."
    author: "Torsten Rahn"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "Still no.\nThe toolbar is a tool, not art. The headline of the article is: \"White space (visual arts)\"\nForm over function sucks, function over form rules.\n\n(My fault is to write this here since it seems to be the concern of Qt, not  of Oxygen.\n You could do it in KDE 1)"
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "> Still no. The toolbar is a tool, not art.\n\nA tool - especially if it's in wide use - is always subject to considerations of look and feel. Due to the sheer importance of look and feel in today's world you've got to make compromises between better looks, better usability and flexibility.\nWhat you obviously think about when talking about usability is efficient and comfortable usage of a kind of poweruser who doesn't care about esthetics. However that's only one small part of the whole broad spectrum of users KDE supports.\n\n> My fault is to write this here since it seems to be the concern \n> of Qt, not of Oxygen. You could do it in KDE 1)\n\nI remember someone from Trolltech having said that it was dropped because nobody really used it. And worse: I have seen many people who \"lost\" their menubar in Windows and KDE 1 because they managed to move it in some way out of sight.\n"
    author: "Torsten Rahn"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "if menubar could have been \"lost\" in windows, then it was a windows bug. \n\nit's sufficient to implement the functionality. no need to replicate the bug also ;-)"
    author: "neurol23"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "\"a kind of poweruser who doesn't care about esthetics\"\n\nI'm not a poweruser (whatever that is) and I do care about esthetics.\nIf you had looked at my screenshot you would have seen ample amounts of \nwhite space, but in the content where it belongs.\n\nI just don't think an empty space in the upper right-hand corner of every\napplication is good esthetics or good usability.\n\n\"someone from Trolltech having said\"\n\nI am sure Matthias Ettrich can hack this during a coffee break or some such.\n"
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "I think that's one of the most important reason i neither use epiphany nor konqueror, but firefox.\n\nIn my opinion we don't need 90% of the options to configure the toolbar, we need just ONE function to make the menubar behave like many people want and expect it.\n\nThere's a screenshot of my firefox in the attachments.\nI would never suggest configuring konqueror like this as a default, but the people who want to configure it, should be able to do it.\n"
    author: "soc"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "You can remove the menubar in konqueror (it's set to the keyboard shortcut ctrl+M by default) and disable/enable whatever toolbars you like, so you actually can make konqueror look like that pretty easily.  Saving the session so that it starts up with the menubar disabled is kind of tough (since the session save option is on the menu), but if  you temporarily put the option in a toolbar or as a keyboard shortcut you can do it."
    author: "nobody"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "I don't want to remove the bars completely, I want combine them to one bar.\nThat includes the grab-bar too, only one bar at the top is enough."
    author: "reihal"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-08
    body: "You probably mean something like that ! (see atached screen shot, which is in full screen mode)\n\nYou have to right click a tool bar an choose \"configure tool bars\" in the bottom.\nthen, add and remove what you whant to be on you \"one line\" pannel.\n\nFinaly, go to \"configure\", then toolbars, and unselect everything but the one you customised.\n\nuse ctrl + m to hide the menu.\n\n\nYOU'RE DONE"
    author: "kollum"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "I already tried the hint with the menu, but I just don't like it.\n- I want to be able to access the menu without activating it and deactivating it after use. There a so many icons for doing useless things, but this one is missing.\n- It looks like crap, even the adressbar isn't vertically centered. There is way to much vertical spacing, but no horizontal spacing at all. This is due to the fact, that you can't make the icons smaller than 22*22 in the gui of konqueror, which wastes screenspace and looks horrible."
    author: "soc"
  - subject: "Re: Nice looking, but..."
    date: 2007-03-09
    body: "I have to correct myself:\n- I just saw that if kcontrol is installed you can set the icon size from there for all programs, including konqueror.\n- I installed the QtCurve theme, now many things look quite good, compared to the default ones provided with kde 3.5.\n\nSorry for my mistakes!"
    author: "soc"
  - subject: "Icon colors"
    date: 2007-03-08
    body: "It looks like it's coming along really well, I like this look much more than crystal. The only suggestion I have is regarding the general kde color scheme. I realize that blue's a popular color but it'd be nice if there was a green or brown version of these for those of us who don't like blue."
    author: "Matt"
  - subject: "Re: Icon colors"
    date: 2007-03-08
    body: "Brown would be great. I'm currently using a brown theme. The problem is the coloring of the icons. I mean, these colors are defined inside the svgs, right? So this could get quite difficult"
    author: "Jonathan"
  - subject: "They aren't blue, they are black/white"
    date: 2007-03-08
    body: "What is the philosophy behind much of the icons being black and white? I like color.\n\nhttp://websvn.kde.org/trunk/KDE/kdelibs/pics/oxygen/32x32/actions/media-podcast.png?revision=640052&view=markup\nhttp://websvn.kde.org/trunk/KDE/kdelibs/pics/oxygen/32x32/actions/media-skip-forward.png?revision=640052&view=markup\n\nhttp://websvn.kde.org/trunk/KDE/kdelibs/pics/oxygen/32x32/actions/ etc."
    author: "Ian Monroe"
  - subject: "Re: They aren't blue, they are black/white"
    date: 2007-03-09
    body: "colour is indeed great. think of a painting full of beautiful, rich colour. now consider a frame that is just as colourful. imagine using that same frame on every single painting, regardless of the colours in the painting. it would look pretty bad.\n\nthat's what our action icons are: they are art that frames the real show: the application. they should be atractive, recognizable and get out of the way without conflicting with the contents of the application's informational display.\n\nyou may note that mimetype, filesystem and device icons are more colourful and application icons even more colourful yet. action icons are, due to how and where they are used, purposefully muted and kept simple when it comes to colour and shapes, however."
    author: "Aaron Seigo"
  - subject: "Re: They aren't blue, they are black/white"
    date: 2007-03-09
    body: "+1!\n\nI just got a MacBook, and noticed how all toolbar icons are in grayscales. So far I noticed:\nit's beautiful.\ndoesn't sit in my way.\nfeels really uncluttered.\ndoesn't demand attention when I don't need the toolbar.\nlet me focus to what's really important.\nmacs also have really colorful icons, exactly for applications.\n\nSo I feel Oxygen is going to become a key element to make KDE feel uncluttered. :)"
    author: "Diederik van der Boor"
  - subject: "Re: Icon colors"
    date: 2007-03-08
    body: "Have you used Icon Effects?\n\nThese allow you a lot of things, including general manipulation of colours. I personally reduce the saturation, which turns the blue into a light grey/blue, which I find pretty, but you could mess around more. There is even a nice GUI for it.\n\nKay"
    author: "Debian User"
  - subject: "Looking better and better"
    date: 2007-03-08
    body: "Troy,\n\nThank you for these wonderfull updates! It really makes my day. The progress in the new icon set is really nice.. I like them all! Well except the tar one, maybe that one is still beeing modified?\n\nIs there also some progress at the \"sound theme\" ? It would be nice to hear something about that."
    author: "Justin van Wijngaarden"
  - subject: "Re: Looking better and better"
    date: 2007-03-08
    body: "The sound themes are still very much a work in progress.  I know there is progress, but I haven't really looked into it so far.  They don't present themselves as well as screenshots do either, sadly. :P"
    author: "Troy Unrau"
  - subject: "Compatible to freedesktop?"
    date: 2007-03-08
    body: "Does this mean that Oxygen is naming compatible to the freedesktop standard? Does this mean that it really use the same names as tango for example? That would be really great!\n\nWith the KDE4 release, do you think it is possible to dual license the artwork under LGPL/EPL. That would make it possible for us, the qooxdoo developers, to also use the Oxygen icons in our JavaScript Framework qooxdoo (http://qooxdoo.org) which is also under LGPL/EPL."
    author: "Sebastian Werner"
  - subject: "Re: Compatible to freedesktop?"
    date: 2007-03-08
    body: "As far as I know, oxygen icons are compatible with the freedesktop standard, and \"licensed under a creative commons v3 attribution, share-like license\" (http://aseigo.blogspot.com/2007/03/daytime-excitement.html)"
    author: "shamaz"
  - subject: "Re: Compatible to freedesktop?"
    date: 2007-03-08
    body: "1. Yes, it does use the freedesktop.org icon naming specification (the one initially published by the Tango team). So you can load Oxygen as a GNOME theme and get happy with it.\n\n2. Stemming from KDE's requirements, it currently has a few more standard icons than what is specified in the icon naming spec. Which means that KDE applications will make use of those \"non-standard\" standard icons, and KDE applications won't get all the required icons from a theme that restricts itself to \"standard\" specified icons.\n\nThat's mainly because the icon naming specification has strong roots in GNOME/GTK+ naming, and the Oxygen team has to work together with the Tango team to get more of the important KDE stuff into the naming specification. It's a matter of communication, but now that Oxygen actually follows the overall naming specification scheme, it will be a lot easier to work out.\n\nI'm looking forward to kdelibs just containing icons from the spec (and a few additional application icons), and the spec containing all icons that are for KDE apps. Let's see how this works out."
    author: "Jakob Petsovits"
  - subject: "Re: Compatible to freedesktop?"
    date: 2007-03-09
    body: "yes, we're hoping to move at least some of our icon names up into the spec where there are gaps in the spec. this will likely happen post-4.0 as things shake out and firm up more."
    author: "Aaron Seigo"
  - subject: "Re: Compatible to freedesktop?"
    date: 2007-03-09
    body: "Having read http://en.wikipedia.org/wiki/Eclipse_Public_License I doubt that this will happen. Not only can you use the Icons without attribution then, but also claim them for yourselves, under restrictive license.\n\nLook here from techbase:\n\n----\nImage previews of Oxygen are released under the Creative Commons Attribution-NonCommercial-NoDerivs 2.5 License. This is to keep Oxygen fresh for KDE 4, that will be released sometime in the middle of 2006. \nThe Oxygen icon theme will be released under a GNU License. Most likely, this will be LGPL - however there is hope to have an official GNU License for icons/images by that time.\n----\n\nSo before KDE release the Oxygen Icons are not even Free. Afterwards they likely still want to be them KDE icons, not Eclipse icons.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Compatible to freedesktop?"
    date: 2007-03-09
    body: "the license changed last week before being brought into kdelibs. the CC v3.0 share alike, attribution license is now being used. it's an art appropriate license that affords the freedoms we expect from similar licneses we use with our software."
    author: "Aaron Seigo"
  - subject: "Re: Compatible to freedesktop?"
    date: 2007-03-10
    body: "Ok, Cool. So hopefully this means that the new licence (cc v3) is compatible with debians DFSG?"
    author: "Petteri"
  - subject: "Re: Compatible to freedesktop?"
    date: 2007-03-10
    body: "i don't think this is a settled question yet, sadly."
    author: "Aaron Seigo"
  - subject: "professional"
    date: 2007-03-08
    body: "Great to see Oxygen evolving as much more than just an icon set. The team really gives a professional look to the desktop, and that's what gets me most excited. Congratulations to the excellent team!"
    author: "Mootjes"
  - subject: "audio icon"
    date: 2007-03-08
    body: "I just browsed through the icons and they are lovely.\n\n1. Something about the 'audio folder' icon does seem a little odd to me. The icon here: http://websvn.kde.org/*checkout*/trunk/KDE/kdelibs/pics/oxygen/128x128/places/folder-sound.png?revision=640052\n\nCompared to most of the other icons, this icon almost seems bland; I mean the speaker part, not the folder part. I don't particularly like the yellow border of it either.\n\nWell, maybe that is just a personal feeling."
    author: "Darkelve"
  - subject: "Re: audio icon"
    date: 2007-03-08
    body: "> Well, maybe that is just a personal feeling.\n\nIt is, I like that speaker. Then again this is art, its _all_ going to be personal feelings :)"
    author: "Ben"
  - subject: "Fine grained Icon control"
    date: 2007-03-08
    body: "In KDE3 you can chose an icontheme in the control panell. in KDE4 will we be able to take the MIMEtypes from one iconset and the apps from another. And an individual icon from a third. (without manageing jpgs in ~/.kde/)\n\nFor example, I may chose the location icons from cystal, the rest of the icons from Oxygen, and download a debian spiral for opening the Menu.\n"
    author: "Ben"
  - subject: "Shadows"
    date: 2007-03-08
    body: "I like the icon set in general, but the shadows annoy me. Look at the \u0093Back\u0094 & \u0093Forward\u0094 icons from this article\u0092s screenshot. Then compare them to, say, Tango. In Oxygen, the shadows are way oversaturated and appear like an important part of the icon (which they are not). Whereas in Tango, it\u0092s just a vague gray spot beneath the icon \u0097 you won\u0092t really notice it unless you specifically pay attention. And from what I can see, this is a problem with many other Oxygen toolbar icons. I hope something can be done about this before the release."
    author: "Vasiliy Faronov"
  - subject: "Re: Shadows"
    date: 2007-03-08
    body: "I agree. Actually I don't understand why the shadows are included in oxygen icons... Everybody is talking about the benefits of SVG (scalable, modifiable etc.) but we don't use them !! I'm sure it's feasible to add the shadows _at runtime_.\nThis would have several benefits : less encumbered svg icons, less duplication in svg sources (-> easier to maintain), *customizable shadows* , action icons more reusable for other things than just icons (ex: animation, games) etc. etc.\n\nThe only problem is... it can impact performances. But I think it could be OK if we have a \"disable icon shadows\" somewhere :) \n\nI really hope the oxygen team will consider these possibilities ! \nThe ideas of Aaron (http://aseigo.blogspot.com/2007/03/efficient-scallable-icons.html) would surely help in this case."
    author: "shamaz"
  - subject: "Re: Shadows"
    date: 2007-03-08
    body: "Agree 100%"
    author: "logixoul"
  - subject: "Re: Shadows"
    date: 2007-03-08
    body: "I also agree.\n\nShadows could be a useful extension if they would provide some information. As one can estimate if an icon is \"on\" an window or \"above\" by its shadow, this would give a \"2.5 dimensional\" impression of a flat 2D Desktop. That is an easy to understand, intuitive and unobtrusive way to show the \"state\" of an icon. It would be a pity not to use it by \"hardwiring\" the shadows into the icons just as eye candy.\n\nEspecially since active window and menu shadows are used that way: To guide the users eye to the active and therefore topmost element on the desktop."
    author: "furanku"
  - subject: "Re: Shadows"
    date: 2007-03-08
    body: "..or you could set the shadow stuff when you select the theme. Then it generates the theme using your custom shadows and you have no overhead as it would then use your generated custom svg files basically derived from the original/default. Perhaps that could even be applied to more things about the icons."
    author: "ac"
  - subject: "Re: Shadows"
    date: 2007-03-08
    body: "*customizable shadows*\n\nOh dear god no.  If KDE4 has an option like \"Icon shadow colour\" or \"Icon shadow size/angle\" then the usability team might as well go and hang themselves :)"
    author: "Leo S"
  - subject: "Re: Shadows"
    date: 2007-03-08
    body: "Shadows soft|strong|off would be enough for me.\n\nI really REALLY don't want shadows under my icons."
    author: "zenity"
  - subject: "Re: Shadows"
    date: 2007-03-09
    body: "That \"brilliant\" idea has been floating around since years already. Believe me: it's neither as easy to accomplish nor is it always desirable. A hand tuned shadow at small sizes will always look better than a plain algorithm.\nThe objects depicted on icons will look strangely stiff and non-vivid if you apply a uniform automatically applied shadow to them. We already did such experiments with the HiColor icontheme in KDE 2.x and in the end decided against that feature. \nI'm sure you're either a programmer or at least you never drew a larger set of icons."
    author: "Torsten Rahn"
  - subject: "Re: Shadows"
    date: 2007-03-09
    body: "Actually, I do NOT understand why it can be a problem. \nTalking about kde 2.x seems off topic to me here. \nI'm talking about SVG icons here, not fixed size icons.\nMost (all ?) of oxygen icons use the same template for shadows. So the XML code of this template is inside each icon XML source.\nThere are several way to compose the shadow and the icon :\na) render them separately as bitmap and blend them.\nb) merge the XML sources of the icon and the shadow together (via XSLT, DOM or whatever you want), and THEN render it.\nc) I'm quite sure there are other ways ;)\n\nI understand solution a) can make strange looking icons. But I don't see why the b) solution could be bad. If the transformation is applied correctly, the resulting source should be the same isn't it ?\nPlease elaborate a bit :)\n\nps: I'm a programmer, but you are right : I never drew a large set of icons :)."
    author: "shamaz"
  - subject: "About Style and Decoration"
    date: 2007-03-08
    body: "First of all, great article.\n\nIn all the Gnome vs KDE flamewars it seems to boil down to the same conclusions: KDE is (much) better in functionality and is better engineered, but Gnome has the slick and clean user interface. I agree with these people. I too find the current default Gnome interface (metacity with clearlooks) very well designed and a pleasure to work with. It is my hope de Oxygen style and decoration will mimick this as much as possible. Also, the way the items on the taskbar in Gnome look is great. They are outlined and this is currently not possible in KDE3. Don't even know if this is themeable or not. Maybe a good idea for KDE4 then?\n\nOn a different note, I noticed Dolphin got the nav-bar. That's great, but I would like to see the nav-bar further integrated into KDE, such as in the file selector, etc. Again, just like in Gnome.\n\nLook at http://gnome-look.org/content/show.php?content=19527 for what I mean.\n\nIf these things would be implemented in KDE4, I am sure it will win over a lot of Gnome users (and hopefully even Novell) to KDE.\n\nJust my $ 0.02"
    author: "cossidhon"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-08
    body: "\"First of all, great article.\"\n\nAgreed :)\n\n\"In all the Gnome vs KDE flamewars it seems to boil down to the same conclusions: KDE is (much) better in functionality and is better engineered, but Gnome has the slick and clean user interface. I agree with these people. I too find the current default Gnome interface (metacity with clearlooks) very well designed and a pleasure to work with.\"\n\nAgreed :)\n\n\"It is my hope de Oxygen style and decoration will mimick this as much as possible. Also, the way the items on the taskbar in Gnome look is great.\"\n\nI've been playing around with the Oxygen style and Windec for a while now, and I fear you may be a little disappointed - it looks great and refreshingly un-Windows-like (though I would fully expect the hard-core KDE detractors to continue shouting \"lok KDE == teh windoze!1\" even if it looked 100% different in every way), but if I were to compare it to existing desktops, I'd say it looks to me more like OS X than GNOME.\n\n\"They are outlined and this is currently not possible in KDE3. Don't even know if this is themeable or not. Maybe a good idea for KDE4 then?\"\n\nThe Kicker (which houses the taskbar) will be killed off in KDE4 in favour of Plasma.  I think a while back that aseigo said that there will be a Plasma DataEngine for the taskbar, so you will probably be able to grab a taskbar implementation that suits you from Hot New Stuff.\n\n\"On a different note, I noticed Dolphin got the nav-bar. That's great, but I would like to see the nav-bar further integrated into KDE, such as in the file selector, etc. Again, just like in Gnome.\n\nLook at http://gnome-look.org/content/show.php?content=19527 for what I mean.\"\n\nThis has certainly been discussed and is wanted by several devs (a strong emphasis on shared code/ components is a KDE hallmark, after all).  I guess we'll have to wait and see if such a move would cause any usability issues.\n\n\"If these things would be implemented in KDE4, I am sure it will win over a lot of Gnome users (and hopefully even Novell) to KDE.\"\n\nFrom my experiences on the heavily GNOME-centric Ubuntu Forums, it seems that a surprisingly high number of GNOME users are keeping a close eye on KDE4 with a view to switching if it proves to be to their taste.  Many of the real die-hards, of course, will continue to make excuses - I know of several people whose *sole* reason (they admit this explicitly) to not use KDE is that most of the apps begin with \"K\", which is frankly the most ludicrous reason I have ever heard.  So expect some new convertees, but not a mass exodus :)\n\nI think you'll find that Novell's choice of GNOME - which, according to most surveys I've seen, is the least favoured desktop by a factor of 2:1 - is grounded more in politics than anything else, and an aesthetic face-lift in KDE4 will not cause them to switch.\n\n \n\nJust my $ 0.02"
    author: "Anon"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-08
    body: "> I'd say it looks to me more like OS X than GNOME.\n\nAnd this is bad how? ;-)"
    author: "Anonymous"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-10
    body: "  \" I think you'll find that Novell's choice of GNOME - which, according to most surveys I've seen, is the least favoured desktop by a factor of 2:1 - is grounded more in politics than anything else, and an aesthetic face-lift in KDE4 will not cause them to switch\"\n\nof course it is hard to accept that and we all know that redhat novell ubuntu have choosen gnome over kde just for the license.\n\nhonestly ISV don't pay Microsoft or apple to develop software for their platform. and how they are supposed to do that for linux.\n\nimho as long as qt is under gpl gnome has always an advantage over us.\n\ni know it is not politically correct to say that but this the truth.\n\n \n\n"
    author: "djouallah.mimoune@gmail.com"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-08
    body: "\"Look at http://gnome-look.org/content/show.php?content=19527 for what I mean.\"\n\nYou mean\n\nhttp://gnome-look.org/content/preview.php?preview=1&id=19527&file1=19527-1.png&file2=19527-2.png&file3=&name=Clearlooks ?\n\nThe default sized dialog there is only able to show 3 1/2 directory entries. Now that is awful, terrible! What kind of use cases do GNOME developers have? I normally have at least 10-40 entries in every directory. The KDE file dialog normally shows ~13 * 4..6 = 52..78 entries there.\n\nForcing me to scroll every single time certainly decreases productivity and usability a lot. KDE 3.5.x even has that navigator. What are we missing now? Huge Add and Remove buttons for the navigator?"
    author: "demise"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-09
    body: "I'm not a fan of the GTK file dialog (in fact, i loathe it), but the default dialog size shows more than 3 1/2 icons, I think either that dialog was scaled down (the KDE one defaults to really small as well, you won't be getting 52-78 without resizing it), or the extra archiver options at the bottom cut away most of the dialog without increasing the default size (which is actually what I think happened).  On my system the GTK file dialog will show 9 icons (and its about twice the size of the default KDE file dialog I believe, which probably shows as many if not slightly more)."
    author: "Sutoka"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-09
    body: "> It is my hope de Oxygen style and decoration will mimick this as\n> much as possible.\n\nor maybe provide something nice, fresh and usable. the clearlooks/plastik styles are looking pretty dated, to be honest. it's also interesting that many people seem to have forgotten which desktop went for that look first =)\n\n> They are outlined and this is currently not possible in KDE3\n\nof course it is. it was even the default until 3.5. look in the configure dialog =)\n\nthough if you want visual simplicity, more bevels don't add much. "
    author: "Aaron Seigo"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-09
    body: "strange, one of the reasons I use KDE is that I can't stand the way gnome looks. :) Plastik was my theme even before it became the kde default.\nhopefully there'll be a good range of standard themes available to satisfy people migrating from different OS's or desktops. it's amazing how much I reduced the level of complaints from my mum just by changing her comp's theme to the redmond one ;)"
    author: "Chani"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-09
    body: "+1, though i prefer .NET  )"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-09
    body: "\nPlease DO NOT put this breadcrumb nav bar! or at least allow config for not using it!\n\nThis nav bar is everything but usable in a day to day use. Why?:\n\n1) can't go thru chmod 711 directory: exmaple: the /home directory is root.root owned with 711 permission: with breadcrumb: no way to got to ~user\n\n2) can't enter directories starting with a dot\n\n3) Far from bein as fast as autocompletion, and if there are lots of sub directories (example in /home on a filer), then the dropdown menu may exeed the sise of the screen or occupy a large surface and you'd have to find where is what you search for...\n\n4) no way to use widlcard like ~/*.txt to see only files ending with .txt\n\nAs for the look, I don't mind a change for default to metacity with clearlooks, but personaly, I don't see much difference with the Plastik them (maybe I'm wrong though).\nPersonaly, I prefer MacOS X looks alike (Noia 2 in firefox is not perfect but quite cool to me).\n\n\n"
    author: "olahaye74"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-09
    body: "> or at least allow config for not using it!\n\nit already is. please try using the software?\n\n> can't enter directories starting with a dot\n\nthis isn't true, actually. i fixed this last week.\n\nthe rest of your items are indeed something that i as a power user often use. they are mostly useless and irrelevant to the bulk of the desktop computing using public. this is why dolphin uses the breadcrumb with an easy, one-click method for switching to a traditional and fully editable label =)\n\n> I don't see much difference with the Plastik them\n\niirc, we shipped with plastik and then later gnome shipped clearlooks in response to \"the kde theme is much nicer\" criticisms. so, yeah. i think the similarity is not coincidental =)"
    author: "Aaron Seigo"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-11
    body: ">> or at least allow config for not using it!\n \n> it already is. please try using the software?\n\nI did a little bit and tryed to right click. Silly me, I forgot the aim is to become like a mac and there is no right-click there......\n\n> the rest of your items are indeed something that i as a power user often use.\n\nI disagree, going thru a 711 chmoded directory is not a power user feature. It's a common thing in multiuser environment were sysadmin takes care of security!\nIn an high school or in a company you often chmod 711 the /home directory so if a user makes a mistake on his home directory, other user won't see!\n\nOlivier."
    author: "olahaye74"
  - subject: "Re: About Style and Decoration"
    date: 2007-03-10
    body: "just don't have some lame \"basic\" mode file selector like gnome.  Its very annoying to be required to click on \"advanced\" every time you want to save a file, and to have two different file selectors for saving files.  I use KDE mainly because gtk annoys me in this manner."
    author: "kde-user"
  - subject: "Howto?"
    date: 2007-03-08
    body: "I tried to make my application use these fresh new icons. KIcon(\"document-new\") (and a lot of other tries) failed, though.\n\nThen I went on to see how dolphin did it, but my Dolpin uses 100% crystal icons. So i grep:ed the source code tree in kdebase and kdelibs for uses of go-next and the like (which in the screenshot is oxygen), but alas! Nothing turned out.\n\nSo I wonder how I actually start using all these goodies in my playground application."
    author: "The noob"
  - subject: "Re: Howto?"
    date: 2007-03-08
    body: "Wait a couple of days and that'll all settle down in SVN.  You still need to run kcmshell icons and unset the old default."
    author: "Troy Unrau"
  - subject: "Re: Howto?"
    date: 2007-03-08
    body: "Hey, now I'm replying to myself. :)  What I mentioned above is only part of it.\n\nThere is a patch floating around that does the renaming in kdelibs.  It hasn't been officially applied to kdelibs and kdebase quite yet, and it didn't even apply cleanly for me.  However, it does exist, and will hit SVN very shortly.  (I haven't checked SVN today, but I did hear \"Thursday\" as a possible check-in date.  Latest is probably Monday.)"
    author: "Troy Unrau"
  - subject: "Re: Howto?"
    date: 2007-03-08
    body: "Thus, I will bide my time,\nThough it does befog my ardour \nAnd may fortune favour thee,\nAs thine writings endows us with joy\n"
    author: "The noob"
  - subject: "Re: Howto?"
    date: 2007-03-09
    body: "They are in SVN now :)"
    author: "Troy Unrau"
  - subject: "Re: Howto?"
    date: 2007-03-09
    body: "I noticed! :) My app runs them now. Hurray!"
    author: "The noob"
  - subject: "Tabs in Dolphin"
    date: 2007-03-08
    body: "Hello,\nI know this is not directly related to Oxygen icons & theme (which I really like), but will Dolphin get tabs? I tried olphin and I like it, but without tabs I will be forced to use Konqueror for file management. I really like tabs and I am used to it.\n\nPlease can you implenment it? Is it planned?"
    author: "Michal Krenek (Mikos)"
  - subject: "Re: Tabs in Dolphin"
    date: 2007-03-08
    body: "Yeh this is the feature I think it is missing. The split view makes up for it a little, but I really like using tabs instead of separate instances of an application."
    author: "Luke Benstead"
  - subject: "Re: Tabs in Dolphin"
    date: 2007-03-09
    body: "In one of the Dolphin articles a week or two ago, one of the examples given was that tabs were something that fit naturally in a web environment, but (somehow) wasn't considered useful for file browsing.  The lack of tabs in Dolphin, as a file manager, was therefore considered an /advantage/ of some sort!!??!!\n\nPersonally, it looks to me like someone's trying to GNOMEify/simplify/dumbify things, as I not only regularly use tabs while file browsing, but also regularly use several of the other features the article deigned from the web side and confusing/complex in a file manager/browser app or the reverse (such as the up arrow being only useful while file system browsing, WTF???, that's a major bonus of using Konqueror as a web browser IMO!).\n\nI DO like the breadcrumbs widget, if it works as advertised, and continues to be single-click (and I expect keyboard accel) switchable with the traditional path/URL entry widget.  I hope Konqueror gets it as well, again with the condition that it continues to be toggle between that and the path/URL entry textbox with the appropriate click.  \n\nHowever, other than the breadcrumbs widget, I've yet to see a single thing in all the Dolphin previews and feature articles that I'd consider useful, and several things missing that I use all the time, so I'm very hopeful that as promised, Konqueror will continue to be available and configurable as the file manager of choice, and hopefully won't eventually have its file management functionality hollowed out for lack of maintenance as everybody focuses on Dolphin.\n\nHowever, knowing the way KDE has seemed to be the choice of the power user, precisely /because/ it exposes so much configurability to the user, and /because/ it hasn't GNOMEed/simplified/dumbed itself into uselessness, I'm pretty optimistic the (apparent comparative) power user's choice, Konqueror, will remain and only become stronger.  \n\nHere, I expect I'll emerge the KDE4 Dolphin and play around with it for awhile, but unless it gets substantially more functionality than I've seen in the reviews so far, I expect at the first update, I'll unmerge it rather than updating it (unless it turns out to be a dependency of something I DO use, like say Kontact, which I don't use, apparently is at least on Gentoo, for KMail, which I do use).\n\nBack on Oxygen, for icons and widget colors, all too often, desktop defaults seem sickly drab/pale/washed-out/gray to me.  The below URL has a now slightly dated (KDE 3.5.1, composite was still pretty new) example of my preferences.\n\nhttp://members.cox.net/pu61ic.1inux.dunc4n/pix/screenshots/index.html\n\nDuncan"
    author: "Duncan"
  - subject: "Re: Tabs in Dolphin"
    date: 2007-03-09
    body: "Well, hopefully you'll be pleasantly surprised by the time 4.0 hits the streets. :)"
    author: "Troy Unrau"
  - subject: "Re: Tabs in Dolphin"
    date: 2007-03-10
    body: "Indeed.  Quite how KDE has managed to create and maintain a desktop environment that is so completely customizable to the way I work, except that they have /not/ been afraid of giving the user those options (unlike other desktops I'm sure most of us have tried), remains somewhat of a mystery to me.  Few others even come close, but KDE... once customized, is much like a glove to my hand.  It's quite amazing, really, but certainly, given that as past and current history, I'm quite looking forward to KDE 4! =8^)\n\n(OT for this particular article, but the new audio framework by itself, is the single most anticipated improvement here.  How many people, both user and developer, have fought the morass that the once great idea that was aRts became over the years?  Certainly here, that was the single worst part of KDE to deal with, the /only/ really bad part, and I'm sure I'm not the only one to be glad KDE is finally freeing itself of what unfortunately became a huge millstone dragging it down.  That alone is anticipated to be worth the wait for me -- everything else then being just bonuses, but what a huge lot of them! =8^)\n\nDuncan"
    author: "Duncan"
  - subject: "Re: Tabs in Dolphin"
    date: 2007-06-14
    body: "Im also missing Tabs very much. What I have seen of Dolphin is quite nice, but it lacks tabs, so i won't use it. What also disturbs me very much that the split view is not extendable to 4 or more parts, and theres NO chance to configure it! Is KDE getting as ugly, unconfigurable and unusable as GNOME? \n\nI want\n\n*Tabs in Dolphin\n*MORE options to configure Dolphin! Please!\n*An option for splitting the split view to 4, 8, 16... Parts\n*Please, no Gnomization of KDE. Every Time i work on Gnome, I hate it. It's ugly and \"prevents\" me from working with such idiotic applications like nautilus. \n\n"
    author: "blueget"
  - subject: "Re: Tabs in Dolphin"
    date: 2007-06-14
    body: "Why don't you just use Konqueror? Sounds like Dolphin is not for you."
    author: "Anon"
  - subject: "Re: Tabs in Dolphin"
    date: 2007-06-16
    body: "Dolphin is cool, and its a good filemanager, but it is very limited. Konqueror is a Internet Browser, not a filemanager."
    author: "blueget"
  - subject: "Re: Tabs in Dolphin"
    date: 2008-01-20
    body: "I too feel the lack of tabs in Dolphin feels very Gnomish. Saying that Konqueror is a Web Browser not a filemanager is just..well... the best I can say is that's one point of view, definitely not shared by many many users.\nSurely it has always been one of the strengths of kde that the user gets to choose as much as feasible, how things work. Nobody HAS to use tabs - but yes some do, and depend on them.\nHowever things pan out, the whole kde experience is really excellent. Keep up the good work one and all."
    author: "skoff"
  - subject: "Oxygen looks great but the folder icon..."
    date: 2007-03-08
    body: "Hi,\n\nIt's nice to see how Oxygen is progressing, most of the icons are a lot better than Crystal, with a modern look and feel.\n\nBut I cant' say the same for the folder icon, because it appears worst than the one in Crystal, it's so 2D, so simplistic it almost disappears in the middle of the other icons (in the Dolphin screenshot).\n\nJust as a example, the folder icon of this iconset below is a lot better than the current Oxygen one. Maybe you can improve that concept or update the Crystal folder icon...\n\nhttp://www.kde-look.org/content/show.php?content=38467\n\nAnyway, you are doing a great job. ;)\n\nThanks and sorry for my bad english."
    author: "kde.fan.from.brasil"
  - subject: "Proposals"
    date: 2007-03-08
    body: "KDE should follow the proposals from Mandriva and implement standard directories in home for \n- Videos\n- Documents\n- Images\n- Music\n\nThat would be useful in the sense that if you download a ogg file it defaults to the Music folder etc.\n\nThe locations described by Dolphin are kind of \"devices\". What you really want is the document driven approach."
    author: "benk"
  - subject: "Re: Proposals"
    date: 2007-03-08
    body: "Thankfully, all of the icons shown on the left in Dolphin are fully configurable :)  So distros like Mandriva can simply change those defaults.\n\nThat said, I don't disagree with you. :)"
    author: "Troy Unrau"
  - subject: "Re: Proposals"
    date: 2007-03-08
    body: "That is being discussed as an XDG implementation, with input from Mandriva. We'll hopefully get it in KDE 4.\n\nWhich in turn means we need icons for those dirs."
    author: "Thiago Macieira"
  - subject: "Re: Proposals"
    date: 2007-03-08
    body: "Nice to hear it. May I ask how will you change the location? "
    author: "Ben"
  - subject: "Icon of an image, learning from other desktops,etc"
    date: 2007-03-08
    body: "I'll be numerating my points, so that they are clearer:\n\n1 - I really like how Gnome (and now windows vista has it as well) uses previews of multimedia files for their icons. So you are dragging pictures around, deleting movies, or hearing previews of sounds on mouse-over.\n\n2 - I also like the minimalistic aproach of Gnome's nautilus, but for new users to computing, I'm seeing a nice feature in the right pane, where it appears dynamic contextual actions that can be done with file selected. \nI usually see newbie users in windows XP using those options a lot (in the left pane).\n\n3 - Also, have you guys gone and look at the original nautilus idea? Instead of the minimalistic spatial file browser that it is now, it was intended more as an organizer/finder/browser/player/wathcamacallit document finder program. It introduced emblems back at a time when tags weren't in anybody's mind, previews of multimedia files in the icons themselves, zoom levels, side-pane contextual options, special views, like music view for a directory full of mp3's, etc.\n\n4 - Have you also looked at the Journal idea from the olpc? It seems to be a step forward, in that it won't be as much a media player as the original nautilus ideas was, and will throw away the directory metaphor, and instead use time, and tags for browsing, collecting, and searching documents.\n\n5 - Finally, you might want to head on to conferences where people with proven usability skills hangout, and share/take some ideas from them. For example, you might want to talk with Andy Herzfeld, the leader of the first version of nautilus. He probably has some great insight on what was good about his vision, and what he eventually figured out was not.\n\n6 - Test every other operating system in the world for fresh ideas! Have a MacOSX and a Windows Vista machine nearby, and see how they work, what pleases and displeases you, and what pleases and displeases other people.\n\n7 - The biggest advantage I see KDE has over any other desktop (maybe MacOSX has this as well) is the integration. It's a very well integrated desktop, everything talks to everything, kwallet is beautifull and still unmatched by any other desktop's password system, etc. Now it's time to bring forward those things, so that even the \"commons\" can have acess to it as well."
    author: "jobezone"
  - subject: "Re: Icon of an image, learning from other desktops,etc"
    date: 2007-03-09
    body: "As an side: Anyone remember Eazel? Nautilus was made by that company who burnt 17 millions in the process of creating it, well starting it. That was an amazing waste of investor money back then. The company went bankrupt, Nautilus was a mess and not maintained for a long time.\n\nAlso, you make suggestions like you would to an individual. Do you really think the KDE project as a whole not already has individuals who do everything they find useful to gather ideas. Ideas can be collected and suggested on their own merits by anybody and reported as wishes on bugs.kde.org and then KDE mailing lists, so why should anybody go out and gather? I mean, there are still people with Amiga experience, lots with OS/2, some know how NextStep was doing things and some yeah, some knew how it was without GUIs. ;-)\n\nIt's my understanding that KDE is now creating a file manager, with at most support for emblems on top of managing files in directories. It's a freaking damn file manager to be after all. Obviously Konqueror has always already made previews, I am using that since KDE 2.2 at least.\n\nAnd furthermore, KDE is having other projects that are aiming at making the desktop be something of a next generation, or semantic desktop, buzzwords there are many. But that's not going to be ripe to be forced into Dolphin.\n\nOne reason why Eazel failed with Nautilus was its pushing for integration of not yet possible things, before it even being useable as a file manager. Lets just continue to solve those problems independently, until both are equally well understood.\n\nAnd yours talking about \"proven usability skills\". Pardon?\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "Re: Icon of an image, learning from other desktops"
    date: 2007-03-09
    body: "Hello fellow Debian user (as I too am one), my suggestions were according to the request made in the main story:\n\"I'm glad that so many people are showing interest in KDE 4's development, but please try to provide constructive feedback to help improve KDE 4. Many of the developers read the comments on the dot and implement things that users request if they are well-reasoned.\"\n\nAs far as Eazel and Nautilus, you are very correct, the thing was heavy and bloated from the beginning, and took many versions to get to at least be a good file browser (when early on it tried to be many more things). Still, you can learn from other people's mistakes, and that was my point with interacting with the ex-eazel guys.\nAnd I wasn't implying that there are no people in KDE with \"proven usability skills\"."
    author: "jobezone"
  - subject: "Re: Icon of an image, learning from other desktops,etc"
    date: 2007-03-09
    body: "1. we've had that since 2.x\n2. dolphin++\n3. yes, and we'll be bringing nice features like this in the sidebars during the kde4 cycle\n4. this is one of the points of nepomuk/strigi. it'll take a while to percolate \"up\" into the apps, but we've been mulling how to do this for the last 3 or so years.\n5. good idea. and people often do that. not everyone, but some.\n6. curious: you really don't think we do that?\n7. \"commons\"? i'm not sure what you're suggesting there (though i'd like to =). i do think we've brought things forward as much as we reasonably can; we've even taken things like hot new stuff to fd.o and take things from fd.o (among other places) regularly ourselves. that said, not everyone shares the integration / framework vision that the kde project embodies. it's sort of a matter of leading the horse to water ... good news for kde is that kde4 adds even more of these nice bits and pieces for anyone to use who would like to."
    author: "Aaron Seigo"
  - subject: "Re: Icon of an image, learning from other desktops"
    date: 2007-03-09
    body: "Thanks aseigo for replying.\n\n1.Yes, I know, but was this the default? Or it was something you choose during the first run on KDE (the ammount of effects, etc) I wondered about this, because the dolphin screenshot in this story doesn't show it like that, but I understand it's still early in its development.\n\n2.Yes, I was complimenting the screenshot of dolphin showing the various options :)\n\n3.Ok\n\n4.Didn't know there was software effort in that part, great!\n\n5.Yep, one of the biggest advantages in free software \"world\" is that people from \"apparent\" competing positions, can cooperate with each other.\nThere is a great quote by Pietro Ubald (Italian philosopher) which says : \"The next great evolutionary leap for mankind will be the discovery that cooperating is better than competing\"\n \nYou should definitely someday meet with Seymour Papert, or Alan Kay, and hear their vision of computing. You can also look up Papert's constructionist theory, which, in a superficial way could be summarized as being Piaget's constructivist theory (you learn by doing), applied to computers. \nIn fact, there are many videos and texts of all of them on the web, and I would advise everyone in the KDE4 effort to have a look at them.\n\n6.Ok, you guys do, excellent!\n\n7.Commoners was perhaps the proper word, but not meant in any pejorative way :) It was meant as the person from the street, which never had a spectrum when he was a kid.  :)\nThe latest Linus vs Gnome discussion delved into an important point: That computing in general should be easy to grasp for someone new to computing, but at the same time leave enough room to get more power from the computer (low ground and high ceiling someone called it). I think Gnome managed to have an excellent low ground (in my opinion), but then they cut off the opportunity to evolve much beyond that, except using gconf-editor (and even so...). This is the reason why there are \"Tweak\"-like apps for GNOME, but none for KDE. KDE has a gigantic high ceiling with nice stairs leading up (for example, manDVD, a great DVD-movie authoring program, fully developed using kommander!), but the ground could get a little lower, or, in my opinion, to as low as you could possibly go (so that a person who just bought his first personal computer, can immediately and to the most of his abilities, get functional with KDE4).\n\nOh, and the \"hot new stuff\" is wonderful, and I fully miss it in Gnome.\n\nKeep up the good work, and looking forward to trying out KDE4 (and maybe switching to it)."
    author: "jobezone"
  - subject: "Re: Icon of an image, learning from other desktops"
    date: 2007-03-10
    body: "1) default\n2-6) =)\n7) we're working on that."
    author: "Aaron J. Seigo"
  - subject: "Re: Icon of an image, learning from other desktops"
    date: 2007-03-10
    body: "1. It's in View->Previews, the Enable/Disable Previews toggle. And it's by default enabled (unless you moved the eye candy slider way to the right in the first time wizard). I'm sure about that because previews are the first thing I have to deactivate in every OS (big resource hog for minimal gain; and audible previews gotta be the worst \"feature\" ever dreamt up by man, ymmv)"
    author: "ac"
  - subject: "Re: Icon of an image, learning from other desktops"
    date: 2007-03-11
    body: "7) But if we have a \"low floor\" won't that mean that everyone will use KDE? You can't want that\n\nJust kidding :)\n\nIn a more serious note, I'd say KDE's floor is about as low as its going to get. Any further would require real computer skills to be taught in schools. Right now people just think that because it involves a computer they cannot learn it. So they don't try. Being even easy won't help that, and once the education system fixes it I think we'll all be pleased to see KDE is already easy enough.\n\n"
    author: "Ben"
  - subject: "reminds me somehow to Windows..."
    date: 2007-03-08
    body: "Great work! I like the clear and sane look!\nBut the image-icon reminds me very strongly to windows... (I always prefered crystal when compared to Windows - its a pity if he Oxygen-Style takes that direction)\nConcerning Dolphin: I like the approach of having a simple application for filebrowsing. One thing came to my eyes in the sceenshot: the preview seems to be a seperate area in the main-window (just like in Windows and OSX). I actually liked the way Konqueror presented previews - as tooltip: very screen-efficient, everything vou have to know without wasting screenspace.\nSince the preview in Dolphin is in a seperate area here my proposal: when previewing an audio-file, why not have a 'play' button there? (same for video) Macs 'finder' does something like that.\nWhat happened to all the cool ideas on the Appeal-Website and the KDE-Artist-Forum? Things like zoom-effect on mouseover and grouping possible actions directly around the selected icon really make sense! \n"
    author: "Heiko"
  - subject: "End Session"
    date: 2007-03-08
    body: "In the end session screenshot, the selection method used (putting a dotted rectangle around the edge of the button) seems really out of tune with the rest of the graphics (and a bit ugly).\n\nHave you guys considered making the selection brighter somehow?  For instance, in the screenshot you could make the box which is selected have a lass transparent background, and it could stand out in this way.\n\nThis would look better and more consistent with the new KDE look.\n\nKeep up the good work, guys!"
    author: "Gustavo"
  - subject: "Re: End Session"
    date: 2007-03-08
    body: "The end session artwork is relatively new, so there will likely be some polishing before it's shipped.  And also, I only have two buttons in that shot, as I used 'startkde' to launch KDE.  If I had launched from kdm, there may have been additional buttons there that aren't shown in my screenie."
    author: "Troy Unrau"
  - subject: "Re: End Session"
    date: 2007-03-08
    body: "Yeah, I'd envision the button focus made evident by a chenge in button tint or, say, an orange halo around the edges."
    author: "jazzuban"
  - subject: "Re: End Session"
    date: 2007-03-09
    body: "Another problem with this screenshot is the icons used to represent the \"end current session\" and \"cancel\" actions...\n\nIn my opinion, the icon for \"end current session\" seens to invite the user to back to the session, and not end it;\n\nBut were said, the work on KDE 4's new logout dialog is just beginning and I believe that this is going to be revised...\n\nAnyway, congrats for the excellent work on KDE 4, I can't wait to begin using it!!\n\n[]s"
    author: "Saulo"
  - subject: "An idea about Oxygen - make it look like 3D"
    date: 2007-03-08
    body: "I really like this new icons' style but, please, make them look 3D, so that they really have three dimensions.\n\nRight now it looks like they are 'shot' (drawn, of course) as if your standing right in front of them.\n\nOn this picture [http://static.kdenews.org/content/the-road-to-kde-4/vol11_4x_dolphin.png] the selected icon looks very ugly - no one can figure out where from the light comes and the shadow is like an alien ;-)\n\nThe red old fashioned \"Root\" icon looks much prettier."
    author: "Artem S. Tashkinov"
  - subject: "Re: An idea about Oxygen - make it look like 3D"
    date: 2007-03-09
    body: "> make them look 3D\n\nheh... some people want usability, others want flash and 3D and ... yeah. it's hard to ballance all these things. in any case, complex icons are actually a downer for general use. still, someone could indeed make such an icon set. i don't see it being the default for kde4.\n\nselection drawing is not finalized for kde4. there are far nicer patches floating around and one of them will undoubtedly be making it in for 4.0"
    author: "Aaron Seigo"
  - subject: "Performance"
    date: 2007-03-08
    body: "Will performance degrade with the use of SVG graphics?"
    author: "Youssef"
  - subject: "Re: Performance"
    date: 2007-03-09
    body: "no, they are pre-rendered into png's and those are loaded. i think we can do better than that, but at least at this point it's no worse than it was before.\n\nthe icon naming spec and mimetype system may help make it worse, however, now that there is a name based degredation heuristic (e.g. if you can't find foo-bar-baz, the loader is supposed to try foo-bar then foo in certain cases; for mimetypes there's a default icon that one is supposed to fall back to). iow, we'll likely end up doing even more disk seeks in the future..."
    author: "Aaron Seigo"
  - subject: "Cool but what about speed?"
    date: 2007-03-08
    body: "I think the new icon set is really cool but a lot of work should be done. The question I have is that the SVG artworks needs time and CPU resources to be rendered; So what are the probable sln to get these eye candy thinks work but without consuming  CPU time? "
    author: "Landolsi"
  - subject: "Re: Cool but what about speed?"
    date: 2007-03-08
    body: "Well, there are a couple of things: KDE pre-renders png images for a few default icon sizes, such as toolbar icons.  For the really big ones, it renders on the fly.  There is also some discussion about caching icons at various sizes so that they don't need to be rerendered.  This of course breaks down for some of the really dynamic, more animated stuff, as you'd have to be caching a high-res, lossless movie...\n\nSo, in summary: most icons are drawn from pre-rendered png's that are generated from the SVGs."
    author: "Troy Unrau"
  - subject: "No way"
    date: 2007-03-08
    body: "I like KDE and I have used it for about 5+ years now. \nAll this huplaa around those new Oxygen icons makes no sense to me at all. \nSo we have a pile of new icons... but those look nice only when they are at size _\"huge\"_. \n\nWay too many details! It's almost absurd. (look at the \"DEVICES\") \n\nSorry, but those icons look almost useless when \"small\" or \"tiny\".\n\ndict.org:  icon is a _symbol_, especially a symbol whose form suggests its meaning or the object it represents.\n\nPicture (in absurd detail!) of a camera is NOT a symbol! \n\ndict.org:   \nIcon (computer science) a graphic symbol (usually a simple picture) that denotes a program or a command or a data file or a concept in a graphical user interface\n\ncheers! \n\n"
    author: "Brrrr"
  - subject: "Re: No way"
    date: 2007-03-09
    body: "most of these icons are not meant to be used at 16px or 22px sizes. there are special versions of several icons for those sizes that eliminate much of the detail, but some are plain not expected to be used in those situations.\n\nwelcome to the world of sane screen resolutions; 1984's 9 inch black and white monitors are history."
    author: "Aaron Seigo"
  - subject: "Re: No way"
    date: 2007-03-09
    body: "some of us are using itty bitty laptops and need all the screen space we can get for content, not icons :)\neven on this page, I have a horizontal scrollbar :("
    author: "Chani"
  - subject: "Re: No way"
    date: 2007-03-09
    body: "On my last test, just a view days ago, I saw that GNOME is not really usable in resolutions <= 1024x768. Please don't make the same mistake with KDE!\nSome icons (I looked for the prerendered .png currently in svn) simply are to detailed...\nBut in general, I think I like the style :-)"
    author: "birdy"
  - subject: "Re: No way"
    date: 2007-03-09
    body: "i suggest looking at which icons are usable at small sizes in oxygen and which probably aren't. i'm not going to ruin the surprise for you, but when coupled with some basic facts from cognitive science you may find that it won't impact use on smaller resolutions.\n\nand really, 1024x768 doesn't require 16px icons."
    author: "Aaron Seigo"
  - subject: "Re: No way"
    date: 2007-03-10
    body: "hm, to back up that assumption, i should mention that my notebook computer, which i live and die by, is a 14\" widescreen that runs at 1280x768 and i use what many have described as \"obscenely large icons\" for filemanagent. i also use 32px icons on the toolbar, usually with text. omg! =)"
    author: "Aaron Seigo"
  - subject: "Re: No way"
    date: 2007-03-11
    body: "At work we have just bought a dell that sports a 12\" screen that runs at 1280x1024 so... :D\n\nAnyway, don't listen to these trolls that want GUIs to stick to 90s...they can use KDE 2.0, not 4.0 if they want :)"
    author: "Vide"
  - subject: "Re: No way"
    date: 2007-03-09
    body: "Actually, for once, I have to agree with the trolls.\n\nAn icon is a symbol, with as few lines in it as possible. My professor in cognition used to say that as long as you can leave out a detail and people will still think their first thought \"that is a camera\", it is a too complex icon. When you no longer can remove anything from the icon without people starting to actually think, it is the perfect icon. Think road signs! binary colored, high-contrast  quickly and easily recognized symbols that can't be misunderstood.\n\nCertainly we don't want to few details, as that will imply a lot of mental activity to figure out what it is, but at the same time we don't want to clutter the icon with too much details, which will also increase mental activity in order to recognize its meaning.\n\nA better word for an icon is pictogram. Think of it as a corporate logo or as a road sign. (or public toilet sign or whatever sign). They are supposed to be quickly identifiable by our brain. \n\nOf course, it's great we have so beautiful large photorealistic arts in KDE, so some people can use it as icons. It is certainly a benefit and something we should encourage. Personally though, the only reason I made the swicth from Gnome to KDE 2 was the highcolor iconset. That is the most beautiful, functionalistic iconset I have ever seen outside of Windows.\n\nThough, most of the icons in Oxygen are really great and I am impressed with the thought-through overall design that will run through the entire desktop experience in KDE, not just another icon set.\n\nIf I had any artistic talent I would have made my own dream iconset and never ever ranted about others work. Unfortunately, I don't have any kind of artistic talent. :(\n"
    author: "The noob"
  - subject: "Re: No way"
    date: 2007-03-09
    body: "the road sign metaphore is useful but also highly limited. we want both usability and beauty in our tools. sometimes one comprimises the other, and this is ok as long as it is within reasonable limits. pursuing the maximization of one at the expense of the other will result in a desktop that fewer people enjoy using. the trick is to maximize both so neither offends and both are workable (e.g. usable and beautiful, if not maximized in either, but the maximal combined value)\n\nas for complexity, etc.. please go look at the different types of icons. they have different guideline details; e.g. action icons are kept purposesfully simple compared to, say, application or device icons.\n\nthis was originally about sizing in any case, and your cog sci prof will probably be able to tell you all about identification of small objects, perhaps some info on motor control issues and certainly information on how larger numbers of items (esp crowded) increases the cognitive load to the point that the interface becomes frustrating and unpleasant for most people.\n\ncramming things down into 16px or even 22px boxes is not the answer. i think the cell phone world started learning that not so many years ago ;)"
    author: "Aaron Seigo"
  - subject: "Re: No way"
    date: 2007-03-11
    body: "Thanks for that answer!\n\nOff course, it's a trade-off, and as you point out, app icons needs more love to be identifible. \n\nI completely trust this team on this matter.\n\nSorry for your time I wasted."
    author: "The noob"
  - subject: "Visibility"
    date: 2007-03-08
    body: "Seriously, Tango icons are far more visible. Oxygen icons are very detailed, have great artistic features and some are very usable. I don't question the style, but how items are displayed.\nIn the idea of being constructive, it would be nice to catalog the icons in a collaborative site (Wiki), where users can provide feedback. But that may be hard, since an icon requires a lot of time to design, and it may not be very constructive to yell and scream."
    author: "Youssef"
  - subject: "Re: Visibility"
    date: 2007-03-09
    body: "art design by committee tends to produce pretty dismal results. particularly when that committee is made up of a lot of non-artists.\n\nit would be interesting if some users took this bull by the horns and set up such a thing to see what emerges. i'd be very hesitant to try the patience and use the time of the artists involved with kde's artwork in such an experiment, however.\n\npersonally, i find tango drab and ugly. there is a good amount of personal preference in these things, and moving to the icon naming spec opens the way to use the icons of your choice much more easily."
    author: "Aaron Seigo"
  - subject: "logout dialog"
    date: 2007-03-08
    body: "If you find the KDE 4's new logout dialog ugly... don't worry... it is. The SVG was only here for test purpose. But now it's gone... it won't hurt your eyes anymore, a new one is commited. Happy oxygen week ;)\n\n"
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: logout dialog"
    date: 2007-03-11
    body: "you have just saved my day :)"
    author: "Vide"
  - subject: "More appealing..."
    date: 2007-03-08
    body: "I have to say from what I've seen on http://websvn.kde.org/trunk/KDE/kdelibs/pics/ Oxygen is coming along very well and will give KDE4 a much more professional look in general. I'm particularly interested in the benefits of the \"resolution independence\"."
    author: "Jonathan Zeppettini"
  - subject: "Displaying Information in the sidebar"
    date: 2007-03-09
    body: "I really love the bigger preview and additional information during the mouseover. How is this done with the sidebar ? Will there be a double click default setting ? ;)\n\nSure a good sidebar is a really great feature, but always using Ctrl+Mouse to get a bigger preview for the image is not good.\n\nWill the mouseover feature be available in kde4 ?\n\nGreetings\nFelix"
    author: "Felix"
  - subject: "business cards"
    date: 2007-03-09
    body: "Man, I still like crystal.  The electronic business card has.. business cards.  Oxygen's has a silhouette with partially see through shoulders?  I can't call either inspired: using cards to represent business cards isn't exactly creative, but I don't find that a bad thing.  IMHO it beats using an abstract head.\n\ntinfoil hat on\nChanging the name of icons is not so nice.  All the applications get to change their code, and then where are people left who prefer to use old icon themes?  Of course people who care can update the old themes to use the new names but it seems like \"oxygen lockin\"\ntinfoil off.  \n\nIf I had one criticism to make, it would be that the mimetype icons use too little of the space available.  The green speaker cone and soundwaves thing is very minimal, with a lot of white space around it.  The disembodied head too.  I'd use more room!  Make them easier to differentiate at a glance!  I'm not an artist though. \n\nIt'll be nice if someone grabs the oxygen icons and puts up a kde3 icon set at kde-look.org.  The Oxygen folks might have forbidden it, dunno.  I'd love to see them up close though and be able to actually speak from experience instead of scrounging for details from the occasional screenshot."
    author: "MamiyaOtaru"
  - subject: "Re: business cards"
    date: 2007-03-09
    body: "> it seems like \"oxygen lockin\"\n\nNonsense. It's about having a standard naming scheme between different desktop environments."
    author: "Paul Eggleton"
  - subject: "Re: business cards"
    date: 2007-03-09
    body: "i agree that the vcard icon isn't the best. there are a number of icons that could be better, and the artists know this =) many of the icons are already much better than in current crystal though, imho.\n\nthe cards in the crystalsvg icon are hardly intuitive, however. they could be anything card like: credit cards, debit cards, name tags, business cards, candy tins ... the oxygen mimetype at least embodies the purpose of a vcard: identity.\n\n> Changing the name of icons is not so nice. All the applications\n> get to change their code, and then where are people left \n> who prefer to use old icon themes?\n\nit's a standardized naming scheme, so it should have the -opposite- effect you are concerned about here. this is a standard we've adopted, not authored. so ... \"oxygen lockin\" is rather innacurate.\n\nbut what about those old icon themes? we have 6 themes in kdeartwork to 'port'; just as the code porting resulted in a nice script, i'm sure we'll end up with something similar for icon themes so you can just run them through a script the changes the names. voila.\n\n> It'll be nice if someone grabs the oxygen icons and puts\n> up a kde3 icon set at kde-look.org\n\nit'd also be nice if kde4 has a distinctive look."
    author: "Aaron Seigo"
  - subject: "MPEG Layer 3 Audio"
    date: 2007-03-09
    body: "In the description, it says \"MPEG Layer 3 Audio\" for a MP3 file. Why not simply say MP3 Audio. \"MPEG Layer 3 Audio\" is too geeky for a normal user. Same with \"Mime Encapsulated W...\"\n\nAlso, the description should be short enough to be completely visible. \"Plain Text Document\" can be renamed to just \"Plain Text\". Infact, \"PNG Image\" can become just \"Image\", and \"MPEG Video\" can become \"Video\" only. File type for advanced users is anyway visible via the extension, so why confuse joe's grandma?"
    author: "nilesh"
  - subject: "Re: MPEG Layer 3 Audio"
    date: 2007-03-10
    body: "I completely agree with this comment. The information should be cristal-clear and not just for the sake of geekness!"
    author: "David"
  - subject: "Re: MPEG Layer 3 Audio"
    date: 2007-03-10
    body: "I also have to agree. You can maybe show a more complete naming for mime in the properties dialog.\nKDE shpuld start thinking about simplification. This change won't be any problem for power users, but for common people, will be much better."
    author: "Iuri Fiedoruk"
  - subject: "Re: MPEG Layer 3 Audio"
    date: 2007-03-11
    body: "File extension tells you squat about the file. It can easily be very inaccurate. The kde information is taken by inspecting the file and so it will be as accurate as the file sniffing capabilities are. I definitely don't want to see things like PNG Image just turn into image, I often care about the types of these things and just being able to hover my mouse over a file and gets it details is very useful.\n\nIf you do want to dumb it down at least provide some setting in the control center to always show advanced view. Having to always go into some properties box on a file and click on an advanced tab to see all the real information would be a major pain.\n\nTo be honest I don't really care about joe's grandma, I care about getting my work done and kde has excelled at letting advanced users do things that you just can't do with any other system and save a huge amount of time in the process. If you want joe's grandma to use it then make some joe's grandma settings just leave the advanced stuff there also and keep improving it. It is wonderful to be able to put an sftp url into the file upload box in a webpage in konqueror and submit it and have it all just work or being able to edit a document stored on a remote server all transparently."
    author: "Kosh"
  - subject: "Re: MPEG Layer 3 Audio"
    date: 2007-03-12
    body: "I completely disagree! KDE4 should not be dumbed down like GNOME. KDE4 should be the most powerful desktop environment ever!\n\n> Why not simply say MP3 Audio.\nHow else will the average Joe's learn what MP3 stands for? :-) Why not educate people, rather than dumb down an entire desktop environment?\n\n> Infact, \"PNG Image\" can become just \"Image\", and\n> \"MPEG Video\" can become \"Video\" only.\nThat would remove the only useful part of the description! I can tell whether a file is an \"Image\" or a \"Video\" by the preview. A lot of people actually work with files, believe it or not, and like to know what type of file it is. (And, like the previous poster mentioned, file extensions can lie, and in Linux they are completely optional.) For instance, is this file a JPEG image, or a BMP that needs to be compressed before it's published it on the company website? Is it an Scalable Vector Graphic or a GIF? Before I email this attachment, is it a DOCX or an RTF? These differences matter to web developers, graphic artists, and many other people.\n\nEven Average Joe cares sometimes. Say he wants to download some music and video to his portable media player, but it only accepts JPEG's and MP3's. He needs to be able to tell AVI's from MOV's and OGG's from WMA's in order to know which files to convert. Or in my case, if it's a video, I need to know whether it's an MPEG video or an Ogg Theora before giving it to my Window's using friend, who wouldn't know how to play an Ogg Theora.\n\nAccurate descriptions will not confuse Joe's Grandma. If it merely said \"PNG\" maybe it would, but \"PNG Image\" is still pretty clear. If Joe's Grandma is anything like my mom, she does not use the file explorer anyway - she only uses the \"Open/Save\" dialog."
    author: "kwilliam"
  - subject: "Preview ?"
    date: 2007-03-09
    body: "I would like to know if you will include previews ? I think a folder witch is containing pictures for exemple should show a small preview on the icon of 2 or 4 pictures, this could looks realy nice and be useful in some cases."
    author: "Agrou"
  - subject: "Re: Preview ?"
    date: 2007-03-09
    body: "This sort of thing is planned, and in many cases implemented.  In fact, I had to create 0-size files with random extensions to create those screenshots specifically so that there was no previews in konq, and you could see what the actual icons looked like. :)\n\n(I know I could have turned previews off and then back on, but this was faster.)"
    author: "Troy Unrau"
  - subject: "Re: Preview ?"
    date: 2007-03-09
    body: "we already support folders showing emblems reflecting content. the emblems aren't thumbnails of the images inside the folder (would make a nice addition, indeed) and we do already support thumbnails on files, of course, for the last 7 or so years..."
    author: "Aaron Seigo"
  - subject: "Dolphin Quick search"
    date: 2007-03-09
    body: "I was wondering if dolphin will have a quick filter/search box like most mail applications have. I was browsing a large directory and I would have loved to just types \"bla\" in a box to have only the files show with \"bla\" in it. That would make working with large directories much easier ( and might even make me lazy and start throwing more documents in the same folder, but is that really bad ;) )\n\nLooking forward to KDE4"
    author: "Bas Grolleman"
  - subject: "Re: Dolphin Quick search"
    date: 2007-03-09
    body: ">I was wondering if dolphin will have a quick filter/search box like most mail applications have.\n\nIt won't, it already has. Tools -> Show filter bar, and start filtering :).\n\n  -138-"
    author: "138"
  - subject: "Re: Dolphin Quick search"
    date: 2007-05-04
    body: "However this doesn't seem to include directories (they are not filtered), which is quite annoying.\n\nTobias"
    author: "Tobias"
  - subject: "KDE's look and feel is too big and too thick"
    date: 2007-03-09
    body: "I think everything should be more refined and more polished, with clean 1px borders instead of thick and blurred borders, and handlers. Colors are too dull, we need to bring life into KDE. KDE 3 also had dull colors. The thick arrow used to show a file is a shortcut is really too big and too thick. It needs to be more discreet and subtile. The \"File | Edit | View\" tool bar is too high, it needs to use less screen space, reducing its padding would do the trick. Also, I suggest using DejaVu Sans Condensed, it looks more professional and less rounded. If you'd like me to participate in improving KDE's look and feel, please contact me: landemaine=at=gmail=dot=com"
    author: "Charles"
  - subject: "Re: KDE's look and feel is too big and too thick"
    date: 2007-03-09
    body: "The fonts usually depend on the distro, not on KDE.  And that widget style will not be the default for KDE 4 either, as there is an Oxygen Style in the works that is seeing active development, so that problem may just go away.  It uses colours in key places."
    author: "Troy Unrau"
  - subject: "Re: KDE's look and feel is too big and too thick"
    date: 2007-03-09
    body: "> I think everything should be more refined and more polished\n\ni agree. this takes time and effort and one person only moves so quickly. please join us in making things more refined and polished =)\n\n> Colors are too dull\n\nyour opinion is, for better or worse, at odds with most people on this point.\n\n> It needs to be more discreet and subtile\n\ni agree. do you have an alternate piece of artwork to show what you mean? preferably something you've made that could then be used? (licensing issues =)\n\n>  tool bar is too high, it needs to use less screen space,\n> reducing its padding would do the trick\n\nnegative space; it's not wasted space. this is a design fundamental. something apple gets and almost none of their users do, except intuitively.\n\n> DejaVu Sans Condensed\n\nthat's not really up to us, as the system integrators / OSVs tend to make the final font decisions. i agree it would be nice to use a standard font for our publicity screenshots that shows well, though =)\n\n> If you'd like me to participate in improving KDE's look and feel\n\nit works the other way around actually: you subscribe to the mailing lists that interest you, join the irc channels perhaps, start creating with others. we don't have the time or ability to go \"headhunting\" =) if you need some pointers on where to start, please feel free to ask. i hope to see you around."
    author: "Aaron Seigo"
  - subject: "Re: KDE's look and feel is too big and too thick"
    date: 2007-03-09
    body: "> that's not really up to us, as the system integrators / OSVs tend to make the final font decisions. i agree it would be nice to use a standard font for our publicity screenshots that shows well, though =)\n\nI just use whatever kubuntu (feisty now) gives me by default.  If we could decide on a standard, I'd be happy to use it :)"
    author: "Troy Unrau"
  - subject: "Re: KDE's look and feel is too big and too thick"
    date: 2007-03-10
    body: "we talked about this at the last LSB face-to-face i was at. turns out the problem is that distros ship their own tweaked versions of various fonts.\n\nthiago suggested we could just say, \"this is the font. ship it!\" and maybe we should take the driver's seat here because the OSVs are certainly screwing it up right now.\n\nit still comes down to somehow getting red hat, novell, mandriva, etc to ship the same fonts without tweaking them or otherwise replacing them with similar but different fonts.\n\nthis is even worse for people trying to do content creation on linux, e.g. write documents for use in the office."
    author: "Aaron J. Seigo"
  - subject: "Set video thumbnail"
    date: 2007-03-10
    body: "I don't know if this is the right place to post it, but I would like to be able of selecting the thumbnail images of my videos.\n\nFor instance, while I'm playing a video, then with the right click: \"set frame as thumbnail\".\n\nThat way I could set the frame where appears the title of the movie and not just the first frame (usually blank) or the middle of the movie (uninforming).\n\nBy the way, the icons look great. Well done!"
    author: "David"
  - subject: "Re: Set video thumbnail"
    date: 2007-03-10
    body: "I like the idea of specified frame video thumbnails, but it does sound like it could be somewhat complex to implement.  To do it right would require a KDE core interface that individual video players could hook into, then implementing the UI in each video player.  It's possible, but requires a lot of coordination and a lot of folks seeing it as worth the trouble both to integrate, and to ship as a core part of KDE.  Having an arbitrary frame hard coded is just \"easier\".  Unless it's already in process, therefore, I expect it to be unlikely for KDE 4.0, but it'd sure be nice for 4.1 or 4.2 or whatever. =8^)\n\nKnowing KDE, maybe it's already in process. =8^)\n\nDuncan"
    author: "Duncan"
  - subject: "Re: Set video thumbnail"
    date: 2007-03-10
    body: "Why not taking the frame, converting it to a svg or png. Than place it in .kde and update the .desktop or whatever file for the movie ? Aren't there such files for this ?\n\nSure a general solution is better, but is it really a problem if only kde players support the feature ?\n\n\nFelix\n"
    author: "Felix"
  - subject: "Mimetypes"
    date: 2007-03-16
    body: "I really like the oxygen icons.\nI was thinking about the mimetype icons in the first screenshot - they look much too thick. Shouldn't they look like paper, not (thick!) plastic?"
    author: "T1m"
---
One of the big visual changes just happened in KDE 4, the 
transition of kdelibs to the <a href="http://oxygen-icons.org/">Oxygen Icon set</a>.  This transition is 
still in progress, and it includes a massive icon naming scheme 
change that affects thousands of files.  But, the Oxygen artwork 
project much is more than just an icon set, it's a unified way to do 
artwork for KDE 4.  SVG an essential part of Oxygen, so many 
applications that are now capable of SVG display are also using 
Oxygen styled artwork.  Read on for more...






<!--break-->
<p>Please keep in mind that the artwork I am showing today is a
<em>work in progress</em>, but shows things that have already made
their way into KDE's SVN as the new default.  Oxygen will be the
default art scheme throughout KDE 4, but many of the elements can
still use some tweaking.  If you have constructive feedback on any
of the artwork demonstrated today, the Oxygen team would be happy to
hear about it in the comments. :)</p>

<p>Back on the first of January, I wrote an <a
href="http://dot.kde.org/1167723426/">article</a> showing some SVG
widgets making their way into KDE, thanks in part to Qt's new SVG
capabilities.  Some of the artwork shown in that article was
placeholders that were produced by the Oxygen team.  Since then,
there have been improvements to much of those graphics, but the
really big visual change that just happened is the inclusion of the
new Oxygen Iconset into the KDE libraries as the new defaults.</p>

<p>Oxygen is a far reaching project, and extends well beyond icons.
They have a sort of unofficial tagline: "a breath of fresh air for
your desktop", which encompasses the look and feel of the whole KDE
environment.  They are a team of developers and artists that are
dedicated to making things look beautiful.  And not just shiny
effects either, they are ensuring that KDE has a unified, easy to
recognize interface.  For example, icons that end up in toolbars all
have the same shadows below them to give them a consistent look.
Colour palettes have been created for the artwork to ensure that
icons don't clash with one another, and yet are still easily
recognizable.  All of the icon sources are SVG files create using
Inkscape (and other SVG capable programs), and having the sources
available makes it easier to make simple tweaks to the SVG files.
</p>

<p>We also now have an official icon naming scheme for KDE 4.
Previous versions of KDE grew the naming scheme organically as KDE
evolved, so it was somewhat random in many places.  The Oxygen team
was responsible for developing parts of this naming scheme, but they
did so as part of freedesktop.org so that there is less confusion
about icon schemes between Gnome and KDE (and other environments) in
the future.</p>

<p>So, rather than just talk about Oxygen, I have some screenshots
to show the icons in action.</p>

<p>Below is a screenshot of Dolphin showing Oxygen icons, and a shot
of Konqueror (from KDE 3.5.6) showing the same folder.  Many of
these mimetypes also have previews available for them, when previews
are enabled.</p>

<p align="center"><a
href="http://static.kdenews.org/content/the-road-to-kde-4/vol11_4x_dolphin.png"><img
src="http://static.kdenews.org/content/the-road-to-kde-4/vol11_4x_dolphin_thumb.png"
alt="Dolphin Showing Oxygen Icons" /></a></p>
<p align="center"><a
href="http://static.kdenews.org/content/the-road-to-kde-4/vol11_356_konq.png"><img
src="http://static.kdenews.org/content/the-road-to-kde-4/vol11_356_konq_thumb.png"
alt="Konqueror and Crystal Icons" /></a></p>

<p>You'll notice in the Dolphin shot that there are still a few old
icons sticking around, even though the Oxygen iconset includes
replacements to those icons.  One of the biggest changes that
happens are part of the Oxygen transition is that many icons got
renamed.  Old code may be referring to the old icon names, rather
than the newly corrected Oxygen names -- when the crystal SVG icons
are removed from kdelibs, it will become more apparent which names
are affected.  For those who like the old icons better, they will
also get renamed, and be offered as an icon-theme within the KDE
artwork package.</p>

<p>As the Oxygen Icons have now been made the default, you will be
seeing them in all future articles in the Road to KDE 4 series, and
should get a better appreciation of how complete this artwork is.
Of course some icons still have room for tweaking, which is easy
thanks to using SVG sources.  I'm not providing the screenshots of
the whole iconset in this article as you can find them in <a
href="http://websvn.kde.org/trunk/KDE/kdelibs/pics">websvn</a> or by
building KDE 4 yourself.   The next snapshots of KDE 4 will of course include the new icons as they are now considered the default.</p>

<p>But, like I said, Oxygen isn't just about the icons.  There are a
lot of other places within KDE where the Oxygen artwork is popping
up.  Here is a shot of KDE 4's new logout dialog.</p>

<p align="center"><img
src="http://static.kdenews.org/content/the-road-to-kde-4/vol11_4x_logout.png" alt="KDE4's
logout screen" /></p>

<p>One of the biggest advantages to using Oxygen artwork in various
locations throughout KDE is that it is (mostly) resolution
independent.  Which means, certain applications can be made to scale
to any size you want, and it will still look good.  So, for
instance, if you are playing KBounce (from KDE Games), and you want
it to be big or small, it just adjusts the size for you.</p>

<p align="center"><a
href="http://static.kdenews.org/content/the-road-to-kde-4/vol11_4x_kbounce.png"><img
src="http://static.kdenews.org/content/the-road-to-kde-4/vol11_4x_kbounce_thumb.png"
alt="KBounce in KDE 4, two
difference sizes..." /></a></p>

<p>So while KDE 4 is not a true, resolution independent desktop, and
this isn't necessarily a goal for KDE at this time, some KDE
components do now operate on a resolution independent basis.</p>

<p>There is another two elements of Oxygen currently in development,
that are not yet complete.  These are the Oxygen Widget Style, and
the Oxygen KWin Decoration.  These have not yet been made the
defaults for KDE 4 as they are not yet far enough along.  But owing
to the fact that it has not yet become the default for KDE, I'll
decline to show it off just yet.  Just bear in mind that the Oxygen
Icons and related artwork are just a few elements of the Oxygen
project.  The Oxygen team is making a lot of progress on the Style
and Windeco, but this whole project is an enormous amount of
work.</p>

<p>There are also other visual elements of KDE 4 underway that do
not directly involve the Oxygen team, but will work together with
them when required.  These are things like KWin's composite branch,
or the Plasma Workspace theming capabilities.</p>

<p>For those that are interested in helping KDE out through
artwork, you should visit #kde-artists on irc.kde.org and get in
contact with some of the artists there.  They are quite friendly,
and take constructive feedback from artists and non-artists
alike.</p>

<p>Individual KDE projects are also looking for artists:  Recently,
Carsten Niehaus of Kalzium <a
href="http://edu.kde.org/kalzium/iconsets.php">put out a request</a>
for some help producing some kid-friendly icons to represent the
elements of the periodic table in an optional kid-friendly layout.
Anyone up to the task should visit the #kalzium irc channel.</p>

<p>Also, the Amarok project has recently informed me that they are
in need of some artwork for their upcoming 1.4.6 release (for KDE
3.5.x) which doesn't need to be Oxygen styled, as Oxygen is intended
for KDE 4.  Join the #amarok irc channel if you're interested, and
talk to 'markey'.</p>

<p><b>Editorial aside:</b> I'm glad that so many people are showing
interest in KDE 4's development, but please try to provide
constructive feedback to help improve KDE 4.  Many of the developers
read the comments on the dot and implement things that users request
if they are well-reasoned.  For example, Peter Penz implemented the
Tree View in Dolphin, and Rafael Fernández López made changes to the
Job Progress Manager based on your constructive feedback.  Your
feedback is very welcome, but as last week's article has shown, when
the comments get out of hand, it becomes harder to sift through them
for the constructive ones.  On the flip side, that article
absolutely demolished the previous dot.kde.org comment records.
Hopefully we can break those records again one day as the interest
in KDE 4 grows.  Until next week...</p>



