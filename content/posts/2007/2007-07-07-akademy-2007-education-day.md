---
title: "aKademy 2007: Education Day"
date:    2007-07-07
authors:
  - "dcraen"
slug:    akademy-2007-education-day
comments:
  - subject: "Wrong link to FreeNX"
    date: 2007-07-07
    body: "the correct link should read: http://freenx.berlios.de\n"
    author: "Terje"
  - subject: "Unfortunately missed it"
    date: 2007-07-07
    body: "Thanks a lot for the summary!\n\nI was really torn between attending the Edu day and attenting the BoF and tutorial sessions going on in the second seminar room.\n\nNot that I would regret attending them, but since I am from a family of teachers I have quite some interest in advanced of educational Free Software.\n\nSoftware for deployment in schools and for teachers and students to use at home should definitely not be under tight control by only a few companies.\n"
    author: "Kevin Krammer"
  - subject: "Thumbs up!"
    date: 2007-07-07
    body: "You folks rock! I am very glad to read that the KDE Edu project is moving along so beautifully. I am definitely looking forward to KDE 4 with the new Edu package.\n\nSchoolNet Namibia (http://www.schoolnet.na) so far has hooked up 300+ schools to Linux, KDE and the Internet in this country. So the next major release of KDE is of especial interest here.\n\nAssuming that Mario Fux did the NX integration with LTSP, I would be highly interested in hearing how exactly he did it. So Mario, if you read this and are inclined so, please get in touch with me. ;-)"
    author: "Uwe Thiem"
  - subject: "Re: Thumbs up!"
    date: 2007-07-09
    body: "Sorry, I did not integrate NX with LTSP. But you'll find something in the german Skolelinux wiki [1].\n\n[1] http://wiki.skolelinux.de"
    author: "Mario Fux"
  - subject: "OLPC XO"
    date: 2007-07-08
    body: "The OLPC XO operating system is based on Fedora, see http://fedoraproject.org/wiki/OlpcFedoraPartnership . This also means the regular Fedora KDE packages will most likely run on the system, and if not, a simple rebuild will be enough.\n\nIt should also be possible to roll a hybrid of the Fedora KDE and OLPC spins targeted at the OLPC XO hardware."
    author: "Kevin Kofler"
  - subject: "About classmate at Brazil"
    date: 2007-07-08
    body: "When you say \"north brazil\" you are talking about the Amazon Forest states.\n(Maranhao, Para, Amazonas, Rondonia, Roraima, Acre, Mato Grosso states)\nAre you guys shure about this info? Are you talking about North or NorthEast?\n\nThe experiences with Classmate are spread around the country.\n\nHere you can see a experience at SouthWest Brazil, Sao Paulo state\n(have almost the size of Germany) at Bradesco Foundation.\n\nhttp://www.slideshare.net/apiscite/experiencias-classmate-en-brasil\n(there are 39 slides, almost all are photos, really nice)\n\n\nPirai city at Rio de Janeiro state acquired 400 classmates. \n(SouthEast Brazil) 24 K habitants. "
    author: "Ventura"
  - subject: "Debian 3.2.2?"
    date: 2007-07-08
    body: "Since when did systems run on Debian 3.2.2? Debian 3.0, 3.1 and 4.0 I know."
    author: "Andrew Donnellan"
  - subject: "Re: Debian 3.2.2?"
    date: 2007-07-09
    body: "Morning\n\nThat's a mistake. The system was first run on Debian 3.1 with KDE 3.3.2 or something like that and is now migrating to Debian 4.0."
    author: "Mario Fux"
  - subject: "Re: Debian 3.2.2?"
    date: 2007-07-09
    body: "Morning\n\nThat's a mistake. The system was first run on Debian 3.1 with KDE 3.3.2 or something like that and is now migrating to Debian 4.0."
    author: "Mario Fux"
  - subject: "Videos?"
    date: 2007-07-13
    body: "That's for the write-up.  The topics sound all very interesting.  I was just wondering if any video's were taken of the education day talks as I'd love to watch them but can't seem to find any with the main videos."
    author: "Philip"
---
The long-anticipated <a href="http://akademy2007.kde.org/codingmarathon/schoolday.php">Education Day at aKademy 2007</a> opened at 9 am on Monday, 3rd July with an introduction by Anne-Marie Mahfouf, maintainer of the KDE-Edu suite of educational applications. The main aim of the day was to unite the many different diverse consumers of the award-winning assortment of software, such as teachers, system integrators, and students, and to share ideas, success stories and best practice. With such a concentration of interested parties, the organisers were strongly optimistic that the event could provide a useful discussion venue for the coming year of educational developments. Read on for a report of the day.



<!--break-->
<p>The day was primarily organised by Anne-Marie Mahfouf and Inge Wallin, both of <a href="http://kde.org/">KDE</a>, and sponsored by the Netherlands-based organisation, <a href="http://www.codeyard.net/index.php?lang=en">CodeYard</a>. Sebastian Kugler of CodeYard explained their interest in the event: "Our goal is to create a community around Free Software in education and thereby convey the ideas, workflows and principles of Free Software development to education, with a long-term vision of raising awareness of Free Software in education and breeding a new wave of Free Software people".</p>

<a href="http://static.kdenews.org/dannya/akademy_edu_audience.jpg"><img src="http://static.kdenews.org/dannya/akademy_edu_audience_small.jpg" alt="Good attendance at the Education Day" title="Good attendance at the Education Day"></a>

<br />
<h3>KDE@School (using FreeNX)</h3>

<p>Mario Fux introduced his area of expertise, integrating <a href="http://freenx.berlios.de/">FreeNX</a> (used for thin-client systems) in Swiss schools. The focus of the talk was Mario's experience in one school, the Ried-Brig school in southern Switzerland. The full lifecyle of the project was considered, starting with the proposal, following on to the implementation, and then an evaluation of both the technical performance and user acceptance of the system.</p>

<p>The case study presented consists of 8 classes situated in a relatively conservative region of Switzerland. The school has limited resources, which means that they must manage a long technology cycle, with the same computers many years old and becoming dated. This presented an opening of opportunity for Mario. Mario proposed to use FreeNX on a Linux platform - with the proposal readily accepted, Mario performed the installation free of charge. The cost for an application server was only 1000 euro, and came with 2GB ram running on Debian 3.2.2.</p>

<p>Naturally, such a "radical" departure from de facto technology procurement standard imposed some rigourous conditions. Aside from regular concerns, the system needed to fully interoperate with existing web applications, printers, and provide for their word processing and other office needs. After a small period of adjustment, teachers rapidly became comfortable with the system, particularly valuing it for its stability over previous products. The project gathered a lot of feedback, which has played a significant role in the further development of the system, with many teachers lending their participation to the research project.</p>

<p>The project is still fairly new (now in the school for 2 years). In 2006, they bought 2 new servers (more than doubling the capacity of the system). Mario is looking forward to the prospect of installing KDE 4 system-wide, and the associated benefits this will bring to the project. As part of the evaluation stage, questionnaires were distributed to classes: an interesting discovery was the extreme popularity of the "Potato Guy" software, which produced stacks of output on the colour laser printer until it was replaced by a monochrome model.</p>

<br />
<h3>Intel Classmate PC</h3>

<p>Helio Chissini de Castro introduced the <a href="http://www.intel.com/intel/worldahead/classmatepc/">Classmate PC</a> from micro-processor producer Intel. The Classmate PC is a small, rugged computer in a laptop-style form factor. Designed for beginning computer users in developing countries, the Classmate has 256 mb of RAM, and has a choice of 3 'distributions' - one based on Windows, the other two exclusively featuring a KDE-based desktop.</p>

<p>The discovery of an <a href="http://www.laptop.org/">OLPC XO</a> computer in the room prompted an short (yet unscheduled) comparison session, where differences in the two competing projects were noted. Intel doesn't believe that the "one laptop per child" model is appropriate for its target markets (middle stage developing countries), since many homes already have a standard, family computer. OLPC also takes a more holistic, system-oriented approach, building both the hardware and software, whereas Intel is less concerned with the software side, leaving this to established software vendors such as <a href="http://www.mandriva.com/">Mandriva</a>, where Helio is employed to work on such integration tasks. A general consensus was that the Intel platform was more "open", and could possibly allow a greater variety of tasks to be performed, important if the device is to be a companion throughout the educational stages of a student.</p>

<a href="http://static.kdenews.org/dannya/akademy_edu_devices.jpg"><img src="http://static.kdenews.org/dannya/akademy_edu_devices_small.jpg" alt="OLPC XO vs. Classmate PC" title="OLPC XO vs. Classmate PC"></a>

<p>The case study presented by Helio covered his experience in North Brazil, where Classmate PC's with Mandriva, KDE 3.5.4 and OpenOffice are currently engaged in trials. A question was raised regarding whether children with no computer at home will be able to understand this new technology? The response was that children learn very quickly, and that they already are used to working with different user interfaces, like those found on games consoles and mobile phones.</p>

<br />
<h3>mEDUXa is in Canary Islands schools... where do we go from here?</h3>
<p>After lunch, Agustín Benito Bethencourt covered the recent work of the <a href="http://www.grupocpd.com/archivos_documentos/info_meduxa/meduxa_project_released/">mEDUXa</a> project in the Canary Islands, Spain. mEDUXa is a large-scale, government-sponsored initiative to provide a technology-enhanced education to students. So far, over 35,000 computers have been distributed to 1,100 schools, with a potential userbase of 325,000 users. Furthermore, Agustín expects these numbers to double by 2009.</p>

<p>The mEDUXa project started in 2005 with a choice between GNOME and KDE. KDE was chosen as the best solution in the evaluation stage, partly due to the lower resource requirements and the excellent KIOSK framework, which allows administrators to effectively lock down computers to prevent their misuse and reduce maintenance needs. The system is based on the KDE-derivative of Ubuntu, Kubuntu. The project has 4 main elements: the network, desktop clients, school servers and central servers, which aid administration and system upgrades.</p>

<p>Simplicity is a primary goal of the project, with simplified desktop profiles provided to different student strata, including the important 6-12 age group. A large advantage is that the KDE-based desktop can be comfortably provided in several different languages, which greatly aides language teaching and immigrant students (English, French, German and Arabic), and distinguishes the system from proprietary competitors.</p>

<p>Agustín announces that a wiki containing project documentation can be found at <a href="http://www.hexperides.org/">hexperides.org</a>, and many of the custom system modifications can be found on <a href="https://launchpad.net/~hexperides/">Launchpad</a>.</p>

<br />
<h3>Skolelinux as a platform for Free Software in schools</h3>

<p>Knut Yrvin proceeded to talk about the <a href="http://www.skolelinux.no/en/">Skolelinux</a> distribution and its implementation in schools in Europe. More than 450 schools are currently using Skolelinux, which started in 2001 and is most concentrated in the Scandinavian region. Only 5% of users are native English speakers, which means that 95% of students in these schools cannot easily use computers which are lingually-inflexible.</p>

<p>Whilst all students are trained in office productivity applications, this misses out the huge potential of students to creatively express themselves. Currently, students are treated like consumers, whilst they should learn the basis of technology, and how it can be used to create the future.</p>

<p>Skolelinux focuses on the most important requirements schools have of any system: cost, existing hardware, and language. With the Windows platform, many schools found that the cost of software licensing was preventing them from procuring new hardware. With only basic administrative systems in place, one technician was required to physically visit a computer for many common administrative tasks. The solution was to use graphical clients with a more centralised implementation to allow for upgrades and software installation. Using Skolelinux, schools have been able to achieve cost savings of up to 40% over traditional rollouts, with a greatly reduced technical burden: a single technician in a school of 4,000 students can perform all necessary tasks in 2-4 hours a week.</p>

<br />
<h3>GCompris (presentation and OLPC integration)</h3>

<p>Bruno Coudoin presented <a href="http://gcompris.net/-en-">GCompris</a>, the award-winning activities environment with a specialised interface for young children. GCompris is currently translated into 60 languages (including audio voice samples in many cases), and has recently seen a revamped and easier to use menu layout adopted after consultation with users.</p>

<p>Highlights of GCompris include a basic word processing application, a drawing and animation creation suite, and a locked down local chat application for the classroom, where children can practise communication and typing skills towards the same end. Sound is an essential element in GCompris, and the audience universally accepted that audio interaction and notifications need to be much more thoroughly integrated in existing and future applications. A new direction of development is adding administrative functionality directly in GCompris, where classes of users can be set up, with tailored activities to suit their current stage of academic development.</p>

<p>Bruno was the bearer of an OLPC XO machine, and discussed the current issues regarding the inclusion of GCompris on the machine. One of the main issues was the size of GCompris, which through the extensive use of graphics and audio has bloomed up to 100 mb - a hefty payload for a machine with extremely limited memory resources. The other significant problem is the integration in the Sugar interface paradigm. <a href="http://wiki.laptop.org/go/Category:Sugar">Sugar</a>, which is the natively-developed GUI for the OLPC, uses the concept of a "task frame" for each "activity" (these activities are generally applications in the traditional sense). For GCompris to be fully integrated, its activities would also need to inhabit this area. However, this does not suit the concept of GCompris as a standalone environment, and in any case, GCompris has far too many activities to fit into this task frame, which appears to be visually restricted to a maximum of around 15 icon buttons.</p>

<br />
<h3>Lightning Talks</h3>

<p>After a short break, lightning talks covering two applications which have seen rapid development over the past year were given. These applications were <a href="http://edu.kde.org/marble/">Marble</a>, and KAlgebra.</p>

<p>Torsten Rahn started the session, presenting the concept and wide promise of Marble, the desktop globe application, before eagerly moving into a live demonstration of Marble.</p>

<p>Torsten describes the various areas geographic displays have been historically used in KDE, from KWorldWatch to time zone selection in KControl. Now, Torsten would like to greatly scale up this use of geo-integration, in places like KAddressbook and other KDE-PIM applications, and many other areas throughout KDE. With a quick masterclass in creating a trivial, 5 minute application in Qt Designer using the Marble widget, the audience immediately more attentive. The ease displayed in the creation of this demo brought Torsten's ideas from theoretical and distant to tangible and excitably close.</p>

<p>Torsten described the support for emerging de facto standards such as Google Earth's KML placemark format, and his expectation of great results from the three Summer of Code projects assigned to Marble, <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=58AE7448FBEEEAE0">implementing GPS support</a> (with the student working on this project making an important breakthrough programming through the intervals in the talks!) and a <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=DCE4DBD4A0509DC7">2 dimensional projection</a> to complement the existing 3d model, which will aid integration into other applications with easier manipulation needs.</p>

<p>Next, Aleix Pol Gonzalez introduced KAlgebra, a MathML-based calculator and plotter (2d and 3d) for algebraic functions. KAlgebra provides a simple calculator for advanced calculations so that students can calculate and script common functions (such as calculating the perimeter of an object) in a console, with support for saving popular functions into a dictionary for speedy regular usage.</p>

<a href="http://static.kdenews.org/dannya/akademy_edu_apol.jpg"><img src="http://static.kdenews.org/dannya/akademy_edu_apol_small.jpg" alt="Lightning talks" title="Lightning talks"></a>

<br />
<h3>Panel discussion</h3>

<p>With all in attendence interested in varying capacity in education, a quick poll was conducted to gauge the background of the crowd. Around twelve people were developers of educational software, four were system integrators, and one was a teacher.</p>

<p>Inge Wallin opened the panel discussion with the question "What educational needs are we not meeting today?" As a question familiar to many attendees, Inge stated his own priorities of teacher control over the content and operation of computers in their class, and the pressing need for data, especially geographically and lingually localised data sets, and an infrastructure for uploading and downloading this data.</p>

<p>Mario Fux echoed earlier discussions by asking for wider and more appropriate usage of sound, with <a href="http://phonon.kde.org/">Phonon</a> being an ideal technology for KDE 4 applications to integrate these interaction elements. Mario also stated the need for a better dialogue between developers of educational applications and their users, who are a mix of teachers, system integrators and students.</p>

<p>Anne Østergaard (a board member of the GNOME Foundation) was interested in Marketing, and suggested an educational software portal online, and a promotion of links with the media, in particular documentary makers and producers of teaching training videos. Knut Yrvin would like to see a simplified desktop interface developed, similar to the Sugar effort of the OLPC, and a continued emphasis on lower resource requirements, possible through the use of WebKit as a web rendering component over the more heavyweight Gecko engine used by Firefox, and the substitution of OpenOffice.org with <a href="http://www.koffice.org/">KOffice</a>.</p>

<p>The day ended at around 5:35 pm with attendees having a better understanding of both the current state of the Free education offering and the real-world needs of our users.</p>


