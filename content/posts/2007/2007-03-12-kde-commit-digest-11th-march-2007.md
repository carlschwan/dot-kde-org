---
title: "KDE Commit-Digest for 11th March 2007"
date:    2007-03-12
authors:
  - "dallen"
slug:    kde-commit-digest-11th-march-2007
comments:
  - subject: "Windows Debug Build Download"
    date: 2007-03-12
    body: "Where can i download the Windows Debug Build, with the Windows Installer\n\nLooks like this is a fine way to try Kde 4"
    author: "lukilak@we"
  - subject: "Re: Windows Debug Build Download"
    date: 2007-03-12
    body: "The installer can be found at http://websvn.kde.org/trunk/kdesupport/kdewin32/installer/ but I'm not sure how usable it is as yet. It wouldn't hurt to give it a try though."
    author: "Matt Williams"
  - subject: "Re: Windows Debug Build Download"
    date: 2007-03-12
    body: "Can you build an .exe for the installer ? or do i need a installer for the installer :)"
    author: "ch"
  - subject: "Re: Windows Debug Build Download"
    date: 2007-03-12
    body: "you can download the installer from here\nhttp://82.149.170.66/kde-windows/installer\n\nactually it takes me a lot of time to find it, after searching in some obscur mailing list\n\nfriendly"
    author: "djouallah mimoune"
  - subject: "Re: Windows Debug Build Download"
    date: 2007-03-15
    body: "The installer in the recent state is mainly for win developers and was announced on the kde-windows mailinglist. See http://mail.kde.org/mailman/listinfo/kde-windows for more informations. \n\nEveryone interested in KDE on windows is welcome to try out and contribute for example packaging service and/or hacking the sources to make the installer more usefull. \n\nThe installer is Qt 4 based and is compiled using a static QtCore, QtGui and QtNetwork library, which means there is only one executable required. \n\n\n\n\n\n"
    author: "Ralf Habacker"
  - subject: "Re: Windows Debug Build Download"
    date: 2007-03-12
    body: "There are currently no actual debug libs for win32 available. We had no time to create them because there are some open issues but I hope to get this working for the kde alpha builds.\nBut you can compile them by your own when you've some time :)"
    author: "ChristianEhrlicher"
  - subject: "KDE4 feature freeze?"
    date: 2007-03-12
    body: "\"So, if everything goes well, the first inclusion of Marble (already very stable) into KDE-Edu should happen around April 2nd 2007.\nThis would still be right in time for a possible feature freeze on May 2nd and offer enough time to solve remaining issues and take delays into account.\"\n\nSo will KDE4 be on feature freeze around may? that would make a final release around december right? or is he talking about Marble feature freeze?"
    author: "Patcito"
  - subject: "Re: KDE4 feature freeze?"
    date: 2007-03-12
    body: "There is no final decision yet. The last I saw was talk about a release in oktober..."
    author: "superstoned"
  - subject: "Re: KDE4 feature freeze?"
    date: 2007-03-13
    body: "It's spelled \"october\", though I don't have an idea if you're a German, or simply an KDE addict :)."
    author: "zonk"
  - subject: "Re: KDE4 feature freeze?"
    date: 2007-03-12
    body: "> This would still be right in time for a possible feature \n> freeze on May 2nd and offer enough time to solve remaining \n> issues and take delays into account.\"\n\nTranslation: If there *was* a KDE4 feature freeze by start of May (which would be pretty early, however no decision has been made yet) then it *would* still be right in time.\n\n> So will KDE4 be on feature freeze around may? \n\nNo decision has been made yet. And I wouldn't be surprised if the schedule  would look completely different once a decision has been made.\nI'm just saying that at the current point of time it *looks* pretty safe to me to *assume* that there is enough time to consider inclusion of Marble into KDE-EDU.\n"
    author: "Torsten Rahn"
  - subject: "Re: KDE4 feature freeze?"
    date: 2007-03-12
    body: "ok thanx a lot for the clarification and Marble of course. Can't wait for the merged version with kgeography :)"
    author: "Patcito"
  - subject: "don't freeze!"
    date: 2007-03-12
    body: "Go for the features! Make KDE4.0 as good as else KDE4.1 would be, incorporating all the eye candy to the max. Why? Because with KDE4, a big marketing iniative will roll, not only from the KDE PR team but also from the distro makers which want so sell their products. Possible consequence: n00bs coming to kde, getting a product with less eye candy than possible, and half a year later, they have to update to 4.1 to get the real revolutionary things.\nIMHO, KDE4 should be 'da b0mb rel1Z' to hit it big on first try. Make a 3.98 stable release, or handle the feature freeze as flexible as possible (which equals no feature freeze)...\nOr what do you think?"
    author: "eMPee"
  - subject: "Re: don't freeze!"
    date: 2007-03-12
    body: "The worst thing, and why this extreme position is not going to prevail, is that users may get the idea that KDE is pretty and feature rich, but unstable.\n\nThat would kill a reputation KDE has so far. So people will aim for stability over candy, and that's why a period of freeze is needed, and that's why it's going to happen.\n\nTo me personally, I believe KDE 4.0 final, will be starting the wait for applications to make use of the new techs, and 4.1 to come to get it entirely polished. Yet, that's what you get with .0 versions in KDE world. :)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: don't freeze!"
    date: 2007-03-12
    body: "I personally would rather see KDE 4.0 get the base right. Then I would put emphasis on usability of the UI and regressions. Slowly bleed features in every 6 months after that. \n\nIt is important to not \"wait until its absolutely perfect\". Then we would be talking about a vista like development cycle. Nothing will ever be perfect. It also makes more sense to release early and often and get back to the 6 month cycle once the core has matured. "
    author: "Ryan"
  - subject: "Re: don't freeze!"
    date: 2007-03-13
    body: "> It is important to not \"wait until its absolutely perfect\". Then we would be talking about a vista like development cycle.\n\nSo Vista is \"absolutely perfect\"? HA!"
    author: "Jon"
  - subject: "Re: don't freeze!"
    date: 2007-03-13
    body: "Obviously it doesn't work :D"
    author: "Ryan"
  - subject: "random digest"
    date: 2007-03-12
    body: "Man, I love the Random Digest feature on your site. :)"
    author: "X11 User"
  - subject: "kpilot is good but what about phones?"
    date: 2007-03-12
    body: "Are there any plans in KDE4 to make it easier for those who have cell phones to access such devices? I was recently playing around with kandy (KDE3) and it um, seems a bit limited and not particularly user friendly."
    author: "taurnil"
  - subject: "Re: kpilot is good but what about phones?"
    date: 2007-03-12
    body: "http://www.kmobiletools.org/ looks like a more updated project to access to your cell phone.\nBut... \"I must anyway tell you i'm quite disappointed. After all these years, kmobiletools still is a one-man project. It's shameful in my point of view that if someone is busy, for study, work, real life, or (why not) a girlfriend, the entire project freezes.\".\nSo, if you can help...\n"
    author: "Mobile"
  - subject: "Re: kpilot is good but what about phones?"
    date: 2007-03-12
    body: "Hey thanks for the link. Gave it a whirl and it does seem a bit better than kandy."
    author: "taurnil"
  - subject: "Re: kpilot is good but what about phones?"
    date: 2007-03-13
    body: "isn't opensync going to help here? and probably akonadi? some more cooperation between this kind of projects (there is kpilot as well) would be cool..."
    author: "superstoned"
  - subject: "not really porting"
    date: 2007-03-12
    body: "Its not really porting whats going on in Amarok 2.0 anymore, its features and refactoring. We're going to leave getting rid of the Q3Support stuff until after the dust has settled on the refactorings."
    author: "Ian Monroe"
  - subject: "Re: not really porting"
    date: 2007-03-12
    body: "Right, I have noticed a shift in the work being done on Amarok. I decided to call it \"porting\" for this week (though further qualifying it with the comment on the music store work), with a planned change to another descriptive word soon. I guess \"soon\" has now become \"next week\" ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Link\u00f6ping - the center of the earth"
    date: 2007-03-12
    body: "Cool that the screenshot is centered around Link\u00f6ping!  I recommend you all a visit (I might even show you around a bit, as dfaure found out)."
    author: "Inge Wallin"
  - subject: "KPilot"
    date: 2007-03-12
    body: "KPilot? Wasn't that going to be superseeded by some general syncing mechanism? Anyway, I'd love to be finally able to ditch Palm Desktop for a good application running natively under KDE! Go for it, KPilot team!"
    author: "palm user"
  - subject: "Re: KPilot"
    date: 2007-03-13
    body: "that's what I thought too. I think  it's OpenSync, maybe the new kpilot is based on it, I'm too tired to check it out right now though :)"
    author: "Patcito"
  - subject: "Re: KPilot"
    date: 2007-03-13
    body: "The new OpenSync frontend is KitchenSync. I tried a svn-version of it with OpenSync 0.21 and it works (tested on KDE 3.5.5 + kitchensync-svn + OpenSync 0.21 + Palm T|X).\nWith OpenSync 0.22 one missing feature (pictures in contacts) will be added which I need/want."
    author: "Carsten Niehaus"
  - subject: "Re: KPilot"
    date: 2007-03-14
    body: "> KPilot? Wasn't that going to be superseeded by some general syncing mechanism?\n\nIMHO it should have been \"Akonadi\" but, as for Plasma, I haven't heard/seen much about it until now...\n\nhttp://pim.kde.org/akonadi/\nhttp://conference2006.kde.org/conference/talks/9.php\n(with slides & ogm video)\n\nThat's a pity :  My Sys-Op is rather OSS-Friendly (well, the last MS Exchange License's Bills we received did a lot, I must admit :) and it's certainly the last point we're not confident enough about : providing a stable and feature-full syncing utility inside a Linux distro !!\n\nI won't cry any longer & I prefer to cheer you all up KDE guys : Thanks a lot !\n\n\nYojik77"
    author: "Yojik77"
---
In <a href="http://commit-digest.org/issues/2007-03-11/">this week's KDE Commit-Digest</a>: The <a href="http://oxygen-icons.org/">Oxygen</a> iconset is moved from playground to kdelibs, changes made throughout KDE to support the new icon names specification. The Crystal iconset is moved from kdelibs to its kdeartwork retirement home. More work on the Oxygen widget style. Security fixes in <a href="http://ktorrent.org/">KTorrent</a>. Initial work on "uninstall" functionality for the <a href="http://commit-digest.org/issues/2007-01-14/#1">KDE Windows installation utility</a>. New "Snowish" theme for the <a href="http://alas.matf.bg.ac.yu/~mr02014/english/main.html">Kamion</a> user information migration utility. Continued graphics improvements across kdegames. Improved wireless network encryption support in <a href="http://solid.kde.org/">Solid</a>. Further work on the <a href="http://amarok.kde.org/">Amarok</a> 2.0 porting, with particular attention to the Music Store integration elements. <a href="http://pim.kde.org/components/kpilot.php">KPilot</a> is to make a surprise return for the KDE 4.0 release.
<!--break-->
