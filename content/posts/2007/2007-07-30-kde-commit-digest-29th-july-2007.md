---
title: "KDE Commit-Digest for 29th July 2007"
date:    2007-07-30
authors:
  - "dallen"
slug:    kde-commit-digest-29th-july-2007
comments:
  - subject: "New Font Manager"
    date: 2007-07-30
    body: "I like the way fonts are grouped, and all the features shown, but a better preview would be nice. The KDE3 version had a nice preview over a range of sizes, so you could see the detail (which is important for fonts). I prefer that.\n\nI also prefer the KControl interface over System Settings, but that's another issue. (There's too much blank space, and I hate having to navigate back to the overview all the time.)"
    author: "Soap"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "> I also prefer the KControl interface over System Settings\n\n+1; where I can report this? - noticing that in a discussion, I was pointed to kde-look.org as a place for determining the future of KDE, but that really does not look like a place where one can \"slap\" the people responsible for promoting this piece of crap\n\nPLEASE do not feel offended by the words \"piece of crap\" - this is only my opinion on SS which is based on the usability (as Soap says) and on the fact that I do not like throwing away good things just to make them similar to those in the Windows world ... but it DOES NOT mean that I am spitting on the work put in SS; when there are users who like SS, it is a good job doing SS, it is all about choice, but please do not take away my choice from me just to satisfy those who like it the Windows way, because if they want it that way, they can just go and buy Windows, but I have nowhere to go ... and do not tell me I \"have the choice\" to take kcontrol codebase and maintain it myself, so that it won't die in the shadow of SS - this is beyond my skills, and I would have to leave other projects I invest my time in, which is a bad choice"
    author: "kavol"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "Who codes decide. So, stop complaining and start coding."
    author: "Vide"
  - subject: "A bit simplistic"
    date: 2007-07-30
    body: "Though I agree that the coder has the final word and that every people involved in a Free software project should get praise for his/her work, I'm happy that most coders actually take into account inputs from casual users like myself and do not reject my opinion for the single reason that I do not (and cannot) code.\nBecause free software would not be as good and valuable as it is if they did. Fortunately, most are too clever to act in such stupid way.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: New Font Manager"
    date: 2007-08-02
    body: "Don't believe this dodge.  If you write the code, they will find some other excuse (NIH)."
    author: "James Richard Tyrer"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "+1, me too!\n\nThe better way is to mantain both, KControl (my choice) and System Settings (newbie users?). But it will make some confusion, so why not a fusion?\ni.e.: a left view with categories (like Kcontrol), a center view vith category icons / component view (like System Settings)"
    author: "Minkiux"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "Maintaining both is just more work.  System Settings is better code than kcontrol at the moment, so it will be used. However, I do agree that it is not quite optimal.  The problem is that we are rapidly approaching 4.0 and we need to ship a control panel that functions.  Kcontrol does not function at all right now; System Settings does.\n\nFor 4.1, if someone can create a system that embeds the KCM modules into an even better system, it would most likely get shipped, but only if it was better than System Settings."
    author: "Troy Unrau"
  - subject: "Re: New Font Manager , old bullshit code ?"
    date: 2007-07-30
    body: "the position \"the old was bad code\" has been used many many times on kde code , as i read kde-core-devel,.. was kde 3.5.x really this crappy code ??????\n\nit looks like everything was crap code , from konqueror (unmaintable) , kcontrol, kdedesktop, and so on...\n\ngreetings."
    author: "srettttt"
  - subject: "Re: New Font Manager , old bullshit code ?"
    date: 2007-07-30
    body: "kcontrol wasn't crappy code. it was just an interface we, or rather the software, outgrew.\n\nthe reason it didn't work in trunk/ is because nobody fixed it after the kcm's .desktop files were moved from applnk/ (now dead) to services/. it's probably only an hour's work or so (if that; could be a 15 minute fix even) but nobody did it because it had been decided a long while ago (malaga) that based on the interface, kcontrol would go.\n\nso it wasn't a code quality issue."
    author: "Aaron J. Seigo"
  - subject: "Re: New Font Manager , old bullshit code ?"
    date: 2007-07-31
    body: "well, it looks like circular reasons :-) - kcontrol is not included because it is unmaintained, and it is unmaintained because it was decided not to include it ...\n\noh wait, you say \"based on the interface\"? - please, can you point me to some serious discussion? all I've seen on the web so far is like \"ah, there are shiny icons in the SS, not that ugly - mostly textual - tree like in kcontrol\" or \"(k)Ubuntu does it the right way, Ubuntu rulezz\" but nobody really took care about how long it takes to find/set up something, how many clicks and mouse mileage it takes or how is the keyboard navigation ..."
    author: "kavol"
  - subject: "Re: New Font Manager , old bullshit code ?"
    date: 2007-07-31
    body: "\"well, it looks like circular reasons :-) - kcontrol is not included because it is unmaintained, and it is unmaintained because it was decided not to include it ...\"\n\nNo: \"kcontrol is not included because it is unmaintained, and it is unmaintained because no one stepped up to maintain it\".  The fact that no one stepped up to maintain it has nothing to do with the fact that KControl wasn't going to be included in KDE4.0, as that decision hadn't been made yet!"
    author: "Anon"
  - subject: "Re: New Font Manager , old bullshit code ?"
    date: 2007-07-31
    body: "And two posts up we have Aseigo saying the exact oposite, that its a small fix but no one stepped up because it was already decided that Kcontrol would go. \n\nDosn't bother me though, I'm just curious about what actually happened, the interface isn't too important to me just providing SS has the same amount of power as Kcontrol, \n\nAlthough I would like to see it renamed KSystemSettings. The K always tells me that a piece of software will intergrate into my desktop and won't drag many dependencies along, I'd like to see it continue. "
    author: "Ben"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "I agree"
    author: "Minkiux"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "yeah i like the old way better , +1 :-)\n\nsearchable and hierarchic and grouped , what else so you want"
    author: "srettttt"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "> just to make them similar to those in the Windows world\n\nactually, SS is making it more like the mac, not windows. (go look at each of those systems and compare =). i actually know for a fact that the person who started SS had macos in mind.\n\nso ... makes your comment a bit odd ;) but yeah, i understand what you are saying. so here's a solution:\n\nsomeone can take kcontrol, fix the applnk/->services/ issue, and maintain it as an option in extragear. how's that?\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: New Font Manager"
    date: 2007-07-31
    body: "> actually, SS is making it more like the mac, not windows\n\nwell, nothing odd and nothing strange - we all know how Microsoft loves to copy ;-)\n\nI've seen such an interface for the first time on Windows XP and I guess it is the same for majority of people, since Microsoft has more market share than all the rest together ... so there is a pretty chance that someone voting for SS over kcontrol is inspired by Windows ... never mind\n\n\"such an interface\" - I mean icons behind some strange categories; I am not good at guessing what the heck had the author of the icon in mind, so reading the text is much faster for me ... and reading the text is much faster when it is line under line as in kcontrol and not scattered between the icons like in SS (not talking about the number of clicks needed to browse and the strange division General/Advanced - what is more \"advanced\" in configuring login screen then in configuring printers, for example?)\n\n> i actually know for a fact that the person who started SS had macos in mind.\n\nok, then he can go and simply buy Mac ;-)\n\n> someone can take kcontrol, fix the applnk/->services/ issue,\n> and maintain it as an option in extragear. how's that?\n\nme not, I am not a programmer\n- however, you say fixing it is about an hour of work so I am willing to pay one hour wage for this if someone is interested (where to put such a bid?)"
    author: "kavol"
  - subject: "Re: New Font Manager"
    date: 2007-08-01
    body: "It's just a control panel.\n\nI swear, people are so afraid of change. What with this and Konqueror/Dolphin, if everything stays the same - there's no progress. As long as System Settings has all the features of Kcontrol (and there's no reason why it wouldn't) there's no reason to complain."
    author: "Parapara"
  - subject: "Re: New Font Manager"
    date: 2007-08-01
    body: "> if everything stays the same - there's no progress\n\nof course ... but then there is the slight difference between \"progress\" and \"decay\"\n\n> As long as System Settings has all the features of Kcontrol\n> (and there's no reason why it wouldn't) there's no reason to complain.\n\nbut it has not - it cannot be browsed so easily, and it is harder to read the names of the modules one-after-another (actually, the names do not fit below the icons, so I see things like \"Size & O...\", \"Abo...\", but I consider it a bug which is going to be fixed before final)\n\np.s. do you consume only bread & water or do you have wider choice? - after all, it's just a meal, no need to complain ..."
    author: "kavol"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "> I also prefer the KControl interface over System Settings\n\nAs do I. I find System Settings to be lacking in one important area: and that is search. In KControl, I was able to search for a setting if I had no idea where it would turn up. I found that a *very* usefull feature, as it is inevitable that with the amount configurability that KDE provides (and wich we all love, don't we?), choices will have to be made where to put what setting. These choices will not always be obvious for every user, no matter what grouping and ordering you come up with. Searching for options was a very good way of dealing with that, and that is something I can not see in the System Settings approch. Back to trial and error, I fear..."
    author: "Andr\u00e9"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "System settings does have a search field in KDE 4."
    author: "Troy Unrau"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "that is a showstopper for SS i think."
    author: "srettttt"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "Thumbs up to Craig for his wonderful work on this one. A proper font management app was/is fairly high up on the list of things lacking in Linux desktops compared to the proprietary competition for creative/artist type users, and it looks like KDE 4 will finally close that gap. Rock on!"
    author: "Eike Hein"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "Agreed, I find quite amusing that the old interface is in fact better than the new interface, as it show the fonts with different size, which is an important feature.."
    author: "renoX"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "The large preview is still available, its just not shown by default. An option in the \"Settings\" menu controls whether it is shown or not."
    author: "Craig Drummond"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "Great. Love the new interface!\nThe only thing I would like to change is the order of the items in the \"toolbar\":\nThe filter bar (\"Type here to filter on family\") first, then the ComboBox and finally the Settings Button, something like this:\n\n[_____Filter Bar______] ( ComboBox |v) [ Settings ]\n\nDon't know why, just fint it more logical that way. (Specially with the preview enabled, I would like to have the filter bar above the list and not over the preview).\nThat's just my personal opinion, don't know what's best usability-wise.\n\nOnce again, great work, keep it up!"
    author: "Lans"
  - subject: "Re: New Font Manager"
    date: 2007-07-30
    body: "even if the ordering is kept, a little whitespace between the widgets would go a long way =)"
    author: "Aaron J. Seigo"
  - subject: "Re: New Font Manager"
    date: 2007-07-31
    body: "Great that the font preview is still there. I just don't get the position of the Settings in the menu bar. It has nothing to do with the filtering options that are there as well. Maybe the button should go somewhere around the Defaults button, or in the toolbar on top?\nAlso, maybe the title \"Install Preview Fonts\" isn't very clear. Unless you are actually installing previews of fonts (whatever that may be, time limited or incomplete in glyphs maybe?), I would say that \"Install & Preview Fonts\" (if you want to keep both terms) would be clearer."
    author: "Andre"
  - subject: "Re: New Font Manager"
    date: 2007-07-31
    body: "I'm no UI expert, as you can tell. But, I can't put the \"Settings\" button/menu next to the \"Defaults\" button as that's controlled via kcmshell, and I can't put it in the toolbar as that's \"System Settings\" toolbar. A KControl module can only place widgets between the top toolbar, and the bottom row of buttons.\n\n..but I'm more than willing to re-arrange things if possible.\n\nThe title is \"Install & Preview Fonts\" - I guess KTitleWidget doesn't like the '&'"
    author: "Craig Drummond"
  - subject: "Re: New Font Manager"
    date: 2007-07-31
    body: "Ah, ok, that clears it up. Still, it is a dilemma, as those locations would be far better for this, I think. Ah well, I guess someone will come up with a better and actually implementable suggestion. \nAbout the &: it is used for the shortcut key. Try && instead :-)\n\n"
    author: "Andre"
  - subject: "Re: New Font Manager"
    date: 2007-08-01
    body: "You can escape the \"&\". I believe with \"&&\"."
    author: "fast penguin"
  - subject: "Ehm?"
    date: 2007-07-30
    body: "The new oxygen style looks nice. But, those the tabs are going to end like that? The old oxygen tabs were far better (I know you told me to don't fuck whit the artist part).\n\nAnd, I have another question:\n\nWill the animation (like tab change) still be there?\n\nSorry for my poor English, see you"
    author: "Luis"
  - subject: "Re: Ehm?"
    date: 2007-07-30
    body: "Only the active tab is done, and I don't know exactly what Nuno has in mind. So don't be too worried yet. I'm pretty sure it'll end out great.\n\nAnimations will not be done in 4.0, simply because there isn't time. If tabs will have animation in the future I can't say."
    author: "Casper Boemann"
  - subject: "Re: Ehm?"
    date: 2007-07-30
    body: "Ok, thanks for the info. Nuno is awesome, so I won't worry :P\nSadly I liked the tab animation. But, all animation are gone? Like menu animations, hover animations, etc?\nIf they are, it's a bad new (but not important, I found them to be nice, but not a \"must have\"). \n\nI hope you and all developers keep doing this amazing work!\n\nSee you."
    author: "Luis"
  - subject: "Re: Ehm?"
    date: 2007-07-30
    body: "> Sadly I liked the tab animation\n\nThe tab animations had performance problems, especially with applications such as Konqueror where the content area of the tab is large.\n\nIt was an interesting idea, but unless they happen very quickly I think it will detract from the overall experience.\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Ehm?"
    date: 2007-07-30
    body: "But, what about the other animations?\nAnd, yes, In Konqueror it had some lag. You're possibly right.\n\nI wonder if those nuno mockups are what is on his mind:\n\nhttp://www.jarzebski.pl/read/nowy-styl-oxygen.so"
    author: "Luis"
  - subject: "Re: Ehm?"
    date: 2007-07-30
    body: "wow.. that's the coolest theme i've ever seen (from your link)... would be cool if there'll be such a theme/plasmoids when kde 4.0 is released!\n\nhm.. sad thing that the animations will be gone for 4.0 ... they were really cool.. and i for myself have never encountered any performance problems with them... but if it helps to make kde 4.0 stable it's ok for me ^^"
    author: "LordBernhard"
  - subject: "Re: Ehm?"
    date: 2007-07-30
    body: "yeah, I like the mockup better than the preview screenshot in the digest."
    author: "anonymouse"
  - subject: "Re: Ehm?"
    date: 2007-07-30
    body: "That's a really nice theme, something like that should the the default theme for KDE 4!"
    author: "x0"
  - subject: "Re: Ehm?"
    date: 2007-07-30
    body: "Hopefully KDE4 will have support for SVG based themes.\nSo the mockups creatos will be able to convert their art into themes more easily and we will have some really nice themes.\nDon't worry too much about KDE4 default theme. Remeber KDE 3.0 and see what people are using today and you will see that the default theme isn't a major one, or even that the default look of kde 3.5 isn't the same as KDE 4.0 :)"
    author: "Iuri Fiedoruk"
  - subject: "Is Ark gone from KDE 4.0?"
    date: 2007-07-30
    body: "Reading the commit changes from Henrique Pinto,\n\n\"* Removing ark from trunk/.\nThis version was basically very bug ridden and not maintainable.\nI have a rewrite almost ready in a branch, which will go back here when ready. Unfortunately, this will only happen for KDE 4.1, as the feature freeze is today and my code is not ready for it just yet.\"\n\nDoes this mean Ark wouldn't be available on KDE 4.0? I hope not, Ark is one of the program I used frequently. Maybe it can be put late as an exemption? I just don't think it's a good idea to leave it behind for KDE 4.0. I could just see people complaining that they can't open zip files on the brand new KDE"
    author: "angun33"
  - subject: "Re: Is Ark gone from KDE 4.0?"
    date: 2007-07-30
    body: "To quote Henrique Pinto:\n\"Yesterday, I was informed that the feature freeze would be today. As Ark was \nnot ready for that freeze yet, I removed it from trunk. Then, Dirk Mueller \ntold me that if had not done that, I would be allowed to substitute the \ncurrent Ark version with the new one I'm developing in a branch, but now I \ncouldn't do it anymore because it would be considered a new application.\n....\"\n\n\"We're not intransigent. I think we can let you bring it back, as long as \nyou do it soon.\n\nDo you think you can have Ark done for the KDE 4.0 final release? If you \nthink you can, that's all we need.\" -- Thiago Macieira\n\nIn short, it'll be back. I'm sure any extra help getting it whipped up into shape for KDE4 would be appreciated tho."
    author: "Stephen"
  - subject: "Re: Is Ark gone from KDE 4.0?"
    date: 2007-07-30
    body: "hm.. i really hope that he gets ark back into trunk in time... otherwise it seems to me that kde 4.0 get's a bit cut atm.. i know that it's just a first step to the perfect kde4 release ^^ but it hurts to hear that some things get removed i really liked :-("
    author: "LordBernhard"
  - subject: "Re: Is Ark gone from KDE 4.0?"
    date: 2007-07-30
    body: "I think that Henrique could create a list of \"junior jobs\" needed for Ark. If it wasn't C++ I would love to help but anyway maybe this way someone could step in and help with Ark development."
    author: "Vide"
  - subject: "Re: Is Ark gone from KDE 4.0?"
    date: 2007-07-30
    body: "Nice idea, I'll create the list. Thanks!"
    author: "Henrique"
  - subject: "Widget Style"
    date: 2007-07-30
    body: "These artistic things are very complicated, it's a pity the other maintainer abandoned the project rather then work with the suggestions, but I think I know how he felt when the complaints started appearing.. I almost went frenzy on my work when we started discussing a presentation I was making for a conference..\n\nWhen he says: \"Let's leave the artistic stuff to the artist, please!\", I hope the artists actually improve this theme, because IMHO this theme is not very nice, and I think this new kde4 theme must be awesome, like the Oxygen's icons, so we will have a great advantage against vista artwork (which one I don't like), and be on pair with osx, it would be great :)\n\nBut I trust Nuno, who is a great artist, and the other guys in the artistic part of the widget style."
    author: "Paulo Cesar"
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "Looks pretty smooth from that one screenshot, without being gaudy."
    author: "Ian Monroe"
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "And check here for the target look:\n\nhttp://www.jarzebski.pl/read/nowy-styl-oxygen.so"
    author: "jospoortvliet"
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "In fact no - don't do that as those are out of date, and in fact have never been the target, but only trials.\n\nAnd they were \"stolen\" without asking the artist afaik\n\nFor what it's worth the screenshot I provided is also not the finished look. Just take a look at the broken headline of the groupboxes for evidence of the beta state."
    author: "Casper Boemann"
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "Well, the shots look a lot like your work, and like what Thomas did (pity he left, btw).\n\nSorry if this isn't the real target, I won't link to it anymore..."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "hell, am i dreaming !!!! this is the best mock up i saw for graphic user interface"
    author: "djouallah mimoune"
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "yeah.. i fully agree"
    author: "LordBernhard"
  - subject: "Re: Widget Style"
    date: 2007-08-02
    body: "That is only the plasma target not the actual window-deco. As the screenshot shows it is going to be plain grey, without any wow-effect, just plain boring grey and nothing appealing. Even less usability as it does not provide sharp contras such as Cleanlooks (http://xmelegance.org/devel/qthtml/gallery-cleanlooks.html), just blury grey.\n\nI agree though that criticism is not useful. There will be an end-product which will suit the artist's taste and that's it. That's all an artist can do. If one runs out of ideas an artist cannot simply force himself to have new ones.\n\nIf the artist likes the screenshot above, (have a look at the floating buttons [didn't aaron at akademy tell everybody that one should not put things into an GUI that are not realistic?]) what's there left to say.\n\nThe plasma-design however is fantastic because it is innovative and stylish != grey. I am not sure about other languages but at least in the ones I know grey has a very distinct meaning which is very different from anthracite (plasma).\n\nBTW: If one is not allowed to discuss and critisice it, it is not art."
    author: "Piotr."
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "I sadly have to agree with you. When I first saw the style, I just thought that it wasn't my cup of tea. It was just \"too much\". \"But hey\", I thought, \"it's just work in process and doesn't have to reflect the end result. Better wait to comment on small details and sound negative\".\n\nThe style is now indeed cleaner, for example the buttons have been redesigned. However, there are things that I still dislike. I wonder if the artists are interested to know why, or should I wait?"
    author: "Lans"
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "waiting will be the best thing to do right now. As soon as the style is more completely implemented, some feedback can be given. Wait till next beta..."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: Widget Style"
    date: 2007-07-30
    body: "Will do, thanks for answering. :)"
    author: "Lans"
  - subject: "Status of Windows Builds"
    date: 2007-07-30
    body: "What's the current status of KDE 4.0 builds for Windows?  Is there a site I can download the current builds to give them a try?\n\nThanks"
    author: "Elder Young"
  - subject: "Re: Status of Windows Builds"
    date: 2007-07-30
    body: "http://www.kdelibs.com"
    author: "Ian Monroe"
  - subject: "Re: Status of Windows Builds"
    date: 2007-07-30
    body: "You can try the the kdewin installer. New snapshot released recently too.\nhttp://lists.kde.org/?l=kde-windows&m=118525850911691&w=2"
    author: "Morty"
  - subject: "Re: Status of Windows Builds"
    date: 2007-07-31
    body: "how to install programs with it (like konqueror) i've installed dbus and some stuff with it.. but the kdelibs and such stuff wasn't available in the list\n\ni also wasn't able to found a howto which describes what to do"
    author: "LordBernhard"
  - subject: "Microsoft BOB?"
    date: 2007-07-30
    body: "I'm reluctant to draw the comparison, but does habitat smell of Microsoft BOB to anyone else?"
    author: "Narg"
  - subject: "Re: Microsoft BOB?"
    date: 2007-07-30
    body: "Not really - Bob was a desktop shell, whereas Habitat appears to be an educational app for very young kids."
    author: "Anon"
  - subject: "Re: Microsoft BOB?"
    date: 2007-07-30
    body: "Though I am aware of the existence of Microsoft BOB (abandoned more than a decade ago), I would say that Habitat is similar only in the way that Konqueror is similar to Internet Explorer, KWord is like Microsoft Word, and so on.\n\nI've never seen Microsoft BOB in action, and it is not the inspiration or guiding example for Habitat.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Microsoft BOB?"
    date: 2007-07-31
    body: "Personally, I think that it is quite a bit like BOB, but I am probably one of the few who thinks that it had potential. In particular, with the new 3d graphics AND desktop, I think that the idea is heading there. And while I would not want it for me (heck, I prefer the CLI for too many things), I think that something like this would work great for my mom (nearly 70)."
    author: "a.c."
  - subject: "David's Buzz"
    date: 2007-07-30
    body: "Interesting that dfaure has the highest Buzz rating even though he's been on holiday for the last few weeks ;)"
    author: "Anon"
  - subject: "Re: David's Buzz"
    date: 2007-07-30
    body: "Must be because everyone keeps referring to him now, instead of approaching him on irc or email :)\n\nQ: \"Why is that thing still not fixed\"\nA: \"you're the 100nt that asks, just wait until David comes back from holidays\"\n\n*grin*"
    author: "anon"
  - subject: "work going on on konqueror?"
    date: 2007-07-30
    body: "hm.. i wonder about the work on the interface of konqueror.. will there be changes? every commit i'm looking for changes in konqueror.. but i can't find them... is there work going on beside the changes with the dolphin backend and such stuff... i mean really konqueror only code or improvements, kplugin integration or such stuff.\n\ndoes anybody know about this?"
    author: "LordBernhard"
  - subject: "Re: work going on on konqueror?"
    date: 2007-07-30
    body: "Understand you're only seeing like 0.5% of the commits in the digest, each week... So it's very well possible you missed some work on konqi. And yes, there has been work on it, at least some usability stuff and integration of the Dolphin Kpart.\n\nAnd I have a vague memory of someone working on the extensions system, but I might be mistaken there... ???"
    author: "jospoortvliet"
  - subject: "Re: work going on on konqueror?"
    date: 2007-07-30
    body: "> Understand you're only seeing like 0.5% of the commits in the \n> digest, each week.\n\nMost of the interesting user-visible changes seem to find their way there.  If interesting developments start happening in Konqueror I am sure they will be featured in the digest.\n\nKonqueror in KDE 4 today looks pretty much the same as it did in KDE 3.5, except that the file management view is better because it uses Dolphin's view.\n\nThe HTML and JavaScript engines are part of kdelibs and they have seen much work."
    author: "Robert Knight"
  - subject: "Re: work going on on konqueror?"
    date: 2007-07-30
    body: "More like 15% ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: work going on on konqueror?"
    date: 2007-07-30
    body: "Sorry, got my numbers wrong then. Indeed, less than 3000 commits, over 100 in the digest... wow. I need to work on my math skills..."
    author: "jospoortvliet-former-superstoned"
  - subject: "thanks for Cokoon"
    date: 2007-07-30
    body: "Thanks very much guys who are working on Cokoon.\nthis is what people wanted for a long time!\nDekorator was a temporary solution to fix this on window decorations, but it was not a good solution.\nThanks guys!"
    author: "Emil Sedgh"
  - subject: "Re: thanks for Cokoon"
    date: 2007-07-30
    body: "For what it looks it will be the definitive theme creator, looks really a lot of fun!"
    author: "Iuri Fiedoruk"
  - subject: "oxygen"
    date: 2007-07-30
    body: "New oxygen style is ugly, previous was much better. (tabs, groupboxes, buttons)"
    author: "szookr"
  - subject: "Re: oxygen"
    date: 2007-07-30
    body: "No, it *is* not ugly, *you* find it ugly. That's something completely different. It also empowers you, the one with the problem, to actually address your problem by comming up with something better, or at least, something *you* find better. Who knows, others may agree...\nJust crying out that you find it ugly isn't really constructive, don't you think?"
    author: "Andr\u00e9"
  - subject: "Re: oxygen"
    date: 2007-07-31
    body: "Yeah, think so too, also there is much too little contrast, i liked it e.g. the old groubboxes which had a brighter color than the background. Will this come back in the final version?\n\nApart from that, it's nice too see early screenshots of the new style, we should rather be happy about this, than complaining since the style will naturally evolve until the final version."
    author: "Martin Stubenschrott"
  - subject: "Re: oxygen"
    date: 2007-07-31
    body: "I'm not a fan of it either, but I couldn't think of particularly specific criticism, nor would it matter this late in the game.  If you have specific input such as \"feature X could be improved with Y and Z\" then please speak up.\n\nHowever, the above comment really doesn't help anything."
    author: "T. J. Brumfield"
  - subject: "Re: oxygen"
    date: 2007-07-31
    body: "Here is just MHO on the style:\n\nhttp://img254.imageshack.us/my.php?image=capture32hf5.png\n\nTitle bar:\nSo, White *can* be your enemy, as you can see at the kate-open-file-dialog. Maybe the darker edge on the side line should also go on the top? Or maybe it should all be a bit less white? \nThe corners are not rounded but they look nice and sharp. If you look to long at them the corners of the corners appear to actually stick out a bit, especially on a dark background.\nThe titlebar buttons are nice and simple and have a functional size. But somehow the also have a \"bulky round\" feeling about them that doesn't go well with the oxygen icons but probably goes very well with a more rounded icon set. Do these buttons have to look like buttons or can they blend in better with the title bar, showing only the symbols?\n\nMenu/Button bar:\nhttp://img294.imageshack.us/my.php?image=capture30pf3.png\nThe spacing looks nice, it is not to crowded. Maybe you can even narrow the space between the menu row and the icon row.\nThe background image is far more creative then regular grey or gradient. It gives the otherwise boring background a more playfull look. But how light the shades may be it also looks a bit unorganised and distracting. But of couse this tension is exactly what art is all about.  \n\nThe location bar:\nWhat can I say, it's oke. The lighter backspace and enter buttons were long over due.\n\n\nhttp://img295.imageshack.us/my.php?image=capture36pu6.png\n\nTabs:\nI really like the sharpness of the tabs, they fit very well with the sharpness of the oxygen icons. Although at first I was unshure about the \"unnatural long tab look\" I feel it isn't unpleasant to the eye.\n\nCrumble path:\nLooks realy nice.\n\nSliders:\nThe horizontal sliders \"places\" and \"information\" look a bit out of place. They appear not to be consistent with the title bar buttons, they are both round and light but that is about it. Their marble look doesn't come back in any other element. Also, only judging from these screenshots, I am not totally shure what they are for.\nThe vertical sliders do not have that refined look that you would expect to acompany the oxygen icons. They are a bit bulky and their square look bites the other round elements.\nThe vertical slider buttons are not consistant with the vertical slider buttons. Maybe for a good reason, but I can not judge that from the screenshots.\n\nColumns/Rows:\nI think the row seperators of the left big buttons does not really exist but is coused by the gradient. It does look a but confusing.\nThe row seperators of the \"home content folder\" look clear and nice. The hard black is in big contract to the otherwise soft style, but again goes well with the sharp oxygen icons. \nThe column seperators of the \"apps folder\" uses the same dark colours as the \"home folder\" seperators. I don't think the seperation of the columns and the inversion of the selected column is necessary (sorted column is already clear by the triangle). Maybe you can experiment with it, because the hard contrasting colours put a lot of emphasis on these elements, maybe unification makes it softer.\nThe slide rails for vertical and horizontal are different (not consistent), with the horizotal one \"information\" almost looking like a needle (actually I'm not shure it is a slider), these are unpleasant associations you might want to avoid. But as the seperator for the \"Type:folder\" area it looks great.\n\nDrag points:\nThis must be me, but I never understood why these have to be visible all the time. First thing I always do is to set them to fade out and only appear on mouse over. I suppose this is also how they are supposed to behave and they are only visible for feed back now. If this is not the case then I think they are a bit to dominant.\n\n\nhttp://img254.imageshack.us/my.php?image=capture32hf5.png\n\nArea borders:\nThis is so much better than in most KDE3 styles. No longer boxes in boxes in boxes. No more big bulky lines every where.\n\nButtons:\nThe big buttons with text look a bit ruff but also clean and simple. They do fit in but they very much stand out as seperate elements. Maybe you can make it look like it is not glued on to there but is part of and elevated from the background.\nThe smaller buttons with text are a bit to small to hold the text and or icons nicely. The icon border sometimes falls over the button border. The text sometimes doesn't look centered. I'm shure this will be fixed easily and soon.\n\n\nI have the feeling the artist is trying to find a balance to the sharp and realistic oxygen icons by creating a soft style. I really like the idea, though you can see it is still work in progress. The softer style also makes that items easily look fluffy and big, this can be a real pit fall. I can imagine it is very hard to find the correct balance with all the relevant factors. But I trust the artist to work all this out, things like this just take a lot of time, because it has to grow. I hope the artist stays committed and motivated. It takes a lot of energy, creativity and guts to create a new style promissing style with lots of people watching over your shoulder. Good luck!"
    author: "debian"
  - subject: "Re: oxygen"
    date: 2007-08-02
    body: "As opposed to it's parent, I think the comment above actually *is* usefull and constructive. I would think the author of the style would appreciate getting this in his mailbox, because it might be missed here in the Dot. Maybe you should email him your comments?"
    author: "Andr\u00e9"
  - subject: "Best SVN comment of the week:"
    date: 2007-07-30
    body: "i missed monday so much i wanted to break SOMETHING, so i broke all the plasma widgets. i rock! ;P\n--Aaron"
    author: "Troy Unrau"
  - subject: "Re: Best SVN comment of the week:"
    date: 2007-07-30
    body: "+1"
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: Best SVN comment of the week:"
    date: 2007-07-30
    body: "+1\n\nand the winner for ranks 2-5...\n\n> Aaron J. Seigo committed changes in /trunk/KDE/kdebase/workspace: \n> insomnia strikes. i figure i could:\n> * lay there staring at the ceiling\n> * play games on pogo\n> * fix ebn issues\n>\n> one of those things is at least mildly productive.\n\n> Aaron J. Seigo committed a change to /trunk/playground/base/plasma/applets/digital-clock/clock.cpp: \n> make this not quite so ass big. btw, having offsets (like borders) and not\n> using a constant to reference them when doing manual layouting is amazingly\n> silly. what happens when the offsets change? yeah, you get to redo all the\n> numbers ... by hand. there's these things called computers...\n\n> Aaron J. Seigo committed changes in /trunk/playground/base/plasma: \n> split timetracker engine and applet apart and put the engine with the engines in engine/\n>\n> i love the word \"engine\". i love lamp. i love desk!\n\n> Aaron J. Seigo committed changes in /trunk/extragear/utils/kpager: \n> kpager gets a new home. loverly.\n\n\nYay! looking for easter eggs in June. I can only be curious how April must be like :-)"
    author: "Diederik van der Boor"
  - subject: "Re: Best SVN comment of the week:"
    date: 2007-07-30
    body: "Let me remind everyone that this is the *President* of KDE eV.  It's great that the KDE community can allow this culture."
    author: "bkudria"
  - subject: "Re: Best SVN comment of the week:"
    date: 2007-07-31
    body: "you mean anti-culture "
    author: "djouallah mimoune"
  - subject: "html anchors on Digest don't work"
    date: 2007-07-30
    body: "I never noticed before, but the html anchors in the contents table don't work."
    author: "Phase II"
  - subject: "Re: html anchors on Digest don't work"
    date: 2007-07-31
    body: "I've rewritten the table code (and removed 3.3kb of PHP ;)), fixing the hyperlink issue, and also a bug where icons were shown for sections that did not exist in that digest issue.\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Tagged for beta 1 you say?"
    date: 2007-07-30
    body: "OK, if KDE uses Subversion the same way everyone else does, this means KDE 4.0 beta 1 is now available (just not packaged and propagated to the mirrors).  Is this correct?"
    author: "Matt"
  - subject: "Re: Tagged for beta 1 you say?"
    date: 2007-07-30
    body: "OK, I checked SVN, and the beta (version 3.92) has been available for 2 days already!  Where's the announcement on kde.org?  :/\n\nEasy to get it though:\n\nsvn co svn://anonsvn.kde.org/home/kde/tags/KDE/3.92 kde4-beta1\n\nBe warned, however, that this downloads the entirety of KDE v3.92 (all modules).  Don't kill their servers for no reason!"
    author: "Matt"
  - subject: "Re: Tagged for beta 1 you say?"
    date: 2007-07-30
    body: "It is tagged, but we give the packagers a week to package (and I'm still writing the announcement...)\n\nIt'll get online Thursday, officially."
    author: "jospoortvliet-former-superstoned"
  - subject: "please i need to see a screenshot !!!"
    date: 2007-07-30
    body: "as i am in work   (somewhere in the Algerian desert, where at 7 am, it's already 37 \u00b0) all is the PC are under windows, and i keep watching my 5 opensuse cd, without the right to install it.\n\nso please guys, can you make some screeshots ok KDE svn, just to see the progress made.\n\n\nshit!!! people are in holidays, and i am stuck here, fu.. work. "
    author: "djouallah mimoune"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-30
    body: "I can imagine your need ;-)\n\nBut as KDE 4 Beta 1 will be released soon, and at least OpenSuse will have a livecd, it's very unlikely we won't have a osdir screenshot tour within days ;-)\n\nSo, don't worry, be patient.\n\nBTW the release announcement is quite verbose, and has screenshots :D"
    author: "jospoortvliet"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-30
    body: "hehe thanks dude, grrr, two days waiting for the official announcement !!! but i am not holding my breath as long as kicker is still alive :-)"
    author: "djouallah mimoune"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-30
    body: "<Kidding>\n\"I miss the golden days of 2006 and all the \"Plasma seems still to exist only in /dev/imagination\", \"Most of KDE 4 is just vaporware\", \"Is nothing but a copy of Apple's dash board with some variations\", \"I'm just saying that it isn't *innovative*\" posts.\"\n\nSomeone was missing those days, I think he could have enough fun now!\n</Kidding>\n\ndont worry, it will be there for 4.0 release :)"
    author: "Emil Sedgh"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-30
    body: "come on, i did not say that, all the story is that in the previous alpha, kicker was so broken that,i got the envy to kick the ass of his maintainer.  \nin last summer, i keep repeating the word plasma to a friend, and when he asked me, what the hell is plasma, all i told him, it will be cool, neat, although i did not got a clue about it, i had and still have faith in developers. "
    author: "djouallah mimoune"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-30
    body: "the maintainer of kicker would be me. would you like a fixed kicker for the betas or a replacement for it in plasma? you pick. ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "sorry, MR President, i would prefer a replacement of kicker in beta, but as you have only two hands and just one brain, it's too much for you, it seems even that you don't sleep.\n\nthanks  "
    author: "djouallah mimoune"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "Lovely discussion ;-)\n\nYou guys made me smile :D"
    author: "jospoortvliet"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "here thay are, happy ctrl+c / ctrl+v ;)\n\nhttp://img403.imageshack.us/my.php?image=capture25ei7.png\nhttp://img504.imageshack.us/my.php?image=capture26hd6.png\n[http://img521.imageshack.us/my.php?image=capture27fj0.png\nhttp://img521.imageshack.us/my.php?image=capture28th6.png\nhttp://img504.imageshack.us/my.php?image=capture29np8.png\nhttp://img294.imageshack.us/my.php?image=capture30pf3.png\nhttp://img254.imageshack.us/my.php?image=capture31ws4.png\nhttp://img254.imageshack.us/my.php?image=capture32hf5.png\nhttp://img504.imageshack.us/my.php?image=capture33bs6.png\nhttp://img504.imageshack.us/my.php?image=capture34hz3.png\nhttp://img254.imageshack.us/my.php?image=capture35ez0.png\nhttp://img295.imageshack.us/my.php?image=capture36pu6.png\nhttp://img261.imageshack.us/my.php?image=capture37fs2.png\nhttp://img261.imageshack.us/my.php?image=capture38ga0.png\nhttp://img404.imageshack.us/my.php?image=capture39tf0.png"
    author: "Emmanuel Lepage Vall\u00e9e"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "> here thay are, happy ctrl+c / ctrl+v ;)\n\nNah, klipper URL detection to the rescue!\n"
    author: "cm"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "I'm sorry, but the Oxygen style is the most god-awful style I've ever seen, even worse than most GTK+ styles.  There's way too much extra spacing, there's no differentiation between widgets and widget types and...it just looks terrible.  I hope this is a *very* rough draft."
    author: "joel"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "Man, does no one have basic manners anymore? Fine, you don't like the shots you've seen of the (early, still in the process of being rewritten) style. You've even got some actual points about spacing to make. So why do you have to use language like 'god awful'? \n\nI mean, do you do anything creative yourself? Even cook or anything else that requires you to have to put something in front of someone else and then be judged on the results? How on earth would you feel if your efforts were judged as harshly as you seem to feel fit to judge others in front of potentially thousands of people?\n\nNothing wrong with comments, nothing wrong with having opinions, but sheesh, why can't people who are presumably here because they're interested in the project at least manage some basic standards of politeness?"
    author: "Borker"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "I apologize for my language.  It's the internet.  It's how people talk.  But, I apologize nonetheless.\n\nI still think that it's a step backwards.  Spacing is a BIG problem I find in styles on Linux.  It simply does not make good use of screen real estate.  GTK is especially guilty of this, but Qt/KDE styles aren't innocent either.  Windows and Mac are both much better about making good use of screen real estate."
    author: "joel"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "\"I apologize for my language. It's the internet. It's how people talk. But, I apologize nonetheless.\"\n\nIt's the internet?? it's how people talk??\nMaybe lamers on slashdot/digg talk like this, but it's not *the internet* for sure."
    author: "Vide"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "I retract all my comments :(."
    author: "joel"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-08-04
    body: "Better not to air an opinion on this holy cow of a subject. (You are not alone)\nJust wait for the avalanche of styles coming for KDE4 on www.kde-look.org.\nEven QT4 have an CSS style tweakability now, so I have heard."
    author: "reihal"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "The new arrows are beautiful, and I was a fan of the old ones O_o"
    author: "Luis"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "I think this looks great for a Beta, actually really good for a beta. Thanks goes out to all the developers, I know you guys are trying your best and I can't wait to test the Beta when it's official. You haven't let me down yet and I'm sure you won't in this release. May the wind be always at your Back!"
    author: "jmiahman"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "Thank you for posting these!"
    author: "Andr\u00e9"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-07-31
    body: "thanks Emmanuel for the screenshots, i would like to make a formal apologize for my trolling about the kicker.\n\nyou rock guys, i think many people are just jealous about the progress made to kde. "
    author: "djouallah mimoune"
  - subject: "Re: please i need to see a screenshot !!!"
    date: 2007-08-02
    body: "So kicker will be out of kde4? Not that it's a bad thing... I think plasma would be a really good substitute for it. Just need to build a start menu in plasma and it should work...\n\nIMHO the oxygen theme doesn't really have as much OOMP as the default kde3 theme. There's just something lacking. Hopefully the artist will be able to bring it together in later versions...\n\nben"
    author: "ben schleimer"
  - subject: "koffice website"
    date: 2007-07-30
    body: "The koffice website http://www.koffice.org is down."
    author: "Marcel Cavalcante"
  - subject: "Re: koffice website"
    date: 2007-08-02
    body: "(DNS) problems;  fixed now."
    author: "Thomas Zander"
  - subject: "configuration directories?"
    date: 2007-08-01
    body: "I wonder if KDE4 will improve the settings saving policy. Currently I can hardly use KDE in heterogenious networks since different KDE version destroy the desktop settings.\n\nIn our network, each workstation has access to the same home directory containing the KDE settings in ~/.kde   \nWhen using different KDE versions, older KDE versions do not support certain settings and overwrite them such that the desktop looks odd when logging in with the original (newer) KDE version again. The problem with the settings is more dramatical when using different distributions in the same network. Among openSUSE workstations it at least \"works\", but when logging in from a Fedora box, they use their own widget style, their own specific desktop icons, their own KDE menu side image, etc. Kicker's panel configuration at least breaks (applets, alignment, menu bar on top...)\n\nDo I just miss a point, such that I can KDE prevent doing this or will situation improve in KDE4?\n"
    author: "Sebastian"
  - subject: "Re: configuration directories?"
    date: 2007-08-05
    body: "I don't know how this actually works, but I haven't seen any references of anyone working on it. So if you want to do something about it, you should try to figure out what goes wrong (not necessarily a coding thing, btw), and try to create a detailed bugreport. Of course, attaching a patch to that bugreport is the fastest way to get this fixed..."
    author: "jospoortvliet"
---
In <a href="http://commit-digest.org/issues/2007-07-29/">this week's KDE Commit-Digest</a>: <a href="http://plasma.kde.org/">Plasma</a> continues to mature, with improvements to the Twitter applet (and the creation of a complementary data engine), and the adoption of a common visual style for Plasmoids, and the integration of support for <a href="http://netdragon.sourceforge.net/">SuperKaramba</a> applets through the creation of the SuperKaramba Plasmoid. More work on the re-implementation of the Magnatune interface within the new music store framework, and integration of the recent Plasma work for <a href="http://amarok.kde.org/">Amarok</a> 2. More work on KBlocks, whilst KMines and KLines become the first KDE applications to take advantage of the <a href="http://commit-digest.org/issues/2007-07-22/#2">recently-developed KPixmapCache</a>. More work on colour mixing in <a href="http://www.koffice.org/krita/">Krita</a>. Import of Habitat, a realistic interaction environment, to playground/edu. A return to work on Cokoon, a framework for widget and window decoration creation. <a href="http://www.rsibreak.org/">RSIBreak</a>, <a href="http://www.basyskom.de/index.pl/kcall">KCall</a>, and the <a href="http://en.opensuse.org/Kickoff">Kickoff</a> menu start to be ported to KDE 4. KDE 4.0 Beta 1 is tagged for release.
<!--break-->
