---
title: "The LXer Interview: Sebastian K\u00fcgler of KDE"
date:    2007-07-13
authors:
  - "sruecker"
slug:    lxer-interview-sebastian-kügler-kde
comments:
  - subject: "bravo"
    date: 2007-07-13
    body: "really, really nice interview sebas =) enjoyed reading it..."
    author: "Aaron J. Seigo"
  - subject: "Re: bravo"
    date: 2007-07-16
    body: "Same here. I enjoyed reading it and finding out more about the very same community I belong to. There's just so much happening that one can't keep up with all the news.\n\nAnd bravo to Sebas as well for his progress and the work he's done."
    author: "Thiago Macieira"
  - subject: "Nice indeed"
    date: 2007-07-13
    body: "Yep, I also really enjoyed the interview. Thanks Sebas :)\n\n\nPS: A funny detail: First I thought the photo at the top was meant to be you. Then I thought, wait a minute, Sebastian sure has changed a lot since I last saw him.. ;)\n"
    author: "Mark Kretschmann"
  - subject: "Photo Confusion"
    date: 2007-07-13
    body: "Hello,\n\nI am glad that you all like the Interview. Sorry for the confusion on the photo. At LXer we put a picture of the Editor with the Articles they write.\n\nAgain, sorry for the confusion.\n\nRegards,\nScott Ruecker\nEditor-in-Chief\nLXer Linux News"
    author: "Scott Ruecker"
---
A look inside what makes KDE tick, a glimpse of what the future holds, and more in the <a href="http://lxer.com/module/newswire/view/89530/index.html">LXer interview with Sebastian Kügler</a>.

<!--break-->
