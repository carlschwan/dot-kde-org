---
title: "K3b 1.0 Has Finally Landed"
date:    2007-03-17
authors:
  - "str\u00fcg"
slug:    k3b-10-has-finally-landed
comments:
  - subject: "890 bugs closed in the last year"
    date: 2007-03-17
    body: "the title says it all... impressive work! Thanks a lot for K3b!"
    author: "MK"
  - subject: "A big Thanks :)"
    date: 2007-03-17
    body: "This is one of my best KDE applications, so a big thank you :)\n\nSo the obvious question though is, what are your plans for the application?  Can a pretty much perfect program get better?  KDE 4 porting? etc.\n\nCheers *toasts to k3b*"
    author: "Troy Unrau"
  - subject: "Re: A big Thanks :)"
    date: 2007-03-17
    body: "We'll, as allready mentioned in another article, the inclusion of the functionality as k9copy provides, would make it _the_ perfect app. It's like merging Nero with DVDshrink..."
    author: "cossidhon"
  - subject: "authoring"
    date: 2007-03-17
    body: "Nah. Copying DVD:s is lame. You should be making your own. Include a DVD authoring tool instead!"
    author: "Martin"
  - subject: "Congratulations!"
    date: 2007-03-17
    body: "K3B seems like it should've been a 1.0 a long time ago.  I've been using it to make CD's and DVD's for years.  For that, I thank you.  I appreciate your effort to make it very stable and loaded with features before deeming it version 1.  Like others, I can't wait to see what you have planned for the future."
    author: "Louis"
  - subject: "Re: Congratulations!"
    date: 2007-03-17
    body: "Congratulations to all the people who have been hardly working on K3b, especially Sebastian!!!\n\nI think Louis is right, K3b 0.12* was already very stable, so K3b deserved a 1.0 release way earlier. ;)\n\nI assume it is going to be ported (as _many_ people rely on it) to KDE4 anyway. But besides that I was wondering if the project is going to change it's name into K4b (I've always assumed the 3 is from KDE3)?. Another thing I was wondering is if K?b is going to be ported to other platforms?\n\nThumbs up!"
    author: "Niels van Mourik"
  - subject: "Re: Congratulations!"
    date: 2007-03-17
    body: "First of all, a big thank you to Sebastian! What a great app K3b is.\n\nSecond (in answer to Niels):\nhttp://wiki.kde.org/K3b\n\"And if you ever wondered what K3b stands for: K is obviously from KDE, 3b=bbb=burn, baby, burn!\" ... so the \"3\" doesn't seem to be related to a specific KDE version."
    author: "Joao"
  - subject: "Re: Congratulations!"
    date: 2007-03-17
    body: "IIRC K3b is an abbreviation of \"KDE Burn Baby, Burn\", so the \"3\" has nothing to do with KDE 3."
    author: "Eckhart W\u00f6rner"
  - subject: "K4b"
    date: 2007-03-17
    body: "Just name it KDE Burn Baby, Burn Better\n\nVoil\u00e0"
    author: "Pierre"
  - subject: "Re: K4b"
    date: 2007-03-17
    body: "There already is an application called K4D.\n\nRenaming k3b to k4b would make less unique in name.\n"
    author: "whatever noticed"
  - subject: "Re: K4b"
    date: 2007-03-17
    body: "Also, there is already an application called K-3D.\n\nHowever, I think that it's not such a great idea to rename K3b for renaming's sake, especially if the 3 doesn't relate to the KDE version. Makes for bad promotionability.\n\nSebastian, K3b 1.0 is awesome! Thanks so much for your steady work, this is what makes K3b one of the best and most important free software applications that exist. Keep on rocking with K3b and Nepomuk!"
    author: "Jakob Petsovits"
  - subject: "Re: Congratulations!"
    date: 2007-03-17
    body: "This is indeed the reason it's called k3b.  I don't think there's any good reason to change it."
    author: "Troy Unrau"
  - subject: "Re: Congratulations!"
    date: 2007-03-17
    body: "K3B IMHO means\n- KDE Free Burner\n (K three B)\n\nA free burning app for KDE.\n\nK4B would mean KDE For Burn.... less sexy ;-)"
    author: "olahaye74"
  - subject: "Re: Congratulations!"
    date: 2007-03-17
    body: "Three (3), free. It disn't the same, but it does rhyme. I don't know, I'm a Windows user."
    author: "AC"
  - subject: "Re: Congratulations!"
    date: 2007-03-17
    body: "Never thought of it that way, funny pun. "
    author: "Lans"
  - subject: "Well done :-)"
    date: 2007-03-17
    body: "K3B is definitely a quality application.  I've been using it for a long time myself, prior to that, I was using XCDRoast.\n\nIt integrates nicely into KDE applications, supports many features, and is reliable.  I've never had a serious problem with it.  It does exactly what I ask of it, every time.\n\nIf more applications were constructed in the same manner as K3B, the IT industry would look a lot better."
    author: "Stuart Longland"
  - subject: "Re: Well done :-)"
    date: 2007-03-17
    body: "So true. K3B really has that *just works* quality. Recently, tried to burn a CD. So I do a rmb on a ISO file, to burn it. K3B turns up. Wait, I have no cd burner attached - so I grab an usb one, attach it - hit burn - and it just starts. No configuration or anything at all. Didn't even have to restart K3B... I don't remember if the burn dialog was already there and it immediately enabled the burn button, but at least it just showed up in the devices section and was usable."
    author: "superstoned"
  - subject: "Re: Well done :-)"
    date: 2007-03-21
    body: "I also used XCDRoast initially on linux and absolutely hated it (I'm sure it has improved since then as this was many years ago). \n\nK3b has joined the group of high quality applications (along with kopete + kmail + firefox + amarok) that I use everyday. \n\nCongratulations Sebastian and to the rest of the K3B team for a job well done.\n"
    author: "Ryan"
  - subject: "Just another thank you!"
    date: 2007-03-17
    body: "Nothing much to add to what the others have said, except MY thanks!\n"
    author: "Bent Terp"
  - subject: "Re: Just another thank you!"
    date: 2007-03-21
    body: "thank you"
    author: "andrea"
  - subject: "DCOP calls?"
    date: 2007-03-17
    body: "I presume that K3b will have changes before it is a KDE4 app, out-of-the-box since DCOP is being replaced with DBUS, or did I miss something?"
    author: "Marc Driftmeyer"
  - subject: "Re: DCOP calls?"
    date: 2007-03-18
    body: "Every kde3 application needs some changes before it is a KDE 4 application.\nK3b is no exception.\nchanging dcop into dbus is one of those changes.."
    author: "whatever noticed"
  - subject: "How nice"
    date: 2007-03-17
    body: "That's a great news! :)\nSebastian, thank you for your wonderful work and commitment to the project! \nI've been using K3b for years and it just works fine. We are with you, keep it going. :)"
    author: "Gruffy"
  - subject: "Tanks"
    date: 2007-03-17
    body: "thanks for this great piece of software. Although i don't use it very much, it is really really amazing, what you have done."
    author: "-Flo-"
  - subject: "Thanks!!!"
    date: 2007-03-17
    body: "I cannot express with words how grateful I am for this impressive high-quality application! It's applications like this that make me stay with linux. Again, many many thanks to you (and to Mandriva for supporting you)."
    author: "JakubZ"
  - subject: "Many Thanks!"
    date: 2007-03-17
    body: "Thank you very much, Sebastian! K3b is truly a great piece of software. It has everything you need and want for burning."
    author: "winter"
  - subject: "Windows port?"
    date: 2007-03-17
    body: "$SUBJECT is meant as a serious question.\n\nPrevious k3b versions were already the best CD/DVD burning software I've had the pleasure to use, and from the looks of the screenshots version 1.0 looks even better. So... is there a chance that the port to Qt4 opens a possibility for a Windows port of k3b? "
    author: "Frank Becker"
  - subject: "Re: Windows port?"
    date: 2007-03-17
    body: "Well, k3b is a frontend for commandlinetools like transcode, cdrtools, growisofs, etc. etc.\nAFAIK none of these are available under Windows..\n"
    author: "whatever noticed"
  - subject: "Re: Windows port?"
    date: 2007-03-17
    body: "Yes, the cdrtools are available for Windows. At least they were 4 years ago :-)\n\nMy congratulations for Version 1.0, too. \n\nMacht weiter so, Jungs.\n\nGr\u00fc\u00dfe, marrat"
    author: "marrat"
  - subject: "Sweet! Thank you :)"
    date: 2007-03-17
    body: "For me, K3B is the best burning app out there! And I tried a few... It has all I want, and more. The 'feature' I'm looking forward now, is Amarok being able to use K3B's burning process so it can burn music CD's without starting 'full' K3B."
    author: "Darkelve"
  - subject: "Re: Sweet! Thank you :)"
    date: 2007-03-17
    body: "afaik, K3B will have a library, making this possible."
    author: "superstoned"
  - subject: "Re: Sweet! Thank you :)"
    date: 2007-03-17
    body: "And I should've said: this is a future thing, it's coming ;-)"
    author: "superstoned"
  - subject: "Excellent"
    date: 2007-03-17
    body: "Certainly one of the greatest KDE applications available, and still one that I consistently see users of all desktop environments (GNOME, XFCE) using. :)"
    author: "apokryphos"
  - subject: "Re: Excellent"
    date: 2007-03-17
    body: "It's not like they have much choice... K3B has always been lightyears ahead of the competition, just like Amarok and KPDF and several of the KDE edu apps like Kstars and Kalzium. And it seems we are getting more and more of these apps in KDE 4 :D"
    author: "superstoned"
  - subject: "Me2"
    date: 2007-03-17
    body: "Just to add my 2c. Keep up the excellent work. Nothing comes even close to K3b."
    author: "encho"
  - subject: "Nice"
    date: 2007-03-17
    body: "It looks like you have done some nice things with the GUI. I really like what you've done and what you have plan. Thanks for all your work. "
    author: "tikal26"
  - subject: "congrats!"
    date: 2007-03-17
    body: "K3b was the First KDE Killer App for me! Even before KMail, K3b was pulling ahead of what other CD burning apps (principally XCDRoast) could do. \n\nI'm glad it's finally got a version number that won't scare people off! "
    author: "Caoilte"
  - subject: "Big congrats."
    date: 2007-03-17
    body: "K3B is the only CD burner I ever used, why use others if they are sub-quality! I still remember two years ago when I sent you an e-mail concerning a problem and, to my big surprise, I got an e-mail from you that solved the issue. That left a good impression and I am sure that I wasn't the only.\n\nCongratulation on an excellent application. From the posts, it sure looks like you made many touched many people by making them happy. I am sure your are proud and satisfied. Keep up the good work and may be your next project would be in video screen capture and editing. Just a hint! \n"
    author: "Abe"
  - subject: "k3b"
    date: 2007-03-17
    body: "thanks mandriva for zour support!"
    author: "fred"
  - subject: "Re: k3b"
    date: 2007-03-18
    body: "english on german keyboard or german on english keyboard? ;)"
    author: "panzi"
  - subject: "Congratulations!"
    date: 2007-03-17
    body: "Sebastian, Just saying Thank You doesn't seem near enough for everything you've done with k3b! Your hard work and dedication has payed off in spades, everyone who uses Linux is deeply in your debt. May the road always rise to meet you.\nSal\nhttp://www.pclinuxos.com/news.php"
    author: "Sal"
  - subject: "Killer Apps"
    date: 2007-03-17
    body: "Remember when KDE had the killer desktop/framework but not the killer appas ?\n\nWell, now there are IMHO 4 killer apps in KDE. Apps that are clearly better than anything else inLinux, and most likely in any other OS.\n\n* Konqueror as a all in one browser (thanks to kparts, etc)\n* Amarok\n* K3b\n* KDevelop\n\nThere are other booming applications, like Digikam, that will soon reach this category. Not to mention the improvements with KDE4 (Phonon, Plasma ...). This is getting REALLY exciting. Thank you all for the outstanding work. \n\nSebastian, are you aware of the magnitude of your achievement? \nRemarkable!\n\nCheers!"
    author: "KubuntuUserExMandrake"
  - subject: "Re: Killer Apps"
    date: 2007-03-18
    body: "For Amarok I still miss the features of the CD player and ripper.\n\niTunes is top of the class here.\n\nThat's all what is missing:\na) get rid off KCD\nb) play CDs inside Amarok\nc) rip the files as ogg\nd) autocreate a torrent from a playlist."
    author: "Fiasma"
  - subject: "Re: Killer Apps"
    date: 2007-03-18
    body: "\nb) seems already there (1st menu, 3rd item...)\nc) can be done using konqueror:\njust use the audiocd:/ url and copy the ogg directory.....\nd) may not be \"ethical\" ;-)"
    author: "olahaye74"
  - subject: "Re: Killer Apps"
    date: 2007-03-18
    body: "c) Is THAT what that's all about?  I really wondered about that, but assumed it was a just cool CD that I put in.  Where do I set options for things like bitrate and vbr level for that?"
    author: "Wyatt"
  - subject: "Re: Killer Apps"
    date: 2007-03-18
    body: "kcontrol -> Sound & Multimedia -> Audio-CDs"
    author: "panzi"
  - subject: "Re: Killer Apps"
    date: 2007-03-18
    body: "I'd rather not see KCD go away, at least not to be replaced by Amarok.  I just don't like Amarok right now.  I'm one of those who is amazed when people say konqueror has a cluttered UI (I like it how it is), but Amarok's UI is too much for me.  Too much going on.\n\nI'm quite content with Kaffeine (0.4.3) for videos, Okle for DVDs, Juk for mp3 and kcd for CDs.  Do one thing, do it well."
    author: "MamiyaOtaru"
  - subject: "more command line tools"
    date: 2007-03-18
    body: "sorry to put a feature request here, but it just came to my mind.\n\nwould it be possible/reasonable to integrate tools like nrg2iso or mdr2iso?\n"
    author: "Andi"
  - subject: "Re: more command line tools"
    date: 2007-03-18
    body: "Looking at the code of nrg2iso indicates that after some sanity checks, it just chops off the first 307200 bytes. It might make more sense to add support for doing this into k3b directly.\n"
    author: "Richard Moore"
  - subject: "Re: more command line tools"
    date: 2007-03-18
    body: "Looking further, IAT seems like the best bet as it supports more formats. It's written by the author of mdf2iso and seems to be intended to replace it.\n\nhttp://iat.berlios.de/ "
    author: "Richard Moore"
  - subject: "Proud"
    date: 2007-03-18
    body: "You said you are proud to announce the release of K3b 1.0.\nI say we, as a community, are proud of what you've achieved; of your vision and the persistence you showed to make it a reality.\n\nFeel proud, we're proud of K3b and specially we're proud of you!\n\nThanks!\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "K3b is truly a killer app."
    date: 2007-03-18
    body: "Easily beats any commercial suite on the market in terms of its feature set and ease of use."
    author: "Jonathan Zeppettini"
  - subject: "Thaks a LOT!"
    date: 2007-03-18
    body: "Thanks for making everyday life easier for us KDE users!\n"
    author: "Jukka"
  - subject: "Wonderful"
    date: 2007-03-18
    body: "Original, usable, intuitive, powerful, congratulations to the team!"
    author: "Youssef"
  - subject: "Thanks also from me!"
    date: 2007-03-18
    body: "K3b has always been a special app for me. It might very well be the application that kept me from switching back to Windows. When I tried to switch to Linux around the SuSe 8.x time the biggest obstacle was the fact that there was no decent CD burning application. The only application with an acceptable interface was CD Bakeoven. But you could not burn CD audio directly from MP3. I just could not believe there is not even a decent app to burn Audio CD's. I considered switching back to Windows because this was minimum functionality required for me to use KDE full-time. Soon after I discovered K3b which was then in its infancy. Anyway - it already worked great. I agree to many others who stated that K3b deserved the 1.0 long ago IMHO.\nK3b is really a special application in OSS because it just never fails. It does exactly what you want and expect. Even with seldom-used functions. I never tried to burn a Video-DVD. Recently, a colleague wanted to burn one and his Windows system had lots of unexplicable errors and problems. He asked me if I could burn this under Linux. I was sceptical but tried it anyway. This went so flawlessly it's simply amazing. Thanks a lot for all the hard-work that went into this rock-stable product.\n\n"
    author: "Michael"
  - subject: "Web page."
    date: 2007-03-18
    body: "Congratulations!\nI have used K3b since I started Linux. I just took it for granted.\nYour screen is one of the best from a User's point of view.\n \nthanks\n"
    author: "Edward Perry"
  - subject: "Thanks for a great piece of work"
    date: 2007-03-18
    body: "This application has supported me and my family with multiple s/w Cds and DVDs and video DVDs of weddings, Christenings and family get-togethers, with very few problems (most of which were mine).  Carry on and enjoy life.\n    George"
    author: "George Tharalson"
  - subject: "Its a pity"
    date: 2007-03-19
    body: "Its a pity that after all the stirling work of sebastian he cannot sit back and bask in the glory of a job well done. I suppose its the nature of computing that components like cdrecord and DCOP are superseeded and heroes who take on such great projects as K3b cannot stop maintaining them.\n\nWell done, K3b has worked flawlessly for me ever since the earliest version.\n\nKen"
    author: "KenSand"
  - subject: "THANKS!"
    date: 2007-03-19
    body: "What a fantastic effort is K3B - thanks so much for seeing it through to v1 - it's people like you who are the true hero's in this open source world, against a monopolistic evil giant that will never get a penny from me to pump up their bank accounts - thanks again for making this work in Linux!!!!"
    author: "Shelton"
  - subject: "Thanks!"
    date: 2007-03-19
    body: "K3b is a killer app!"
    author: "Joergen Ramskov"
  - subject: "Thanks Sebastian!"
    date: 2007-03-19
    body: "Just went thru the announcement at k3b site :) however I miss a description about the all-rewritten video capabilities of the application... has he forgotten!?"
    author: "NabLa"
  - subject: "Big Respect"
    date: 2007-03-19
    body: "You championed this project in an excellent manner.\nGreat job and continue success."
    author: "David Muir"
  - subject: "Thanks!"
    date: 2007-03-19
    body: "Thank you very very very very very very much for this great app!"
    author: "Antonios Dimopoulos"
  - subject: "I think I actually \"owe\" the k3b team!!"
    date: 2007-03-19
    body: "I'm a DBA professionally, I know as much about computers in general as the next geek in line.  I'm an expert nowhere other than databases however.  About two years ago my gentoo box pooped on an emerge sync && emerge -DNu world, major system update for none gentoo peeps, and I got seriously frustrated and didn't want to spend 3 days rebuilding my box, so I installed xp.  I used it for a few weeks until I need to burn a cd, nothing fancy, just burn a simple cd.  I downloaded every program possible and even PAID for one and couldn't get it done, I finally ended up re-installing linux JUST to use k3b to burn a stupid cd.\n\nIt's that amazing of an application, I rarely burn cd's, never dvd's, but I am dependant enough on k3b that it alone keeps me stuck to linux!!!  I'm a huge open source proponent anyways, but I like things that \"just work\", I am currently an Ubuntu user, it's the first binary distro to \"just works\" like I want it to enough to get away from gentoo.  I started out on LFS actually, but whatever thems is details about me life that no one cares aboot :)\n\nMy point is simple, k3b is as close to perfect as perfect gets, I think I'm still using 0.12, and I can't for the life of me imagine what you've done to it to make it 1.0, shoulda been 1.0 a long time ago.  Thank you, thank you, and did I mention thank you???"
    author: "Joshua Austill"
  - subject: "Greatest burning app"
    date: 2007-03-20
    body: "Sebastian! k3b is an amazingly great piece of software. Dear, you've shown some closed-source applications, what quality software looks like!\nAs we use k3b in our business we already donated some money, but besides from this we want to say \"thanks\" for your much appreciated work."
    author: "Thomas"
  - subject: "K3B Sucks"
    date: 2007-03-20
    body: "Hahaha I know it's lame, but I had to say it.  All this praise must be bad for Sebastians ego.\nBut seriously, I have to agree with everybody here.  K3b rocks.  Whenever I try to show someone (a windoze user) how cool linux is, K3b is the first app I show them.  And it only got a '1' now?  Rubbish, it's been worth a '1' for a long time. You could teach a lot of people about UI design as well. \n\nOki"
    author: "oki muhrer"
  - subject: "Congratulations"
    date: 2007-03-22
    body: "What can I say? K3b is awesome and since the first versions it made a world of a different with the until then existing crap^H^H^H^H software on linux for burning."
    author: "Juanjo Alvarez"
  - subject: "Blu-Ray/HD-DVD"
    date: 2007-04-02
    body: "I want to congratulate you one more time.\nI want to ask you something: AFAIK there is support from the command line tools for Blu-Ray burning. Have you planned to include this support to K3B?\nCongratulations and Thank You!"
    author: "Antonios Dimopoulos"
  - subject: "A reason to get them kde libs"
    date: 2008-08-13
    body: "k3b must be the only reason I install these (damn ;)) kde libraries. Other than that, I'm more of a gtk person. \n\nThanks :) "
    author: "Jon Arbuckle"
---
I am proud to announce the <a href="http://k3b.plainblack.com/k3b-news/k3b-1.0-announcement">release of K3b 1.0</a>. After years and years of development, all the sweat (actually in the summer it can get sticky in front of the screen), all the tears (ok, admittedly, not that many), and all the countless hours I spent on a single application finally we have what I think is worth the big 1. K3b has come a long way since the early days in 1998 when it started as a dummy project for me to learn C++ and Qt development. The reason for that (besides my hacking) is the amazing feedback I got from the open source community and especially all the KDE worshippers. Give yourself a hand. Without all of you K3b would not have lived this long. I also want to thank <a href="http://www.mandriva.com/">Mandriva</a> for supporting the K3b development these last few months. Their KDE support makes a big difference.  A <a href="http://k3b.plainblack.com/k3b-news/k3b-1/k3b-1.0-changelog">ChangeLog</a> is also available.




<!--break-->
