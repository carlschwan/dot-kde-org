---
title: "aKademy Tutorials, BoFs, Power and Video"
date:    2007-05-29
authors:
  - "jriddell"
slug:    akademy-tutorials-bofs-power-and-video
comments:
  - subject: "yeah!"
    date: 2007-05-29
    body: "Wow, I'm so looking forward to Akademy..."
    author: "superstoned"
  - subject: "Emacs?!?!?!?!?!"
    date: 2007-05-29
    body: "Alright, now tell me WTF does Emacs have to do with KDE? And why is there nothing about Vi in the schedule? :P"
    author: "flamebait"
  - subject: "Re: Emacs?!?!?!?!?!"
    date: 2007-05-29
    body: "It's the editor most KDE developers use, going by the answers to People Behind KDE.  Less so now than when the first series of People started though.  \n\nAnyone is welcome to lead a workshop, just e-mail the address on the site and we'll try and schedule you in.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Emacs?!?!?!?!?!"
    date: 2007-05-29
    body: "Because emacs is a professional tool, while vi is just for newbies?  :-P"
    author: "Inge Wallin"
  - subject: "Re: Emacs?!?!?!?!?!"
    date: 2007-05-29
    body: "come on, let's keep the religious wars *off* the dot. we've got enough trolling on the kde4 posts already ;)"
    author: "Chani"
  - subject: "Re: Emacs?!?!?!?!?!"
    date: 2007-05-29
    body: "What exactly do you have against religious wars?  This is deeply offensive to those of us who are in support of both religion and war."
    author: "KDE User"
  - subject: "Re: Emacs?!?!?!?!?!"
    date: 2007-05-29
    body: "Ahahaha! I love it when irony strikes on the interweb.  That just made my day."
    author: "Troy Unrau"
  - subject: "Re: Emacs?!?!?!?!?!"
    date: 2007-05-29
    body: "What about those of us that supports war on religion?\nDawkins rules!\n"
    author: "reihal"
  - subject: "Re: Emacs?!?!?!?!?!"
    date: 2007-05-29
    body: "Now now now.. they're *both* rubbish. :-P"
    author: "Tim"
  - subject: "Americans?"
    date: 2007-05-29
    body: "Any other Americans coming? So far I'm the only one on http://techbase.kde.org/Events/Akademy2007/Attendees I think. I know [the North American] Seigo will be there.\n\nI already have my huge British plug. :)\n\nI'm for sure going, but I have to say though... I'm not really sure that video is the most persuasive. :P"
    author: "Ian Monroe"
  - subject: "Re: Americans?"
    date: 2007-05-29
    body: "I like the qt dance video better :)\n\nhttp://trolltech.com/company/newsroom/press-kit/qt4dance\n\nThe akademy video didn't even mention all the drinking, the football (soccer) matches, the undulating..."
    author: "Troy Unrau"
  - subject: "Re: Americans?"
    date: 2007-05-29
    body: "Riiight.....I only had about 2 minutes of video footage, so I was a bit limited with content.  Let's call it more of a \"Proof of Concept.\"  The best I could do was an exhausted San Siego from too much previous undulating.\n\nIf people have video for events (LinuxTag, developer sprints, etc) and want some non-linear editing work done on it, let me know."
    author: "Wade Olson"
  - subject: "Re: Americans?"
    date: 2007-05-29
    body: "And now the question that everyone wants to ask: what software did you use to make this video? ;)"
    author: "Smarter"
  - subject: "Re: Americans?"
    date: 2007-05-29
    body: "At least I won't be there to stalk around looking particularly perverted, though Rob may yet catch a sleeping Aaron unawares."
    author: "Tom"
  - subject: "Re: Americans?"
    date: 2007-06-01
    body: "Haha fair enough. :) I'll be sure to get some of the corny digital camera video at aKademy this year."
    author: "Ian Monroe"
  - subject: "Re: Americans?"
    date: 2007-05-30
    body: "You're not the only one. :)\nI'll make sure to update the referenced page soon to reflect that."
    author: "Rex Dieter"
  - subject: "Matchmaker"
    date: 2007-05-29
    body: "if i understand correctly 'Matchmaker' is about finding appropriate life partner.\n\nif so, then take a look at socionics:\nhttp://humanmetrics.com/cgi-win/JTypes1.htm\n\nafter finding out what your 'type' is (say it is 1111), start looking for women (men) that have dual 'type' (0001). for example: if you're INTj, then you should be happy with ESFj."
    author: "anonymous-from-linux.org.ru"
  - subject: "Matchmaker?!"
    date: 2007-06-01
    body: "Great! What we need now is another program for perverts and the lonely hearts club! Aww come on guys, if you are so lame you can't find a mate then buy one. As for you women out there, if you are also that ignorant, maybe the gene pool can stand some weeding out. Do we really need this crap in the open source compendium? "
    author: "Rod Donovan"
---
With only a month to go the <a href="http://akademy2007.kde.org/schedule.php">schedule for aKademy 2007</a> is filling up.  Our <a href="http://akademy2007.kde.org/codingmarathon/tutorialday.php">tutorial day</a> has been popular enough to fill up two days covering subjects from <a href="http://doc.trolltech.com/4.0/qt4-interview.html">Interview in Qt 4</a> to Emacs, Kopete plugins and an introduction to KDE development.  Over in the <a href="http://akademy2007.kde.org/codingmarathon/bof.php">Birds of a Feather</a> meetings<a href="http://en.wikipedia.org/wiki/BoF"><span style="vertical-align: sup;">[?]</span></a> we have sessions including LSB compliance, Qt Jambi, Korundum and the intriguing KDE Matchmaker.  Such a high quality programme would normally cost hundreds of euro in the proprietary software world, but this is free software and thanks to an increasing number of <a href="http://akademy2007.kde.org/sponsors/">sponsors</a> we do not charge you a penny, but please remember to <a href="http://www.kde.org.uk/akademy/">register your attendance</a> or we cannot let you in.  For those of you new to the British Isles, we will have a number of European to British power adaptors on sale at aKademy at cost price.  Finally, for those unsure if they are coming to aKademy, this <a href="http://kubuntu.org/~jriddell/akademy2007.ogg">video shows the fun we had last year</a>.
















<!--break-->
