---
title: "Book Review: Foundations of Qt Development"
date:    2007-09-19
authors:
  - "jthelin"
slug:    book-review-foundations-qt-development
comments:
  - subject: "."
    date: 2007-09-19
    body: "\u00ab or even a beginner to GUI programming \u00bb\nI'm currently reading \"C++ Primer\" and planed to buy this book, but I never had any experience with GUI programming. What book should I read to not be a \"beginner to GUI programming\"?"
    author: "Qt-fan"
  - subject: "Re: ."
    date: 2007-09-19
    body: "Well according to this review, this book would be just fine."
    author: "Ian Monroe"
  - subject: "Waiting for it"
    date: 2007-09-19
    body: "I bought it some days ago to Amazon and I am waiting for it, among others! (still 2 weeks or so I think...)\n\nI want to have it now :)"
    author: "apol"
  - subject: "Let's buy it"
    date: 2007-09-19
    body: "In Wikipedia they would cry \"what shameful self-promotion\"! argh! But I am very glad that such a work is made available and find it very useful.\n\nQt is one of the best designed pieces of software I ever inspected. It is great and I hope the style of Qt also applies to your book: well structured and logical.\n\nWhat is its price? 50 Euro?"
    author: "Martin"
  - subject: "How does it compare to other Qt books?"
    date: 2007-09-19
    body: "Hi\n\nI've just learned the basics of C++ and would like to start learning Qt for GUI development. Now we have three books about Qt. Which one is recommended for a beginner like me. Currently I have a feeling that people recommend \"The Book of Qt 4\" for beginners like me. Is this true? It would be nice to see some detailed comparison of all these books so we could choose the right one more easily.\n\nThanks for the pointers."
    author: "Tsiolkovsky"
  - subject: "Re: How does it compare to other Qt books?"
    date: 2007-09-19
    body: "Well, don't know about comparison, but I've heard \"C++ GUI programming wth QT 4\" is downloadable from edonkey network. It's a bit old, though - the pdf I have is of June 2006.\n"
    author: "anonymous"
  - subject: "Re: How does it compare to other Qt books?"
    date: 2007-09-19
    body: "Please keep in mind that I only skimmed in my copy and havn't read it thoroughly yet, but I dare to say that this book puts just the right parts together to give you a very good help to start writing Qt applications when you are not that experienced or a C++ beginner with some programming experience in a similiar language. At lest I think it does so for me. A buy I don't regret.\n\nPrice was a little less than 40 &#8364; on Amazon.de when I bought it."
    author: "Val"
  - subject: "Re: How does it compare to other Qt books?"
    date: 2007-09-19
    body: "For people who read the book of Qt4 and Foundations of Qt Development. Which one is the best? Is one of them more noob-friendly?"
    author: "."
  - subject: "Re: How does it compare to other Qt books?"
    date: 2007-09-20
    body: "What C++ book would be a good complement to this book for an unexperienced C++ coder like me? One that has poses attention to the tidbits and pitfalls of C++  and teaches \"good\" C++ coding?"
    author: "Val"
  - subject: "Re: How does it compare to other Qt books?"
    date: 2007-09-27
    body: "that would be \"Thinking in C++\".\nThe second edition is free.\nYou can get it at http://mindview.net/Books"
    author: "Erlend"
  - subject: "ISBN numbers for this book"
    date: 2007-09-19
    body: "ISBN-10: 1590598318\nISBN-13: 978-1590598313"
    author: "Stefan"
  - subject: "Thats not a review..."
    date: 2007-09-19
    body: "When I saw 'Book Review' and 'Johan Thelin' in the same sentance, I wondered how an author could review his own book.  However, this isn't a review, it's really only a publicity blurb and I have no problem with that, but I still think there should be an acknowledgement in the text that the co-poster is the author.  Changing the title would help too, call it a preview or overview or something.\n\nJohn.\n\n"
    author: "Odysseus"
  - subject: "Re: Thats not a review..."
    date: 2007-09-20
    body: "Yeah please do that or let a third person make a real review.\nOther than that it is just advertisment and should be labled as such!"
    author: "mat69"
  - subject: "Re: Thats not a review..."
    date: 2007-09-20
    body: "Also: \"The publishers were kind enough to send KDE Dot News a copy of the book\" rings a bit false, considering one of the \"reviewers\" is actually the author. I would assume he already has a copy of his own book. And I'm pretty sure that if I were to call up the marketing department at Apress, the publisher, they would never have heard about KDE Dot News.\n\nStill, thanks to the Dot for being honest and putting Johan as an author of this article. And I'm sure the book is great! I can say that without bias as I have not even read it :-)"
    author: "Martin"
  - subject: "Re: Thats not a review..."
    date: 2007-09-20
    body: "Johan's publisher sent me a copy to review a few weeks ago.  He posted the first paragraph of this story to the dot and I expanded it into the full article above.\n"
    author: "Jonathan Riddell"
---
A few weeks ago the APress title <a href="http://www.apress.com/book/view/1590598318">Foundations of Qt Development</a> left the printing presses. The book introduces Qt in a step by step fashion, but also delves into most areas of the toolkit. The highlights include an in-depth look at the model/view classes,  as well as introductions to all the tools and widgets needed to get started.  The publishers were kind enough to send KDE Dot News a copy of the book, read on for a short review.










<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right">
<img src="http://static.kdenews.org/jr/foundations-of-qt-book.png" width="125" height="164" alt="book cover" />
</div>

<p>There have been a number of Qt 4 books published recently, which is a good sign of a platform with a healthy and growing developer community.  One of the problems for authors is that Qt is already one of the best documented programming libraries around, however there will always be a need for books from people who need a tutorial to get a good starting tutorial or to teach them all the corners of Qt.</p>

<p>This book starts with an assumption that the reader knows C++ and has some understanding of its standard template library.  It jumps in to describing the Qt style of C++ and how it is different to STL.  The rest of part 1 describes using widgets, dialogues, Qt Designer and main windows.  In only four chapters the reader will have a good understanding of the most important parts of Qt.</p>

<p>Part 2 covers every section of the Qt library starting with Interview, the model view classes.  Other chapters include creating your own widgets, files, translations, using plugins, multi threading, databases, networking, build systems with a detailed discussion of QMake and CMake (a must for KDE developers) and unit testing.</p>

<p>At the book's site you can download example source code as well as the sample chapter "Files, Streams and XML". The book is available both as a ordinary paper book and as a eBook. As a live appendix to the book, the site <a href="http://www.thelins.se/qt">thelins.se/qt</a> provides links, resources and articles.</p>

<p>While not a book for a beginner programmer, or even a beginner to GUI programming, this is a book with a thorough and complete coverage of Qt and would suit well those looking to move to a better development framework or even those who use Qt but want to better understand the complete power of KDE's most important library.</p>








