---
title: "aKademy 2007 Call for Participation"
date:    2007-01-04
authors:
  - "kduffus"
slug:    akademy-2007-call-participation
comments:
  - subject: "July?"
    date: 2007-01-04
    body: "I hadn't noticed the new dates yet: June 29th - July 8th.\n\nThis means I have to choose between the Roskilde Rock Festival and Akademy.. Tough"
    author: "Carewolf"
  - subject: "Re: July?"
    date: 2007-01-04
    body: "I think it's obvious: KDE rocks."
    author: "Adriaan de Groot"
  - subject: "Re: July?"
    date: 2007-01-04
    body: "Maybe we should co-locate Akademy with the Roskilde festival for 2008?"
    author: "Martin"
  - subject: "5 years of development?"
    date: 2007-01-04
    body: "The opening statement in the \"Call for Participation\" page says:\n\n> After more than five years of development this will mark a new level of \n> user experience, technical excellence in the framework, and \n> opportunities for free software on the desktop. \n\nKDE 4 can only have been in development for 2 1/2 years at most because the first Qt 4 preview was only released in late June, 2004.  I am assuming that serious development really did not begin until at least a year later when the final Qt 4 release appeared."
    author: "Robert Knight"
  - subject: "Re: 5 years of development?"
    date: 2007-01-04
    body: "Let's be generous and count 5 years between the release of KDE 3.0 and the release of KDE 4.0, ok? That's assuming we don't manage to squeeze it out by April 1st 2007."
    author: "Adriaan de Groot"
  - subject: "Re: 5 years of development?"
    date: 2007-01-04
    body: "But KDE developers are not so unfaithful as to leave a major release behind as soon as her .0 release was out the door.  In fact, there had 5 more happy releases together before they moved on."
    author: "Robert Knight"
---
The KDE community is getting ready to set a major milestone for the free desktop with the upcoming release of KDE 4. This will mark a new level of user experience, technical excellence in the framework and opportunities for free software on the desktop. The KDE contributors conference, which is part of aKademy, the world summit of the KDE community, will be the place to present the newest developments, long-term strategies or interesting input from the surrounding communities, projects and societies. Be part of it, present your thoughts, ideas and work at <a href="http://akademy2007.kde.org">aKademy 2007</a> in Glasgow, Scotland.





<!--break-->
<p>Presentations from all KDE contributors - translators, 
artists, coders, writers and organisers - are welcomed by the programme 
committee.  We also welcome presentations by people not directly contributing to KDE.</p>

<p>Topics relating to the Free desktop that might be covered at the conference include:</p>
<ul><li> KDE 4 architecture and technologies
</li><li> KDE 4 applications
</li><li> Desktop related hardware and software technologies
</li><li> Innovative human-computer interaction design
</li><li> Cool programming tools, patterns and techniques
</li><li> Advancements in l10n and i18n
</li><li> Quality Assurance in Open Source projects
</li><li> Legal, social, philosophical or promotional matters related to KDE
</li><li> Desktop software standards, usability and accessibility
</li><li> Performance analysis and improvements
</li><li> Cross-desktop technology and collaboration
</li></ul>

<p>Submission details are available on the <a href="http://akademy2007.kde.org/conference/cfp.php">Call for Participation</a> page.</p>





