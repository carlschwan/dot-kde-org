---
title: "KDE Commit-Digest for 22nd April 2007"
date:    2007-04-23
authors:
  - "dallen"
slug:    kde-commit-digest-22nd-april-2007
comments:
  - subject: "API review"
    date: 2007-04-23
    body: "\"Matthias Kretz, K\u00e9vin Ottens and Will Stephenson spent the week in Oslo as guests of Trolltech. The goal of the week was to apply Trolltech's expertise in API design to the Solid and Phonon APIs.\"\n\nCool! Many thanks to the Trolls. Are all of the KDE4 API:s similarly reviewed?"
    author: "Martin"
  - subject: "Re: API review"
    date: 2007-04-23
    body: "i would like the nepomuk api included for kde4.0"
    author: "lukilak@we"
  - subject: "Re: API review"
    date: 2007-04-23
    body: "It is being integrated into KDE 4, however as with many APIs, they will only really mature as the apps start to take advantage of them... so NEPOMUK may not have a huge visual presence in 4.0"
    author: "Troy Unrau"
  - subject: "Jamendo Homepage"
    date: 2007-04-23
    body: "Recently there are more and more webpages showing up which redirect you automatically to your local language. Would be a great idea if those pages were all of the same quality. Unfortunately they aren't. Sometimes they contain older news and sometimes the language is just strange. Don't you hate it to read such strange word-by-word translations of English expressions like \"spread the word\". They would rather keep those pages in English if they are unwilling to invest in proper translation. I rather read a foreign language than really bad German all the time.\n"
    author: "Michael"
  - subject: "Re: Jamendo Homepage"
    date: 2007-04-23
    body: "It would'n be in english but in french, Jamendo is a belgium (fr side) website :)\n\nBut it's support in amarok is, well, finaly pointing out :)\nAmarok will hopefully suport more than just download + listen music, as jamendo is a whole plateform for musician to get famous (whith date of concerts, criticism of albums, ..., oh, and lirycs)\n\n"
    author: "kollum"
  - subject: "Re: Jamendo Homepage"
    date: 2007-04-23
    body: "The Jamendo support is currently VERY preliminary, and as I wrote in the intial commit message, the main reason I started is now is to mature / refactor the service framework that will hopefully allow many more services like Magnatune and Jamendo to be easily integrated into Amarok.\n\nAs for supporting more than just listening and downloading, this requires a more flexible way of getting data from the Jamendo site, as the xml file I am currently working with does not contain much of this information. (except for lyrics for some tracks). It might be possible to parse the site for some of this info though...\n\nAnyway, I have pretty big ambitions with regards to integrating services into Amarok, but am limited by the time available and the multitude of interesting services out there... Hopefully, when the framework is a bit more stable, more people will be interested in adding and maintaining services"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Jamendo Homepage"
    date: 2007-04-23
    body: "Well, actually it is a Luxembourgish site, not a French one ;-).\nCheckout: http://www.jamendo.com/en/static/contact/"
    author: "anonymous"
  - subject: "svg"
    date: 2007-04-23
    body: "> When a new SVG cardset is chosen it uses the full SVG capabilties. Unfortunately, using so much SVG rendering is not exceedingly fast. KPat uses threading and pixmap caches to speed this up: Maybe it would be useful to have something like this in the kdegames libraries.\n\nBoth plasma and ksysguard also cache etc do speed up svgs.  The best thing way to do this is for everyone to use Plasma::svg  (that aseigo wrote) so that we can have a common caching etc class.  Perhaps kdegames should use this?\n"
    author: "John Tapsell"
  - subject: "Re: svg"
    date: 2007-04-23
    body: "plasma::svg is part of kdebase/workspace, and therefor cannot be relied upon to be present on all platforms (as workspace is only present on unix/X11) and kdebase is not a requirement for running programs from kdegames, for instance.\n\nBetter solution would be to make it more generic, and put it in kdelibs... that deadline, however, is very rapidly approaching...  you should probably talk to Aaron about this..."
    author: "Troy Unrau"
  - subject: "Re: svg"
    date: 2007-04-23
    body: "i think my quad core can handle your tiny svg's perfectly - please but your effort somewhere else"
    author: "srettttt"
  - subject: "Re: svg"
    date: 2007-04-23
    body: "Not everyone has quad cores... I'd rather KDE 4.0's minimum requirements to not make Vista's look low end..."
    author: "Sutoka"
  - subject: "Re: svg"
    date: 2007-04-23
    body: "right now i'm really only interested in working on this functionality for apps in workspace/, the reason is that we can experiment more aggressively there both with APIs as well as techniques.\n\nonce those classes mature and prove themselves, them i'll look to see if they make sense for kdelibs. in particular: are there enough apps that would benefit from these classes, and benefit enough to add to the size of libkdeui.\n\nthat's been my plan from the start with libplasma, and so far it seems a sensible one.\n\nso maybe for 4.1 =)"
    author: "Aaron J. Seigo"
  - subject: "Re: svg"
    date: 2007-04-24
    body: "This is not strictly necessary, many games in kdegames already have their own caching mechanism for SVG elements, and in some cases optimized for the usage pattern of the game. Most games that are using SVG have some sort of cache manager that leverages the QCachePixmap class in Qt, and some go one step beyond and even cache rendered backgrounds between sessions, so the game starts faster the next time it is launched."
    author: "Mauricio Piacentini"
  - subject: "Thanks for the great DE!"
    date: 2007-04-23
    body: "I have been a Linux/KDE user since mandrake 5.3 festen ( I still have the linuxmall disc!) And I always loved the way KDE worked/works, I like the idea that it doesn't treat the user like and idiot, that I can adjust anything I like or lock down the DE for people that actually need it to be locked down :P\n\nIn my opinion, it has the best integration than any other DE, most all KDE apps are complete and useful, love the feature of getting new content seamlessly within \nvarious apps like wallpapers and kopete. and oh! the magic that is kopete, what a sane app, Ktorrent is looking real nice, still uses alot of my system resources though,looking forward to future releases of this torrent client. but still better in my opinion than having to install that slow Java bittorrent client.   \nK3B, there is nothing to say about K3B except Sebastian Trueg and co RULE!\n\nI really have to thank ALL of the developers of KDE, Without you I probably would have never stuck with Linux much less made it my only OS. \n\nP.S.\n\nAmarok, the only music app I have ever used on a daily basis. it's worth is that of 50 proprietary music apps! \n "
    author: "madpuppy"
  - subject: "Guidance looks realy interesting!"
    date: 2007-04-23
    body: "The Guidance tool looks very intersting. I need to test it :-)\n\nI wonder if there is any cooperation between the Guidance developers and the distro@s? Thay all have their own different tools taht does basically the same. There should be some common ground and devlop toghether here?\n\nBirger"
    author: "Birger"
  - subject: "Re: Guidance looks realy interesting!"
    date: 2007-04-23
    body: "Not sure about others, but Kubuntu uses Guidance extensively."
    author: "NabLa"
  - subject: "Marble in Edu?"
    date: 2007-04-23
    body: "Great to see Marble coming along, might be time to jump in soon, but my one question is why the base widget is going into the Edu module?  I'm sure KWeather, Kontact, Kopete, DigiKam, and KControl don't want to be dependent on the entire Edu module just get the base widget?  How is this going to be organised?\n\nCheers!\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Marble in Edu?"
    date: 2007-04-23
    body: "This is a good point. Perhaps the notion is that Marble is a bit heavy for kdelibs?"
    author: "Ian Monroe"
  - subject: "Re: Marble in Edu?"
    date: 2007-04-23
    body: "Hi Odysseus,\n\n> Great to see Marble coming along, might be time to jump in soon,\n\nWe'd certainly be happy if you jumped in to help.\nI'm always trying to gather the latest information about virtual globes on other websites and as such your biggest contribution to Marble so far has probably been http://www.kartographer.org which contains quite some ideas for inspiration ;-) \n\n> why the base widget is going into the Edu module?\n\nThe idea is to let it mature there as an educational application first. Later on once it proved to be ready for real-life purposes I'd like the backend to move into a more central place where it can be used by other applications as well. So it's rather a matter of not jumping the gun and doing everything step by step.\n\nTorsten\n"
    author: "Torsten Rahn"
  - subject: "SK ???"
    date: 2007-04-23
    body: "I see SuperKaramba is still evolving and got new features included. That's very surprising since I thought it was planned to be superseded by Plasma ?! Can someone explain what is happening ?\n\nIs it still evolving for compatibility reasons with KDE3.x's widgets or... ?"
    author: "Plop"
  - subject: "Re: SK ???"
    date: 2007-04-23
    body: "SuperKaramba is still being developed so that when people start to use KDE 4, they will still have the ability to run their widgets if they choose.  Yes, Plasma is supposed to supercede SK, but it doesn't hurt to keep existing functionality available for users that would like to use it in future versions of KDE.\n\nWe also try to limit features unless they are bugfix related for the most part as we don't want to \"take Plasma's thunder\" if you get my drift.\n\nThe kross work is to demonstrate the ability to write bindings quickly for applications.  For more of the reasoning and explaination behind that, stop into IRC and talk it up in the #superkaramba channel on irc.freenode.net.  I'm personally not the one that did the kross work, so please ask in the channel.\n\nThanks for listening.\n-Ryan aka p0z3r\n\nhttp://www.p0z3r.org"
    author: "Ryan"
  - subject: "Re: SK ???"
    date: 2007-04-24
    body: "While Ryan already provided a very good reply, I like to add, that you probably shouldn't see it only as SK improvement, but also as one for Kross... SK does have very good use-cases for scripting + working scripts for testing + an already working implementation (those based on python) to compare with and the SK-team did everything related to the SK-internals (thx btw) while most of the work that was done by me, was done at the Kross backends and are therefore reusable anyway. So, all in all this looks like a win-win relation to me :)\n"
    author: "Sebastian Sauer"
  - subject: "kwin"
    date: 2007-04-23
    body: "there seem to be a cool developement in kwin. will this go in a 3.5 release or do we have to wait a year to get this on our computers?"
    author: "Beat Wolf"
  - subject: "Re: kwin"
    date: 2007-04-23
    body: "You'll have to wait :)  Additionally, this development is still happening in a branch in order to not destabilize the development environment for other KDE 4 coders.  Lubos' plan, last I checked, was to merge before 4.0 ships, though it needs a good configuration interface to all of the neat stuff that's in the works..."
    author: "Troy Unrau"
  - subject: "Re: kwin"
    date: 2007-04-25
    body: "From Lubos' blog on kdedevelopers.org:\n\n\"And finally, since the kwin_composite branch, while still far from being done, is not really different from trunk when compositing is disabled, it will be merged soon back to trunk, defaulting to compositing turned off.\""
    author: "Simon"
---
In <a href="http://commit-digest.org/issues/2007-04-22/">this week's KDE Commit-Digest</a>: A week-long <a href="http://phonon.kde.org/">Phonon</a>/<a href="http://solid.kde.org/">Solid</a> developer sprint redefines and strengthens their API's. The start of a command-line client for <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>. Continued improvements in the <a href="http://konsole.kde.org/">Konsole</a> refactoring work. More work on visual effects in the KWin window manager composite support branch. Experiments to utilise <a href="http://solid.kde.org/">Solid</a> for connection management in <a href="http://www.mailody.net/">Mailody</a>. Initial support for the <a href="http://jamendo.com/">Jamendo</a> music service in <a href="http://amarok.kde.org/">Amarok</a>. A KDE frontend for Marble is begun, to complement the Qt-based original interface. LSkat, KLines and <a href="http://edu.kde.org/klettres/">KLettres</a> get support for scalable graphics. <a href="http://netdragon.sourceforge.net/">SuperKaramba</a> now supports widgets written in Python and Ruby using <a href="http://kross.dipe.org/">Kross</a> - Kross is now the default scripting engine for SuperKaramba. <a href="http://home.gna.org/kiriki/">Kiriki</a> is moved from playground/games to the kdegames module. The <a href="http://kde-apps.org/content/show.php?content=18703">Guidance</a> utility suite is moved to the extragear module, becoming the first non-C++ application in KDE SVN.
<!--break-->
