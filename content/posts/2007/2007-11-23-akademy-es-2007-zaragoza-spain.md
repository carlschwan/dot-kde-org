---
title: "Akademy-es 2007 in Zaragoza Spain"
date:    2007-11-23
authors:
  - "agonzalez"
slug:    akademy-es-2007-zaragoza-spain
comments:
  - subject: "Alfredo Stallman?"
    date: 2007-11-22
    body: "Heh, every time I see a picture of Alfredo, his beard is longer. :-)"
    author: "Inge Wallin"
  - subject: "Re: Alfredo Stallman?"
    date: 2007-11-22
    body: "lol I'm afraid it's dissapeared now, but it's already growing again for the next meeting :-)"
    author: "Alfredo Beaumont"
  - subject: "Re: Alfredo Stallman?"
    date: 2007-11-23
    body: "Damn! No pictures till then then!"
    author: "Isaac"
  - subject: "Stickers in Egypt?"
    date: 2007-11-22
    body: "Is there any way to get a KDE sticker for my laptop without having to use a credit card (where are they sold anyway?) I live in Egypt and I would really appreciate if someone explained an alternative way to get one."
    author: "A KDE advocate"
---
This past weekend, November 16th through the 18th, Zaragoza Spain was the home of <a href="http://www.ereslibre.es/akademy-es">Akademy-es 2007</a>. The conference began early Saturday morning and finished Monday with a Hackathon. Akademy-es 2007, hosted by Hispalinux, Wireless Zaragoza, and the Zaragoza council, was a conference specifically for KDE developers and users from around Spain.






<!--break-->
<p>The conference began early Saturday morning and consisted of talks by various members of the KDE community. Pau Garcia presented CMake and its various features, Aleix Pol presented KDevelop 3 + CMake and even presented the new ideas pertaining to KDevelop 4. Alfredo Beaumont presented KOffice 2 and how it is taking shape as well as talked about ODF technologies and Flake. Thomas Nagy presented the mindmapping tool for documentation generation, Semantik.</p>

<p><a href="http://static.kdenews.org/nixternal/akademy-es/alfredo.jpg"><img src="http://static.kdenews.org/nixternal/akademy-es/smalfredo.jpg" alt="Alfredo Beaumont presenting KOffice 2 at Akademy-es 2007" /></a><br />Alfredo Beaumont presenting KOffice2</p>

<p>After lunch on Saturday, more presentations took place. Aleix Pol presented KAlgebra by explaining its purpose as well as what it can do right now and the future of it. Albert Astals presented some of the KDE 4 development technologies by using KTuberling as an example, which became an instant hit at Akademy-es 2007. Rafael Fernández presented Goya by explaining how to use it in order to get MVC-based interfaces with widgets. To close the talks on Saturday, Isaac Clerencia and Javier Uruen presented Inspektor, a frontend for strace, and explained how they have started its development as well as some of the problems they have faced.</p>

<p><a href="http://static.kdenews.org/nixternal/akademy-es/akademyesgrupo.jpg"><img src="http://static.kdenews.org/nixternal/akademy-es/smakademyesgrupo.jpg" alt="Akademy-es 2007 Group Photo" /></a><br />Akademy-es 2007 Group Photo</p>

<p><a href="http://static.kdenews.org/nixternal/akademy-es/cena.jpg"><img src="http://static.kdenews.org/nixternal/akademy-es/smcena.jpg" alt="Akademy-es 2007 dinner" /></a><br />Everyone enjoying food and drink Saturday evening</p>

<p>Sunday morning was all about Antonio Larrosa displaying the latest improvements of KDE 4. Antonio also spoke of the expectations from the KDE 4 libraries to KDE 4's applications and desktop features.</p>

<p>For a complete breakdown of the weekends festivities, feel free to <a href="http://www.ereslibre.es/akademy-es/index.php?option=com_content&task=view&id=31&Itemid=44">view the program</a> as well as check out some of the presentations from the conference.</p>