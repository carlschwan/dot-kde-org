---
title: "Interview with the KTorrent developers"
date:    2007-05-02
authors:
  - "Rudd-O"
slug:    interview-ktorrent-developers
comments:
  - subject: "I love KTorrent"
    date: 2007-05-02
    body: "It's a great program - Thank you Joris, Ivan and any others!\n\nI've only had one problem with it though - since I upgraded to kde 3.5.6 on ubuntu, KTorrent crashes for me every few hours.  Any ideas why?\n\nAlso I prefered it when there was always a search tab available.  It was changed so you have to click search first, then enter the search, which makes a new tab, then click the search button again to hide the search dialog.. \n\n"
    author: "John Tapsell"
  - subject: "Re: I love KTorrent"
    date: 2007-05-02
    body: "There seems to be a new version of a certain other torrent client, which sends a (broken) command which crashes ktorrent. The latest ktorrent fixes this, so get it ;-)\n\nNot sure where I read this, though, after noticing the crashes I started a search for the 'why'. I found a mail or something about it, and now it's fixed with the new version :D"
    author: "superstoned"
  - subject: "Re: I love KTorrent"
    date: 2007-05-02
    body: "Thanks.  I found the ubuntu bug report:\n\nhttps://bugs.launchpad.net/ubuntu/+source/ktorrent/+bug/110881\n\nHopefully this will be applied soon."
    author: "John Tapsell"
  - subject: "Re: I love KTorrent"
    date: 2007-05-02
    body: "if you're using kubuntu\n\na new package is available here for feisty:\nhttp://buntudot.org/people/~jdong/ktorrent/2.1.4/feisty/ktorrent_2.1.4~0jdong1~7.04prevu1_i386.deb\nfor edgy:\nhttp://buntudot.org/people/~jdong/ktorrent/2.1.3/edgy/ktorrent_2.1.3-0ubuntu1~6.10prevu1_i386.deb\nit was made by the official ktorrent packager for ubuntu.\n\nI found them on the official ktorrent dl page.\nhttp://ktorrent.org/index.php?page=downloads\n\nthe problem is with DHT dl I think. it's fixed now."
    author: "Patcito"
  - subject: "Re: I love KTorrent"
    date: 2007-05-03
    body: "Hey John,\n\nGood to find you helping around KDE. Seen your People Behind KDE piece on KSysGuard. Keep up the good work."
    author: "Roy Schestowitz"
  - subject: "Re: I love KTorrent"
    date: 2007-05-02
    body: "i had the same problem on kubuntu with kde 3.5.6. when i read the backtrace i saw something about dht. so i deactivated dht and ktorrent doesn't crash anymore. i'm not sure if that was the problem but i thought it would help others to know."
    author: "hvm"
  - subject: "Upgrade immediately!"
    date: 2007-05-05
    body: "Yup, it was DHT, all right. A bug in the DHT handling code made it possible to crash KTorrent by sending a malformed DHT message, and recently some buggy BitTorrent client appeared, that is sending exactly that kind of buggy DHT messages. It's fixed in KTorrent 2.1.4, so grab it and reenable DHT! This feature is too useful to be missed like this ;)."
    author: "zonk"
  - subject: "Rudd-O"
    date: 2007-05-02
    body: "What kind of site is that? :)\n\nA heckload of Google ads and a picture of some Miami Vice guy? I clicked on the 'About me' page (I thought maybe it's some KDE guy) and read:\n\n'In three words: I'm smart, handsome, and confident. I consider myself really good at thinking, socializing, programming, identifying songs and singing, driving, reading and writing.'\n\n...and that's all one needs to know I guess.\n\nThumbs up for KTorrent tho, I use it myself every day - it integrates sooo nicely into KDE. :-)"
    author: "Frerich"
  - subject: "Knode?"
    date: 2007-05-02
    body: "Ktorrent: yet another KDE program I've yet to try.\n\nAnd can we get an interview with Knode developer(s) sometime?"
    author: "Darkelve"
  - subject: "Re: Knode?"
    date: 2007-05-02
    body: "That interview alredy exists: http://behindkde.org/people/vkrause/"
    author: "Dominik"
  - subject: "Re: Knode?"
    date: 2007-05-06
    body: "Hey, thanks!"
    author: "Darkelve"
  - subject: "Great Program!"
    date: 2007-05-02
    body: "Have to agree with everyone else, this program just keeps getting better and better!  Keep up the great work!"
    author: "am"
  - subject: "Finally some recognition :)"
    date: 2007-05-02
    body: "It's nice to see that KTorrent gets to stand in the spotlight. It has really improved quickly, so I'm quite positive that the little annoyances will be fixed soon enough.\n\nMany developers in the BitTorrent world seem very isolated, but the devs listen to suggestions and seem to want to have contact with tracker admins, which is very good.\n\nGreat job, devs!"
    author: "mirshafie"
  - subject: "Great program. Thanks"
    date: 2007-05-02
    body: "I use KTorrent constantly and, now I've switched back to SUSE from Kubuntu, I have one that doesn't crash all the time :-).\n\nHowever, I prefered that tabbed layout. It makes better use of the the space in a layout that is already wide and it's fewer clicks to get to the info you want.\n\nMaybe we could have an option to change the layout.\n\nThe latest version seems quicker, too.\n\nThanks for the effort. Kepe up the good work."
    author: "DSC"
  - subject: "Re: Great program. Thanks"
    date: 2007-05-05
    body: "Read the comment below. SuSE ships a crippled version of KTorrent, that happens to be crippled by removing DHT functionality that was found to have a crash bug. You can have the same effect in Kubuntu by simply disabling DHT in prefs, or yet better, upgrade to 2.1.4 which had the bug fixed."
    author: "zonk"
  - subject: "Need an uncrippled version of KTorrent for SUSE"
    date: 2007-05-02
    body: "SUSE ships a crippled version of Ktorrent.\nI believe DHT is disabled due to some laws preventing peer to peer distribution or software in Germany.\n\nThis causes problems for SUSE users outside of Germany.\nSo my question is:\n\n  where can I get a Ktorrent rpm for openSUSE 10.2 that is not crippled?\n\n"
    author: "Yeah Right"
  - subject: "Re: Need an uncrippled version of KTorrent for SUSE"
    date: 2007-05-02
    body: "Try the download page on the KTorrent site."
    author: "Morty"
  - subject: "Re: Need an uncrippled version of KTorrent for SUSE"
    date: 2007-05-02
    body: "At your usual stops? Packman and Guru"
    author: "Anonymous"
  - subject: "Re: Need an uncrippled version of KTorrent for SUSE"
    date: 2007-05-05
    body: "Ditch RPMs! Compile it from source! It's Linux, you're talking about! ;^D"
    author: "zonk"
  - subject: "And kmldonkey?"
    date: 2007-05-02
    body: "Ktorrent is my torrent client. Light, fast and... KDE integrated! what more can i want?.. well, a redesign of the GUI, but this'll come with 2.2 version ;-) NICE!\n\nBy the way, has anyone know if kmldonkey will be ported to kde4? It seems abandoned. No matter, but, at least ported? I don't want to use a GTK frontend.\n\nBye!"
    author: "Me"
  - subject: "Re: And kmldonkey?"
    date: 2007-05-02
    body: "It's not aborted. I am one of the devels and I still use it daily mostly for http, ftp and torrent downloads. It's just, that I am very happy with it and don't miss there anything. Also it didn't crashed or showed any other bug one single time in the last 2 years or so.\nRe port to KDE4: for sure since I also like to use it on KDE4 :) Only question is, at what 4.x release it will be done."
    author: "Sebastian Sauer"
  - subject: "Re: And kmldonkey?"
    date: 2007-05-02
    body: "OK thanks, as you, for me kmldonkey is almost perfect, and think that isn't neccesary adding new features. Knowing that it'll be ported is enought. :-)"
    author: "Me"
  - subject: "Re: And kmldonkey?"
    date: 2007-05-03
    body: "Well, this is awesome to know.  Do you have any plans on utilising giFTd as well as mldonkey and co?  I'd like to see kmldonkey continue to expand in its P2P and download managing greatness. :)"
    author: "Matt"
  - subject: "Re: And kmldonkey?"
    date: 2007-05-05
    body: "Sorry for the late reply. Well, there are no extensions planned. Also there exist already a pretty cool KDE-app for giFT, http://www.kde-apps.org/content/show.php/Apollon?content=9899 :)"
    author: "Sebastian Sauer"
  - subject: "torrents"
    date: 2007-05-02
    body: "there is still a problem in the creation of torrents. That really should be made easier. Only few people understand how it works. I would like to select an amarok musician and say, okay, let's make  torrent out of it."
    author: "andre"
  - subject: "Thanks a lot"
    date: 2007-05-02
    body: "Thanks a lot for the best torrent program."
    author: "I\u00f1aki Baz"
  - subject: "Re: Thanks a lot"
    date: 2007-05-03
    body: "Yeah, thanks guys. I've been using KTorrent for quite some time and it worked well for me. Switched to RTorrent now though (no X) and love it as well.\n\nAnyway, great work on KTorrent...keep it up!!  <3"
    author: "hummingbird"
  - subject: "Re: Thanks a lot"
    date: 2007-05-03
    body: "Yeah it's too bad that any decent bittorrent site bans ktorrent because it cant report to the trackers correctly. Some cool app there...not. Seriously if you want a REAL torrent app then use rtorrent. KTorrent cant even do what its supposed to do correctly, so why are people singing its praises?!\n\nPS: I can't belive I'm the first one to post this little tidbit of info"
    author: "Noah"
  - subject: "Re: Thanks a lot"
    date: 2007-05-04
    body: "> KTorrent cant even do what its supposed to do correctly, so why are people \n> singing its praises?!\n\nMaybe because it just works for lots of people who never had the particular problem you had (for instance, I never had, KTorrent worked always fine for me. I am not a too heavy bittorrent user though). To codemn an app because of one single bug is a bit weak. \nI would also first check if the protocol is clearly defined here, and who implements it wrong, the trackers or KTorrent."
    author: "Frank"
  - subject: "Re: Thanks a lot"
    date: 2007-05-04
    body: "If the \"single bug\" is actually downloading torrents, then for a torrent downloading program that's kind of a biggie..."
    author: "John Tapsell"
  - subject: "Re: Thanks a lot"
    date: 2007-05-04
    body: "This was about reporting back to trackers, not about downloading. Incorrectly reporting to the trackers may get you banned on some tracker. It's obviously not a big problem, since it does not affect most of KTorrent users.\n\nBesides the KTorrent developers are very responsive to this kind of bugreports, and try to solve any issues cooperating with tracker admins. And he does not even mention wich version of KTorrent having the problem. The developers fixed some issues regarding this prior to 2.1."
    author: "Morty"
  - subject: "Re: Thanks a lot"
    date: 2007-05-04
    body: "This was about reporting back to trackers, not about downloading. Incorrectly reporting to the trackers may get you banned on some tracker. That's may and some. It's obviously not a big problem, since it does not affect most of KTorrent users.\n\nBesides the KTorrent developers are very responsive to this kind of bugreports, and try to solve any issues cooperating with tracker admins. And he does not even mention wich version of KTorrent having the problem. The developers fixed some issues regarding this prior to 2.1."
    author: "Morty"
  - subject: "Re: Thanks a lot"
    date: 2007-05-04
    body: "hmm, I just tried rtorrent, and it does indeed download about 4 times faster for me.  Your explanation does make sense - any sources that sites are banning it?\n\n"
    author: "John Tapsell"
  - subject: "I love KTorrent"
    date: 2007-05-04
    body: "Just wanted to say that."
    author: "Morgan"
  - subject: "Download speed needs to be improved"
    date: 2007-05-04
    body: "KTorrent is indeed a very good and promising program. But, it needs a lot of work on the download speed issue. If you open the same torrent file on KTorrent and Azureus, you will notice that Azureus downloads the file much faster than KTorrent. "
    author: "Vasilis"
  - subject: "Re: Download speed needs to be improved"
    date: 2007-05-04
    body: "However Azureus slows my machine down to a crawl, not to mention its terrible Linux support and the large number of memory leaks... the speed improvement is shadowed by these flaws."
    author: "Luca Beltrame"
  - subject: "Re: Download speed needs to be improved"
    date: 2007-05-05
    body: "I had to enable some options to make KTorrent fast I believe, UPnp, DHT, and the like (Can't check, I am at a computer without KTorrent installed right now)."
    author: "Frank"
  - subject: "how's the speed these days?"
    date: 2007-05-20
    body: "last time I used ktorrent, it was unusably slow, and vaguely annoying. but that was a long time ago. I guess I'll have to try it again sometime. normally I either use some really basic ncurses client or an almost-as-simple gtk one. it's been so long since I've downloaded anything that I don't even remember what they were called..."
    author: "Chani"
  - subject: "Good Work"
    date: 2007-06-10
    body: "I have to admit that the first time i used ktorrent, i didn't find it very functional for me, and i kept using azureus. However not long ago i decided to give it another try, my surprise was big. It is almost all  i could ask for a BT client (and also it's light, fast, and kde integrated). I am VERY VERY VERY pleased with ktorrent. GOOD JOB!!!!!! You are doing it great!!!!!!!!!!!!!!!!!!!!!! THANKS THANKS THANKS!!!!!!!"
    author: "pedro"
  - subject: "Great"
    date: 2007-06-12
    body: "Good program. Love the KDE integration, intergated search, UPNP... can't wait for 2.2."
    author: "riddle"
---
One of the most hotly debated topics on the Internet today is, without a doubt, BitTorrent: the most popular peer-to-peer network protocol today. Within KDE, the primary BitTorrent client is <a href="http://www.ktorrent.org/">KTorrent</a>. Joris Guisson and Ivan Vasi&#263; - the developers of KTorrent - have done an awesome job, so the least they deserve is public recognition - <a href="http://rudd-o.com/archives/2007/04/25/interview-with-the-ktorrent-developers/">we're pleased to have them as guests today</a>.


<!--break-->
