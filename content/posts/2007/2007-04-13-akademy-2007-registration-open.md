---
title: "aKademy 2007 Registration Open"
date:    2007-04-13
authors:
  - "jriddell"
slug:    akademy-2007-registration-open
comments:
  - subject: "Glasgow!"
    date: 2007-04-12
    body: "Glasgow! The stabbing capital of Europe!\n\nYou'll be fine as long as you aren't English and don't talk about football teams :)\n\n(I'm joking, just glad it's in Scotland!)"
    author: "Mike Arthur"
  - subject: "Re: Glasgow!"
    date: 2007-04-13
    body: "Glasgow?\n\nDoes anyone else have that song \nplaying in their head after reading that?"
    author: "Darkelve"
  - subject: "Re: Glasgow!"
    date: 2007-04-14
    body: "Humourous as these comments may be, safety at Akademy is an important issue so I feel I should comment.  Glasgow may have a reputation for murder and sectarianism but this comes as much from TV programmes like Taggart and the rivaly from Edinburgh folk like me and the first poster on this story as much as real life.  Akademy is in the city centre which is well policed and well populated.  Locals are friendly and approachable and talking to residents of Glasgow they have never had any problems in the area between the conference and our accommodation.\n\nAs for the other issue, Glasgow has a booming call centre industry on the strength of the spoken accent.\n\n"
    author: "Jonathan Riddell"
  - subject: "accent!!!!"
    date: 2007-04-13
    body: "The glasgow accent is sooo difficult to understand. People eat away half the words while speaking.\n\n(Rants of a foreigner in UK) "
    author: "Anonymous"
  - subject: "See you in Glasgow!"
    date: 2007-04-13
    body: "Bring a knife!"
    author: "glasgae"
  - subject: "Re: See you in Glasgow!"
    date: 2007-04-13
    body: "Bring a gun, because you know what they say about bringing a knife to a gunfight."
    author: "Chris Blauvelt"
  - subject: "Re: See you in Glasgow!"
    date: 2007-04-13
    body: "That's not a knife, that's a spoon!"
    author: "Patrick Thomson"
  - subject: "Re: See you in Glasgow!"
    date: 2007-04-13
    body: "I see you've played Knifey-Spooney before!"
    author: "SSJ"
---
<a href="http://akademy2007.kde.org/">aKademy 2007</a> is now <a href="http://www.kde.org.uk/akademy/">open for registration</a>.  aKademy is KDE's World Summit, a week long event for all KDE contributors, industry partners and users.  The week starts with a two day conference, and is set to include a tutorial day and a schools and education day.  As always, attendance to aKademy is free of charge, but you must register.  Registration must be in by the end of the month if you want the aKademy Team to book your accommodation for you.  See you in Glasgow!
<!--break-->
