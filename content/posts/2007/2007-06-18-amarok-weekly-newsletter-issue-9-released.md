---
title: "Amarok Weekly Newsletter Issue 9 released!"
date:    2007-06-18
authors:
  - "Ljubomir"
slug:    amarok-weekly-newsletter-issue-9-released
comments:
  - subject: "I hope this is a typo..."
    date: 2007-06-19
    body: "\"support for iPod devices with RockBox firmware\"\n\nThe iPod isn't the only device with Rockbox support.  In fact, I'd have to say that it's the _least_ supported (when compared to the other devices it supports) considering how long it takes for new iPods to be supported by it.  Hopefully this means that Amarok will support _all_ devices with Rockbox on them!"
    author: "Matt"
  - subject: "Re: I hope this is a typo..."
    date: 2007-06-19
    body: "I used to have an Archos MP3 player with RockBox, it was using mass storage device protocol, so ther's no reason it wouldn't be supported.\nPerhaps ipods aren't able to use msd protocol and so Rockbox uses another protocol, or whatever ...\nAnyway, I don't see any reason why most device using RockBox can't be supported, and I think they were, perhaps the only new thing is ipods being supported, so please, stop complaining."
    author: "Anarky"
  - subject: "Re: I hope this is a typo..."
    date: 2007-06-19
    body: "I don't think that was a complaint, more of a \"gosh I hope they can...\""
    author: "Steve"
  - subject: "Survey"
    date: 2007-06-19
    body: "How are you supposed to fill out the survey if it's a PDF?\nShould one print it, fill it out, scan it and then mail it?"
    author: "Evil Eddie"
  - subject: "Re: Survey"
    date: 2007-06-19
    body: "PDF if for printing and distributing amongst friends. It's on you to interpret them and report back to the mailing list in textual form. Usage of PDF is not mandatory as long as you provide answers to all questions. \n"
    author: "Ljubomir"
  - subject: "Re: Survey"
    date: 2007-06-20
    body: "8-o  What's up dock.? Needs a bridge over Trobled waters ?\n\nDon't you know how to edit your pdf files with Linux?\nDon't you know how to fill your pdf surveys with Linux?\noH mY LoVelY GoD ...!!! This can't be true.....\n\nNow all your worst worries finished..\n\nA Czech Technology arrived: PDF Editor\n\nhttp://pdfedit.petricek.net/\nhttp://sourceforge.net/projects/pdfedit\n\n------\n\n:-D Evil Eddie says: OoHh mY pdF is free now..."
    author: "The man who heard voiceS"
  - subject: "Re: Survey"
    date: 2007-06-22
    body: "It is not widely known but kwrite has the ability to edit PDF files"
    author: "Dan Warner"
  - subject: "35 is too young for survey cutoff"
    date: 2007-06-27
    body: "Hey, geezers use Amarok too!\n\nSigned, a nearly 50 year old.   "
    author: "dthacker"
  - subject: "Re: 35 is too young for survey cutoff"
    date: 2007-06-28
    body: "It was just a suggestion. We appreciate your input as much as any other. Thanks for caring :)"
    author: "Ljubomir"
---
The <a href="http://ljubomir.simin.googlepages.com/awnissue9">ninth issue of the Amarok Weekly Newsletter is out</a>. In this issue, we interview an <a href="http://amarok.kde.org/">Amarok</a> developer (Ian Monroe), take a look at the future of Amarok - Version 2.0 - and continue to provide nice usage tips.

<!--break-->
