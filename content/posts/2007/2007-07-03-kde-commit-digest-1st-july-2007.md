---
title: "KDE Commit-Digest for 1st July 2007"
date:    2007-07-03
authors:
  - "dallen"
slug:    kde-commit-digest-1st-july-2007
comments:
  - subject: "opera, firefox sucks !!! "
    date: 2007-07-02
    body: "it seems to be the most hated bug i have in kde is still alive even with second alpha 2 release, either in doplin or konqueror when you have  local files in html ( especially with a long name) you can't open them in opera or firefox.\nplease i am not a hacker, don't tell me that they don't support kde, all i know is that it works perdectly well in nautilus under gnome.\n\napart from that you rock guys, and happy hacking in glasgow !!!  "
    author: "djouallah mimoune"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "why don't you use konqui to open those html files? =)\n\nmaybe there are just not the right calls implemented yet to open firefox.."
    author: "Bernhard Rode"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "cause i prefer firefox, many files don't well render in konqueror, yes i know there is something wrong with \"calls implementation\" but it sucks cause it is a very basic feature.\n\n"
    author: "djouallah mimoune"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "Do the following:\nSettings > Configure Konqueror > File Associations\nType \"html\" in the filter line, then choose \"html\" from the text tree.\nThen, add firefox to the top of the application preference order.\nFinally, in the embedding tab, choose \"show file in external viewer\"\n\nPress apply and it should then work."
    author: "boo"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "i already done it, but it don't work, specially with a long name, and it is worst when the name has a no ascii caracter.\n\nbut in nautilus it just work"
    author: "djouallah mimoune"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "You said you're using KDE 4.0 alpha 2. Where do you obtained it ? is a Live-CD available ?\n\nThanks"
    author: "Makosol"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "If you build from svn it says alpha 2 currently."
    author: "Heja AIK"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "i am using opensue 10.2 \nhttp://software.opensuse.org/download/KDE:/KDE4/openSUSE_10.2/ "
    author: "djouallah mimoune"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "I think that's the fault of your distribution, not of KDE. KUbuntu here gives me these options."
    author: "panzi"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-03
    body: "I'm pretty sure that's not what he's talking about."
    author: "Soap"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-02
    body: "I think what you're saying is that when you try to open an HTML file with a long file name (and no ASCII characters apparently makes it happen as well?) Firefox/Opera can't open it?\n\nIf you have some examples of file names that will cause this posting a bug report with the names that'll cause that problem to bugs.kde.org would be very much appreciated.\n\nAlso if you take some non-html file (like maybe a PNG) and rename it similarly to the other files that cause problems do you get errors with it as well? (that'll be useful to help figure out whats going wrong with opening the files)"
    author: "Sutoka"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-03
    body: "thanks really guys for your feedback, that's excatly what i tried to say but as you see my english sucks, ok i'll fill a bug report, and i hope this is not a kde problem but a opensuse problem.\n\nps: sorry for the lag i have not net connexion in my home yet, f.. country side"
    author: "djouallah mimoune"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-04
    body: "Have you tried taking a look at the \"Application Command\" line to see what the call is actually doing? \"opera %U\" passes along non-ascii chars, I believe..."
    author: "Joe"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-04
    body: "Are you aware what a great many things of higher priority are not yet fixed or completed in KDE4?\n\nApart from that, do not despair, I am sure, these kinds of things will get fixed, if not before release, after it.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: opera, firefox sucks !!! "
    date: 2007-07-04
    body: "it is not about kde4, man, it 's a kde 3.x bug, i was expecting that with a new release it will be fixed, i was wrong"
    author: "djouallah mimoune"
  - subject: "The Iconcache."
    date: 2007-07-02
    body: "Will it be included in 4.0 or not?"
    author: "}('-'){"
  - subject: "Re: The Iconcache."
    date: 2007-07-03
    body: "that's the goal, but it's still yet to be seen if we (or rather, rivo =) will make it. we're very hopeful and its current looking pretty well."
    author: "Aaron J. Seigo"
  - subject: "Re: The Iconcache."
    date: 2007-07-04
    body: "Thanks for mentoring that thing, it will sure make people go wow.\n\nBesides, will it be possible to pre-populate the cache during installation of packages? My current understanding is that every user maintains its own cache, but it would be nice to also consider a read-only system made pre-populated cache. Or is there too many settings like icon effects that go into things?\n\nKay"
    author: "Debian User"
  - subject: "Re: The Iconcache."
    date: 2007-07-05
    body: "Bummer that you are building a single cache mechanism. It would be better to build a unified cache that can work over the network. In particular, a good one would be a local cache client to a another generic cache server (say squid with its own extensions to handle the icons). In taking that approach, it makes it easier to have unified cache / system as well as encouraging gnome to use it."
    author: "a.c."
  - subject: "Re: The Iconcache."
    date: 2007-07-09
    body: "Ehm, a cache over a network?!?!? sorry, but that wouldn't really speed things up, you know... It's about performance here. Gnome is working on something like this as well, there was a mail about it - we won't be able to share much, if anything."
    author: "jospoortvliet"
  - subject: "Re: The Iconcache."
    date: 2007-07-09
    body: "BTW to add to this, there are a million small caches (just keeping the result of some computation in memory = cache) in a OS, for many small things. The icon cache is only one of them, though it's a bit larger than most."
    author: "jospoortvliet"
  - subject: "Oxygen"
    date: 2007-07-02
    body: "Is there stil work being done on the oxygen widget style? Are there any screenshots of the new scrollbar?"
    author: "EMP3ROR"
  - subject: "Re: Oxygen"
    date: 2007-07-03
    body: "Work is definitely happening, however we've started to decline posting screenshots of unfinished work as frequently as people complain about them in the comments which is a demotivator.  So in the interests of getting work done: less screenshots."
    author: "Troy Unrau"
  - subject: "Re: Oxygen"
    date: 2007-07-03
    body: "Ok, that's good."
    author: "EMP3ROR"
  - subject: "Re: Oxygen"
    date: 2007-07-03
    body: "I don't think that's a totally (emphasys here) good idea.\nYes, developers hate critics and it's a de-motivator, so hiding the work sometimes is good.\n\nBut on the area of look&feel... you see developers mostly just suck at it :)\nSo user feedback is a good thing.\n\nLook at the default KDE themes and the most popular ones, they're not the same.\nFor what I recall (correct me if I'm wrong) Keramic made into KDE by popular demand.\n\nSometimes user feedback IS a good thing. Maybe you could just place some mockups of the UI and sk for feedback? Place some different and state none of them are going to be the final result."
    author: "Iuri Fiedoruk"
  - subject: "Re: Oxygen"
    date: 2007-07-04
    body: "Instead of bitching, why don't you help?"
    author: "Joe"
  - subject: "Re: Oxygen"
    date: 2007-07-05
    body: "I was more than educated on my message. Just gave MY PERSONAL opinion on the matter and made a SUGGESTION.\nHow can this be bitching?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Oxygen"
    date: 2007-07-04
    body: "The ppl working on the look are artists, and I think we should trust them and let them do their job. It's a thing about taste anyway, and interference from geeks won't make it any better."
    author: "jospoortvliet"
  - subject: "Re: Oxygen"
    date: 2007-07-04
    body: "The \"developers\" in this case are \"artists\", who have some proficiency with the look and feel thing. (They also authored the icon set). Or rather, the actual coding is being done by developers who implement the artists' specifications, but in this respect it matters little."
    author: "illissius"
  - subject: "Re: Oxygen"
    date: 2007-07-05
    body: "Sorry, I didn't knew that.\n\nAnyway ... they are too much \"artistic\" for my taste, mostly on scrollbars :)\nBut I should not say that, should I? The new interface is secret! ;)\n\nOK, just goofing a bit around, seriously, the new theme looks nice mostly, but I (notice, I, it's my taste, personal opinion only, not a saying the new look is ugly, bad, or anything like that) will imediatly replace ith for something like Qtcurve :)"
    author: "Iuri Fiedoruk"
  - subject: "the award-winning KDE Commit-Digest"
    date: 2007-07-02
    body: "Congratulations!!!  You totally deserve it.\n\nThanks for all these great commit digests."
    author: "rikrd"
  - subject: "Re: the award-winning KDE Commit-Digest"
    date: 2007-07-04
    body: "Yeah! Thank you very much, danny!"
    author: "infopipe"
  - subject: "Sonnet"
    date: 2007-07-03
    body: "I for one am really glad to see that Sonnet is being worked on again.  The project showed great progress.  One thing I particularly enjoy about open-source software is the ability to write a great library or function and allow all the other apps to use it.\n\nUnless such a feature was port of the core Windows API, you're not likely to see someone independently write a feature like this in the Windows world, and see various commercial apps pick it up or utilize it.\n\nKeep up the good work (including on putting out these digests).\n\nI can't wait for KDE 4, Amarok 2, and KOffice 2!"
    author: "T. J. Brumfield"
  - subject: "Crazy Question"
    date: 2007-07-03
    body: "As the core features of KDE 4 approach freeze, what have the KDE developers taken away from this whole process?\n\nRumor has it that after Longhorn Server, the next version of Windows will be rewritten from the group up and deliver more on the promise of this huge revolution in the way that Vista failed to (IMHO).  Windows 3.1 changed the way so many used their computers.  Windows 95 was a huge leap up again, and the 2000/XP/Vista family to a lesser extent.\n\nAs far as I understand it, somewhat the technology of QT 4 dictated some of the planning and design for KDE 4.\n\nAs soon as KDE 4 rolls out the door in October, there will be praise and complaints, and while people will need to bugfix and get ready for KDE 4.1, should there be an eye towards a distant KDE 5?\n\nIf you were designing a new system from the ground up tomorrow, how would you design it?  Don't many any assumptions on existing technology, or the framework it is built upon.  How should it operate at the core level, and the usability level?  I think KDE 4 addresses many of the core level issues, but if you really wanted to rethink things, there is no reason why the KDE hackers couldn't make suggestions for what they'd like to see out the kernel, user land tools, QT, etc. to enable them to make KDE 5 everything it could be.\n\nIn fact, I think it would be neat to have a chat with developers from GNU, Linux kernel, BSD kernel, Gnome and KDE sit and discuss what works and doesn't from all the systems out there (free and commercial) and how a dream system would be designed from the ground up.\n\nThe Linux kernel (though it adds impressive new features all the time) seems to be growing immensely in code size, complexity, and regressions.  I love new features, but there is perceived bloat about KDE (though from the benchmarks I've seen, it uses less memory and runs faster than Gnome).  This reminds me of a small tangent, but I've always loved that Linux/Gnu seems to push new technology, while at the same time providing great legacy support.  KDE is very modular, and extremely customizable.  I hope future iterations keep old hardware and scaling very much in mind.\n\nThoughts?  Suggestions?"
    author: "T. J. Brumfield"
  - subject: "Re: Crazy Question"
    date: 2007-07-03
    body: "Not an answer to your question, but noteworthy IMHO:\n\nThe most user-visible part of KDE, the desktop and the taskbar, has been entirely\nrewritten for KDE4. The old kdesktop and kicker programs have been removed, and \nreplaced by Plasma. Plasma takes care of the desktop and (soon) the taskbar.\n"
    author: "Yves"
  - subject: "Re: Crazy Question"
    date: 2007-07-03
    body: "In terms of interface, I would like to have something like KDE but with some less features (avoid the bloat) and that could run on almost everthing, including cellphones.\nSomething like qtopia."
    author: "Iuri Fiedoruk"
  - subject: "Re: Crazy Question"
    date: 2007-07-03
    body: "Use SimpleKDE"
    author: "RichiH"
  - subject: "Re: Crazy Question"
    date: 2007-07-03
    body: "Yes and no. \nIt's good but still not slim enought ;)\n\nBut thanks anyway."
    author: "Iuri Fiedoruk"
  - subject: "Re: Crazy Question"
    date: 2007-07-04
    body: "What does \"bloat\" mean, again? Do you even have a clue? No, you don't."
    author: "Joe"
  - subject: "Re: Crazy Question"
    date: 2007-07-03
    body: "I believe that KDE should build a graphics system which is based 100% on QT.  In my opinion, the Linux desktop will never be very unified until we choose one graphical widget engine and discard the others.  This may sound harsh since so many other people are putting so much work into gnome, but the longer we wait the more time and money gets waisted in a project which should be dead.  Think about our current desktops with x.org.  From my understanding, QT4 has all the power to provide every single part of the display that a desktop ever needs (fonts, buttons, pictures, 3D objects, real-time effects, direct svg rendering).  Under X, however, we have many different engines running at the same time which provide all these features for use.  Even though QT4 can provide real-time transparency for objects embedded inside an application window, X still has control of the windows, so QT can't provide transparency from window to window, we need AIGLX to do this, which, of course, cannot provide transparency with embedded objects inside an application like QT.  If we had only one engine which provided transparency, then we could have build applications which defined their own custom transparency, which would exist at only certain parts of the application window, and would have different opacity throughout.  Features like this would give us the power to create outstanding graphical desktops.  If you don't understand what I'm trying to say about transparency, look closely at a screen shot of the vista menu when it's set to transparent.  Only specific parts of the menu are transparent, while others aren't.  From my understanding we couldn't build something as basic as this even in KDE4."
    author: "BATONAC"
  - subject: "Re: Crazy Question"
    date: 2007-07-03
    body: "And then if I would say: \"Lets scrap qt and base everything on gtk+\", then we would have a disagreement, would we not?\n\nAnd then the only thing that matters is who has the strongest mind-control device.\n\nNothing bad about Qt, I like it a lot.\nBut I think a lot of the Free/Open source developers do not want to rely on a toolkit controlled by one single company."
    author: "Jimmy"
  - subject: "Re: Crazy Question"
    date: 2007-07-03
    body: "\"a graphics system which is based 100% on QT.\"\n\nThis already pretty much exists as the Qt Embedded edition (it uses the Linux framebuffer device), which is mostly geared towards embedded development at the moment I believe.\n\n\"In my opinion, the Linux desktop will never be very unified until we choose one graphical widget engine and discard the others.\"\n\nWhy should Linux limit itself to a single graphical widget engine when the other mainstream OSes out there don't (Apple and Microsoft BOTH aren't even consistent on their own OS!)  The complaints about Linux generally come from when the engines weren't at all compatible with each other and are mostly just hold overs with little REAL points left over (button order, file open dialogs are the two main things I can think of that isn't really made consistent yet, which there are already partial solutions to both).  The clipboard problems of old are gone for well behaved apps pretty much, the widget styling is mostly solved with the gtk-qt-theme engine from KDE's side, the icons will be solved with the FD.o icon naming spec (which Oxygen follows... and for some reason for the longest I thought the spec was named Tango... maybe I'm just crazy or something).\n\n\"we need AIGLX to do this, which, of course, cannot provide transparency with embedded objects inside an application like QT\"\n\nActually this is wrong, AIGLX/Xgl (Composite really) can allow parts of windows to be transparent.  The window simply (for various definitions of 'simply') set an ARGB value ('A' is for Alpha), though I'm not sure if Qt implements the required functionality to set an ARGB value.  (I may be confused about the specific method used to implement the transparency, but I do know it is possible thanks to the Composite extension)."
    author: "Sutoka"
  - subject: "I did not know this."
    date: 2007-07-04
    body: "\"AIGLX/Xgl (Composite really) can allow parts of windows to be transparent. The window simply (for various definitions of 'simply') set an ARGB value ('A' is for Alpha), though I'm not sure if Qt implements the required functionality to set an ARGB value. (I may be confused about the specific method used to implement the transparency, but I do know it is possible thanks to the Composite extension).\"\n\nHats off to you sir, if this is true, my bad."
    author: "BATONAC"
  - subject: "Re: Crazy Question"
    date: 2007-07-09
    body: "Qt supports this, indeed, by a patch (I think from Zack-the-graphics-ninja) in qt-copy. It will be in Qt 4.4 officially, of course, but KDE 4 will have it."
    author: "jospoortvliet"
  - subject: "Can't We All Get Along?"
    date: 2007-07-04
    body: "I agree but don't think it will happen any time soon.\n\nIt frustrates me to see so much redundancy in developer efforts across Gnome and KDE, instead of focusing everyone's efforts to make the best possible desktop.\n\nFrom an idealogy standpoint, there should be choice.\n\nWhat I'd love to see is one toolkit (and I'd vote for QT).  It won't happen overnight, but what if people agreed to start forking Gnome apps and adding QT options for them, much like OpenOffice.  And in some cases, developers can even talk about merging programs like Kopete and Pidgin and start working on finishing features that have been promised for years like full voice and video support.\n\nThose who are insistent on using GTK can do so, but if there was a QT branch of Gnome, then Gnome could exist effectively as a configuration of a newly merged desktop project.\n\nImagine KDE and Gnome fully merging in the new year into the Free Desktop (to be named).  When you install it out of the box, you pick your flavor.  It then installs the configurations for a Gnome-stylized interface, a KDE-stylized interface, an OSX-stylized interface, an XP-stylized interface, a Vista-stylized  interface etc.\n\nThe entire desktop runs off QT and one universal set of libraries.  All the new KDE 4 technologies like Sonnet, Solid, Phonon, etc. can be pulled into Gnome, but Gnome's UI and design can remain the same.\n\nBasically, unify all the core underlying technology, and in doing so, perhaps we'll see fewer repeated projects (like Pidgin and Kopete).\n\nOr another bridge to such an endeavor might be a universal library.  Say this library has Function X, which calls open a file dialog.  If the user is using GTK, Function X calls up that portion of code.  If the user is using QT, Function X calls up that portion of code.\n\nDevelopers code using the universal calls, and let the wrapper call out the library."
    author: "T. J. Brumfield"
  - subject: "Re: Can't We All Get Along?"
    date: 2007-07-04
    body: "Awesome ideas man! Awesome! Merging projects! That's exactly what we need. Maybe the unification of Beryl and Compiz will be an encouragement to other competing projects to unite, and we can start the *age of the unification of the Linux desktop.* "
    author: "BATONAC"
  - subject: "Re: Can't We All Get Along?"
    date: 2007-07-04
    body: "Hello,\n\nplease understand that the only reason that Beryl and Compiz can merge is that Beryl is a fork (that means, the same code base changed in different ways) and that was not too long ago. Only due to that the merging is possible.\n\nFor projects like QT and GTK, they don't even use the same programming language. \n\nOr if we look at Solaris and Linux, there is practically barely almost nothing that can be shared, except very peripheral things, like a driver after adopting it a lot.\n\nIf you want to suggest QT and GTK to merge, you fail to understand a lot of things. First QT is in its entirely the sole possession of Trolltech, which they license under GPL (and promised to do forever more) and other licenses at their choice. In order to be able to do that, they cannot accept outside contributions, but instead have to do every patch they receive themselves again. \n\nAny code currently in GTK is therefore entirely out of question for legal/business reasons alone.\n\nFurthermore, QT is programmed in C++, and GTK in C. I doubt you can find any single line that is the same. Or even interfaces that are halfway similar. C and C++ differ, a lot. And it's not just because of extra syntax. Things get solved in different ways therefore. So you can't merge for that programming language difference.\n\nThe design of QT and GTK are entirely different. How \"things that happened\" are communicated, there is no commonality. How user interfaces are made up, even how many of the widgets work, i.e. how they are to be influenced by the application, is entirely different. That is a technical barrier absolutely not to overcome.\n\nWith KDE and Gnome, things are similar. Only where one isolated job can be solved in a stand alone library, the sharing becomes possible. But KDE and Gnome are about frameworks. They are inter-connecting all their functions for the sake of integration of all their functions with another.\n\nSo even the sharing of the file open dialog becomes nearly impossible, or too hard to achieve in the correct way.\n\nSo a merge of the projects would mean to\n\na) Decide which one to give up as a general consensus (impossible).\nb) Convince those that had their reasons to pick the programming language, to change their mind (near impossible).\nc) Convince those that knew everything about their project to learn everything from the ground up anew (near impossible).\n\nNotice how nothing of this applies to the merging of a fork. Nobody has given up anything in Bery/Compiz, except for project name, and the fork that made their code GPL had to agree to relicense it to BSD as they had forked it.\n\nSo really, Apache and Zope, KHTML and Firefox, Solaris and Linux, there is overlap in projects, but it's not possible to merge them. Never ever. I welcome anybody to point out how unrelated projects ever merged if there were more than 3 developers or 10k lines of source code.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Can't We All Get Along?"
    date: 2007-07-04
    body: "Making GTK apps use Qt would pretty much require a rewrite of all the GTK apps.  Merging really isn't the solution, and despite the split man power, having an alternative is a good thing, it creates (friendly) competition between the desktops, and each will inevitably try different things.\n\n\"Or another bridge to such an endeavor might be a universal library. Say this library has Function X, which calls open a file dialog. If the user is using GTK, Function X calls up that portion of code. If the user is using QT, Function X calls up that portion of code.\"\n\nThere is already a project doing this, though I believe it uses a combination of dbus and shell scripts to prevent apps from having to link against too many things."
    author: "Sutoka"
  - subject: "Confused about RAW file format support"
    date: 2007-07-04
    body: "I read that KPhotoAlbum has \"support\" for Pentax PEF raw format and I must admit that I do not know what is meant by this.  Perhaps all this means is that it is possible to display RAW format files without converting them to JP2 with some standard coversion.  This would be a useful feature, but I think that some term other than 'support\" would be more appropriate.\n\nAs a serious photographer, I would like to see a KDE application that could convert from the various camera RAW formats to the Adobe CFA format (DNG), and an application that could (interactively) make a 24 bit bitmapped format from it.  Actually, it would be more efficient to convert various RAW formats to DNG and then have the ability to display DNG in KPhotoAlbum."
    author: "James Richard Tyrer"
---
In <a href="http://commit-digest.org/issues/2007-07-01/">this week's KDE Commit-Digest</a>: <a href="http://conference2007.kde.org/">Akademy 2007</a> kicks off in Glasgow, Scotland. Continued work in <a href="http://plasma.kde.org/">Plasma</a>, with improvements in the Photoframe and Dictionary Plasmoids, and the addition of ChemicalData, <a href="http://pim.kde.org/akonadi/">Akonadi</a> and Battery Plasmoids. Support for <a href="http://solid.kde.org/">Solid</a>-based network status support in <a href="http://www.mailody.net/">Mailody</a>. Support for multiple blogs in KBlogger. Automatic downloading of map tiles in <a href="http://edu.kde.org/marble/">Marble</a>. Theming support added to KBounce. Load and Save support in Kollagame, a game development IDE. More work in the Kaider translation utility. Support for the PEF raw format for Pentax cameras in <a href="http://kphotoalbum.org/">KPhotoAlbum</a>. KPhotoAlbum begins to be ported to KDE 4, with more progress in porting <a href="http://www.digikam.org/">Digikam</a> to KDE 4. Initial work in the <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=C3E84FB73E7DE5F8">OpenPrinting</a> and <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=A605EEA634DA5998">Context Sensitive Help</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> projects, with continued work in the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9064143E62AF5BA6">KRDC</a> project. Initial steps toward high-precision computing support in <a href="http://www.koffice.org/kspread/">KSpread</a>. Attempts made to ensure Sonnet is ready for inclusion in KDE 4. Systemsettings moves to kdebase for KDE 4.

<!--break-->
