---
title: "KDE 4.0 Beta 4 and Development Platform RC Released"
date:    2007-10-31
authors:
  - "jpoortvliet"
slug:    kde-40-beta-4-and-development-platform-rc-released
comments:
  - subject: "Full Gears Ahead!"
    date: 2007-10-31
    body: "Thanks to all contributors for this release!\n\nKDE 4.0 is really shaping nicely, and with all the new API additions both in kdelibs4 and Qt4, the future of KDE 4 will shine.\n"
    author: "christoph"
  - subject: "Re: Full Gears Ahead!"
    date: 2007-10-31
    body: "Agreed. I'm quite impressed with beta4 here at openSUSE 10.3. The apps are amazing. Start up fast, good graphics, subtle animations everywhere, cleaned up interfaces and all make a good first impression. :-) I'm starting to like the Oxygen style more and more too.\n\nThe only two apps which feel a bit behind are kwin and plasma. Plasma still manages to crash now and then. KWin has a performance problems with compositing, which is a major issue for first impressions. If KWin is slow, every app feels slow because they rely on KWin to repaint.\n\nThere are still random bugs everywhere, but I haven't had any crashes except for plasma. It's the kind of bug you can expect with a beta. The major stuff is working, feels workable, and makes me really happy :-)"
    author: "Diederik van der Boor"
  - subject: "Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "I know the panels are a work-in-progress. Does anyone has any info how the work-in-progress panels will ultimatly look like. Are there mockups?"
    author: "Don"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "thats how nuno thinks the panels should look like:\nhttp://www.nuno-icons.com/images/estilo/image219.png"
    author: "chris"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "afaik they've disagreed with nuno's vision for the panel. They are aiming for the panel they've got at the moment and just improve the style a bit and add the remaining features (the features from KDE3).\n\nI really hope they get a good panel soon because the one they've got at the moment is just ugly (imo)\n\nFurther I hope I'll get my KDE4 pc ready early to help developing KDE (Debian). But I have some problems with starting the applications (I've compiled kdesupport, kdelibs, kdebase and kdegames), but I can't start any of the apps (I've followed the tutorial from techbase). I hope somebody knows what's going wrong here (he can't open the display (I've already tried to export the Display variable)"
    author: "Bernhard"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "You might try xinit:\n\nxinit startkde -- :1\n\nWill start a KDE desktop in a new screen (ctrl-alt-F8).\n\nIf you want to run KDE 4 applications from within your KDE 3, I've done the following:\n\nInstall KDE 4 as user 'kde4', including all .bashrc settings of course.\nUse 'sux - kde4' to go to the kde4 user from within konsole in KDE 3 and then just start applications..."
    author: "jospoortvliet"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "first thx for the answer! it hasn't worked out completely but it's a first step.\n\nHere my situation:\n\nI'v got a seperate user (kde-devel), I've edited the .bashrc like stated on techbase. With the sux - kde-devel command I can start kde4 programs (but some programs report errors (e.g. ksysguard (no localhost found)). (if that's important: atm I'm running a gnome session because I didn't want to mix up KDE3 and KDE4 environment so I simply didn't install KDE3 (a fresh debian system)\n\nAnd I still don't know how to start a seperate X- Session because xinit startkde -- :1 gives me an error that I haven't got the permissions to do that -.-\nAnd I can't start it as root because that he dosn't find the paths."
    author: "LordBernhard"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "You could try to start a new session using sudo - that would give you all the permissions while retaining the paths.\n\nI prefer to use the xephyr-based solution that is described in the wiki as well - that won't let you use composite (afaik), but won't crash your other session if kwin b0rks while switching the sessions (yes, that used to be a known bug). That way I can check if it's already stable enough to be run in a \"real\" session while keeping me save and warm.\n\nOh, and to get rid of the remainings of any old build you may have installed, you should run rm -rf ~/.kde4/ every now and then."
    author: "Tom Vollerthun"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-11-01
    body: "Xephyr supports composite, and I believe newer versions support using Indirect GLX (instead of doing it entirely in software).\n\nYou might need to do +extension composite to get it to work (I don't remember the exact command I used yesterday, but I did have kompmgr (KDE3's old xcompmgr style composite manager) running in it perfectly fine."
    author: "Sutoka"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "You don't mention running the \n\nxhost +local:kde-devel\n\ncommand.  Run it as a user who is able to startx e.g. your usual user."
    author: "Anon"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "I would like to see some external Plasmoid which will implement Nuno's vision ;]"
    author: "Kompo"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "\"afaik they've disagreed with nuno's vision for the panel.\"\n\nIf we ask people (i mean users...) to choose between the nuno vision and the ugly actual one, the result should be simple and clear... So, how the disgusting current one can be keept? Who is \"they\"? (just to know).\n\nNuno Pinhero developped very innovative and fresh vision for KDE4 (panel, kick ass tabs design http://www.nuno-icons.com/images/estilo/new%20style%20tabs.png , kwin buttons, tables...) and i'm really sad to see these ideas are not here anymore... I tought at first it was just a work in progress issue, but it seems we finaly are going to have an ugly panel, and a conservative look... and not because we are missing talented artists. \n\n\n"
    author: "Johann Ollivier-Lapeyre"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-11-01
    body: "He who codes, decides. It's that simple. If you don't agree with what's being done, start coding yourself and you can the direction the project is taking."
    author: "Andr\u00e9"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-11-01
    body: "\"He who codes, decides. It's that simple.\"\n\nAnd the artist who do the artwork must just shup ut? and the guy specialized in usability must shut up too? Just because they have to be developpers to be listened? Stupid and explain very well why many OSS projets are ugly and un-usuable like 80's softwares...\n\nHopefully, KDE coders i worked with [kopete, kdegames..] doesn't have a such giant ego, and this is more the \"who know and do the work, decides\" rules, in each area, everyone respecting the other's work and expertise.\n\n\n\n"
    author: "Johann Ollivier-Lapeyre"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-11-02
    body: ">> And the artist who do the artwork must just shup ut? and the guy specialized in usability must shut up too? Just because they have to be developpers to be listened?\n\nNo, but in the end it comes down to who is doing the work.  It has nothing to do with not respecting non-developers, but when push comes to shove, a mockup is no use to end users.  It requires a developer to put in a hell of a lot of time to implement that mockup (don't get me started on how mockups always cherry pick their displayed content to make it look good).  \n\nMockups are great, and there are a ton of them, but implementation is much harder, and without implementation there is nothing."
    author: "Leo S"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-11-02
    body: "\"Stupid and explain very well why many OSS projets are ugly and un-usuable like 80's software\"\n\nBut does not explain why is _every_ X window manager I have ever tried more usable than Windows GUI. :-)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "\"afaik they've disagreed with nuno's vision for the panel\"\n\nthis just keeps cropping up everywhere, doesn't it?\n\nwhat we're not going to be doing is putting the panel in the center of the screen with the launcher interface button (e.g. kickoff/kmenu/lancelot/whatever) in the middle of that.\n\nthe look is a completely different issue and no we won't be keeping the look of the current panel. it's ... ugly. and we all know that. it was a quick hack to get a panel there to get the rest of the stuff needed for proper panels in place.\n\nand yes, if you don't like what we come up with you can always write your own Containment plugins."
    author: "Aaron J. Seigo"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "\"the look is a completely different issue and no we won't be keeping the look of the current panel. it's ... ugly.\"\n\nHappy to read that from you. I admit i started to be afraid reading this post ;)\n\n\"what we're not going to be doing is putting the panel in the center of the screen with the launcher interface button (e.g. kickoff/kmenu/lancelot/whatever) in the middle of that.\"\n\nI'm not talking about 4.0 (time...),  but with the \"Plasma power\", this idea, not  perfect for now, could be experimented and matured... don't you think?"
    author: "Johann Ollivier-Lapeyre"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "if you want to experiment with it, write your own containment.\nisn't that the point of plasma? :) everyone has the chance to make their own stuff and play with it. which things end up being the defaults depends on what actually ends up being usable.\nor, y'know, you could help make the existing panel suck less for 4.0... whatever."
    author: "Chani"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "\"isn't that the point of plasma? :)\"\n\nIn part yes, this is why Plasma is a great piece of software :D\n\n\"which things end up being the defaults depends on what actually ends up being usable.\"\n\nYes, but it depend also of leaders's view like Aaron, even if at end it is usable.  If for him, this is a big \"never in the center\", this could just be a waste of time. Hopefuly, he is not like that, as i know, so i'm confident to have great defaults which fit the users's need.   \n\nAnyway, it seems after some talk on IRC there are already some guys working to \"make the existing panel suck less for 4.0\", so i prefer to continue to make the games suck less ;)\n"
    author: "Johann Ollivier-Lapeyre"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-11-01
    body: "\"what we're not going to be doing is putting the panel in the center of the screen with the launcher interface button (e.g. kickoff/kmenu/lancelot/whatever) in the middle of that.\"\n\nwhy not? the panel in the center is cool, the k-menu in the middle, don't know...\n"
    author: "david"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "Looks very much like yet-another-CDE-Imitation to me\u0085 *sigh*\n\nDefinitely not something I'd like as the default look."
    author: "Deucalion"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "That looks very cool.  Even if it isn't supplied as default someone should code it (plasma is supposed to be very easy to code, right?) and make it available as a replacement.  If it's liked by enough people I expect the distributions will package it up."
    author: "Adrian Baugh"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "Nuno's panel looks really beautiful, I hope that something like that will be implemented instead of the present one."
    author: "Bobby"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "Glad to hear it's a WIP.  I thought for a moment that they were settling on this taskbar style. For me, the main problem is that the icons are too big for the taskbar -- so much so, that they look mis-rendered(!).  I'm guessing a lot of functionality is still missing from the task bar too.\n\nThat said, KDE4 is shaping up quite nicely now, which I'm very happy to see.  They said it couldn't be done guys, but you're making it happen.  Nice job :D"
    author: "Lee"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "\"a lot of functionality is still missing from the task bar too\"\n\nyep. taskbar and systray both."
    author: "Aaron J. Seigo"
  - subject: "Re: Damn, still that ugly taskbar!"
    date: 2007-10-31
    body: "Hmmm, I really think we should be mentioning stuff like that in the release announcements or on the Dot, with a link to a more detailed \"what works / what doesn't / whats coming next\" feature list on the Plasma wiki.  I see so many comments/blogs/e-mails from people who keep expecting stuff to be there, go in and find it isn't, and walk away loudly complaining about how bad the betas are.  We need to try manage those expectations through education.\n\nIn fact, that leads into the bigger issue of addressing expectations for 4.0 itself, there's a noisy minority out there that seems to have the expectation that KDE4 will be born fully-grown, and they seem to be getting louder with each beta.  I'd love to see a 'Truth in Advertising' series of articles on the Dot explaining what will be in 4.0 and (more importantly) what won't be.  It could start with an initial article explaining the whole \"4.0 as foundation, 4.1 as realisation\" thing, followed by an article for each module highlighting each apps status, what's in and what's out for 4.0, and what's been deferred to 4.1."
    author: "Odysseus"
  - subject: "Oxygen Consistency"
    date: 2007-10-31
    body: "I wished there had been more of a unified artistic vision from the early stages.  I assumed with the Oxygen team that such a vision would have been laid down early.\n\nThe glass border around the plasmoids looks great, but in turn, the window decorations around the actual Kwin windows look out of place.  (I'd love to see a widget style that better complements the glass plasma theme!)  The oxygen theme for widgets has lacked contrast, as has been reported repeatedly, but the splash of color that is thrown in is green, which is an odd departure from the traditional KDE blue.  Then you get an odd black Krunner look which just stands out, and doesn't seem to mesh with anything else.\n\nThe other complaint I've often heard (and mirrored myself) is the white space, or empty space seen all over the place in Oxygen.  Oddly, some areas of the KDE 4 desktop seem nice and tight, and aspect's of Nuno's mockups seem nice and tight (though there is plenty of white space used in the mockups in other areas as well).  This is also inconsistent.\n\nIndividually some of these components look nice, but shouldn't the widgets, plasma borders, window borders, dialogs, and icons all have had a consistent design scheme?\n\nBugfixes and showstoppers take priority, but I seriously hope 4.1 features a more unified visual theme across KDE, and I'd love it to closely match Nuno's mock-ups."
    author: "T. J. Brumfield"
  - subject: "Re: Oxygen Consistency"
    date: 2007-11-01
    body: "I totally agree with you!\n\nA decade down the line and KDE doesn't get this yet.\n\nWhen are we going to move away from hack-n-slash to sleek-n-tight?"
    author: "Oxygen blows!"
  - subject: "Re: Oxygen Consistency"
    date: 2007-11-01
    body: "> When are we going to move away from hack-n-slash to sleek-n-tight?\n\nIs this the royal \"we\" perchance?"
    author: "Paul Eggleton"
  - subject: "Kubuntu packages ?"
    date: 2007-10-31
    body: "Anyone knows if Kubuntu will provide packages for those willing to play with it ? Will it install side by side ?\n\nThanks in advance ! Cheers!"
    author: "KubuntuUser"
  - subject: "Re: Kubuntu packages ?"
    date: 2007-11-07
    body: "I wonder why not? how long did it take to get beta3 on kubuntu?\n\nThis will make a lot more bug-finders..."
    author: "student"
  - subject: "Background Picture"
    date: 2007-10-31
    body: "First I would like to say a big thank you to all the developers who are so industrious and creative in this wonderful project.\nKDE4 Beta 4 is a very big improvement over the last release. it's much faster and seems to be more stable. I am even getting the desktop effects but there is no desktop background. Beta 3 had a wonderful flower but it's now gone and I don't even know how to add a background picture. Anyone has an idea?\nThanks in advance."
    author: "Bobby"
  - subject: "Re: Background Picture"
    date: 2007-10-31
    body: "> no desktop backgroun\n\neither a setting your plasmarc, a bug in your packages/installation or DesktopContainment isn't working.\n\n> know how to add a background picture\n\nthe configuration dialog for this is still pending."
    author: "Aaron J. Seigo"
  - subject: "Re: Background Picture"
    date: 2007-10-31
    body: "I'm missing a desktop background too (with opensuse packages from yesterday) (although the svn build from today seems to be ok).\n\nI wonder where the desktop plasmoid has gone. Is it going to get replaced so that you >must< have a desktop or will it be a plasmoid in the future (as before)"
    author: "Bernhard"
  - subject: "Re: Background Picture"
    date: 2007-11-01
    body: "I updated the packages a few hours ago but all I have is the same black background. Just out of curiosity though, what's the background for beta4? The beta3 background was breathtaking. \nThis realease has improved a lot over the one before. Great job and thanks for your dedication and hard work."
    author: "Bobby"
  - subject: "Is it ready?"
    date: 2007-10-31
    body: "I'm a huge KDE fan, but frankly, calling this a \"beta\" is a joke. Unless some miracle occurs in the next two months, I'm not expecting a usable KDE4 in January. We may get a damned good development platform, but for a desktop I'll probably be sticking with 3.5.x.\n\np.s. I expect 4.1 to totally rock, however."
    author: "David Johnson"
  - subject: "Re: Is it ready?"
    date: 2007-10-31
    body: "Well, that is the plan actually.  KDE 4.0 will have a usable desktop, 4.1 will have the full desktop.  We need to ship the dev platform, otherwise the devs will never have a stable platform to target.  Remember, the apps only rely on the dev platform, not the desktop.  Many apps in 4.0 are only simple ports to the new platform, most of the creative new stuff will come in the 4.1 and 4.2 release cycles.  You can actually run KDE4 apps under the KDE3 desktop, and vv, so choose which mix of desktop and apps best suits your personal needs.  Me, I have faith that Aaron and the plasma gang will deliver a desktop in 4.0 good enough for my daily use."
    author: "Odysseus"
  - subject: "Re: Is it ready?"
    date: 2007-11-02
    body: "I figured that was the case, but everytime I see a KDE press release, it sounds like 4.0 is going to have a fully usable and stable desktop. It may have been better to tag this unconventionally as 3.99."
    author: "David Johnson"
  - subject: "Re: Is it ready?"
    date: 2007-11-05
    body: "I completely agree.  I get the impression they want the best of both worlds - getting \"4.0\" out the door, while being able to respond to any criticism with \"it's not finished yet!\"  You can't have it both ways.\n\nIf they want to release an incomplete, unfinished version, it should be labelled 3.9, because no matter how many times they say that 4.1 is the \"real\" finished version (and they don't seem to be saying that at all in their press releases, but only in response to complaints) everybody is going to expect a feature-complete, stable 4.0.\n\nYou can't have your cake and eat it too.  If you want to release something early that isn't feature-complete, then don't call it 4.0.  If you want to call it 4.0, then deal with all the people who expect it to be feature-complete without telling them that they are wrong for expecting a feature-complete 4.0 when you've been busy with press releases telling everybody how great it's going to be.\n\nAt the moment, it seems to be the following:\n\nAre you going to praise KDE?  If so, then 4.0 will be an amazing advancement and the best thing ever!\n\nAre you going to complain about missing features and crashes?  If so. you are silly for expecting 4.0 to even run, you should wait for 4.1!  Everybody knows that 4.0 is not a finished product.\n"
    author: "Jim"
  - subject: "Re: Is it ready?"
    date: 2007-11-06
    body: "KDE 4.0 will be a complete and usable desktop. It will have some regressions compared to KDE 3.5.x, but it won't be any worse than Vista, and ppl pay hundreds of dollars for that ;-)\n\nall in all, it will be the typical 4.0 release, indeed."
    author: "jospoortvliet"
  - subject: "KDE Four Live also updated"
    date: 2007-10-31
    body: "In addition to the openSUSE packages, an updated \"KDE Four Live\" Beta 4 was also released right on time for the release: http://home.kde.org/~binner/kde-four-live"
    author: "apokryphos"
  - subject: "Re: KDE Four Live also updated"
    date: 2007-11-01
    body: "\n[reposted from my comment on lwn - maybe people here will find it useful too]\n\nTo test the liveCD with KVM, you need to do some trickery to avoid KVM \naborting at start up. My Core Duo machine runs at a quite acceptable \nspeed.\n\nas root\n# mount -o loop /home/user/.../KDE-Four-Live.i686-0.6.iso /mnt/\n\nas normal user\n  create dummy hda file\n$ dd if=/dev/zero of=/tmp/hda bs=1M count=1\n  run KVM\n$ kvm -hda /tmp/hda -cdrom KDE-Four-Live.i686-0.6.iso -m 512 -boot \nd -kernel /mnt/boot/i386/loader/linux -initrd /mnt/boot/i386/loader/initrd\n\nTo run the live CD at a nice resolution:\n1. load KVM and wait for KDE to load\n2. press Ctrl-Alt-2, type \"sendkey ctrl-alt-f1\", then press Ctrl-Alt-1\n3. log in as root and edit /etc/X11/xorg.conf eg with vi\n4. in section monitor change HorizSync to 30-90 and delete UseModes line\n5. delete Section \"Modes\"...EndSection\n6. Change all Modes \"800x600\" to Modes \"1024x768\"\n7. Save xorg.conf, then press alt-f7\n8. Press Ctrl-Alt-2, type \"sendkey ctrl-alt-backspace\" then ctrl-alt-1\n\nTo get the network working, open a root terminal, and do dhclient eth0\n\nHopefully you should now have KDE4 in a usable 1024x768"
    author: "cbcbcb"
  - subject: "PulseAudio and Phonon?"
    date: 2007-11-01
    body: "How will KDE 4 work with Pulse Audio? I know it will be included by default in fedora 8. PulseAudio seems very promising - will it become integrated into Phonon?"
    author: "Darryl Wheatley"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-01
    body: "To add another question, what is the relation of a sound server and a multimedia framework? Multimedia framework provides an API to play audio/video and a sound server manages audio devices, but what is their relation to each other? Do multimedia frameworks depend on a specific sound server?"
    author: "Skeithy"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-01
    body: "Phonon provides an API to applications for playing sound and video.\nIt in turn has backends that actually do the decoding and playing. \nOne of the backends that exists uses xine and xine-lib to do the decoding and playing....so when used with this backend, Phonon will support any file type that xine can play.\nThere's a backend for gStreamer that's in progress. I presume that for KDE apps on Mac and Windows there will need to be an appropriate backend built for those too (perhaps the xine-lib version will just work on those platforms...I don't know)\n\nEach of the backends will have it's own configuration as to how it gets the data to your ears - they will support whatever sound-servers or devices that the backend is built on....so if you want to use pulseaudio, and the backend you're using supports pulseaudio for output, it should be possible.\n\nI know that xine supports pulseaudio (because I use it with an LTSP setup ) so everything should \"just work\"!\n"
    author: "bonybrown"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-01
    body: "Phonon provides an api to standard applications.  Then depending on the system, phonon can use a variety of backends to decode the audio e.g. xine,gstreamer...  Xine/gstreamrer then output to a soundserver which mixes it and sends to the soundcard."
    author: "Thomas"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-01
    body: "Sound servers are usually used for either mixing sounds from different source into one, probably applying effects to each source, or for sending the data somewhere else, e.g. over the network to a partner sound server.\n\nMultimedia frameworks do not need a soundserver, i.e. it is not a technical requirement.\nThe mixing can also be done in the audio driver or the audio hardware itself, things like redirecting sound can also be handled by the framework itself.\n\nUsually multimedia frameworks therefore provide several output options, e.g. directly using the sound device and a couple of sound servers.\n"
    author: "Kevin Krammer"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-01
    body: "Not directly. PulseAudio doesn't do any decoding, but PulseAudio can be used as an output device for things like xinelib, gstreamer, and mplayer (I think gstreamer has a pulseaudio backend, maybe xine, not sure about mplayer)."
    author: "Sutoka"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-01
    body: "I'm confused. If you look at the screenshot of the various pulseaudio config utilities at http://0pointer.de/public/pulse-screenshot.png (warning: biggish image), you see stuff like PulseAudio Manager which lets you \"view & modify the Daemon's internals\", a device chooser (I guess phonon already covers this?) and Volume Control, which lets you configure \"sinks\" and have different volume levels for the various applications and computer speakers. I'm guessing Phonon will be doing this anyway?\n\nIn an interview with the Fedora man responsible (http://fedoraproject.org/wiki/Interviews/LennartPoettering), he talked about it being all about \"ear candy\" and a \"compiz for sound\" e.g. active window has sound at 100%, other window's sounds are at 20%; clicking left button makes sound come out of left speaker and same with right. Will Phonon offer similar features or will it need PulseAudio to offer this functionality?\n\nWhat I don't understand is, if pulseaudio is a sound server, won't KDE 4 need one too? Or is Phonon going to let us choose both the media engines (xine/gstreamer/mplayer/nmm) and sound servers used?"
    author: "Darryl Wheatley"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-01
    body: "\"active window has sound at 100%, other window's sounds are at 20%; clicking left button makes sound come out of left speaker and same with right\"\n\nThis will also need support by the windowmanager etc, but I don't think there should be any problem for Phonon to support this. As long as the underlying media engine/soundsystem support mixing there is nothing special about it.\n\nThat said, I'm not particularly impressed with what I read abut PulseAudio in that link. Actually It does not offer anything new comparing to what aRts does(A better design perhaps, but that should be expected compared to something as old as aRts). \n\n"
    author: "Morty"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-02
    body: "Forgive me for not understanding, but does Phonon actually use the backend (xine, gstreamer etc.) only for decoding and then bring the sound back in and output it to the final destination (e.g. alsa)? Or does it let the backend do the actual sound output too? It was my understanding that the latter was the case.\n\nIf this was provided then surely Pulse adds a lot to the table? Using a Pulse as the final output device would allow for easy hotplugging of audio devices and seamless output. With pulse you could easily start playing a tune in Amarok and have it come out your laptop's speakers, plug in a USB soundcard and then move the playing stream across to it. No need to stop the music and reconfigure things and play it again? Perhaps phonon can do this without pulse (e.g. it has it's own callback based API) and if so then I guess it doesn't add too much other than network device transparency, simultaneous output to all your local sound devices and simultaneous output to all your network sounds devices... which although very cool, it not of interest to most average joe users."
    author: "Colin Guthrie"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-05
    body: "The callgraph actually looks like:\nPhonon -> xine-lib/GStreamer -> PulseAudio -> ALSA\n\nXine-lib or GStreamer are used to implement the Phonon backend. (KDE 4.0 will come with Phonon-Xine. As far as I know, Trolltech is working on a GStreamer backend for Phonon.) They handle decoding using a plugin framework, then hand off the decoded stream to either the hardware or a sound server such as PulseAudio through an output plugin.\n\nPulseAudio can also work as an ALSA plugin, in which case the applications think they're talking to a native ALSA device, but what they get is not a hardware device, but a connection to PulseAudio. This is very similar to how dmix works. In a Phonon context, this makes the callgraph look like this:\nPhonon -> xine-lib/GStreamer -> ALSA (PulseAudio plugin) -> PulseAudio -> ALSA (hardware device)\nI've read reports that xine-lib's native PulseAudio output plugin currently isn't very stable, so with xine-lib using the PulseAudio ALSA plugin is more reliable. Fixing this in xine-lib is on the PulseAudio TODO list, but in a KDE 4 context it might become moot with the Trolltech-backed Phonon-GStreamer anyway."
    author: "Kevin Kofler"
  - subject: "Re: PulseAudio and Phonon?"
    date: 2007-11-01
    body: "In Phonon you select what backend you want to use, the backend will turn your mp3 and ogg into actual sound. \n\nYou can also select where you want that sound to end up, providing the backend supports it, soundcard, headphones, Pulse, JACK.\n\nScreenshot here: http://dot.kde.org/1170773239/1170778900/1170862970/1170863051/kcmphonon5.png\n"
    author: "Ben"
  - subject: "Great Release but I am not able to run it"
    date: 2007-11-01
    body: "The release was very fast from beta 3. Great work KDE team.\nI am sorry to say I was not yet able to get a glimpse of KDE 4. I installed KDE 4 (beta 4) through opensuse YAST but whenever I start KDE 4 I get the error \"startkde: Call to lnusertemp failed (temporary directories full?). Check your installation.\" Whenever I run any KDE 4 application via KDE 3 then I get another error. This is the error I got for Dolphin \"dolphin: symbol lookup error: /usr/lib/libdolphinprivate.so.1: undefined symbol: _ZN9KDirModel10insertRowsEiiRK11QModelIndex\". This is what I get for KReversi \"kreversi: symbol lookup error: /usr/lib/libkio.so.5: undefined symbol: _ZN11KConfigBase8rollbackEb\" . Any idea whats happening?"
    author: "Swaroop"
  - subject: "Re: Great Release but I am not able to run it"
    date: 2007-11-01
    body: "Try by uninstalling what you have installed (all KDE4 packages) and then install the kde4-meta-default package from he openSuse site using their one-click install. It will work after that and you will get updates in the future."
    author: "Bobby"
  - subject: "Re: Great Release but I am not able to run it"
    date: 2007-11-03
    body: "Same result for me:\n\nI need to uninstall beta3, delete .kde4 directory in /home and re-install kde4."
    author: "Volker "
  - subject: "Re: Great Release but I am not able to run it"
    date: 2007-11-10
    body: "I have the same problem with clean fresh install from the OpenSuse 1-point click in Opensuse 10.3. I have the same problem as root or regular user, unable to try KDE4 as a result. So far Google showen several suggested solutions (including deleting ~/.kde4 and reinstalling fresh) but none seem to work.\n\nIf anyone found a solition please share,\n\nThanks Milan"
    author: "milan zimmermann"
  - subject: "Re: Great Release but I am not able to run it"
    date: 2007-11-11
    body: "Make sure that none of the Suse DVD packages are installed before installing the new packages. \nI used the search function in YaSt to find and delete all old KDE4 packages before installing the new ones using the openSuse one-click install and it worked fine. I used the default installation without adding extra packages at first.The only problem that I have is that the background picture is missing and I don't know how to add one. Some of the widgets from the previous version are also missing (in my installation) even though I installed the plasma extra gears."
    author: "Bobby"
  - subject: "KDE4 beta4 live CD"
    date: 2007-11-01
    body: "The Debian KDE Team now provides a KDE4 Live-CD.\nhttp://pkg-kde.alioth.debian.org/kde4livecd.html"
    author: "cheko"
  - subject: "Re: KDE4 beta4 live CD"
    date: 2007-11-01
    body: "Tried that with qemu. Really nice way to sneak peak whats coming up. Overall it looked nice. Couple of annoyances still.\n1. There seem to be too much space everywhere, gui should be tighter.\n2. The K-menu system reminds me of Windows vista in a bad way. Hopefully it will be somehow improved when 4.0 actually comes out."
    author: "Petteri"
  - subject: "Re: KDE4 beta4 live CD"
    date: 2007-11-01
    body: "Thank for mentioning the dead space everywhere.  I really hope this gets tightened up.\n\nKickoff isn't going to be redesigned, but there is some talk about reinventing the traditional KMenu as an option as well, and I sure hope that makes the cut for 4.1"
    author: "T. J. Brumfield"
  - subject: "Re: KDE4 beta4 live CD"
    date: 2007-11-01
    body: "Congrats to the Debian KDE Team, I tried it and in general it looks sweet, however the default font settings could be better, for example, instead to use a bitmap font use DejaVu or Liberation.\nAlso I liked a lot Gwenview, it is easy and fast, and the zoom widget for the previews at the bottom is cool. Okular it is amazing also.\nSystemsettings it is way better organized than what KControl was, although some modules have bigger screen size than the default systemsettings size and create ugly scroll bars.\nIn general I liked Kickoff, but I would prefer to click to change the tab view and not on mouse hover.\nOxygen style and windeco are great but I think that the windeco if it \u00a1s not using a composite manager with drop shadows it should have better window border contrast, because it is too thin and sometimes it create some confusion.\n\nIn general this beta has a very good feelings, and I think that some people who think to not use KDE4 before 4.1 should be surprised with 4.0."
    author: "Josep"
  - subject: "Re: KDE4 beta4 live CD"
    date: 2007-11-02
    body: "> The Debian KDE Team now provides a KDE4 Live-CD.\n\nBut the Debian KDE Team couldn't spend the 30sec it would have taken to mention username and password somewhere on the login screen? Great Job!\n\nThankfully you're automatically logged in on the console. \nResolution and refresh rate are completely fscked up, changing the resolution zooms in on the middle of the desktop and moving the mouse to the edges of the screen to pan doesn't work so there's no apparent way to access panel or plasma toolbox.\n\nDolphin crashes when you hover over a character device (don't know if it does it only if the info pane is on...), the oxygen style is slower than anything I've ever seen and for some reason the Alt key works in KDE but not in X11 shortkeys.\n"
    author: "ac"
  - subject: "Re: KDE4 beta4 live CD"
    date: 2007-11-05
    body: "The openSUSE Live-CD is better: auto-login, default to Oxygen style and win-deco, has more applications (Debian misses even stuff like KMail, KGet, any multimedia apps)."
    author: "Anonymous"
  - subject: "GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "With reference to the screenshot showing the 4 beta...\n\nIs there no end to the SI nonsense labelling disk and file sizes in gibibytes, mibibytes and kibibytes?  In 3.5.x, the SI units are thoroughly ignored in favor of GB, MB and KB *AS GOD INTENDED*.  If anyone ever gets me to actually say \"gibibyte\" out loud, I will cut out my own tongue with a rusty MRSA tainted axe.\n\nGo ahead, say those units out loud.  Say 'em while looking in a mirror.\n\n\"My 500 gibibyte disk has 340 files of mibibyte and kibibyte sizes\"\n\nPLEASE STOP THIS MADNESS!  JUST SAY NO TO SI!\n\n--\nBMO"
    author: "Boyle M. Owl"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "I do agree on the \"it's a bit confusing for average users\", I don't think that this is the right place to complain, though. Makes it looks like an armchair developer (with tongue in place, still).\n\nIt's perfectly suited for the kde-usability list."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "Do you actually say gigabytes or megabytes? Don't you abbriviate it? I think most people do. So gigabytes becomes gigs and megabytes is megs. Right?\nWhy couln't you do that to gibibytes and mibibytes? Gibis? Mibis? Kibis? \nOr why not Gibs, Mibs and Kibs? Pick something you fancy. I prefer Gibis, but that's a language thing I think.\n\nHooray for the KDE-team that does what should have been done a long time ago. Hopefully we can drag the US of A into the 21:st century (or atleast the 20:th) and make them use metric units as well soon.\n"
    author: "Oscar"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "\"Hooray for the KDE-team that does what should have been done a long time ago. Hopefully we can drag the US of A into the 21:st century (or atleast the 20:th) and make them use metric units as well soon\"\n\nDo you use metric seconds, hours, days?\n\nComputers are \"naturally\" base 2.  Not base 10.  A file size in RAM should be the same as in magnetic media.  How else do you map it?  Eh?  \n\nAnd as what I say about disk sizes, it's been \"Salesman Gigabytes\" or megabytes, especially when I explain it to someone who isn't a geek like me.  I say that because if I say \"gibibytes\", I sound like a total tool and I confuse whoever I'm talking to, make their eyes glaze over and they stop listening.  \n\nThis is all because some time in the early/mid 90's disk manufacturers moved from a base 2 unit to base 10, because it made drives \"look bigger\".\n\nSeagate just lost a settlement recognizing that they deceived customers by stating their disk sizes as base 10 units instead of base 2, which cheated customers out of appx 7 percent of space.  These days all the manufacturers do it because it's not in their interest to truthfully say how big their disks are in true gigabytes, because they _all_ lie.\n\nThis \"gibibytes\" and \"mibibytes\" (ugh) shenanigans is all about \"fixing\" the \"salesman gigabytes\" problem.  It should be the other way 'round, don't you think?  It's not about usability at all, because if it was about usability, units between RAM, and magnetic media would be consistent, wouldn't it?\n\nPeople are not dumb.  They certainly understood base 2 units before the marketroids changed disk measurement to base 10 and that kilobytes and megabytes in RAM were the same as on the disk or tape.\n\nYou can say gibibytes and mibibytes and sound like a total tool if you want, but you won't catch me saying it, or anyone else I know.  The only people pushing this have never mapped bytes on the disk into RAM and edited it in hex (or any other programmer worth his salt).\n\nExcuse me while I go and build something out of 74LSxx logic gates.\n\nGet off my lawn, you kids.\n\nOh yeah, and as for metric units, they aren't all that special and are just as arbitrary as \"english\" measurement.  It's not my fault you can't count in base 12 (or two, or 8, or 16)\n\n--\nBMO"
    author: "Boyle M. Owl"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-03
    body: ">Do you use metric seconds, hours, days?\n\nBzzt, wrong. The problem isn't that memory uses base 2 but that they hijacked the SI prefixes that were in use for well over a century. Embrace and Extend of another standard with your incompatible extensions, hmm who does this remind me of...\nOr in short: If you are against MiB, you are a Microsoft-lover =)\n\n>Computers are \"naturally\" base 2. Not base 10.\n\nYou mean like your CPU clockrate? Or your Ethernet connection? Or your DVD size? Or even your harddisk's latency?\nHow about the 1.44MB Floppy that is an unspeakable hybrid between KiB and MB.\n\n>A file size in RAM should be the same as in magnetic media. How else do you map it? Eh? \n\nOh yes, the main use of the disk space value in the file manager status bar.\nNot once have I used it to check the free space of my harddisk (where it might be of some minor use seeing how harddisks and most usb drives and stuff are measured in SI), but I use it constantly to map files to RAM address space in my mind.\n\n>And as what I say about disk sizes, it's been \"Salesman Gigabytes\" or megabytes, especially when I explain it to someone who isn't a geek like me. I say that because if I say \"gibibytes\", I sound like a total tool and I confuse whoever I'm talking to, make their eyes glaze over and they stop listening\n\nThen stop using gibibytes and start using GB. Non-geeks certainly don't give a fsck about addressing RAM.\n\n\n>Seagate just lost a settlement recognizing that they deceived customers by stating their disk sizes as base 10 units instead of base 2, which cheated customers out of appx 7 percent of space. These days all the manufacturers do it because it's not in their interest to truthfully say how big their disks are in true gigabytes, because they _all_ lie.\n\nYeah the American court system is broken. Tell me something new. And they don't lie, they use a different definition that's prevalent and consistent for just about anything but RAM and some other memory chips.\n\n>This \"gibibytes\" and \"mibibytes\" (ugh) shenanigans is all about \"fixing\" the \"salesman gigabytes\" problem. It should be the other way 'round, don't you think? It's not about usability at all, because if it was about usability, units between RAM, and magnetic media would be consistent, wouldn't it?\n\nWhy not make RAM consistent with magnetic and optical media?\n\n>People are not dumb. They certainly understood base 2 units before the marketroids changed disk measurement to base 10 and that kilobytes and megabytes in RAM were the same as on the disk or tape.\n\nIf people weren't, the other harddisk manufacturers wouldn't have been forced to follow suit after Maxtor(?) came up with the base 10 idea.\n\n>You can say gibibytes and mibibytes and sound like a total tool if you want, but you won't catch me saying it, or anyone else I know. The only people pushing this have never mapped bytes on the disk into RAM and edited it in hex (or any other programmer worth his salt).\n\nWhich would be about...well just about all of them.\n\n>Oh yeah, and as for metric units, they aren't all that special and are just as arbitrary as \"english\" measurement. It's not my fault you can't count in base 12 (or two, or 8, or 16)\n\nthe problem isn't that it's either one of those but that english measurements use just about *all* of them, plus 10 and 20.\n5280 feet to the mile, base, well it's because the furlong is 660 feet\n12 inch to the feet, base 12\n32 fl oz to the quart, 4 quarts to the gallon, base 2\n16 oz to the pound, 2000lb to the ton, base \"we love you all\"\n\nthe english measurements aren't arbitrary, they're fubared.\n\n"
    author: "ac"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-04
    body: "Sorry but the time second *is* a SI base unit. Even the meter itself is defined via time (lightspeed)! So seconds *are* metric. The second is therefore the most metric unit you can think of.\n\nAnd minutes hours and days aren't exactly base 2. It is a (today) somewhat confusing system introduced by the Babylonians. Ever heard of base 60 and 360 and why it was choosen by the old Babylonians? They did choose base 360 because they had _no_ computer power. Base 360 allows for a lot of divisions without remainder.\n\nSo you claim metric units aren't special. Aha. So it must be a total coincidence that it fits so perfectly into daily life: Originally a Kilogramm was defined via a litre of water and a litre is a dm\u00b3. Ah and degree Farenheit has such a logical definition (qicksilver and blood) in contrast to degree Celsius/Kelvin (just water). And I hope you know that in the US there are different area units around that are not just a factor of 2, 10 or something else but are a horrible floating point factor...\n\nAh and by the way SI is being used in the scientific world only (beside Atom Units and Electron Volt) and US scientists from the NIST and other institutions play a *big* role in the anticipated new more precise definition of the Kilogramm...\n\nSo SI is the world standard, cause it simply makes sense to every rational brain and cause it is so damn easy to caculate with as thousands of scientists from all over the world have invested heir whole lifetime into the SI project.\n"
    author: "Arnomane"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "Nope, the point is apps like Dolphin tend to display GiB instead of GB.\n\nPersonally I find this a bit overdone. Can't it just be GB like the rest of the world? ..which indeeds screws SI over, but try explaining Gigabytes (GB) instead of Gigibytes (GiB) :-P"
    author: "Diederik van der Boor"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "In German \"..GiB Frei\" >> Give free.... why not GB????"
    author: "german"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "Gawd I hope this is configurable. I would hate to think that Dolphin is going to look like a GiBbering idiot."
    author: "David Johnson"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "No kidding.  The time to be pedantic about GB/GiB is over.  Everyone is using GB these days, how about we just conform to that instead of being different over a little thing.  I guarantee there are gonna be a lot of \"GB is spelled wrong in the taskbar\" comments."
    author: "Leo S"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "1 MW = 1 000 000 W\n1 km = 1 000 m\n1 THz = 10^12 Hz\n1 k&#8364; = 1 000 &#8364;\nso 1 kB = 1 000 B for a lot of people (ask to your barber or your baker)\n\nHD manufacturers uses SI prefix correctly, french (but probably in a lot of other countries) network and telecommunication specialist uses SI prefix correctly. Why do you want \"k\" means 1000 for all of us but \"1024\" for some people of the computers world?\n\nThink about that: we write our numbers in the decimal numeral system, and SI prefix are made for the decimal numeral system. The binary prefix are good for numbers written in the binary numeral system, but very bad for the decimal numeral system. We write and think in the base ten, not two."
    author: "Capit. Igloo"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "Except 1000 bytes isn't as useful as 1024 bytes in computer terms. Besides which, a kilobyte has been 1024 bytes and a megabyte 1,048,576 bytes for a very long time. It's going to be next to impossible to convince people that the SI way is worth changing over to, particularly as \"mebibyte\", \"kibibyte\" and \"gibibyte\" just sound silly, and ordinary computer users simply won't care enough to make the distinction."
    author: "Paul Eggleton"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-03
    body: "> \"Except 1000 bytes isn't as useful as 1024 bytes in computer terms.\"\n\nRight, but why you want to destroy the SI standard just for one case? And no, \"mebibyte\", \"kibibyte\" and \"gibibyte\" don't sound silly and they are useful disambigious terms."
    author: "Capit. Igloo"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-02
    body: "I forgot to mention - hard drive manufacturers don't use the \"SI\" measurement necessarily because it is correct. They use it mostly because it allows them to confuse some people into thinking a drive has a bit more capacity than it actually does. According to a recent article[1], Seagate recently settled a lawsuit over this and is offering refunds or other compensation because of this capacity difference.\n\n[1]\nhttp://www.computerworld.com/action/article.do?command=viewArticleBasic&taxonomyName=storage&articleId=9045141"
    author: "Paul Eggleton"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-03
    body: "> \"They use it mostly because it allows them to confuse some people\"\n\nOk, but don't forget that those \"some people\" are all people of the computers world. For ordinary people, k=1OOO, and they understand correctly the size of USB keys or hard drives.\n\nThen, it's difficult to compare 578 MiB and 591872 KiB without a calculator, but it's very very easy to compare 578 MB and 578 000 KB..."
    author: "Capit. Igloo"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-04
    body: "And...? Programs, music, photos etc occupy MBytes and GBytes as well, everything depends on how your filemanager shows you the bytes used. So, this is a non.problem IMO, just be coherent though the whole system... and I'm all for MB/GB, not MiB/GiB, cause you, the user, have 10 fingers and are used since forever to use base 10."
    author: "Vide"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-03
    body: "I like that they are using it; I'm all for not messing up the SI units. If people prefer to use SI units, fine (maybe I do myself), but let 1 GB then really mean 1*10^9 bytes. Making 1 giga byte mean 2^30 bytes is just silly."
    author: "Andre"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-05
    body: "I concur, your excellency."
    author: "Tim"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-05
    body: ">In 3.5.x, the SI units are thoroughly ignored in favor of GB, MB and KB *AS GOD INTENDED*\n\nI'm an atheist and prefer coherency over historical mistakes.\n\nIn French, kibi and gibi (sounds like kebe and gebe for us) doesn't sound bad at all (just unusual).\n\nMy vote is for SI units."
    author: "renoX"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2007-11-06
    body: "> >In 3.5.x, the SI units are thoroughly ignored in favor of GB, MB and KB *AS GOD INTENDED*\n>\n> I'm an atheist and prefer coherency over historical mistakes.\n\nPerhaps Boyle M. Owl's god is the same god who say Pi=3 ;)\nhttp://gospelofreason.wordpress.com/2007/06/13/god-said-pi-3-stand-by-your-beliefs-dammit/"
    author: "Capit. Igloo"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2008-04-05
    body: "SI should take a back seat when it comes to data storage, data transfer, and basically anything with computers.  \"kilo\", \"mega\", \"giga\" all have different meanings to geeks; I have yet to hear \"kibi\", \"mibi\" (\"mebi\"?) or \"gibi\" said out loud.\n\nObviously as it has been noted, hard drive manufacturers and telecom started this baloney and no credit should be given to metric purists for this change.\n\nSI terms correlate with each other (1 liter water weighs 1 kilogram), as was their intended purpose.  Data doesn't correlate with other metric units, but is applicable versus seconds (time is always the \"other\" metric unit).  If bits-per-meter makes sense, or bits-per-liter, then in those cases SI units can fly.  Otherwise, the term kilobyte (for example) means 1024 bytes and was never intended to rip off SI units.  If I know what a byte is, I could know that kilobyte means 1024.  If computer-illiterate people are trying to understand what a \"megabyte\" is and they're being told that means one million bytes, how does that help them, since they don't even know what a byte is?  One of my friends has been studying C++ at college for a year, and I asked him what a byte was... he didn't know.  That's pathetic, but it illustrates... what would it matter if he knew the difference of mega and mibi, if he thinks 4 gigs of RAM is 4 billion bytes?\n\nAt least the terms that are imposed on the world shouldn't sound so stupid.  Or perhaps they should sound stupider.  \"Kibblebytes\", \"Metalbytes\", \"Gigglebytes\", and \"Turtlebytes\", there we go.\n\nI'm sorry, but it will be a LONG time before we hear \"mibs\" of RAM.  A LONG time.  My computer has 4 gigs of RAM, period.\n\nWhy didn't they accept this possibility:\n\nMB = 1048576 bytes\nmB = 1000000 bytes\nMb = 1048576 bits\nmb = 1000000 bits\n\netc.?  I mean, to everyone using KB ... now you're saying Kelvin Bytes.  And, what the hell is a decibit or decibyte?"
    author: "Dave"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2008-06-15
    body: "Bytes are NOT counted by 10 fingers. Bytes are counted in digital by those who think like a \"machine\" not like a human, while they're creating these technical HW or SW. Non-technical humans that are sellers or consumers of HDD's might want to count bytes by 10 fingers. Programmers, HW designers, technical people count bytes in base2.\n\nGibi etc is non-scientific, arbitrary chosen spelling by some human brain who don't see through of the entire reality. SI is perfect for humans from humans. But don't try to assimilate our machine base2 GB/MB/KB's to fit human base10 GB/MB/KB's. Yes, they're different, but it should not be in this stupid sounding way, degrading the \"machine\" version.\n\nFor those, same as the guy who invented \"Gibi\", who live in the process but don't see through, let me write down my thoughts. It's about humans thinking that they're the ultimate, on top of all, the best and ultimate creation that was possible. That's wrong.\n\nIt's humans(*) v.s. \"machines\" (by machine, i mean hi-tech stuff in mechanical form (like airplanes, cars, etc), electrical (like IC's, PCB's), etc etc...\n\nIn this particular \"machine\" we talk of PC's, CPU's and their number system's base.\n\nPC's, digital \"brains\",memories use base2, due digital ON/OFF design\nHuman brains use base10 numbers, due 10 fingers.\nHuman English brains use base26 language, due 26 characters in alphabet.\nHuman (mammal) body use base4, due DNA's 4 nucleic acids\nVisible matter in universe uses base118+, due molecular elements in periodic table\n\nUniverse gives birth to stars which produced the heavy elements, which made up the material for our earth and thus mammals and especially human body and it's brain; humans are in process of giving birth to next generation life form, the \"machines\". All of these have their own body and life, their baseXX number system and give birth to next better level life form. Humans are now in process of producing / generating the machines, all happens inevitable. Mathematics in general is the language of the universe.\n\nSo, stop making the \"machines\" to look dumb, being a toy created by humans. They will take over some day. Who don't see through, didn't understand who him/herself is. Why should \"machines\" use the dumber version GiB and humans get the \"better\" GB ? Because a human said so, ok. Keep your humanly base10 for your human life needs, ok, if it's when you want to buy a HDD as a human. The inner workings of a PC or HDD will always use base2 anyways. You're dealing with a new life form, this is not the seeds in a pumpkin that you're counting with your 10 fingers. You're counting bytes here!\n\nAtomic reaction is a process that will never happen on earth naturally. It happens naturally in a star like the sun. Humans (oh, it was ONLY ONE human, Herr Einstein !) understood the language of universe and we dumb humans/governments are using the magic powers of it to destroy us. Electric, transistor, base 2 numbering was created in similar ways. I mean all is about science being the reference where correct human brain output meets together to built the next life form. 99.99% will not understand what i wrote here, never mind. :-)\n\nAll this is a draft and quick typing. Have a nice day. ;-)\nTayfun\n"
    author: "Tayfun"
  - subject: "Re: GiB? MiB?  Whiskey Tango Foxtrot?  Over?"
    date: 2008-06-15
    body: "Bytes are NOT counted by 10 fingers. Bytes are counted in digital by those who think like a \"machine\" not like a human, while they're creating these technical HW or SW. Non-technical humans that are sellers or consumers of HDD's might want to count bytes by 10 fingers. Programmers, HW designers, technical people count bytes in base2.\n\nGibi etc is non-scientific, arbitrary chosen spelling by some human brain who don't see through of the entire reality. SI is perfect for humans from humans. But don't try to assimilate our machine base2 GB/MB/KB's to fit human base10 GB/MB/KB's. Yes, they're different, but it should not be in this stupid sounding way, degrading the \"machine\" version.\n\nFor those, same as the guy who invented \"Gibi\", who live in the process but don't see through, let me write down my thoughts. It's about humans thinking that they're the ultimate, on top of all, the best and ultimate creation that was possible. That's wrong.\n\nIt's humans(*) v.s. \"machines\" (by machine, i mean hi-tech stuff in mechanical form (like airplanes, cars, etc), electrical (like IC's, PCB's), etc etc...\n\nIn this particular \"machine\" we talk of PC's, CPU's and their number system's base.\n\nPC's, digital \"brains\",memories use base2, due digital ON/OFF design\nHuman brains use base10 numbers, due 10 fingers.\nHuman English brains use base26 language, due 26 characters in alphabet.\nHuman (mammal) body use base4, due DNA's 4 nucleic acids\nVisible matter in universe uses base118+, due molecular elements in periodic table\n\nUniverse gives birth to stars which produced the heavy elements, which made up the material for our earth and thus mammals and especially human body and it's brain; humans are in process of giving birth to next generation life form, the \"machines\". All of these have their own body and life, their baseXX number system and give birth to next better level life form. Humans are now in process of producing / generating the machines, all happens inevitable. Mathematics in general is the language of the universe.\n\nSo, stop making the \"machines\" to look dumb, being a toy created by humans. They will take over some day. Who don't see through, didn't understand who him/herself is. Why should \"machines\" use the dumber version GiB and humans get the \"better\" GB ? Because a human said so, ok. Keep your humanly base10 for your human life needs, ok, if it's when you want to buy a HDD as a human. The inner workings of a PC or HDD will always use base2 anyways. You're dealing with a new life form, this is not the seeds in a pumpkin that you're counting with your 10 fingers. You're counting bytes here!\n\nAtomic reaction is a process that will never happen on earth naturally. It happens naturally in a star like the sun. Humans (oh, it was ONLY ONE human, Herr Einstein !) understood the language of universe and we dumb humans/governments are using the magic powers of it to destroy us. Electric, transistor, base 2 numbering was created in similar ways. I mean all is about science being the reference where correct human brain output meets together to built the next life form. 99.99% will not understand what i wrote here, never mind. :-)\n\nAll this is a draft and quick typing. Have a nice day. ;-)\nTayfun\n"
    author: "Tayfun"
  - subject: "Slackware packages?"
    date: 2007-11-03
    body: "I would really like to test-run this beta. Are there any slackware packages for it?"
    author: "Eduardo"
  - subject: "Re: Slackware packages?"
    date: 2007-11-03
    body: "You'll probably have to compile it yourself. I just run the svn code, and I haven't seen any packages. You might be able to find a slackbuild?"
    author: "slacker"
  - subject: "Whay so much wasted space? "
    date: 2007-11-04
    body: "I have used KDE for a long-long time.\nYes, the new look is OK but why so much wasted space? (see http://www.kde.org/announcements/announce_4.0-beta4/kde4.0-beta4.png)\nSystem-settings looks like a half-ass knock-off from OS X. Why?  \nFrankly, I am sick of all this OSX/Vista/XP lookalike crap in KDE. \nFrell all this \"familiar look, easy to switch from * to KDE\" nonsense.\n\nI personally think, that all those photorealistic pictures of \"a PC from previous century\" and application that looks like a .. pfff when did you receive a OSS application in a carboard box? Those are all outdated and ancient ideas and symbols.\nImagine how bad those pictures (a.k.a icons) look on a small scale.\n\nFor a example,  look at the Okular \"icon\". Even this huge picture has lost its most important detail. Can you really expect to see those spectacles at smaller scale/size? \nIf you recognize  Okular icon by a blue squigly line (and not the spectacles) , then drop the rest. It has no meaning nor function.\n\nCompare the \"System Settings\" and \"My Computer\" images. Are my \"settings\" set for a different \"computer\"? Why are those images of a computers different?\n\n\"Recently used\" - did I use calendar or is this just another example of a really bad \"Recently used\" icon?\n\nStylish (having style or artistic quality) does not mean allmost unrecognizable blur of pixels. \n\n\nKISS - keep it simple stupid! ;) \n\n"
    author: "NamNamee"
  - subject: "Re: Whay so much wasted space? "
    date: 2007-11-04
    body: "I was waiting for someone to say exactly what you said. I cannot agree more. The fate of KDE's adoption by now-Windows & now-Mac users lies in the hands of Plasma. Unfortunately, from what I have seen thusfar, there is just no reason for anyone to switch to KDE 4. There is little added benefit (besides QT4) and nothing visually attractive about it. I find disturbing that while such great mockups were created by users on kde-look, etc., few...if not any ideas from them were implemented. Anyhow, in my opinion, the Oxygen group has done an EXCELLENT (10/10) job on creating a visually attractive icon set. Unfortunately their work will not shine as I had once thought.\n\nJust my 2-cents -"
    author: "js0n"
  - subject: "Re: Whay so much wasted space? "
    date: 2007-11-05
    body: "I am not trying to defend the KDE team but please don't forget that KDE4/Plasma is not finished yet! I can remember reading critics on MAC OSX Leopard some months ago which are all gone now with the final release. \nLet's appreciate these guys's hard work, make useful suggestions and/or critics etc and just wait and see. I am sure that things will change a lot by the time RC1 is there and that KDE4 will shine."
    author: "Bobby"
  - subject: "Re: Whay so much wasted space? "
    date: 2007-11-05
    body: "\"I am sure that things will change a lot by the time RC1 is there and that KDE4 will shine\"\n\nEven then, people are stilling missing the point:\n\nKDE4.0 is the *beginning* of the KDE4 cycle, not the end!\n\nMany people are still, incomprehensibly, falling into the trap of thinking that once KDE4.0 hits, then the UI and widgets and desktop will be completely frozen in place, never to significantly change again, which is completely false.  Plasma and Oxygen will likely change and improve significantly during KDE4's lifetime.  There is absolutely no reason at all why this should not be the case."
    author: "Anon"
  - subject: "Re: Whay so much wasted space? "
    date: 2007-11-05
    body: "Could be that they are mixing it up with Vista which will not change for the next 5 years.\n\nLike you said, KDE4 will continue to evolve just like KDE3 did (still does). We have seen a lot  of changes and improvements along the way and even though KDE4 is being workde on intensively we still see KDE3 continue to improve. That's wnat I personally love about the Open Source/Linux world."
    author: "Bobby"
  - subject: "Re: Whay so much wasted space? "
    date: 2007-11-05
    body: ">> Compare the \"System Settings\" and \"My Computer\" images. Are my \"settings\" set for a different \"computer\"? Why are those images of a computers different?\n\nUm, don't you think the \"System Settings\" icon looks very much alike the one from KDE3? Maybe they just haven't made the new \"System Settings\" icon yet? You know, it's fully acceptable to think a bit before you post. But then again, it's easier to just whine and complain, don't you think? ;-)\n\nDo you remember how the first releases of GNOME 2 used to look like? They looked horrible, IMHO. However, they slowly but incrementally changed, and now they've got a very decent looking desktop, I think. I'd guess about the same thing will happen to KDE 4.\n\nRock on devs! Despite what the trolls say, we still love you! ; D"
    author: "Anonymous"
  - subject: "Re: Whay so much wasted space? "
    date: 2007-11-06
    body: "\"Frankly, I am sick of all this OSX/Vista/XP lookalike crap in KDE.\"\n\nSorry, I got stuck in this icon BS but my main question is:\n\"Why is KDE trying to _look_ like a mixture of OSX and XP/Vista crap? WHY!?\"\nI am 100% sure, that most of the KDE developers and designers (software and GUI) are way more talented than MS boys and girls and can create something truly new and wonderful. You have the freedom to invent, they (MS sock puppets) do not! \n\nThe problem is (yes, I know, its just a start for the new KDE 4) that I fail to see the \"revolution\".\nBack to this icon BS... If you make icons and panels look all slippery and sticky, like a piece of melting candy on a hot pavement... you end up... oh, never mind. \n\nKDE team, please stop copying those bull shit (and mostly old and stolen) \"ideas\" from MS and invent! Invent something NEW! You have the brains to do it. Most of us do not, but we can understand when you hit the bullseye! \n\nThere is a long (way too frak'n long) article at the theinquirer.net \"Linux desktop lacks innovation\". (http://www.theinquirer.net/gb/inquirer/news/2007/11/02/linux-innovation-missing) \nSadly, I have to agree with this guy on too many points he makes.\n\nPS! Developers, please DO NOT design GUI! your understanding of \"intuitive\" and \"good looking\" is utterly distorted and wrong! Let the usability/GUI/art people do that work for you! ;) \n\nCheers! \nPPS! I am waiting for the KDE4 like we all do, but some as kicking is necessary too! :) \n"
    author: "NamNamee"
  - subject: "Re: Whay so much wasted space? "
    date: 2007-11-07
    body: "> PS! Developers, please DO NOT design GUI! your understanding of \"intuitive\" and \"good looking\" is utterly distorted and wrong! Let the usability/GUI/art people do that work for you! ;) \n \nYou know, this is a spectacular idea!\n\nUnfortunately, for some unknown reason, the last time they did this, someone naming themselves \"NamNamee\" posted a comment on The Dot telling them not only that this has been a bad idea, but telling the whole \"KDE team...\" to \"...please stop...\" the \"...bull shit...\"\n\nMaybe you, incidentally having given the same name for your comment, could given some insight into this obvious clash of concepts.\n\nBecause I am just a developer and as such either lack the intelligence, creativity or combination of them to understand how it is supposed to work to let usability experts and artists do the design and then throw away what they came up with.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Whay so much wasted space? "
    date: 2007-11-06
    body: "\"Um, don't you think the \"System Settings\" icon looks very much alike the one from KDE3? Maybe they just haven't made the new \"System Settings\" icon yet? You know, it's fully acceptable to think a bit before you post. But then again, it's easier to just whine and complain, don't you think? ;-)\n\nDo you remember how the first releases of GNOME 2 used to look like? They looked horrible, IMHO. However, they slowly but incrementally changed, and now they've got a very decent looking desktop, I think. I'd guess about the same thing will happen to KDE 4.\n\nRock on devs! Despite what the trolls say, we still love you! ; D\"\n\nI do agree with you. The devs need our encouragement and support. The most of us who are finding faults can't even program the ugliest icon that the first version of GNOME had. It's easy to find faults but to find solutions is another thing.\nI hear people complaining and saying that KDE4 looks like Vista and blah, blah, blah. Where is the resemblance? I have Vista installed since it was released and have been trying it since the betas (even though I actually hate the OS) and I just can't see the similarities between it and KDE4, apart from the ugly task bar which I pray to God will disappear by the next beta release ;D\n\nPertaining to KDE's similarities to Windows (even though it's much more functional and prettier) and GNOME's to MAC OS like the guy from the INQUIRER described: http://www.theinquirer.net/gb/inquirer/news/2007/11/02/linux-innovation-missing, I would just like to say that I personally don't think that the developers should go all the way to create something that's totally different from the DEs on the market because a lot of users who are moving to Linux are former Windows users and I don't think that they would feel comfortable if everything is totally different from what they are used to. I am not saying that a terrible DE like Windows should be cloned, what I am trying to say is that innovation doesn't mean that something has to be totally different from what's already there, it doesn't mean that the wheel has to be reinvented. What it means is that we should make perfect what's already there like the Japanese do."
    author: "Bobby"
  - subject: "KDE 4 is..."
    date: 2007-11-11
    body: "... totally screwed up.\n\nDid MS pay for it?"
    author: "barteqx"
  - subject: "Re: KDE 4 is..."
    date: 2007-11-11
    body: "I think porting KDE 3 to QT 4 is the best idea."
    author: "barteqx"
  - subject: "Re: KDE 4 is..."
    date: 2007-11-11
    body: "I give your trolling 3/10.   Best of luck with it, though!"
    author: "Anon"
  - subject: "Re: KDE 4 is..."
    date: 2007-11-12
    body: "Where there is no negative there is also no positive.\nVista is also screwd up even though it's finished. KDE4 is still in beta stages and I am sure that it will be much better than Vista.\n"
    author: "Bobby"
  - subject: "Something to say... "
    date: 2007-11-15
    body: "First i want to thank the KDE 4 Developers for creating this wonderful attractive Desktop, it for sure beats the looks compared with OS X and Vista..\n\nI think Some of you people are very impolite and un thankful (never learned manners?? ) , if you don't like KDE 4 just don't use it!! :-) \"ITS THAT EASY\" \nBut stop talking all that negative crap about KDE 4 when it ain't even released yet... don't get me wrong, nothing against critic but some People should say what they don't like in a more Human way, not like a dam Idiot... \n\nI think KDE 4 is doing a great job, i like a good looking Desktop and when the applications all harmonize with each other.. \n\nKDE 4 is just what i am waiting for.. keep it up!! \n"
    author: "Solardeity"
  - subject: "Stable. Make it rock stable please."
    date: 2007-11-16
    body: "And I don't just mean kdelibs."
    author: "szlam"
---
The KDE Community is happy to release <a
href="http://www.kde.org/announcements/announce-4.0-beta4.php">the
fourth Beta for KDE 4.0</a>. This Beta aimed at further polishing of
the KDE codebase and we would love to start receiving feedback from
testers. At the same time, a Release Candidate for the KDE 4.0 Development Platform is <a href="http://www.kde.org/announcements/announce-4.0-platform-rc1.php">released</a>. This Development Platform contains the bits and pieces needed to run and build applications using KDE 4 technology. The purpose of this Development Platform is to make it easier for third parties to port their applications to KDE 4, without having to wait until the full desktop is polished enough for the final 4.0 release.



<!--break-->
<p>As KDE has largely has been in bugfix mode, this last beta aims to
encourage testers to have a look at KDE 4 to help us find and solve
the remaining problems and bugs. Besides the stabilization of the
codebase, some minor features have been added, including but not
limited to much work on <a href="http://plasma.kde.org">Plasma</a>.
Sebastian Kügler notes: <em>"The improvements have been huge, and plasma
is much closer to what it needs to be before the release. I am
confident we will be able to finish it and present a very usable
plasma to the users with KDE 4.0".</em>
</p>
<p>
KDE 4 is the next generation of the popular KDE Desktop Environment
which seeks to fulfill the need for a powerful yet easy to use desktop
for both personal and enterprise computing. The aim of the KDE project
for the 4.0 release is to put the foundations in place for future
innovations on the Free Desktop. The many newly introduced
technologies incorporated in the KDE libraries will make it easier for
developers to add rich functionality to their applications, combining
and connecting different components in any way they want.
</p>
<p>
The <a href="http://www.kde.org/announcements/announce-4.0-beta4.php">announcement
page</a> has more information and download links for packages for
several distributions.
</p>



