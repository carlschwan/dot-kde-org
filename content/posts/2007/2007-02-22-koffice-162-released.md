---
title: "KOffice 1.6.2 Released"
date:    2007-02-22
authors:
  - "iwallin"
slug:    koffice-162-released
comments:
  - subject: "Awesome!"
    date: 2007-02-22
    body: "That's wonderful!  Thanks KOffice team for all the hard work.  KOffice has saved me a number of times.  Kspread is wonderful at reading the csv files I need to use at work (WAY better than OOo) and has saved me from tearing out my hair in frustration!"
    author: "Vlad"
  - subject: "Re: Awesome!"
    date: 2007-02-22
    body: "I must say the same. No oversized memory footprint, quick handling, and a rich and useful set of features. Cool! Thanks!"
    author: "Markus Heller"
  - subject: "Re: Awesome!"
    date: 2007-02-24
    body: "grrrrrrr can't wait to use  kplato under windows."
    author: "djouallah mimoune"
---
The KOffice team today <a href="http://www.koffice.org/news.php#itemKOffice162released">released KOffice 1.6.2</a>, the second bug-fix release in their 1.6 series. Although this is a maintenance release, there are some new features in Krita (new filters and a smudge paint operation) and Kexi (a new User Mode to deploy Kexi applications). Many bugs were fixed, thanks to the helpful input of our users. We also have updated languages packs with no less than 4 new languages. You can read more about it in the <a href="http://www.koffice.org/announcements/announce-1.6.2.php">announcement</a>, and the <a href="http://www.koffice.org/releases/1.6.2-release.php">release notes</a>. A <a href="http://www.koffice.org/announcements/changelog-1.6.2.php">full changelog</a> is also available. Currently, you can download binary packages for <a href="http://kubuntu.org/announcements/koffice-162.php">Kubuntu</a> and <a href="http://download.kde.org/stable/koffice-1.6.2/SuSE/">SUSE</a>.



<!--break-->
