---
title: "Third KDE 4 Development Snapshot Released: \"Kludge\""
date:    2007-02-23
authors:
  - "sk\u00fcgler"
slug:    third-kde-4-development-snapshot-released-kludge
comments:
  - subject: "Buildung"
    date: 2007-02-23
    body: "Will there be Live-CDs?\nWill there be nightly builds?"
    author: "Jan"
  - subject: "Re: Buildung"
    date: 2007-02-24
    body: "This is a single milestone release, sort of like an alpha or beta release, only it's not that level of stable yet.\n\nHere's a list of available binaries I've got so far:\nKubuntu: http://kubuntu.org/announcements/kde4-3.80.3.php\nOpenSuse: http://repos.opensuse.org/KDE:/KDE4/openSUSE_10.2\n\nIf you know of any other distros/platforms that have binaries for 3.80.3, please feel free to post them in the comments, as it helps everyone find them :)\n\n(there's also http://kdesvn-build.kde.org/ , a script that helps build KDE 4 from SVN - it build trunk, rather than the tagged tarballs, but for some people, this is what they want...)"
    author: "Troy Unrau"
  - subject: "Re: Buildung"
    date: 2007-02-24
    body: "Nice to see Suse packaging all modules, and also them splitting their packages more granular."
    author: "Anonymous"
  - subject: "Re: Buildung"
    date: 2007-03-09
    body: "Fedora:\nFC6 i386: http://apt.kde-redhat.org/apt/kde-redhat/fedora/6/i386/RPMS.unstable/\nFC6 x86_64: http://apt.kde-redhat.org/apt/kde-redhat/fedora/6/x86_64/RPMS.unstable/\nRawhide i386: http://apt.kde-redhat.org/apt/kde-redhat/fedora/development/i386/RPMS.unstable/\nRawhide x86_64: http://apt.kde-redhat.org/apt/kde-redhat/fedora/development/x86_64/RPMS.unstable/\n\nCurrently, I have kdelibs4, kdepimlibs4 and kdebase4 packaged up. These are safely parallel-installable with the KDE 3 packages. If you want to migrate your KDE 3 settings, copy your \"~/.kde\" to \"~/.kde4\". (Yes, the packages are patched, this is not the default behavior of KDE 3.80.3.) Thanks to Rex Dieter for building my SRPMs and hosting the RPMs in his repository."
    author: "Kevin Kofler"
  - subject: "Re: Buildung"
    date: 2007-03-10
    body: "Now FC5 too:\nFC5 i386: http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/i386/RPMS.unstable/\nFC5 x86_64: http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/x86_64/RPMS.unstable/"
    author: "Kevin Kofler"
  - subject: "default filemanager"
    date: 2007-02-23
    body: "What's wrong with konqueror as default file manager?\nDolphin does not look so good..."
    author: "CHX"
  - subject: "Re: default filemanager"
    date: 2007-02-23
    body: "I've used Dolphin and as a file manager it works fairly well, though I find the lack of tabs and built-in console a little annoying (but then again, it is pre-1.0). But, to be honest, if the profiles issue in Konqueror were sorted out so that we could have different toolbar sets per profile as well as different sidebar folders per profile, then most of Dolphin's advantages would evaporate.\n\nI really don't need to see my internet bookmarks toolbar when doing file managing and I don't really need to have file-manager type buttons when browsing the internet..."
    author: "David P James"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "This has been what i've hoped devs for years to do.. One of the things keeping me on gnome actually. "
    author: "Goalie"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "It also kept me away from KDE for a long time.  I've been waiting for a separate file manager and I am very relieved to see it finally happen."
    author: "Alethes"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "there are seperate filemanagers available for kde (krusader for example)\n\nand you can use konqueor as file manager only, while using firefox as browser"
    author: "otherAC"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "while this isn't an attempt on our part to get gnome users to convert, it is an attempt to broaden our appeal in general.\n\ni want to hear macos users say \"ok, -now- i can use kde because <list of kde4 changes, this one amongst them>\".\n\nand when i say \"broaden\", this implies growth rather than shifting. so we need to continue to satisfy our current user base (we owe them, if nothing else =), which means not jumping a \"this is the ONLY way\" position of insanity.\n\nit'll be fun to see just how well this all works out."
    author: "Aaron J. Seigo"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Well I also use a mac on a daily basis. There really isn't that much open source for macs especially stuff that runs native. There's a lot of shareware and a lot of commercial. GTK+ doesn't run native while QT does. There's a real demand and it looks like KDE will get there soon :D\n\nWhen Kde4 is released I will certainly be cherry picking all kinds of applications as I currently do. I will also let windows users know of all the cool kde apps especially amarok."
    author: "Goalie"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "kde apps on mac os is great and better than the alternative of just mac os with non-kde apps, but i'll be honest: i'd rather see mac users using kde apps on kde. =) that's my personal goal (as opposed to the goal of the kde project as a whole =) ... which means we have to make something compelling enough out of the kde platform that we can woo mac and windows people off their current systems "
    author: "Aaron J. Seigo"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "I love the idea of running my favorite KDE apps (and there is a lot of them) on my macbook, since I can't run Linux on it due employer requirements. I would however prefer running them on a OS supporting a full KDE environment.\n\nThere is one thing keeping me on OS X, the most important is FileVault, encrypted home, everything else I could do without or has a working replacement for under Gentoo."
    author: "m_abs"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "> ok, -now- i can use kde because <list of kde4 changes, this one amongst them>\n\no.k. here comes a still and future ex-Windows user to tell about kde migration:\n-now- i can use kde because:\nKonqueror renders most web-pages quite well\nKonq is also a good file manager, but a separate app would be preferable \nDolphin will be a good thing. Web- and file browsing are unrelated, IMHO\nAmarok is a hammer, albeit not totally stable (crashes upon hilbernation)\nOOffice integrates quite nicely and can convert M$-Office files nicely\nKOffice is very feature-complete (KPlato etc...), COULD be better than OO\nOxygen seems nice, Solid as well, though I've not tried KDE4 yet.\nIn all, the KDE4 technologies seem extremely promising!\nand of course: Linux is the best feature of KDE: real multitasking and user!!!\n\n\nWhat still needs improvement until Windows users can fully switch IMHO:\nKOffice needs better M$ filters. Can the new DOC to ODF filters be integrated?\n\nWhat could be done to increase the good impression:\n3D: IF, then \"real 3D\" \u00e0 la looking glass, not a \"2D+\" \u00e0 la compiz, please!\n\nAnd of course the long-time missing linux features:\nhilbernation, graphics card support, my scanner doesn't work...\nbut KDE for this KDE is not to blame!\n\nBy the way: congratulations for a post system that works without registration!!!"
    author: "sulla"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: ">KOffice needs better M$ filters. Can the new DOC to ODF filters be integrated?\nAFAIK: No. OO.org is converting to it's internal representation, not to ODF. That makes it very hard, if not impossible to reuse in KOffice. \n\nPersonally, I think it is more important to really have a *good* ODF support.\n\n>3D: IF, then \"real 3D\" \u00e0 la looking glass, not a \"2D+\" \u00e0 la compiz, please!\nNo, thank you. I have never seen a usefull 3D interface just yet. Sure, some 3D effects can be usefull and make the environment very good looking, but I think a complete 3D interface is just nonsence while we still have 2D monitors. \n"
    author: "Andre"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "And add to that that most people can't see 3D, only 2D. Lots of people do not have a perception of depth."
    author: "Tim Beaulen"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "You forgot to cite the recent study where 88% of respondents admitted this.\n/end_sarcasm"
    author: "illogic-al"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Well, if Sun can do a Word<->ODF converter on Windows (even if using half of OpenOffice internally) then it should theoretically be possible to have something similar for KOffice. Or maybe have OpenOffice as optional runtime dependency and use it for converting MS Office documents to ODF documents by calling it from the command line and reusing this output.\n\nMaybe Sun's new filter for MS Word will get OpenOffice some more refactoring so that the converter part can be used seriously as a library in some time."
    author: "Jakob Petsovits"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Thats what I ment. Probably one could use this converter and execute it from the KOffice load dialog as a command line script when accessing a word file. As long as it makes the word file acessible in Kword, I wouldn't mind the fulter using some OO library or even java...\n\nAs for 3D: thats why I put \"IF\" in capital letters: I also don't think that 3D is a good idea per se. I don't think the compiz effects are useful. Its no 3D after all, just using some 3D engines of the graphics card. I liked Suns idea however to attach notes to the back side of windows, thats real 3D. and if you move windows in the background they become smaller. And the 3D CD-changer also looks cool. But this is all real 3D, i.e. 2D windows live in a real 3D space. Of course, the apps are all 2D themselves. However, I could imagine a spreadsheet document having 3D data like today you have a 2D table and several sheets one beside the other..."
    author: "sulla"
  - subject: "Re: office converter"
    date: 2007-02-24
    body: "I'd also like to see a many-to-many command-line office-format-conversiontool. Would fix most, if not all, of our worries and duplicate work.\n\nAs for your 3D: The resolution of the current monitors is actually quite low for doing that kind of stuff: the windows would quickly start becoming fuzzy and unreadable if you make them smaller/zoom them out..."
    author: "laurencevde"
  - subject: "Re: office converter"
    date: 2007-02-26
    body: "probably OO.o could make it possible to convert MS office doc's to ODF from the commandline (though you'd have to wait for OO.o to start in the background, which means you'd have to wait 5 minutes to convert a file) but then they'd have to spend time on it, to no real benefit to OO.o users, and why would sun do that?"
    author: "superstoned"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "this is a HUGE amount of work, beyond the resources the koffice team currently have. they've rightfully prioritized features, stability and ODF ahead of this. new efforts are not discouraged, of course!\n\nfor ms office integration there is OOo. not perfect, but it works just fine in kde and helps resolve the ms office issue in the immediate term."
    author: "Aaron Seigo"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "I work regularly on Windows and on KDE. On Windows I use Firefox as a browser and of course Explorer as File manager. This is actually cumbersome, annoying and I much prefer my KDE experience where I use the same app to do both.\nI like to browse FTP sites and to be able to sort by type of files, by date or anything. I like to be able to use the \"copy to\" fight-click option to select several files or entire directories and copy them in a single click where I want them on my disk. I like to upload files on my personal web site by just drag and drop, without having to fire up a specific and annoying FTP app.\nI also like to connect to other machines through SSH or SMB transparently.\nAs a matter of fact, Konqueror versatility has probably been THE feature that made me prefer KDE since the begining.\nI can't see any reason why having a different app for browsing and file management is preferable.\nAnd all this thing about clutered interface and everything seems to me a lot of wasted time : most people in the world use MS apps who are clutered as hell, that shows that most people are not really paying that much attention to this.  I don't say that it's not desirable to clean up GUIs a bit, just that forcing people to change their habits in order to achieve this goal doesn't seem to me the best way to attract users. How much oversimplified MaCOS X is, it has not really attracted that many new users to MAC and a lot of people I know, who are not computer scientists far from it, actually find Windows easier than Mac.\nWhat I like in KDE is that it allows me to work the way I want. I don't want to be forced into a way of working just because someone somewhere thought it was better. Most of desktops do that and none actually succeed. So please do not go that way too."
    author: "Richard Van Den Boom"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "What you are writing about is network transparency, and Dolphin supports this, too (at least it is stated on their website, I haven't used it already). \nAs I understand it, only webbrowsing, also viewing HTML sites, execute Javascript code, ... isn't supported.\nAt least I hope I am right here, because these features are what I'm missing most when I have to work on a Windows desktop.\n"
    author: "veru"
  - subject: "You're right but..."
    date: 2007-02-24
    body: "... it's actually very nice to be on an HTML site, click on a link to a FTP site and keep on browsing on the same windows or in a tab.\nHaving to fire up a new app, with a potentially different behaviour is annoying to me."
    author: "Richard Van Den Boom"
  - subject: "Re: You're right but..."
    date: 2007-02-25
    body: "you realize you can keep on using konqueror right? Right?"
    author: "illogic-al"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Currently there is no choice, either you use konqueror, i.e. all-in-one, or not. Koqueror is a power-user tool and hence a bit too much for \"beginners\" who just want to browse the web or manage their files. Hence Dolphin was introduced.\n\nThose that need and want konqueror, just continue using it."
    author: "Anonymous"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "I disagree, beginners would have no trouble with Koqueror. They'll just ignore most of its features. on the other hand dolphin is much faster"
    author: "Ben"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "> beginners would have no trouble with Koqueror\n\nplease tell universe that it messed up and to fix reality.\n\nthank you."
    author: "Aaron Seigo"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "yeah right, I'll go tell my 9 and 7 year old.\nThey have both FF and Konq installed on their respective system and both choose to use konq exclusively."
    author: "Kay"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "And this has nothing to do with them seeing you use konq? \nNothing to do with konq's opening links (mail, etc) by default in your desktop environment? Let me take a wild guess on this on, it's KDE. \nOh, by the way we're talking about a file manager here, not a web browser. And konqueror, for the record, is still going to \na) be around AND \nb) be doing both."
    author: "illogic-al"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: "It doesn't matter why they use it, the fact that they _can_ use it says plenty.\n\nYou don't have to cripple things to make them easy"
    author: "mabinogi"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "I live in a famaly of permement beginners, and I've studied their software habits. \n\nWhat they don't understand they ignore* meaning functionality cannot hurt them unless it bloats the program or introduces a security risk.\n\n* There is one exception, if the computer asks them a question, something like this is the first time you've used this program, please custiomise it. This can confuse them and then they ask me for help. "
    author: "Ben"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "I don't live in a family of beginners. I won't pretend to be a studier of software habits. I have made a few observations on my short time here on planet earth with respect to people and computer programs.\na) People don't like change\nb) People like simple interfaces\nHowever, if an interface is changed to be simpler then\nc) People don't like simple interfaces if it requires them to change how they work.\n\nBoggles my mind. The thing with dolphin being available is that \"people\" in group A will be happy since they can keep on using konq; people in group B will be happy since they get to use something simpler, and the rest of you in group C will be happy as you get to complain about something. \nThen you'll actually be even happier when you start using the apps and settle into  groups A or B again. There'll even be a rare few who will, astonishingly, settle into groups A and B! \ni.e. switch to Dolphin and then not want to change."
    author: "illogic-al"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "> beginners would have no trouble with Koqueror\n\nplease tell the universe that it messed up and to fix reality.\n\nthank you."
    author: "Aaron Seigo"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "I am fixing reality ever second sunday, but reality is broken beyond normal fixing. So I am going to replace it this summer, so you better hurry"
    author: "Universe"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "My wife is everything but a power user. She's an animator, having graduated from an art school, and she has no experience whatsoever with a computer. She had no difficulty at all browsing the web and managing her files with Konqueror, without any other input from me than the ones needed to understand what's a directory and what's a file, things you will always have to understand, whatever the file manager.\nMost people only copy/paste, create new, double-click to open a file in the appropriate app and move files and directories by drag and drop.\nThey can manage easily any file manager as long as they find these options easily (which is the case with Konqueror). They will juste discard other options at first. Having the other options visible is not really an hindrance as far as I've seen and sometimes, it allows the most adventurous beginners to actually try the options they wouldn't even know to exist otherwise.\nThe speed issue can be an argument, but on a recent computer, I don't find Konqueror's speed a trouble."
    author: "Richard Van Den Boom"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "> She's an animator, having graduated from an art school, and she has no experience whatsoever with a computer.\n\nThat's how I know you're a) LYING, b) know nothing about \"art school\" and c) Probably don't have a wife.\n\nCheers.\n\nP.S. They use macs a lot. Some institutions (non-universities mostly) even use PCs in the animation studios when they want to live dangerously."
    author: "illogic-al"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "> I can't see any reason why having a different app for \n> browsing and file management is preferable.\n\nfor many it isn't. for many more it is.\n\nwe're trying not to force our (or your) perception and preferences on them. we're also trying to optimize for the majority, nor are we trying to ship hobbled apps (people assume so little of the final state of dolphin *sigh*)\n\nso if you can't see a reason for different apps, rejoice.\nfor those who do, now they can rejoice too.\n\nwhat a concept! it's like we dropped the chocolate in the peanutbutter!"
    author: "Aaron Seigo"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: ">for many it isn't. for many more it is.\nAnd what are you basing this claim on? Any statistics, any poll that proves this?\nOr are you just basing this claim on what you think is the good thing, based on some personal experiences?\nMy personal experiences, for what it's worth, tells that it does not really matter. The whole world statistics, showing that most people use Explorer which is at the same time a web browser and a file manager, seem to prove you wrong, beginners easily use the same app for both tasks.\n\nWell... anyway, I'm probably making too much noise about this, since the willingness seems to make both apps live, I guess there's not much point being worried."
    author: "Richard Van Den Boom"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "Of course we have the statistics, here they are:\n\n66% of the universe agrees, adding dolphin as the default file manager will not cause KDE to implde upon itself.\n\nHappy?\n\nMY FAVORITE line of yours however has got to be this one: \n>Or are you just basing this claim on what you think is the good thing, based on some personal experiences?\nMuch like you and you \"wife\" mayhaps?\n\nbut this one comes pretty close too: \n>The whole world statistics, showing that most people use Explorer which is at the same time a web browser and a file manager, seem to prove you wrong, beginners easily use the same app for both tasks.\n\nAh the whole world statistics. So because users on a different system, who are largely unaware of alternatives choose to actually Live with what they are presented with, it is automatically good. \nBy your logic we can say that the jews absolutely loved their concentration camps. Good on yah sir. Good on yah.\n\n "
    author: "illogic-al"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "Are you really that stupid?"
    author: "Sergio Pistone"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: "Come on, guys, let's not fight about this. We're KDE users, and many of us (including me) don't understand why anybody would use Gnome - it's less powerfull, and the so-called usability we don't care about. But many people seem to do, no matter what we thing or what 'statistics' we quote or use. Now KDE is adding an option for those who prefer this 'usability', without crippling the userinterface or removing capabilities in any way. This will help some people use KDE, and won't make a difference to us.\n\nSo WHY are we fighting about this?!?!? Please, a big hug, no more flames, and be happy..."
    author: "superstoned"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "Well in all fairness by default is forcing a shift.  I have been testing out an older stable version of Dolphin in the repos and reading Dolphins progress.  It could be a good thing, but I'm still dubious.  The interface seems overly simplistic and slightly lacking (no up arrow to navigate up and out of the directory tree).\n\nI really don't want to see a shift towards:\n1. Gnome UI -- because \"simplicity\" is the popular thing this year (just say no to the sparse wide fat huge buttons of gnome).\n\n2. Firing up several applications that each perform a unique task -- This is especially a concern.  I don't want to return to the thinking of yesteryear where we compartmentalize resource locations and functionality.  I like that, now, I can edit files using Kate over SSH.  I like that the remote server I'm working on is seamless to me.  I don't want to fire up Kftpgrabber or FileZilla, a web browser, a minimalist file manager, a graphic viewer, etc.\n\nEven with that concern I'll keep an open mind.  After all an open mind is what drew me from gnome to KDE."
    author: "R Liden"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: "KDE is adding an option for those who prefer this 'usability', without crippling the userinterface or removing capabilities in any way. This will help some people use KDE, and won't make a difference to us who don't like it - Konqi won't go or lose anything. So let's assume the open mind doesn't go, allright?"
    author: "superstoned"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "I totally agree! In every point.\n\nAnd I have a question concerning this stupid folder dialogue. Can I switch back to the normal file dialogue?\n\nWhy shouldn't I see what's IN the folders? I can't understand this design decision. It's far the weakest \"feature\" in the whole KDE. It's annoying each and every time I have to select a folder/directory.\n\nTim\n \nP.S.: What's great in konqueror is this great symmetry. If you break it, you break konqueror. I like konqueror for being such versatile. I don't have to learn a lot of different user interfaces for the same actions. And more: I can do what I want to do. No artificial limitations. Exception: this stupid folder dialogue. What I'm missing since switching to KDE 2 is the possibility to select multiple items on HTML pages like I can in the other protocols like file, ftp, sftp or so.\n"
    author: "Tim"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: "the only thing changing in KDE 4 will be this:\n\nThe current 'filebrowsing' kpart in konqueror will be have a new 'shell', called dolphin. It will be the default filemanager. It's a bit like we get a second konqi, but then a very limited (filebrowsing) one, meant for the basics of filemanagement, and the old konqi will still be available."
    author: "superstoned"
  - subject: "The First Release"
    date: 2007-02-24
    body: "KDE 4 has been broadly advertised and it may have induced a rather strong hope among both KDE and non-KDE users. IMHO, the first stable release ought to be truly stable and well featured (no missing applications or so). Otherwise, potential new users may just throw KDE away.\n\nBesides, long life!"
    author: "Hobbes"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "Well, some Gnome users find Dolphin to look quite a lot like Nautilus :o)\nhttp://www.digiplace.nl/pivot/entry.php?id=650"
    author: "otherAC"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "Yes. Because obviously that one blog post, and the single reply, were generated by  >1 user. \nAlso I suspect you're using the fact that most people here not understanding dutch to make a point which doesn't exist. \nFrom what I understand the poster made no comparison to nautilus and dolphin. \nFurther, The reply displayed below was saying they prefer Thunar to Nautilus as far as file managers go. \nBut what do I know, I don't read dutch either."
    author: "illogic-al"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "Ok, let me translate it:\n\nI just read that another snapshot of KDE4 is released. Fine of course, development continues, also at KDE and that is how it is supposed to be. I also saw that KDE adds a seperate filemanager to KDE4: Dolphin. It won't replace Konqueror, which can do more than just filemanagement (by the way, i could never get used to konqueror). The first thing i noticed about Dolphin is the looks of this new filemanager. I won't say anything, just draw your own conclusions. ;-)\n[picture of nautilus]\n\ntwo replies\n\n \nKDE is moving more and more towards gnome. :)\nby: Me - 24 Februari '07 - 14:31 \n\n\nLet say: Modelled after a good example :) At KDE too there might be people that find Konqueror a bit of overkill. I find Thunar more comfortable (especially faster) than Nautilus.\n\nSo far the translation of that blog.\n\nSo in short he makes the statement that Dolphin reminds him of Nautilus.\nAnd looking at the screenshot of Dolphin and at Nautilus on my (kde) desktop, i can only agree with him ;)\n"
    author: "otherAC"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: ">>[picture of nautilus]\n\nthat should be [picture of dolphin] of course :)\nThe blog can also be found on http://planet.nl.gnome.org\n\nwhich imho makes it a gnome blog.\nBut that is not the point: this gnome blogger points out that the simplistic interface of dolphin reminds of nautilus.\nAnd he is right in my opinion..\n"
    author: "otherAC"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: "Nobody on the whole page is comparing Dolphin to Nautilus, there is only one comment which said Thunar is more comfortable compared to Nautilus... Or am I missing something?!?!\n\nWell, doesn't matter really ;-)"
    author: "superstoned"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: ">>Nobody on the whole page is comparing Dolphin to Nautilus, [..] Or am I missing something?!?!\n\nYup, you are drawing other conclusions than the blog poster did.\nHe is pointing to nautilus (which is the filemanager that the blog poster uses -- see other blogs of him), but is does not want to say that explicitly :o)\n \n>> Well, doesn't matter really ;-)\n\nIndeed, but still: he's right :)\n"
    author: "otherAC"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Well as much as I am a fan of konqueror the one thing that will have me running to dolphin would be:\nright-click>\n------------sort icons by>\n-------------------------- name, date modified, size, etc\n\nThis is a feature I get sooo annoyed that is missing in konqueror.\nOK, it's nothing major and probably a bad habit I picked up from windowz as a child, but as the ability to change view exists under the menu's at the top of the application why should I have to move half way across the screen to be able to use this feature. I am not complaining but having this feature would make konqueror easier to use or dolphin better imho.\n\nFantastic app's though both of them, keep up the amazing work!\n\nThe dolphin change for file browsing, tbh I was happy keeping all my browsing and file management open in the one window. The thought probably would make interface designers ill but I am happy tabbing between documents as it means I can auto-hide my taskbar and have the whole screen to use for whichever task"
    author: "Bob"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Mice make computers harder to use, not easier. You can pull down that menu and make a selection in four keystrokes, without having to look at the screen, and without having to move your hands anywhere, let alone push the mouse aalllll the way across the screen. Yeah, I know, it's so hard. But I think you'll probably survive."
    author: "Andrew"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "I have to agree with you that the keyboard is more functional than the mouse.\n\n(Hence why I am still confused that my classmates are made to use a GUI in windows to program in fortran)\n\nI am also using a laptop which means that my mouse is below my keyboard and the main advantage of X is that I can use a mouse. Not talking about usability, just simplicity.\nI either memorize the shortcuts for a lot of programs (that are altered by different distro's) or I use the mouse, which, is good for first person shooters, navigating VERY bad GUI programs which insist on mouse interaction, and other uses.\nAlso \n\nI just point it out as I know a few new converts that would like this feature as well. It just irritates me that copy/move to have been integrated into the right click which is more irritating than using the mouse to select files, and keyboard to move by address bar.\n\nIf it's a complex job or a very full directory I still prefer using the command line to copy files.\nJust wishing someone would indulge my laziness. I am sure I am not the only person who comes from windows missing this 'feature'.\n\n\nEnd of the day, it's just my bad habit so I will get used to it or forget it now that over the past 2 years I have discovered the command line and KDE."
    author: "Bob"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "I love konqueror.. most kde users do.. they love it because of what it is - please don' try to change kde. You'll gain some users, but loose some in the process.."
    author: "viseztrance"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "First, I love konqueror too.\nBut, I just dont get it, why everybody want his own preferences to be the default.\nDolphin is an simpler(featureless) and easy to use filemanager. \nIs it so insane to start with the simple interface as default for beginners? \nIf you are a more experienced user and want to use a more powerfull app like konqueror you should the hell be able to change that default. So stop whining."
    author: "Bernd"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Seriously - what part of \"Konqueror will still be available\" are people failing to grasp, here?"
    author: "Anon"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Did you knew that Microsoft Word can save in other formats than the infamous \"doc\" files ? I bet you did, but most people don't. Default means a lot. As long as konqueror will still be there, no problem i guess, but it hasn't seen too much development lately, has it not ?"
    author: "viseztrance"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: ">>Did you knew that Microsoft Word can save in other formats than the infamous \"doc\" files ? <<\nNo, because I dont use that crap. ;)\nI know the power of defaults, and especially for people that don't have an opion on that subject.\nIf I'm right, you are you fearing that if less people use konqueror, there will be less development on it ?\nThen I say it's not the peoples duty to use a software in the hope that it becomes usefull someday.\nIts the duty of a sofware to be usefull to the people, so that it deserves it's users.\nI personally think/hope/whish that konqueror is able to hold its userbase without being the default filebrowser."
    author: "Bernd"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Actually, it's increasingly difficult for beginners to have desktops that provide three or more applications which seem to do the same thing. At some point they just use the default and period. Which probably means very few will ever learn all the interest of Konqueror.\nTo avoid confusion, many distros will probably remove easy access to the apps that are not the default, remove them from the menu, etc. Just like there is Kaffeine in most distros but not Kplayer.\nAgain, as I posted above, the assumption that beginners should have simple looking apps with just three icons and is flawed in my opinion. What they just need is to be able to do the little things they already know easily, which is the case with Konqueror. The rest, they just ignore it at first and slowly they begin to use the features that they don't know. If you just hide them, they'll never use them. Making Konqueror a \"power-user\" app will guarantee you that most people will never have the chance of becoming a \"power-user\" (I put it in quotes because I think it's a stupid way of calling users that try to use more advanced features of their apps)."
    author: "Richard Van Den Boom"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "> Making Konqueror a \"power-user\" app will guarantee you that most people will never have the chance of becoming a \"power-user\" (I put it in quotes because I think it's a stupid way of calling users that try to use more advanced features of their apps).\n\nBut... that is the very definition of a power user. That's like saying you think \"chef\" is a stupid way of calling a cook who is more advanced than normal cooks in the kitchen."
    author: "illogic-al"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "Apart from you're ending comment about \"power-user\" I fully agree with this. \n\n"
    author: "Ben"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: "Konqi won't be a 'power-user' application. Konqi will be the webbrowser in KDE 4, Dolphin will be the filemanager. As konqi is based on Kparts, it CAN do filemanagement, just like it can do it now, but it won't be the default filemanager (though you can easily set it as default in Kcontrol or its successor).\n\nWe won't have many apps doing the same, we won't have one app doing two things like the current situation, but two apps each doing one thing as good as possible. Without losing any power or flexibility. Let's rejoice, please, as KDE doesn't take the Gnome route of REMOVING stuff, but is reorganizing and improving things instead..."
    author: "superstoned"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "why developers think that they know better?\nwhey you dont try to take a poll and ask users about something like this?\nYes!Dolphin (maybe) is a good Simple File Manager.but whats the matter with Konqueror?maybe Distro's will change their filemanager to dolphin, if thats usefull.\nKDE aims to be a powerfull Desktop Enviroment.Changing default file manager to Dolphin is againts this.so why developers dont try to make a new __EVERYTHING__ for KDE, to make it simple?\nI agree that making Toolbars change ith Profiles is the best solution."
    author: "Emil Sedgh"
  - subject: "Re: default filemanager"
    date: 2007-02-24
    body: "Two points:\n1.  A poll of Developers and Power users (how many casual users REALLY read the Dot?) would be ignoring a large part of the current user base, and a massive potential user base.\n2.  Who will most likely be able to change their settings:  The power users, or the new users?"
    author: "Sutoka"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: "+1\n\nWell, +100 actually. You get it, many here don't. I don't like dolphin, but I can change the default. For most ppl, Dolphin will be better for filemanagement, and they can still enter a local url in their 'webbrowser' anyway, so it's not like filebrowsing is 'hidden' in konqi... So what's the problem all these ppl seem to have?!?!?"
    author: "superstoned"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "> why developers think that they know better?\n\nwhy do users think their personal opinion constrained to their experience is better?"
    author: "Aaron Seigo"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "Not better, more important than the developers opinion. Users know how they (wish to) use the software. That is quite important.\nThat said, changing the default to something simpler doesn't seem to have bad consequenses for konqui, does it?"
    author: "eMPe"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "I dont think my personal experience is the best one.I said taking a poll is a good solution, because you are asking more people."
    author: "Emil Sedgh"
  - subject: "Re: default filemanager"
    date: 2007-02-26
    body: "The results of that poll would be incredibly missleading.  At best you'd just get the opinions of Developers and Power Users, which would be missing out on all the casual users and probably a huge chunk of the power users as well."
    author: "Sutoka"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: ">>why developers think that they know better?\n\nIn the end, they write the code..\n\n\n>> whey you dont try to take a poll and ask users about something like this?\n\nmost users don't take polls.\n\n\n>> Yes!Dolphin (maybe) is a good Simple File Manager.\n\nSo, what's the problem?\n\n>>but whats the matter with Konqueror?\n\nit's too technical and has too tight integrated filemanagement webbrowsing features\n\n>>maybe Distro's will change their filemanager to dolphin, \n\nwhich says enough about konqueror\n\n\n>> KDE aims to be a powerfull Desktop Enviroment.Changing default file manager to Dolphin is againts this.\n\nwhat's the difference between distro's defaulting to dolphin or kde defaulting to it and distro follow that choise?\nBoth ways dolphin becomes the default filemanager on many systems.\npowerusers can change that easily to konqueror\n\n>> I agree that making Toolbars change ith Profiles is the best solution\n\nThat's how konqueror works in kde 3.x\nApperently, that's not considered the best solution for kde4\n\n"
    author: "otherAC"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: ">> In the end, they write the code..\nI know this.i know they write code and they make my life easier.I am a KDE fan, im living with it and i love it.but i dont think every time I want to write a comment, i should repeat these.everybody knows these.\n\n>> most users don't take polls.\nwhich is better?asking 10-50 developers or asking all of dot people who read dot?\n\n>> it's too technical and has too tight integrated filemanagement webbrowsing features\nI think making Konqueror's code cleaner is easier than making a new file manager.and its really the good piece of Konqueror.its both Filemanager and web-browser.\n\n>> what's the difference between distro's defaulting to dolphin or kde defaulting to it and distro >> follow that choise?\n>> Both ways dolphin becomes the default filemanager on many systems.\n>> powerusers can change that easily to konqueror\n\nKDE should try to use the best applications with its default package.lots of distro's will not touch it."
    author: "Emil Sedgh"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: ">>I know this.\n\nBut you asked the question :)\n\n>>which is better?asking 10-50 developers or asking all of dot people who read dot?\n\nI'd say the developers.\nAverage users don't read the dot.\n\nBest choice would be to ask a team of experts to determine what's the best choice for kde.\nNot a bunch of random users..\n\n>>I think making Konqueror's code cleaner is easier than making a new file manager\nYep, and that is what i would like to see.\nBut apperently the kde developers choose otherwise.\nI hope they would change their mind and make konqueror a less cluttered webbrowser/filemanager, but with dolphin in kdebase they chose another path.\n\n>>KDE should try to use the best applications with its default package.lots of distro's will not touch it.\n\nWhat's best is not easy to determine.\nIt looks like distro's are looking for a more simplified desktop, so even if konqueror is default and dolphin part of extragear, chances are that kubuntu for example chooses dolphin as default..\n\n\n"
    author: "otherAC"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: ">> In the end, they write the code..\nI know this.i know they write code and they make my life easier.I am a KDE fan, im living with it and i love it.but i dont think every time I want to write a comment, i should repeat these.everybody knows these.\n\n>> most users don't take polls.\nwhich is better?asking 10-50 developers or asking all of dot people who read dot?\n\n>> it's too technical and has too tight integrated filemanagement webbrowsing features\nI think making Konqueror's code cleaner is easier than making a new file manager.and its really the good piece of Konqueror.its both Filemanager and web-browser.\n\n>> what's the difference between distro's defaulting to dolphin or kde defaulting to it and distro >> follow that choise?\n>> Both ways dolphin becomes the default filemanager on many systems.\n>> powerusers can change that easily to konqueror\n\nKDE should try to use the best applications with its default package.lots of distro's will not touch it."
    author: "Emil Sedgh"
  - subject: "Re: default filemanager"
    date: 2007-02-25
    body: "pls distinguish between filemanager and html rendering.\n\nthere is IMHO nothing equal to konqueror in respect to network transparent accessing of files with all sort of protocols (ftp, sftp, fish, smb, nfs , svn and many more).\n\nUnfortunately not all safari enhancements are ported back yet, so the html and js script engine are not state of the art and it is sometimes necessary to use other browsers. Sometimes some workarounds are not implemented and hence do not treat non standard html and js coding. And there may be bugs too ;-)\n\nIn respect of statistics mentioned, sometimes it's the fault of the web page desingers which check for valid browsers and reject konqueror. Changing the identification to firefox allows to visit the site, but this is propably not  counted as konqueror."
    author: "ferdinand"
  - subject: "Digg this"
    date: 2007-02-23
    body: "Digg this and help spread the news:\nhttp://digg.com/linux_unix/Third_KDE_4_Development_Snapshot_Released_Kludge"
    author: "Tsiolkovsky"
  - subject: "Re: Digg this"
    date: 2007-02-23
    body: "Please, please stop this spamming. Lets digg a big hole and burry digg in it forever. And yes, I know the arguments why this \"digging\" is supposed to be helpfull or important, they come by again and again.\n"
    author: "Andre"
  - subject: "Re: Digg this"
    date: 2007-02-24
    body: "Why not just put a digg this button on every page so these people wouldn't bother posting it in comments."
    author: "Goalie"
  - subject: "Re: Digg this"
    date: 2007-02-24
    body: "Because then we'd have to update the code to include every possible news submission website that's popular on any given week."
    author: "Troy Unrau"
  - subject: "Re: Digg this"
    date: 2007-02-24
    body: "That's actually a good idea."
    author: "Mark Kretschmann"
  - subject: "Re: Digg this"
    date: 2007-02-24
    body: "What about a button \"Digg this\" which shows a page just with content muuuh or something like that ? ;) There could also be a random delay, faking it...\n\nFelix"
    author: "Felix"
  - subject: "Re: Digg this"
    date: 2007-02-24
    body: "Why would we need to \"help spread the news\"? This is an alpha release of interest to developers; I'm sure the target audience pay attention to the dot at least."
    author: "Haakon Nilsen"
  - subject: "Re: Digg this"
    date: 2007-02-24
    body: "let me be the first to say : big up Tsiolkovsky, many thanks for putting it up!"
    author: "teh fierst"
  - subject: "Re: Digg this"
    date: 2007-02-24
    body: "man I get sick of this digging stuff\n\nThis is dot.kde.org, not digg"
    author: "another ac"
  - subject: "Documentation updates?"
    date: 2007-02-24
    body: "I was checking out Solid again, but I was a bit dissapointed by the fact that the website that promisses comprehensive documentation for developers seems to lag behind the actual development of the system itself quite a bit. In fact, there is no documentation to be found at all. How do people expect developers to start using these technologies if you can't find how to use them? "
    author: "Andre"
  - subject: "Re: Documentation updates?"
    date: 2007-02-24
    body: "In the case of solid, it's still somewhat of a moving target for certain things, so writing documentation about all of the features this early is a hindrance, and it needs to be updated 10 times a week.  That said, if you check out #solid on irc.kde.org (redirects to freenode), you should get a decent response when the developers are awake :)"
    author: "Troy Unrau"
  - subject: "Re: Documentation updates?"
    date: 2007-02-24
    body: "Of course, I see that. However, this article also states that the succes of a summer release will depend on application developers porting their applications to KDE4. How are they supposed to do that if the API is still a moving target? I am sure the developers are very helpfull on IRC, but I for one would at least want to have some sort of reference documentation, some sort of overview on how this lib is put together on a higher level before I would considder using it. Not all details need to be filled in in that, but at least give us *someting*?\n\nThe Solid website as it is now is really lagging behind a lot. In the API section is still says: \"First uploads of the API documentation to this website will likely occur in Q2 2006 given the current roadmap.\" "
    author: "Andre"
  - subject: "Re: Documentation updates?"
    date: 2007-02-24
    body: "I agree that API documentation is important, and that Solid is still in flux is no excuse at all. Apidox should be written together with the code, not as an afterthought, so if there's code but no docs then the development practices are subject to be improved.\n\nThat said, updating the web page is most likely not as important as getting fine code (and apidox) pushed out."
    author: "Jakob Petsovits"
  - subject: "Re: Documentation updates?"
    date: 2007-02-25
    body: "I just had a look: \n\n* There are three tutorials for Solid\n\n* There is a Mainpage.dox which gives you a short overview\n\n* I checked 5 header-files, in all 5 _all_ public methods have full API-docs\n\n* There are unittests for both hardware-recoqnition and network-stuff.\n\nWhat more do you want??? And this is not even the complete documentation because there is more to come."
    author: "Carsten Niehaus"
  - subject: "Re: Documentation updates?"
    date: 2007-02-25
    body: "Er. I believed the parent poster without checking it myself. Blame on me, and on those people stating that there is no proper documentation."
    author: "Jakob Petsovits"
  - subject: "Re: Documentation updates?"
    date: 2007-02-25
    body: "Are you sure? Well, I guess you are if you say so...\nMaybe I looked in the wrong place or something, because I didn't find them. I just wend back to look again, but I still can't find them on the Solid website. However, you are right, I did find the documentation you describe in the sources themselves. I am glad there are docs, and to help others find them as well, I plan to try and put them up on the solid wiki or something like that."
    author: "Andre"
  - subject: "Re: Documentation updates?"
    date: 2007-02-25
    body: "Just as an example look here:\n\nhttp://websvn.kde.org/trunk/KDE/kdelibs/solid/solid/battery.h?revision=635769&view=markup\n\nThe examples:\n\nhttp://websvn.kde.org/trunk/KDE/kdelibs/solid/examples/\n\nAPI-docs:\n\nhttp://api.kde.org/cvs-api/kdelibs-apidocs/solid/html/namespaceSolid.html\n\nExample of the class \"Solid::NetworkHw::NetworkHw(QObject * backendObject)\"\n\nhttp://api.kde.org/cvs-api/kdelibs-apidocs/solid/html/classSolid_1_1NetworkHw.html\n\n"
    author: "Carsten Niehaus"
  - subject: "Re: Documentation updates?"
    date: 2007-02-25
    body: "AH! Excellent!\nThank you for these links. I still think it would be usefull to put these links on the Solid website, so you can find these documents from the Solid website where this documentation is still announced as forthcomming. It is good to see that the documentation is actually there. I am going to look into them to see if they might be usefull for a project I want to try... "
    author: "Andre"
  - subject: "Re: Documentation updates?"
    date: 2007-02-25
    body: "Docs should probably be written FIRST, along with unit tests, so that everyone knows what they're aiming for, and knows it's been comprehensively thought out ahead of time.  Still, that's only one development methodology, and I'll be happy just to see it work :)"
    author: "Lee"
  - subject: "why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "why not showing some screenshots, i know till now all the changes are under hood as some dev has said.\n\nfriendly mimoune "
    author: "djouallah mimoune"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "Because, as has been stated many times, it currently just looks like a (very, very!) badly broken KDE 3.5 Desktop :)\n\nWhen you're building a Rolls Royce, the paint-job is always the last thing to be applied ;)\n"
    author: "Anon"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "I think you'll find that the interior trim is the last thing to be applied. Painting is usually one of the first things they do on any car (after the chassis has been welded together)."
    author: "Paul Eggleton"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "Oh. Well, bang goes that analogy, then :("
    author: "Anon"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-25
    body: "Well, you can still analogise the flashy parts of the GUI with a car's interior trim, if you like :)"
    author: "Paul Eggleton"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: ">> i know till now all the changes are under hood as some dev has said.\n\nThen why do you want to see screenshots? As far as I know, it just looks like a broken KDE 3.5.x. Maybe not so broken anymore, and maybe a new window decoration or something like that, but probably not so exciting as it could (and will) be in a few months."
    author: "Lans"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-25
    body: "we could make screenshots of the sourcecode, or screenshots of the buildingprocess, or screenshots of the output of kde4 starting, screenshots of the error messages kde4 applications send to stdout/stderr\n\n:o)\n"
    author: "otherAC"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "You get decent screenshots nearly every week as part of the \"Road to KDE 4\" series. I'd say that's as good as it gets, there's really not much more to visually show off than what Troy covers in his articles."
    author: "Jakob Petsovits"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "I guess we'll have to wait for the inclusion of oxygen icons in order to see something \"new\". I'd really like to see a comparison of a kde desktop with oxygen icons and with the classic crystal icons to see if this is already a little sexier.\nDoes anybody have already seen the oxygen style and theme in the wild ? "
    author: "thom"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "Oxygen is still a work in progress, but here are some screenshots:\n\n1. KDE3 session running KWrite (KDE4) using the oxygen icons (where available) and the oxygen style (as it is at the moment, which is a snapshot of course).\n\nhttp://img157.imageshack.us/my.php?image=snapshot4jr1.png\n\n2. KDE3 session running KWrite (KDE4) and Kate (KDE4) using the oxygen icons (where available) and the cleanlooks style.\n\nhttp://img120.imageshack.us/my.php?image=snapshot5ti2.png\n\n\n<strong>These screenshots are made from svn snapshots and do not indicate anything expect the state of the theme and style at the moment of the snapshot!</strong>"
    author: "Tim Beaulen"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "thanks,\nLooks really promising.\nI wonder why there has been so little buzz around the development of the oxygen style itself, seems like a very important challenge for kde4 eye candy."
    author: "thom"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "Nuno\n\nhttp://pinheiro-kde.blogspot.com/\n\nand David\n\nhttp://oxygen-icons.org/users/david/?p=32\n\nhave been blogging a little bit about it.\n\nJust FYI: I've been building KDE4 with the Oxygen icons, style and windec for a while now, and it looks even more different from \"standard\" KDE than the shots Tim posted.\n\nI won't post screenshots, though, as there is still a little unflattering corruption caused by bugs in the style code (especially with tabs), and I'm not sure if the Oxygen guys would want it posted ;) Suffice to say that it looks (to me, at least) more like OS X than Windows, but more matte, smooth, and less \"bulbous\" looking - kind of silky, understated, light and creamy :) There's some animated effects in there (again, involving tabs) that some people might find garish, though.\n\nHope you found my incredibly vague description helpful ;)"
    author: "Anon"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "Maybe we should create kde4rumors.com to get people even more excited before the official screenies release with blurry cell phone pics showing small parts of the style and features. This could be a funny way to preview alpha stuff without the negative comments due to the inevitable visual glitches.\n\nI am currently building kde4 from the kdesvn build script, do you know if oxygen artwork will be right there or if I need to do something more ? \n "
    author: "thom"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "Hehe - good idea =)\n\n\"I am currently building kde4 from the kdesvn build script, do you know if oxygen artwork will be right there or if I need to do something more ?\"\n\nOxygen is not in the mainline, yet, so you'll need to grab it from svn.  It resides at:\n\nkde/trunk/playground/artwork/Oxygen/\n\nI believe you'll need Inkscape for creating the icon style - look in the \"utils/\" directory for more info.  Since KControl is not currently functional, you'll need to invoke the kcmshell directory to set the theme/ style/ windec to oxygen - \"kcmshell --list\" will give you the info you need.\n\nGood luck!\n"
    author: "Anon"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "ok i have set icons and windec but when run kcmshell style it give me only QT4 style (plastique, cleanlooks and etc...) but not Oxygen\n\nP.S.\ni have compiled and installed oxygen style\n"
    author: "vincent"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-25
    body: "> with blurry cell phone pics \n\nahahahaahahahahaha! best. idea. evar!\n\nthanks for making me laugh =)"
    author: "Aaron Seigo"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-24
    body: "I've been planning to write an Oxygen article for a while, but I have a policy of only featuring things that have made their way into trunk as the defaults.  Oxygen hasn't been merged yet, so I've just been waiting for that date.\n\nWhen the merge happens, count on an article."
    author: "Troy Unrau"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-03-03
    body: "thanks ! "
    author: "patpi"
  - subject: "Re: why you are afraid to show a screenshots !!!!"
    date: 2007-02-26
    body: "You'll be able to view some screenshots in my KDE4 presentation, which I'll try to get online today or tomorrow, together with the second-day report from FOSDEM."
    author: "superstoned"
  - subject: "Nightly builds"
    date: 2007-02-24
    body: "For KDE/Win there seem to be nightly builds (and build warnings)\n\nhttp://public.kitware.com/dashboard.php?name=kde\n\nand tests. What about KDE/Linux?"
    author: "Bert"
  - subject: "Re: Nightly builds"
    date: 2007-02-24
    body: "For linux, see this dashboard :)\n\nhttp://developer.kde.org/~dirk/dashboard/"
    author: "Troy Unrau"
  - subject: "Re: Nightly builds"
    date: 2007-02-24
    body: "Shouldn't these points be part of the debugger of the developer's local build?\n\nIs that dashboard integrated into the EBN suite?"
    author: "wum"
  - subject: "Suse Konqueror Opinion"
    date: 2007-02-24
    body: "From the Suse 10.2 review\n\n\"Konqueror -- and other KDE-related bloat -- has to go. Nobody uses Konqueror for Web browsing -- I have Web stats from half a dozen sites to prove it. It has a klunky interface, doesn't work with plugins very well, doesn't easily do tabbed windows like Opera and Firefox do, and in general has no advantages over its competitors. Get rid of it! We only need one Web browser, and the market has spoken as to its preference -- Firefox. At very least, Konqueror should not have an icon in the default quicklaunch area of the KDE menubar. Konqueror aside, take a look through the kickoff menu sometime and notice how many software categories there are, and how many programs are in each one of them. How much of this crap do we really need? One of my chief complaints about SUSE for many years has been that there is too much standard KDE crap in the default install. People should choose what programs they want, not be force-fed programs that they may never use. This will also make the installation procedure speedier.\"\n\nhttp://www.softwareinreview.com/cms/content/view/66/\n\nI think Konqueror has an advantage: it integrates with KDE applications but that is mostly Firefox fault which does not integrate sufficiently and wants to open all videos with totem, all pdf with evince etc. etc."
    author: "Malte"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-24
    body: "> and the market has spoken as to its preference -- Firefox\nNO: The market has spoken: IE\n\nand why the hell do we have KDE and linux and other OSes all?\nThe market has spoken: Windoof (thats a pun for German speaking users)\n\nI don't see what is so wrong with Konqueror: The only issue I experience is that it is quite slow opening a bookmark folder with 10 pages in tabs simultaneously. FF is much much quicker (BTW, why is that?). Generally, FF is also a good deal quicker at rendering pages.\n\nAfter all, Konqueror is the base of Safari, also not too bad a browser!"
    author: "sulla"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-24
    body: "heise.de \u0096 a large german IT-news page \u0096 statistics:\nsource: http://www.keks.de/web/heise-browser\nyear: % Linux + Unix -- % Konqueror/KDE\n2002-01: 09.8% + 1.8% -- 2.1%\n2005-01: 12.0% + 0.7% -- 2.3%\n2005-09: 11.5% + 0.5% -- 1.9%\n2005-12: 11.8% + 0.5% -- 1.9 %\n2006-06: 12.3% + ? -- 2.0 %\n2006-07: 13.0% + 0.5% -- 2.1%\n2006-11: 13.0% + ? -- 2.1%\n2007-01: 12.7% + ? -- 2.0 %\n2007-02: 13.2% + ? -- 2.2 %\n\nWhen Konqueror is so successfull, why oh why is it stuck at 15% of linux/unix browser marketshare?\n"
    author: "testerus"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-24
    body: "Probably just the percentage of distros that make it their default browser.\nBy the way, why Koffice since the market has decided that OpenOffice is the best? And why Kmail since obviously Thunderbird is much more used?\nBy the way, many distros provide KDE as default desktop but provide Firefox as default browser : wouldn't that mean that Konqueror as a file manager is not that bad?\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-25
    body: "Nope. It just means that there is no app that could be a good replacement for the bloated konqueror. Frankly speaking, konqueror is one of those apps that I hate, but still use them because there is nothing to replace them with. Like the uberbloated OpenOffice, for example."
    author: "zonk"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-25
    body: "why is konqueror bloated?\nOn my modest system, it is lean and fast.\n\nIt has a simple browser profile, so if the interface is too cluttered you can change profiles.\n\nas for browsers: if competitors like opera and firefox can't replace it, why should kde get rid of it?\nWhat browser is left for you?\n\nas for file managers: if none of the competitors can replace konqueror, then what would you use if KDE got rid of it?"
    author: "otherAC"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-25
    body: "Konqueror is, after Linux itself, the thing that realy make me stay with linux.\nMake konqueror a native windows/macOS app, and I will soon like these OS much more.\nAnd I'm always sad when I have to open a page in firefox in order to display it :(\n\nIf you don't like it, use firefox, opera, epiphani, ... but don't claim this is a bad application."
    author: "kollum"
  - subject: "Re: Suse Konqueror Opinion>>>"
    date: 2007-02-25
    body: "Sorry but the claim that \"Konqueror\" is not as much represented is because it not being default is WRONG.\n\nI like KDE.\nI use KDE.\n\nI like Firefox.\nI use Firefox.\n\nI am sorry to say, but Konqueror has several shortcomings.\nIT IS THE FAULT OF KONQUEROR.\n\nThere is no misrepresentation of that.\n\nSometimes you have to speak it out :)\n\n\nPS: THe servlets in Konqueror are great though, its \nwhat I miss in Firefox. "
    author: "me"
  - subject: "Re: Suse Konqueror Opinion>>>"
    date: 2007-02-25
    body: "I love Konqueror\n\nI use it for 95% of my web browsing and it renders most sites fine.\n\nIt is so much faster than Firefox as well... It integrates with the rest of KDE nicely (kget, etc)...\n\nIt's a matter of choice.. use what you like"
    author: "another ac"
  - subject: "Re: Suse Konqueror Opinion>>>"
    date: 2007-02-25
    body: "Well, as we say in France, everybody sees noon at his door.\n\nFirefox doesn't allow me to sort FTP directories on the type of file or on the date of modification.\nFirefox doesn't allow me to download files where I want in the single click, I need to use their damned download manager which is about as flexible as my left toe.\nFirefox doesn't allow me to upload files to a FTP site easily.\nFirefox doesn't allow me to duplicate a tab easily.\n\nThese are some examples of things I can't do with firefox, that's why I prefer Konqueror.\nI still believe that most people use the default program until for some reason they need to try another one (lack of functionnality, someone show them something else which is better, too unstable, etc.). For most people, it never happens, so they stick to the default."
    author: "Richard Van Den Boom"
  - subject: "Re: Suse Konqueror Opinion>>>"
    date: 2007-02-26
    body: "let's not forget firefox doesn't allow you to drag an url to it's empty tab bar to open it (or unto another tab), you can't drag'n'drop tabs between windows, into the filemanager or onto your desktop, you can't do splitscreen nor filemanagement, etcetera. Firefox - I don't get how you can even use it... It's the least worse filebrowser on windows, but on linux, it's not very usefull."
    author: "superstoned"
  - subject: "Re: Suse Konqueror Opinion>>>"
    date: 2007-02-25
    body: ">I am sorry to say, but Konqueror has several shortcomings.\n\nWhat shortcomings? Your post is big on hyperbole and very short on details."
    author: "Paul Eggleton"
  - subject: "Konqueror Opinion"
    date: 2007-02-27
    body: "I have been using KDE and Konqueror for many years - since it was at the beta level. It is, for me the browser of choice. I like the clean integration to the KDE desktop. You cannot just simply drag a file off a web or ftp site and drop it on your desktop to create a local copy with any other browser. I like the fact that there are dozens of protocols, fish:// and smb:// are both particularly useful.\n\nOK, sometimes it's not as good as FF at rendering supposedly Windows only web pages, but thank goodness these are not as common as they used to be.\n\nWhat I would really like to see is Konqueror being given the same javascript interpreter as is used by Mozilla, and an option to be able to use Gecko as the HTML rendering engine if desired. Is this even possible?\n\nAs far as a file manager is concerned, I have a couple of friends who are still running the archaeological XWC simply because it has unique features which no other file management system offers. A true re-implementation of that package would be very welcome!\n\n"
    author: "Christopher Sawtell"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-24
    body: "I mostly use Konqueror because it is much faster than Firefox and because its rendering is more pleasant. In addition, as most KDE applications, it has many (possibly user-defined) shortcuts and it is very flexible. One can be much more efficient with Konqueror than with Firefox. I switch to Firefox from time to time when the website is not compatible with Konqueror, which is seldom the case with clean websites.\n\nKeep up with the good work!"
    author: "Hobbes"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-24
    body: "I also use konqui and firefox interchangeably. It's very useful to have multiple browsers. Personally, I use firefox for most things, but use konqui for anything using embedded media. I really hate to loose 150 open tabs because of a crash in Java/Mplayer/Flash - and Konqueror allows you to just kill the subprocess without terminating the main browser."
    author: "Richard Neill"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-26
    body: "I've got to ask, how do you manage 150 tabs in Firefox OR Konqueror?\n(I use Opera.  I like konqueror, but Opera is just better)"
    author: "Yuriy"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-24
    body: ">Nobody uses Konqueror for Web browsing -- I have Web stats from half a dozen sites to prove it. \nThis is probably because people like to use the same browser on Windows or on Linux, not directly related to Konqueror strength/weaknesses \n\n>market has spoken as to its preference -- Firefox. \nBleh, Opera is much better: FF responsiveness suck big time.\n\nDo not pass your preferences as 'people', I myself prefer the 'all-in-one' interface of Konqueror than multiple browsers."
    author: "renoX"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-24
    body: "In all honestly I'd use Internet Explorer before Firefox.  Firefox is slow, not standards compliant (slightly better than IE doesn't makes you the second worst), horribly ugly and will NOT fit in with any environment, the whole 'profiles' thing in Firefox seems to be code they just forgot exists and is only there to ignore the user when an instance locks up on startup or you try and open multiple instances too fast for it.\n\nI think Konqueror's tab/split view are far superior to Firefox's, plus any problem in Firefox is the fault of the extension writers, yet the feature thats touted as the best thing in Firefox is the extensions!  Theres been HORRIBLE memory leaks in Firefox since the dawn of time that the developers have mostly ignored (I think they finally fixed a couple with 2.0, or at least they claimed... most of what I've heard is its even worse in 2.0).\n\nSorry for the rant, but the firefox hypocrites really annoy me (like the article on slashdot about a guy finding a security hole in Firefox every day for a month is a GOOD thing, while a single minor bug in IE is just unacceptable)."
    author: "Sutoka"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-02-25
    body: "I use Konqueror exclusively now, even though still have Firefox installed, but almost never use it. Konqueror is fast, light, stable, and its KParts technology is amazing. Firefox is slow, and eating too much memory. I guess approximately 10% of Linux / Unix users choose Konqueror as their browsers, and by porting KHTML to Windows, market share for Konqueror will increase significantly. Unfortunately some websites recognise only major browsers and not Konqueror."
    author: "eds"
  - subject: "Re: GNOME TROLL"
    date: 2007-02-25
    body: "From the article style, I'm guessing it is a GNOME user, who writes about KDE. There is a sentence about the new kickoff menu in suse: \"It makes KDE almost useable\". Well I honestly hate that new start menu in openSUSE, even after playing half an hour with it. And he complains about what they did with GNOME in SLED, an other opinion that a KDE user would never make...\n\nI do not think you can take this article to complain about konqueror. I more think that those Gnome users don't have such a nice place as dot.kde.org, and are now visiting this news site...\n\nAnd how about what kind of bloat? Look at windows. It has much more bloat, and if you want to have less bloat, don't use GNOME with GTK+, just use something like XFCE or even simpler....."
    author: "boemer"
  - subject: "Re: Suse Konqueror Opinion"
    date: 2007-03-05
    body: "Huh?  I use Konqueror all the time, and I've read plenty of posts on Slashdot and elsewhere from other Konq users.\n\nKonq is a great web browser for most purposes.  It's lighter weight and faster than Firefox, and it works better with KDE.  It doesn't work properly on some sites, however, so I just use FF on those sites.\n\nAs someone else said, the market has spoken to its preference: IE.  So why do we have Safari, Opera, Firefox, and Konqueror?  Because not all of us like IE, thankfully.  How many Mac OS X users do you know who use IE?\n\nUsing a browser should be like using an audio player or an email client.  I should be able to listen to the same MP3s/Oggs with any music player I use, and I should be able to read my emails with any email client I choose, and for the most part, it works this way (all the *nix/OSS music players support Ogg).  Why should I have to use a particular browser to browse web sites?\n"
    author: "Grishnakh"
  - subject: "two graphical shells to core KDE functions"
    date: 2007-02-24
    body: "Most people commenting on Dolphin and Konqueror here seem to be focused on the file management part only and argue which app would be a better default (an argue I am not interested in).\n\nI would be interested to know *how* Dolphin will affect the default web browser GUI of Konqueror because Konqueror will stay as web browser if I understand it correctly. KHTML/KJS will stay anyways regardless if it is the Apple or KDE flavour (a replacement chance has gambled away by the closed-minded Mozilla foundation at least two times).\n\nI personally must confest that I have often two Konquerors opened in order to confuse myself less. ;-) One browser Konqui and one file manager Konqui, although I also very often I open on the fly a web page in the file manager or a directory on the web browser (Konqui lets you \"just do\" things ;-).\n\nSo what I particular would be interested in?:\n* A pure file manager (Dolphin?) for local *and* remote files (I am pretty sure that Dolphin will only have a chance if it is network transparent). So basically a grahical (pseudo-)file management shell for all KIO-Slaves.\n* A viewing shell (Konqueror?) for all kinds of KParts, for example KHTML, image plugins, documents...\n\nSo Dolphin and Konqueror would be just two frontends to basic KDE features any application knows to handle implicit, too (saving files on remote places, displaying HTML emails and so on...).\n"
    author: "Arnomane"
  - subject: "Re: two graphical shells to core KDE functions"
    date: 2007-02-24
    body: "Konqueror will ~stay the same as it is now, i.e. an all-in-one power-tool. Additionally to that there will be Dolphin. Which one you use is your choice."
    author: "Anonymous"
  - subject: "Re: two graphical shells to core KDE functions"
    date: 2007-02-24
    body: "So will there be a lightweight web browser which focus only on web browsing like dolphin focus only on file managing ?\nThis might be a good idea as Zack shown some progress on integrating webkit into qt4.\nA light cross platform webkit webbrowser could be very nice."
    author: "thom"
  - subject: "Re: two graphical shells to core KDE functions"
    date: 2007-02-25
    body: "agreed; peter penz came along with dolphin, we just need someone to do similarly with a web browsing app along a similar vein =)\n\ngeorge staikos has been working on something, hopefully it emerges ... \n\nand it perfectly ok if it gets added post-4.0. kde4 is more than the four-dot-oh"
    author: "Aaron Seigo"
  - subject: "Re: two graphical shells to core KDE functions"
    date: 2007-02-25
    body: "if we could get the same buzz that firefox has then we could use the app to promote kde.\nLet's call it Kevlar (light and secure) for the argumentation purpose. \n\"Kde: where amarok, kevlar and koffice feel at home\"  youhou !\n\nI firmly believe your involvment and (super)vision of kde is going to make the difference. "
    author: "thom"
  - subject: "Re: two graphical shells to core KDE functions"
    date: 2007-02-25
    body: "Okay, sooooooooooooooooooooooo if Dolphin is just a different front-end to all the same things Konqueror does and WILL do ... why do we need this kind of duplication?  It makes absolutely NO sense if Konqueror will still be as capable a file manager as it is now!  (The most capable I've ever used BTW)\n\nI think the dev resources could be MUCH better used to just improve Konqueror ... like give Konq the Dolphin features/interface while it's in file manager profile mode ... and back to the web interface for internet browsing.\n\nThis just seems really needless and wasteful."
    author: "DigitaLink"
  - subject: "Re: two graphical shells to core KDE functions"
    date: 2007-02-25
    body: "Read much? \nhttp://enzosworld.gmxhome.de/ and http://enzosworld.gmxhome.de/news.html"
    author: "illogic-al"
  - subject: "Re: two graphical shells to core KDE functions"
    date: 2007-02-25
    body: "You're forgetting the tiny detail called \"feature sharing\". KDE is excels at this.\n\nBasically, Dolphin and Konqueror will share much of the backend. The views, icons, layouts, etc. will probably be the same in both applications. Just the shell will change -- and the fact that Dolphin won't load any KPart under the sun like Konqueror is meant to do.\n\nThere's no (or very little) waste of resources if code is shared.\n\nFinally, there's always the fact that this is Open Source: we cannot force people to work on what they don't want to. If those guys want to work on Dolphin, who are we to tell them otherwise?\n\nUnless, of course, you're prepared to sponsor a developer."
    author: "Thiago Macieira"
  - subject: "OT web based Kopete???"
    date: 2007-02-24
    body: "http://wwwl.meebo.com - web based multi messenger, no downloads, no installs, just works!\n\ndo we still need gaim, kopete, other multi-messengers when web based are available?"
    author: "fast_rizwaan"
  - subject: "Re: OT web based Kopete???"
    date: 2007-02-24
    body: "Beside I suspect unashamed advertising. Yes we do!"
    author: "Bernd"
  - subject: "Re: OT web based Kopete???"
    date: 2007-02-24
    body: "Yeah cause gaim and kopete aren't installed on pretty much every linux distribution out there by default, and NEVER work, right (there goes every single 'feature' meebo has)?  Talk about shameless advertising...  Make like a java applet and fade into obscurity.\n\n(Wow I must have woken up on the wrong side of the bed this morning or something...)"
    author: "Sutoka"
  - subject: "Re: OT web based Kopete???"
    date: 2007-02-24
    body: "yes"
    author: "xyz"
  - subject: "Re: OT web based Kopete???"
    date: 2007-02-24
    body: "So... do you promise you won't steal my MSN password? o_O"
    author: "Darkelve"
  - subject: "Re: OT web based Kopete???"
    date: 2007-02-25
    body: "Of course we do. Have you seen email clients disappear after the invention of webmails?\n\nNo, they have not. The reasons are actually quite simple: they are faster to use, more integrated with the desktop and quite often more powerful. (Hey, I feel like I'm describing KDE here)"
    author: "Thiago Macieira"
  - subject: "Summer 2007"
    date: 2007-02-24
    body: "So as I'm in Australia, does this mean the release by summer of KDE4 is imminent?\n"
    author: "devnull"
  - subject: "Re: Summer 2007"
    date: 2007-02-25
    body: "No, for you the release will be December 21st :-)"
    author: "Thiago Macieira"
  - subject: "Oversized dialogues"
    date: 2007-02-25
    body: "With KDE applications I often get bugs like you can see in the attached screenshot file. Oversized dialogues because text fields contain text other than expected. \n\nCant there be a KDE gui testing environment which prevents this to happen, which tests windows, a smarter windows management code? Is that provided by QT4? I really hope that KDE will put an end to these annoyances."
    author: "Andy"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: "that dialog essentially needs one line of code:\n\nlabel->setWordWrap(true);\n\nsend in a patch or let the devs know and i'm sure they'll pop it in post-haste.\n\n> gui testing environment\n\nwould be nice. huge amount of work. send more workers. elf needs food badly."
    author: "Aaron Seigo"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: "you could use RC versions of amarok and report your findings to the amarok team.\nbugs.kde.org is a good starting point for those reports"
    author: "otherAC"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: "I use mandriva and these are the recent packages. I don't know how to use the KDE build system to compile Amarok myself. As so many different libs are involved I assume it should take some time.\n\nThe point is that it's not only amarok, you get it with so many KDE applications from time to time. And it was always a problem with KDE. So the question is: That window had a width that exceeds the screen size. Can't that be \"tested\"? In the sense of: expected behaviour, unintended occurance. \n\nI am thinking of a talkback KDE version with realtime testing. Whenever the window or dialogue manager realises that the window exceeds screen size, it saves a screenshot and makes a log entry. This directory can then be submitted to a KDE testing server.\n\nOr: Everytime a font character is used that cannot be displayed.\n\nMany bugs you will only find in a realworld environment. When you have a tool as Amarok, why not use real files .\n\nBy the way: most often the bug occurs with the context tip screen."
    author: "Andy"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: ">>Many bugs you will only find in a realworld environment. When you have a tool as Amarok, why not use real files .\n\nThats why i suggest to use prereleases of the software and report the bugs you find while testing it in a realworld environment.\n\nFor example font testing would be quite hard in a testing environment because KDE does not ship any fonts, so it can't create a situation that every character in every language will be displayed with the right fonts that can display them."
    author: "otherAC"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: "A test suite can create extraordinary conditions and simulate certain activities.\n\nE.g. you have a collection of 5000 fonts on your server and try them out with a test program and a script. Regressions are then also available.\n\nOr you have a tool that fetched any pdf from the internet and then tries to feed it to okular. Failed pdfs get saved and examined.\n\nOr let khtml do random webbrowsing until it fails.\n\nOr you have talkback versions where e.g. all console error messages het logged.\n\nIn general no window should ever get more than 100% width of screensize. But that is something which can be tested. "
    author: "Andy"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: "Generally testing and generating test are very time consuming and require lots of work. And in the end you are still limited by what the developer of the test thinks of, and what he decides is required to test. Its likely the developer of the application does not consider all extraordinary conditions, and often it's the same developer writing the test making it very unlikeley the test will cover it. That's why it's important to report such bugs back to the developers, it's the only way they can learn of the problem.\n\nDesigning test that are both efficient and have high coverage are difficult and require skill. And in the end it's always a tradeof between the reasonable amount of time spent testing and what's being tested. Designing tests is a valued skill in the enterprise, you can make a decent living as a full time test engineer.\n\nTesting random stuff is not particularly usefull, it's time consuming, it's very hard to tell what you have tested and it's not deterministic. You most likely test some things 100s of times, where other stuff never gets tested. And it lacks repeatability, making reproducing and thus fixing the bugs harder.\n\n "
    author: "Morty"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: "What is the problem? Real world = Real random broken content. So the pdf thing works like this: You fetch a random file from the net, open it. If the test program crashes you have a file that crashes the application.\n\n----\n\nThen there are some occasions where usually a popup dialogue is displayed, e.g. because a certain protocol does not support the functionality. A talkback version enables you to report the incident as a bug because that is what it is. \n\n"
    author: "Andy"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: "In this case the problem boils down to coverage and time, and that what makes this approach not usable.\n\nSimply put, with your pdf example, the odds are good that the first 10.000 random files you download will not crash the application. One reason for this is that most pdfs only uses a small part of the pdf specification, and that part being the general case which by nature the best supported. You have now spent lots of time basicly testing the same thing over and over again. \n\nBut even worse from a testing perspective, you still have no clue what you have not tested. And very little indication of which part of the application you have tested, without dissecting the the 10.00 files to see what part of the spec they represent(And most likely that's already covered by a few of your reference pdfs).  \n\n"
    author: "Morty"
  - subject: "Re: Oversized dialogues"
    date: 2007-02-25
    body: "On an unrelated note, Herreweghe's Bach sucks, you should really try some J.E. Gardiner ;)"
    author: "g."
  - subject: "GUI-Effects"
    date: 2007-02-25
    body: "Will there be any UI-Effects like in E17 or Opera. Or at least it should be possible. Polyester is cool, but I think some hacks were used:\nhttp://aseigo.blogspot.com/2006/09/qtimeline-timers-and-what-to-avoid-in.html"
    author: "ENFORCER"
  - subject: "Re: GUI-Effects"
    date: 2007-02-26
    body: "Qt4 will make many GUI effects easy to create and fast to run, so I guess we'll see some neath stuff..."
    author: "superstoned"
  - subject: "Re: GUI-Effects"
    date: 2007-02-26
    body: "Does that include theming effects like getting a marble looking kde, like in kde2?"
    author: "ac and the rest"
  - subject: "Kludge"
    date: 2007-02-27
    body: "\"A kludge (or, alternatively, kluge) is a clumsy or inelegant solution to a problem or difficulty. In engineering, a kludge is a workaround, typically using unrelated parts cobbled together. Especially in computer programs, a kludge is often used to fix an unanticipated problem in an earlier kludge; this is essentially a kind of cruft. Those illustrating the tenor of the term often say that it takes a skilled craftsman intimate with the task, the material at hand, and the operating environment to construct a workaround clunky enough to be called a kludge.\"\n\nFrom the wikipedia.\n\nDid kde choose this word to indicate why they added Dolphin to kde4?\n"
    author: "whatever noticed"
  - subject: "Kubuntu packages don't work"
    date: 2007-02-28
    body: "Apparently the startkde script needs /usr/bin/qdbus, which is not provided by the libqt4-core-kdecopy package, which kde4base-dev depends upon :(\n\nWhenever I start KDE4 from /usr/lib/kde4/bin I get the message \"Could not start D-Bus. Check your installation.\"\n"
    author: "Jeroen van Disseldorp"
  - subject: "Re: Kubuntu packages don't work"
    date: 2007-09-03
    body: "I have exactly same problem. Have you used Xephyr? For me Xephyr just launches new empty window with no way of running any command in it."
    author: "Mathew"
  - subject: "Slow :| why?"
    date: 2007-03-03
    body: "I thought porting to QT4 will increasy speed? It didn't happen? why? I hope it will be fixed before stable relese\n\nbtw. other improvements are realy great"
    author: "patpi"
---
The <a href="http://www.kde.org">KDE project</a> announces the availability of the third development snapshot of the upcoming KDE 4. This snapshot is meant as a reference for developers who want to play with parts of the new technology KDE 4 will provide, those who want to start porting their applications to the new KDE 4 platform and for those that want to start to develop applications based on KDE 4. This snapshot is not for end users, there is no guarantee that it will be stable, and the interfaces are subject to changes at any time.



<!--break-->
<p>This new development version of KDE features the following new technologies:
<ul>
    <li>Sonnet which has previously been covered on <a href="http://www.linux.com/article.pl?sid=07/02/01/1935238">Linux.com</a>.
        </li>
    <li><a href="http://solid.kde.org/">Solid</a>, KDE 4's unified layer to deal with hardware and network resources.
        </li>
    <li>Vastly-improved support for the Windows and Mac OS platforms by cleaning up the source code from dependencies on X11. See <a href="http://kdelibs.com/">kdelibs.com</a> for more information.
        </li> 
    <li>The recently added filemanager <a href="http://enzosworld.gmxhome.de/">Dolphin</a> which will be the default filemanager for KDE. Konqueror will still be available and share much code with Dolphin.
        </li> 
</ul></p>

<p>After <a href="http://dot.kde.org/1155935483/">"Krash"</a>, the first development snapshot, this is another milestone towards KDE 4.0 which will be released later this year. The KDE developers aim at a release in summer 2007. Reaching this target depends on application developers picking up the new technology to use in their applications. While "Krash" marked the milestones initial Qt 4 port, the use of <a href="http://wiki.kde.org/tiki-index.php?page=DBUS">D-Bus</a> as the inter-process-communication system, the merge of <a href="http://phonon.kde.org/">Phonon</a> as the multimedia framework and <a href="http://www.cmake.org/">CMake</a>, KDE's new buildsystem defines this latest release.</p>

<p>The next planned change is new integration of <a href="http://oxygen-icons.org/">Oxygen</a>, the new artwork concept. Work on <a href="http://plasma.kde.org/">Plasma</a> is also taking up pace.</p>

<p>Download the <a href="http://www.kde.org/info/3.80.3.php">3.80.3 source</a>, or install packages for <a href="http://kubuntu.org/announcements/kde4-3.80.3.php">Kubuntu</a> or <a href="http://software.opensuse.org/download/KDE:/KDE4/">openSUSE</a>.</p>

<p>For those who want to keep track of the development process, <a href="http://dot.kde.org">KDE Dot News</a> regularly covers new and upcoming technologies through the series "Pillars of KDE 4" (informing about upcoming technologies) and "The Road to KDE 4", which covers new functionality that has been integrated in the official development tree.</p>

<p>Questions about KDE 4 are answered on various mailing lists such as <a 
href="https://mail.kde.org/mailman/listinfo/kde-devel">kde-devel</a> and <a href="https://mail.kde.org/mailman/listinfo/kde-buildsystem">kde-buildsystem</a>, as well as on #kde4-devel on irc.freenode.org. Documentation for getting up to speed with KDE 4 development is available from <a href="http://wiki.kde.org/tiki-index.php?page=KDE4">a number</a> <a href="http://developer.kde.org/build/trunk.html">of sources</a>.</p>

