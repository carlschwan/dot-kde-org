---
title: "Qt 4.3.1 Allows for More Free Software Licences"
date:    2007-08-08
authors:
  - "jriddell"
slug:    qt-431-allows-more-free-software-licences
comments:
  - subject: "Licenses Added?"
    date: 2007-08-08
    body: "What were the permissible licenses for one's app before? Just the GPL ... ? If so, that's quite a large number of additional exceptions!"
    author: "Anon"
  - subject: "Re: Licenses Added?"
    date: 2007-08-08
    body: "Previously it was GPL, openssl and QPL, so this is indeed something extra.  I would still strongly recommend everyone use \"GPL 2 or later\" though since having different licences causes a lot of hassle."
    author: "Jonathan Riddell"
  - subject: "Re: Licenses Added?"
    date: 2007-08-08
    body: "and of course of win32 and mac Qt was not QPL/GPLv2 but only GPLv2. this clarification is quite big for those platforms in particular."
    author: "Aaron J. Seigo"
  - subject: "Re: Licenses Added?"
    date: 2007-08-08
    body: "No GPLv3 yet? i think that being able to license Qt apps under GPLv3 would be a very important step in this moment, and i hope that this license will be eventually added too (possibly without removing support for GPLv2 i mean)."
    author: "Maurizio Monge"
  - subject: "Re: Licenses Added?"
    date: 2007-08-08
    body: "Qt's main market is potable handheld devices etc. I think they would like to go for GPL3!"
    author: "Anonymous bin Ich"
  - subject: "Re: Licenses Added?"
    date: 2007-08-09
    body: "If Qt is already GPL v2 or later, you can write GPL v3 applications with Qt still."
    author: "Matt"
  - subject: "Re: Licenses Added?"
    date: 2007-08-10
    body: "The problem is that it isn't, it's GPL v2 only.\n\nNot all is lost though, because Trolltech keeps control of the copyright, so they can change the license or add a new one whenever they want. So what people are unhappy with is that the GPL v3 hasn't been added as an option yet.\n\nI hope this clarifies the situation."
    author: "Kevin Kofler"
  - subject: "Re: Licenses Added?"
    date: 2007-08-13
    body: "No, Qt was available under the GPL, meaning that you could already use it with any GPl-compatible license, not just the GPL."
    author: "Jim"
  - subject: "Will kde-4.0 upgrade it's base Qt?"
    date: 2007-08-08
    body: "While doing Qt-4.3.1 + kdesvn build right now, was wondering does the kde-4.0 be needing Qt-4.3.0 or 4.3.1+?\n\nAs there are so long list of bugs fixed already, and before the kde-4.0 comes out, there simply must be bugs killed off from Qt... Qt-4.3.2 coming out before kde-4.0?\n\nJust a silly side question. ;-)"
    author: "anon"
  - subject: "Re: Will kde-4.0 upgrade it's base Qt?"
    date: 2007-08-09
    body: "Oh well, so the Qt-4.4 is coming with flicker free graphics: http://labs.trolltech.com/blogs/2007/08/09/qt-invaded-by-aliens-the-end-of-all-flicker/\n\nMaybe the question should be that could the kde4 be able to move to that after kdelibs have moved to Beta stage?"
    author: "anon"
  - subject: "Re: Will kde-4.0 upgrade it's base Qt?"
    date: 2007-08-09
    body: "qt minor releases should be binary compatible. so all you need is to update to a newer qt version when its released to get the improvements. but qt 4.4 still needs a few months as far as i know, so it will not be ready for kde 4.0."
    author: "ac"
  - subject: "Re: Will kde-4.0 upgrade it's base Qt?"
    date: 2007-08-09
    body: "Yep, I knew that, was just wondering if it would have come out before kde-4.0, it could have made a difference on the kdelibs. :-)\n\nHopefully the fixes are used on the kde-side of things when it comes out thought."
    author: "anon"
  - subject: "SWT/Qt?"
    date: 2007-08-08
    body: "Does this mean that it would now be (legally) possible to create an SWT/Qt implementation? As far as I know, SWT is licensed under the Common Public License."
    author: "Mirko Stocker"
  - subject: "Re: SWT/Qt?"
    date: 2007-08-08
    body: "SWT is licensed under the Eclipse Public License, according to wikipedia EPL was designed to replace the CPL. But the EPL isn't included on the list, so I guess not yet."
    author: "Skeith"
  - subject: "Re: SWT/Qt?"
    date: 2007-08-08
    body: "I think it was always possible to create a SWT/Qt implementation based on Qt/X11's QPL licence.\n\n"
    author: "Kevin Krammer"
  - subject: "No GPLv3"
    date: 2007-08-08
    body: "I would have thought GPLv3 would have been one of those exceptions.\n\nAs it is right now, you can still not license your code under GPLv3, link it to QT and distribute without violating Qt's GPLv2.\n\n"
    author: "Paul"
  - subject: "GPLv3 & EPL"
    date: 2007-08-08
    body: "The two most important uses for a license exception in QT have not been included: namely, GPLv3 and EPL ( for that historically elusive QT-based\neclipse ).\n\nDisappointing to say the least."
    author: "autocrat"
  - subject: "Re: GPLv3 & EPL"
    date: 2007-08-08
    body: "Yup, very disappointing indeed."
    author: "fish"
  - subject: "Re: GPLv3 & EPL"
    date: 2007-08-08
    body: "they're still discussing the implications of GPLv3, but I wouldn't be surprised if Qt 4.4 will also be available under the GPLv3."
    author: "jospoortvliet"
  - subject: "Re: GPLv3 & EPL"
    date: 2007-08-08
    body: "I wonder if the EPL being left is related to the recent release of the QtJambi and a SWT-Qt backend being perceived somehow as competition (which would be a mistake, given that it will improve the situation for people using SWT apps on Linux, but it will have no direct impact for SWT developers as they still wouldn't benefit from the Qt API) :S\n"
    author: "attendant"
  - subject: "Re: GPLv3 & EPL"
    date: 2007-08-09
    body: "Taken from my comment in my blog post at http://labs.trolltech.com/blogs/2007/08/08/qt-431-released/ :)\n\nEPL and CPL are very similar. We have CPL listed in the GPL Exception. We weren\u0092t aware of this interest for the EPL. But now that we have heard the interest for EPL from various sources, we have brought it to notice of our legal department. IMO, EPL is just a matter of time, maybe even the next patch release.\n\nAs for GPL v3, we are actively working on it. From what I understand, it is a bigger step and not something we can just provide with an Exception."
    author: "Girish"
  - subject: "Re: GPLv3 & EPL"
    date: 2007-08-10
    body: "Well, every thread I've seen with people interested in making a SWT-Qt backend ends up concluding that it can't be done because of licenses issues... So yes, I'd say it would be a big deal."
    author: "attendant"
  - subject: "Free to Commercial"
    date: 2007-08-09
    body: "Slightly OT, but I wish that one day TT will make it possible for me to make a commercial version of my open source program. I'm a single developer who've created an application using the open source version of Qt. I have no job or other income at the moment, so it'd be perfect for me to create a commercial version of my software. Unfortunately, the license doesn't allow that (I can see the reason for that clause and I agree with it, but I think TT shold be open to exceptions).\n\n"
    author: "Apollo Creed"
  - subject: "Re: Free to Commercial"
    date: 2007-08-09
    body: "Using GPL Qt != cannot sell my software.\nUsing GPL Qt == should release the code for my software."
    author: "Girish"
  - subject: "Re: Free to Commercial"
    date: 2007-08-09
    body: "First of all, that's not true, because you can sell GPL software.\n\nSecond of all, did you have a point?"
    author: "Apollo Creed"
  - subject: "Re: Free to Commercial"
    date: 2007-08-09
    body: "Sorry, read your msg too fast.\n\nThe thing is, surely the \"cannot close software once it's developed using GPL Qt\" clause is intended to prevent people (companies) from using GPL Qt in a group of people and then purchasing _one_ license to release it as proprietary software? I think in cases where it's obviously only one developer there should be room for some flexibility. \n\nAgain, I apologize for the above comment.\n\nBy the way, are there any examples of people who create and sell GPL software and actually make some money from it? I did consider the following:\n\n* Make source available on the project's homepage, and perhaps a Linux binary\n* Make a Windows binary that you sell (of course if you know how, you can download the source and compile for Windows)\n\nHowever, it doesn't feel \"right\"... I'm afraid that people would think that I'm trying to fool them into paying for something that they can have for free.\n\nWhat do you guys think?"
    author: "Apollo Creed"
  - subject: "Re: Free to Commercial"
    date: 2007-08-09
    body: ">By the way, are there any examples of people who create and sell GPL software and actually make some money from it? \n\nSure. I did, and I'm sure many others did as well. But... that was specially made stuff, basicly for in-house use and not for general sale. I think there's a difference there. Still, you should not think it is impossible. You are only obliged to make the source available to the ones you give the binairy, AKA, the ones buying your software. So, you give them the source on the CD as well. Why would they want to distribute that? They might, of course, but they would be putting you out of business, and so their support would be gone as well. If your software is worth buying, people are probably interested in you supporting it as well. Making your sources available for general download isn't a requisite of the GPL. "
    author: "Andre"
  - subject: "Re: Free to Commercial"
    date: 2007-08-09
    body: "TT needs to make a living somehow. Still, try to talk to sales, they will have an open ear for your problem I bet. Just drop them a mail. Other than that, for startups and small companies there is a special deal where you can get Qt licenses at a 65% discount. Check http://trolltech.com/products/qt/licenses/licensing/smallbusiness.\n\n"
    author: "Daniel Molkentin"
  - subject: "Re: Free to Commercial"
    date: 2007-08-09
    body: "Thanks for the reply.\n\nFor the record, I agree with TT's business model completely. They are doing a great thing letting people use their code for free while selling their product at the same time.\n\nI did drop them a mail about a year ago asking whether it'd be possible for me to get permission to buy a commercial license and then release my software commercially even though I've developed it using the open source version, but the reply quoted the relevant section from the license, that you have to use the commercial version from the beginning.\n\nAgain, I can see the reason for that and I just think it'd be nice for me personally if they had replied in a positive way."
    author: "Apollo Creed"
  - subject: "Re: Free to Commercial"
    date: 2007-08-09
    body: "Just to be clear: my problem isn't that I have to pay the price for the commercial version, that's perfectly OK and I'd be happy to do that; the problem is that I would have to re-implement the whole thing from the beginning, that's just too much work for me."
    author: "Apollo Creed"
  - subject: "Re: Free to Commercial"
    date: 2007-08-10
    body: "You could release most of your code as a LGPL library. Then buy Qt commercial and write an application from scratch using that and your LGPL library. \n\nOf course the more you release as LGPL, the easier it would be for potential competitors to use the same library to beat you."
    author: "Allan Sandfeld"
  - subject: "Re: Free to Commercial"
    date: 2007-08-10
    body: "Talk to Trolltech sales. If lawyers ran companies, nothing would ever be sold.\n\nThe purpose of that license clause is to prevent people from using a full team of developers to create the \"open source\" version, then purchasing a single license for the proprietary release. This doesn't sound like your case at all. If you started your open source project not knowing that you would eventually make it proprietary, then you are in the clear."
    author: "David Johnson"
  - subject: "enough!"
    date: 2007-08-09
    body: "Quit focusing on licenses already, it wastes everybody's time.  Let's get KDE4 out of the door already."
    author: "Anonymous"
  - subject: "Re: enough!"
    date: 2007-08-09
    body: "Huh? I think you really don't want Trolltech's lawyers trying to develop KDE 4. ;-)"
    author: "Kevin Kofler"
  - subject: "Re: enough!"
    date: 2007-08-09
    body: "Look, Qt is at stable version 4.3.1.  Trolltech can *afford* to lose time with licenses.  What stable version number is KDE at?  Enough said."
    author: "Anonymous"
  - subject: "Re: enough!"
    date: 2007-08-10
    body: "I'm confused.\n\n1. \"Quit focusing on licenses already, it wastes everybody's time.\"\n\nHow is discussing licensing issues wasting time? The only people participating are those who think such licensing considerations are important, and surely that's all that matters; if they didn't spend time on what they thought was important, you wouldn't have a KDE at all.\n\n2. \"What stable version number is KDE at? Enough said.\"\n\nEnough said? You asked what I assume is supposed to be a rhetorical question, but I can't see anything resembling an argument in it. Would you care to explain what you mean?"
    author: "Jeff"
  - subject: "reading the link..."
    date: 2007-08-12
    body: "I see this:\n\nB) You must, on request, make a complete package including the complete source code of Your Software (as defined in the GNU General Public License version 2, section 3, but excluding anything excluded by the special exception in the same section) available to Trolltech under the same license as that granted to other recipients of the source code of Your Software.\n\nWhat is the purpose of this? Seems kind of arbitrary."
    author: "illissius"
  - subject: "Re: reading the link..."
    date: 2007-08-13
    body: "I am not a lawyer, but as far as I can tell this means you can't take a BSD-licensed library, modify it, keep the source code private, link the result to Qt, then distribute it without the source code for the BSD-licensed part and claim the exception applies because the code is BSD-licensed, you have to provide the source code at least to Trolltech on request. You don't have to do anything special if you just distribute the entire source code in the first place, because then it's already available to Trolltech."
    author: "Kevin Kofler"
---
Trolltech have <a href="http://troll.no/company/newsroom/announcements/press.2007-08-07.3596046644">announced the release of Qt 4.3.1</a>.  This release <a href="http://trolltech.com/developer/notes/changes/changes-4.3.1/">adds bug fixes and performance optimisations</a>.  More significant however is the new <a href="http://trolltech.com/products/qt/gplexception">licence exceptions</a> added to their Free Software edition.  This means Qt software can be used along with a larger range of other Free Software libraries and dependencies.  The <a href="http://labs.trolltech.com/blogs/2007/08/08/qt-431-released/">Trolltech blog</a> is celebrating the release with photos of the Trolltech support teams.


<!--break-->
