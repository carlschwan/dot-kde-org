---
title: "Wade Olson Interviewed About KDE 4"
date:    2007-08-29
authors:
  - "sk\u00fcgler"
slug:    wade-olson-interviewed-about-kde-4
comments:
  - subject: ":D"
    date: 2007-08-29
    body: "Nice interview, Wade ;-)"
    author: "jospoortvliet"
  - subject: "Definitely nice!"
    date: 2007-08-29
    body: "Definitely nice! Though I found the questions a bit standard, but the answers give a nice positive image for Open Source software. It's a blessing to read something like this instead of the next OOXML scandal, and I hope more people will pick this news up. I like reading good news too :)"
    author: "Diederik van der Boor"
  - subject: "Re: Definitely nice!"
    date: 2007-08-30
    body: "I really like the answers, great interview!\n\n>> instead of the next OOXML scandal\n\nOh, you're Swedish too? Or have I missed something important? (Wouldn't be surprised if I have; haven't had access to the net for some weeks :/)"
    author: "Hans"
  - subject: "Re: Definitely nice!"
    date: 2007-08-31
    body: "Seems to me more that the discussion about the ISO scam lacks audience."
    author: "bert"
  - subject: "mainstream-ish sources"
    date: 2007-08-30
    body: "Very good blog!! Let's hope the \" mainstream-ish sources\" will pick that one up too!"
    author: "cossidhon"
  - subject: "Re: mainstream-ish sources"
    date: 2007-08-30
    body: "I was actually referring to Aaron's blog \"On the success of KDE4\", but I hope both stories get picked up."
    author: "cossidhon"
  - subject: "Re: mainstream-ish sources"
    date: 2007-09-04
    body: "Nah, they all love GNOME these days, sadly, with them telling everybody to use Ubuntu (when they should be promoting *K*ubuntu or another KDE-based distro instead)"
    author: "Amy Rose"
  - subject: "In other news..."
    date: 2007-08-30
    body: "http://www.osnews.com/comment.php?news_id=18529\n\nFlames are raising and raising for KDE 4.0, and I have to agree with the part that there was too much hype for KDE4. Man I hate those fancy sites for new KDE technologies as Plasma and Phonon, it's pure hype.\nIn the other hand I think people are over-reacting, I still remember how bad 3.0 was :)\n\nBut in the end, I think the discussion is healty and will lead to better releases or better communication between developers and users."
    author: "Iuri Fiedoruk"
  - subject: "Re: In other news..."
    date: 2007-08-30
    body: "Having read part (about half) of that discussion, I do not find it to be overly flaming. There is quite a good balance in fact. \n\nI find it amusing to see how KDE 4.0 will probably have all the nice, cool base technolgies in place, but not yet the applications to use them (not so bad, that will happen in time), while in the Vista release most of the cooler technologies got dropped and all[1] that's left is a shiny new UI. I think I know what I prefer...\n\n[1] OK, ok, that is not all of course, but you get my point."
    author: "Andr\u00e9"
  - subject: "Re: In other news..."
    date: 2007-08-31
    body: "Yep, it's not as bad as I expected. Partly because some KDE guys jumped in probably (superstoned.... :-)).\nI really don't see any reason why some people want to believe that KDE4.0 will not be great. Maybe it will not be as great yet as some were expecting, bu great nonetheless. \n\nAnyway, there are comment I've also seen elsewhere about how \"more gradual development is better than those big shake-ups\". \nI made a few limited software development but nothing big. However, it seems to me that this is the type of comment only people who have absolutely no knowledge of software development can make. I mean, you make your designs based on required specifications at a certain time but for a DE, these can change dramatically with years, both because of what's required from the DE and because of what's available with new technologies. At some point, a shake-up seems to me the only way, you just can't get evolutionary all the time.\nWhat do people who have experience in large software projects think about that? "
    author: "Richard Van Den Boom"
  - subject: "Re: In other news..."
    date: 2007-08-31
    body: "Really, how do you think the millions of lines of KDE4 do get written? The largest part is using stuff from KDE3, of course!\n\nIf you have 1 Million lines, and one developer can write like 10k lines per month at absolute maximum. Consider, where you will take the 1,2 Million lines that your project needs.\n\nI think only the linux kernel is a fine exception to this, with 20MB patches per month, significant parts get replaced.\n\nDesign changes are necessary, but for the largest part, code is to be reused in big parts. When Plasma replaces kicker and kdesktop, it's mostly new stuff... but in kwin on the contrary, old code prevails. And most of the old things are not deeply rewritten, only optimized.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: In other news..."
    date: 2007-09-01
    body: "Yes, you're right, of course.\nI guess I didn't think about that well enough."
    author: "Richard Van Den Boom"
  - subject: "Re: In other news..."
    date: 2007-08-30
    body: "I think the only real hype i saw as instigated by the plasma revolution.\n\nIt cooled off now a lot, the practical view has settled in.\n\nWhat I will test a lot is the scripting support and widget usabilty - for ruby :)\n\nThis will be my personal pet peeve against gnome, because I know for sure\nthat I cant live without ruby anymore  (and i dont care if they ingetrated \nc#/mono, I want ruby ... )"
    author: "she"
  - subject: "Re: In other news..."
    date: 2007-08-30
    body: "Not to detract from your choice of desktop and development environment, GTK/Gnome has Ruby bindings as well:\n\nhttp://ruby-gnome2.sourceforge.jp/\n\nGood luck programming Ruby, no matter which set of libraries you choose."
    author: "phoebus"
  - subject: "Re: In other news..."
    date: 2007-08-30
    body: "> I think the only real hype i saw as instigated by the plasma revolution.\n> \n> It cooled off now a lot, the practical view has settled in.\n\ncan you expand on this? you leave out who you are referring to in each of these sentences, what the \"hype\" and \"practical view\" are/were and what the consequences of each are.\n\nperhaps unsurprisingly, i happen to care about these things and so would like to hear what you are thinking in greater detail."
    author: "Aaron J. Seigo"
  - subject: "Re: In other news..."
    date: 2007-09-01
    body: "Maybe I can elaborate on this a tiny bit.\n\nFrom my point of view, there was at the start of KDE4 cycle a massive amount of blogs from you, the now KDE president, then active voice. A website was created as the first action. \n\nYou have defended with great vigor and enthusiam the idea that Plasma would be innovation. Even when at the time, this was understood as SuperKaramba merging into kdelibs only. Some said it was not innovation, but you insisted.\n\nI understand, you needed to pickup people to make sure, the common vision of what Plasma would be, could actually form. In my memory, it was also the first to have this kind of physical name, Solid, Phonon, etc. were to be follow up, which was a genius move alone.\n\nYou were about the only one visible at the time and it was obvious that you cared. Then you got hired to work on KDE and Plasma seemed to drop in priority, to the point, that people started to question if there was anything to be done, and seriously considered that it would not make it into 4.0.0, which was considered a grave problem for the very same reasons you gave initially on why Plasma was to be had.\n\nWhat a lot people didn't see, was the massive amount of refactoring, cleanup and rearchitecturing that you personally did before working on Plasma. There was a total cool down and complete silence from you, not even mentioning Plasma over months, while being far from a no-blogger. I personally was frustrated a lot that you did this, while not disputing your right to it, or maybe I did. I will apologize for that.\n\nNow Plasma exists and I give all the credit you deserve as part of making it come true. You made sure a group gathered to redo the visual appearance of KDE, you made sure the vision was built and finally, you also did a lot of code, to follow through on a great design. I really personally love the data source, data sink approach a whole lot, and it shows great potential, and growing use every day.\n\nFrom my view, Plasma went all different than the other new KDE techs, for which small groups/individuals created complete plans and/or code first. Seems like a necessity, because Plasma is much harder to describe in words than the other things.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: In other news..."
    date: 2007-09-01
    body: "thanks for the explanation. greatly appreciated.\n\n> A website was created as the first action\n\nmore accurately, it was the first action you saw. it was not even close to the first action i took =)\n\n> Then you got hired to work on KDE and Plasma seemed to drop in priority\n\na lot of people put a huge emphasis on me getting hired. it really changed almost nothing except my time availability. trying to draw conclusions between me getting paid and any of my actions since is going to result in wrong conclusions time and again simply because there aren't connections between my motivations or my chosen actions and that position. it simply allows me to more fully commit to what is going on.\n\n> was a total cool down and complete silence from you,\n\ni reached a point where i was personally sick in my heart with dealing with people who could not and would not be able to see the larger picture of things and refused to leave it alone. i had no means for cathartic release, so i withdrew large parts of my thoughts and communication from the outside. i wasn't happy about it then, and am little more satisfied with it i now.\n\nit negatively impacted my motivation and my productivity. the result is that i will probably hold a fairly significant sense of negativity towards the general community of free software user enthusiasts for a while because of that period of time.\n\ni don't expect unearned support or adoration, but i do expect a community where people are actually community members and supporters rather than consumer oriented feeders without a sense of empathy.\n\nwhat i did in the early days of kde4 thinking and development was what i felt was necessary to get things on track and moving. along with many others, who are the true heros of kde, i put a huge amount of personal energy in to these things. i also put myself in the line of fire by being visible, outspoken and someone who was willing to stand as a representation of the responsibility those in the project quietly hold and live by.\n\ni think it was all worth it, and would do it all over again. i'm personally quite ok with how things are unrolling. but i continue to pay for taking the early steps i did. how fun is that?\n\ni'll continue to be miffed at people who proceed to rain on me and those i care about. as a means to deal with it, i've started thinking of such people as \"customers\" and not \"my users\" or \"my community members\" because that offers me the emotional distance i need from the sort of behaviour that many are evidently quite committed to engaging in.\n\nthis is all a very long winded way of saying: the community can either have a bunch of paid lackeys who probably don't truly give a rat's ass about you or you needs .. or you can play your part in creating a community. too many people seem to have forgotten that community isn't a gift you receive but a state of being you create with others.\n\n> You were about the only one visible at the time\n\ni'm glad that's changed, and i'm highly supportive of that change. it's the best thing for the project and its longevity. it's something several of us in the project have worked consciously towards making happen. i urge people who associate personal interest with the kde project to be nicer to those who have become visible and who will be more visible in the future. it's really in everyone's best interests to do so.\n\n> and it was obvious that you cared\n\nfwiw, i still do and don't yet see that changing one bit =)"
    author: "Aaron Seigo"
  - subject: "Re: In other news..."
    date: 2007-09-01
    body: "http://www.freesoftwaremagazine.com/blogs/how_to_recognise_prevent_and_treat_burnout"
    author: "bert"
  - subject: "Re: In other news..."
    date: 2007-09-02
    body: "Hello Aaron,\n\non LKML there recently was a guy who proposed a new thinking in file systems, with some ideas. All he got was critics. I think LWN had a link to the guy who explained it, why that is so. In short, it's the best thing of all to learn what's wrong with something early on. \n\nIt's sad though when all the critics one gets is on the value level. Like this is not something to be excited about. Like don't do this, because do that instead. Like despair, others got better than even your plan.\n\nDrawing fire is not fun. I remember you did it with \"Usability\" too. That was an even more important discussion to be had, and so successful. Your drawing and standing the fire, allowed that the insight can build up, and people who would never have stood the hate, to join the course and establish it within the community as a normal thing.\n\nConsidering that you are outspoken and will continue to be, may I point out, that it's _calling_ for fire to express your visions through existing means, that have lots of shortcomings. I remember seeing you mention \"Nautilus\" back then in Plasma discussion. It's not forbidden, it just means many angry replies without merit, this will reduce your communication ratio. I am a 100% sure, you either didn't understand that or like fire. The later of which is under doubt now. ;-)\n\nOne thing there is in the KDE user community: Part of it, that turned to KDE during KDE3 cycle, is absolutely scared of developers, coming from the Gnome of the time. Communication with that part of the community has been tough in both discussions, Usability and Plasma. There's not enough trust in that part of the community yet, there likely will only be by the time we see a 4.1 release.\n\nAll I can say is, I sincerely hope you have fun when you look back at how things turned out and when you code. Were I you, having helped push Usability into acceptance, would make me so proud.\n\nAnd that customers thing: The other day, I saw many guys on a linux news site, complain in the context of a Gnome beta, that betas and release candidates were announced. That's customers, they only want ready for consumption.\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: In other news..."
    date: 2007-09-02
    body: "The definition of a successful free/open-source project is that is must have\n* A good idea\n* A community\n* Running code\n\nFor the entire lifetime of Plasma, it had all of those. (you mentioned superkaramba already).\n\nThe difference with Plasma was not so much the code or community; it was that Aaron used the ideas and concepts that big commercial projects use to create a community. And he thus reached a lot of people that don't know what a community is, they just know the concept of software producer v.s. software consumer.\nAnd the result was what Aaron took a break over; quite a lot of people didn't see the cool ideas as a call to action (as is usual in a community) but as a promise that Aaron was going to deliver something. And when they stopped seeing regular progress they assumed he was cheating. Which is completely wrong; he never promised anything like what people were expecting.  He just promised a goal for KDE4. Which is still many years in the 'making'.\n\nI see this problem getting bigger as KDE gets more mainstream.  A lot of ideas, schedules and deadlines which are clearly community based, and thus are calls to action for everyone. But they are seen by the people outside of the communities (os-news anyone?) as promises.\nThe media and the end-users would ideally learn that Open Source is about openness and thus no promises are actually made until the code is there.\n\nShort of them learning our ways; anyone an idea on how to bridge this gap better?\n\nOn a side-note; I regularly see questions from the PR people we have in KDE about a promise for something in a next release, or a date for a release.  Those people actually do understand open source, but they feel that the users need promises, even though we can't make them.  Which IMO is sad, really."
    author: "Thomas Zander"
  - subject: "Re: In other news..."
    date: 2007-08-31
    body: "I just realised that all of the stuff in \"the vision\" of the Plasma page is pretty much put in place. When I first read the vision, I thought \"well, it's just adding some more features and polish to the current desktop\", then when I read some more of the concepts I was getting hyped. And now, everything seems to be just about ready:\n\n1. Ability for the same applets can be both in desktop and panels.\n2. Extensions - which will be really cool once more applets have them.\n3. Consistency - animations, background, engines, etc.\n4. New krunner, taskbar, and menu (out of necessity?)\n\nJust about everything promised is there, or will be shortly. Now, we just need more applets and engines, which should happen as more people switch to KDE4."
    author: "Soap"
  - subject: "Re: In other news..."
    date: 2007-08-30
    body: "please define what you mean by \"pure hype\".\n"
    author: "Aaron J. Seigo"
  - subject: "Re: In other news..."
    date: 2007-08-31
    body: "From front page:\n\"Desktop computing has changed radically in the last 20 years,\nyet our desktops are essentially the same as they were in 1984.\nIt's time the desktop caught up with us.\"\n\nI (emphasys on my person please, let's not start a flame war) don't see KDE4 desktop being different than enything else already in the market.\n\nMy understanding for the text (and forgive me, I'm not a natural born english speaker) is that Plasma is about reviewing the office paradigm of the desktop (for those who don't had Human Interface classes, the computer UI resembles a office with desks, metal containg cases for files, trashcan, and list goes on).\n\nBut, I have to agree with the first comment after mine in one thing: reading the text now that I know better what Plasma is becoming (Dashboard for KDE), makes much more sense, and is very less hype than the first time I read it.\nMaybe we where all wrong at the time, understanding by the text what Plasma meant. Maybe not.\n\nAt least here in Brazil those kind of sites, it's more used for limited-time campaingns for new released products or products that have a short life period. And in this case, you describe the product as something as revolutionary, give little information on what is or how it will be and build a huge hype around it.\nI am a web developer, so I'm used to manage this kind of things, and belive me, I would classify Plasma site as pure hype and rank it very low in google ranking system if chance was given to me. ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: In other news..."
    date: 2007-08-31
    body: "I think two things are important to note here:\n\n> I don't see KDE4 desktop being different\n> than anything else already in the market.\n\nDefine \"The desktop\". Some believe that's everything you see and thought Plasma would replace the window/menu/toolbar/widgets paradigm. I was a bit confused first too. Cute, but perhaps a bit overestimated. The desktop = kdesktop + kicker today.\n\nHowever, with KDM and Amarok using libplasma, Plasma actually starts to get more impact then expected. The fact I can drag an photo or contact vcard to the desktop and see something different then an icon is something we haven't seen before. The fact it can be dragged from Amarok or to the taskbar is even more amazing.\n\n> now that I know better what Plasma is becoming (Dashboard for KDE)\n\nYou're forgetting the form-factors, desktop zoom, extenders and data source / engine designs here. The whole picture is a lot bigger then \"widgets\". If you wanted just that, superkaramba would be sort-of good enough.\n\nWith plasma it becomes amazingly simple to rebuild your desktop with different start menu, taskbar and systray components. It's basically a do-it-yourself desktop. All the power and potential is there, so I do believe the _desktop_ will eventually change because of it."
    author: "Diederik van der Boor"
  - subject: "Re: In other news..."
    date: 2007-08-31
    body: "See the link I posted under A. Seigo comment.\nIt clarifies Plasma a lot when he says that Plasma isn't revolutionary, but can lead to one revolution.\n\nI agree in almost everthing you said, exept that Plasma still looks to me as Dashboard, and I belive, the features you talks about are present in MacOSX. You know, the taskbar on Mac is more than a taskbar and can do different things. I'm not a Mac user myself, but most things you talk about seems to me that already exists in a way or another, and most of them in Dashboard/Mac, just not in *nix/KDE.\n\nBut I will love to be proven wrong :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: In other news..."
    date: 2007-09-01
    body: "some of the things do exist in other systems, including mac os. many do not. Diederik listed a few, and over time i'm sure you'll get acquainted with some of them first hand =)\n\ni'm not going to list them and try and convince you. in return, perhaps you could lay down scepticism and claims of \"hype\" until i and the other plasma developers have a chance to deliver on what we are working towards.\n\n4.0 is going to have the germ of the project and you'll see it unfold into being release by release. if you really want to track things closely, we'll be doing frequent and regular releases of plasma outside of main kde releases as a means to support the growth of a testing group."
    author: "Aaron Seigo"
  - subject: "Re: In other news..."
    date: 2007-08-31
    body: "PS: loved http://aseigo.blogspot.com/2007/08/on-success-of-kde4.html\n\"KDE4 isn't a revlolution\".\n\nCould not have said it better, and what I mean by disliking Plasma site is exactaly that it passes the idea that it is meant to be a revolution :)\n\nMaybe now it's more clean what I was trying to say? (I hope so).\n\nIf KDE4 can lead to a revolution os a complete different story :)\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: In other news..."
    date: 2007-09-01
    body: "> \"KDE4 isn't a revlolution\"\n\nto help those that won't read that blog entry, the above is a misquote. the correct quote is: \"4.0 won't be a revolution\".\n\n> it passes the idea that it is meant to be a revolution \n\nit passes that idea because it *is* meant to be a revolution. the part you seem to be not grasping is that getting to \"revolution\" takes a whole lot of steps in between. that's how revolutions happen. you're getting to see those steps in between, something you don't usually get the privilege of, and because of that you're claiming \"no revolution\". what malarky.\n\nsee, plasma is not going to be released with 4.0 and then never touched again. plasma is not something that is a one stop affair with a release this year and then \"whoop, that's it!\". the design of it goes far beyond that first release. it is not \"done\" with 4.0. it is begun with 4.0.\n\nyou're trying to treat it as a end-of-life product when it's a living document. and yeah, that begins to piss me off.\n\n> If KDE4 can lead to a revolution os a complete different story \n\nat least that part is getting clearer for people. thank $DEITY."
    author: "Aaron Seigo"
  - subject: "Re: In other news..."
    date: 2007-09-02
    body: "One common mistake is that KDE4 and KDE4.0 are two quite different things.\nPlease quote correctly!\n\nKDE4 is the platform that will live for 5 years.\nKDE4.0 is the very first release of that platform and thus will have the basic building blocks.  A foundation with basic stuff on there already.\n\n\nIts like the first 'version' of a sewer system being 4.0, they just put pipes in the ground but a lot of houses still had no way to use it so it went through the streets anyway.  Not much of a revolution.\nThen some time after that everyone got to build on the idea and we ended up with clean streets and less diseases.  Which surely is a revolution. But one that could not be possible were it not for the first release.\n\nHope that makes it more clear :)"
    author: "Thomas Zander"
  - subject: "Good News"
    date: 2007-08-31
    body: "KDE 4.0.0 is delayied in 2 months:\nhttp://lists.kde.org/?l=kde-devel&m=118856887429944&w=2\n\nNow all this complains seems to had some reasoning, but the developers made the right move.\nMaybe now there is time for some pieces that where not going into 4.0 to get in?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Good News"
    date: 2007-08-31
    body: "Application Feature Freeze, except exemptions, is still in effect even with the delay."
    author: "Luca Beltrame"
  - subject: "Re: Good News"
    date: 2007-09-01
    body: "So the reason some pieces where dropped is basically that they can't work without changing kdelibs API?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Good News"
    date: 2007-09-01
    body: "No, simply because they require too much work to be put in, and rather than handing down something rushed and partially non functioning, they decided to postpone it so that there will be enough time to finish.\n\nAFAIK, kwebdev is also not shipping for KDE 4.0 for the same reason (can someone from the project confirm that?).\n\nAnd while I'm at it, I'm going to write why (IMO) statements like \"wait till everything's ready\" are not totally sensible at the moment. Basically, KDE 4.0 is part of the huge change that KDE4 will be. However, a key factor in a success of a DE is availability of applications, not necessarily tied with the DE itself. THe problem with an excessive (i.e. unplanned, don't misunderstand, I fully agree with the Release Team's decision to push the release to December) delay is that no 3rd party will start developing for the KDE 4 platform. Therefore I think it is important to bring KDE 4.0 out (not *rushing it*, of course) in order to make application developers aware of its existence."
    author: "Luca Beltrame"
  - subject: "Re: Good News"
    date: 2007-09-01
    body: "Whoops, I meant \"kdewebdev\". Sorry."
    author: "Luca Beltrame"
  - subject: "Re: Good News"
    date: 2007-09-02
    body: "\"Dropped\" is the wrong word. It indicates that we no longer have that feature which is incorrect.\nThe correct way of thinking about this is that due to time constraints and basically not being able to fix everything (can't focus on a million things at ones) some pieces got a lower priority and thus will not be released with 4.0, simply because they are not ready yet.\n\nAs we say; there always is life after the next release!"
    author: "Thomas Zander"
  - subject: "Re: Good News"
    date: 2007-09-01
    body: "That's probably enough to make kde 4.0 suitable for production,\n(more testing, less crashes, and in everybody hope zero data losses)\nso it will safely enter in distros for end-users like kubuntu.\n\nAlso this means that in the consequent 4.1 develop there will really be\ndeveloper time to add features instead that heavy bugfixing."
    author: "nae"
  - subject: "Re: Good News"
    date: 2007-09-01
    body: "That's probably enough to make kde 4.0 suitable for production,\n(more testing, less crashes, and in everybody hope zero data losses)\nso it will safely enter in distros for end-users like kubuntu.\n\nAlso this means that in the consequent 4.1 develop there will really be\ndeveloper time to add features instead than heavy bugfixing."
    author: "nae"
  - subject: "Re: Good News"
    date: 2007-09-02
    body: "> KDE 4.0.0 is delayied in 2 months:\n> Maybe now there is time for some pieces\n> that where not going into 4.0 to get in?\n\nNo no no.\n\nIt means more bugfixes, more crashes fixed, and a more stable desktop. Those additional 2 months are neccesairy to get stuff working.\n\nIf you told people 5 months before the release would be in december, KDE still had to delay it. A release date is needed to focus on making stuff work, otherwise people stay in \"thinker mode\" instead of \"release mode\". I'm glad to see how this is handled by the release team so far :)\n\n"
    author: "Diederik van der Boor"
---
Gartner Webdev has <a href="http://rackit.gartnerwebdev.com/2007/08/28/ready-for-a-new-k-an-inside-look-at-kde-40/">published an interview</a> with KDE's very own marketing renegade Wade Olson. Wade talks about KDE 4, what makes it special and what KDE's role in the software market is. He sheds some light on the user's point of view and discusses his vision about the way the computer and software industry is moving. Read the interview for yourself.


<!--break-->
