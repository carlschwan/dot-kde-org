---
title: "The Road to KDE 4: Okular and Ligature Document Viewers"
date:    2007-02-14
authors:
  - "tunrau"
slug:    road-kde-4-okular-and-ligature-document-viewers
comments:
  - subject: "oh no..."
    date: 2007-02-14
    body: "OK since I'm making the 1st comment, I just want to tell everybody this :\nPlease don't start another discussion like \"Ligature vs Okular\", or \"which viewer SHOULD be default ?\". This type of discussion already happened many times (on mailing lists, commit-digest, etc.), and I don't think they helped.\nPlease respect the work done on both of these projects. They provide it to you for FREE, and you are FREE to use it or not.\nThis is what oss is about."
    author: "shamaz"
  - subject: "Re: oh no..."
    date: 2007-02-14
    body: "and btw, thank you Troy for the great article :)"
    author: "shamaz"
  - subject: "Correct, 1+1 isn't 2 [was Re: oh no...]"
    date: 2007-02-14
    body: "I think it's important to understand that 1 + 1 isn't 2 here. If you combine the developers working on Ligature and Okular you won't end up with one project that's twice as productive.\n\nThe same can be said about KDE and GNOME. Combining these reduces the productivity as each set of developers have a different vision. Two parallel projects keep each other motivated to become the best one.\n\nIt also creates playground to implement new features. Sometimes a project won't accept an idea because it's to controversial. When the developer can implement it in the other project, get successful with it, the first project may copy the feature."
    author: "Diederik van der Boor"
  - subject: "Re: Correct, 1+1 isn't 2 [was Re: oh no...]"
    date: 2007-02-17
    body: "Eh, maybe, but we're talking about document viewers here, not video editors or desktop environments.\n\nCreating two separate applications to simply read the same document files is basically redundant. I don't see what kind of \"controversial features\" can come out of a program so simple, short of allowing a user to change a background color or something.\n\nStuff like this is why people say KDE is so bloated, because this is basically recreating the whole Noatun/Kaboodle thing."
    author: "matt"
  - subject: "Re: Correct, 1+1 isn't 2 [was Re: oh no...]"
    date: 2007-02-17
    body: ">  the whole Noatun/Kaboodle thing\n\nI always wonder how anyone could ever see any sort of duplication between Kaboodle and Noatun.\n\nPerhaps some people like to have to click on ever music file separately and really hate playlists, or there are people who really love to have random sound files they have been checking in their filemanager end up in their playlist.\n\nIt's like the whole textprocessor/spreadsheet thing: how would anyone ever need a spreadsheet since textprocesssors can also do tables?"
    author: "Kevin Krammer"
  - subject: "Re: Correct, 1+1 isn't 2 [was Re: oh no...]"
    date: 2007-08-11
    body: "spreadsheet analogy doesn't really work... text processors usually can't do formulas in tables... big difference"
    author: "yomama"
  - subject: "Re: oh no..."
    date: 2007-02-14
    body: "I understand what you are saying but I still can't see the point of having to programs which seam to be pretty much the same.\nI reminds me somehow on kate and kwriter. They both are regular editors and today it seams to me that kate is the only program which is used. So why was there a kwriter in the first place?\nIt is ok, having a large variety of programs, which might also compete but please not within a project like KDE.\nBut it is still an early stage and only time will tell which viewer will be used by most of the users."
    author: "linuksamiko"
  - subject: "Re: oh no..."
    date: 2007-02-14
    body: "Well, the kate/kwrite thing is a bad example, as those are pretty much the same. Kwrite is just a lighter version of Kate, and less confusing for many normal users."
    author: "superstoned"
  - subject: "Re: oh no..."
    date: 2007-02-14
    body: "even more, kwrite is kate with a simple skin.\n\nkwrite is also magical: if you remove $KDEDIR/bin/kwrite, you can still start the application from kmenu :)\n"
    author: "otherAC"
  - subject: "Editor-like Selection of Text"
    date: 2007-02-14
    body: "One of the most interesting and useful features, to me, is the use of \"editor-like\" selection of text, as opposed to the old (and, to my mind, rather confusing and useless) \"lasso\" selection of text.  I believe both Okular and Ligature incorporate this, which is great!"
    author: "Anon"
  - subject: "Re: Editor-like Selection of Text"
    date: 2007-02-14
    body: "Those are cool indeed, just like the annotation support in okular (why didn't Troy mention this?). There where talks about basic pdf editing support like rotating, extracting or adding pages (like basic graphics viewers can rotate & have basic filters) but sadly they decided a viewer shouldn't edit the files, even tough Okular already DOES change them when annotating them.\n\nAside from this, I think the development of these apps goes in the right direction, and even tough some day one might be abandoned by it's users and developers, currently they both profit from the competition - as Adobe Acrobat and Evince aren't really competition (both okular and Ligature are lightyears ahead of both in most areas, evince can't do anything and Adobe is a usability- and performance nightmare)."
    author: "superstoned"
  - subject: "Re: Editor-like Selection of Text"
    date: 2007-02-15
    body: "Oh, please, superstoned, you're always trolling around! the only thing okular supports and evince doesn't is the annotation support"
    author: "Myself"
  - subject: "Re: Editor-like Selection of Text"
    date: 2007-02-15
    body: "> the only thing okular supports and evince doesn't is the annotation support\n\n\"only\"... our implementation is far from being finished but... do you have an idea of the complexity of the annotation stuff? It seems not,\n\nBtw, and we support sounds, too ;)"
    author: "Pino Toscano"
  - subject: "Re: Editor-like Selection of Text"
    date: 2007-02-15
    body: "Annotations are complex, but we have a Great Maintainer and some willing people!! Btw: annotations need to be reworked a bit :-)\nGrande Pino ;-)\n"
    author: "Enrico Ros"
  - subject: "Re: Editor-like Selection of Text"
    date: 2007-02-15
    body: "\"Editor-like Selection of Text\",that's we really need.The \"lasso\" selection of text is so poor,especially for using ktranslator on a foreign pdf files.Adobe does it good."
    author: "yuanjiayj"
  - subject: "Choice is good."
    date: 2007-02-14
    body: "Long live Ligature, long live Okular! Thanks to all their authors :)\nAnd thanks Troy."
    author: "Slubs"
  - subject: "Re: Choice is good."
    date: 2007-02-14
    body: "Thanks for sorting this out. I had heard about both project, but didn't know the real difference.\nThanks Troy for keeping us updated."
    author: "Lans"
  - subject: "chm"
    date: 2007-02-14
    body: "okular crashes on Prentice.Hall.PTR.C++.GUI.Programming.with.Qt.4.Jun.2006.chm file"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: chm"
    date: 2007-02-14
    body: "That's a bug in khtml, which is used by okular's chm plugin internally.\n\nThe bug report was forwarded to the khtml developer already\n\nCiao,\nTobias"
    author: "Tobias K\u00f6nig"
  - subject: "Re: chm"
    date: 2007-02-14
    body: "what is the bug number?"
    author: "germain"
  - subject: "Re: chm"
    date: 2007-02-15
    body: "http://bugs.kde.org/show_bug.cgi?id=141546\nThere are a lot of problems with KHTML right now so opening chm files is a problem."
    author: "Kunal Thakar"
  - subject: "Postscript in Okular"
    date: 2007-02-14
    body: "At the moment I still prefer KGhostView for Postscript Documents. PS look ugly in KPDF after the PDF transformation. Does anyone know whether this has changed with Okular ?"
    author: "Sven"
  - subject: "Re: Postscript in Okular"
    date: 2007-02-14
    body: "Hi,\n\nin okular we use libqgs now, which accesses the ghostscript API directly to render the PS document, so no convertion to PDF is needed any more.\n\nCiao,\nTobias"
    author: "Tobias K\u00f6nig"
  - subject: "digg it!"
    date: 2007-02-14
    body: "http://digg.com/linux_unix/The_Road_to_KDE_4_Okular_and_Ligature_Document_Viewers"
    author: "Patcito"
  - subject: "firefox-like search bar?"
    date: 2007-02-14
    body: "hi, will any of these two programs support a firefox-like search bar?"
    author: "anonimous"
  - subject: "Re: firefox-like search bar?"
    date: 2007-02-14
    body: "Both have a searchbar on top of the preview sidepane, much more usefull as it also highlights found items in the previewpane and only shows relevant pages. Nothing a firefox-like searchbar would add to that. I'd rather see such a searchbar in KDElibs, and used instead of the currend find dialog in all apps (maybe with an optional fallback to that dialog), btw :D"
    author: "superstoned"
  - subject: "Re: firefox-like search bar?"
    date: 2007-02-14
    body: "aha!! I hadn't realized that search bar (it's in KPDF aswell!!)  Talk about a naive user, jejeje\n\nI think a nice feature would be that the Ctrl+F shortcut would make the search bar gain focus instead of showing the find dialog.  Maybe give the user some feedback about the focus.  And maybe add to the search bar two buttons for cycling over the results.\n\nanyway, both viewers look great!"
    author: "Ricard"
  - subject: "Re: firefox-like search bar?"
    date: 2007-02-14
    body: "Yeah, cool. I didnt notice it before either. Maybe because my preview pane is usually not so wide. I think it might deserve a more prominent place."
    author: "Sven"
  - subject: "Re: firefox-like search bar?"
    date: 2007-02-15
    body: "what is definitely missing is the option to cycle through your results. Personally I don't see the feature of displaying only the pages with a particular keyword as particularly useful/usable."
    author: "dave"
  - subject: "Re: firefox-like search bar?"
    date: 2007-02-24
    body: "> Personally I don't see the feature of displaying only the pages with a\n> particular keyword as particularly useful/usable.\n\nI must have looked at 100+ scientific papers in KPDF, and I really enjoyed this feature. If you've already read the paper it's a lot easier to re-find the place you were looking for by scrolling the previewer, and if you don't know the article it gives a very quick overview of how many times and where your search term occurs. When you're fumbling around in the (huge) literature for something perhaps relevant it's very helpful.\n\nThumbs-up to KPDF from here, keep up the good work :-)"
    author: "Niels"
  - subject: "Re: firefox-like search bar?"
    date: 2007-02-14
    body: "Ligature and its predecessor have such a search bar since KDE 3.5."
    author: "Wilfried Huss"
  - subject: "Re: firefox-like search bar?"
    date: 2007-02-14
    body: "Wilfried, \n\nYou mentioned long ago that there might be a Ligature release for KDE 3.x. Is that still the plan?"
    author: "Martin"
  - subject: "Re: firefox-like search bar?"
    date: 2007-02-14
    body: "> You mentioned long ago that there might be a Ligature release for KDE 3.x. Is that still the plan?\n\nYes, that is still the plan. That was the whole point of doing parallel\ndevelopment of a KDE3 and KDE4 version in the first place. Unfortunately\nI have been really busy in job and private life lately, so I didn't find\ntime to work on Ligature in the last months.\n\nHopefully we will be able to release soon, since no more new features are\nplaned, just stabilization and bugfixing."
    author: "Wilfried Huss"
  - subject: "Somo points about the text"
    date: 2007-02-14
    body: ">A note about DVI files in general: to view them you need to install some TeTeX >files, which on my distribution totals 85 megabytes - a likely reason why DVI >files are not a popular format for documents despite their competent rendering >abilities. When Ligature finds a hyperlink in a DVI file, it underlined the >text in blue to indicate that you could click on it, which while useful in >some circumstances, made documents with links look quite ugly. okular on the >other hand does not underline links in DVI files, but they still work as >expected.\n\nHey, this can be configure and completily dissable ;) or almost in kdvi it's possible (I did this right now) \n\nAnd about dvi files, this format it's a bit old. This came with the first LaTeX compiler when the pdf don't exist!!!! and is used in the \"scientific world\", or almost the kown that this format exist :P"
    author: "cruzki"
  - subject: "Ability to fill in pdf forms?"
    date: 2007-02-14
    body: "Will either of these enable one to fill in forms on pdfs that support them?"
    author: "redbeard"
  - subject: "Re: Ability to fill in pdf forms?"
    date: 2007-02-14
    body: "Both use the same library for pdf, Poppler. Shared with evince from gnome, btw. If poppler gets support for this before KDE 4 comes out, both will have it. If not, then not."
    author: "superstoned"
  - subject: "Re: Ability to fill in pdf forms?"
    date: 2007-02-16
    body: "A quick search on google gave this link:\n\nhttp://lists.freedesktop.org/archives/poppler/2006-August/002072.html\n\nLooks promising."
    author: "Samuel Weber"
  - subject: "Editing PDFs?"
    date: 2007-02-14
    body: "Viewing PDF, multi-page Tiffs and other, similar multi-page media is only half the game.\n\nThe app that allows to:\n1. Mark / Type on the individual pages + export that back into PDF, and\n2. Extract, move, rotate pages\n\nWill the the only one that matters in the end. So, my money's on the group that tries to empower users, rather than chain them."
    author: "Daniel"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-14
    body: "Okular will (does) support annotating of pdf's, and save it, but extracting, moving and rotating pages won't be supported because they think a viewer shouldn't change files (except for the annotating, apparently, tough previous versions did save annotations in a separate file)."
    author: "superstoned"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-14
    body: "so... which program is supposed to be for editing pdfs (like rotating, moving, filling in)... I've been searching for such a program, but until now I wasn't able to find a good one :-(\n\nbtw: great work with okular and ligature\n\nwith friendly greetings\n\nbernhard\n\nps: sorry for my bad enlish"
    author: "LordBernhard"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-14
    body: "acrobat reader comes to mind...."
    author: "otherAC"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-15
    body: "Maybe this does some of the things you wish for:\nhttp://pdfedit.petricek.net/pdfedit.index_e\n\nI remember reading a review of this program together with some other a while ago, but I can't remember where.\n"
    author: "Bakterie"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-15
    body: "Umm, I suppose Kwriter (KOffice) has the ability to edit PDF files... right?"
    author: "Medieval Mutt"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-15
    body: "There is no app called Kwriter. \n\nThere's KWrite which is a simple text editor that was just moved out of the standard KDE packgages. \n\nAnd then there's KWord, the part of KOffice you're referring to.  But its PDF import filter, as cool as it is, has its limits.  "
    author: "cm"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-15
    body: "\"they think a viewer shouldn't change files\"\n\nWell, all picture viewers in KDE allow rotating a diplayed picture, and then saving it back so IMHO there's no reason a pdf/ps/.. viewer should be different...\n\n- tendays"
    author: "tendays"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-15
    body: "\"all picture viewers in KDE allow rotating a diplayed picture, and then saving it back\"\n\nAnd that's exactly the thing i HATE with those viewers! I'm opening an image, rotating the VIEW, to look at it from a different angle, and when closing (or moving to the next image) I'm being asked if I want to save changes. What changes!?!? If rotating the view constitutes a \"change\", then why zooming it does not? Rhetorical question, of course. And first of all, why do I have to be afraid of damaging my file by opening it in a VIEWER, and accidentally pressing wrong button on exit? Hope someone fixes this misfeature..."
    author: "zonk"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-16
    body: "Tip; alter your gwenview config to never ask you this anymore ;)"
    author: "Thomas Zander"
  - subject: "Re: Editing PDFs?"
    date: 2007-02-14
    body: "\nI think you do have a good point.  Aside from filling in editable PDF's, I would *really* wish we could extract, move, and concatenate.  I can imagine it right now...dragging pages around in the preview pane....or drop a PDF file into the place you want its pages inserted.\n\nI don't understand why we have to have TWO very similar programs for viewing PDF's and ZERO programs for editing them (although I do understand why we aren't supposed to discuss it).  I think in a perfect KDE world, there would be one good general file viewer and seperate editor.  So, if these projects could get together and decide which one is going to be the \"Best File Viewer Ever\" and which is going to be the \"Best File Editor Ever\" that would be awesome.\n\nI have always thought KDE's PDF handling was ming-boggling great. Being able to print-to-pdf from any application...and it was always many times faster at viewing than Adobe.\n\n\n\n"
    author: "Henry S."
  - subject: "Re: Editing PDFs?"
    date: 2007-02-14
    body: ">>I don't understand why we have to have TWO very similar programs for viewing PDF's and ZERO programs for editing them\n\nWell, that's not too hard to understand: untill today, no-one bothered to create a pdf editor.\n\nBut joining okular and ligiture does not mean that we would be closer to a pdf editor than in the current situation.\n\nif you thinks its weird to have TWO applications doing the same thing, checkout kde-apps.org and you will find many areas where there are even more applications available for the same job..\nWould merging them all mean that we would get one great application that can do it all?\nDon't think so..\n\nAnd as last note: ligiture and okular did not start as similar applications: both started as viewers for a certain task, and while they mature they started to grow in each others direction.\n\n"
    author: "otherAC"
  - subject: "Re: Editing PDFs? - Rotate feature !!!!"
    date: 2007-02-15
    body: "A lot of documents are batch-scanned and distributed as PDF.\nThe content of this documents may be mixed portrait or landscape mode. That's the way they are.\nAs long as the receiver prints the document that's fine because he can turn them as needed.\nBut it's hard to to ready landscape docs in portrait mode on screen (except on laptops ;-).\n\nSo the argument - see one of the postings - not to modify PDF files is only the half truth and the users will have to use xpdf or acroread instead.\n\nSo please reconsider to add rotation."
    author: "ferdinand"
  - subject: "A small trick"
    date: 2007-02-15
    body: "I have found a small trick that works rather well, to extract specific pages : I print them using the PDF pseudo printer of Kprint, selecting the appropriate pages in the process. It generates a new file with the pages you want.\nMaybe playing with the print settings and order of printing should allow you to rotate and move pages.\n"
    author: "Richard Van Den Boom"
  - subject: "Creating PDF's Easily"
    date: 2007-02-14
    body: "An important feature I'd like to see in either or both of these is the ability to easily create PDF's from scanned documents.  With full acrobat, you can scan a document directly into a PDF.  In fact, if you have multiple pages to scan it will keep prompting you for the next page until you indicate you are done.  I use that feature to archive my important records.  It's really about the only time I NEED windows.  If this feature is added I will be a happy camper."
    author: "cirehawk"
  - subject: "Re: Creating PDF's Easily"
    date: 2007-02-14
    body: "You can quite easily use convert from ImageMagick to do the same, just scan everything to jpg and do convert *.jpg output.pdf. Might take some memory though.\n"
    author: "Michal"
  - subject: "Re: Creating PDF's Easily"
    date: 2007-02-14
    body: "you can't use kooka, and print the resulting file to a pdf??? Works fine here. Every KDE app can print to PDF, and why would a viewer support scanning? Or burning to cd, or sending email etcetera? Basic page-oriented editing, ok, copy-paste from it, printing, saving, editing meta-data, annotating - fine. But scanning?!?"
    author: "superstoned"
  - subject: "Re: Creating PDF's Easily"
    date: 2007-02-14
    body: "Of course I can do that.  But that takes a step or two more than the process I'm talking about. Plus, I haven't been able to easily combine multiple pages into a single PDF file.  Maybe Kooka is the right place to put this functionality."
    author: "cirehawk"
  - subject: "Re: Creating PDF's Easily"
    date: 2007-02-16
    body: "it includes even more steps, but you can insert all scanned images into a word processor (kword, openoffice writer/draw) and then save/export/print it to pdf"
    author: "otherAC"
  - subject: "Re: Creating PDF's Easily"
    date: 2007-02-15
    body: "Yeah, I've been thinking about writing a small program to do that sort of thing (working title Kopier), just a basic app that simply joins up input from scan or file to output to print, file or fax.  Think of it as a software MFC.  The underlying KDE framework is there, it's just finding a nice way to join it all together.\n\nI don't see this as part of a universal document viewer program.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Creating PDF's Easily"
    date: 2007-02-15
    body: "Cool.  If you were to do that, you would have my gratitude (not that all the KDE developers don't have it).  I'd be happy to test it when or if you get to that point."
    author: "cirehawk"
  - subject: "off topic remark"
    date: 2007-02-14
    body: "it seems the default file manager now for KDE is dolphin heeeeeeeeeeeah cool\nit has moved to kdebase.\n\n\nso thx really, i think after all the idea of konqueror as a super browser and file manager and file view was not a good idea\n\n"
    author: "djouallah mimoune"
  - subject: "Re: off topic remark"
    date: 2007-02-14
    body: "It's still a testbed. Might turn into the new filemanager, but it's also possible every usefull feature will be ported back to konqueror and it'll go..."
    author: "superstoned"
  - subject: "god please no"
    date: 2007-02-15
    body: "Please don't remove the location bar like GNOME did for their stupid file dialog.  This is awful and unusable for our users and breaks the paridigm for nothing. :(\n\nI hope this Dolphin doesn't switch the Yes/No dialog buttons either. :("
    author: "KDE User"
  - subject: "Re: god please no"
    date: 2007-02-15
    body: "I encourage you to first try out the latest stable version of Dolphin to really see its potential. I'm a die-hard Konqueror fan, But I do think that there are things that Dolphin gets right. That's not mentioning a cleaner code beneath.\n\nThe location bar in Dolphin is not removed. The devs would be crazy to do that. The button (or shorcut keys) to show it is clearly there. But the breadcrumb widget itself is more than a match for what Nautilus currently has.\n\nDolphin is apparently meant to be a light, simple, and fast file manager. It can never really replace Konqueror in functionality. So Konqi won't be going away anytime soon. I do hope, though, that a lot of this work will be somehow applied to Konqueror, too."
    author: "Jucato"
  - subject: "Re: god please no"
    date: 2007-02-16
    body: "Dolphin and konq share things like icon views, and so forth.  So improvements to Dolphin are also improving konq.  Which is a good thing, because Dolphin, while a very smooth file manager, just does not have the power user functionality that I've come to rely on from konq.  Things like splitting panes and then doing drag and drop from an ftp site to my server via fish, and so forth.  \n\nSo, Dolphin will improve konq. and provide an easier to use file manager for more basic user needs.  Analogous to how kwrite is a simplified kate, with reused components... (I know it's not a 1:1 analogy, but it's simple enough)\n\nSo it's all good. :)"
    author: "Troy Unrau"
  - subject: "Re: off topic remark"
    date: 2007-02-15
    body: "My information may be a little out of date, but last I heard Dolphin was simply going to be a test bed for the KDE4 file manager, but isn't actually going to be the KDE4 file manager."
    author: "Corbin"
  - subject: "Re: off topic remark"
    date: 2007-02-16
    body: "Dolphin was moved into kdebase, and is planned to become the default handler for file:/, as well as the default file manager listed in the apps menu.\n\nThat said, konq is not going away, and improvements to dolphin also improve konq, since they share libraries, such as the icon view."
    author: "Troy Unrau"
  - subject: "Re: off topic remark"
    date: 2007-02-16
    body: "This is doubly bad for Konq because the exposure that most users get to Konqueror Web Browser is through the filemanager because *most* distributions change the default web browser to firefox. So the only time you see Konqueror is when you click on it for file management, oh and then you find out it's world-class web browser and keep using it for everything.\n"
    author: "KDE User"
  - subject: "Re: off topic remark"
    date: 2007-02-16
    body: "This varies greatly depending on the distro.  On Kubuntu, I don't even have firefox installed, nevermind it being the default browser.  And distros like gentoo of course always let the user decide.\n\nSome KDE specific distros (I'll use kubuntu as the example again, since that's what I know best) are even moving openoffice out in favour of koffice as the default."
    author: "Troy Unrau"
  - subject: "Re: off topic remark"
    date: 2007-02-15
    body: "I'll only use Dolphin if it has a tree view (of only directories) in a separate panel, and if there is an address field where I can type in where I want to go (and is on by default, or can be made so).  Dolphin from the screenshots I have seen does not have these, and so is of no interest to me."
    author: "MamiyaOtaru"
  - subject: "Re: off topic remark"
    date: 2007-02-15
    body: "Dolphin may not currently have a tree view like the one in Konqueror's navigation panel. But it has something that's equally functional, and might even actually be better.\n\nWhen you click and hold on a folder name button in the breadcrumb widget, you get a dropdown list of the subdirectories in that folder. A picture speaks a thousand words, so this screenshot might explain it better.\n\nNote: This is a screenshot of Dolphin 0.8.1 on KDE 3.5.6\n\nhttp://jucato.org/stuff/dolphin.png"
    author: "Jucato"
  - subject: "Re: off topic remark"
    date: 2007-02-15
    body: "Wow. That is a pretty pretty file manager. When I first heard about dolphin my first thought was why would anybody want to replace konqueror. I understand now, although I think I'd still rather see a lot of those changes ported to konqueror instead of having them in a separate application."
    author: "reldruh"
  - subject: "Re: off topic remark"
    date: 2007-02-15
    body: "Oh no, please...\n\nI gave dolphin a try and didn't like it at all. Can it do ftp'ing? I didn't get it to work...also, looots of space on the left (in the sidebar) is empty and wasted for nothing...not excited at all, sorry guys."
    author: "fish"
  - subject: "Re: off topic remark"
    date: 2007-02-15
    body: "And yeah, where's the Krusader/MidnightCommander like shortcuts?\n\nI miss being able to move files and do things with the F Keys...\n\nAnd yeah, I still think Krusader should be the default filemanager for KDE!"
    author: "fish"
  - subject: "Re: off topic remark"
    date: 2007-02-15
    body: "I think it's a bit unfair to be looking for Krusader/MC shortcuts in an app that doesn't even try to emulate/imitate Krusader/MC.\n\nhttp://enzosworld.gmxhome.de/ explains what Dolphin is and what it aims to be. And what it currently has.\n\nAs for KIO, I have tested it to handle the following protocols already:\n\nhome:/, file:/, system:/, media:/, remote:/, applications:/, sftp:/, and ftp:/\n\nI can't test fish:/ and smb:/ as I have no way of testing these.\n\nSo, as for your other question, yes, Dolphin works with FTP/SFTP. \n\n(I find this all extremely strange, as I'm a hardcore Konqueror lover/user, and yet here I am talking about Dolphin...)"
    author: "Jucato"
  - subject: "Re: off topic remark"
    date: 2007-02-16
    body: "The only thing I know about Dolphin are screenshots making it look roughly like Gimp's load/save boxes (I said roughly...), with the path as buttons and things like that.\nI just wanted to say that personnaly, I absolutely hate this stuff and that since Gimp has put it in, it takes thrice as much time to just load or save a file than before. Being unable to cut/paste a path using the good X11 middle button is something I'm really furious against.\nFor the same reason, the full tree on the right side is incredibly more efficient than hovering over a directory name to see the other subdirectories : I can visualize the full tree by just rolling the wheel of my mouse and quickly find directories I've been searching for. Partial views make it a lot more complicated to do so.\nAnd as a matter of fact, Konqueror being a file manager and a browser is absolutely perfect for me and a great idea, in particular ssh, telnet and ftp accesses. I miss it so much on other systems where I need to fire several apps to do the same.\nThere are things that need to be changed, I suppose, for KDE 4, but the Konqueror concept is not one IMHO.\n "
    author: "Richard Van Den Boom"
  - subject: "Re: off topic remark"
    date: 2007-02-16
    body: "Konqueror isn't going anywhere :)"
    author: "Troy Unrau"
  - subject: "Wow wow wow! "
    date: 2007-02-14
    body: "This looks awesome!\n\nBy the way, thus anyone here use the ComicBook format? I like anime and comics but I've never encountered a ComicBook format... "
    author: "Darkelve"
  - subject: "Re: Wow wow wow! "
    date: 2007-02-14
    body: "djvu and [applied] math rules here..."
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: Wow wow wow! "
    date: 2007-02-16
    body: "See http://en.wikipedia.org/wiki/CDisplay_RAR_Archived_Comic_Book_File\n\nThere's a list of applications at the bottom that support this format.  I just nabbed the first file I could find :)\n\nCheers"
    author: "Troy Unrau"
  - subject: "Has finally a decision been made? quote from text"
    date: 2007-02-14
    body: "I agree with shamaz that another discussion like \"Ligature vs Okular\" isn't really productive, but is seems that won't be necessary anymore.\n\nquote from text; \"Regarding availability, okular will be available wherever your distribution packages it\"\n\nIt seems that Ligature will be part of the default KDE-tarballs (kdegraphics). Distributions have of course a possibility to override this.\n\nAnyway, personally I'm happy that only one \"universal viewer\" is part of the default KDE-tarballs. I personally also don't care which one it is. I just think that desktop/menu clutter isn't helpful for users.\n\n"
    author: "LB"
  - subject: "Re: Has finally a decision been made?"
    date: 2007-02-14
    body: "Don't trust everything you read ;-)"
    author: "Albert Astals Cid"
  - subject: "Re: Has finally a decision been made?"
    date: 2007-02-14
    body: "Care to explain? I thought that the dot was a reliable source. If you have more information please enlighten us."
    author: "LB"
  - subject: "Re: Has finally a decision been made?"
    date: 2007-02-15
    body: "It is a reliable source, usually. In this case your interpretation of the sentence (or the sentence itself) seems wrong, see http://developernew.kde.org/Projects/KDE4ReleaseInfo"
    author: "Albert Astals Cid"
  - subject: "Re: Has finally a decision been made?"
    date: 2007-02-16
    body: "ah, thanks for clearing up that the decision for the universal viewer hasn't been made yet ;-)\n\nMaybe the editor of this article could be so nice to change the text?"
    author: "LB"
  - subject: "Re: Has finally a decision been made? quote from text"
    date: 2007-02-14
    body: "\"quote from text; \"Regarding availability, okular will be available wherever your distribution packages it\"\n \n It seems that Ligature will be part of the default KDE-tarballs (kdegraphics). Distributions have of course a possibility to override this.\"\n\nthat's not what it says.\n\nwhat is says is that a lot of distributions split up kde packages into smaller ones, like kdegraphics-screenshot, kdegraphics-pdf_viewer, kdegraphics-ligiture, kdegraphics-povmodeling, etc. etc\n\nSo if kde defaults to ligiture and the user wants okular there won't be a problem, he/she can simply install okular and remove kdegraphics-ligiture.\n\n\n"
    author: "otherAC"
  - subject: "Re: Has finally a decision been made? quote from text"
    date: 2007-02-15
    body: "That's what I (am trying to) say, KDE defaults to Ligature and distributions can provide a way to override this.\n\nHowever according to Albert this article isn't correct, but he hasn't (been able to) tell what's incorrect yet."
    author: "LB"
  - subject: "Re: Has finally a decision been made? quote from text"
    date: 2007-02-15
    body: "> It seems that Ligature will be part of the default KDE-tarballs\n> (kdegraphics). Distributions have of course a possibility to override this.\n\nThere's no ligature in kdegraphics packages of _stable_ releases.\nThe only ligature in official KDE modules is the KDE 4 version that can be found in kdegraphics; it has been lonely to much time, and now the time to restore the status quo of KPDF/okular in KDE 4 again has come.\n"
    author: "Pino Toscano"
  - subject: "Re: Has finally a decision been made? quote from text"
    date: 2007-02-15
    body: "\"There's no ligature in kdegraphics packages of _stable_ releases.\"\n\nHehe, but there is with the old name kviewshell. Like there was KPDF, now extended to okular.\n\nBut having both in kdegraphics would be... strange. Perhaps such module bundlings would have to be rethought anyway. Does it really help? Who needs them in the current form?"
    author: "Slubs"
  - subject: "Comparing Snail to Caterpillar!"
    date: 2007-02-14
    body: "IMO, KPDF is caterpillar and KGhostview is a snail when rendering PDF files.. and I hope Okular will be a tortoise like the Acrobat.. which is a bit faster than KPDF or KGhostview.\n\nHave you looked at foxit pdf reader.. the Rabit..?\nhttp://www.foxitsoftware.com/pdf/desklinux/\n\nfoxit Renders too fast and has lot of features which okular may be having.. or deserve to have..\n\n"
    author: "fast_rizwaan"
  - subject: "Re: Comparing Snail to Caterpillar!"
    date: 2007-02-14
    body: "> KPDF is caterpillar\n\nNice joke ;)\n\n> KGhostview is a snail when rendering PDF files..\n\nIt's like using a paper cutter to cut the grass of your garden - it may work, but it's not the best way to use that tool.\n\n> I hope Okular will be a tortoise like the Acrobat.. which is a bit faster\n> than KPDF or KGhostview.\n\nWe work also to bring a fast okular, don't you want it? ;)\nOh, and acroread is generally slower here - notably exceptions to that are patterns, where xpdf/poppler is quite slow.\n\n> Have you looked at foxit pdf reader.. the Rabit..?\n\nNot yet.\n \n> foxit Renders too fast and has lot of features which okular may be having..\n> or deserve to have..\n\nWe will see.\nOh, it's not open source - then I think I'll still stick to acroread if I have to use a non-opensource application, at least it renders correctly and has all the functions I need.\n"
    author: "Pino Toscano"
  - subject: "Re: Comparing Snail to Caterpillar!"
    date: 2007-02-14
    body: ">>Have you looked at foxit pdf reader.. the Rabit..?\n\n~> ./ReaderLinux\nSegmentation error\n~>\n\nHmmm....\n\nI think i'll stick with kpdf ;)\n"
    author: "otherAC"
  - subject: "A More Intuitive UI"
    date: 2007-02-14
    body: "I've always been a passionate user of kpdf which is now re-branded okular, but i believe it deserves a more intuitive user interface and iconification. I am particularly pleased with the transformation and i believe okular can only get better.\n\nKeep up the good work guys, really appreciate it."
    author: "Lawal Adekunle"
  - subject: "My two cents"
    date: 2007-02-14
    body: "Speaking frankly, guys, I don't care much about new KDE4 applications/hacks/etc.\n\nWhat do we (yes, we) need most:\n\n1) Much faster applications start up\n\n2) Decent stability (even in the latest KDE 3.5.6 some applications keep crashing randomly)\n\n3) Decent, reworked KOffice with better MS .doc .xls .ppt support\n\n4) Better distro and hardware support with possibility of setting up network interfaces, configuring X.org settings, proper package management (e.g., yum equivalent), etc.\n\n5) More applications included in the base, like absolutely needed: Kaffeine, KNetStats, KTorrent, _maintained_ KSensors (with core and k8temp support), kxkb,  GwenView, KRename, kio_burn, kio_iso, KCHM, K3B\n\n6) Reworked KMenu and applications' shortcuts grouping with true support of drag and drop and human understandable logic\n\n7) A single \"kicker + kdesktop + kfind + kcalc\" application, so that kicker finally could be made truly transparent\n\n8) Smooth scrolling in all scrollable controls and, of course, KHTML\n\n\n\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "Hello,\n\nwhy \"we\"? You have no right to speak for us. :)\n\n1) KDE applications already start up pretty fast. I barely ever have to wait for anything. And if so, it's the bad occurrence of having to use Firefox or OpenOffice. Notable not KDE applications.\n\n2) What are you talking about? I am having an uptime of 2 weeks now on my desktop, and the last time something crashed, was Kontact (when the IMAP server was not running, and I clicked cancel to the password dialog) at startup.\n\n3) I don't need KOffice to do that, OpenOffice does that trick already. For my own documents, I write them in ODF now, or KOffice native formats. I would rather have KOffice people concentrate on fine ODF support. Once our main customer goes ODF, I would love to NOT have to use OpenOffice still.\n\n4) You, see it's funny. Maybe because I use Kubuntu, but everything works fine in Linux, which doesn't under Windows. For example, my optical wireless mouse, when the batteries get low, I have to exchange them, because under Windows (gaming) it stops working. Can't configure anything. The batteries continue to work for many weeks. Then my onboard sound driver, the last driver was released 2003, 4 years ago. It must have been perfect. Ok, except that I can play Oblivion and Eve only without sound. With sound the system crashes to power off reproducible and within like 30 minutes of play. Can't blame a 2003 driver for not working with 2006 games though. But an update is not expected. The WLAN PCI card (RaLink, GPL driver) that I bought, works perfect with Linux. I am having many hours of interrupt free connection (and fast) to my router. Under Windows, this is not so. Sometimes I have to reboot, because connection never lives longer than a few seconds, it's maddening, sometimes problems start after 1 or 2 hours, then go away again.\n\nNo really. It's quite good, hardware integration and correctness under Linux has not been an issue for years to me.\n\n5) Why do you need applications included in the base at all? Is that somehow magically going to make them better? I am content as long as they end up packaged by Debian and thereby Ubuntu.\n\n6) I am not using the KMenu. Never not at all. I have quickstart buttons a few, and then I use ALT-F2 mini-cli to launch things. One or two keys, and it completes to what I want. When I want to find applications, I use google. Once I have their names, I search in Kubuntu the package and install it. Then I don't bother to hunt it in menus, I know it's called \"krita\" anyway.\n\n7) Reads like what Plasma is going to give. Maybe an unfriendly side note and I have no right to say so, but Plasma was a more popular theme on certain persons blogs for a lot time, until Trolltech recruited. I would like a statement, if Plasma still has the priority, it had.\n\n8) I don't need smooth scrolling. Scrolling text is hard to read. I need fast scrolling. And that I have in KHTML and Kate parts. Sometimes I am even annoyed by too much smoothness in KPDF, where I rather want to see things now. Thing is, while it's moving, I can't read it.\n\nSo you, go away, don't say \"us\". I need completely different things than you do.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "7) You referring to Aaron? If so I think it's a little unfair, isn't he sponsored by Trolltech rather than employed and they don't therefore tell him what to do. I suspect he stopped blogging on plasma because the hype is too much already and he's maybe fed up with everyone asking about it and speculating."
    author: "Simon"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "Granted, it is entirely unfair. And yes, who else? Who owned the plasma hype since its inception?\n\nBeing fed up from the hype after having produced the hype _entirely_ yourself, would be unfair too. I welcome the fact that he became sponsored, it meant a whole lot of gain for KDE and us, don't get this wrong. He certainly is one of the more talented KDE developers, and a voice.\n\nI still remember how innovative plasma should be. But maybe it is too boring now, before it's done even. I have \"talk is cheap\" on my mind here. Applies to me and him.\n\nGuess, I shouldn't react that way, just because everybody else has their pet techs shaping up already, doesn't mean Plasma needs to be ready.... ah well, we want news about it. now already ;-)\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "\"8) I don't need smooth scrolling. Scrolling text is hard to read. I need fast scrolling. And that I have in KHTML and Kate parts. Sometimes I am even annoyed by too much smoothness in KPDF, where I rather want to see things now. Thing is, while it's moving, I can't read it.\"\n\nI have a dell laptop with a crappy ATI onboard video (no acceleration with fglrx or xorg driver), and I have SuSE on the laptop which includes a smooth scroll patch for at least Konqueror.  On some larger pages (like a long Slashdot article), trying to scroll down can take MINUTES (because it can't redraw/rerender fast enough) instead of instant without smooth scrolling (1 redraw versus say 50, which would think would be easier?).  Unfortunately I have no clue how to disable the smooth scrolling short of switching distros.  Also 'scrolling text is hard to read' is DEAD on, I've never seen any application use smooth scrolling that didn't make it hard/painful/impossible to somewhat read the text while scrolling.  Basically, you're spot on especially on this one (mostly commenting cause while scrolling a long page it uses so much CPU time that Amarok will stop playing until it finishes)."
    author: "Corbin"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "Amen.  I disable smooth scrolling as fast as I can in Kopete's contact list (a place where scrolling doesn't happen that often and there isn't that much to read).  Smooth scrolling is way overrated."
    author: "MamiyaOtaru"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "To disable smooth scrolling for HTML in SuSE linux, put \n\nSmoothScrolling=false\n\nin the [KDE] section of your $HOME/.kde/share/config/kdeglobals file.\n\nIt's unfortunate kopete used its own independent setting here: I think this is one feature you either want enable everywhere or disabled everywhere.\n\nI'd put a switch to enable/disable such features globally in say the style panel of kcontrolcenter, but..."
    author: "Luciano"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "1) KDE apps? The only ones I find a little slow are things like Firefox and OOo (because they have to load more libraries not being kde integrated? I don't know). Not much kde can do about them, except continue to improve the kde alternatives. Likely some improvements with qt4 anyway?\n2) Really? Which ones? Only thing I've managed to crash recently is a beta of k9copy, which isn't core kde and is a beta (need to look into a bug report sometime)\n3) Koffice is getting reworked. Already 1.6.x is a big improvement over 1.5.x Not so sure about .doc support etc, with MS switching to OOXML and everyone else to ODF I think it's unlikely to be pursued (sure, it would be nice to support every format)\n4) I agree, although some of this is more the distro's jobs. Adept is rapidly improving in debian land. Knetworkmanager is quite usable. A graphical xorg thing would be nice, did I see one on kde apps?\n5) Kaffeine - I agree, or KMplayer. Is Ktorrent not in the base, I lose track? Gwenview is moving into kdegraphics, I think? K3b, again nice. Some distros install these by default anyway. There's nothing else in your list that I need, except kio_iso sometimes. So no _we_ don't need all that\n6) Reworked kmenu is coming, actually I don't mind the current one anyway\n7) Plasma?\n"
    author: "Simon"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "Ahh, someone else was quicker. Apologies for the (largely) duplicate post"
    author: "Simon"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "ad. 5\nYeah, it's not _needed_. But still MUCH more useful to the majority of ppl than some apps that DID get to the base. Think KPovModeler, for example..."
    author: "zonk"
  - subject: "Re: My two cents"
    date: 2007-02-16
    body: "it takes two to party, and if the teams of amarok, gwenview, kaffeine etc. don't want to get tied into the core kde packages, then it's not going to happen...."
    author: "otherAC"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "ACK! \n\nI too think that KDE needs much more focus on usability and stability. Using KDE nowadays feels like many features have been roughly implemented but not been tested and stabilized enough. Many things are just buggy like mediamanager or kdesktop, which I think are nice and important parts of KDE but they are way too buggy.\n\n"
    author: "Marc"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "Definitely...\n\nIn my case, Konqueror is the app that crashes the most. I'm tired of submitting bug reports.\n"
    author: "Dima"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "Konqueror? Crashes? I don't recall such thing happening to me and I'm using kde on _fedora_, for chrissakes. Plus I have konqueror always open (and I mean always, I practically never log out)."
    author: "atte"
  - subject: "Re: My two cents"
    date: 2007-02-16
    body: "can you point us to some of those bug reports?"
    author: "otherAC"
  - subject: "Re: My two cents"
    date: 2007-02-16
    body: "Konqueror crashes when\n1) Opening certain SVG files (bug reports already exist)\n2) Using embedded kaffeine/video kpart and watching certain videos"
    author: "Artem S. Tashkinov"
  - subject: "Re: My two cents"
    date: 2007-02-18
    body: "Stop using Kaffeine. Use the KMPlayer part and your crash problems will be solved."
    author: "Thiago Macieira"
  - subject: "Re: My two cents"
    date: 2007-02-24
    body: "don't use kaffeine. it is very bugy and slow. use KPlayer. it is very stable, fast (based on mplayer) and has a stable kpart for konqueror too. your crashes will go... it is not konquerors fault!"
    author: "litb"
  - subject: "Re: My two cents"
    date: 2007-02-24
    body: "Ok, I don't get it. What the deal?\nI thought Kaffein is to become the default video player for KDE.\nKMPlayer isn't even in the main repositories of my distro (kubuntu).\nPersonally I just tried KMPlayer again because you guys mentioned it,\nand I have to say, I prefer Kaffein. I think it just looks better. ;)\nYes, I can the confirm that kaffein is sometimes crashing the Konqueror, but I don't often preview videos in Konqueror.\nMaybe someone could elaborate why Kmplayer should be better, except that it's more stable, which i think is a very blury statement."
    author: "Bernd"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "I think you should check your installation.\nkde is rock solid on all my machines, no crashes at all..\n"
    author: "otherAC"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "> 1) Much faster applications start up\n\nAgreed :)\n\n2 seconds is not fast enough. I always have a second until an app launches. It should be \"boom! within one second\".\n\nThis is mainly a GNU linking issue though. All apps with similar symbols (e.g. functions names gaim_...()) suffers from this. All C++ functions also have a similar prefix (the classname / return value) suffer from this, including Firefox and OpenOffice. Ever noticed Firefox starts up faster in Windows? ;)"
    author: "Diederik van der Boor"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "I think Thiago has been working on this with KDE4, trying out the -fvisibility=hidden/ protected/ whatnot GCC flags.  I can't remember what his conclusions were, though."
    author: "Anon"
  - subject: "Re: My two cents"
    date: 2007-02-18
    body: "We're working on that, but it will take a fairly recent gcc and binutils to make most out of it. I.e., gcc 4.2 and binutils not yet released at the time of this writing."
    author: "Thiago Macieira"
  - subject: "Re: My two cents"
    date: 2007-02-15
    body: "> This is mainly a GNU linking issue though.\nPerhaps Firefox is compiled without visibility=hidden? On Windows this would not be a problem (exports are private by default).\n"
    author: "Tim"
  - subject: "windowdecoration "
    date: 2007-02-15
    body: "I like your windowdecoration color, what color do you use for the inactive windows?"
    author: "Marius"
  - subject: "Re: windowdecoration "
    date: 2007-02-16
    body: "For KDE 4, I use the defaults.  In fact, I delete .kde each week to ensure that my shots are accurately showing off the evolving defaults for KDE 4.  (Next week will show off some newly minted oxygen stuff, as that is now slowly becoming the default.)\n\nFor KDE 3.x, I use #6569AE blended with #7B69B8 for active titlebar, and #DFE1E6 blended with #9D9EBA for inactive titlebar.  I use the Crystal window decoration and the Plastik style.  Other than a minor palette tweak or two, these are mostly the kubuntu defaults I believe... however they may be the defaults from a few releases ago, as I've been upgrading rather than reinstalling.\n\nHope that answers your questions :)"
    author: "Troy Unrau"
  - subject: "Working application"
    date: 2007-02-16
    body: "Other plattforms as Mac or Win will be very important in the KDE4 process as the next generation KDE applications would already run there. I think pdf viewers would be a great advertisement instrument for KDE on the MAC. And users are important to mature an application and find or close bugs. Everybody hates the slow Acrobat Reader. So okular or ligature can really become the Firefox of pdf reading."
    author: "Bert"
  - subject: "Will okular be able to display all pdf's that i th"
    date: 2007-02-17
    body: "row at it and also display them correctly AND allow me to edit forms? Or else i'm not interested. \n\ni'll stick to adobe reader.\n\nIn fact, I would be happier with only adobe reader on linux instead of kpdf, evince, etc etc etc because atleast then, the users doesn't have to waste his time on trying out different pdf viewers only to discover that the only one that supports the most basic(basic - as it's the whole point of the program; to be able to view pdfs and annotate forms) functionality is in a proprriatory app.\n\nIn fact, i'd rather youd scrap both okular and ligature if it meant that the backend library would get more work and eventually support this functionality. Then i'd just use the \"old\" pdf viewers, only now i'd be assured that they actually WORKED.\n\nToo hard? Then.. if you truly care about your users atleast pop up a message that tells the user that not all pdf's will work and display correctly when you open the program. In fact i'd like one for mplayer aswell and amsn telling me which formats/functionality and movies they DOESN'T support as this is more useful than being told it works \"for most\".\n\nThis is something users will be GRATEFUL for, it's not something they will get ANGRY for... Oh well i have a hangover so this text is a bit weirdly written but my point still stand."
    author: "kris"
  - subject: "Re: Will okular be able to display all pdf's that i th"
    date: 2007-02-17
    body: "Not likely to happen, as such information usually already exists in the form of TODO lists that are on these application's websites.  See the Supported Formats list for okular, for example.\n\nPoppler (the PDF lib powering Evince, Okular, Ligature...) is pretty great, and I've never had a problem rendering a PDF with that library so far.  The existing applications that you are referring to, like kghostview, use other libraries that are less maintained.  The Poppler library really is becoming the king.\n\nThat said, I don't know the status of forms, since I've never encountered a PDF form in my life... other than the fact that I'm aware of their existence..."
    author: "Troy Unrau"
  - subject: "Re: Will okular be able to display all pdf's that i th"
    date: 2007-02-18
    body: "> telling me which formats/functionality and movies they DOESN'T support \n> as this is more useful than being told it works \"for most\".\n\nImagine walking into a car dealer and the first thing the sales guy tells you about a car you are looking at is what doesn't work and what it can't do. Is that going to make you think it's a good car? Do car dealers usually do that? No, of course not. People would never see notices like that as something useful or positive - it would just discourage them from using the applications, even if they would be perfectly fine for their needs.\n\nAs for PDF forms, you consider this basic functionality, but for me I have never needed to fill in one, or even seen one. Besides, it has already been stated that the functionality will appear when it gets supported in the base library."
    author: "Paul Eggleton"
  - subject: "amarok"
    date: 2007-02-18
    body: "Isnt the reason for, why amarok is not in the official kde packages, because they don't want to be?\nAs far as I remember they don't like kde's release cycles wich are quite unflexible. Same aplies for k3b I think."
    author: "adrian"
  - subject: "pdf in ligature"
    date: 2008-05-14
    body: "to open pdf files in ligature, you need to install poppler (and the devel libs) first. w/o poppler, ligature installs fine, but it won't read pdfs."
    author: "ligature"
---
Focusing again on applications this week, specifically I'll look at two of the promising document viewers for KDE 4, <a 
href="http://okular.org/">Okular</a> and <a 
href="http://websvn.kde.org/trunk/KDE/kdegraphics/ligature/">Ligature</a>.  
They are two of the rising stars of KDE 4, but they both have their 
roots as KDE 3 applications that have grown up. Read on for more...

<!--break-->
<p>In the past, KDE has had a wide variety of programs designed for viewing all sorts of file formats, and using the KParts technology of KDE, these viewers were able to be embedded into other KDE applications such as Konqueror as and when they were needed. Supported formats included TIFF, PDF, PostScript, fax, DjVu files, and many more. okular and Ligature have grown up from some of these earlier designs to become much more than simple document viewers.</p>

<p>Historically, KDE has shipped with a program called KGhostView, which used the GhostScript backend to render PDF and PostScript files for display. KDE has even used it as the Print Preview utility. What follows is a shot of KGhostView in action for KDE 3.5.6. Please note that some of the font rendering glitches may be due to the font selection of my distribution, and are not necessarily a reflection on KGhostView's ability to render this file.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol7_356_kghostview.png"><img src="http://static.kdenews.org/dannya/vol7_356_kghostview_thumb.png" alt="KGhostview in 3.5.6" /></a></p>

<p>In the KDE 3 series, a new contender emerged for viewing PDF files that was much faster than KGhostView. This new program, KPDF, has since eclipsed KGhostView in many important areas, like functionality, speed, and so on. KPDF is shipped with many modern distributions as the default PDF viewer for KDE. Below is the same file as viewed with KPDF.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol7_356_kpdf.png"><img src="http://static.kdenews.org/dannya/vol7_356_kpdf_thumb.png" alt="KPDF in 3.5.6" /></a></p>

<p>From personal experience, KPDF has been the subject of some of the <i>'oh wow!'</i> type experiences I've had while using KDE. I've clicked on web links to PDF files that specify 'frame' as their target, and had KPDF embed quickly and seamlessly, so much so that for a moment I forgot that the frame wasn't HTML anymore.</p>

<p>It also has quite a few advanced features that KGhostView never really pulled off, such as text searching, copy and paste from PDF, and more. It is also many times faster at rendering, especially when loading PDF files containing a lot of vector imagery. I use a lot of maps in my work, which often ship as PDF files - using KGhostView to view these files was incredibly slow, as you could literally watch the vectors being drawn on the map. KPDF would load the same map visible instantaneously, letting me do my work instead of waiting for the computer.</p>

<p>KPDF recently made the decision to broaden its support and start to view files of types other than simply PDF, partially thanks to coders sponsored by Google's Summer of Code program. The main reason they decided to do this inside KPDF instead of starting new, individual applications is that KPDF already had many advanced features implemented that didn't necessarily need to be duplicated for these other file formats. To more accurately reflect its broadened scope as a viewer for many file formats, it has been rebranded as 'okular'.</p>

<p>Users of KDE 4 are in for a treat with both okular and Ligature, as they are both shaping up to support a wide variety of (occasionally overlapping) media formats. But since they can both be embedded into KDE applications using standard interfaces, a user should be equally happy using either one of these viewers. I'll talk about okular first, since I have more information sources for it. Huge improvements are noticeable in okular over the already very functional KPDF. So far, it looks to be one of the best applications of KDE 4.</p>

<p>Pino Toscano (pinotree on irc.freenode.org) is the lead developer of okular. Currently it is being developed in KDE SVN and its sources are available in <a href="http://websvn.kde.org/trunk/playground/graphics/okular">
/trunk/playground/graphics/okular</a> for anyone who is interested in trying it out. It is already quite stable in the KDE 4 environment - actually it is one of the most stable KDE 4 applications I've had the pleasure of testing so far. It is also known to be building as part of the KDE/Mac packages. Benjamin Reed submits the following screenshot showing okular in action on the Mac:</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol7_4x_mac_okular.png"><img src="http://static.kdenews.org/dannya/vol7_4x_mac_okular_thumb.png" alt="okular/Mac in 4.x" /></a></p>

<p>He also adds: "Holy crap, okular is fast on OS X. No more Acrobat for me! :)"</p>

<p>I didn't test all of these formats myself, but according to its <a href="http://www.okular.org/formats.php">Supported Formats</a> list at the okular website, it already has full or partial support for the following 11 document types: PDF, PS, TIFF, CHM, DjVu, DVI, XPS, OOo, FictionBook, ComicBook and standard graphics files. Work continues on making sure that support for all of these formats is flawless, and more formats may be added down the road. The okular that is released alongside KDE 4.0 may or may not have all of these formats enabled, depending on their stability at that time, as well as choices that your distribution may make.</p>

<p>Below is a shot of okular viewing the ComicBook format, often used to distribute comics online. Indeed, okular may end up becoming one of the most popular ComicBook viewer applications, especially considering how many platforms it will run on courtesy of KDE 4.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol7_4x_okular.png"><img src="http://static.kdenews.org/dannya/vol7_4x_okular_thumb.png" alt="okular/X11 reading a .cbr file in 4.x" /></a></p>

<p>Pino has shown his willingness to work with the <a href="http://www.okular.org/news.php#itemokularjoinsTheSeasonofUsabilityproject">usability folks</a> to help improve the ease of use of okular, and it is now part of the <a href="http://dot.kde.org/1170202411/">Season of Usability</a> project. It will likely see a fairly-thorough overhaul of many interface elements before KDE 4.0 is released which can only make this application even better.</p>

<p>The other contender for your document viewing needs in KDE 4 is Ligature, recently renamed from KViewShell. It lives in the kdegraphics module, so it is currently the default viewer for the files that it supports. It could however be overridden such that okular is used by anyone who prefers using okular for a given format. The only reason I can find for Ligature to be in the kdegraphics module rather than okular is historical - KViewShell (which Ligature is based on) was part of kdegraphics in the past. However, this doesn't mean KDE is shunning okular either: for example, Amarok is one of KDE's best applications, though it doesn't reside in the official kdemultimedia package.</p>

<p>Ligature currently sports support for PDF, PostScript, EPS, fax, Tiff, DjVu, and TeX files based on the plugins available in SVN. I'm under the impression that 'fax' is for a sequential image type format using TIFF files. Its predecessor, KViewShell did not have support for several of these formats in the main kdegraphics branch, but a separate branch exists for these formats for KDE 3.5.x.</p>

<p>I tried to get a screenshots showing Ligature displaying PDF files, but it wouldn't load them. I tried a PostScript file, and it loaded it but did not display anything. So, I had to resort to a rather boring DVI file in order to show off the current state of the user interface, but this does not show off its rendering capabilities very well.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol7_4x_ligature.png"><img src="http://static.kdenews.org/dannya/vol7_4x_ligature_thumb.png" alt="Ligature/X11 reading a .ps file in 4.x" /></a></p>

<p>It does bear a close resemblance to okular as far as its user interface is concerned. This is mostly due to the fact that they utilise the same standard Qt and KDE libraries to draw many of the user interface elements. Since I could not get it to render any documents, I could not compare its actual usability to okular. Please keep in mind, however, that it is in a state of development at the moment, so being broken on any given day is nothing to be overly concerned about.</p>

<p>A note about DVI files in general: to view them you need to install some TeTeX files, which on my distribution totals 85 megabytes - a likely reason why DVI files are not a popular format for documents despite their competent rendering abilities. When Ligature finds a hyperlink in a DVI file, it underlined the text in blue to indicate that you could click on it, which while useful in some circumstances, made documents with links look quite ugly. okular on the other hand does not underline links in DVI files, but they still work as expected.</p>

<p>For those wondering about duplication of efforts, okular and Ligature use different internal architectures, but many of the library dependencies depend on are the same (much like how MPlayer and xine have very different internals, but can still use the same low level libraries to decode media). This means that while they cannot easily be merged into one project, any work that trickles down into the lower level libraries will be beneficial to both projects. Regarding availability, okular will be available wherever your distribution packages it, and since most distributions end up splitting packages like kdegraphics into their constituent apps anyway, Ligature will fall into the same category for most users. Of course, GNOME users can also use okular or Ligature as well, if they have the required KDE libraries installed, but of course they can also use <a href="http://www.gnome.org/projects/evince/">Evince</a> which shares many of the same backend libraries, but is better integrated into the GNOME environment.</p>

<p>That's all for this week folks. Hope this clears up any confusion about the nature of both okular and Ligature.</p>
