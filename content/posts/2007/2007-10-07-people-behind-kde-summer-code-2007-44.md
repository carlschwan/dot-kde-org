---
title: "People Behind KDE: Summer of Code 2007 (4/4)"
date:    2007-10-07
authors:
  - "dallen"
slug:    people-behind-kde-summer-code-2007-44
comments:
  - subject: "Go guys!"
    date: 2007-10-07
    body: "Very nice interview! A very good reading on this sunny sunday morning ;-)\nKeep up the good work, I really like every project (just checked out step and painterly, I don't perform music notation or play with molecules. yet.)\n\n"
    author: "an user"
---
<a href="http://behindkde.org/">People Behind KDE</a> releases the fourth and final interview in its series of interviews with students who are working on KDE as part of the <a href="http://code.google.com/soc/kde/about.html">Google Summer of Code 2007</a> - <a href="http://behindkde.org/people/soc2007-four/">meet Marijn Kruisselbrink, Alexandr Goncearenco, Emanuele Tamponi and Vladimir Kuznetsov</a>!

<!--break-->
