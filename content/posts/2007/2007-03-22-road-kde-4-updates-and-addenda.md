---
title: "The Road to KDE 4: Updates and Addenda"
date:    2007-03-22
authors:
  - "tunrau"
slug:    road-kde-4-updates-and-addenda
comments:
  - subject: "Keep it up!"
    date: 2007-03-22
    body: "Great job!\nKDE is going to blow my shoes away! :D\n\n"
    author: "T1m"
  - subject: "Thumbs up!"
    date: 2007-03-22
    body: "Very impressive! Good work! "
    author: "winter"
  - subject: "Alt+Esc in Games"
    date: 2007-03-22
    body: "I would like to have a feature in KDE(krunner) that overrides the game you are playing so that you can exit a fullscreen game at any time and go to the desktop by pressing Alt+Esc (Similar to how it is done in windows).  \nThis IMHO is a great feature that I've missed several times in KDE.\n\nAlso, great article! These articles really help to keep the community on the edge."
    author: "pascal"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-22
    body: "It's not a KDE problem, but SDL. Vanilla SDL allows game to grab keyboard. Some distros like ubuntu have SDL patched to prevent this"
    author: "Stalwart"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-22
    body: "What is SDL?"
    author: "pascal"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-22
    body: "The Simple Direct media Layer or similar acronym.  It's a graphics and sound layer designed for making games, visualizations, and so forth.  Not part of KDE, but used by KDE applications on a few occassions.\n\nWhat is happening is that SDL is telling X that all keystrokes have to go to SDL instead of other programs.  This effectively locks other programs out of receiving these keystrokes - which is good when you're trying to hit F2 to change weapons at the same time as pressing ALT to jump... You don't want the run dialog popping up :)\n\nThat said, certain key combos should still be able to be passed through to X... CTRL-ESC and CTRL-ALT-DEL come to mind.  (I'm not sure how SDL treats CTRL-ALT-BKSP... anyone know?)"
    author: "Troy Unrau"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-23
    body: "The real problem is that there's no common, configureable shortcut system, applications have to obey to. One of the big usability issues of the Linux desktop."
    author: "Carlo"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-23
    body: "Hmm...  I think I'll email the freedesktop guys about this, there should be a way in X to configure key combos to be passed through to the WM."
    author: "f00fbug"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-23
    body: "The question is not X, but abstracting the actions from the key combinations and mapping those via a chosen config (so e.g. KDE apps would use Gnome shortcuts in a Gnome desktop and vice versa). This is everything else than simple, since you need to provide fallback shortcuts or even generate them (or ask the user to chose ones) in rare cases for applications, which use shortcuts for actions that are not available in such a common desktop action-scheme. Also getting all and every application support such a change, will take  time."
    author: "Carlo"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-22
    body: "A similar question: In some situations in KDE 3, such as when you have a menu (e.g. K menu or Konqueror menu) open or when you have dropped down a \"combo\" box (e.g. the address field in Konqueror), global keyboard shortcuts are somehow grabbed and do not work. This means for instance that the volume -/+ keys on my notebook are inoperative. It would be very nice if this were resolved for KDE 4.\n\nI would be happy to file a bug (if one does not exist) if someone just told me which component of the system is responsible for this behaviour."
    author: "Martin"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-22
    body: "\nI've always placed bug reports using my intuition.  The bug \"handlers\" can then re-assign them if they feel they belong somewhere else.  It is better to have a misplaced bug report than none at all."
    author: "Henry S."
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-22
    body: "True, this is an annoying problem. I must admit I never thought about reporting it..."
    author: "superstoned"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-03-23
    body: "Same situation here."
    author: "panzi"
  - subject: "Re: Alt+Esc in Games"
    date: 2007-04-22
    body: "Oh. That explains this problem I've had with KDE locking up completely when a program starting pops up the KWallet password prompt while I'm typing in Konqueror's address bar. Ctrl+Alt+Bksp has been the only solution :-(\n\nNo, I never filed a bug report since I'm using \"ancient\" 3.4.2..."
    author: "Niels"
  - subject: "Drop the + and - in TreeView, use triangle arrows"
    date: 2007-03-22
    body: "Mac OS X, Windows Vista, and GNOME have stopped using + and - for expanding and contracting folders in the TreeView, and are now using triangle arrows pointing either sideways or down. I think triangle arrows are more intuitive because they actually point in the direction of the folder contents. The + and - are just too cryptic and geekish. "
    author: "AC"
  - subject: "Re: Drop the + and - in TreeView, use triangle arrows"
    date: 2007-03-22
    body: "This appearance is a function of the widget style.\n\nI took my screenshots with what are currently the KDE 4 default widget style, but it will not be the final KDE 4 widget style.  The final style for KDE 4 is still a work in progress (hence why it hasn't been made the default yet) but this is definitely the sort of change that can happen there.\n\nThat said, I don't see anything particularly wrong with the current form, as + and - intuitively mean 'expand' and 'collapse' to me.  And changing it just because OS X and/or Gnome have changed it is not a great argument.  That same argument would suggest that we change our button orders, and various other things as well, which is not going to happen.\n\nThat said, I am under the impression that this is not really a ease-of-use issue, and if it does get changed in a given style, it'll be because the artists who are writing the style think that it looks better :)  Which is more valid (in my opinion) than the other arguments :)"
    author: "Troy Unrau"
  - subject: "Re: Drop the + and - in TreeView, use triangle arr"
    date: 2007-03-22
    body: "\"And changing it just because OS X and/or Gnome have changed it is not a great argument.\"\n\nIt have already been changed once, so why not twice? \nKDE 1 rules. Nice clear small icons, too!"
    author: "reihal"
  - subject: "Re: Drop the + and - in TreeView, use triangle arr"
    date: 2007-03-23
    body: "Yesh, KDE was only good until KDE2 ;-)\n\nThat said, the minimalistic style of the days had its appeal."
    author: "Debian User"
  - subject: "Re: Drop the + and - in TreeView, use triangle arr"
    date: 2007-03-23
    body: "I notice KDE used to have nice fonts :-)"
    author: "Erik"
  - subject: "Re: Drop the + and - in TreeView, use triangle arr"
    date: 2007-03-23
    body: "Well, the fonts depend on the distro... but nonetheless, those fonts are not using anti-aliasing.  You can turn anti-aliasing off and have the fonts look somewhat like that still -- they are much more readable in lower font sizes that way, as well as quicker.\n\nX and fonts is still a big problem, after years of work..."
    author: "Troy Unrau"
  - subject: "Re: Drop the + and - in TreeView, use triangle arr"
    date: 2007-03-22
    body: "Speak for you're self, I like + & -"
    author: "Ben"
  - subject: "Re: Drop the + and - in TreeView, use triangle arr"
    date: 2007-03-23
    body: "++"
    author: "Carlo"
  - subject: "Re: Drop the + and - in TreeView, use triangle arr"
    date: 2007-03-22
    body: "The TreeView widgets are not drawn by KDE apps directly, but by QT (more specifically, by the QT style plugin you use). Thus, some widget styles will have +/- icons in TreeViews, some will have triangle arrows, some will have something totally else :). It's your choice which widget style you use. And some, like the default KDE3 Plastik style, have this configurable. Just run Control Center (why isn't it named \"Kontrol Center\"?), go to the (Appearance&Themes | Style) applet, and click on the \"Configure\" button beside the style selector combobox. In the Plastik style the option is named \"Triangular tree expander\". Enjoy :)."
    author: "zonk"
  - subject: "Will KDE 4 eyecandy be fast?"
    date: 2007-03-22
    body: "Real-time effects are something I'm really looking forward to in KDE 4.  I'm sick and tired of fake transparency, etc.  The video of krunner was really neat show of the power of QT4 because the text, which was scrolling quite fast in the background was showing trough the krunner window in real-time.  This is what I'm wondering, however, I recognize that QT4 is supposed to be faster, but will it be faster while doing chores that were never done before in KDE?  Is real-time transparency with QT4 just an optimization of code which should have been done a long time ago, or will it require a new graphics card and CPU running with aiglx, etc.?  It would be nice if i could boast that all this Vista/OSX technology will be available for even old Pentium IIIs with KDE 4, but is that really true?"
    author: "Batonac"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-22
    body: "Actually, that video is showing off a feature I haven't yet talked about for KDE 4 (mostly because it hasn't been merged into trunk yet).  It's using kwin_composite, a GL accelerated kwin which does the real transparency at the video card level.  \n\nI've briefly talked to the kwin_composite dude (Seli), and he informs me that it will make it into KDE 4.0, barring any unusual problems.  It's a good solution for KDE to appeal to those wanting a pretty desktop (like Beryl), but at the same time it has a great fallback mechanism whereupon those without GL acceleration can use some effective software rendering, or have all effects turned off.  Basically, it's all about adding bling, while maintaining KDE 3's level of performance (or actually improving it where possible).\n\nWhen it merges into trunk, I'll feature it here :)"
    author: "Troy Unrau"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-22
    body: "OK, so let me get this strait.  The transparency in that video had nothing to do with the QT4?  It was just done through a beryl/compiz like setup?  If this is true, what is the advertised QT4 real-time transparency good for then?"
    author: "batonac"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-22
    body: "Take a closer look - there are two transparencies happening:\n\nOne is showing a widget that you can see part of the background image from.  It is shown off in the still screenshot I posted above, which does not use any beryl/compiz-style transparency.  You will notice that the run dialogs' background is showing even in the line-edit.  This is an application-internal type of transparency, possible because Qt controls the whole widget stack.  It's quite slick... \n\nBut Qt doesn't control X, so when doing transparency between windows, X methods need to be used. This beryl/compiz like window tranparency is handled by kwin_composite and relies on Qt, GL, and the X Composite extensions.  This video is not using beryl/compiz, but uses a similar implementation found in kwin_composite.  It uses Qt for some effects (such as blurring/recolouring the background) and Qt-driven GL calls for others (such as wobbly windows, no demonstrated in the video) where appropriate."
    author: "Troy Unrau"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-23
    body: "Hey thanks troy for keeping up with me, I understand now.\n\nYou gave me an idea though, we really should build an X replacement that's completely controlled by QT, a QT accelerated graphics display system for Linux, where QT can control all elements of the display, not just the internal parts of QT programs.  That would be talking.  Its unification like this that's needed in order for Linux to take over the desktop."
    author: "batonac"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-23
    body: "That's all well and good but what happens when I want to run a non-QT app?  There are several here and there that are pretty good......"
    author: "Frogstar Robot"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-24
    body: "A display system that supported only QT would force the KDE community to create KDE programs that provide ALL computing needs.  This would be a good thing since it would provide complete unification of the Linux desktop.  Think about it.. ALL programs would use the same file save and open dialogs.  ALL programs would use the same color scheme/widget style.  ALL parts of the display would be powered by the SAME graphics engine which means LESS CONFIGURATION and LESS CONFUSION.  KDE taskbars should be able to have true real-time transparencies just through QT 4, but NO, in order to do this, we have to write additional 3d extensions to kwin which will be working IN ADDITION to the new QT Arthur paint engine, instead of being powered by it.  Arthur is probably powerful enough to do this, but QT doesn't control X, so it can't be done.  I'm really sick and tired of X windows actually.  X doesn't have native SVG render support so all SVG used in QT programs have to be rendered and cached before they can be displayed by X.  If the graphics system was controlled by QT, as QT would improve, so would the graphics system, new versions of QT wouldn't have to constantly maintain backwards compatibility with an out-of-date graphics system."
    author: "batonac"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-25
    body: "Go buy a Mac, you really want one of those. "
    author: "Vide"
  - subject: "KDE should be better than Mac OS X"
    date: 2007-03-26
    body: "Ha! You're probably right, I probably should just get a mac, but Mac OS isn't completely opensource.  KDE Linux really should be a complete Mac OSX replacement, but currently, it doesn't quite cut it."
    author: "batonac"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-23
    body: "Does the fallback include using 2d compositing/EXA?  I much prefer this as it is, at least on my machine, considerably faster and nicer to look at than all that Beryl crap, which runs slow and looks dumb."
    author: "me"
  - subject: "Re: Will KDE 4 eyecandy be fast?"
    date: 2007-03-23
    body: "You'd have to ask Seli about who the fallback mechanism works in more details... or, once it's merged back into trunk, I'll do the research and write about it.  \n\n*puts on a fedora with a press card on the front*"
    author: "Troy Unrau"
  - subject: "KWin OpenGL fallback"
    date: 2007-03-24
    body: "This subthread is of concern to me as well, as I've presently a now aging but until very recently \"best freedomware 3D support available\" ATI Radeon 9200 series card.  The native xorg Radeon driver in merged framebuffer mode supports accelerated OpenGL on this thing up to 2048x2048, but I'm running two monitors at 1600x1200 resolution, stacked for 1600x2400, so there's 352 lines' resolution unaccelerated at the bottom of the combined display.\n\nWith KDE3 (3.5.6 currently, on Gentoo/amd64), running xorg (now the 7.3-rcs), I've found EXA works waaayyy better than XAA, with composite and composite effects (only the transparency, fading takes time, and shadows just don't add anything to my experience, maybe because I run light foreground on dark background by default, so I can't see them in many cases, unless I reversed them of course, which is just... weird) turned on.  It works quite well, actually, with the single exception being OpenGL apps go blank when moved partially into that zone... not so great when that's my main work monitor.  I'd hope that before the entire desktop goes OpenGL, xorg would kill that 2048 max height/width acceleration issue -- and of course the xinerama OpenGL accel issue if it still exists as well.  That's not under KDE's control of course, but a decent fallback to 2D EXA would be fine, as long as it remains a viable option.\n\nOf course, if there's one thing KDE two and three have been good at -- one of the main reasons it's my desktop of choice -- it's giving people reasonable options, and I'm reasonably sure that's going to continue with four as well.  I'm just commenting here in the interest of ensuring it does. =8^)\n\nBTW, here's a now dated (a bit over a year old, Feb 2006, KDE 3.5.1) screenshot (1/3 size).  Astute KDE users will likely recognize the layout of the page as modified from one generated by the Konqueror Create Image Gallery tool. =8^)\n\nhttp://members.cox.net/pu61ic.1inux.dunc4n/pix/screenshots/index.html\n\nDuncan"
    author: "Duncan"
  - subject: "Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Will the Dolphin style of navigation become the default file open/save dialog?\nWill Dolphin have easy navigation?\n\nI read this article here: http://insanecoding.blogspot.com/2007/03/file-dialogs.html\n\nAnd it made me a bit disturbed to hear about the planned changes and what Dolphin is like.\n\nWill Dolphin end up getting proper inputting of paths, and not have annoying \"virtual directories\" ?"
    author: "Nach"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Okay, since this question keeps coming up over and over...\n\nThe KDE File Dialog is part of kdelibs.  The icon views and so forth that Dolphin is using are part of Dolphin but anything that would be useful to other programs are being sent down the chain into kdelibs as well.  So anything sent down can be shared between Dolphin and the File Dialogs (and Konqueror, and any other program using icon views, like K3B, for instance).\n\nThat said, the only thing they are complaining about in that blog is the breadcrumb.  And it is indeed *configurable* to have the old style selector in Dolphin as the *default*, and no, the KDE 4 File Dialogs do not use the breadcrumb (nor have I heard any rumblings that anyone plans to make that change).  The keywords here are _configurable defaults_, as KDE tends to be.\n\nThe other thing they are complaining about is Qt4's dialogs (in a non-even-beta-release version of Qt 4.3), but since KDE implements it's own dialogs, Qt's changes here will not affect KDE.\n\nSo really, no need to worry... "
    author: "Troy Unrau"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "He also noted the lack of renaming possibility (at least for KDE 3), which is a feature I found particularly useful (in fact like every basing file management you can do in the file dialog). If it would come with KDE 4 it will be great (and unobtrusive: right-click or F2 press). For everything else, as long as it's configurable it's okay.\n\nThe file dialog is something quite important, given how often it is used, making it feature complete spares time. (I think it is the thing in Gnome I dislike the most, given how unusable it is.)"
    author: "Nanaky"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "I've used a bit of a workaround for the lack of renaming before: choosing properties and then renaming from within that dialog.  I agree though, adding rename on F2 (or whatever the user has configured for keypress) and/or in the context menu would be useful (and less clicks than my workaround)."
    author: "Troy Unrau"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Well, F2 would be good shortcut to support, but your workaround is the exact same number of clicks as if we had a rename action in the right click menu.\n\nIn both cases it is:\nRight click the file\nChoose Properties/Rename\nType the new file name\nHit enter/Click Ok.\n\nThe file name is highlighted by default when you open properties, so its not any slower."
    author: "Leo S"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Placing rename as a top-level function in the open/save dialog is mixing concepts and growing context menus while not providing anything extra, as troy showed you can still rename if you need to.\n\nThe thing is, while you are working in a file save dialog filesystem manipulation is not one of the things that is on the feature list for almost all cases.  If you want to do filesystem manipulation, start your Dolphin to do so.\n\nPeople that cry 'feature creep' or 'bloated' tend to point to the amount of features available in a given UI. Getting the amount of features right is a case of balance.  And discussions we had over the last 2 years made us decide that the balance is kept by not providing filesystem operations in the file dialog itself.\n\nHope that explains it."
    author: "Thomas Zander"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "What if I want to save a file with a specific name, but a file with that name already exists that I want to keep? Or what if I have just created a new folder and want to change its name? Both of these circumstances are helped by the presence of a top level rename option. Yes, I could go via a Properties... dialogue, but they seem to be fundamental enough to the operation of saving files that it deserves its own entry."
    author: "Jono"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "You do have a point here, imho. I use the properties dialog for renaming, and just being able to use F2 or a click to rename a file would make sense. Having to start konqueror/dolphin, go to the location in the filedialog, just to rename a file - not really efficient. Besides, you can create new folders, why not rename a file?"
    author: "superstoned"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Like I already posted, your aversion to the Properties option is purely psychological.  It is the same speed as a dedicated rename action.  Think about the actions involved.  Try it.  \n\nClick to rename is a horrible feature in windows.  Ever watch a newbie trying to double click?  Half the time they rename by accident.\n\nF2 would be nice to have though."
    author: "Leo S"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-23
    body: "The problem with the Properties dialog is that it is not obvious that you should open it if you want to rename a file.  It already occurred that I wanted to change a filename in a dialog and then it did not pass my mind to have a look in the Properties dialog for that feature (by the way, thanks for pointing it out).\n\nI agree that click to rename is a horrible feature.  Failing to double click properly is something that happens all the time to me, and I am double clicking already for more than ten years.  No need to say that I hate double clicking and programs that force me to double click."
    author: "Vlad"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Was it the usability experts that said to remove these features?   Or was it something random that you decided?"
    author: "John Tapsell"
  - subject: "SCREW the simplicity attitude! Go for EFFICIENCY!"
    date: 2007-03-22
    body: "Well, I always thought KDE was the UNIX desktop from the professionals for the professionals, and we very much expect filesystem manipulation from the file dialogs. Heck, even Windows has it, screw the 'rather not confuse the user mentality'. I love KDE because it lets you do things the way you expect them to work, and I and many others (http://insanecoding.blogspot.com/2007/03/file-dialogs.html) do expect a renaming in the file dialogs to work. Troy's workaround is just that, an inefficient glitch and not the solution.\nAll people on the KDE team please note: do not sacrifice any power features for an assumed 'ease of use' for the masses, many avid KDE users who promoted it for years are very pissed off by moves like this. I for one am, and Aaron 'knight of ui' himself has first induced this feeling by acting like a gnome robot on this: http://bugs.kde.org/show_bug.cgi?id=96570\n\nQuotation from the first link:\n\"I always found that feature useful if you wanted to save a file as the same name as something existing, but wanted that old file to be backed up. Perhaps some KDE developers have been spending too much time with GNOME/GTK devs.\nSpeaking of which, I've been previewing some stuff for KDE 4. Now while I have no idea what the final product would look like, some changes as they currently stand are a bit disturbing.[...]\nI don't know why KDE devs are adopting stupid GNOME ideas, or taking a step backwards to design mistakes and oddities from Windows, but I sure hope someone knocks some sense into them soon.\n[...]\nThe copying of stupidity is uncanny. It seems they removed features and changed defaults to make it resemble GTK more, for some absurd reason.\n[...]\nI don't know what's becoming of KDE and Trolltech these days, they seem to be taking the bad from GTK/GNOME and throwing away their own good technology.\n[...]\nI can't even begin to describe what a major step backwards it is. What happened to the sanity? Where's the intelligence? Where's all the good stuff? Why am I looking at garbage from a lesser API, in the best cross platform one available?!?\""
    author: "eMPee"
  - subject: "Re: SCREW the simplicity attitude! Go for EFFICIENCY!"
    date: 2007-03-22
    body: "Chill :) \n\nWe have no plans to remove any features from the file dialogs."
    author: "Troy Unrau"
  - subject: "Re: SCREW the simplicity attitude! Go for EFFICIENCY!"
    date: 2007-03-22
    body: "cant chill right now have a very important exam tomorrow X==P\nanyways what i said not only applies to file dialogs but everything in kde, that's why i quoted out of context ;)"
    author: "eMPee"
  - subject: "Re: SCREW the simplicity attitude! Go for EFFICIENCY!"
    date: 2007-03-23
    body: "You know, KDE4 is not even Alpha yet. For these detail levels, a review is not yet appropriate, I believe. Arguing about what's not yet there is NOT the point of the previews."
    author: "Debian User"
  - subject: "Re: SCREW the simplicity attitude! Go for EFFICIENCY!"
    date: 2007-03-26
    body: "Yes it is.  Better to talk about stuff now than when things are set in stone.  Reminds me of the Deus Ex 2 dev cycle.  There was stuff I didn't like and said so, and people told me to chill, it wasn't done yet.  When the demo came out, the same stuff was there and I expressed my dislike and (other) people said \"I didn't hear you complain about that (loss of) feature when it was talked about before the demo.\"  Thing is, by then it was too late.  \n\nKDE4 is looking more and more like Deus Ex 2, with stuff being cut out and people saying not to knock an alpha.  Yeah, like any changes people might like will magically happen before final when development up to this point indicates strongly that they won't.  I'm just ignoring Dolphin for now as it just gets me too worked up.  I'd rather not ignore KDE4, but I suppose I should do that too."
    author: "MamiyaOtaru"
  - subject: "Re: SCREW the simplicity attitude! Go for EFFICIEN"
    date: 2007-03-23
    body: "I admire your dedication to calm thigs down. :-D\nOne thing I'd like to point out : it seems to me that every feature that's in KDE is there because some user at some point requested it and because many users actually like it. That's open source after all.\nSo when you decide that any type of feature is not right because it's not consistent with a \"concept\" (OK, I know that no change is planned, I speak in general ;-)), you'll sure to piss off many users.\nSo it really seems to me that any change that may impact the way people work should be pondered and not really discarded just because of \"concepts\". There are already a lot of desktops with \"concepts\" and this usually means you like them or not because you digg in their concepts or not.\nI think that a lot of people liked KDE because the whole concept seemed to be : \"Do as you like it\". So when there is a feeling that a way of working, considered the best for unclear reasons that look more like personal likings than argumented and statically supported choice, is forced down the users, it is not surprising to see uproars. \nOf course, many posts are too harsh or make too many asumptions (mine included), but when a real uproar occurs, it should be taken seriously, I think.\nAnyway, I still like to thank all the KDE developpers for their involvement in this projet, I'm sure that in the end, the whole thing will be great. I just hope there won't be too much friction in the process."
    author: "Richard Van Den Boom"
  - subject: "File dialog (and manager) context menus"
    date: 2007-03-25
    body: "This is one of the few things I've found myself missing from MSWormOS (which as you can tell I don't miss very much!).  Not just renaming, but fully working context menus (delete, run, move, rename, specific file-type sensitive actions like extract, etc), both within the file dialog, and within Konqueror itself, when clicking on the directory one is actually /in/ (as opposed to something in it).\n\nBack on MSWormOS, one \"power user\" trick I used to use /very/ frequently, was popping up the Open/Run dialog, then hitting the browse button, and using what was in effect a mini-file-manager-app.  This was faster and easier than opening up a full Explorer (or alternate file manager), just to view or alter some facet of a file or directory (name, attributes, maybe just get a graphical dir listing complete with icons) somewhere, or even move or open/run it, not from the run dialog, but directly from the fully functional context menu directly in the file dialog.\n\nThis doesn't seem to work in KDE (3), but I'd sure like it to. =8^)\n\nSimilarly, when I right-click on a blank spot in a Konqueror dir listing, I expect to see the context menu for the dir itself, NOT some item that happens to be selected, possibly by default as the first item in the listing, even if it's not even displayed because it is scrolled out of the display window somewhere.  If I want the context menu for an item, I will right click on it, using shift and ctrl if necessary to get the menu for a compound selection.  If I simply right-click on a blank space in the listing, that's NOT clicking on an icon, but on the displayed directory itself, so that's the context menu I want, NOT the one for some icon that might not even be on screen!\n\nOf course, I'm only using KDE because it's the desktop that already best meets my needs for power and customizability, so these complaints are only in the interest of making the best even better, but I still hope the functionality can make it into KDE 4. =8^)\n\nDuncan"
    author: "Duncan"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Just a quick question: is it possible to run a search for files/folders within Open/Save dialogs?\n\nSort of like Suse has integrated Beagle into Konqueror search field.\n\n"
    author: "Darkelve"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Yes, at least using KIOslaves. Maybe they will improve the dialog and make it more visible, though."
    author: "superstoned"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "Nepomuk (under a new name, not sure which name will be the final name) and strigi should do this.  However, they are not 100% implemented yet.  This would be an ideal dialog to take advantage of searching features, especially for the Open dialog.\n\nCheers..."
    author: "Troy Unrau"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-23
    body: "If you install the kio-locate IO slave, you can type locate: in all file dialogs. Of course you'll have to install and set up locate, not all distributions do this by default."
    author: "Morty"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "One click in the Control Center enables Konqueror instead of Dolphin as the default file manager. Not reason to worrry for those who do not like Dolphin the way it is when KDE4 gets released."
    author: "mutlu"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-26
    body: "Except that using Konqueror means not getting any of the fixes Dolphin was intended to bring vis a vis having the same app for web and filebrowsing (huge options dialog, mixed bookmarks, unable to have differnet sized toolbars etc etc).\n\nIt's like saying \"We'll fix that!  Don't like the other changes it brings?  Don't use the fixed then!\"  Thanks."
    author: "MamiyaOtaru"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-26
    body: "damnit I told myself I was going to ignore Dolphin.  Oh well, as long as I am replying to myself, the line should be \"We'll fix that! Don't like the other changes it brings? Don't use the fixe<b>s</b> then!\"  Fixes, not fixed.  need edit.  "
    author: "MamiyaOtaru"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-22
    body: "My god, thank you for posting this.  I pray that the people pushing Dolphin and GTK style file dialogs will have some sense knocked in to them."
    author: "KDE User"
  - subject: "Re: Will Dolphin be fixed?"
    date: 2007-03-26
    body: "Dolphin is NOT a gtk style file dialog.  No one is pushing a GTK style file dialog.  \n\nWho would have thought a SINGLE widget would get so many people all freaked out (that you can change to the normal line edit!)."
    author: "Sutoka"
  - subject: "User Interface Suggestions"
    date: 2007-03-22
    body: "Here are some user interface suggestions:\n\n1) Don't use ellipses (...) to signify overflowing text. Instead, progressively fade the final three characters. This is already implemented in Kicker's taskbar for window titles that are too long (see attached screenshot). It would be nice if the fade-out approach was automatically available on all GUI items whose text might overflow and get cut off: drop down menus, tabs, text fields (URLs, search boxes), tooltips etc.\n\nFading the last couple of letters is analogous to but more space-efficient than the ellipses (...). Ellipses also have a completely different meaning in a menu item or button label: to tell the user they must provide information before completing their task (see\nhttp://weblog.obso1337.org/2006/kde4-hig-request-1-the-ellipsis/). So we should avoid using ellipses to signify shortened words so as not to dilute its original meaning.\n\n2) Dolphin: The Usability Team should take a look at the interaction of the Bookmark panel and the main panel(s) to the right. When I saw \"Home\" highlighted purple in the bookmark, I expected the contents of the Home Folder to be displayed, instead of Home/dl/temp.\n\n3) In the Progress Manager search field (and for all other text fields, for that matter), there should be a dropdown containing previous searches. The magnifying glass should be a button to the left of the field that users can click. Some users aren't used to the command prompt, so you can't expect them to know that pressing ENTER executes the text -- they expect a clickable button.\n\n4) Progress Manager: Is there really a need for a horizontal separator between the Configure button/Search field and the In Progress/Finished tabs? Same for the vertical separator between Configure button and the Search field.\n\n5) KWord: Given that there is plenty of vertical space on the toolbar, why not make the Bold, Underline, Italic, Strikethrough buttons two-tiered (ie. arranged on top of each other using two rows)? Same for all the other buttons that don't have text captions.\n\n6) Kword: Some of the separators seem redundant, like the one above \"Scripts\" on the right side. Also, why are there are there two types of separators (dotted and straight-lined)? And why are there fat black bars on the left and right side of KWord's text area?"
    author: "Bill"
  - subject: "Re: User Interface Suggestions"
    date: 2007-03-22
    body: "\"Also, why are there are there two types of separators (dotted and straight-lined)? \" You mean in the toolbar? \nThe dotted ones indicates a toolbar that can be moved and re-docked or floated elsewhere on the screen( it's like the toolbar's \"grippy\" handle)\nThe straight ones are separators between items in the SAME toolbar.\n\nThat said, I'm sure there's still some refinement to the number/layout of the toolbars ( and hopefully the usabilty team will get involved, so we'll see some consistency between apps....even if only between the KOffice apps). \n\nAs for stacking the toolbars - that may indeed be possible with docking toolbars (can't try it just now )... I don't know if the toolbar button size / text display options can be set on a per-toolbar basis.\n\n"
    author: "TonyB"
  - subject: "Re: User Interface Suggestions"
    date: 2007-03-22
    body: "I agree with your thoughts about ellipses. Not only would that be very useful (ellipses would only be used for menu entries that require more input), but it'd also look very slick."
    author: "Roderic Morris"
  - subject: "ellipsis"
    date: 2007-03-22
    body: "You wrote;\n \"Ellipses also have a completely different meaning in a menu item or button label: to tell the user they must provide information before completing their task\"\n\nActually; the meaning there is the same as the meaning here.  It signifies there is information missing."
    author: "Thomas Zander"
  - subject: "Re: ellipsis"
    date: 2007-03-22
    body: "but you cannot know _what_ info is \"missing\"\n"
    author: "ac"
  - subject: "Re: ellipsis"
    date: 2007-03-22
    body: "The difference is, whether the UI is missing information that you should provide, or if you are missing information that the UI should provide. IMHO it's quite a fundamental difference."
    author: "zonk"
  - subject: "Re: User Interface Suggestions"
    date: 2007-03-22
    body: "The fadout effect was the first thing I've tried to switch off when I started using the new KDEs, and the only thing I couldn't achieve. I feel that it stresses my eyes, as I'm struggling to read the fade out text. Wouldn't it be possible to have this effect configurable?"
    author: "Tiberiu Ichim"
  - subject: "Re: User Interface Suggestions"
    date: 2007-03-22
    body: "> I'm struggling to read the fade out text.\n\nActually the fade out effect lets you see more text than the ellipses do. With ellipses, the last three letters are completely replaced with three dots, which are not very informative. I'd rather have three slighly faded letters than no letters at all.\n\n> Wouldn't it be possible to have this effect configurable?\n\nYup, I agree that it should be configurable."
    author: "Vlad"
  - subject: "Re: User Interface Suggestions"
    date: 2007-03-22
    body: "I would be very interested to hear what options are currently on the table for point 5. The mix of text under icon and icon only for toolbars is very bad aesthetically and from a space efficiency point of view. I've not seen any changes to improve this layout since KDE4 screenshots started coming out, so I'm wondering if anything is planned yet?\n\nI like the idea of toolbars of different heights existing simultaneously, this is not possible with KDE3. You could save a lot of space by stacking 2 thin toolbars alongside a thick one. \n\nOf course I would turn off the text under icons anyway ;) If I had to have text then text to the right is a lot better and saves more space in my opinion. Gwenview defaults to a mix of these two styles quite nicely with the text only on the less obvious icons."
    author: "Matt"
  - subject: "Re: User Interface Suggestions"
    date: 2007-03-23
    body: "Compliment for your well done comments. I esp. like the fading stuff. Now that you said I noticed that my Kicker does it. It's really very intuitive in meaning. That's great usability. \n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Ellipsis vs. fading"
    date: 2007-03-23
    body: "I truly hope fading of text is NOT used (or an option is provided to revert to ellipsis).  The fading text on the task bar makes it look like my monitor is smudged in multiple places.\n"
    author: "StormReaver"
  - subject: "Re: User Interface Suggestions"
    date: 2007-03-24
    body: ">> 2) Dolphin: The Usability Team should take a look at the interaction of the \n>> Bookmark panel and the main panel(s) to the right. When I saw \"Home\" \n>> highlighted purple in the bookmark, I expected the contents of the Home Folder \n>> to be displayed, instead of Home/dl/temp.\n\nWhat about the overall placement of the bookmarks? They seem to be wasting quite a bit of space in their own panel; why not move them up in to the main taskbar next to the view change options, or in with the information tab like in Windows Explorer?"
    author: "Christopher"
  - subject: "Re: User Interface Suggestions"
    date: 2007-03-26
    body: "Browser sidebars are ugly usability wise\n\nBookmarks should be in the main window or tab, same goes for history"
    author: "Andy"
  - subject: "Oxygen icons?"
    date: 2007-03-22
    body: "The icons shown in Dolphin are just bad. They dont't workat all in that small size.\n\nThat dog-eared paper symbol for files other than text based documents is just wrong. At least remove the paper symbol for the multimedia type files. Where is Everaldo? (And Mosfet?)"
    author: "reihal"
  - subject: "Re: Oxygen icons?"
    date: 2007-03-22
    body: "Mosfet never drew icons - he did widget themes and so forth, in the KDE 2.x series.  Coding, not drawing.  He had gotten married and moved on, as far as I know. I don't know about Everaldo, but the crystal icon set has not been updated in a long time.  (It still lives in KDE 4 in the kde-artwork module for those who prefer it... Actually, the hi-color icon theme is still there from KDE 2.x as well, if anyone cares :P )\n\nKDE has a constant but slow turnover of developers, artists, translators, and so forth.  The biggest causes being university graduation; marriage/children; and new jobs with high time commitments. Uni graduation is the worst though, as many of our most prolific coders are also students. Unless they get jobs at Trolltech, graduation usually seriously hinders their KDE time. I myself may fall prey to that one within the next while :)\n\nAnyway, you can resize the icons to fit your needs.  In the Dolphin config dialog, there's a slider for icon size... the icons are SVG, so they look decent at just about any size you'd like, except perhaps really small sizes :)  Still a work in progress though folks :)"
    author: "Troy Unrau"
  - subject: "Re: Oxygen icons?"
    date: 2007-03-22
    body: "I agree. The icons in these screenshots look positively horrid. They're much too detailed for a small icon size."
    author: "Beavis Christ"
  - subject: "Re: Oxygen icons?"
    date: 2007-03-22
    body: "\nI think they look fine at this stage.  I like the photo-realistic\ndirection it is going.  Perhaps there will be further improvements\nas we approach KDE 4.0 that may please more folks.  As a programmer,\nI don't like to be overly critical of works-in-progress.\n"
    author: "Henry S."
  - subject: "Re: Oxygen icons?"
    date: 2007-03-22
    body: "Indeed, I like'em too, and they're still working on them. What I don't like, though, is the shadow: it sometimes looks really silly and out-of-place. Check the dolphin screenshot, where there is a shadow beneath the little button which can turn the breadcrump into a lineedit... Looks really weird."
    author: "superstoned"
  - subject: "Re: Oxygen icons?"
    date: 2007-03-22
    body: "Not intirly oxygen foult there, kde tends to miss use icons were they are not suposed to be, like action icons as mimetype icons and so on. This is beeing worked out. "
    author: "pinheiro"
  - subject: "some criticism here, 2--"
    date: 2007-03-22
    body: "first of all, glad the code is coming along and we'll finally have the desktop revolution by late autumn, thanks for anyone contributing to this great project.\nI have some issues with the present state as shown in the screenshots here, one of them, yes, being the icon set. Current Oxygen icons\n- do have too little contrast around the *border*. There is some sort of shadow thing in the SVG files, which would have to be increased quite a bit to make the icons easily recognizable using light window styles. Crystal is great not only because it has colorful icons with a nice color palette but _great contrast_ of the icons (mostly)..\n- the folder icon looks mashed up in the tree. The KDE3 one is much much better, have a look and see for yourself someone.\n- the view angles are problematic for me. Some icons (printer, scanner,...) need more 3d imho (=> tilt them a bit?), some less (folder icon..). That is of course only my highly subjective point of view.\n- The screenshot of the progress manager shows one most ugly thing: the 'file://' in front of local paths. That is WAY bad. Please make it go away everywhere by default. We never needed it the decades before and it is not a feature. Them escaped sequences (%20) look not good too.\n- Considering microtypographic standards, text kerning in Koffice is just not good enough right now. Perhaps the output of LaTeX would be a good reference target to aim for, kinda.\n\nExcept from the last, those are all minor things though and I know they'll be fixed when stuff gets on air. Thx for that already ;)"
    author: "eMPee"
  - subject: "Re: some criticism here, 2--"
    date: 2007-03-22
    body: "Agree with all! That is why everyone is talking about the icons being washed out.\n\n"
    author: "Manabu"
  - subject: "Re: some criticism here, 2--"
    date: 2007-03-26
    body: "The kerning is indeed as awful as it always was.  The standard response was always \"it will be fixed with Qt4.\"  Is the answer actually \"never\" then?  "
    author: "MamiyaOtaru"
  - subject: "Keep small icons, but better ones."
    date: 2007-03-22
    body: "The icons sizes are more sane this time. I don't like big icons, and they are realy unusable in 800x600 screens. Remember: these screns are 10% of all computers surfing the web, and probably more if you count the ones not connected. I was in one of those some time ago, and Windows 2000, and even Windos XP desktops were much more usable than KDE and especialy Gnome. I hope it at least don't get worse in KDE4."
    author: "Manabu"
  - subject: "Re: Oxygen icons?"
    date: 2007-03-22
    body: "Ha.\n\nSo I see many comments here that means nothing to me. Really.\n\n@reihal: \nhave you tryied the theme or you are just guessing that the icons shown there won't resize well?\nFYI there are small version of almost every icon.\nEveraldo is here: www.everaldo.com\n\n@all:\nthe size shown there is 32x32. Dolphin is loading an action icon called folder.svg instead of the real folder in \"places\" directory.\n\n\n\n"
    author: "David Vignoni"
  - subject: "Re: Oxygen icons?"
    date: 2007-03-23
    body: ".\nEveraldo is here too:\nhttp://www.kde-look.org/content/show.php/Crystal+SVG?content=8341\n\nEverybody and his dog supports Crystal SVG icons, except the KDE developers.\nIt's a mystery.\n.\n\n"
    author: "reihal"
  - subject: "Re: Oxygen icons?"
    date: 2007-03-23
    body: "For a while Crystal did not have a license at all. Secondly, there were problems in getting the sources for the icons. \nI fully support the switch from Crystal to Oxygen (which I like better anyway)."
    author: "Luca Beltrame"
  - subject: "Thanks for the article!"
    date: 2007-03-22
    body: "Thanks for another great article.\n\nAnd I must say... I love the job progress thingy"
    author: "Darkelve"
  - subject: "Do samething with grey colour."
    date: 2007-03-22
    body: "Please I hate grey could you... i don't know mix it with white? "
    author: "misiaczkowski"
  - subject: "Re: Do samething with grey colour."
    date: 2007-03-22
    body: "UI colors are configurable in the Control Center. Don't like it - use a different color scheme.\n\nPozdrawiam :)"
    author: "zonk"
  - subject: "KDE4, quickly!!"
    date: 2007-03-22
    body: "Please don't waste your time with dolphin!\nDon't waste your time with win$32, win$16, win$47!\nDon't waste your time with \"new\" icons!\nDon't waste your time with \"better\" interface for kcontrol!\n\nWe enjoy ad want your wonderful work!!"
    author: "Basic user"
  - subject: "Re: KDE4, quickly!!"
    date: 2007-03-22
    body: "You have a lot to learn about how KDE and open source communities work. \n\nProgrammers work on whatever they feel like working on: in this case, Dolphin.  Artists work on artwork, but they work on what they feel like working on: in this case, icons.  You cannot simply assign them to another task - KDE is not a corporation with a bottom line and managers and so forth.\n\nSo not a single developer is wasting their time, as they are working on what they prefer to work on.  If they weren't, they'd be doing nothing.  And given the choice between the two of them..."
    author: "Troy Unrau"
  - subject: "Re: KDE4, quickly!!"
    date: 2007-03-22
    body: "There is a lot of work that MUST be done before kde4 can be released.   While there is some talk that maybe kde 4.1 libraries won't be binary compatible with 4.0, everyone agrees we don't want to.   (but we may discover some things that need to be redone after 4.0)\n\nThere is still a lot of known library work that must be done.   The people doing that work are not the people working on things like dolphin, win32, mac, icons, or userinterface.    Too many chefs in the library code would spoil the broth, and anyway most developers are not ready for library work (which is harder than regular development).    That isn't saying we don't need more people working on the library, we do  - but the people working on the \"other\" stuff are not necessarily the people who would be good in the library.  Icon work is something for artists, not developers - both skills rarely exist in the same person.   \n\nAlso, library development requires that everything be used.  Dolphin (and other things that can change latter) gets help from library developers, because the library developers do something that seems interesting and they want to see it work.   When you write a new library interface useful for eye candy, you want to see it, so you stop the library work for a bit to make some application use that eye candy.\n\nLast, most developers are not paid for any particular work.   So if they want to do something that isn't important for kde4, we can't make them.  "
    author: "Henry Miller"
  - subject: "Re: KDE4, quickly!!"
    date: 2007-03-22
    body: "That's trolling. "
    author: "David Vignoni"
  - subject: "Re: KDE4, quickly!!"
    date: 2007-03-23
    body: "No, it's paranoia.\nFear of abandonment.\n"
    author: "reihal"
  - subject: "Some suggestions"
    date: 2007-03-22
    body: "Hi,\n\nIt is wonderful to see the result of everyone's hard work. Thanks for the interesting reading Troy.\nIt's great to see that you actually can contribute and affect, even if you don\u0092t know programming or have the title \"usability expert\". I am just a normal user, but here are some of my suggestions:\n\n1. Eye candy for logout screen\n\nI like the updated logout screen. However, I think it would look even nicer with some small changes:\na) White (transparent) border instead of the current gray and\nb) a shadow.\nHere is the result: http://img160.imageshack.us/img160/6779/img1nl0.png\nThe left one is the \"current\" one, and to the right you can see my proposal.\nI think the dots to indicate the focused button look out of place, but I guess it has lower priority and (hopefully) will be fixed later.\n\n2. Krunner \"alt+F2\" interface\n\nI like the mockup of Leo Spalteholz on KDE-look. You can find it here: http://www.kde-look.org/content/show.php/TBC+-+Run+Dialog+Mockup?content=53576\n\nWhy? Because it feels less \"cluttered\", and you see directly what the commands do.\nI have made a new mockup, based on Leo's. Please note two things: I haven't included the buttons, and the fonts look horribly.\nhttp://img262.imageshack.us/img262/9200/img2lb9.png\nThis way, with the information divided into two lines, I think you get a better overview. Again, please ignore the ugly fonts. \nA Horizontal scrollbar should in my opinion be avoided and (almost) any cost.\n\nAnd I wonder if the text \"Enter the name of an application ---\" [let's call this the help text] really is necessary? If it is, a solution could be to show this text in the \"result\" list below the text field, if the user hasn\u0092t typed anything in the text field.\nThe problem is what's shown in the list by default, when the text field is empty? I think showing the last 5 used commands [now called history] or something similar would make sense.\nIf that's the case, maybe you could shown [help text] by default, and if the user presses the up/down arrow keys on the keyboard, then show [history]. If s/he starts typing, show the result as always.\n\nI also think that the Launch and Cancel should look like buttons.\n\n3. Process Manager\n\nI hope the interface gets some love (too much information / takes too much space right now in my opinion), but it's really good to see the improvements!\nAnd a comment to Bills' suggestion,\n>> The magnifying glass should be a button to the left of the field that users can click. Some users aren't used to the command prompt, so you can't expect them to know that pressing ENTER executes the text -- they expect a clickable button.\nI don't know if it's a search field of filter field, but it\u0092s probably the latter. Then it filters as you type, no need for a button.\n\n4. The :::: Toolbars ::::\n\nSorry, I don't know what to call them. But they appear in e.g. Dolphin\u0092s \"Bookmarks\" panel, \"Folders\" etc.\nFirst, I really dislike the dots. I know they indicate that they are movable, and that it depends on the style you're using.\nAnd what does the \"Restore\" button do? If it just undocks the panel, then I see no reason for it - you can as well just drag it, can't you?\nI like Adobe's solution, for example in Premiere:\nhttp://www.thg.ru/video/sony_hdr-hc1e/images/sony_premiere1_big.jpg\nThey appear as tab-like things, and you can group panels if you want. I don't know if the grouping is necessary, but it could be useful in for example KOfice, I think.\nNot quite possible to see this in KDE4 though. However, can\u0092t you make the drag able space smaller, for example have the text left aligned and the dots just to the left of the text?\n:: Example                          [x]\n\n5. Some side notes\na) The folder icons look like they are floating - maybe remove the shadow underneath?\nb) Bill (again) wrote about ellipses. I like the fade effect, but it's sad that it distracts some people.\n\nFinally, I just want to say: thanks for reading this, and keep on with the great work! "
    author: "Mogger"
  - subject: "Re: Some suggestions"
    date: 2007-03-22
    body: "Thanks for all the constructive feedback :)\n\n1) The logout screen is composited on the fly based on a background SVG and some button SVG's - so it's a really simple change to make.  What I suggest is that you grab the SVG's in question, and play with them.  I'm sure the artists would appreciate any improvements.\n\nThat said, the button SVG's got updated a few hours after I took my screenshot.  Not sure how the new one looks, side-by-side...\n\n2) The KRunner interface is still seeing a lot of work, and what the still shows is not the whole interface.  The list is going to be replaced (eventually) by something better :)  The mockup looks nice though: maybe you should pop into #plasma on irc.kde.org and make sure that they see it :)\n\n3) It is indeed a filter field...\n\n4) I think the dots and so forth that you are seeing in my screenshot are a very work-in-progress sort of appearance.  Grouping is available in KDE 4 as well, and it'll automatically degenerate into tabs.  So this then becomes a matter of style.  The default style will be changed before KDE 4.0 launches...\n\nThere's still much work happening on this widget, and I notice changes happening on a week-to-week basis."
    author: "Troy Unrau"
  - subject: "Re: Some suggestions"
    date: 2007-03-22
    body: "Thank you for your response. I know that these are all work in process, just thought it is better to point something out early, than complaining when the work's almost finished. :)\n\nAbout Krunner, I read about some \"icon parade\" that is going to replace the list view. However, when I read \"The interface is not yet final, but it's getting closer to completion.\", I thought that this view will be the one featured in KDE4.\n\nI would like to help with the development, however, I simply haven't got enough time. The series \"The road to KDE 4\" is really great, now I can follow (at least a part of) the development without compiling and hacking stuff. "
    author: "Mogger"
  - subject: "Re: Some suggestions"
    date: 2007-03-22
    body: "thanks for these comment !\n\nFor the 1), i think what you did is better than mine. I'll update soon. thanks.\n\n "
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: Some suggestions"
    date: 2007-03-23
    body: "Hi mogger,\n\n  This is what the process list currently looks like:\n\nhttp://commit-digest.org/issues/2007-03-18/files/designer_processes1.png\n\n  I might even need to add another column for niceness.\n\nI'm welcome to suggestions for making it smaller/nicer.\n\n"
    author: "John Tapsell"
  - subject: "Re: Some suggestions"
    date: 2007-03-23
    body: "Hi John,\n\nI apologize for mixing things; what I meant with \"Process Manager\" is the one shown in the artivle, the job process manager: http://static.kdenews.org/dannya/vol13_4x_kuiserver.png\n\nIt's the Job Process Manager that I think shows too much information at the moment, not the process manager."
    author: "Mogger"
  - subject: "Visual style for KDE 4?"
    date: 2007-03-22
    body: "Hi,\n\nFirst let me congratulate all of you for the great work. It's nice to see how the things are progressing...\n\nBut I have one question about the visual style of KDE 4: which way do you plan to follow, one more clean, MacOS like, or just improve the current style (Plastik)?\n\nOne good example of a clean style is this mockup below. It's very pleasant to look, all the elements are easy to find, there are no divisions between the parts of the window (frames?):\n\nhttp://www.kde-look.org/content/show.php/Kde4+Mockup?content=28476\n\nI really hope that you go in that direction. ;-)\n\nThanks."
    author: "kde.fan.from.brasil"
  - subject: "Re: Visual style for KDE 4?"
    date: 2007-03-22
    body: "I loved that mockup, too - really hoping that kde4 could look just like that. Plastik is great, but what i've seen to that point from kde4 doesn't blow me away... the style of the mockup certainly would. So pleeeease kde-folks, don't forget your promises about \"breathteaking beauty\"!"
    author: "Philipp"
  - subject: "Re: Visual style for KDE 4?"
    date: 2007-03-22
    body: "yes, there will be a new style, and a very good one."
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: Visual style for KDE 4?"
    date: 2007-03-22
    body: "Will the style be cleaner than the ones in KDE 3? By cleaner I mean;\n\n* Less (visible) handles, as is currently possible with the lipstik-style. \n* Less frames. Attached is a screenshot of what I mean when I say frames. In my opinion, they make the GUI seem a bit bloated. "
    author: "AC"
  - subject: "Re: Visual style for KDE 4?"
    date: 2007-03-22
    body: "Will the style be cleaner than the ones in KDE 3? By cleaner I mean;\n\n* Less (visible) handles, as is currently possible with the lipstik-style. \n* Less frames. Attached is a screenshot of what I mean when I say frames. In my opinion, they make the GUI seem a bit bloated. "
    author: "AC"
  - subject: "Re: Visual style for KDE 4?"
    date: 2007-03-22
    body: "Regarding frames, Qt now supports a CSS-like styling mode, which means it's really easy to remove things like frames with very little code.  Making styles much easier to code and look nice... :)"
    author: "Troy Unrau"
  - subject: "Re: Visual style for KDE 4?"
    date: 2007-03-22
    body: "Hip hip hurray!\n\nI Can't wait! ;)"
    author: "T1m"
  - subject: "Very important"
    date: 2007-03-22
    body: "Really things are progressing, and the whole team is putting a lot of effort. But I was wondering about digikam, will it be present in KDE4? It is really one of the best app of kde. "
    author: "Landolsi"
  - subject: "Re: Very important"
    date: 2007-03-22
    body: "I have seen some news about digikam, particularly regarding sharing image manip plugins between it and a few other applications.  I'll add it to my list of topics to feature, but can't guarantee I'll feature it anytime soon :) I have quite a lineup :)"
    author: "Troy Unrau"
  - subject: "Re: Very important"
    date: 2007-03-23
    body: "I don't think a any maintained is not going to be ported to KDE4.\n\nBut before a stable release is done, don't expect everything to be ported already. Not every application developer will target things he doesn't himself get easily. \n\nKDE4 will certainly also run KDE3 applications. \n\nThat said, my prediction is that Digikam will certainly be ported. I agree its one of the best applications there are for KDE.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Very important"
    date: 2007-03-23
    body: "Looking at the commit comments at: http://websvn.kde.org/trunk/extragear/graphics/digikam/\n\nIt seems that digikam is already in the process of being ported.  It can benefit from a number of KDE and Qt 4 features... (solid, kipi, and more...)\n\nKDE 4 will indeed still run KDE 3 applications /assuming you have the KDE 3 libraries still installed/ and that there are no configuration file problems :)"
    author: "Troy Unrau"
  - subject: "Re: Very important"
    date: 2007-03-23
    body: "These commits is not a real port, but just a source code polishing before to port implementation. We have planed to do it later 0.9.2 release, during this summer.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Now with Less Ugly?"
    date: 2007-03-23
    body: "I normally don't comment on such stuff but this time you're asking for it:\nhttp://static.kdenews.org/dannya/vol13_4x_kword.png\n\nWTF? The font handling's been improved? Really? Could have fooled me.\nThe kerning's still a disaster:\nThis sc-ript d-emonstrat-es...u-sing the Pyth-on...and so on.\n\nIn fact the rotated text telling us about the improved font handling is jumping all over the place.\n\nIf there's an improvement compared to the last screenshot, I fail to see it.\n\nOnce again, I understand that development screens won't be perfect but in this case, if you really believe that this is \"greatly improved\", you're either talking strictly about internals or you're delusional. (Or that last screenshot was vastly more hideous than I can remember :P)\n\n"
    author: "ac"
  - subject: "Re: Now with Less Ugly?"
    date: 2007-03-23
    body: "The last screenshot was worse than your remember.  I agree it's not yet perfect, but at least in this one they look somewhat aligned :)\n\nThat said, it wasn't the sole point of the kword screenshot :)"
    author: "Troy Unrau"
  - subject: "Re: Now with Less Ugly?"
    date: 2007-03-23
    body: "After looking at the old screens once more, I'm still not convinced (they're a bit blurry, perhaps that hides the imperfections =P) but I'll take your word on it. =)\n\nOh and Troy, thx for all the great articles. "
    author: "ac"
  - subject: "Re: Now with Less Ugly?"
    date: 2007-03-23
    body: "How many times will we hear that kerning has been improved, only to find out that it is still no good? Isn't there a correct way of doing it? Can there be only incremental hacks that don't seem to approach the target?\n\nI would like to see \"kerning now correct\", not \"kerning now better\" once again. Please explain why I am wrong!"
    author: "Bob D"
  - subject: "buttons incorrectly sized"
    date: 2007-03-23
    body: "and heres me thinking the \"Ok\", \"Apply\" and \"Cancel\" buttons would be the same size in KDE4, the openusability fails."
    author: "Todd"
  - subject: "Re: buttons incorrectly sized"
    date: 2007-03-23
    body: "Umm, that has nothing to do with anything that the usability folks are doing... it's simply the way Qt handles the buttons by default.  That said, is there any particular reason why they should be the same size?"
    author: "Troy Unrau"
  - subject: "Re: buttons incorrectly sized"
    date: 2007-03-23
    body: "If Windows, OSX, and Gnome can do it, why not KDE?"
    author: "Todd"
  - subject: "Re: buttons incorrectly sized"
    date: 2007-03-23
    body: "I didn't say it couldn't be done. I asked why it should be done..."
    author: "Troy Unrau"
  - subject: "Re: buttons incorrectly sized"
    date: 2007-03-24
    body: "Confusion maybe? The user interprets size to have meaning?\nWhy shouldn't it be the same size?"
    author: "reihal"
  - subject: "Cool!"
    date: 2007-03-23
    body: "It's really wonderful. \nAnd nice to see how hard working there on kde4. I can't wait, speed up guys!:)"
    author: "MetaMorfoziS"
  - subject: "What about the state of art of Plasma?"
    date: 2007-03-23
    body: "Hi  Troy,\nThanks for answering my question about digikam.\n\nI have another request, I kept looking at some early screenshots of plasma or some plasmoids, but I found none. What is the state of art, I mean the progress state, of these important new parts of kde?"
    author: "landolsi"
  - subject: "KWord: Kerning or hinting not right?"
    date: 2007-03-23
    body: "Hi, I think the space between \"c\" and \"r\" in word \"script\" is way too big. Same for \"l\" and \"a\" in \"languages\". I wonder whether this a font-specific issue, or a Qt issue?"
    author: "Boris Du\u009aek"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-23
    body: "Again, probably fonts.  *sigh* I use the default fonts whereever possible when I take my screenshots, but the defaults vary greatly from distro to distro... you can probably blame *ubuntu for my defaults.  It really does look a lot better when using the webfonts released by microsoft, but of course I can't use them in my screenshots since no distro is legally allowed to include them by default.\n\nFonts are in sad shape, and there's been a few discussions among the kde artists about making KDE depend on a few specific fonts, to ensure that distros ship decent fonts... However there are many issues to discuss still, including licensing, where these fonts would live, whether we can ship fonts from other sources, and which fonts fit these guidelines but don't look like crap :P\n\nDiscussion is ongoing, but it's not likely something to happen for KDE 4.0 (later in the 4.x series, maybe...)  \n\nIf you run a shop that produces fonts that look good and include all of unicode, and would consider licensing it under an opensource license, the whole free desktop world would bake you cookies :)"
    author: "Troy Unrau"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-23
    body: "What about Lbertine Open Fonts (http://linuxlibertine.sourceforge.net/)? That is a high quality open font."
    author: "mata_svada"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-23
    body: "That's indeed a really good font -- very impressive."
    author: "Boudewijn Rempt"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-23
    body: "Yes really it is very cool"
    author: "Landolsi"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-23
    body: "No. I'm sorry to say this, but only Microsoft's Times New Roman works on a screen.\nI found it impossible to use any other font for reading running text."
    author: "reihal"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-23
    body: "> Again, probably fonts.\n\nNot exactly true. In this case I also don't exactly see the MS fonts behaving any better (have to see it before believing it). The fonts just cannot do anything about this issue here.\n\nThe spacing is sometimes wrong due to the screen not having enough dpi and the program having to emulate what will come out of the printer. It has to add or remove some spacing sometimes or the letter on your screen would be way off compared with what you get on paper. And luckily Latin has spaces between letters, imagine what would happen with Arabic or Devanagari with extra spaces in between letters. Take any word processor and type a full line of \"i\"'s, you'll see that the spacing will be different sometimes, depending on font size/zoom, and that will happen with every font you'll find.\n\nI doubt there's a real solution to this. You could either have very fuzzy fonts, or have irregular spacing, or have your program change the glyph shapes to keep the spacing ok. Maybe it could be a bit improved by only allow changing spacing between words instead of glyphs, I don't know. In the end it has to compromise somewhere, and I think only 300dpi screens could \"solve\" this by making the spacing differences invisible...\n"
    author: "Eimai"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-24
    body: "What version of FreeType were the screenshots done in?\n\nFreetype 2.3.2 is the most stable version:\n\nCHANGES BETWEEN 2.3.2 and 2.3.1\n\n  I. IMPORTANT BUG FIXES\n\n    - FreeType  returned incorrect  kerning information  from TrueType\n      fonts when the bytecode  interpreter was enabled.  This happened\n      due to a typo introduced in version 2.3.0.\n\n    - Negative  kerning  values  from   PFM  files  are  now  reported\n      correctly  (they were read  as 16-bit  unsigned values  from the\n      file).\n\n    - Fixed  a small  memory leak  when `FT_Init_FreeType'  failed for\n      some reason.\n\n    - The Postscript hinter placed and sized very thin and ghost stems\n      incorrectly.\n\n    - The TrueType bytecode  interpreter has been fixed to  get rid of\n      most of the  rare differences seen in comparison  to the Windows\n      font loader.\n\n\n  II. IMPORTANT CHANGES\n\n    - The auto-hinter  now better deals  with serifs and  corner cases\n      (e.g.,  glyph '9'  in Arial  at 9pt,  96dpi).  It  also improves\n      spacing  adjustments and doesn't  change widths  for non-spacing\n      glyphs.\n\n    - Many   Mac-specific   functions   are  deprecated   (but   still\n      available);  modern replacements  have been  provided  for them.\n      See the documentation in file `ftmac.h'.\n"
    author: "Marc Driftmeyer"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-24
    body: "Stay away from FreeType 2.3.2 for now, and go directly to 2.3.3 instead when it is released. 2.3.2 has some serious hinting issues. It mentions it on its news page at http://freetype.sourceforge.net/index2.html#release-freetype-2.3.2 -- see this thread http://lists.nongnu.org/archive/html/freetype/2007-03/msg00028.html\n\nTo get an idea how bad it is, see for example this screenshot http://666kb.com/i/amj20dveeaqvvgtx4.jpg\n"
    author: "Eimai"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-24
    body: "Yes that definitely is a problem one would expect to be caught during QA cycles."
    author: "Marc Driftmeyer"
  - subject: "Re: KWord: Kerning or hinting not right?"
    date: 2007-03-29
    body: "Hm. I help out on the DejaVu fonts*, and from what I've seen reported most spacing issues result from renderer bugs; the hinting/kerning for DejaVu Latin characters being largely a solved problem.\n\nWhat really jumped out at me as being wrong in the KWord screenshot is the non-straight baseline on the rotated text. Since the plain text looks fine in that regard, I would guess there is some subtle mis-calculation taking place in the renderer when handling rotated text.\n\nThe screenshot would also be helped a lot by hyphenation to prevent the text from being stretched so much in justification. Though thats an entirely separate issue.\n\n* latest 2.16 RC snapshot, please try it out, report bugs on bugs.freedesktop.org: http://dejavu.sourceforge.net/snapshots/dejavu-ttf-20070327-1710.tar.bz2"
    author: "John Karp"
  - subject: "cool stuff"
    date: 2007-03-23
    body: "the logout dialog looks sort of bad, however. very last-generation with the square edges (needs more round!) and dotted focus-thingie (continuous and light, perhaps colored, would be better, I think). it looks like a standard button-widget was transparently overlayed onto a background, and especially with the colorswitch in the background near the bottom, the effect is rather jarring."
    author: "illissius"
  - subject: "Arrow to the left as logout ?"
    date: 2007-03-25
    body: "Hi,\n\nis it a good idea to have an Arrow to the left as logout icon ?\n\nNormaly it means undo or go back. Sure I go back, since I came from console or a display manager. But this can be some hours, days, weeks or even months ago.\n\nI would suggest the power off button or a arrow to the bottom. The left button could be used for the cancel action.\n\n\nGreetings \nFelix"
    author: "Felix"
  - subject: "Re: Arrow to the left as logout ?"
    date: 2007-03-27
    body: "I must confess I'm quite happy somebody else dare to tell it !\n\nThese buttons are quite confusing, and IMHO, deserve a correction.\nFor instance, the Gnome symbol for exiting a sessions, an half open door is much clearer (uglier maybe but clearer!).\n\nThe power-off button is a good hint (as in KDE 3.5.x, let's be conservative some times !) and in second rank the Gnome idea.\n\nBTW, if you could change your mind about vertically oriented words, it's quite...cheesy, actually. Well, that's a lousy comment too, I suppose :-(\n\nI'm not a psychological expert but I think people who're achieving a great deal of work to create a whole new KDE \"experience\" (sic) do want some visual difference to show that something happened, that they accomplished something big, something really new. It's only I'm not sure it's always good to change for the sake of changing...\n\n\nFeeling as ugly as an \"inspecteur des travaux finis\" (in French, that means \"reviewer of completed works\" : the guy who did nothing, arrives after the hard work is done and tells everybody, with a despising tone, how bad is all the stuff they've done !) but feedback is something you might consider, isn't it ?\n\nThanks for all & take your time, 3.5.6 will be fine enough for months !\n\nYojik77"
    author: "Yojik77"
---
Well, so far I've published a dozen articles about KDE 4 over the last 12 weeks. A lot of content has been covered, but there is rapid progress still being made on those topics. So, in no particular order, this week's issue deals with addenda and updates to the last 12 articles, so that you can see some of the rapid progress happening as KDE races forward. Read on for details.


<!--break-->
<p>First, when I demonstrated KRunner back on <a href="http://dot.kde.org/1167723426/">January 2nd</a>, it was barely useful, contained temporary artwork (it still does), and looked pretty basic. Since then, it's seen a lot of work. It is now installed by default, sounding the final death of many of the elements that previously belonged to KDesktop, one of KDE's oldest components. It (mostly) works, pops up when you press F2 (see note <sup>1</sup>), handles CTRL-ESC to pop up the task manager, handles CTRL-ALT-DEL to pop up the logout dialog, loads the screensaver and screen locking routines as expected, and behaves in a very useful and beautiful fashion. Below is a shot of the new KRunner in action:</p>

<p align="center"><img src="http://static.kdenews.org/dannya/vol13_4x_krunner.png" alt="KRunner" /></p>

<p>There's also this <a href="http://plasma.kde.org/media/krunner.mpeg">short movie</a> (a week or two old) showing off how KRunner works when searching for commands to run. The interface is not yet final, but it's getting closer to completion. When it is further along, you will certainly get more updates.</p>

<p>Speaking of artwork, during the <a href="http://dot.kde.org/1173332156/">Oxygen article</a>, I showed off KDE's new logout screen. At the time that was using temporary artwork that was a proof-of-concept placeholder. It's been updated somewhat, and now looks like this:</p>

<p align="center"><img src="http://static.kdenews.org/dannya/vol13_4x_logout.png" alt="Logout Screen" /></p>

<p>That's not the only screenshot that needs updating. After the <a href="http://dot.kde.org/1172721427/">Dolphin article</a>, there were many requests for a tree view in Dolphin. Well, Peter Penz, the lead developer of Dolphin listens to feedback, and within hours, there was a preliminary tree view checked in to KDE SVN. After a few weeks of development, here's what the work-in-progress tree view looks like in Dolphin (this is also a good opportunity to show off some Oxygen icon artwork improvements as well):</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol13_4x_dolphin.png"><img src="http://static.kdenews.org/dannya/vol13_4x_dolphin_thumb.png" alt="Dolphin w/ treeview" /></a></p>

<p>And one more shot: back in January, I wrote about some of the work being done on KDE's <a href="http://dot.kde.org/1169588301/">Job Progress improvements</a>. This section has seen much work since the very initial code I showed off back then, with much of the user feedback to that article helping shape its development. It now has support for pausing downloads, storing a list of finished tasks, searching for keywords among the active tasks (useful if you have 30 tasks on the go), has a simple configuration dialog, and more. The backend that powers this whole system has seen a lot of work, with more discussion with the GNOME folks on standardizing the mechanism so that applications using this progress reporting will run seamlessly on either desktop. Here's a shot of the job monitor and its configuration dialog (see note <sup>2</sup>):</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol13_4x_kuiserver.png"><img src="http://static.kdenews.org/dannya/vol13_4x_kuiserver_thumb.png" alt="UI Server" /></a></p>

<p>okular has received preliminary support for PDF forms, thanks to improvements to the Poppler backend. okular is the first Poppler-based viewer to add support for forms, but more are expected to follow. The implementation isn't particularly useful at the moment, and looks too ugly for screenshots, but the initial support is there. There has been a lot of development happening in okular - which, alongside other KDE developments, you can read about in the weekly <a href="http://commit-digest.org/">KDE Commit-Digest</a> - including support for additional formats, reworked text searching, and more.</p>

<p>Work on Kalzium powers forward: artwork for a student-friendly view is being developed; a better use of the empty space in the center of the table is in place; and work on libavogadro-based 3D molecule viewer is making steady progress.</p>

<p>The rendering in KOffice with regards to text and shape rotation has improved. Part of the problems with the screenshots in my <a href="http://dot.kde.org/1168284615/">original article</a> is that I was using a bad default font that was shipped by my distribution. Here is a shot of a similar document, but you can see what a difference 2 months can make. In this shot, you'll see a number of new things, including a new 'default text' feature. Where you see the famous 'Lorem ipsum' text, clicking onto the text clears the widget of text, leaving your cursor on a blank text shape. Also shown is content generated automatically using the Kross scripting features, and several Flake shapes also inserted using Kross plugins. The user interface also has seen a lot of improvements, however there is still work to be done: missing icons, font and widgets sizes, and so on.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol13_4x_kword.png"><img src="http://static.kdenews.org/dannya/vol13_4x_kword_thumb.png" alt="KWord" /></a></p>

<p>There were many more changes made to KDE since these articles have gone live, and unfortunately I've only had a change to cover a small handful of them. Of course, for a real look at all the work that's being done, you would need to build the sources yourself on a regular basis. In the meantime, I'll return with more new articles so you don't have to build the sources (though I certainly won't discourage you from doing so!).</p>

<hr />

<p><sup>1</sup><em>Bug Alert:</em> There is currently a nasty, unsolved bug where KRunner stops responding to ALT-F2 after a period. Fear not, this sort of bug will not be present by the time KDE 4.0 hits the streets, as it would be considered 'show stopper' bug. If, however, you need an excuse to get into KDE 4 development, here's a point of entry that will quickly get you accustomed to KDE and Qt programming.</p>

<p><sup>2</sup><em>Power User Tip:</em> This uiserver screen is usually hidden by default when nothing is happening. If you are running KDE 4, you can make it visible at any time by calling the following command:
<br />
<pre>qdbus org.kde.uiserver /kuiserver/MainWindow_1 com.trolltech.Qt.QWidget.show</pre><br />
</p>

