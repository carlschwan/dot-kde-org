---
title: "Qt Jambi v4.3 Released"
date:    2007-06-07
authors:
  - "msmith"
slug:    qt-jambi-v43-released
comments:
  - subject: "Link to open source download"
    date: 2007-06-07
    body: "Since I don't see the link to get the GPL'd version on that page anywhere, here is where you can get it:\nftp://ftp.trolltech.com/qtjambi/source"
    author: "Matt"
  - subject: "Re: Link to open source download"
    date: 2007-06-08
    body: "The Eclipse integration is still binary-only. That's pretty disappointing."
    author: "Kevin Kofler"
  - subject: "Re: Link to open source download"
    date: 2007-06-08
    body: "Also, I'd recommend against actually trying to build the source code, because there's several errors in it (apparently it was never tested with the actually-released Qt 4.3.0). I wanted to try to see if the Java code builds with the new GCJ in Fedora 7 which supports Java 1.5 features, but I never even got that far! The C++ code choked on:\n* one pointer type issue with JNI, which may be due to GCJ's JNI headers, so let's ignore this one (-fpermissive \"fixed\" it)\n* lots of pure virtual methods in the Qt Designer interfaces with no implementation anywhere to be seen in the Qt Jambi code, yet the code tried to instantiate the resulting abstract objects; no Java involved at all as far as I can see, so I think everyone who tries to build it will see this, even with Sun's Java\n* an attempt to include a private Qt Designer header which is not installed; again, nothing to do with Java\nand that was too much. I don't care much about Java anyway, so I don't really need this. But if Trolltech is trying to outreach to Java programmers, this is not the way, they'll just conclude Qt is crap. :-("
    author: "Kevin Kofler"
  - subject: "Re: Link to open source download"
    date: 2007-06-08
    body: "> \"one pointer type issue with JNI, which may be due to GCJ's JNI headers, so let's ignore this one (-fpermissive \"fixed\" it)\"\n\nWe mention in our FAW which virtual machines we have tested Qt Jambi against. We previously didn't do GCJ because of lacing 1.5 support, but we'll probably get around to that pretty soon. The current release of Jambi is tested against Apachy Harmony and Sun JDK.\n\n> \"lots of pure virtual methods in the Qt Designer interfaces with no implementation anywhere to be seen in the Qt Jambi code, yet the code tried to instantiate the resulting abstract objects;\"\n\nI compile this everyday without problems so this is a bit surprising to me. Could you be a bit more spesific?\n\n> \"an attempt to include a private Qt Designer header which is not installed; again, nothing to do with Java and that was too much. I don't care much about Java anyway, so I don't really need this. But if Trolltech is trying to outreach to Java programmers, this is not the way, they'll just conclude Qt is crap. :-(\"\n\nThe private headerfile is included in the package and should compile just fine. As for Java developers, the idea is that they'll use the prebuilt binaries. The source package is for cross language development, mixing Java and C++ and using the generator.\n\n"
    author: "Gunnar Sletta"
  - subject: "Re: Link to open source download"
    date: 2007-06-08
    body: "The missing pure virtuals are in the generated files in cpp/com_trolltech_tools_designer. But I now see the generator uses some JNI calls, so it may be those JNI calls are malfunctioning, which would be Fedora 7's GCJ's fault, not yours. I'm not sure though, so here's the list of stubs (for lack of an idea how those should actually be implemented) I had to add (to get as far as that private header):\nIn class QtJambiShell_JambiPropertySheet:\n    virtual int count() const {return 0;}\n    virtual int indexOf(const QString &name) const {return -1;}\n    virtual QString propertyName(int index) const {return QString();}\n    virtual QString propertyGroup(int index) const {return QString();}\n    virtual void setPropertyGroup(int index, const QString &group) {}\n    virtual bool hasReset(int index) const {return false;}\n    virtual bool reset(int index) {return false;}\n    virtual bool isVisible(int index) const {return false;}\n    virtual void setVisible(int index, bool b) {}\n    virtual bool isAttribute(int index) const {return false;}\n    virtual void setAttribute(int index, bool b) {}\n    virtual bool isChanged(int index) const {return false;}\n    virtual void setChanged(int index, bool changed) {}\nIn class QtJambiShell_JambiResourceBrowser:\n    virtual void setCurrentPath(const QString &filePath) {m_currentPath=filePath;}\n    virtual QString currentPath() const {return m_currentPath;}\nprivate:\n    QString m_currentPath;\nIn class JambiMemberSheet:\n    virtual int count() const {return 0;}\n    virtual int indexOf(const QString &name) const {return -1;}\n    virtual QString memberName(int index) const {return QString();}\n    virtual QString memberGroup(int index) const {return QString();}\n    virtual void setMemberGroup(int index, const QString &group) {}\n    virtual bool isVisible(int index) const {return false;}\n    virtual void setVisible(int index, bool b) {}\n    virtual bool isSignal(int index) const {return false;}\n    virtual bool isSlot(int index) const {return false;}\n    virtual bool inheritedFromWidget(int index) const {return false;}\n    virtual QString declaredInClass(int index) const {return QString();}\n    virtual QString signature(int index) const {return QString();}\n    virtual QList<QByteArray> parameterTypes(int index) const {return QList<QByteArray>();}\n    virtual QList<QByteArray> parameterNames(int index) const {return QList<QByteArray>();}\nI have no idea of where the real implementations of all these functions went if they were supposed to be there.\n\nFor the private header, what it's looking for is this one:\nQtDesigner/private/qdesigner_utils_p.h\nwhich is nowhere to be found in qtjambi-gpl-src-4.3.0_01.tar.gz."
    author: "Kevin Kofler"
  - subject: "Web Start is simply cool"
    date: 2007-06-07
    body: "The web start demonstration is really impressive!\nGood work, Trolltech!"
    author: "Evil Eddie"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-07
    body: "Looks like it does not work on solaris :("
    author: "anon"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-07
    body: "What's impressive with a demo that does not even run on a current SUSE-10.2 with KDE 3.5.7 + 3.90.1 and Qt 4.3.0 + 3.3.8, neither with Firefox 2.0.0.4, nor with Konqueror 3.5.7 + 3.90.0 ??\n\nWhat's impressive if Firefox qoes in an infinite loop opening up the same web page over and over in different windows?\n\nWhat's impressive in neither seeing a good error message nor finding an even basic description on the Trolltech website what this web start thingie is supposed to do?"
    author: "scratching my head"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-07
    body: "Blame Java ;-) My experience with Java and Webstart is that you can just pray that it works and it doesn't work much of the time. In this case it worked for me ... "
    author: "Torsten Rahn"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-07
    body: "Have you tried using a different browser? Sounds like Firefox has a strange bug in it. \n\nAnyway, Java Web Start is not responsible for this. In fact, it has very little to do with what happens in your web browser at all. For more information on it, Wikipedia is your friend:\n\nhttp://en.wikipedia.org/wiki/Java_Webstart"
    author: "Strange?"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-07
    body: "> Have you tried using a different browser?\n> Sounds like Firefox has a strange bug in it. \n\nI didn't have a problem with Firefox in Windows XP. Perhaps it's some Linux binary compatibility problem?"
    author: "Diederik van der Boor"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-08
    body: "The Java Web Start starts in a separate process, so it should not have any effect on Firefox. However, I would expect either the XP and Linux versions of Firefox to contain some bugs which are not present in the other.\n\nI also didn't understand which exact page on which it was failing. Tell us, and we'll see if we can fix it.\n"
    author: "Eskil Abrahamsen Blomfeldt"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-07
    body: "Works fine for me using Konqi 3.5.7/Sun Java 5/Kubuntu...\n\nWhat are you using???"
    author: "Odysseus"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-08
    body: "> \"What's impressive in neither seeing a good error message nor finding an even basic description on the Trolltech website what this web start thingie is supposed to do?\"\n\nIf Java Webstart actually starts, no matter how far it gets you should get the webstart logo plus and some sort of error, regardless of how it goes wrong. So it seems more like java is not properly functioning on your machine.\n\nThe webstart is a collection of all our demos and examples and should runs on all linux boxes we have tried it on. The only native dependency currently is Xinerama...\n\nIf you get any sensible error messages out of this, I'd be interested to hear about it."
    author: "Gunnar Sletta"
  - subject: "Re: Web Start is simply cool"
    date: 2007-06-08
    body: "> \"Looks like it does not work on solaris :(\"\n\nWe didn't provide binaries for Solaris as it didn't get prioritized high enough. If there is high demand for Solaris binaries we'll add that in the future. The source packages are cross platform and should build on all platforms though...\n"
    author: "Gunnar Sletta"
  - subject: "Re: Web Start is simply cool"
    date: 2008-07-16
    body: "Dear Sir,\n\nWe are trying to use qtjambi through web start with a jnlp file. It works the first time but for the second time you want to launch the application, it says there is an error as the cache key is olready in use.\nWould you help us to fix it.\n\nThanks,\n\nPaulin Bekambo."
    author: "Paulin Bekambo"
  - subject: "Re: Web Start is simply cool"
    date: 2008-07-16
    body: "Dear Sir,\n\nWe are trying to use qtjambi through web start with a jnlp file. It works the first time but for the second time you want to launch the application, it says there is an error as the cache key is olready in use.\nWould you help us to fix it.\n\nThanks,\n\nPaulin Bekambo."
    author: "Paulin Bekambo"
  - subject: "Get it running"
    date: 2007-06-11
    body: "For those who had problems getting it running (I had these with Fedora 7 for example) you can have a look at this howto: http://liquidat.wordpress.com/2007/06/11/howto-sun-java-web-start-on-fedora-7/\n\nIt is Fedora related and specific but should give the more experienced users an idea how to handle problems - if there are problems at all."
    author: "liquidat"
---
Trolltech has <a href="http://trolltech.com/company/newsroom/announcements/press.2007-06-06.2984198523">announced the final release</a> of <a href="http://trolltech.com/products/qt/jambi/index">Qt Jambi</a> version 4.3 (corresponding to Qt v4.3.0), the Java binding for Qt. The bindings are available under the same dual licensing deal as Qt.  The <a href="http://dist.trolltech.com/developer/download/webstart/index.html">webstart demonstration</a> gives you a feel of what it can do.  


<!--break-->
