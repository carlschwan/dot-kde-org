---
title: "Will Stephenson Talks KDE 4 on Novell Open Audio"
date:    2007-07-31
authors:
  - "jriddell"
slug:    will-stephenson-talks-kde-4-novell-open-audio
comments:
  - subject: "Transcript?"
    date: 2007-07-31
    body: "Is there a transcript available? Usually it takes less time to read than to listen..."
    author: "anon"
  - subject: "Re: Transcript?"
    date: 2007-07-31
    body: "more then that, it's easier to understand when it's written, not all kde users are English aware  ;-)"
    author: "djouallah mimoune"
  - subject: "Re: Transcript?"
    date: 2007-08-02
    body: "It is really worth your time, listen to it.\nIMHO also the first part of the show. Gives you a peek into how people at Novell are working."
    author: "Kevin Krammer"
  - subject: "Re: Transcript?"
    date: 2007-08-02
    body: "i just can't understand them, they speak english ;-) all i know is French and arabic -( \n\nthanks for the advise anyway ;-)"
    author: "djouallah mimoune"
  - subject: "Re: Transcript?"
    date: 2007-08-02
    body: "While text often consumes less time, spoken word denotes emotion in voice of both interviewer and interviewee. An advantage of spoken audio is that you can do it inbetween (some) work. For example, I can play a game without audio, and have spoken word on. Its also usable while lying on couch or in bed."
    author: "anon"
  - subject: "Re: Transcript?"
    date: 2007-08-05
    body: "A problem for our dear Djouallah above is that he simply doesn't speak English, and can't understand them well enough to follow what is discussed. A transcript is pretty crucial in such cases..."
    author: "jospoortvliet"
  - subject: "Re: Transcript?"
    date: 2007-08-06
    body: "yeah, at last someone understood me ;-) "
    author: "djouallah mimoune"
  - subject: "Good interview"
    date: 2007-07-31
    body: "A very nice summary and complete overview of the things coming in KDE 4 and what's going on. Great interview!"
    author: "apokryphos"
  - subject: "Re: Good interview"
    date: 2007-07-31
    body: "As usual, it's not complete. How can anything less than 8 hours of talking be complete when so much is coming?!?!?"
    author: "jospoortvliet"
  - subject: "Re: Good interview"
    date: 2007-07-31
    body: "It also doesn't help that the interview was conducted even before Akadamy. A lot has happened in that time, including the Alpha 2 and Beta 1 releases."
    author: "Soap"
  - subject: "Re: Good interview"
    date: 2007-08-01
    body: "I'm a bit disappointed by the interview. You don't hear anything new compared to what you could read on the dot."
    author: "Evil Eddie"
  - subject: "Re: Good interview"
    date: 2007-08-01
    body: "Sorry, but the attentive Dot reader such as yourself wasn't the listener I had in mind when giving the interview.  My aims were to inform the Novell audience of users, customers and staff about all the benefits KDE brings, and generally to spotlight the work of the KDE team on openSUSE and Novell products."
    author: "Bille"
  - subject: "Re: Good interview"
    date: 2007-08-01
    body: "And I don't even remember WHAT it was, but there was something new for me in there ;-)"
    author: "jospoortvliet"
  - subject: "Re: Good interview"
    date: 2007-08-01
    body: "If you don't remember things, a lot of things will be new for you all the time ;-)"
    author: "kleiner"
  - subject: "Re: Good interview"
    date: 2007-08-02
    body: "LOL xD"
    author: "Luis"
  - subject: "Re: Good interview"
    date: 2007-08-02
    body: "Every time people say this to me I have to laugh my ass off!"
    author: "Anonymous"
  - subject: "Re: Good interview"
    date: 2007-08-05
    body: "My ex mentions I'm a lot like a goldfish sometimes: Hey, a little castle... Hey, a little castle... Hey, a little castle... Hey, a little castle... Hey, a little castle... Hey, a little castle... Hey, a little castle... Hey, a little castle... Hey, a little castle...\n\n\n:D"
    author: "jospoortvliet"
---
The <a href="http://www.novell.com/feeds/openaudio/?p=170">current edition of Novell Open Audio</a> podcast features an interview with KDE core developer Will Stephenson. He discusses what is coming in KDE 4, Novell's commitment to KDE and the changes he has been working on recently. The first forty minutes of the podcast are a review of some of the projects from <a href="http://ideas.opensuse.org">Novell Hack Week</a>, with Will's segment starting at 39:50 minutes in.
<!--break-->
