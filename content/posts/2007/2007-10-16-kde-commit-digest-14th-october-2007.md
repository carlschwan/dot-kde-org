---
title: "KDE Commit-Digest for 14th October 2007"
date:    2007-10-16
authors:
  - "dallen"
slug:    kde-commit-digest-14th-october-2007
comments:
  - subject: "khtml vs. webkit"
    date: 2007-10-16
    body: "Fist off, just wow. What an impressive list of stuff and an amazing amount of work. \n\nAnyway I noticed a lot of commits about khtml. It all sounds really cool, but can anyone explain what will become of this work once trolltech releases a version of qt with webkit? If KDE moves to webkit (which sounds like it would make sense to the more developed thing) might it be possible to port these changes over? Will work be dropped? Is webkit already using similar stuff?\n\nAnyway super job. Aside from the complaints about qprinter, it sounds like things are really going to rock, even for a 4.0 release."
    author: "Christian"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "There are presently no plans in the KHTML development community to abandon development of KHTML and the KHTML library. There is tons of cooperation w/Apple on the JS side of things; and the codebases are very similar, but KJS development is still independent. On the HTML side of things, no one has ever offered an integration path that makes sense, that doesn't throw out tons of advancements, and that does not compromize project values by putting volunteer developers into 2nd class status.\n\n\n\n"
    author: "SadEagle"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "Im really glad to see the khtml is still in development.as a web developer I just want to say kudos, khtml rendering is so nice, better than Gecko in most cases and sure its so faster.\n\nonly things that i think are needed in Konqueror as a web browser are:\n1)WYSIWYG mode.as i looked around there was a kafka project which is dead for about 4-5 years...is there any plan to implement it?\n\n2)Better javascript debugging...a Web Browser, needs help from webmaster to grow, can you see how 'SpreadFirefox' really worked?and to attract web developers Konqueror needs something like 'Firebug' and 'Web developer toolbar'.\nKonqueror's JS debugging looks so weak to me and thats the only reason im keeping firefox: just to see better javascript debugging output\n\nI really tried to do not say my personal needs, these are what i get when talking to other users and designer/developers.\n\nsorry for my bad English"
    author: "Emil Sedgh"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "IIRC: Kafka was renamed Quanta which was renamed KDEWebDev.  And so it goes....\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "iirc kafka was a differrent project, and Quanta is a Part of KDEWebDev module which contains KLinkStatus, Quanta+, Kommander and 1-2 others...\n\nand as far as i know, kdewebdev only contains klinkstatus for kde 4.0 because Quanta+ depends on kdevplatform module which will be ready for 4.1\nso no quanta+ for 4.0 :)"
    author: "Emil Sedgh"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "No, the Kafka code was taken and was improved and integrated into Quanta, where it is called VPL. But unfortunately it didn't really meet the expectation. \nAs the other poster said, there won't be a Quanta for 4.0. "
    author: "Andras Mantia"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "better javascript debugging would indeed be very useful.\na while back I had a job that involved working on some rather wacky javascript which was designed to work in both FF and IE. it was utterly unusable in konq, and I wanted to fix that, but I had to give up because I just couldn't debug it."
    author: "Chani"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "> On the HTML side of things,\n> no one has ever offered an integration path that makes sense that doesn't throw\n> out tons of advancements, and that does not compromize project values by\n> putting volunteer developers into 2nd class status.\n\nSounds like good arguments. :-)\n\nI'm curious, what integration paths didn't make sense?"
    author: "Diederik van der Boor"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "It would really rock if KHTML and WebKit could merge to a point where they would be (almost) compatible - maybe the biggest problem for a webbrowser is marketshare. Only with marketshare, sites are tested against a browser, and if sites aren't tested against it, no amount of developer time or effort can ever ensure it always works.\n\nMerging WebKit and KHTML, or at least ensuring they are reasonable compatible so you can put the user agent for Konqi on WebKit or something would make Konqi a viable alternative - as it's rendering engine has a serious market (and mind) share.\n\nWouldn't backporting improvements made in KHTML to WebKit make sense as a first step towards integration?"
    author: "jospoortvliet"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "\"Wouldn't backporting improvements made in KHTML to WebKit make sense as a first step towards integration?\"\n\nThis presumes that SadEagle and CareWolf have Webkit commit rights, and I'm not sure if this is the case.\n\nIf they don't, they should have - they have proven themselves *more than* worthy."
    author: "Anon"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "I don't think it has anything to do with commit \"rights\".\n\nIt has more to do with willingness to contribute to Apple Corporation.\n\nDerek"
    author: "D Kite"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "Not quite. It has to do with my lack of willingness to be an unpaid employee to Apple Corporation. There is a difference there. \n\n"
    author: "SadEagle"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "> my lack of willingness to be an unpaid employee to Apple Corporation\n\nWell, is KHTML paying you? My guess is that it's not (though you and other contributors to KDE are doing a wonderful job). So I would take payment out of the equation and focus on which project is most advanced and valuable to KDE users."
    author: "Scott"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "There is a difference between being an unpaid contributor and an unpaid employee.\n"
    author: "SadEagle"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "I totally agree with you but I still have to admit that from a user point of view I'd love to see a true partnership and merge.\n\nI assume you don't want to become an employed contributor to Apple but expect Apple to be more of a KDE contributor/supporter.\n\nWhen I look at this page here http://ev.kde.org/supporting-members.php I see quite some names but there is one missing... ;-)\n\nApple got a great gift from Free software projects (not only KDE also FreeBSD, GNU...) and certainly would have been unable to code anything remote equivalent to Mac OS X (when they started coding Mac OS X, Apple still had quite some financial constraints and although I don't now any figures the developer team was probably a small one at that time).\n\nAlthough they never did violate any free software licenses there's a huge difference between just acting according to the licenses and to contribute to a project. And as Apple loves to say that they support Open Source well they should actually start doing it really.\n\nThe most easisest thing for Apple would be becomming a supporting member of KDE eV. Much more complicated for them (mentally) would be turning Webkit also spiritually into project of equal partners (Apple, KDE, Trolltech, Nokia...).\n\n(Although I don't know any current details of the Webkit SVN I am quite sure that some smart police of internal branching would make it possible that every company/project can provide a Webkit-product at their favourite fixed date).\n"
    author: "Arnomane"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "Much more complicated for them (mentally) would be turning Webkit also spiritually into project of equal partners (Apple, KDE, Trolltech, Nokia...).\n\nThat is exactly what is happening right now. Apple is working rather closely with KDE, Trolltech and Nokia to merge things and keep it all together. Being an unpaid Apple employee simply isn't the question - Apple is paying ppl to work on OUR webbrowser tech - that's imho a better way to look at it."
    author: "jospoortvliet"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "This is a classic misunderstanding of free software community and project.\n\nIt isn't about a product. It is process. There is no way at all to describe the process that has led to WebKit as anything but insulting and demeaning to free software developers. Everything in the way that tree has been maintained demonstrates that Apple considers it a one way flow, and that they would gladly have free contributors to their bottom line. That is their right, and our mistake here was not seeing that whole situation for what it was.\n\nI don't want a browser. I want free software. I want a healthy community. I want a development structure that allows encourages excellence and pride. From that has come a kick-ass browser. And from that process will continue to come a kick-ass browser, among other things.\n\nPrimary focus on product harms the process.\n\nDerek"
    author: "D Kite"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-18
    body: "This criticism isn't very constructive or specific. How about some concrete ways in which Apple can improve the Webkit community/process?"
    author: "Scott"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "\"Apple is working rather closely with KDE, Trolltech and Nokia to merge things and keep it all together\"\n\nIncorrect. Outside of JS and plain talking, Apple has never worked closely with KDE.\n\n"
    author: "SadEagle"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "No, I see no reason for Apple to support KDE --- they have their own UI, their own OS, and it's their right (actually, an obligation, being a public company) --- to care for their interests. What we have to realize, however, is that they are acting in their interests, not ours. They may coincide, for short or long time, but what a lot of people are suggesting is giving up control over a portion of our destiny. That may work out OK, but we have to be honest about this.\n\n"
    author: "SadEagle"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "I think the issue isn't that you become an unpaid employee of Apple, but that Apple becomes an unpaid contributor to KDE. Not taking advantage of all the resources they, Nokia, TT and others pour in KHTML/Webkit would be a waste, right?"
    author: "jospoortvliet"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "My highest respect to you, and all people who are still taking care of KHTML. Apple only cares about the Webkit, and doesn't care about KDE at all (heck, they only care about $$ actually).\n\nThanks for still working in KHTML, even though many people really want Webkit as KHTML replacement."
    author: "AC"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "To Apple's credit, they are almost as good w/commit rights as KDE is. Well, in KDE you pretty much only have to ask for an account. There, you have to submit some patches, ask for an account and fill in some mostly reasonable paperwork so their lawyers don't get a heart attack. \n"
    author: "SadEagle"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "That would be quite good, and your overall view is spot on (I know we can make a non-trivial number of websites suddenly work by lying on our 'appName' navigator field as Safari does --- may be we should do that for 4.0?) --- except it's very difficult to do with ~4000 commits; and it might take a lot longer to do so than it was to develop them, due to changes in source structures and inefficiency introduced by them having too much manpower. Unfortunately, even when aware of our implementations/fixes, Apple folks still tend to pretty much ignore them, except, again, in the matters of JS. There has also been plenty of talk about corporate $$$ from KDE-friendly companies on this matter, but none of them actually care about this sort of thing. What they care about is the new portable/cell-phone web browser craze.\n"
    author: "SadEagle"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "Well, now Trolltech is going for this, I guess there will be WebKit for KDE anyway - and even more money and ppl being poured into it. I think taking advantage if that is simply the smartest thing to do, esp with apple becoming more open and all."
    author: "jospoortvliet"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "Switching to WebKit is not even an option until at least KDE 4.1, so we need to keep developing KHTML, no matter what solution is picked in the end. "
    author: "Carewolf"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "+1 Good point indeed! :-)"
    author: "Diederik van der Boor"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "As a matter of negotiation tactics with Apple? I mean, I feel that the real problem underlying the development is that KDE is running out of options. KDE 4.0 takes far far longer than expected and most features are not ready for KDE 4.0 \nIt was a severe mistake to put so much effort on the KDE 4 cycle, we all see the early celebration of new projects and then find out that actually nothing was coded."
    author: "Rock"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "\"and then find out that actually nothing was coded\"\n\nHave you considered trying running a diff between KDE 3 and KDE 4? :-) \n\nSeriously, people have given enormous amounts of their creativity, intellect, and sweat to this transition. If you can't contribute, at least please don't come swooping in to make disparaging and ill-informed remarks about the Herculean efforts that have been freely given.\n\nI for one am awfully excited about KDE4, and I think it's just great that a release is coming soon. Free software does (& should) work differently than proprietary software, because there are no forced upgrades: \"release early, release often\" is a great mantra for open source. I think it's amazing that things are already looking so good. Waiting for everything to be absolutely perfect would, without a doubt, slow the project down. Once it's out there, one can hope to attract more well-informed and constructive input (hint, hint) and perhaps even new developers to the project.\n\nIndeed, I have only one gripe with KDE4: it's looking so \"delicious\" that I find myself tempted to abandon my beloved 3.5 series fairly soon. I'm sure I'd then discover some things that worked better in the old days. While I'm not a GUI coder, I might inevitably be tempted into putting some work into making KDE4 better. So, see what you're doing to me KDE4 developers? You're distracting me from the things I know I _should_ be doing, instead you're tempting me into helping out with the free desktop! Bad bad! I just hope I can hold out a bit longer!  Aiiieeee! (Sudden sickening silence, then the sounds of KDevelop being fired up.)\n"
    author: "T"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "> Have you considered trying running a diff between KDE 3 and KDE 4? :-) \n\nlol..\n\nI read somewhere that the change \"from KDE 3.5 to KDE 4.0\" already takes more commits then \"starting from nothing to KDE 2\". :-) So keep up the good work everyone!"
    author: "Diederik van der Boor"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "Well, with a staggering 300-400 commits a day (387/day on average, last week), the amount of work going into KDE 4 sure exceeds everything KDE ever did..."
    author: "jospoortvliet"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "\"As a matter of negotiation tactics with Apple?\"\n\nWhat does this even mean?\n\n\"I mean, I feel that the real problem underlying the development is that KDE is running out of options.\"\n\nHuh? Options for what?\n\n\"KDE 4.0 takes far far longer than expected\"\n\nIt was scheduled for October 23rd.  If it all goes to plan, it will be roughly seven weeks late.  Even if it doesn't go according to schedule, it will only be a few months late.  Whoop-de-doo.\n\n\"and most features are not ready for KDE 4.0\"\n\ns/most/some/\n\nAnd developers have known this and have been saying this since the beginning of the year:\n\nhttp://www.kdedevelopers.org/node/2600\n\nMost of the KDE3 features weren't all magically present in KDE3.0, either, oddly enough!\n\n\"It was a severe mistake to put so much effort on the KDE 4 cycle,\"\n\nThe Armchair Developer has spoken! What exactly should have been done, then? You're already whining about how KDE4.0 is taking oh-so-much-longer than scheduled, and now you want to delay it even further while continuing to use a less productive toolkit that Trolltech no longer officially supports?\n\n\"we all see the early celebration of new projects and then find out that actually nothing was coded.\"\n\nSuch as?\n\n\"The real question is whom to fire 'cause Raptor was not delivered. Development by obscurity does not work.\"\n\nThe developer of Raptor is not paid for his efforts, so he can't be fired (and firing the lead developer of a project generally just makes it later, fyi).  Also, Raptor is developed in public SVN:\n\nhttp://websvn.kde.org/trunk/playground/base/raptor/"
    author: "Anon"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "The problem with the missing features is that some of the features currently missing in KDE 4.0 are stuff which was there in KDE 3! (See e.g. Quanta completely missing.) Regressions are bad. Every such regression is going to be a real problem for distributions like Fedora which want to pick up KDE 4 early. And if distributions can't ship 4.0, who is going to be able to use it? Few people are able and willing to compile KDE themselves. It takes hours."
    author: "Kevin Kofler"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "These apps can be run in KDE 3.5.x form, so that's not a regression and distros can just ship it. Sure, there ARE real regressions, like printing, but that'll be fixed asap."
    author: "jospoortvliet"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "By the way, I sense a name conflict which is going to confuse KDE 4 users a lot: one of Soprano's dependencies is also called Raptor and it has nothing to do with Raptor the menu!\nhttp://librdf.org/raptor/"
    author: "Kevin Kofler"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "Honestly, why do you spread this utter bollocks?\n\n* KDE does *NOT* take far longer than expected, start reading the release schedule and compare it to the initial one.\n\n* It was *NOT* a mistake to put such effort into KDE 4. Sure, it was risky to do all those changes 'at once', but it has great results, it has strengthened our community and it shows KDE has a great vision for the future.\n\n* \"... actually nothing was coded\". Really, pull your head out of your arse and look at what's actually being done (hint: last week had more than 2700 commits to the codebase).\n\nIf you're only here for spreading FUD and annoying people (like you do with your other responses, then just go away. We don't need you.\n\n"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "The guy has a point, I just tried the resent beta of KDE4, is much an alpha or maybe a pre-alpha quality.\n\n/mw worrie."
    author: "Div"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-16
    body: "\"I just tried the resent beta of KDE4, is much an alpha or maybe a pre-alpha quality.\"\n\nThis I agree with, but \"Rock\" barely had anything even resembling a point - he was incoherent, confused and/ or factually wrong in almost every assertion he made.  Don't encourage him, please :)"
    author: "Anon"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "I agree with him in the part where he says that to much effort have been taken to Plasma, it will be nice in the begining but desktop widgets are so 2003 now.\n\nThe whole Plasma idea as the desktop core I think it was a misstake, why spend so much time in something the user will get tire of in a couple of hours like plasma?\n\nI really hope there is an option to turn off all the cluttered animations.\n\n"
    author: "Div"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "\"I agree with him in the part where he says that to much effort have been taken to Plasma\"\n\nAre you kidding me? Plasma (and the Oxygen style) have received, by a very big margin, the *least* amount of development time, and from only a tiny core of contributors!\n\n\"it will be nice in the begining but desktop widgets are so 2003 now.\"\n\nAnd yet, Dashboard and Compiz Fusion's screenlets are immensely popular.  Heck, I've heard many people say that the sole reason they don't switch from GNOME to KDE is that KDE doesn't have arbitrarily resizable icons on the desktop.\n\n\"The whole Plasma idea as the desktop core I think it was a misstake, why spend so much time in something the user will get tire of in a couple of hours like plasma?\"\n\nAgain, hardly any time has been spent on Plasma, and people tend to interact with their desktops/ panels/ taskbars/ launchers/ menus on a pretty frequent basis. \n\n\"I really hope there is an option to turn off all the cluttered animations.\"\n\nCongratulations on avoiding the obvious term \"bloated\" and substituting an even more non-sensical pejorative in its place (how can an animation be \"cluttered\"?) What are you expecting Plasma to look like, anyway? Spaceships and hamburgers whirling round the screen every second of the day? And Aaron has already said that Phase will eventually allow animations to be shortened or switched off centrally.\n\nNo wonder you found \"Rock\"'s meanderings so compelling; you're almost as misinformed as he is!\n"
    author: "Anon"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "\"Are you kidding me? Plasma (and the Oxygen style) have received, by a very big margin, the *least* amount of development time, and from only a tiny core of contributors!\"\n\nThe Plasma Idea started two years ago, but it could only be implemented with QGraphicsView and that came with Qt 4.3, that makes me thing that the whole Plasma idea is just for Qt marketing and not for the sake of the users, after all, tha idea came from a Qt employee.\n\n\"And yet, Dashboard and Compiz Fusion's screenlets are immensely popular. Heck, I've heard many people say that the sole reason they don't switch from GNOME to KDE is that KDE doesn't have arbitrarily resizable icons on the desktop\"\n\nIm sure they are popular, but not in a scale to base YOUR WHOLE DESKTOP ON IT, it is ridiculous.\n\n\"Congratulations on avoiding the obvious term \"bloated\" and substituting an even more non-sensical pejorative in its place (how can an animation be \"cluttered\"?) What are you expecting Plasma to look like, anyway? Spaceships and hamburgers whirling round the screen every second of the day? And Aaron has already said that Phase will eventually allow animations to be shortened or switched off centrally.\"\n\nIf every widget, icon, window gets animated it gets in your way, when I use KDE3 is the firsth think I do is turn off animations, now take the small amount of animations in KDE3 and multiply it in an exponential way, you get KDE4 animations, come on, even OSX is moderated in that kind of stuff and surely they have the power animate every pixel if they want.\n\nMy point is, Plasma is a KDE4.1 material not KDE4.0, some how is being pushed to much, why? I don't know, well, I know, but I don't think is apropiate to come out with it.\n\n"
    author: "Div"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "\"The Plasma Idea started two years ago, but it could only be implemented with QGraphicsView and that came with Qt 4.3, that makes me thing that the whole Plasma idea is just for Qt marketing and not for the sake of the users\"\n\nAaron wanted Plasmoids to be rotatable and scalable - like GNOME's desktop icons, and OS X's dashboard widgets - and QGraphicsView gives this automatically.  With increasing LCD resolutions, resolution-independence is becoming more and more necessary.  I can't even begin to imagine why you think the entire KDE4 desktop metaphor was created just to promote Qt, which hardly needs promotion.  It's just a complete non-sequitur.  Frankly, I doubt many of TrollTech's customers even use QGraphicsView - it's very niche, and a large amount of Qt apps are simply simple GUI apps that just use the common-or-garden buttons, listviews and other ordinary widgets.\n\n\"after all, tha idea came from a Qt employee.\"\n\nLet's name names here: You are referring to Aaron Seigo, who is sponsored by TrollTech to *work on KDE*.  He is also noted for his huge contributions to kdelibs (which don't affect TrollTech in any way, shape or form), and is also *KDE*'s e.v. President.  Attempting to portray him as some kind of TrollTech puppet is both insulting and, worst of all, based on some of the most tenuous \"logic\" I've ever seen.  Come on!\n\n\"Im sure they are popular, but not in a scale to base YOUR WHOLE DESKTOP ON IT, it is ridiculous.\"\n\nWhy not? If you're going to base your desktop on something, why not make it flexible and powerful widgets /applets that can be re-located seamlessly where the user wants them, be it on the desktop or their panels? \"Everything is a Plasmoid\" is a wonderfully elegant way of approaching things.  Also, \"WHOLE DESKTOP\" is slightly misleading choice of words, given that the desktop is such a tiny portion of the KDE project.\n\n\"If every widget, icon, window gets animated it gets in your way, when I use KDE3 is the firsth think I do is turn off animations, now take the small amount of animations in KDE3 and multiply it in an exponential way, you get KDE4 animations\"\n\nWhat are you basing that on? It's certainly not my experience of KDE4.  Drop the silly hyperbole (\"and multiply it in an exponential way!!!1\"), please :) And as noted, it will eventually be switch-offable - the KDE guys are not stupid, and not given to wasting resources unnecessarily.  With the increasing prevalence of laptops running on batteries and thin-clients running on networks, they'd have to be very silly to make this unconfigurable - and the KDE guys are not silly.\n\n\"My point is, Plasma is a KDE4.1 material not KDE4.0, some how is being pushed to much, why?\"\n\nI'll agree that Plasma won't be anywhere near it's full potential by 4.0 (it may well, frankly, suck outright), but I have no idea what you mean by \"some how is being pushed to much, why?\".  Can you explain, please?\n\n\n\"I don't know, well, I know, but I don't think is apropiate to come out with it.\"\n\nHey, you've already accused the lead developer of perverting the KDE4 desktop into a mere marketing vehicle for TrollTech, so why be coy now? ;) Seriously, you obviously have a loss of trust in the KDE guys at the moment, and rather than keep your fears to yourself where they can fester, why not detail them and let the KDE guys explain their side?              "
    author: "Anon"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "Good points you have there, here is my worries in a bigger scope and why I think Plasma is affecting:\n\n\nRight now Computers sellers like DELL and HP are finally shipping Computers with Linux, and right now they ahre shipping mainly GNOME as the base desktop. Is it because GNOME is better than KDE? I don't think so, both are good and better than what vista have to offer, so what's the reason? I think is stability, GNOME may be using an inferior toolkit like GTK but is stable, Versi\u00f3n 2.20 it is really stable, now lets see the impressions we are getting from KDE4.0, one word: unstable.\n\nIf KDE4.0 is going to be unstable then Im afraid it can miss the bubble is coming for the Linux Desktop in the next year, will KDE4.1 be more stable? surely, but knowing in the history of KDE releases time line, it will take at least another 8 months after the release of 4.0 and mean while the compentence is not sleeping.\n\nSo the delays and unstability Plasma has brought are dangerous, I'll waith for the final release to see how stable can get, but I don't have much hopes right now. The features Plasma will bring are nice, but certainlty I don't think it would be that necessary for KDE4.0 sucess, because at the end it is something the users already have seen in other desktops, vista, osx, gnome, etc.\n\nIm hoping to be wrong at the end."
    author: "Div"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "Cool, now we're getting somewhere :)\n\n\"Right now Computers sellers like DELL and HP are finally shipping Computers with Linux, and right now they ahre shipping mainly GNOME as the base desktop. Is it because GNOME is better than KDE? I don't think so, both are good and better than what vista have to offer, so what's the reason? I think is stability, GNOME may be using an inferior toolkit like GTK but is stable, Versi\u00f3n 2.20 it is really stable, now lets see the impressions we are getting from KDE4.0, one word: unstable.\"\n\nOk, this is certainly true: there has been a huge upswing in GNOME adoption, and the recent DesktopLinux survey - if the results are accurate, which is not necessarily the case - shows that for the first time, there appear to be more GNOME users than KDE ones.  However:\n\nDell (and, presumably, HP)[1] are mainly offering GNOME because it is the default desktop of Ubuntu which, regardless of whether it deserves it, is currently The Favourite distro, by a landslide.  Frankly, I don't think Dell even care about what desktop they are deploying - in their IdeaStorm poll, people wanted Ubuntu, and I think Dell just went with that.  GNOME is certainly more stable than KDE4.0 but of course, that's why we have the KDE3.5.x branch which is still pumping out huge amounts of bug fixes (and thus, extra stability).  In a nutshell - I don't think that the stability of GNOME 2.20 as opposed to KDE 4.0 is the driving force behind the choice of GNOME (which, as seen in my footnote, hardly has a monopoly in the hardware world) for Linux laptop vendors, so I'm not seeing too much damage there.\n\n\"If KDE4.0 is going to be unstable then Im afraid it can miss the bubble is coming for the Linux Desktop in the next year, will KDE4.1 be more stable? surely, but knowing in the history of KDE releases time line, it will take at least another 8 months after the release of 4.0 and mean while the compentence is not sleeping.\"\n\nThis presumes two things that I believe are erroneous:\n\n1) that this year is going to see a tidal wave of adoption of Linux by the masses [\"200X will be the year of Linux on the Desktop\" has been a running joke now since X = 0 ;)]; and \n\n2) If KDE misses this wave, then this will cause massive, long-term (irreparable?) damage to the KDE project.\n\nI dispute 1) because we've been hearing it for years and, though the signs pointing towards it coming true are stronger than ever, it still seems to be way, way off - Dell's own figures suggest that Linux sales are strong, but nowhere - nowhere - near toppling Windows.\n\nI dispute 2) because I have seen many *new* linux users - new computer users, in fact - given a choice between GNOME and KDE, and *choose KDE*.  There is a certain mindset that seems to attract people to either of GNOME and KDE, and it seems to be a human universal - put simply, a sizeable chunk of the population (not just power-users, as the stereotype would have us believe) will always prefer KDE to GNOME (and another chunk will always prefer the reverse).  Thus, I don't really see an exodus from KDE to GNOME any time soon.  Note that if, say, 10,000,000 people start using the Linux desktop this year and only a tiny minority - 10%, say - choose KDE, *KDE will still have grown*.  I can't remember who said it - it may even have been Aaron again - but the Linux desktop is not even close to being a zero-sum game yet, and even if GNOME grows faster, *both desktops are still growing*.\n\nAdditionally, say KDE4.2 *really* delivers the goods - say it's fast, lighter than GNOME (a distinct possibility - figures show that KDE3.5.x and GNOME are very evenly matched in terms of RAM usage, and Qt4 offers an instant and automatic reduction in RAM usage), has powerful but elegant apps with consistent and well-designed UIs (we already have powerful apps; most just need a little nip-tuck here and there, and better defaults) incredibly easy to administer (KDE's configuration is a major plus here - Kiosk lets you sculpt it just the way you like it.  You can even configure it to be less overwhelmingly configurable, if you want!) well-integrated and flat-out beautiful (go Oxygen Team!) - do you think that people will still avoid switching to it, just because 4.0 was rough and unstable? I'd say that anyone who does avoid it for this reason was simply looking for an excuse not to use it, and were a lost cause anyway.\n\n\"So the delays and unstability Plasma has brought are dangerous, I'll waith for the final release to see how stable can get, but I don't have much hopes right now. The features Plasma will bring are nice, but certainlty I don't think it would be that necessary for KDE4.0 sucess, because at the end it is something the users already have seen in other desktops, vista, osx, gnome, etc.\"\n\nOk, the \"delays and unstability\" of KDE4.0 are definitely *not* solely the result of Plasma - in fact, I'd say that it has hardly contributed a thing, and that it was the massive work of porting to Qt4 and the new KDE4 APIs that has consumed most of the time.  Remember - there are over 5 million lines of code in KDE's svn, and Plasma is a only a few tens of thousands - if that.  As a concrete example of things other than Plasma contributing to delays - just a week ago or so (a very short time before the originally planned release date of October 23rd, mind), a huge change to KDE occurred - the merging of the new KConfig branch.  This was something that people wanted for 4.0, and had absolutely nothing to do with Plasma. \n\n\"Im hoping to be wrong at the end.\"\n\nI think you are going to be right on a lot of things - KDE4.0 will be *very rough*, and will likely suffer a big PR backlash, especially from that minority of GNOME users that still think the Desktop War is going on and that KDE must die.  Commercial vendors will avoid KDE4 for a long time, probably at least a year.  GNOME will gain users at the expense of KDE.  \n\nWhat I don't agree with is that Plasma is going to be the major cause of delays and instability for 4.0 - frankly, there's a million things that will be wrong for 4.0, and the majority will be unrelated to Plasma in any way shape or form.  Also, as noted, even if Plasma had been feature-complete months ago, there were a whole bunch of things that still needed to be done before 4.0 and which wouldn't have been done for October 23rd.  \n\nI also don't think that KDE4.0's roughness will cause irreparable damage - it will be a big, shiny, and humiliating black-eye, sure, but no one has ever died from a black-eye :) KDE's technical infrastructure is top-notch (part of what makes KDE apps so feature-rich - Qt4 and kdelibs are so comprehensive that they do much of the heavy-lifting for you) and it's development is healthier than ever - just look at the graph:\n\nhttp://kde.mcamen.de/statistics.html\n\nand I have no doubt that the devs can hack and chip and polish on the rough and jagged piece of glass that will be KDE4.0 until it becomes a jewel.  And then, at the risk of sounding like a troll - it will be *leagues* ahead of GNOME, and people will see what they've been missing and switch to KDE in their droves ;)\n\nIn short - short term future looks bleak; long-term future looks rosy.  Don't panic :)\n\n[1] - Note that the EEE PC - which has received an order for 1Million laptops from Russia - uses KDE by default.  I think the intel Classmate does, too (?)"
    author: "Anon"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "What do you call the initial schedule? KDE 4 has been planned for early 2007, and even before that for fall 2006, and I think at one point in the distant past even 2005. Compared to the first actual schedule, it isn't slipping much, but coming up with said schedule in the first place has taken ages."
    author: "Kevin Kofler"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "The first real release schedule targeted October 2007. Yes, ppl have talked about a release before that, but hey, ppl always talk. The project never made a decision to release beginning of 2007 or something like that. We have a delay of 2 months on a 2 year schedule. MS has a delay of 3 years on a 2 year schedule. Who does better?"
    author: "jospoortvliet"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "But the years of completely unscheduled development, combined with the nonsense ETAs given by even some high-ranked KDE people, for example http://vizzzion.org/stuff/presentations/LWE2006-Utrecht/10YearsOfKDE.pdf which claimed \"Will be available in first half of 2007\" (and that was Sebastian K\u00fcgler, not a random dot.kde.org commenter), is exactly what gives people the perception of slipping."
    author: "Kevin Kofler"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-17
    body: "Is just me that thinks using webkit is a good idea because it will share developers time and resources between KDE, Trolltech, Nokia and Apple?"
    author: "Iuri Fiedoruk"
  - subject: "Re: khtml vs. webkit"
    date: 2007-10-18
    body: "It isn't just you, but it also isn't a good idea. None of these companies necessarily care about khtml in the long run. Certainly Apple hasn't been very respectful of khtml developers (read some of the earlier comments). Do you really want to tell people who are doing this *for fun* that they should instead go and do what some usurping company wants? Let them decide.\n\nRespect your developers, they're doing it because they love doing it. Telling them to throw out their codebase because somebody with more money forked it... \n\nIf there's a way to merge, I'm sure it will happen. But I think we should make sure that it is also best for KDE. Apple has a desktop already, they don't need ours. "
    author: "Annony"
  - subject: "End of KDE 3's kicker?"
    date: 2007-10-16
    body: "does this mean the end of kicker? Should we be saying our fond farewells?\n\nGoodbye, old friend!"
    author: "Level 1"
  - subject: "Re: End of KDE 3's kicker?"
    date: 2007-10-16
    body: "He was a true patriot! :salute:"
    author: "Erunno"
  - subject: "Re: End of KDE 3's kicker?"
    date: 2007-10-16
    body: "ok i take it that many people like it, so here is my stance on this\n\nDIE kicker DIE!\n\nBut anyway, I will salute to him too. Bye little fellow! :)"
    author: "she"
  - subject: "The battles of Lubo&#353; Lu&#328;\u00e1k "
    date: 2007-10-16
    body: "Following up on last week's Dot discussion, Lubo&#353; Lu&#328;\u00e1k tells us:\n\n\"I officially declare Klipper to be kind of working now.\"\n\nThis should allow Lubo&#353; to devote all of his time to a more important pursuit: getting the Dot to handle all the characters in his name. Expected to join him is struggle is Ho\u00e0ng &#272;&#7913;c Hi&#7871;u, while Anders Lund has declared that he does not see the urgency, despite living in a country full of funny characters."
    author: "Martin"
  - subject: "Re: The battles of Lubo&#353; Lu&#328;\u00e1k "
    date: 2007-10-16
    body: "Moonspeak?  I think some of that post got lost in translation :)\n\n\"Expected to join him is struggle is Ho\u00e0ng &#272;&#7913;c Hi&#7871;u, while Anders Lund...\""
    author: "Level 1"
  - subject: "Re: The battles of Lubo&#353; Lu&#328;\u00e1k "
    date: 2007-10-16
    body: "No, that's exactly the purpose, and the funny thing ;-)"
    author: "jospoortvliet"
  - subject: "Re: The battles of Lubo&#353; Lu&#328;\u00e1k "
    date: 2007-10-16
    body: "ROTFL\n"
    author: "Lubos Lunak"
  - subject: "Updated sidux  to 3.5.8 -  feels faster!"
    date: 2007-10-16
    body: "Last night I dist-upgraded my sidux-installation via the smxi-script when I noticed that 3.5.8 was entering sid. Thanks for this release! The desktop feels snappier than before - I love it! Keep up the great work! And don't let the haters get you down!"
    author: "bedalius"
  - subject: "lol.."
    date: 2007-10-16
    body: "Someone is making it very interesting peek through the detailed lists.. :-p\n\n> Aaron J. Seigo committed changes in /trunk/KDE/kdebase/workspace/plasma/plasma:\n> bye bye old control box. how we loved you in our time of debugging, yet despised\n> your flicker and general lack of beauty.\n\n\n> Aaron J. Seigo committed changes in /trunk/KDE/kdebase/workspace:\n> Now the kicker lay down to sleep,\n> I pray svn its code to keep;\n> And if it crashes ere I wake,\n> I pray a backtrace you do take.\n> \n> <fadeout>\"s-i-i-lent night ... h-o-o-ly night ... doo doo doo, doo doo doooo..\"</fadeout>\n\n\n:-)"
    author: "Diederik van der Boor"
  - subject: "Kickoff"
    date: 2007-10-16
    body: "Kickoff looks so bad. Can't you guys, make it optional?   "
    author: "Jon"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "I think it looks bad too, but we should not forget, this is our personal toughts.everything in the world will be disliked by many people...so please stop trolling.\n\nand there will be other launchers like Rapor too."
    author: "Emil Sedgh"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "Well Kickoff will be the default for 4.0, Raptor is meant to be ready for 4.1. Still, Kickoff may become an acquired taste, just like coffee once was for me. You hate it at first, but after you use it several times you realise how great it really is :)"
    author: "Alan Denton"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "mmmmhhhh.... coffee!"
    author: "infopipe"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "kickoff offers nothing more than standard menu it just offers more complexity and less usability. Did suses gnome team do the usability survey for this."
    author: "nobody"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "Nice trolling. Go home, now."
    author: "Vide"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "well, I've wondered if somebody starts it again :-)\n\nif you dislike Kickoff and want something else, please put your vote here: http://bugs.kde.org/show_bug.cgi?id=150883\n\nnote - that bugreport (enhancement request) is not intended to harm Kickoff in any way, please do not express your feelings like \"Kickoff is ugly\" etc., just use the voting feature and (optinally) tell what would suit you better (make comparison explaining your usage)"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "Such a bugreport is rather useless - the choice right now is Kickoff or no menu. As Kickoff is infinitely better than no menu at all, Kickoff it is.\n\nIf someone writes a better/other menu (like Raptor) it might make it for 4.1."
    author: "jospoortvliet"
  - subject: "Re: Kickoff"
    date: 2007-10-17
    body: "> Such a bugreport is rather useless - the choice right now is\n> Kickoff or no menu.\n\nthere is still some time left to change the choice ... but it won't be done if there is no interest, so this is a way to express the interest\n\nand, as mentioned in the comments, even if it is too late for 4.0, it may show that it may be worth at least for 4.1, instead of leaving it out completely"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-17
    body: "\"change the choice\" means that someone would have to step up and implement a new KMenu in record time so it gets at least *some* user testing before release. Plus, I have a hard time imagining Robert Knight dropping all the Kickoff code he has written in the last few weeks to start anew. Better have one (more or less) finished and polished implementation of a menu for 4.0 than several half-assed."
    author: "Erunno"
  - subject: "Re: Kickoff"
    date: 2007-11-09
    body: "If you read the comments, you will notice that a simple menu WAS implementer and in kdeextragear. The hope is that it becomes a configurable option (Kickoff OR QMenu).\n"
    author: "riddle"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "sure! but i think a not-so-good kickoff is much better then no menu at all for kde 4.0 :P."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "Couldn't we replace kicker with kittens? Everyone loves kittens."
    author: "Anonymous"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "I'm allergic to kittens."
    author: "Johhny Awkward"
  - subject: "Re: Kickoff"
    date: 2007-10-17
    body: "No, you can't be. That's just impossible. Kittens are too cute to cause allergic reactions. It MUST be something else."
    author: "jospoortvliet"
  - subject: "Re: Kickoff"
    date: 2007-10-17
    body: "it's probably Raymond disguised as kittens, everyboy hates Raymond"
    author: "-"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "The real question is whom to fire 'cause Raptor was not delivered. Development by obscurity does not work."
    author: "Rock"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "You must be a really good project manager."
    author: "Paper"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "If he were, he'd have to fire himself I guess..."
    author: "Andre"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "> The real question is whom to fire 'cause Raptor was not delivered.\n\nThis alone shows you don't know how KDE is developped. Fire a volunteer?\n\nRaptor development is open, and fireing lead developer of a project is a bad idea. Especially since they nicely explained why things are late, and doing the best they can to deliver a good plasma for KDE 4.0 and 4.1."
    author: "Diederik van der Boor"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "Rock, remember: your teenage years are the _best_ time to learn to program. You can always work on project management when you're older. KDE can use your help now!\n"
    author: "T"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "I wonder how many of you did actually use the KDE4.0 version of Kickoff or at least the KDE3 one?\n\nI really like it and I think you should give it a try first."
    author: "TeeZee"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "Speaking as someone who also likes kickoff...\n\nIt seems there are two reactions to kickoff. Either you love it and it changes how you use the menu, or you hate it and want it out of your life.\n\nI have the old kicker on a machine and I have grown to profoundly dislike it.\n\nMaybe we could find a psychology test therein.\n\nKnowing myself, those who like kickoff are halfway to stark raving mad.\n\nDerek"
    author: "D Kite"
  - subject: "Re: Kickoff"
    date: 2007-10-16
    body: "I tried Kickoff on KDE3. And I *almost* like it, but reverted to standard kmenu. My biggest problem is that I didn't found a convenient way to navigate in Kickoff with the keyboard, and while I love to use my mouse, I hate touchpad, so on a laptop (you can't allways plug a mouse) I need to be able to use a lot the keyboard (and alt+f2 is not allways convenient if you don't know the exact name of the application you want to launch)."
    author: "Cyrille Berger"
  - subject: "Re: Kickoff"
    date: 2007-10-17
    body: "afaik, keyboard use is an important target for Kickoff in 4.0 ;-)"
    author: "jospoortvliet"
  - subject: "Kickoff and toolbox"
    date: 2007-10-16
    body: "First of all, I like the looks of kickoff, but there is something thats not quite right when using it. Something with the behavior of the buttons. Is it possible to optionally use the old menu?\n\nSecond of all; The desktop toolbox...why is it needed? Shouldn't it be hidden and only brought forward when needed? I would imagine that configuring the desktop would be something you did at rare occasions, but the rest of the time didn't use.\n\nNo offense to the authors of this code of course..."
    author: "invictus"
  - subject: "Re: Kickoff and toolbox"
    date: 2007-10-16
    body: "Plasma aims to (eventually) make the development of taskbars, menus etc fairly trivial - in fact, they will be programmable in at least Javascript[*], and likely Python and Ruby, making them as easy to write *and* deploy & install as Firefox extensions, so you can (hopefully!) expect a wide-range of alternatives to be released by the community as time goes on.\n\nAgain, though, this will likely be post-4.0.\n\n[*] - before anyone screams \"but Javascript is insecure!\": most Javascript vulnerabilities are really browser-specific, and are unlikely to apply to a desktop.  Also, Javascript is flexible, has a fast and lightweight parser in kdelibs already and, most importantly for security, is well sandboxed by default (unlike Ruby and Python, currently)."
    author: "Anon"
  - subject: "Re: Kickoff and toolbox"
    date: 2007-10-16
    body: "what about the behaviour of the buttons? could be a bug - it's not like we've got any shortage of those :)\n\n\nthe toolbox... er... it is hidden, mostly. there's just a little picture in the corner that swooshes out when you put your mouse there. I'm not sure what it'll look like when plasma's not under such heavy development.\nplus, it's good to have *some* kind of visible thingy for the newbies. if it was totally hidden, we'd get tons of complaints from people that didn't have enough curiosity left in them to find it."
    author: "Chani"
  - subject: "Re: Kickoff and toolbox"
    date: 2007-10-16
    body: "What about the classical context menu? Rightclick on the desktop->configure or something like that? It works and the users only see it when needed."
    author: "Anon"
  - subject: "Re: Kickoff and toolbox"
    date: 2007-10-17
    body: "I do not think that it's a good solution.\nFor the first it's kinda ol' school, and KDE4 have to be new an innovative. For the second, if we put too many options in such a configure-list it would be messy - I think :)\nI think the best solution would be a good-looking toolbox which appear when you drag your mouse up to the right corner - kind off the way MS Win-Vista do it, just better ;)"
    author: "Kristho"
  - subject: "Re: Kickoff and toolbox"
    date: 2007-10-17
    body: "Hmmm...I don't think Vista is doing that... except maybe the sidebar or something."
    author: "Anon"
  - subject: "korqueror without \"search box\"?"
    date: 2007-10-16
    body: "won't konqueror 4.x have a \"search box\", like 3.5 series?\n\nLook at this picture (right part):\nhttp://files.fredemmott.co.uk/ss-20071014.jpg\n\nI use it a lot: google, imdb, dictionary... :-("
    author: "Me and my brain"
  - subject: "Re: korqueror without \"search box\"?"
    date: 2007-10-16
    body: "I cannot find it in Beta2 (packages by kubuntu)\nbut maybe\nits there but not compiled (commented out, etc)\nits there but not as default\nits not there (not ported)\n\nlosing functionalities may happen for 4.0, dont worry, hopefully even its lost atm, it will be back in time (post 4.0)"
    author: "Emil Sedgh"
  - subject: "Re: korqueror without \"search box\"?"
    date: 2007-10-16
    body: "Also, you can search for anything from the address bar.  For example:\n\"gg: blah\" searches google for blah\n\"wp: blah\" searches wikipedia for blah\n\netc.  Have a look at web shortcuts in the konq settings for a full list.  I find a lot of people don't know about these."
    author: "Leo S"
  - subject: "Re: korqueror without \"search box\"?"
    date: 2007-10-16
    body: "That is handy but currently it pollutes the address history with non-address entries and not being able to easily remove individual entries makes that an annoyance. ;)\n\nThe search box itself could use some love too, the various implementations just don't feel right sometimes. If it was something that is there when you need it but goes away when it's not doing anything. So instead of clicking in the search field you could just have a button or clickable area that expands and is ready for your typing input and not taking up 120 pixels or so of toolbar space all the time.\n"
    author: "firephoto"
  - subject: "Re: korqueror without \"search box\"?"
    date: 2007-10-16
    body: "It's a plugin, not part of core functionality, so you probably just need to install it from kdeaddons... Woops, extragear. And as was stated, it's easier to just use url shortcuts like gg:\n"
    author: "SadEagle"
  - subject: "Re: korqueror without \"search box\"?"
    date: 2007-10-17
    body: "Yeah, maybe it's easier, but for the newcomer it's just another thing he/she have to learn - and I think the google-searchbox is more newcomer-friendly.\nSo, make the searchbox as a opportunity :)"
    author: "Kristho"
  - subject: "Re: korqueror without \"search box\"?"
    date: 2007-10-16
    body: "The search box is a plugin at kdeaddons module probably not installed in the screenshot... try to remove kdeaddons in your KDE3 instalation when you'll see what I said."
    author: "Hugo"
  - subject: "Re: korqueror without \"search box\"?"
    date: 2007-10-16
    body: "Thanks all! That screeshot isn't mine."
    author: "me"
  - subject: "KDE4 Rocks, Stupid!"
    date: 2007-10-16
    body: "It looks great, but why isn't my favourite feature included (please vote for my bug report). It crashes somestimes as well. KDE 4 won't be as good as KDE 3 unless it can suspend and restore properly. I like Oxygen, but why is it all white with no contrast? The alt-f2 thingy is black. Why isn't it white? I did something and it didn't work - can someone tell me why? How do I fix it? The programmers need to work harder. The usability people need to usabil harder. I like the flashy graphics, my friends will want to use KDE. Why are you all working on the graphics though, we need more features. Why does Aaron look like Tom Green? He needs to stop looking like Tom Green and work harder on Plasma. And Lubos would be far more productive if he didn't mess around with silly character sets all the time."
    author: "Johhny Awkward"
  - subject: "Re: KDE4 Rocks, Stupid!"
    date: 2007-10-16
    body: "I think that just about covers it; well played! :)"
    author: "Anon"
  - subject: "Re: KDE4 Rocks, Stupid!"
    date: 2007-10-17
    body: "Tom Green needs to stop looking like Aaron."
    author: "T. J. Brumfield"
  - subject: "Re: KDE4 Rocks, Stupid!"
    date: 2007-10-17
    body: "He can't, it helps him get laid more often."
    author: "-"
  - subject: "Re: KDE4 Rocks, Stupid!"
    date: 2007-10-17
    body: "I don't know what game is being played, but I believe you win."
    author: "ms"
  - subject: "Re: KDE4 Rocks, Stupid!"
    date: 2007-10-17
    body: ">>The usability people need to usabil harder.\n\nI just about fell out of my chair on that one."
    author: "Louis"
  - subject: "KDE4.X is like RatPoison"
    date: 2008-09-25
    body: "KDE4.X is exactly like RatPoison - you need a very good terminal knowledge for using it fine..."
    author: "nitrofurano"
  - subject: "Krunner in plasma?"
    date: 2007-10-16
    body: "I'm not sure if I'm understanding this correctly, but it seems that Krunner is now no longer a separate process, but a part of Plasma?  \"KRunner/Runner -> Plasma/Runner as it is actually hosted by libplasma\"\n\nIf so, I remember the initial rationale was to keep it simple and separate so that it is more stable and you can always bring up a run dialog or shutdown/logout options.  Has that been changed?"
    author: "Leo S"
  - subject: "Re: Krunner in plasma?"
    date: 2007-10-17
    body: "No, that thing was about where the code lives, KRunner is still a separate process, afaik ;-)"
    author: "jospoortvliet"
  - subject: "Screenshots"
    date: 2007-10-16
    body: "Like a lot of people I've gone back and forworth on my impression of the Oxygen style (mostly over the amount of contrast), but I have to say the screenshots in the linked article look flat out great. Amarok 2.0 is also coming together incredibly well it seems (Amarok and Konqueror are probably my two most used applications by far, especially since it seems Amarok is on and playing whenever my computer is on).\n\nA lot of people seem disappointed that every single feature that was in KDE 3 series and even pondered about for KDE 4 series aren't in KDE 4.0, but you gotta remember that Rome wasn't built in a day. Also can't forget that sometimes to take a step forward you have to take a step backwards first. At the very least it looks like KDE 4.0's libraries are gonna be incredibly useful (and how boring would it be if 4.0 delivered everything you could ever possibly want? 4.1 would just be lame then ;) to develop all sorts of applications.\n\nSince I seem to be rambling, I'll wrap it up by simply saying: Thank you, to every single KDE contributor out there, you've all created something simply amazing!"
    author: "Sutoka"
  - subject: "Re: Screenshots"
    date: 2007-10-17
    body: "Agree!\nI think it would be great with more contrast to the Oxygen-theme, no-contrast is kinda boring :( And it's not easy for the eyes.\nI think it could be great if they look a bit at bespin - thats beatiful :D"
    author: "Kristho"
  - subject: "Re: Screenshots"
    date: 2007-10-17
    body: "Bespin was poorly coded, and was kicked out.  It was the original Oxygen.\n\nI do agree the style still needs more contrast.\n\nI really like how many gradients are used in the KDE 3 Domino style.  Is it possible to add support for some custom gradients in Oxygen so we can tweak it to have as much contrast as we'd like?"
    author: "T. J. Brumfield"
  - subject: "Re: Screenshots"
    date: 2007-10-17
    body: "It is actually better than it was, they did a great job in the last 2 week to add some contrast. Now even on my 10 years old CRT screen, i can clearly see all widgets. I really start to love this theme, even if i was not so sure about it at the beginning. \n\nI hope that the new oxygen will have all feature of the old one (for 4.1), i loved all those small details on tab switching and overmouse stuff.\n\nNow I think the next thing to do is finishing the windeco to match with the nice mini-windeco of QT4 panel. After that, at the exeption of inactive windows the overmouse menu item, the theme will be almost complete!"
    author: "Emmanuel Lepage Vallee"
  - subject: "Re: Screenshots"
    date: 2007-10-17
    body: "Yes -- every day Casper commits a little tidbit that makes the style or the windows decoration better. Finished? Not yet -- but when I'm using KDE 3.5.x, I'm actually yearning to open a full KDE 4 session, because the widgets are getting so pleasing -- not in-my-face-look-at-me, not drag. Pleasing."
    author: "Boudewijn Rempt"
  - subject: "KDEPrint is Broken for KDE 4.0?"
    date: 2007-10-17
    body: "How is this not a showstopper?"
    author: "Marc J. Driftmeyer"
  - subject: "Re: KDEPrint is Broken for KDE 4.0?"
    date: 2007-10-17
    body: "Printing is available via QPrinter, which has gotten much better since Qt3.  Of course, not as good as KDEPrint, but it'll do for now."
    author: "Leo S"
  - subject: "Re: KDEPrint is Broken for KDE 4.0?"
    date: 2007-10-17
    body: "So I assume via QtGui you will have a UI temporarily that will be accessible for any app in KDE 4?"
    author: "Marc J. Driftmeyer"
  - subject: "Re: KDEPrint is Broken for KDE 4.0?"
    date: 2007-10-17
    body: "Yep, that's what I'm spending every waking moment outside work doing, converting every KDE program over to QPrinter..."
    author: "Odysseus"
  - subject: "Re: KDEPrint is Broken for KDE 4.0?"
    date: 2007-10-17
    body: "It is a showstopper, and it would have taken several months to fix up, which is why we're removing it for now and switching to Qt for 4.0.  Come KDE 4.1 the situation will be greatly improved (fingers crossed).\n\nAny printing gurus out there, feel free to step forward and commit your time for 4.1 :-)\n\nJohn."
    author: "Odysseus"
  - subject: "Re: KDEPrint is Broken for KDE 4.0?"
    date: 2007-10-18
    body: "Thank you for your professional honesty, John.\n\nMy C++ has been years, but I'm in the midst of retooling the C/C++/ObjC triple set, but it takes some time.\n\nWhen I can I will definitely find some projects I can work on and help out.\n\nThe QPrinter definitely won't keep me from using KDE 4 on Debian.\n\nI've got OS X and Linux w/ KDE and GNOME environments.\n\nWhen you look back around 1989 to now, life is really good."
    author: "Marc J. Driftmeyer"
  - subject: "No trolling..."
    date: 2007-10-17
    body: "...but as a long time KDE fan I belong to the people who are less than enthusiastic about the current \"betas\". \n\nProbably the following points will be regarded as trolling, but it's simply my opinion.  I'd like to be proven wrong, as I'm basically looking forward to KDE4!\n\nFirst of all, even considering the fact that KDE seems to define \"beta\" and \"alpha\" in another way than other devs, the SVN-Builds I try out are simply far from usable. At least with openSUSE. Single apps work great, but the desktop as a whole - not.\n\nThen, sorry to say it again, Kickoff. I always disliked this menu concept in openSUSE, but you could always, with a mouseclick, revert to the old KDE menu. Even in Windows XP I can revert to the \"old style\" startmenu.\nI really don't care which usability studies say that Kickoff is better for me - I'm simply slower with it. Kickoff for some reason doesn't feel right. Please don't lecture me that I have to get used to it and all will be fine - this is the wrong (Gnome-like) attitude. \nThat's obviously my subjective personal opinion, however, it's my personal computer and therefore I'm entitled to have an personal opinion on how it works. Others will have other opinions, and everybody should be able to choose his/her solution.\n  \n\"Usability experts\" simply have to accept that nowadays desktop paradigms like the startmenu are more or less common knowledge. In cars you also leave the pedals at the same position and don't change them because usability studies show that drivers would be more productive if switch them.\n\nThen, plasma. I really like the great desktop effects. There is great potential in it. But, to be honest, the current implementation again breaks too many desktop paradigms I have learnt by heart over years. Configuration by a desktop toolbox which constantly pops up in some corner (I thought this was just a hack for the beta versions, but I read above that this will stay)? Why not the context menu? Why reinvent the wheel how a desktop is configured?\n\nBTW, configuring the desktop is a nice thing, and plasma is really great playground for this, but sooner or later the desktop is basically the platform to start one's applications. Its not a standalone app on its own right. \nThis seems to be a bit lost in all the plasma hype. A working taskbar with a working startmenu and a desktop with app and data icons is, for me, the most basic aspect of a desktop. This simply isn't there currently. Or, you have to do some plasma magic to get this - but why???\n\nAll this basically means that I'm really glad when having my KDE3 desktop back after playing around a bit with KDE4. When I remember the switch from KDE1 to 2 und from 2 to 3, I always stayed with the beta releases of the new version as soon as possible.\n(BTW - in SUSE's KDE4 preview you still can start a working KDE3 kicker - that always saves my day when trying KDE4...)\n\nSo now, come on flame me as a troll ;)."
    author: "Peter Thompson"
  - subject: "Re: No trolling..."
    date: 2007-10-17
    body: "Most applications are really Beta quality, it's just that Plasma isn't. The desktop is important, which is why it feels pre-alpha to you. But really, KDE 4.0 Beta 3 isn't Alpha quality, it's really getting close to RC.\n\nAnd to be honest, though Kickoff is great in theory (thorough usability testing showed it to be the superior menu compared to much competition), I don't like it that much either. Maybe it has to grow on you. Anyway, there is currently only ONE menu which will be mostly ready for KDE 4.0 (freeze in a few days!!!) and that's Kickoff. Nothing we can do about it, and it's not a bad decision, it's just all we can do.\n\nAnd about the desktop/plasma configuration, let them play with it, OK? If you want to innovate, you have to experiment. And while most experiments turn out wrong, you WILL find and improve things. It's the only way. You should praise them for trying, you know. Think about it this way:\nIf you want to promote innovation, you gotta reward brilliant failures and punish mediocre successes. Because every mediocre success is a lost opportunity for a brilliant new innovation, and every brilliant failure must be rewarded so the brilliant ideas will continue to flow.\n\n\nAnd yes, Plasma isn't ready yet - but it will be usable for 4.0, it's a showstopper."
    author: "jospoortvliet"
  - subject: "Re: No trolling..."
    date: 2007-10-17
    body: "Couldn't have put it better myself."
    author: "Parminder Ramesh"
  - subject: "Re: No trolling..."
    date: 2007-10-17
    body: "\"If you want to promote innovation, you gotta reward brilliant failures and punish mediocre successes. Because every mediocre success is a lost opportunity for a brilliant new innovation, and every brilliant failure must be rewarded so the brilliant ideas will continue to flow.\"\n\nJust wanted to quote that so it gets said once again!"
    author: "Boudewijn Rempt"
  - subject: "Re: No trolling..."
    date: 2007-10-18
    body: "I've still never seen how a menu that is slower and makes me take extra steps to get to my programs is vastly superior.\n\nProvide Kickoff for those who want it, but please also provide a basic KMenu, and also bring on Raptor!"
    author: "T. J. Brumfield"
  - subject: "Re: No trolling..."
    date: 2007-10-18
    body: "Extra steps, extra steps, why in the world I still hear that? It has been discussed 1 thousand times. Most people don't use more than 8 applications, favorite menu is faster, a lot faster in the 90% of the cases.\n\nYes, the normal menu has favorites too, but they are on the top, that a mistake, a big one, and they are little and you actually have to aim them, so, it's far for being nice. "
    author: "Luis"
  - subject: "Re: No trolling..."
    date: 2007-10-24
    body: "I don't use the menu for launch my favorite aplications! I place one click icons on the tasktbar that are much faster than any menu for that. I use the menu only to turn-off the system and to reach all the dozens of aplications that I open 10% of the time.\n\nSo any design like kickoff is redundant and slow for me. I also don't like adaptative menus like Raptor. As those 10% aplications that I use are fairly random, I would have to relearn the position of them everytime that I open the menu. I preffer organize the menu my self, with instant drag-and-drop if possible (windows has it for ages, kde only have that slow kmenuedit aplication), and then leave it that way. So I can have some degree of muscular memory, and be faster.\n\nSomeone posted an gigant new menu with various pannels, that also solve the \"drift for the right\" problem to motion impaired. That maybe intersting for me."
    author: "Manabu"
  - subject: "Dolphin dilemma?"
    date: 2007-10-17
    body: "Just wondering, in the latest Dolphin screenshots I don't see the actions sidebar (e.g. \"Compress here\", \"Mail to recipient\" etc.) I know there's nothing to worry about, but was it removed? Or is it hidden in the options somewhere? Or am I missing something?"
    author: "Darryl Wheatley"
---
In <a href="http://commit-digest.org/issues/2007-10-14/">this week's KDE Commit-Digest</a>: Replacement of the "toolbox", and a new KRunner in <a href="http://plasma.kde.org/">Plasma</a>, with many applets moving from playground into extragear in KDE SVN. <a href="http://netdragon.sourceforge.net/">SuperKaramba</a> is now fully integrated into Plasma. A move away from KDEPrint facilities, towards more basic functionality for KDE 4.0. More work on restoring the functionality of the Klipper applet. Basic sound support in <a href="http://edu.kde.org/parley/">Parley</a>. General work on <a href="http://www.khtml.info/">KHTML</a>, with more specific work on image loading and testcases. More work on KDE colour scheme handling. A rethink of device handling for <a href="http://amarok.kde.org/">Amarok</a> 2.0. Generic style saving work in <a href="http://koffice.org/">KOffice</a>. Various optimisation work across KDE. Kaider moves to extragear-sdk. Kicker is removed from KDE SVN. KDE 3.5.8 and KDE 4.0 Beta 3 (with <a href="http://en.opensuse.org/Kickoff">Kickoff</a> included as the menu option) have been tagged for release.

<!--break-->
