---
title: "Full Tilt for Krita!"
date:    2007-05-28
authors:
  - "brempt"
slug:    full-tilt-krita
comments:
  - subject: "german paypal"
    date: 2007-05-28
    body: "When I click on th Make a Donation button it leads me to the german paypal. Anyway to make it to the local paypal site?"
    author: "Patcito"
  - subject: "Re: german paypal"
    date: 2007-05-28
    body: "Oops... I forgot to change the lc tag in the paypal form from DE to EN for the dot. The paypal button on http://www.koffice.org/krita should give you an English paypal form."
    author: "Boudewijn Rempt"
  - subject: "Re: german paypal"
    date: 2007-05-29
    body: "Uh, am I doing something wrong? The site seems to mandate that I create a Paypal account (which I am NOT going to do, due to past issues with them) before they'll validate the transaction?"
    author: "S."
  - subject: "Re: german paypal"
    date: 2007-05-29
    body: "If you don't want to use paypal, you can use one of the other donation options, like direct bank transfer or a check: http://www.kde.org/support/support.php.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: german paypal"
    date: 2007-05-29
    body: "Wonderful! Thank you Boudewijn!"
    author: "S."
  - subject: "Re: german paypal"
    date: 2007-05-29
    body: "How can I specifiy that I make a direct bank transfer for \"Krita\"?"
    author: "Ronan"
  - subject: "Re: german paypal"
    date: 2007-05-29
    body: "If your bank don't allow you to add a comment on the transfer form, you can mail me (boud@valdyas.org) and I'll notify the e.V., who'll tot up your donation with the other Krita donations."
    author: "Boudewijn Rempt"
  - subject: "Re: german paypal"
    date: 2007-06-02
    body: "What's wrong with PayPal? I've been using them for years."
    author: "Scott"
  - subject: "Donated!"
    date: 2007-05-28
    body: "Donated 30 Euros; I hope others will do the same! "
    author: "Anon"
  - subject: "digg it!"
    date: 2007-05-28
    body: "http://digg.com/software/Help_the_Krita_Team"
    author: "Patcito"
  - subject: "2008 Libre Graphics Meeting"
    date: 2007-05-28
    body: "Hey, I live in Warsow, and I dont't know anything about Libre Graphics Meeting. Is there any website I can get info about it?"
    author: "Zbyszek"
  - subject: "Re: 2008 Libre Graphics Meeting"
    date: 2007-05-28
    body: "Not yet -- http://www.libregraphicsmeeting.org is still about this year's meeting. But the proposal has been made and accepted, so the organization should be getting busy."
    author: "Boudewijn Rempt"
  - subject: "Re: 2008 Libre Graphics Meeting"
    date: 2007-05-28
    body: "Actually it's not Warsow which has been proposed but Wroc&#322;aw."
    author: "Cyrille Berger"
  - subject: "Re: 2008 Libre Graphics Meeting"
    date: 2007-05-28
    body: "Even cooler :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: 2008 Libre Graphics Meeting"
    date: 2007-05-28
    body: "Yes, a Libre Graphics Meeting on German soil. :)"
    author: "Evil Eddie"
  - subject: "Re: 2008 Libre Graphics Meeting"
    date: 2007-05-28
    body: "please delete parent post by evil eddie. I was in Gdansk last week and not in Danzig as some stick-in-the-muds would prefer. And yes, I'm from Germany."
    author: "Thomas"
  - subject: "Re: 2008 Libre Graphics Meeting"
    date: 2007-05-28
    body: "Sounds more like Czech soil to me :D (not meant to offend anybody; the place certainly had a turbulent history)"
    author: "Jonathan Verner"
  - subject: "10 Euros here"
    date: 2007-05-28
    body: "10 Euros from me, I wish you all the best!"
    author: "Anon"
  - subject: "&#8364;20 from me!"
    date: 2007-05-28
    body: "Go Krita! Keep up the good work :)"
    author: "Joergen Ramskov"
  - subject: "Re: &#8364;20 from me!"
    date: 2007-05-28
    body: "Heh, that euro symbol didn't make it through :)\n\nAnyway, it was supposed to say 20 euro from me."
    author: "Joergen Ramskov"
  - subject: "Out of curiosity..."
    date: 2007-05-28
    body: "Just out of curiosity, how much does this tablet cost?  I didn't even know things like these existed until I saw a Wacom at the Apple Store a couple years ago or so, and I've been interested in them ever since."
    author: "Matt"
  - subject: "Re: Out of curiosity..."
    date: 2007-05-28
    body: "Check out this site:\n http://www.wacom-shop.net/cgi-bin/wacom.storefront/465aeedc00033f28273f50f3361905d7/Catalog/1069\n"
    author: "AC"
  - subject: "Any additional way to help too?"
    date: 2007-05-28
    body: "Is there any additional way we can help with the development. I'm asking this because my sister has a Wacom tablet (Graphire4 or something like that). So is there any other way she can help?"
    author: "Jure Repinc"
  - subject: "Re: Any additional way to help too?"
    date: 2007-05-28
    body: "Sure, test new versions of Krita for errors in tablet features. The fact that not that many people own a tablet also means there are not that many people who are able to test new code."
    author: "Eckhart W\u00f6rner"
  - subject: "Re: Any additional way to help too?"
    date: 2007-05-28
    body: "Yes and while non working tablets are rarely the fault of Krita, but a problem of configuration in Xorg or in a distributions, they also need help to test that part. And helping them is helping Krita indirectly ;)"
    author: "Cyrille Berger"
  - subject: "15 euro from me"
    date: 2007-05-29
    body: "I like Krita :)"
    author: "cookiem"
  - subject: "...and 10\u0080 from me..."
    date: 2007-05-29
    body: "I like krita too."
    author: "duckmat"
  - subject: "Ask Wakom"
    date: 2007-05-29
    body: "Hi,\n\nhave you asked Wacom for tablets? \nI know that the Gimp developers got one or two from Wacom some time ago. Perhaps you can ask Sven or Simon for the contact. \n\n"
    author: "Max"
  - subject: "Re: Ask Wakom"
    date: 2007-05-29
    body: "Actually, it may be useful to ask for a 2 for 1 kind of deal. That is, if we come up with the money for 2, they give 4.  While it is useful to have 1 freeby, it is even better to have 4 at low costs."
    author: "a.c."
  - subject: "Re: Ask Wakom"
    date: 2007-05-29
    body: "I've tried to contact Wacom several times but I didn't get a reply."
    author: "Boudewijn Rempt"
  - subject: "I don't have a graphics tablet..."
    date: 2007-05-30
    body: "... but I might one day and I use Krita so supporting it is well deserved.\n\n20 euro on its way"
    author: "Simon"
  - subject: "Somethings"
    date: 2007-05-30
    body: "Somethings broken in the world of open source. Couldn't you get the pad as a gift from the hardware manufacturer? "
    author: "Ben"
  - subject: "Why wacom?"
    date: 2007-05-31
    body: "Why only supported tablet for linux seems to be only Wacom? There are alot good tablets and even cheaper.\n\nLike http://trust.com/products/default.aspx?cat=_Current&grp=TABLETS&type=12X9-INCH&item=14070\n\nI buyed one for my sister who used windows. On that time it didnt work on linux. After OpenSUSE 10.2 i tryed it again and it got reconized and worked as normal mouse. But pressing dont work as should  (512 levels).\n\nAnd it is good tablet, draws straight line with ruler and works great (on windows side ;-)).\n\nAnd it is very cheap for A4+ size, only 75 euros so many art students could afford it."
    author: "Fri13"
  - subject: "Re: Why wacom?"
    date: 2007-07-02
    body: "Being the underdogs, those guys, at least, should be willing to provide development hardware."
    author: "Lee"
  - subject: "Sorry.."
    date: 2007-06-01
    body: "I wish I had money, but I don't. When I have money, I'd like to donate ^^;"
    author: "Amy Rose"
  - subject: "A good thing to donate for"
    date: 2007-06-01
    body: "\"Natural painting\" is still one of the things I'd like to see on the OpenSource segment. I hope someday it will be a good alternative to CorelPainter. And if it's this promising as krita or the whole koffice package, I don't have a doubt about this ;-). This is definitely worth a donation."
    author: "soriac"
  - subject: "Re: A good thing to donate for"
    date: 2007-06-01
    body: "Yesterday Emanuele Tamponi committed the first bits of code for the paint mixer palette :-)"
    author: "Boudewijn Rempt"
  - subject: "Does anybody even use Krita?"
    date: 2007-06-02
    body: "Every time I'm at http://kde-look.org, the artists there say they created their work with The GIMP (or Inkscape in the case of SVG - leaving Karbon14 in the cold as well).\n\nI've yet to come across anybody anywhere (outside of this thread I suppose) who actually says they use Krita.\n\nI've got no artistic talent so I barely use any of this stuff. :)\n\nPersonally I think you should hit up the Trolltech folks for donations. :)"
    author: "Scott"
  - subject: "Re: Does anybody even use Krita?"
    date: 2007-06-02
    body: "I regularly have contact with a couple of artists who use Krita. Most of them do so because the tablet handling in the 1.x releases fits their way of working best. And, well, a user base is something that has to grow. Especially for graphics applications users aren't eager to switch at the drop of a pin."
    author: "Boudewijn Rempt"
  - subject: "Natural Paint Lover"
    date: 2007-07-02
    body: "I don't have the funds now to donate but really want too. Before I discovered Krita I had to rely on painter for my painting.IMO-GIMP = Photoshop (style) and Krita = Corel Painter (style) and I prefer Painter's style over Photoshop's anyday. Oh and for the person wondering who uses Krita, I do. As well as Inkscape, Karbon, Blender,and a plethora of other gfx apps. To the developers PLEASE ADD A CUSTOMIZABLE WAY (HOTKEY) TO RESIZE YOUR BRUSH SIZE INTERACTIVELY (i.e.like painter (so I can finally ditch windows ;-)   )\n\nOh and the color mixer would be cool too :D"
    author: "Darth Omnious"
  - subject: "Re: Natural Paint Lover"
    date: 2007-07-02
    body: "I saw the first version of the color mixer work yesterday :-). And yes, we'll try to add the hotkey to resize the brush size for 2.0."
    author: "Boudewijn Rempt"
---
Krita development is in a crucial phase, we are adding fun, useful and 
amazing stuff at a stunning rate. But there are things that no Krita 
developer can do, because we lack the proper hardware. Krita's 
renaissance started with a simple Wacom Graphire tablet, and it led to some 
great new possibilities. But to implement support for modern tablet features like tilt and stylus rotation we need to buy more advanced tablets and art pens, and to make that possible, we need your help!








<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right; width: 400px;">
<img src="http://www.koffice.org/krita/tablet.jpg" height="275" width="400" />
<br />The tablet that started it all
</div>

<p>It all started with this simple tablet, but now, to make Krita support modern painting features like stylus tilt, rotation and airbrush rate we need a set of more advanced tablets. And those are costly!</p>

<p>Ideally we would have at least two Intuos tablets, the size
does not matter much for development, with two 6D art markers (for rotation) 
and two airbrushes (for rate). We need two tablets because that way two 
developers can hack and test each others work. That way we will not have the 
"but it worked for me" situations right after a release that we all hate so much!</p>

<p>And we do not want to wait too long: to get these features out with 
KOffice 2.0, and not <a 
href="http://techbase.kde.org/Schedules/KOffice_2.0_Release_Schedule">delay 
the release</a>, work has to start soon. In fact, Emanuale Tamponi, our Google Summer of Code student working on natural painting has almost finished his preliminary research and is panting to start coding! Thus we ask you, our users, if you value Krita and want us to develop innovative, first-grade  painting and drawing capabilities, please help us out!</p>

<p>We have asked the KDE e.V. to aid us with the financial-service part, and you 
can donate on the <a href="http://www.kde.org/support/support.php">KDE
Donation page</a>. If you donate through the button below your donations will be automatically tagged for Krita. If you donate through a bank order or US cheque, please mention it is for Krita.</p>

<p>And do not worry, if there is more money than we need for tablets, it will be spent for Krita, for instance by getting the Krita developers to Warsaw for the 2008 Libre Graphics Meeting.</p>

<p align="center">
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="kde-ev-paypal@kde.org">
<input type="hidden" name="item_name" value="Krita">
<input type="hidden" name="item_number" value="2000">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="cn" value="Your message here:">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="tax" value="0">
<input type="hidden" name="lc" value="EN">
<input type="hidden" name="bn" value="PP-DonationsBF">
<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
</p>








