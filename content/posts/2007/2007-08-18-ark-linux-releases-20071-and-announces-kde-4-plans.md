---
title: "Ark Linux Releases 2007.1 and Announces KDE 4 Plans"
date:    2007-08-18
authors:
  - "djensen"
slug:    ark-linux-releases-20071-and-announces-kde-4-plans
comments:
  - subject: "Ark Linux"
    date: 2007-08-17
    body: "How good is Ark Linux compared to others?  I've mostly been using kubuntu and debian.  They are nice, but I wonder if a kde-centric approach would work better for me."
    author: "Level 1"
  - subject: "Re: Ark Linux"
    date: 2007-08-18
    body: "well, in the year 2005 mepis was a good choice. they have decided to go back to debian so i hope they will return their fame"
    author: "nick"
  - subject: "Re: Ark Linux"
    date: 2007-08-19
    body: "They are working hard on that. The last 'quickies' contained a link to the mepis KDE 4 live DVD warren has build. They are aiming at a perfectly integrated KDE 4 in as short a time as possible ;-)"
    author: "jospoortvliet"
  - subject: "Re: Ark Linux"
    date: 2007-08-21
    body: "Ark Linux is great! It's the only one that detected my video card (the others wouldn't start the X server). It runs very fast. And, not to mention, it's how I learned Linux.\n\nAlso, I'll add, it's a multi-purpose distro. It's not specific to one task, you can use it for just about anything. I have one computer setup as a home file server and another set up as a gaming machine."
    author: "Pikidalto"
  - subject: "Broken FAQ"
    date: 2007-08-17
    body: "I wanted to learn more about Ark Linux, but the FAQ was broken. Notably the answer to \"How is Ark Linux different from other distributions?\" didn't exist.\n\nI checked the Wiki, but couldn't find a simple FAQ entry, or an equivilent. I'm going to read further about installation, and running the distro, but it would be nice to get that info quickly on their website."
    author: "Soap"
  - subject: "Re: Broken FAQ"
    date: 2007-08-17
    body: "On behalf of the Ark Linux team, i am very sorry for this FAQ business! It is fixed now, and it was entirely my fault. The FAQ is on the website, not the wiki, and it was my fault that it broke - i have made a temporary workaround, which makes it work, the URLs are just not as pleasant :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Donations"
    date: 2007-08-18
    body: "Thanks for you and the team btw! \nI see you guys are asking for donations. I won't mind send you a few bucks, but I couldn't find your budget page. I guess you don't share it publically? I don't care about personal acknowledge (I use a pseudonym), but I feel more comfortable knowing the donative is for a good cause (consider something simple like http://www.libsdl.org/finances.php ). Cheers!"
    author: "fast penguin"
  - subject: "Re: Donations"
    date: 2007-08-18
    body: "Sure, we *would* share it publicly... The problem with releasing a budget is just that well... you need a budget to be able to do that :) We simply haven't got enough money for that. But to say what the money go towards, it's paying for the domain name, and the rest goes towards keeping bero alive :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Donations"
    date: 2007-08-21
    body: "Well, we can be more detailed ;)\n\nTotal donations so far (in the 6 years we've been up and running) are 327 EUR.\n\nOf those, we've spent $20.85 per year on the domain names (arklinux .org/.com/.net)\n\nWe've spent 80 EUR on a new harddisk for our server.\n\nWe currently have 230 EUR sitting in our paypal account. I probably won't have to take anything off for food in the next month. We'd like to get a faster build machine some day, but you just don't get a decent box for what we have.\n"
    author: "bero"
  - subject: "Re: Donations"
    date: 2007-08-21
    body: "If I actually had some money in my own bank account, I'd be more than willing to donate, but as it is, my bank account is dried up from the first day of summer (my mother doesn't have a summer job-she only works during the other three seasons). If I can get some money saved up this winter, I'll donate."
    author: "Pikidalto"
  - subject: "Re: Broken FAQ"
    date: 2007-08-18
    body: "Very impressed that you responded to that so quickly, definitely gonna try ark out just for that :)."
    author: "Roderic Morris"
  - subject: "Re: Broken FAQ"
    date: 2007-08-18
    body: "Hehe, thanks, it's good to know i made a difference! ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Broken FAQ"
    date: 2007-08-18
    body: "That was a very fast response. Thank you, it was helpful.\n\nI'm installing Ark right now. I'm really impressed at how easy and fun (tetrix) it is to install. Great job including information about how to use the interface for the absolute beginners out there."
    author: "Soap"
  - subject: "World of Warcraft & Video Card Actually Detected"
    date: 2007-08-21
    body: "World of Warcraft runs great on Ark Linux. In fact, it's the only distro that I can play WoW on because the other distros that I've tried don't detect my video card."
    author: "Pikidalto"
  - subject: "mnadriva will Announces 2008"
    date: 2007-08-22
    body: "and the user can see KDE 4 in action "
    author: "shay"
---
The KDE centric distribution <a href="http://www.arklinux.org/">Ark Linux</a> has <a href="http://www.arklinux.org/user/admin/blogger&blogger_entry=admin.1187352000">announced the 2007.1 release</a>, which includes KDE 3.5.7, the newly released Amarok 1.4.7, much better support for encrypted wireless connections out of the box, in addition to a large number of updates to software and drivers. This will be the last release with KDE 3, from the announcement: "<em>The Ark Linux team already has plans for KDE 4 and Ark Linux 2007.2, one of our goals is to integrate KDE 4 as deeply into the system as we can; for example by rewriting our hotplug system to integrate with KDE's 'Solid' and using KDE 4 to give the user immediate access to a newly plugged in device.</em>"


<!--break-->
