---
title: "Quickies: Awards to Enter, Magnatune Hires Amarok Developer, and an Old Interview from the Time Machine"
date:    2007-08-03
authors:
  - "tunrau"
slug:    quickies-awards-enter-magnatune-hires-amarok-developer-and-old-interview-time-machine
comments:
  - subject: "Typing error"
    date: 2007-08-03
    body: "I would assume \"International Fee Software Awards\" should be \"International Free Software Awards\""
    author: "NicolaiM"
  - subject: "Re: Typing error"
    date: 2007-08-03
    body: "I'm pretty sure Libre translates to Free, so yes."
    author: "T. J. Brumfield"
  - subject: "Re: Typing error"
    date: 2007-08-03
    body: "Fixed, thanks.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Typing error"
    date: 2007-08-03
    body: "Wow, that's a nasty typo- I wonder if it's in my original submission? \n\n*checks*\n\nNope - I think I'll bring out my giant rubber mallet now for the editors.\n\n/me goes off in search of Danny, mallet in hand. :)"
    author: "Troy Unrau"
  - subject: "Re: Typing error"
    date: 2007-08-03
    body: "Tsss, be nice to Danny, I'm pretty sure he corrects much more mistakes than he introduces ;-)\n\n/me slaps Troy"
    author: "jospoortvliet"
  - subject: "Fantastic interview"
    date: 2007-08-03
    body: "The interview with Matthias is a very nice read. The most insightful thing I got from it is that the free software desktop has stayed so constant over all these years. We are still hoping to take over the computers of the world and it is just as likely as it was then.\n\n"
    author: "Jos"
  - subject: "google cache of interview"
    date: 2007-08-04
    body: "in case that the linux center site doesn't work for you, the google cache link is: http://209.85.129.104/search?q=cache:Gg9M67puKcYJ:www.linux-center.org/articles/9809/interview.html+ettrich+interview+linux-center"
    author: "alterego"
---
A few quickies again this week: the <a href="http://tropheesdulibre.org/?lang=en">4th Trophées du Libre</a> (International Free Software Awards) contest is open. Please consider submitting your favourite KDE applications since the award is some &#8364;3000 in each category. Also new this week: Nikolaj Hald Nielsen has <a href="http://amarok.kde.org/blog/archives/462-Hired-by-Magnatune!.html">announced</a> that he is being hired full time to work on Amarok, courtesy of the Magnatune music store. (Don't worry, this doesn't exclude support for other music stores).  PyQt <a href="http://www.riverbankcomputing.com/pipermail/pyqt/2007-July/016821.html">released a new version</a> of their bindings</a>.  And every once in a while, we stumble upon an old piece of KDE history that just needs to be shared. Check out this <a href="http://www.linux-center.org/articles/9809/interview.html">1998 Interview</a> with KDE's grand-daddy: Matthias Ettrich.
<!--break-->
