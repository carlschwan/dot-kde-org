---
title: "Amarok Weekly News #8 released"
date:    2007-04-20
authors:
  - "Ljubomir"
slug:    amarok-weekly-news-8-released
comments:
  - subject: "File properties KPlayer style"
    date: 2007-04-20
    body: "I cannot find where I can set things so that Amarok will remember them for the individual file like KPlayer does in its File Properties. Like if I want to have some presets that I want to be used only for some specific songs but not for any of my other music... Any chance to implement this?"
    author: "aha"
  - subject: "Nice issue"
    date: 2007-04-20
    body: "Hey Ljubomir, thanks for another informative issue :)  \n\nI'm personally looking forward to what QGraphicsView can do for Amarok and Plasma too.  Last I heard, work was being focused on getting user interface elements to work well within qgv... if amarok is attempting to embed widgets into the graphics view, perhaps the developers should harass the plasma people to see how they're doing it, if they haven't already. :)"
    author: "Troy Unrau"
  - subject: "Re: Nice issue"
    date: 2007-04-21
    body: "Another nice thing about QGraphicsView is that it allows us to do animations. Given that we've always loved iCandy in Amarok, we're having a lot of fun with adding little touches like smoothly collapsing text boxes.\n\nAlso we've added a funky animation on startup, which could possibly replace the splash screen."
    author: "Mark Kretschmann"
  - subject: "Amarok"
    date: 2007-04-20
    body: "Can Amarok compete with iTunes once it is ported to Windows? We'll see.\n\nOne reason why Amarok is second class is that with iTunes imports of Cds is so easy. what do we have for KDE? KAudiocreator, a usability hell. Or KsCd for playing CDs which I never run to play CDs on a default installation without tweaks.\n\nAmarok has the potential to replace the crap as iTunes does and go beyond. How good Amarok really performs, we'll see when its ported."
    author: "andre"
  - subject: "Re: Amarok"
    date: 2007-04-20
    body: "K3b can rip CDs I think. Could be wrong though. But you're right, it should be integrated into Amarok somehow. Perhaps K3b/KAudiocreator and Amarok could share music naming info (i.e. the ~/Music/%artist/%album/%artist - %title.%ext)"
    author: "Tim"
  - subject: "Re: Amarok"
    date: 2007-04-22
    body: "I find Amarok and K3B are intercrated just right. You just goto the playlist menu and pick burn cd k3b comes right up with the playlist in k3b and all you have to do is click BURN.\nBy De'Wayne"
    author: "De'Wayne"
  - subject: "Re: Amarok"
    date: 2007-04-20
    body: "you can just drag the stuff with audio cd on your drive - they appear already as mp3 and are encoded on copy ...\n\nits wont get any easier than that."
    author: "funnyfanny"
  - subject: "Re: Amarok"
    date: 2007-04-23
    body: "I disagree. Because when using Amarok, I don't care about files - I only care about music. Amarok is doing the file-stuff for me. That's part of why it's a great application.\nSo I don't have the idea to use the filemanager to add the files. And what do I do next to add the music? Well - use amarok anyway to import the music into my collection (because amarok manages my music files for me).\n\nI have a music collection, and I have a musicmanager. If I want to add new music, I'd expect my manager to do it.\n\nAnd it should be really easy for Amarok to use the audio-kio-slave. \n\nFor those, that manage their music by themselves, this may bot be a big issue. But people using Amarok as manager too, this would be a great help.\n"
    author: "Birdy"
  - subject: "Re: Amarok"
    date: 2007-04-21
    body: "KAudiocreator works fine for me. Put in a CD, the album and track names appear, click two buttons and it rips in your favourite format. Plus you can customise everything, such as the naming of of files and the encoding parameters, if you like.\n\nIf you hate computers you can even set it to rip automatically as soon as the CD is inserted, provided the FreeDB lookup succeeds."
    author: "Martin"
  - subject: "Re: Amarok"
    date: 2007-04-21
    body: "Kaudiocreator works but usability wise it is awful. This also includes the configuration. Usually you want three different issues: lossless, ogg or mp3, and with compressed: quality or size optimized. But you don't have to make these choices each time you rip a CD.\n\nWith iTunes it goes like this: You select your default rip format and all the stuff in the settings. \n\nInsert a CD, tracks are displayed. You can play the CD or you press a button to rip the files and automatically add them to your collection. Perfect. Same goes for the player functionality."
    author: "bert"
  - subject: "Re: Amarok"
    date: 2007-04-22
    body: "Incidentally, what you describe is exactly how KAudiocreator works. Are you complaining about the possibility to set up more than one compression scheme in the configuration? Just delete all but one if you don't like that.\n\nI have no idea what you mean by \"usually you want three different issues,\" each of which apparently at two quality settings. Do you mean that you usually create five versions of every file that you rip?"
    author: "Martin"
  - subject: "Re: Amarok"
    date: 2007-04-23
    body: "His point is you shouldn't need to fire up KAudiocreator, Amarok should do it for you. Not sure I fully agree with him, but I think it should be possible to drag the files from the playlist in amarok to your collection or something."
    author: "superstoned"
  - subject: "Re: Amarok"
    date: 2007-04-23
    body: "Actually I create 3 different versions:   .flac (lossless) which I store at home on a large disk - as a backup for when (not if) my CDs get scratched to unplayability.   .ogg with high quality for my laptop (which doesn't have enough space for all my music in .flac, but has plenty for .ogg versions), and .mp3 at fairly low quality that I burn to CD for my car.  \n\nI'm sure people with more music devices need more formats customized to the right compromises for each device."
    author: "Henry Miller"
  - subject: "Re: Amarok"
    date: 2007-04-21
    body: "not really, we saw Amarok performs very well\n"
    author: "Marc Collin"
  - subject: "Re: Amarok"
    date: 2007-04-22
    body: "Well, KAudioCreator and KsCD are both based on standard widgets in the KDE Multimedia module.  Give them a polish and improve their usability and it wouldn't be hard for Amarok and K3B to re-use them.  A while back I did a redesign to merge both KAC and KsCD into one app with what i thought was improved usability, but Aaron shot it down as less usable.  Ah well, maybe one day I'll just do it anyway :-)\n\nMy main complaint is that the current widgets while having all the features needed implement them in a nightmare GUI that just confuses uses.  It needs a good clean-up, better config options, and wizards to walk the newbie through the process.  Also, when you enter a CD, at least now you get the pop-up to choose between KAC and KsCD, but its still a pain to switch between them, or to have both open and fighting over  the CD.\n\nJohn."
    author: "Odysseus"
  - subject: "Player window"
    date: 2007-04-20
    body: "Please please please do not remove the player window!!! Amarok would not be the same without it... :("
    author: "JakubZ"
  - subject: "Re: Player window"
    date: 2007-04-20
    body: "Instead of the player window (which had been unmaintained for quite some time), you could use a Plasma widget (like Superkaramba). Thanks to our extensive dbus interface it's fairly simple to do this."
    author: "Mark Kretschmann"
  - subject: "Re: Player window"
    date: 2007-04-20
    body: "Hopefully something like the player window will be available in Amarok 2 (whether the player itself of a plasma widget). It makes the app much more 'alive' - without being too distractive.. I've gotten so used to it, I cannot even imagine listening to music without it.."
    author: "JakubZ"
  - subject: "Re: Player window"
    date: 2007-04-22
    body: "yeah, i agree i dont need the playerwindow as i minimize amarok in the tray for playing , but i like the player window really very much - please dont remove it for me"
    author: "wingsofdeath"
  - subject: "Re: Player window"
    date: 2007-04-22
    body: "I'd like to see amarok incorporate a toolbar button or keyboard shortcut to TOGGLE back and forth between the playlist window and the mini-player (like iTunes does).  It seems pretty useless to have both of them on-screen at the same time, and I think this would be a lot nicer than the current setup."
    author: "Anonymous Coward"
  - subject: "Re: Player window"
    date: 2007-04-23
    body: "...did you miss the part where the player window is being removed?"
    author: "Ian Monroe"
  - subject: "Re: Player window"
    date: 2008-08-20
    body: "\nremoving the player window completely is a terrible mistake!\nnot everyone wants his whole screen occupied with amarok\nneat little widget would be great!"
    author: "toudi"
  - subject: "HTML inadequate"
    date: 2007-04-21
    body: "From the article:\n'At the time of Amarok 1.0 the idea was innovative and technology (HTML/CSS) was adequate. As times moved on, and rich &#8220;Web 2.0&#8221; services began to show up, the current solution seemed more and more limited.'\n\nI don't know how this was figured. Web 2.0 applications are written in HTML/CSS/JavaScript... how can the current solution (a rendering widget with support for those technologies) appear limited in that light? Smooth animations are both possible and easy in with HTML/JavaScript.\nWhat is the real reason the KHTMLPart was dropped?"
    author: "Cerulean"
  - subject: "Re: HTML inadequate"
    date: 2007-04-21
    body: "Take a look at contextbrowser.cpp from Amarok 1.x, and you should see what \"the real reason\" is. Embedding HTML code in C++ is a mess.\n\nQGraphicsView is a lot nicer to program with, allows to do animation, scaling, SVG rendering and more. It's much more suitable for what we want to achieve.\n"
    author: "Mark Kretschmann"
  - subject: "Re: HTML inadequate"
    date: 2007-04-21
    body: "And if you want you can even put JavaScript, erm.. QtScript in it and script your gfx the way you like!\nLong life QGraphicsView, huge kudos to AMAROK !!!! (and the sexy amarok programmers!)\n\n"
    author: "alphaman"
  - subject: "Re: HTML inadequate"
    date: 2007-04-22
    body: "Yes, for future reference as Amarok developers we love praise on how sexy we are! It makes us code sexier too!"
    author: "Seb Ruiz"
  - subject: "O_O"
    date: 2007-04-21
    body: "I really like the upperpart of the new GUI (with bookmarks!).\nGreat work !"
    author: "shamaz"
  - subject: "Suggestion for the Playlist Redesign"
    date: 2007-04-21
    body: "The new design (http://amarok.kde.org/wiki/Image:Justplaylist.png) is okay, but it seems less functional! One of the powerful things about the current playlist is it lets you sort through your music by attributes. You can reorder songs reorder by length, or track number, or year, or composer. (or bitrate or file size or - well, you get the idea.) It's part of what makes Amarok a powerful \"music manager\" and not just an \"audio player\". What if you added a button with a drop down (or pop up) menu that lets you reorder tracks by attribute? That way users could still reorder (sort) songs easily.\n\nAlso, rather than the \"playing\" marker with difficult-to-read-veritcal-text, why not use the cool blue pulsing effect Amarok currently uses? I really like it."
    author: "kwilliam"
  - subject: "Re: Suggestion for the Playlist Redesign"
    date: 2007-04-23
    body: "Don't worry, there's a plan to do that - the playlist is simply not being fiddled with at the moment because it's a summer of code project :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Suggestion for the Playlist Redesign"
    date: 2007-04-23
    body: "I find the pulsing blue marker kind of annoying. Glad to know someone likes it. ;)\n\nIf you read my plan, I'm very aware of the issues of the mockup. Figuring out solutions is part of the project."
    author: "Ian Monroe"
  - subject: "Cool stuff, but what about...?"
    date: 2007-04-22
    body: "- Tabbed playlists\n- A pub/fullscreen mode\n- Recording / capturing streaming media to disk\n- Volume normalization without a need for scripts\n- A better use of covers (maybe similar to Cower Flow)\n\nWill any of this make it into Amarok 2.0?\n\nKeep up the good work, very much looking forward to next stable branche release!   :)"
    author: "hummingbird"
  - subject: "Fish"
    date: 2007-04-22
    body: "\"Also a nice side effect of using Phonon engine, besides playing video files, is the full support for KIO protocols, like ftp or fish.\"\n\nAh, wonderful. I will be able to listen easily to my music when i am away from home, with just an internet connection. This will be a great feature! Thank you in advance. :)"
    author: "Med"
  - subject: "Re: Fish"
    date: 2007-04-22
    body: "Of course KPlayer has been able to do that for what like three years?\n\nSince I found it back then, I haven't had any use for any other media player!"
    author: "niko"
  - subject: "Re: Fish"
    date: 2007-04-23
    body: "And actually Amarok had this feature back in the Amarok 1.1, Amarok 1.2 series. It got removed since KIO didn't allow seeking back then and it wasn't worth supporting.\n\nKIO supports seeking for KDE 4 though. I guess this is really what the news is. :)"
    author: "Ian Monroe"
  - subject: "Amarok"
    date: 2007-04-22
    body: "Does Amarok also handle CUE files for playing? They are essentially very simple playlists with marks to the associated image files.\n\nI can open the flac image with the whole CD image. But not the cue file. Is there any way to mount the Cue file as if it was a CD? Or to automatically split the large image file?\n\nPERFORMER \"GY&#65533;GY CZIFFRA\"\nTITLE \"Great Pianist 20th Century - CD2\"\nFILE \"Great Pianist 20th Century - CD2.flac\" WAVE\n  TRACK 01 AUDIO\n    TITLE \"Etudes Op 10 No 1 in C\"\n    INDEX 01 00:00:00\n  TRACK 02 AUDIO\n    TITLE \"Etudes Op 10 No  2 in A minor\"\n    INDEX 01 01:53:00\n...\n\n  TRACK 25 AUDIO\n    TITLE \"Polonaise No 6 in A flat, Op 53 'Heroic'\"\n    INDEX 01 57:45:00\n\nAs far as I know K3B supports these CUE files pretty well. But when you don't want to burn them, just add the tracks to your collection, how do you proceed?"
    author: "Rock"
  - subject: "Re: Amarok"
    date: 2007-04-23
    body: "I'm afraid you currently have to use k3b to convert the file to a iso, and mount that. As K3B is working on KIOslaves (now fully supported by Amarok), this might change for the good ;-)"
    author: "superstoned"
  - subject: "Re: Amarok"
    date: 2007-04-23
    body: "Amarok's done cue files since a while now - not by separating the file into tracks in the collection, but by showing the cue file markers in the context browser when you're playing the song :) Technically, the cue file's filename has to be exactly the same as the music file (so that if you have the file \"Great Pianist 20th Century - CD2.flac\" your cue file must be called \"Great Pianist 20th Century - CD2.cue\" - we don't actually look for the cue file before the track is loaded, so we can't use the cue file's information for this purpose)\n\nThe idea behind this methodology is fairly simple... If you've got a cue file for something, it is almost always because the tracks are so intertwined that cutting them up would make no sense - so we basically view the cue file entries as a set of bookmarks (like the red markers up in the mockup in this post you hijacked for this support question ;) - might i suggest #amarok on freenode or the forums next time 'round?)"
    author: "Dan Leinir Turthra Jensen"
---
A <a href="http://ljubomir.simin.googlepages.com/awnissue8">new issue</a> of the <a href="http://amarok.kde.org">Amarok</a> newsletter is out. It talks about interesting new developments, Amarok's <a href="http://amarok.kde.org/en/node/226">Summer of Code</a> projects, the current events in the 1.4 stable branch, and continues to provide cool Amarok-related tips.






<!--break-->
