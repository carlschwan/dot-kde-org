---
title: "People Behind KDE: Daniel Molkentin"
date:    2007-01-18
authors:
  - "dallen"
slug:    people-behind-kde-daniel-molkentin
comments:
  - subject: "awesome"
    date: 2007-01-18
    body: "I just noticed that this series is in its third season and on a new domain to boot.  Glad to see People Behind KDE alive, and to be reading this interview.  \n\nAmazing to think that Daniel was just 15 years old when he first heard of KDE!"
    author: "KDE User"
  - subject: "Re: awesome"
    date: 2007-01-18
    body: "Hi, I met Daniel at the KDE anniversary in Esslingen. He really is a nice guy with great visions for KDE. "
    author: "Bernhard Rode"
  - subject: "Bullets are great!!!"
    date: 2007-01-18
    body: "Thanks Daniel for changing stars to bullets.. they really look cool with password fields..\n\nFrugalware 0.6rc2 and kubuntu feisty 7.04 already has your contribution :)"
    author: "fast_rizwaan"
  - subject: "Re: Bullets are great!!!"
    date: 2007-01-18
    body: "There is no Kubuntu Feisty 7.04 yet and every distro release or development version shipping KDE 3.5.6 will feature that."
    author: "Anonymous"
  - subject: "Re: Bullets are great!!!"
    date: 2007-01-19
    body: "And FC6 will almost certainly get 3.5.6 as an update, so unless you're using the development version of Feisty or backported packages from kde.org, we'll likely get it before you. :-)"
    author: "Kevin Kofler"
  - subject: "hah."
    date: 2007-01-19
    body: "using gentoo compiling kde from svn sources I for one have to say: I alreasy 0wnz it. w00t."
    author: "eMPe"
  - subject: "Re: hah."
    date: 2007-01-19
    body: "It was in my Windows desktop ages ago [ducks for cover]\n\nMust have been ages ago because it's been ages since I had a Windows desktop\n\nDanimo - I hope you did a patent search before you introduced this feature? We wouldn't want to be infringin on MS valuable IP\n\n;-)"
    author: "Simon"
  - subject: "Re: hah."
    date: 2007-01-20
    body: "In case of any patent problems I would suggest to replace the bullets with a lower case 'o'. What do you think?"
    author: "MM"
  - subject: "Re: hah. hah, haha"
    date: 2007-01-19
    body: "That makes two of us, Gentoo+KDE+k3b+mplayer+beryl=PEFECTION"
    author: "Joshua Austill"
  - subject: "Site down"
    date: 2007-01-23
    body: "People Behind KDE seems to be taken down by script kiddies."
    author: "ac"
  - subject: "Re: Site down"
    date: 2007-02-02
    body: "Yup - fixed soon after though.\n\nThanks,\nDanny"
    author: "Danny Allen"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we meet a developer who has unfinished business with midges, someone who prefers bullets to stars - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/danimo/">Daniel Molkentin</a>.


<!--break-->
