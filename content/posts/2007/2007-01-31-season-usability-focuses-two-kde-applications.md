---
title: "Season of Usability Focuses on Two KDE Applications"
date:    2007-01-31
authors:
  - "sk\u00fcgler"
slug:    season-usability-focuses-two-kde-applications
comments:
  - subject: "Basket looks great!"
    date: 2007-01-31
    body: "Hey, I've never even heard of Basket before, but it looks featureful and useful.  I'll have to install it on my laptop to test it out."
    author: "Troy Unrau"
  - subject: "Cross-machine Synchronisation?"
    date: 2007-01-31
    body: "If Basket becomes able to cleanly sync my baskets across machines, then it may well have solved all the note-taking issues that have been plaguing me for so many years.\n\nThanks to all the lads and ladies working on this; you continue to make my day! :)"
    author: "Jeff"
  - subject: "Why not increase usability by reducing bugs?"
    date: 2007-01-31
    body: "OK, It's not really basket-related, but IMO a bug-fixing session would do much more for usability than adding new features to new applications.\n\nthe number of kmail annoyances alone should keep a number of peoplpe quite busy for a while."
    author: "Mathias Homann"
  - subject: "Re: Why not increase usability by reducing bugs?"
    date: 2007-01-31
    body: "Bad usability is a bug. By improving usability, they're fixing bugs."
    author: "Erik Hensema"
  - subject: "Re: Why not increase usability by reducing bugs?"
    date: 2007-02-01
    body: "Assuming there is a correct \"usability\" answer to everything _is a bug_"
    author: "Allan Sandfeld"
  - subject: "Re: Why not increase usability by reducing bugs?"
    date: 2007-01-31
    body: "well, yes, but then there would have to be a number of ppl working on kmail, and there simply aren't. the kde pim apps don't get much love, esp kmail. mailody is there for a reason, the kmail codebase is pretty hard to get started with, let's hope mailody will be good enough soon to replace kmail with a clean and nice base."
    author: "superstoned"
  - subject: "Re: Why not increase usability by reducing bugs?"
    date: 2007-01-31
    body: "mailody: you mean this here? http://www.mailody.net/\n\nsounds interesting enough. when it's capable to use more than one imap account, i'll look into it. not before.\n\n"
    author: "Mathias Homann"
  - subject: "Re: Why not increase usability by reducing bugs?"
    date: 2007-01-31
    body: "Right, so -- as parent comment suggested -- *get* some people working on fixing bugs. The PIM people welcome new contributors and bugfixers with open arms. There is mentoring available. Certainly the KMail code has an undeserved reputation for horrible complexity. Yes, it's complicated. Yes, it takes dedication to get into it. But it's still not rocket science.\n\nMailody *can't* replace KMail because -- for one thing -- Mailody makes the explicit design decision to support IMAP only."
    author: "Adriaan de Groot"
  - subject: "Re: Why not increase usability by reducing bugs?"
    date: 2007-01-31
    body: "indeed, but if it's ported to akonadi one day, it'll support everything... And afaik, tom is planning to do that."
    author: "superstoned"
  - subject: "Re: Why not increase usability by reducing bugs?"
    date: 2007-01-31
    body: "You mean like the 19 bugs fixed in 3.5.6? Or the ~150 bugs fixed post KDE 3.5.0 but pre-3.5.6?"
    author: "Carsten Niehaus"
  - subject: "Re: Why not increase usability by reducing bugs?"
    date: 2007-01-31
    body: "Organizing a bughunting weekend is pretty straightforward. Bram Schoenmakers does bug *triage* (which is different: it cleans up bugzilla and is a good preparation for a hunting weekend), but organizationally it's similar. Announce on dot. Use mailing list kde-quality to coordinate. Make sure everyone has a checkout before the weekend starts. Claim an IRC channel on freenode (#kde-bugs or #kde-hunting). Get started. Verify bugs. Create test case. Oh, having a hot build cluster ready helps too. Nothing like twiddling your thumbs while PIM compiles, eh.\n\nNo, really. There's a lot that folks can do to push things forward. Or back, depending on the phraseology."
    author: "Adriaan de Groot"
  - subject: "bad site"
    date: 2007-01-31
    body: "fine and dandy, but the basked openusability site looks really horrible in konqueror. and i just tested it in IE6, and it looks good there... tssss..."
    author: "superstoned"
  - subject: "Re: bad site"
    date: 2007-02-02
    body: "In my konqueror (3.5.6), the site looks ok - most of the times. Sometimes I have to reload so Konqueror uses the CSS properly. Seems to be a Konqueror bug. Can you send me a screenshot so I can check if it's the same problem? Thanks.\n\nBelieve me, the site isn't designed for IE, I don't even have one ;-) \n"
    author: "Frank Ploss"
  - subject: "Re: bad site"
    date: 2007-02-07
    body: "btw, it also has text \"This multi-purpose note-taking application can helps you to:\" now - probably should be either 'can help' or 'helps' ;)"
    author: "richlv"
  - subject: "Openusability"
    date: 2007-01-31
    body: "I wish it won't happen what happened with kmail: <br> http://www.openusability.org/reports/get_file.php?group_id=55&repid=43#search=%22kmail%20openusability%20%22mailing%20list%22%22\n<br><br>\n\nSometimes selfdefining usability experts have incomprehensible point of view, and not very well meaning to who asks a revision...<br>\nhttp://lists.kde.org/?l=kde-usability&m=116057387701141&w=2 "
    author: "Murdock"
  - subject: "Re: Openusability"
    date: 2007-01-31
    body: "And that's why OpenUsability experts assist KDE developers. OpenUsability has a bunch of professionals in that area.\n\nAnd I agree that some self-defined 'experts' are harming the reputation of the usability folks."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Openusability"
    date: 2007-01-31
    body: "I still think that that usability \"improvement\" was a step back. I still have trouble finding some of the options that belong (and were) in the properties dialog. Ah, well. I guess I'm not an expert, only an actual daily user. "
    author: "Andr\u00e9"
  - subject: "Re: Openusability"
    date: 2007-01-31
    body: "I agree, i don't understand why such a countertrend dispositions should improve usability. Is a common feature to have settings in tabs, the scope of that study is to eliminate tabs, scattering the contents in the main user interface.\nI'm not an usability expert, too, I'm a user. I agree users are not designers, but I'm sure designers MUST be users. Maybe the author of that usability study doesn't use kmail, or at least doesn't use often that features he moved elsewere."
    author: "Murdock"
  - subject: "Re: Openusability"
    date: 2007-01-31
    body: "I think those changes are mostly an improvement.  Except I don't understand the rationale of \"only about 1% of users use the mailing list functionality, so lets move it out of the properties and into a right click menu\".    A feature that few people need is now more visible to the end user just because there is some crusade to get rid of the tabs in the properties dialog.  I doubt most users would ever check the properties dialog, so I don't see a problem with advanced settings being hidden there.\n\nOf course, the bigger problem is that usability experts, developers, and armchair critics are all making claims about what features the average user needs, without any real data to back them up.  What KDE really needs is a centralized way to automatically store and submit usage data that people can opt-in to.  Then we will start making progress with respect to deciding what features are important."
    author: "Leo S"
  - subject: "Re: Openusability"
    date: 2007-02-09
    body: "Go back a couple posts (actually I assume it was you I was replying to); here's the key phrase:\n\n\"I don't know if this is a new usability trend, but I think that's generally a \nbad idea. Usability, in my opinion [...]\"\n\nThat's pretty much a tale-tell sign for \"this thread is going to be noise\".  That basically translates, for anyone that's been around the list for a while, to \"I can't be bothered to actually read anything about what usability really is (or even the list FAQ), but I'll go ahead and give you my opinion and then make conclusions based on that.\"\n\nSorry, that really is just a waste of people's time.  (And for the record, I'm not a usability person.  I would have responded the same way if you'd posted to core-devel with unqualified development suggestions.)"
    author: "Scott Wheeler"
  - subject: "Okular usuability"
    date: 2007-02-01
    body: "I know that I reopen a please-not-again topic, but I really hope they will transform Okular into the KViewShell user interface. I know, from the economical point of view, the activity in Okular's repository compared with the small activity in KViewShell's gives Okular the favour and I do not want to discuss which one should be included in KDE4. Though, I believe the last KViewShell snapshot still exceeds Okular in my personal experience and if Okular will be included it should copy some ideas from KViewShell.\n\n(1) The thumbnail bar seems to be slower in Okular. \n(2) The thumbnail bar does not recognize 'smooth scrolling' in Okular.\n(3) I often turn the side bar off. Then, in Okular, I can not see important file information sice they do not appear in the status bar, but in the side bar (eg. current pager number). \nMoreover, thumbnail creation should be turned off, while the thumbnail bar is invisible.\n(4) The 'edit page number' widget works nice, but the visual appearance seems to make use of hard-coded stuff such that it looks odd when using certain style engines (see Baghira for example). \n(5) Also, I find the default buttons in the toolbar better selected in KViewShell.\n(6) I do not know about the development plans, but would it be somehow possible to jump to the right item in the bookmark list while I scroll through the document?\n\nJust my 2 cents. "
    author: "Sebastian"
  - subject: "Re: Okular usuability"
    date: 2007-02-01
    body: "> (1) The thumbnail bar seems to be slower in Okular.\n\nWhat's slower here?\n\n> (2) The thumbnail bar does not recognize 'smooth scrolling' in Okular.\n\nIt does not have smooth scrolling yet.\n\n> (3) I often turn the side bar off. Then, in Okular, I can not see important file information sice they do not appear in the status bar, but in the side bar (eg. current pager number). \n\nSee for example this: http://www.okular.org/screenies/okular-backend-djvu-1.png\nAs the interface is not yet decided into stone, screenshots may become obsolete soon. The current ones (except the DjVu one recently updated by myself) are 4 months old.\n\n> Moreover, thumbnail creation should be turned off, while the thumbnail bar is invisible.\n\nIt is since KPDF ages.\n\n> (4) The 'edit page number' widget works nice, but the visual appearance seems to make use of hard-coded stuff such that it looks odd when using certain style engines (see Baghira for example). \n\nOdd like?\n\n> (5) Also, I find the default buttons in the toolbar better selected in KViewShell.\n\nOf course we have an usability expert, and now a Season of Usability student, just for fun? Come on, they are not decided into the stone yet, so expect them to change.\n\n> (6) I do not know about the development plans, but would it be somehow possible to jump to the right item in the bookmark list while I scroll through the document?\n\nIf I understood you correctly, see the grey icon \n"
    author: "Pino Toscano"
  - subject: "Re: Okular usuability"
    date: 2007-02-05
    body: "Well, my expericences are from last early autumn. I am sorry, if they do not apply anymore.\n\n>> (1) The thumbnail bar seems to be slower in Okular.\n> What's slower here?\nIt is just an impression that occured when I scrolled through a pdf that the thumbnail creation durates a little longer. Maybe the time is the same, but first, the document should be created and then the view of the thumbnail, not vicer versa. There were moments where the application did not respond during thumbnail creation as well. \n \n>> (3) I often turn the side bar off. Then, in Okular, I can not see important file information sice they do not appear in the status bar, but in the side bar (eg. current pager number). \n>See for example this: http://www.okular.org/screenies/okular-backend-djvu-1.png\nGREAT! \n\n>> Moreover, thumbnail creation should be turned off, while the thumbnail bar is invisible.\n>It is since KPDF ages.\nSorry. didn't seem to react faster when making it invisible.\n \n>> (4) The 'edit page number' widget works nice, but the visual appearance seems to make use of hard-coded stuff such that it looks odd when using certain style engines (see Baghira for example). \n> Odd like?\nWell, Baghira has a stippled background. First, the horizontal bar, indicating the position in the file, looked like a misdrawn background stipple of Baghira. Second, the nackground of the button was not drawn according to the Baghira stipple scheme, but was just one plain color.\n \n>> (5) Also, I find the default buttons in the toolbar better selected in KViewShell.\n>Of course we have an usability expert, and now a Season of Usability student, just for fun? Come on, they are not decided into the stone yet, so expect them to change.\nWell, your point.\n \n>> (6) I do not know about the development plans, but would it be somehow possible to jump to the right item in the bookmark list while I scroll through the document?\n> If I understood you correctly, see the grey icon \nI wonder what you mean by 'grey icon'. If it is the tiny arrow in the contents list, then, yes, this is what I wanted for ages! Just one thing: Wouldn't it be better to move the selection to the current item instead of showing an arrow? There are some KDE styles which replace the plus/minus signs in the treeview by arrows leading some inconsistency... \n\nElse: Thanks, now I am looking forward...\n"
    author: "Sebastian"
  - subject: "Usability or Learnability"
    date: 2007-02-01
    body: "I notice in many usability reports the claim thaty many features are hidden and not easy to enable. This implies that the interface should be fixed, and often by expliciting the feature in an exaggerated way that perhaps bloats the design and restrains the productivity playground the user needs. I think usability should meet both learnability and productivity. Compare Konqueror and Nautilus, many say that Nautilus is usable while Konqueror is heavily complex, a complexity that makes it the most useful and productive software for the users who take time to learn. This is also a reason why many prefer the command line interface to the graphical one."
    author: "Youssef"
  - subject: "Okular"
    date: 2007-02-01
    body: "My main problem is: How to integrate Firefox and okular. I hate this evince tool which is used by default."
    author: "fanc"
  - subject: "Basket"
    date: 2007-02-01
    body: "http://basket.kde.org/screenshots.php\n\nItalic, bold etc. only need to be shown when text is selected."
    author: "fanc"
---
The <a href="http://www.openusability.org/season/0607/">Season of Usability</a>, run by the <a href="http://www.openusability.org">OpenUsability</a> project has kicked off with two KDE applications in the focus: <a href="http://basket.kde.org/">BasKet Note Pads</a> and the KDE 4 universal document viewer <a href="http://okular.org">Okular</a>.   Usability, as one of the important focus points of the upcoming fourth major version of KDE, is also an <a href="http://usability.kde.org/">active part</a> of the KDE project. The <em>Season of Usability</em> manifests KDE's close involvement with OpenUsability.






<!--break-->
<p>These projects follow a structured process to improve the usability of the applications. First, the uses of the respective applications are analysed. The next step involves developing personas from the data gathered in the previous step. Based on those personas, a design for the user interface is made. The last stage of the usability analysis is the evaluation of the process.  Then the changes are implemented.
</p>

<p>The webpage of the <a href="http://basket.openusability.org/">BasKet OpenUsability project</a> will keep you up to date with their progress.</p>


