---
title: "KDE Commit-Digest for 19th August 2007"
date:    2007-08-20
authors:
  - "dallen"
slug:    kde-commit-digest-19th-august-2007
comments:
  - subject: "Mosfet's back!"
    date: 2007-08-20
    body: "So does this mean that liquid will be resurrected? I tried to get it in shape a long time ago, but it was too tough and I ended giving up :)"
    author: "Sarath Menon"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-20
    body: "Nope, he's not interested in styles anymore. Some other stuff he did might live again, though ;-)\n\nAnd of course there is new stuff he is working on. pretty cool, imho."
    author: "jospoortvliet"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-20
    body: "Mosfet's back, and we're really happy about it. Whatever he chooses to do, it will be a touch of class to the desktop for sure! I(we) owe to him ;-)"
    author: "a reader"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-20
    body: "As a newcomer to KDE - who is Mosfet, and what kind of thing has he/ does he do in KDE? Everyone seems excited, so I guess he's really talented at something :)"
    author: "anon"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-20
    body: "He is a developer from quite some time ago, who kind'a vanished. He wrote some pretty popular stuff in his days, like the Liquid style (mac aqua like) and Pixieplus (image editor) and some other nice things in the KDE libs."
    author: "jospoortvliet"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-20
    body: "He did lots of style and window decoration work, he was main developer for it in KDE2 if I'm not mistaken. He created the KDE classic styles(regular and highcolor) and the B2/B3 styles I think. And the KDE2 and Laptop window decorations.\n\nBut more importantly IMHO, his news site(Nobody had invented blogs yet:-) was the only place giving regular news about KDE in the pre KDE 2 days. Back then it was no dot, no PR people, developers blogs etc. That site did a tremendous job keeping public interest in KDE during the long development cycle leading up to KDE2. "
    author: "Morty"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-20
    body: "he also worked on such things as kicker. like many of the core hackers back in the kde2 days, his code was to be found all over the place. as the project has grown, more people have become \"specialists\" concentrating on a specific set of apps or features. interesting how that all evolved, imho."
    author: "Aaron J. Seigo"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-20
    body: "We all knew Mosfet would be back someday (or never really left :P)\n\nHe did some real cool stuff back in the day, looking forward to the interesting stuff He will bring to the table once he gets up to speed... \n"
    author: "madpuppy"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-20
    body: "In fact (Anecdote), half of the post about Mosfet were people arguing about his gender... I hope he change haircut since ;-)"
    author: "oliv"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-21
    body: "And that is something I never could figure out. Why anyone would care enough to make that such a big deal what gender erm, he/she is/was. "
    author: "taurnil"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-21
    body: "Yep. Besides, such a talented hacker can only be a girl. ;)"
    author: "fast penguin"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-22
    body: "Don't start that crap again!"
    author: "reihal"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-21
    body: "Mosfet was a HIGH maintenance type developer who was very talented. THere is a good saying that applies nicely to him.<BR><BR>Everybody brings joy in your life; Some in coming and others in going. Mosfet was a LOT of both.<BR><BR>But assuming that he has calmed down and grown up, I am betting that he will be WELL received."
    author: "a.c."
  - subject: "Re: Mosfet's back!"
    date: 2007-08-21
    body: "An alpha with an alpha personality."
    author: "semsem"
  - subject: "Re: Mosfet's back!"
    date: 2007-08-21
    body: "Besides Lubos he is the only (unfortunately!) KDE developer I know of who knows how to write efficient code. If you somewhere see MMX special effect code it's probably Mosfet's code..."
    author: "Max"
  - subject: "Great work on Marble"
    date: 2007-08-20
    body: "Great work on Marble!\n\nBy the way, the \"Plasma\" link has no href so it just links to this page."
    author: "LiquidFire"
  - subject: "Re: Great work on Marble"
    date: 2007-08-20
    body: "Also, the kpilot keyring link points to the thumbnail and not to the full-size image : http://commit-digest.org/issues/2007-08-19/files/kpilot_keyring.png ."
    author: "AC"
  - subject: "Re: Great work on Marble"
    date: 2007-08-21
    body: "Fixed both issues.\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "translucent overlays"
    date: 2007-08-20
    body: "wow. you should definitely add them to kdelibs for KDE 4.1"
    author: "nick"
  - subject: "Re: translucent overlays"
    date: 2007-08-20
    body: "clear case for a styleguide (discussion). that looks 'gangsta awesome'!"
    author: "elveo"
  - subject: "A storm"
    date: 2007-08-20
    body: "KDE 4 is going to take the free desktop by a storm!"
    author: "KDEer"
  - subject: "pixie"
    date: 2007-08-20
    body: "it would be great if Pixie and Gwenview could share some code cause I think Gwenview picture rendering is really slow compared to say digikam but maybe that's just my setup."
    author: "Patcito"
  - subject: "Re: pixie"
    date: 2007-08-21
    body: "No, Gwenview is everywhere slow.\nBtw. Kuickshow (with e.g. 64MB image cache) is extremely fast."
    author: "Max"
  - subject: "Re: pixie"
    date: 2007-08-22
    body: "you should try the latest version of the kipi plugins - there is a fast X-overlay based viewer that speeds up things greatly. If you are using an older version of the packages, you can still compile it from source. Search on kde-apps for it, its not that hard to find and has an obvious name (which i dont remember).\n\nAll said, nothing beats pixie - its like comparing word perfect to office 2007 :)"
    author: "Sarath Menon"
  - subject: "Pixie"
    date: 2007-08-20
    body: "wow.. pixie is just sooo cool.. even in such an early state.. i hope he's going to improve it and release it... at least with kde 4.1\n\nbtw: great work on marble!"
    author: "LordBernhard"
  - subject: "Where is Pixie?"
    date: 2007-08-20
    body: "Anyone knows where to see the progress of Pixie? Has he commited those changes to svn? It would be great to see those ideas in action :-)"
    author: "Anonymous"
  - subject: "Re: Where is Pixie?"
    date: 2007-08-20
    body: "It's all private still, afaik."
    author: "jospoortvliet"
  - subject: "kicker last days ;-)"
    date: 2007-08-20
    body: "Initial implementation of panels commit by mbroadst, waiting for the next commit \" kicker moved to black hole\"  \n\ncheers "
    author: "djouallah mimoune"
  - subject: "Klickety?"
    date: 2007-08-20
    body: "Hey!\n\nI hate to be a stick-in-the-mud, but, I guess Klickety is gone for good then?  That's been one of my favorite quick time killers for MANY moons now. 'Tis a shame it's gone the way of the dodo.\n\nThanx,\nM.\n\n\n\n"
    author: "Xanadu"
  - subject: "Re: Klickety?"
    date: 2007-08-20
    body: "Nothing's ever \"gone for good\" if someone is willing to resurrect and maintain it :)"
    author: "anon"
  - subject: "Re: Klickety?"
    date: 2007-08-21
    body: "heh!  Good point!\n\nI wish I could pick it up.  I know next to nothing about C++, and the little I do know is only super-basic text-book type stuff.  If I had at least a small clue what needed to happen to get a KDE app from QT3 --> QT4, I'd pick it up and play with it.  And that would take learning QT in addition to C++.\n\n/me Sighs\n\nI guess I gotta start somewhere, though...\n\n\n(this is by no means saying I'm picking this up since I'm sure people would want it *working* and not broke like I'd probably leave it... :\\ )\n\n-- \nM.\n"
    author: "Xanadu"
  - subject: "Re: Klickety?"
    date: 2007-08-21
    body: "> If I had at least a small clue what needed to happen to get a KDE app from \n> QT3 --> QT4, I'd pick it up and play with it \n\nHere is for Qt:\n\nhttp://doc.trolltech.com/4.2/porting4.html\n\nThere is a porting tool that does some of the trivial rewrites:\n$ qt3to4 myfile.cpp\n\n\n\nhttp://techbase.kde.org/Development/Tutorials/KDE4_Porting_Guide\n\nSmall clue provided! ;-)"
    author: "Martin"
  - subject: "KMobileTools"
    date: 2007-08-21
    body: "Will KMobileTools use KitchenSync / OpenSync now? Will that be one of the \"engines\" or what will the relation be?"
    author: "Martin"
  - subject: "Re: KMobileTools"
    date: 2007-08-21
    body: "Tobias merged the OpenSync code into KDE4's kdepim _today_. See \n\nhttp://websvn.kde.org/trunk/KDE/kdepim/kitchensync/\n\nfor details."
    author: "Carsten Niehaus"
  - subject: "Re: KMobileTools"
    date: 2007-08-22
    body: "I have really high hopes for opensync, but it seems its progressing very slowly. Looking at mailing lists and svn, it seems there's currently not too much happening.\n\nWill kde4 use opensync .20, .30 or the (hopefully) upcoming .40?"
    author: "anonymous coward"
  - subject: "Re: KMobileTools"
    date: 2007-08-28
    body: "Hi,\n\nfirst sorry for the late answer, unfortunatelly (well, for you ;-)) I was on vacation. Currently - due to lack of time - there are no concrete plans for an OpenSync engine in KMobileTools, but this will definitely get a high priority on our todo-list.\n\nIf you (or anyone else) is willing to implement such an engine, you're welcome. It shouldn't be a too hard job.\n\nBest regards\nMatthias"
    author: "Matthias Lechner"
---
In <a href="http://commit-digest.org/issues/2007-08-19/">this week's KDE Commit-Digest</a>: The <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> for 2007 nears its end. Implementation of more features in the Step physics simulation package. More graphical game themes in KMahjongg, KWin4 KShisen, KGoldRunner and KJumpingCube. The start of a new game, KDiplomacy. More development in the <a href="http://sourceforge.net/projects/qimageblitz">Blitz</a> graphics library. Lyrics <a href="">Plasma</a> applet and other interface work for <a href="http://amarok.kde.org/">Amarok</a> 2. The start of the implementation of panels, and a clipboard engine in Plasma. More features in the ODBC Data Sources KControl module. Animation support in the Raptor menu. KCacheGrind is ported to QGraphicsView. MusicXML import function in <a href="http://koffice.org/">KOffice</a>. Ability and an application for viewing statistics in KPixmapCaches. KGraphEditor imported, an application to edit "dot" graphs. A new architecture for <a href="http://www.kmobiletools.org/">KMobileTools</a>. KTrace renamed to Inspektor.

<!--break-->
