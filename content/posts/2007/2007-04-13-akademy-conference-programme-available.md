---
title: "aKademy Conference Programme Available"
date:    2007-04-13
authors:
  - "jriddell"
slug:    akademy-conference-programme-available
comments:
  - subject: "Videos"
    date: 2007-04-13
    body: "Do you have plans to make videos of the talks available during or post Akademy for those unable to attend? If so (yay :), have you looked into how to improve the audio quality over last year's vids?"
    author: "Peter"
  - subject: "Re: Videos"
    date: 2007-04-14
    body: "Yes, they'll be available in WMV format with DRM... ;)"
    author: "Evil Eddie"
  - subject: "Re: Videos"
    date: 2007-04-15
    body: "Cool. Does that mean I have to install the latest version of WMP just to view them?"
    author: "Segedunum"
  - subject: "Re: Videos"
    date: 2007-04-14
    body: "I was just about to ask this! It would be really easy to just put the videos on video.google.com. Videos are important for a world wide project like KDE. I would like to go but I can't afford to."
    author: "antiNeo"
  - subject: "Re: Videos"
    date: 2007-04-14
    body: "Yeah, but Evil Eddie just told the same joke. :-)\n\nWait, are you suggesting a non-free distribution/archiving method for real?"
    author: "gg: evil"
  - subject: "Re: Videos"
    date: 2007-04-14
    body: "Why not? Google Video wouldn't have to be the only way of distributing it, but it would certainly be handy if I don't wanna torrent a bunch of ogg files. There's nothing wrong with using proprietary software once in a while when it's better for your needs, i.e. if I were watching at school on an old Windows 98 machine. Flash works fine, so so does Google Video.\n\nSorry for the derail -- I too would love to watch many of the presentations at aKadamy 2007. The Programme has loads of interesting stuff listed; lots for developers, but still plenty I'd like to hear about."
    author: "clinton"
  - subject: "Re: Videos"
    date: 2007-04-14
    body: "Huh? I was refering to the free bandwidth. ;-)\nIt's not like Google video uses a .wmv format with DRM for it's videos. A lot of LUGs use it. Although some .ogg files over bittorrent would be more ideal."
    author: "antiNeo"
  - subject: "Re: Videos"
    date: 2007-04-14
    body: "It is not free. You pay for it by turning over personal information and by being exposed to ads. If you upload a video to them you are also forcing anyone who views that video to do the same. No one collects more personal information than does Google, and almost no person on the Internet escapes being caught in their vast databases.\n\nThe videos themselves are shown in Flash(TM) and are also offered as downloads for \"Windows/Mac\" or \"Video iPod/Sony PSP\". No DRM yet, but probably some watermarking.\n\nSeriously, SUN:s old slogan \"The Network is the Computer\" is becoming ever more true. It is time that the Computer became free and open."
    author: "gg: evil"
  - subject: "Re: Videos"
    date: 2007-04-14
    body: "\"If you upload a video to them you are also forcing anyone who views that video to do the same.\"\n\nIf they don't want Google to know they watched some videos from "
    author: "Sutoka"
  - subject: "Re: Videos"
    date: 2007-04-14
    body: "Oops accidentally tapped 'ctrl' then 'a'...\n\nAs I was saying, If they don't want Google to know they watched some videos from aKademy then there would still be downloading the .oggs from the torrent or from any servers that are hosting it.  The torrent solution is bad for people that only wish to watch a couple of the videos, and the .ogg files on the server aren't as good as using flash since most every computer supports playing flash videos while a huge percentage still can't play ogg's.\n\nI doubt antiNeo was proposing that you ONLY put the videos on google's video service, but using it in addition to the methods used last year will make it easier for people to watch the videos (though personally I like YouTube more than google video, seems to have a much nicer interface IMO, though you can't download from it straight from the site normally).\n\nDon't forget that a massive amount of websites have AdSense on them (including FLOSS websites).  And I'm pretty sure anyone that doesn't want Google to collect information about them has Google's adsense and/or Google's analytics software blocked.  And another thing, there are multiple FLOSS flash implementations and at least one of them (swfdec IIRC) can already play YouTube videos, and most likely Gnash isn't far away either.\n\nOh and if you think Google may watermark the videos, just try downloading the videos from 2 different computers (or even browsers), and then do a diff to see if theres any difference.  If there is no difference, no watermark, if there is, then maybe there is."
    author: "Sutoka"
  - subject: "Re: Videos"
    date: 2008-05-12
    body: "thi5 i5 my first tyme here on this vida web site. te me como?umm im bonita peoplez. to bonita 4 yall=]"
    author: "LEEsEE<3"
  - subject: "about confrence"
    date: 2007-07-03
    body: "HI.........i will like to know if there is any conference available for a student "
    author: "Kazeem Olaiya"
---
The <a href="http://akademy2007.kde.org/conference/programme.php">programme for aKademy's conference</a> is now available.  With KDE 4 technologies now moving into place the talks give a superb overview of the state of the art on the free desktop.  Themes include KDE 4 pillars, language bindings, applications, quality control, libraries, operating systems &amp; distributions and community.  The conference closes with the most important event in the KDE calendar, the <a href="http://www.kde.org/history/akademyawards/">annual aKademy Awards</a> given to the most dedicated of KDE developers.  Keynotes are still to be announced and of course remember to <a href="http://www.kde.org.uk/akademy/">register your attendance</a>.
<!--break-->
