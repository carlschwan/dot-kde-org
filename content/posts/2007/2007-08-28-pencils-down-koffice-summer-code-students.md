---
title: "Pencils Down for KOffice Summer of Code Students!"
date:    2007-08-28
authors:
  - "brempt"
slug:    pencils-down-koffice-summer-code-students
comments:
  - subject: "congratulations to all!"
    date: 2007-08-27
    body: "This is one of the most exciting stories I have seen on the dot in a long time.  Congratulations to all, and may fame and glory come to you. :)"
    author: "KDE User"
  - subject: "Re: congratulations to all!"
    date: 2007-08-28
    body: "Hej, I fully agree, that was awsome what I just read about the SOC-students.It is impressive, what they have done. Mixing colours (without a big computer cluster) or having music notes in ODF, that is great.\nYou all did a good job. To the students and all the other KDE programmers out there: \"Keep up the good work\""
    author: "baumhaus"
  - subject: "Wow!"
    date: 2007-08-27
    body: "Dang, those guys are GOOD! Really impressive accomplishments, ALL of them. (I usually don't use caps, but today I am making an exception.) Now, could Google please hire these guys as KDE hackers full time please?"
    author: "Martin"
  - subject: "Re: Wow!"
    date: 2007-08-28
    body: "Damn, aren't those guys a little bit _too_ good? I was thinking about applying next year, but seeing these results, I've realized that it's just far beyond my (currently non-existent) ability. ;)\n\nNo seriously; great work, all of you! The results are truly amazing, looking forward to trying it out myself.\n\nOff topic question: Is it possible to run KOffice2 on Windows too? (Seeing that Mac OS X screenshot, I guess the answer is yes?)"
    author: "Hans"
  - subject: "Re: Wow!"
    date: 2007-08-28
    body: "I'm sure you can compete: the gsoc is enormous learning experience for all involved. I bet some of our students didn't know they'd be able to get this far. Personally, I've seen proposals for funded research that were less ambitious than some KDE gsoc projects.\n\nOh, and about windows: Adrian Page has been committing fixes to get KOffice compiled with Visual C++, so I guess the answer is yes."
    author: "Boudewijn"
  - subject: "Wow..."
    date: 2007-08-28
    body: "good job.\n\nAbout that \"sheet music\"; can you also *play* the music somehow (e.g. simple piano midi instrument), or is it \"read only\"? (yes I know Koffice is not a music composer program...)"
    author: "Darkelve"
  - subject: "Re: Wow..."
    date: 2007-08-28
    body: "well, you may need an instrument to play, and of course you need to actually _read_ what's written on the sheet. (sorry.. couldn't resist)"
    author: "Thomas"
  - subject: "Re: Wow..."
    date: 2007-08-28
    body: "That sounds like a job for Rosegarden to me, not KOffice. However, maybe it's possible to interface with Rosegarden for this purpose?"
    author: "Andr\u00e9"
  - subject: "Re: Wow..."
    date: 2007-08-28
    body: "Maybe phonon can play a major advanteg in stuff like that. It can be integrated quite easy into your existing code.\nIf koffice could actually READ the notes, it shouldn't be such a big problem. I like the idea that koffice can play the song you made up. I hope it will be implemented."
    author: "baumhaus"
  - subject: "Re: Wow..."
    date: 2007-08-29
    body: "The ability to import snippets of music from something like Rosegarden would be useful. Whether that is better implemented as a KOffice import or a Rosegarden export, I'm not sure.\n\nDoes it spell-check your musical score, putting red squiggly lines under out of key notes?"
    author: "Steven"
  - subject: "Re: Wow..."
    date: 2007-08-29
    body: "\"Does it spell-check your musical score, putting red squiggly lines under out of key notes?\"\n\nI don't think there is a way for a computer to know whether a score is 'right' or 'wrong'... or were you joking? :|"
    author: "Darkelve"
  - subject: "MusixXML"
    date: 2007-08-29
    body: "Rosegarden can do MusicXML... I guess that would make interfacing easier?\n\nBasically just a play button and perhaps an instrument chooser would suffice."
    author: "Darkelve"
  - subject: "Compliments!"
    date: 2007-08-28
    body: "very nice work ! thanks !\n\ni personally like the color mixing stuff most - i will never use it, but i like the complexity behind the mathematical model..."
    author: "jumpingcube"
  - subject: "Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "Is ODF not the default file format for KOffice anymore? How is sheet music compatible with ODF in any way, shape, or form? And what do you mean \"he had to hack stuff into KWord\" because \"KWord did not support ODF at all anymore\"?! I thought all open source word processors were agreeing that ODF was THE standard to work with from now on! Aren't you essentially adding \"proprietary features\" that lock users into KWord, or at least force alternative editors like OpenOffice and AbiWord to play catch-up by implementing sheet music support?\n\nIs ODF is some kind of joke, like OpenXML? I tried once to open ODF documents I made in OpenOffice in KWord, and they didn't render correctly. (Whereas if I saved them in Microsoft format, they would!) On the other hand, if OpenOffice is incorrectly supporting the ODF standard (as asserted in your article), how will that change in future releases of OpenOffice? Will my files suddenly change their appearance when/if OpenOffice begins interpreting them according to the standard? Don't take this the wrong way - I appreciate the length that Sun is going to to make ODF a meaningful specification (unlike Microsoft's CrapXML format), and I appreciate all the work done on OpenOffice and KOffice, but when are they going to produce documents that are interpreted by each other the same way? Or is this going to be like the CSS compatibility wars between IE, the W3C specs, and the other browsers?"
    author: "kwilliam"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "I think he simply means that the major work in rewriting KWord for Version 2.0 broke a lot of things, including saving in the default file format, ODF.  THe have not fixed it so that when 2.0 is released, it will work.\n\n\nAt least I hope that is what he means..."
    author: "David"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "I think he simply means that the major work in rewriting KWord for Version 2.0 broke a lot of things, including saving in the default file format, ODF.  The have now fixed it so that when 2.0 is released, it will work correctly.\n\n\nAt least I hope that is what he means... ODF definitely should be a standard across office suites, especially FLOSS ones."
    author: "David"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "I think he simply means that the major work in rewriting KWord for Version 2.0 broke a lot of things, including saving in the default file format, ODF.  They have now fixed it so that when 2.0 is released, it will work correctly.\n\n\nAt least I hope that is what he means... ODF definitely should be a standard across office suites, especially FLOSS ones."
    author: "David"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "There simply wasn't any file load and save code anymore in KWord at one point in time. KWord didn't support anything at that moment -- which is quite a normal occurrence in the middle of software development. And even in 1.6 there were parts of the ODF unsupported. So when porting of the 1.6 ODF loading and saving code started, bits have been added to support ODF more fully."
    author: "Boudewijn Rempt"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "No, ODF is the default file format for KWord.\nThe problem is just that there wasn't a lot of things working about file loading, and nothing for file saving. \nNow, there are problems on both sides : OpenOffice mis-implementing the standard, and KOffice mis-implementing... That's quite an issue, I agree.\nMoreover, implementing the whole specification is near of impossible.\nI hope both OpenOffice and KOffice reach a near 100% implementation in the future, but that'll take time.\nFor the sheet music, well... it has to be KOffice specific, there is really no way for it to be compatible with ODF since ODF doesn't specify this.\nBut you can still build valid documents that way. Here is a part of the saving code :\n    writer.startElement(\"music:shape\");\n    writer.addAttribute(\"xmlns:music\", \"http://www.koffice.org/music\");\nOf course, OpenOffice won't be able to interpret this."
    author: "Pinaraf"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "Why is it not possible to implement the complete specification? I understand why it is impossible for OOXML (quite too long) but ODF should be short enough to implement every feature (at least in a good time)\nAnd why doesn't OOo and KOffice work a little closer together to implement the same stuff? That would be a little better for the user that he can exchange documents between those two without having problems."
    author: "baumhaus"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "I think it's possible to implement all of ODF -- but it'll take some time. And, yes, there is cooperation: KOffice people will go to the OpenOffice week in Barcelona in September. But, well, OO.org is sometimes a little arrogant and assumes that what OO.org does is, by definition ODF, regardless of the actual spec. And we try to be very faithful to the spec, but we're also rather more innovative than OO.org, which means that we're following the spec, but doing things with it that haven't been tried before. \n\n(Plus, we're really short-handed. Don't forget that only Jaroslow, the Kexi maintainer, is paid for his work on KOffice: all the other code is writting by us in our copious spare time. So... if you fancy an easy way into KOffice hacking, come help us out with implementing more and more of the ODF standard. I promise you, it's not hard, and it's a very rewarding thing to do.)"
    author: "Boudewijn Rempt"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "even thos I would like help, I don't think that I have, what it takes to be a GOOD programmer. I allways wanted to get started in a team but I don't think that I'm any good.\nThat for I'm really impressed what you are doing. I have alot of faith in KOffice2"
    author: "baumhaus"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-29
    body: "It's really just a matter of starting... You'll be amazed at what you can do."
    author: "Boudewijn Rempt"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-29
    body: "You can get started with quite small work and do relatively easy things. Getting to the stage where you can build KOffice from scratch is a difficult first step but it gets easy from there.  The Koffice developers were very good about reviewing the few small patches I sent in and were very constructive about it all.  \n"
    author: "Alan"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "Well, it's really really hard to implement 100% of any specification. It *is* impossible for OOXML (far too big + some undocumented things), and for ODF it's just hard and needs time.\n(BTW, a full ODF implementation requires Java since you're supposed to be able to embed Java applets in an ODF document)"
    author: "Pinaraf"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "Now this is strange...\nIs Java an ISO standard?"
    author: "Luciano"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-29
    body: "No it's not."
    author: "Cyrille Berger"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "Supporting java applets should be extremely easy since way java support is implemented in khtml means applets can be embedded by simply using KJavaAppletWidget.\n\n"
    author: "Richard Moore"
  - subject: "Re: Is ODF a second-class citizen in KWord?"
    date: 2007-08-28
    body: "btw, a 100% implementation would also require OLE (9.3.3), DDE (4.4.3, 6.6.9, 8.10), binary plugins (9.3.5), multiple scripting-languages (or at least all that got used by competitors, 2.5), multiple database-drivers (or at least all that got used by competitors, 6.5, 8.6.2), would need to even know about custom app- (2.4) and meta-data (3.3), macros (6.6.7), Change Tracking (8.10), would need a audio-/soundplayer (9.7.1), etc. and that's just a quick look at the table of content ;)\n"
    author: "Sebastian Sauer"
  - subject: "Good job"
    date: 2007-08-28
    body: "Kudos to all the SoC folk!"
    author: "Eugeniu"
  - subject: "Kerning"
    date: 2007-08-28
    body: "Oh no! Look at the \"statistics\" screenshot. Kerning is as bad as it always has been. The developers used to promise that this would be fixed with Qt 4; more recently they have been pointing to the fonts. Please, show just one rendered PDF or even a screenshot where the text really looks good and prove me wrong! Because so far, sadly, and I am not trolling here, but speaking from experience, I have never seen acceptable output from KWord: any version or font family."
    author: "Martin"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "Forgive my ignorance, but what does the kernel have to do with fonts?"
    author: "KDE User"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "Well, in Windows, the truetype bytecode interpreter is part of the kernel, but that's not what the original poster meant. He means the way letter shapes (glyphs) are slightly moved about to create a pleasing result. Like a lower-case o being shifted a little bit to fit under a capital V in the the sequence \"Vo\". That's what's called kerning. Whether kerning works or not is dependent on the font -- if the included kerning tables suck, then the result will suck, and on a couple of other things, including Qt Scribe and Freetype.\n\nWhy exactly the kerning is off sometimes, with some fonts when using Freetype and Qt Scribe is a bit of a mystery to me. I've seen great results with KWord 2.0 myself, but so I've seen great result with KWord 1.6. But I haven't investigated myself, being busy with Krita, and I'm sure that Thomas Zander is hot on the trail of the remaining font problems."
    author: "Boudewijn Rempt"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "Ok, I investigated a bit. Apparently there's currently a bug in Qt that makes KWord obey the hinting settings. If you set hinting to anything but None (as you should anyway, hinting is evil), your text will be really bad. Another issue is with fonts: some fonts just have extremely bad kerning tables. Look at the attached image to see some very good lines of text in KWord, and some less good ones: Sans Serif and Arial are pretty bad, Georgia fair to middling. Times New Roman, Perpetua, Zapf Chancery, Bembo and Garamond are really pretty good. But then, those are expensive, professional fonts.\n\nActually, I think Bembo has never looked as good on screen as it does in KWord trunk."
    author: "Boudewijn Rempt"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "Thank you for a most awesome explanation and illustration!"
    author: "KDE User"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "Looks like only the windows fonts look really good (I'm looking at the 'u' in \"Tussen\"). It looks good in Arial, Garamond, Times New Roman, but not in the the other linux-specific? fonts...\n\nWell if it works, at least with the windows fonts, than it is okay. Although I hope ofcourse those other fonts will get better over time...."
    author: "boemer"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "Actually, only the first font (\"sans serif\") is a free font. The rest are mac/windows commercial fonts. Pretty expensive ones, too. And I'm so fond of Bembo..."
    author: "Boudewijn"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "IMHO:\n\nGaramond: The \"r\" is too strong -- it stands out in \"typographie\" and \"kerning\".\n\nBembo: Bad kerning in in \"Waar\" and \"Tussen\".\n\nPerpetua: Bad kerning in \"voorbeeld\""
    author: "Niels"
  - subject: "Re: Kerning"
    date: 2007-08-29
    body: "Aww, the character positioning looks really bad for all of the fonts in that screenshot.\n\nThe problem is that Qt (being based on the OS's native font system) does only support integral character positions, making it impossible to get correct layout (line lengths etc.) and correct positioning at the same time.\n\nPlease see <A HREF=\"http://antigrain.com/research/font_rasterization/index.html\">http://antigrain.com/research/font_rasterization/index.html</A> for some awesome results that are possible using FreeType directly, bypassing the OS's font system.\n\nEspecially worth noting is <A HREF=\"http://antigrain.com/research/font_rasterization/sample_arial_03.png\">this screenshot</A> (yes the font is small, but this even more so proves the author's point)."
    author: "christoph"
  - subject: "Re: Kerning"
    date: 2007-08-29
    body: "Given that there's no word processing software yet that works the way the author of that article proposes, it's a bit silly to hold it up as the standard. Compared to the same text in OO.org or Abiword, KOffice 2.0 is doing really well."
    author: "Boudewijn Rempt"
  - subject: "Re: Kerning"
    date: 2007-08-29
    body: "Still, you have to admit that the results look pretty amazing. At least to my rather untrained eye."
    author: "Andr\u00e9"
  - subject: "Re: Kerning"
    date: 2007-08-29
    body: "> The problem is that Qt (being based on the OS's native font system) does \n> only support integral character positions, making it impossible to get \n> correct layout (line lengths etc.) and correct positioning at the same time.\n\nQt's font system uses qreal (doubles) in the API everywhere and maps that to a high-res grid of integers to keep things fast.  There is virtually no loss there so the statement above doesn't really apply to the KWord case.  Sorry."
    author: "Thomas Zander"
  - subject: "Re: Kerning"
    date: 2007-08-30
    body: "You are right about the API and internal calculations using qreals, but the actual rendering is not done in sub-pixel positioned precision.\n\nTry rendering a couple of letters each moved by a fractional pixel coordinate, and you will see that they all end up looking the same.\n\nCompare that to KPDF's rendering (3.5.7, didn't try 3.91 yet), where the bitmap of a letter is different depending on (sub-pixel) position.\n\nOf course it also has to do with Qt (or FreeType?) trying very hard to cache the bitmaps. In earlier versions of Qt/FreeType, you could bypass caching by scaling with, say, a factor of 1.00001, and get very good results. But in recent versions, the cache is used even for rotated fonts, making the result look really funny.\n\nAnd seeing the good quality of KPDF and its good speed, I doubt that a Word Processor couldn't get nearly the same results.\n"
    author: "christoph"
  - subject: "Re: Kerning"
    date: 2007-08-31
    body: "Sorry maybe I'm blind but I see no problems at all with kerning in Boudewijn's screenshot"
    author: "Vide"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "That's not only Qt...\nI just upgraded my (free) ATI driver to the latest git to get xrandr 1.2 support, and all the font rendering is now far far better everywhere. For instance in KWord the text underline was in the middle of the text, now its position is good. So be careful with this font thing..."
    author: "Pinaraf"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "yes, very right.  I said to Boud on IRC its likely in the 'lower layers', it ended up on his post above as 'in Qt'. Almost right :)\n\nThere are actually various shared libs being used by a large set of font rendering stuff on Linux. This are all rapidly improving since not only KWord, but all of Qt, Pango (for gtk stuff) and IIRC also firefox are sharing that lower level stuff.\n\nKWord is not released yet, and we are all working hard on fixing all the things that need to be fixed to make KOffice have the most beautiful and versatile text layouting and rendering engine for you.\n\nSo, for the guy that went slightly off topic on this GSoC story; this is alpha stage software and we are aware of such issues.  Working on it! :)"
    author: "Thomas Zander"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "For KWord 1.6 it's rather simple to explain the difference between fonts. Type-1 fonts are fine while Truetype fonts are broken. So actually, when I use KWord I only use Type 1 fonts (like e.g. Bitstream Charter or Adobe Utopia).\n\nFurther, in KWord 1.6 I perceive kerning problems only in printout. DejaVu Sans e.g. looks better on screen in KWord 1.6 than in the screenshot of KWord 2.0. Problems begin only when you print a text or create a PDF.\n\nI really hope that this will be fixed in KWord 2.0. Most fonts are Truetype fonts nowadays. Without a proper kerning of these fonts KWord 2.0 will be unusable for most people.\n\nAnother problem is support of OTF fonts (i.e. Type-1 based Open Type fonts). In KWord 1.6 they are rendered correctly on screen, but replaced by other fonts in PDF and printout. BTW, OpenOffice.org doesn't support them either on screen nor in printout or PDF. These fonts, too, become more and more common. So, complete support of OTF fonts is also important.\n"
    author: "Stefan L\u00fccking"
  - subject: "Re: Kerning"
    date: 2007-08-28
    body: "Yep, correct support of TrueType and OpenType is highly desirable, especially for Windows versions of KOffice."
    author: "Richard Van Den Boom"
  - subject: "Beta 2 public release day"
    date: 2007-08-28
    body: "thank you\n\nps= great work guys!!!"
    author: "[OT] When will be released Beta 2?"
  - subject: "Re: Beta 2 public release day"
    date: 2007-08-29
    body: "Of KOffice? No idea; we are at around alpha 3 now :)"
    author: "Thomas Zander"
  - subject: "Great work!"
    date: 2007-08-28
    body: "Excellent work from all concerned, and I hope you all stay on - you are great assets to the KDE project :)"
    author: "Anon"
  - subject: "Re: Great work!"
    date: 2007-08-28
    body: "Yeah, only the collaborative editing guy disappearing sucks... :("
    author: "jospoortvliet"
  - subject: "I didn't they Koffice had so much work done"
    date: 2007-08-28
    body: "It seemed like development was at blitzkrieg pace in preparation for KDE 4, but I didn't hear too much about koffice. This is some excellent news, I've always favored the Koffice suite over openoffice + gimp and now they've gotten even better."
    author: "Skeith"
  - subject: "Re: I didn't they Koffice had so much work done"
    date: 2007-08-28
    body: "Oh darn, I didn't THINK Koffice had so much work done"
    author: "Skeith"
  - subject: "Re: I didn't they Koffice had so much work done"
    date: 2007-08-28
    body: "When I last counted, ca a year ago, all of KOffice was approximately 850,000 lines of code.  Now, when I asked a day or so ago, somebody counted to a little over a million.  It's big, people, and it's being worked on."
    author: "Inge Wallin"
  - subject: "Congratulations!"
    date: 2007-08-31
    body: "Congratulations for coding this summer for the open community. It is such code that we all appreciate.\n\nWell done! :)"
    author: "Alan Haggai Alavi"
---
With an avalanche of last-minute commits, the KOffice Google Summer of Code students finished yet another great Summer of Code. We had some very exciting projects this year, and most of them were as great a success as <a href="http://dot.kde.org/1157452184/">last year</a>.  Read on for details of the achievements.















<!--break-->
<p>This year we had:</p>

<ul>
<li>Emanuele Tamponi, Painterly Features for Krita (Mentor: Bart Coppens)</li>
<li>Fredy Yanardi, Text-tool plugins for KOffice apps (Mentor: Tomas Mecir)
<li>Marijn Kruisselbrink, Music Notation support for KOffice (Mentor: Boudewijn Rempt)
<li>Pierre Ducroquet, Improve OpenDocument Compatibility in KWord (Mentor: Sebastian Sauer)
<li>Sven Langkamp, Selection Visualisation (Mentor: Camilla Boemann)
</ul>

<p>We had one disappointment this year, our student due to work on collaborative editing disappeared, only to surface around mid-term with a code drop &mdash; before disappearing again. So no collaborative editing in KWord this year. You cannot win them all.</p>

<p>But the other five students have delivered outstanding work, often going far beyond what their mentors thought possible initially. Let's take a look at what they have done up to now!</p>

<h3>Emanuele Tamponi</h3>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-emanuele.jpg"><img align="left" src="http://static.kdenews.org/jr/koffice-soc-2007-emanuele-small.jpg" width="75" height="117" /></a>

<p>In <a href="http://commit-digest.org/issues/2007-08-12/">last week's Commit Digest</a> we already saw part of what Emanuele has done this summer: mixing colours in a way that looks and feels completely natural, by simulating the way light is reflected of pigment. This is a two-way process: to go from a colour defined in RGB to a colour defined by the properties of pigment and light and back to RGB again. The first step is usually done by using a spectrometer and actualling measuring the colours. Emanuele has created code to simulate that without special equipment, and in a way that is repeatable and makes it possible to experiment.</p>

<p>But that was only part of his work: another part of the project was to make it possible to paint with more than one colour at the time and to be able to make the brush pick up existing colours from the canvas: complex loading and bidirectional paint transfer. The colour mixing really grabbed Emanuele, leading him to create more and more sophisticated code, leaving little time for the complex loading and bidi paint transfer part of his project. However, he managed to check in a preliminary version <i>just</i> in time... and right now he is staying in his sea-side cottage coding, coding and coding. More commits are to follow!</p>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-colour-mixer.png"><img  src="http://static.kdenews.org/jr/koffice-soc-2007-colour-mixer-small.png" width="300" height="335" /></a>

<h3>Fredy Yanardi</h3>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-fredy.png"><img align="left" src="http://static.kdenews.org/jr/koffice-soc-2007-fredy-small.png" width="75" height="56"></a>

<p>The new Flake architecture for KOffice 2.0 not only enables us to create applications that integrate all kinds of possible content in specialised documents, there is also infrastructure to make it easy to add interesting functionality to shapes through plugins. Fredy's mission was to implement a number of these plugins, both to demonstrate the flexibility of our architecture and to add useful functionality to KWord. Remember, with KWord 2.0 rewrite we lost a lot of existing functionality because it has actually been a complete rewrite.</p>

<p>By now we have:

<ul>
<li>Bookmarks</li>
<li>Change Case</li>
<li>Autocorrection</li>
<li>Thesaurus with wordnet integration</li>
<li>Document statistics</li>
</ul>

<p>And because he was done before the end of the Summer of Code he added a new popup widget to select colours from palettes and edit new palettes.</p>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-static-docker.png"><img src="http://static.kdenews.org/jr/koffice-soc-2007-static-docker-small.png" width="300" height="188" /></a>

<h3>Marijn Kruisselbrink</h3>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-marijn.png"><img align="left" src="http://static.kdenews.org/jr/koffice-soc-2007-marijn-small.png" width="75" height="100" /></a>

<p>They said it could not be done. But Marijn did it: fully editable music right inside any KOffice document. Essentially a plugin of the same level of complexity as KOffice's justly famous formula editor. The plugin supports multiple voices, measures, adding and removing of notes of every description, loading and saving in ODF documents, import and export to and from the standard <a href="http://musicxml.org/">MusicXML</a> format. Marijn started his work already before the Summer of Code, when he was able to show notes in KWord <a href="http://www.valdyas.org/fading/index.cgi/hacking/musicflake1.html">when he visited me in April</a>. Interesting bits are the automatic line wrapping, which is more complex in music than in text.</p>

<p>This plugin more than anything else shows off the power KOffice has gained through the Flake architecture. Marijn is very enthusiastic and will continue working on the music flake: connected notes, lyrics, multi-page music and fallback support for OpenOffice and other ODF-compatible applications are all on his to do.</p>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-music-shape.png"><img src="http://static.kdenews.org/jr/koffice-soc-2007-music-shape-small.png" width="300" height="251" /></a>

<p>Oh, and as you can see... it is actually possible to seamlessly develop KOffice on OS X, too.</p>

<h3>Pierre Ducroquet</h3>

<img align="left" src="http://static.kdenews.org/jr/koffice-soc-2007-ducroquet-small.png" width="125" height="154" />

<p>Confession time! Right after the KWord rewrite started, KWord did not support ODF at all anymore. Pierre's work therefore included essentially recreating ODF support, together with his mentor, Sebastian Sauer. He was present at the ODF meeting in Berlin, in the KDAB offices, and has gone from strength to strength. Not too mention that he had to hack stuff into KWord that was not even supported by Qt Scribe like the line-through styles. Heck, even OpenOffice doesn't support this part of the standard! Among the other hurdles he had to jump was a test suite that depended on OpenOffice.org default behaviour that contradicted the ODF standard. Great work, and <a href="http://behindkde.org/people/soc2007-two/">Pierre</a>, too, has said he will keep working on ODF support in KOffice. </p>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-kword-odf.png"><img src="http://static.kdenews.org/jr/koffice-soc-2007-kword-odf-small.png" width="300" height="178" /></a>

<h3>Sven Langkamp</h3>

<img align="left" src="http://static.kdenews.org/jr/koffice-soc-2007-sven.png" width="100" height="146" />

<p>Krita never had the famous marching ants selection visualisation invented by <a href="http://www.folklore.org/StoryView.py?project=Macintosh&story=MacPaint_Evolution.txt&topic=MacPaint&sortOrder=Sort%20by%20Date">Bill Atkinson for MacPaint</a>. We have always shown our selections as masks, following my philosophy that there is not that much difference anyway. Besides, I did not know how to do marching ants.  Only marching ants is perhaps a little meager for a whole summer of code project, so Sven took on the whole selection-is-a-bitmap thing.</p>

<p>Last year, Emanuele created a bezier curve framework for Krita that led to magnetic selections and bezier curve selections: however, those curves were mercilessly converted to pixels. Given the hard work Jan Hambrechts and Thorsten Zachmann have put into the flake vector path shape, it seemed silly not to try and re-use that in Krita.</p>

<p>Krita's selections now consist of two components: a masks that allows gradations of selection and editable paths that allow editing of the path shape at all times. The combined selection can be visualised as a mask, as marching ants, both or not at all. Sure, there's still a lot of work to do, and I managed to break some of his work when working on nodes, layers and mask &mdash; my bad! &mdash; but Sven has advanced Krita 2.0 in a really important way.

<p>A selection that consists of vectors:</p>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-vector-selection.png"><img src="http://static.kdenews.org/jr/koffice-soc-2007-vector-selection-small.png" width="300" height="179" ></a>

<p>And one that consists of a pixel mask:</p>

<a href="http://static.kdenews.org/jr/koffice-soc-2007-pixel-selection.png"><img src="http://static.kdenews.org/jr/koffice-soc-2007-pixel-selection-small.png" width="300" height="179" /></a>

<h3>In Other Words...</h3>

<p>Like last year, the KOffice hackers have been very lucky with the students who offered to work on our project. The interaction has been great, with students coming to the ODF hack sprint and Akademy. KWord now has three people regularly working on it instead of just one. That said, there is still plenty of space in KOffice for aspiring hackers who want to cut their teeth on a challenging, visible and fun project. If you feel that there should be a presentation tool more fun than Keynote, a diagram application cooler than Omnigraffle, a chart app that makes statistics blush when they lie &mdash; or if you think file formats are really cool, then do not wait for next year. <a href="https://mail.kde.org/mailman/listinfo/koffice-devel">Join us now</a>!</p>








