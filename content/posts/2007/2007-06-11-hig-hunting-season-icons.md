---
title: "HIG Hunting Season: Icons"
date:    2007-06-11
authors:
  - "oschmidt"
slug:    hig-hunting-season-icons
comments:
  - subject: "Freedesktop"
    date: 2007-06-11
    body: "WTF? The Freedesktop icon specification looks just directly taken from Gnome. They even haven't had the decency to create new names. Each time I see something like this I like Freedesktop less. They're supposed to provide desktop-independent standards and instead they're just taking things from Gnome and telling everyone to follow them as standards. Which KDE things have they promoted to standards? DBUS? Well, there was no acceptable alternative from the Gnome side. Anything else? .. That's not what I come up with when I'm thinking of desktop-independence."
    author: "djworld"
  - subject: "Re: Freedesktop"
    date: 2007-06-11
    body: "Well, I've said \"desktop-independent\" but I meant \"integrating things from desktops in a way which is not particular to any desktop\". But what they understand is \"let's take everything from Gnome and make the KDE guys embrace them as lambs\"."
    author: "djworld"
  - subject: "Re: Freedesktop"
    date: 2007-06-11
    body: "ignoring that d-bus was based on the lessons learned from dcop ... here are some:\n\nthe icon theme spec; .desktop file spec; get hot new stuff / xds; netwm .. those are the ones that pop to the top of my head.\n\ni'm not sure what your issue is with the naming, to be honest. some names had to be picked, none of us used the same names, it makes sense to use some of what exists somewhere already.\n\noxygen will be adding a large number of names to the spec as well (we'll be pushing them upstream as we get closer to release and names are less likely to shift).\n\nlooking beyond fd.o technologies around kde have seen impressive usage. html has been taken up with a lot of enthusiasm by industry with apple, nokia and adobe (among other smaller companies) basing core products on it. qt is widely used and just recently trolltech donated the font glyph composition code to the broader community; you'll see it in future versions of the software gnome uses to render their fonts (among others).\n\nall that said, i have run into the problem of non-kde projects being far less interested in adopting things that make sense from outside their little fiefdoms. personally, i see that as a sign of maturity on the part of the kde project. and in the end, it benefits us much more than any negatives that might come along (which have been very few, btw).\n\ne.g. the media center project that is taking root right now is happening with kde 100% because we are the project that is willing, able and capable of looking beyond its own fences.\n"
    author: "Aaron Seigo"
  - subject: "Re: Freedesktop"
    date: 2007-06-11
    body: "what media center project?\n\n\n:D"
    author: "superstoned"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "I too would like to know which media center project is being referred to here."
    author: "smiledawgg"
  - subject: "Re: Freedesktop"
    date: 2007-06-13
    body: "looks like it's linuxMCE that's being integrated into KDE4 by aseigo and others.\nThis is great!....Can't Wait!\n\nhttp://wiki.linuxmce.com/index.php/Version_1_1"
    author: "smiledawgg"
  - subject: "Re: Freedesktop"
    date: 2007-06-11
    body: "It's not just the naming itself, as I said. You're making a great job on the technical side, of course, and I think I'll use Plasma very often when KDE 4 comes out. KDE has always had its own great identity and it's just that I don't like seeing how this identity gets weaker as it gets closer to our competitor desktop. See Dolphin for example, to me it's very similar to Nautilus. Having the \"ease of use\" as an excuse it doesn't expose the user the cool features KDE has that differentiate it from others, like the KIO slaves (I don't think Dolphin is a bad idea AT ALL, as long as Konqueror still stays there; although I definitely won't use it). Or the Home icon in Oxygen, it looks very similar to its counterpart from Gnome. It's just an example of KDE's identity getting closer to that of Gnome. Interoperability with other desktops is necessary, of course, but making KDE closer to Gnome is not a good idea.\n\nMost users simply take the desktop that comes with their distro, you know. They may read about Plasma and Solid in digg.com but they don't really know what it is and don't care about KIO, DCOP (now DBUS),... and since unfortunately now \"the distro\" means Ubuntu they're getting Gnome. KDE should be very different from Gnome to their eyes, otherwise they won't find reasons to change to \"some strange thing called KDE that looks like more of the same\" instead of keeping \"the old well-known thingy that works\" and it may lead newbies to say things like \"ey, which Gnome is that one you're using?\". I don't think we're going to get much users that way and I don't like seeing KDE loose market share. We should keep an own identity and improve our marketing if we want to get a higher market share. How did Ubuntu become the most used distro? Through innovative features no other distro ever had or just through marketing, sending free professional-looking packed CDs to everyone?"
    author: "djworld"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "\"See Dolphin for example, to me it's very similar to Nautilus. Having the \"ease of use\" as an excuse it doesn't expose the user the cool features KDE has that differentiate it from others, like the KIO slaves\"\n\nHow about actually waiting until Dolphin is anywhere near to completion before casting judgement on it? It's only about a year old! And I don't know what you're driving at with your \"kio_slaves\" statement - do you mean to imply that Dolphin doesn't support them? If so, you are wrong, and any file manager that has usability as its goal should have network transparency as a matter of course.  \n\nWhy on Earth do people keep assuming that \"usability\" is the same thing as \"lack of features\"? That's pretty much the exact opposite of usability!"
    author: "Anon"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "I agree. KDE3 was a little hard to use in a \"so many checkboxes!\" kind of way compared to other desktops. So the KDE guys are improving it for KDE4. And if this new and improved KDE has a few GNOME-like elements then let it be. Its better than keeping with the old KDE3 ways. At the end of the day KDE is still going to be leaps and bounds ahead of GNOME.\n\nAnd if you're going to talk about market share look at the bigger picture, Unix desktops still have less than a few percentage points!\n\n---------\nschalken.wubbles.net"
    author: "Schalken"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "From what I've seen its more about organising the checkboxes than removeing any. Look at Konsole in the Road to KDE 4 for the perfect example.\n\nDolphin (needs a K) is the only exception, but it makes up for it by being a lot more lightweight. IMO that's its real advantage not ease of use. "
    author: "Ben"
  - subject: "Re: Freedesktop"
    date: 2007-06-13
    body: "And reorganising checkboxes is indeed a valid method of improving usability. It's probably safe to say GNOME chooses the other method - remove them.\n\nEither way, the KDE desktop is only getting better.\n\n--------\nschalken.wubbles.net"
    author: "Schalken"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "> Why on Earth do people keep assuming that \"usability\" is the same thing as \"lack of features\"? That's pretty much the exact opposite of usability!\n\nBecause of Gnome's constant misuse of the term? "
    author: "Ben"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "The ones misusing the term are those bashing GNOME's decisions."
    author: "Reply"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "My ideal is the shell, I has all the features but you don't have to see them.\n\nIt is important that user interfaces don't get cluttered.\n\nOne application - one task, the old unix rule and guess what: it works."
    author: "max"
  - subject: "Re: Freedesktop"
    date: 2007-06-13
    body: "> It is important that user interfaces don't get cluttered.\n\nThat's why KDE is organiseing the interfaces. Its possible to have lots and lots of options without a cluttered interface.\n\n> One application - one task, the old UNIX rule and guess what: it works.\n\nOne extordanary powerfull application, one task. By cutting features and hiding options Gnome is going against the UNIX philosophy. "
    author: "Ben"
  - subject: "Re: Freedesktop"
    date: 2007-06-11
    body: "Have Freedesktop people solved the security hole in the desktop files ? Last time I checked, it was still there (.desktop files can run programs even without the executable bit set, and they can also set their own icons, so creating a malicious file is not too hard). I think some of their standards are good/useful, but sometimes, I would like people to stabilize/correct specs instead of adding more stuff."
    author: "oliv"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "Thats not really a security hole, any app/script that is run can run something that it has read access to.\n\nFor example, they could just copy the file in question over to somewhere where they have access (like /tmp) and run chmod 755 on it. Or if its a script they could just run /usr/bin/perl /path/to/script or /bin/bash or whatever type of script they are trying to run.\n\nWhat about if you have /tmp mounted with noexec and are using normal binaries, you say?\n\nWell, try this:\n\n$ cp /bin/ls /tmp/\n$ chmod 644 /tmp/ls\n\nif you try to run it now you'll get a permission denied error:\n$ /tmp/ls\nbash: /tmp/ls: Permission denied\n\nbut run it like this instead (the file in /lib may be called something else on non-x86 architectures):\n$ /lib/ld-linux.so.2 /bin/ls\n\nand it works! even if you don't have execute permission, and even if /tmp is mounted noexec.\n\nAs you can see marking something as not executable doesn't have any benefits at all security wise, so why should .desktop files try to fix the unix security model?\n\nPersonally I don't see this as a security flaw, its just the way its made, marking something as executable is more of a convenience thing so you can keep track of what is meant to be executed and what is nto."
    author: "TPC"
  - subject: "correction"
    date: 2007-06-12
    body: "oops, the last line should obviously be /tmp/ls not /bin/ls."
    author: "TPC"
  - subject: "Re: Freedesktop"
    date: 2007-06-12
    body: "I see this as a flaw, because of the fact that .desktop files can set the name displayed in the file managers and the icon used. So you can create a file with an icon of e.g. image file, a nice foobar.jpg name, non executable, which will run a script deleting your home directory. It is the accumulation of those \"minor\" flaws which makes a serious hole in my opinion.\n"
    author: "oliv"
  - subject: "Re: Freedesktop"
    date: 2007-06-13
    body: "It dose sound like a flaw to me. But if someone can get a .desktop onto you're computer and point it to what they want they'll probobly just chmod+x their .evilscript  \n\nPersonally I'd just modify it so that perl, python, bash and .desktops refuse to run an non executible binary. That way not giving executible permissions will be a valid security move. "
    author: "Ben"
  - subject: "kdelibs icons"
    date: 2007-06-11
    body: "I plan to do a full review of the Oxygen icon names in kdelibs at aKademy. A few months ago, I compared the icon names in devices/ with the icon naming spec, and they differed substantially. I believe most of the ones that I found still do. The highest number of remaining name offenders is likely to be found in actions/.\n\nKen got the modification proposal list back then, but nothing has been done yet. The difficulty here is that renaming icons to their proper names (and making sure new icons are created with proper names already) can't be done with a single 'svn rename', it requires collaboration between artists and committers with a full checkout of KDE, so that the icons can be changed in all places where they are used. aKademy seems like the ideal place to get all the necessary people together and resolve this issue in teamwork mode.\n\nSo, kdelibs does not yet contain a full \"implementation\" of the icon naming specification. Searching for misuses or missing icons, like proposed in this part of the HIG Hunting Season, makes sense nevertheless :)"
    author: "Jakob Petsovits"
  - subject: "ben"
    date: 2007-06-12
    body: "Is there any list to check if an icon theme is complete?"
    author: "semsem"
  - subject: "Re: ben"
    date: 2007-06-15
    body: "Actually, NO there isn't.  We must always presume that no icon theme is ever complete.\n\nThe current list of the standard names is here:\n\nhttp://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html"
    author: "James Richard Tyrer"
  - subject: "Icons: \"go-*\""
    date: 2007-06-14
    body: "I notice that the icon naming spec does not yet make the distinction between: \n\n    go-previous  & go-back\n\n    go-next & go-forward\n\nThe current KDE icon names include: back, forward, next, & previous.  All 4 of these icons are necessary since all four of them might appear on the same toolbar in some applications.\n\nActually, the new: \"go-next\" icon corresponds to the old: \"forward\" icon and the new \"go-previous\" icon corresponds to the old: \"back\" icon.  And, the current Oxygen icon set does not include an icon named \"go-back\" and \"go-forward\". \n\nThere is a clear difference in meaning here.  The icons \"go-back\" & \"go-forward\" should be used for what the spec calls a \"list action\" while the icons: \"go-next\" & \"go-previous\" should be used for sequential actions such as browsing or a search.  \n\nYes, that does mean that the icons \"forward\" & \"back\" are not really the correct actions.  This certainly confounds the issue since that is the current common usage.\n\n"
    author: "James Richard Tyrer"
---
The great work of the Oxygen icon artists is a much discussed and anticipated part of KDE 4. The new icons now follow the <a href="http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html">freedesktop.org naming specification</a> which makes it easier to share icons between applications of several desktop environments. In the HIG hunt this week, we will check that this work lives up to its full potential by looking for missing icons and wrong uses. Read on for more details.

<!--break-->
<p>The most important part of the checklist is searching for misused or missing icons, both in the default icon theme Oxygen, and in the Monochrome icon theme needed for accessibility. A second topic is checking for use of the KDE system settings. Most applications will use the correct setting automatically via the KDE libraries, so this is mainly about corner case bugs.</p>

<p>One particular issue of the guidelines has been very controversial in the past (defaulting to an application-specific icon theme in Amarok). The HIG team is not interested in starting any flamewars, so please skip this issue when doing these checks. The checklists are meant as guidance for developers, not as laws.</p>

<b>Reporting Procedure</b>
<p>The procedure for reviewers is pretty simple:</p>
<ol>
<li>You pick an application which is included in KDE 4 Alpha, but not yet reviewed by another person. See <a href="http://wiki.kde.org/tiki-index.php?page=hig_hunting_season">this wiki page</a> for reference.</li>
<li>You open a checklist and go through the checklist items.</li>
<li>For each infringement you find, post a bug. In the title, write "HIG" and the number of the checklist item which is not met (e.g. CL4/1.4).</li>
<li>To test for the existence of monochrome icons, make sure that the monochrome icons from the kde-accessibility module are installed. Select the icon theme in KControl, then restart KDE to ensure that all applications are aware of the new setting.</li>
<li>On the wiki page of the checklist, add a section for the application you reviewed and link all bug reports you have created.</li>
</ol>

Happy hunting!
