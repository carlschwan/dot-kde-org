---
title: "KDE App of the Month is Back with kdesvn"
date:    2007-04-17
authors:
  - "mvolkert"
slug:    kde-app-month-back-kdesvn
comments:
  - subject: "Rajko Albrecht on software patents"
    date: 2007-04-17
    body: "The interview has a very nice quote on software patents:\n\n   \"In my opinion software is something like photography:\n ideas are in 95% percent  always the same. But the photo\n itself is the special. Eg., I try to protect my photos.\n But I would never piss someone having the same idea\n making their own photos.\""
    author: "Jos van den Oever"
  - subject: "What about integration with Quanta and Kdevelop?"
    date: 2007-04-17
    body: "KdeSVN is great, the best SVN  client I've tried. But because there is not integration with Quanta or real good integration with Kdevelop it's really hard to take advantage of it.\n\nI listened that maybe Cervisia will include SVN support so Quanta will have SVN integration, but no have listened no more about it.\n\nNote: Of course SVN kioslaves are NOT a solution for using SVN in Quanta and so."
    author: "I\u00f1aki Baz"
  - subject: "Re: What about integration with Quanta and Kdevelop?"
    date: 2007-04-17
    body: "kdesvn can be integrated with Quanta via Quanta's supportfor arbitrary Kparts as plugins. Go to Settings->Configure Plugins, add kdesvnpart (the file name is kde3/libkdesvnpart.la in Debian). Check 'read only part', select 'Project Folder' for input and whichever output window you prefer. Set the name and icon. Then, it will show up on the plugins menu."
    author: "qwandor"
  - subject: "Re: What about integration with Quanta and Kdevelop?"
    date: 2007-04-17
    body: "Thanks, didn't know about it. I suppose this feature is relatively new since I couldn't find it some mouths ago. \n\nThanks!"
    author: "I\u00f1aki Baz"
  - subject: "Re: What about integration with Quanta and Kdevelop?"
    date: 2007-04-19
    body: "I do not think it is particularly new, but it is well hidden."
    author: "qwandor"
  - subject: "Re: What about integration with Quanta and Kdevelo"
    date: 2007-04-17
    body: "For integration of kdesvn in quanta see\nhttp://wiki.kde.org/tiki-index.php?page=Quanta%20Plus%20Using%20Tips\n"
    author: "Mark Volkert"
  - subject: "Re: What about integration with Quanta and Kdevelo"
    date: 2007-07-07
    body: "hello, I've followed the schema you proposed but the plugin won't work on quanta.\nI have slackware 12 and\n\nQt: 3.3.8\nKDE: 3.5.7\nkde-config: 1.0\n\nI've double checked that the path is correct.\nkdesvn itself works fine.\n\nany idea?"
    author: "sathia"
  - subject: "Very nice and extremely useful app, but ..."
    date: 2007-04-17
    body: "... my #1 wish for future versions would be icon overlays in Konqueror to indicate the status of versioned files. Those who ever used TortoiseSVN know what I mean. I know that this is not currently possible with Konqueror, as Rajko told me in reply to a bug report, so this plea goes to the Konqueror devs: Please add an interface (D-Bus?) for the KDE 4 version of Konqueror (and maybe Dolphin) to make this possible. :-)\n\nBest regards and thanks for this great program,\nChris"
    author: "Chris"
  - subject: "Re: Very nice and extremely useful app, but ..."
    date: 2007-04-17
    body: "I use TortoiseSVN at work and like it too.\nBut the overlay is what I don't like. Specially when you have more than e.g. 200 project files, you don't get an overview about changed files. The file list is too big and you have to scroll. Crazy file icons make it even worse.\n\nI like the different view modes of konqueror. Viewing directory contents like a file manager or viewing directory as a repository incl. a lot svn information.\n\nRegards, Mark"
    author: "Mark Volkert"
  - subject: "Re: Very nice and extremely useful app, but ..."
    date: 2007-04-17
    body: "> But the overlay is what I don't like. Specially when you have more than e.g. 200 project files, you don't get an overview about changed files.\n\nI think both are useful.\n- overlays to see what's changed in one folder.\n- a list of files for lots of changes. I use the \"SVN commit..\" menu in TortoiseSVN for that.\n- and perhaps a \"svn diff | colordiff\" overview because it still does things I can't do in a GUI.\n\nI really like how TortoiseSVN does things. Konqueror/kdesvn should learn from it too."
    author: "Diederik van der Boor"
  - subject: "Re: Very nice and extremely useful app, but ..."
    date: 2007-04-17
    body: "Why don't you switch to the kdesvn kpart?\nkonqueror will nicely embed it and show you icons with overlays.\n\nAnd if you have\nSettings -> Save View changes per folder,\n\nkonqueror will automatically switch to kdesvn mode each time you enter the directory again.\n\n"
    author: "Michael"
  - subject: "Re: Very nice and extremely useful app, but ..."
    date: 2007-04-30
    body: "I have currently the same problem with Kdesvn and rapidsvn.\nBoth clients are - hate to say that - lightyears away from\nTortoiseSVN. Kdesvn have IMO a lot of bugs. After \nupdate it says the files need a update in a loop. A commit fails\nwith a locked file and so on. It ofen crash. \nIf it hangs totally, i have to reboot Windows to fix the\nsituation in TortoiseSVN.\n\nEven TortoiseSVN supports drag and  drop to create a versioned\ncopy from e. g. branch to trunk. rapidsvn looks like to work\na bit better, but doesn't have drag'n drop and my try to\nmove a folder fails.\n\n"
    author: "Carsten"
---
After one year of silence we are back with another issue of App of the Month. This time we selected a developer tool, <a href="http://www.alwins-world.de/programs/kdesvn/">kdesvn</a>. It is a well integrated KDE client for <a href="http://subversion.tigris.org/">subversion</a>. The overview takes a look at some basic functions. We also have an interview with kdesvn's developer Rajko Albrecht, covering the development process and much more. Enjoy application of the month in <a href="http://kde.org.uk/apps/kdesvn/">English</a>, <a href="http://kde.de/appmonth/2007/kdesvn/">German</a> and <a href="http://cekirdek.pardus.org.tr/~baris/kdesvn/description.html">Turkish</a> (<a href="http://cekirdek.pardus.org.tr/~baris/kdesvn/interview.html">Turkish interview</a>).





<!--break-->
