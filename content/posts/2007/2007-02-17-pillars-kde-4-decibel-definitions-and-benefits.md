---
title: "The Pillars of KDE 4: Decibel Definitions and Benefits"
date:    2007-02-17
authors:
  - "nogden"
slug:    pillars-kde-4-decibel-definitions-and-benefits
comments:
  - subject: "Sincere thank you."
    date: 2007-02-17
    body: "Pillars = What a great stream of articles. Lots of questions are answered. Thanks for you work. "
    author: "Daniel"
  - subject: "Re: Sincere thank you."
    date: 2007-02-17
    body: "You're welcome. My pleasure! Stay tuned for more. :)"
    author: "Nathan Ogden"
  - subject: "Re: Sincere thank you."
    date: 2007-02-17
    body: "He's right, this series is brillaint.\n\nAnd tell me, what's the next pillar going to be? solid? phonon?"
    author: "ben"
  - subject: "Re: Sincere thank you."
    date: 2007-02-17
    body: "Oxygen is our next target. We are just now starting to work on it, though."
    author: "Nathan Ogden"
  - subject: "Re: Sincere thank you."
    date: 2007-02-18
    body: "This series is brilliant!\nHi quality content on the dot, you can usually find it on the digg front page, seems to be a very good promotion.\n\n\n-stephan"
    author: "stepan"
  - subject: "Re: Sincere thank you."
    date: 2007-02-17
    body: "I absolutely agree. \n\nThe dot has gained so much quality with the KDE reports, I can't believe how much (quantity) and good (quality) content it has now.\n\nYou really help the community building.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Sincere thank you."
    date: 2007-02-17
    body: "Seconded!"
    author: "Darkelve"
  - subject: "Digg this"
    date: 2007-02-17
    body: "Help spread the word about this wonderful articles:\nhttp://digg.com/linux_unix/The_Pillars_of_KDE_4_Decibel_Definitions_and_Benefits"
    author: "Tsiolkovsky"
  - subject: "Re: Digg this"
    date: 2007-02-17
    body: "Help making blogs a better place and kill these Digg-posts from every blog out there. They are f'ing *annoying* and nothing more than useless spam for Digg.\n\nYou're even worse than Jehovah's Witnesses."
    author: "Does_not_digg_it"
  - subject: "Re: Digg this"
    date: 2007-02-17
    body: "100% agreed"
    author: "Mark Hannessen"
  - subject: "Re: Digg this"
    date: 2007-02-17
    body: "Digg has millions of visitors each day, if we can KDE on its front page that's really a great exposure for KDE. People that don't even know about KDE might learn about it. What's wrong with that? I don't get it."
    author: "Patcito"
  - subject: "Re: Digg this"
    date: 2007-02-17
    body: "The odds of someone who frequents a tech site such as Digg not knowing about KDE are small, in my opinion.  Also, have you not seen the huge backlash against Ubuntu on that site due to the fact that there are so many articles about it? There is such a thing as over-exposure, you know :)"
    author: "Anon"
  - subject: "Re: Digg this"
    date: 2007-02-17
    body: "Thats defianately true. but this article is definately worthy of being read. Perhaps the best thing to do is let the digg readers, rather than the dot.kde readers decide whats best for dig."
    author: "Ben"
  - subject: "Re: Digg this"
    date: 2007-02-17
    body: "Don't confuse a \"campaign to simulate popularity of KDE\" with \"helping to promote KDE\". \n\nAnd readers of digg, could you please not post links to digg here, if the sole purpose is to to the above? The readers of digg likely can make their own decisions.\n\nAnd honestly, do you think that if you don't know KDE, but read this article, you are going to learn anything?? That article is for KDE3 users.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "houston?"
    date: 2007-02-17
    body: "wasn't houston renamed recently? It would be good to mention something like this in a \"definitions list\"."
    author: "lynx"
  - subject: "Re: houston?"
    date: 2007-02-17
    body: "Yes, I actually removed move references to Houston, just calling the thing Decibel daemon in most places.\n\nIt still uses \"Houston\" in its name on the D-Bus, so I did not ask Nathan to change that entry. Developer's will stumble over the name at some point;-)\n\nBest Regards,\nTobias"
    author: "Tobias Hunger"
  - subject: "Re: houston?"
    date: 2007-02-17
    body: "so the d-bus name differs from the name used everywhere else? would it make sense to harmonize the two so it's a bit more obvious and follows the 'principle of least suprise'? this would probably need to be done before lots of applications start using it (and therefore relying on 'houston'), of course...."
    author: "Aaron J. Seigo"
  - subject: "Re: houston?"
    date: 2007-02-18
    body: "You are right, it does make sense to do get rid of the name completely and I am contemplating to rename the service from de.basyskom.Decibel.Houston to de.basyskom.Decibel. I would like to wait a bit longer before I go through with this change (a release or two), just to make sure nothing else will pop up that I need to stick into de.basyskom.Decibel.\n\nThe Decibel client library contains a couple of string constants with all the important service names and object pathes, so renaming the daemon is not a big deal (at least while binary compatibility is not an issue).\n\nIt might be nice to put the Decibel service into org.kde at that point. I used de.basyskom mostly because I did not want to pollute the org.kde namespace without getting permission first."
    author: "Tobias Hunger"
  - subject: "Re: houston?"
    date: 2007-02-18
    body: "Renaming de.basyskom.Decibel.Houston to org.kde.decibel makes a lot of sense. \n\nThe process of getting permission is a simple email to the kde-core-devel mailing list. I am sure that there will be no objections.\n\nOlaf\n"
    author: "Olaf Schmidt"
  - subject: "About that office suit."
    date: 2007-02-17
    body: "An office suite could use Decibel to embed chat or even video conferencing with the author of a document or support channels.\n\nHow exactly will the office suit know who to call? extra metadata?\n\nand will there be a Koffice window with an ICQ chat in it, or will Koffice just call Kopete?"
    author: "ben"
  - subject: "Re: About that office suit."
    date: 2007-02-17
    body: "It should support decibel, and just tell it to open a chatwindow. It can be embedded, yes..."
    author: "superstoned"
  - subject: "Re: About that office suit."
    date: 2007-02-17
    body: "<technical blahblah>\nThe idea is to ask the Decibel daemon for a channel an ID of a contact stored e.g. in Akonadi. Decibel will then either return that channel to the calling application to do with it as it pleases (useful for those developers who want to write their own chat software) or it can invoke a channel handler for it. The channel handler is a Decibel-aware application (e.g. based on kopete)  configured by the user for a combination of protocol, type of data transmission and contact.\n</technical blahblah>\n\nSo a office suite could e.g. look up the author of a document in its metadata, check for the name found there in Akonadi and then ask Decibel to connect to it, using whatever the user had configured as a GUI to handle the actual communication. One D-Bus call is all that takes:-)"
    author: "Tobias Hunger"
  - subject: "Re: About that office suit."
    date: 2007-02-17
    body: "well that awnsers everything. \n\nThank you."
    author: "ben"
  - subject: "Collaberative editing"
    date: 2007-02-17
    body: "I love the idea two people can open a document and work on it at the same time. I can't see myself useing it but it feels powerfull and fun to have it around. Like dd."
    author: "ben"
  - subject: "Re: Collaberative editing"
    date: 2007-02-17
    body: "I've used this functionality before with Google Docs and it is in-fact very handy.\n\nI want it in kdevelop!"
    author: "Ian Monroe"
  - subject: "Re: Collaberative editing"
    date: 2007-02-17
    body: "Well the usefullness depends entirely on you're persoanal circamstances ;)\n\nAs for Kdevelop, seems like their's no reason why not. "
    author: "Ben"
  - subject: "Decibel hackathon announced"
    date: 2007-02-17
    body: "Tobias just announced ( http://lists.kde.org/?l=kde-core-devel&m=117166156214361&w=2 ) that in March, there will be a Decibel Hackathon. Some kopete and Akonadi people already confirmed that they'll be there.\n\nSo we can expect much more progress on the integration part with other KDE4 technologies. :-)\n"
    author: "Sebastian K\u00fcgler"
  - subject: "POTS"
    date: 2007-02-17
    body: "In case any one was wondering why a new telephony system would be designed and then called \"plain old telephony system\" it is because that term is an American tongue and cheek variation of the proper meaning \"Post Office Telephone System\". I believe the system was first designed and implemented by the the British post office and later adopted around the world.\n\nJust a small correction, but for some reason I felt compelled to tell."
    author: "David Taylor"
  - subject: "Packages"
    date: 2007-02-17
    body: "openSUSE packages to test Decibel are at http://software.opensuse.org/download/home:wstephenson/openSUSE_10.2 (install source)\n\nNo KDE is needed, since it builds cleanly on Qt 4.\n\nYou can have a look at the test programs, or get into the APIs and header files and maybe write a UI.  Tip: http://developernew.kde.org/Development/Tutorials/D-Bus/Accessing_Interfaces is a really good read."
    author: "Bille"
  - subject: "Re: Packages"
    date: 2007-02-17
    body: "openSUSE is great!\nThey even offer kde4 packages:\nhttp://software.opensuse.org/download/KDE:/KDE4/\nLatest build is 14 feb, that's just 3 days ago!!"
    author: "otherAC"
  - subject: "aKademy 2006 Video"
    date: 2007-02-18
    body: "You can watch an aKademy 2006 video about Decibel here (http://home.kde.org/~akademy06/videos/aKademy06-Decibel_-_You_Are_Not_Alone!_-_Tobias_Hunger.ogg).\n\nThe PDF of the slides of the presentation is here (http://akademy2006.kde.org/conference/slides/decibel_akademy2006.pdf).\n\nI forgot to put the links in the main article."
    author: "Nathan Ogden"
  - subject: "Great"
    date: 2007-02-20
    body: "Great project.\nI'm impressed with KDE.\nI'm starting a project to finance some GPL projects like KDE. If everyting well I will post a new here, a need the support of a teleco enterprise, this week I will have got more information.\n\nThanks"
    author: "M.Roman"
---
In <a href="http://dot.kde.org/1170892771/">part 1</a>, we gave a general overview of <a href="http://decibel.kde.org/">Decibel</a>. In this part, we cover everyone's favorite section - the definitions! Well, at least we hope that the definitions will be informative. We describe some benefits for developers and benefits for users. Read on for the details.





<!--break-->
<p>Decibel is a service that is concerned with real time communications; therefore, everything that connects one user with another user and makes it possible to get replies instantaneously is in the scope of Decibel. Decibel is based on the <a href="http://telepathy.freedesktop.org/wiki/">Telepathy</a> <a href="http://www.freedesktop.org/wiki/Software/dbus">D-Bus</a> API's and uses the <a href="http://tapioca-voip.sourceforge.net/wiki/index.php/Tapioca">Tapioca</a> implementation of these API's.</p>

<h2><a name="part2">Definitions</a></h2>
<p>A few Decibel-related definitions:</p>
<h4>Real Time Communication (RTC)</h4>
<p>Real time communication refers to all computer-supported interactive means of communication. This includes text chats (AIM, MSN, IRC, Jabber, etc.), telephony (VoIP or CTI), video conferencing, and more. Other means of communication such as email and newsgroups are not instantaneous, and as such are beyond the scope of Decibel.</p>

<h4>Computer Telephone Integration (CTI)</h4>
<p>Computer Telephone Integration deals with a phone connecting to and being controlled by a computer. For example, the computer could be used to dial a phone number on the phone using the computer's address book. Also, the computer could display the contact information of an incoming call (by looking up the incoming phone number from the computer's contact data).</p>

<h4>Telepathy (the project)</h4>
<p>Telepathy is a project being hosted at freedesktop.org. Its focus is to create a set of API's that talk to Real Time Communication services. These API's are based on D-Bus and are pretty low level.</p>

<h4>D-Bus</h4>
<p>D-Bus is also a freedesktop.org project, heavily influenced by KDE's DCOP, and used as a simple means of communications between applications. The primary purposes of D-Bus are for communication between desktop applications (for better perceived integration), and communication between desktop applications and the operating system (including running system daemons and processes).</p>

<h4>Voice over Internet Protocol (VoIP)</h4>
<p>VoIP, or Voice over Internet Protocol, is the delivery of voice conversation over an IP-based network. This can be over a local network, using VoIP technologies for intra-office communications, or over the internet for inter-personal communications. VoIP service may make use of an analog POTS (Plain Old Telephone Service) line for access to the traditional telephone system, or simply a connection between two VoIP applications.</p>

<h4>Tapioca (the project)</h4>
<p>Tapioca is a project that is working towards implementing the Telepathy specification. Those working on the Tapioca project provide language bindings that are not available from the Telepathy developers. They also attempt to smooth over the 'rough edges' of Telepathy somewhat.</p>

<h4>Houston</h4>
<p>Houston is a part of Decibel. It is a policy daemon that tracks the user's online status for all communication channels they use, persistently stores settings, reacts to connections initiated from external sources, and more.</p>

<h2><a name="part3">Developer Benefits</a></h2>
<p>One potential benefit for developers is reported by Tobias: "Application developers will find with Decibel a centralized place to store real time communication settings like account data and online states, a means to establish outgoing connections using these settings and to react to incoming connection attempts. This makes it possible to do things like 'go offline with all my accounts' or 'notify me on all incoming text chats so that I can log them'".</p>

<p>Tobias continues, "Decibel will make it easy for a developer to do things like 'start a text chat with the person with these contact details.' Currently, an application developer will need to find and access the user's account data (which can be scattered over several applications), find a protocol the user and the requested contact have accounts for, bring that account online (using one of several libraries) and then initiate a chat session. Decibel tries to hide all these details from an application developer if he does not want to care".</p>

<p>Developers with experience in real time communications and those interested in working on Decibel itself are the most likely to be interested in developing for Decibel, although any developer would likely receive some benefit. Keep in mind though that Decibel will not automatically make someone a good programmer. It will just enable good programmers to be more efficient. Having said that, it will enable your application to better integrate with other applications, thus increasing the desirability of the application.</p>

<p>Interested developers can help in several ways. The build system used for Decibel has some problems that need to be resolved. Also, the API's need to be tested. This includes things such as connecting the Houston daemon to <a href="http://pim.kde.org/akonadi/">Akonadi</a> and creating a plugin mechanism for Houston so that it can become desktop-neutral. Other issues to be worked on include writing protocol implementations following the Telepathy specification and coming up with graphical interfaces for the demonstrations of Decibel's capabilities. The Decibel website could also use an overhaul.</p>

<p>To find out more about the project, developers can visit the project <a href="http://decibel.kde.org/">website</a> and Tobias' <a href="http://basysblog.org/">blog</a>. Chatters can visit the IRC channel #decibel at irc.freenode.net. Please also visit <a href="http://www.nlnet.nl/">NLnet</a>, the organization sponsoring the development of Decibel.</p>

<h2><a name="part4">User Benefits</a></h2>
<p>Since Decibel is a service rather than an application, users are not likely to see direct benefits from Decibel. Rather, the benefits they see will be indirect ones. Also, keep in mind that while these benefits are possible, it is still up to each application to decide what features will or will not be used.</p>

<p>There are two main factors to keep in mind in dealing with the benefits of Decibel. First, since Decibel deals with Real Time Communications, the benefits would be realized in this arena. Second, since there is currently no comparable system with which to compare Decibel, all examples of benefits will be what Decibel 'could do' as opposed to what Decibel 'does do'.</p>

<p>However, these two factors do not mean that the benefits users see must be small. On the contrary, the integration Decibel provides make it possible for users to see some exciting benefits in at least two major areas.</p>

<p>First, applications normally associated with real time communications can add more features. For instance, an email program could use Decibel to update the online status of contacts in its address book and mail views.</p>

<p>Second, applications not normally associated with real time communications could use Decibel to implement communication features. An office suite could use Decibel to embed chat or even video conferencing with the author of a document or support channels. Since Decibel will make it easy to set up communication channels between users, it might even jump start the development of collaboration features. For example, a graphics program could use Decibel to set up a communication channel to another instance of itself running for another user somewhere on the internet. This channel could then be used for collaborative editing of a graphics document.</p>

<p>Decibel just reached the version 0.2.0 milestone which is a mostly feature-complete proof-of-concept implementation of the framework. Upcoming versions of Decibel will focus on integration into the KDE environment as well as improving the existing functionality and demo applications. Decibel will need some more releases before it can be used widely. Obviously, much work remains to be done. However, we hope you have a better understanding of the future possibilities with Decibel.</p>


