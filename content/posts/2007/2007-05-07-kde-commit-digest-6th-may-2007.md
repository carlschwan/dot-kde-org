---
title: "KDE Commit-Digest for 6th May 2007"
date:    2007-05-07
authors:
  - "dallen"
slug:    kde-commit-digest-6th-may-2007
comments:
  - subject: "Yay!"
    date: 2007-05-07
    body: "Is it sad I have spent quite a few hours refreshing the dot waiting for today's commit digest? :)\n\nThanks a lot for your excellent work, Danny!"
    author: "E"
  - subject: "RSS..."
    date: 2007-05-07
    body: "Ever heard about RSS? KDE even has a reader called Akregator..."
    author: "zonk"
  - subject: "Re: RSS..."
    date: 2007-05-07
    body: "He would then be having to refetch the RSS feed, no difference made."
    author: "Sutoka"
  - subject: "Re: RSS..."
    date: 2007-05-08
    body: "But he wouldn't have to hit the fetch button himself, would he? ;)"
    author: "Jucato"
  - subject: "Re: RSS..."
    date: 2007-05-08
    body: "Well if he was using Konqueror he could just set Auto-Refresh and then he wouldn't have to click either. (I wonder how long we could do this till someone says he could just have someone from the Dot tell him on IRC?)"
    author: "Sutoka"
  - subject: "Re: RSS..."
    date: 2007-05-10
    body: "And Konqueror then puts an icon into the systray to alert about the arrival of a new message?\n\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: RSS..."
    date: 2007-05-08
    body: "this is a silly thread =)\n\nakregator watches the rss for you and pops a nice little icon in your systray when there is something to read. hooray! =)"
    author: "Aaron J. Seigo"
  - subject: "Re: RSS..."
    date: 2007-05-07
    body: "... but probably, at least in this case, Akregator isn't able to turn on <ironic mode> :)"
    author: "me"
  - subject: "Re: RSS..."
    date: 2007-05-07
    body: "Well, I would do the same - I don't use RSS feeds personally ;)\n\nDanny\n"
    author: "Danny Allen"
  - subject: "Re: RSS..."
    date: 2007-05-20
    body: "but, but, akgregator is shiny!\nit's like tabbed browsing... I didn't see the point until I tried it, and now I get really annoyed if I have to go without."
    author: "Chani"
  - subject: "Re: Yay!"
    date: 2007-05-08
    body: "Yes."
    author: "RandomOne"
  - subject: "Re: Yay!"
    date: 2007-05-09
    body: "It's not good that there is no RSS feed favicon in firefox/opera's adres bar. If this favicon would be there user \"E\" haven't had to spent a few hours refreshing the dot waiting for digest. :)\n\nThink about this. This would be better becouse non kde users which don't have agregator could subsxcribe to Dot!"
    author: "Dolphin rulez ;)"
  - subject: "Re: Yay!"
    date: 2007-05-10
    body: "There is an \"rdf\" link on the left side of the main page, that leads to the feed. Though I really think it should be more visible, and better described, so that non-techies can find it..."
    author: "zonk"
  - subject: "Good stuff!"
    date: 2007-05-07
    body: "This is probably the commit-digest that has me the most excited. Obviously, the KDE 4 Alpha is pretty cool, but I'm a big fan of KTorrent, I've been looking forward to generic music store support in Amarok (got to get emusic!), and while I'm not entirely satisfied with it, KFTPGrabber is still my FTP client of choice.\nIt's a great selection for me!"
    author: "Alec Munro"
  - subject: "Dotters!"
    date: 2007-05-07
    body: "Nice to see that \"got the Dot\" comments are readed by developers and attracting them.\n\n\n \"However, it is not all doom and gloom. In the aftermath of a KDE News article detailing the changes, many people with programming knowledge have stepped forward to see if they can save their favourite games. I predict that the cut list will be shorter when KDE 4.0 is released (and again shorter when KDE 4.1 is out).\"\n\n\"Remove the double frame around the view which KDE.dotters complained about.\"\n\n\" Remove frame around playing field and margin between playing field and window border - for consistancy with other games ( eg kreversi , kbounce )\"\n\n\"Remove frame and spacing around play area. Removed frame around score box. Use flat group boxes for the info pane. This works well in the Clearlooks and upcoming Oxygen styles, but not so well in the KDE 3ish Plastik theme.\""
    author: "Emil Sedgh"
  - subject: "Re: Dotters!"
    date: 2007-05-08
    body: "That's fantastic!!\n\nAwesome, which such small changes KDE4 will look MUCH nicer and cleaner, thanks for all devs which read the comments and changed the frames!"
    author: "Martin Stubenschrott"
  - subject: "KTorrent vs QBitTorrent"
    date: 2007-05-07
    body: "How does KTorrent compare to the newer QBitTorrent in terms of speed and memory efficiency?\n\nhttp://qbittorrent.sourceforge.net/\n\nThe authors of QBitTorrent claim that by building on top of LibTorrent (http://www.libtorrent.net) there is less overhead in coding the app because the \"business logic\" is shared with other Bittorrent projects and consequently there are more people to fix bugs. LibTorrent also claims to be very fast and memory efficient, and there is no language barrier with Qt apps because LibTorrent is also written in C++."
    author: "AC"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-05-07
    body: "Qbittorrent is very good. I use at it home, and it averages around 21MB of ram usage. It is a minimal client, but I love the ram usage, and plus it does a decent job of downloads :) I am hoping that ktorrent catches up, I love its interface much better. "
    author: "Sarath"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-05-07
    body: "Given that \u00b5Torrent is satisfied with less than 6MB of RAM, some people prefer Wine'ing it instead. Other (like me), switched to console-based rTorrent. Linux still needs a good, non-bloated GUI BT client..."
    author: "zonk"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-05-08
    body: "Why?  Bloat is highly overrated given the appearance of shared libs \"bloating\" the profile of the app.  Isn't an apps job to make life easier through features etc?\n\nPersonally I think KTorrent is incredible.\n\nMaybe those small footprint apps are great and easy to use etc .. but the \"bloat\" comments are SOOO late 1990's."
    author: "Jim"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-05-08
    body: "While on the whole I agree with the sentiment, beware that linux still runs on old systems.  There are people who first try linux on old machines from the late 1990s, and bloat is still an issue there.   Also, in the embedded market cost is often a major factor, so systems are very under powered compared to what you can buy locally: bloat is a major issue again.   (linux is very strong in the embedded market)\n\nSo while KDE as a whole needs to move beyond bloat - after all normal people can afford 1 gig of RAM, and 2+ Ghz processors (the former was unaffordable in the late 1990s, and the latter didn't exist at any price), so we should take advantage of them.  However lets not forget where we came from either.   Sometimes bloat is still an issue.   Where we can prevent bloat we should do so.  However where bloat == features that people love we should add bloat."
    author: "Henry Miller"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-05-09
    body: "I think he was referring to the fact that the amount of memory used by kdelibs that ktorrent use is counted on ktorrent's memory usage, even if they are shared libraries that are already loaded by the desktop, other programs, etc. So even if ktorrent per se was pretty lightweight, that overhead would still make it seem bloated. Of course, using ktorrent on a desktop other than kde would result in high memory usage because then ktorrent would more likely be the only user of those libraries."
    author: "Fede"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-05-08
    body: "Well, maybe the comments are SOOO late 1990's, but my PC is not so late 1990's..."
    author: "zonk"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-05-09
    body: "GUI _is_ bloat for a bittorrent client IMO.  That's why I use rTorrent (CLI, no ncurses, C++)."
    author: "Matt"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-05-10
    body: "Yes, I do use uTorrent with Wine, for two reasons:\nI did not found any torrent client as fast (downlading speed) as it exept for Azureus that is way too bloated.\nIt is very lightweight, even running with Wine, and my PC is a Duron 1.6 with 512 RAM, so less bloat IS A GOOD THING yes. :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: KTorrent vs QBitTorrent"
    date: 2007-09-11
    body: "have not used QBittorrent as of yet, but i love Ktorrent.  I have never had such a positive expieriance with torrent in YEARS.  I used to only get acceptable speeds with bittonado, and utorrent was never my cup of tea.  azures uses a JVM (i dont have ANY java on any of my machines...ever)  plus i never really like it when i did use it on a buddy's linux workstation.\n\n"
    author: "Techzilla"
  - subject: "Blu_Ray and HD_DVD at Solid"
    date: 2007-05-07
    body: "Will we have a KhdDVDbackup at KDE4? Is it possible to merge at KDE4 (if someone code a GUI frontend to hdDVDbackup) such software? \n\n09-F9-11-02-9D-74-E3-5B-D8-41-56-C5-63-56-88-C0\n\n"
    author: "Anderson"
  - subject: "Re: Blu_Ray and HD_DVD at Solid"
    date: 2007-05-07
    body: "probably not. very limited user base, anyway?\nso what?\n\n<09-25-17-02-116-227-91-216-65-86-197-99-86-136-192>_10\n\n"
    author: "Gregor"
  - subject: "Re: Blu_Ray and HD_DVD at Solid"
    date: 2007-05-08
    body: "I am sure you can use a kde frontend for mencoder to do this..."
    author: "Mark Hannessen"
  - subject: "KDE hen&egg"
    date: 2007-05-07
    body: "So the alpha version is going to be released? \n\nI wonder when you can recommend me to install a developer version. Are there guides on how to compile KDE?\n\nI mean currently KDE4 coding is a bit monasty style, hmm?"
    author: "Bert"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-07
    body: "There have been several snapshot releases aimed at outside application developers already. http://techbase.kde.org/ has a large amount of guides and tutorials covering KDE4 coding."
    author: "Eike Hein"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-07
    body: "> I wonder when you can recommend me to install a developer version.\n\nnow is a decent time.\n\n> Are there guides on how to compile KDE?\n\nyes and they are on TechBase: http://techbase.kde.org/Getting_Started\n\n> I mean currently KDE4 coding is a bit monasty style, hmm?\n\nmonasty style? hm.. monastic style? in a convent for monks? not sure i'm getting what you're saying here =) care to explain more?"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-09
    body: "I think they mean cathedral style."
    author: "AC"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-07
    body: "There will be on techbase a detailed guide to compile the Alpha 1 tarballs so you will be able to set up your development user as soon as the Alpha 1 is released.\nWelcome to the exciting KDE 4 development world!"
    author: "Anne-Marie Mahfouf"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-07
    body: "When is it going to be released, by the way? Did it get pushed back to some concrete date, or is it a \"any minute now\"?"
    author: "ac"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-08
    body: "October 23rd 2007"
    author: "Patcito"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-08
    body: "I was asking about the aforementioned Alpha 1."
    author: "ac"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-08
    body: "Anytime now, indeed. I expected it last week, actually..."
    author: "superstoned"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-08
    body: "Wasn't it supposed to be released a week after being tagged? If so it would be out on the 10th."
    author: "Luca Beltrame"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-08
    body: "Hey, that's my birthday! Nice present :-)"
    author: "Mike"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-08
    body: "what about a live CD?"
    author: "Christopher B. Pye"
  - subject: "Re: KDE hen&egg"
    date: 2007-05-08
    body: "Don't worry, there'll be a few :D"
    author: "superstoned"
  - subject: "Kate search bar"
    date: 2007-05-07
    body: "Konversation (for KDE 3) has had such a search bar for a while now :). It's great."
    author: "Peter"
  - subject: "Re: Kate search bar"
    date: 2007-05-08
    body: "yeah, I wonder if there is any progress on the debate about getting such a thing into kdelibs..."
    author: "superstoned"
  - subject: "Re: Kate search bar"
    date: 2007-05-08
    body: "http://bugs.kde.org/show_bug.cgi?id=93439"
    author: "logixoul"
  - subject: "Re: Kate search bar"
    date: 2007-05-08
    body: "yeah, I'm subscribed to that bug. but the last comment is from march... :("
    author: "superstoned"
  - subject: "Credit where credit is due"
    date: 2007-05-07
    body: "Wow I had a bit too much wine yesterday when I wrote the message to Danny... :)\n\nI forgot something really important, the original author of the KDE3 userscript plugin is neofreko (blog.neofreko.com). I took the code and ported it to kde4 but he did the most important parts..."
    author: "NamShub"
  - subject: "Re: Credit where credit is due"
    date: 2007-05-08
    body: "Yeah, I also had too much Wine yesterday. Native linux apps are much better."
    author: "joker"
  - subject: "What about Plasma ?"
    date: 2007-05-07
    body: "I'm so sad to see that, again, nothing is going on with Plasma... Are all the KDE developers working on games ? 'cause that's a fact, games (to me, the least important part of KDE) are evolving quickly, and Plasma is still just an idea, a project, nothing more :s\n\nShould expect it to be ready for KDE 4.0 ? Or 4.1 ? Or never ?"
    author: "Cypher"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-07
    body: "Repeat with me\n\"You can't force volunteers to do what you want, they do what they want\""
    author: "Albert Astals Cid"
  - subject: "Again?"
    date: 2007-05-08
    body: "Please go and read the comments in past articles. This discussion is *really* getting old..."
    author: "Luca Beltrame"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-08
    body: "Plasma is ready, all the libs are in trunk. Now it's time for people to write applets. There are tutorial on techbase, it's pretty easy, you can have a look and try if you want."
    author: "Patcito"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-08
    body: "I wonder how many people are aware of this? It would be great if the Plasma site could be updated to reflect this and hence get more people involved in writing applets."
    author: "Jeff Parsons"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-08
    body: "erm. no. thanks for the positivity though ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-08
    body: "Plasma is ready for what?\n\nTo be a retread of SuperKaramba?  Whatever happened to this:\n\n\"It is time that the desktop caught up with modern computing practices and once again made our lives easier and more interesting when sitting in front of the computer. Just like those icons did for people back in 1984.\n\nDevelopment of KDE4 has just begun, and it is during these major release cycles that we have the opportunity to retool and rethink our applications and environment at the fundamental level. The fact that the current desktop concepts have lasted this long is a testament to their effectivity, and we should not simply abandon all sense of the familiar and the useful. Yet we can not stay where we are either.\n\nThis, then, is the goal and mandate of Plasma: to take the desktop as we know it and make it relevant again. Breathtaking beauty, workflow driven design and fresh ideas are key ingredients and this web site is your portal onto its birth.\""
    author: "T. J, Brumfield"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-08
    body: "Would you *please* stop posting the same thoughts over and over? I'll say it again, you're doing no good to Plasma, in whatever state it is. Being negative accomplishes no purpose."
    author: "Luca Beltrame"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-08
    body: "I 100% Agree with this.Please, give devs some time and stop this negative energy.at this time, these negative feedbacks are more than just comments.these are really disgusting!\nso I want to ask you, \"Could You Please Stop This?!\"\n"
    author: "Emil Sedgh"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-09
    body: "I don't think -either- of these two extreme approaches works very well, or is particularly constructive: continually asking \"where's Plasma?\"/\"is the Plasma dream dead?\"/\"why isn't anyone working on Plasma?\", OR the response to it, \"stop asking about Plasma\"/\"you can't force FOSS developers to work on something they don't want to\"/\"give them time\".\n\nHowever, most of the questions about Plasma seem to be because people are interested in knowing where it's at and where it's going. Sure, there are a couple of posts that get a bit rude, but most of the rest seem perfectly reasonable to me; I dare say there would be a lot of people interested in helping out by writing Plasmoids, or testing this or that (myself included in both groups).\n\nThe problem is that without knowing anything about where Plasma is or where it's going, we have no idea of what we -should- be able to do yet, or whether we -should- be waiting for some milestone before trying to do X, Y or Z. Wiki pages often seem to be out of date, so there really is no reliable information to base these decisions off.\n\nAsking questions that could be useful to everyone if answered is a -lot- more constructive than giving a blanket response to essentially \"shut up; asking questions is not allowed here\".\n\nI don't mean any offence by any of this; I'm just trying to get the point across that snapping at people who could be -helping- KDE isn't going to help anyone."
    author: "Jeff Parsons"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-09
    body: "Hi\nSure, everyone is intrested to know about development of Plasma and where is it going and many many other things.but, asking it every week and every day will not help.nothing is not changed from last discussion, so there will no answer!\n\nbelieve me, asking will not help, it will hurt!\n\nI think giving Plasma Ideas will help much better ;)"
    author: "Emil Sedgh"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-09
    body: "I'm sorry, I wasn't aware of this previous discussion. I've had a look, but can't seem to find anything. I'd be greatly appreciative if you could provide a link or links; any information like that would help me. :)\n\nI just hope these previous questions didn't get similar answers, or their existence wouldn't really be a reason for people not to ask again. Also, it would help if the more important results of these discussions were reflected on the Wiki; I'm sure there are others like myself who had no idea they even took place, because we've missed some important thread in a \"dot\" article.\n\n\"I think giving Plasma Ideas will help much better ;)\"\n\nThe problem there is that people -can't- give constructive ideas or input of any kind if they don't know Plasma's direction at all. Offering the same suggestions over and over for things that are already decided, or things that don't fit in with Plasma's current goals, or are too far in the future... none of these could help, either.\n\nBelieve me: I'm not the kind to blurt out, for example, \"well that sucks!\" if the answers are that Plasma is still in an initial planning stage, or X important aspect of it is yet to be decided; such an answer would help me because I'll at least know where to start with offering suggestions or solving outstanding problems.\n\nI'd love nothing more than to help, and to help others help! :)\n\nThanks\nJeff\n\n(p.s. I understand that the first part of my post could be construed as sarcasm, given how many times I've seen things phrased that way with sarcastic intent. Please understand that I did -not- mean it in this way, but that I have a splitting headache and can't seem to think of a way to avoid risking that interpretation.)"
    author: "Jeff Parsons"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-09
    body: "Hi\nprevious discussion was on KDE 4 Games news, but we have such thread in every 2-3 weeks once and we get no answer!\n\nand about giving ideas, currently Plasma is not completely designed.so we have this chance to give ideas and then, Devs will see the ideas and try to design Plasma with our ideas.this is what i think.maybe devs could help us on this?\n"
    author: "Emil Sedgh"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-10
    body: "My main gripe has been the lack of direction on the project.\n\nIgnoring it, and still not having a direction for Plasma is neither an answer nor a solution.\n\nAsking that the team pick a direction is not harmful.  Ignoring the lack of a direction is.\n\nThere are already a great deal of mockups and concepts for the team to select from.  I could offer opinions on ones I've favored, but effectively I've done that by rating some of the mockups.\n\nAre you honestly telling me that you don't think it is an issue that there hasn't been any decisions on how to refactor the workplace?  Or that these concepts are still being bounced around right now, as opposed to picking a direction, say 18 months ago?"
    author: "T. J, Brumfield"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-10
    body: "Actually I've suggested that the team solidify a game plan, and pick a direction.  Right now there are tons of great mock-ups, some of them mentioned in the todo lists for Plasma, but some of the concepts discussed are exclusive to one another.\n\nQuite frankly, the Plasma planning/discussion phase should have passed a great deal ago.\n\nI didn't create a website and promise a revolution.  I didn't give interviews and hype the project.  I didn't tell people that I was delivering something grand.\n\nIf I knew anything about QT coding, I'd jump in myself.  And honestly, that is one of my goals right now, to brush up on my admittedly weak C++ skills and learn about QT4.\n\nI am more than willing to volunteer time and help out projects.  I've done it before, and I'll do it again.\n\nBut the ball was dropped here.  KDE4 planning has been going on for ages.  Plasma has been the most discussed aspect of it, for ages.  And yet, not only has there been little in actual coding, but the initial stages of planning seemed to be skipped over.\n\nI am very excited about every single aspect of KDE4, except Plasma.\n\nMy comments are fair.  What we are getting is simply a new means to create widgets, and one super-app that removes my choice to swap out certain apps.  I have less freedom on the desktop, and more overhead.\n\nAs for rethinking the workplace, grand innovation, etc. I haven't seen any.\n\nAgain, pull up the Plasma web page.  Read over it, and compare that to what we are getting.  Tell me there isn't a great disparity there.\n\nIgnoring the situation isn't productive either.  Quite frankly, while I will check out all the newly revamped games, I'm pretty sure most users would have preferred that the flagship element of KDE4 get some attention.\n\nAnd you may call me rude, but I'm quite polite in comparison to the bile and vitrol that will be spewed by countless people who are looking for any reason to bash on KDE.  If Plasma launches merely in its present form of a refactored SuperKaramba, expect critics around the web to take notice.\n\nI absolutely love KDE.  I wish the KDE community the best.  However, the fastest growing distro right now is Gnome based.  More and more focus is on Gnome-based apps and development.  There is a big anti-Novell/Suse backlash because of the MS deal, and thusly one of the big pro-KDE distros is losing favor.\n\nI'm concerned about the future of KDE.  And the last thing the community needs is for there to be a major negative backlash when KDE4 finally goes out the door.  There are only a few months before the expected 4.0 release.\n\nI am hoping and praying that in that time, the Plasma team manages to prove me wrong and implement at the very least one feature that does rethink how we operate on the desktop, and perhaps look at some of the highest rated 4.0/Plasma mockups."
    author: "T. J, Brumfield"
  - subject: "Re: What about Plasma ?"
    date: 2007-05-09
    body: "Yo,\n\nYou are, I think it's worth pointing out, not wrong. Plasma has not been making visible progress, as far as I can tell.\n\nHowever, that doesn't make the tone or implications of your comment right, either.\n\nSee, the thing is, the people who'd make Plasma the awesome thing it surely is in your head, are currently very, very busy making about the whole rest of KDE 4 awesome. Seriously, look into what people like Zack Rusin and Aaron Seigo and Lubos Lunak and many others have been up to lately. The awesome /is/ getting worked on, just not the Plasma awesome specifically.\n\nAt this point, the one person that is in the best position to make Plasma move forward is you, personally. I'm very serious. You sound passionate about Plasma, and that's GREAT. KDE needs more passionate people! Have you tried getting in touch with people like A. Seigo to ask what you can do to get started? Given the visibility of something like Plasma, you're likely to have it gain a lot of traction within a few hours of your posting the first screenshots and working patches.\n\nAre you up to it?"
    author: "S."
  - subject: "Search bars?"
    date: 2007-05-07
    body: "I do like the search bar of kate - however, I don't like that there is still no shared thing to do that.\n\nKate has its own implementation, konversation has, and also does konsole. But especially konqueror shows not sign whatsoever of integrating that search technique - although it is a bug-wish with high votes.\nWhy not develop one solution and simply share it among all apps?\n\nThat really gets strange/disappointing now..."
    author: "liquidat"
  - subject: "Re: Search bars?"
    date: 2007-05-07
    body: "The most important thing from your perspective is not how it works under the hood, but whether the search bars look and behave in a consistant manner.  This is still very much do-able.\n\n> Why not develop one solution and simply share it among all apps?\n\nThere is more application specific code involved here which cannot be shared among applications.  You couldn't develop the search bar widget, put it in kdelibs and throw it into Konqueror and magically get incremental search in one line of code ( unlike say a font or open file dialog which you create, run and get a simple return value and dispose of ).\n\nOf course, there are parts we could share ( mainly the search bar widget - which is very simple code-wise ), but we missed that opportunity and will have to revisit it after KDE 4.0\n"
    author: "Robert Knight"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "Ah, ok, I wasn't aware of the fact that searching is so application specific.\n\nSad, because I'm afraid that we will not see such a search in konqueror anytime soon under these circumstances :("
    author: "liquidat"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "Well in Konqueror you can press the slash key '/' and then type away. It will search the current page without the need for a search dialog."
    author: "Matt Williams"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "But this is a totally hidden option, and people still use CTRL+F and still gets the classic modal search dialog, which is IMO very ugly and not really usable (it covers the most important part of the screen: the document you are searching in!!)"
    author: "Davide Ferrari"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "Yes, I know that this feature is there - but there are\n- first several reasons why this feature is only available for a minority (read: geeks who use simple input systems; it is not for normal people and not for people who have to input Chinese or other characters like that)\n- second many reasons to implement a Firefox like search bar as well - replace the ctrl+f search with a Firefox like bar, and you can still keep your / search\n\nFor more information read the bug http://bugs.kde.org/show_bug.cgi?id=93439 , the discussions show very clear the problems with the other available search functions (there are three in Konqueror atm)."
    author: "liquidat"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: ">but we missed that opportunity and will have to revisit it after KDE 4.0\n\nWhy? It's not like kdelibs are frozen solid. A small but already widely used thing like this should be cleaned up and included. The benefit regarding HIG and UI consistency would outweight any problems by including it after the freeze by far. It's about 4 or 5 different implementations of it already in different apps, so why not use a little bit of common sense regarding the freeze.\n"
    author: "Morty"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "there's also 4.1. we can't simply keep adding stuff to 4.0 forever; it has to stabilize and what not at some point. but something like this can certainly be added in 4.1."
    author: "Aaron J. Seigo"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "For you information, okular has one as well ;)\n\n> Kate has its own implementation, konversation has, and also does konsole.\nSo okular does.\n\n> Why not develop one solution and simply share it among all apps?\nAs Robert pointed out already, it's not really easy to do that. Every search bar relies on specific stuff of the application, thus reducing the potential sharing to just a simple widget.\nSpeaking for okular, the code that does that is quite simple, as you can see:\nhttp://websvn.kde.org/trunk/KDE/kdegraphics/okular/ui/findbar.cpp?view=markup\nIf one of the concern is usability and common look&feel of all the search bars, I don't think it would be a problem \"unify\" their behaviours and their looks - an HIG would help here."
    author: "Pino Toscano"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "I find it odd that there is so little code to share - the ctrl-F dialog is in KDElibs, right? why can't it be redesigned and (preferably configurable) replaced with this widget?"
    author: "superstoned"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "> I find it odd that there is so little code to share - \n> the ctrl-F dialog is in KDElibs, right?\n\nYes.  The search bar however is not a dialog, so managing it is different.  The find dialog itself doesn't actually do very much either.\n\nIt requires changes to both kdelibs and the applications, the search bar is not quite a drop-in replacement.  kdelibs is now frozen, so as I said before we'll have to revisit that after 4.0\n"
    author: "Robert Knight"
  - subject: "Re: Search bars?"
    date: 2007-05-08
    body: "Well, I guess that might not even be a bad thing, as the search bar can mature a little before it's added to kdebase..."
    author: "superstoned"
  - subject: "Nuvola !"
    date: 2007-05-07
    body: "Yeah - always been my favorite Icons-Set !! And now its imported in KDE4\n\nthis rocks so much !"
    author: "srettttt"
  - subject: "Re: Nuvola !"
    date: 2007-05-08
    body: "Think the same... been using Nuvola for years and never went back to Crystal..."
    author: "NabLa"
  - subject: "and the small folder icon!"
    date: 2007-05-10
    body: "it's similar to the crystal one and it rox! i think the 0xygen scheme has to beat a certain standard here.."
    author: "eMPee"
  - subject: "Re: and the small folder icon!"
    date: 2007-05-10
    body: "..set by this screenshot i forgot the link to!\nhttp://www.kde-look.org/CONTENT/content-pre2/5358-2.png"
    author: "eMPee"
  - subject: "Breaktrough in menpower of the KDE development!"
    date: 2007-05-07
    body: "> Commit Demographics \n>    \n> Sex \n> 99.3 %             Male \n> 3.75 %             (unknown) \n> 0.548 %            Female\n\nWhich makes 103.6 percent KDE developers. ;) Is that already considered in the release plan, or can we now expect an earlier release?"
    author: "furanku"
  - subject: "Re: Breaktrough in menpower of the KDE development!"
    date: 2007-05-07
    body: "Hehe, i've had some problems generating the statistics this week (and they usually don't add to 100 anyway, due to rounding issues ;))\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Breaktrough in menpower of the KDE development"
    date: 2007-05-08
    body: "Future KDE developers have in fact discovered time travel and are working on existing projects using HyperKate and the KIO::spacetimecontinuum IO slave."
    author: "Borker"
  - subject: "disapointed"
    date: 2007-05-09
    body: "personally I keep finding myself disapointed with the updated releases of kde.  I wish those that work on kmail would completely overhaul it and release a decent email application, rather one that is so aimed at secruity it is really like using outlook express 5, well, would have to be earlier than that because OE5 had a lot more features avaible for content rich emails than this joke of a email client :(  It really is the only let down of linux, and what holds a lot of companies back from using KDE, instead using gnome."
    author: "Dreamer"
  - subject: "Re: disapointed"
    date: 2007-05-09
    body: "Ok, thanks for your trolling. Now, go back where you came from"
    author: "Vide"
  - subject: "Re: disapointed"
    date: 2007-05-10
    body: "Good on you for providing valuable feedback. The community can't grow without it."
    author: "fred"
  - subject: "New Oxygen smileys"
    date: 2007-05-10
    body: "Hey, I like them very much! Really nice and funny! (I would like to see the 16x16 version, though, to see how they scale down)"
    author: "Davide Ferrari"
  - subject: "kwin_niftiness"
    date: 2007-05-11
    body: "Right... i can't believe noone's mentioned that before... mv kwin_composite kwin is quite possibly the niftiest thing this time 'round (bar all the other very nifty things that's been going on obviously ;) ). Silently, KDE 4 Alpha will include it's very own amazing niftiness in the window manager area. In short: We have beauty on the desktop :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: kwin_niftiness"
    date: 2007-05-11
    body: "Yeah, it's weird how nobody has commented on this.  Anyway, looking at the code, it's amazing how much can be achieved in just a few lines (like the rest of KDE, I suppose :)) so well done again to Lubos, Rivo and Phillip!\n\nStill needs a great deal of optimisation, though - I have an nvidia 5200 which Beryl *screams* on, but simple things like kwin_composite's \"minimise\" animation are *incredibly* slow - I'd estimate about \"3 frames per second\" slow (you can very nearly count tthe individual frames) :(. I've no idea how it would run on my far less powerful integrated Intel on my laptop (on which Beryl is still very smooth).\n\nStill, I'm sure this will be tackled in due course :) Maybe Zack Rusin could be persuaded to take a look ... ?"
    author: "Anon"
  - subject: "Re: kwin_niftiness"
    date: 2007-05-11
    body: "Maybe it's because of the combination of x.org's horrid xcomposite extension and nvidia's half-broken xrender implementation. I don't see anything using xcomposite to have any kind of acceptable performance.. ever.\nMaybe there will be a kwin_opengl at some point."
    author: "JustMeh"
  - subject: "Re: kwin_niftiness"
    date: 2007-05-11
    body: "\"Maybe it's because of the combination of x.org's horrid xcomposite extension and nvidia's half-broken xrender implementation. I don't see anything using xcomposite to have any kind of acceptable performance\"\n\nNo, because, as mentioned, Beryl and Compiz are *very* fast and smooth on it.  "
    author: "Anon"
  - subject: "KATE - easier plugin support"
    date: 2007-05-12
    body: "I really hope they are planning to add easier plugin support for Kate in KDE4... It's quite insane that the only thing I dread when I switch completely back to Linux this summer (I'm on an iBook now, but spend most of my time in Ruby or in an ssh session to a Linux box), is TextMate! But the ease of adding your own snippets and the bundles you can already get are insane! If any of the developers of editors have not tried it yet, at least check out some of the screencasts. But what's important is not just how great the bundles are once installed - but how easy it is for users to add them...\n\nI recently helped a friend of mine downloading and converting the php tables that Wikipedia uses to convert form traditional to simplified Chinese, and converting it into a sed script - incredibly useful stuff... And now he wants to be able to select text in Kate, click a button and have it converted (piped to a shell command, and the selected text replaced with the output)... Apparently there is an extension for this, but you have to type the command each time, you cannot bind it to a key. \n\nI am not saying I want Kate to become KDevelop light. I don't need that. But I do expect that when I fire up a new editor, without any changes in settings, and I save a file as \"*.rb\", it automatically sets the tab settings to soft tabs-2 spaces, it colors it correctly (already), and when I type def<tab> it expands it properly. Basic stuff. (The syntax for definining snippets is also awesome. Of course this is where KDE really has the opportunity to do a one-up on Textmate - what if we could get some kind of a generic advanced (Textmate-ish) snippet support, shared for all text editing applications? Kate,  KDevelop, KMail, Koffice, konsole(?), Konqueror textboxes? If we could get this to work, I might be convinced to switch back from Firefox to Konqueror (especially with new \"greasemonkey\" support)... Or patch Firefox to do it as well, when running on KDE :) !!\n\n(Sorry, I cannot do C, so I won't be helping to implement this... I am looking forward till there are some KDE projects in Ruby, then I'd love to help!)\n(sorry for blabbering on, but I get really excited about this stuff!)"
    author: "Stian Haklev"
---
In <a href="http://commit-digest.org/issues/2007-05-06/">this week's KDE Commit-Digest</a>: Atlantik, KFouleggs, Klickety, KPoker, Kenolaba, KAsteroids, KSnake, KSokoban, KJumpingCube and KTron move to playground/games. KDE 3.90.1 (KDE Alpha 1) is tagged to be released. General improvements in <a href="http://ktorrent.org/">KTorrent</a>. Progress in the generic music store support in <a href="http://amarok.kde.org/">Amarok</a>. <a href="http://kftpgrabber.sourceforge.net/">KFTPGrabber</a> begins the port to KDE 4. The <a href="http://phonon.kde.org/">phonon</a>-<a href="http://solid.kde.org/">solid</a>-sprint branch is merged back into trunk. BluRay and HD-DVD support in Solid. <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>-KDE components moved from kdereview to kdelibs in time for KDE 4. kwin_composite is merged back into trunk, becoming the window manager for KDE 4. Further progress in the <a href="http://konsole.kde.org/">Konsole</a> refactoring effort, with the refactor branch merged back into trunk for KDE 4. More progress in kdegames. <a href="http://ksudoku.sourceforge.net/">KSudoku</a> moves to kdereview. <a href="http://namshub-kde.blogspot.com/">Konqueror Userscript Plugin</a> is imported into KDE SVN. <a href="http://www.kde-look.org/content/show.php?content=5358">Nuvola</a> is imported into kdeartwork as another new iconset for KDE 4.

<!--break-->
