---
title: "KDE Commit-Digest for 15th July 2007"
date:    2007-07-16
authors:
  - "dallen"
slug:    kde-commit-digest-15th-july-2007
comments:
  - subject: "KBFX?"
    date: 2007-07-16
    body: "\"KBFX, an alternate K menu, moves to kdereview\"\n\nAs I know KMenu and KBFX, both will replaced by Raptor Menu of Plasma and Kicker will have no place is 4.x Series.Where doest KBFX Wants to go?"
    author: "Emil Sedgh"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "It's in the realm of possibility that Raptor is \"just\" a renamed KDE4 version of KBFX since it's developed by the same team as far as I know. Raptor has been a very elusive target for some time now which is worrisome to a slight degree as the application launcher menu (or start menu  :-P) is a commonly used tool which has hopefully gone through some usuability review."
    author: "Erunno"
  - subject: "Just kill it!"
    date: 2007-07-16
    body: "IMHO, we should kill the menu-idea and integrate the Applikation-start in the desktop itself. But well, this, could be to innovative for most of the visiting windows-geeks :->\n\nBut, hopefully there comes one day a functional quicksilver-clone, so the menu-button could replace with a button to show the dialog of this clone ;)"
    author: "Chaoswind"
  - subject: "Re: Just kill it!"
    date: 2007-07-16
    body: "Hopefully, Plasma will lower the barrier to entry and making writing/ installing such things so easy that we'll see all sorts of novel and innovative designs thrown together.  Only time will tell, though :)"
    author: "Anon"
  - subject: "Keep stuff OFF the desktop"
    date: 2007-07-16
    body: "Personally, I rarely SEE my desktop, never mind use it.  Usability wise, it makes no sense to have windows that don't reach the edges of the screen.  It's easier to throw a mouse into the corners/edges and click, and it's better to have a larger, more detailed, full-screen view without contrast issues between the desktop and the background.  Combine that with multitasking, where I have lots of windows open, and even closing one still leaves other windows... they all close in time, but rarely back to a blank desktop.\n\nThis all makes the desktop pretty much useless for interactivity.  That's why we have taskbars; to provide an OS-interface that's actually visible and usable despite having windows open.\n\nSo, no, please don't put anything on the desktop.  Also, please don't make any of these new plasma things necessary.  Ideally, please make sure they're all fully dockable in the panel/kicker, too."
    author: "Lee"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-16
    body: "I rarely use my desktop as well, but that's partly due to the fact it's pretty useless now - so it's a habit not to use it! I'm open for changes, provided the desktop is easily accessible and useful...\n\n[i]Ideally, please make sure they're all fully dockable in the panel/kicker, too.[/i]\n\nDon't worry, they'll do that. Each applet will have 4 states, one on desktop, media center, panel and another one I don't recall."
    author: "jospoortvliet"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-16
    body: "Thats true. The Desktop ist mostly useless, and that's why he should become more functional. I think, it would also better if work more like a normal window, with a regular entry in the taskbar and such.  "
    author: "Chaoswind"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-16
    body: "Sometime ago, I made a suggestion that both of you could probably live with:\n\nhttp://home.earthlink.net/~tyrerj/kde/NO-menu0.png\n\nThe row of icons acts like a child panel that can be covered with other things but which is brought to the front when the mouse touches an edge or corner of the screen -- this would be configurable as would the position of the menu.\n\nClicking on these icons would bring up that section of the menu which would act just like it currently does."
    author: "James Richard Tyrer"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-17
    body: "That's possible now, but it makes it awkward (if not impossible) to have things show status, such as when you have mail waiting (note: not when it arrives), or when your network or cpu usage is climbing, or when you just want to keep an eye on the time regularly over the next few minutes before you have to go somewhere."
    author: "Lee"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-18
    body: "> That's possible now, ...\n\nYes, but some further improvements could be made if it was a special type of panel rather than just a regular child panel.\n\n> but it makes it awkward (if not impossible) to have things show status,\n\nThat is a child panel; it is in addition to the regular panel."
    author: "James Richard Tyrer"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-17
    body: "you never see your desktop, others go to it all the time. oh my, what ever will we do?! =)\n\nhow about this: we make the difference between panels and desktop go away. we make it so that anything that can appear on your desktop can also appear on a panel and the only difference that \"thing\" sees is a difference in a form factor variable.\n\nwhich is exactly what we've done.\n\nlet's go to the next step here: let's allow the desktop to be treated similarly to the rest of your windows and allow it to be brought to the front of the existing windows!\n\nwhich is exactly what we've got planned.\n\nthe original poster wanted to see a quicksilver clone. well, i'm not much on cloning (think of sheep! baaaa means no!) but krunner will be pretty close to providing what quicksilver does in 4.0 and we'll be able to continue improving it for 4.0+N\n\neveryone happy now?"
    author: "Aaron Seigo"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-17
    body: "\"everyone happy now?\"\n\nYep :)\n"
    author: "Lee"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-18
    body: "personally, I love what Gimmie did: http://www.beatniksoftware.com/gimmie/Main_Page\n\ndid you have a look at that?"
    author: "jospoortvliet"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-18
    body: "of course. i've also discussed the various issues with the author in person. we once shared a row of seats on an airplane even... made for a much less boring trip.\n\nthat said, i'm a lot less interested at the moment in creating something like gimme than i am in creating a system where creating things like gimie is stupidly simple for those that want it."
    author: "Aaron J. Seigo"
  - subject: "Re: Keep stuff OFF the desktop"
    date: 2007-07-25
    body: "Personally, I don't like having to lose any real estate with any program I am using.  Like an earlier post stated, \"Usability wise, it makes no sense to have windows that don't reach the edges of the screen.\"  Which is true, at least for me, which can be accomplished now by having the kicker panel \"auto-hide\".  However, I also like having certain things visible to me all the time.  Which makes what I want impossible right now, I can't have my cake and eat it too as the saying goes.  If I want to always see the clock, I can't have the window reach the edges of the screen, if I want the window to reach the edges of the screen, I can't see the clock.  I hope with all the compositing stuff that is going on now, someone will figure out how to use transparency to our advantage by letting us finally have AND eat that cake.  Why can't a small transparent clock reside above all windows,  with an easy drag to move it if it gets in the way, or click to hide it if necessary, or better yet, by simply moving the mouse to a corner.  That way we could have \"the best of both worlds\"."
    author: "Mike E"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "No, I dont think so.\nIf it was just a Rename, then...why rename?I dont know about its development team but Im sure to make a Plasmoid, you need to Create a DataEngine to Feed the Plasmoid.Raptor menu should be a Plasmoid that uses a DataEngine and as I know, there is no dataengine for Application List, so the raptor Team have to develop the DataEngine too...\nKBFX is a Kicker applet which doesnt work as a Plasmoid.\neven if it works, Im sure Plasma team doesnt wants an Official Plasmoid which is included in a Default KDE Workspace be a Not-Native Plasmoid that uses different architucture.\n"
    author: "Emil Sedgh"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "I'm aware that Raptor will be using Plasma as there's a projekt page about it on Techbase. I was more thinking about the user interface when calling it a \"renamed KBFX\" and less about the technical foundation. But you're right, of course, Raptor should be drastically different from a technical point of view."
    author: "Erunno"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "> there is no dataengine for Application List,\n\nIs'nt the new filewatch-engine useable for this? IIRC i read such a thing in their description."
    author: "Chaoswind"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "Thats a FileSystem Browser.There is a video showing it.I dunno where, I cant remember, maybe Youtube or Dev Blogs...\nRaptor Menu needs a DataEngine that gives information such as Categorized List of Applications, Newly Installed Applications, etc."
    author: "Emil Sedgh"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "From http://marc.info/?l=kde-panel-devel&m=118410336007163&w=3\n\n[..]the engine is intended to be used in making the kmenu replacement[..]"
    author: "Chaoswind"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "then, sorry for wrong information..."
    author: "Emil Sedgh"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "See http://techbase.kde.org/Projects/Plasma/Menu  for more uptodate info."
    author: "debian"
  - subject: "Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-16
    body: "I'm sorry but from what things look like now it would be absolutely crazy to consider using kbfx or raptor instead of Kickoff, the K menu developed by several top KDE developers and which comes as the result of several usability studies. It has also been in openSUSE and Sabayon by default for some time, and has worked incredibly well.\n\nFrom what I've seen of Raptor it seems to be a mild attempt at trying to replicate most of the Kickoff Stuff. Why, exactly, is Kickoff not just used? (I'm sure it could easily be ported if this was to be decided)\n\nYes, everyone keeps saying \"kicker will be out, plasma will draw the panel\"... yes, so what? Are we going to have a K menu or not? If we are, I can see absolutely no good reason for not having Kickoff in by default. It's incredibly awesome, makes you very productive, a lot clearer instead of the current KDE menu which is a little antiquated now, etc. There have also been extensive studies on it, which is great. \n\nSo: why not Kickoff? KBFX and Raptor (unless it pretty much just completely imitates Kickoff) are not viable options IMO. "
    author: "apokryphos"
  - subject: "Kickoff rocks"
    date: 2007-07-16
    body: "At least once you are used to it.\n\nWith some plasma-Modifications it could easily be one of the very best start menus available."
    author: "Max"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-16
    body: "<I>So: why not Kickoff? KBFX and Raptor (unless it pretty much just completely imitates Kickoff) are not viable options IMO.</I>\n\nSo basically you have no clue what Raptor will be like, but you've already decided that it will not be viable.  Hmm, maybe you should just stick to saying \"I like Kickoff.\""
    author: "Matt"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "No, I've seen several posts about it, the links provided on techbase, and the _original_ mockups for the idea. Again I ask, why was kickoff not considered? If it was, what were the reasons for rejecting it? I've had several discussions on #kde4-devel, #plasma etc where no reasonable (and even remaining) objections were presented. \n\nSo tell me :)"
    author: "apokryphos"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-16
    body: "because kickoff is completely useless? at leased if it still works like shown on the videos...\n\nkickoff completely destroys the main idea of a menu: easy browsing. it should as easy as possible to browse through the menu. back buttons instead of additional windows and scrollbars make a menu completely useless...\n\na normal menu is useable without any clicks (after its opened, at leased), you can switch bitween serveral levels/folders with the move of the mouse.\n\n\nkickoff needs at least a breadcrump like thing, and automatic resizing - instead of the back button and scrollbars..."
    author: "ac"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-16
    body: "The Main idea is that whit the favorite menu you shouldn't need to browse the applications menu. "
    author: "Luis"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "> kickoff completely destroys the main idea of a menu: easy browsing.\n\nVideos? The usability studies are fully available, with videos, for you to see. No-one told these people how to use the menu before they used it. The stats are also public: people are more productive with it.\n\nDespite what you've been tricked into thinking with the current K Menu, the main point of the menu is NOT to quickly browse _all_ applications, though that is _a_ function of it. The general user won't use more than 5 applications 95% of the time. Once you realise this it's almost senseless to give \"all applications\" precedence over favourites.\n \nThere are so many other great things with it that it's hard to decide really where to begin. Search with integrated results (alt+f2 and katapult's fatal flaw), quick access to history, nice, not too big. "
    author: "apokryphos"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "I think the main function of the main menu is to browse all applications. You can (currently) put icons of the most used applications on the panel, or you can put them on the desktop and put a quick browser applet for the desktop on the panel. You can even have more than one custom menus with the quick browser applet. And, there is also a place for most/recently used applications at the top of the standard K menu.\n\nEven people use 5 applications regularly, there _is_ a need for a menu for browsing all the applications, especially for a beginner who wants to explore the system.\n\nKickoff is much less configurable than the standard K menu. It is good for some people and useless for others. As Mark Twain said there are lies, damned lies and statistics."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "I completely disagree that that's the main menu's function. It's pointless to say that the menu's main purpose is to do something that would be done on rare occasions. If you think users should browse all applications every time they want to launch an application then you are quite mistaken.\n\n> Even people use 5 applications regularly, there _is_ a need for a menu for browsing all the applications\n\nThis would only be a problem if the \"all applications\" was no longer accessible. Whereas in actual fact it is very accessible, and its location is quite obvious to everyone. \n\nKickoff is reasonably configurable from the CLI, though making it more configurable is a bit of a trivial task so could hardly be a reason for rejecting it. \n\nPlease let me know which people kickoff is useless for. Proposing a non-existent usecase, like someone who browses all applications every time they want to launch an app, really just doesn't cut it."
    author: "apokryphos"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "well, whats the point of the menu then? the all-apps-menu in kickoff is way slower to use. not only when browsing through every app - everything thats not on the first level or hidden because of the scrollbars is slower to reach then in the standard k-menu.\n\nthere is nothing better about the all-apps-menu of kickoff. the things that may be better are the extension to the menu. though these could be used with a standard menu as well...\n\n\ni don't think usebility experts would argue about that... the kickoff menu is the way it is because it should look cool, and different. a kmenu with search and favorites would be way more useable."
    author: "ac"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "K menu has \"favorites\": you can set the number of mostly or recently used programs  which appear at the top of the menu. It also has a search box in which you can search programs. You have Kerry for desktop search - in the current concept K menu is for launching applications, while desktop search is a different thing, so it is not integrated in the K menu."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "> It also has a search box in which you can search programs.\n\nThe original KDE menu doesn't have this."
    author: "Anonymous"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "I use KDE 3.5.5 on openSUSE 10.2. openSUSE people may have patched it."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "\"If you think users should browse all applications every time they want to launch an application then you are quite mistaken.\"\n\nNo, I don't think so. But there are other possibilities to start frequentely used applications: panel icons, quick browser panel applet, recently used items at the top of the K menu. We need those also, of course."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "After all, why is the Kickoff style of navigation (which I think is definitely slower than the classic menu navigation) better?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-18
    body: "I don't like it either, but they've done a bunch of usability studies on several subjects, and they clearly showed that ALL actions require less mouseclicks and less time with Kickoff compared to the current KDE menu."
    author: "jospoortvliet"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-18
    body: "how is that possible if you can't use the main menu without clicking? the current k-menu only needs clicking to start applications, folders are opened with mouseover alone. and thats still the ideal case for kickoff. if your menu is \"too large\", it gets even worse because of scrollbars....\n\nkickoff can't be faster for everything. its only faster if you use the new extras. but those extras could be used with a standard menu, too..."
    author: "ac"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-18
    body: "Standard K menu needs exactly 2 clicks for everything. Kickoff needs at least 2 clicks for everything and more than 2 for starting an application from the all applications tab. So all actions require the same or greater number of clicks in Kickoff than in the standard K menu."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-18
    body: "Correction: I understand. You don't need to click to open the Kickoff menu. But I don't think it makes it significantly faster."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-21
    body: "\"The general user won't use more than 5 applications 95% of the time\"\n\nOnce you realise you don't need a start menu for your 5 applications (a quickstart menu in kicker/plasma panel'll do) favourites becomes quite obsolete. The menu is used for the stuff you don't often need access to, so adding an extra layer doesn't make it more productive. There are other solutions to adress the 5 applications 95% of the time 'rule'. Kickoff (IMHO) is a complete and utter mess and solves the problems you bring up in the wrong way."
    author: "Terracotta"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-16
    body: "> From what I've seen of Raptor it seems to be a mild attempt\n> at trying to replicate most of the Kickoff Stuff.\n> Why, exactly, is Kickoff not just used?\n\nYou seam to forget one of the motivations for Plasma. People had a hard time writing new panels / plugins. Each task panel had to re-implement the backend code for accessing task data. For RSS feeds, people had to re-implement rss parsers. The list goes on.\n\nPlasma separates this. It provides \"data sources\" for all kinds of data. There will be \"data sources\" for things like taskbar tabs, menu data, the time, rss fees, etc. etc... You can write a new GUI on top of any of those, and another. Each one re-uses the same \"data engine\" plasma provides, both reducing CPU time and code size.\n\nSo instead of copying Kickoff, I assume the Plasma developers make a \"start menu data source\", and a default GUI for it. It will be painfully easy to write a different start menu instead. That's what the Plasma design is about too!\n\nAnd there is one more thing...\nCopying is boring, and you can do better then that. e.g. take the new dock appearance of Mac OS 10.5. Perhaps they spent 4 months on it, and were happy with the results. At some point you'll always be glad what you've got out, and you'll be blinded by any imperfections. In this case we have two options:\n- copy that UI litterally\n- spent one month extra of thinking \"how can I improve _upon_ this\"?\nAfter all you have a fresh mind and not tired of 4 months research.\nAnd it makes all the difference."
    author: "Diederik van der Boor"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "> Copying is boring\n\nUsing KDE's own work is 'copying'? Using something different which might be worse just doesn't make much sense. Kickoff has been proven to work, and there's no good reason (even in your explanation of Plasma) for why the menu could not be ported (yes, using all the later plasma goodness). Of course we're essentially talking about the UI. \n\n> - spent one month extra of thinking \"how can I improve _upon_ this\"?\n\nWhat exactly is there to say that Kickoff itself could not be improved? \n\nWhat I'm mildly worried about is getting a far worse menu (Raptor, from what I've seen of the mockups) when a much better one is available, for almost no reason at all. I have no idea why you would want to ignore all the research that has gone into Kickoff already. We want usability to improve in KDE4? If so, why exactly are we ignoring the studies that have taken place? It doesn't even look like the people doing the raptor mockups have even looked into the openusability stuff. Not a very comforting approach."
    author: "apokryphos"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "Kickoff is not available, as -- to my knowledge -- it's not ported to KDE4 (yet?). Raptor is KDE4 code, and with the feature freeze approaching, the solution that is available will be used. That doesn't mean that this menu won't change in the future (replace, improve), it's only an observation you seem not to have made yet: How things work in KDE development."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-17
    body: "I think you're missing the point here. I'm trying to say people should think first. Not just assuming something is good because MS, Apple or openSUSE designers invented it. KDE4 wants to make a good first impression, so they should consider more options. Look what exists, learn from it, and improve upon that.\n\n> > Copying is boring\n> Using KDE's own work is 'copying'?\n\nWhat other term would you propose then? :-p If I use idea's from other projects, and incorporate them in my own it can be not copying, depending on the similarities between the two. But just taking a project, improve only 1-5% means you're copying that 95%.\n\n> Kickoff has been proven to work, and there's no good reason (even in your\n> explanation of Plasma) for why the menu could not be ported (yes, using all\n> the later plasma goodness). Of course we're essentially talking about the UI.\n\nThereby you're ignoring the fact how code is written. \"Only talking about the UI\" and \"except for the explanation of plasma\" are easily said. Just like \"put this body around a different shaped car please\".\n\nThose things are not that easy! It means code architectures don't match at all, and any potential entangled part needs to be cut loose first. The whole point is Plasma is to make it easy to \"only copy the UI\". Almost every project out there doesn't allow this.\n\n> > - spent one month extra of thinking \"how can I improve _upon_ this\"?\n> What exactly is there to say that Kickoff itself could not be improved?\n>\n> I have no idea why you would want to ignore all the research that\n> has gone into Kickoff already.\n\nWhat exactly is there to say that Kickoff is being ignored?\n\n> It doesn't even look like the people doing the raptor mockups have even\n> looked into the openusability stuff. Not a very comforting approach.\n\nCtrl + F for kickoff in this page please: http://techbase.kde.org/Projects/Plasma/Menu"
    author: "Diederik van der Boor"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-18
    body: "I have only a passing knowledge on the research conducted for Kickoff, but considering my personal experience with the K-menu, I could easily imagine an important flaw in the user-testing.\n\nFirst of all my suspicion on the research is this; Novell funded most of the research, so the K-menu used for testing was the implementation used in recent Suse distributions.\n\nBased on how big the perceived difference for me between the K-menu in Suse 9.3 and 10.0 and the K-menu in Kubuntu 6.06-7.04, the exact same technology can with only a minor change go from causing a nightmarish to a blissful experience.\n\nMy issue with the implementation Suse followed (as I remember it) was:\n1) Putting the burden of choice between applications on the user. Putting too many options in the menu.\n\n2) Naming and placing applications in unintuitive ways (I believe Amarok was placed in multimedia, which was OK, but it was named \"Audio Player (Amarok)\", which _sounds_ like good nomenclature until you have many applications called \"audio player\", or wish to find the application quickly without using search)\n\n3) Putting slightly too many features in the same place ('all applications', 'favorites' and 'search' made it a little too cluttered for me)   \n\n\nNow, it seems to me that Kickoff fundamentally has the same goals as the Suse menu. It tries to accomplish the feat of supplying a wealth of information, but this is something I personally do not want it to do. In my eyes it is a clever way of solving a dumb problem, and the research conducted on kickoff might (if I'm correct in my starting assumption) have been influenced by not asking the right questions. \n\nPersonally I primarily use (not counting pervasive applications like Kget, Kmix, Klipper etc.) the following applications: Amarok, Akregator, Kopete, Ktorrent, Firefox and Konqueror. Out of these only Firefox requires me to use the K-menu since the other applications start on default (or in the case of konqueror, via an applet). \n\nKickoff would allow me to open this program (Firefox) easily, but so would the Kubuntu version K-menu (Internet -> Firefox becomes very fast after a while :) or any other non-cluttered, intuitive menu. \n\nOther, fairly important applications such as Kaffeine, Dosbox (personally important), K3B or Ark do not really need the menu either, since they are mostly opened 'automatically' in the instances where they are needed.\n\n\nSo far, so good. What I have pointed out so far doesn't really put Kickoff in a bad position even for a user like me. But as others have said, Kickoff isn't the most pleasant menu when looking for an application that _isn't_ used regularly, and since many people (not just me) almost only use the menu for such things, Kickoff does not seem like the right choice for KDE4. Granted, all the efforts put into it should not go to waste, but I would not like to see it become the default or even to see the alternative default (Raptor) being overly inspired by what Kickoff seems to try achieving.\n\n\nIn the end I trust the KDE developers will find a solution that is far more intelligent than anything I could come up with, even if it ends up resembling Kickoff. But having said that, you shouldn't think Kickoff has unanimous support amongst regular users just because of the results of scientific research. Science depends on the question asked, and I'm not sure Kickoff answers the same questions that Raptor will address.\n\n\nHope someone reads this tripe :P (and sorry if the English is a bit rusty, I haven't written in English for awhile.)\n-NicolaiM"
    author: "NicolaiM"
  - subject: "Re: Erm, Kickoff? (Re: KBFX?)"
    date: 2007-07-18
    body: "About the original SUSE K menu:\n1. But almost all of these applications have to be in the menu because users should be able to find and run them somehow. Kaffeine and Ark is often started through an associated file, but for example I start K3b many times from the menu.\n2. You can set this in the panel settings.\n\nAbout starting firefox: in openSUSE there is a place for recently or frequentely used applications at the top of the original K menu."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "DISCLAIMER: I'm in no way connected to/know the author of the following software, other than I requested it be included in Kubuntu.\nWhy KBFX? TastyMenu is lightyears ahead, usability- and bug-lessness-wise, imho. (I never could get KBFX to work/look \"right\" (tried on both Kubuntu and gentoo), might be me, though).\nTastyMenu takes the best ideas from both KBFX and SuSE/Novell's Kickoff and integrates them into the best menu I've seen on any OS.\nhttp://www.notmart.org/index.php/Software/Tasty_Menu_0.8.2"
    author: "Martin"
  - subject: "Re: KBFX?"
    date: 2007-07-16
    body: "I fully agree."
    author: "Tiberiu Ichim"
  - subject: "Re: KBFX?"
    date: 2007-07-18
    body: "That's weird, tasty menu has less bugs just because it's not skinnable,\nreally simple and lacking the new features. It's also too big for most\nusers. It would be better to keep the actual kmenu then..."
    author: "mane"
  - subject: "Re: KBFX?"
    date: 2007-07-18
    body: "I don't really like tastymenu, but I do think your arguments are bogus:\n\nA menu shouldn't be skinnable, that's stupid. The whole point of themes in KDE is not to need skins. Even Kopete does just use the default KDE look, and so should any menu replacement. Kickoff is weird enough already...\n\nAnd, what's the problem with a big menu? If you're browsing through the menu,  you don't do anything else - a menu could be fullscreen. A larger menu with bigger targets is better than a small one. I don't like the raptor whatever mockups, you're always scrolling, text is small, it has weird themes (just slowing down the system and making it look funny) so it's not usable at all.\n\nThe current K-menu (in the 'kubuntu incarnation' is pretty good, indeed. But I support the research going on in new area's. Personally, I'm a big fan of Gimmie, it looks like a much more usable menu replacement:\nhttp://www.beatniksoftware.com/gimmie/Main_Page"
    author: "jospoortvliet"
  - subject: "Re: KBFX?"
    date: 2007-07-19
    body: "1) It's really sad that people puts all their efforts givin some \neyecandies to DE, like the BLACK Kore theme, and then someone say:\n\"themeable/skinnable things aren't important at all\". I already noticed\nthat most of GPL software is done from programmers to programmers\n(just look at those ugly krita tools-icons, it's obvious that there isn't\nany real artist using krita....), but we are in year 2007 BC, please try\nto keep this in mind.\n\n2) That menu's so big that can't be correctly displayed on minimal displays,\nand so big that you have to move the mouse through the whole screen to use it even at 1024x768, that's uncomfortable!!!!"
    author: "mane"
  - subject: "Re: KBFX?"
    date: 2007-07-19
    body: "About skins: I don't know the official concept but I think KDE style itself should be nice and skinnable while applications and other elements like menus should smoothly integrate to this look instead of having their own look and skins."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KBFX?"
    date: 2007-07-19
    body: "\"we are in year 2007 BC\"\n\nAre you sure?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KBFX?"
    date: 2007-07-20
    body: "They figured if they could take the KDE source code back 4,000 years in the past, that would give people enough time to look at, fix it up, and get it out the door on time for the KDE 4 release."
    author: "T. J. Brumfield"
  - subject: "Re: KBFX?"
    date: 2007-07-20
    body: "Or they figured that there are gnome users here trying to give wrong\nsuggestions just hoping that they can do something to prevent users migration\nto kde 4 (for example because all the ugly totem bugs....).\nTo all them , give a look here, at the last comment, there are also kde users\nin gnome spaces (and I laugh for this):\nhttp://www.tux-planet.fr/blog/?2007/07/02/161-gnome-mockup-3"
    author: "mane"
  - subject: "Oxygen re-write"
    date: 2007-07-16
    body: "\"The start of a rewrite of the Oxygen widget style\"\n\nThe Oxygen widget set looked quite yummy, last time I saw it, and lots of effort had been put into it - what's the rationale behind the re-write, may I ask? Is the windec also being re-written? Also, given that the corners of the Oxygen windec are curved, will clicking the very top-right pixel of the screen when a window is maximised close that window?"
    author: "Anon"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-16
    body: "\"what's the rationale behind the re-write, may I ask?\"\n\nThe code is a pain in the ass. Ugly, unmaintainable, not worthy for inclusion into kdelibs. It's so broken that it's better to start over from a clean base, which is the aim of this rewrite. Keep the nice looks while drastically improving the code."
    author: "Jakob Petsovits"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-16
    body: "That's the best possible reason for the re-write - glad to hear it, and good luck! :)\n\nAny idea on an ETA for getting the re-write up to the current (functionality + looks) level?"
    author: "Anon"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-16
    body: "ok.. wish you good look.. if possible keep the current style (or improve it :p).. it looks just great ^^"
    author: "LordBernhard"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-16
    body: "ups.. look = luck ^^ sry. i'm a bit confused atm ^^"
    author: "LordBernhard"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-16
    body: "Nice freudian slip ;-)"
    author: "Michael"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-16
    body: "Good to hear!\nI also have a good attitude at writing \"write-only\" code, at least when i don't care about good design of the program, but i know wery well how other people react when i do (especially Paolo :) )"
    author: "Maurizio Monge"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-17
    body: "That doesn't surprise me. QStyle is a bad platform to extend for original styles -- its granularity is meant for wrappers; all styles implementations I have seen are pretty ugly and I can see how the code can get unmanageable if you are just a bit sloppy. I have started doing a layer on top of QStyle that should end in cleaner, smaller styles. If you wanna help out: http://gtk4qt.sourceforge.net/qsimplestyle/"
    author: "blacksheep"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-16
    body: "hm.. don't unterstand that too.. i'm following the development of kde4 through the opensuse buildservice and i have to say: it just looks great... only the window decoration doesn't work properly here... it changes but it's completely red (instead of grey).. don't unterstand why..\nthe plasma widgets are also great.. they perform well for there early state\n\nso.. move on with the great work ^^"
    author: "LordBernhard"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-16
    body: "I get the red borders too. Also, the oxygen style is really low contrast - makes it very difficult to use. Although I wonder if the colours on my computer are right - everything is light grey except tab bars which are black. And the text on selected tabs is also black. But I can't seem to change this."
    author: "Anonnimus Cowwud"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-17
    body: "The red windecs are caused by debugging code, they're shown when some pixmaps couldn't be loaded (for whatever reason) to make it extremely clear that something's wrong. So it's there on purpose."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-18
    body: "how can we locate which pixmaps are missing?"
    author: "LordBernhard"
  - subject: "Re: Oxygen re-write"
    date: 2007-07-17
    body: "> Also, given that the corners of the Oxygen windec are curved, will clicking the very top-right pixel of the screen when a window is maximised close that window?\n\nIn KDE 3, there is an option that controls whether maximized windows can be resized and moved[1]. If that option is disabled, well-written window decorations will enter a mode in which they do not paint borders if a window is maximized, and if they normally have rounded corners, they will morph to either do away with them in that mode, or they will at least expand their mask to accept clicks in the very corner. Of course, as the default window decoration, any Oxygen decoration will eventually be faithful to that option.\n\n\n1 = KControl -> Desktop -> Window Behavior -> Moving -> Allow moving and resizing of maximized windows"
    author: "Eike Hein"
  - subject: "New Protocols in Kopete?"
    date: 2007-07-16
    body: "I thought Kopete still had to go through a whole switch to phonon and other new backends (for presence etc.), along with porting all of the old protocols.  Has this already happened, or is it possible to add protocols independently of that work?  Or has the other stuff been set aside for the moment?\n\nI only ever had three problems with kopete: a) stability, and b) speed of the chat history and some other gui stuff, and c) no audio/video chat support.  I still use it over anything else.  If that stuff's fixed, I'll be more than happy with \"Kopete 4\"."
    author: "Lee"
  - subject: "Re: New Protocols in Kopete?"
    date: 2007-07-16
    body: "I guess you meant decibel instead of phonon, the latter is a multimedia backend. I'd really like to see improved file transfer speed over msn protocol."
    author: "Fede"
  - subject: "Re: New Protocols in Kopete?"
    date: 2007-07-16
    body: "I imagine it uses phonon powered dings and beeps "
    author: "Ben"
  - subject: "Re: New Protocols in Kopete?"
    date: 2007-07-16
    body: "Kopete should Use the Decibel Project which uses Telephaty FD.O Standard.thats Planned for 4.1 I think."
    author: "Emil Sedgh"
  - subject: "Re: New Protocols in Kopete?"
    date: 2007-07-17
    body: "But Kopete has more protocols and better implementations that telepathy has currently. So a switch would be painfull."
    author: "Allan Sandfeld"
  - subject: "Re: New Protocols in Kopete?"
    date: 2007-07-17
    body: "Telepathy is just a specification.\n\nKopete can both offer functionality as Telepathy components as well as use Telepathy components to extend its functionality.\n\n\"Using Telepathy\" does not reduce Kopete's functionality"
    author: "Kevin Krammer"
  - subject: "FINALLY !!"
    date: 2007-07-16
    body: "\"David Vignoni committed changes in /trunk/KDE/kdelibs/pics/oxygen:\nSwitching arrows to use Pinheiro's Scribus arrows.\"\n\nThank you :)\nThey look very good."
    author: "shamaz"
  - subject: "Re: FINALLY !!"
    date: 2007-07-16
    body: "They do look good but the previous/after arrows look just like the up/down rotated (without ajusting the reflection/shadow)."
    author: "Coward"
  - subject: "Re: FINALLY !!"
    date: 2007-07-16
    body: "And they haven't got a shadow like all the other icons."
    author: "EMP3ROR"
  - subject: "Re: FINALLY !!"
    date: 2007-07-17
    body: "hopefully the shadows all go away. they were an interesting experiment, but didn't really work in practice."
    author: "Aaron Seigo"
  - subject: "Re: FINALLY !!"
    date: 2007-07-18
    body: "Great! so it was noticed those shadow's didn't work..."
    author: "jospoortvliet"
  - subject: "Qxygen style"
    date: 2007-07-16
    body: "How about a pixmap engine where everybody can easily change the appearance without coding skills. Hasn't Mac OS X such thing?\n"
    author: "anonymous"
  - subject: "Re: Qxygen style"
    date: 2007-07-16
    body: "Or even a SVG one (using the same schema of icons that generate pngs for speed) that would allow resizing of the toolbars/widgets without much visual distorsion? :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Qxygen style"
    date: 2007-07-16
    body: "http://websvn.kde.org/trunk/playground/artwork/cokoon/"
    author: "logixoul"
  - subject: "Re: Qxygen style"
    date: 2007-07-16
    body: "What can I say?\nSeems like a awesome work!"
    author: "Iuri Fiedoruk"
  - subject: "Re: Qxygen style"
    date: 2007-07-17
    body: "The problem with SVG is that it does affline transfomations in the entire canvas, so you can't set some shapes to a fixed size -- which you want for borders; you don't want the border of a button to stretch, just the contents. The upcoming Gimp vectorial-based graphics language would be a better candidate."
    author: "blacksheep"
  - subject: "Re: Qxygen style"
    date: 2007-07-16
    body: "KDE3 already allows limited pixmap-based styles (an example being the bundled \"RISCOS\"). The now unmaintained Detente (http://www.digilanti.org/werks/detente/) exists to facilitate this."
    author: "logixoul"
  - subject: "Love Krita, question about Kopete."
    date: 2007-07-16
    body: "Alright, as a typical person who learnt digital image editting on Photoshop (version 5 or something), I got used to how things were done in it.  I've used GIMP several times as well, and although I can usually do the same thing in it, I like Krita a lot more.  Starting with KOffice 2.0, I believe that Krita can become a Photoshop competitor someday, and I can finally start recommending something other than the GIMP for people whom want a free photo and image editor that supports lots of professional photography techniques (and is available on Windows and Mac OS X).\n\nAbout Kopete: it's nice to see more protocols added to it, but I was wondering how far it was in making a rock-solid Jabber implementation.  It'd be nice if Kopete could try to promote using Jabber or IRC a bit more over using proprietary protocols, and having support for most of the XMPP standards and proposals could go a long way towards helping that.  I can also recommend this over Pidgin to people as well (although Miranda IM for Windows is pretty sweet, it's a total power user sort of application).\n\nThanks, KDE guys!  You rock!"
    author: "Matt"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-16
    body: "As I said on the last topic on Kopete, Decibel is a new KDE Project for Real Time Communications Managent.since IRC and Jabber are Real time communications, Decibel handles them.\nand as I know, Kopete will use Decibel az 4.1. Decibel itself uses Telephaty FD.O standard, so you should ask for better IRC Implementations from Telephaty people.\nAnd...Im not sure...I just said what I tought."
    author: "Emil Sedgh"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-16
    body: "Please note that Krita aims to be a PAINTING APP, not purely a photoeditor. There is a lot of overlap, sure, but I think it should be noted ;-)"
    author: "jospoortvliet"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-16
    body: "well, in the long run it doesn't make much difference. all photo editing stuff is usefull in a painting app too. thats why so many \"paintings\" are still done in photoshop, even though apps like painter or artrage are around for many years now. its brush engine is weak - but the more complex your painting gets, the more you need all the other stuff."
    author: "ac"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-17
    body: "> ... all photo editing stuff is usefull in a painting app too\n\nNot really.  There is a lot of stuff that is specific to photos that is no help at all to creating original work.  And The GIMP seems to lack some of these features as well.  There is also the question of drawing (I draw but do not paint).  xFig is very good at drawing but is rather dated, yet there is no other app I know of with those features.  It would be nice to see these features in a new app like The GIMP or Karbon14 (such an app would also need to do SVG to replace the app specific xFig file format).\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-17
    body: "what should be specific to photos? things like the levels etc. are usefull for painting too. photo editing is about perfecting the final touch of an image. there is no reason not to use the same tools on your own paintings."
    author: "ac"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-18
    body: "> what should be specific to photos?\n\nEverything which is designed for the specific purpose of modifying an existing bitmapped image."
    author: "James Richard Tyrer"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-18
    body: "maybe you should read my complete post before responding...\n\ngo take a look at places like cgsociety.org and see for yourself how photoshop is used. all features that are meant to be used for enhancing photos greatly boost the flexebility of the artist. and thats why someone paints digitaly, because you can change things with ease."
    author: "ac"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-18
    body: "Things like red-eye removal or correction of lense distortion etc. are not needed by a paint app."
    author: "AC"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-19
    body: "> maybe you should read my complete post before responding...\n\nPerhaps you need to read my post more thoroughly. :-D\n\nStrange, you keep saying enhancing and I keep saying modifying.  Perhaps this is a language issue.\n\nIAC, I fully understand that there are paint features that can also be used to enhance photos.  I use The GIMP to fix photos.  However, the fact that there is a great deal of overlap does not mean that there aren't operations that are specifically designed for photos that are not needed to produce original work of any kind."
    author: "James Richard Tyrer"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-18
    body: "You might like InkScape, which replaced XFig for me after many years."
    author: "ac"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-18
    body: "I use InkScape, but it lacks some of the xFig features which I like.\n\nSo, I still wind up drawing things with xFig an importing them to other programs."
    author: "James Richard Tyrer"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-17
    body: "Are you really able to use Krita in a productive way? Then please\nwrite some tutorials, judging by the lack of scripts and tutorials I was\nthinking that nobody was using it (nor pro nor hobbist)..."
    author: "mat"
  - subject: "Re: Love Krita, question about Kopete."
    date: 2007-07-19
    body: "It might just be that Krita is really usable without tutorials ;)"
    author: "Thomas Zander"
  - subject: "Digikam ported to KDE4"
    date: 2007-07-16
    body: "Today Gilles finished base porting of Digikam to KDE4. Unstable as hell so don't try it with your prize winning photo collection but...  :)"
    author: "m."
  - subject: "Re: Digikam ported to KDE4"
    date: 2007-07-16
    body: "YEAH!\n\nLove digikam...\n\nIt imho only needs a huge GUI cleanup (though a lot of work has already been done)."
    author: "jospoortvliet"
  - subject: "Re: Digikam ported to KDE4"
    date: 2007-07-16
    body: "Great work, thanks. Digikam is a great application and it's nice to see it arrive in KDE4 :)"
    author: "Erunno"
  - subject: "Re: Digikam ported to KDE4"
    date: 2007-07-18
    body: "But what?"
    author: "grammar_nazi"
  - subject: "Re: Digikam ported to KDE4"
    date: 2007-07-18
    body: "A fresh little screenshot of digiKam under KDE4...\n\nhttp://www.digikam.org/?q=node/243\n\nGilles Caulier"
    author: "RobertC"
  - subject: "Re: Digikam ported to KDE4"
    date: 2007-07-19
    body: "Looks nice, but why vertical tabs?"
    author: "reihal"
  - subject: "KDE fonts??"
    date: 2007-07-16
    body: "Anyone knows whether KDE 4.0 will include Redhat's beautiful Liberation fonts to be used as default? They are much more a pleasure to look at than any default fonts used by KDE at the moment. Their license appears to be good for Linux and KDE too. Thanx."
    author: "cb"
  - subject: "Re: KDE fonts??"
    date: 2007-07-16
    body: "I'd imagine that's up to the distros, not the KDE devs."
    author: "Anon"
  - subject: "Re: KDE fonts??"
    date: 2007-07-16
    body: "Are you implying that KDE developers do not include any fonts in the generic KDE version that they release to the distros? "
    author: "cb"
  - subject: "Re: KDE fonts??"
    date: 2007-07-16
    body: "Yes.  What KDE currently provides to the distributions are tarballs of the source code and data and instructions on how to build them.\n\nThe distributions are responsible for providing the dependencies ( libraries, data ) needed to compile and run KDE."
    author: "Robert Knight"
  - subject: "Re: KDE fonts??"
    date: 2007-07-16
    body: "Something I forgot to mention in my first post is that it is certainly possible to recommend or suggest the inclusion of certain fonts with KDE applications, and I think there were discussions about doing that.  "
    author: "Robert Knight"
  - subject: "Re: KDE fonts??"
    date: 2007-07-17
    body: "yes, i'd love to see us have a \"KDE4 looks best if the following fonts are used: *list of fonts*.\" then we could provide default settings with some assumptions as to fonts and even note OSes that don't provide those fonts along with kde4 libs as officially deficient by definition =)"
    author: "Aaron Seigo"
  - subject: "Re: KDE fonts??"
    date: 2007-07-17
    body: "Such recommendations or guidelines...sounds like a job for Captain HIG or CIGMan.\n\n"
    author: "Wade"
  - subject: "Re: KDE fonts??"
    date: 2007-07-17
    body: "The only readable font on a computer screen is MS New Times Roman i Windows.\nWhen I import this font to KDE it renders unreadable. Why is this?\nAnd why isn't there a readable font at all in Linux? (I haven't tried those Liberation fonts, I don't like Red Hat)\n"
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-17
    body: "You don't have to use or even like RedHat to use the liberation fonts.  They are freely downloadable to anyone to be used with any distro."
    author: "Louis"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "I have to try that. Looks like they are total copies of the best MS fonts.\nIt only took 10 years."
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "The point is to be metric copies of the fonts (i.e. to render documents exactly the same when those fonts are used).\n\nI honestly prefer the dejavu fonts (and bitstream)."
    author: "Sutoka"
  - subject: "Re: KDE fonts??"
    date: 2007-07-17
    body: "> why isn't there a readable font at all in Linux?\n\nHmm, I wonder what I've actually been reading all these years..."
    author: "Paul Eggleton"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "With readable I mean with similar rendering and kerning as in print.\nI can't read large chunks of text with bad kerning.\nI'm not the only one to think that Windows does it best:\n\nhttp://www.joelonsoftware.com/items/2007/06/12.html?hi=joel"
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "Qt3 doesn't do kerning, so you have to wait for KDE4 for that to work."
    author: "Selene"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "It only took 10 years."
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "Not to feed a troll, but why do you hate RedHat?  They have done more for Linux than any other company and they have remained steadfast in the whole patent debacle with Microsoft.  They are a pure Linux, pure Open Source company with one of the best distributions out there.  Fedora, for being essentially a bleeding-edge RHEL beta is surprisingly stable and of high quality.  Red Hat hires a lot of people who contribute to critical parts of the Linux stack: \n\nSee http://fedoraproject.org/wiki/RedHatContributions for a list of things RedHat has done for Linux and Open Source.\n\nI can't believe people are sour about the Fedora/RHEL split, which was not at ALL a bad idea.  That's the only thing I could consider to be a negative of RedHat."
    author: "anonymous"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "Linux yes, but what did they do for KDE?"
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "True, they're not big KDE fans, but KDE needs linux as well..."
    author: "jospoortvliet"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "And besides, RedHat is mentioned in several KDE-application at the credit section."
    author: "whatever noticed"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "No, Kde doesn't need Linux. It runs on BSD, Solaris and Unixes.\nAnd soon on OSX and Windows, I've heard."
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "\"And soon on OSX and Windows, I've heard.\"\n\nkdelibs and many of its apps will run on Windows & OS X, but it is almost certain that the *desktop* itself (Plasma, panels, window manager) won't."
    author: "Anon"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "Please do note that RH is totally turning around its former KDE policies and their releases are getting to be really good.\n\nSo, please do give them a second chance."
    author: "Anon"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "\"Please do note that RH is totally turning around its former KDE policies and their releases are getting to be really good.\n\nSo, please do give them a second chance.\"\n\nOh? How so? And since when? I've seen nothing to suggest this."
    author: "Anon"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "http://fedoraproject.org/wiki/Releases/FeatureKDE4"
    author: "Anonymouse"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "Are you serious? They (or rather, Fedora - not Red Hat proper) are simply packaging a (likely very immature) KDE4.0 instead of KDE3.5.x, largely due to the fact that Qt3 is being EOL'd.  How does this represent an improvement at all, let alone one that would inspire us to \"give them a second chance\"?"
    author: "Anon"
  - subject: "Re: KDE fonts??"
    date: 2007-07-20
    body: "at LinuxTag (about a month ago) I played with fedora (the main red-hat distro for end users) we saw a kde that was packages just \"as is\", which is a huge leap for them as the previous ones were severely changed from kde proper.\n\nthe package quality has basically gone up tremendously in their release of the latest 3.5.x ones.\n\ni'm not sure how you got to the link from kde4. that part is irrelevant to the points made here."
    author: "Thomas Zander"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "Oh, I won't then."
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "Ok, I will then."
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-19
    body: "I said \"don't like\", I didn't say \"hate\"."
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "Fontconfig settings are very important in font readability.  I like to turn off anti-aliasing and turn up sub-pixel rendering/hinting, while turning off auto-hinting.  Play around with your ~/.fonts.conf and see what works for you.  Also try making sure the binary interpreter is enabled when you build freetype."
    author: "anonymous"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "Thanks for the tips."
    author: "reihal"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "Sub-pixel rendering, all kinds of hinting, both the binary interpreter and the auto-hinter, are all various techniques to do anti-aliasing. You can not have any of that without having anti-aliasing."
    author: "Allan Sandfeld"
  - subject: "Re: KDE fonts??"
    date: 2007-07-18
    body: "I beg to differ.  The binary interpreter vs. the auto-hinter IS relevant when anti-aliasing is not enabled.  I have seen first hand the difference with a number of fonts.  The glyphs still have to be properly shaped, regardless of whether you are also anti-aliasing them.\n\nWhat I didn't say is that I have anti-aliasing off only for fonts in the range of 8-12 points.  Fonts outside that range, or fonts that are bold or italic I do have set to be anti-aliased because they look better.  But interface or document fonts in that range look a lot better without anti-aliasing.  In my personal opinion, of course."
    author: "anonymous"
  - subject: "Re: KDE fonts??"
    date: 2007-07-16
    body: "I can't answer the question, but I just want to mention that DejaVu has slightly better hinting than Liberation, to my sight."
    author: "logixoul"
  - subject: "Re: KDE fonts??"
    date: 2007-07-17
    body: "An DejaVu is much more complete than liberation. Liberation has been created to fill the gap of fonts with the same metrics as microsoft default fonts which is good to display correctly documents created on the windows platform for instance. DejaVu, a spinoff of Vera doesn't have the metrics constraints and therefore are easier to develop and improve. Liberation is nice to have but i think is very far from reaching the quality and the completeness of DejaVu."
    author: "Med"
  - subject: "..."
    date: 2007-07-16
    body: "I hate the idea of K apps on Windows/OSX, but, anyway:\nAs far as I know Plasma won't be port to windows/OSX, so if amarok will may use  Plasma for the context view, isn't this making it unportable to windows/OSX? (I know the libraries of plasma are part of kde, but they can be use under windows/OSX?)\n\nIs it possible that Kopete could use windows lists skins (somehow like Adium? \n\nAre you aiming to the same look whit the new Oxygen Widget Style? (Somethings just look amazing, but it lacks contrast and it look a little plain)"
    author: "Luis"
  - subject: "Re: ..."
    date: 2007-07-17
    body: "I'm with you I don't really like the idea of KDE being ported to windows, they want KDE? they should go to linux, oh well at least I will be able to force my friends to download KOffice so that we can have colaborative editing :D."
    author: "Carlos Licea"
  - subject: "Re: ..."
    date: 2007-07-18
    body: "go look up \"opensource\" asap....\n\n\npeople can work on whatever they want."
    author: "ac"
  - subject: "Amarok not coming to Windows"
    date: 2007-07-17
    body: "I dual-boot for gaming, and I predominately use Windows at work.  I was really looking forward to being able to install Amarok on Windows personally.  I hope the decision to include Plasma in Amarok doesn't preclude Amarok from a Windows port.\n\nI would like to see an official answer on this."
    author: "T. J. Brumfield"
  - subject: "Re: Amarok not coming to Windows"
    date: 2007-07-17
    body: "here is your official answer: us amarok developers have no intention not allowing amarok to work on windows.\n\nthat said, most of us (if not all) work on linux/unix, and our focus is on the linux desktop. our focus is, and will be, on linux. but i see no reasons why an enterprising developer cannot port amarok to windows---with kdelibs working on windows, amarok will have the ability to be functional as well.\n\nthe plasma integration should not directly affect the porting effort at this time :)"
    author: "leo franchi"
  - subject: "Re: ..."
    date: 2007-07-17
    body: "I think the ability of writing cross platform KDE apps is good: many people want to write cross platform programs and they will use another toolkit unless KDE libs are ported to Windows and Mac OS."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: ..."
    date: 2007-07-17
    body: "I use Linux at home, but I have to use Windows on work because of this damn outlook/exchange thing. \nI would LOVE to use Kate, Quanta and Amarok on my work.\n\nI don't really understand why so much hate for people wanting apps ported to windows. This will probably bring much more developers to use qt, kde and ultimately Linux."
    author: "Iuri Fiedoruk"
  - subject: "Re: ..."
    date: 2007-07-17
    body: "> isn't this making it unportable to windows/OSX\n\nnot at all, actually. libplasma itself is actually pretty damn portable. bizarre, i know ;-)"
    author: "Aaron Seigo"
  - subject: "Re: ..."
    date: 2007-07-17
    body: "Ok, thanks for the answer.\n\nSee you. "
    author: "Luis"
  - subject: "KGeography maps in/on Marble "
    date: 2007-07-16
    body: "What happens when you drag a KGeography map into Marble?  Or stack the 50% transparent plasmoid of one atop the other?\n\n:-) 8-/ <3 !"
    author: "skierpage"
  - subject: "Re: KGeography maps in/on Marble "
    date: 2007-07-19
    body: "> What happens when you drag a KGeography map into Marble\n\nNothing happens so far. We think about unifying those apps for KDE 4.1 though."
    author: "Torsten Rahn"
  - subject: "Donation"
    date: 2007-07-17
    body: "I just donated like 15 GBP as a thanks for the KDE Commit-Digest.\nI don't know how much a GBP is, but I hope it's not equal to a\nthousand dollars.  But hopefully it'll help them keep coming!"
    author: "Henry S."
  - subject: "Re: Donation"
    date: 2007-07-17
    body: "Thanks for the donation Henry!\nA nice and much appreciated surprise for me to wake up to today ;)\n\nThanks again,\nDanny"
    author: "Danny Allen"
  - subject: "Plasma in Applications"
    date: 2007-07-17
    body: "I thought plasma was basically Kdesktop, Kicker and SuperKramba combined, how come its possible to include it in applications? "
    author: "Ben"
  - subject: "Re: Plasma in Applications"
    date: 2007-07-17
    body: "What I understand of it:\n- Plasma has a basic library (libplasma) for the infrastructure of data sources, engines, etc.. Other applications use that for their canvas.\n- The plasma desktop application uses X11. So that will not be portable. The library however is."
    author: "Diederik van der Boor"
  - subject: "Re: Plasma in Applications"
    date: 2007-07-17
    body: "Diederik has it right. essentially to bring together all those other apps into plasma we needed to write a whole bunch of support code. no surprise there, of course.\n\nwhile writing this support framework (libplasma) i tried to ensure that the design was kept fairly straightforward and generalized. now it turns out that that code is also pretty useful to a lot of other people who are trying to do for their apps what plasma is trying to do for the desktop (make it look prettier and work better). more people than i actually expected, to be honest.\n\nlooks like kdm might even end up using it."
    author: "Aaron Seigo"
  - subject: "Re: Plasma in Applications"
    date: 2007-07-18
    body: "kdm may be using it?? what does this exactely mean? will there the possibility to use widgets there if they base some parts on libplasma????"
    author: "LordBernhard"
  - subject: "Re: Plasma in Applications"
    date: 2007-07-18
    body: "that's the idea."
    author: "Aaron J. Seigo"
  - subject: "Re: Plasma in Applications"
    date: 2007-07-19
    body: "woo! clock plasmoids everywhere! :)"
    author: "Chani"
  - subject: "start of a rewrite of the Oxygen"
    date: 2007-07-17
    body: "I don't know how Oxygen widget set looks like, but I hope, that \"clean GUI\" ideas such as: http://www.kde-look.org/content/show.php/KDE+4.x+Dark+Liquid?content=62269 will be  implemended. No edges, no gui noise, no extra borders between widgets! That sort of mockups looks great! Examples: \n\nhttp://www.kde-look.org/content/show.php?content=34997\nhttp://www.kde-look.org/content/show.php?content=28476\nhttp://www.kde-look.org/content/show.php/Dolphin+mockup%21?content=56040\nhttp://www.kde-look.org/content/show.php/KDE4+Simple+Style?content=55317\n\nAlmost all of this mockups has highest rating meaning that many people think the same.\n\nGood luck!"
    author: "Max"
  - subject: "Re: start of a rewrite of the Oxygen"
    date: 2007-07-17
    body: "\nI've always loved Adrien Fac\u00e9lina/Erioll's mockups.  The \"FileFinder\" mockup\nis freaking amazing, as is the rest of his mockups.  I really love\nthe contrasting colors in the widgets.  I don't know if he is already\ndiscussing with the Oxygen team, but I wish he would.\n"
    author: "Henry S."
  - subject: "Re: start of a rewrite of the Oxygen"
    date: 2007-07-17
    body: "I second that... eriol's are the best mockups hands down.\n\nNote that they don't even need something like transparancy to look fresh, cool and modern... and at the same moment mature, calm and clean - that's what i call great great design. And it doesn't really resemble any other OS or Desktop. \n\nI really do hope that the oxygen guys read these comments and those on kde-look.org. It would be sad if the awesome work under the hood of KDE4 wouldn't be reflected in some extraordinary design of the gui - an simply transparent, rounded-edged stuff isn't extraordinary anymore for sure. \n\nSo please, people, look at eriol's designs!"
    author: "Philipp"
  - subject: "Re: start of a rewrite of the Oxygen"
    date: 2007-07-18
    body: "Maybe it's better to post this discussion to kde-artists mailing list (https://mail.kde.org/mailman/listinfo/kde-artists) or in the other list where Oxygen developing are discussing. "
    author: "Max"
  - subject: "Re: start of a rewrite of the Oxygen"
    date: 2007-07-17
    body: "What is the problem with borders? They help navigation."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: start of a rewrite of the Oxygen"
    date: 2007-07-17
    body: "Except when there's too many of them - borders are fine... when there's a good, solid reason for them being there :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: start of a rewrite of the Oxygen"
    date: 2007-07-19
    body: "Too bad that the KDE people remove all borders and dividers all together..."
    author: "Someone"
  - subject: "Re: start of a rewrite of the Oxygen"
    date: 2007-07-17
    body: "No more than a 1 pixel border is needed.\n"
    author: "reihal"
  - subject: "Re: start of a rewrite of the Oxygen"
    date: 2007-07-17
    body: "i really hope that some of this mockups get included.. especially the filefinder (property button in the sidebar --> very good) and the thing with the contacts look really cool and useable (maybe not from the left border but a widget providing this functionality would be really cool... wish i could do it for myself ... don't know how.. are there any tutorials on how to write plasmoids? hope i don't need skills with kde4 development (have just begun learning) but i've got pretty good c++, qt, c# skills.. hope they'll do it ^^ "
    author: "LordBernhard"
  - subject: "Two Things"
    date: 2007-07-18
    body: "two things i really would like to see in kde4:\n1) more consistent behaviour inbetween applications, especially concerning hotkeys. e.g. if shift-right switches to the next tab in konsole and yakuake, then why doesnt it do that in konqueror and kopete? also ctrl-m hides the menubar in konqueror and kmplayer, but doing that isnt even possible in kmail and juk...\ncould these things not be controlled by underlying kde-infrastructure so that they work the same with all kde-apps?\n\n2) better and centralized privacy options, e.g. the possibility to disable the use of  \"histories\" and \"recent files\" throughout all kde-apps. also it would be nice if each app would provide \"clear recent files list\"-action, this could be the last element of the \"recent files menu\"\n\nwhat do you think? it shouldnt be too hard from a technical pov, or am i mistaken?\n\nthanks!"
    author: "Hannes Hauswedell"
  - subject: "Re: Two Things"
    date: 2007-07-18
    body: "I can't argue with either of those ideas."
    author: "Ben"
  - subject: "Re: Two Things"
    date: 2007-07-18
    body: "I agree.\n\n3)Consistent MDI/tabbing and sidebar system in applications. Konqueror, Konsole, Kate, Kile, Quanta, KDevelop, KOffice all handle more than one documents/tabs at the same time and all handle them differently. Kate, Kile, KDevelop, Konqueror, KOffice all have sidebars/tool windows but all handle them slightly differently."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Two Things"
    date: 2007-07-18
    body: "> e.g. if shift-right switches to the next tab in konsole and yakuake, \n> then why doesnt it do that in konqueror and kopete?\n\nPlease bear in mind that Konsole and Yakuake (which is built on top of the embedded Konsole) do have certain restrictions placed on them about which key combinations they can and cannot use as shortcuts in order to remain compatible with various terminal programs.\n\nFor example, there are no Ctrl+[Character] or F[Number] shortcuts in Konsole by default because terminal programs (such as Vim) use them.\n\nI think the 'correct' tab switching shortcut is Ctrl+PageUp , Ctrl+PageDown as defined in kdelibs.  I will probably relax the 'no Ctrl+[Character] shortcuts by default' restriction in Konsole for that particular shortcut because I couldn't find any important terminal applications using it by default.   \n\n> could these things not be controlled by underlying kde-infrastructure \n> so that they work the same with all kde-apps?\n\nMany of the them are.  What KDE currently provides for many actions is the menu/toolbar item consisting of translated text, icon, shortcut and position in the menus, but not the actual code which gets executed when you click on said item because that depends on the application.\n\nThe show, hide menu bar item is one of those.  KDE defines the how the menu item behaves, what it looks like and where it appears.  The few lines needed to add it to an application and do the right thing are currently done at the application level.  "
    author: "Robert Knight"
  - subject: "Menu, also important for marketing"
    date: 2007-07-19
    body: "In regards to all the menu discussions here:\n\nPlease remember that the menu is a very important first impression item when a new user tries a system.\n\nIt is of course very important that the menu is technically good, but it is also very important that if \"feels\" good for newcommers. \n\nKDE4 is in many ways a fresh start for KDE and will attract users from all sorts of environments. It is important that they feel comfortable and get a good first impression.\n\nA shure way do push users away is to have a menu that is \"experts only\" or looks like it is made for engineers only.\n\nMy personal expereince is that menus like Kickoff (have not tested the other newcommers) are more attractive to newcommers/newbees than the standard Kmenu. There are several aspects of the Kickoff menu that I do not like and I often end up using the search to get to the app I need. But for new users that are not in to KDE, it seems to be a good match.\n\nBirger"
    author: "Birger"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-19
    body: "Well, to add my 5 cents, I have been using the 'traditional' KDE menu for nearly 5 years, and I like the Kicker menu (as implemented in OpenSuse 10.2) much better; for me it is cleaner (less items in each list and/or more relevant items), I can organize things better (Favorites) and find things quicker. The only thing I do not like is the slight delay when you hover from a tab (big tabs at the bottom) to another tab, should be instantaneous IMO."
    author: "Darkelve"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-19
    body: "It is actually Kick-Off that you are using."
    author: "Sebastian"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-20
    body: "Yes, that's what I meant.\n\nI get confused with all the names (KickOff, Kicker, KBFX, ...)"
    author: "Darkelve"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-22
    body: "1. The delay is configurable, IIRC.\n2. Its purpose is to let people quickly throw their mouse to different parts of the screen with no fear of accidentally switching tabs."
    author: "logixoul"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-19
    body: "Kickoff is not surely better for newbies. Standard K menu works just like a menu in a KDE application, which is mostly like a menu in other GUIs. In contrast, Kickoff is very different from other menus so the user have to get used to a new, unusual thing. Furthermore, a more interested user who is new to Linux and KDE wants to explore the new system and find out what kind of applications are installed. This is exactly what is uncomfortable with Kickoff."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-19
    body: "Yet Vista's new menu is closer to Kickoff than Window's old Start Menu."
    author: "T. J. Brumfield"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-20
    body: "Why is this important now? "
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-20
    body: "Because you said new users can't get used to a new menu.  Oddly enough that didn't stop Microsoft from greatly changing their menu with Vista.\n\nI don't think that should ever be a reason to hold back a good program, to suggest that users can't possibly learn something new.\n\nThat sounds a little too close to Gnome philosophy for me."
    author: "T. J. Brumfield"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-20
    body: "I don't think we should stay at old programs to make it easyer to learn. I'm just not sure that Kickoff is better for beginners than the original K menu as Birger wrote."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Menu, also important for marketing"
    date: 2007-07-22
    body: "> I often end up using the search to get to the app I need\nYou're saying it like it's a bad thing :^)"
    author: "logixoul"
  - subject: "KDE got boring"
    date: 2007-07-19
    body: "I've actually just left linux and gone back to windows, solely because right now I don't see KDE as being that interesting.  Kopete is lacking, Kmail is substandard.  Really the only decent and exciting apps with kde are amarok and k3b.  Also having a dial up connection mostly updating KDE weekly is a nightmare.  I don't see how many windows people are going to use kde4 with the size of the updates.  Will be keeping an eye out for KDE4, and prolly come back to linux to try that, and hope that kmail and kopete teams start improving their products."
    author: "Richard"
  - subject: "Re: KDE got boring"
    date: 2007-07-19
    body: "That's a strange motivation for switching back to windows because windos have improved even less in the recent years. (And keep in mind that the KDE team has been focusing on KDE 4 of late.) Or do you mean the amazing vista eyecandy?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KDE got boring"
    date: 2007-07-19
    body: "If it is merely Vista eye-candy, you can make KDE look pretty close to Vista as it is, and there is a near-perfect Vista Transformation Pack for XP.\n\nHonestly, I can't think of a single good reason to actually run Vista, and I can think of plenty of reasons not to."
    author: "T. J. Brumfield"
  - subject: "Re: KDE got boring"
    date: 2007-07-20
    body: "It was irony."
    author: "Gr\u00f3sz D\u00e1niel"
---
In <a href="http://commit-digest.org/issues/2007-07-15/">this week's KDE Commit-Digest</a>: Much work in <a href="http://amarok.kde.org/">Amarok</a>, with the implementation of a CoverFlow-esque OpenGL album art visualisation, codenamed "CoverBling", and Service Framework and Plasmification efforts. Sample OpenGL-based applets added to <a href="http://plasma.kde.org/">Plasma,</a>, with Plasmoids to watch for changes to files, for browsing files, and to monitor network interfaces. General progress in the <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=DCE4DBD4A0509DC7">2d projection</a> and <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=D67FADC741D4F4C4">KML</a> in <a href="http://edu.kde.org/marble/">Marble</a>, <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=C3E84FB73E7DE5F8">OpenPrinting</a>, and <a href="http://code.google.com/soc/kde/appinfo.html?csaid=5D66C1579098460C">KOrganizer Theming</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> projects. KWallet support in KRDC. KMines essentially rewritten with a QGraphicsView base, with support for multiple background SVG themes in KGoldRunner. More manipulation and view work in Kreative3d. Implementation of Kubelka-Munk paint mixing research in <a href="http://www.koffice.org/krita/">Krita</a>. Internet integration in Kaider, with a WebQuery view and example script to use Google Translate. <a href="http://okular.org/">okular</a> becomes usable as a print preview component. KTrace, a "strace" interface for KDE 4 added to playground/sysadmin. Beginnings of support for ComunIP, a Brazilian IM protocol in <a href="http://kopete.kde.org/">Kopete</a>. More progress in the porting of Digikam and KTorrent to KDE 4. The start of a rewrite of the <a href="http://oxygen-icons.org/">Oxygen</a> widget style. KBFX, an alternate K menu, moves to kdereview.
<!--break-->
