---
title: "People Behind KDE: Summer of Code 2007 (3/4)"
date:    2007-09-02
authors:
  - "dallen"
slug:    people-behind-kde-summer-code-2007-34
comments:
  - subject: "ruby soc"
    date: 2007-09-02
    body: "Does anybody have news on how on \"the better support for Ruby and Rails in kdevelop soc\" is going? I've never seen any news posted about it on the dot or in the blogs."
    author: "Patcito"
  - subject: "Re: ruby soc"
    date: 2007-09-02
    body: "oops, just realized there on http://code.google.com/soc/2007/kde/about.html."
    author: "Patcito"
  - subject: "Re: ruby soc"
    date: 2007-09-02
    body: "http://techbase.kde.org/Projects/Summer_of_Code/2007/Projects does list it though."
    author: "Patcito"
  - subject: "Re: ruby soc"
    date: 2007-09-02
    body: "Bad news.\n\nI don't exactly know how much was achieved, but rumour has it that the student stopped working on it sometime in July. Don't expect all too great outcomes from this project in the short term."
    author: "Jakob Petsovits"
  - subject: "Re: ruby soc"
    date: 2007-09-02
    body: "Unfortunately there's close to 0 outcome for this SoC project,the student lost interest shortly after the official start of the project."
    author: "Andreas Pakulat"
  - subject: "Re: ruby soc"
    date: 2007-09-02
    body: "Yes, this is a real shame. Maybe I'm biased but I see turning KDE4 into a killer python and ruby development environment as really important. But we are devoting only something like 2-5% of KDE developer resources to do that, when in my opinion it should be much more. So the loss of 3 months dedicated developer effort to ruby support for KDevelop has a serious effect on broadening the KDE developer base with more user friendly languages than c++."
    author: "Richard Dale"
  - subject: "Re: ruby soc"
    date: 2007-09-02
    body: "I've recently been using qt4ruby a bit for small projects, and it's been really good. Kate also has nice support for ruby's syntax highlighting.\n\nI'm waiting until I start using KDE4 to check out how korundum is doing.\n\nOf course, I learned ruby primarily to jump back into programming. Instant gratification is nice, and it's suitable for small starter projects. I'm planning on switching to c++ when/if I get more serious, and as projects get bigger."
    author: "Soap"
  - subject: "Re: ruby soc"
    date: 2007-09-02
    body: "I just agree there. Beside the integration itself, better documentation at http://techbase.kde.org/Development/Tutorials#Rapid_Application_Development would be damn useful too. If just each python- and/or ruby-lover would write a few lines...\n"
    author: "Sebastian Sauer"
  - subject: "completely off-topic, i know..."
    date: 2007-09-02
    body: "but shouldn't the 4.0 delay have some kind of \"official\" announcement?\n(or maybe there was and i just missed it?)"
    author: "attendant"
  - subject: "Re: completely off-topic, i know..."
    date: 2007-09-02
    body: "That'll come, I'm sure."
    author: "jospoortvliet"
  - subject: "Re: completely off-topic, i know..."
    date: 2007-09-02
    body: "i guess the announcement is delayed as well :o)"
    author: "whatever noticed"
  - subject: "Re: completely off-topic, i know..."
    date: 2007-09-02
    body: "No, it's right here:\n\nhttp://www.planetkde.org/"
    author: "reihal"
  - subject: "Re: completely off-topic, i know..."
    date: 2007-09-02
    body: "That's not official IMHO, it is just a personal blog. If you go to kde.org, there is a section called \"Latest Announcements\" and one called \"Latest News\". I take those to be the official and semi-official news outlets respectively. The latter is just a feed from the Dot."
    author: "Martin"
---
<a href="http://behindkde.org/">People Behind KDE</a> releases the third interview in its series of four interviews with students who are working on KDE as part of the <a href="http://code.google.com/soc/kde/about.html">Google Summer of Code 2007</a> - <a href="http://behindkde.org/people/soc2007-three/">meet Leo Franchi, Juan González Aguilera, Andrew Manson and Marcus Hanwell</a>!

<!--break-->
