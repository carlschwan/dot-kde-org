---
title: "KDE Commit-Digest for 18th March 2007"
date:    2007-03-19
authors:
  - "dallen"
slug:    kde-commit-digest-18th-march-2007
comments:
  - subject: "any screenshot of the nepomuk GUI?"
    date: 2007-03-19
    body: "any screenshot of the nepomuk GUI?"
    author: "Patcito"
  - subject: "Re: any screenshot of the nepomuk GUI?"
    date: 2007-03-19
    body: "to say it sucks, because its not ready?"
    author: "lukilak@we"
  - subject: "Re: any screenshot of the nepomuk GUI?"
    date: 2007-03-19
    body: "To explain this flame-bait looking comment, if you would read back on the articles of late, you will notice people going completely off on one because they saw screenshots of extremely early development version of things - not constructive critisism, simply a case of people saying \"Uh, looks crap!\". So... people are getting scared of showing screenshots of their early work because people don't understand the difference between a tech demo and a finished product ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: any screenshot of the nepomuk GUI?"
    date: 2007-03-19
    body: "so because of a handful of assholes, thousands of users will be deprived from the excitement of peeking? why do people hate us screenshot voyeurs?"
    author: "zenity"
  - subject: "Re: any screenshot of the nepomuk GUI?"
    date: 2007-03-19
    body: "Perhaps because those thousands of users will be really deprived if developers loose their motivation to work on their projects...?"
    author: "Diederik van der Boor"
  - subject: "Re: any screenshot of the nepomuk GUI?"
    date: 2007-03-20
    body: "i am only saying that this is a great example of just how a few stupid people can ruin it for the vast majority :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: any screenshot of the nepomuk GUI?"
    date: 2007-03-19
    body: "No screenshot, but you can find and download the FOSDEM presentation (PDF) from their website if you go there.\n\nI really enjoyed reading more bout Nepomuk.\nEven though I don't understand all of the technical stuff,\nI do see where they are trying to go.\n\nNepomuk is the thing that I'm really anticipating, after KDE4.x . And more professional games coming to Linux :)"
    author: "Darkelve"
  - subject: "Thanks Danny"
    date: 2007-03-19
    body: "This digest is very good, and I have built kdelibs on mac os x now. \nI hope there are some special entry on the KDE on other platform.\n\nFor example the progress on KDE mac os x and KDEWin32"
    author: "younker"
  - subject: "Re: Thanks Danny"
    date: 2007-03-19
    body: "i don't know if i had to laugh or cry, but right now the esiest way to test kde4 is on Macos just download some packages and it is up and running.\n"
    author: "djouallah mimoune"
  - subject: "Re: Thanks Danny"
    date: 2007-03-19
    body: "It's just as easy on Suse as they provide precompiled packages. Just set up Yast to download the packages.\n\nAnd there are no reasons why it can't be just as easy on other distributions. The only thing missing is someone to make the packages. There are already volunteers doing it for Suse and Mac, do you volunteer for your distribution?"
    author: "Morty"
  - subject: "Re: Thanks Danny"
    date: 2007-03-19
    body: "Morty\n\nI know this issue has nothing to do with kde it is a linux limitation\nMy friend can you imagine a person who want to update his linux distribution ( wine, codec etc ) and has no a direct internet connection to his computer. That why the work done bye the KDE/MAC fame  is fantastic just download some files ( lets say in a cyber caf\u00e9) copy them to a flash disk, go home, install them and it is up and running, no weird dependencies problem. I hope they will do the same for windows, for me, kdewin-installer is not the right solution. \nFriendly  \n"
    author: "djouallah mimoune"
  - subject: "Re: Thanks Danny"
    date: 2007-03-19
    body: "Since I lived for over 5 years without direct internet connection at home, the copying of some files was the medtod I used to update my linux distributions. Including the KDE 2 and 3 betas, so I have no problem imagine this :-)\n\nAs long as you don't get random files, but the whole upgrade sets. Combined with your install media, weird dependencies are not a particulary big problem. Staying away from clueless packagers helps of course, and people distributing check-install packages."
    author: "Morty"
  - subject: "Re: Thanks Danny"
    date: 2007-03-19
    body: "ok Morty i am not savvy as you, i was speaking in the contexte of a powerless user as me\n\nfriendly\n\ni hope we are not abusing the dot ;)"
    author: "fromthewrongcountry"
  - subject: "Re: Thanks Danny"
    date: 2007-03-20
    body: "well, it also depends a little on the distro you're using. Suse for example offers KDE updates in a seperate foder on their FTP - just copy the whole content, take it home, and upgrade your KDE. But debian doesn't have that, so you can't do that. Kubuntu DOES have a seperate folder again...\n\nBut yes, it's harder than Mac OS X' way. Linux simply isn't designed around 'I have no internet connection'..."
    author: "superstoned"
  - subject: "Re: Thanks Superstoned"
    date: 2007-03-20
    body: "Superstoned\n\nAl least someone can understand my frustration with installing software in Linux, I think you understand exactly my point.  \n\nThanks \n"
    author: "djouallah mimoune"
  - subject: "Re: Thanks Danny"
    date: 2007-03-19
    body: "I think, there is not many platform specific commit in SVN. Most of the code is supposed to be cross platform thanks to QT4.  \nThere are exceptions of course, like phonon or solid back-ends, or the win32 installer to name a few.\nIf you want news about the mac osx version, a good place might be Benjamin Reed's blog (http://www.racoonfink.com/) or http://www.finkproject.org. The progress seems very good on mac osx ! \nFor Win32 progress, there is this site : http://kdelibs.com (not often updated)\n\nAnd if you have not, you can also read this article (from January)\nhttp://dot.kde.org/1168899755/\n\ngood luck :)"
    author: "shamaz"
  - subject: "George Takei"
    date: 2007-03-19
    body: "I guess somebody committed this by accident? Or as a joke? :-)\n\nhttp://commit-digest.org/issues/2007-03-18/moreinfo/642905/#visual"
    author: "AC"
  - subject: "Re: George Takei"
    date: 2007-03-19
    body: "Humm probably should have been the easter egg.... To bad it is already found..."
    author: "boemer"
  - subject: "Re: George Takei"
    date: 2007-03-20
    body: "the bad side of FOSS development ;-)"
    author: "superstoned"
  - subject: "True RAD still missing in KDevelop 3.4"
    date: 2007-03-19
    body: "I am used to Delphi and KDevelop is rather confusing.\n\nNew KDE application projects should open in design mode.\n\nWhen you add a component and doubleclick on it, the code for an onclick \"event\" or whatever the equivalent thing in QT is called, should be automatically added to your code.\n\nThe IDE should hide stuff which I don't actually need right away. There is way too much clutter."
    author: "Anders"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-19
    body: "In Qt there is no onclick event. Rather, a signal is emitted. You connect this signal to a slot, where you respond to it. There is a graphical signal/slot connection editor. This is one of the elementary principles of Qt. IMO there is no point in catering to someone who does not understand the basic principles of the software stack he is working with."
    author: "slougi"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-19
    body: "I disagree. There is very well a point in catering to everyone who wants to build a KDE application right away without endless studying of hundreds of manual pages. This is what the original post is about. And besides there is not much difference between event/event-handler and signal-slot anyway. At least not for a novice user. You have a button. And you want to do something as soon as someone clicks on it. This should be extremely easy to achieve. It's just basic functionality for a development environment. It isn't right now. Delphi can do that. KDevelop can't. So as much as I like KDevelop I think the original poster has a very valid point here.\n"
    author: "Michael"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-20
    body: "what's so difficult about QObject::connect(&myButton, SIGNAL(clicked()), &myObject, SLOT(myFunction()))?\nIt's just as hard to do that in Delphi if you aren't willing to learn :-)"
    author: "Jonas"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-20
    body: "well BUT the Delphi form designer adds that line by itself when you double click a button..! ...\nsometimes it's the atom-layers you're ahead of the competition that actually enable the average Joe user to use your application.."
    author: "eMPee"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-20
    body: "> In Qt there is no onclick event. Rather, a signal is emitted. You connect this signal to a slot, where you respond to it.\n\nAbsolutely wrong.\nMost of the event handling of Qt is based on reimplementing the virtual function that represent the event handlers, eg mouseEvent(), paintEvent(), etc. If not that, you can attach an event filter for a generic QObject (and every widget is a QObject, too).\nBut there are no signals about the events, unless you provide them on your own.\n\n> This is one of the elementary principles of Qt.\n\nYou know that pretty well, it seems..."
    author: "Pino Toscano"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-20
    body: "Ok, and I did know that. Perhaps I posted in haste. However, the usual model is to connect to the QAbstractButton::clicked signal, and not to reimplement the event handlers. I'd argue that reimplementing an event handler is quite a lot more involved than responding to a signal."
    author: "slougi"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-20
    body: "Adding to my previous post (again having posted to quickly :), there is also the issue of which event handler to reimplement.\n\nLet's take the simple mouse click. For correct handling, the mouseReleaseEvent is probably correct, and if you want to go there it could probably be automated in the IDE to reimplement this function. Were it not automated, it would be quite easy to make the mistake of reimplementing for instance mousePressEvent, at which point the applications behaviour would deviate from other applications.\n\nAlso, as far as I know, Qt events differ from Delphi events in several aspects. It's been a while since I wrote any delphi code, but as I recall events there are more like static slot connections. That is, a certain callback is defined (usually in the form holding e.g. the button), and the button from which the event originated is passed as a parameter. I don't have any profound insights (not that I usually have any :) on what this difference means, just pointing it out.\n\nQMetaObject::connectSlotsByName could probably be used in an IDE to achieve something similar to Delphi."
    author: "slougi"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-20
    body: "Hi,\n\nI just wanted to second Anders point.  I believe theres alot of potential in KDevelop should it go RAD,  I've been watching/using it since the first release and love it for \"console/terminal\" apps but find it gets clumsy with fairly bigger  \nQt/KDE projects.  \n\nI've also notices that theres still alot of KDE hackers who still havent bought into KDevelop, they use Vi or Emacs :-( Sad really.\n\nThe gap i see is for a powerful RAD enviroment much like Delphi or Visual Studio 2005, they give you the a powerful code editor and all, with out the RAD tools getting in the way.\n\nRAD is what mostly made Windows succeed interms of App support, it was way to easy to write apps in VB and Delphi, meaning millions of apps added a yr to the platform.  We need something for GNU/Linux that will bridge that gap!  And I believe KDevelop being the Ultimate IDE should be that bridge, the closest i've seen so far is Gambas, which has done very very well, just not too keen on the language but excellent work, and great IDE with RAD, many powerful controls, SDL, alsa.sound etc...\n\nPlease, will we ever see this in KDevelop or should someone/I/We start a new separate project? (scratch the itch?)\n\nAnything in this direction for KDevelop4?  (Crossing my fingers here...)\n\n\n\n\n"
    author: "Samuel Mukoti"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-03-20
    body: "Hi,\n\nfor one, I know about 10 developers that use KDevelop3.4 on a daily basis for GUI-related development (about 5 of them are KDevelop hackers). So it can't be all that bad. I do however also understand the OP and see how its not as easy as a single click to create a gui-app that reacts on a button click.\n\nHowever, we are limited by Qt's designer and the interfaces it provides. We won't write another Qt designer. \n\nFor Qt3/KDE3 apps we copied Qt designer and made an embeddable KDE version from it. That integrates at least the creation of a subclass from the Form including stubs for all custom slots you created. For Qt4 projects this doesn't work as Qt4 apps don't work inside Qt3 apps.\n\nFor KDevelop4 we have a easier job to integrate the Qt4 designer as its specifically designed for that, however I don't know enough about its interfaces to state here what will be possible. What I can imagine is a way to generate the auto-connect-slots via a shortcut or toolbar button for a widget in the subclass. We will also have a much nicer UI which allows to work on GUI and Code at the same time much easier, at least with multiple monitor setups."
    author: "apaku"
  - subject: "Re: True RAD still missing in KDevelop 3.4"
    date: 2007-05-22
    body: "So, what is developer's decision? Will there true RAD in Kdevelop4 or will not?"
    author: "Milosz"
  - subject: "oxygen new KNotify icon"
    date: 2007-03-20
    body: "The first version looks better IMO... :)\n\nhttp://commit-digest.org/issues/2007-03-18/moreinfo/643013/#visual"
    author: "DanaKil"
  - subject: "Re: oxygen new KNotify icon"
    date: 2007-03-20
    body: "I like the old shape with the new filling. :)"
    author: "logixoul"
  - subject: "Re: oxygen new KNotify icon"
    date: 2007-03-20
    body: "I partially agree here - maybe the shape of the new icon with the colours of the old one."
    author: "Pino Toscano"
  - subject: "Re: oxygen new KNotify icon"
    date: 2007-03-20
    body: "well open up the source of the SVG file and zoom out and you'll recognize the advancement of the icon in the new version! It looks better @16px then the old one...\nanyways I still think all objects in those oxygen icons should not be pictured exactly from the front, especially the more complex ones like printers and so on... because everyone is accustomed to perceive things like scanners or discs with a certain depth, which would better be reflected in a slightly isometric view (angle pi/16 or so... ;)"
    author: "eMPee"
  - subject: "I had to remove completely KOffice"
    date: 2007-03-27
    body: "KOffice is not fully compatible with Open Document Format. For example opening\nand then saving an odt in kword will lose objects like math formulas.\nAt every koffice update my konqueror re-associate odf formats with Koffice,\nat the second file \"burned\" I choosed to completely remove koffice.\nPlease, don't work for fancy, improve file compatibility with Office and at least with OpenOffice, or the suite will be useless if not harmful."
    author: "mattepiu"
---
In <a href="http://commit-digest.org/issues/2007-03-18/">this week's KDE Commit-Digest</a>: Interface experiments in <a href="http://amarok.kde.org/">Amarok</a> 2.0, with the aKode engine shown the door. Initial work on incremental parsing functionality in <a href="http://www.kdevelop.org/">KDevelop</a>. Further functional development in the Step educational physics simulation package. More refinement of the <a href="http://oxygen-icons.org/">Oxygen</a>-themed KDE Games artwork, revised sounds in the Oxygen sound theme and more work done on the Oxygen widget style. The Oxygen iconset is dual-licenced as Creative Commons and LGPL. Support for the <a href="http://www.plkr.org/">Plucker</a> document format in <a href="http://okular.org/">okular</a>. Zoom work (ViewBar) and <a href="http://scan.coverity.com/">Coverity</a> fixes in <a href="http://koffice.org/">KOffice</a>. Basic Phishing protection and the start of user documentation in <a href="http://www.mailody.net/">Mailody</a>. Optimisations in KJS (JavaScript interpreter) and KSysGuard. Import of Athec into playground/games and KBackup to playground/utils in KDE SVN. First <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>-based GUI elements appear. KSplashX displaces KSplashML as the splash screen engine for KDE 4.
<!--break-->
