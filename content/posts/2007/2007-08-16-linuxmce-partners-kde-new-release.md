---
title: "LinuxMCE Partners with KDE for New Release"
date:    2007-08-16
authors:
  - "aseigo"
slug:    linuxmce-partners-kde-new-release
comments:
  - subject: "This is soo much needed!"
    date: 2007-08-16
    body: "I know there is a big market for things like harddisk recorders already, but honestly if you actually tried the offers out there it just makes you cry.  My dad bought one a year ago after he saw me use a self-build one.  And I highly admire his persistence to use it since the thing drives me crazy. Its got a user interface that is really really sad.\n\nSo, having an open source software stack so companies can include that instead of pushing their own sad piece of quickly-manufactured software is a great prospect IMO.\n\nGo libre-stuff, go!"
    author: "Thomas Zander"
  - subject: "Re: This is soo much needed!"
    date: 2007-08-17
    body: "Yes, the market seems quite eager for cheap third party software. I was surprised that an Aldi brand and a Philips subsidiary brand, totally unrelated, both had the same DVD/Photo/MP3 player software running."
    author: "Dennis"
  - subject: "Cool......."
    date: 2007-08-16
    body: "With all the lovely stuff going into Plasma, I can well see why it would be a compelling platform for building a lovely looking media centre interface. MythTV currently uses Qt as well.\n\nPeople may laugh at open source media centres like this and say \"What chance have they got?\", but to be honest, it's the only way forward for many of us to get the 21st century TV and entertainment systems we all thought we were going to get, but just haven't. The proprietary guys are falling over themselves trying to lock you into a particular supplier or subscription, with DRM systems so onerous that all the usefulness that could have been there has merely been taken out. All the restrictions merely take away all the advantages and convenience of having a media centre type system in the first place!\n\n\"For instance, one can already purchase motion sensitive remote controls made specifically for LinuxMCE as well as pre-assembled, plug-and-play high end LinxuMCE based media systems from companies such as Fiire.\"\n\nWell, they might well have another customer pretty soon ;-)."
    author: "Segedunum"
  - subject: "Ubuntu Media Center / Mythbuntu on the wrong path"
    date: 2007-08-16
    body: "It would be nice to see Ubuntu Media Center (https://wiki.ubuntu.com/UbuntuMediaCenterTeam) and Mythbuntu (http://mythbuntu.org) join LinuxMCE and the KDE team to create one kickass Free/Open Source media center. Given that MythTV is built on Qt, that would make perfect sense, wouldn't it? But no -- they want to build their tools based on GTK and Gstreamer (which are LGPL) so that they can force proprietary DRM and closed-source software on the user. Even the Mythbuntu installer and desktop is based on GTK instead of Qt -- how silly is that?\n\nI congratulate LinuxMCE for building your exciting technology on top of the truly Free Software desktop: KDE. Too bad Ubuntu has so many anti-KDE, anti-Qt, anti-GPL, pro-DRM zealots calling the shots. (BTW: I'm not referring to Mark Shuttleworth or Jonathan Riddell, but to some of the GNOME developers hired by Canonical)."
    author: "AC"
  - subject: "Re: Ubuntu Media Center / Mythbuntu on the wrong path"
    date: 2007-08-16
    body: "i find the persistent NIH in the open source world frustrating as well. as i've blogged about before, where there is real innovation happening or even just simple \"because i wanted to learn\" kind of things, duplication of effort doesn't bug me one bit; but reinvention for reinvention's sake to fullfill goals that basically boil down to \"it's not the colour of green i use\" is silly.\n\nwhether or not the *buntu efforts fall into that category or not, i really don't know to be honest. i'll leave that judgement up to others.\n\nwhat i do see is a bunch of '0%'s on their progress list, though they do have remotes over lirc working apparently ;) elisa is also a young project; the cynic in me says it's primarily a way to promote gstreamer while the realist in me says it's a very young project with a *long* ways to go.\n\nlinuxMCE is pretty darn mature and builds on some other solid projects like MythTV that have been around for a long time and proven themselves. the pragmatist in me likes that.\n\nthat these projects weren't grown up inside of KDE doesn't bother me. that LinuxMCE didn't try and build their own OS and that they looked at various options (yes, not just KDE, but also GNOME and XFCE; not just *buntu, either) before deciding which way to flow makes me feel much more confident in their abilities and pragmatism.\n\nas for projects like MediaBuntu, well ... we've essentially already created and shipped exactly what they are aiming for with this release. it's a working media center + desktop + ubuntu base.\n\nif other individuals or projects would like to coordinate efforts or even outright join in with what we're doing, that would be awesome. they would all be *more* than welcome. of course, it's free software and people will do what they want =) given Mark's recent \"coordinate and have a single rhythm\" mantra lately, it would make sense to me to see his own community embrace that first.\n\ni have started talking with some people in the wider open source media center world about these things. hopefully we can build up some more steam and power behind this train =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Ubuntu Media Center / Mythbuntu on the wrong p"
    date: 2007-08-16
    body: "Right on, Aaron! It's great to see LinuxMCE reach out to KDE. I think they would do good to also let their other \"upstream\" projects such as MythTV and Linux know about them. These are two huge communities, and simply sending a couple of patches or messages from a LinuxMCE.org email address with a footer/signature advertising LinuxMCE will go a long way toward raising awareness and their reputation.\n\nThey should also blog on PlanetKDE now that they are using and improving Plasma!"
    author: "AC"
  - subject: "Re: Ubuntu Media Center / Mythbuntu on the wrong p"
    date: 2007-08-17
    body: "\"It would be nice to see Ubuntu Media Center (https://wiki.ubuntu.com/UbuntuMediaCenterTeam) and Mythbuntu (http://mythbuntu.org) join LinuxMCE and the KDE team to create one kickass Free/Open Source media center. Given that MythTV is built on Qt, that would make perfect sense, wouldn't it? But no -- they want to build their tools based on GTK and Gstreamer\"\n\nI take a pretty extreme view to NIH syndrome, and I don't really apologise for it. If there are legitimate reasons for re-architecting something, such as you want to get certain things out of a toolkit you're using you wouldn't out of anything else, then fine. However, if all you're doing is re-writing something in another toolkit to get the same effect with the same functionality, in some cases having to work harder, then I find that daft.\n\nThis happened in openSUSE with the Gnome install profile. The people there steadfastly refused to have TaskJuggler in there, even though it was the best project management tool we have. All traces of Qt and kdelibs were to be eliminated, even though GTK is installed on a KDE install and no one cares.\n\nAs for GTK apps on KDE, couldn't care a less. We have QtGTK which gets GTK apps to fit in on the desktop and I don't particularly see why they should be rewritten in Qt with KDE libraries.\n\nI just find all the rewriting for GTK stuff to come out of insecurity."
    author: "Segedunum"
  - subject: "Re: Ubuntu Media Center / Mythbuntu on the wrong p"
    date: 2007-08-17
    body: "Insecurity indeed. They try to lock KDE out of their environment, as they feel they can't compete head-to-head with it on a technical plane. So they politicalize the whole issue...\n\nPity, in FOSS, the best technology should win. Yet we have exaile (amarok clone), and that project to clone Kalzium... Aaah, let's see it as a compliment. Those apps are soo good, even the KDE/Qt haters can't be without them so the rewrite em..."
    author: "jospoortvliet"
  - subject: "Re: Ubuntu Media Center / Mythbuntu on the wrong p"
    date: 2008-01-25
    body: "Mythbuntu isn't on the wrong path\n\nMythbuntu even using older version of xfce. it's to reduce resource. and it's still on 1 cd. so you can using live disk on non-linux pc and joint myth w/o install.\n\nbut it have only small program pre-install. it's like it's make for run only myth. suitable for being home media server. (and live cd on new computer)\n\nwhile LinuxMCE still have other program install but it as big as 3 cd and no live mode suitable for multi function Media Center\n\nsince it's run on myth tv. so both can run together"
    author: "Pi"
  - subject: "KDE4 integration plans"
    date: 2007-08-16
    body: "Hi!\n\nFirst i'd like to thank you for the article but to be honest: i would have liked to hear more about the \"port\" going on for KDE4. Or things like if it will get shipped directly with KDE (basic package/multimedia package) or if you'll need to download it from there homepage and install it and it just fits into the plasma desktop (design wise). Or are these things not decided yet? If this is the case i'm sorry for this post ^^. At last i've got to say: great work so far. move on ^^\n\nfriendly greetings\n\nBernhard"
    author: "LordBernhard"
  - subject: "Re: KDE4 integration plans"
    date: 2007-08-16
    body: "> Or are these things not decided yet?\n\nsome are, some aren't, but i'd prefer to keep the discussions in articles such as these down to what is already done. which is why i wrote what i did and didn't go off too far into the \"this is what will happen in N months\".\n\nimho, it's a major step that there is a kde3 based distro that features a bolted-on media center with everything set up. i think it's also a major step that the projects are working on kde4 integration.\n\nif i went out and speculated on exactly how everything will look like in 6 months time, there would be people screaming \"vapourware!\" and, to be honest, i  also might get some parts wrong and people would be set up for disappointment.\n\ni'm *really* glad you are curious about these things, however, as you did hit on some of the most critical issues with your questions: they are exactly the kinds of interesting points we will be working on.\n\nwhat i will say is this: the desire is to get as much of the LinuxMCE code into KDE's svn where we can all work on it there together."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE4 integration plans"
    date: 2007-08-16
    body: "thx so much for the answer ^^ this is absolutely enough information ^^ and i hope that one day (hope soon) i'll be able to help you guys developing on kde.\n\nfriendly greetings\n\nBernhard"
    author: "LordBernhard"
  - subject: "Re: KDE4 integration plans"
    date: 2007-08-16
    body: "just one last question (i hope i don't annoy you ^^): are there any plans to make linuxmce (or what ever the KDE4 \"port\" will be called) platform independable? \ni mean afa i can see all major features of linuxmce are also possible under windows and libplasma is also platform independable isn't it?\nit would be totally ok for me if it isn't but for some friends of mine it would be cool if they were able to use a >good< mediacenter under windows."
    author: "LordBernhard"
  - subject: "Re: KDE4 integration plans"
    date: 2007-08-16
    body: "i am personally uninterested in a windows port. suggest to your friends that they use linux, and maybe exactly because it has a good mediacenter. \n\ni see benefit in reaching out to developers and extending open standards in supporting windows, but my personal focus remains on Free software. last i checked, windows didn't meet that criteria very well.\n\ni mean, i suppose we could make windows better by developing for it primarily and help keep microsoft's entrenched position in the desktop world which is the primary source of our (the Free/open source software world's) biggest problems. i could also pretend i care about proprietary software enough to work on it; but i don't. given that this is a free/open source project, people trying to convince me otherwise and change my position is ... funny.\n\ni do think that there's a balance to be struck here, one that i really don't see many people benefiting from the windows/mac ports owing up to. to me that balance is to be struck somewhere between \"all\" and \"nothing\" and should be drawn based on keeping focus on Free/open source software as a holistic entity since we do rely on it and owe it the existence of things like KDE today."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE4 integration plans"
    date: 2007-08-16
    body: "well.. thx again for the answer.. i'll see what i can do to port my friends to kde ^^\n\ni see what you mean with windows and i totally agree... it's just hard to change peoples minds.. but well .. if they want something better they should switch ^^\n\nthink i've taken enough of your time today ^^ have a good hack and spare time ^^ (does this fit together? ^^)\n\nfriendly greetings\n\nBernhard"
    author: "LordBernhard"
  - subject: "Hardware awareness"
    date: 2007-08-16
    body: "I watched the video. A major portion of the magic here seems to come from awareness of connected devices. Could parts of that be merged into Solid? \n\nInterestingly, a lot of the hardware information is provided by the user; which room is a particular light located in? Which port of the digital TV receiver is connected to which input of a particular TV set?"
    author: "Martin"
  - subject: "Re: Hardware awareness"
    date: 2007-08-16
    body: "Ooh! Ohh! Yes, can that become part of the Solid hardware framework? How hard would that be? It would be sweet if I could use cron jobs to control my house appliances!"
    author: "kwilliam"
  - subject: "Re: Hardware awareness"
    date: 2007-08-16
    body: "they actually use a lot of the same tech that solid does, namely hal/dbus for many (though not all) of the hardware events. the \"what to do with the device\" after that is a large part of the value add. i'm not sure it's solid's place to set up application specific configuration files based on those events.\n\nin any case, we will be exploring how to share more code in this area as hardware awareness is obviously an important part of both the media center and the desktop."
    author: "Aaron J. Seigo"
  - subject: "Re: Hardware awareness"
    date: 2007-08-16
    body: "There is a space between \"what does this device look like according to HAL\" and \"what to do with this device\". That is the user-supplied \"meta data\", of which I gave two examples, and this seems to be precisely where the magic happens.\n\nIf the Solid layer could incorporate the additional information that a particular light has \"location: living room\" and \"role: decoration\", then any application using Solid could with a little effort achieve the same level of magic as does LinuxMCE. So if the descriptional parts of whatever DigitalTVBox, IRSensor etc. classes are in LinuxMCE today could be merged into Solid that would be a great unification, possibly?"
    author: "Martin"
  - subject: "Other applications"
    date: 2007-08-16
    body: "Actually, what appeals to me the most about a self-built MediaCenter PC as opposed to a TiVo box is the ability to add other apps.  I have DVR functionality in my cable box, but I stream media from my computers to my TV through a hacked XBox because it also plays games, has XBMC, and I can run emulators off it.  It is nice but lacks any real horsepower.  I've been considering building a decent MediaCenter PC and ditching the DVR service from my cable company.  This looks to be a great alternative!"
    author: "T. J. Brumfield"
  - subject: "Re: Other applications"
    date: 2007-08-16
    body: "> is the ability to add other apps\n\nyes, that is will be one of the strengths of having an open platform that we include with the plasma desktop: the apps will grow nicely. and those apps that just aren't media center appropriate (form factor wise) can be run from the desktop that runs under the media center =)"
    author: "Aaron J. Seigo"
  - subject: "Nice, but"
    date: 2007-08-16
    body: "It looks nice and it is always good, when commercial products join to support a project. \n\nBut after all the things I've read until now, I'm a little bit upset.\nImho a KDE Plasma MediaCenter would mean I zoom to my MediaCenter and their, I'll get some widgets connecting to Amarok/DigiKam/... via DBUS and give me the ability to show/hear my media in a comfortable way. Very cool might be sharing my Libraries via Zeroconf.\nFor me at the moment LinuxMCE seems to be some kind of sperate piece of Software, they use so many different technologies. This is Ok for MediaCenter only Screens, but not on my Desktops. Here I think the footprint should as small as possible...\n\nI believe in the kde team, and I know that many killer features will be released with 4.1. After the base is done in 4.0.\n\nLinux MCE does many things good. I will try it out as soon as I have some sparetime left.\n\nAnd to the LinuxMCE guys... you did a great piece of software and with your decision to join plasma, you got style too....\n\n\nAbout the XBMC:\nI think its the perfect MediaCenter UI.\nMaybe we should get this project (http://www.xboxmediacenter.com/wiki/index.php?title=Linux_port_project) team to join us. "
    author: "Bernhard Rode"
  - subject: "future development?"
    date: 2007-08-16
    body: "I don't quite understand it either. It makes sense that they aren't using Amarok, since its not a \"10 foot interface.\" But I don't quite get where Plasma fits into all this, perhaps its the future direction of development for LinuxMCE.\n\nI watched a lot of the LinuxMCE video, its simply amazing."
    author: "Ian Monroe"
  - subject: "Re: future development?"
    date: 2007-08-16
    body: "Plasma is not just the desktop. It's a set of libraries for drawing and animating cool things. (For instance, Amarok 2 uses Plasma to render it's Context Browser.) Perhaps they are thinking of rewriting LinuxMCE's user interface to take advantage of the Plasma libraries, so they don't have to reinvent the wheel. That might make the interface more memory efficient, better looking, and easier to customize the look. (I'm just making an educated guess here; I'm not a developer so I don't really know what LinuxMCE/KDE4 integration would be like.)"
    author: "kwilliam"
  - subject: "Re: future development?"
    date: 2007-08-16
    body: "You're preaching to the choir here.  Ian is an amarok devel :)"
    author: "Troy Unrau"
  - subject: "Re: future development?"
    date: 2007-08-16
    body: "I hope that the Linux MCE Guys integrate in the Technlogies which build our pillars such as Plasma, Decibel, Phonon....\n\nIf we can get all their functionalities into the Desktop... that would be unbelievabel cool.\n\nI think a lightweight GUI to connect to a server would be great a great step.\n\nThen announcing/sharing local pictures/movies/... to/with a central server via upnp protocol or just zeroconf would make it even better."
    author: "Bernhard Rode"
  - subject: "Re: future development?"
    date: 2007-08-16
    body: "> I don't quite get where Plasma fits into all this\n\nit's the \"point of desktop integration\" and i happen to be the point of contact, so ... yeah. =) it's not so much that there will be a heart transplant in linuxMCE where plasma is put into the core, but that the two projects will integrate around the desktop. personally, i hope to have plasmoids available in media center interface (that's why there is a \"media center\" form factor) and that some of the current linuxMCE components will draw in the future from kde technologies (e.g. solid or phonon, certainly more qt4)"
    author: "Aaron J. Seigo"
  - subject: "Re: Nice, but"
    date: 2007-08-16
    body: "> For me at the moment LinuxMCE seems to be some kind of sperate\n> piece of Software\n\nindeed. the same would be said of XBMC as well if they came along. that's the whole point of starting to work on integration.\n\nkde could start our own media center from scratch and try and build up a group of developers and all the software needed. maybe we could call is MediaBuntu or something ;)\n\nor we could work with an existing project. unfortunately, all of the media centers are pretty \"foreign\" to the desktop as add-on projects.\n\nso if we don't go the NIH route, then we have to work on integration and harmonizing technologies and what not. note the part about providing a standardized interface mechanism? they emphasized that versus \"here is our media player! use our media player! booyah!\" they currently use myth / xine and you know what? those can be swapped out. that appeals to me.\n\nwhat's really cool is the MCE guys are *looking* for a way to integrate with a desktop, so i don't have to go out and convince someone about that first important change in mindset.\n\nas for XBMC, it does look really cool (i've played with it, it's slick in a few ways). but their linux port is really, really not even close at this point. i'd be somewhat surprised as well if their linux port turned into a complete fork with a different set of goals (e.g. not being a stand alone system). \n\npersonally, i'd rather start with someone that already works and already has the mindset of desktop integration. we can put some nice oxy art on top of MCE and provide access to more apps and tap into larger developer pools.\n\nto be perfectly blunt, i think XBMC is a buzzword and a coolio fad in the hobbyist world. that doesn't mean it's crap, it just means it's not particularly suited for what we need, cool buzzword truthiness aside."
    author: "Aaron J. Seigo"
  - subject: "Plasma hotness -vs- GNU's flash project?"
    date: 2007-08-16
    body: "As plasma seems to be becoming this great rich media vehicle, would it be possible / desirable to make it the front end to something like GNU's flash clone? With SVG, animation effects engine as well as the scriptability of plasma it seems like they would be a somewhat natural pairing..."
    author: "Borker"
  - subject: "Re: Plasma hotness -vs- GNU's flash project?"
    date: 2007-08-16
    body: "Well, Flash is only really used in web pages. Plasma is a desktop technology. So unless browsers support Plasma applets (and it's not even natively portable to OS X or Windows because it's based on X11), chances are not too good for Plasma to replace Flash.\n\nOn the other hand, chances are very good for Plasma to attract a shitload of desktop and web developers that create all sorts of useful and spiffy mini viewers and helpers which go far beyond what was achieved with SuperKaramba in KDE 3."
    author: "Jakob Petsovits"
  - subject: "Re: Plasma hotness -vs- GNU's flash project?"
    date: 2007-08-16
    body: "\"(and it's not even natively portable to OS X or Windows because it's based on X11)\"\n\nThe Plasma application itself isn't portable to non-X11 platforms, but isn't libplasma (or whatever its called, the lib that amarok is looking at using) still portable to any platform Qt runs on, since its mostly just built on top of QGraphicsView?"
    author: "Sutoka"
  - subject: "Re: Plasma hotness -vs- GNU's flash project?"
    date: 2007-08-16
    body: "I wasn't really thinking of replacing but implementing. Apps like amarok are embedding plasma into them, i can just imagine a lot of opportunities to take advantage of the existing realm of flash apps that could be embedded into apps (web browsers, standalone flash game players etc) as well as onto the desktop in a nice natively supported code base.\n\nA neat toy for example would be a flash game player with a library similar to what some of the xmame frontends provide. \n\nAnyways, just an idea I thought I'd bring up for a bit of discussion"
    author: "Borker"
  - subject: "Demo video"
    date: 2007-08-16
    body: "I appreciate the effort put into that video walkthrough, but in all honesty it's utterly disorienting. Does anyone else find it to be a bit of a sensory assault, or is it just me?"
    author: "Jack H"
  - subject: "Re: Demo video"
    date: 2007-08-16
    body: "i like it , so we are 50:50 here, would say its representative.\n\nnegative comments always outwights positive comments by ~20, cause most people dont post or say something if they -like- it.\n\nso my positive comment counts more than your negative comment :-)\n\nanyway this linuxMCE ist so fantastic , WOW!!!\n\ni cant wait to see the work"
    author: "jumpingcube"
  - subject: "Re: Demo video"
    date: 2007-08-17
    body: "Oh yeah, LinuxMCE looks amazing, it's just the production style of the video walkthrough itself just made it difficult for me to watch."
    author: "Jack H"
  - subject: "Re: Demo video"
    date: 2007-08-16
    body: "I found the video very impressive ... sensory assault? yes, for my taste a bit too much, on the other hand: the software is also useful if you do not install cameras and sensors all over your home."
    author: "MK"
  - subject: "Re: Demo video"
    date: 2007-08-17
    body: "I don't like the interface that much either.\nPLease put some oxygen and usability guys in this thing."
    author: "thomas Deffieux"
  - subject: "Video quality"
    date: 2007-08-17
    body: "Is it possible to create a videofile that is not HD? A _lot_ of systems are not capable of playing HD files and the flash quality is, well, less than optimal. Just a normal wav, mpeg, ogm and/or wmv is _very_ much appreciated.\n\nThanks!!"
    author: "LB"
  - subject: "KDE 4.0 \"media center\" Form factor"
    date: 2007-08-17
    body: "What do you think about using the \"media center\" form factor just as a Orbiter.\nSo kde can build the UI in a KDE Style way and just connect to a Server, which runs the whole MCE as a Media Director."
    author: "Bernhard Rode"
  - subject: "Re: KDE 4.0 \"media center\" Form factor"
    date: 2007-08-17
    body: "sounds good to me personally.. but i'm no kde dev (yet) ^^"
    author: "LordBernhard"
  - subject: "Perhaps I'll get an answer here..."
    date: 2007-08-17
    body: "First off, thanks for your work on KDE. I've been using it since 1.x and can patiently wait for 4.x. ;) Based on what I've read, it seems 'KDEmce' is LinuxMCE/PlutoHome rewritten to take advantage of Qt/KDE. Is this accurate?\n\nSince MythTV used Qt, why not port Pluto's sweet frontend to MythTV? This to me would mean more to the community. I don't think another Linux media center is needed, especially when LinuxMCE/Pluto Home is essentially a pretty face on top of MythTV (and other software).\n\nKind regards,\n\nCecil"
    author: "cesman"
  - subject: "Re: Perhaps I'll get an answer here..."
    date: 2007-08-24
    body: "LinuxMCE is not just another media center. It's full featured home automation system and media is just its part. So, that's why it should have its own GUI.\n\nIMHO it was not correct to integrate MythTV so  close to the Plutohome as it's done. MythTV should be just a part of the system like Asterisk or VDR, for example."
    author: "Michael"
  - subject: "LinuxMCE is the best!"
    date: 2007-08-24
    body: "That good idea to make integration home automation system and KDE. I like Plutohome and I prefer KDE as working environment. Now I can use both - thanks to LinuxMCE and KDE development teams. Waiting for Plasma release to see how it will work with LinuxMCE."
    author: "Michael"
---
When picking a media center solution for your PC, it tends to be a matter of compromise. There are solutions that are visually attractive, solutions that are Free/open source software, solutions that are more complete than others and solutions that integrate well with a desktop environment. In the past there have been few, if any, that have been all of these things.  After an extensive beta testing period a new version of <a href="http://linuxmce.com/">LinuxMCE</a>, release 0704, was recently made available to the public that shows how we can indeed have our media center cake and eat it too.  Read on for details of this release and future plans for KDE integration.



<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 235px; float: right">
<img src="http://static.kdenews.org/jr/linuxmce-logo.jpg" with="235" height="214" />
</div>
<p>LinuxMCE uses X.org's window compositing functionality to provide a simple to use fullscreen interface that is blended on top of the media being played. This interface is a key focus of LinuxMCE according to lead developer Paul Webber. "The traditional PC user interface doesn't work well on a TV.  So a different interface is needed, which is called the '10 foot' interface (in reference to the fact that people interact with media devices such as televisions from a distance). The '10 foot' interface is still in the same state as the desktop was pre 1983. There is no standardisation and each application has to figure out how to present its functionality to a user."</p>

<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 250px; float: left">
<a href="http://static.kdenews.org/jr/linuxmce-screenshot.jpg"><img src="http://static.kdenews.org/jr/linuxmce-screenshot-wee.jpg" width="250" height="141" /></a>
</div>

<p>In an effort to address the '10 foot' interface challenge, LinuxMCE and <a href="http://plasma.kde.org/">KDE Plasma</a> developers are working to bring the two projects together. The first step was making a prototype showing how LinuxMCE and KDE can work together, which is showcased in the 0704 release where LinuxMCE is integrated with <a href="http://www.kubuntu.org/">Kubuntu</a> and KDE 3.5.<p>

<p>Future efforts will integrate LinuxMCE technology closely with Plasma to make it a compelling media centre option for KDE 4. Bringing the much needed interface standardisation along with a rich set of features to the masses requires such a combined effort between the desktop environment and the media centre projects. This ambitious goal is possible due to both KDE and LinuxMCE being open source projects which hobbyists and corporate interests alike, can freely participate in.</p>

<p>Of course, interface alone is not enough: LinuxMCE also provides a compelling set of both traditional and innovative media center functionality. From its built-in media browser, one can play media both locally as well as on devices scattered throughout the house over the network. MythTV is used as the basis for the default media player functionality, but with LinuxMCE's design this can be swapped out for other options such as VDR.</p>

<p>Built in thin-client features allow systems to be booted remotely then have media streamed to them, with consumer electronic devices such as HD TVs, stereo components and iPods detected and set up upon being plugged in, often automatically with little to no user setup required. Wizard based screens with instruction video walk-throughs are provided for devices that can not be autoconfigured, making it simple to add even more complex hardware components to the system.</p>

<p>These are only some of the capabilities presented by this very comprehensive system. Other interesting features include its home automation system, motion sensitive gyro remotes that provide full access to all on-screen features with just three buttons and <a href="http://wiki.linuxmce.org/index.php/Features">much more</a>.</p>

<p>As a showcase of its breadth and depth, a 25 minute <a href="http://wiki.linuxmce.org/index.php/Video">video walkthrough and presentation</a> has been made that shows all the key features of LinuxMCE 0704.</p>

<p>Encouragingly, there is also an emerging marketplace growing up around LinuxMCE. For instance, one can already purchase motion sensitive remote controls made specifically for LinuxMCE as well as pre-assembled, plug-and-play high end LinuxMCE based media systems from companies such as <a href="http://fiire.com/">Fiire</a>. Such consumer electronics products are helping open source media centers move from being in the realm of the enthusiast to being a realistic option for the average person.</p>

<p>While much attention is being paid to the KDE 4.0 release, there is already thought and work going into the future of KDE beyond the first KDE 4 release. The integration of media centre technology in concert with the LinuxMCE project is a prime example of this. By bringing a usable, consistent and comprehensive interface to the modern maze of media, Free software is staking out a leadership position in this area of client-side computing.</p>

<p><a href="http://wiki.linuxmce.org/index.php/Download_Instructions">Downloads of LinuxMCE</a> are available via FTP/HTTP and BitTorrent. DVD distribution of LinuxMCE can also be purchased at cost (US $3-$5 at the time of this writing) via Google Checkout on the <a href="http://linuxmce.com/">LinuxMCE main page</a>.</p>
