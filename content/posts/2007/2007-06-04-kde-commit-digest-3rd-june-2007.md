---
title: "KDE Commit-Digest for 3rd June 2007"
date:    2007-06-04
authors:
  - "dallen"
slug:    kde-commit-digest-3rd-june-2007
comments:
  - subject: "Blur"
    date: 2007-06-03
    body: "\"would be great if we had blur zack ;)\"\n\nNo blur? Even Opera has blur!"
    author: "EMP3ROR"
  - subject: "Re: Blur"
    date: 2007-06-03
    body: "opera supports the svg blur filter?"
    author: "Aaron Seigo"
  - subject: "Re: Blur"
    date: 2007-06-03
    body: "I think so.\nAt least Opera 9.21 blurs this:\nhttp://www.w3schools.com/svg/filter1.svg"
    author: "EMP3ROR"
  - subject: "form factors vid"
    date: 2007-06-03
    body: "so, the form factors screen cast got chopped at the end in the version on the commit digest. you can grab the full thing via bittorrent here:\n\nhttp://torrents.thepiratebay.org/3703447/plasma_formfactors_hi.ogg.3703447.TPB.torrent\n\n"
    author: "Aaron Seigo"
  - subject: "Re: form factors vid"
    date: 2007-06-03
    body: "Just wanted to throw in some praise for what is being done with Plasma and it's supporting technologies.\n\nI've always believed in doing something the Right Way and I really believe that you are taking the time to do this with Plasma.\n\nThank-you, from one happy KDE user :)"
    author: "Mark Veinot"
  - subject: "Re: form factors vid"
    date: 2007-06-04
    body: "Could somebody give me a pointer on how to play this file on windows? I tried installing the codecs from http://www.gromkov.com/faq/faq2004-0076.html, but WMP still complains that it can't find the codecs...\n"
    author: "Andr\u00e9"
  - subject: "Re: form factors vid"
    date: 2007-06-04
    body: "WMP? WTF? MPlayer works on Windows."
    author: "guh"
  - subject: "Re: form factors vid"
    date: 2007-06-04
    body: "\nvlc is is a great media player try it on windows \n\nhttp://www.videolan.org/vlc/\n"
    author: "Sebastian Schmitt"
  - subject: "Re: form factors vid"
    date: 2007-06-05
    body: "How was it done?"
    author: "semsem"
  - subject: "Oxygen"
    date: 2007-06-03
    body: "Am I am the only one who thinks the new folder icons look ugly inside of Konqueror or dolphin?\n\nI mean, they look 3 dimensional but 3-dimensional towards the spectator. The angle/perspective does not fit. The icons are wonderful for the desktop but they make Dolphin look bad.\n\nTake a flat folder icons from another icon set to get an idea.\n\n"
    author: "Nye Gen"
  - subject: "Re: Oxygen"
    date: 2007-06-03
    body: "No, I completely agree with you! Usabilty here, usability there, but I positively hate those folder icons - I've said it before and I'll say it again!\n\nI appreciate the guys work and that there is a more coherent HIG and everything, but personally I don't like those folder icons."
    author: "Matija \"hook\" Suklje"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "You guys are just confusing the image with a folder. They really intended it to look like just a blue rectangle. Leave the guys alone. Your blue rectangles are perfect guys!"
    author: "Daniel"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "There is something anal about a rectangle.\nI think this folder analogy is to old and needs to be replaced.\n\nLets see, I have some pictures, music, videos and such. Do I stuff\nthem in a folder? Nah, if it's porn I hide it in box under the bed.\nYeah, thats it, a cardboard box is a better symbol. A brown cardboard\nbox from IKEA."
    author: "reihal"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "I must admit that I prefear the previous version : http://danakil.free.fr/folder-blue.png\n\n:-/"
    author: "Heller"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "Maybe you're right when you compare the two icons, but I think when you look at the whole picture the new ones are better."
    author: "EMP3ROR"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "++"
    author: "Anon"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "I totally agree!\nThe previous was a lot better !!"
    author: "Cissou"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "I totally agree, but it seems the previous version is bigger and more complicated for pcs to render. This means that an old computer would be slower since the icon is shown almost everywhere.\n@ Nuno: am I right or is there another explanation?"
    author: "Jonathan"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "The icons are rendered to raster images for the most commonly used sizes ( 16, 22, 32, 64, 128 and in some cases 256 pixels on each side ) and stored in PNG format on disk, so there shouldn't be any difference in the rendering cost.  File size will vary slightly depending on the image data.\n\nComplex vector graphics do take longer to render than simple ones, but rendering from the vector 'source' only needs to be done if the icon is required in a non-standard size."
    author: "Robert Knight"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "i love it that you oxygen guys make all folders look like folders by default (death to the 'house' on /home/$USERNAME)..\n\n(ohh, and i kind of agree that the current folder icon it not easily distinguished as such)"
    author: "cies breijs"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "I second that."
    author: "Iuri Fiedoruk"
  - subject: "Re: Oxygen"
    date: 2007-06-04
    body: "Will there be a way of coloring the icons according to the color theme? \n\nI don't think so since icons'd only habe to be recolored in part (i.e. shadows keep their color). This means there would either need to be specific areas that are recolored, declared inside of the svg files or software that detects colors and their tones and recolors them (which also means that a blue would be recolored even though you might not want it).\n\nIs this a plan or should I write it on the wishlist for KDE 5?"
    author: "Jonathan"
  - subject: "Re: Oxygen"
    date: 2007-06-05
    body: "Have you tried KControl -> Appearance & Themes -> Icons -> Advanced -> Set Effect button -> Colorize?"
    author: "Jucato"
  - subject: "Re: Oxygen"
    date: 2007-06-05
    body: "I use the Oxygen Icons in KDE 3, they look great, the folder icon is wonderful, the old one looks crappy when you get back to it (I use to think like you).\n\nJust a KDE 4 dolphin screenshot:\n\nhttp://img230.imageshack.us/img230/3381/instantnea1ol5.png"
    author: "Luis"
  - subject: "Re: Oxygen"
    date: 2007-06-05
    body: "Thursday 08/Mar/2007\n\nhttp://dot.kde.org/1173332156/1173385442/\n\n;-)"
    author: "Artem S. Tashkinov"
  - subject: ":-)"
    date: 2007-06-03
    body: "great videos, i'm looking forward to kde4 and plasma :-)"
    author: "Beat Wolf"
  - subject: "Very informative screencasts"
    date: 2007-06-03
    body: "Thanks to all the people who keep the KDE community informed of what is going on developmentwise. The screencasts are very interesting. Even though the only plasmoid is still the clock, Aaron explains quite a lot about the inner workings, which should really shut down those who still believed that Plasma was all talk and no code.\n\nThings are really getting into shape, it seems. Way to go!"
    author: "Patentia A. Waite"
  - subject: "Re: Very informative screencasts"
    date: 2007-06-04
    body: "You just gotta love the cheesy outro music..."
    author: "mata_svada"
  - subject: "Re: Very informative screencasts"
    date: 2007-06-04
    body: "oh, just wait until next week. i've found even better audio and i think i'll have some tunes on the intro, too ;)"
    author: "Aaron Seigo"
  - subject: "Re: Very informative screencasts"
    date: 2007-06-04
    body: "aaahh.....cheesy music, the backbone of amateur video."
    author: "reihal"
  - subject: "\"back\" and \"forward\" icons"
    date: 2007-06-03
    body: "I've been wondering about the \"back\" and \"forward\" oxygen icons.  Are these the final set?  I find that the small back triangle disappears on me and becomes hard to find.  I remember seeing some updated arrows in nuno's blog, are those going to filter in eventually?"
    author: "Vlad"
  - subject: "Re: \"back\" and \"forward\" icons"
    date: 2007-06-04
    body: "I liked the arrows in here: http://img120.imageshack.us/my.php?image=snapshot5ti2.png"
    author: "EMP3ROR"
  - subject: "Re: \"back\" and \"forward\" icons"
    date: 2007-06-04
    body: "Tack to round borders. Uuh, no thanks. ;)"
    author: "Lans"
  - subject: "Re: \"back\" and \"forward\" icons"
    date: 2007-06-04
    body: "Edit: How did I press 'T' instead of 'B'? Oh well, for those who hasn't figured it out, it should be \"Back\", no \"Tack\" (which means thanks in Swedish. ;)"
    author: "Lans"
  - subject: "Re: \"back\" and \"forward\" icons"
    date: 2007-06-04
    body: "yeah, I think those rule."
    author: "superstoned"
  - subject: "Re: \"back\" and \"forward\" icons"
    date: 2007-06-07
    body: "argh, no, i didn't like those ones. i prefer simple black arrows/triangles."
    author: "Benoit Jacob"
  - subject: "Re: \"back\" and \"forward\" icons"
    date: 2007-06-05
    body: "I love the current ones, when I first look the Oxygen Icons, those icons were the ones that capture me.\n\nThey problem, only problem, is that they just don't work whit dark color schemes. \n\nI didn't, and I don't like those arrows you mention. But, hey, is just one man opinion."
    author: "Luis"
  - subject: "Re: \"back\" and \"forward\" icons"
    date: 2007-06-06
    body: "perhaps it really is that everyone has their own opinion, but even if the icons stay the same, I think that they should at least be larger. Currently they are much smaller than the average size of all the other icons."
    author: "Vlad"
  - subject: "Re: \"back\" and \"forward\" icons"
    date: 2007-06-09
    body: "I agree. Imho, the icons look fine. But usability-wise, they're not that good. Dark colorschemes don't work with them, and they're pretty small."
    author: "superstoned"
  - subject: "korganzier"
    date: 2007-06-04
    body: "I'm really happy to see work being put into KOrganizer. Its quite a useful piece of software, the calendar-presentation has just been very very ugly for a long time. Just look at how iCal or Outlook draw a calendars, its much clearer and visually more appealing.\n\nFinally some great dude starts to hack on this, and what does he come up with? \"Picture of the Day\" and \"This Day in History\" plugins. I personally find that to be utterly useless. I can already see my mom and our company's secretary asking \"why is there a taiwanese aborigine on top of my monday?\". I would be unable to explain.\n\nNow I see that I'm complaining about the work someone else does for free. And that sucks. So, I hereby rephrase this comment to:\n\nI am surprised by the way you set your priorities, but hey, have fun!"
    author: "me"
  - subject: "Re: korganzier"
    date: 2007-06-04
    body: "Actually, most people here program for fun. And what is more funny than seeing some secretary asking \"why is there a taiwanese aborigine on top of my monday?\" :)))))."
    author: "zonk"
  - subject: "Re: korganzier"
    date: 2007-06-04
    body: "Yeah, because what you find to be utterly useless is exactly what everyone else thinks too. I'm sure the developer was thinking exactly that when he developed these plugins.\n\nSeriously, your complaints are stupid. I could easily imagine that these plugins would be disabled by default or otherwise they will be easy to disable. "
    author: "Joergen Ramskov"
  - subject: "Re: korganzier"
    date: 2007-06-04
    body: "As you are probably well aware, people tend to work on stuff they like, they find interesting and they dare to take on. Why not look at this effort as a way to get aquinted with the KOrganizer code base, and that could potentially lead to input in other - perhaps in your eyes more usefull - areas? I know I started working on a KDE program in much the same way: try your hand at a small item, and end up working all over the codebase."
    author: "Andr\u00e9"
  - subject: "Re: korganzier"
    date: 2007-06-04
    body: "\"Picture of the Day\" and \"This Day in History\" plugins. I personally find that to be utterly useless.\" \n\nI think its a good idea if there is enough screen real estate for it.\n\nThat could possibly be replaced with corporate information such as the \"positive mantra of the day\" or \"here is a pic of the IT Support guy who you'd like to kick\" or \"today is the day you should ......\""
    author: "Ian"
  - subject: "Re: korganzier"
    date: 2007-06-04
    body: "I like the idea. You might even be able to show them on your desktop using plasmoids, which would be funny."
    author: "Jonathan"
  - subject: "Re: korganzier"
    date: 2007-06-05
    body: "I'm the guy doing utterly useless work :)\n\nI'm working on this for a Summer of Code project, whose goal is to improve KOrganizer's theme support and write some example plugins (see http://techbase.kde.org/Projects/Summer_of_Code/2007/Projects/KOrganizer_theming_improvements); you'll get more info on this on my blog: http://blog.loic.corbasson.fr/\n\nNow, on the \"ugliness\" of the calendar, I do not share your opinion: while certainly not perfect, I think KOrganizer does a quite good job. Could you please elaborate on the precise things you'd like to see improved? I do not have much time (in France, the spring semester ends in the end of June, so now is projects' ending and finals' preparation time), and no Outlook or Mac handy, so this would help me grasp what you mean. Even though it's outside the scope of what I have to do for Summer of Code, I'll have a look at what I could improve when working on the CalendarTheme interface in July if it's really that ugly :)"
    author: "Lo\u00efc Corbasson"
  - subject: "Re: korganzier"
    date: 2007-06-09
    body: "Imho, the calendar looks fine, but I don't know any better either. I DO know the MS outlook one is nothing better. Maybe some gradients or something, smooth, rounded corners... Sounds doable with Qt4/KDE4.\n\nBTW I applaud your offer to work on this! I think the parent really should answer your questions. And nice work, I DO like the plugins a lot ;-)"
    author: "superstoned"
  - subject: "close icon in kwite"
    date: 2007-06-04
    body: "In the video you see kwrite, and there is a close button. But it looks like a \"at\" char on a piece of paper, and the first thing I thought was this is \"email the file\"."
    author: "pascal"
  - subject: "Icon (and visuals) Inconsistencies"
    date: 2007-06-04
    body: "Hey, just something I noticed when watching Aaron's videos.  The centre of the clock has shine on the top, while the outside of the clock has shine on the top left. For consistency I think the Icons and visuals should be checked so that all shadows, and light sources are presented standard (from the same direction).  Example, if the light source is in the top left, then the shiny part should be on the top left and the shadows on the bottom right. This should be preserved in all icons/visuals (all icons should have the light source from the same direction and present shadows accordingly). \n\nJust a suggestion. Cheers.\n\nLJ"
    author: "LJ"
  - subject: "Re: Icon (and visuals) Inconsistencies"
    date: 2007-06-04
    body: "LJ that is a very valid point. In that context I quickly want to make another one, particularly about the new Oxygen icons. First of all, they mostly look real kewl and I am certain they will be perfect on release. Right now a huge problem I have with these though (as f.e. with the folder icon) is common to most if not all Oxygen icons: *MISSING CONTRAST*!!! There is no surrounding conture around them to clearly distinguish them from the pixel jungle they're supposed to live on. Look: http://davigno.oxygen-icons.org/?p=43\ndavigno has created some nice split view icons, and while I really like them, they are surely hard to see on a light background.\nMaybe one solution would be to have all those icons include a proper conture and color that matching to the kde color scheme so enough contrast and consistent looks can be achieved? Not an easy issue here it seems..."
    author: "eMPee"
  - subject: "Re: Icon (and visuals) Inconsistencies"
    date: 2007-06-04
    body: "\"The centre of the clock has shine on the top, while the outside of the clock has shine on the top left.\"\n\nyep. that's why i pointed out that the artwork is not final, so people wouldn't get too concerned about it =)\n\nbtw, if you'd like to contribute a clock face, please don't hesitate! i'm getting tired of looking at that same clock day after day, especially since i'm often looking specifically at it ... details on the svg can be found here:\n\nhttp://techbase.kde.org/Projects/Plasma/Theme#Current_Theme_Elements"
    author: "Aaron Seigo"
  - subject: "Re: Icon (and visuals) Inconsistencies"
    date: 2007-06-13
    body: "Cool, I leave for home in a week and will have about a week off.  Will look into making a clock face then. Thanks for the link Aaron.\n\nCheers.\n\nLJ"
    author: "LJ"
  - subject: "Thank you!"
    date: 2007-06-04
    body: "Only to say a big THANKS to all the developers for their great work! I've been using KDE for 4 years and I'll stick with it!\nCheers,\nMeo"
    author: "Meo"
  - subject: "Re: Thank you!"
    date: 2007-06-04
    body: "oh my god!! a tourist from the past!!!"
    author: "noone"
  - subject: "Very Nice"
    date: 2007-06-04
    body: "The Plasma video was a good \"shut your mouth up\" for me, I'm totally sold now :)\nThe Plasma idea is very good indeed, and I was really worried when I saw a clock, because I tought it would be just a replacement for Karamba, but looks like I was wrong.\nBut I still don't see how apps will integrate Plasma, at least not for a long time."
    author: "Iuri Fiedoruk"
  - subject: "Re: Very Nice"
    date: 2007-06-04
    body: "> I still don't see how apps will integrate Plasma\n\nwe can't let all the sekrit plans out all at once, right? ;) seriously though, i have plans for this that should be pretty interesting. i'm hoping to get at least some of them done in time for 4.0. i'll screencast that stuff when it becomes available in any case."
    author: "Aaron Seigo"
  - subject: "Re: Very Nice"
    date: 2007-06-05
    body: "Are changes in the apps required or could the DataEngine \"simply\" be a wrapper app around DBUS functions (provided, of course, that the apps in question already export their data to dbus)? I'm trying to think of data that would need to be exported otherwise but couldn't find any reason...\n\nThinking further, would it be possible to have \"really generic\" DataEngine, like a DBUS one, that would basically let a plasmoid connect to any DBUS interface you wish?\n\nCurrently, I don't understand the need for the cia engine; why couldn't there just be a configurable one simply called \"rss\"? I guess that's the final plan?\n\nThe screencasts are great. I hope more and more developers will take the time to showcase features this way... (Voice-over is a nice touch but I can totally understand mute one, especially for non-english people). Especially on mondays where compiling is... harder, it's nice to see new stuff, like Kwin effects, KPluginSelector in action, and plasmoids.\n\nFinally I must say you really begged my curiosity with the \"Media Center\" form factor (!!!) Can't wait to see what comes out of it. :)"
    author: "NamShub"
  - subject: "how do u pronouce kde !!"
    date: 2007-06-04
    body: "i  just saw the screencast made by seigo, but i get confused do u pronouce kde like he did, or it is a canadian accent.\n"
    author: "djouallah mimoune"
  - subject: "Re: how do u pronouce kde !!"
    date: 2007-06-04
    body: "Most people would pronounce it like that if they were talking in a hurry.\nKay Dee Ee collapses into Kay Deeee."
    author: "logixoul"
  - subject: "Re: how do u pronouce kde !!"
    date: 2007-06-04
    body: "in north america we tend to pronounce it \"kay-dee-ee\" but it's highly regional. in germany it's something like \"kah-duh-ee\"; in brazil it seems to be \"kahdee\" ... \n\nand yeah, i have a bit of a messed up accent. comes from having spent half my youth in various small towns on the west coast of canada, half of it in hawaii and all of it being a bit odd to start with ;)"
    author: "Aaron Seigo"
  - subject: "Re: how do u pronouce kde !!"
    date: 2007-06-04
    body: "ok thanks for the clarification, but don't worry u have a cool accent( at least i can understand it ) "
    author: "djouallah mimoune"
  - subject: "Re: how do u pronouce kde !!"
    date: 2007-06-06
    body: "You could be shocked hearing how's pronunced in italy :P"
    author: "RockMan"
  - subject: "Re: how do u pronouce kde !!"
    date: 2007-06-06
    body: "please how do u pronouce it !!!!!!!!!!!!!!!!"
    author: "djouallah mimoune"
  - subject: "Re: how do u pronouce kde !!"
    date: 2007-06-06
    body: "kappa-dih-eh (or something similiar :P)"
    author: "Vide"
  - subject: "Klickety"
    date: 2007-06-04
    body: "Hey!\n\nIs Klickety still on the \"Not Gonna Be In KDE4\" list?  Just wondering if it's still hanging from the Gallows Pole or not.\n\nThanx!\nM.\n"
    author: "Xanadu"
  - subject: "Yay Rockbox!"
    date: 2007-06-05
    body: "Small suggestion for the Rockbox portion of Amarok: would it be possible to include some sort of feature that could automatically update your copy of Rockbox?  They have daily snapshots, and it's in constant development, so that would be useful to have."
    author: "Matt"
---
In <a href="http://commit-digest.org/issues/2007-06-03/">this week's KDE Commit-Digest</a>: Start of the <a href="http://oxygen-icons.org/">Oxygen</a> Meeting in Milan, with a <a href="http://davigno.oxygen-icons.org/?p=42">focus on the Oxygen widget style and window decoration</a>. Continued developments in <a href="http://plasma.kde.org/">Plasma</a>, with the addition of a second example Plasmoid, for accessing developer commit feeds. More work in <a href="http://konsole.kde.org/">Konsole</a>, with the addition of a command-line tool to manage Konsole user profiles. Support for RockBox-based devices in <a href="http://amarok.kde.org/">Amarok</a>. Initial work begins on a <a href="http://wikipedia.org/">Wikipedia</a>-based "Picture of the Day" and "This Day in History" plugins for <a href="http://korganizer.kde.org/">KOrganizer</a>. Work begins strongly on the KRDC Summer Of Code project. <a href="http://edu.kde.org/marble/">Marble</a> becomes a library, with a plugin for Qt Designer created to allow application authors to include mapping functionality. Many module changes: Marble and KAlgebra move to kdeedu, KJumpingCube, KSudoku and <a href="http://kdebovo.googlepages.com/">Bovo</a> move to kdegames, in accordance with KDE 4 development timelines.

<!--break-->
