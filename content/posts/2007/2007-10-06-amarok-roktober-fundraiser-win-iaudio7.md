---
title: "Amarok Roktober Fundraiser, Win an iAudio7"
date:    2007-10-06
authors:
  - "lpintscher"
slug:    amarok-roktober-fundraiser-win-iaudio7
comments:
  - subject: "Nice"
    date: 2007-10-06
    body: "Just paid 10 euros. Amarok is a good program that I use a lot.\nI wonder if there is a general KDE donation project somewhere? \n\n-- \nCheers"
    author: "Absalon"
  - subject: "Re: Nice"
    date: 2007-10-06
    body: "http://www.kde.org/support/support.php\n\nJust google for 'donate to kde' and klik the first link to come...\nAnd you have your answer."
    author: "kollum"
  - subject: "Re: Nice"
    date: 2007-10-06
    body: "Thanks. Just donated to the KDE project too.\n\n-- \nCheers"
    author: "Absalon"
  - subject: "bank account?"
    date: 2007-10-06
    body: "Hi! I'd like to sent some money, but detest paypal. Could you please post a  bank account (either german or including those IBAN/SWIFT numbers), where I can send some money to?"
    author: "anonymous coward"
  - subject: "Re: bank account?"
    date: 2007-10-06
    body: "http://www.kde.org/support/support.php\n\nThere is a IBAN/BIC there. Within Europe, the bank transfers are generally gratis (free). I see that Mirko is still listed there as the treasurer. That's not right, it's not Cornelius. I'll add to my \"fix it\" list."
    author: "Adriaan de Groot"
  - subject: "Re: bank account?"
    date: 2007-10-06
    body: "Thanks for the info, Adriaan!\n\nI put \"amarok\" as \"verwendungszweck\", I hope they'll put it into the right hans ;)"
    author: "anonymous coward"
  - subject: "Re: bank account?"
    date: 2007-10-06
    body: "ermm, \"hands\" was the word I would have liked to use."
    author: "anonymous coward"
  - subject: "Re: bank account?"
    date: 2007-10-06
    body: "Who's hans, some german guy? 8=O"
    author: "Philipp G"
  - subject: "Re: bank account?"
    date: 2007-10-09
    body: "If you donate via IBAN from UK, It will cost about another \u00a312.50, \u00a310 for your bank to fax (yes fax) to European account, \u00a32.50 fee for receiving account.\n\nI don't like paypal, what about getting Google checkout?  "
    author: "Gerry"
  - subject: "Re: bank account?"
    date: 2007-10-07
    body: "Just email donations@getamarok.com for info."
    author: "Sven Krohlas"
  - subject: "Paypal"
    date: 2007-10-06
    body: "I've decided: instead of saying 'I really should donate...' I'm going to start giving money.\n\nSo as soon as Paypal get their arses in gear expect \u00a320 or whatever from me :)"
    author: "Anders Larsson"
  - subject: "Re: Paypal"
    date: 2007-10-06
    body: "...and not because of the prize. :P\n\nBut I always assumed Paypal needed a credit card. If this isn't the case then I can get an account no problem.\n\nSo yay."
    author: "Anders Larsson"
  - subject: "Re: Paypal"
    date: 2007-10-06
    body: "I use paypal just fine using EFT (Electronic Funds Transfer) which is linked directly to my bank account.  Payments are not instantaneous this way, but it works for me (in Soviet Canuckistan anyway)"
    author: "Troy Unrau"
  - subject: "me don't like Paypal too, but..."
    date: 2007-10-06
    body: "Amarok is worth it, don't you think so?\nBut please find another good way for donating. Why not simple bank transfer?\n\nAnyway, keep on rokin'!"
    author: "Torsten Dorschel"
  - subject: "I used PayPal"
    date: 2007-10-06
    body: "I Have a PayPal Account and it made donating very easy. It also works with your normal Bank account, you don't need a Credit Card"
    author: "David"
  - subject: "i'd love to put $$ into your bank"
    date: 2007-10-14
    body: "Amarok is the best damn thing since... um, the last fine thing, whatever it was. \n\nI'd love to donate towards it's ongoing development, it seems only fair seeing my next computer will allow me to keep my current one as a dedicated Amarok machine.\n\nSo what was so special about sliced bread anyway?"
    author: "personthingy"
---
It is <a href="http://amarok.kde.org/en/node/245">Roktober time again</a> at the Amarok Project and they are giving away an <a href="http://www.cowonamerica.com/products/iaudio/7/">iAudio7</a> mobile music player to encourage donations. Roktober is the time to review the events of the past 12 months, start planning and do the fundraising for the next year. If you do not have the time to actively help with the project this is your call, for each &#8364;10 donated you will be given an entry into the prize draw.  Amarok funds are spent on the webserver, travel &amp; accommodation for developer meetings and event staff expenses. It has never been easier to help keep Amarok rockin'.



<!--break-->
