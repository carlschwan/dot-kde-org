---
title: "KDE Commit-Digest for 26th August 2007"
date:    2007-08-27
authors:
  - "dallen"
slug:    kde-commit-digest-26th-august-2007
comments:
  - subject: "tabs in dolphin?"
    date: 2007-08-27
    body: "so dolphin won't have tabs?\nI find splitting a little too geeky for the noobs."
    author: "Patcito"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-27
    body: "Have you seen average persons browsing the web? Tabs are overkill for them. Want tabs? Use Konqueror. Don't add even more useless features (*) to Dolphin.\n\n(* useless for average people. Geeks can use Konqueror/Krusade.)"
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-27
    body: "I usually have more than 2 or 3 directories open. In that case tabs seems better than splitting the screen in 4 parts. So I guess I'll need 3 or 4 instances of dolphin running at the same time, that seems overkill to me. Anyway, I'll probably write the patch myself when I have time."
    author: "Patcito"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-28
    body: "No other filebrowser on earth has tabs. Well, OK, some do - but none which are used by normal people ;-)\n\nThe idea of split screen is to drag'n'drop between two locations (often offline and online, or a removable device and harddisk). That's something which a normal user now and then has to do. Tabs are only useful if you have to do some serious filemanagement - and how does, besides some geeks like us? So just use Konqueror, as dolphin won't be cluttered with tabs.\n\nLook - I do agree tabs are useful, I will use konqi and not Dolphin just for that. But they are NOT useful for average users, so let's not waste time on them, OK? Dolphin will be embedded in Konqi anyway, so it's not like you loose out."
    author: "jospoortvliet"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-29
    body: "USELESS? Its damned not useless! Look: If I am not aware of tabs, or I don't want to use them then I don't use them! But I want to use tabs in Dolphin! And it makes me really angry, that some stupid people only think of stupid users and don't implement a feature that will add a few kbytes to the ~500 mbyte KDE 4!\n\nKDE is not for \"average people\" as you describe them. \"Average poeple\" (including you) can use GNOME, that crippled piece of shit, and there they have Nautilus, a bad copy of the Windows explorer, which nearly hasn't got *any* features!"
    author: "blueget"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-31
    body: "Please, take a pill and knock back a tall cool one.  Konqueror is still totally viable, unless I'm missing something crucial here.\n\nThough you definitely have a point- KDE isn't for the \"average\" user (the average user characteristically using Windows and Explorer, or Ubuntu/fad-distro-of-the-month with Gnome usually), and I highly doubt that that will be changing for the next couple of years."
    author: "Wyatt"
  - subject: "Re: tabs in dolphin?"
    date: 2007-09-02
    body: "KDE4 is very much going to be for the average user. You should read up on the developer blogs and the changes being made to KDE apps in terms of design - it's all to accomodate new users - the average user, so to speak. If KDE4 was going to cater to the same, geeky and frankly narrow-minded crowd - they wouldn't be making Dolphin as the default manager (to which people still object, even though Konqueror is still going to be there). It's all a change for the better, if people are so afraid of change (and those too for the better) then they should stick to the current releases."
    author: "Katobatao"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-27
    body: "Tabs seem way geekier. Splitscreen is pretty useful, stuff like mightnight commander and how the windows versions are called are pretty popular - while Konqi is the only filemanager with tabs that I know off...\n\n(for myself, I won't use dolphin anyway, love konqi - but I can see that for average users, tabs are just useless bloat)."
    author: "jospoortvliet"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-27
    body: "My mother, father and sister have been using Firefox for a couple of years now and did not even notice tabs in that period! And even once I explained it, they are not really using them. My brother uses them I think, but he's a civil engineer, and I had to explain it to him once before he started using them."
    author: "Darkelve"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-27
    body: "I find tabs extremely useful for web browsing - it makes flicking between pages very easy - but much less useful for file management, as it's a pain to drag files from one tab to another.  In this situation split-pane seems to work best."
    author: "Adrian Baugh"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-28
    body: "Well, I love tabs for filebrowsing, but mom and dad don't even use them for webbrowsing - so indeed, no tabs in a normal filemanager is the way to go. Most of our competition doesn't even have split screen..."
    author: "jospoortvliet"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-27
    body: "I think that the tab issue is no problem. Dolphin can be used embedded in konqueror, and then you can use the konqueror tabs to have several instances open in tabs at the same time."
    author: "Yortx"
  - subject: "Sounds Gnomish..."
    date: 2007-08-28
    body: "Our users are too stupid to use/understand tabs, so let's remove that choice for them.  I'm sure they'll appreciate how we patronize them."
    author: "T. J. Brumfield"
  - subject: "Re: Sounds Gnomish..."
    date: 2007-08-28
    body: "I agree. Plus if they don't want to use tabs, then they don't have to. But why punish people that want to use them? If we started removing every single part of kde that noobs won't use or won't understand, we might end up with something that look very much like gnome. Plus dolphin, has support for ssh and ftps (which is great!), but I think there are way more people that would enjoy tabs then enjoy ssh and ftps."
    author: "Patcito"
  - subject: "Re: Sounds Gnomish..."
    date: 2007-08-28
    body: "We have konqueror. Adding options and UI parts just to accommodate the 0.1% powerusers who insist on using dolphin instead of Konqi is just plain stupid."
    author: "jospoortvliet"
  - subject: "Re: Sounds Gnomish..."
    date: 2007-08-28
    body: "a tab is just a simple qt widget, it doesn't consume any memory when you don't use them and adding them to the code should be a one liner. And I don't think only 0.1% use them. I want to use dolphin because it will have great nepomuk/strigi integration that konqueror probably won't have and I'm sure many power users will do the same."
    author: "Patcito"
  - subject: "Re: Sounds Gnomish..."
    date: 2007-08-30
    body: "The moment you make Dolphin the default and then remove options from users because they are too stupid to understand them, you are in fact stealing plays from Gnome's playbook.\n\nI don't have a problem on the whole with a dedicated file manager being the default file manager.  I do have a problem with not being able to configure that file manager to operate how I want it.  I consider this a massive step backwards in design philosophy.\n\nIn use, it is a minor inconvienence since I can fire up Konqui, however it is the principle that really irks me.  I expected more from the KDE team."
    author: "T. J. Brumfield"
  - subject: "Re: Sounds Gnomish..."
    date: 2007-08-29
    body: "There's a difference. As Aaron Seigo once said in a comment, Dolphin is for *simple uses* not simple (stupid?) users. Dolphin is meant to be a no-frills, simple, dedicated file manager. Simple enough for basic file management uses, but still powerful by taking advantage of KDE technology. And judging by how some (less vocal, less heard) users have grown to like Dolphin even on the older KDE 3.5.x version, then I'd say it succeeded in its purpose.\n\nAs for me... I'll still be sticking to Konqi in KDE 4. My use cases are never simple. :P"
    author: "Jucato"
  - subject: "Re: tabs in dolphin?"
    date: 2007-08-30
    body: "Couldn't you just add the tab support, but let people decide how THEY want to use the software? That way everybody would be happy."
    author: "Incd"
  - subject: "Re: tabs in dolphin?"
    date: 2007-09-01
    body: "I agree. A dedicated file manager is a good idea, I'm sure I would use it if I could and if it was better for my purpose.\n\nCouldn't the developers simply add a configuration point like \"Display mode\" where you can choose between \"Split view (default)\" and \"Tabs\".\n\nAbout me: I use Konqi for normal working and Yakuake for complex tasks (e.g. the great rename command). I'm using tabs, for copying rather Ctrl-C/V than Drag/Drop."
    author: "Stefan"
  - subject: "No statistic ?"
    date: 2007-08-27
    body: "Why is there no statistic at all in this digest ?"
    author: "Anonymous"
  - subject: "Re: No statistic ?"
    date: 2007-08-27
    body: "You should go and read http://commit-digest.org/ if you want statistics."
    author: "extropy"
  - subject: "Re: No statistic ?"
    date: 2007-08-27
    body: "I missed your point, the commit-digest.org post has no statistics, not this one."
    author: "extropy"
  - subject: "Re: No statistic ?"
    date: 2007-08-27
    body: "well, it's rihgt that http://commit-digest.org/issues/2007-08-26/ doesn't show statistics as usual :\nbug killer, buzz, commit country etc ...\n\nThere's \"only\" the sumary, the interview, and the list of commits."
    author: "kollum"
  - subject: "Re: No statistic ?"
    date: 2007-08-27
    body: "Fixed now,\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Mixer"
    date: 2007-08-27
    body: "I scanned the digest for more info on the removed mixer from Phonon, but I could not find more than what's already in this article. Does anyone know why this step was made? I find a mixer to be a crusial part of the ability to play sound in a desktop system. Is there a place to find some more info on this?"
    author: "Andr\u00e9"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "\"No mixer functionality in Phonon for KDE 4.0\" doesn't necessarily mean the functionality was there before. Could it be that this was a planned feature that didn't make it in time?"
    author: "anonymous coward"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "It might be that whilst phonon cannot mix the music, because it is really (on linux at least) a layer between the application and alsa, sound will still be mixed by alsa.\nThis would just mean that rather than, say 3 audio streams being sent to phonon, being mixed into one and that is sent to alsa, the 3 that phonon receives are sent separately to asla, where they are mixed.\nI'm no expert however, and may be completely wrong."
    author: "Jack"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "Unless I'm misremembering project names, Phonon's whole point is to be an abstraction layer so you can have different underlying player engines, rather than committing to one.  Also, the engines are like gstreamer, xine, rather than alsa/oss/etc?\n\nAll that said, I don't think this will be a big deal.  I'm sure they'll figure something out.  I'm more worried about WHY it's not going to be there.  Is there some sort of fundamental design problem that needs to be fixed, and will take time?  Or is it just more work to implement a phonon mixer than most of us would imagine?"
    author: "Lee"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "Well, you see... theres this thing called a code freeze, and it has occured.. When there is only one person working on a given subsystem, as is the case with phonon, there tends not to be enough time to accomplish everything, and therefore some things need to be cut."
    author: "Dan"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "Ouch.  So you're saying that there wasn't enough time to complete the mixer before the feature freeze for 4.0, eh?  Well, there's always 4.1, right?"
    author: "Matt"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "Indeed, and planned just 6 months after 4.0 ;-)"
    author: "jospoortvliet"
  - subject: "Re: Mixer"
    date: 2007-08-28
    body: "If you don't have ALSA mixing properly use PulseAudio. Phonon will make it easy to set all you're apps to use Pulse so it dose its job. "
    author: "Ben"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "let me define what mixer will not be in 4.0:\n- There is a hardware mixer in some soundcards (no way to not support it ;-) )\n- There's a software mixer provided by dmix (will be supported)\n- There's the possibility to create multiple AudioOutput objects in one application, those outputs will use sw/hw mixing on the driver/soundcard\n- Last one: create two MediaObjects and connect them to one AudioOutput. That's the functionality that won't make it into 4.0. It's basically impossible to implement using xine anyway."
    author: "Matthias Kretz"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "To be honest I don't really get what you mean - can you give me a short use case, a short example of what will be possible and what not?\nWill I still be able to Mute my speakers while I have an incoming call on my USB-headset?"
    author: "liquidat"
  - subject: "Re: Mixer"
    date: 2007-08-27
    body: "It will make no difference for you as a KDE user. It only makes a difference for the developer using the Phonon API.\n\nYes, you are able to mute your speakers when you have an incoming call on your USB-headset."
    author: "Matthias Kretz"
  - subject: "Re: Mixer"
    date: 2007-08-28
    body: "Perfect, thanks for that explanation."
    author: "liquidat"
  - subject: "Re: Mixer"
    date: 2007-08-28
    body: "Thanks for your explanation."
    author: "Andr\u00e9"
  - subject: "New Icons - Status"
    date: 2007-08-27
    body: "I was wondering if all the new icons will be ready. the last time I saw a screenshot for the control center most of the icons were missing."
    author: "DITC"
  - subject: "Re: New Icons - Status"
    date: 2007-08-27
    body: "I am not sure if the situation is the same: In Kalzium I had the same issue and Pino simply moved a directory inside Kalziums directories, not the icons appear. That has something to do with the new file/directory naming in KDE4."
    author: "Carsten Niehaus"
  - subject: "KFind?"
    date: 2007-08-27
    body: "Dolphin will launch KFind for searching, but why not Strigi? Or is Strigi used as a backend for KFind?"
    author: "bsander"
  - subject: "Re: KFind?"
    date: 2007-08-27
    body: "I think it's not all ready yet, but that'll change."
    author: "jospoortvliet"
  - subject: "Re: KFind?"
    date: 2007-08-27
    body: "Not by default please, unless strigi changes a LOT for KDE 4.\n\nIt's not a dig against strigi itself, but for me personally, strigi-like apps will only be useful when they understand RDF and other semantic stuff that's generated in realtime, or when files enter the system.  If they're essentially just a modern locate, which build a huge DB in non-realtime, then I'd rather have a normal find-like search dialog.\n\nI know where my important files are.  The rest, I use so occasionally that I can wait for a moment to find/grep them.  For me, desktop searching will become useful when it creates a paradigm shift, from working with individual apps and documents to working with information and relationships."
    author: "Lee"
  - subject: "Re: KFind?"
    date: 2007-08-27
    body: "Strigi-technologies like deep-grep and deepfind could be used in kfind as well, improving on it in terms of speed and features (the deep* also search in archives etc). And strigi & Nepomuk are working pretty close, so I guess at least some semantic stuff should make it into KDE 4.0 - though the big stuff might be postponed to 4.1"
    author: "jospoortvliet"
  - subject: "Re: KFind?"
    date: 2007-08-27
    body: "I thought the semantic desktop was supposed to be a major feature/aspect of KDE 4.  Strigi works now, and has for a while, but it won't be included in KDE 4?\n\nThat just doesn't make any sense to me."
    author: "T. J. Brumfield"
  - subject: "Re: KFind?"
    date: 2007-08-27
    body: "yes it will, the digest even speaks of the sidebar in Dolphin using Nepomuk by default... Strigi is already deeply integrated, currently offering the features KDE 3 had (but faster and more thorough), and in the forseeable future, it'll do even more. Don't worry :D"
    author: "jospoortvliet"
  - subject: "Re: KFind?"
    date: 2007-08-27
    body: "Don't confuse KDE 4.0 with KDE 4."
    author: "Sutoka"
  - subject: "Re: KFind?"
    date: 2007-08-28
    body: "the \"semantic desktop\" is,first of all, buzz word ;)"
    author: "Andreas"
  - subject: "foo"
    date: 2007-08-27
    body: "Is KChart's output going to be a flake?\n\nP.S: Am I the only one thinking that the \"Title\"-field is superfluous?"
    author: "anonymous coward"
  - subject: "Re: foo"
    date: 2007-08-27
    body: "Short answer: Yes.\n\nLonger answer:  KChart 2 will provide both a Flake shape, a KPart for embedding in e.g. Konqueror or Dolphin and a stand-alone application."
    author: "Inge Wallin"
  - subject: "Re: foo"
    date: 2007-08-28
    body: "that makes three things, not the two you imply with both ;-)\n\nhehe\n\nPretty cool stuff, I must say..."
    author: "jospoortvliet"
  - subject: "Dolphin becomes Konqueror"
    date: 2007-08-27
    body: "Oh my god, it happens again. The KDE file manager becomes a big mess. Why does a file manager for common people need 2327423 view modes? Why can't the nerds who like split view just install Krusade? Why does Dolphin need columns view? To please those 5 people who use it in Mac OS X and have to use Linux on another PC?\n\nLook at Thunar. That's file manager for non-nerds."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-27
    body: "I don't think Dolphin was ever meant to be a file manager for people that don't know how to use a file manager.\nI thought it was meant to be a file manager that was _only_ a file manager, and therefore very good at being a file manager.\n\nAlso, I think people vastly under estimate the abilities of 'non nerds'.\n"
    author: "mabinogi"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-27
    body: "First we complain about Dolphin lacking features and being a Nautilus-clone (no offense). Now we complain that it's too complex.\n\nI too was skeptical first, but now I really like Dolphin. Might even use it as my default in KDE4 (sorry Konqueror)."
    author: "Lans"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-28
    body: "+1"
    author: "jospoortvliet"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-09-02
    body: "I hope only that it's possible to de-activate Dolphin and re-activate Konqueror as file manager.\nIf that should not be the fall, I think I will first try to work as long as possible with KDE3 and then I will give a chance to Gnome."
    author: "mcz"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-27
    body: "> Why does Dolphin need columns view?\n\nThe columns view is useful when you have deep hierarchies to browse through.  For example, I usually use the standard icon view in Dolphin, with files grouped by type.  When browsing my local KDE source tree however I use the columns view.  Dolphin remembers the view mode for each directory automatically, so usually it will be at the right view when I go into a directory, without me needing to change the view.\n\nI see trolling about Dolphin quite frequently on the dot, but I get the impression that this is from users who have not actually used the KDE 4 version of Dolphin.  My personal experience (as a 'power user') is that it works pretty well in practise and the attention to getting the UI right really shows in comparison to Konqueror.\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-27
    body: "Well, I was skeptic about Dolphin when it was first announced that it would replace Konqueror as the default file manager especially when comparing Dolphin3 and Konqueror3 but during the recent months my opinion about Dolphin4 turned pretty much by 180 degree (especially due to trying out the SVN version) and now I'm really looking forward to it. And that many improvements find their way into Konqueror via the Dolphin part is just the icing on the cake.\n\nGreat job!"
    author: "Erunno"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-27
    body: "How does optional viewing modes ruin the file manager for you?\n\nYou want a basic default view?  You got it.\n\nOther people want choice, and you think everyone should be limited solely to what you want?"
    author: "T. J. Brumfield"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-27
    body: "Yeah, that's right, I also can hardly understand why so many people say, feature XY won't be used by normal users, so it won't be implemented into Dolphin. A file manager should work exactly the way I want. Sure, the default config shouldn't confuse beginners, but it should be flexible, why not make a plugin-infrastructure like in firefox?\nI have about 50 plugins for Firefox and use every one of them (although only 10 of them are really important). \nSo, if I want to use tabs in file management, why are there so many people claiming, that it's of no use? For me it is (in contrast to the split feature). If I drag a file to a tab and its content is immediately shown, I can drop the file there or even drop it in a folder of that tab. So I don't need so many different file managers open all the time, because THAT is really annoying (especially when doing alt+tab or the cool compiz scale feature and there are so many windows of the same app)."
    author: "ezTol"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-28
    body: "That is what konqueror is. So go and use it (just like I will). And if you want even MORE, then there is Krusader.\n\nThe point of Dolphin is this:\n- years and years, people complained konqi was too complex.\n- the developers tried to remedy that\n- but it didn't work. Being so powerful inevitably meant SOME UI bloat. Things could be improved, but only to a certain point.\n\nSo, Dolphin was written to provide a basic fileMANAGER. Not browser (so no embedded views and such). Not with all the stuff, plugins and extensions and tabs Konqueror has. Just basic, usable, efficient.\n\nOK?\n\nSo, what you describe has been possible in KDE for 6 years. Drag'n'drop a text file in the empty tab bar in konqi, and it opens it embedded. Worked for years. But that's simply not what Dolphin is meant to be. So back off, use Konqi, dump firefox (slow, bloated and inefficient anyway, how dare you criticize KDE technology while using such inferior crap?) and be happy."
    author: "jospoortvliet"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-08-29
    body: "Firefox isn't all that bad, but I agree I wish it was better.  Theres a lot of things, like the downthemall extention, that konqi is badly missing. Anyway, how do you dare expect people to use kde stuff exclusively, when there is lots of good software out there?  Free software!\n\nBut thats beside the point.  If you look at konqi, there is a button for the babel fish extention even if file browsing mode.  Its useful, but it should be buried behind service menus and such.  Its clutter when its on the toolbars.  But thats no reason to get rid of it!  The same story with tabs."
    author: "Level 1"
  - subject: "Re: Dolphin becomes Konqueror"
    date: 2007-09-30
    body: "I like Konqueror and even after Kubuntu changed default file manager to Dolphin I'm still using Konqueror. It's just better. Especially the tabs. I do not need these left and right sides. As noted above there's Krusader already for split view."
    author: "akrus"
  - subject: "Minor thing..."
    date: 2007-08-27
    body: "Regarding the status bar that shows how much space is free (e.g. 215.5 GiB free), is there an option to make it simply 215.5 GB free? I thought GB was the standard abbreivation for gigabytes, but even if the other one is standard usage it would be nice to have either option. Apart from that, I think Dolphin looks fantastic!"
    author: "Jim Ramm"
  - subject: "Re: Minor thing..."
    date: 2007-08-27
    body: "> is there an option to make it simply 215.5 GB free?\n\nNo, but the change would be trivial to make.  I think GiB is the more 'correct' if you are being pedantic about it, but yes, KB/MB/GB are more standard."
    author: "Robert Knight"
  - subject: "Re: Minor thing..."
    date: 2007-08-27
    body: "GiB is actually sort-of a standard. There is a difference between GiB and GB. A GiB is 2^20 bytes, while a GB is 10^6 bytes. In the world of computers, with the exception of hard drive manufacturers, we are used to using the 2^n way of working, hence the GiB. This difference is one of the reasons your 500GB harddrive doesn't look like 500G(i)B in your OS. I think using GiB is the best way to go. "
    author: "Andr\u00e9"
  - subject: "Re: Minor thing..."
    date: 2007-08-28
    body: "It is SO not \"sort of\" a standard. It's more correct, but not used much. End of story.\n\nDon't be lame."
    author: "Joe"
  - subject: "Re: Minor thing..."
    date: 2007-08-27
    body: "Yeah. GiBi is more accurate than GB.\nLook here for reference http://en.wikipedia.org/wiki/Binary_prefix\n\nNot that it matters much to most people but when you try a (http://www.space.com/missionlaunches/launches/orbiter_errorupd_093099.htm) mars landing it is kind of important to pay attention to units and prefixes."
    author: "Oscar"
  - subject: "Re: Minor thing..."
    date: 2007-08-27
    body: "I like pushing the proper standard on people. Force them to use the right terminology, so they actually learn. If/when you add GB/MB/kB/TB, please make sure it fits the standard:\n\nTi,Gi,Mi,Ki -> powers of 2^10\nT, G, M, k -> powers of 10^3\n\nIt'll save those of us in the know a lot of confusion in the long run."
    author: "Soap"
  - subject: "Re: Minor thing..."
    date: 2007-08-27
    body: "Up until very recently GB (in the realm of computers) meant what GiB means now.  Probably if hard drive manufacturers hadn't insisted on trying to bend the existing de-facto computer standard GB would still mean GB."
    author: "Sutoka"
  - subject: "Re: Minor thing..."
    date: 2007-09-03
    body: "> Up until very recently GB (in the realm of computers) meant what GiB means now.\n> Probably if hard drive manufacturers hadn't insisted on trying to bend the\n> existing de-facto computer standard GB would still mean GB.\n\nExcept that this wasn't only the HDD maker: in the telecom industry a kilobauds is 1000b/s not 1024b/s.\n\nOverloading kilo and giga was a bad idea, so let's get rid of bad ideas, sure it's painful in the short term, but in the long term, it'll be better."
    author: "renoX"
  - subject: "Re: Minor thing..."
    date: 2007-08-27
    body: "AFAIK\n1 gigabyte=1 GB=10^9 bytes\n1 gibibyte=1 GiB=2^30 bytes"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Ncurses based kate interface?"
    date: 2007-08-27
    body: "Is there any change to use Kate with an ncurses interface? Or is there any other editor for those of use who don't want to use VI.\n\nWhen I start kate from the commandline just to edit a config file it takes too much time to start up and you always get these X error messages (don't know how to suppress them). \n\nX Error: BadDevice, invalid or uninitialized input device 169\n  Major opcode:  147\n  Minor opcode:  3\n  Resource id:  0x0\nFailed to open device\nX Error: BadDevice, invalid or uninitialized input device 169\n  Major opcode:  147\n  Minor opcode:  3\n  Resource id:  0x0\nFailed to open device\n\nI would really like to have a fast editor with syntax highlighting that compares to the good old times with TurboC like IDEs and has the functionality of Kate. Kate is fine but when I want to edit the commandline I need a simple editor in \"text mode\". \n\nSure, MC also has a built in editor."
    author: "bert"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "What's wrong in pico / nano?"
    author: "ZeD"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "link?"
    author: "bert"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "Nano is in the repository of every distribution I've used, it's installed by default in Debian and Debian-based distributions. Settings are in /etc/nanorc (don't forget to uncomment syntax highlighting)."
    author: "nano_user"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "yast2 -i nano\noder\napt-get update && apt-get install nano\noder\nurpmi nano\noder\nemerge nano\n\nich denke das wars (hoffe hab mich nicht geirrt ^^)\n\nprojekthomepages: http://www.nano-editor.org/\nhttp://picogui.org/\n\nmfg, Bernhard"
    author: "LordBernhard"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "apt-get install pico "
    author: "John Tapsell"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "I guess it's an Xserver config problem,\nfor a fast fix try:\n\n1) Backup your configuration: open a terminal and write:\n sudo cp -a /etc/X11/xorg.conf /etc/X11/xorg.conf.backup\n2) Edit /etc/X11/xorg.conf : open a terminal and write:\n kdesu kate /etc/X11/xorg.conf\n3) Comment out problematic entries. Replace:\n         InputDevice \"stylus\" \"SendCoreEvents\"\n         InputDevice \"cursor\" \"SendCoreEvents\"\n         InputDevice \"eraser\" \"SendCoreEvents\"\n\n"
    author: "Momo"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "compare them to kwrite/kate? Anyway, Ive been using MCedit for a long time, it's pretty nice and basic. Now I'm a vi user..."
    author: "jospoortvliet"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "That is Kubuntus fault, due to the nonexisting Wacom tablet device in xorg.conf"
    author: "Ljubomir"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "These errors are because your X.org file has tablet support, but you don't have a tablet. Remove those sections from xorg.conf and the errors disappear."
    author: "jospoortvliet"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "Simple answer: no, there is not. Kate is very much build agains all the (GUI) goodness that Qt and KDE have to offer. You can't expect the developers to split that all off again, especially since there are many, many excellent console-based text editors already (pico, nano, vi, emacs, etc...)\n"
    author: "Andr\u00e9"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-27
    body: "Also, for suppressing error messages, you can run a program like this: <i>program >/dev/null 2>&1</i>\n\nI recently whipped up a script to do that called \"stfu\" (attached)."
    author: "Matt"
  - subject: "Re: Ncurses based kate interface?"
    date: 2007-08-28
    body: "I guess you're using Kubuntu Feisty?  That's a well know problem with X being set up to have a wacom tablet when you don't, if you search the Ubuntu forums you'll find advice on how to edit xorg.conf to fix it.\n\nJohn."
    author: "odysseus"
  - subject: "Fastest editor in the wwwest"
    date: 2007-08-29
    body: "You probably don't need to install nano because it's probably already on board... try \"which nano\" in a shell to find out. I use this alias in my .bashrc file and I would suggest it's the fastest and simplest console based editor available... both to type and start, and also to save and quit...\n\n# alias e\nalias e='nano -w -t -x -c -R '\n\nso you type \"e filename\" to edit something (can't be any simpler than that) and then control-x to save and quit (I would struggle to suggest anything quicker and simpler than that too). \"nano --help\" for more command line suggestions and obviously \"man nano\" for an extended explanation of most commands. And here is an example of using a more complex nano based alias to provide a very simple shell based notes system which is handy for pasting special commands or any notes you may need later on...\n\n# alias note\nalias note='echo -e \"-- $(date) --\\n\\n\\n\\n$(< ~/notes)\" > ~/notes && e +3 ~/notes'\n\nIt creates a file called \"notes\" in your home directory so you can do something like \"head ~/notes\" to see the last couple of entries. I first created these 2 aliases nearly 10 years ago (pre-nano with pico) and still use them nearly every day.\n"
    author: "Mark Constable"
  - subject: "fte"
    date: 2007-08-30
    body: "Try FTE if you don't like vi etc.  My advice is learn vi, though --- I avoided it for years, but it only took me a few days to learn and prefer it for all my editing, once I really tried.  Now, if only the KDE-based vim was in debian :/"
    author: "Lee"
  - subject: "kicker still alive"
    date: 2007-08-27
    body: "I was exited in seeing the replacement of kicker this week as it was said the work began last week.\n\nIt seems to be much more work than a simple plasmoid, thought, gess we need to wait for a few weeks more.\n\nOh, and unfortunately, seeing the start of kchart make me feel a bit less atracted by KOffice, as I recently discovered Latex/Kile. Then I hope Kile could be integrated into KO as a WISIWIW text editor, side by side with the WISIWIG KWord for the other kind of people ;)\n\nOh, and digikam seems evolving quickly, it was not so long ago that the XMP support was started if I am not mistaken by the poll on the digikam blog. And that I waited for long :\n\nMarcel Wiesweg committed changes in /trunk/extragear/graphics/digikam/libs/database: \n   Add a preliminary implementation of a CollectionManager that supports multiple album roots and uses Solid for detecting if a particular path is available or not.\n\nWith that, I'dd hope it's possible to share albums on an (s)ftp/fish/smb/nfs share somewere, and more interesting, on flickr or picassa spaces.\n\na good week. congrats to everyone working on the BDE (best desktop ever)"
    author: "kollum"
  - subject: "Re: kicker still alive"
    date: 2007-08-27
    body: "\"With that, I'dd hope it's possible to share albums on an (s)ftp/fish/smb/nfs share somewere\"\n\nactually, first thing that came to my mind was an ability to do that with cd/dvd media...\n\nnow THAT would be pretty cool, if possible to do."
    author: "richlv"
  - subject: "Re: kicker still alive"
    date: 2007-08-27
    body: "Solid provides a UUID for storage volumes. If this can be used to uniquely identify CD/DVDs, we can support that. But things will evolve step by step."
    author: "Marcel Wiesweg"
  - subject: "Re: kicker still alive"
    date: 2007-08-27
    body: "What do you mean by WISIWIW? \nIf you like Latex, but would like a more WYSIWYG or WYMIWYG type interface, take a look at lyx. It is based on Latex, but offers a more graphical representation of your document.\n\n"
    author: "Andr\u00e9"
  - subject: "Re: kicker still alive"
    date: 2007-08-27
    body: "Well, one of the things I love with LaTeX is the quality of the doc produced.\nBut second to that is the WISIWIW thing :)\nSo LyX doesn't apeal to me.\n\nWhat I wanted to say is that beeing able to import Kcharts, or Kivios doc would be nice. Alow to work with Kspread to do 'text' tables ( I mean, without the latex $...$) but enjoying the formulas calculation.\n\n"
    author: "kollum"
  - subject: "Re: kicker still alive"
    date: 2007-08-27
    body: "This is what I do:\n\nUse KWord for WYSIWYG (what you see is what you get). Useful for quickly drafting documents with fairly complex formatting. eg. resumes (should be an accent on the 'e'), simple flyers, etc.\n\nUse LyX for WYSIWYM (what you see is what you mean). Useful for fairly short papers with consistent formatting. It's easier to depart from standard LaTeX formatting, like when you need to quickly set margins, or ensure that figure frames are drawn after the position you created them, etc. It also provides a nice visual for editing. (used for small school papers, etc.)\n\nUse Kile mainly for markup reference, but also for long papers, or ones with a template set up. It's not as good for stuff with a fast approaching deadline, but gives more control, and adds less stuff to the document markup.\n\nI absolutly LOVE entering math equations in LyX, because it has visualisation as you enter it. If I was working in Kile, I'd open up LyX, just to do the math and copy it over.\n\nWhat's great is that they're all KDE based technologies. Kile and LyX are also the best LaTeX based editors I've tried.\n\n---\n\nAs for KChart, I've been disappointed with it in the past, because it didn't have XY-scatter. I ended up using gnumeric, which had a very goodchart formatting tool as well. KChart is one of the projects I'd like to get involved in once I get more time and experience for programming.\n\nPS. Searching for Latex in google actually came up with the LaTeX home page as the first result."
    author: "Soap"
  - subject: "Re: kicker still alive"
    date: 2007-08-27
    body: "I forgot to mention that my understanding is the Kicker replacement is taking longer, because it's a container, not a plasmoid.\n\nPlasmoids (clock, weather, etc.) go into the containers (desktop, panel, media centre), and change their appearance accordingly. I would think it takes more work to set up a container than most plasmoids."
    author: "Soap"
  - subject: "Re: kicker still alive"
    date: 2007-08-27
    body: "KChart 2.0 will have scatter graphs.\n\nAnd it's a very good time to start helping out with KChart right now, since we're just starting to get into gear.  You should start by checking out the koffice module (trunk/koffice) and read the file koffice/kchart/DESIGN.  That should give you an overview. Then you can take a look at the very detailed TODO file and find something to work on."
    author: "Inge Wallin"
  - subject: "Re: kicker still alive"
    date: 2007-08-28
    body: "Unfortunately LyX does not use KDE technologies, just Qt."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: kicker still alive"
    date: 2007-08-30
    body: "I would like to suggest an idea for KDE 'start menu'. \n\nAfter I saw kroller, I think it can be a good start point for a 'start menu' replacement. If you press the KDE 'start menu' all the entries are icons and text, so why not to start another kroller bar with this icons when the KDE button is pressed ?\n\nI'm using Opensuse 10.2, the 'start menu' display \"Favorites\", \"History\", \"Computer\", \"Applications\", \"Leave\". The idea is to use an application like kroller with this entries and when one of the icons is clicked another bar, upper the initial, will display the icons for this selection, for example if I click \"Favorites\" the new bar will display \"Firefox\", \"Amarok\", \"Konsole\", \"Konqueror\" and \"OpenOffice\".\n\nWell it's just an idea."
    author: "anonymous"
  - subject: "kopete"
    date: 2007-08-27
    body: "kopete used to be one of the kde killer apps. where is it now? i see nearly no news on it's developement. sure, even in it's current state it's ok, but for many windows users a good msn implementation is one of THE killer features. currently webcam support is more than buggy, and audio doesnt work at all.\nI'm not complaining, i'm sure the developers are coding on kopete (things like telepathy integration etc), but there are no news on it. somebody knows more?"
    author: "Beat Wolf"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "The only thing I know is that it won't be ready for KDE 4.0 but only for 4.1 - and you're right, it feels a bit abandoned :("
    author: "jospoortvliet"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "kopete devs should have joined gaim a long time ago. a qt interface for gaim and *one* open-source messenger to rule them all would be great. but it seems the oss camp loves doing the same work 10 times instead of once... o.O"
    author: "fish"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "I dunno about Gaim, but the Kopete devs are working on Decibel/telepathy stuff, thus sharing all technical stuff over freedesktop.org - even better than what you suggest. Writing a Qt interface for Gaim, removing all GTK crap, would equate rewriting it - pretty useless. Sharing the underlying messenger stuff is much more efficient - and it's the road they are taking."
    author: "jospoortvliet"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "looking forward to a dot article about all this.\n\nAnd by the way, what does it mean that kopete wont be ready for kde 4.0? will there be no kopete at all? or kopete from 3.5? how does that work?"
    author: "Beat Wolf"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "You don't know GAIM architecture at all. There is no \"gtk crap\" to remove. Libpurple is pretty modular and is used by the cocoa Mac OSX app Adium."
    author: "t"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "Ouch! No kopete for 4.0?? Are you sure? That is very bad!!\nThese days IM is up there with email and web.\n"
    author: "Bxs"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "I find it hard to imagine there will be NO Kopete... probably just an older/not yet rewritten version."
    author: "Darkelve"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "You will be able to just use the KDE 3.5.x kopete version. And of course, I might be mistaken... (Let's hope so)"
    author: "jospoortvliet"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "BTW it's not like NOTHING is going on, it's also that the lower level technologies like Akonadi and Decibel are aiming for KDE 4.1, so I guess it just makes more sense to aim for that too. Remember we want to do a KDE 4.1 release in six months, so youll hopefully have KDE 4.1 with Kopete, Decibel, Akonadi and of course a lot more before the summer next year :D"
    author: "jospoortvliet"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "I'd really, *really* like to see the KDE Promo team start going into damage control mode and making damn sure that people know just how rough KDE4.0 is going to be, because otherwise it's going to be a PR disaster.  Especially given the ludicrous hype around e.g Plasma & Tenor from before those projects were even properly started.  I have every faith that the devs will pull off an excellent DE, platform and suite of apps in the med-long term, but the distinction between KDE4 and KDE4.0 is still completely lost on pretty much every user I've seen talking about it and really needs to be impressed on them: most are still thinking that KDE4.0 will be some absolutely incredible paradigm-shift that cures all of the problems associated with the KDE project so far, and they are going to be very disillusioned when they find out the real situation.\n\nGNOME has already gobbled up a huge share of the userbase according to recent polls, and I fear that having people judge the whole KDE4 series based on the buggy and incomplete KDE4.0 release will only exacerbate the situation :/  \n\n"
    author: "Anon"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "what would also be good is to name the areas were it will be difficult to meet the deadlines."
    author: "DITC"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "This is open source. One does not do \"PR damage control\". You want to know the state of development, you go look at the SVN. It is more important to impress developers and cutting-edge \"advanced\" users at this stage than anyone else. If the armchair pundits are unhappy, well, tough. \n\nSecond, KDE4 is a huge change, massive. It is a complete dev platform. It is cross-platform. But many KDE apps are not developed in the SVN, and for their developers, it would make no sense to make a release against an unreleased platform.\n\nThus, the goal of 4.0 is to a have a stable base so the migration of the community apps is possible and happens. But this means that KDE will be \"complete\" only around 4.2. This is fine, because if an (incomplete) 4.0 doesn't happen, the big porting effort will never take place. And as things go, 4.0, even if incomplete, may well be the coolest DE available on any platform :)"
    author: "hmmm"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "So what's wrong with a delay for 4.0. Nothing. IT's ready when it's ready. I say it again: First Impression matters!!! If 4.0 is going to miss major functionality from 3.5.x, then we should delay 4.0. Just my $0.02."
    author: "cossidhon"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "I've been trying to follow the release planning over the past months somewhat and back then when the current schedule was decided it seemed like the right way to get the developers into release mode and hammer KDE4 into a workable shape. Now with the release date coming closer I've also share other people's concern about the possible PR disaster that may occur should KDE 4.0 be less functional then KDE3, especially when missing common software like the IM. There's also still a lot of basic functionality missing like the taskbar and applets this close to the release date and I fear that it won't get tested enough before the final release.\n\nIt's totally possible that I'm talking out of my arse here due to not being aware of what's going on in developer land but would it not possibly be better to delay the release for a few months and have some additional Beta and RC releases instead? KDE 4.0 is a huge rewrite of the desktop and in my opinion this would warrant a longer initial testing phase. Plus, application developers might have a chance to finish porting some applications so that at least they are usable on a KDE3 level. A delay would probably be the lesser evil than the media and user backlash from which future releases will have to recover."
    author: "Erunno"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "KDE 4.0 will be advertised as being \"rough\" and most distro's are going to stick to 3.5.x by default but will let users try 4.0 if they wish. The \"incredible paradigm shift\" will begin with 4.0, but won't be completed for awhile."
    author: "Skeith"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "If it's \"rough\" it's called \"beta\" and never EVER released as a final product!"
    author: "Sergey"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "i would suggest to the people in this thread researching a bit more on open source release methodology. \"release early, release often\", \"betas don't get used\", \"release when it is ready, but also before it is ready\", etc..\n\nseems to be a lot of nerves in this thread sweating over what will happen in the future. the future is always a bit scary because it's rarely ever filled with absolute certainty. the first hint of success or failure is rarely the last word, let alone the middle word =)\n\nas for damage control, some people here are reacting to development code (that is leaps and bounds beyond where it was 6 months ago) as if it was the release itself, as if there was no public communication planned and as if we're releasing a proprietary product versus a community product.\n\nhowever, it is nice to see how much people do care."
    author: "Aaron J. Seigo"
  - subject: "Re: kopete"
    date: 2007-08-27
    body: "why not be honest to the users and call it \"technical preview\", then? Oh, and I was using KDE for at least as long as you are, so no need to spoon-feed me the \"release often\" philosophy cr@p, I'm quite familiar with the concept, thank you."
    author: "Sergey"
  - subject: "Re: kopete"
    date: 2007-08-28
    body: "But I'd like to note that any issue for negativity tends to pop up pretty often in KDE recently:\n\n- First it was Plasma;\n- Then Konq vs Dolphin;\n- Then \"KDE 4.0 will be sub-standard\".\n\nThink of $ISSUE and it will be brought up. Not that it's going to help the developers, translators, promo people, in any way...\n\nPersonally I'm just waiting to see the release, and judge on final code. "
    author: "Luca Beltrame"
  - subject: "Re: kopete"
    date: 2007-08-28
    body: "They are being honest. \n\nWho depends on a x.0 product for all their needs?\nGive me x.1 every time!\n\nAnd stop bitching, bitch."
    author: "Joe"
  - subject: "Re: kopete"
    date: 2007-08-28
    body: "I really don't like that tone of your's, it resembles more of a gnome-related forum :-(\nSo stop being hysterical and let people do their thing, it'll turn out just fine."
    author: "Philipp"
  - subject: "Re: kopete"
    date: 2007-08-30
    body: "Have to agree with you.  I don't see any need to release this as \"KDE 4.0\", when the features won't be complete until 4.1.  Traditionally, major numbers are for features/compatibility issues, while minor numbers are for bug fixes.  KDE hasn't done it that way, but it's still what the majority really expect.\n\nBy saying the upcoming release is KDE 4.0, lots of people are going to try it out, expecting big things.  When it's incomplete, they're just going to go back to GNOME, thinking the KDE users are crazy, and not look at KDE again until 5.x.\n\nI'd also suggest calling it \"3.99\", or \"KDE*BASE* 4.0\" or \"KDE Development Platform 4.0\" or yes, \"4.0 technical preview\".\n\nAs for users \"not using betas\"... well, that's because you haven't mislead them, and they know it's not yet suitable for them.  That's EXACTLY what we need to do."
    author: "Lee"
  - subject: "at last somebody pulled the plug"
    date: 2007-08-31
    body: "and delayed the release till December. Thats' a huge relief for everybody\nhttp://lists.kde.org/?l=kde-core-devel&m=118856879831611&w=2"
    author: "Sergey"
  - subject: "Re: kopete"
    date: 2007-08-28
    body: "KDE devs have a different definition of beta than that of most computer users.\nThere's nothing bad in that, is just a matter of warning:\nfor example gentoo kde-herd decided that kde-4.0_beta1 was far away from the\nstability and thoroughness needed to enter the official portage (never\nhappened before for a beta), and they'll keep this position even to RC and\n4.0 release if will be the case.\n\n(said that, for sure KDE 4.0 will contain at least a 3.5 version of kopete)"
    author: "nae"
  - subject: "Wikipedia integration"
    date: 2007-08-27
    body: "When I read \"Wikipedia integration\" I remembered the following dot-article:\n\nhttp://dot.kde.org/1119552379/\n\nI never heard from this anymore - does the mentioned API already exist?"
    author: "Sepp"
  - subject: "Re: Wikipedia integration"
    date: 2007-08-28
    body: "I have seen some apps integrate wikipedia stuff (games, edu apps like marble, multimedia apps like Amarok), but no integral, KDE-wide API for that yet. Maybe a 4.1 or 4.2 thing, then..."
    author: "jospoortvliet"
  - subject: "Re: Wikipedia integration"
    date: 2007-08-28
    body: "DBpedia is a very useful way of querying the content of Wikipedia semantically (as opposed to using a free text search) via the SPARQL query language, and it combines the data with other sources, such as Musicbrainz or Geonames which makes it powerful. Currently it is a bit slow though as the site only has one machine, instead of a google-like data center.\n\nYou should be able to use apis in the Soprano KDE4 lib to query SPARQL end points such as DBpedia with C++ code, (or you can use ActiveRDF with SPARQL in Ruby as I tried out recently)."
    author: "Richard Dale"
  - subject: "KDE3 apps in KDE4?"
    date: 2007-08-27
    body: "How easy will it be to use KDE3 apps in a KDE4? I'd like to move over to KDE4 as soon as it's ready, but there are some applications that don't seem like they'll be ported to the new libraries for the 4.0 release.\n\nWill it simply be a matter of having the KDE3 libs installed as well? Sort of like running KDE apps in Gnome?"
    author: "Soap"
  - subject: "Re: KDE3 apps in KDE4?"
    date: 2007-08-27
    body: "A lot of people already do the reverse, running KDE4 applications in KDE3.  It should pretty much be the same as running Gnome/KDE apps in KDE/Gnome, just having the libraries installed and you should be fine.  Though you'll probably have to tweak your settings in both systemsettings (kde4) and kcontrol (kde3) for things like style and color schemes.\n\nHopefully KDE 4.1 will kill off KDE 3 so that KDE land won't end up like GTK land (stuck with both GTK 2 and GTK 1 for the longest time)."
    author: "Sutoka"
  - subject: "Re: KDE3 apps in KDE4?"
    date: 2007-08-28
    body: "The short-short answer - yes :) KDE3 apps will run against the KDE3 libraries just as fine inside a KDE4 environment (in fact, there was a long discussion at akademy on how to deal with this)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Please push back the release"
    date: 2007-08-28
    body: "The first beta was actually a second alpha.\n\nSome of the core technologies that are supposed to be the backbone of KDE 4 aren't ready yet, and it looks like many apps were just gaining steam, only to halt in the face of feature freeze.\n\nPeople who are impatient and who want to run KDE 4 right away can always do that with alpha/beta builds, and do so today.  However, those who see a release build will no doubt expect a proper release.\n\nI know this release is huge, and entails a great deal of work, but as much as Ubuntu and the Gnome crowd has been growing and growing, I think this release is crucial to the future of the KDE community.  This is just my opinion, but I'd rather see two releases in October.\n\nKDE 4-RC1 and 3.5.8, and then ship KDE 4.0 when it is properly ready.  How many incredible features that just missed the cut would make it if the feature freeze were reopened and pushed back say two months?\n\nEvery major distro that includes KDE will probably appreciate it more to have a fully-functional KDE 4 that blows people away, rather than one that will open up a slurry of gripes and complaining.\n\nFurthermore, if the KDE devs posted a press release stating that they need a little more help to get over the hurdle, and then send it to Slashdot/Digg, etc. then it might encourage some more volunteers to come see what is happening.  Now that the most of the core libraries are in place for KDE 4 (save for stuff like Decibel apparently) it is a better environment for new developers to start hacking on the code.  Some fresh blood, and one last big push could really make the KDE 4.0 release even bigger and better, rather than a perceived let down.\n\nAnd frankly, even if the porting to QT 4 was time intensive, and writing all the underlying libraries was time intensive, the end users will only care about what is immediately visible to them.  Two months will not only mean more features sneek past the feature-freeze, but those systems that were already in place for 4.0 currently can get some testing, spit, and polish.\n\nSeriously, I hope this gets some serious thought and consideration."
    author: "T. J. Brumfield"
  - subject: "Re: Please push back the release"
    date: 2007-08-28
    body: "KDE 3.x line was (by my standards) unusable until it hit 3.4.x, and became somewhat pleasant for use in late 3.5.x. But, without 3.0 there would be no 3.4.x\n\nYou will not buy \"ready\" in 2 extra months .0 Conveys readiness in different ways. The only kind of people that KDE 4. needs now is the application builder, and for them, 4.0 will be ready as in \"There is a chance that some significant portion of API will not change from now on\" \n\nTo simplify the formula: without .0 an open source product is never ready."
    author: "Daniel"
  - subject: "Re: Please push back the release"
    date: 2007-08-30
    body: "That's fine for devs who understand it, but go ask GNOME users how many of them tried KDE 3.x (where x < 4) and gave up because it crashed a lot."
    author: "Lee"
  - subject: "Re: Please push back the release"
    date: 2007-08-31
    body: "I think they should've tried another distribution. I don't see why KDE apps should be any more inclined to crash than Gnome apps, and I'm sure they aren't."
    author: "jospoortvliet"
  - subject: "Re: Please push back the release"
    date: 2007-09-01
    body: "But that's the thing: KDE probably has better engineering, better object orientation, more integration, better oversight... and yet the early 3.x releases were buggy as hell.  It really was 3.3 or 3.4 before things started getting reliable (kmail's IMAP, for instance).  Users try the early releases, and take this as an indication of reliability/maturity overall.  Thankfully the KDE 4.0 release date has been pushed back now, so hopefully that'll help.\n"
    author: "Lee"
  - subject: "Re: Please push back the release"
    date: 2007-09-01
    body: "http://troy-at-kde.livejournal.com/6794.html"
    author: "Lee"
  - subject: "Re: Please push back the release"
    date: 2007-09-03
    body: "I was glad to see it.  I am very curious about the specific wording.  The feature freeze is still in place, however there will apparently be exceptions.  For those who can code, I think this is an opportunity to make a last push, and even if no new features make it in, I think two months of testing will help immensely."
    author: "T. J. Brumfield"
  - subject: "Can we stop this please?"
    date: 2007-08-28
    body: "For whatever reasons, some features planned in KDE4 will not make it to KDE4.0 but in 4.1. As if this did not happenned to other projects.\nSo what? What's the deal? Why would that mean that KDE 4.0 will be unusable or severely limited?\nI'm pretty confident everybody will make sure everything's in place at release time and I certainly don't judge anything based on the state of the beta.\nThis is all very exciting, let's cut the doom and gloom crap, please.\n\nAs for Gnome, please stop panicking because of this DesktopLinux survey, you know it's utterly worthless. And if inded Gnome is making inroads in popularity, it tells more about Ubuntu's success than Gnome appeal. So there's little KDE devs can do about that, just let them concentrate on the work on KDE4 to make it so appealing it will be chosen as default by the bigger distributions. "
    author: "Richard Van Den Boom"
  - subject: "Re: Can we stop this please?"
    date: 2007-08-28
    body: "It isn't simply the DesktopLinux survey, though those numbers are pretty alarming.  Everyone I speak to mentions Ubuntu, from corporate IT guys who have never tried Linux, to random people at my wife's college.  Ubuntu is spreading like wildfire.  Everyone says it is the perfect Linux to try if you've never tried Linux before, and thusly most people who are new to Linux are experiencing Gnome.  Anytime I talk to such a user, they're happy with something simple that seemed to work the first time, and don't want to up and switch again.\n\nDistrowatch.com also paints a similar picture.  Google search results show that Ubuntu is a hot topic as well.\n\nAnyone who attempts to dismiss how quickly Gnome and Ubuntu are growing and just burying their head in the sand.\n\nI understand it isn't a competition, and the growth of open-source software and Linux on the whole is a win for everyone.  However, more and more support seems centered on what Ubuntu and Gnome are doing, and I'm worried that a poor showing with the KDE 4.0 release certainly isn't going to help.\n\nYou're confident that everything will be in place by release time.  What are you basing that claim on?\n\nEach week I look forward to the new digest, however each week I am growing more and more disappointed to see yet another feature that won't make release.  Will decibel be in place by release time?  Given that it was one of the core technologies touted in KDE 4, and all the core technologies were supposed to be done some time ago, I was shocked to hear it didn't finish in time for release.\n\nSo should I base my expectations of KDE 4.0's release on the actual real-world use of the beta (crashes all the time, and many core aspects of the desktop you'd use everyday are still missing), the digests (more and more features being cut) or you're assertion that everything is just going to appear in October?"
    author: "T. J. Brumfield"
  - subject: "Re: Can we stop this please?"
    date: 2007-08-28
    body: "One of the problems faced by the Kopete team is our own success, most of the long term have got jobs and other responsibilities that we didn't have in the days of KDE 3.0, and so development doesn't proceed as quickly as you might like. \n\nHowever, Decibel is ready and already use in KCall (for VoIP).  Kopete's usage of it is not ready for KDE 4.0, and rather than put time into writing code that will only be used in KDE 4.0 and then discarded, we're aiming to make a better Kopete for KDE 4.1.\n\nYour worry that everything is going to be terrible is based on two states which are perfectly normal for the end of a release cycle:\n\n*) Betas crash\n*) Features only get cut, not added at this point\n\nThose of you who remember KDE 2 betas and KDE 3 betas can confirm this.  If you want something to feel good about, try installing and starting each KDE 4 alpha in sequence and seeing how much progress we have made."
    author: "Bille"
  - subject: "Re: Can we stop this please?"
    date: 2007-08-28
    body: "Ubuntu has success because it's well made, not especially because it's Gnome. But they chose Gnome and now there's little KDE can do about that because they won't change to something else. Having a good KDE4 will be good for distributions which provide KDE as default but that's all you can expect. There's no point in worrying about that since there's little we can do about it.\nAnd no, DesktopLinux numbers should not considered alarming since they're basically worthless.\nI'm not basing any claim on anything anyway, I have just faith the KDE devs want to release something they're proud of and thus that it will be very nice. I see no point in bugging them with politics or false expectations, I prefer them to concentrate on doing the best they can, which is already fantastic.\nIf I was interested in running what everybody runs on a computer, I would be running Windows. So I don't mind if 80% of Linux users run Gnome, I use KDE because I find it better and everybody else be damned. A project the size of KDE will always attract devs anyway, and I'm pretty confident it will continue to improve steadily because people do it for fun and not because they have a commercial or political agenda.\nSo why worrying?"
    author: "Richard Van Den Boom"
  - subject: "Re: Can we stop this please?"
    date: 2007-08-28
    body: "Even the choice for Gnome isn't set in stone. If KDE 4 is good enough, Mark said they would consider switching to KDE as main desktop. Of course, they've spend a huge amount of resources on getting Gnome catch up on KDE in terms of features - that's a waste, sure. But then again, Economy dictates to ignore 'sunk costs', so I hope they can and will ;-)"
    author: "jospoortvliet"
  - subject: "That would be nice..."
    date: 2007-08-28
    body: "...if only as a reward for all the people involved in KDE."
    author: "Richard Van Den Boom"
  - subject: "Re: Can we stop this please?"
    date: 2007-08-28
    body: "Yes, please stop saying things like \"some features planned in KDE4 will not make it to KDE4.0 but in 4.1.\" when Phonon will ship without a mixer, or things like \"Krita is perfectly useable and has almost the same number of user that Gimp has\" when there are 2 gazzillion gimp tutorials on the net and one, maybe two for Krita.\n\nThis will just ruin credibility, users will be moved to think things like \"kde 4 is so simple that it's sound framework has not planned a mixer\" or \"krita is so ugly that it's users are all childs: they can't still write\".\nBeing open-source doesn't means at all that testers/users voices shouldn't be taken in account.\n\nI'd like also to point out that still (beta1 shipped, release planned in less than 2 months) there isn't a system tray at all (another little \"feature\" that will not make it for KDE 4.0????). \nNo crying, no screams, no panic: I just think devs should warn users about what to expect from KDE-4.0 , I'm already sure that Ubuntu will not switch to KDE for KDE-4.0 and about sure that most distros will keep 3.5.7 as productivity DE until the release of KDE-4.1 or maybe 4.2...."
    author: "nae"
  - subject: "Re: Can we stop this please?"
    date: 2007-08-28
    body: "You are the one helping ruining credibility by giving meanings to one-liner in a changelog you don't understand or while propagating claims I've never read. Well, not just you, most people here who tends to overdo things.\nThe story about the mixer was explained above as having basically no impact on users : you'll have your sound mixer all right. \n\nThis digest is to check where things are going for people interested in following the development process, most people are just not concerned about KDE4 until it's actually released so they won't be disappointed, except maybe if you go and tell them all along that everything is going badly.\nMaybe you'll be disappointed by KDE4 because you had high expectations and have been following from the begining what was supposed to be available at release. But most Linux users will actually discover what's new in KDE with the first release, so they'll probably not be very concerned about having Akonadi in KDE 4.1 or not (or whatever else is postponned).\nYou're just assuming some things won't be there because they're not here yet. So you're crying, panicking, screaming. Yes, that's exactly what you do.\nAct as an adult and wait until people who actually do the working finish their job. Then you can judge and comment.\nThere's no reason yet to believe that KDE4.0 won't be great, even with some missing features.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Can we stop this please?"
    date: 2007-08-28
    body: "Hear hear."
    author: "Paul Eggleton"
  - subject: "Re: Can we stop this please?"
    date: 2007-08-29
    body: "Have I ever said something like \"Kde 4.0 will not be great\" ????\nPlease try to LISTEN, I'm not judging at all.\nWhat I'm saying is that my expectations were about those of every kde user\n(because of the pr...), and that the RISK is that, after all this hype\n(that I didn't contribute to spread, really) the 4.0 version of kde will\ndelude so much that most of the USERS will consider it no more than a\ntechdemo (devs can have the definition they want for techdemo, but\nusers have their, most common, one).\n\nI still think developers should make clear what 4.0 is and what's not\n(and I'm not telling about Akonadi, but also about TENOR, about\nDECIBEL/KOPETE, about PHONON and it's backends, and all the other subprojects\nthat will not be really finished for october).\n\nSo please act as an adult yourself and consider the possibility that this\nissue could be real, it's nothing so dramatic to admit that what wasn't\ndone in more than 12 months can't be done in the remaining 1,5 , this\nobviously given the assumption that the public svn is what devs are\nusing, and there isn't a private svn with months-ahead code hidden inside\n(yes, in this case I'm totally wrong and you're totally right, happy?)."
    author: "nae"
  - subject: "KSensors anyone?"
    date: 2007-08-29
    body: "It seems like this project is completely abandoned, yet, it's the most convenient application for viewing lm_sensors data (also a patch for viewing HDD temperature exists).\n\nDoes anyone have a time and skills to port this application from KDE3 to KDE4?"
    author: "Artem S. Tashkinov"
  - subject: "Re: KSensors anyone?"
    date: 2007-08-29
    body: "Why are you saying \"most convenient\" ?\nIsn't superkaramba better? \n(and now that there is plasma,\nit should be even faster....)"
    author: "nae"
  - subject: "Re: KSensors anyone?"
    date: 2007-08-31
    body: "It resembles Motherboard Monitor, besides it works well and has almost zero problems.\n\nNo SuperKaramba theme can present information which I need the way I want without all that unnecessary fuss. Plasma widgets are not yet mature and ready."
    author: "Artem S, Tashkinov"
  - subject: "Dolphin and Tabs"
    date: 2007-08-29
    body: "I HATE this! Why is Dolphin going to be crippled by not having tabs? Is there ANY good reason for this? If you don't want to use tabs, you simply don't use them, even if you could. But if you want to use tabs, and some stupid developers won't implement tabs because they think they have to be gnomish and patronize users? What's the problem in implementing tabs? That it will add a few kilobytes to a 500 Megabyte KDE?"
    author: "blueget"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-08-30
    body: "Time."
    author: "bsander"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-08-30
    body: "Given that the code is in QT, and it requires very minimal code, time isn't the answer.\n\nPeople were already complaining that Dolphin was getting too similar to Konqui, and it was defeating the entire purpose of a simple file manager.  Personally, I don't see why the FM has to be simple.  I've seen some brilliant mock-ups to clean up the look and UI for Konqui and Dolphin without sacrificing the capabilities."
    author: "T. J. Brumfield"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-08-31
    body: "Why do you care? Why not use konqueror? There is nothing dolphin can do which Konqueror can't. Why turn Dolphin into Konqueror, what's the purpose of that?\n\nCan we please keep Dolphin easy and basic? Sorry, but name me ONE big desktop environment with a filemanager which has split view - let alone a tabbed interface. Not Mac OS X'finder, not Thunar, not MS Explorer, you won't see it in Gnome nor BeOS. I think implementing tabs in a filemanager which is supposed to be basic and simple is stupid, and I'm glad the dolphin dev's didn't do it. I will use Konqueror, so should you. "
    author: "jospoortvliet"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-08-31
    body: "Who said that Dolphin we aimed at being simple? I thought (and I may be wrong here) that is was aimed at being a very good filemanager, and just a filemanager at that. That is not the same in my view. I personally like tabs very much in file management, and will probably continue to use Konqueror for this. That no other big desktop uses it is a moot point in my view. Why is that relevant? Can't KDE make up it's own mind? Ah well, we'll see how it pans out. He who codes decides in the end, and since I'm not coding, I guess I should just shut up now. "
    author: "Andr\u00e9"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-08-31
    body: "\"Who said that Dolphin we aimed at being simple? I thought (and I may be wrong here) that is was aimed at being a very good filemanager, and just a filemanager at that. That is not the same in my view.\"\n\nSeriously.  A file manager that aimed to be simple would not give the option to embed a terminal, which is far, far more of a power-user feature than tabs, IMHO."
    author: "Anon"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-09-02
    body: "I think this totally ignores the problems (both technical and social) of refactoring an app the size of konq.\n\nThe fact is that dolphin is meant to be a pure filemanager, it will not blatendly copy features from konq that had a stupid user-interface mechanism anyway.\nSo, the 'basic' idea were never there. Not sure where you got that from.  Easy was the goal; and its a gnomism to assume that means basic or simple or lacking advanced features.\n\nAll it means is that the guys and gals creating Dolphin are very much aware that this app should stay 'easy' to use. Which means that usability wise a lot of work is being done to allow new features to not get in the way.  But, again, it doesn't mean features shouldn't be added.\n\nI hope that meme can die soon :)"
    author: "Thomas Zander"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-09-03
    body: "Quite frankly, I thought the purpose of Dolphin was to create an app that was a dedicated FM, the way that Okular is a dedicated document viewer.  Konqui is not an incredible FM because it tries to be all things to all people.  There are many things about Dolphin that I like, and embedding a terminal is nice, but I still prefer yakuake.  I enjoy being able to use tabs to eliminate having multiple windows open.\n\nI think a split view works better if you're just using two locations, as you can easily drag-and-drop directly across a split view.  However, if you're working with several locations at once, tabs are essential.\n\nA good FM should be flexible and allow choice."
    author: "T. J. Brumfield"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-09-03
    body: "Isn't that exactly what I said as well?"
    author: "Thomas Zander"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-08-31
    body: "Why do you care? Why not use konqueror? There is nothing dolphin can do which Konqueror can't. Why turn Dolphin into Konqueror, what's the purpose of that?\n\nCan we please keep Dolphin easy and basic? Sorry, but name me ONE big desktop environment with a filemanager which has split view - let alone a tabbed interface. Not Mac OS X'finder, not Thunar, not MS Explorer, you won't see it in Gnome nor BeOS. I think implementing tabs in a filemanager which is supposed to be basic and simple is stupid, and I'm glad the dolphin dev's didn't do it. I will use Konqueror, so should you. "
    author: "jospoortvliet"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-08-31
    body: "Dolphin isn't basic and I think it was never intended to be \"basic\".\n\nWhat about the embedding of a terminal, which is already in Dolphin? Is THAT \"basic\" in your mind???\n\nDolphin is intended to be a good filemanager, not to be \"basic\", \"easy\"  or what else you use as description for crippled.\n\nName me ONE big Desktop Environment, that has got a browser which you can also manage files with, besides KDE. That no other Desktop Environment has got it is not an argument! If all people would argue like this, then NOTHING new would be able to be developed! That's like if you'd say \"No other Desktop Environment has got a thing like plasma, so we'll stop it\" or something like that!"
    author: "blueget"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-09-22
    body: "So will Dolphin have tabs or what? \n\nWill there at least be an add-on to have tabs? "
    author: "Gary"
  - subject: "Re: Dolphin and Tabs"
    date: 2007-09-22
    body: "At this point it's looking like No and No, which is a huge shame as, with a bit of ingenuity, tabs could be added in such a way that the casual user was never even aware of them."
    author: "Anon"
  - subject: "Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-22
    body: "Why are there so many borders is the every Dolphin panel? It's a (Oxygen) style problem or a application one?\n\nLook at the screenshots, the places panel have a border around it, the name 'places' (and the buttons) are almost over this line. And appears to me that every panel have it's border, and vertical and horizontal \"splitters\". The information panel (2nd screenshot) it's pretty overcrowded, so full of stuff that it's difficult to find the information that you need.\n\nhttp://img164.imagevenue.com/img.php?image=30400_7_122_1144lo.jpg\nhttp://img192.imagevenue.com/img.php?image=30406_sreencast_122_505lo.jpg \n\nPlease remove all that borders you can. Sorry to say that, but in this shape Dolphin looks a lot like a KDE 3 application running a modded Plastik theme.  \n\nAnother problem is the green progress bar. It's pretty strange, too big, with a weird proportion. I suggest that you create a small contour around it and make it a little smaller (in the vertical).\n\nThanks and sorry for my bad english."
    author: "kde.fan.from.brasil"
---
In <a href="http://commit-digest.org/issues/2007-08-26/">this week's KDE Commit-Digest</a>: "Pencils down" marks the end of the <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> for 2007. Python highlighting support, with work on a new, handwritten <a href="http://en.wikipedia.org/wiki/Lexer">lexer</a> in <a href="http://www.kdevelop.org/">KDevelop</a>. A data engine and associated Plasma applet for KGet. Start of the <a href="http://plasma.kde.org/">Plasma</a>-based Wikipedia and Service Info applets for <a href="http://amarok.kde.org/">Amarok</a> 2. Wikipedia integration, and other improvements in the Step physics simulation package. A console added to KAlgebra. New graphical themes for KGoldRunner. XMP metadata support in <a href="http://www.digikam.org/">Digikam</a>. More progress in the unobtrusive search dialog for <a href="http://kate-editor.org/">Kate</a>. Usability work across many applications. No mixer functionality in <a href="http://phonon.kde.org/">Phonon</a> for KDE 4.0. The start of development on <a href="http://koffice.kde.org/kchart/">KChart</a> 2.

<!--break-->
