---
title: "The Road to KDE 4: Kalzium and KmPlot"
date:    2007-01-30
authors:
  - "tunrau"
slug:    road-kde-4-kalzium-and-kmplot
comments:
  - subject: "digg it!"
    date: 2007-01-31
    body: "http://digg.com/linux_unix/The_Road_to_KDE_4_Kalzium_and_KmPlot"
    author: "Patcito"
  - subject: "Re: digg it!"
    date: 2007-01-31
    body: "No"
    author: "illogic-al"
  - subject: "glossary"
    date: 2007-01-31
    body: "> Making improvements to the Glossary would be a great opportunity for a \n> chemistry-inclined person to contribute to Kalzium in KDE 4 without having\n> to be a programmer.\n\nOr, if you're not chemistry-inclined why not suggest words or terms that you don't know and include those terms in the glossary? that way terms commonly known to chemists are not overlooked. \n\nthanks for the great work!\n"
    author: "bkn"
  - subject: "Kalzium looks promising"
    date: 2007-01-31
    body: "Especially the 3D viewer. Is there any chance of having a PDB viewer (Protein Databank) integrated into KDE? That would be really great. Apart from that, great work. Can't wait for KDE4"
    author: "Rithvik"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "It seems to me that all these molecule databases would be a perfect candidate for the GHNS (Get Hot New Stuff) feature.  It seems a bit unnecessary to include everything in the chem world in kdeedu.  It's already big enough as it is."
    author: "Inge Wallin"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "Sounds like a good idea, I'll forward it to Carsten if he hasn't already read your comment. I guess we could make it so that the first time the user launches the mol viewer, he gets proposed to download molecule files."
    author: "Benoit Jacob"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "Well, *some* molecules should of course be shipped with Kalzium.  It's just that all these databases are terribly big, and not especially useful for everybody."
    author: "Inge Wallin"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "Sure, we'll ship at least a few molecules. By the way, the whole gzipped archive weighs 300 kb."
    author: "Benoit Jacob"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "So maybe a good behavior is to show by default ony a few mols, and then put a button in the GUI saying \"download more molecules\".\n\nWell, we need to think more about this."
    author: "Benoit Jacob"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "We use OpenBabel to load molecule files, and OpenBabel supports 62 file formats as Troy counted. I checked, PDB is among the supported formats.\n\nhttp://openbabel.sourceforge.net/wiki/Category:Formats"
    author: "Benoit Jacob"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "That is understood. Openbabel does convert and support many formats, but it looses a lot of annotations on the way, such as residue information.\nThe 'molecule' screensaver in xscreensaver does a similar thing by showing PDB files. But PDB viewers show more than just the atoms in the molecule as most molecule viewers do. e.g secondary structure as ribbons, backbone trace, residue annotations etc. A PDB viewer is more than a molecule viewer. There are a lot of pdb viewers there, but none which integrate with KDE or konqueror like Chime  (http://www.mdl.com/products/framework/chime/) does with IE on windows (jmol (jmol.sourceforge.net) is good enough though). A Plugin or kpart like that could help such projects like the gromacs gui ( http://kde-apps.org/content/show.php?content=47665 ). This doesn't make sense for kalzium maybe, but the libavogadro thingie looks promising."
    author: "Rithvik"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "ah ok, thanks for explaining. I'm no chemist and I'm only doing the math and OpenGL stuff here, but I'll point Carsten and Donald to your comment."
    author: "Benoit Jacob"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "Well, there is Bioclipse (http://www.bioclipse.net/): http://bioclipse.net/index.php?option=com_content&task=view&id=11&Itemid=5\n\nOf course, that is not in Konq, but for that you can of course use JMol (http://jmol.sourceforge.net/). Look at the screenies if you want to see it in action: http://jmol.sourceforge.net/screenshots/"
    author: "Carsten Niehaus"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "I have mentioned and use jmol. I didn't know of bioclipse. Thanks."
    author: "Rithvik"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "I've been working on pulling some extra information (annotations?) into OpenBabel.  I know that there is already work for residue information (although i haven't worked on that directly).  libavogadro will support it, and thus kalzium."
    author: "dcurtis"
  - subject: "that's the spirit. thank *you* all"
    date: 2007-02-01
    body: "just a short note the feature is coming to a library that is used by a library that is used by our favorite KDE application AND 'competing' apps.. That's the real power of FOSS. Sh1t, I just love this. It's like 'sweets hitting the fan' or so...lol."
    author: "eMPe"
  - subject: "Re: that's the spirit. thank *you* all"
    date: 2007-02-01
    body: "When I started that 3D-feature I wrote my own cml-parser. After some days I thought that is a really stupid idea and ported Kalzium to OpenBabel. It took me about 2 hours and five emails.\n\nIn the end OpenBabel has a couple of bugs left, the API-docs are now cleaner (because of my stupid questions ;-) and OB even has some new features I needed. Avogadro is also using OB, as is GHemical and others."
    author: "Carsten Niehaus"
  - subject: "Re: Open Babel problems?"
    date: 2007-02-01
    body: "If you have particular \"problems\" with Open Babel, you really should report them to the bug tracker. Please don't just gripe about the project on a random forum -- your bugs won't be fixed that way. Better yet, please give us example files of stripping out annotations and tell us what we should do instead. Residue information certainly should be retained when reading a PDB file. Check out PDB -> Mol2 for example.\n\nYes, Avogadro does have options for rendering a residue at a time, or secondary structure, etc. We've planned it that way. But Avogadro is also based on Open Babel, so if you really think there are problems in the underlying library, it'll be a problem with Kalzium, Avogadro... well quite a few viewers.\n\nI like to think Open Babel as a project is pretty good about responding to critiques and suggestions. So please tell us what we're doing wrong so we can fix them!\n\nhttp://sourceforge.net/projects/openbabel"
    author: "Geoff Hutchison"
  - subject: "Re: Kalzium looks promising"
    date: 2007-01-31
    body: "I am working together with Jerome and his project:\n\nhttp://chem-file.sourceforge.net/\n\nCurrently, that project ships 524 structures. If you are missing any structure tell me and I promise it will be there ASAP.\n\n"
    author: "Carsten Niehaus"
  - subject: "LaTeX support"
    date: 2007-01-31
    body: "Does anybody know, if LaTeX support is planned for KmPlot or Kalzium (exporting molecules).\n\nThat would be really cool!"
    author: "Peter"
  - subject: "Re: LaTeX support"
    date: 2007-01-31
    body: "Kmplot would benefit from sharing some code to edit formulas with KFormula from KOffice.  It's basically two different implementations that do the same thing. Probably also two different implementations to save them in various formats, parse them and possibly other stuff too.\n\nPS. KFormula does have export to Latex even if I hear that there is a bug in it."
    author: "Inge Wallin"
  - subject: "Re: LaTeX support"
    date: 2007-01-31
    body: "http://bugs.kde.org/show_bug.cgi?id=136710\n\nThe bug appears to be that it is disabled."
    author: "Steve"
  - subject: "Re: LaTeX support"
    date: 2007-01-31
    body: "That would only work for 2D-structures, right? Well, if OpenBabel gets this feature I would implement it in Kalzium, yes."
    author: "Carsten Niehaus"
  - subject: "Re: LaTeX support"
    date: 2007-01-31
    body: "Yes,only for 2D-formulas. Do you know:\nhttp://imt.chem.kit.ac.jp/fujita/fujitas3/xymtex/indexe.html\n\nSome thing like that would be pretty cool."
    author: "Peter"
  - subject: "Re: LaTeX support"
    date: 2007-02-01
    body: "Well, Kalzium would inherit this feature automatically if Open Babel had it.\n\nSo my best suggestion is to either push for this or find someone to implement it in Open Babel:\nhttp://sf.net/projects/openbabel\n\nThe particular file format request looks like this:\nhttp://sourceforge.net/tracker/index.php?func=detail&aid=1451051&group_id=40728&atid=447448\n\nHowever, it lacks a lot of detail! If you have a few minutes to flesh out the request, including the \"LaTeX\" format you want (Fig?) and how this might work, it would be greatly appreciated.\n\n-Geoff"
    author: "Geoff Hutchison"
  - subject: "Re: LaTeX support"
    date: 2008-03-30
    body: "Kmplot will export to an .eps file, and you can include that in a LaTeX file.  That's really the best way to get graphics into a LaTeX file anyway."
    author: "Brent"
  - subject: "potting experimental data with kmplot?"
    date: 2007-01-31
    body: "The new interface of kmplot looks pretty cool and the feature listing is impressive. I just wonder why it does not support plotting of x,y-data (or have I overlooked something on the feature page)? To me as an engineer kmplot would be much more useful if I could compare experimental (x,y) data to mathematical functions, which kmplot plots so well."
    author: "ups"
  - subject: "Re: potting experimental data with kmplot?"
    date: 2007-01-31
    body: "The next version of KChart will hopefully be able to do that.  KOffice 2.0 is not released yet, but then again, neither is kmplot 4."
    author: "Inge Wallin"
  - subject: "Re: potting experimental data with kmplot?"
    date: 2007-01-31
    body: "Do kmplot and kchart share libraries?  I hope there isn't extra duplicated effort in drawing graphs, and the kmplot graphs look much nicer than those in kchart 1.6.\n\nLooking forward to KOffice 2.0!"
    author: "Yuriy"
  - subject: "Re: potting experimental data with kmplot?"
    date: 2007-02-01
    body: "Kst from extragear is a very powerful tool for data plotting, which you might find useful"
    author: "Philip Rodrigues"
  - subject: "Re: potting experimental data with kmplot?"
    date: 2007-02-06
    body: "One really good solution I have found is www.zunzun.com .  The website author uses open-source tools to display and fit data, as well as data analysis statistics and graphs.  I used it to do data analysis for a paper I recently prepared, and recommended it to the lab supervisor at my university for use in the undergraduate  physics labs.\n\nAnother good option is Gnuplot.  I've also done plotting in Kile, the KDE Latex interface, though it was function plotting and not fitting.  While I agree that it would be a good addition to KDE, I have to give a shout-out for some tools that have been very useful for me."
    author: "Me"
  - subject: "Re: potting experimental data with kmplot?"
    date: 2007-04-19
    body: "I'm glad you found my web site useful.  The site fitting code is available under a BSD-style license at http://sf.net/projects/pythonequations.\n\n     James"
    author: "James R. Phillips"
  - subject: "Wasted effort - help Maxima, Axiom, Scilab"
    date: 2007-01-31
    body: "With _real_ mathematics open source projects like Axiom (CAS), Maxima (CAS), and Scilab (matlab like, feature-full and fast, unlike the stagnated Octave), and R (full-blown professional statistics environment - but in serious need of some spreadsheet interface) it's a real shame that KDE chooses to waste time with replicating functionalities that are already in other software, when eforts could be made to further those serious packages.\nStop writing toys. Stop playing."
    author: "jj"
  - subject: "Re: Wasted effort - help Maxima, Axiom, Scilab"
    date: 2007-01-31
    body: "The projects you mention have nothing to do with Kalzium and Kmplot.\n\nObviously you haven't RTFA."
    author: "Benoit Jacob"
  - subject: "Re: Wasted effort - help Maxima, Axiom, Scilab"
    date: 2007-01-31
    body: "KmPlot is designed for quick-and-easy plotting for simple math, not full blown math suites.  If you want to do serious math, you use a serious math program, not a program that ships as part of an educational suite.\n\nYou wouldn't use javascript to write a kernel.  Don't use KmPlot for your math thesis."
    author: "Troy Unrau"
  - subject: "Re: Wasted effort - help Maxima, Axiom, Scilab"
    date: 2007-02-03
    body: "https://www.youos.com/ -- operating system written in javascript :-)"
    author: "name"
  - subject: "Re: Wasted effort - help Maxima, Axiom, Scilab"
    date: 2007-01-31
    body: "still, the direct competitor is gnuplot..."
    author: "nick"
  - subject: "Re: Wasted effort - help Maxima, Axiom, Scilab"
    date: 2007-01-31
    body: "\"Stop writing toys. Stop playing.\"\n\nExcuse me? Are you telling developers they can't have fun? Be a little nicer when you make requests!"
    author: "kwilliam"
  - subject: "Re: Wasted effort - help Maxima, Axiom, Scilab"
    date: 2007-01-31
    body: "You should mention Python + SciPy + NumPy + Matplotlib. This should be the winning combination, IMHO. At least, this the best I found in open source, after having wandering for a while (Scilab, Octave, Gnuplot, R and others)! I encourage people to write libraries/toolboxes in this framework. Scilab and Octave fail to compete with Matlab, even if Scilab is good. I think that Python with brilliant libraries such SciPy is the best path for open source software to compete with Matlab. The work is well advanced, but help is needed!\n\nYour sentences \"Stop writing toys. Stop playing.\" had to lead to disagreements! On one hand, you cannot blame people writing new software _for you and for free_. On the other hand, developers with ambition bring useful tools, which is better including for them, in the end. But, in my experience, many many developers do not understand that. I do not know why. Open source might suffer from a lack of steering people, just like there are good managers in firms that prevent talented people from wasting their time."
    author: "Hobbes"
  - subject: "Scilab is not open source"
    date: 2007-02-01
    body: "Um, except that Scilab is not a \"real\" open-source project.  In fact, it is not \"open source\" at all according to the commonly-accepted OSI definition, despite the Scilab web page's prominent claim to be an \"open source platform\".\n\nIn particular, their license prohibits commercial redistribution of modified versions.\n\n(And if you're about to object, \"But who cares about that?\", think about it: it gives a single organization, INRIA, an effective monopoly over commercial support for Scilab.  If all free software had that restriction, commercial Linux distros would basically be out of business and the world would be quite different.)"
    author: "Frodo"
  - subject: "there is a need for a KDE Maxima"
    date: 2007-02-02
    body: "Telling volunteers what to do (and especially what not to do) is the height of superiority and ignorance. I haven't used KmPlot, but Kalzium is actually really handy. \n\nI agree that a KDE computer algebra system would be really nice. I currently use Mathematica, and its interface frankly just sucks. It totally breaks down if you commit the sin of turning the numlock key on (yes, its a math program that doesn't let you use the number pad!) and its the only program I've seen to misbehave with Beryl. A KDE frontend to Maxima would be killer."
    author: "Ian Monroe"
  - subject: "Re: there is a need for a KDE Maxima"
    date: 2008-01-22
    body: "Have you tried Kayali?\n\nhttp://kayali.sourceforge.net/"
    author: "Christian Br\u00f8nnum-Hansen"
  - subject: "Re: Wasted effort - help Maxima, Axiom, Scilab"
    date: 2007-05-22
    body: "I totally way disagree... Plenty of programs _nominally_ have KmPlot's functionality. KmPlot is the _only_ program I've seen that does plots easily and quickly, and that allows you to change the coordinate plane dynamically and to trace Y-values along a function curve. I'll grant that I'm not a super open-source uber expert, but I know that ease of use counts and the time I would waste trying to figure out how to do plots in other programs (most implementations I've seen are simply frontends for GNUplot) is time I could have spent working and getting things done. KDEplot is simple, extensible, and powerful, and that's very important. Saying it's a \"wasted effort\" belies all the time and energy it has saved me in my math classes, but moreso it belies all the money it saves for all math students who need a quick and easy plotting program at home and don't feel like paying $80+ for a graphing calculator. (Remember, not everyone is going to learn about GNUplot frontends. Personally, I can barely get my brain around it and it's so frustrating, I'd much rather use KMPlot's simple, friendly, and dynamic interface, where I can add functions, change the grid, edit functions, etc. on the fly and with the fewest processes possible.)\n\nPersonally, I love Maxima... I use it and KMPlot together as something of a tag team for massive math productivity. A KDE frontend for Maxima would be killer, and one that would integrate with KMPlot would be sublime (in fact, combining such tools to create an integrated math toolkit, like Mathematica, would be a great goal). Just, please don't insult the work that KMPlot's team has put into this piece of software. I know it's not wasted effort because of how important it is to my math education. KMPlot is easy, simple, and fun to use, and that matters. I can't wait for the new version. And if you think developers should \"stop playing\" or \"stop making toys\".. you might want to realize that Linux basically started with some guy (that is, Linus) seeing if he could make his own OS just for the fun of it. There's value in toys and in tinkering; if you enjoy making your software, you'll make software that you will enjoy using, and that is the software that will be useful.\n\nSure, other programs do what KMPlot does... just not as well."
    author: "Tina Russell"
  - subject: "About the R/S Phrases Icon"
    date: 2007-01-31
    body: "At the Tools menu there is a R/S Phrases with an important simbol as the icon.\n\nThe icon used at R/S Phrases means Biohazard, it must be used only with biological  threats like pathogenic baterias, virus, genetic modified foods, pieces of tissues from a sick guy and so on.\n\nWhen you use a simbol for a diferent meaning, like the heavy rock band Biohazard, this simbol become weak. I think this simbol for R/S Phrases must be changed.\n\nIf you link this simbol with a rock band or a R/S Phrases you get a wrong meaning at your mind. If you see a package with this simbol at the streets you think: \"Cool man, let me see what I got today.!\" and hoooo hoooo you are infected.\n\nThis can sound geek, but this simbol is an international standard.\n\nThanks for the amazing work folks.\nKDE must go on.!! \n:-)"
    author: "\"from Brazil with love\""
  - subject: "Re: About the R/S Phrases Icon"
    date: 2007-02-01
    body: "R/S means \"risk and safety\".\n\nhttp://en.wikipedia.org/wiki/Risk_and_Safety_Statements\n\nSo I think the icon is appropriate."
    author: "Benoit Jacob"
  - subject: "Re: About the R/S Phrases Icon"
    date: 2007-02-01
    body: "That simbol means \"Biological Hazard\".\n\nThis is compliant whith the ISO 3864, wich specifies international standards for safety symbols.\n\n-> http://en.wikipedia.org/wiki/Biohazard\n\n\"\"A biological hazard or biohazard is an organism, or substance derived from an organism, that poses a threat to (primarily) human health. This can include medical waste, samples of a microorganism, virus or toxin (from a biological source) that can impact human health.\"\"\n\nIt's not relatad to other kind of threaths or hazards, like chemical products or radioative metals.\n\nWe have the radioactive simbol, the toxic simbol, the chemical hazard simbol.\n\nPlease, check this page: \n\n-> http://en.wikipedia.org/wiki/Hazard_symbol\n\n\n:-)"
    author: "\"from Brazil with love\""
  - subject: "Re: About the R/S Phrases Icon"
    date: 2007-02-01
    body: "thanks for explaining; i'll report to Carsten if he doesn't already know."
    author: "Benoit Jacob"
  - subject: "unicode"
    date: 2007-01-31
    body: "What's up with those -> arrows in the equation editor? We have Unicode (&#8594;) for a reason. Or is KDE not up to 1991 standards yet?"
    author: "Anonymous Coward"
  - subject: "Re: unicode"
    date: 2007-01-31
    body: "It's because most keyboards do not have a key to input an arrow, that's all.\n\nHow would you write an arrow in a text box on most keyboards? -> obviously. :)\n\nUsing the same arrow in the output - well, that may not be necessary."
    author: "Troy Unrau"
  - subject: "Re: unicode"
    date: 2007-02-01
    body: "\"How would you write an arrow in a text box on most keyboards? -> obviously. :)\"\n\nalt-gr+i works quite well. It is the default on quite a few X keyboards layouts."
    author: "Med"
  - subject: "Re: unicode"
    date: 2007-02-01
    body: "Actually, most people don't know that. Also, Kalzium is not X-specific but also works on Mac and Windows where you usually don't have a X-Server."
    author: "Carsten Niehaus"
  - subject: "Re: unicode"
    date: 2007-02-04
    body: "AltGr + i --- cool! I didn't know that.\nThe things one can learn from reading the .news!"
    author: "Kjetil Kilhavn"
  - subject: "Re: unicode"
    date: 2007-02-05
    body: "alt-gr... That's one of those keys that don't exist on all keyboard, right? at least i don't have one on my keyboard ;) (powerbook g4 - yes, i was pretty surprised to find out that key was missing too)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: unicode"
    date: 2007-02-08
    body: "what is the \"gr\" in alt-gr? I don't think I've ever seen anything labelled gr on a standard keyboard."
    author: "Chani"
  - subject: "Re: unicode"
    date: 2007-02-01
    body: "Sorry, yes, I'm just referring to the onscreen output, not the keyboard input. :)"
    author: "Anonymous Coward"
  - subject: "Re: unicode"
    date: 2007-02-01
    body: "In the first version we used the Unicode-version. But on many systems there are fonts installed which do NOT ship that symbol. Instead of a nice symbol they got a square.\n\nThat is now one year ago and the situation still didn't change. We could force certain fonts or test if the symbol is included in the font (and if not use '->' as a fallback). \n\nAre you willing to send me a patch or are you just trolling?"
    author: "Carsten Niehaus"
  - subject: "Re: unicode"
    date: 2007-02-01
    body: "If you want to look for code that tests if a glyph is in the font, take a look at the password box that now tests if a certain black circle is in the font and otherwise display a * IIRC.\n\nBut please do something like this, the \"->\" just looks clumsy, especially because the \"-\" and \">\" don't vertically align properly. You should allow that users enter \"->\" of course.\n\nAnd btw, situation has changed a bit since last year, since many distros have switched from bitstream vera to dejavu as their default font, and the latter has the arrow symbol. Also, testing a bit with Qt4 shows that the font substitution algorithms have improved since Qt3. So if one font has the arrows, it should be able to display them. If you don't have DejaVu, it should be able to pick it up from Arial from MS corefonts for example (that's at least how Qt4 does it here when I uninstall DejaVu). I would safely say that most users have at least one font with the arrows installed these days, and in that case Qt4 will show one for whatever font you have picked.\n"
    author: "Eimai"
  - subject: "Re: unicode"
    date: 2007-02-01
    body: "Doesnt the solver itself matter a bit more than a unicode detail ?\n\nNow for the unicode, many linux distributions provide a default font where the arrow does not exist.\n"
    author: "......."
  - subject: "So...."
    date: 2007-01-31
    body: "Will KmPlot formulas and images can be integrated in Kword?"
    author: "Aceler"
  - subject: "Colors and UI"
    date: 2007-02-01
    body: "Why must all geeky apps consist of nothing but fluorescent colors? We left the EGA color palette ages ago, so please make an effort not to burn out eyes. Simply turning down the saturation slides might help.? And no, I'm not some artistic type: I'm a programmer and I have a sense of color, simply because I paid attention to it. Do the same, and you'll find people will suddenly not cringe when they see your presentation slides, graphs or flowcharts.\n\nAs for kmplot: I have to wonder why almost 2/3rds of the toolbar is dedicated to 3 buttons labeled \"coordinate system I/II/III\" when literally the only useful element is their icons. The huge buttons would just be as informative without a label (or if you have to add one, how about \"Axes\" ?), and would be best merged together in a single drop-down or horizontal button group. Or best: provide a simple \"+\" shaped diagram and let the user toggle each of the axes on or off by clicking them.\n\nSeriously, take some cues from OS X's built-in Grapher application. It's tons easier to use, because it doesn't shove advanced features in your face if all you want to do is plot a graph.\n\nhttp://en.wikipedia.org/wiki/Image:Grapherexample.jpg (toolbar hidden)\nhttp://en.wikipedia.org/wiki/Image:GrapherToroid.jpg (toolbar visible and in 3D mode)\n\nIt has a much simpler layout, and keeps complicated options (such as exact coordinate ranges) away from the main screen using inspectors (aka floating palettes). Graphing applications should make it trivial to make simple doodle graphs and not be too obsessed with exact measurements until you need them.  The math entry is transparent... you type things like \"e^x\" \"sin x\" \"1/x-1\" but they get formatted and edited appropriately.\n\nGenerally speaking, I think you have a problem if you design a graphing application where only a small part of the window is actually *covered by the graph*. Wasn't KDE4 supposed to be much more usable?\n"
    author: "-"
  - subject: "Re: Colors and UI"
    date: 2007-02-01
    body: "The toolbar icons can/will change, nevermind the fact that you can disable text in the toolbars anyway.  The function of those buttons just adjusts the viewport anyway, not really changing coordinate systems. The actual adjustment when you click the button is pretty slicks - the graph smoothly slides around in the viewport.\n\nRegarding the last line, I resized the window to that point - which is much smaller than my screen.  If I had maximized the program, the graph would occupy nearly 80% of the screen space.  Just a function of reducing screenshot size for publishing.\n\nPlease keep in mind that this is a development version of the program, and that the features going in are still a work in progress.  However, if you've ever used KmPlot from KDE 3.x, you'd already understand how much of a huge improvement this is.\n\nAnd that's really the point of these articles, to show that there is progress happening for KDE 4.  Release is still many months away - we probably won't even see a beta until summer, so things can still change greatly."
    author: "Troy Unrau"
  - subject: "Whine away"
    date: 2007-02-01
    body: "Someone teach this \"programmer\" what open software development is.\n\nI'm just fed up with all this people whining and shooting at the developers everytime they show something new. Do you know what open development is? Do you think OSX's Grapher looked like that final version all the while they were developing it?.\n\nThe toolbar label issue is something talked about a thousand times already. IT IS NOT FINAL. KDE4 will have HCI guidelines (which are on the works) and we'll see how the usability theme pans out.\n\nWhy do this people come here whining all the time?. Or look at Zack's last couple of blog entries. Even when warns he was \"just trying to do something visually funky\" someone has to complain \"and what is the usability case, exactly\". Go get a life and stop whining. Ever heard what creativity is?\n\nKDE4 UI has looked much like KDE3 all this time, it is still more than half a year away and developers are still getting the core libraries right. They haven't focused on looks at all yet.\n\nSorry -, whoever you are. Your first point was valid, but the rest... \n"
    author: "Vigilant"
  - subject: "Re: Colors and UI"
    date: 2007-02-02
    body: "I agree with many of you points. However, as other persons already mentioned, you could have expressed yourself better. The last comment is stupid.\n\nIf I were you I would talk to the developers and give suggestions. And not compliance."
    author: "Lans"
  - subject: "including ball-view"
    date: 2007-02-01
    body: "hi guys,\n\nfirst of all, i'd like to say thanks for the great work on KDE-Edu. I'm a bioinformatics student at the University of Tuebingen and one of our professors is developing this program (library respectively) to view biological molecules (BALL-View) and to perform biochemical computations (BALL lib).\n\nAs it is using QT and it's GPL/LGPL I thought you might want to have a look at it:\nhttp://www.ball-project.org/Gallery\n\n\ncheers,\nChris"
    author: "chris"
  - subject: "Re: including ball-view"
    date: 2007-02-02
    body: "thanks for drawing our attention to this app, it looks impressive."
    author: "Benoit Jacob"
  - subject: "From crowded to minimalism?"
    date: 2007-02-04
    body: "The old main window was probably a bit crowded, but the pendulum seems to have swung quite far the other way.\n\nIf the font size of the symbols can't be set by the user I suggest increasing it to the size from the 3.5 version.\n\nSome people (teachers?) may also find it useful to have the mass available in the main window for all elements. Will the information that is displayed be configurable (hooray), or did you just decide it was too crowded (hoo)?"
    author: "Kjetil Kilhavn"
---
Since not all of the development for KDE 4 is in base technologies, this week features two of applications from the <a href="http://edu.kde.org/">KDE-Edu</a> team: <a href="http://edu.kde.org/kalzium/">Kalzium</a>, a feature-filled 
chemistry reference tool, and <a href="http://edu.kde.org/kmplot/">KmPlot</a>, a powerful equation graphing and visualization program. Read on for the details.



<!--break-->
<p>These educational tools have received a lot of work for KDE 4. In particular, Kalzium and KmPlot developments are happening at an amazing rate.</p>

<br />

<p>Kalzium (the German word for Calcium) has been a part of KDE since version 3.1 and is now one of the most useful applications developed by the KDE-Edu team. Initially it was just a program that displayed the periodic table, alongside some useful numbers like atomic weights, boiling points, etc. It was later extended to include a lot of background information on the elements, and more detailed chemistry information (such as emission spectra) which made it a very useful chemistry reference.</p>

<p>In KDE 3.5.5 (which I used for these screenshots, even though 3.5.6 was released last week), Kalzium looks something like this when first loaded:</p>
<p align="center"><a href="http://static.kdenews.org/dannya/vol5_355_kalzium_default.png"><img src="http://static.kdenews.org/dannya/vol5_355_kalzium_default_thumb.png" alt="Kalzium in KDE 3.5.5" /></a><br /><i>Click for fullsize.</i></p>

<p>You can see that the interface is pretty simple, and presents a lot of information. If you click on an element it brings up even more information on its properties.</p>

<p>The main user interface in KDE 4 does not look that different, except for the fact that Qt 4 introduces some appearance changes, and there are some more icons (some that haven't been drawn yet) in the toolbar. Here's a peek at Kalzium in the KDE 4 development series:</p>
<p align="center"><a href="http://static.kdenews.org/dannya/vol5_4x_kalzium_default.png"><img src="http://static.kdenews.org/dannya/vol5_4x_kalzium_default_thumb.png" alt="Kalzium in KDE 4x devel" /></a><br /><i>Click for fullsize.</i></p>

<p>So Kalzium is visually quite similar between versions at this point. However, the important thing to note in the KDE 4 screenshot is the tools menu. In KDE 3.5.5, this menu contains only Plot Data and Glossary.</p>

<p>Plot Data shows the elements plotted in a variety of useful ways, such as mass, radius, electronegativity, etc. while the <a href="http://edu.kde.org/kalzium/glossary.php">Glossary</a> shows definitions for many of the more common chemical terms. It is apparently missing the above mentioned electronegativity, so evidently there is still room for improvement here. Making improvements to the Glossary would be a great opportunity for a chemistry-inclined person to contribute to Kalzium in KDE 4 without having to be a programmer.</p>

<p>Anyway, back to the new tools. I'll focus on a few of the newly developed tools that will make Kalzium even more useful in KDE 4:</p>

<p>The isotope table will display a list of isotopes and their decay methods - as a geologist for example, it is important for me to know that Potassium-40 usually decays by electron capture.</p>

<p>The new equation solver is also quite useful, as seen in the following screenshot provided by Kalzium lead developer Carsten Niehaus:</p>
<p align="center"><img src="http://static.kdenews.org/dannya/vol5_4x_kalzium_solver.png" alt="Kalzium Equation Solver in KDE 4x devel" /></p>

<p>You basically just punch in a chemical equation leaving letters in place of the numbers you are looking for, and it spits out a response. In high school chemistry, students are expected to be able to solve these sorts of equations manually, but like most equations, once you solve enough of them, it simply becomes tedious. This equation solver can save a lot of time for complex equations.</p>

<p>And finally, the most visible change to Kalzium is the inclusion of the Kalzium 3D work, which turns the program into a 3D molecule viewer. Initially, it was developed by the Kalzium developers for use in this application only, but some collaboration has since happened and it will now be using libavogadro a library jointly developed by the Kalzium and <a href="http://sourceforge.net/projects/avogadro/">Avogadro</a> developers.</p>

<p>According to the Kalzium developers work is progressing on porting the 3D modeller to use libavagadro, an effort led by Donald Curtis, providing a more general/powerful framework for rendering/manipulating molecules with Qt and OpenGL library. It is shared between Kalzium and Avogadro (and more). Avogadro is a much more advanced molecular modelling programs, useful for creating the actual molecule files, and doing quantum chemistry. Kalzium 3D will simply act as a viewer for files constructed using these programs.</p>

<p>Kalzium developer Benoît Jacob submits the following screenshot showing the 3D molecule viewer in action using the new Kalzium 3D functionality. This functionality is already SVN as this article goes to press, however work continues with libavogadro integration.</p>
<p align="center"><a href="http://static.kdenews.org/dannya/vol5_4x_kalzium3d.png"><img src="http://static.kdenews.org/dannya/vol5_4x_kalzium3d_thumb.png" alt="Kalzium 3d in KDE 4x devel" /></a><br /><i>Click for fullsize.</i></p>

<p>Kalzium will likely ship with a library of common molecules ready to view provided by the <a href="http://wiki.cubic.uni-koeln.de/bowiki/index.php/Main_Page">BlueObelisk project</a>. Thanks to the <a href="http://openbabel.sourceforge.net/">OpenBabel</a> library, it should also be able to open molecule files in a huge variety of formats (I counted 62 file formats that it already supports).</p>

<br />

<p>On to our next KDE-Edu feature: KmPlot. For a while already, this application has had the ability to plot regular functions, parametric functions, and polar functions, as well as show derivatives (or regular functions) and a few other goodies. It has been useful as an equation visualization tool, but the interface has been awkward, with many little cluttered dialogs to fight with.</p>

<p>Below is KmPlot in KDE 3.5.5 with it's default settings, and three functions plotted, one of each type:</p>
<p align="center"><img 
src="http://static.kdenews.org/dannya/vol5_355_kmplot.png" alt="KmPlot in KDE 3.5.5" /></p>

<p>The dialogs used to plot these equations look something like this, except there is one unique dialog for each type of plot:</p>
<p align="center"><img src="http://static.kdenews.org/dannya/vol5_355_kmplot_dialog.png" alt="KmPlot dialog in KDE 3.5.5" /></p>

<p>Here's a quick look of the new KmPlot interface with the same three functions plotted. No more dialogs to mess with, and the plots can be in shapes other than square! Plus Qt 4 gives everything a nice anti-aliased touch.</p>
<p align="center"><a href="http://static.kdenews.org/dannya/vol5_4x_kmplot.png"><img src="http://static.kdenews.org/dannya/vol5_4x_kmplot_thumb.png" alt="KmPlot in KDE 4x devel" /></a><br /><i>Click for fullsize.</i></p>

<p>KmPlot has received a huge amount of work, and should be one of the KDE 4's killer apps for students, engineers, and more. It plots differential equations now, has a new equation editor, and (as seen in the above screenshot) gives tips as to how to correct your equations.</p>

<p>The new equation editor is shown below with a differential equation being edited:</p>
<p align="center"><img src="http://static.kdenews.org/dannya/vol5_4x_kmplot_editor.png" alt="KmPlot equation editor in KDE 4x devel" /></p>

<p>As you can see, it's much easier to enter an equation when you can design the functions in a nice syntax checking editor like this one. There is a lot more work going into KmPlot than I can describe in just this article, so if you are interested in more information, check out its <a href="http://edu.kde.org/kmplot/development.php">development status page.</a></p>
<br />

<p>KDE-Edu is a growing project, with many great applications being developed for a wide variety of age groups. They will have support for Windows and Mac as well, thanks to the improved QT 4 and KDE 4 libraries, and should become more popular programs as a result. Since there is so much great work happening here, expect some other KDE-Edu applications to show up in future articles.</p>



