---
title: "HIG Hunting Season in its 3rd Week"
date:    2007-05-27
authors:
  - "ereitmayr"
slug:    hig-hunting-season-its-3rd-week
comments:
  - subject: "Translators could help"
    date: 2007-05-27
    body: "i think this is something where the translators can really help. They usually see every message in the application. So they can report all of the messages that look like a mess. Most of the time translators ask what a message is supposed to mean it's a message that should be done better. (exception obscure configuration options that most user can ignore)."
    author: "martin"
  - subject: "Re: Translators could help"
    date: 2007-05-27
    body: "> i think this is something where the translators can really help.\n\nthey really _already_ help .. with.. you know.. translation."
    author: "elveo"
  - subject: "\"Root folder\" in the example"
    date: 2007-05-27
    body: "The proposed message: \"You do not have the permissions to copy to the Root folder\" instead of \"Access denied\" in the example is interesting: KDE should determine if the destination was the root folder, the home folder of an other user etc.? Many other situations can be imagined where somebody doesn't have the permission to write to a directory."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: \"Root folder\" in the example"
    date: 2007-05-28
    body: "I'm not sure if \"the permissions\" is grammatically correct. I see how it came about, but... it really looks wrong."
    author: "Chani"
  - subject: "Re: \"Root folder\" in the example"
    date: 2007-06-01
    body: "Try this instead: \"You do not have permission to copy to the Root folder\"\n\nThat seems grammatically correct"
    author: "Amy Rose"
  - subject: "Kopete is an easy target to criticise."
    date: 2007-05-28
    body: "It's _filled_ with cryptic errors and notifications that aren't even configurable via the \"Configure Notifications\" KNotify thinger.  For instance, I use jabber.org for a Jabber account, and they're not exactly the most robust provider out there, so I frequently get disconnected from them.  Even though I have disabled _all_ notifications for Kopete, I still get an annoying, blinking (via the taskbar) popup that it couldn't connect for one of several reasons (e.g., \"operation not supported\", \"connection refused\", etc.).\n\nThen there's Konqueror which has some inaccurate error messages that appear when your internet connection isn't exactly as stable as it should be (you might be torrenting something for example; this is especially bad when trying to torrent some Linux distro or other stuff on a slow ADSL connection).  No, the server _can_ be found, you just didn't try hard enough!  These errors also seem to appear if you have misconfigured MTU settings for example."
    author: "Matt"
  - subject: "Re: Kopete is an easy target to criticise."
    date: 2007-05-28
    body: "Good to see your feedback. Could you actually put this up on the wiki, so it will actually get fixed? I don't see your review on the wiki page (yet)."
    author: "Sebastian K\u00fcgler"
  - subject: "HIX"
    date: 2007-05-31
    body: "I tried to listen to a realplayer sample in amazon.com using Konqui. then I get a pop up windows which gives me the option to open it with amarok or save it or cancel it. In my opinion this save as option should be provided only as a context menu and the whole dialogue needs to get obsoleted. I took play with amarok and get the error when loading audio files, there is no audio channel. I have absolutely no idea what when wrong. As I have the realplayer installed I think using realplayer would work. Yet, no option in Konqui\n\n"
    author: "semsem"
---
Are you fed up with cryptic error messages you don't understand? Then get involved! This week's target of the <a href="http://dot.kde.org/1178743323/">HIG Hunting Season</a> is <a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Warning_Messages">warnings and error messages</a>. Read on for more details.


<!--break-->
<p>The <a href="http://dot.kde.org/1178743323/">HIG Hunting season</a> is an experiment to include the community in the search for obvious infringements of the KDE Human Interface Guidelines. This week we turn towards <i>Error and Warning Messages</i>: Most computer users know that feeling when all of a sudden, a message pops up showing cryptic error codes. Often, "confirm" is the only option it offers - and users are at a loss.</p>

<p>To avoid such situations in KDE 4, we ask all community members to report warnings and error messages that make no sense, are not understandable, make use of technical wording, are not constructive or otherwise annoying. This <a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Warning_Messages">checklist</a> lists further details and some examples.</p>

<p>As a dedicated "hunting" for warnings and error messages is difficult, we ask you to report such annoyances whenever you come across them during your daily work. We ask you to report them for both, <i>KDE 3.x and KDE 4</i> applications. If you don't have the time to report a bug immediately, create a snapshot and send it to <a href="https://mail.kde.org/mailman/listinfo/kde-usability-devel">kde-usability-devel</a>.<p>

<p><b>Reporting Procedure</b>

<ol>
<li>Whenever you find a problematic warning or error message (see <a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Warning_Messages">checklist</a> for reference), create a screenshot.</li>
<li>Open a bug in the <a href="http://bugs.kde.org">bug system</a>. In the title, write "HIG CL3:", in its body link the <a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Warning_Messages">checklist</a> and attach the screenshot. Describe the problem you found.<br>
Alternatively, send the screenshot to <a href="https://mail.kde.org/mailman/listinfo/kde-usability-devel">kde-usability-devel</a>.</li>
<li> On the <a href="http://wiki.kde.org/tiki-index.php?page=Checklist+3%3A+Warning+and+Error+Messages">wiki page</a> of this checklist, add a section for the application you reviewed and link all bug reports you have created.</li>
</ol></p>

<p>Of course you are also welcome to review KDE 4 applications according to the prior checklists:
<ul>
<li><a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Configuration_Dialogs">Configuration Dialogs</a></li>
<li><a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Text_and_Fonts">Text and Fonts</a></li>
</ul></p>

<p>Happy hunting!</p>

