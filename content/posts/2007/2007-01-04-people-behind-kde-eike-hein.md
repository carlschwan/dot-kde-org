---
title: "People Behind KDE: Eike Hein"
date:    2007-01-04
authors:
  - "dallen"
slug:    people-behind-kde-eike-hein
comments:
  - subject: "Photo"
    date: 2007-01-04
    body: "Oh, come on ... Southpark avatars are ok for blogs, but \"People Behind KDE\" IMHO should allow a quick peek into the real life of developers. So unless you're completely disfigured and don't have the guts to show your face that means: We would prefer a real photo ..."
    author: "southpark phantom"
  - subject: "Re: Photo"
    date: 2007-01-04
    body: "Whether someone wants to post a pic or not is none of your business really.  \n\nGood interview, I like these stories.  Gives a more personal feel to the project and lets people know who's working on what."
    author: "Leo S"
  - subject: "Another good interview!"
    date: 2007-01-04
    body: "I use both Konversation and Yakuake every day - love them both!\n\nI'm using Kubuntu now, but I have used Gentoo some years ago - Kuroo looks amazing!"
    author: "Joergen Ramskov"
  - subject: "Germany is KDEs hartland?"
    date: 2007-01-04
    body: "I think labeling Germany as the heartland of KDE is a bit presumptuous. Surely KDE contributers are spread out all over the world.\n\nOtherwise, the interview was a fine read. Eike Hein has a artful style of writing. Maybe we can expect a novel some day?\n"
    author: "EP"
  - subject: "Re: Germany is KDEs hartland?"
    date: 2007-01-04
    body: "I'm fine with giving the Germans the credit they deserve! Let's all do our best to outdo them in the next year, though."
    author: "Martin"
  - subject: "Re: Germany is KDEs hartland?"
    date: 2007-01-04
    body: "Though of course KDE contributors span the globe, Germany has long been regarded as the \"heartland\" of KDE - the project started and had its early years dominated by the country. Even in the present day, take a look at the concentration of contributors here: http://worldwide.kde.org/pics/fullmap-nonames.jpeg\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Germany is KDEs hartland?"
    date: 2007-01-05
    body: "thanks for the link. I just added myself :)"
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: Germany is KDEs hartland?"
    date: 2007-01-05
    body: "Why are there so few KDE people on the American west coast?"
    author: "Martin"
  - subject: "Congrats Danny!"
    date: 2007-01-05
    body: "Kudos to Danny for taking over The People Behind KDE interviews. \nI'm pretty sure that under your direction they will be as successful as all your other KDE work!\n\nCongrats on your first and wonderful interview!\n\n"
    author: "Tink"
  - subject: "problem related issues"
    date: 2007-01-25
    body: "Hi,\nI have switched over to ubuntu from xp very recently. I am finding a lot of material on line too. I have recently installed kopete and am finding it extremely hard to get any help from KDE. Problem is this - \n1. It does not show me the handbook\n2. If i am to use the webcam, i am not able to chat with my webcam. \n\nIs there anyone whom i can contact or get suggestion from? Please let me know.\n\nregards,\nvenkatesh."
    author: "Venkatesh"
  - subject: "Re: problem related issues"
    date: 2007-05-26
    body: "You should try installing kbuntu. This will help you:\n apt-get install kubuntu-desktop\n\nTry after that, and report if the documentaion is not showing up."
    author: "Anonymous bin Ich"
---
In a brand new series of <a href="http://behindkde.org/">People Behind KDE</a> we meet a coder from the KDE heartland, Germany who enables us to communicate with the global developer community through Konversation. Someone who is not satisfied with a static terminal window, tonight's star of People Behind KDE is <a href="http://behindkde.org/people/hein/">Eike Hein</a>.


<!--break-->
