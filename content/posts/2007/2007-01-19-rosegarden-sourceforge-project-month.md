---
title: "Rosegarden is SourceForge Project of the Month"
date:    2007-01-19
authors:
  - "jgaffney"
slug:    rosegarden-sourceforge-project-month
comments:
  - subject: "Rosegarden"
    date: 2007-01-20
    body: "I have used NoteEdit for sheet music so far but I don't really like its interface so far (but the results are very impressive, that's why I did choose it).\n\nSo I'd be really interested to know which strengths and weaknesses Rosegarden has in the field of sheet music composing. I just used it ocasionally for sample editing (but must admit that I didn't use it regularly).\n\nI'm really deeper interested in Rosegradden as I am searchig for an integrated environment for all kinds of music editing (recording, sound generation, samplin and sheet music).\n\nKeep up the good work."
    author: "Arnomane"
  - subject: "Thank you"
    date: 2007-01-20
    body: "Thank you for the great software!"
    author: "User"
  - subject: "Linux Audio Software"
    date: 2007-01-20
    body: "Rosegarden, the name fills my heart with incredible joy. It was in 2000, the early beginning of my study, when I first saw the name Rosegarden (it was rg2 back than) on a Solaris machine but some time later rg4 with a 0.X.X release on a Linux machine. I fell immediately in love and subsequently began to banish Windows from my computer while substituting with Linux. A typical story I think. However, I never succeeded to make real music on that machines though everything was there. The whole attempt ended because I allways thought this and that feature is missing. Oh dear how wrong was I. It was so stupid to compare it with Cubase and other tools. Of course there will always be something we can call gab. But I totally forgot the music while being to much concerned about \"important\" technical issues. Being 7 years older and somehow wiser I see that Rosegarden4 1.4.0 is matured and an incredible tool. Stevie Wonder called the GX-1 from Yamaha \"The Dream Machine.\" I say Rosegarden 4 is a \"Dream Machine\" too. Enjoy making music with it as I do.\n\nBut there is something I always wondered about. You know MusE^1? No? That's strange. Both tools are comparable. Both are Qt apps (not sure if MusE also depends on KDE libs). Both are MIDI/Audio sequencers. Both use jack. Both handle LADSPA. Both handle software soft-synths, external devices and so on. Try to compare them (^2). The only real difference which I (as an average user!) recognize is that MusE has an automation architecture for both midi and audio which rg4 leaks (no problem here, since there is a workaround for rg4 see ^3). Why is MusE less popular and is there a reason why both project never joined forces (technical implications ...)? I mean it as a serious question (I'm not a \"feature hunter\"!). I guess there is simple reason why Werner Schweer started MusE (e.g. he just wanted to ;) ). Ideas?\n\n-------------\n1:   http://www.muse-sequencer.org/\n2:   http://www.rosegardenmusic.com/tour/ vs. http://www.muse-sequencer.org/wiki/index.php/Features\n2:   http://sourceforge.net/mailarchive/message.php?msg_id=37846607"
    author: "pete"
  - subject: "Re: Linux Audio Software"
    date: 2007-01-21
    body: "And I'm sure both are equally impossible to set up!\n\nI never did work out how to set up the soundfont, LADSPA, JACK, ALSA, MIDI and so on.\n\nAnd supposedly rosegarden can wave audio, but I could never find anything at all in the menus that referred to it at all.\n\nI remember cakewalk - you didn't have to set anything up at all! MIDI on linux is a mess."
    author: "Tim"
  - subject: "Re: Linux Audio Software"
    date: 2007-01-21
    body: "Your point is valid.  However, it's hard to fault the excellent developers of either of these apps.  They both rely on a lot of reverse engineering and home-grown driver workarounds.  That's because hardware vendors don't provide a CD for Linux users to insert and run to get all of our devices magically working.  I had to do a lot of Googling to get my MIDI working, but I'm willing to put in the extra work so I can use free/open software.  If MIDI out-of-the-box is a requirement for you, then Linux is going to disappoint."
    author: "Louis"
  - subject: "Re: Linux Audio Software"
    date: 2007-01-21
    body: "The simple way around this is to use a Linux distribution which did the hard work for you. To name a few.\n\n* Musix (Live-CD) which is still in development but already useable (http://musix.org.ar/en/index.html)\n* Studio to Go! Seems the be of good quality and worth the price you have to pay for(http://www.ferventsoftware.com/)\n* 64Studio. Very polished, but the current 1.0 has some problems. I have to run alsaconf and modprobe snd_seq (as root of course) before I can start. But these problems seem to be solved in the coming release of 64Studio (http://64studio.com/)\n\nHowever, there is more to be found on http://linux-sound.org/distro.html \nlinux-sound.org is BTW a very good place to start from"
    author: "pete"
  - subject: "Re: Linux Audio Software"
    date: 2007-01-22
    body: "Firstup -- Alsa should be set up automatically for you on a modern distro and should work out of the box. Ladspa shouldn't require any setup either.\n\nThe getting soundfonts to run on machines that support hardware soundfonts is a little tricky (though I think you can get Rosegarden to do this for you). For machines that don't you can simply use qsynth... just route your midi outs into qsynth's midi ins. (in rosegarden you can just use the soundfont based softsynth as well)\n\nJack can be a little tricky, you really want it to be able to run with realtime privelages on Debian based systems the easiest way to do this is to install the realtime-lsm package. You will then need to install qjacktctl. (this program allows you to start and control jack)... If you are in the unfortunate position of having a sound card that does not support hardware mixing then there are a few more issues you might bump into.\n\nFor routing Midi and Jack connections (jack works like midi but for audio streams) I recommend an application called patchage...\n\nAs for Rosegarden and Audio you need to have jack running for this part of the interface otherwise it wont show any of the audio options.\nIf you right click on a track you should be able to make that track midi, softsynth or audio... also you will have access to the \"manage Audio files....\" option in the segments menu...\n\nHope that helps :)"
    author: "Danni Coy"
  - subject: "Re: Linux Audio Software"
    date: 2007-01-22
    body: "KDE4 has been praised to solve many multimedia related problems. I don't know the details much thought.\nIs it going to solve the problems mentioned here? That would be nice!\n\nI tried installing Rosegarden and Muse some time ago and failed badly. This Jack driver messed my whole machine.\nThis audio / multimedia driver stuff in Linux is currently a mess, not on the same level with how nicely other parts of the system work after installing a good distro.\n\n\nJuha Manninen\n"
    author: "Juha Manninen"
  - subject: "Re: Linux Audio Software"
    date: 2007-01-23
    body: "\"KDE4 has been praised to solve many multimedia related problems. I don't know the details much thought.\n Is it going to solve the problems mentioned here? That would be nice!\"\n\nNo, unfortunately not. It ill help to improve some audio apps but that's it. The problems mentioned here are due to real-time issues of the kernel and poor midi/audio-handling (via alsa).\n\n\"I tried installing Rosegarden and Muse some time ago and failed badly. This Jack driver messed my whole machine.\"\n\nSorry to here. Did you follow any guidelines? Try to find some information at linux-sound.org. This really a good place to start from.\n\n\"This audio / multimedia driver stuff in Linux is currently a mess, not on the same level with how nicely other parts of the system work after installing a good distro.\"\n\nYes and no. I'm running 64Studio (Linux distribution) and it works like a charm. The same applies to musix.org.ar. Midi and audio is up and running. Rosegarden and qjackctl (http://qjackctl.sourceforge.net/) take over the ruling parts. DSSI (http://dssi.sourceforge.net/), hydrogen (http://www.hydrogen-music.org/), zynaddsubfx (http://zynaddsubfx.sourceforge.net/), jackrack (http://jack-rack.sourceforge.net/) and so on do the fine tuning. It's sometimes hard to use but overall it's working fine. I can connect and rout everything I want and that's great.\n\n\n \n"
    author: "pete"
  - subject: "No automation?"
    date: 2008-11-25
    body: "Uggh, so I've been wanting to get into rosegarden thinking that it would be awesome to have a good Linux music making machine. \n\nBut...I've been hearing there is no automation!?!?! Thats a pretty big thing to lack. THis can't be true right? \n\nAnd also, pre-fader efffects, it it true it lacks that too? \n\nBoth pretty big deals. I thought Mario Paint had automation. "
    author: "Baruch Lawrence"
  - subject: "Re: No automation?"
    date: 2008-11-25
    body: "Oh, so I guess I got some misinformation...sorry. "
    author: "Baruch Lawrence"
---
<a href="http://www.rosegardenmusic.com/">Rosegarden</a>, a powerful KDE based professional music composition and editing     
environment, has been named <a href="http://sourceforge.net/potm/potm-2006-12.php">SourceForge project of the month</a> for December.      
With a notation editor, event editor, intuitive MIDI Studio concept, vast       
mixing and plugin capabilities, Rosegarden provides the audio professional      
with the tools they need for a complete Digital Audio Workstation.  In the    
article, the programmers discuss how the project started, the intended          
audience, and the defining moment for Rosegarden as a widely used and           
important application.  Congratulations to the Rosegarden team!                       


<!--break-->
