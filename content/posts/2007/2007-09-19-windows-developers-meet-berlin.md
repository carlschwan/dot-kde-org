---
title: "Windows Developers Meet in Berlin"
date:    2007-09-19
authors:
  - "hschr\u00f6der"
slug:    windows-developers-meet-berlin
comments:
  - subject: "Congrats!"
    date: 2007-09-18
    body: "Thank you guys! Hopefully your project will bring more people into KDE :)"
    author: "dc"
  - subject: "Re: Congrats!"
    date: 2007-09-20
    body: ".. and that it will not make more developers go out"
    author: "***"
  - subject: "Krita arrived as well"
    date: 2007-09-18
    body: "And Krita now <a href=\"http://www.valdyas.org/fading/index.cgi/2007/09/18#kritawin\">builds and runs</a> on Windows as well! "
    author: "A KDE Advocate"
  - subject: "hooray"
    date: 2007-09-18
    body: "Great. :)\n\nLast time I tried building Amarok on Windows (back in May) I got stuck compiling on the step for QtDbus. \"Emerge\" scripts are a good idea."
    author: "Ian Monroe"
  - subject: "Re: hooray"
    date: 2007-09-19
    body: "You will have not much even if it compiles - there is no backend yet.\nbut perhaps you check http://windows.kde.org from time to time, we might put some more information there."
    author: "saroengels"
  - subject: "Re: hooray"
    date: 2007-09-19
    body: "Yes no phonon backend for windows... yet.\nBut TrollTech is going to provide one with QT 4.4...\n( see http://vir.homelinux.org/blog/index.php?/archives/45-Phonon-Trolltech.html )"
    author: "shamaz"
  - subject: "Re: hooray"
    date: 2007-09-19
    body: "I'm sure the void backend works just fine."
    author: "Ian Monroe"
  - subject: "Re: hooray"
    date: 2007-09-19
    body: "But some people expect a music player to... uh... actually _play music_! ;-) Always those over-demanding users. ;-)"
    author: "Kevin Kofler"
  - subject: "Mmmm"
    date: 2007-09-19
    body: "I dont really know what to make of all this. I'm watching this development for quite some time now and there are always the two opposed points\na) This helps getting Win users used to Linux software, and they will switch at some point.\nb) This helps keeping Win users where they are, because they get the advantages from Linux for free.\n\nJudging by my Windows-using colleagues I'm afraid it's rather b), because they have all the apps they are used to and Kate and K3B and so on, on top. A shame - this comes at a time where I found Linux software finally in many aspects superior and colleagues often looked enviously on Kate, K3B or Amarok. \n"
    author: "Michael"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "Well, for me, having KDE on Windows means:\n\n1) being able to use KDE software while I'm at work - because they don't let us use Linux and I get frustrated by Windows' lack of various things. (Although I wish KDE on Windows could also mean KWin, but alas not.)\n2) having my wife be able to use KDE software on her laptop. She wants to use Linux but doesn't think it has all the software she needs, so if she switches to KDE software eventually she'll realise she doesn't need Windows any more. And she really wants the new KDE4 user interface look, which will probably persuade her the most though.\n"
    author: "James"
  - subject: "Re: Mmmm"
    date: 2007-09-20
    body: "Same here.\nI will be able to use a decent development enviroement (quanta and kdevelop) in windows for free at work where I have to use windows because of the damn outlook-exchange.\nAlso, I can spread open-source apps like quanta, kate, amarok and krita to friends, and I belive that with time they will yes see that they don't *need* windows, as their prefered applications (add firefox here) just run on linux better than in windows, so why should they keep using it?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Mmmm"
    date: 2007-09-20
    body: "> as their prefered applications (add firefox here) just run on linux better than in windows, so why should they keep using it?\n\nWith more windows developers, apps will work better on the other platform. For example, Firefox works already better on Windows than on Linux, except for a few platform-specific security bugs, which are not a visible problem for most users.\n\nAlso, users keep using what they are used to, you are underestimating greatly the resistance to change (inertia law does apply here).\n\nGeeks should not only study marketing, but economics.\n"
    author: "***"
  - subject: "Re: Mmmm"
    date: 2007-09-21
    body: "Well, Firefox never ran particularly wonderfully on Linux - in fact, it was originally started as an alternative to Seamonkey on Windows, simply because of how slow and bulky that one felt on there. That it also runs on Linux is just a fluke, really ;) (yeah, i know it's not entirely that simple, but that /is/ why they made Firefox to start with)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Mmmm"
    date: 2007-09-20
    body: "And to me it means koffice and konqueror on windows.  Not that I would have occasion to take advantage of that, but the more people who use koffice the better, so we aren't stuck with openoffice for cross platform officing if we don't like it.  And the more people who use khtml the better, as perhaps web designers might actually make sure their sites work with it, or at least don't deliberately block it."
    author: "MamiyaOtaru"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "Well what's so bad if you run Free Software on Windows? Why should everyone switch to GNU/Linux?\nI don't think that there is only a or b. For me it is also marketting, saying the world that you are here and that you can test it whenever you want. Without a risk. You could also win some programmers.\n\nI don't see benefits in forcing people to use GNU/Linux if they want to run free software.\nIf you think about it one most if not the most succesful free software apps is Firefox. Actually because (!) it can run on most platforms.\n\nPS.: You know what keeps a lot Win users where they are? Lack of OpenGL games. If most games still use DirectX MS has locked in a lot customers.\nPPS.: Don't mention Tuxracer, UrbanTerror, UT2k4 ... I know them all and they are a minorty."
    author: "mat69"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "Point is that Windows is proprietary and evil, and as a platform, will never be anything else than hostile and restricting to us and it's own users.\n\nOf course, it's not that horrible, but it's not all shiney on the win platform either. I'm for supporting it, but against investing too much in it. And I support Aaron when he wants to ensure KDE works better and has more features on Linux/FOSS. We are a FOSS project, we're part of an ecosystem. We need to support that ecosystem if we are to grow and prosper."
    author: "jospoortvliet"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "> And I support Aaron when he wants to ensure KDE works better and has more features on Linux/FOSS\n\nHow should this work ? Should windows developers having good ideas only be allowed to implement this for unix users or to fix bugs only for unix using #ifdefs ? \n\nThey will contribute code only if they could use it on windows. \n\n\n"
    author: "Ralf Habacker"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "No, we just won't go out on a limb to get windows to support stuff it doesn't have the proper architecture for."
    author: "jospoortvliet"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "DirectX isn't the problem. Wine comes with a DirectX 9 implementation which seems quite mature to me (using 0.9.42). I tried the German strategy game \"Anno 1701\" (known as \"1701, A.D.\" or such in English-speaking countries) on a plain Wine + Winetools (i.e. no Cedega). It went into fullscreen mode (rendered with DirectX) but failed because of missing copy protection drivers. These refuse to install on Wine (with meaningful messages like \"Error 2\"). I haven't tried other games, both my games collection and my spare time is too small-sized, but I think that what we need most is copy protection drivers (i.e. support for Wine from the big copy protection manufacturers)."
    author: "Stefan"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "You yourself should know then, that even though Wine is a great program it still does not support most games (a \"playable\" way --> normal fps, sound, music, multiplayer).\nIt's not like you could buy a game without looking in the AppDB first and being sure that it'd work."
    author: "mat69"
  - subject: "Re: Mmmm"
    date: 2007-09-20
    body: "I most of the times use the pirate, try, buy method.\nwine is great, but it just has a few gaps to much.\nnot to say that it hasn't improved greatly over time."
    author: "Mark Hannessen"
  - subject: "Re: Mmmm"
    date: 2007-09-20
    body: "I think Wine has improved a lot over the years. But the task the Wine team set out to achieve is enormous too. My thanks go to wine for every time it spares me rebooting while running Linux!"
    author: "Joachim"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "It means that the supporting infrastructure of the free desktop doesn't get the attention it needs.\n\nAny significant application depends on the stack below it to provide services to any number of things; hardware, IPC, etc. The comment above refers to graphics services.\n\nWindows provides a very good infrastructure. Libraries are available for almost anything. That is not the case for Linux, yet. Many of the lower level services are incomplete or out of date, ie. not supporting things like HAL or dbus. The drive to keep them up to date are consumers. The consumers in this case are application developers, who more often than not send patches to fix what they need in the library.\n\nThis is a typical situation where end users, who for the most part contribute very little or nothing, drive development. We will see how it works out.\n\nDerek"
    author: "D Kite"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "That sounds convincing. Though I don't believe that this will become a problem, as if the user is satisfied by the free software there would be no reason to assume that a free OS would be worse, especially if many people point out the advantages of Gnu/Linux.\n\nImo it's like a freebie you give away to gain new consumers. If it tastes well why not try the rest of the production line or why not trusting that label?\n\nHopefully it works out good."
    author: "mat69"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "From purely selfish point of view of someone who likes combination of KDE and Linux. Why would I want to force users into using it? Because more Linux users mean more Linux applications and better driver support. Simple as that.\n\n> PS.: You know what keeps a lot Win users where they are? Lack of OpenGL \n> games. If most games still use DirectX MS has locked in a lot customers.\n\nAnd what is the reason? Lack of Linux gamers again - if there was sufficient economic motivation, the \"lock in\" would not be a problem. After all this is all matter of investments and expected returns."
    author: "JS"
  - subject: "Re: Mmmm"
    date: 2007-09-20
    body: "That's like the hen and egg problem (meaning circular argumentation).\n\nYou could also say that people will only switch if their applications are supported, while companies insist that they will only port if enough people have switched allready.\nA vicious circle that luckily proved to be wrong on so many occasions (where would we be if it was true).\n\nImo what it boils down to is marketing.\nYou can sell so much bs because of good marketing. Imo marketing was long time something ignored by free software programers, as I guess they have a more technical approach to things. Companies like MS understood that it's marketing what sells products, not necessarily good products."
    author: "mat69"
  - subject: "Re: Mmmm"
    date: 2007-09-20
    body: "Oh, let's think the other way round one moment, what is so good in supporting Windows ?\nThe code base increase (which means more bugs) ?\nLimiting the range of possible optimizations on Unix-like platforms ?\nPostponing the releases for supporting extra platforms instead of the intended one ?\nHaving more developers from different platforms and increase the likeliness of conflicts ?\n\nFunny you should mention Firefox, one of the most terrible (free) code base ever (resource consumption, security, etc). I guess our definitions of success are different.\n"
    author: "ita_est"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "First of all:\na) it would mean that KDE apps compete on a much larger market.\nb) I don't think that the current Amarok has any chance against iTunes, esp. because its web radio support is very inconvenient.\nc) a larger user base does not harm an application.\nd) the more you rely on Open source then less dependent you are on the underlying platform. When you use K3B on Windows your personal NERO lock-in will not block a switch.\ne) Let's face it: the lockin is directx + Office + the Adobe tools. Linux is better when you don't need these. And directx support gets better, the office monopoly is weakening and the adobe tools could be ported for professionals as graphic specialists need strong horsepower and Linux is the better platform for the mainframe."
    author: "Menne"
  - subject: "Re: Mmmm"
    date: 2007-09-19
    body: "Well your colleagues K3B envy will still continue, K3B are one of the applications most unlikely to get a Windows port. \n\nAs a frontend using tools like cdrecord, cdrdao, mkisofs and cdrtools, tools not having(or having limited) windows versions. Those tools are also closely linked too how *nix system function, making a port very hard. So the likelyhood for a port are small, since K3B are raterher useless without those underlying programs."
    author: "Morty"
  - subject: "Re: Mmmm"
    date: 2007-09-20
    body: "All of those programs are available on windows and are reported to work.\nIt's also not too hard to access a CD burner directly."
    author: "PutHuhn"
  - subject: "good job but slow window resizes"
    date: 2007-09-19
    body: "congratulations to the great progress you are making. i believe it's very important that we support the win32 platform. keep up the good work.\n\ni just tested some apps (kate, kwrite, ...) on my winXP and they worked quite good, but unfortunately when i resize the app window, this is really. almost as slow as the java window resizes. where does this come from? is it qt because it needs to emulate the windows UI?\n\n"
    author: "Fabian"
  - subject: "Re: good job but slow window resizes"
    date: 2007-09-19
    body: "Qt does not emulate anything. It's all native code. The reason why it's to slow is maybe due to the use of debug libs instead high optimized release ones."
    author: "Christian Ehrlicher"
  - subject: "Re: good job but slow window resizes"
    date: 2007-09-20
    body: "maybe it's also because in KDE4 nearly every graphic is an svg(z) and so it needs to get resized."
    author: "LordBernhard"
  - subject: "Re: good job but slow window resizes"
    date: 2007-09-21
    body: "> maybe it's also because in KDE4 nearly every graphic is an svg(z) and so it needs to get resized.\n\nThat's only true for kdegames. Kate and Kwrite don't use svg for graphics."
    author: "cloose"
  - subject: "Re: good job but slow window resizes"
    date: 2007-09-21
    body: "It's because of the unfortunate way Qt has handled widgets so far - this, however, changes with the next minor version (i seem to remember that it won't matter for KDE 4.0 unfortunately, but 4.1 should do wonders there). There was an entry on the topic somewhere on the Qt developers' blog, but... i can't remember exactly where, i'm afraid (and right now i'm too tired to go looking)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: good job but slow window resizes"
    date: 2007-09-21
    body: "If you're referring to this:\n\nhttp://labs.trolltech.com/blogs/2007/08/09/qt-invaded-by-aliens-the-end-of-all-flicker/\n\nthen this won't speed it up a all - it simply removes the flicker.  *Many* people have remarked on the sluggishness of Qt4 resizing now, so it's obviously something introduced in Qt4 that the Trolls are going to have to investigate."
    author: "Anon"
  - subject: "Why i only create kernel drivers/in kernel softw  "
    date: 2007-09-25
    body: "Because linux, outside of the kernel, offers nothing unique. And even inside of the kernel it is just a unix clone. Hopefully plasma will be really cool, and very Dependant on X."
    author: "John"
---
During the last weekend, the <a href="http://windows.kde.org">KDE on Windows</a> developers conducted their second
real life meeting in the <a href="http://www.trolltech.com">Trolltech</a> offices in Berlin Adlershof, incorporating new developers and improving infrastructure. Read on for more details.





<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/kdewinmeeting07.jpg" border="0"><img src="http://static.kdenews.org/jr/kdewinmeeting07-wee.jpg" width="400" height="160" /></a><br /><small>
From left to right: Cornelius Schumacher (KDE e.V.), Daniel<br />Molkentin (Trolltech), Patrick Spendrin, Bernhard Loos,<br />Holger Schröder, Christian Ehrlicher, Ralf Habacker,<br />Torsten Rahn, Jaroslaw Staniek</a>. (<a href="http://static.kdenews.org/danimo/kdewinmeeting07.jpg">Click to enlarge)</a></small></div>

<p>On Friday evening, the participants were welcomed by the Berlin Trolls and introduced to the office. After a nice meal at a local restaurant, the group evaluated the main working areas of the participants and created a meeting roadmap.</p>

<p>Saturday started with discussions about Ralf Habacker's installer, the autobuild system by Patrick Spendrin and Holger Schröder's emerge scripts. 
The KDE installer will be used by users to install KDE components and application on their computers. Christian Ehrlicher was busy switching the build process to the Python-based emerge scripts (unrelated technically to Gentoo emerge, but performing a similar function) which are used to create binary packages from the sources in a convenient and reproducable way. The autobuild system will be used to check the status of different packages for fitness on the Windows platform on a daily bases.

<p>The hackers goal is to integrate all tools into a complete solution to build and distribute KDE on Windows, thus obsoleting the
<a href="http://techbase.kde.org/Getting_Started/Build/KDE4/Windows">fairly complex build description</a> on KDE TechBase.</p>

<p>Meanwhile, Jaroslaw Staniek was able to get <a href="http://www.kdedevelopers.org/node/2997">KWord running</a> natively on Windows.</p>

<p>On Sunday, KDE e.V. treasurer Cornelius Schumacher dropped by for a short chat, while the group continued hacking and improved their skills on the Trolltech kicker table.</p>

<p>The meeting lead to an important milestone for KDE on Windows as the team will soon be able to provide a stable build and installation infrastructure in order to make it as easy as possible for new developers to get started with KDE on Windows. With the new installer, getting started with a native KDE will only be a matter of a few clicks.</p>

<p>Thanks go to <a href="http://www.trolltech.com">Trolltech</a> for providing the team with a meeting location and broadband internet as well as to <a href="http://ev.kde.org">KDE e.V.</a> for sponsoring travelling costs and accommodation for a very productive meeting. 




