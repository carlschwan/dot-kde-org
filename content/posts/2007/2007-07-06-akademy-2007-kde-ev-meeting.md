---
title: "aKademy 2007: KDE e.V. Meeting"
date:    2007-07-06
authors:
  - "jpoortvliet"
slug:    akademy-2007-kde-ev-meeting
comments:
  - subject: "the king is dead... long live the king !"
    date: 2007-07-06
    body: "congratulations to our new president :)"
    author: "shamaz"
  - subject: "Re: the king is dead... long live the king !"
    date: 2007-07-06
    body: "Hail to the King!"
    author: "Erunno"
  - subject: "Wrong Day"
    date: 2007-07-06
    body: "KDE e.V. meeting was on Monday, not Tuesday."
    author: "Anonymous"
  - subject: "Where is the nerds?"
    date: 2007-07-06
    body: "That five guys at the first photo don't look like geeks or nerds....!\n\nWhere are the KDE nerds.....?"
    author: "Nobody"
  - subject: "Re: Where is the nerds?"
    date: 2007-07-06
    body: "They are at aKademy...\n"
    author: "Fred"
  - subject: "Re: Where is the nerds?"
    date: 2007-07-06
    body: "> That five guys at the first photo don't look like geeks or nerds....!\n\nBathing in the warm glow of our laptop screens in the hacking labs :)"
    author: "Robert Knight"
  - subject: "Re: Where is the nerds?"
    date: 2007-07-06
    body: "You should know that the typical KDE hacker looks far more attractive than your average nerd :P"
    author: "Andreas"
  - subject: "Re: Where is the nerds?"
    date: 2007-07-06
    body: "I'm an average nerd, you insensitive clod!\n\nWhoops, wrong dot :P"
    author: "woogs"
  - subject: "Re: Where is the nerds?"
    date: 2007-07-06
    body: "The group photo would perhaps be a better representation - I'm sure that will go up shortly if it hasn't already."
    author: "Troy Unrau"
  - subject: "Re: Where is the nerds?"
    date: 2007-07-06
    body: "The group photo is in the \"First Impressions\" article (http://dot.kde.org/1183306162/)\nOr, go to http://static.kdenews.org/danimo/akademy07/group-photo.html for the interactive thingy :)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Where is the nerds?"
    date: 2007-07-07
    body: "Look like a bunch of geeks to me. :P"
    author: "Ian Monroe"
---
<a href="http://akademy2007.kde.org/">aKademy 2007</a> goes on. Monday was filled mostly with the <a href="http://ev.kde.org/">KDE e.V.</a> meeting, and finished with a social event at the city chambers. Read on for the aKademy 2007 e.V. meeting report.




<!--break-->
<h3>e.V. Meeting</h3>

<p>Officially, KDE is represented by the KDE e.V. which is located in Germany. The meeting started with general housekeeping tasks, followed by reports from the e.V. departments and the working groups. With the departure of <a href="http://people.kde.org/eva.html">Eva Brucherseifer</a>, our long-standing president of the KDE e.V. for the past 5 years, the assembly elected a new board member. KDE e.V. would like to take this oportunity to thank Eva again for all the great work she performed during this time of great change within the KDE community and technological landscape, and we are pleased to hear that she will continue to contribute to KDE. The new elected board member is <a href="http://en.opensuse.org/User:Kfreitag">Klaas Freitag</a>. After internal private discussion within the board, it was decided that Aaron Seigo will assume the presidency of the KDE e.V. board.</p>

<a href="http://static.kdenews.org/dannya/akademy_ev.jpg"><img src="http://static.kdenews.org/dannya/akademy_ev_small.jpg" alt="KDE e.V. board, 2007" title="KDE e.V. board, 2007"></a>

<p>The assembly also discussion of a Research working group, proposed by e.V. member <a href="http://elektron.et.tudelft.nl/~cmlot/index.html">Claire Lotion</a>. The purpose of a Research working group would be to assist third parties who would like to collaborate with KDE on research-related topics, and make it easier to apply for and collect research grants for KDE-related projects. Many in the audience expressed interest in this, and several volunteered for the group. A kde-research mailing list</a> has already been set up for this group.

<p>Another topic was the hiring of an administrative person to help with the growing admistrative burden on the e.V. board in KDE, such as corporate sponsorship and conference organisation. We had invited the Vice Chairman of the GNOME Foundation, <a href="http://www.anne-oestergaard.dk/">Anne Østergaard</a>, to share their experiences with such a position. It was decided to proceed with the preparation of hiring someone for this position. This person will become the first direct employee of KDE e.V. Some of the other topics of discussion included a KDE statement on software patents, licensing, and membership procedures.</p>

<p>As many attendees at aKademy are not members of the KDE e.V., much hacking and unscheduled meetings took place in the other building in the meantime.</p>

<br />

<h3>The Lord Provost and the City Chambers</h3>

<p>After the e.V meeting had concluded approaching 6 pm, we moved to the other building to hack and talk until it was time to start walking to the city chambers. The <a href="http://akademy2007.kde.org/contact.php">aKademy Team</a> had arranged a meeting with the <a href="http://www.glasgow.gov.uk/en/YourCouncil/Council_Committees/LordProvostsSecretariat/">Lord Provost of Glasgow</a>. The building was very impressive, and we were told that it was built in 1888, and time had clearly not depreciated its beauty. The Provost welcomed us to Glasgow, and expressed his interest in our work. After his speech, we proceeded to the food, generously provided by our friends at <a href="http://trolltech.com/">Trolltech</a>.</p>

<img src="http://static.kdenews.org/dannya/akademy_provost.jpg" alt="Lord Provost of Glasgow" title="Lord Provost of Glasgow">

<p>After the food (which was very good), many of us proceeded into the town for a few drinks, celebrating the hospitality of Glasgow. It had been another busy day, and even though much of the day was taken up by the e.V. meeting, we ended up with over 320 commits (up from approximately 200 on Saturday and Sunday).</p>



