---
title: "LWN.net: Marble Puts the Whole World on Your Desktop"
date:    2007-10-03
authors:
  - "rjohnson"
slug:    lwnnet-marble-puts-whole-world-your-desktop
comments:
  - subject: "Correction"
    date: 2007-10-03
    body: "OpenStree*t*Map.\n\n(Please, please, please delete this comment after the correction is made)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Correction"
    date: 2007-10-03
    body: "I thought I read it as OpenStreetMap :)"
    author: "jasper"
  - subject: "Re: Correction"
    date: 2007-10-03
    body: "I thought I copied it as OpenStreetMap :) oops"
    author: "Richard Johnson"
  - subject: "Congrats to takat and the gang... (NT)"
    date: 2007-10-03
    body: "..."
    author: "Odysseus"
  - subject: "Great news, I was wondering about Marble"
    date: 2007-10-04
    body: "I remember the developers talking about trying to find a balance between having a large installation file or grabbing things from a server. Its great to hear they've settled on a small dataset by default, but can be expanded through several different resources. I'd like to see more info on the default dataset though."
    author: "Skeithy"
  - subject: "Re: Great news, I was wondering about Marble"
    date: 2007-10-04
    body: "What exactly do you want to know? All the information about the source of the data is described at:\n\nhttp://www.kde-apps.org/content/show.php/Marble+-+Desktop+Globe?content=55105"
    author: "Torsten Rahn"
  - subject: "Marble is great"
    date: 2007-10-04
    body: "I just emerged marble (version 0.3) and I got to say that is is pretty cool.\nBut I do have a question about zooming in and out using a scroll mouse\n\nI noticed that scrolling upwards zooms you out and scrolling downwards zooms you in. I don't you if there are other people like me but i expected it to be reversed. I also checked it against google eaths behavior and they do have to reversed behavior.\n\nEven though you are not required to confirm to google earth, I think that it would be nice to have these applications behave the same way on this aspect. Since that is probably also what most people would expect it to do.\n\nOtherwise its a pretty decent application."
    author: "Mark Hannessen"
  - subject: "Re: Marble is great"
    date: 2007-10-04
    body: "> I noticed that scrolling upwards zooms you out and scrolling downwards \n> zooms you in. I don't you if there are other people like me but i \n\nPlease use Marble version 0.4.3:\n\nhttp://www.kde-apps.org/content/show.php/Marble+-+Desktop+Globe?content=55105\n\nIn 0.4.3 version Marble zooming was made compatible with KStars. Therefore I guess the issue is fixed there.\n\nTorsten"
    author: "Torsten Rahn"
  - subject: "Re: Marble is great"
    date: 2007-10-04
    body: "There isn't an ebuild for 0.4.3 in portage yet, but I'll surely try it out as soon as it is out."
    author: "Mark Hannessen"
---
<a href="http://lwn.net">LWN.net</a> has taken a look at the world via <a href="http://edu.kde.org/marble/">Marble</a> in an article titled <a href="http://lwn.net/Articles/250358/">Geographic display and input using Marble</a>. Impressed with the fact that Marble is only 9MB in size, does not require OpenGL or any high-end hardware support and has a future with embedded devices, games and the incorporation of <a href="http://www.openstreetmap.org/">OpenStreetMap</a> data, LWN.net speaks highly of the 0.4 release, and even provides a little information on what you can expect with the 0.5 release. "<em>...unlike the others, it does not rely upon enormous data sets accessed via the internet; it is, instead, self-contained and fairly lightweight.</em>"



<!--break-->
