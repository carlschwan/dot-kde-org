---
title: "10th Issue of the Amarok Newsletter is Out"
date:    2007-09-19
authors:
  - "Ljubomir"
slug:    10th-issue-amarok-newsletter-out
comments:
  - subject: "Plasma-ified center should be flashy playlist"
    date: 2007-09-19
    body: "What will go in the big blue empty space that takes up the middle of the screen?\n\nhttp://ljubomir.simin.googlepages.com/20070916_amarok.jpg/20070916_amarok-full.jpg\n\nI, for one, would suggest a flashy, informative playlist with information about the current song appearing as an expanded item in that playlist. Let's ask for some mockups and suggestions by the Oxygen artists and usability team (or anyone else for that matter)."
    author: "AC"
  - subject: "Re: Plasma-ified center should be flashy playlist"
    date: 2007-09-19
    body: "\"What will go in the big blue empty space that takes up the middle of the screen?\"\n\nThe context browser"
    author: "A KDE Advocate"
  - subject: "Re: Plasma-ified center should be flashy playlist"
    date: 2007-09-19
    body: "We were planning interactive porno animations. With clickable parts."
    author: "Mark Kretschmann"
  - subject: "Re: Plasma-ified center should be flashy playlist"
    date: 2007-09-19
    body: "Where do we send donations to make this happen?\n\nMake it a national priority!"
    author: "T. J. Brumfield"
  - subject: "Re: Plasma-ified center should be flashy playlist"
    date: 2007-09-19
    body: "+1"
    author: "jospoortvliet"
  - subject: "Side buttons"
    date: 2007-09-19
    body: "Is it possible to remove the side buttons? They may look better but are still a throwback to the past from a usability perspective. Okular has a better side button style with big clear icons."
    author: "Abe Potter"
  - subject: "Re: Side buttons"
    date: 2007-09-19
    body: "I really like Amarok's side buttons."
    author: "Soap"
  - subject: "Re: Side buttons"
    date: 2007-09-19
    body: "May I refer you to http://icanhascheezburger.com/2007/06/24/donut-worry/"
    author: "Ian Monroe"
  - subject: "animations to slow?"
    date: 2007-09-19
    body: "following aarons akademy talk, i think those playlist animations should be a bit faster."
    author: "ac"
  - subject: "svg"
    date: 2007-09-19
    body: "Personally I don't really like SVG user interfaces.\nAs soon as you use a low resolution everything looks worse as svgs (most times) only look great on high resolutions. They also need more resources as far as I have experienced\n\nOne example I tried recently is a Mahjong (sic?) game using svgs instead of bitmaps. I was only able to recognise the gaming pieces when I made the window full screen or close to it. If I tried to play it on a small window I often found my nose very close to the screen.\nSomething that did not happen with the bitmap gaming pieces that can be optimized for high and low resolutions.\n\nSo do you optimise Amarok for low resolutsions as well?\n\nPS.: Perosnally I don't like the new playlist, but that has been discussed elsewhere allready."
    author: "mat69"
  - subject: "Re: svg"
    date: 2007-09-19
    body: "Well I don't mind changes as long as I can use the old style if I find the new playlist format too space-consuming or something like that. Or just disable the pretty effect if I'm running on battery.\n\nI also read somewhere they were removing the queue option, could someone at least leave it as an option to those of us who like it (and use it). At least it's a nice feature to have until there's a proper way to have mutiple playlists (even if only one playing at a time)."
    author: "Coward"
  - subject: "Why new toolbar is in upper part of program?"
    date: 2007-09-19
    body: "Is there any particular reason why new toolbar is moved to upper part of program?\n\nI think old position was much better in sence of usibility. \nAt least in my pc amakor is in system tray 90% of time. If I maximase it and want to pause/play, or stop I olny need to go whit mouse pointer about 10cm to toobar in v1.4\nIt look that in version 2 I my mouse pointer will need to go at least 30 cm to reach new toolbar. Thats pretty much."
    author: "Dumas33"
  - subject: "Re: Why new toolbar is in upper part of program?"
    date: 2007-09-19
    body: "Right click on the icon in the system tray, and there you go. \nhttp://ljubomir.simin.googlepages.com/awnissue3#tips\n\nBTW, some users, like me, have system tray on top of the screen. \nBTW2, Almost all apps have toolbar on top. \n\nI don't see any sacrificed usability. :)\n\n  "
    author: "Ljubomir"
  - subject: "Re: Why new toolbar is in upper part of program?"
    date: 2007-09-19
    body: "And you also have keyboard shortcuts [1], kicker applet [2] and tons of karamba themes. No need to stress your arm ;)\n\n[1] http://www.keyxl.com/aaa207b/116/Linux-Amarok-MP3-software-keyboard-shortcuts.htm\n[2] http://ljubomir.simin.googlepages.com/awnissue7#tips\n"
    author: "Ljubomir"
  - subject: "Re: Why new toolbar is in upper part of program?"
    date: 2007-09-19
    body: "Still, I agree with him. The previous setup was very efficient, this huge thing just wastes a lot of space... Imagine someone with a 800x600 screen (I recently had a laptop like that) or 1024x768 (and that's what I recently bought)."
    author: "jospoortvliet"
  - subject: "Re: Why new toolbar is in upper part of program?"
    date: 2007-09-19
    body: "Lets not discuss the amount of wasted space, and widgets layout at least until the first beta release. There are simply many more concerns for developers (like making it play music, and not crash) right now, than maximizing the usage efficiency. All problems will be dealt with, in the proper time. \n"
    author: "Ljubomir"
  - subject: "Re: Why new toolbar is in upper part of program?"
    date: 2007-09-20
    body: "well.. if it's svg then it probably wouldn't be that hard to make the size scalable. not sure if it would happen though.."
    author: "Mark Hannessen"
  - subject: "Re: Why new toolbar is in upper part of program?"
    date: 2007-09-19
    body: "I like the new position, if only for the fact that I can have a longer progress bar/slider. This is very useful for scrubbing, specially when I'm transcribing audio interviews. It's almost impossible to get that precise movement with the current layout. I end up having to use Kaffeine to do this.\n\nIn fact, aside from the side buttons, I like this new layout more than the 1.x one. So if my vote counts, keep it up. :)"
    author: "Jucato"
  - subject: "GUI"
    date: 2007-09-19
    body: "I really really think the UI needs to be looked at by a usability expert. The side buttons are tabs, so make them look like tabs. The top bar looks ugly as sin, at least make the white bit in the middle properly rounded.\nPut the playlist in the middle, thats where it belongs.\n\nI hope they don't release it looking anything like this. Amarok has some great features, just don't make an bloated GUI and mess this up."
    author: "Ben"
  - subject: "Re: GUI"
    date: 2007-09-19
    body: "Well, it's the absolutely unchangeable final design, no matter what. Really.\nAh, Sorry, forgot that irony isn't always obvious on the net. Let's put a smiley here so this post won't generate a flame war. ;)\n\nAs Ljubomir pointed out (after you've posted your comment, so I don't blame you), making Amarok2 shine and look pretty isn't the top priority right now. And I'm sure feedback from usability experts and many discussions will lead to the final design.\n\nPS: Don't take it personal, but IMO your last sentence was really unnecessarily."
    author: "Hans"
  - subject: "Re: GUI"
    date: 2007-09-20
    body: "In defense of Ben: Had this in fact been the \"absolutely unchangeable final design\", then yes, his post would indeed have been pointless. The positive interpretation of him posting here is that he realizes that the design can still be changed and therefore suggests some improvements. Consider it a part of the \"many discussions [that] will lead to the final design.\"\n\nI think I like the new design myself, as long as you can in some way enlarge the playlist to cover most of the window when you need it."
    author: "Martin"
  - subject: "Re: GUI"
    date: 2007-09-20
    body: "It wasn't my intention to be offensive, just wanted to point out that these kind of complaint appear every time a screenshot of Amarok2 comes up.\n\nI recommend curious roKers to read this: http://amarok.kde.org/blog/archives/468-to-all-the-self-described-critics.html"
    author: "Hans"
  - subject: "Re: GUI"
    date: 2007-09-20
    body: "I just want to say how sick I am of reading all of the whiners and complainers.  Being a programmer, I understand 100% how to look at alpha or pre-alpha software and know where it is heading.  To complain about *details* so early in the development process is ridiculous.  We should be grateful for their hard work.\n\nI, for one, think that the new KDE and Amarok are gonna be great.  Like everyone else, I see many problems in the screenshots, but I *know* that the developers see them too and don't need us to complain every 2 seconds."
    author: "Henry S."
  - subject: "Re: GUI"
    date: 2007-09-20
    body: "Don't get my worg. It's not about whinig or complaining.\nAmarok is realy the best player. All improwment are great, expatialy new playlist. \nBecause of amarok a lot of my friends use KDE not Gnome for they home desktops.\n\nBut anyway, it always should be reason for doing something. Espatialy in sofware ingeenering.\nSo, why new toobar is on top? There is no clear answer in this thread:\n\n-Maybe some folks have KDE toobar on top? But by default it is in bottom.\n-Some people use keybord shorcuts or rigth-click on system tray icon to control amarok - maybe we don't need these bottons in main window?\n\nI understand that it is a lot of work to rebuild amakor for kde4. If you have many other concerns for developers why you startet to rebuild UI so drasticaly?\n"
    author: "Dumas33"
---
After a long summer break, the Amarok newsletter is back. In the <a href=http://ljubomir.simin.googlepages.com/awnissue10>10th issue</a> we take a look into Amarok 2 development, talk about some interesting user interface changes &amp; new features and instruct you how to Rok with your Bluetooth-enabled mobile phone. Enjoy!




<!--break-->
