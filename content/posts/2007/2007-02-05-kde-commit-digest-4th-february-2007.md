---
title: "KDE Commit-Digest for 4th February 2007"
date:    2007-02-05
authors:
  - "dallen"
slug:    kde-commit-digest-4th-february-2007
comments:
  - subject: "SVN comment of the week :)"
    date: 2007-02-05
    body: "My favourite SVN comment of the week: (Ian Munroe regarding amarok 2.0 and porting from autofoo to cmake)\n\n\"now amarok successfully doesn't compile with cmake\n\ngood bye autocrap!\"\n\nHehe\n\nThanks dannya"
    author: "Troy Unrau"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "hehe, yeah! :) but now where's Amarok 1.4.5? I thought it was supposed to be released yesterday..."
    author: "fish"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "1.4.5 is ready and \"released\", just not yet announced. Announcement will follow later today."
    author: "Mark Kretschmann"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "Amarok 1.4.5 is already in Kubuntu feisty. Running fine over here..."
    author: "Sepp"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "yeah, here too, in archlinux..."
    author: "superstoned"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "hm, what about edgy?"
    author: "fish"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "We don't know yet if it can be backported. 1.4.5 requires newer libgpod and libmtp versions. Might be a problem."
    author: "Mark Kretschmann"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "So a major and still pretty fresh distro like kubuntu prolly won't get the last Amarok in the 1.4.x series? I seriously hope you are kidding...wtf."
    author: "fish"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "Use Suse, they provide the latest KDE (ond other applications) for Suse 9.3, 10.0, 10.1 and 10.2."
    author: "max"
  - subject: "Re: SVN comment of the week :)"
    date: 2007-02-05
    body: "you dont want to switch Distros every Package, do you?"
    author: "distroswitcher"
  - subject: "kplato get support for chart"
    date: 2007-02-05
    body: " thanks Danny for this great review.\n\nit seems with the next kplato we can draw some sexy EV curve.\n\nreally thanks for all, and specially the french squad"
    author: "djouallah  mimoune"
  - subject: "Amarok..."
    date: 2007-02-05
    body: "Speaking of Amarok 2.0, will its design return to some measure of sanity in terms of dependencies? A few weeks ago, an Amarok upgrade tried to install the whole freaking Mongrel web server. You know, the Ruby on Rails thing. Right. And it was a non-optional dependency, regardless of whether you intended to use the small bit of functionality it's used for. Speak of jumping the bloat shark. It's looking increasingly like the GNOME thing of leveraging their software to promote the developer's chosen bit of obscure pet technology... and screw the end user... :(\n\n(For the records, I no longer use Amarok. JuK is sweet and, you know, does what I want without bloat...)\n\nSo, will Amarok 2.0 will return to the sort of quality design that is generally typical of KDE apps, and, dare I hope, use Kross and OPTIONAL plug-ins? Please. /Please/. :("
    author: "Oh please..."
  - subject: "Re: Amarok..."
    date: 2007-02-05
    body: "Ok, first of all, Mongrel had been a part of Amarok since 1.4.3 or so. Noone complained then, because we bundled Mongrel. Now, we still bundle Mongrel, but Gentoo's packager decided to pull it in as an external dep (which is technically sane). Suddenly people started to complain :)\n\nSecondly, DAAP (and with it Mongrel) is actually optional in 1.4.5. I made this change shortly before the release.\n\nThird, we're not too happy with the Mongrel dep either, but it was the only thing that worked as a DAAP server. The long term plan is to use Ruby's standard Webrick server instead.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok..."
    date: 2007-02-05
    body: "I agree with the previous post. It's quite funny that a music player (Amarok) with dependencies is bigger in size than a whole office suite (KOffice).\nIt's shocking to see the dependencies (e.g. on http://packages.debian.org/unstable/kde/amarok). :-(\n\nFor the record: I hardly use Amarok anymore."
    author: "Eckhart"
  - subject: "Re: Amarok..."
    date: 2007-02-05
    body: "For the record: Amarok 1.4.5 has exactly three required dependencies: \n\nKDElibs, TagLib, xinelib\n\n\nThis is shocking, you say?"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok..."
    date: 2007-02-05
    body: "there are compile-time and run-time deps..."
    author: "nick"
  - subject: "Re: Amarok..."
    date: 2007-02-05
    body: "for the record: i use self-compiled svn-version on debian unstable. and this is the only app that i compile myself..."
    author: "nick"
  - subject: "1.4.5 on digg.com"
    date: 2007-02-05
    body: "http://digg.com/linux_unix/Amarok_1_4_5_released_2_0_development_started\n\n\ndigg it :)"
    author: "Mark Kretschmann"
---
In <a href="http://commit-digest.org/issues/2007-02-04/">this week's KDE Commit-Digest</a>: Work begins on <a href="http://amarok.kde.org/">Amarok</a> 2.0. KBlackBox becomes the latest games application to take the step into the scalable graphics arena. KTTT, a tic-tac-toe game, is ported to KDE 4. Further progress made on the knewstuff2 framework. Software RAID monitoring, along with other improvements in KSysGuard. <a href="http://www.mailody.net/">Mailody</a> gets support for printing HTML emails. Improved support for custom emoticons in <a href="http://kopete.kde.org/">Kopete</a>. The sublime-integration user interface branch is merged back into the main <a href="http://www.kdevelop.org/">KDevelop</a> source tree. <a href="http://stepcore.sourceforge.net/">Step</a>, an educational physics simulation package, is imported into KDE SVN. KitchenSync is removed from KDE SVN, in anticipation of an <a href="http://www.opensync.org/">OpenSync</a>-based implementation.
<!--break-->
