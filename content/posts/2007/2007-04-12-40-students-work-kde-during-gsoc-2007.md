---
title: "40 Students To Work On KDE During GSoC 2007"
date:    2007-04-12
authors:
  - "dmolkentin"
slug:    40-students-work-kde-during-gsoc-2007
comments:
  - subject: "Awesome!"
    date: 2007-04-12
    body: "These projects are all very exciting.  Me, I'm most excited that Urs is going to revamp KRDC.  I use it _every day_ at work to manage servers on our Windows network.  The proposed tabs would be a huge help.  Big thanks from me to Urs and Brad for KRDC, and congratulations to everyone who was accepted, and good luck to all."
    author: "Louis"
  - subject: "Re: Awesome!"
    date: 2007-04-12
    body: "I'm also using KRDC every day at home and at work for connecting to remote RDP/VNC hosts and was very afraid to see it probably dying and disappearing in KDE 4! KRDC is a great tool for sysadmins.\n\nThat's one of the greatest news of the day :)."
    author: "Me"
  - subject: "Re: Awesome!"
    date: 2007-04-12
    body: "Will the revamp also include krfb ? i'm a very glad krfb user (despite the occasional crashes). \nIf i need to assist people remotely, i just can ask them to \"send me a desktop sharing invitation\", and i get everything i need.. (the last thing i want to do when people have trouble is explaining how to install and start x11vnc and mail their IP address)\n\nI like this feature a lot, especially because it is targetted to desktop users (and not to remotely manage a server, plenty of tools exist on all platforms to do that). "
    author: "anon"
  - subject: "Re: Awesome!"
    date: 2007-04-12
    body: "No, it does not.\nBut: Good news: there is an almost rewritten krfb in SVN! The works has been done the last few weeks by Alessandro Praduroux."
    author: "Urs Wolfer"
  - subject: "Re: Awesome!"
    date: 2007-04-13
    body: "You guys are weird.  I immediately kick any admins back to the windows world if they install an x server on any server machine.  I love KDE, but in my opinion, ssh is the only tool you need to admin a server.  An x server is just a waste of system resources on a server.  But hey, if there are guys who use it for admin, then great.  Personally, I'm more happy about the automatic printer driver installation.  That's a *great* step towards making KDE common on the desktop, in place of winders."
    author: "Mike"
  - subject: "Admin consoles.."
    date: 2007-04-13
    body: "  Ever hear of admin consoles and workstations?"
    author: "Kevin Bowling"
  - subject: "Re: Awesome!"
    date: 2007-04-16
    body: "you are partially right, but there are other uses, like remote assistance (helping someone out)"
    author: "anon"
  - subject: "Re: Awesome!"
    date: 2007-04-12
    body: "Is there any chance that KRDC might support the NX protocol at some point?"
    author: "ac"
  - subject: "Re: Awesome!"
    date: 2007-04-12
    body: "Threre is! NX support is a \"nice-to-have\" point for my project. Even if I don't get it done in the SoC time, I will add it afterwards."
    author: "Urs Wolfer"
  - subject: "Re: Awesome!"
    date: 2007-04-13
    body: "Sweet! That would be awesome. I wish you the best of luck!"
    author: "ac"
  - subject: "Re: Awesome!"
    date: 2007-04-12
    body: "tabs in RDC like in Konsole ?? Whooohooooo :>"
    author: "asda"
  - subject: "Re: Awesome!"
    date: 2007-04-12
    body: "Thanks for the encourage comments!\n\nBTW: It would be great if some people could \"spend\" RDP / VNC servers for testing purposes. I would just need an account on that machine. The more differnent testing systems I have, the more bugs I can fix.\n\nIf you would like to help me, please conctact me: uwolfer @ kde.org"
    author: "Urs Wolfer"
  - subject: "Correct & suggest"
    date: 2007-04-12
    body: "Actually, there's also a project for Python support in KDevelop, which makes the KDevelop project count go up to five, not four.\n\nAlso, I believe that in \"instant messanger\", the messEnger is actually spelled with an \"e\", while messAging is spelled with an \"a\". Hope you don't mind me playing the nit-picker.\n\nOther than that, I'm thrilled to see these projects getting implemented - 40 of them is _really much_, and even if only half of them would finish successfully, it means a tremendous advance for KDE.\n\nMentors and students, keep in mind that communication is everything. Students have to subscibe to their specific project's mailing list, and should hang out on IRC if possible. In order to finish the project successfully, students must be involved with the actual project, its team members, its conventions, and its current developments. Students who work independently from the \"live\" code base and plan to merge their improvements at the very end of the program are much more likely to fail than those who get their code into the project in small incremental commits.\n\nDrupal (which also takes part in the Summer of Code) expects a weekly progress report from their students, so that communication is facilitated, progress is encouraged, and most importantly the students are made a genuine part of the community. I find one week a bit short (maybe two could still be enough), but on the whole I think this would be a splendid idea for KDE projects as well. Thiago, think about this.\n\nOh, and dannya, please don't quote me in the Commit Digest this time ;)"
    author: "Jakob Petsovits"
  - subject: "Re: Correct & suggest"
    date: 2007-04-12
    body: "Thanks for the nitpicking, Jakob. Both corrected."
    author: "Daniel Molkentin"
  - subject: "Re: Correct & suggest"
    date: 2007-04-12
    body: "Yes, communication is crucial. For Google, it's very important that you also tell the world outside about your progress. \n\nSo students: Blog, blog, blog!"
    author: "Sebastian K\u00fcgler"
  - subject: "GNOME got at least 19 more projects than us"
    date: 2007-04-12
    body: "Let's compare our projects to GNOME's:\n\nThe GNOME organization got 29 project, but Google allowed additional GNOME-centric organizaitons to apply independently:\n\n1) AbiSource: 5\n2) GNU Project: 2 desktop-related\n3) GNUCash: 4\n4) Inkscape: 7\n5) Maemo: 1 desktop-related\n6) OLPC: 4 desktop-related\n7) Pidgin (GAIM): 7\n\nSo GNOME actually got 59 projects. Seems kind of unfair to me, given that KDE has more users."
    author: "Hans"
  - subject: "Re: GNOME got at least 19 more projects than us"
    date: 2007-04-12
    body: "Could we please stop this \"theirs is longer\" whining? Plus your math is completely nonsense, e.g. Maemo is only related to GNOME in that it uses GTK+, big deal. Sames goes for Inkscape and most other projects. \n\nThat said, KDE got quite some projects in nother projects such as Ubuntu. KDEs strength is to be make people work as one community, and as such, we got a lot of projects assigned. How about doing your first step into something productive instead of whining about the unfairness of the world?"
    author: "Daniel Molkentin"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-12
    body: "I think the original poster has got a point if you refine the statement:\nIt's not that the Gnome project gets more projects assigned, it's that KDE-related projects and applications are not allowed to participate independently because they are seen as part of KDE, when sometimes they only use the kdelibs or profit from KDE infrastructure. So this actual benefit turns into a real drawback.\nKOffice (they kind of accepted it now) are seen as \"KDE\", as is Amarok, while in comparison there's a bunch of gtk music players which got accepted, one a xmms spinoff, while a player with the scope of Amarok is being left out and has to fight with the whole of KDE for projects.\nIn comparison other projects like KWord, Karbon14, KMyMoney or Kopete also haven't got the slightest chance of being accepted independently which probably frustrates some developers (and users). So they won't get a certain guaranteed share of Google SOC help which would relate to their status as a successful, much deployed free software project -- only because they are (seen as?) KDE apps (the definition of that is not really clear btw).\n\nWith regard to KDE Google's selections are a mystery."
    author: "Phase II"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-12
    body: "My vision is that kde and google have A LOT of gsoc projects. Who cares about numbers ? Everyone wins !\n\nMaybe in the future, when kde apps will be completely cross-platform, google will change his mind about how to classify mentoring organization. Most of the projects  you are talking about (\"KWord, Karbon14, KMyMoney or Kopete\") are mostly used by linux kde users. I think this cannot be said about gaim or inkscape for example (lots of Win32 or xfce users).\n\nThat being said, I'm not really sure being an independant organization guarantee more gsoc projects... so please stop \"spitting in the soup\" (like we say in France)\n\n\n"
    author: "shamaz"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-12
    body: "\"so please stop \"spitting in the soup\" (like we say in France)\"\n\nThat's a good one.  In the US, we say \"Don't look a gift horse in the mouth.\""
    author: "Louis"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-13
    body: "Don't you US guys rather say \"please stop pooping at the party\"? ;-)"
    author: "Robert"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-13
    body: "Right, we don't like party-poopers.  Aren't languages wonderful!"
    author: "Louis"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-14
    body: "hehe, that is international one!"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-12
    body: "Please don't assume bad things of others, when you didn't check the most basic things.\n\nIt was KDE's decision to not have duplicated efforts in applying as many entities. As such Koffice, Amarok, etc. specifically themselves decided to NOT make their separate offers. Nothing about being \"not allowed to\".\n\nSo KDE having only one set of offers is all about KDE being a united community with a huge amount of consensus, making this possible.\n\nYou can also trust Google to NOT judge the stuff about this much for Gnome and that much for KDE. It's based on the candidates info, the task usefulness to people, and more stuff.\n\nKDE as a project is a whole better off when it gets to be a part of the decision process and one strong partner. Not to forget, be sure that Google will certainly monitor how well things work out, and e.g. KOffice alone may not be able to provide the Mentoring infrastructure as good as the whole of KDE.\n\nAnd obviously Windows is VERY unfairly neglected by Google by your reasoning, it must have at least twice as much users as KDE ;-) and yet barely a project, so what's wrong? Could it be that Gnome can be enhanced in more areas, being technically a few years behind?\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-13
    body: "<blockquote>It was KDE's decision to not have duplicated efforts in applying as many entities. As such Koffice, Amarok, etc. specifically themselves decided to NOT make their separate offers. Nothing about being \"not allowed to\".</blockquote>\n\nNope.. KOffice asked if they could apply separately, but was denied.\n \n"
    author: "Allan Sandfeld"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-13
    body: "KOffice was denied last year. This year, a decision was made to be part of KDE, and not try to split."
    author: "Brad Hards"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-16
    body: "As brad pointed out; KOffice has not applied separately, there is not even a business behind KOffice to do so.  Frankly; joining forces gives us a bigger gain as you don't need to have KOffice developers to mentor KOffice specific projects.\nTo be clear; there are for sure people that will have suggested to apply separately, but in the end the idea never made it.\n\nps. I'm a KOffice core developer ;)"
    author: "Thomas Zander"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-13
    body: "\"Please don't assume bad things of others, when you didn't check the most basic things.\"\n\nFirst, I don't assume bad things of others, namely Google. As it should be clear, it is their competition and they are free whom to chose and whom not. They don't need to justify their choices. It's rather the general penalization of a certain bunch of projects because of their strong ties to a certain toolkit/framework, while without this these projects, and this is the point, would be allowed to apply independently, and probably would have a high stand at the selection process doing so.\n\n\"It was KDE's decision to not have duplicated efforts in applying as many entities. As such Koffice, Amarok, etc. specifically themselves decided to NOT make their separate offers. Nothing about being \"not allowed to\".\"\n\nSecondly, please recheck _your_ facts. Amarok was not chosen and KOffice wanted to participate since at least last SoC, but didn't try because the message from Google was to not consider KOffice as independent organization. Also I'm not sure that there is a really broad consensus among all KDE developers or if there a some high-profile applications which would like to try their luck on their own.\nAnd whether KDE and its projects benefit more or less from being seen as a single entity, regardless of whether they could apply independently, is highly debatable and not easy to answer -- to cut this discussion short.\n\n\"And obviously Windows is VERY unfairly neglected by Google by your reasoning, it must have at least twice as much users as KDE ;-) and yet barely a project, so what's wrong? Could it be that Gnome can be enhanced in more areas, being technically a few years behind?\"\n\nPlease quote properly. This assumption was done by the original thread starter, not by me."
    author: "Phase II"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-13
    body: "There is no consensus among KOffice developers wether we should or should not apply on our own, we are divided on the question. Some thinks we might get more projects to KOffice this way, personnaly I am rather skeptical. And I am also perfectly happy to share with KDE the burden of administration management of the SoC."
    author: "Cyrille Berger"
  - subject: "Re: There's a sort of bias"
    date: 2007-04-15
    body: "If you dont know if there is a consensus within KDE, \nthen please make this clear from the START."
    author: "she"
  - subject: "Re: GNOME got at least 19 more projects than us"
    date: 2007-04-13
    body: "Well SoC is about developer proposals and not number of users (assuming one accept  your assumption about KDE vs GNOME user statistics). If you compare the total number of GNOME proposals (174) and KDE proposals (213) and then the number of slots given GNOME (29) and slots given KDE (40) you could say that GNOME got the unfair treatment due to getting a lower percentage of proposals approved compared to KDE -> 16.6% vs 18.7%.\n\nThat said I think the comparison breaks down no matter how you look at it. A lot of proposals in both the GNOME and KDE list will benefit the other desktop almost equally as they funnel effort into shared technologies.\n\nChristian"
    author: "Christian"
  - subject: "optimism"
    date: 2007-04-12
    body: "Sounds like a great list.  I can't help thinking that you are being over optimistic though by using \"will\" all the time.  Is there a rundown anywhere of how many SOC projects from last year bore fruit? "
    author: "Cliffton"
  - subject: "Re: optimism"
    date: 2007-04-12
    body: "If you consider that 40 people for four months is like Google sponsoring 13 KDE developers for a year, and that's a huge contribution from Google...\n\nA few of the SoC projects from last summer were not completed but have turned into longer term projects and are still ongoing, growing far beyond the initial specifications as well as drawing developers into KDE and opensource in general.  If this happens in even one SoC project, then the world is a better place, imho, and I'd happily take this result :)\n\n"
    author: "Troy Unrau"
  - subject: "good!"
    date: 2007-04-12
    body: "Wow, really awensome! Those great projects handly-picked, finely selected, lining up for a good start on a great summer!!! Some of them really strike me, they sound too good to be true :-D \n Many many many compliments to all the students involved ^_^. \n!! GO GO GSoC-KDE !!\n"
    author: "alphaman"
  - subject: "Revamp of context sensitive help"
    date: 2007-04-12
    body: "I'm very happy that Joshua Keel will implement the new context sensitive help infrastructure designed by OpenUsability.\n\nOur help system has not evolved much since a long time. With the new design possibilities given by Qt4 and nepomuk, we will have the possibility to create something smarter."
    author: "cmiramon"
  - subject: "Re: Revamp of context sensitive help"
    date: 2007-04-12
    body: "/me's looking forward to that as well. It's not just KDE which hasn't progressed in this regard, but the whole industry has had a stand-still... If it was for Microsoft (after all, a monopolist), nothing would ever improve of course. Free Software seems to give the software industry a chance of real, more and better sustained innovation."
    author: "superstoned"
  - subject: "Re: Revamp of context sensitive help"
    date: 2007-04-18
    body: "That's not completely true... for example they have some sort of sensitive context help since Office 2003 (4 years back in time)"
    author: "Vide"
  - subject: "great !"
    date: 2007-04-12
    body: "These is an exciting list!\nAnd about kdeprint, wine is mentoring a project to use win32 printer drivers from linux ! Well, this is not really related to kde, but that would be AWESOME for my canon printer =)"
    author: "shamaz"
  - subject: "Re: great !"
    date: 2007-04-12
    body: "Whereas my Epson R800 produces better results from the latest gutenprint driver than from the Epson windows driver. I suppose it would be okay for new as-yet unsupported printers (though see turboprint for a less klugey, albeit commercial, solution)."
    author: "Adrian Baugh"
  - subject: "kpilot AND kitchensync?"
    date: 2007-04-12
    body: "Please tell me why both KitchenSync/OpenSync AND kpilot are accepted. It seems they both do the same thing, just that OpenSync has a wider audience as it supports more devices and is platform/desktop agnostic.\n\nI think kpilot has a very limited future, simply because the PalmOS is dying. Even if not, Opensync does have a palm-plugin.\n\nOh well, I guess everyone is allowed to work on what they want. Sometimes, I'd just like to be the boss of opensource :)"
    author: "anonymous coward"
  - subject: "Re: kpilot AND kitchensync?"
    date: 2007-04-12
    body: "I'd not work for a boss of Open Source that thinks that two projects with overlapping functionality are abundant -- that's IMO one of the cornerstones of Free Software. :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: kpilot AND kitchensync?"
    date: 2007-04-12
    body: "Actually, you're only kind of right.  =;)\n\nKpilot and opensync both perform record-based syncing operations.  Kpilot is focused on being the best maintenance/sync/functional interface that you could ever want for a Palm-OS-based device, while Opensync has much loftier goals of being a general syncing machine.  Therein lies the rub, or part of it at least.\n\nKpilot does much more with palm pilots than opensync can do or is planning to be able to do.  For example, it sets the time on your palm pilot, communicates with AvantGo for website downloads to your Palm, sends mail from your Palm, and has the ability to do a variety of non-record-based activity that opensync doesn't address.  That's not to say one is better than the other.  We just have different goals.\n\nNow, that being said, part of the purpose of the SOC KPilot project is to refactor and dramatically improve KPilot's conduit base code so that we can not only more easily maintain it and stabilize it and reduce bug-counts, but also have a closer model to Opensync's record-based conduit syncing algorithm.  The hope is that at some point in the future, once Opensync has stabilized (there's a big jump from 0.2x to 0.3x for Opensync, which is what the Opensync SOC project is addressing), and we clean up kpilot, we might have more synergy with Opensync and get rid of some of the duplicate code.\n\nOh, and as far as KPilot and PalmOS dying...  I'd say we're dying at the same rate that COBOL is dying.  That is to say that there is an awfully large user base of the new and legacy PalmOS-en (people are still using Handspring Visors!!!) just as there is of COBOL (which is a sad, sad thing to be sure, but truth nonetheless).  =:)\n\nHTH!!  =:)"
    author: "Jason 'vanRijn' Kasper"
  - subject: "Re: kpilot AND kitchensync?"
    date: 2007-04-12
    body: "> people are still using Handspring Visors!!!\n\nHey! I have a Handspring Visor!  It's the main openSUSE KPilot test device."
    author: "Bille"
  - subject: "Spare !?"
    date: 2007-04-12
    body: "Are these 40 GoC Projects from Students with Spare Time or New Contributors ?\n\nIf not, then Progress on KDE would not be faster because they already work on parts on kde, which will get no attention in the meantime.\n\n\n"
    author: "asda"
  - subject: "Re: Spare !?"
    date: 2007-04-12
    body: "Nothing to worry about.\n\nFirst, most of them are new contributors and second, those who already contribute can now fully concentrate on contribution, i.e. are able to increase the amount of time they spend on KDE work.\n\nSo we can be quite certain that all projects will add to KDE's progress"
    author: "Kevin Krammer"
  - subject: "Photorealistic SVG support"
    date: 2007-04-12
    body: "I'm using KDE 3.5.1 and tried this (http://svn.sourceforge.net/viewvc/*checkout*/inkscape/inkscape/trunk/share/examples/gallardo.svgz) as a Desktop background. This is a photorealistic SVG image (http://www.inkscape.org/screenshots/gallery/inkscape-0.45-photorealistic-car2.png) but it appears just flat on the desktop. \n\nDoes the latest version of the KDE support photorealistic SVG features such as Gaussian Blur? If the KDE still does not support these features, I think its a worthy upgrade to KDE."
    author: "Sagara"
  - subject: "Re: Photorealistic SVG support"
    date: 2007-04-12
    body: "i agre that kde (qt) should suport gousinan blur but thont expect to use those images has a desktop image they are complete cpu hogs.\nLast realistic wallpaper i made (http://pinheiro-kde.blogspot.com/2007/04/discovering-new-desktop.html) took 3 hours to render in inkscape, has fast has qt can be it will still be quite unusable for wide desktop image usage.\nHas  source form for images its realy great   "
    author: "pinheiro"
  - subject: "Re: Photorealistic SVG support"
    date: 2007-04-13
    body: "To avoid CPU hogging when rendering large photo realistic SVGs:\n1. Its possible to render at a low priority, isn't it? Then you won't notice any slowdown in your work. Ideally this priority setting is user configurable (eg. Low=lower than applications, Very-Low=Run when machine is idle like a screen saver, High=Process as when required like now). \n\n2. Cache rendered images, so that no need to render again and again.\n\nBtw, your wallpaper is very nice. Where can I find the SVG file?, so that I can try and see how long does it take at my end.\n\nHere is another photo realistic SVG: http://www.inkscape.org/screenshots/gallery/inkscape-0.45-cupoftea.png"
    author: "Sagara"
  - subject: "RandR"
    date: 2007-04-13
    body: "I can't wait the improved RandR support. Good screen hotplugging will be great, especially when you have to do presentations. Perhaps we will at least be able to do presentations as powerpoint does on os x."
    author: "Med"
  - subject: "Not just KDE!"
    date: 2007-04-13
    body: "Be sure to check out the other projects that got accepted.   While some are uninteresting, many are.  For example, gcc got several projects accepted, and since nearly all KDE installs are compiled with gcc, every improvement to gcc is an improvement to KDE (it also helps gnome if you are one of the idiots who thinks you need to keep score)\n\nBe careful before you pass a project off as uninteresting, sometimes a project does something that has a wider scope that you might guess.  Wine's printer driver project comes to mind as something that could be awesum if done well.  There are plenty of others. "
    author: "Hank Miller"
  - subject: "Pretty nice :)"
    date: 2007-04-13
    body: "Really nice proposals here, I hope all they become true. Thanks to everyone involved"
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Congrats"
    date: 2007-04-13
    body: "Congrats to the students that got their submissions accepted.  Thanks to Google for having such a great promotion.  And last but certainly not least thanks to all the mentors that will be giving up time and energy to help.  This is a great list of projects, we as users should feel very proud.\n\nI'm not a c++/qt programmer, but am a Perl programmer, I'm not a great artists, but good enough to have a couple things accepted into Kalzium, and would like to extend an offer to help out if there are things that I can do.  [Read over documentation, write crummy Perl scripts, or produce mediocre graphics. :-) ]\n\nCheers, and good luck."
    author: "Brian"
---
The KDE project is happy to announce the selection of <a href="http://code.google.com/soc/kde/about.html">40 KDE projects</a> for the <a href="http://code.google.com/soc">Google Summer of Code 2007</a>. This is the third consecutive year that KDE is participating in the initiative. Though Thiago Macieira, KDE's Summer of Code co-ordinator, states that reviewing the 213 submissions was difficult, Aaron Seigo, member of the <a href="http://ev.kde.org/">KDE e.V.</a> board, has the <i>"highest confidence in the final list, with ambitious and exciting new technology and functionality set to grace the KDE desktop, which is very fitting with what we are trying to achieve with the KDE 4 vision"</i>. Read on for more information about the selected projects.








<!--break-->
<p><a href="http://www.kdevelop.org/">KDevelop</a>, with five accepted projects, will get support for <a href="http://www.cmake.org/">CMake</a>, and brand new code completion and Ruby language support. Kommander will also be integrated into KDevelop. Another five projects are related to <a href="http://pim.kde.org/">KDE-PIM</a> applications and the new <a href="http://pim.kde.org/akonadi/">Akonadi</a> PIM data store. With a total of six accepted projects, <a href="http://koffice.org/">KOffice</a> will receive a collaborative editing mode, and other accepted applications focus on elements of <a href="http://www.koffice.org/krita/">Krita</a> and <a href="http://koffice.kde.org/kword/">KWord</a>.
</p><p>
Other KDE applications receiving new features from students are the <a href="http://quanta.kdewebdev.org/">Quanta</a> web development editor, the <a href="http://kopete.kde.org/">Kopete</a> instant messenger, and the award-winning <a href="http://amarok.kde.org/">Amarok</a> mediaplayer. The KDEPrint printer backend will be re-designed and substantially improved, allowing the user to download missing printing drivers automatically. The Marble desktop globe will receive support for GPS and the KML file format (as used by Google Earth) as well as a 2D projection mode. The <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> desktop search and <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>, the semantic desktop framework are also both covered by another two Summer of Code projects.
</p><p>
For ongoing reports on the progress of these projects, see future editions of the weekly <a href="http://commit-digest.org/">KDE Commit-Digest</a>.
</p>


