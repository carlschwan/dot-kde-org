---
title: "People Behind KDE: Volker Krause"
date:    2007-04-19
authors:
  - "dallen"
slug:    people-behind-kde-volker-krause
comments:
  - subject: "Hardworking and underexposed"
    date: 2007-04-19
    body: "Volker is one of the PIM team and a typical one of them: hardworking and underexposed. There's a whole bunch of them slaving (sort of) away at less-than-glamorous applications and doing really good work. Thanks, Volker."
    author: "Adriaan de Groot"
  - subject: "Kudos to Volker"
    date: 2007-04-19
    body: "KNode is IMO one of the best usenet agents and I'm really addicted to it, I've tried many others but none convinced me to leave Knode :)\nGreat work!"
    author: "Vide"
  - subject: "First read!1"
    date: 2007-04-20
    body: "Found this interview through g**gle even before it appeared on the dot :)\nI was just curious who was behind this (for me unknown) name while I had a look on the libakonadi sources.\n\nAlso thanks from me to Volker and the other underground worker.\n"
    author: "michael"
  - subject: "Thanks!"
    date: 2007-04-20
    body: "Thanks Volker for all your good & hard work! It's definitely appreciated."
    author: "Sebastian K\u00fcgler"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we travel over to Germany to talk to the key to your personal information storage, a highly dedicated KDE-PIM developer (though hide any small animals when visiting his apartment!) - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/vkrause/">Volker Krause</a>.
<!--break-->
