---
title: "Python Enters KDE SVN with Guidance"
date:    2007-04-19
authors:
  - "jriddell"
slug:    python-enters-kde-svn-guidance
comments:
  - subject: "ruby?"
    date: 2007-04-19
    body: "Any reason this wasn't done in Ruby instead of Python?  The problem in Python is that it is whitespace sensitive, so if you have a tab instead of a space, a space too many or too little, unpredictable things would happen.  Ruby seems to take all the good things from Python without having this issue."
    author: "AC"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "I was hoping these sorts of \"Languages X is better than Y\" comments wouldn't crop up.\n\nSome people like python - let them use it :)  The point of the article is that KDE has bindings for building good applications while using interpreted languages.  KDE has good support for Python AND Ruby, and others too...\n\nAdditionally, with Kross in KDE 4, which is a generic way to add interpreted language components to any of your applications, Python and Ruby are equally well supported."
    author: "Troy Unrau"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Well see, if the author makes his article sound like Python is somehow more efficient than C++, he basically invites this kind of \"C++ vs Python\" flamewar, because uninitiated people may and will believe whatever they read.\n\nYes, C++ does have an overhead, when compared to plain C, and *not* Python of course. And the fact that g++ is a lousy compiler does not exactly help. But implying that Python is the language of choice when you try to write efficient code is just plain misleading."
    author: "hmmm"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "I don't think the author is claiming Python is more efficient than C++, he merely claims that writing programs in  python is more efficient. You can focus on your application code without having to worry about lots of issues like pointers/memory leaks and other boilerplate stuff."
    author: "Marijn Kruisselbrink"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "And let the user worry about increased CPU and memory usage."
    author: "x"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "It's not worth noting that they use Qt and kdelibs (both written in C++) for their GUI (so there is no discernible speed difference) because these \"NewerSuperiorLanguage is so much slower than OlderLowerLevelLanguage\" statements have been around for years -- they were said when people moved from assembly to Fortran, from C to C++, and now from C++ to Python. "
    author: "Cerulean"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Higher Level != Superior. It depends on the purpose.\n\nC is (arguably) superior to C++ when it comes to kernel level work, but inferior for any GUI work for example.\n\nPython is superior to C++ for writing a script real quick (think a SuperKaramba theme), but inferior for pretty much everything else."
    author: "hmmm"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Lower = Low abstracinon level\nSuperior = Superiro abstactino level\n\nAs somene has said, there's no best language, there's is best language for acertain problem.\n"
    author: "Inmanete"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "I think python has some advantages over ruby.. Regardless i think the aim is to encourage developers. While i enjoy c++ i certainly like other languages much better for desktop work. Maybe others feel the same..."
    author: "Ryan"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "I don't like the idea of whitespace sensitivity either but the Python fans seem to cope quite well with this \"issue\".  I would never tell them they're using the wrong language based on that dislike. \n\nFeel free to implement a kick-ass KDE application of broad interest in ruby. \nI would love to see one.  That would prove the real-world viability of Korundum.  \nNot that I doubt it ... \n\nOr is there even already one I am not aware of?   Maybe in playground? \n\nAnyway: \nCongratulations to the Guidance people!  This is great news. \n\n"
    author: "cm"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "And based on the fact that Python has more users as it is far, far more popular than Ruby (though Ruby seems to have cropped up because of the excellent Korundum bindings and its visibility in Amarok), I'd say the \"issue\" the grandparent discovered is pretty much non-existant. "
    author: "Cerulean"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "It's not an issue, it's a feature. :-)\nIt forces you to indent properly.\nHonestly, i very rarely run into that problem and when I do, I've always had an error at first run when it occured because the interpreter found something wrong in the indent. So, in my experience, there's very little fear that you get 'unpredictable results', just errors easily corrected since the interpreter provide you with the file and line number where the indent seems off.\nPython is very easy to program and very easy to re-read, even if it's not your own code. There are also tons of efficient modules allowing to do a lot of things very easily. With bindings like PyQT and PyKDE, I've been able to build a small video editing app (too crude to be released anyway) using transcode and imagemagick, in a month, including learning Python and object programming. And the only language I knew before was fortran.\nTells a lot on how easy this language is and how powerful PyQT and PyKDE bindings are."
    author: "Richard Van Den Boom"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Had a quite similar experience with it in (since) October... I'm writing ljKlient, and before it, I had not even seen python source, was terrible at OO, and had no idea how to code a GUI, let alone in Qt+KDE.  I come from the Perl world, myself."
    author: "Ralesk"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "From Fortran to Object Oriented paradigm, Python (with 2 different video libraries) and GUI toolkits in ONE month??? Seems 'a bit' exaggerated..."
    author: "bpfurtado"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "I promise it's true. I just read Dive in Python and similar books you can find on the net, wrote some initials programs in a procedural way then switch to OO programming (in Python way, it's so easy and powerful it's actually a pleasure to work with) and used essentially Qt docs to program graphical interface, since sip and PyQT allows you to use the actual QT names for objects and methods in your Python code.\nWhat I find remarkable in Python is that I never have the feeling that the language is getting in the way : everything seems totally logical to me and fluent. Everything is coherent and clean.\nMind you, I'm probably not programming in an OO way which is very usual : I don't use any particular tool to design my programs (UML or things like that, I don't even knoww if it's needed), just try and run, and I create objects the way it seems logical to me, but it may perfectly be that my code is horrible. :-)\nBut it works for me.\nAnd really, in Python, once you know how to use a module, you know for all, you just need to use the online documentation (extremely complete) as a dictionnary to get the method names.\nTry it, you'll see."
    author: "Richard Van Den Boom"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Coming from a C++ and web programmy background, I was doing cool things with Python in 4 or 5 days, and am still learning interesting and useful bits now!"
    author: "Cerulean"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "I wrote the very first usable version of ljKlient in a couple hours after my first python lines.  It was able to post a blog entry with a title, the username and password hard-coded.\n\nIt's a ridiculously easy language (although some parts of the library make me dizzy! and I still can't get used to the python regexp module's syntax), and Qt also makes the OO paradigm ridiculously easy to learn :)"
    author: "Ralesk"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "thw whitespace thing is the most cool \"invention\" about phyton which make code more readable and compact !\n\nSure there are always people who cant use it, then use something else with {} :>\n\n\n"
    author: "funnyfanny"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Ah, the good old tab war.  Be consistent, make sure your co-developers don't mess with your source files' indentation, and if they do, smack them on the knuckles.\n\nIn more detail:\n- Start in whichever method.  Tabs save bytes (not that that matters), 1 character per indentation also has a semantic meaning attached to it, and whoever reads the code can choose how many ems of width an indentation should be represented as.  Spaces are convenient because so many editors prefer them over tabs, and they are perceived more robust, although mixing tabs into the issue messes things up here just as much as the other way around.\n- Agree and set things in stone: employ Kate, Vim, emacs, etc. magic comments, and if you use tabs make sure people can't mess them up with soft tabs and other hybrid means recommended by certain editors' certain documentations.\n\nFrom another aspect, that Python is indentation-sensitive and that that's a bad thing is an irrational fear.  I started using it only half a year ago and have so far not found that really troublesome (although I did once or twice want to use extra indentation for emphasis purposes, but that was just a minor problem).  Proper, consistent, tidy, strict indentation is also a Good Thing in every other programming language, and should be employed whenever possible.  Hell, I can't live without indenting descendants' rules in CSS!"
    author: "Ralesk"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Yup: the same reason it wasn't done in Perl. Perl and Ruby are designed to make coders feel great about themselves, Python is designed to make good software."
    author: "dhh"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "... Okay, the above was a kneejerk reaction, and I shouldn't have posted that. Guess I'm annoyed that you can't talk development anywhere anymore without having some Ruby fan show up and begin a marketing spiel. This is bothersome, and actually doesn't speak very well in favor of Ruby, y'know."
    author: "dhh"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "That's the wrong way around. Ruby is actually more intutive to use, even without indentation==scoping rules. (which sucks btw if none of your co-workers can agree to use tabs or spaces) Once you use blocks+each to progress arrays, you'll never go back. And the upcasting in python is wacked. Actually 90% of OOP in python is wacked. Ruby does it right, even though it has drawbacks of it's own. (standard libs are sub-par compared to python, no Unicode, and slow still) \n\nThat aside, it would be nice to see the ability in Kross to wrap scripts in a standard interface so they can be exposed to other scripting languages like qt C++ is wrapped. Then it wouldn't really matter which scripting language you could use. I think that's the idea behind COM and IDL.\n\nCheerio\nBen"
    author: "Ben"
  - subject: "Re: ruby?"
    date: 2007-04-20
    body: "> Ruby is actually more intutive to use\n\nDon't pass off your own preferences as objective fact.  *You* find Ruby more intuitive.  There's plenty of people who find Python to be more intuitive.  Neither are objectively right or wrong, it's personal preference.\n\n> which sucks btw if none of your co-workers can agree to use tabs or spaces\n\nIf none of your co-workers can agree on something as trivial as this, then give up now, you are never going to be able to agree on libraries, platforms, or pretty much anything necessary to build software.\n\n> Actually 90% of OOP in python is wacked. Ruby does it right\n\nWhy do you feel the need to attack Python in order to promote Ruby?  Can Ruby compete on its own merits without you feeling the need to put down the competition?  Fanboys do more to dissuade me from picking up Ruby than most other factors.\n"
    author: "Jim"
  - subject: "Re: ruby?"
    date: 2007-04-26
    body: "<i>If none of your co-workers can agree on something as trivial as this, then give up now, you are never going to be able to agree on libraries, platforms, or pretty much anything necessary to build software.</i>\n\nWhitespace differences are pretty hard to check when doing a code review.. I mean, it is invisible... But you are right about sticking to one standard.\n\n\n<i>Why do you feel the need to attack Python in order to promote Ruby? Can Ruby compete on its own merits without you feeling the need to put down the competition? Fanboys do more to dissuade me from picking up Ruby than most other factors.</i>\n\nNo, you're right. Use the right tool for the right job. I'm just having a hard time seeing why people would develop on python instead of ruby given the choice.\n\nBen"
    author: "Ben"
  - subject: "Re: ruby?"
    date: 2007-04-26
    body: "> I'm just having a hard time seeing why people would develop on python instead of\n> ruby given the choice.\n\nWell... I am working with Python because I like it better. This is probably true for other developpers around, you see ?\n\n--\nAntoine"
    author: "edomaur"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Congratulations to Simon! Let's have more Python *and* Ruby apps in KDE4 - just try both and use whichever you prefer. There is no such thing as the 'best programming language' anyway, and choosing which one to use for a project depends on many things, some of which are very subjective. "
    author: "Richard Dale"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "Actually there is a whole list of people that need to be congratulated. Phil Thompson and Jim Bublitz for PyQt and PyKDE -- it would have been hard to develop Guidance without those bits ;-) -- Sebas (powermanager), Yuriy Kozlov (wineconfig), Martin Boehm (grubconfig), and the whole team of packagers, testers and bug wranglers on #kubuntu-devel who have done a lot of the hard work of hunting down the bugs. And thanks to anyone else that I've forgotten. It has been a team effort.\n\nI hope that people who may not be into C++, but want to develop, go check out Qt/KDE development with Python / Ruby. The Free Desktop and KDE has a lot of important applications already, but there is still a huge potential universe of software which needs to be written. Developer time and effort is a limited resource, and Python and Ruby are two very effective ways to get the most out of this resource. And it's fun too! ;-)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "All the reasons. People is free to do whatever they what and have different tastes than you. Respect them."
    author: "IrYoKu"
  - subject: "PERL-isms take away from Ruby's potential"
    date: 2007-04-19
    body: "I like the pure object oriented ideal of Ruby, but the fact that it incorporates so many Perlisms in its syntax makes it hard to understand. Even Yukihiro Matsumoto, Ruby's creator, admits that the Perl baggage can make Ruby look cryptic:\n\nQuestion: What bits of Perl did you incorporate in Ruby?\n\nAnswer: A lot. Ruby's class library is an object-oriented reorganization of Perl functionality--plus some Smalltalk and Lisp stuff. I used too much I guess. I shouldn't have inherited $_, $&, and the other, ugly style variables.\n\nOne of the reasons Perl lost popularity to Python was the fact that Python's syntax is much more straightforward.\n\nSecondly, Python's (rather perfectionist) philosophy has been to provide one clear way of doing things, coined TOOWTDI. Not only does this make Python code portable between programmers who all know the same coding style, but it also cuts down dramatically on the need for tutorials that show all kinds of obscure ways to do things.\n\nFinally, the fact that Google has chosen Python to do much of its internal development speaks wonders for the robustness of the language. And we all know that Google does things right :) In fact, Python's founder Guido van Rossum was hired by Google last year.\n\n"
    author: "Vlad"
  - subject: "Re: PERL-isms take away from Ruby's potential"
    date: 2007-04-19
    body: "Those are global variables, they start with a dollar sign and Matz calls them 'ugly style variables' in your quote above. They have absolutely nothing to do with the syntax of Ruby, which is actually no more similar to perl than python is. You don't even have to use those global variables if you don't want to."
    author: "Richard Dale"
  - subject: "Language limitations"
    date: 2007-04-19
    body: "A language is not only measured by what it can do, but also by what it can't.\n\nA language that has a bad feature that allows to do things in a wrong, misleading or unreadable ways, is worse than a language that does not allow it.\n\nSo even if Perl arguably allows for good, readable programming style, it still is an inferior language because it easily allows bad, unreadable programming style.\n\nRuby shares this with Perl, by allowing these ugly globals, even if \"you don't have to use them\" -- as other people will use them, and you will have to read and use their code."
    author: "Eyal Lotem"
  - subject: "Re: Language limitations"
    date: 2007-04-19
    body: "You're just asserting your subjective opinions as though they are objective facts - it is not an objective fact that a language that allows you do to things in many different ways (ruby) is necessarily worse than a language that encourages you to do things in one way (python). \n\nYou are also implying that because ruby shares a few global variables with Ruby, therefore all critisisms of perl are valid critisisms of ruby which is obviously false."
    author: "Richard Dale"
  - subject: "Re: Language limitations"
    date: 2007-04-19
    body: "I think these language wars are futile, as all three of perl, python and ruby are syntactically pretty timid languages compared to languages with decent functional, metaprogramming and optional type inference support, but this caught my attention (and not just because it's from the famous Richard Dale! :)\n\nThe problem with ruby and perl's ugly global variables is not that they exist, but that everyone seems to use them, and in perl's case, is encouraged to do so by tutorials and so on. Whether it is the fault of the language or the culture around it is basically immaterial as, to programmers who have to read other people's code, the effect is exactly the same."
    author: "taj"
  - subject: "Re: PERL-isms take away from Ruby's potential"
    date: 2007-04-23
    body: "\"And we all know that Google does things right\"\n\nafter seeing the linux version of google earth, I beg to differ.\nreally, that was just painful. :( I can't believe there was actually qt under all the mess."
    author: "anon"
  - subject: "Re: ruby?"
    date: 2007-04-19
    body: "\"Any reason this wasn't done in Ruby instead of Python?\"\nProbably because YOU did not write a superior ruby implementation.\nSo go ahead and do that now."
    author: "Jimmy"
  - subject: "PyKDE"
    date: 2007-04-19
    body: "On the linked PyKDE webpage, the last news entry is from September 2005. Are they still actively maintained? Or they just completed, so there is news until KDE4?"
    author: "ac"
  - subject: "Re: PyKDE"
    date: 2007-04-19
    body: "It seems to me that there maintained now inside the kdebindings packages, aren't they?\nSo the website may just not be updated anymore.\nAnyway, that should confort you about their presence in KDE4. :-) "
    author: "Richard Van Den Boom"
  - subject: "Re: PyKDE"
    date: 2007-04-19
    body: "That is not the PyKDE webpage, it's the web page for the PyKDE Extensions. As the name say, it's an extension to the PyKDE package."
    author: "Morty"
  - subject: "Re: PyKDE"
    date: 2007-04-19
    body: "PyQt is actively maintained; last release was a week ago. PyQt has supported Qt4 for some time now.\n\nPyKDE is still being maintained and developed for KDE 4 by Jim Bublitz.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: PyKDE"
    date: 2007-04-19
    body: "I think PyKDE4 will come out after the API stabilises.  At least I have heard a couple times from the PyQt guys that they don't support beta Qt's, for that reason.  Once a release is set in stone, they'll support it as soon as possible."
    author: "Ralesk"
  - subject: "yay"
    date: 2007-04-19
    body: "Good to see the system config modules going in, since those will be rarely used and performance is irrelevant, but please don't make the guidance power manager the default for KDE.  It works, but kpowersave works just as well, and uses a fraction of the memory.  This is an applet that will be running all the time, and I really see no need for it to be using 20+ MB of RAM just to tell me that the power cord is plugged in.  Kpowersave is good and fast, so I'd like to see either that or an improved klaptop (with hal support) as the default for KDE 4."
    author: "Leo S"
  - subject: "Re: yay"
    date: 2007-04-19
    body: "+1\n\nI think that's true. I love guidance, and also the new kcontrol replacement in Kubuntu, but the powermanager is just to heavy. C++ is just a much better choice for something which runs all the time..."
    author: "superstoned"
  - subject: "Re: yay"
    date: 2007-04-19
    body: "Sigh. I would much rather trust it being written in a memory-managed language like Python or Ruby for that EXACT reason -- there are far more likely to be memory problems with a long-running applications written in C/C++ because of memory leaks.\n\nHave you actually got any statistics on how much 'heavier' a language like Python (used in things like Eve Online where speed is of the essence) is -- especially a GUI application?"
    author: "Cerulean"
  - subject: "Re: yay"
    date: 2007-04-19
    body: "Eve is using Stackless Python, which is not CPython, and they are using it because they have this super cluster full of CPU and memory, but care only for latency in communications.\n\nBut read what the author wrote, may no longer be true, but still:\n\n----\nThere's a couple of notable problems with it though, the first is that being Python it'll have a larger startup time than C++ and take up more memory. \n----\n\nHe should know, right? And this:\n\n----\nThe other is that the python-dbus bindings only work with the glib mainloop so it has to keep polling to find changes, that's quite evil in a power management applet. It may well be ported to Qt 4.2 c++ once that's out with the dbus and system tray goodness it'll bring.\n----\n\nYou know, if that power management applet still uses that glib mainloop, you really will also see, how it consumes your power faster than the C++ solution would. But I really assume that's just outdated.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: yay"
    date: 2007-04-20
    body: "PyKDE applications add about 8MB to the shared mem usage (once, not for every PyKDE application, obviously).\n\nI'm actually more concerned that powermanager is not (yet) event-based, we're polling every 5 seconds (and thus wake up the CPU, bad thing for a powermanager). The reason for this is that handling dbus events from a PyQt3 application was not possible due to clashing event loops."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: yay"
    date: 2007-04-19
    body: "I'm not planning on trying to get powermanager into KDE4 in its current form. \n\nI *do* think however, that we could easily do with something simpler than kpowersave for a default KDE4 install. The thing I really like about powermanager is that it really has no frills, just basic things, easily accessible via one click. It really shows that usability people have been involved in the development process before even the first line of code has been written. \nIn the end, which one to ship is up to the distro of course, and that's usually based on what application integrates well with their existing powermanagement infrastructure and which UI fits their target group best.\n\n"
    author: "Sebastian K\u00fcgler"
  - subject: "Python vs. C++"
    date: 2007-04-19
    body: "Comparisons between these two languages are based on bloated opinion and foolish pride. This is in the same vein of comparing a quarter horse with the Easter Bunny.\n\nPython rocks but it is not meant to carry huge loads. C++ has an overhead that could break a mule but it is solely designed to be able to tackle large-scale systems development. You imply cannot compare the two.\n\nPython is interpreted and C++ is compiled.\n\nThe same simple wxWidgets frame compiled into an executable on Windows by both py2exe and a wxDev-C++ will yield an executable file that fire up just as quickly but with a distinct difference. The difference for which I mention is the py2exe is about 14 MB's where as the wxDev-C++ exe file is around 4 MB's.\n\nThe speed differences will drop dramatically for Python as the application system's framework grows where the C++ framework influenced speed will drop less drastically.\n\nDevelop a C++ framework and build modules with Python that are internally managed from the C++ framework for excellent memory management and on the spot worry free garbage collections. Python like C++ are some of the best languages out there and to consider leveraging both in tandem makes me feel kind of giddy.\n\nThis idiotic way of thinking in terms of 'either or' is crap. Systems designers and engineers need to get in the habit of thinking in terms of 'more, more, more, and then we'll talk.'\n\nMore extensibility, more functionality, more resource handling, more speed, more room on the system form factor outlines.\n\nKDE handling Python code rules. It would appear that KDE talks and walks the 'more, more, more' line. Fantabulous;-)\n\nSoon we might even brain-wash the KDE crew to make designers where you can actually skin your shell on the spot and export the design as files that other KDE users could install and use on their systems.\n\nShell skinning in .svg with scripting for media FX is already on the horizon and that is looking good.\n\nPython scripting with pyGL libraries capitalizing .svg graphics will take some work but it would be worth it. Use Inkscape to design the .svg graphics and go.\n\nSomeone should design a background application in Python that runs like web content on Windows. That Python program then runs media as the desktop wallpaper at the user's request. All you have to do is set up an application to manage the media and provide menu options for the users preferences and viola.\n\nI plan on doing all of this myself later on with a little more experience as most of the programming I've done is really only to study design principles and programming theory.(i.e. - OOD, OOP, and AOP)\n\nPython 3000in three years. Google movie with Guido van Rossum at the podium. If you've not seen it yet then what are you waiting for?"
    author: "Scorpion"
  - subject: "Re: Python vs. C++"
    date: 2007-04-20
    body: "> Python is interpreted and C++ is compiled.\n\nNo, in its normal mode of operation (executing scripts), Python is compiled to byte-code the first time you run it.\n\n> Python 3000in three years.\n\nPython 3 will probably be released next year.  An alpha is planned for this year."
    author: "Jim"
  - subject: "Re: Python vs. C++"
    date: 2008-09-19
    body: "Python modules are compiled to byte code. And you can specifically tell python to compile a module/script. If you simply run a script, it will not be compiled to byte code, although any imported modules will be. Also, this has no affect on the program's runtime performance, it simply allows it to be loaded faster."
    author: "Joel"
  - subject: "Great News"
    date: 2007-04-24
    body: "this is great. I think for many KDE application except for the really big ones like KOffice and some where performance is critical, Python and Ruby are the better choices than C++, simple because of the much higher productivity.\n\nfor Ruby Fans: check out the recent Milestone 8 of the Netbeans IDE. the Ruby support rocks (still in development)"
    author: "Markus"
---
The first non-C++ application in KDE's SVN has been moved from the playground module to <a href="http://extragear.kde.org/">Extragear</a>.  <a href="http://www.simonzone.com/software/guidance/">Guidance</a> is a number of system configuration modules and a laptop power manager.  The recent 0.8 release added a kcontrol module for setting up Wine and improvements to the power manager.  One of the aims of KDE 4 is to increase the use of KDE bindings, such as <a href="http://rubyforge.org/projects/korundum/">Ruby's Korundum</a> and <a href="http://www.riverbankcomputing.co.uk/pykde/index.php">PyKDE</a>, which will make coding KDE easier for those who do not want to worry about pointers and compilers.




<!--break-->
<p>Guidance was originally developed for Mandrake Linux and is now used by default in Kubuntu.  It is hoped that moving it to KDE Extragear will encourage testing and development on other distributions.  Guidance requires <a href="http://www.simonzone.com/software/pykdeextensions/">PyKDE Extensions</a> as well as the PyKDE bindings themselves.</p>

<p>Speaking at <a href="http://kde.me.uk/index.php?page=fosdem-2005-kdepython-talk">FOSDEM 2005</a> about the reasons to use Python over C++ Guidance author Simon Edwards said "Productivity goes up twice compared to C++... It looks similar to any C++ KDE program but without the C++ overhead. The C++ API is transparently translated, so it's easy for seasoned C++ KDE developers to use Python for the job."  Simon will be <a href="http://akademy2007.kde.org/conference/talks/49.php">talking about Python in KDE</a> during the Bindings stream at aKademy.</p>

<p>More information on language bindings to KDE is available on <a href="http://techbase.kde.org/Development/Languages">the Techbase bindings page</a>.  If you are starting a KDE application, think about which language is best for you.</p>



