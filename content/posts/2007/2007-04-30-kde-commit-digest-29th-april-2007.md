---
title: "KDE Commit-Digest for 29th April 2007"
date:    2007-04-30
authors:
  - "dallen"
slug:    kde-commit-digest-29th-april-2007
comments:
  - subject: "karbon"
    date: 2007-04-30
    body: "nice to see some work is going into karbon."
    author: "Patcito"
  - subject: "Amarok for Windows"
    date: 2007-04-30
    body: "For the Amarok Windows screenshots, go here:\n\nhttp://amarok.kde.org/blog/\n"
    author: "Stephen"
  - subject: "New joystick icon."
    date: 2007-04-30
    body: "I'm not usually one to gripe about this stuff, but I don't really believe the new \"Joystick\" icon will be easily recognizeable:\n\nhttp://commit-digest.org/issues/2007-04-29/moreinfo/658198/#visual\n\nSure, it's tempting to use a Wii controller since it's new and cool, but that icon doesn't say \"Joystick\" to me.  Looks more like my TV remote.  No offense intended."
    author: "Louis"
  - subject: "Re: New joystick icon."
    date: 2007-04-30
    body: "I agree. I've not yet seen the Wii, and looking at this was like wtf? Also, it's kind of hard to make out the buttons on the controller.\n\nThe original icon is better but too dark."
    author: "Johnny"
  - subject: "Re: New joystick icon."
    date: 2007-04-30
    body: "I couldn't agree more.  In my opinion the joystick icon should be modeled after what is possibly the most identifiable joystick the classic Atari CX-40 (http://xrl.us/v5fg) that was included with the venerable 2600."
    author: "kinema"
  - subject: "Re: New joystick icon."
    date: 2007-04-30
    body: "It was changed back a few hours afterwards :)\n\nhttp://websvn.kde.org/trunk/KDE/kdelibs/pics/oxygen/scalable/devices/joystick.svg?view=log"
    author: "David Miller"
  - subject: "Re: New joystick icon."
    date: 2007-04-30
    body: "I really prefer the old Bold, Italic and Strikethrough icons as well as the old Calculator icon.\n\nThey seem large enough that I'm not sure why they went to simpler, more high-contrast versions.\n\nI believe the old versions looked sleeker, and more modern.  The new versions look less refined.\n\nI don't mean to be negative, and I appreciate everyone's hard work, but I figured someone should say something.\n\nIs there any reason to go to more simplistic icons there?"
    author: "T. J, Brumfield"
  - subject: "Re: New joystick icon."
    date: 2007-05-01
    body: "Look at the folders these are in, they get resized to sizes as small as 16\u00d716."
    author: "Kevin Kofler"
  - subject: "ksysguard changes"
    date: 2007-04-30
    body: "If anyone is interested, this is what ksysguard looks like with the X window information.  Suggestions/comments are very welcome.  I've also changed the memory column to just show the memory usage in \"k\"'s.  \n\nhttp://img341.imageshack.us/img341/7210/ksysguard3yp0.png\n\n\n(Before the \"k\" change: http://img294.imageshack.us/img294/5198/ksysguard1pv2.png )\n\nOne thing I'm worried about..  it's not obvious how to show the command etc columns.  You have to right click on the header and show it.  It's obvious to any of us, but would there be a situation in which someone new to computers would need this info and be unable to discover how to view it?"
    author: "John Tapsell"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "Hi,\n\nIt looks very good I think, much easier then the current ksysguard!\n\nDo you think it would be a good idee, to turn the combobox on \"Own Processes, Simple\" by default, since you can only kill those?\n\nCheers"
    author: "kill an app"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "I'm trying to avoid doing that.  Instead all your own processes are sorted to the top by default (as in the screenshots)  and clearly colour coded.\n\nBtw, if you try to kill someone else's process, it asks for the root password.\n"
    author: "John Tapsell"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "Great great great!\n\nAnyway, I'm not really sure about the difference between \"Name\" and \"Title name\", I mean, I know the difference but what is \"title\" supposed to represent?"
    author: "Davide Ferrari"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "Yeah I have to think of a better column name for that.\n\nIt's what's written in the window title bar. (If a process has multiple windows, it uses the most recently created one.  The tooltip lists the other windows).\n\nIt only applies to X applications of course.\n\n"
    author: "John Tapsell"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "Window Title should be fine, though it's not really a major issue. All in all, it's far sweeter :-). Great work!"
    author: "Cerulean"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: ">I'm trying to avoid doing that\n\nPlease don't, it's missguided attempt to simplification just to avoid on checkbox. It does not make it any more simple and it results in grater complexity. And it needs workarounds to add perceived simplicity, like your own always on top functions. \n\nThe common way of sorting will not work properly, since there seems like no easy way to separate two common tasks. Sort for the processes consuming most cpu/mem, for all or only your own processes. How do you switch between your on top and not? It's simpler just to remove the unneeded data in those cases. Color cooding is nice and will be helpfull in some cases, but it's far nicer and easier to work with if the unneeded data is hidden. \n\n\nIn addition, the program icon is nice. But the implementation needs some more work, processes not having icons break left justification. Making it look somewhat messy "
    author: "Morty"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "One more thing. \n\nIf you want to remove stuff, why not remove the toolbar. Seriously I doubt that more than 1-2% use the worksheet functionality, and for them it's still avaliable in the file menu. For the rest it's just visual noise."
    author: "Morty"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "yeah, that's a good idea. I vote for this."
    author: "superstoned"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "Actually it's a misguided attempt to avoid hiding information from the user.  If a process is using all the cpu/mem then you don't want it hidden from you look at the process list.\n\nYou can easy filter for only your apps.  You just change the combo box in the top right.  Just to be clear on this, I'm talking about the defaults.  You can always just change the combo box to Own Processes, and have it exactly as you say."
    author: "John Tapsell"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "Aahh, sorry. I completly missed the whole combobox thingy :-) My mistake. \n\nThen it's only the default we don't agree on, not really a big thing. But I have to say I prefer hidig of the information the user can't act on. For users not having the root password, it's just annoying knowing what's hogging the cpu/mem and not being able to act on it ;-)"
    author: "Morty"
  - subject: "Re: ksysguard changes"
    date: 2007-05-01
    body: "For my case, it's better not to hide, because I'm the only\nuser of my PC and laptop, and I use the root account only\nwhen necessary. When cpu/mem hog occurs, it's important to\nidentify immediately which process is causing it, and then\nreact appropriately (either as a normal user or root).\n"
    author: "me"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "> Please don't, it's missguided attempt to simplification just to avoid on checkbox. \n\nYou misunderstood John - he's trying to avoid having to change the default setting of that drop-down to only show the user's own processes. He's not trying to avoid having the option, i.e. it won't be removed."
    author: "Eike Hein"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "> In addition, the program icon is nice. But the implementation needs some more work, processes not having icons break left justification. Making it look somewhat messy\n\nDone:\n\nhttp://img54.imageshack.us/img54/653/ksysguard4dm1.png"
    author: "John Tapsell"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "wonderfull!!!"
    author: "superstoned"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "Nice! Great work."
    author: "Morty"
  - subject: "Re: ksysguard changes"
    date: 2007-05-03
    body: "Thank you.  Scanning for a process name looked like it was going to be a pain with justification broken.  Glad you're on the ball :)"
    author: "MamiyaOtaru"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "The kill process button should just a context menu or moved to the upper bar"
    author: "andre"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "nah, I don't fully agree on this. John should imho see if there are more usefull things which need to be there (eg information about the process? Switch to process?) OR, as you say, remove it. But if he keeps the button and adds a few, I think they should stay on the bottom."
    author: "superstoned"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "It's in the context menu, but since it's going to be one of the most common tasks, I wanted it to be fairly easy to do.  The toolbar will probably be hidden by default to save space."
    author: "John Tapsell"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "I've never commented on the dot before but saw this screenshot and felt I had to.  I use Ksysguard all the time and I feel that this new version is so much better from a useability and presentation point of view.  It's just plain gorgeous.\n\nBest regards,"
    author: "Mark Dickie"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "I just have to say that having the window title is BRILLIANT!  I don't know how many times I've opened up KSysGuard to figure out who's taking my RAM, only to be faced with half a dozen processes by the same name.  I suppose I should have filed a wishlist bug, but I must admit I didn't think about it.  Great to know it's there!"
    author: "mactalla"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "mouseover gives a lot of info in the process tab, really cool! But maybe you can also make the sensors tab a bit more informative, eg most ppl wouldn't know what 'nice' or 'wait' are? And what's the diff between buffered memory and cache memory?\n\nLoad average might use some explaining as well - what do the numbers mean?\n\nbtw it's really looking better and better, very nice work you're doing!"
    author: "superstoned"
  - subject: "Re: ksysguard changes"
    date: 2007-05-03
    body: "So maybe use of the What's This? feature is warranted for that.  I know what niceness is (priority basically), but I don't know what wait is.  :/"
    author: "Matt"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "Absolutely great work!"
    author: "T. J, Brumfield"
  - subject: "Re: ksysguard changes"
    date: 2007-04-30
    body: "man, the positive attitude and constructive collaboration in this thread is mind boggeling.\n\nyou guys rock!"
    author: "anonymous"
  - subject: "this one is a good week"
    date: 2007-04-30
    body: "Sebastian Trueg committed changes in /trunk/extragear/multimedia/k3b/libk3b/jobs: \n   Actually there is no need to reload the medium anymore. The original problem was due to the mounting for verification and that the new data does not show up before reloading. But now that the medium is not mounted anymore but read directly we do not have to reload.\n\nYipeee.\nThe most anoying bug in K3B has gone\n(yes, I've a slow burner, and I'am realy happy to be able to leave and let the burner burn whithout me to interact)\n\nIt's nice to see work on karbon, and Jamendo integration in Amarok is sweet.\n\nA good week to read the commit digest, thanks Danny, and thank you all devs !"
    author: "kollum"
  - subject: "Oxygen & englis bias"
    date: 2007-04-30
    body: "Have a look to the new eraser's icon, to the zip's icon, you need to understand english for one and you need to know what the \"zip\" (an english word) is a metaphor for a algorithm.\n\nhttp://commit-digest.org/issues/2007-04-29/files/658198/now/eraser.png\nhttp://websvn.kde.org/trunk/KDE/kdelibs/pics/oxygen/128x128/mimetypes/application-x-gzip.png?revision=644353&view=markup\n"
    author: "Morreale Jean Roc"
  - subject: "Re: Oxygen & englis bias"
    date: 2007-04-30
    body: "Regarding the eraser icon -- its an action icon designed to be viewed at 32x32 22x22 and 16x16. At these resolutions the text becomes unrecognisable. So when you are using KDE 4 you'll only see it at these small sizes.\n\nAs for the gzip mime, we are currently working on redoing a lot of the mimetype icons."
    author: "David Miller"
  - subject: "Re: Oxygen & englis bias"
    date: 2007-04-30
    body: "At small sizes, if you can't read the text, then why change the icon so drastically in the first place to one where the text is such a major aspect of the new design?\n\nA red stick with unreadable text doesn't say \"Eraser\" to me, where as the old pencil/eraser image does.\n\nAnd given how big localization is to KDE, I don't see why English text should be in icons, period.  And I say that as someone who only speaks English, but you should be consistent in your project efforts."
    author: "T. J, Brumfield"
  - subject: "Re: Oxygen & englis bias"
    date: 2007-05-03
    body: "You're wrong, the new eraser icon is 100% percent easy to understand, and anybody speaking any language knows what ZIP is! (Sorry for my poor english, I am mexican)"
    author: "Luis"
  - subject: "Re: Oxygen & englis bias"
    date: 2007-05-03
    body: "If the text is unrecognizable, wth is it doing there bloating up the file size?  A lot of these have more detail than will ever be seen at 32x32, adding to the filesize and rendering complexity (if SVG).  Of course, it's better to have it and trim later, so perhaps as this gets finalized the smaller icons will be trimmed some.  Couldn't hurt anyway, if stuff isn't visible in the first place ;)"
    author: "MamiyaOtaru"
  - subject: "plasma"
    date: 2007-04-30
    body: "No single commit related to plasma, again...\nLooks like KDE4 will come with kicker/kdesktop...? (means, without the most awaited feature/vaporware)"
    author: "ac"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "Personally I don't mind if it gets pushed back to 4.1. It may encourage new people joining in and speed up the task."
    author: "Luca Beltrame"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "You are right. It's amazing to see what get's done under the hood of KDE4. The problem is that the normal user only sees the eyecandy and that is what matters for him. KDE4 might be superior to e.g. gnome in a technical way but it would still look like Windows 95 if plasma isn't ready."
    author: "DITC"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "Actually, Windows 95 wasn't too bad. It was just a pity that it wasn't that stable underneath."
    author: "ac"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "Guys, chill already with this obsession with Plasma!  I don't deny that\neye-candy is important, but the first release of KDE4 already has a huge\nnumber of cutting-edge technologies.  It would rock even if the looked\nlike FVWM circa 1995!  Moreover, don't forget the distinction between\nKDE4 and KDE 4.0.  You will get your Plasma eye-candy no matter what:\nyou might just have to wait a wee bit longer.  Besides, lest you forget,\nOxygen is looking awesome. Combine that with the new composition features\nin Kwin, and even the most fervent eye-candy lover is bound to be pleased\nwith KDE 4.0.\n\nHaving said that, I think that it's time for the Plasma developers to\nclarify its current status.  All this speculation is becoming annoying,\nand the crowds want some reassuring words.  Aaron: if you're reading this,\ncould you drop us a line here or on your blog?\n"
    author: "dario"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "now we're talking about eyecandy, don't forget the new Oxygen theme! check it out, lots of cool animations, it's really becoming a hot style :D"
    author: "superstoned"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "The problem with Plasma was that it is practically the opposite of normal open-source development.\n\nSuccessful open-source projects start with a proof-of-concept or quick hack and are refined into production code.\n\nPlasma, on the other hand, started with a bunch of mock-up drawings and a *lot* of articles saying how brilliant it was going to be, and the *actual code* was an afterthought.\n\nIn every instance I have ever seen, the latter type of project has failed miserably and never even gotten off the drawing board.\n\nThe clamour about Plasma is a direct result of the hype that was coming directly from the developers who had yet to actually write any code.  And now people are asking where is this great new tech they have been told about over and over, and it's *their* fault?  Um, no.\n\nMoral of the story: don't hype something if you don't have running code.\n"
    author: "Jim"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "I think you and many others have let your mind misslead you by looking at mockups, rather than reading orginal descriptions of Plasma.\n\nThe main point of Plasma was to bring sanity to the whole kikcer/kdesktop mess. Intergrating those two into sane libraries, and including Superkaramba functionality combined with the new advanced GUI features from Qt 4.2 as an added bonus. It would create the fundation for something more, making a whole lot of new and fancy stuff possible. The key word here is fundation. \n\nAnd most of that is already delivered. Most of the required functionality from Kicker, kdesktop and Qt are already there in library form. And when KDE 4.0 are released, if it only uses those libraries to deliver the functionality of kikcer/kdesktop. Even looking exactly the same as KDE 3, it fulfills it's promises. The fundation is ther and it's for people to build on.\n\nAnd it's telling how much cool stuff you will be able to do with it, is not exactly hype when all is there to see and use. It does not require much imagination to see that either. Thinking of the power Qt gives you, combining that with better intergration of Superkarambas possibilities and libraries giving you easy access to the the desktop/wokspace. The new krunner already demonstrates a smal part of this. Lots of Plasma is done, it's time to start building with it."
    author: "Morty"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "I agree that maybe we need a word about the status of the plasma project. I think that we will some plasma in KDE 4.0.They have being working on it for a long time now and I have seeing tht some people are working on some plasmoid, clock and calendar and raptor seems to be feasible. I think that mos important things is that we will get new applications with kde 4.0. I was so worry that we would get kde 4.0 with all nice internals and plasma and no one would use it. I think it is really important that with KDe 4.0 we will get things like Amarok 2.0, Koffice 2.0, Okular, DOlphin, Konqueror, etc and other things that will use the new technologies like Solid, Phonon, Nepomuk. To be sincere I am more interested in getting Nepomuk than  plasma. I think taht we will get a small version of plasma with raptor, a clock and a calendar and it will evolve from there. \n"
    author: "tikal26"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "Its not the first time that people are talking about status of Plasma.but from the first Aaron told us that Plasma needs to be done in its time.\na few weeks ago Plasma development being started and now developers are working on it.they have IRC Meetings every wednesday and they are creating widgets.\nThe Plasmoid's are currently just some Ideas, first the libraries should be completed.\n\nif you try the SVN, you will see that plasma developers are currently working on Widgets.\nPlasma is in trunk/KDE/kdebase/workspace (i think)\n\nI Think Trusting Developers of plasma is the best thing we could do."
    author: "Emil Sedgh"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "I might be wrong on this but... \nTo me it seems that the currently, people are working on the library part and finishing this up. Solid, phonon etc are all base libraries that applications need to access. \nPlasma to me however, is really much more in the application area (yes there are libraries, but they are working on them!) and thus will be developed later on, once libraries are really done\n\nGive the devs a break :)\n\nP.s. I would love to have a more official statement of this!"
    author: "Benjamin Schindler"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "Has the design and concept of Plasma even been nailed down?\n\nWhen I look at the ToDo lists, there seems to be several suggestions for varying ways to do different things, and there doesn't seem to be a consistent theme, design or concept.\n\nPlasma right now I'm pretty sure exists solely in two ways.  The first is rolling SuperKaramaba, KDesktop and Kicker into one app, which I somewhat disagree with but can understand.  Right now I can replace Kicker with other apps if I want to customize my desktop.  I have the option of running with SuperKaramba widgets or not depending on my desire for toys/overhead.  With one superapp, I get the overhead and lack of choice.  With the superapp, the three can be designed to work together to make customizing the desktop a more unified experience.\n\nBut is there anything revolutionary?  Not that I see.  There are some proof-of-concept mockups, but last I've heard, no one even decided on which mockups to attempt to include or code.  So, Plasma exists in this regard as a bunch of \"wouldn't this be cool\" ideas tossed around.\n\nHonestly, there are several KDE4 mockups and concepts that I love.  Some might work better or worse than others.\n\nI think what should have happened a good year ago, is to get together strong project leadership, and community involvement to pick and choose the best of these mockups, and focus on a couple tangible ways to improve and revolutionize the desktop experience.\n\nIf that hasn't really happened, then the Plasma that everyone wants is still a LONG, LONG way off."
    author: "T. J, Brumfield"
  - subject: "Re: plasma"
    date: 2007-04-30
    body: "revolutionary?\nWhats does  revolutionary means?at last, it will a desktop.\n\nSuperKaramba was a great Idea.but its current implementation is not so great.\nKicker is working good, but creating applets for it is hard and its just a taskbar.nothing more.\n\nso the plasma idea is to merge these.having Superkaramba's cool idea's and eyecandy (and easy karamba creation) with kicker's good implementation on a great desktop.Karamba's are currently doing a few things.controlling AmaroK, showing CPU Usage and a few others.Plasmoids should do more.you could find some original mockups in svn.\n\ndevelopers were waiting for a good KDE4 Libs to start Plasma.and they started this.I think in 1-2 months we will have example and testing Plasmoids."
    author: "Emil Sedgh"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "\"This, then, is the goal and mandate of Plasma: to take the desktop as we know it and make it relevant again. Breathtaking beauty, workflow driven design and fresh ideas are key ingredients and this web site is your portal onto its birth.\"\n\nThe Plasma website keeps talking over and over again about innovation, about huge leaps forward, about completely rethinking things, etc.\n\nWhat are we getting apparently?  Three existing apps merged into one.\n\nYawn.\n\nEspecially considering I really prefer kbfx over kicker in the first place."
    author: "T. J, Brumfield"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "another, maybe even more important part of Plasma is to give the user more control, make it easier to add and change plasmoids/applets. Thus allowing the users to (help) innovate. So KDE 4.0 will come out with some basic (KDE 3.5-looking?) applets, and ppl will be able to easilly build new applets and share them through GetHotNewStuff2; KDE 4.1 will come with the best things floating around on GetHotNewStuff2."
    author: "superstoned"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "Good point. As I (poorly) stated earlier in this thread, I believe that what is most important is that the foundation is in place. Once KDE 4.0 is out, I strongly  believe that such a foundation will enable people to build new things upon it, and also to probably attract new developers.\n\nAnd a word to the KDE developers: please don't let the \"negative people\" affect you. I really appreciate what has been going on so far for KDE 4.0. Lots of new and interesting technologies."
    author: "Luca Beltrame"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "I'm very excited for all the other aspects of KDE4, however when you make so many huge promises to the community, and Plasma has been the most hyped aspect of KDE4, in the end the KDE community is going to look bad if it amounts to nothing.\n\nThere were tons and tons of great mock-ups of ways to shake up the desktop and do innovative things.  But in looking at the todo lists, they haven't even decided on a direction yet, let alone begun to implement it.\n\nThat sounds like poor planning to me."
    author: "T. J. Brumfield"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "So this great new innovation and rethinking is to repackage existing technology?\n\nActive Desktop was the big innovation in Windows 98, almost ten years ago in case anyone forgets."
    author: "T. J. Brumfield"
  - subject: "Re: plasma"
    date: 2007-05-03
    body: "Yet nobody cared (except for when their desktop background disappeared and instead had some Internet Exploder error page for their background).  Hmm..."
    author: "Matt"
  - subject: "Re: plasma"
    date: 2007-05-03
    body: "No, plasma is mostly about CONNECTING existing (at least, in KDE 4) technologies like solid, phonon and decibel through a superkaramba-like interface to make it easy accessible for developers. This will give them the power to easily do new things. They won't have to worry about the details (eg just say 'show a cpu meter here' and they don't have to parse /proc/cpuinfo themselves, and make workarounds for bsd) because plasma does that. They just have to connect the parts which are supplied by plasma. Remember, most 'innovation' today is about connecting existing technology in new ways, thus plasma fits perfectly in there."
    author: "superstoned"
  - subject: "Re: plasma"
    date: 2007-05-05
    body: "SuperKaramba already provided a means to script widgets.  The rest of KDE4 provides backends like Solid and Phonon.\n\nYou want to upgrade SuperKaramba to allow for easier scripting, go right ahead.\n\nBut making Kicker, Desktop and SuperKaramba into one app isn't a revolution.  Actually, it adds overhead for those who don't want it, and removes choice from those who preferred to swap out Kicker for something else.\n\nShow me what actual new thing I can do with it.  Show me how it revolutionizes how I use my desktop.  I could do widgets before, and I can do widgets now, with less freedom.\n\nNow read the Plasma website, and the Plasma todo list, and any Plasma notes you can find.  Tell me they didn't over-hype and under-deliver."
    author: "T. J, Brumfield"
  - subject: "Re: plasma"
    date: 2007-05-05
    body: "To me they really didn't overhype. I think most people confused the vision with the actual results. \nAs a side note: you should stop being so negative with this. Don't you think it gives developers even less will to work on such things? If you think they under-delivered, you can fix that: you can always hop in and help.\n\nI'm sure the developers don't mind criticism, as long as it's done in a positive and constructive way, which is sadly not the point of your posts (but not only you... look at the whole Konqueror vs Dolphin \"flame\"...)."
    author: "Luca Beltrame"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "> means, without the most awaited feature/vaporware\n\nnew plan: i'm going to start bringing masking tape, card board and a set of magic markers with me to events i go to after 4.0 is out. \n\ni'll tape out a nice little queue area and write \"plasma pilgrimage\" on the card board with the magic markers. i'll sit in a comfy chair at the end of the queue with the sign by my side. then each one of you who has annoyed and otherwise bothered me with this drivel in spite of the positive effects the workspace efforts have had (already, even) on kde 4.0 and what will be shipping in 4.0 (no, not kdesktop and kicker) can stand in line and entertain me to make up for having to endure you.\n\ni can't wait."
    author: "Aaron J. Seigo"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "Excellent reply :)\n\nI'm personally getting very very tired of all the negative energy building up.  There are so many people (developers too) that write that the things I (and apparently, Aaron) build are not up to their standards. Well, you'd almost think they feel like I'm on their personal payroll.\n\nYes, folks.  Your honest concern turns into negative energy. And that's the opposite of what working in KDE was meant to give."
    author: "Thomas Zander"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "Guys, in the name of \"the rest of the KDE users\", thank you for the great work, and thank you for Plasma, it's a great thing indeed. The desktop _does_ need a new paradigm. 3D is a new interaction model, but we still need a workflow oriented desktop. THANK YOU!"
    author: "KubuntuUser"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "So the lesson to be learned is that users shouldn't be promised things that don't exist yet, or things that aren't fully defined.  With plasma, I remember that I (and obviously many others) was very confused about what it will be when it was first mentioned.  The early news around it mainly talked about vague concepts such as a new workflow and beautiful workspaces.  So in everyone's head, plasma turned into this magical feature that would solve their computer problems.  Of course that is not the reality, and now users are disappointed about it.\n\nPlasma will be very cool, and users should complain less, but its not like this  came out of nowhere."
    author: "Leo S"
  - subject: "Re: plasma"
    date: 2007-05-04
    body: "But why, oh why do they (and you) have to be so negative?.\n\nA lot of groundwork has long been laid, and besides the last steps could not be done much sooner (kdelibs had to take a more definitive form), and Aaron has done huge amounts of kdelibs work (and presentations around the world :) that has taken most of his available time. So a part done, but no visible, shiny results to see yet. The thing is, it may turn out great even in 4.0 already, you can't know.\n\nSo why not let them work? Why not whine less? Are they/you going to apologize for the annoying, repetitive remarks if 4.0 turns out good, and 4.1 the most amazing, wonderful, whatever desktop there is?. Let-developers-work. Offer your help, your suggestions, coding skills, whatever. Judge the final results.\n\nTo Aaron (and Thomas): forget them. Keep on explaining what you are doing. There more than enough KDE enthusiasts who know how to make suggestions instead of demands.\n"
    author: "The Vigilant"
  - subject: "Re: plasma"
    date: 2007-05-01
    body: "Haha!  Good one.  Alas, you are just suffering the inescapable consequences of being a public figure.  There are going to be the naysayers; just ask a politician.  I could have sworn you mentioned in your last blog that Plasma work is coming soon on your todo list.  Those of us keeping up know that the time will come.  Don't lose heart!"
    author: "Louis"
  - subject: "Sync?"
    date: 2007-04-30
    body: "One thing that I find sorely lacking in KDE is a sync program. One great feature in windows was the possibility to sync two different computers so that I could work on either one of them and the file would then semi-automatically be updated on the other. This allowing me to sync my laptop with my desktop and going away for a trip and sync it again when coming back. All documents changed on the laptop would be synced to the desktop and all changes on the desktop would be synced to the laptop. In cases where both files had been updated I was prompted to choose which one to keep.\n\nSomething like this should be available in KDE imho. And given that we work with open file formats it could even be greater than the windows counterpart, as it should be able to just find where the changes are in case of two conflicting files.\n\nI've seen opensync but that seems to be a library more than anything else and not really useful to me. I've also found this which seems to be right on track, but unfortunately not a QT-application: http://www.conduit-project.org\n\n\nIs there a sync program in KDE that I just don't know about or is there ongoing development in this direction? Please don't ask me to write it myself as I'm a really bad coder.\n\nOscar"
    author: "Oscar"
  - subject: "Re: Sync?"
    date: 2007-04-30
    body: "Would be enough to have a dual-boot syncronisation first... E.g. regarding your music collection, fonts..."
    author: "benh"
  - subject: "Re: Sync?"
    date: 2007-04-30
    body: "Is there a sync program in KDE that I just don't know about ... ?\n\nYes there is http://www.opensync.org/wiki/kitchensync .\n"
    author: "Sunny Sachanandani"
  - subject: "Re: Sync?"
    date: 2007-05-03
    body: "According to this http://lists.kde.org/?t=116811174300002&r=1&w=2 it seems as if kitchensync is about to be removed and also not included in KDE4 at all?\n\nBut I might have misunderstood something."
    author: "Oscar"
  - subject: "Re: Sync?"
    date: 2007-05-01
    body: "You can put your data in a Subversion repository and have a working copy on each account. Then you can use \"svn update\" and \"svn commit\" to sync. And it gives you revision control."
    author: "Erik"
  - subject: "Re: Sync?"
    date: 2007-05-01
    body: "I've been doing this with CVS for years. \n\nBut unfortunately some of the files change very often or very thorougly, for example:  \nKonqueror stores access times to bookmarks so every time a bookmark is *used* the bookmark file changes(*).\nKorganizer seems to reorder its calendar file completely if a single entry is edited (**).  \nKopete stores the time a contact has last been online. \n\nThis is very prone to create CVS conflicts so a simple file-based synching has its issues. It works better than nothing though.\n\n\n(*):  My solution: I'm stripping out the date information before committing, it's not mandatory. \n(**): My solution: I use a calendar in a file on a remote host and let Korganizer's built-in up-and-download do the work.  I commit from that remote host, for backup purposes.  \n\n"
    author: "cm"
  - subject: "Re: Sync?"
    date: 2007-05-01
    body: "Unison is great for this kind of work. It's not a KDE util, but there is a fine GTK front-end for it.\n\nIt does not have the \"informed diff\" feature you suggest, which would be a cool idea, but for most purposes it works just fine the way it is."
    author: "CAPSLOCK2000"
  - subject: "Re: Sync?"
    date: 2007-05-20
    body: "I use unison too, although I didn't know there was any gui. it seems like it's supposed have the ability to use something else for merging conflicts, but I didn't understand the config and couldn't get that part working. I just had to remember not to add bookmarks on both comps at once. :)"
    author: "Chani"
  - subject: "Re: Sync?"
    date: 2007-06-01
    body: "I'm a little late to the party... but just happened to be looking for one myself  (for simple backup reasons, rather than pure sync reasons).\n\nTake a look Komparator:\nhttp://www.kde-apps.org/content/show.php/Komparator?content=33545\n\nJosh.\n"
    author: "Josh"
  - subject: "kwin_composite!"
    date: 2007-04-30
    body: "One rather important event that I didn't see mentioned is that kwin_composite was merged back into trunk, making it the default Window Manager*! Congrats to Lubos, Rivol and Phillip (and anyone else I may have missed) and perhaps we can expect an article on this in the near future, Troy? :)\n\n\n\n* Note that composite support is optional, before anyone gets their panties in a bunch about it :p"
    author: "Anon"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "This (important!) commit happened after the week selection boundary, which is Sunday at midnight in my timezone (i'm in the UK).\n\nTherefore it will be covered next week.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "I'm in the middle of a very busy two weeks here, where I'm basically working 14hour days, 7 days a week.  If I get a chance next Sunday, I'll write the article :)  I need to give Lubos some time to answer questions via email and so forth, but it'll get coverage as soon as I have a day off :)"
    author: "Troy Unrau"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "If you need any help... Drop me an email?!?"
    author: "superstoned"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "hope the new kwin will have better compiz support.  "
    author: "djouallah mimoune"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "The point of the new kwin is not to use compiz at all.  In fact, it implements much of compiz/beryl type functionality directly in kwin, meaning you get a stable, feature filled, well tested window manager, plus many of the effects you know and love.\n\nPlus, it has a fallback mechanism, so that if your video card/drivers do not support a certain feature, you'll still have a working window manager."
    author: "Troy Unrau"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "fu.. my english, i know that already, but in case i want to use compiz, because i read some where that metacity has a better compiz support then kwin.\n\nok perhaps some troll will say it's just a redundancy. "
    author: "djouallah mimoune"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "What you are saying doesn't make sense. You can use compiz OR metacity OR kwin, you can't use 2 of them at the same time."
    author: "Narishma"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "compiz is a windowmanager.\nNot very usefull to run both kwin and compiz at the same time.\nif you want to use compiz, you should run it in stead of kwin. \nSo what you really want is better kde-support for compiz, or better compiz-support for kde.\n\n\n"
    author: "whatever noticed"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "Metacity and Kwin both don't (have to) support compiz, as compiz is just another windowmanager (like metacity and kwin). So they don't run at the same time... But Gnome's panels DO support compiz better than Kicker does, that's a fact."
    author: "superstoned"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "wow, two minds, same thought at the same time :)\n\nAnd both from The Netherlands as well :o)"
    author: "whatever noticed"
  - subject: "Re: kwin_composite!"
    date: 2007-05-01
    body: ":D"
    author: "superstoned"
  - subject: "Re: kwin_composite!"
    date: 2007-04-30
    body: "ok fine, i get it , don't flame me like that, i was just asking"
    author: "djouallah mimoune"
  - subject: "Re: kwin_composite!"
    date: 2007-05-01
    body: "we're just telling you ;-)"
    author: "superstoned"
  - subject: "Re: kwin_composite!"
    date: 2007-05-03
    body: "Sensitive much?  Check the story of the boy who cried wolf.  If you keep saying people are flaming you when they aren't, soon people will not take you seriously when you talk of flaming."
    author: "MamiyaOtaru"
  - subject: "Re: kwin_composite!"
    date: 2007-05-03
    body: "GNOME has better compiz support because compiz was developed first for GNOME.  Metacity is the default window manager in GNOME nowadays, but compiz is a replacement.\n\nMetacity is also implementing compositing functions I believe, so now we'll have Compiz, Beryl, kwin_composite, Metacity, and Xfwm4.something as composite window managers."
    author: "Matt"
  - subject: "Re: kwin_composite!"
    date: 2007-05-01
    body: "I haven't gotten a good answer about this, but will EXA-style compositing be used with the new KWin?  That is, what will happen to kompmgr?  Even if it doesn't have fancy effects beyond shadows, I'd still like this kind of thing to remain simply because it's fast and works well on my ATI x300, whereas Compiz/Beryl barely work, are slow and often lockup my machine."
    author: "Joel Feiner"
  - subject: "Re: kwin_composite!"
    date: 2007-05-01
    body: "See comment of Troy above yours"
    author: "whatever noticed"
  - subject: "Amarok 2 is looking good"
    date: 2007-04-30
    body: "I am really looking foward to Amarok2. I think this with Krita is the things that I am really impatient to get. The wrok that Nikolaj is doing paired with a generic APi for music store just opens the doors for so many things and with all the work going on and the two summer of code projects amarok should redefine the whole rediscover you music motto."
    author: "tikal26"
  - subject: "typo"
    date: 2007-05-01
    body: "a quick note, there's a minor typo - \"music store inte_r_gration\" ;)"
    author: "richlv"
  - subject: "Re: typo"
    date: 2007-05-01
    body: "Fixed, thanks.\n\nDanny"
    author: "Danny Allen"
---
In <a href="http://commit-digest.org/issues/2007-04-29/">this week's KDE Commit-Digest</a>: Continued work across kdegames, with the kbattleship-rewrite merged back into trunk/. Start of scalable interface support in <a href="http://edu.kde.org/kanagram/">Kanagram</a>. Further functionality enhancements implemented in the <a href="http://konsole.kde.org/">Konsole</a> refactoring effort. Small refinements in KSysGuard. More work on the <a href="http://www.kdevelop.org/">KDevelop</a> Subversion plugin. Preparations for RSYNC support in the icecream distributed compilation utility. Progress made in the Amarok-on-Windows porting and generic music store integration for <a href="http://amarok.kde.org/">Amarok</a> 2. Initial milestones reached in the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9B5F3C70D02AA396">Music Notation Flake shape</a> <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9B5F3C70D02AA396">Summer of Code</a> project in <a href="http://koffice.org/">KOffice</a>. Support for boolean operations on paths in <a href="http://koffice.kde.org/karbon/">Karbon</a>. <a href="http://www.kde-look.org/content/show.php/Primary?content=39469">Primary</a> iconset imported for KDE 4, as part of a general cleanup effort in kdeartwork - more iconsets to be added soon.

<!--break-->
