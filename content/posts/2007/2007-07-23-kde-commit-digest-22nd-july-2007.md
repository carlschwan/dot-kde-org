---
title: "KDE Commit-Digest for 22nd July 2007"
date:    2007-07-23
authors:
  - "dallen"
slug:    kde-commit-digest-22nd-july-2007
comments:
  - subject: "Plasma Galore video Not working"
    date: 2007-07-23
    body: "Danny the plasma galore video is not playing."
    author: "Ant"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "Yep, working on getting it fixed at the moment.\nWhile i'm fixing it, click the download link underneath the video to watch it.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "The avi version looks corrupted here too, i've tested with mplayer (on linux) and with winamp on windows.\n"
    author: "Ant"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "It worked fine for me using Kaffeine."
    author: "Soap"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "for whatever reason, mplayer has problems with files that come out of recordMyDesktop. xine (and therefore kaffeine and other xine based players) work just fine, however.\n\nthat said, it works perfectly here in mplayer.. it's stretched due to my widescreen (that's a recordMyDesktop annoyance), but it looks good =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "Ok, afaik, the problem with kaffeine, mplayer etc. with recordmydesktop and similiar videos is due to an issue with the xv driver. Especially on gentoo linux I've never succeeded in playing those files with the xv driver. For mplayer just use the -vo x11 option to playback the stuff with the x11 driver. Use -vo help to display other available codecs. On my box trying to playback the files with -vo xv displays a blue screen but the x11 driver works flawlessly. Kaffeine lets you define a suitable video codec in the xine-parameters dialog.\n\ngood luck"
    author: "momesana"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "p.s. with codec I meant driver "
    author: "momesana"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "There is a youtube version now.\n\nBTW. I don't wanna be negative about plasma but I _never_ see my desktop, since I always have maximized windows. I think it can be very useful on an independent layer, something like mac's expose."
    author: "x0"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "Plasma is not just about the Desktop (as in the \"background\") : Plasma also encompasses the panels, taskbar, menus, etc.  libplasma also contains some useful code that is being adopted in decidely non-widget-like applications, such as Amarok."
    author: "Anon"
  - subject: "Re: Plasma Galore video Not working"
    date: 2007-07-23
    body: "besides, maybe when the desktop becomes more usefull and easier to access, you WILL see and use it ;-)"
    author: "jospoortvliet-former-superstoned"
  - subject: "trolls commit mass suicide after Plasma video"
    date: 2007-07-23
    body: "I miss the golden days of 2006 and all the \"Plasma seems still to exist only in /dev/imagination\", \"Most of KDE 4 is just vaporware\", \"Is nothing but a copy of Apple's dash board with some variations\", \"I'm just saying that it isn't *innovative*\" posts.\n\nTime to pre-announce KDE5 features for them to snipe at. :-)"
    author: "skierpage"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-23
    body: "> I miss the golden days of 2006\n\nlol ... i sure don't ;)\n\n> Time to pre-announce KDE5 features for them to snipe at.\n\nfocus follows mind, in KDE5! (finally!)\n\n(old kde joke there)"
    author: "Aaron Seigo"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-23
    body: "Oooh! Inside jokes! Explain it please. \n\n(As an easter egg why not include a greyed out Focus follows mind option) "
    author: "Ben"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-23
    body: "I guess you just spoiled it ;-)"
    author: "Andre"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-23
    body: "I could be done.\nIt just needs support for some kind of brain-computer interface... who knows maybe in 5/6 years it will really be a feature."
    author: "Coward"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-23
    body: "Actually, the technology already exists.  I saw it on the Science Channel.  A sensor is inserted into part of the brain that controls the hand/arm.  Then the user can control the mouse cursor with his mind by thinking about moving his hand.  Of course, it's intended for people who have lost limbs, or are paralyzed, but it mass market applications may follow as the tech evolves."
    author: "Louis"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-24
    body: "I read about that brain computer interface in Scientific American more than a couple years ago (they where using monkeys, I think) but there's also more down to  earth implementations of the this technology like the one mentioned in slashdot some time ago (http://games.slashdot.org/article.pl?sid=07/03/07/2358232&from=rss - this one is using a helmet like device) I also remember reading something about a currently available product (but only for ppc mac) somewhere but can't find the link. Apparently it was mostly used for music creation an really simple games.\nThere's also stuff like http://www.ibva.com/ ... "
    author: "The same Coward"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-24
    body: "Actually, I would contend the criticism is still pretty valid.\n\nDesktop widgets aren't new or revolutionary.  For real-life day to day usage, most people have applications open and won't see the desktop much.  I will admit that the number of data engines might make Plasma superior to all previous widget-implementations in that you can make very viable application replacements, however what I've seen is somewhat superfluous.  A dictionary applet seems nice, but it is just as easy to throw that word into the search feature of Firefox, and with various search plugins, you can pull up the same dictionary data without this additional app.\n\nAnd for all this discussion of a clean interface, the idea of cluttering my work space with widgets seems counter-intuitive.  I had a \"corkboard\" app with widgets back in the days of Windows 95, and it seemed fun, but it was never really practical.  It got in the place of proper usage.  I've toyed with various widget implementations since then, and they've never added much beyond eye candy.  My Google start page tells me the weather and such.  I don't need it on my desktop at all times.\n\nThe Plasma website spoke of rethinking of how we use our computer and this huge revolution.  Some of the KDE 4 mockups over at kde-look were pretty ingenious, but we're not seeing anything like that.\n\nIf all this development has centered on creating an API for widgets, so be it.  To each their own, and in the world of free software, either you code it yourself, or somewhat be grateful for what you get.\n\nI am still very eager for KDE 4, and I appreciate free software, but I am disappointed, and I feel misled by the very vague Plasma website.  What we are receiving really doesn't seem to add up with what the project's supposed goals were."
    author: "T. J. Brumfield"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-24
    body: "I'll have to reiterate this for the nth time: your argument does not contribute anything positive in *any* way. Instead of unconstructively bashing developers, find a way to help them."
    author: "Luca Beltrame"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-24
    body: "I use to say the problem of plasma was that page they built and the text written there.\nPlasma today looks just like a super karamba with steroids, nothing like a revolution in the desktop paradigm as announced when it was born.\n\nI was very critic of Plasma, but once I understood where it was really goind (and not that page/text) I really don't care much about it now (don't take me wrong here, it IS cool, but not what I am looking for in KDE4), it's just eyecandy for a while :)\nMaybe in KDE 4.1 we can start to see some integration of it into the apps structure and kdelibs, I sure hope so."
    author: "Iuri Fiedoruk"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-25
    body: "> Plasma today looks just like a super karamba with steroids, nothing like a\n> revolution in the desktop paradigm as announced when it was born.\n\nI don't think so. Behind the scenes, the plasma-status is already revolutionary enough for the most people, including some small developer like myself.\n\nWell, ok, we could argue wheather it's actually revolutionary, or only evolutionary. But i think, the first steps are good enough for a bright future."
    author: "Chaoswind"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-25
    body: "Well, if you think that Plasma today is something very similar to other plataforms as Karamba and that thing on Mac I can't remember the name, it's just another one.\n\nBut if you say it's a new, easier way of doing that on KDE I say: UH HU, GREAT! :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-25
    body: "\"Actually, I would contend the criticism is still pretty valid.\"\n\nYep.\n\n\"Desktop widgets aren't new or revolutionary. For real-life day to day usage, most people have applications open and won't see the desktop much. I will admit that the number of data engines might make Plasma superior to all previous widget-implementations in that you can make very viable application replacements, however what I've seen is somewhat superfluous. \"\n\nWell, the fact that they're better than any other is good enough, but, I guess they'll be integrate whit notifications and applications, know it's even extendig to applications internals, like whit amarok. So if even the kicker replacement will be a plasmoid it'll bring easy ways of replace it and a enormous integration whit others plasmoids.\n\n\"-And for all this discussion of a clean interface, the idea of cluttering my work space with widgets seems counter-intuitive. I had a \"corkboard\" app with widgets back in the days of Windows 95, and it seemed fun, but it was never really practical. It got in the place of proper usage. I've toyed with various widget implementations since then, and they've never added much beyond eye candy. My Google start page tells me the weather and such. I don't need it on my desktop at all times.\n-A dictionary applet seems nice, but it is just as easy to throw that word into the search feature of Firefox, and with various search plugins, you can pull up the same dictionary data without this additional app.\"\n\nJust because you do it in another way doesn't mean that's the better way, what is more useful:\n\na)Open Konqueror, Go Google, Check Weather.\nb)Check weather\n\nor your first example:\n\na)Install Firefox, open Firefox, add the search engine, look for the word\nb)Look for the word\n\nYou have a rare concept os usuability, and what is \"add extra applications\" as far as I am concerned is the need of install firefox, or others extra non-kde appliactions. \n\n\"The Plasma website spoke of rethinking of how we use our computer and this huge revolution. Some of the KDE 4 mockups over at kde-look were pretty ingenious, but we're not seeing anything like that.\"\n\nGive time to time. KDE 4.0 isn't KDE 4, Have you ever seen the difference between KDE 3.0 and 3.5.7?\n\n\"I am still very eager for KDE 4, and I appreciate free software, but I am disappointed, and I feel misled by the very vague Plasma website. What we are receiving really doesn't seem to add up with what the project's supposed goals were.\"\n\nHa, I don't think you know that the real goals were, me neither. But what I see is nice.\n\nSOOOO CHEERS FOR THE DEVELOPERS."
    author: "Luis"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-26
    body: ">a)Open Konqueror, Go Google, Check Weather.\n>b)Check weather\n>\n>or your first example:\n>\n>a)Install Firefox, open Firefox, add the search engine, look for the word\n>b)Look for the word\n\nWell.. I already have firefox instaled and it is open almost all the time, already with the search engine instaled because other needs. So its only go to Firefox (if I'm not already at it) and seach for the word. In the case of the plasmoid, I'd always have to click to show desktop (I never see it), install that plasmoid only for that (I don't belive that all sorts of plasmoids such as dictionaries, wether, etc will come by defaut in KDE) and finaly look for the word on that aplication that will run in the background always eating memory only for this.\n"
    author: "Manabu"
  - subject: "Re: trolls commit mass suicide after Plasma video"
    date: 2007-07-26
    body: "A super little mini application, that consume a lot less memory than Firefox.\nFirefox may be install on your computer, but KDE focus on KDE, so they'll try to give better functionality whit just KDE applications.\n\nOh, and just calling floating widgets, aka dashboard (or even clicking on show desktop if you don't have composite) is still fast than searching whit your \"probably open\" firefox."
    author: "Luis"
  - subject: "Ark"
    date: 2007-07-23
    body: "so we will have iso, zip and tar only for KDE4? I guess tar also means bzip2 and gzip right? that would be enough for me though, I don't care about rar et al though it would be nice to have 7z too :-). \nAnyway, good work, I love the new UI."
    author: "Patcito"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "Please don't confuse KDE4 and KDE4.0, support for 7z, rar and other stuff can be added in later version."
    author: "Cyrille Berger"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "Yes of course I meant KDE 4.0, sorry for the typo. Not having support for those formats during the whole 4.0 release could be a pain for some users though I personally don't care :-)"
    author: "Patcito"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "it would be great also to have a nice progress bar that shows what's happening, what files are being processed etc... "
    author: "Patcito"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "if it uses KJob, that'll show up in the kuiserver ui ... and that will hopefully be swallowed in plasma if it is running"
    author: "Aaron Seigo"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "\"...and that will hopefully be swallowed in plasma if it is running\"\n\nOoh - neat :) Are you envisioning a RunningTasks (or some better name ;)) DataEngine? It would be nice to be able to create different Plasmoids to enhance the functionality of the kuiserver - for example, by default it just lists tasks, progress, Cancel/ Pause etc, but power-users can install augmented ones as Plasmoids that also give options for what to do on completion of each individual task (run a script; log to a file; etc) or completion of all current tasks (shut down computer; etc)."
    author: "Anon"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "There's already a (basic) kicker applet for this in the KDE 4.0 alpha 2 release."
    author: "Lee"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "It uses KJob, but I created my own (somewhat broken at the moment) progress widget because I couldn't find out how to make jobs show properly in the UI server at all. *Something* appeared at the dialog, but just the application name, no job description, no error messages, no progress information, nothing. Any help on this issue would be *really* appreciated."
    author: "Henrique Pinto"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "IIRC, this is basically impossible if Ark continues to use command-line utils (tar, zip, etc) as the utils simply do not provide this information (% progress especially).\n\nIf Ark switches to a \"library\" style of compressing/ decompress e.g. using libtar, libzip etc, then this is much easier."
    author: "Anon"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "If Ark uses command line functions, then why is is so unbelievably slow?"
    author: "James Richard Tyrer"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "Because it uses command line utils?"
    author: "Martin"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "Maybe some more-or-less-broken KIO behaviour, so it first copies the file, then (de)compresses it, then copies it back or something... It didn't have a very active maintainer for a long time. I thought someone did pick it up for KDE 4, though."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "There is worse: I was waiting for kio_rar and kio_7z to make their way\nto the standard kde kio_slaves (I'm using them actually on kde 3.5.7, and beside 7z being a bit slow, they're great!!!)"
    author: "matte"
  - subject: "Re: Ark"
    date: 2007-07-23
    body: "Depends on the freeze date. If the freeze is on July 25 (in two days!), then I'll have no time to implement more plugins. If the freeze is moved to August, I'll work to have at least RAR and 7Z supported too."
    author: "Henrique Pinto"
  - subject: "Please rar"
    date: 2007-07-23
    body: "rar is important for advanced computer users who prefer rar always over zip. having rar support in KDE 4.0 will be a good required feature."
    author: "Fast_Rizwaan"
  - subject: "Re: Please rar"
    date: 2007-07-23
    body: "I guess it could always be added as a plugin at a later date than the freeze anyway, so any distro's could still choose to include it, even if it is not in the official KDE 4.0 release."
    author: "Andre"
  - subject: "Re: Please rar"
    date: 2007-07-23
    body: "yes, later as rar, 7z etc. plugins at kde-apps or by distros would also be good. thanks for all the \"passionate\" work with KDE!"
    author: "Fast_Rizwaan"
  - subject: "Re: Please rar"
    date: 2007-07-23
    body: "Just so you know, I've never seen a Linux user use either RAR or ZIP. They all use gzip or bzip2. Of course, RARs and ZIPs are often *received* from somewhere..."
    author: "logixoul"
  - subject: "Re: Please rar"
    date: 2007-07-23
    body: "Or *sent* to somewhere..."
    author: "G\u00f6ran Jartin"
  - subject: "Re: Please rar"
    date: 2007-07-23
    body: "> KDE 4.0 will be a good required feature.\n\nKDE 4.0 will be a tech demo. It's better not to expect too much and wait for some 4.1.x or 4.2.x release that will actually provide a decent number of applications and features as well as the single feature \"number of bugs you can live with\", before it's worth comparing KDE 4 with KDE 3.5.x functionality-wise."
    author: "Carlo"
  - subject: "Re: Please rar"
    date: 2007-07-23
    body: "\"advanced computer users\"?\n\non unices, advanced computer users use gzip or bzip2."
    author: "Patcito"
  - subject: "Why RAR?"
    date: 2007-07-25
    body: "Why not use 7z instead? 7-Zip/p7zip is Free Software (once you remove the non-Free RAR decompression plugin as has been done in the Fedora and Debian packages) and it also compresses better. And you don't have to pay a registration fee for compression."
    author: "Kevin Kofler"
  - subject: "Re: Why RAR?"
    date: 2007-07-26
    body: "> Why not use 7z instead?\n\nBecause the user interface is crap."
    author: "Carlo"
  - subject: "Re: Why RAR?"
    date: 2007-07-27
    body: "7z is a format.  There is no official 7z gui for Linux, so I'm not sure how you're so sure the user interface is crap when the user interface doesn't exist.\n\nPerhaps you're talking about the 7z file manager in Windows?  That doesn't have the best UI, but I'd take that over Ark in a heart beat.\n\nRegardless, rar isn't a free or open format.  I believe the poster was suggesting that instead of fighting for a rar plugin, doesn't it make more sense to first focus on a 7z plugin since it is an open source alternative, at it compresses better?"
    author: "T. J. Brumfield"
  - subject: "Re: Why RAR?"
    date: 2007-07-27
    body: "> There is no official 7z gui for Linux\n\nI meant the crappy command line interface, of course.\n\n\n\n> Regardless, rar isn't a free or open format.\n\nI didn't say a word about rar, nor do I care for it, the need to unpack an incoming rar archive every now and then aside."
    author: "Carlo"
  - subject: "Re: Why RAR?"
    date: 2007-07-27
    body: "> I meant the crappy command line interface, of course.\n\nHow does it get easier than:\n7z a foo.7z file1 file2 file3 ...\nfor compressing and\n7z x foo.7z\nfor decompressing?\n\nAnd as a GUI, the latest Krusader (1.80.0) supports 7z."
    author: "Kevin Kofler"
  - subject: "Re: Why RAR?"
    date: 2007-07-29
    body: "I believe peazip does as well, but neither are official gui ports of 7-zip."
    author: "T. J. Brumfield"
  - subject: "Plasma Video ?"
    date: 2007-07-23
    body: "Which plasma video? What is everybody talking about? I see no links to a video :("
    author: "PlasmaBeliever"
  - subject: "Re: Plasma Video ?"
    date: 2007-07-23
    body: "look at the digest itself :-)"
    author: "heimo-p"
  - subject: "besides whats obviously cool..."
    date: 2007-07-23
    body: "...my most hated UI glitch is still in there - gross that's UGLY:\nplease please please someone fix the columns to automatically fit into the damn windows, *please* .. it was annoying in the KDE3 file dialogs and is still so, even if some windows have to be made a bit wider- please make it all go awaaaaayyyy.... maybe the twitching will stop with that, too.\nAlso, I hope someone is supervising the usability of Oxygen on small resolutions (icon borders)..\nWhat would be awesome aswell is if Plasma could not only support SVGs with animation, but also MNGs, for example for icons - much less resource intensive and still kewl.\nAnd what you said about the media notifier stuff is right Aaron - but I hope people will be able to put those Plasma blobs in a container of their choice, i.e. in a boring old style window f.e., or on the desktop, or in those slicker expanders..\nwell anyways good work keep on it."
    author: "eMPee584"
  - subject: "Re: besides whats obviously cool..."
    date: 2007-07-23
    body: "I have to agree that columns auto-width function needs some work.\nBut about oxygen, small resolution , handheld and such should really use\nsomething more simple, so I don't think that developers time should be wasted\nto adapt it to little displays."
    author: "mat"
  - subject: "Re: besides whats obviously cool..."
    date: 2007-07-23
    body: "> I hope someone is supervising the usability of Oxygen on small resolutions (icon borders)\n\nSure, David Miller is doing that."
    author: "logixoul"
  - subject: "why?"
    date: 2007-07-24
    body: "Why would the mayor of Toronto be interested in some flowerish icon theme when he has far more important issues to tackle?\n> Mayor David Miller has a vision for Toronto. He wants to build a revitalized\n> city of strong, safe neighbourhoods, clean streets, effective public services, ...\n\n\nscnr >:==)\n"
    author: "eMPee584"
  - subject: "Re: besides whats obviously cool..."
    date: 2007-07-31
    body: "I prefer using SVG (scalable vector graphics) for everything rather than raster \"fixed\" images."
    author: "asdx"
  - subject: "Bravo!"
    date: 2007-07-23
    body: "Nice work! Thank you all so much. I am happy for anything that keeps a commercial OS out of my life. Thank you!"
    author: "winter"
  - subject: "Mouse gesture recognition rewrite?"
    date: 2007-07-23
    body: "Does the global shortcut system also involve mouse gestures? Its rewrite reminded me of the mouse gesture recognition, and if I remember correctly, it was seen as being needed to rewrite that, too because being limited and/or messy(?). Is there anything planned? Will the old system have to stay if there's no rewrite?\n\nSome more advanced systems: a freehand system on kde-apps and a recognition system from a Trolltech employee:\nhttp://kde-apps.org/content/show.php/Freehand+Gestures?content=30108\nhttp://www.thelins.se/index.php?title=Mouse_Gesture_Recognizer\n\n(which was also being featured here: http://doc.trolltech.com/qq/qq18-mousegestures.html  )"
    author: "Phase II"
  - subject: "Re: Mouse gesture recognition rewrite?"
    date: 2007-07-23
    body: "hm... i'd love to see such easy to handle mouse gestures like in opera for konqueror (and maybe dolphin). hope they are going to improve them and the menu where you can find them... i didn't know for years that kde is capable of mouse gestures"
    author: "LordBernhard"
  - subject: "Re: Mouse gesture recognition rewrite?"
    date: 2007-07-23
    body: "ha!\n\nThe \"Freehand Gestures\" is actually my old project from before I decided to work on integrating Magnatune.com into Amarok. I though that was dead, buried and forgotten :-)\n\nI spend all my allotted \"hacking time\" on Amarok now, so I have not had any time to work on it since unfortunately. If somebody wants to pick it up, they are more than welcome ( and I will help them get started ). In the repository, there is actually the basics of a pretty unique gesture editor where you just draw a gesture a number of time and the editor calculates the parameters based on the variations in your drawing of the gesture.  \n\n- Nikolaj"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Mouse gesture recognition rewrite?"
    date: 2007-07-23
    body: "Rocker gestures please! Pretty please! Or just forget the pleads and implement it.\n(Rocker gestures = click and hold one button, and then click the other one)\nOpera has it, and at least one of the FF gesture plugins.\n\nAlso, is the voice recognition still in, after the rewrite?"
    author: "Martin"
  - subject: "Plasma Speed"
    date: 2007-07-23
    body: "The vid is absolutely great and cause of that: plasma is absolutely stunning ^^ the only 2 things i worry about are those:\n\n1.) the widgets are really fast, but when you resize them they get really slow.. is this just a work in progress and in the future it won't make any differnece or isn't that an avoidable problem?\n\n2.) the alpha 1 and before had a zoom function on the desktop... it wasn't really fast (problem mentione above) but it worked.. where has it's functionality gone.. i havn't seen any blog entry that this features had been canceled (has it? the button are still there and it has also been mentioned in the vid (3 zoom steps???))\n\nbut all in all you've all done really great work... i hope i'll have enough time in the near future to help you coding\n\nfriendly greetings\n\nbernhard"
    author: "LordBernhard"
  - subject: "Re: Plasma Speed"
    date: 2007-07-23
    body: "for the first question, I think thats because when you resize a Plasmoid, the SVG image needs to be resized and rendered.I Guess it will not change in future.\n\nas I heared in IRC and Mailing List, people are working on Zoom functionality."
    author: "Emil Sedgh"
  - subject: "Re: Plasma Speed"
    date: 2007-07-24
    body: "well... i know that the svg image needs to be rendered new.. but why does it behave so slow AFTER resizing?"
    author: "LordBernhard"
  - subject: "Re: Plasma Speed"
    date: 2007-07-25
    body: "maybe because it has to be rerendered all the time or something? the SVG/icon cache just got in, it might solve it."
    author: "jospoortvliet"
  - subject: "Re: Plasma Speed"
    date: 2007-07-24
    body: "As an answer to question 2:\n\nI worked on the zooming, but for some reason the connection between the buttons and the zooming functions were removed.  I have recently started to continue working on the zooming functionality (after some time with a broken build environment).  Work on it has been dead since my initial commits.  There has been some discussion recently on the panel-devel mailing list.\n\nAs far as speed goes, I am planning on replacing the applet with a shaded rectangle around the bounding box.  Because it won't be drawing the svgs when zooming, there should be a significant speed increase.\n\nFor more information on the functionality, see here:\nhttp://mail.kde.org/pipermail/panel-devel/2007-July/001511.html"
    author: "Aaron (fonz)"
  - subject: "No more gray?"
    date: 2007-07-25
    body: "Well, I can truly say I'm blown away by all the fantastic work going on - getting better every day! One thing I'd like to discuss is the colors of the menu bar, arrows and window decoration in KDE applications like Konqueror, Dolphin, digikam and KOffice suite for example. Currently it's a monotonous sort of gray/white, reminiscent of windows 95 (no offence meant!). I was very impressed by the soft blue colors in (gasp!) Office 2003, which had options highlighted in orange - a combo better on the eye than the previous white/gray/blue. I was thinking maybe we could have a soft green gradient default color for our menu bars and arrows (though all customizable in true KDE fashion), perhaps with soft red?/yellow? highlight of options. I showed an example of what I mean in the attachment. It's Firefox with the Office2003 skin. Please note that by no means am I suggesting we copy what others do, but simply draw inspiration and make our own better alternative.\n\nI realize there's still plenty of time til KDE 4 is released, but what do our gui gurus have planned? Will we depart from the old-school colors and forge something better? If this post sounds annoying or rude, then it was not meant in that way. I hope the community can have a friendly and frank discussion over these areas.\n\nGood luck with all your projects!"
    author: "Denise Burton"
  - subject: "Re: No more gray?"
    date: 2007-07-26
    body: "That's a good comment.  I personally would like to see a more colourful, vibrant colour scheme supplied out of the box, along with more backgrounds in various applications to add some variety.\n\nThe desktop itself is well on the way there.  \n\nWith regards to the competition, Microsoft have noticeably put effort into de-graying their applications and desktop over the past few years and I think it looks better for it - although very strong colours can be fatiguing if you have to look at them for extended periods of time.  If I remember rightly (it has been a while since I used Microsoft Office seriously ) I replaced the strong blue colours with something else on Publisher 2003 for this reason. \n\nAs for what colours would replace grey, that's not an easy question.  KDE's traditional colours have been light blue.  The KDE 4 logo uses a various darker shades of blue and the desktop itself currently uses translucent black backgrounds with translucent 'silver' borders."
    author: "Robert Knight"
  - subject: "Re: No more gray?"
    date: 2007-07-26
    body: "From what I've seen the new Oxygen widget style and theme however were gray.  I like gray personally, and dark tones, but often I like to use a touch of gold for a highlight color to dress up a gray theme."
    author: "T. J. Brumfield"
---
In <a href="http://commit-digest.org/issues/2007-07-22/">this week's KDE Commit-Digest</a>: <a href="http://plasma.kde.org/">Plasma</a> progress, with new Plasmoids: Browser, Notes, 3D Earth Model, Twitter, Desktop, and Tiger (scripting example), and the development of a mouse cursor data engine. Bug fixing spree in TagLib, <a href="http://k3b.org/">K3b</a>, and the <a href="http://kopete.kde.org/">Kopete</a> Cryptography plugin. Support for encrypted storage devices in <a href="http://solid.kde.org/">Solid</a>, with better integration of device support in <a href="http://amarok.kde.org/">Amarok</a>. Further integration of Plasma in Amarok. Work on making <a href="http://konsole.kde.org/">Konsole</a> follow KDE settings more strictly. Much work on revamping <a href="http://en.wikipedia.org/wiki/Ark_(computing)">Ark</a> for KDE 4. Various functionality improvements in <a href="http://uml.sourceforge.net/">Umbrello</a>. The start of a new version of the Smoke bindings access mechanism, Smoke2. Continued work in kdegames, with the import of KBlocks, a Tetris-like replacement for KSirtet. Rewrite of search-and-replace in <a href="http://kate-editor.org/">Kate</a>. Import of the Kubelka-Munk mixing algorithm, with a restoration of the MetaData framework in <a href="http://www.koffice.org/krita/">Krita</a>. Work to enable networked document collaboration imported in <a href="http://koffice.org/">KOffice</a>. GetHotNewStuff experimentally reactivated in <a href="http://okular.org/">okular</a>. A rewrite of the global shortcuts system added to KDE SVN. <a href="http://code.google.com/soc/kde/appinfo.html?csaid=1EF6392A4C8AEADD">Pixmap Cache</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> project merged into kdelibs, in time for KDE 4.0. KBoard, renamed Tagua, is removed from KDE SVN to continue in an external code repository.

<!--break-->
