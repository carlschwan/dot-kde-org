---
title: "KDE 4.0 Release Candidate 1 Hits the Streets, Codename \"Calamity\""
date:    2007-11-21
authors:
  - "sk\u00fcgler"
slug:    kde-40-release-candidate-1-hits-streets-codename-calamity
comments:
  - subject: "Dock"
    date: 2007-11-20
    body: "New dock is so cool ... can't really wait for KDE 4!"
    author: "bele"
  - subject: "Re: Dock"
    date: 2007-11-20
    body: "Cool, are there any screenshot's of this on the Net?"
    author: "Fred"
  - subject: "Re: Dock"
    date: 2007-11-20
    body: "Better - aseigo has a screencast, which should be released as part of this week's commit-digest!"
    author: "Anon"
  - subject: "Re: Dock"
    date: 2007-11-20
    body: "but, but, but...I want it NOW!!\n\n:-)"
    author: "Fred"
  - subject: "Re: Dock"
    date: 2007-11-20
    body: "Please please please... Publish this week's commit-digest, pleasseeeee !!! :)"
    author: "Plop"
  - subject: "Re: Dock"
    date: 2007-11-21
    body: "http://www.tuxmind.altervista.org/wp-content/uploads/2007/11/kde_4_rc1.jpg\nFound one."
    author: "Jonathan"
  - subject: "hope"
    date: 2007-11-20
    body: "im using the svn version.\n\nmany people says that kde3.0 was untable, full of bug and that kde will be the same, but kde4 look really stable, some parts crash, but the are apps like juk, kwin4 like composite works without problems, dolphin stable, i have hope that when kde4.0 is released can be used like kde3.5.x."
    author: "Rudolhp"
  - subject: "Re: hope"
    date: 2007-11-20
    body: "> are apps like juk\n\nremember that kde3 apps work just fine in kde4 sessions. yesterday i accidently launched konqueror from kde3 and it took me a few moments to figure out why it looked so odd, even if the rendering viewport was updating better. ;)\n\n> can be used like kde3.5.x\n\nwell, it'll be usable for day to day work. 3.5 was rather polished, though. but yeah, you should be able to get by just fine =)"
    author: "Aaron J. Seigo"
  - subject: "Re: hope"
    date: 2007-11-20
    body: ">remember that kde3 apps work just fine in kde4 sessions. yesterday i accidently launched konqueror from kde3 and it took me a few moments to figure out why it looked so odd, even if the rendering viewport was updating better. ;)\n\njuk from kde4 ;)"
    author: "Rudolhp"
  - subject: "Congratulations!"
    date: 2007-11-20
    body: "Congratulations to all contributors of KDE 4 on reaching this milestone! Excellent work, and I'm looking forward to a splendid KDE 4 series."
    author: "Andr\u00e9"
  - subject: "Re: Congratulations!"
    date: 2007-11-21
    body: "I too wish to offer my Congratulations."
    author: "Ben"
  - subject: "Debian"
    date: 2007-11-20
    body: "First: Thanks to all the KDE-Devs for your hard work! I'm shure KDE4 will rock!\n\nI'm currently switching back from Kubuntu to Debian unstable. It's been a while, since I last used Sid, so here my question: are RC-Packages uploaded to unstable, or just to experimental?\nHopefully the Debian KDE maintainers release a new livecd with RC1-Packages soon (currently beta4):\nhttp://pkg-kde.alioth.debian.org/kde4livecd.html"
    author: "Sepp"
  - subject: "Re: Debian"
    date: 2007-11-20
    body: "Why not use the openSUSE based one?"
    author: "Anonymous"
  - subject: "Re: Debian"
    date: 2007-11-20
    body: "Maybe because he intends to run KDE on Debian?\n\nThe most plataforms this RC is tested on, the most bugs will be discovered and ironed.\n\nBesides, the Debian KDE maintainers have done a top-notch work while packaging the betas, identifying a number of problems.\n\n\nSepp: I'm afraid it will take a while for KDE 4.0 to land on Sid..."
    author: "Quique"
  - subject: "Re: Debian"
    date: 2007-11-20
    body: "Good, but not boot it Suse live-cd to me\nsorry my 'english' \n"
    author: "Juhani Lyly"
  - subject: "Re: Debian"
    date: 2007-11-20
    body: "Because Debian is better than Suse? XD"
    author: "I\u00f1aki Baz"
  - subject: "Re: Debian"
    date: 2007-11-21
    body: "I switched from SuSe to Debian a year ago, and now I ask myself: Why did I wait so long?!"
    author: "Rob"
  - subject: "Re: Debian"
    date: 2007-11-20
    body: "They are only in experimental.  However you can run them very easily.  Add the experimental repos to your sources.list, then start aptitude with aptitude -t experimental, and upgrade all your KDE packages to 3.9.96.  When I upgraded from KDE3 to KDE4 beta 3, it more or less hosed my KDE installation, and I had to force some packages to install manually with dpkg.  However it is getting better and better, so soon you should be able to run all the KDE3 apps side by side with the kde4 versions.  \n\nAt the moment, most of RC1 is there.. Still waiting for oxygen icons package to be able to upgrade kdebase, but everything else upgraded fine."
    author: "Leo S"
  - subject: "Re: Debian"
    date: 2007-11-21
    body: "Do kmail and digikam (both KDE3) still work if I upgrade to KDE4 in Debian?\n\nWhat do you mean by \"hosed\"?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Debian"
    date: 2007-11-21
    body: "Hosed as in \"I had to uninstall a lot of KDE3 apps because they conflicted with KDE4 packages\".  Not sure if this is still the case\n\nSeems like digikam and kmail can be used side by side with KDE4. Not 100% sure because I don't use them myself.  However some apps (yakuake for example) will not conflict but will no longer run (crashes on start).\n"
    author: "Leo S"
  - subject: "Re: Debian"
    date: 2007-12-08
    body: "now the only conflicts are kcontrol, kdebase-bin-kde3, kdesktop, kformula, and kpersonalizer. Oh and the oxygen icon package is now in experimental also. I intend to be totally insane and go fully experimental and unstable.\n"
    author: "G2g591"
  - subject: "Re: Debian"
    date: 2007-11-21
    body: "Thanks, I'll try it that way, then. I currently have a fresh install anyway, so if things go seriously wrong, I can wipe my HD and start again..."
    author: "Sepp"
  - subject: "Nearly all there"
    date: 2007-11-20
    body: "Congratulations guys, I am using an svn version updated an hour ago and it rocks!  The only small problems I see are:\na) System tray is WIP\nb) Krunner dialog looked nicer in last weeks version\nc) Still can't drag 'widgets' to the bottombar (which is immovable)\nd) Location bar height has been squeezed to < font height in Konq\ne) Okular will not show PS in the window but in a tiny separate window.\nThese are mostly cosmetic problems and are clearly obvious to the talented developers (so I don't clutter bugs.kde with them).  This is just to forewarn those who will be testing this Beta.\n\nLooking good guys!  - Especially with the dark 'Wonton Soup' colorscheme for those of us who value our retinas ;)"
    author: "fogge"
  - subject: "Re: Nearly all there"
    date: 2007-11-20
    body: "a) System tray is WIP\n\nyep. getting closer every day now though.. it positions itself properly on screen even. ;)\n\nb) Krunner dialog looked nicer in last weeks version\n\nit's just a different SVG showing that krunner is more properly themable, following along with the other plasma bits, now.\n\nc) Still can't drag 'widgets' to the bottombar (which is immovable)\n\nyou can now, but it's still not movable by the user (it is movable though in the code ;)\n\nd) Location bar height has been squeezed to < font height in Konq\n\nbeing addressed right now on k-c-d; qt style issues\n"
    author: "Aaron J. Seigo"
  - subject: "Congratulations"
    date: 2007-11-20
    body: "Looks like the day I'm goind to do the switch to 4 is coming near.\nJust some questions about KDE4, so I can decide if RC1 is for me:\n\n- Does it use much more resources than 3? I have a small duron 1.6Ghz with 512 RAM, you know.\n- is it stable enought to at least run without crashing without user interaction? (I mean, opening ktorrent and letting the box downloading) :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Congratulations"
    date: 2007-11-20
    body: "im testing from bet3 and feels really stable. the problemas are the non main apps, like juk, okular, the fails, not often, but fails."
    author: "Rudolhp"
  - subject: "Re: Congratulations"
    date: 2007-11-20
    body: "CPU is rarely the bottleneck nowadays, do here are some informal mem stats.  Here, \"memory use\" is defined as the entry in the \"used\" column and the \"-/+ buffers/cache:\" row in the output of free -m.  Note that I use my own custom Qemu VM image, so there is some overhead - in particular, it uses the KDE3 KDM.  The KDE4 install was compiled with maximum debugging info, then all executables were stripped with objdump.\n\n- X + KDM only (baseline): 34M\n- Boot to default Plasma, with background (+wallpaper), panel, klipper, Kickoff and Clock: 76M\n- As above, but with a Konsole open (2 tabs), very small KWord (alpha still!) document, very small kwrite document, and Konqueror with one tab open to dot.kde.org - 142MB.\n\nSo I think it's using slightly more than KDE3 at the moment (although if someone running KDE3 could repeat the experiment and give numbers, that would be great!), but considering the devs are mainly at the Make It Work/ Make it Work Well phase, rather than the Make it Work Fast (and Light), I think it's doing pretty OK.  IIRC, KWord took up a big chunk of this, which comes as no surprise as it is still alpha.\n\nSo 512MB should be fine, I'd say :)"
    author: "SSJ"
  - subject: "Re: Congratulations"
    date: 2007-11-20
    body: "Thanks a lot for the info!\nLooks like I'll have the chance to test kubuntu's packages :D"
    author: "Iuri Fiedoruk"
  - subject: "Re: Congratulations"
    date: 2007-11-21
    body: "CPU is an issue for some people, e.g. the teams that refurbish old donated hardware with Linux and give it to the needy. (and no, I can't say I'm one of them) So it shouldn't be ignored.\n\n"
    author: "Ben"
  - subject: "Re: Congratulations"
    date: 2007-11-22
    body: "CPU is an issue with my wife. She has a 750 Mhz Duron machine with 512 MB RAM. Now she is using KDE 3.5.8 and it's not fast but fast enough. I would like to put KDE 4 on her computer but, if it is much slower that 3.5 I won't change it over. I guess I'll stop upgrading her box before KDE 4 goes into Lenny (now I upgrade it with apt-get from time to time).\n\nIt's not that we are poor; we can afford to buy her a new computer but she wouldn't want to buy one when she's getting along just fine with what she has."
    author: "Kyo"
  - subject: "Re: Congratulations"
    date: 2007-11-22
    body: "I think you shouldn't count on KDE 4.0 being as fast and optimized as 3.5.8. It will get better over time as things stabilize a bit, but I think the focus for 4.0 is on getting it to work, not on getting it to work fast."
    author: "Andr\u00e9"
  - subject: "Re: Congratulations"
    date: 2007-11-22
    body: "Indeed.  Note in particular that KDE3.0 was hardly a speed (or not-using-much-memory) demon when released, but steadily got lighter and lighter as time went on."
    author: "Anon"
  - subject: "Re: Congratulations"
    date: 2007-11-21
    body: "I tried beta 4 and last barely a few hours with it because it was considerably slower than 3.5.  Eating up large amounts of memory, so hopefully that has improved.  "
    author: "Richard"
  - subject: "Congratulations"
    date: 2007-11-20
    body: "Congratulations.Everyone, This Is The Result Of Many Years Of Development Of A Huge Community.This Is The Moment That We Were All Waiting For!\n\n(My experience: Applications are in a good shape, stable and usefull.but Workspace needs the last bits...oxygen, plasma and kwin need some little things to be done)"
    author: "Emil Sedgh"
  - subject: "kde4rc1"
    date: 2007-11-20
    body: "great work, guys... today, i updated svn and i couldn't belevie my eyes! it's so wonderful! thank you a lot\n\nP.S.: after locking the screen i can't get back, so i have to restart X, my password isn't accepted at all.. not a disaster, but... :)"
    author: "duSoo"
  - subject: "Re: kde4rc1"
    date: 2007-11-20
    body: "http://techbase.kde.org/Getting_Started/Build/KDE4#Locked_sessions might help out with that issue."
    author: "Chippy"
  - subject: "Re: kde4rc1"
    date: 2007-11-20
    body: "Interesting that this bug appears here again. I don't use KDE4 and had this problem for ages with Kubuntu. After every \"apt-get upgrade\" the problem was back again and I had to do a CHMOD on that file. I always thought it was a distro problem and a little bit annoyed that they didnt solve this seemingly simple bug for years."
    author: "Michael"
  - subject: "Re: kde4rc1"
    date: 2007-12-29
    body: "If you ever find yourself in this annoying situation, try logging in as root from a VT and typing:\n\nkillall kdesktop_lock\n\nHope they fix it soon :)"
    author: "Luke Anderson"
  - subject: "Great news!"
    date: 2007-11-20
    body: "Thanks for all the hard work, everyone! (devs, artists, documentation teams, etc)"
    author: "mactalla"
  - subject: "Re: Great news!"
    date: 2007-11-20
    body: "I second that.\nThanks for all this.\n\nyou made my day, and my desktop :D"
    author: "Shuss"
  - subject: "Re: Great news!"
    date: 2007-11-21
    body: "Yes, thank you all for all the hard work (and patience)."
    author: "nae"
  - subject: "dot.kde.org is UGLY"
    date: 2007-11-20
    body: "The dot needs an update. See\n<A href=\"http://bugs.kde.org/show_bug.cgi?id=152375\">http://bugs.kde.org/show_bug.cgi?id=152375</A>\n\nDear dotties, please vote and hope ;-)"
    author: "Anonymous Coward"
  - subject: "Re: dot.kde.org is UGLY"
    date: 2007-11-20
    body: "I think this is the lowest priority for now.\nThis site uses slashcode (some old version i guess), it does the job well enough. Even though i'm a hardcore KDE user and like eye-candy, i don't see any reason to \"upgrade\" the site to a different back-end."
    author: "anonymous"
  - subject: "Re: dot.kde.org is UGLY"
    date: 2007-11-21
    body: "No it isn't, it's based on squishdot(http://www.squishdot.org), which is based on Zope(http://www.zope.org).\n"
    author: "Andreas"
  - subject: "Re: dot.kde.org is UGLY"
    date: 2007-11-21
    body: "Well, it IS kind of weird that akregator can't get the story contents in a \"normal\" way. I would like to see better RSS support"
    author: "Fred"
  - subject: "Why not Kalamity?"
    date: 2007-11-20
    body: "I am a bit surprised that Calamity isn't written with K. I really admire you guy's fantasies though.\nThanks for you great work and the fun in it :)"
    author: "Bobby"
  - subject: "Re: Why not Kalamity?"
    date: 2007-11-20
    body: "They started recently to always use names with \"C\", making fun of the old \"K\" naming habit. Expect more such names now. :-)\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Why not Kalamity?"
    date: 2007-11-21
    body: "I guess that should be \"Cay\" :-)\n\nI'm also very excited, and can't wait to get the stable release. Have any of you installed the RC on Slackware 12 (or Slamd64 12)?\n\nJim"
    author: "doeppjakab"
  - subject: "Re: Why not Kalamity?"
    date: 2007-11-21
    body: "Personally I second this.  The rampant K-ing of things has always titillated this editor."
    author: "Wyatt"
  - subject: "Re: Why not Kalamity?"
    date: 2007-11-21
    body: "Its a joke about the constant use of Ks \n\nI think they'll be back to Ks as soon as the betaing is over. "
    author: "Ben"
  - subject: "Re: Why not Kalamity?"
    date: 2007-11-21
    body: "Kalamity would be better for the German speaking users (like me) because C tends to stick in the  throat :D"
    author: "Bobby"
  - subject: "Re: Why not Kalamity?"
    date: 2007-11-22
    body: "Yes, it is just a RC.These problems will get fixed."
    author: "bull"
  - subject: "What's the status of plasma with python?"
    date: 2007-11-20
    body: "Is it possible with calamity, to write plasmaoids in python?"
    author: "Chaoswind"
  - subject: "Active Window"
    date: 2007-11-21
    body: "Judging from current screenshots the active and inactive windows seem to have no visual distinction. Will KDE 4 have an option to color the caption of the active window in anything but grey? Or maybe better, frame the entire window with a thin colored line? I think the lack of distinction between active and inactive windows is one of the most serious usability flaws of Mac OS 10.4."
    author: "Dominic"
  - subject: "Re: Active Window"
    date: 2007-11-21
    body: "The default colors are configurable.  In fact, most of the critizism of oxygen comes from the poor color scheme, but if you choose the other themes, its very pretty. I especially like the dark green one (moonlight meadow or something like that... its really beautiful.)"
    author: "Level 1"
  - subject: "Re: Active Window"
    date: 2007-11-23
    body: "\"default colors\" and \"poor color scheme\" really shouldn't go together :("
    author: "MamiyaOtaru"
  - subject: "Re: Active Window"
    date: 2008-07-22
    body: "activar windows"
    author: "mllorent"
  - subject: "kubuntu gutsy packages available!"
    date: 2007-11-21
    body: "kubuntu gutsy gibbon packages available:\nhttp://kubuntu.org/announcements/kde4-rc1.php"
    author: "mikkael"
  - subject: "Re: kubuntu gutsy packages available!"
    date: 2007-11-21
    body: "But these packages doesn't contain the new panel :(... and many plasmoids are missing... I think I will go back to SUSE"
    author: "Kubuntu user"
  - subject: "Re: kubuntu gutsy packages available!"
    date: 2007-11-21
    body: "Ugh, kubuntu packaging of kde4 has been sub par. It makes KDE4 look buggier than it really is. I suggest people use opensuse if they want to test drive kde4."
    author: "Garza"
  - subject: "Re: kubuntu gutsy packages available!"
    date: 2007-11-21
    body: "I am using openSuse but I haven't been able to test rc1 properly. I updated from the latest beta to rc1 yesterday and realized that a few things were missing so I uninstalled everything and did a reinstall but I can't start kde4 anymore. I don't know if there is anyone here with similar problem."
    author: "Bobby"
  - subject: "Re: kubuntu gutsy packages available!"
    date: 2007-11-22
    body: "Have you tried deleting the ~/.kde4 directory (or, move it somewhere if you've done more than just look at older versions of KDE4 and wanna keep your settings)?"
    author: "Sutoka"
  - subject: "Re: kubuntu gutsy packages available!"
    date: 2007-11-22
    body: "I found the problem. I installed the default system but forgot to install the basis system. Now it's running but I haven't had any background picture since the last beta and some plasmiods that were in beta 3 are missing even though plasma extra-gears is installed. \nI also deleted the .kde4 directory more than once but there is still no background picture and the plasmiods are still missing."
    author: "Bobby"
  - subject: "Re: kubuntu gutsy packages available!"
    date: 2007-11-23
    body: "Compile KDE-svn if you really want to test kde4. It's a snap with the kdesvn-build script (yes, also in *buntu). I\u00b4m typing this comment from KDE-svn now.\n\nIt is really workable and it's friggin BEAUTIFUL. "
    author: "Coolvibe"
  - subject: "Software hits the streets"
    date: 2007-11-21
    body: "What kind of word do we live in when kids can pick up software from the streets?\n\nYou don't know where that software has been."
    author: "T. J. Brumfield"
  - subject: "Not a troll"
    date: 2007-11-21
    body: "I don't want to sound too trollish.\n\nWhat ever happened to konstruct?  Not that I don't like the build instructions on the techabse website the explanations are great, it just seems a lot of time is spent building these packages when it would be great if it was still automated.  It would be nice just to download, compile and install so that I can get on with the testing side of things.  If something doesn't work during the compiling I'm just going to wait for the packages, which is a shame.\n\n"
    author: "matt"
  - subject: "Re: Not a troll"
    date: 2007-11-21
    body: "Great example...  Guess I'll just wait for the packages\n(no i'm not going to file a bug report.  I have no idea what information would be needed. Nor do I have a clue what the problem could be, especially since it is likely to be machine specific.  My concern is not compiling and packaging. All I really want to do is test if my day to day tasks are going to still work and log bugs based on that, so that when the final release is available I can put it on my work desktop.)\n\nSS -I/home/kde-devel/qt-copy/mkspecs/linux-g++ -DHAVE_QCONFIG_CPP -DQT_NO_THREAD -DQT_NO_QOBJECT -DQT_NO_GEOM_VARIANT  /home/kde-devel/qt-copy/src/corelib/io/qtextstream.cpp\n/home/kde-devel/qt-copy/src/corelib/io/qtextstream.cpp: In member function \u0091bool QTextStreamPrivate::getReal(double*)\u0092:\n/home/kde-devel/qt-copy/src/corelib/io/qtextstream.cpp:1940: error: \u0091qQNaN\u0092 was not declared in this scope\n/home/kde-devel/qt-copy/src/corelib/io/qtextstream.cpp: In member function \u0091QTextStream& QTextStream::operator<<(double)\u0092:\n/home/kde-devel/qt-copy/src/corelib/io/qtextstream.cpp:2414: error: \u0091qIsNaN\u0092 was not declared in this scope\nmake: *** [qtextstream.o] Error 1\n"
    author: "matt"
  - subject: "Re: Not a troll"
    date: 2007-11-21
    body: "try http://kdesvn-build.kde.org\n\nCheers, non-troll :P"
    author: "Troy Unrau"
  - subject: "Re: Not a troll"
    date: 2007-11-21
    body: "HOLY, this is fan-flippin-tastic.\n\nCheers\n\n"
    author: "Matt"
  - subject: "Re: Not a troll"
    date: 2007-11-21
    body: "More importantly, how come the build instructions linked from the RC1 info page are to make an SVN build?  I tried those instructions for the last beta and they just didn't work.  Shouldn't the build instructions for RC1 tell you how to build RC1?\n"
    author: "Jim"
  - subject: "Great Job!"
    date: 2007-11-21
    body: "I just loaded the SUSE live disc, and RC1 looks amazing. Love the look and feel, the widgets look great, and the applications are running nicely! Great job devs! KDE4 looks like it's going to be a joy to use!"
    author: "Incense"
  - subject: "hrmf..."
    date: 2007-11-21
    body: "about the only thing that looks interesting there is the new kmenu...\n\nthe rest? meh...\n\nbtw, does anyone have a list of whats being done with konqueror? i keep hearing that its not being neglected over dolphin but im not sure where to look for news about whats being worked on."
    author: "turn_self_off"
  - subject: "Re: hrmf..."
    date: 2007-11-21
    body: "The much-requested \"Undo close tab\" has been written, for a start, but unfortunately it was not finished in time for the 4.0 feature freeze.  Expect it in 4.1."
    author: "Anon"
  - subject: "Re: hrmf..."
    date: 2007-11-21
    body: "\"interesting\" things are forbidden, we are in release mode and feature freeze.\nIOW, get your head out of your ass."
    author: "logixoul"
  - subject: "Re: hrmf..."
    date: 2007-11-22
    body: "cute..."
    author: "turn_self_off"
  - subject: "Please add links to screenshots"
    date: 2007-11-21
    body: "I like to see some screenshots. :) \nI use my PC for works so I can not install release candidates etc. \n\nThank you for the screenshot links. \n\n\n\n"
    author: "Step"
  - subject: "Re: Please add links to screenshots"
    date: 2007-11-21
    body: "http://www.ossblog.it/tag/Kde"
    author: "me"
  - subject: "Re: Please add links to screenshots"
    date: 2007-11-23
    body: "I assume (HOPE!) that little Down Arrow in this pic:\n\nhttp://www.ossblog.it/galleria/big/kde4rc1/6\n\non the \"Turn Off Computer\" button means that the KDE Log Out dialog will support Suspend/Hibernate?\n\nM.\n"
    author: "Xanadu"
  - subject: "Can't get composite to work with kwin"
    date: 2007-11-21
    body: "\nHi everyone!\nFirst of all congratulation for this release candidate. The future looks VERY promising... (and the present too!). I've been installing betas for a while and I've never been able to get kwin composite effects to work with my graphics card (geforce GO5600 with nvidia binary drivers, running under OpenSUSE 10.3). Composite is enabled in Xorg and compiz fusion work for me but kwin... no luck! It keeps on saying \n\nkwin(17309) KWin::Extensions::init: Extensions: shape: 0x \"11\"  composite: 0x\n \"4\"  render: 0x \"a\"  fixes: 0x \"40\"\nkwin(17309) KWin::Workspace::setupCompositing: No compositing\n\nAm I missing something?\n\n\nThanks in advance!\n\n"
    author: "Federico Gherardini"
  - subject: "Re: Can't get composite to work with kwin"
    date: 2007-11-21
    body: "Did you have the Xorg header files installed at compile time? Without those headers, compositing is disabled."
    author: "chris.w"
  - subject: "Re: Can't get composite to work with kwin"
    date: 2007-11-21
    body: "I'm using the OpenSUSE 10.3 packages therefore I'm not compiling kde4 myself. Anyway after much googling around I found the following message\n\nhttp://lists4.opensuse.org/opensuse-kde/2007-11/msg00003.html\n\napparently OpenGL compositing doesn't work in the OpenSUSE packages but xrender compositing does??\n\n"
    author: "Federico Gherardini"
  - subject: "Re: Can't get composite to work with kwin"
    date: 2007-11-21
    body: "I have the same problem with the gutsy packages.\nkwin config shows that the effects are enabled, but they are not in fact.\n\nMy graphics hardware is the following:\nIntel Corporation 82915G/GV/910GL Integrated Graphics Controller (rev 04)\n"
    author: "yves"
  - subject: "Re: Can't get composite to work with kwin"
    date: 2007-11-23
    body: "It works and the effects are great! I couldn't get it working either because I had XGL (which KDE4 seems to be allergic to) enabled for Compiz Fusion. What I did was enable the composite extension in the xorg.conf file and used xorg as the default display manager. You can change that in YaST.\n\nI am really impressed by RC1, it improved a lot over the last beta and seems to be quite stable. I had a few problems with the openSuse packages and I am still missing some plasmoids and a  background picture but apart from that I can see that KDE4 is going to kick some big time asses!"
    author: "Bobby"
  - subject: "nice (but unusable for me)"
    date: 2007-11-21
    body: "The oxygen gui style, window decoration and icons have become beautiful. There are lots of really great features and applications. But while I had hoped to switch to KDE4 as soon as possible, some not so great decisions make it impossible for me. Some of the broken features will probably get fixed soon, but some are apparently broken by design and even listed as \"improvements\".\n\nOne missing feature that I just can't work without: panel autohiding. This black bar eats a lot of my desktop space and makes \"maximized\" windows a lot smaller than I'm used to. Also, I haven't yet found a way to set more than 4 virtual desktops, but I'm sure that this will get addressed soon. (Haven't yet bothered to edit the config file.) I'm aware that the whole desktop workspace is still work in progress. No need to panic. :-)\n\nI can't stand the kde menu (forgot its name). I find it hard to believe that it has survived any usability testing, but I accept it as a fact that there was some. The hard coded size is a step backwards and reminds me of (old?) MS Windows file dialogs. It's much too easy to switch tabs by accidentally leaving the file/category area. It's easy to miss the backwards button on the left (now that it's no longer left aligned). But I hear that there's a classical menu in the works, so that's not a big problem. And most of the time I start applications with konsole, anyway.\n\nGwenview has become completely unusable for me, which is a big showstopper, as I use that very often. Not only does it pop up completely unwanted menus when hovering over a thumbnail (I'll patch that away in my copy), it has dropped the possibility to adjust gamma/brightness/contrast. Yes, I could keep using gwenview from KDE3, but having two Qt-libs in memory isn't exactly a desirable goal. Need to check out pixie ...\n\nSomeone will probably jump on me and criticize me for ... erm ... criticizing,\nand that's OK.  :-)"
    author: "Melchior FRANZ"
  - subject: "Re: nice (but unusable for me)"
    date: 2007-11-21
    body: "I trust that the panel isn't finished yet and can be moved, resized and hidden when it's done. At the moment, the system tray doesn't even work for me, i.e. it seems to be there, but doesn't show any icons :-("
    author: "chris.w"
  - subject: "Re: nice (but unusable for me)"
    date: 2007-11-21
    body: "> Gwenview has become completely unusable for me\n\nMe too.  If you have a lot of images in a directory and use the detailed view, it hangs for ages because it no longer looks at the file information to get the date, it uses the EXIF information instead, meaning it has to read each and every image in the directory before it can display the list of images.  What's worse is that there's no option to switch this off.\n\nThere were also regressions with keyboard shortcuts as well.  Can't remember exactly what they were because I stopped using it because it got that bad compared with previous versions.\n"
    author: "Jim"
  - subject: "Re: nice (but unusable for me)"
    date: 2007-11-21
    body: "Windows' Explorer only creates thumbnails for visible icons. Therefore, it was faster than Konqueror/Gwenview since ages. Indeed, I hoped the thumbnail preview kparts will include this in KDE4 series as well. It doesn't seem so---"
    author: "Sebastian"
  - subject: "KMenu/Kickoff"
    date: 2007-11-21
    body: "It doesn't matter that damn near everyone who tries Kickoff complains about it.  It doesn't matter that users don't like it.  It doesn't matter that it slows people down.  It doesn't matter that people have been extremely vocal about this week after week.\n\nA usability shows it is superior, and that is vastly more important than actual feedback from your users.\n\nDon't ask me how something some slow and poorly designed is superior, but someone said so, and they were like official and stuff.\n\nYou just can't argue with that.\n\n(However, the good news is that we'll have options like Raptor and Lancelot, which both look great!)"
    author: "T. J. Brumfield"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "\"It doesn't matter that damn near everyone who tries Kickoff complains about it. It doesn't matter that users don't like it. It doesn't matter that it slows people down. It doesn't matter that people have been extremely vocal about this week after week.\"\n\nFacts:\n\n-> Raptor is not done\n-> Lancelot is not done\n-> There is no K-Menu for plasma\n-> One developer ported Kickoff to plasma\n\nSo there is only *one* choice atm. So scream, shout, cry, troll at much as you want. KDE 4.0 will ship with Kickoff as the default menu unless some other developer creates an releaseable alternative *really* fast."
    author: "cloose"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "When it was first announced that Kickoff would be rewritten for KDE, people were very vocal in opposition, and people asked for a simple Kmenu option at the very least.  I'm certainly not opposed to the new Kickoff existing for those who want to use it.\n\nHowever, repeatedly all the detractors were told that Kickoff is just better and that our criticism wasn't valid.  Why?\n\nNot sure.\n\nApparently a menu that buries items and forces you to click five times and wait boosts productivity and you're can't argue with that."
    author: "T. J. Brumfield"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "Then probably vote for http://bugs.kde.org/show_bug.cgi?id=150883 ?\nand maybe even try to follow the step-by-step intro + provide feedback?\n\nReally, I am all with you on that matter but I just fail to see how complaints on the dot or whereever else could change anything. Don't waste your energy here, but participate direct at the place that is designed for it and where users and/or developers are able to fetch details, opinions, etc. related to one special case at one central place rather then seeking through the internet, the articles and the boards. Really, there is a reason why such a bugzilla ticket-system is such an essential tool.\n"
    author: "Sebastian Sauer"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "The original standard KMenu is less usable than the Kickoff menu for one simple reason: search.  Most users know what they want to get to, but it can be hard to get to it through any menu system that has more than just a few entries.  The things I've seen on Raptor make it look to me like it will be totally unusable by anyone who wants more than eye-candy, like actually being able to quickly start the application that you want to, which is the whole purpose of a menu in the first place.  Lancelot seems to be promising, but is obviously no where near usable at the current time.  Kickoff needs some work on when to click and when a mouse hover should do, but at the moment it is far superior to anything else that exists."
    author: "Mike E."
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "The interesting part in the whole discusion is, that a few people tend to argue that there own taste is better cause the others are more worse. What I miss a bit is to accept that others may have another taste and there is no way to convince them ;)\nI guess nobody ever sayed, that this or that solution will be the only one. In fact I guess we will have soon so many alternates, that I'll spend again a few hours trying them all like I did with the KDE3-themes. Well, kickoff is the vanilla-default, and? With one (or n) clicks that can be changed rather fast and probably your favorite distributor may even pick up something total different to be more unique, brand the desktop according to there philosophy or just cause they don't agree with the default, and? Well, all in all, what really counts is, that the base to allow to integrate easy additional alternates is there. Plasma does enable this.\n"
    author: "Sebastian Sauer"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "How does search make a menu better for everyday use?\n\nIf I don't know where an app is, or what the name is, then search might be nice.  However, KDE is very good about just dropping apps in nice logical places in the menu.  The old KMenu was great for quickly navigating to the app I wanted with a few hovers.\n\nSearch seems nice at first for a new user, but in the long run, even using search is slower that a quick hover.\n\nKickoff is so bad, that after trying it out, I just found myself hitting Alt+F2 all the time, and the habit stuck now.  My wife was trying different binary distros for her laptop (I'm a Gentoo guy) and openSUSE and Sabayon defaulted to Kickoff.  kdemod on Arch uses it as well, and she hates the thing.\n\nIt isn't as bad the the Vista menu, but is pretty close."
    author: "T. J. Brumfield"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "\"Search seems nice at first for a new user, but in the long run, even using search is slower that a quick hover\"\n\nDisagree with that as you usually only have to enter the first letters of any application to get results and in my case when using it as an application launcher the results are in most cases what I've looking for. And entering two or three letters and hitting enter is usually done quicker than (depending how many submenus any given menu has)  Plus, there are hard to measure delays during usage of menues including visual scanning of each layer for the next node/leaf to click.\n\nIt also really depends on the speed of the underlaying indexing tool. For instance, Spotlight is obscenely fast with coming up with results while Beagle in combination with Kickoff is far slower in that regard. I hope we'll see an improvement when Kickoff uses Strigi as the indexing backend."
    author: "Erunno"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "Non geeks users don't use keyboard to search apps, so searching tool is useless...\n\nI see how people use vista, they never search apps via menu because most of the time they don't remember app name..."
    author: "Cedric"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-22
    body: "Most \"normal\" users don't have a clue what applications are named, so search doesn't help them. Most ordinary (non-geek) users I know remember apps by the \"pretty picture\" next to it (the icon) and will browse through menus looking for the picture (never the name or description, *just* the icon image). \nSearch is, for this (I suspect vast) group of users, completely useless.\n"
    author: "JJ"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-22
    body: "> Most \"normal\" users don't have a clue what applications are named, so search doesn't help them.\n\nYou cannot only search for applications names but categories/keywords/descriptions."
    author: "Anonymous"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-22
    body: "See, this is what annoys me about many Kickoff discussions: People have only used *any* Kickoff implementation for a very short time or not at all but still feel like armchair quarterbacking about the advantages and disadvantages of it. Otherwise I can't imagine where those misconcenptions about Kickoff's functionality are coming from.\n\nPeople are likely to get better results when they type \"burn cd\" and get K3B as a result as with icons it's always dependent on how meaningful any give icon is and if the user interprets it in the same way as the designer. This is less likely to happen in task-based description like the aforementioned burning of cds."
    author: "Erunno"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-22
    body: "Kickoff is very usable and especially easy for KDE newcomers to use. It's almost foolproof. I can't understand the most of the discussions around Kickoff myself. I personally am quite happy with Kickoff, all I would like is that it would be made more configurable like KBFX. Kickoff only needs one or two screws here and there to be tighten and a few eye-candies."
    author: "Bobby"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-22
    body: "Another fact: if Raptor or Lancelot would be \"done\"/tested by users there would be also people complaining about it."
    author: "Anonymous"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-22
    body: "Thank you very much. Lancelot looks very nice but like you said, it's not done. Raptor's concept sounds good but I haven't had the possibility of testing it so I can't make a comparison and I can do just about anything with Kickoff which is presently the only usable Start Menu for KDE4."
    author: "Bobby"
  - subject: "Re: KMenu/Kickoff"
    date: 2007-11-21
    body: "There's probably also users like me who have been using Kickoff since openSUSE 10.2 released it and are quite content with it and don't feel the need to praise it on each and every occasion that presents itself. \n\nI still fail to see how the application browser is a regression for anyone *but* the power users who are used to moving their mouse very precisely (which is required for tree-based menues as slipping often means that either the node disappears or another gets opened). Kickoff on the other hand has a very large horizontal click area and an acceptable height, the buck button (at least when left aligned) is almost impossible to miss and, in my oppinion, the decision to only show a node with its leaves/subtrees at any given moment makes it far easier to navigate as too many entries can make a tree based menu look gigantic (I've been burned by that too often in Windows where application don't even go to sub-categories) and less overwhelming (yes, it obfuscates the fact that there are dozens of applications in your menu by only showing small subsets). Plus, introducing scrolling allows you to have additional information (like descriptions) for each entry which in a tree based menu could lead too aforementioned escalation of height.\n\nAnd judging by some complains here it sounds like people use the application browser all day long (why?)."
    author: "Erunno"
  - subject: "Re: nice (but unusable for me)"
    date: 2007-11-22
    body: "Uhm..OK I'll bite. Press Alt+F2 and then in krunner type 'kcmshell4 desktop' then enter. It lets you set a number of virtual desktops as in KDE3. Then start the pager-applet from the plasma widget browser and your ready to switch desktop like crazy!\nkcmshell4 --list gives you a list of kcontrol modules. Some of them work, some of them don't. "
    author: "uuhmyeahrightnstuff"
  - subject: "Re: nice (but unusable for me)"
    date: 2007-11-22
    body: "Sounds like an excellent procedure for a *release* candidate!"
    author: "Melchior FRANZ"
  - subject: "That's wonderfull"
    date: 2007-11-21
    body: "Congratulations to the KDE team for their great work. My respect. :)"
    author: "Prom"
  - subject: "A suggestion [semi-OT]"
    date: 2007-11-21
    body: "Include apps (either extragears or playgrounds) that have not kde3 versi\u00f3n into \"test distros\". I'm referring specially to kaider."
    author: "marce"
  - subject: "Dual Head"
    date: 2007-11-21
    body: "Can anyone tell me how it performs on a Xinerama or TwinView setup? Thanks!"
    author: "bsander"
  - subject: "Re: Dual Head"
    date: 2007-11-22
    body: "It works really well, I'm using it daily."
    author: "sebas"
  - subject: "Re: Dual Head"
    date: 2007-11-22
    body: "sadly not that well here. the panel is only semi-visible and my most-left monitor starts rendering the background somewhere mid-screen. the rest is just white. no sure if it's related to that, but i can't make the rc last longer than 2 minutes before crashing completely."
    author: "muesli"
  - subject: "kwin4 and Compiz"
    date: 2007-11-21
    body: "Some screenshots I see here ( http://vizzzion.org/?blogentry=742 ) have compiz-like effects but are powered by kwin. Is Kwin4 intended to replace compiz for KDE or can you still use them both?"
    author: "Skeith"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "KWin4 is a game. I guess you mean KWin 4. :-P"
    author: "chris.w"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "That's confused me a time or two, as well.  Maybe a rename is in order?"
    author: "Louis"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "Totally agreed."
    author: "chris.w"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "What, like... KWin Composite? ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-22
    body: "I was kinda suggesting a rename for KWin4.  I just don't get the \"win\" part in that name.  It subliminally tells me that it has something to do with windows or windowing, when, in reality, it's a Connect 4 game.  Would Konnect4 push legal boundaries?  Or how about something new, sans \"k\"?"
    author: "Louis"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-22
    body: "I think Konnect4 was not chosen for that reason. \n\nHow about KJoin4?"
    author: "Paul Eggleton"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "You can enable compiz, but without swapping out the window manager itself, it isn't really all that nifty.  You can use compiz and KDE 4 at the same time, but the new Kwin has built in composite effects, and largely is meant to replace compiz and compiz-fusion."
    author: "T. J. Brumfield"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "For some reason I thought that Kwin didn't provide its own effects, but had just been made compatible with compiz (as in you didn't have to switch it when using Compiz). Thanks for clearing that up."
    author: "Skeith"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "Kwin in KDE 3.5.x did not provide effects.  Kwin in KDE 4.x does."
    author: "T. J. Brumfield"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "Well, actually KWin in KDE 3.5 has not been \"made compatible\" with Compiz Fusion, but rather the other way round: There is a Compiz plugin that can emulate the look & feel of KWin, so you can control the decoration settings via the KDE control center as if you were actually using KWin. I guess that's what you meant."
    author: "chris.w"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-21
    body: "Yes, Kwin is the default window manager and now has compiz-style effects.  It is probably still possible to use compiz though."
    author: "Leo S"
  - subject: "Re: kwin4 and Compiz"
    date: 2007-11-22
    body: "You can't use both KWin and Compiz at the same time. Both want to be the X Server's Window Manager (+ Composite Manager). Compiz does have a plugin that will allow you to use KWin's window decorations, but that ONLY emulates the borders for Compiz, without actually using KWin's (far superior IMO) window management code.\n\nCompiz (last I checked) can't work at all if your system isn't running with Composite extension enabled with AIGLX support (or Xgl, which seems to have lost the appeal it had), KWin on the other hand will gracefully fall back depending on what your system supports. If your system has both, it'll use both, if it has Composite but not AIGLX, it'll use XRender for the compositing. If it doesn't even have Composite, it'll simply be a window manager like it is in KDE 3.x. I think for a good while it'll still be a wise choice for a large amount of users to run without XRendered's Composite/GLed' Composite, although Fedora seems to be wanting to try to flesh out the last of the Composite bugs which I'm sure will help out a great deal.\n\nAh the fun of rambling when I'm alternating which eye I'm attempting to keep open!"
    author: "Sutoka"
  - subject: "Mac OS style menu bar?"
    date: 2007-11-21
    body: "Will KDE 4 support the Mac OS style menu bar at the top of the screen? I liked that feature very much in KDE 3, but couldn't find it in KDE 4."
    author: "furanku"
  - subject: "Re: Mac OS style menu bar?"
    date: 2007-11-21
    body: "I second this! I use this feature in KDE all the time and could not live without it. I hope this feature will be in KDE4 soon!"
    author: "kiwiki4e"
  - subject: "Re: Mac OS style menu bar?"
    date: 2007-11-22
    body: "I third this. The integration of all KDE apps to use this bar was beautiful, one of my most recent discoveries in KDE. The only downside to it, is you can't manipulate it - In my two-screen scenario, it's stuck using the entire top of the screen (As in, across both). It'd be cooler if it was more like a kicker applet, and be managed like the other bars.\n\nI haven't tried the RC out yet, so I wouldn't know if this is a redundant post or not =/"
    author: "kakalto"
  - subject: "Re: Mac OS style menu bar?"
    date: 2007-12-06
    body: "I had the same problem. In kde 3 you can change the file ~/kde/share/config/kickerrc and set the value of XineramaScreen to your preferred screen. The problem is, that there must be a bug that always resets the value to 0 (both screens). But if you remove the write-permissions it works. The only problem now is that you have to reset this write-permissions as root every time you want to change something on the menubar. But it works this way.\n\nHope this helps."
    author: "dreadhead"
  - subject: "Re: Mac OS style menu bar?"
    date: 2007-12-06
    body: "oh sorry... I meant ~/kde/share/config/kicker_menubarpanelrc..."
    author: "dreadhead"
  - subject: "Re: Mac OS style menu bar?"
    date: 2007-11-22
    body: "I'm not sure what the status of it is in 4.0, there are chances it got broken during the Qt4 porting. But I use it as well in KDE3 and will definitely have a look into it as soon as possible (which probably means somewhen post-4.0).\n"
    author: "Lubos Lunak"
  - subject: "Re: Mac OS style menu bar?"
    date: 2007-11-22
    body: "Much appreciated, Lubos - as was your recent blog :)"
    author: "Anon"
  - subject: "Re: Mac OS style menu bar?"
    date: 2007-11-22
    body: "Thank you very much!"
    author: "furanku"
  - subject: "Re: Mac OS style menu bar?"
    date: 2007-11-23
    body: "very cool! Thank you Lubos."
    author: "cloose"
  - subject: "bugs.kde.org overloaded?"
    date: 2007-11-21
    body: "It seems like every time I try to search bugs on bugs.kde.org, I wait and wait and wait until I get a timeout. For the very same reason, I can't file any bug reports, although I've already found plenty of problems to report. Seems like the server can't take the sudden onslaught of testers ..."
    author: "chris.w"
  - subject: "Re: bugs.kde.org overloaded?"
    date: 2007-11-21
    body: "Sorry for the double post :-("
    author: "chris.w"
  - subject: "Less Dead/White/Empty Space"
    date: 2007-11-21
    body: "Judging from the new screenshots, it looks like much of the dead/empty/white space has been trimmed.  It isn't nearly as tight as I'd like, but it looks much better.  I'm also assuming eventually there will be plenty of themes for me to choose from on kde-look later on regardless.\n\nI think it is starting to look really good.\n\nPerhaps it is time for me to stop just trying out live cds everyone once in a while, and actually install KDE 4 for myself.  I think this weekend, I will take the plunge!"
    author: "T. J. Brumfield"
  - subject: "RC quality"
    date: 2007-11-21
    body: "I'm not trolling, it isn't what I want to do.\n\nHowever, don't you think some basic stuff should be there for been able of calling it Release Candidate? And yes, I believe most KDE 4.0 is already on that state. \n\nBut come'on, plasma support a lot of nifty effects, plasmoids, dashboard, etc. and I'm not able to change my wallpaper... WTF\n\nAnd, could some benevolent soul could tell me where is the config file where I can change the background? Thanks in advance.\n\nOh, I've seen that now Plasmoids support some kind of dialog around them, which aloud me to move them, configure them, rotate them or remove them. I like the idea, but, it's one hundred times more intuitive to move plasmoids dragging them from the border than that, whit the new dialog I had to think how I was supposed to move them!\n\nThis new dialog is also very ugly, but it isn't important, I'm sure it would change.\n\nI know that you know that Plasma panel needs work still, so I won't complain about it, but it isn't RC quality.\n\nCan't you hide all those pop-ups in System Settings? They're annoying.\n\nAside from those, KDE 4.0 RC 1 it's amazing. I love Dolphin, I wrote this from Konqueror, you can flame me, but I like kickoff, ksysguard is ten times better than the KDE 3.5.x version, juk surprised me! Amarok is nice, it has some bugs here and there, and the plasma context is still unusable, but it's shaping nicely, I love the new playlist, Kontact suite, is better too, and overall it's an impressive release. \n\nPS: Ok, I did wrote this in Konqueror, but I couldn't post it! Filling a bug report..."
    author: "Luis"
  - subject: "Re: RC quality"
    date: 2007-11-22
    body: "Good catch on posting bug... Seems like this mode is dropping chars.. Ugh.. Looking into it (and this post is part of it -- want a wireshark log for 3.x)"
    author: "SadEagle"
  - subject: "Re: RC quality"
    date: 2007-11-22
    body: "Can you please the benevolent soul that tells me how to change the background image? XD"
    author: "Luis"
  - subject: "Re: RC quality"
    date: 2007-11-22
    body: "Let's see if this fixes it. Annoying QCString/QByteArray differences"
    author: "SadEagle"
  - subject: "Re: RC quality"
    date: 2007-11-22
    body: "OK, fix committed. Thanks for the heads up --- though I don't see a corresponding bugzilla report."
    author: "SadEagle"
  - subject: "Re: RC quality"
    date: 2007-11-22
    body: "No problem. And, oops, I'm really sorry I wrote that when I was supposed to be going to the university, so I was going to make it tonight. Could you believe that I actually think that this may happen XD?\n\nAnyway, cheers to you for fixing it."
    author: "Luis"
  - subject: "Re: RC quality"
    date: 2007-11-22
    body: "No problem. And, oops, I'm really sorry I wrote that when I was supposed to be going to the university, so I was going to make it tonight. Could you believe that I actually thought that this may happen XD?\n\nAnyway, cheers to you for fixing it."
    author: "Luis"
  - subject: "First impression"
    date: 2007-11-21
    body: "I ignored all beta releases. KDE 4 RC1 is my first ever glimpse at KDE 4. I'm using the Kubuntu packages. What I like:\n* Sound effects. Not too much. Very professional\n* Design of windows, control elements etc\n* Plasma applets. Even rotatable. Can image this will be really great,\nonce more applets become available\n* Icons scaling dynamically when resizing the panes in Dolphin\n* Didnt experience crashes. Seems already very stable.\n* K Menu doesnt look as crowded and filled with icons as it used to be. Only necessary things appear.\n\nWhat I didnt like (still work-in-progress, I know):\n* Everything feels a bit more \"sluggish\" than in KDE 3. When rotating plasma applets this becomes especially evident (though this feature isnt available in KDE 3). But also other, simpler things, like scrolling, clicking menus, etc. take a little bit more time. Not so much that I would call it unusable, but just enough to keep me from using it regularly right now.\n* When I start Okular there is a strange window resize problem.\n* New K menu behaves a bit strange. Especially, I dont like that the menu switches between different completely different things just by \"touching\" them with the mouse. This happened to me accidentally quite often. Very confusing.\n* Taskbar looks strange. Window titles are wrapped around unnecessarily and over 3 lines. Task icons appear in the center for no reason at all, but perhaps this is the new KDE 4 way and I still need to get used to it.\n\n"
    author: "Michael"
  - subject: "Re: First impression"
    date: 2007-11-22
    body: "According to other posts, the Kubuntu packages don't have the new taskbar ( pictured here http://vizzzion.org/images/kde-4.0-post-rc1/desktop.jpg ). Which may be the cause of your strange looking taskbar."
    author: "Skeith"
  - subject: "Remote X via SunRay not impressive"
    date: 2007-11-22
    body: "I'm using my KDE desktop via a openvpn connection (over 10Mbit/sec ADSL) with a SunRay2 thin client and compared to KDE 3.5.x the new KDE 4 RC1 is not too impressive. It seems to redraw my windows and desktop somewhat slower and generally seems more sluggish. Is there any known obvious reason for this?\n"
    author: "J"
  - subject: "Re: Remote X via SunRay not impressive"
    date: 2007-11-22
    body: "Try using freenx it's really fast and nice."
    author: "Petteri"
  - subject: "Re: Remote X via SunRay not impressive"
    date: 2007-11-23
    body: "That's a bit hard when my workplace supplies me with the <a href=\"http://www.sun.com/sunray/sunray2/\">SunRay2</a> and it just connects to the department SunRay server (running Linux).\nI just noticed that when the admin did a test install of KDE 4RC1 that I could pick as my login session, that it was a fair bit slower and more annoying to use compared to my regular KDE 3.5.x session.\nI don't control the software on the server that I log on to and the SunRay thin client is entirely software-less."
    author: "J..."
  - subject: "Re: Remote X via SunRay not impressive"
    date: 2007-12-05
    body: "I was under the impression that Sunray 2 (built in vpn client) doesnot work with openvpn, since SR2 is based on IPSEC and openvpn is an SSL implementation.\n\nCan you share any implementation detail with me ?\n\n\nThanks\n"
    author: "Arif Khan"
  - subject: "Re: Remote X via SunRay not impressive"
    date: 2007-12-05
    body: "I was under the impression that sunray2 (builtin vpn client) doesnot work with openvpn, since sunray 2 is based on IPSEC and openvpn is an SSL implementaion.\n\nCan you share any details of your implementation ?"
    author: "Arif Khan"
  - subject: "Great But Slow"
    date: 2007-11-22
    body: "Hi,\nI just wanted to tell that KDE 4 is looking great but in fact it's too slow... I have tested it on a AMD64 with kubuntu 7.10 and a 256MB video card... I hope it will be fixed"
    author: "Matttias"
  - subject: "kppp"
    date: 2007-11-23
    body: "Is kppp working yet?  This is a showstopper for me, as I have to rely on an external USB modem in my area. It would also be needed for people living in Third World countries."
    author: "The Vcar"
  - subject: "Re: kppp"
    date: 2007-11-23
    body: "Definitely not a matter of Third World countries.\n\nHere in Austria modem usage is on a new rise due to \"mobile broadband\" becoming wildly popular, i.e. dial-in services based on 3G/UMTS Modems."
    author: "Kevin Krammer"
  - subject: "Re: kppp"
    date: 2007-11-23
    body: "I use kppp as well, but in KDE svn it's still broken (it seems). You might want to check out wvdial instead for the time being."
    author: "Coolvibe"
  - subject: "Re: kppp"
    date: 2007-11-25
    body: "I spent some time today fixing KDE4 KPPP issues.\n\nIt now correctly works with the KDE3 KPPP setup, i.e. kppprc from my KDE3 session.\n"
    author: "Kevin Krammer"
  - subject: "Re: kppp"
    date: 2007-11-27
    body: "That is great!  Many thanks."
    author: "The Vicar"
  - subject: "Slackware packages?"
    date: 2007-11-24
    body: "I would really like to test this release, but there aren't any Slackware packages on it, and my box is too slow to build KDE4. If only you could provide a set of Slackbuilds, that would be really nice.\n\nMeanwhile, the RC1 Info page said:\n\n\"For those interested in getting packages to test and contribute, several distributions notified us that they will have KDE 4.0-rc1 packages available at or soon after the release. The complete and current list can be found on the KDE 4.0-rc1 Info Page, where you can also find links to the source code, information about compiling, security and other issues. \"\n\nBut still there aren't any other packages than OpenSUSE... how could we test the release if there aren't any packages? We are end users, after all...\n"
    author: "Eduardo"
  - subject: "To big and messy"
    date: 2007-11-28
    body: "Have waited for KDE4 some time now and tried both Beta4 and RC1. Man I am disappointed. It looks like you have tried to copy MS and build the biggest chunk of software with 90% functionality few people ever need or think about. Also the user interface is too cluttered with big icons and applets and window decorations taking to much space, it just look messy. Even settings and icons are missing in what is called an RC-1??? I am surprised that this much development is still going on just 1-2 weeks before the final release. But most of all, I get the MS feeling when I have to download half the internet just to get a basic UI up and running.\nThis made me a bit worried because I like to use KDevelop both for work and private development. I have now switched to Eclipse/CDT and are very impressed with that IDE. So when I feel that KDE3 starts to become to outdated I guess I have no choice but to switch to Gnome or similar... :("
    author: "H\u00e5vard"
  - subject: "Did you mean \"Kalamity\"? ;-)"
    date: 2007-11-30
    body: "I think you meant \"Kalamity\"...\ni really think so...\n\nI can't wait!"
    author: "Jason"
---
The KDE Community is happy to announce the immediate availability of <a
href="http://www.kde.org/announcements/announce-4.0-rc1.php">the
first release candidate for KDE 4.0</a>.  This release candidate marks that 
the majority of the components of KDE 4.0 are now approaching release quality.







<!--break-->
<p>While the final bits of <a href="http://plasma.kde.org">Plasma</a>, the brand new desktop shell and panel in KDE 4, are falling into place, the KDE
community decided to publish the first release candidate for the KDE 4.0 
Desktop.  Release Candidate 1 is the first preview of KDE 4.0 which is 
suitable for general use and discovering the improvements that have taken 
place over the entire KDE code base. The KDE Development Platform, the basis for developing KDE applications, is frozen and is now of release quality. The source code for the KDE Development Platform can be found under the &quot;stable/3.96&quot; subdirectory, on KDE's FTP server and mirrors.</p>

<p>Building on this, the majority of applications included in KDE 4.0 are now 
usable for day to day use. The KDE Release Team has recently underlined this by <a href="http://lists.kde.org/?l=kde-release-team&m=119521408516846&w=2">calling 
on the community</a> to participate in reporting bugs during the time 
remaining before the release of KDE 4.0 in December.</p>

<p>Meanwhile, preparations for the KDE 4.0 release events are ongoing, with the main event taking place January 2008 in Mountain View, California USA. Make sure you don't miss it!</p>


