---
title: "Running KDE 3.5 on the Nokia N770 and N800 Tablets"
date:    2007-09-19
authors:
  - "tunrau"
slug:    running-kde-35-nokia-n770-and-n800-tablets
comments:
  - subject: "Resources"
    date: 2007-09-18
    body: "That's for those who think that KDE needs much memory, and prefer Xfce and Fluxbox or special platforms (e.g. Opie) on embedded systems.\n\nHopefully, the sys reqs will decrease again with KDE 4."
    author: "Stefan"
  - subject: "and the question is..."
    date: 2007-09-18
    body: "..who wants to run kde on it. it's not meant to run kde, since it's the bottleneck is the cpu and ram."
    author: "nobody"
  - subject: "Re: and the question is..."
    date: 2007-09-18
    body: "> ..who wants to run kde on it. \n\napparently the people who made it, for starters. and then the people who have installed it. this was all done, afaik, by kde users not \"THE\" kde project. which makes this even more cool imho. =)\n\n> since it's the bottleneck is the cpu and ram.\n\nthis is what is called \"a misconception\"."
    author: "Aaron J. Seigo"
  - subject: "Re: and the question is..."
    date: 2007-09-19
    body: "Well you assume that KDE itself needs a lot of RAM and CPU, which is simply not true. Furthermore you can switch of any \"blinking animation effects\" of KDE which again reduces CPU load.\n\nI mysef was able running a usual KDE on a Pentium 200 Mhz with 256 MB RAM with little trouble. Sure video playback is not possible and open a larger graphic will take some time but you definitely can work with it as long as you don't think about multimedia editing.\n"
    author: "Arnomane"
  - subject: "Re: and the question is..."
    date: 2007-09-19
    body: "Ram can be a problem, below 128 mb gets less funny... You can't really multitask anymre, and must be careful with what you start, to ensure you don't hit swap (too much). Cuz swap really hurts linux performance (yes, also a kernel issue).\n\nI wish those kernel guyz listened more to userspace (inotify anyone?)"
    author: "jospoortvliet"
  - subject: "Re: and the question is..."
    date: 2007-09-20
    body: "you can change behavior with /proc/sys/vm/swappiness"
    author: "G\u00fcnter"
  - subject: "Please QT on the Nokias!!!"
    date: 2007-09-18
    body: "What should be ported is Opie, I have a Nokia N800 despite I'm a KDE Desktop user and is an awesome product. I had previously a Sharp Zaurus and I liked much more the user interface than the (Nokia N___). Much faster, much useful, QT based, with possibility of screen rotating, tons of apps, etc.\n\nIf someone finally decides to port Opie or Qtpia to Nokia I will be a 100% happy user :) I would like to help porting but I need someone who helps me (I do not have any previous background on such kind of porting tasks)."
    author: "biquillo"
  - subject: "Re: Please QT on the Nokias!!!"
    date: 2007-09-19
    body: "What? You haven't heard yet?\n\n\"Qtopia Phone Edition is completely GPL\"\n\nhttp://www.linuxdevices.com/news/NS5429713730.html\nhttp://trolltech.com/company/newsroom/announcements/press.2007-09-17.9260755578\n\nSo that's Nokia 770 and 800, Sharp Zaurus, and Qt Greenphone. Nice, eh?"
    author: "Jucato"
  - subject: "Nokia should hire KDE developers"
    date: 2007-09-18
    body: "Of course KDE will run on the Nokia 770 and 800 tablets. But to make the integration even more seemless and in a timely manner (ie. right when the tablet is released to consumers), Nokia should actually hire some KDE developers for this task."
    author: "AC"
  - subject: "KDE/Qt apps and the Nokia N800: Troublesome"
    date: 2007-09-19
    body: "The N800 is really a rather well-done device and software platform, and I have to congratulate Nokia for the progress they've made over the last two years with regard to learning how open source works and engaging with the community ever more and ever better. I've had mine since June, and I enjoy using it quite a bit.\n\nUnfortunately, as a KDE user, the one flaw in its ecosystem is the lack of KDE/Qt-based software for its original operating environment, the Maemo distribution with the Hildon app framework and desktop. The primary cause of this is the Hildon Input Method framework, which was closed source until recently, and requires toolkit-level integration, i.e. Hildon uses a modified version of GTK+ that enables mechanics like having the on-screen keyboard come up when tapping a text input field, and sending keyboard events to the application. This kind of integration isn't available for Qt, so KDE/Qt apps that require a working on-screen keyboard to function essentially can't be used in Hildon.\n\nThe Hildon Input Method Framework has recently been open-sourced as Nokia is moving Hildon development into the Gnome project itself. I'm hoping that this will enable the community to extend Qt 3 and 4 with support for the IMF, but so far this hasn't happened (afaik).\n\nRunning a full KDE session on the device is a really cool hack, but the KDE GUI isn't really designed for the form factor (for one, it's not very suited to operation by finger), and KDE/X also lacks an on-screen keyboard that is as well-behaved as Hildon's, so it's not really the final solution.\n\nAccording to the LinuxDevices article on the recent open-sourcing of Trolltech's Qtopia Phone Edition (well, open-sourcing of the remaining components that weren't open until now) and the announcement of support for the Neo1973, the relevant source drop also contains device configuration files for the N800. While it's a cool proposition to run Qtopia on the N800 and I'm definitely going to try it out, the problem here, unless I misunderstand terribly, is that Qtopia doesn't run on an X server but uses the framebuffer directly, so Qtopia doesn't have access to the large variety of X11 apps - or even KDE apps."
    author: "Eike Hein"
  - subject: "Re: KDE/Qt apps and the Nokia N800: Troublesome"
    date: 2007-09-19
    body: "\"is that Qtopia doesn't run on an X server but uses the framebuffer directly, so Qtopia doesn't have access to the large variety of X11 apps - or even KDE apps.\"\n\nThat's right, it does not use an X server(it may be an option, I don't know). So you miss out on the X11 apps, but it's Qt4 based so most KDE 4 apps should be portable/runable. Just like on other non X11 Qt platforms, like OSX or Windows."
    author: "Morty"
  - subject: "Re: KDE/Qt apps and the Nokia N800: Troublesome"
    date: 2007-09-19
    body: "It's true, large chunks of today's kdelibs could probably be ported fairly easily, and once more people get their hands on shiny Neo1973s it probably will, too - but for now, it hasn't happened yet. So as far as the N800 is concerned, you're currently either screwed out of Qt (when using Hildon, due to the lack of input method integration) or out of anything but Qt (when using Qtopia)."
    author: "Eike Hein"
  - subject: "I have a N800"
    date: 2007-09-19
    body: "Its a very cool, linux-based machine. I got it just last weekend, so I'm still figuring out what it can be done.\n\nI don't think I'm gonna try running kde 3.5 on it, as kde 3.5 isn't really suited for a small device like that, the interface would be too cluttered for that a small screen (800x480 resolution is still pretty nice, but its not a desktop resolution).\n\nI might try kde4 when it comes, if someone gets it running on the thing."
    author: "TPC"
  - subject: "Good but useless"
    date: 2007-09-19
    body: "When I read about it I thought \"Great work! Great but useless\". IMHO current desktop environment is quite enough for N800 or Nokia770 users.  As example of KDE low-resources consumption is good but for practice using - useless. I ahve KDE on my work and home PCs but I don't need it on N800 :)"
    author: "Michael"
  - subject: "Re: Good but useless"
    date: 2007-09-19
    body: "Some people said that about telephone or cinema.\n\nJust think about the paths that this experiment may open.\n"
    author: "Git"
  - subject: "Re: Good but useless"
    date: 2007-09-19
    body: "But of course, for every telephone, there are a hundred of slightly interesting inventions that never really cut it. Not to discourage people who try out things like this, but many inventions are cul-de-sacs of progress. Probably more than the useful ones. :)"
    author: "ac"
---
At Ars Technica, my colleague Ryan Paul has <a href="http://arstechnica.com/journals/linux.ars/2007/09/11/running-kde-on-the-nokia-n770-and-n800">posted</a> about KDE 3.5.6 now being able to run on the Nokia 770 and 800 tablets. If you want to get it up and running, check out the <a href="http://www.internettablettalk.com/forums/showthread.php?t=5474">original forum post</a> by "penguinbait" over at Internet Table Talk which gives the complete steps required in greater detail. The main drawback for these systems compared to full computers is the total memory available (64MB or 128MB), fortunately KDE runs quite nicely on these low memory systems nonetheless.


<!--break-->
