---
title: "Blue-GNU Reviews Kontact: I Once Was Lost..."
date:    2007-10-03
authors:
  - "rjohnson"
slug:    blue-gnu-reviews-kontact-i-once-was-lost
comments:
  - subject: "I love Kontact!"
    date: 2007-10-03
    body: "I use Kontact every day at work, and it is great.  Now, I don't connect it to a IMAP server, just POP3, so I can't speak to how it does with that.  Features like redirect and custom templates save me tons of time.  The only thing I REALLY wish for is integration with the KDE tray calendar, but I think it's on the todo list."
    author: "Louis"
  - subject: "Re: I love Kontact!"
    date: 2007-10-03
    body: "KMail lacks some IMAP features in comparison to Thunderbird.\nOne example is the offline folders. In KMail you can only make the whole account disconnected or not. Thunderbird lets you choose that option for every folder.\nAlso the newest KMail (1.9.7) has a pretty high cpu usage here. I'm reading my mails from an IMAP account over an OpenVPN connection and everty time KMail checks for new mail the cpu usage goes up to 40-80%.\n"
    author: "Mike"
  - subject: "Re: I love Kontact!"
    date: 2007-10-03
    body: "I use Kmail 1.9.6 (the \"enterprise\" version that comes with gutsy), and I have no problems with IMAP at all. Now, I do have a intel core 2 duo, with 2.2ghz on each core, but I'm overall totally satisfied with Kontact and KMail (I also use IMAP over VPN, btw)."
    author: "martin"
  - subject: "Re: I love Kontact!"
    date: 2007-10-04
    body: "You just didn't hit these\u00b9 bugs yet...\n\n\n[1] http://tinyurl.com/3cngbx"
    author: "Carlo"
  - subject: "Re: I love Kontact!"
    date: 2007-10-04
    body: "I _guess_ what is common for all these is that KMail moves the local mails and then after a delay syncs with the IMAP server. The move is not done (yet?) on the server when the sync is done and the new location is empty."
    author: "K\u00e5re S\u00e4rs"
  - subject: "Kontact is aging"
    date: 2007-10-03
    body: "Kontact has lots of great features but also lot's of problems. kmail really need to play catchup woth other email clients. It should provide better html support. Pictures and html sigs comes to mind. It's understandable these things are delayed considering the huge effort that KDE4 is. I don want to troll but the delay in new features caused by the KDE4 effort doesn't come well timed. Lot's of distributions and companies are putting big effort (and money) into Gnome (lord knows why) and gnome is playing catch up with KDE 3.5.x rather nicely. I know KDE is better, but as gnome is default on all the big distro's people only come to appreciate KDE is they are willing to look any further."
    author: "cossidhon"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Why should KMail provide HTML support to the degree of a full featured word processor? If you want fancy documents via email write the document in a word processor and attach the resulting PDF (or if you're bold ODT) containing your document.\n\nI personally hate HTML-only emails. 95% are spam.\n\nWhat is much more important are digital signatures given the frequent \"email identity\" theft (I guess everyone once saw an email with his adress as sender which was not from him). Although it now works perfectly in my KMail it wasn't really easy to set up everything needed.\n"
    author: "Arnomane"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Because *you* don't like it doesn't mean KMail shouldn't have it. |:-(\n\nLikewise, a car company would decide that ABS and Airbags give a false sense of security. Therefore they refuse to implement it. Would you buy a car from that company, or move to another? Worth thinking about! ;)"
    author: "Diederik van der Boor"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Well KMail has HTML email support (reading/writing) since a long time but I think it is wasted time and a wrong path to turn it into a full featured word processor that happens to use HTML as document format and other peoples email inbox as storage.\n\nOT: Well I would buy a car with ABS and airbag. Not sure about ESP cause it is always on and not only in case of an emergency so this results in more potential errors and I had experienced some \"funny\" car computer bugs with ESP. I definitely don't want my car mirrors and my car seat to be adjusted by small electric motors. Not because I dislike comfort but out of the simple reason that these systems are causing more trouble (repairing) than comfort."
    author: "Arnomane"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "You obviously don't work with external clients. Everyone wants proper HTML support. On the enterprise world, image is everything. I do appreciate what you say, and I share it, however employers spend lots of money on brand image and won't listen to that sort of reason. So HTML signatures and a basic level of text formatting (font, size, colour, bold, italics, bullet points, paste image from clipboard, create links) are very necessary in real-world, enterprise usage. \n\nNo need for a full featured word processor here though. Outlook using Word under the hood is obviously overkill.\n\nDigital signatures are obviously needed, however not by most people. Nice to have the feature working.\n\n"
    author: "NabLa"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Sure I do not need to write my emails in HTML as I don't use email a lot in my job beside studies.\n\nBut look I cannot believe that  \"On the enterprise world, image is everything\". Isn't reliability important, too? I definitely am angry that many companies are basing a huge part of their business on email (especially customer relations) write me every day a lot of bright PR in a HTML email but never ever heard anything about digital signatures. Or they say: \"Oh the attached signature could confuse our clients that don't use it\". Spam with the senders email adress confuses more (I know Sender-ID but it does not protect from manipulation, hijacking and more).\n\nDigital signatures are not expensive compared to other measures for secure internet business and as email has currently severe problems with phishing and spam (and thus gains little trust) a killer email app needs to make encryption/signature support as easy as possible.\n"
    author: "Arnomane"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "> Isn't reliability important, too?\nFor the techie, it is. For the one that pays the salaries and has the \"great vision\" is not. Until lack of reliability catches up, of course, in that case the techies are to blame."
    author: "NabLa"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "P.S.: Just in case someone is interested how I got encryption/signatures working nicely in KMail:\n\nhttp://zerlinna.blogweb.de/archives/61-Get-GPG-Decryption-working-within-Kmail.html and the somewhat outdated http://kontact.kde.org/kmail/kmail-pgpmime-howto.php can also give some additional hints.\n\nI also installed kgpg for easy key management, signed my own PGP-key with CACert and uploaded it to a key server. So everyone that never did meet me in person still can trust my emails as long as he trusts CACert.\n"
    author: "Arnomane"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "My wishlist for mail. Preferrably a plugin for most/all popular email clients.\n\nEasy GPG support\n* A dead simple wizard of how to create a key-pair.\n* Automatic signing of messages that I don't have recipients key of.\n* Automatic attachment of my pubkey to people who don't encrypt mails to me.\n* Automatic encryption of messages to people who I have the key of.\n* Automatic adding of keys that are sent to me.\n* Automatic decryption/checking of mails with cached passphrase.\n* Big error messsage if and only if something is wrong. If everything is OK I don't need to know that. I'll just assume it is.\n* A link attached to each mail to me that isn't signed/encrypted detailing how to get and install the plugin.\n\nPGP/GPG is only useful if most people use it and as such it needs wider adoption. \nHow I wish I could program so that I could write this myself. \nOh well."
    author: "Oscar"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "\"* Automatic decryption/checking of mails with cached passphrase.\"\n\nThis is possible with using gpg-agent and pinentry-qt. Per default it has an inactivity timeout of some minutes until you have to reenter your key password. The timout is configurable.\n\n\"* Big error messsage if and only if something is wrong. If everything is OK I don't need to know that. I'll just assume it is.\"\n\nKMail only goes on your nerves if you have to enter the passphrase or complains with a big warning in case you entered a wrong password. If everything is fine KMail doesn't give any alert.\n\nI'd say encryption is a nice to have in case you have confidential information (such as the password for your new server shell account in case you don't use key authorization) but digital signatures are much more important. The reader doesn't need to do anything like entering a password. He just can see inmediatly that the email is genuine and not a bad joke, spam, whatever.\n"
    author: "Arnomane"
  - subject: "Re: Kontact is aging"
    date: 2007-10-05
    body: "Hi. \nYes, I know that there are solutions to what I'm asking and that combining a few tools would probably get me fairly close to what I want to do.\nThat's not the point.\nThe point is that Linux in general expects me do do just that. Combine a few apps that do sort of what I want, and most of the time they overlap but don't go the full distance.\nThere's very few programs out there that manages to include the whole work-process from start to finish. \nI use KDE since it's the best thing out there but every now and then I run into things that I feel could be improved.\nSince I don't do programming I can only do so much. Filing bug reports and bitching in forums seems to be my skills. ;-)\n\nHere's to hoping that Canonical gets their bounty program in order so that I can toss some money at a devel that wants to do what I want. ;-)\n(No, I'm not wealty enough to sponsor a devel on my own).\n\nhttps://blueprints.launchpad.net/launchpad/+spec/bountypledges\nThis would be interesting. I hope they make something out of it."
    author: "Oscar"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Oh, come *on*, not one of these html-mails-are-evil-by-itself comments. Have been reading those since decades now. Please grow up. Thanks."
    author: "Robert"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "I totally agree... the kmail-html thing is the reason i'm staying with thunderbird, it's as easy as that. And most users will agree. In the 21. century you have to be able to format your text, no matter what the few \"i only use mutt anyways\"-fundamentalists are saying.\n\nI simply like sending a mail with pink background and hearty-things to my girlfriend, it keeps her happy and is no problem for me (using templates :).\n\nThe \"attach a pdf every time\" is obviously just meant to provoke, so i won't take that one serious.\n\nAs soon as kmail comes to sense, i'll switch."
    author: "Philipp G"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "I totally agree... the kmail-html thing is the reason i'm staying with thunderbird, it's as easy as that. And most users will agree. In the 21. century you have to be able to format your text, no matter what the few \"i only use mutt anyways\"-fundamentalists are saying.\n\nI simply like sending a mail with pink background and hearty-things to my girlfriend, it keeps her happy and is no problem for me (using templates :).\n\nThe \"attach a pdf every time\" is obviously just meant to provoke, so i won't take that one serious.\n\nAs soon as kmail comes to sense with html, i'll switch."
    author: "Philipp G"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "=:/ f*ing double clicking!"
    author: "Philipp G"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "I din't say that HTML emails are evil by themselves. I did say that HTML-only (!) emails are bad and that more than basic text layout features in HTML mode is IMHO not the right thing for an email client but for a word processor.\n\nYea I know there is IncrediMail out there and my sister used it but she grew up...\n"
    author: "Arnomane"
  - subject: "Re: Kontact is aging"
    date: 2007-10-05
    body: "Can someone please explain how to reply to an html formatted mail with Kmail without screwing the whole formatting?\nNearly every mail I received at work are html mails.\nWhen replying, Kmail transforms it to text without any respect to the original formatting. \n\nIt would be nice if kmail could preserve at least the kind of simple html formatting that it provides for composing (bullets, colors,...) when *replying* to an html mail.\n\nIt is a very common practice when a mail is sent to several persons that each person use a different color to highlight his comment  when replying. \nIf you try to participate in such thread with kmail you simply screw up everything. \n\n"
    author: "jms"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "I have heard some rumours on the internet that Mailody may eventually replace Kmail.  Is this true? Mailody was built to be only an IMAP client so I imagine POP support would have to be added to cater to the needs of a wider audience. \n\nIf I am only spreading an unfounded rumour, I apologise."
    author: "The Vicar"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Mailody will most likely use Akonadi in KDE 4.1, which will provide a backend for all email etc stuff. So it will be easy to replace KMail with it, and in time, it might do just that..."
    author: "jospoortvliet"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Thanks for explaining.  Much appreciated. :-)"
    author: "The Vicar"
  - subject: "But will mailody support POP3?"
    date: 2007-10-04
    body: "That is the question. AFAIK it's not in the plans yet (I hope I'm wrong). So until Mailody starts supporting POP3, it might not completely replace KMail and remain as an alternative focused on IMAP.\n\nI myself don't have much problems with KMail since I use POP3. I do have issues with the other KDE PIM stuff. :)"
    author: "Jucato"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "and, as far as i know, Mailody is a QT and not KDE Application.I dont think that KDE Policy allows non-kde applications to be included in the default module.\nAlso, according to the Mailody's website:\n[i]You might be wondering what Mailody is. It is a mail client. But it is not a full mail client like KMail is. It only talks IMAP.[/i]\n\nThen why Mailody should replace a full-featured, very nice intergrated KMail?\nat least not until Mailody reaches the current point of the KMail..."
    author: "Emil Sedgh"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Yes and it seems the Mailody developer is adamant that it shall remain IMAP only, which is why I was surprised by these rumours.  Is someone intending to fork it and add more features to make it comparable to Kmail?"
    author: "The Vicar"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "html sigs etc are coming, even more, isn't this already included in KDE 3.5.7? If not, I expect it for 3.5.8...\n\nAnd yes, Gnome is catching up with 3.5.x, but hey, 4.0 is almost ready :D\n\nIt'll provide us with a great framework which will allow us to speed up development and innovation, so I think this is the right moment to release it. Of course, sooner is always better, but there is this thing called real life..."
    author: "jospoortvliet"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "Why do you think GNOME is catching up to KDE?\n\nSure, GNOME is gaining features you'll find in KDE, but that goes both ways. You come off as an arrogant prick by constantly claiming that GNOME is catching up."
    author: "thebluesgnr"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "As a framework KDE is superior to GNOME. This is not trolling but just because of the high resusage of nice components within KDE. E.g. I am not aware that all GNOME apps are able using the same spell checker system in every text input field, I am not aware that GNOME has an equally good KIOSK mode (I currently need it and I just figured this out).\n\nBut GNOME is catching up here (e.g. the GNOME password storage and other things) which is a good thing for everyone.\n\nBut there is no doubt that there are quite some GTK/GNOME apps that are the best applications of their class, the same holds for a number of KDE apps. In both desktops there are apps that try to catch up with their counterpart.\n\nSo it's no arrogancy to say that GNOME desktop is still behind to KDE in many ways but that does NOT mean that GNOME is crap or that GNOME is inferior in every aspect.\n\nI use that which is working best and this is a KDE desktop with a lot of KDE and some GTK/GNOME + other frameworks' apps."
    author: "Arnomane"
  - subject: "Re: Kontact is aging"
    date: 2007-10-04
    body: "What are you overreacting at?"
    author: "NabLa"
  - subject: "Kontact is a great PIM, but...."
    date: 2007-10-04
    body: "... the only problem I had with it was speed.  For what ever reason, downloading e-mails from my multiple POP3 accounts took a very long time (sometimes 4 to 5 minutes to complete, if I went several days without checking e-mails).\n\nThese days I use SeaMonkey's e-mail client, which is very nice, now that it supports OpenPGP.  What matters most to me, however, is it's speed.\n\nIf future releases of Kontact could improve upon performance, then I'll be back to using Kontact full-time.\n\nIt certainly beats anything else, in regards to integration and ease of use."
    author: "Judland"
  - subject: "Re: Kontact is a great PIM, but...."
    date: 2007-10-04
    body: "> ... the only problem I had with it was speed. For what ever reason, downloading e-mails from my multiple POP3 accounts took a very long time (sometimes 4 to 5 minutes to complete, if I went several days without checking e-mails).\n\nRight, it's dead slow, when you do not have an MTA doing the email fetching and filtering. The frickin' KMail unfortunatey isn't multithreaded..."
    author: "Carlo"
  - subject: "Re: Kontact is a great PIM, but...."
    date: 2007-10-04
    body: "> ... the only problem I had with it was speed. For what ever reason, downloading e-mails from my multiple POP3 accounts took a very long time (sometimes 4 to 5 minutes to complete, if I went several days without checking e-mails).\n\nRight. It's dead slow, when you do not have an MTA doing the email fetching and filtering. The frickin' KMail unfortunatey isn't multithreaded... :("
    author: "Carlo"
  - subject: "Calendar solution"
    date: 2007-10-04
    body: "My wife and I are on opposite schedules, so we minimalize how often our daughter is in daycare.  We prefer to raise our kid ourselves.  That also means we don't see each other much, and our schedules are pretty crazy.  We have one car, and between my wife being a full time student, activities, her job, my job, and her Mary Kay business on the side, it just gets crazy.\n\nI was beginning to think we should have a proper calendar and I was thinking of something like Goolge Calendar, though I haven't looked at it much yet.  We both use KDE, and is there a way for us to use Kontact to look up, and edit a shared calendar somewhere (be it Google Calendar or some other service)?\n\nIt seems silly to throw up a server for something so small."
    author: "T. J. Brumfield"
  - subject: "Re: Calendar solution"
    date: 2007-10-04
    body: "Is there a way for us to use Kontact to look up, and edit a shared calendar somewhere\n\nAbsolutely. In the calendar view, click add in the lower left. This will add another calendar resource. Here, you can tell it to use a calendar in a local file (among others).\n\nIf you add a remote calendar (eg a google calendar's ics url), kontact will be able to read but not write to it."
    author: "Greg M"
  - subject: "Re: Calendar solution"
    date: 2007-10-04
    body: "The statement seems contradictory.\n\nIt can edit a shared calendar, but if I add a remote calendar it can't write to it?"
    author: "T. J. Brumfield"
  - subject: "Re: Calendar solution"
    date: 2007-10-04
    body: "Calendar is capable of writing to remote calendars if the server will accept writes. I have used 'Calendar in Remote File' with the OpenXchange webdav interface for example. I haven't used Google calendars so I can't say for sure, but I guess it may not acceept writes on that interface."
    author: "Al"
  - subject: "Re: Calendar solution"
    date: 2008-08-26
    body: "Figured even though this email is a bit old i would bump it. Yes you can do this with google calendar, you need to setup java and a program called gcaldaemon. I have it working between my desktop and laptop and syncing back to my treo. Works well enough. wasn't to much of a pain to setup. You can find out more about it here, http://gcaldaemon.sourceforge.net/index.html\n\nYour probably going to want to setup a shared calendar on google and then just point both of your systems to the same calendar it does do remote writes but if i recall deletes are/were a issue. I do know i had a couple of recent events i moved cause i set them up on the wrong week and there was no issues so perhaps the deletes were addresses??? not 100% on that. Hope that helps."
    author: "Timothy Tuck"
  - subject: "Re: Calendar solution"
    date: 2007-10-04
    body: "> (be it Google Calendar or some other service)?\n\nBy using Google apps/services you have to agree to give them any right to use your data as they like. Definitely not the place to leave personal data."
    author: "Carlo"
  - subject: "LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "I really love and use Kontact. But I have really problems with kmail. Simple things like using the up/down arrow keys doesn't change emails. To many other quirks. Absolutely love the integration aspect. I love that I can keep so many windows within one window. I just really wish that I could chop and change the email component, I find Evolutions email and calandering component better in so many ways. But I use Kontact for a lot more than just email and calandering."
    author: "Justin"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "Just use the right/left arrows... but I agree there are some problems with kmail. Plans for kontact are even more integration : being able to see your email, while looking at your calendar and notes in the same window.\n"
    author: "yoho"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "\"Just use the right/left arrows...\"\n\nThis is a serious problem with KMail. Using right and left arrow keys only in a vertical textbox is just plain silly. The developers also can't see how silly that is, naively believe that they have come up with some usability solution."
    author: "Segedunum"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "If up/down were previous/next, then how would you scroll up and down the message?\n\nUsing left/right? ;o)"
    author: "mart"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "\"If up/down were previous/next, then how would you scroll up and down the message?\"\n\nI hear that argument time and again, and it simply isn't necessary nor is it relevant here. You're trying to solve some problem that just can't be solved or doesn't exist. The simple fact is that scrolling through a vertical listbox with nothing but horizontal arrow keys is illogical and silly, no other KDE application does it (for obvious reasons), and certainly shouldn't be the default behaviour in the application."
    author: "Segedunum"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "At first you can change keybindings with a nice GUI. Yea its not about you own desktop. You're complaining about the default and everyone is using the default... ;-)\n\nAnyways: Ignoring facts as you propose is *not* a solution. Up and down arrow is the natural choice for scrolling up and down in a window, especially in larger text documents. So the email content has to keep these key bindings as default, regardless if you personally like it or not.\n\nOk you could use these keybindings in every scroll field but the consequence is that you need to introduce a scroll focus in KMail. And how do you change it? Via mouse. No good idea requiring this for key navigation. It renders it useless. Furthermore the focus needs to be visible.\n\nAnother solution would be using Shift + P and Shift + Down for scrolling one of the scroll areas but it is already used for selecting text in input fields.\n\nSo suggest a better keybinding/solution and I am confident developers will listen."
    author: "Arnomane"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "Agreed. I love the way I can use KMail with just the keyboard, it's the only application which properly does that. I love the left/right key going to next/previous email, it works that way in akregator too. Pretty neath, imho."
    author: "jospoortvliet"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "\"Up and down arrow is the natural choice for scrolling up and down in a window, especially in larger text documents. So the email content has to keep these key bindings as default, regardless if you personally like it or not.\"\n\nIt's not logical in the e-mail listbox, just as it isn't logical in the e-mail content pane either. Regardless of what problem you personally feel that the KMail developers are solving here, this simply is not the logical, default way to do things. Horizontal arrow key navigation in any vertical GUI widget is simply silly.\n\n\"So suggest a better keybinding/solution and I am confident developers will listen.\"\n\nA better key binding for what? Using horizontal arrow keys to navigate through a vertical textbox is just logically insane. KMail should keep the defaults as per other e-mail clients out there, and allow people to use the other (illogical) key binding for people who find it useful. It's a break from logic for the benefit of a few who want to keep their hands off the mouse.\n\nYou're implying that there is some sort of need to scroll through the e-mail list and the e-mail content pane at the same time. As is the case with other applications, this cannot be done in a logical fashion."
    author: "Segedunum"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-04
    body: "> Simple things like using the up/down arrow keys doesn't change emails.\n\nMy god! Change your shorcuts!\n\nAre you sure you don't like kmail or it's only you are not aware of all of it's capabilities?"
    author: "Damnshock"
  - subject: "Re: LOVE Kontact, hate Kmail."
    date: 2007-10-05
    body: "use left-right instead of up-down to navigate through the mails."
    author: "Christian Parpart"
  - subject: "Korganizer"
    date: 2007-10-04
    body: "I use Korganizer all the time.  I like the simple, elegant interface.  I don't use Kontact because I'm already used to Thunderbird, but that could conceivably change in the future."
    author: "Matthew Flaschen"
  - subject: "Re: Korganizer"
    date: 2007-10-04
    body: "> I use Korganizer all the time. [...] I don't use Kontact because I'm already used to Thunderbird.\n\nBe careful with the names. What you mean with Kontact, is KMail. Kontact is the PIM suite containing KMail, KOrganizer, and so on.\n\nI'm using KMail and am very happy with it. KOrganizer is currently abandoned because of vacation. ;-)"
    author: "Stefan"
  - subject: "Exchange integration"
    date: 2007-10-04
    body: "One missing feature is the exchange integration ! Why don't people from kmail look at the evolution code and copy/paste things ? I really miss that one which made me change recently from kmail to evolution... I wish I came back to kmail, but without a propre exchange integration, no way :-/"
    author: "yoho"
  - subject: "Re: Exchange integration"
    date: 2007-10-04
    body: "a) I doubt that transferring a feature from one project to another when both projects have massively different architectures, toolkits and libraries is a simple as \"copy/pasting things\".\n\nb) They are short-staffed and hugely overworked and have a million other things to do.\n\nStill, if it's as easy as you make out, why not do it yourself? I'm sure they'll appreciate the help! :)"
    author: "Anon"
  - subject: "Re: Exchange integration"
    date: 2007-10-04
    body: "a) As noted before, it's not that easy to copy that functionality. Especially as you need access to an exchange server (in the best case with admin rights). And KDE-PIM human resources are very very limited :-(\n\nb) There is some work for an openchange plugin for Akonadi. So there's a good chance that we will see exchange support through Akonadi in KDE 4.1. And there is no doubt, that an openchange based solution is way better than the current \"hacked\" solution from Evolution.\n\n"
    author: "Birdy"
  - subject: "Re: Exchange integration"
    date: 2007-10-04
    body: "There are several threads about this in both the archives of kde-pim and kdepim-users, e.g. http://lists.kde.org/?t=118158834200003&r=1&w=2\n\nMaybe some of them could be of help for you as well"
    author: "Kevin Krammer"
  - subject: "Re: Exchange integration"
    date: 2007-10-07
    body: "I join the plea for better Exchange integration."
    author: "yuval"
  - subject: "Re: Exchange integration"
    date: 2007-11-06
    body: "I totally agree.  I really want to use kde-pim, but without exchange integration I just can't.  \nAnd for the record, I really wish these linux snobs would quit with the \"if it's so easy why don't you do it\" crap.  That's childish.  Not everyone is or wants to be a software developer.  If the world were full of nothing but software developers, then the world would be a pretty sad place.  Who would milk the cows, take out the trash, keep the financial markets going?  If linux is going to achieve mainstream acceptance, that can only happen by providing a system for use by non-software developers--people who have needs and don't write code.  Otherwise Linux will always be the unloved stepchild of a Microsoft world."
    author: "John Whitaker"
  - subject: "Support of vcard in kmail"
    date: 2007-10-04
    body: "One thing I will love in Kmail, it'll be support for vcard. Thunderbird lets you introduce your personal information and you can incorporate it like a vcard.\n\nI'ld love this same functionality in kmail and it'll work well. I am spanish and in Thunderbird the accents show bad."
    author: "alf"
  - subject: "Re: Support of vcard in kmail"
    date: 2007-10-04
    body: "Kontact perfectly supports vcards.... What do you mean? If I get an email with an attached vcard I can incorporate it into my contacts with an easy click..."
    author: "Thomas"
  - subject: "KMail is great, but..."
    date: 2007-10-04
    body: "I'm really missing a fast search mechanism.\nI would like to search all my 100K e-mails in seconds, not 15 minutes.\n\nI guess some kind of Strigi integration could be useful in the future."
    author: "Jan Vidar Krey"
  - subject: "Re: KMail is great, but..."
    date: 2007-10-04
    body: "coming, coming ;-)"
    author: "jospoortvliet"
  - subject: "Re: KMail is great, but..."
    date: 2007-10-06
    body: "Use Kerry Beagle. Works a treat."
    author: "DSC"
  - subject: "POP Filters"
    date: 2007-10-04
    body: "I would really love to understand how POP Filters work in Kmail.\nI simply would like mail to be deleted from the server on the following condition: \"X-Spam-Flag\" contains \"YES\". Tell me how I can make it work  because it does not seem to be possible :-) Very simple\nBTW could POP Filters not get another name that is more understable ?\nOhhh... and why on earth does it keep asking if i want to store the password in file and not i kdewallet (i uninstalled KDEwallet because it bothers me) since i select no the password is stored and also if i select yes. There is something very very wrond.  \nOn Ubuntu/Kubuntu BTW and the behaviour was the same with last version"
    author: "Jand"
  - subject: "Printing contacts"
    date: 2007-10-04
    body: "Printing your contacts via kaddressbook component is ... hmm ... limited ;-)\nNot sure why good printing styles are still missing for such a long time.\nThe other stuff is ok - I like the kolab-kontact-combo :-)\n\nBye\n\n  Thorsten  "
    author: "Thorsten Schnebeck"
  - subject: "Reality check"
    date: 2007-10-04
    body: "Kontact is buggy and I can't say how much I hate popup screens. Even the previous mail function \"jumps\" 3-6 lines. Try to forward a mail with attachments and bet if it gets received. I hardly find any application that returns me so many error popups or constantly freezes on the KDE desktop.\n\nNo Evolution is not much better. It just sucks less. And Thunderbird just works fine except for Mozilla Foundation that does not support Sunbird sufficiently, the missing component."
    author: "gerd"
  - subject: "Re: Reality check"
    date: 2007-10-05
    body: "sorry.. but this sounds strange to me. I'm using kmail/kontact since 4-5 years now with more than 10.000 messages in sizes between 10k and 15mb and for 99.9% of the time Kontact is rock-solid. My mail-folder contains >4 Gb as a compressed bzip tarball. I would not use kmail if it was like what you describe."
    author: "Thomas"
  - subject: "kmail"
    date: 2007-10-21
    body: "Let me know when you can Forward an email that has an attachment in it.  Then maybe I will look at it again.  Until then, no thanks.\n"
    author: "Mike"
  - subject: "Re: kmail"
    date: 2007-10-21
    body: "Forwarding emails with attachment works for me, and has for years.  No idea what you're talking about. \n\n"
    author: "cm"
  - subject: "Re: kmail"
    date: 2007-10-21
    body: "Mike, you can forward an email that has an attachment in it."
    author: "Boudewijn Rempt"
---
...but now <a href="http://blue-gnu.biz">Blue-GNU</a> is showing you the way with <a href="http://kontact.kde.org/">Kontact</a>, in an article titled <a href="http://blue-gnu.biz/content/stay_know_kde_039_s_kontact">Stay In the Know With KDE's Kontact</a>.  The author says "<em>I finally settled on Kontact to keep me in touch with the reality of my hectic daily schedule.</em>"  Blue-GNU breaks down their use of each application integrated into Kontact, starting with <a href="http://kontact.kde.org/kmail/">KMail</a> and then going through <a href="http://kontact.kde.org/components.php#contacts">KAddressBook</a>, <a href="http://kontact.kde.org/korganizer/">KOrganizer</a>, and <a href="http://kontact.kde.org/components.php#notes">KNotes</a>.  It concludes, "<em>...Kontact is one of the best PIMs I've seen. I even prefer it to Outlook, which I used to prefer over the GNU/Linux tools in the past.</em>"

<!--break-->
