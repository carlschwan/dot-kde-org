---
title: "KDE 4.0 Release Event Contest"
date:    2007-10-04
authors:
  - "Wade"
slug:    kde-40-release-event-contest
comments:
  - subject: "My submission"
    date: 2007-10-04
    body: "Funny idea, congratulations on that.\n\nMy version: Because I'm bored. :-)"
    author: "Debian User"
  - subject: "Re: My submission"
    date: 2007-10-04
    body: "so.. this is it when you're enthusiastic... wow.. great... How's it if you feel just \"normal\" or \"moderate\"?"
    author: "Thomas"
  - subject: "Re: My submission"
    date: 2007-10-04
    body: "I'll apply for summer internship if I win.\n\nOh, and KDE is neat."
    author: "EXPERT PROGRAMMER"
  - subject: "Re: My submission"
    date: 2007-10-05
    body: "Stupid question.\n\nThe answer is that he is overload with work load, to earn his living.\nAnd read slashdot.\n\nWhat else?"
    author: "she"
  - subject: "I'm already here!"
    date: 2007-10-04
    body: "I'm actually working at Google, and don't need to fly anywhere...\n\nNot to make you guys jealous, or anything ;)\n"
    author: "Dima"
  - subject: "Re: I'm already here!"
    date: 2007-10-04
    body: "I'm not. And I'm jealous and everything :P"
    author: "Andrei Nistor"
  - subject: "Re: I'm already here!"
    date: 2007-10-04
    body: "Right. But that will not help you (or me) get in to the event, I guess. :-("
    author: "Martin"
  - subject: "Re: I'm already here!"
    date: 2007-10-04
    body: "As long as we end up with enough room (personally, I'm sure there will be), if you're in the Mountainview area and want to attend I doubt it'll be a problem."
    author: "Aaron J. Seigo"
  - subject: "Re: I'm already here!"
    date: 2007-10-04
    body: "That would be great! Would we sign up somewhere or would you just tell Google to admit Googlers to the event?"
    author: "Martin"
  - subject: "Re: I'm already here!"
    date: 2007-10-05
    body: "We will be announcing open registration in a few weeks. We'd like to ensure that press and industry, as well as KDE contributors get the hotel rooms first, and what's left over will be open to anyone willing to find a way out there :)\n\nCheers"
    author: "Troy Unrau"
  - subject: "KDE 4.0 Release Event Contest"
    date: 2007-10-05
    body: "Because I want to witness the release of KDE 4.0, that marks the beginning of the integration process which will bring the powerful new technologies of this KDE 4.0 event. A release that marks the stabilization of the current codebase, and aimed to polish and fixes the bugs."
    author: "Felix E Rendon"
  - subject: "I am Luca."
    date: 2007-10-05
    body: "I call Luca, I have 35 years, alive in Italy.\nSin from small I have had a passion for computer science and from when they are on Linux, I choose KDE like session ..... \nIt would appeal to living to me to the center of Computer science, Computer science: together to You."
    author: "Luca Schembari"
  - subject: "Re: I am Luca."
    date: 2007-10-05
    body: "This is weird."
    author: "Philipp G"
  - subject: "Re: I am Luca."
    date: 2007-10-05
    body: "Well, what the previous poster was trying to say... err.... yep... it's weird"
    author: "Thomas"
  - subject: "Re: I am Luca."
    date: 2007-10-06
    body: "Agreed ... i'm totally confused right now."
    author: "Michael"
  - subject: "Re: I am Luca."
    date: 2007-10-07
    body: "Unfortunately the first step towards learning english doesn't involve Babelfish"
    author: "MamiyaOtaru"
  - subject: "Re: I am Luca."
    date: 2007-10-07
    body: "Entsprech di Google not translate?\n\n(was originally: Not even Google translate? ;)"
    author: "Hans"
  - subject: "Re: I am Luca."
    date: 2007-10-08
    body: "Luca...  don't mind these other folks and their comments.  English speaking or not, I think anyone who chooses KDE is alright in my book.  Keep it up and maybe someday you'll get to come out here to sunny Silicon Valley."
    author: "Sean Kellogg"
---
The KDE 4.0 Release Event Team is pleased to announce a contest for the <a href="http://dot.kde.org/1191409937/">KDE 4.0 Release Event</a>. The winners of this contest will be flown out to Mountain View, California on January 17-19, 2008. Hotel and meals will be covered for them during the event. Read on for information about the contest.



<!--break-->
<p>The purpose of this contest is to find the most enthusiastic KDE community member and reward him or her with a ticket to the KDE 4.0 Release Event held at the Google Headquarters at Mountain View, CA. There, he or she will be representing the community together with the other KDE community attendees. The winner will speak briefly at the event to share the spirit of their winning submission.</p>

<p>To prove that you are the most enthusiastic KDE community member, answer the question: "Why should you be at the KDE 4.0 Release Event?". Entries should be a maximum of 1,000 words, and anyone is free to send in a submission. You can send it in plain-text or another open format to release-event@kde.org. Entries will be accepted from midnight October 1, 2007 until midnight October 21, 2007.</p>

<p>The contest entries will be reviewed by the KDE e.V. board and the Marketing Working Group. The winner will be announced on October 29, 2007. As usual, bribery of the judges is not only allowed, but recommended.</p>

