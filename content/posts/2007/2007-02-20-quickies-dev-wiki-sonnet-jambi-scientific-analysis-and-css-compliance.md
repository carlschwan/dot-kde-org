---
title: "Quickies: Dev Wiki, Sonnet, Jambi, Scientific Analysis and CSS Compliance"
date:    2007-02-20
authors:
  - "jriddell"
slug:    quickies-dev-wiki-sonnet-jambi-scientific-analysis-and-css-compliance
comments:
  - subject: "I guess we don't need Unity..."
    date: 2007-02-20
    body: "...if KHTML support all css-3 features already :)"
    author: "Patcito"
  - subject: "firefox killer rumour"
    date: 2007-02-20
    body: "I heard I think it was on the yahoo video that some KHTML developpers are  doing a Unity based browser that will run on all platforms and will have the same kind of features as firefox such as extentions and all.\n\n Is this true?\n\n"
    author: "Bob"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "Konqueror for KDE 4 will run on more platforms, including Windows and Mac.  Loading firefox extensions will probably not happen though, as we will likely continue to use our own mechanisms. We do already share plugin interfaces for things like flash. Basically, konq will be available to a much wider audience now.\n\n(Unless there is some project I haven't heard about yet...)"
    author: "Troy Unrau"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "I didn't say it will use firefox extentions, I said:\n\" the same kind of features as firefox such as extentions and all.\"\nso that it will have its own extentions (+ netscape plugins). \n\nDid you watch the Yahoo video a while ago? There was a presentation of two KHTML devs and at one point one of them say he's working on something like this, at least I think he says he's working on a Unity based project (and it's not konqueror)."
    author: "Bob"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "It was astroturfing to try to get more devs to gather around a project that is controversial.\n\nThe projet is owned by Apple, so you have to work with Apple, but in reality, you can only work *for* Apple. The attitude stinks, and you either take it or be publically treated of being \"insane\" for wanting more openness.\n\nA couple of Trolltech engineers do swallow their pride and make some progress on the Qt backend. Well, they have a vested interest to do so I guess.\n\nAnd to take the cake, the repository is now frozen because Apple want to stabilize, so everyone else must bow. Nice peek into the future.\n"
    author: "Bill"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "i really didn't want to get involved with this meme again, but this kind of negative and incorrect tripe is just too much.\n\nfirst, it wasn't \"astroturfing\". george actually has been working on a qt4 web browsing application that uses unity. i've seen it.\n\nthe only real reason the project is controversial is that the reasons why it is a good thing requires a bit of big picture thinking, something i understand a lot of people struggle with. \n\n> do swallow their pride\n\nah yes. let's maintain that \"pride\", right? because that's how we get good technology, through unabated self-stroking. forget the big picture issues, forget actually trying to get the best solutions ... let's keep our pride intact. sorry, but this attitude stinks and prevents good decisions from being made.\n\n> so you have to work with Apple, but in reality\n\nyou know what the best way to ensure progress is never made? to ignore progress that is made and fight against any additional progress that might occur. that way you can always remain right that the world is a dirty, nasty place.\n\nwe have people with review and commit access to webkit, something that was deemed a non-starter not so long ago. there is more room for improvement, but it's actually going to take some support.\n\n> they have a vested interest to do so I guess\n\nwhich is what? making good technology that has the best chance of making a real difference in the market and for our users? if that wasn't the vested interest -you- were thinking of i'd love to hear it.\n\n> Nice peek into the future.\n\nyes, because we'd never want to freeze the code base for a release either. working with others means making concessions at times, and i completely agree that apple has a lot left to learn when it comes to working with others. so, apparently, do some of us.\n\napple made some bone-headed moves, but we need to be bigger than that and get what we (and our users) need and want in spite of their past behaviour."
    author: "Aaron J. Seigo"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "Now I'm lost, what browser is everyone talking about?"
    author: "Ben"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "this is not about webbrowser, but about the rendering engine. KHTML was the foundation for Apple WebKit...\n\nThere is a lot of discussion to unite developer power and to drop KHTML in favor of WebKit. On the other hand WebKit is not the most open project (to put it mildly)... \n\nBy the way: is at least the developer mailing list public? (I do not think so)"
    author: "MK"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-21
    body: "<A href=\"http://lists.webkit.org/mailman/listinfo\">WebKit Developer Mailing Lists</A>"
    author: "Anonymous Ferret"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "this is not about webbrowser, but about the rendering engine. KHTML was the foundation for Apple WebKit...\n\nThere is a lot of discussion to unite developer power and to drop KHTML in favor of WebKit. On the other hand WebKit is not the most open project (to put it mildly)... \n\nBy the way: is at least the WebKit developer mailing list public? (I do not think so)"
    author: "MK"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "found it http://lists.macosforge.org/pipermail/webkit-dev"
    author: "MK"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-20
    body: "> i really didn't want to get involved with this meme again\n\nYet you decide to speak up *yet again* on a subject you know *little about*?\n\nExcellent. That's how good decisions are taken indeed.\nNow if you could also blog about it, that would be perfect ;(\n\nYou know, before one can see the \"big picture\", one as first to get the facts straight, something you have yet to do about KHTML/WebKit.\n\nThere are also uninformed people that routinely feel the need to publically vow for the big merger between KDE and GNOME, because the \"big picture\" obviously goes that way...\n\nWell, just like those people, you might be missing a few large holes at our feet while looking \"broader\".\n\nSuch as the fact that there is no cooperation whatsoever except in the rosy rosy world of whishful thinking.\n\nApple, despite being asked many times, has declined to share any part of the steering of the WebKit project with anyone.\n\nIf you are interested in the significance and success of the KDE project, you'd be very misguided to contribute to WebKit in those conditions.\n\nHarri Porten made excellents points about that in a letter he sent to other KHTML developers.\n\nYou'd be effectively working for the competition in the futile hope than one day it would share with you part of its crown.\nThat's the oldest way in the world to maintain people in slavery.\n\nAnyway, Lars Knoll as publically asked for clarifications about the steering of the project on WebKit mailing lists.\nThis was on 2007-02-17. No reply to date.\n\nI *can't wait* to read Apple's answer to that one.\n\n>> they have a vested interest to do so I guess\n> which is what?\n\nTrolltech's vested interest is to increase Qt's significance in the market place.\nNot KDE's. Qt's.\nI'm not implying anything here, it's just a fact.\n\n> ah yes. let's maintain that \"pride\", right?\n\nPride is only an attribute. The attribute of free men.\nIt's not important to maintain the pride, it's important to maintain the freedom at all cost.\n\nDon't be too keen on giving it up in exchange for a few flashy Web 2.0 features you'll never really enjoy with your drawn-into-insignificance Konqueror UA string anyway.\n\n---\nAn \"anonymous\" KHTML coder.\n"
    author: "Bill"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-21
    body: "Well, Bill, Bob, Ben, or whoever thinks they know me so well. :-)  I am working on the Qt backend and I don't work for Trolltech or Apple.  I will be checking in code in a few hours to the apparently frozen repository (oh the horrors of a stabilization phase).  I don't want developers to gather around a controversial project because I don't need more drama like yours.  I do want to bring two communities with huge potential together though.  That's all.  Sorry to spoil your day.\n\nPS: I won't be killing FireFox. :-)"
    author: "George Staikos"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-21
    body: "> Well, Bill, Bob, Ben,\n\nI don't make a mystery of my identity\n\n> I do want to bring two communities with huge potential together though. \n\nAnd you acknowledged just a few days ago in a reply to one of my mail on kfm-devel that this plan had utterly failed.\n\n> drama\n\nDrama? What drama? I'm only interested in clearing up the PR smoke screen around WebKit so that work can continue on KHTML with a bit of serenity.\n\n"
    author: "Bill"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-21
    body: "I am a big fan of the idea of \"unity\", but after reading a bit on the webkit and the kfm-devel list I am rather skeptical about the perspective of such a project. Perhaps staying with a development model that worked very well so far is indeed the right thing....\n\nOr is it time for another blog entry (http://www.kdedevelopers.org/node/1049)? At least to me that not too much has changed since then...\n\n\n-----\n\nfrom Georges email to kfm-devel\n\nhttp://lists.kde.org/?l=kfm-devel&m=117169147422652&w=2\n\n* \"but not as long as we are not considered equals, treated like equals, encouraged to collaborate (vs -discouraged-), and as long as our development model (very large, distributed, voluntary) is not given consideration, it's not going to happen.\"\n\n* \"Yes, after wasting an enormous amount of my own personal time and money on this, unfortunately nothing ever came of it.\"\n\n* \"It now feels like Apple considers KDE to be marginalized or insignificant.\""
    author: "MK"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-21
    body: "Yes\u00b7 I stated the problem and now I'm working to solve it. I guess that's the decision we have to make. I made mine, and yes Zack also made his."
    author: "George Staikos"
  - subject: "Re: firefox killer rumour"
    date: 2007-02-21
    body: "(Actually I was refering to Zacks blog: http://www.kdedevelopers.org/node/1002)\n\nFrom a bit reading the mailing lists I got not the impression that not much has changed since Zack's blog. His comment (April 2005) pretty much sums it up:\n\n\"Apple doesn't have to work with us, it's their right. But it is also my right to not be happy about it, to not be smiling and keep pretending everything is peachy [...]. I feel like we did everything we could. The ball was, is and always will be in Apple's hands.\"\n\nI am more than happy if you can solve the problem, but I am afraid it is indeed in Apple's hand. Good luck!"
    author: "MK"
  - subject: "Gmail?"
    date: 2007-02-20
    body: "Does this mean Gmail will now work (in full AJAX mode) in Konqueror?  Or is it Gmail's fault that it doesn't work, and not some feature shortage in Konqueror?"
    author: "Guga"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: "It's gmail's fault.  It's sad, but if you set your browser agent to firefox in Konq, Gmail works just fine without being crippled by \"html\" mode.  At least it does for me. The only time I find it sometimes borks is when accidentally using page back/forward, instead of navigating through the gmail interface.\n\nWhen that happens and I do a page refresh, I receive an amusing message advising me that Symantec security products may be interfering with my ability to view gmail messages. ;)"
    author: "elsewhere"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: "Reload/back/next is a gmail problem ;) It happens also with other browser (at least it uses to happen also when I still used gmail)."
    author: "Cyrille Berger"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: "RichText Editing? Anyone?\n\nit works fine in webkit... So using it as a base, isn't a that bad idea....\n\nI'm looking forward to unity..."
    author: "boemer"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: "Do you really need it?\n\nPlease also remind yourself that it will only be enabled by websites that recognize Konqueror. When you then take into consideration that not even Google recognize Konqueror, then rich text editing will only work for a few power users that specifically asks Konqueror to spoof as another browser, and then still only on those approximately 4 pages on the entire internet that uses it."
    author: "Allan Sandfeld"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: "> Do you really need it?\n\nyes. this is pretty critical for most web-based content management systems and locks konqi out as an option for a lot of applications.\n\nthen again, maybe we don't really need word processors. vim is good enough.\n\n> not even Google recognize Konqueror,\n\ni was informed that this has changed recently\n\n> for a few power users that specifically asks Konqueror\n\nthis is one reason getting a unified webkit is important. getting web site providers to stop trying to work around web browser breakage and just trusting browsers to be standards compliant is another important, but far larger and much more long term, goal.\n\n> still only on those approximately 4 pages on the entire internet\n\nlike blog sites? or web forums? or CMS backends? or intranet apps? or... =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: ">this is one reason getting a unified webkit is important. getting web site >providers to stop trying to work around web browser breakage and just trusting >browsers to be standards compliant is another important, but far larger and much >more long term, goal.\n\nMmmmm, unbroken world wide web! \n\nYou think you could ever get this into Redmond? "
    author: "Ben"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: ">> not even Google recognize Konqueror,\n \n> i was informed that this has changed recently\n\nUnless I'm using the wrong Google site this unfortunately isn't correct. When I open www.gmail.com I get a \"For a better Gmail experience, use a fully supported browser.\" Bummer :-("
    author: "LB"
  - subject: "Re: Gmail?"
    date: 2007-02-21
    body: "Yes but you can choose to continue anyway, or just fake the user agent."
    author: "Tim"
  - subject: "Re: Gmail?"
    date: 2007-02-22
    body: ">> not even Google recognize Konqueror,\n>i was informed that this has changed recently\n\nReally?  I was the guy that spoke to the google developers in person to get them fix this.  I'd be interested to know if it's being fixed or not.  \n"
    author: "JohnFlux"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: "<i>then rich text editing will only work for a few power users that specifically asks Konqueror to spoof as another browser</i>\n\nThat is not true. Even if you modify the browser identification Gmail won't allow you using rich text editing.\n\nIn fact I have never seen a web using rich text editing working in Konqueror, never. There are varios javascript codes implementing rich text editing and no one of them work in Konqueror."
    author: "I\u00f1aki Baz"
  - subject: "Re: Gmail?"
    date: 2007-02-21
    body: "That's the whole point, I'm using it heavely for contents management systemen, and because I can choice the editor myself and what is supported or not... I could directly use KHTML, would it support it...\n\nCSS3 support ist nice, but I'm still missing a few of the visible features, like rounded borders, multi-column text flows..."
    author: "boemer"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: "The chat feature still does not work through konqueror with or without the firefox user-agent. I'm sure there are other things too. Adding contacts to groups is problematic I think."
    author: "Steve"
  - subject: "Re: Gmail?"
    date: 2007-02-20
    body: "Yes, that's right. GMail is the only reason for me to keep firefox running at home. And since firefox crashes non deterministic with it, it really sucks. :-(\n\nKepp pushing, KHTML people!\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: Gmail?"
    date: 2007-02-21
    body: "Hardly any \"Web 2.0\" pages work in konqueror. It is annoying. Google maps works, but gmail, facebook, etc just do weird things (or nothing)."
    author: "Tim"
  - subject: "Developer wiki name"
    date: 2007-02-20
    body: "First, please read the explanation: http://developernew.kde.org/Talk:Projects/NamingTheWiki\n\nAn excerpt:\n\n\"\"\"Garage for me gives connotations of cool, gritty work. I'm all for Garage. Maybe 'KDE Garage' or 'Tech Garage' or whatever, but I like it. It's also a failry unique yet easily rememberable name. I can easily imagine directing people towards \"the garage\" for tutorials etc.\"\"\"\n\nI think TechBase and TechNet is named after Microsoft's TechNet or MSDN, so let's avoid such hacky names.\n"
    author: "."
  - subject: "Re: Developer wiki name"
    date: 2007-02-20
    body: "> I think TechBase and TechNet is named after Microsoft's TechNet or MSDN,\n> so let's avoid such hacky names.\n\nThat's probably the worst possible reason to (not) choose a name.\nChoose a name that makes sense, and saying things like: \"hakunamatata is a great name since it gives me such a good feeling\" is just like the above reason, not really relevant."
    author: "Thomas Zander"
  - subject: "Re: Developer wiki name"
    date: 2007-02-20
    body: "It's about metaphors most of us know OR brands. \n\nYour comparison looks weird - if you dislike metaphors, then K Desktop Environment would be equally bad name to you (not mentioning \"KDE\"), because: \n\n1. 'K' means nothing\n2. 'Desktop' gives just a feeling of something related to a furniture\n3. 'Environment' is so technical\n\nDo you criticize Solid, Phonon, Akonadi, Oxygen as well?\n"
    author: "."
  - subject: "Re: Developer wiki name"
    date: 2007-02-25
    body: "personally I hate those names. I can never remember which is which! I liked the old naming scheme... it may have been a bit silly, but at least I could tell at a glance whether a program was part of kde. and usually it would give me an idea of what the program was for, too."
    author: "Chani"
  - subject: "Re: Developer wiki name"
    date: 2007-02-20
    body: "To be clear, I called \"TechNet\", \"TechBase\" a hacky name and things like Lab or Garage - a brand or metaphor.\n"
    author: "."
  - subject: "Re: Developer wiki name"
    date: 2007-02-20
    body: "this is why we're having a poll and a community discussion about it. some people like no-nonesense descriptive noun-like term. others like brand names that conjure up images and feelings. either is workable, really, and which you prefer is probably related to your personality. and yes, i too have a personal preference but i'm happy to go with whatever the majority wants.\n\nwhat's really cute is that the name we were originally going to go with based on consensus of the team actually involved with the site is currently leading by a healthy margin. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Developer wiki name"
    date: 2007-02-20
    body: "Leading perhaps because of the mostly poor alternatives ;)"
    author: "Slubs"
  - subject: "Re: Developer wiki name"
    date: 2007-02-21
    body: "Skype already has a Skype Garage for all kinds of garagework announcements that would be hard to read for non-tech readers of their webpage."
    author: "mrp"
  - subject: "Re: Developer wiki name"
    date: 2007-02-21
    body: "Skype already has a Skype Garage for all kinds of garagework announcements that would be hard to read for non-tech readers of their webpage.\n\nhttp://share.skype.com/sites/garage/"
    author: "mrp"
---
<a href="http://nupolls.com/snippet/30229/">Vote for the name</a> of the <a href="http://developernew.kde.org/Projects/NamingTheWiki">new KDE developer and sysadmin wiki</a>. *** Nathan Sanders reveals that <a href="http://www.linux.com/article.pl?sid=07/02/01/1935238">KDE 4's Sonnet will turbocharge language processing</a> at Linux.com. *** Trolltech <a href="http://www.trolltech.com/company/newsroom/announcements/press.2007-02-02.1856542381">announced the first beta release</a> of <a href="http://www.trolltech.com/developer/downloads/qt/qtjambi-beta">Qt Jambi</a>, now available for testing and feedback. *** <a href="http://chainlink.sourceforge.net">ChainLink</a> is a new Qt 4 integrated environment for scientific data analysis and visualisation using Matlab/Octave/Scilab compatible syntax.  Version 0.5.3 was released last week and the author is looking for some talented developers. *** CSS3.info blogs that <a href="http://www.css3.info/khtml-356-is-the-most-css3-compliant-of-all/">Konqueror is the most compliant browser for CSS</a>.  "<em>The latest release of the KHTML rendering engine passes all of the tests in our CSS selector testsuite - making the Konqueror 3.5.6 browser the most CSS3-compatible of all.</em>"


<!--break-->
