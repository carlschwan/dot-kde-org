---
title: "KDE Commit-Digest for 20th May 2007"
date:    2007-05-21
authors:
  - "dallen"
slug:    kde-commit-digest-20th-may-2007
comments:
  - subject: "Qt4 based Wikipedia Offline Reader?"
    date: 2007-05-21
    body: "Any idea where to find it?\nI found that with google:\nhttp://www.kdedevelopers.org/node/984\n\nbut no code."
    author: "Patcito"
  - subject: "Re: Qt4 based Wikipedia Offline Reader?"
    date: 2007-05-21
    body: "The original client called \"Knowledge\" seems dead to me although this is not reflected on the wikipedia page (http://de.wikipedia.org/wiki/Benutzer:Danimo/Knowledge).\n\nBut see http://woc.fslab.de/woc/wiki \nThis is the university project that Daniel Molkentin has been involved with. \n(The mailing list mentioned there is in German though.)\n\n"
    author: "cm"
  - subject: "Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: "This is a bad thing! I really like it and propose it for all kde apps"
    author: "zvonsully"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: "I love it too - it was a classic \"wow\" moment when I first encountered it, circa KDE 3.3.\nHowever, the reasoning behind the removal is timer wakeups, which are hefty culprits in processor (and therefore energy) usage, especially on laptops.\n\nA decision therefore needs to be made on the balance of power usage vs. eye candy, saving the planet vs. saving our eyeballs :)\n\nIt would be nice for these eye candy features to follow a KDE-wide setting though, one which could be dynamically manipulated by Solid power controls.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: "Yes, a power safe/feature slide sounds like a good idea to me, uses Solid in a good way I'd think. A KDE to have such power saving feature scheme somewhere? Or is this too vague a feature?"
    author: "ac"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: "This could be an Optional feature that is turned off by default...??"
    author: "Emil Sedgh"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: ">  I really like it and propose it for all kde apps\n\nIt was an absolutely ghastly hack however, and in its current state it causes Kopete to wake up every 30ms ( that is > 33 times per second ) just to pretend that the scroll bar is being pressed.\n\nThat has quite an impact on the power usage of mobile PCs, and even on non-mobile PCs it will have an impact on the performance of other applications.  \n\nSo unless a proper implementation is forthcoming, it was decided to remove it."
    author: "Robert Knight"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: "Pity, but I can understand those reasons. I wonder, though, if we will ever see smooth scrolling (not the automatic scroll as in Kopete) in KDE. It's more usable as you can follow content with your eyes, and it looks better as well. Besides a suse hack to have smooth scrolling in Konqi, I haven't seen it much. \n\nThough the Domino style has smooth scrolling on all scrollbars, so it must be possible..."
    author: "Superstoned"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: "Personally, smooth scrolling in Konqueror is the one of many thing I disabled. Maybe it's just me, but I feel uncomfortable (giddy) looking at it for a prolonged time."
    author: "me"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: "Hmm, it was actually one of the things I really loved. Each time I move the mousewheel over the scrollbars it makes me itch, because it still scrolls with fixed steps there."
    author: "Diederik van der Boor"
  - subject: "No! Smooth scrolling moved to KStyle?"
    date: 2007-05-21
    body: "Well, I have seen the Domino style for Kde3.5 which implements smooth scrolling for ALL apps. Interestingly, it did not work with Kopete at all where both smooth scrolling implementations produced conflicts (not scrollable at all).\n\nI'd rather like to see the Domino technology (and not only the smooth scrolling feature) in the KDE4 style base class to enable it for all apps...."
    author: "Sebastian"
  - subject: "Re: No! Smooth scrolling moved to KStyle?"
    date: 2007-05-24
    body: "The Kopete smooth scrolling is far better than Dominos."
    author: "Luis"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-21
    body: "Yurgh.  Couldn't stand it (and quickly disabled it). I'd much rather instantly scroll to where I want to go.  Smooth scroll felt floaty and imprecise, and the small amount of time it took to get up to speed and then slow back down to a stop when scrolling irked me to no end."
    author: "MamiyaOtaru"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-22
    body: "Completely agree. Maybe it would be ok if it was faster or used a more sensible interpolation function, but currently it just makes scrolling more tedious."
    author: "Tim"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-22
    body: "Another nod of agreement from this corner. Any small benefit gained from being able to trace the scrolling content with my eyes more easily was negated by the time wasted in waiting for the smooth scrolling to do its thing.\n\nFurthermore, I find the step size is generally small enough for losing track of content to be a non-issue, but I dare say this varies from person to person.\n\nAll that said, however, if a proper smooth scrolling implementation came about, and was strictly *optional* (even if it was enabled by default), I don't see how this would be anything but the best of both worlds. Sometimes you *can* please everybody... or almost everybody. :P"
    author: "Jeff Parsons"
  - subject: "Re: Removed smooth scrolling in Kopete"
    date: 2007-05-22
    body: "Personaly, I use a laptop and usualy don't get a moose whith a scrollweel, so this was realy usefull\n\nBut I have to agree that when you have the scrollweel, this thing is more anoying than good."
    author: "kollum"
  - subject: "Plasma and widgets"
    date: 2007-05-21
    body: "First of all, congratulations to all plasma developers for the first mentioning in commit-digest ! This may finally calm down all that angry unbelieving trolls ;)\n\nI've taken a look at plasma source and wiki pages and still have one question about plasma design: why this rewriting of standard QT QWidgets is necessarily ? Rewritten copy will deffinitely have less functionality than the original one and will possibly have small differences in behavior that will annoy both programmers and users. Yes, QGraphicsView is really great, but according to several blog posts on the planetkde you still can use QWidgets on it. There is some issues with scaling, but in current implementation plasma widgets are drawn using KStyle which is resolution dependant and still has the same problems with scaling.\n\nJust my thoughts, it'll be great if someone could clarify this here or on the wiki. And again thanks for all your wonderful work !"
    author: "Anonymous"
  - subject: "Re: Plasma and widgets"
    date: 2007-05-21
    body: "you said you read the blogs about it, probably referring to the ones by andreas at trolltech. if so, they answered your questions. it's not just about resolution independence, but stacking orders, graphical effects and other transformations.\n\nand hopefully we won't need our own copies of these things for long.\n\n> deffinitely have less functionality than the original one \n\npushbuttons and checkboxes aren't that hard to do. it's not like we're going to be implementing treewidgets and what not.\n\n> and will possibly have small differences in behavior that \n> will annoy both programmers and users\n\ngiven that this isn't meant to be used to write full applications, i highly doubt that. these are simple analogs that are good enough to appear like they belong and fit and are familiar to the user. i doubt most users will even notice and given what applets tend to do, i doubt developers will even care.\n\nit's also temporary; we'll probably be able to replace all this code with qt 4.4 or 4.5, depending on schedules at Trolltech."
    author: "Aaron J. Seigo"
  - subject: "Re: Plasma and widgets"
    date: 2007-05-21
    body: "> you said you read the blogs about it, probably referring to the ones by\n> andreas at trolltech. if so, they answered your questions\n\nI've just reread Andreas' blogs and yes, now the answer is more clear for me.\n\n> it's also temporary; we'll probably be able to replace all this code with qt\n> 4.4 or 4.5, depending on schedules at Trolltech\n\nIt's quite good, I'm looking forward to see how Trolltech will solve this !\n\nThanks a lot for you reply !\n\nAnd one more question: will the plasma widgets support stylesheets (which can be quite tasty for applets) ?\n"
    author: "Anonymous"
  - subject: "Re: Plasma and widgets"
    date: 2007-05-22
    body: "> will the plasma widgets support stylesheets\n\ni'm really not sure at this point. right now they don't, and i'm not sure that's a bad thing. conversely, i'm nearly positive it's not worth the effort for kde 4.0 if we're going to get it for free in qt 4.4 / kde 4.1."
    author: "Aaron Seigo"
  - subject: "C and C++ icons"
    date: 2007-05-21
    body: "Nitpick: The C and C++ mimetype icons look like they say \"exit 0;\" at the bottom, instead of \"return 0;\" or \"exit(0);\"\n\nhttp://commit-digest.org/issues/2007-05-20/moreinfo/665814/#visual\n\nAm I crazy?"
    author: "AC"
  - subject: "Re: C and C++ icons"
    date: 2007-05-21
    body: "It should also say 'int main(......' and being pedantic 'return EXIT_SUCCESS;'"
    author: "Anonymous"
  - subject: "Re: C and C++ icons"
    date: 2007-05-21
    body: "return 0 and return EXIT_SUCCESS mean the same thing in C (although I suppose they may signal different \"successes\", whatever that might mean).\n\nvoid main, though, is just plain bad."
    author: "KDE User"
  - subject: "Re: C and C++ icons"
    date: 2007-05-21
    body: "yes, but ur observation is accurate none the less"
    author: "unknown anonymus coward"
  - subject: "KDE Control Center??"
    date: 2007-05-21
    body: "GOOD WORK!!!...\n\nPlasma and other module like change radically, but how about KDE Control Center? i never hear that KDE control center will change...., hope the next KDE 4, KDE control center will change radically too..\n\n"
    author: "Teddy"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-21
    body: "given that kcontrol doesn't even work (e.g. list and load control panels) right now, i wouldn't be surprised if exactly that happened.\n\nif it weren't for the horribly 3rd level deep nesting mess that results from our current array of panels, i'd happily suggest system settings at this point. it could use some love and improvement, but it's not a bad place to start =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-21
    body: "Are there any drafts of what is wanted for KControlCenter or how the new version should look like?\n\nEarlier there were proposals of a KDE registry? Elektra seems to be dead, right?\n\nI really hope we will see some cleanups in the wallpaper field. The Enlightenment  project shows how to make a good impressions with high-res visuals. "
    author: "Andre"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-21
    body: "Please no registry imbecility in KDE. Thank you.\n\nKControlCenter does need to be better organized though. Like getting rid of tabs and instead having more than two levels in the tree on the left. Would be a vast improvement IMHO."
    author: "aha"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-21
    body: "Yeash, perfect, a 5 levels nested tree! so to reach your option you have to click-click-click-click-etc-etc\n\nI think that the best approach is the OSX/Kubuntu one, although it needs some cleanup (and some configuration panels could even disappear)"
    author: "Davide Ferrari"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-21
    body: "Tee Hee!\n\nInvariably the configuration panels missing in Kubuntu are the ones I needed to get to.\n\napt-get install kcontrol provided the fix.\n\nPlease please please don't force me to edit configuration files. It has already happened once in KDE. And not with something trivial. Anyone who thinks googling something a number of times to first find what the developers call the thing, then the way to change it is easier than clicking on a few buttons to get to a nested configuration pane should try it a few times.\n\nDerek"
    author: "D Kite"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-21
    body: "And what are these panels missing in Kubuntu?? Really, I didn't even noticed something was missing (and I'm asking seriously, I'm curious now :)"
    author: "Davide Ferrai"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-22
    body: "I've noticed. Confused me for ages. I can't remember what they took out but they definitely didn't have all the modules kconfig does (or at least they didn't in the last version)."
    author: "Tim"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-22
    body: "Agreed. Taking out panels is not the solution. It's just Murphy's law. Other than that, the layout in Kubuntu is rather nice-looking.\n\nWhat should be more strongly emphasised in both the regular control panel and in the Kubuntu one is search. It already works really well, and could work even better if a conscious effort was done to add more (hidden) key words to the panels.\n\n(Search works less well in the Kubuntu case, because the dimming of icons that don't match the search terms is way too subtle. I have about a 50% chance of guessing whether an icon is dimmed from looking at it, and I don't even have any disabilities. From an accessibility standpoint the search feature is useless in the Kubuntu panel.)"
    author: "Martin"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-22
    body: "personally I hate the kubuntu thing.\nI suppose it is more usable, though, for everyone except me. :)"
    author: "Chani"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-22
    body: "It's not only you, we are at least two.\n\nI dislike the way it works, but the worst thing by far is the removal of options. Hunting removed settings I know should be there, are beyond annoying."
    author: "Morty"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-22
    body: "KRunner in KDE 4 searches through control panel modules pretty nicely.  I use it so frequently when I'm writing my KDE 4 articles that I pretty much forget that the Control Centre is borken.  Really - I just type 'fonts' into the run dialog, and one of the results is sure to be that config module.  Pretty Slick."
    author: "Troy Unrau"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-22
    body: "I can't find the option to disable the annoying bouncing cursor for example. :(\n(To be honest I haven't really looked for it. I searched for \"feedback\" once but it returned 0 results)"
    author: "Lans"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-22
    body: "> Yeash, perfect, a 5 levels nested tree! so to reach your option you have to click-click-click-click-etc-etc\n\nYes, the exact same number of clicks you make to get to the same screen today, be it two, three, four, or five.\n\nActually two or three normally. Anything beyond that is for very advanced or expert stuff.\n\nOnly the interface is more consistent, and with less clutter, because tabs are visually much messier than nodes of a tree.\n\nBut improving the logical grouping, presentation and consistency of all settings is what matters the most. It takes a lot of care and thought.\n\nAs always, what is the easiest to use is not the easiest to implement."
    author: "aha"
  - subject: "Re: KDE Control Center??"
    date: 2007-05-22
    body: ">the logical grouping, presentation and consistency of all settings is what matters the most\n\nExactly, if it's two or ten clicks/levels does not matter as long as it's presented in a logical and consistent way. Force all kinds of tricks to minimize levels or number of clicks is just silly, if a increasing the number of levels would give you a more logical interface.\n\nBetter to make the most consistent and logical interface possible without imposing such artificial rules. Let the workflow and data decide."
    author: "Morty"
  - subject: "Re: KDE Control Center?? --> webinterface"
    date: 2007-05-22
    body: "i kind of hope that kcontrol could become a lightweight app with mainly KHTML wiget enabling the editing of system setting though a webinterface (maybe spiced up with some small kparts for stuff that will not fit in a webinterface).\n\npopups should be forbidden.\n\nwebinterfaces are more 'scrollable', could easily allow 3rd-party/cross-platform extensions, allows wizard style configuration (like for network connections), a little themeability, etc. etc. etc.\n\nthese setting-web-pages would have to be written in a dynamic programming language (ruby, python, KJS). yet they have to hook onto the KDE translation framework.\n\nam i saying something ridiculous? (sorry)\n\n_cies.\n\n"
    author: "cies breijs"
  - subject: "Re: KDE Control Center?? --> webinterface"
    date: 2007-05-22
    body: "Well, it sounds nice, but we currently have a framework for configuration, the config modules (you can invoke a simple config shell with the commandline tool 'kcmshell', try it). So we're only talking about how and where to present these modules, NOT the layout and configuration itself (which depends on the modules).\n\nYou can easilly have extensions to this, just introduce a new kcm module.\n\nThemability is KDE-wide, we DON't want applications to theme themselves (have a look at Kopete in this regard, it is, like every KDE app, very flexible, but DOESN't have any useless Kopete-specific skinnable stuff). Of course icons etc are OK."
    author: "superstoned"
  - subject: "Re: KDE Control Center?? --> webinterface"
    date: 2007-05-23
    body: "regarding themeability i thought more of a kopete-style like theme then a widget-style like theme.\n\nanyway it was just an idea.\n_c."
    author: "cies breijs"
  - subject: "KDE universal"
    date: 2007-05-21
    body: "Marble / Digest quote:\n\n\"Don't expect wonders though - you won't be able to spot your house any time soon. However as you might have noticed, that's not Marble's ultimate goal :-) See the FAQ in the docs for more info.\"\n\nWhat are you talking about? One day soon KDE will launch their own GPL3 satelites and we will have fully controlled support for this too! ;) Gotta support your KDE Mars users too! KDE universal!"
    author: "ac"
  - subject: "Re: KDE universal"
    date: 2007-05-21
    body: "first konqi in space? ;)"
    author: "Aaron J. Seigo"
  - subject: "Karamba"
    date: 2007-05-21
    body: "Please, please, please... variables or other mechanism to redraw karamba\nonly when changes happens, i.e.: for amarok music cover image, but also\nbackground images or icons needing to be redrawed too often and also text.\n\nActually SuperKaramba is using Much more CPU power than kopete smoothscroll."
    author: "mattepiu"
  - subject: "Re: Karamba"
    date: 2007-05-21
    body: "Currently, yes, but that's mostly due to the horrible way it's transparancy works. I'm pretty confident that the KDE 4 plasma way will be much more efficient..."
    author: "superstoned"
  - subject: "Re: Karamba"
    date: 2007-05-22
    body: "Transparency in trunk is already a lot better and thus loads of repaints are no longer needed.  Which naturally means its a lot smoother and less power hungry.\n\nThis also shows a really good reason why it takes longer than the patience of some people in the audience to create this thing.  We need to work on general graphics features before its useful to build a beautiful plasma and thus it takes longer before there is something visible.\nPlasma just is an appartment flat building that is started 10 stories underground and thus takes a bit of time before it starts to look like something real is happening.\n\nThanks for the wait!  It will be worth it :)"
    author: "Thomas Zander"
  - subject: "Re: Karamba"
    date: 2007-05-21
    body: "Redrawing is still a problem with SuperKaramba. Unfortunately the problem is a bit more complex. Redrawing a meter only when the sensor data is changed is a good idea, but unfortunately reading the sensor value takes almost the same amout of time than the drawing. So half of the time of the update process is already wasted with updating sensors.\n\nAuthors of system monitor themes tend to refresh the themes quite often, meaning that sensor values will often change, like the cpu load changed from 1 to 2%. And this means that we have to redraw very often.\n\nWe also have the problem that some themes force to redraw the widget to dofor animations. Unfortunately the way animations needed to be done in former SK versions doesn't work very well with the QGV, meaning we have to redraw even more in this situation.\n\nOn the positive side fake transparency is gone now, so bigger themes should be much faster now. The move to QGV introduced some performance problems but also gives us the opportunity to do some nice eye-candy in the future and perhaps even integration into Plasma.\n\nIf you have more comments, wishes or bugs feel free to join us at #superkaramba at irc.freenode.org"
    author: "wirr"
  - subject: "Re: Karamba"
    date: 2007-05-21
    body: "Thanks for that detailed and professional answer.\n\np.s. SK rocks :)"
    author: "Sebastian Sauer"
  - subject: "Re: Karamba"
    date: 2007-05-22
    body: "[i]On the positive side fake transparency is gone now, so bigger themes should be much faster now. The move to QGV introduced some performance problems but also gives us the opportunity to do some nice eye-candy in the future and perhaps even integration into Plasma.[/i]\nI thought SK would practically BE Plasma?!? Can you explain the relationship between these two?"
    author: "superstoned"
  - subject: "Re: Karamba"
    date: 2007-05-23
    body: "Copy and pasted comment from another article on the dot:\n\nBy Emil Sedgh\n\n\"SuperKaramba was a great Idea.but its current implementation is not so great.\nKicker is working good, but creating applets for it is hard and its just a taskbar.nothing more.\n\nso the plasma idea is to merge these.having Superkaramba's cool idea's and eyecandy (and easy karamba creation) with kicker's good implementation on a great desktop.Karamba's are currently doing a few things.controlling AmaroK, showing CPU Usage and a few others.Plasmoids should do more.you could find some original mockups in svn.\""
    author: "Lans"
  - subject: "Plasma - User-visible changes"
    date: 2007-05-21
    body: "I searched, but I wasn't able to find it. Are there any screenshots or images for this commit ?\n\nThanks\nFelix"
    author: "Felix"
  - subject: "Re: Plasma - User-visible changes"
    date: 2007-05-21
    body: "They're not in for this week, see PlanetKDE.org, or the direct link: http://aseigo.blogspot.com/2007/05/clockwork-plasma.html\n\nAnd notice the last sentence in the commit digest:\n\n> This week saw the triumphant arrival of user-visible work in the Plasma\n> interface effort. The digest of next week, issue 60, will feature more\n> on these developments."
    author: "Diederik van der Boor"
  - subject: "Re: Plasma - User-visible changes"
    date: 2007-05-21
    body: "Ah, thanks. I thought \"more on these developments\" means there is at least one image ;)"
    author: "Felix"
  - subject: "Re: Plasma - User-visible changes"
    date: 2007-05-21
    body: "You can compile kdebase from SVN and see shiny new clock widget yourself ;-)"
    author: "Anonymous"
  - subject: "Re: Plasma - User-visible changes"
    date: 2007-05-21
    body: "The developers spent much of yesterday fixing odd little rendering glitches that tend to show up after the first fully functioning applet is completed.  So it missed the digest.  That said, if the first images you folks saw had rendering problems, you'd all be out trolling in arms, so I think Danny and co. made the right choice.\n\nCheers"
    author: "Troy Unrau"
---
In <a href="http://commit-digest.org/issues/2007-05-20/">this week's KDE Commit-Digest</a>: User-visible functionality added in <a href="http://plasma.kde.org/">Plasma</a>. Support for animated SVG images in <a href="http://netdragon.sourceforge.net/">SuperKaramba</a>. <a href="http://edu.kde.org/kanagram/">Kanagram</a> becomes the latest application to adopt a scalable, SVG-based interface. Initial code imported, as a statement of intention, to support interaction with Exchange servers and the <a href="http://pim.kde.org/akonadi/">Akonadi</a> PIM data store. Small, incremental improvements in <a href="http://ktorrent.org/">KTorrent</a>. A new round of <a href="http://scan.coverity.com/">Coverity</a> fixes, particularly in <a href="http://koffice.org/">KOffice</a> and <a href="http://amarok.kde.org/">Amarok</a>. Work on loading ODF shapes through <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a> in KOffice. <a href="http://www.kdevelop.org/">KDevelop</a> gets improved support for .ui (user interface layout) files. Branches of <a href="http://kontact.kde.org/kmail/">KMail</a>, KPPP, <a href="http://konversation.kde.org/">Konversation</a> and <a href="http://kopete.kde.org/">Kopete</a> created to enable the integration of <a href="http://solid.kde.org/">Solid</a>-based connection management and notification. KDE 3.5.7 is tagged for release early next week.

<!--break-->
