---
title: "KDE Commit-Digest for 23rd December 2007"
date:    2007-12-27
authors:
  - "dallen"
slug:    kde-commit-digest-23rd-december-2007
comments:
  - subject: "Thanks!"
    date: 2007-12-27
    body: "Thank you, Danny, for yet another nice issue of our weekly digest.\n"
    author: "christoph"
  - subject: "Re: Thanks!"
    date: 2007-12-27
    body: "You have no idea how perfect his timing was for me ;-)"
    author: "jospoortvliet"
  - subject: "Re: Thanks!"
    date: 2007-12-27
    body: "+1!"
    author: "mactalla"
  - subject: "Re: Thanks!"
    date: 2007-12-27
    body: "Have to save money for myself, but thank you very much! I read the digest for more then a year now and it was for me the start for looking at KDE4. Thank you so much :D"
    author: "BartOtten"
  - subject: "Re: Thanks!"
    date: 2007-12-27
    body: "++\n\nDonated a small amount too - you certainly deserve a new lappy :)"
    author: "Joergen Ramskov"
  - subject: "Re: Thanks!"
    date: 2007-12-28
    body: "Donated a few bucks.  Every bit helps!"
    author: "T. J. Brumfield"
  - subject: "Wagon Slayer?"
    date: 2007-12-27
    body: "Hmm.  I like dragons as much as the next guy (probably more, actually) but... Dragon Player?  Does it only play movies with dragons in 'em?  Will it also run my old Dragon 32 games? ;)\n\nIt's easier to spell than Codeine; I'll say that for it :)\n\nHow about \"Cinemagic\" or something?  It's overused in the world of Film festivals, but not yet for a movie player, as far as I know.  \"Flicks\" (it has a K ;) ?  \"Visionary\"?  \"Visionaire\"?  \"Any other word plus 'dragon' in a really small font size\"? ;)\n\nSorry, I'm not being very serious here.  Dragon Player is OK, but it really could be better ;)"
    author: "Lee"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-27
    body: "Cinemagic is already copyright MomCorp, along with \"love\".  The only words that have not yet been copyrighted or trademarked are \"Popplers\" and \"Tasticles\"."
    author: "Level 1"
  - subject: "poppler - that'll be our PDF rendering library"
    date: 2007-12-27
    body: "Taken - see http://poppler.freedesktop.org.\n\nNot really \"copyrighted\" (well, we borrowed it :-), but still in use."
    author: "Brad Hards"
  - subject: "Re: poppler - that'll be our PDF rendering library"
    date: 2007-12-28
    body: "Damnit, someone is moments away from registering tasticles.com"
    author: "T. J. Brumfield"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-27
    body: "Heh I've been open to ideas for about 9 months*. Not that I would've gone with \"Cinemagic\". ;)\n\nAnyways dragons burninate peasants, generally kick ass and of course (and seriously) Konqui is our long-time mascot."
    author: "Ian Monroe"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-27
    body: "Well, \"player\" is descriptive and dragons are powerful and something worth looking at, so I guess it's a decent name for a video player."
    author: "Maarten ter Huurne"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-27
    body: "Flik would have been nice."
    author: "reihal"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-27
    body: "I agree. Dragon Player's not so bad though. At least it isn't Drakon or something equally contrived!\n\nAlso I think Konqui (sp?) makes KDE seem unprofessional - Kubuntu for example removed his image from the shutdown dialog. Maybe he should grow up into a more cool evil dragon!"
    author: "Tim"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-28
    body: "...for a Flickr uploader."
    author: "Ian Monroe"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-27
    body: "Actually I like the name \"Dragon Player\". It's original, could be turned into a brand. And dragons refers to Konqi.\n\nIf someone said their music player could be called \"Amarok\", would you like that name? We all like Amarok now. Part of liking it comes from being a brand you can identify."
    author: "Diederik van der Boor"
  - subject: "Amarok"
    date: 2007-12-27
    body: "I've always liked Amarok as a name, it's unique yet the term 'rok'  gives away its purpose."
    author: "Dave Taylor"
  - subject: "Re: Amarok"
    date: 2007-12-27
    body: "So how about \"Jibbleviewfilm\"?"
    author: "Anon"
  - subject: "Re: Amarok"
    date: 2007-12-28
    body: "Or how about fuck you?"
    author: "Stefan Monov"
  - subject: "Re: Amarok"
    date: 2007-12-28
    body: "For a video player? No, sorry - I don't see it.\n\nMaybe if it were optimised for porn ... ?"
    author: "Anon"
  - subject: "Re: Amarok"
    date: 2007-12-28
    body: "KoMedian"
    author: "anon"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-27
    body: "'Dragon Player' is awful, it sounds like a Taiwanese board game or a Chinese DVD player for children covered in dragon decals.\n\n'Ciniverse' FTW do I have any takers for 'Ciniverse'."
    author: "Dave Taylor"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-27
    body: "I must say though even if the atrocity of a name 'Dragon Player' is used but is still Codeine at heart (basically a bugfree version). Then it'll still be perfect for the task - Amarok is great but I see they are seriously on the precipice of bloat canyon and there's no way back apart from a sane fork."
    author: "Dave Taylor"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-28
    body: "Well Dragon Player is developed by the same people as Amarok, so who knows!"
    author: "Ian Monroe"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-28
    body: "And yet it is, more or less, entirely unlike Amarok in every way.  And both apps are great :)"
    author: "Leo S"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-28
    body: "Hey, what about choosing \"Drak\" as name for that player? I don't like the name \"Dragon Player\" very much, it sounds awful. \"Drak\" is the Czech word for Dragon. It has even got a K in it, and it fits perfectly into the naming of other KDE apps (Especially Amarok :-D)."
    author: "blueget"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-28
    body: "That's a good name!\nDragon Player is not awful, it just has something... maybe player is too generic, and Dragon Player sounds, like someone else said a bit like a tablecard game or something.\n\nI am not a fan of K-names, but please consider Drak."
    author: "tobami"
  - subject: "Re: Wagon Slayer?"
    date: 2007-12-30
    body: "I really dislike some of the new K-less names, such as Dolphin, Plasma and now Dragon Player and I think these names could lead to a loss of identity for KDE. Remember the time when it used to be all about the K?"
    author: "Teo"
  - subject: "Re: Wagon Slayer?"
    date: 2008-01-03
    body: "Still I can't imagine a Kolphin or a Klasma. Maybe they ran out of K names like Ubunzu will soon run out of monkey names ;D"
    author: "Bobby"
  - subject: "another reason to use webkit"
    date: 2007-12-27
    body: "> fix crap-tastic wiki applet. it's so bad i don't even care anymore, as we need Qt4.4/Webkit for it to actually do anything useful.\n\nThankfully it will be there by KDE 4.1, thank you trolltech!"
    author: "Paul"
  - subject: "Re: another reason to use webkit"
    date: 2007-12-27
    body: "Amarok 2 is probably going to require Qt 4.4, regardless of whether KDE 4.1 is out yet or not. Granted its pretty likely that KDE 4.1 will be out by then."
    author: "Ian Monroe"
  - subject: "Re: another reason to use webkit"
    date: 2007-12-27
    body: "Anyone knows when Amarok2 and KDE 4.1 will be release more or less?"
    author: "Patcito"
  - subject: "Re: another reason to use webkit"
    date: 2007-12-28
    body: "2008"
    author: "Ian Monroe"
  - subject: "Re: another reason to use webkit"
    date: 2007-12-27
    body: "yet another \"khtml VS webkit\" thread... \n*yawn*\nDon't feed the trolls please. (unless they are Norwegian)"
    author: "Shamaz"
  - subject: "Re: another reason to use webkit"
    date: 2007-12-27
    body: "yeah, it's good you mention that - Norwegian Trolls should be fed properly. We owe them a lot ;-)"
    author: "jospoortvliet"
  - subject: "Re: another reason to use webkit"
    date: 2007-12-27
    body: "Agreed. I'll pass this one. :-)"
    author: "Richard Van Den Boom"
  - subject: "3 donations? +1"
    date: 2007-12-27
    body: "All I can say? Damn, that is the least conspicuous \"Make a Donation\" button I've ever seen. Danny, whoever you are, you help keep me up to date on the KDE world. I appreciate it. (I read the Planet too, but it doesn't cover the Summer of Code projects, etc.) It took a while to figure out what $5 USD is in British pounds, but it's yours. Merry Christmas! The way I reckon it, all the Digests I've read surely equate to at least one issue of a magazine. (My brother is starting his own magazine, which has made me even more appreciative of the time and effort you must spend aggregating interviews together for the Digest.)"
    author: "kwilliam"
  - subject: "Re: 3 donations? +1"
    date: 2007-12-27
    body: "Dont' get me wrong , i dont want to take this political,\nbut Dollars are nearly nothing worth here in Europe :-)\n\nenter in google : \"5 dollar in pound\""
    author: "usd"
  - subject: "Re: 3 donations? +1"
    date: 2007-12-28
    body: "Yep, the dollar to pound rate right now is horrible."
    author: "T. J. Brumfield"
  - subject: "Re: 3 donations? +1"
    date: 2007-12-30
    body: "You mean +2 ;) It's not much, but everything I can spare this month, hope you'll get some (K) Hot New Stuff soon ;)"
    author: "DeeJay1"
  - subject: "Clockwork"
    date: 2007-12-27
    body: ">> ... and the train clock bla in Plasma\n\n:-)) Yea, this is so cool, thanks for listening to your devote followers...\nThat made my day"
    author: "Marc"
  - subject: "Re: Clockwork"
    date: 2007-12-27
    body: "I won't be happy until we have a steampunk-ish Nixie Tube clock ;)\n\nhttp://en.wikipedia.org/wiki/Nixie_tube"
    author: "Anon"
  - subject: "Re: Clockwork"
    date: 2007-12-27
    body: "+1 ;-)"
    author: "jospoortvliet"
  - subject: "Re: Clockwork"
    date: 2007-12-27
    body: "Not nerdy enough. I want a binary clock with red LEDs.\nhttp://en.wikipedia.org/wiki/Binary_clock"
    author: "reihal"
  - subject: "Re: Clockwork"
    date: 2007-12-27
    body: "yeah the binary clock ! the great missing feature of 4.0 !"
    author: "Cyrille Berger"
  - subject: "Re: Clockwork"
    date: 2007-12-27
    body: "What would you digital-heads know about old school anyway :)\n\n"
    author: "taurnil"
  - subject: "Re: Clockwork"
    date: 2007-12-27
    body: "There's a binary clock in playground/base/plasma/applets/binary-clock. \n\nIt doesn't use red LEDs though right now, but I'm sure you can whip up a patch easily enough to make it the nerd's ultimate wet dream.\n\nOw, and Cyrille, check your facts :P"
    author: "sebas"
  - subject: "Re: Clockwork"
    date: 2007-12-28
    body: "I mean a real binary clock, like this one:\n\nhttp://www.abulsme.com/binarytime/\n\nand not a girlie one like this:\n\nhttp://www.glassgiant.com/geek/binaryclock/binary_clock_flash.swf"
    author: "reihal"
  - subject: "Re: Clockwork"
    date: 2007-12-28
    body: "I'm very happy to see it return!"
    author: "T. J. Brumfield"
  - subject: "online desktop/syncro/conduit"
    date: 2007-12-27
    body: "hi,\ni recently found a great tool for gnome, BUT completely gui-independent, which is really great for syncronization. with this tool, you easyly could upload/save your pictures on flickr, syncronisize with directories, files, gadgets, webpages etc. (http://www.conduit-project.org/)\ncheck out the screencasts. it is very impressive.\nafter having seen the screencasts, i wonder if kde will ever integrate something like that. i mean, it is gui-independent, you \"only\" have to write an ui or integrate it into the \"save-dialog\". it would also allow things to save and store in the internet, comparable to the gnome online desktop.\nwhat do you think? is something similar to this coming?"
    author: "joni"
  - subject: "Re: online desktop/syncro/conduit"
    date: 2007-12-28
    body: "What about creating kio-slaves out of this? I think that would be the best solution. "
    author: "blueget"
  - subject: "I don't understand..."
    date: 2007-12-27
    body: "This is a kind of inconsistent. There was a call to developers to rename apps so that their names reflect their functionality. And now we have a Video Player named Dragon? I don't understand it. And I don't really like it at all."
    author: "mkrs"
  - subject: "Re: I don't understand..."
    date: 2007-12-27
    body: "Where's that call you're referring to?\n\nAnd as it is with names, one tends to judge their quality from one's own experiences (or 'framing'), so if you like Dragon Player, you might appreciate its name at some point as well. Self-fulfilling prophecies, that's what we do. :-)\n\nWhat is _much_ more important than the name (that got so many reactions in the first place) is that we'll shortly have a KDE4 port of my favorite videoplayer, and a showcase for Phonon. Thanks eean, you rock!"
    author: "sebas"
  - subject: "Re: I don't understand..."
    date: 2007-12-27
    body: "Kickoff mitigates possible confusions by showing the generic category by default and the specific name only on mouseover."
    author: "ivanonymous"
  - subject: "Re: I don't understand..."
    date: 2007-12-27
    body: "My favourite video player (Kaffeine) won't make it for KDE 4.0 unfortunately. Kplayer isn't bad but I miss Kaffeine's GUI. I will take a look at Dragon Player, maybe it can serve as a temporary replacement until Kaffeine is ready."
    author: "Bobby"
  - subject: "Re: I don't understand..."
    date: 2007-12-27
    body: "You could try SMplayer. It's Qt (newest version is qt4) gui for Mplayer. Only thing lacking is dvd-menus, but that's more of mplayer's fault. :("
    author: "Syzar"
  - subject: "Re: I don't understand..."
    date: 2007-12-28
    body: "Thanks, I just installed and tried it. It looks very nice and a lot better than kmplayer or kplayer but lack the dvd-menus that kaffeine has like you said."
    author: "Bobby"
  - subject: "Re: I don't understand..."
    date: 2007-12-28
    body: "Why not just use KDE 3 Kaffine in KDE4? It's compatible."
    author: "Jonathan"
  - subject: "Re: I don't understand..."
    date: 2007-12-27
    body: "lol\n\ndragon player is a pretty retarded name.  Just the name alone will make people think it is a gimmick and not a reliable player.  changing names like this will not impress the windows market for kde 4"
    author: "Richard"
  - subject: "Re: I don't understand..."
    date: 2007-12-28
    body: "Well everyone's an armchair expert. :)\nSeriously, such comments are pretty pointless. When the Firefox name was suggested for a then-unknown internet browser, it was scorned and ridiculed to no end by many. The rest, as they say, is history."
    author: "Darryl Wheatley"
  - subject: "Re: I don't understand..."
    date: 2007-12-28
    body: "Not to mention the Wii. :)\n\nI think Sebas is right, if the Wii was stupid we would probably still be making fun of the name."
    author: "Ian Monroe"
  - subject: "Re: I don't understand..."
    date: 2007-12-28
    body: "I love dragons, and calling it dragon PLAYER does make it obvious what the app does.  Some app names give me absolutely no clue what the app does."
    author: "T. J. Brumfield"
  - subject: "Re: I don't understand..."
    date: 2007-12-28
    body: ">> I love dragons\n\nMe too. (I was born in the year of the dragon, but that's not the only reason).\nNow I can't wait to see a sweet Oxygen icon for this app. *drool*"
    author: "Hans"
  - subject: "Re: I don't understand..."
    date: 2007-12-29
    body: "> calling it dragon PLAYER does make it obvious what the app does\n\nNot really. It just say, it play something, but not WHAT. It could be audio or video, but also flash, a virtual maschine (remember vmware player), or just some exotic binary-format like a game boy-rom. At least, it could also be some completly different, like a tamagotchi-like dragon-game ;)"
    author: "Chaoswind"
  - subject: "Re: I don't understand..."
    date: 2007-12-30
    body: "It could also be something for young lads with oriental tattoos, who like to play the field :)"
    author: "Lee"
  - subject: "Re: I don't understand..."
    date: 2007-12-31
    body: "yes, it plays dragons."
    author: "anon"
  - subject: "Re: I don't understand..."
    date: 2007-12-29
    body: "Do you really want to know why people make such a big deal out of names? Because free software operating systems, as a platform, suck and people are always pointing at the wrong reasons for their failure, when in reality far more important issues are left unsolved. "
    author: "Gordon"
  - subject: "Re: I don't understand..."
    date: 2007-12-29
    body: "Such as?"
    author: "Paul Eggleton"
  - subject: "finding appropriate ways to authenticate trolls.."
    date: 2008-01-03
    body: "..and filter them? Like this.. StupidFilter thingie?"
    author: "eMPee584"
  - subject: "kde post 4.0.0"
    date: 2007-12-27
    body: "does the branch of kde 4.0.x will support only bug fixes, or does it accept some minor features ala kde 3.5.x\n\nfriendly "
    author: "djouallah mimoune"
  - subject: "Re: kde post 4.0.0"
    date: 2007-12-27
    body: "you might as well use vista and dump KDE, give it a try and you'd be surprised how much KDE is trying to become like Vista"
    author: "Richard"
  - subject: "Re: kde post 4.0.0"
    date: 2007-12-28
    body: "Poor trolling.  Not even remotely informed or effective."
    author: "T. J. Brumfield"
  - subject: "Re: kde post 4.0.0"
    date: 2007-12-27
    body: "I just can speculate here, but I think the 4.0.x series will only include bugfixes. The main reason why 3.5.x included new small features was because 3.6 was never planned and will never arrive, but the gap until KDE 4.0 needed to be filled. But as there are already plans how to make 4.1 rule the world i don't think such micro feature releases are necessary ;-)"
    author: "Daniel St\u00f6ckel"
  - subject: "Re: kde post 4.0.0"
    date: 2007-12-30
    body: "High exceptions for 4.1 should be killed now, Ruling the world will not happen in a release like 4.1, 4.2 or any other, ruling the world needs some more time."
    author: "Emil Sedgh"
  - subject: "Compositing, which graphics card?"
    date: 2007-12-27
    body: "I don't really know where I should ask this question, so I'll just post it here, hoping that someone knows the answer. My current videocard is a bit outdated (GeForce 2), and because kwin compositing looks so extremely amazing, I'm thinking about purchasing a new one. I don't want to spend a lot of money on a new videocard however. My primary concern is that the new card runs kwin compositing very smooth. Is anybody reading this using kwin compositing with a low end videocard? If so, which one, and does it run smooth? I really appreciate any help picking out a new card...\n\nOh, and I recently (two days ago) built kde from svn, and I have to say I'm really impressed! I even started using kde4 as my main desktop! The only thing I still miss is a kde4 akregator. Does anybody know if people are working on this? Of course I could run kde3 akregator, but compared to all the oxygen beaty it really looks ugly and out of place.\nKDE people: you rock!\n\nOh, and danny: thanks for another commit digest! I'm always looking forward to the newest digest!"
    author: "pinda"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-27
    body: "I am using an nVidia GeForce 6600 LE with 256 MB RAM and it's handling the effects very well. Though I have to confess that the Kwin 4 effects aren't yet as smooth (and as much) as those of Compiz Fusion but the integration is very good. That's one of the reasons why I will use it instead of Compiz in the very near future.\n\nAnother thing: Kwin 4 doesn't like XGL (at least on my machine), it keeps crashing if you enable XGL so you have to use xorg and enable the composite extension in the xorg.conf file. Then you will be cool ;)"
    author: "Bobby"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-27
    body: "Is this with or without the recently added (after RC2) libkwinnvidiahack? I noticed a considerable difference (without is was completely unusable, with it it gets close to compiz, which is still a bit slow for me on my current card).\n\nAnd I don't really care about all the over the top bling compiz has... I'd much rather have a well integrated, mature window manager which is subtle in it's use of compositing: a little bit of transparancy polish in the interface, and some usefull features like expose are all I want. And kwin compositing seems to deliver just that. "
    author: "pinda"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-27
    body: "It's RC2 that I am using. It has improved a lot over RC1 but is still not very \"smooth\". It's quite stable though.\nI agree with you concerning compiz, it's really nice but some of the blings are overkill. Like you I would rather a mature window manager like Kwin 4 with well integrated features. Kwin in on the right track and it's future looks very bright on the KDE Desktop."
    author: "Bobby"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-27
    body: "It's more like nVidia drivers not liking XGL, and since it's able to do compositing directly in the driver XGL it's not needed. Besides XGL was newer a good idea anyway:-)"
    author: "Morty"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-27
    body: "Like I said, XGL works just fine with Compiz using the nVidia driver but not with Kwin 4, which leads me to the logical conclusion that there is a problem between XGl and KWin 4."
    author: "Bobby"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-28
    body: "That's kind of correct, but it's because of your nVidia driver. I think KWin autodetects and uses the nVidia driver directly for the composition effects, I guess this creates problems when you also have XGL running. Two xservers trying to access the same hardware etc, not surprising it becomes unstable. My guess is that Compiz in your setup uses XGL rather than the nVidia driver directly, avoiding that particular problem. "
    author: "Morty"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-27
    body: "I'm running geforce 6200 / 128 mbyte ram and effects are really slow, some of them really unusable. Compiz runs at only acceptable speed (due to high resolution of 1680x1050)"
    author: "Baronek"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-28
    body: "Are you using RC2 or a more recent svn build?"
    author: "pinda"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-28
    body: "Compiz, and KDE 4 effects work nicely (no noticable lag) on my NVIDIA 6150 LE, with 1280x1024 resolution"
    author: "g2g591"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-27
    body: "Hy.\n\nI don't know exactly what my graphic card is, but it was a not so expansive one 4 years ago for my laptop, an ATI mobility radeaon M7 ( 7500 I think ? ).\nIt runs Kwin4 effects ok, even if some plug-ins don't work as expected. May be bugs on the kwin front ? don't know, but I read that bluring was not supposed to work on old cards, so I'm not that surprised."
    author: "kollum"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-28
    body: "kwin runs quite happily on my integrated intel, now that I have xorg configured properly and I'm not running kde3 in another X. :)"
    author: "Chani"
  - subject: "Re: Compositing, which graphics card?"
    date: 2007-12-28
    body: "Yeah, the integrated intel units are the best... unfortunately they aren't sold as seperate expansion cards. That would be great: nothing beats open source drivers."
    author: "pinda"
  - subject: "Intel driver outdated?"
    date: 2007-12-28
    body: "Naa, i used to see things like you, but i ran into the problem that the very nice program MANSLIDE doesn't work on intel graphic chipsets. It's all fine with nvidia and ati but the intel driver just doesn't provide some essential functions needed by the 3D image conversions that MANSLIDE uses. Some say that especially on newer intel hardware the i810 driver is pretty outdated.\n\nSo in order to let it run on my notebook it's inside a virtual machine (virtualbox), wich works fine but is a little slow."
    author: "Marc"
  - subject: "Re: Intel driver outdated?"
    date: 2007-12-29
    body: "The \"i180\" driver is outdated, the \"intel\" driver which up-to-date distributions like Fedora are shipping isn't. :-)"
    author: "Kevin Kofler"
  - subject: "Re: Intel driver outdated?"
    date: 2007-12-29
    body: "Mkay, i did'nt check what ubuntu gutsy uses as default driver for intel graphic chipsets and said it'd be \"i810\" ... Anyways, if it's \"intel\" now, the result is still like i described it :-("
    author: "Marc"
  - subject: "Daily Updates"
    date: 2007-12-27
    body: "Are we going to get a few more updates before the final release? I notice that there haven't been any updates for about a week now, don't know if it was because of Christmas and the weekend or it's a halt."
    author: "Bobby"
  - subject: "Re: Daily Updates"
    date: 2007-12-27
    body: "Whoa - what \"Daily Updates\" would these be? "
    author: "Anon"
  - subject: "Re: Daily Updates"
    date: 2007-12-27
    body: "I used to get an update of the KDE4 packages almost every day until a week ago. Since then nothing more, don't know what happend. I am using openSuse 10.3 btw."
    author: "Bobby"
  - subject: "Re: Daily Updates"
    date: 2007-12-27
    body: "Oh - I thought you were talking about Daily Commit Digests :("
    author: "Anon"
  - subject: "Re: Daily Updates"
    date: 2007-12-27
    body: "Sorry I should have expressed myself more clearly from the beginning by referring to the KDE 4 packages seeing that the article is actually the Commit Digest."
    author: "Bobby"
  - subject: "Re: Daily Updates"
    date: 2007-12-27
    body: "It's called Christmas holidays."
    author: "binner"
  - subject: "Re: Daily Updates"
    date: 2007-12-27
    body: "A week before release tagging?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Daily Updates"
    date: 2007-12-28
    body: "please don't tell me you would seriously expect people with lives and families to skip christmas just to fix a few more bugs in KDE."
    author: "Chani"
  - subject: "Re: Daily Updates"
    date: 2007-12-28
    body: "I would if I could because I had a lot of time this Christmas."
    author: "Bobby"
  - subject: "Re: Daily Updates"
    date: 2007-12-29
    body: "Hey, I fixed a Kompare build failure in the KDE SVN trunk on Dec 23, file conflicts in 2 Fedora rawhide KDE RPM packages on Dec 24 and another RPM file conflict on Dec 25. ;-)"
    author: "Kevin Kofler"
  - subject: "Re: Daily Updates"
    date: 2007-12-27
    body: "you are not alone, i asked in irc, and it seems that in germany( and the western world in general) are on holidays. "
    author: "djouallah mimoune"
  - subject: "Re: Daily Updates"
    date: 2007-12-28
    body: "Well, Its Christmas and soon new year so you should expect most people on holliday until wensday next week."
    author: "J Andersen"
  - subject: "Icons on desktop?"
    date: 2007-12-28
    body: "I had posted this ask in the last commit, but i'd like to ask it again.. When will be desktop icons ready?"
    author: "Marco"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-28
    body: "today I upgraded to a recent snapshot.\n\nThe actual state is that you get the content of ~/Desktop on your desktop, you can resize these applets, remove them.\n\nI didn't try to add an icon on the desktop yet thought, would have to be tested.\n\nOh, and plasma unfortunately recreate them at every startup it seems, in an silly big hype. Well, the detailed commit digest says the feature to layout things got added this week after the snapshot I use. I imagine it's not that much work to add a check to see if a ~/Desktop entry is already in plasma, and it will be just working."
    author: "kollum"
  - subject: "Re: Icons on desktop?"
    date: 2007-12-31
    body: "Still the same as last week.\n\nA drag to the desktop of a file or URL results in an error.\n\nFiles in the Desktop directory are shown on the desktop but the size is unstable and doesn't change on my system unless I log out and restart my KDE session.\n\nThe \"Desktop\" Font Setting still doesn't work."
    author: "AC"
  - subject: "Re: Icons on desktop?"
    date: 2008-01-01
    body: "Improvements over yesterday:\n\nDragging a file to the desktop seems to work.\n\nDragging a URL (kde.org) to the desktop no longer gives the error message, but the link doesn't work."
    author: "whoever"
  - subject: "Oxygen"
    date: 2007-12-28
    body: "Hey, nice work on oxigen since the RC2 release.\n\nThis week, mandriva upgraded it's coocker KDE4 snapshot from RC2, and Oxingen now looks sharp and smooth at the same time.\n\nThe menus looks freakingly nice when they don't draw with glitches at theyre rounded corners, but with this little vertical gradient, roud corners, sweet separation bar, and a relief blue selection, so sweet !\n\nThanks a lot. I'm begining to love oxygen (even if I already loved Icons ).\n\nOn the other side, plasma still lag behind kdesktop and kicker, and I think I will turn it of and replace with kde3 counterparts for the firsts KDE4.0.x releases... But it is improving so fast, I would give it 6 more mounths to overcome KDE3 desktop/pannel.\n\nKeep it up, and thanks Danny for your work, wich is hightly apreciated.\n\nPS : hey, this week, france get dark green again on the map, it had been a few weeks light green, I was woried my country was parting from KDE, now I'm relived ^^"
    author: "kollum"
  - subject: "Re: Oxygen"
    date: 2007-12-28
    body: "six? pfft, I give it three, at most. ;) I mean, sure, kicker and kdesktop had a bajillion features and represented years of work, but plasma's fucking insane, and code is still changing constantly during christmas fucking holidays. :)"
    author: "Chani"
  - subject: "Re: Oxygen"
    date: 2007-12-28
    body: "Damn right, plasma is indeed insane! Look how long it took to bring Kicker and Kdesktop to where they are compared to Plasma. Plasma is being developed at an amazing speed. It will be the Dragon Slayer (like somebody here used the name) among the desktops. I will give it another 6 months to start kicking some real hard butts whether their names are Aero of Aqua it doesn't matter :) \nI would only advise the KDE Team to be a bit quiet on a few things because the mighty M$ and the Big Apple are watching closely and before you know it those features will be called \"their\" innovation possible with patents."
    author: "Bobby"
  - subject: "Re: Oxygen"
    date: 2007-12-28
    body: "yap, and we all thank Aaron, the man with the plan :)"
    author: "Thomas Zander"
  - subject: "Re: Oxygen"
    date: 2007-12-31
    body: "chani, come on, take it easy, and thanks for the remove applet code."
    author: "djouallah mimoune"
  - subject: "Drak"
    date: 2007-12-28
    body: "As I already pointed out in a posting below, I want to make a suggestion for the naming of former Codeine: Drak. \n\nDrak is the czech word for Dragon, and imho it sounds much better than \"Dragon Player\". It has even got a K in the name, and I think it fits perfectly into the naming of other KDE apps, especially Amarok.\n\nSo please consider Drak as name for \"Dragon player\""
    author: "blueget"
  - subject: "Re: Drak"
    date: 2007-12-29
    body: "As I already pointed out I've been open to a name for about 8 months. And now all the ideas come out of the woodwork once I decided. Sorry, too late."
    author: "Ian Monroe"
  - subject: "Re: Drak"
    date: 2007-12-29
    body: "but maybe this the way of the future?  think of a cool name first, *then* come up with the application.  Maybe a firewall program for winblows or something.."
    author: "programming2.0"
  - subject: "Re: Drak"
    date: 2007-12-29
    body: "You didn't do any pointing here on the dot.\nToo late for what? You're not selling a product, are you?"
    author: "reihal"
  - subject: "Re: Drak"
    date: 2008-01-02
    body: "Kde-commits picked it up (and thus the dot did, I'm pretty sure it was part of the kde-commits summary), then I blogged about on PlanetKDE back in May when I started working on Codeine 2.0. At that point I named it Video Player and noted I wasn't very happy with its ungooglability."
    author: "Ian Monroe"
  - subject: "Re: Drak"
    date: 2007-12-29
    body: "I'm very sorry, but I did NOT know *anything* about this. I even barely noticed the application before this commit digest. \n\nI think all the others who are complaining about the name now also just didn't know about Codeine/Dragon Player. If nearly nobody knows you are searching a name for that app, there won't be much ideas for the naming."
    author: "blueget"
  - subject: "Re: Drak"
    date: 2007-12-30
    body: "Hi Ian,\n\nSorry if this seems to late to raise the issue.  You're doing great work with Codeine/DP, and I give you all due respect for that.\n\nThat being said, I have to point out that it's a little disingenuous to say that you've been \"open to a name for 8 months. And now all the ideas..\".  This is the first I've heard of the name change decision, like others above pointed out.  I do recall something about codeine being a temporary name, now that it's been mentioned, but that was way back when the app was first announced, and I naturally assumed that you'd either come up with a name on your own once you got to a stable version, or you'd somehow ask the community for help.\n\nIf you chose the name on your own, or among codeine developers, then that's cool.  Just don't expect outsiders to agree with that relatively closed decision.\n\nOn the other hand, if you asked the community at large for help... where did you ask?  If there's some site I should watch for such things, I'd like to know about it."
    author: "Lee"
  - subject: "Re: Drak"
    date: 2008-01-02
    body: "I didn't ask for name advice now and I got it. I did say on planetkde that I wasn't very happy with the name \"Video Player\" back in May.\n\nThere is no Codeine team, its just me, or at least it was just me until about a week ago."
    author: "Ian Monroe"
  - subject: "Re: Drak"
    date: 2008-01-02
    body: "I've been reading the Dot regularly for months, and I don't think I ever heard of a new name for the video player being sought. I don't think you should look away from good suggestions just because they came in late, what really matters should be the name of the final release. if you change the name before making the final release it will be just a normal part of the development process. anyway, these names might be used for a different video app. after all at least some people feel the need to have an Amarok for video."
    author: "yman"
  - subject: "Anglo-Imperialism"
    date: 2007-12-29
    body: "Why do every programs get renamed with english names? English dominates the IT industry far too much. KDE had a time when it made a difference. "
    author: "Gerd"
  - subject: "Re: Anglo-Imperialism"
    date: 2007-12-29
    body: "English is the official language of the Linux kernel, and much of Linux development.  I might imagine for many KDE devs English is a second language, and yet it seems historically it has always been the common language that most FOSS is developed in."
    author: "T. J. Brumfield"
  - subject: "Re: Anglo-Imperialism"
    date: 2007-12-29
    body: "Time to make a difference."
    author: "Gerd"
  - subject: "Re: Anglo-Imperialism"
    date: 2007-12-29
    body: "No. It is not time to make a difference in using another language inside the KDE project. That would be stupid.\n\nIn case of names it might make sense to use names which can be pronounced in several languages (like Euro). KDE does that with several libraries: Phonon, Plasma, Decibel. Also, most of the KDE release names are related to some German events, names or words. That's ok, and a way to go.\n\nBut there is no sense in using another language in the way of using words which can only be pronounced in some specific language besides English. And  forcing another language into the KDE project as the language of communication is just stupid: English is the common language most people do understand. Every other language is spoken by less people on the Internet. Since you have to use one single main language, English is the natural choice.\nAdditionally: compared to other languages and grammars English so plain easy it can even be learned by members of totally different language cultures (at least to a certain degree) - try to teach languages like Finnish or German and you know what I mean. While English might not be one of the beautiful languages of the world it is definitely one of the easier ones.\nSeveral other languages require another \"alphabet\", think of Chinese or Russian here.\n\nSo, in short: just because you don't like the sound of the English language or the politics of some of the big countries which have English as their main language doesn't mean that there is a need to switch the - in several regards - obvious choice to something else.\n\nBut before I continue: just say what you would prefer - and why it would make sense."
    author: "blubb"
  - subject: "Re: Anglo-Imperialism"
    date: 2007-12-29
    body: "I think one can use words from other languages. This is already the case outside KDE, e.g. with Ubuntu or Kino. In addition, many names do not belong to any language (or are not meant to), e.g. (maybe) Kooka... And it happens that Kooka is fairly easy to remember, even if I do not associate it with any word (maybe I should, but I don't...).\n"
    author: "Hobbes"
  - subject: "Re: Anglo-Imperialism"
    date: 2007-12-30
    body: "What about Amarok? It's definetly no english name (I forgot in which language it's the word for wolf), so don't bother about if the name is english or not. If the name's cool, it's okay I think."
    author: "blueget"
  - subject: "Re: Anglo-Imperialism"
    date: 2007-12-29
    body: ">> Additionally: compared to other languages and grammars English so plain easy it can even be learned by members of totally different language cultures...\n\n... or even by complete retards /:-) - seriously, i dropped it years ago in 9th grade or so, and while my latin is totally gone and my french no more than a stub, english never was a problem. And i kinda like it, too."
    author: "Marc"
  - subject: "Re: Anglo-Imperialism"
    date: 2007-12-29
    body: "English is our lingua franca ( http://en.wikipedia.org/wiki/Lingua_franca ), just to accept it."
    author: "Peppe Bergqvist"
  - subject: "Re: Anglo-Imperialism"
    date: 2008-01-02
    body: "how about Kolnoa? it's the Hebrew word for video, and it's got the K."
    author: "yman"
  - subject: "New name for KDE"
    date: 2007-12-30
    body: "OK I've been sitting on this idea for ages and it had to come out sometime. All the discussion here about Codeine being renamed to Dragon Player has got me going, so here goes.\nNow that KDE is entering a new phase, How about a new name? Actually its more of a modification of the existing name.\nAND HERE IT IS\n\nKomodo.\n\nHere is my reasoning:\n\n*As 'K' in KDE doesn't mean anything, lets give it meaning.\n*It can still be called KDE (or Komodo Desktop or simply Komodo)\n*The symbol of Komodo is a Dragon! A big fearsome looking thing that sits on   top of the foodchain in its environment. An ideal mascot - sorry konqui...\n\nThoughts? (Be kind)"
    author: "marcusesq"
  - subject: "Re: New name for KDE"
    date: 2007-12-30
    body: "Why choose a name that is already taken?\n\nhttp://www.google.com/search?q=Komodo"
    author: "Peppe Bergqvist"
  - subject: "Re: New name for KDE"
    date: 2007-12-30
    body: "KDE was originally \"Kool Desktop Environment\"."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: New name for KDE"
    date: 2007-12-30
    body: "Kool?\nI always thought it was the \"Kalle Dalheimer Experience\""
    author: "Oscar"
  - subject: "Re: New name for KDE"
    date: 2007-12-31
    body: "Nah, it WAS kool desktop environment. Keep in mind that when this started out it was kind of copying CDE (HP's work, that went into Sys V standard) which was commmon desktop environment. Of course, that was a LONG time ago (more than a decade ago). I know that now we say it does not stand for anything, but that is now. Besides, this desktop has expanded a LONG ways past the CDE. It went to looking like WIndows/Apple for a time, and now is finally moving beyond that . I tend to think that we are borrowing from E."
    author: "a.c."
  - subject: "Re: New name for KDE"
    date: 2007-12-30
    body: "Acronyms in open source projects rarely keep their original meaning (KDE, GNOME and Xfce are all guilty of this), or else are recursive acronyms that make no sense, but can be humorous."
    author: "Skeith"
  - subject: "Re: New name for KDE"
    date: 2007-12-31
    body: "Although there are several obstacles, I think your idea is great. KDE is a three-letter acronym with no meaning, and unlike Gnome you can't pronounce it as a word but must spell out each letter (saying KDE as a word sounds more like someone sneezing than anything else). We could have Komodo Office too! And Konqi could seamlessly morph into Konqi the komodo - why not?\n\nThe problems are these: Komodo is already the name of an integrated developers' environment for Windows/OSX (http://www.activestate.com/Products/komodo_ide/) as well as a content management system (www.komodocms.com/) This could be where we might run into difficulties with trademarks etc.\n\nAlthough it would be nice to have a less random name, the fact remains that \"a rose by any other name would smell as sweet\"."
    author: "Darryl Wheatley"
  - subject: "Re: New name for KDE"
    date: 2008-01-02
    body: "Ah. Windows. That was a clone of the Macintosh UI, right? While Windows may be an obscure computer environment to most people today, I was actually using it extensively a decade ago. But then my winning streak in Freecell got so long that I didn't dare play again (and risk breaking it). So I never used Windows again.\n\nStill, it's a bit excessive to avoid the name \"Komodo\" just because there is some program written for \"Windows\" that is allegedly using it, IMHO."
    author: "Martin"
  - subject: "Re: New name for KDE"
    date: 2008-01-03
    body: "I'd rather not loose the name familiarity people have of this product called KDE :)\n\nMarketing leans very much on name familiarity and positive connotations that people attach with known products.  Changing the name means loosing all your previous marketing push.  Not useful.\n\nFor this same reason KOffice and KWord should not be renamed, even though they are boring etc."
    author: "Thomas Zander"
---
In <a href="http://commit-digest.org/issues/2007-12-23/">this week's KDE Commit-Digest</a>: <a href="http://trolltech.com/">Trolltech</a>-sponsored development continues on <a href="http://phonon.kde.org/">Phonon</a> backends. Support for saving to remote URL's in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. A "Now Playing" data engine and applet, and the train clock returns in <a href="http://plasma.kde.org/">Plasma</a>. "Switch Tabs on Hover" can now be disabled, and other refinements in <a href="http://en.opensuse.org/Kickoff">Kickoff</a> for KDE 4.0. Work on a debugger (with a SpeedCrunch-inspired interface) for <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. Work to support the most recent release of the Flash (version 9) multimedia plugin in <a href="http://www.konqueror.org/">Konqueror</a>. SOCKS support in <a href="http://ktorrent.org/">KTorrent</a>. Device handling fixes in <a href="http://pim.kde.org/components/kpilot.php">KPilot</a>. More work on music services in <a href="http://amarok.kde.org/">Amarok</a> 2.0. Further work on the <a href="http://koffice.kde.org/kchart/">KChart</a> <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a> shape in <a href="http://koffice.org/">KOffice</a>. More panorama work, amongst other developments in KOffice. Support for the Bonjour protocol in <a href="http://kopete.kde.org/">Kopete</a>. Initial import and development of a MS Cabinet format archive reader. The temporarily-named Video Player, formerly-known-as-Codeine, has been <a href="http://www.monroe.nu/archives/117-Dragon-Player-2.0-Alpha-1-released.html">finally renamed</a> to <a href="http://dragonplayer.org/">Dragon Player</a>. <a href="http://commit-digest.org/issues/2007-12-23/">Read the rest of the Digest here</a>.

<!--break-->
