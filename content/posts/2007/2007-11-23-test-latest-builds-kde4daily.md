---
title: "Test Latest Builds with KDE4Daily"
date:    2007-11-23
authors:
  - "SSJ"
slug:    test-latest-builds-kde4daily
comments:
  - subject: "Nice work!"
    date: 2007-11-23
    body: "Will probably try this out soon. When using the binary releases of Kubuntu, I'm often not sure if a particular bug has already been fixed or not. This will give me more reason to file bugs;-)\n\nKeep up the good work\nregards Simon"
    author: "Simon H\u00fcrlimann"
  - subject: "First post!"
    date: 2007-11-23
    body: "Hi all,\n\nA bridge update to r739427 (~2 days old) is currently available; this is a 70MB download that takes (on my machine) about 10 minutes to apply.  An update to r740406 is prepared (which I *think* contains Konqueror's new \"Undo Close Tab\" feature, courtesy of Eduardo Robles Elvira and David Faure!), but not yet uploaded.  I'm going to wait around for some successful upgrade reports before making that available, and then hopefully settle into a rhythm of an update every day, depending on whether all targetted modules happen to be compilable at the time.\n\nA few known issues:\n\n- Sometimes logging into your new KDE4Daily session will crash the first time after an upgrade.  Please re-try as it almost invariably works the second time.\n\n- KSnapshot does not appear to function inside of Qemu, so if you want to take snapshots, please use the version of KSnapshot in your host machine.\n\n- Krita does not start due to a missing dependency; from the command-line, within your KDE4Daily VM session please enter:\n\nsudo apt-get install libglew1\n\nand enter the password (\"kde4daily\")\n\n- Processing of backtraces is very slow, especially the first time; please don't let this put you off submitting valuable backtraces!\n\nAbove all else, please download, help seed, play, test and have fun!"
    author: "SSJ"
  - subject: "Re: First post!"
    date: 2007-11-23
    body: "Ok, second post, then.  *Grumbles*\n\nOh, and I'd like to thank aseigo, jos, riddell and annma for their support in getting this mini-project (and my very first contribution to KDE!) off the ground :)"
    author: "SSJ"
  - subject: "Re: First post!"
    date: 2007-11-24
    body: "Minimum recommended hardware?"
    author: "reihal"
  - subject: "Re: First post!"
    date: 2007-11-24
    body: "There's some info on this on in the \"What do I need?\" section of the homepage, but processor-wise I run it on my Pentium M 1.73GHz OK.  RAM is the bottleneck - I'd recommend at least 512MB.  If you are going to be submitting backtraces, I'd recommend much more - 1GB, say.\n"
    author: "SSJ"
  - subject: "Re: First post!"
    date: 2007-11-24
    body: "Just to state the almost very obvious;\n\nthe above specs are NOT general KDE minimum requirements :)"
    author: "Thomas Zander"
  - subject: "Re: First post!"
    date: 2007-11-24
    body: "1 GB isn't much these days, I hope \"everybody\" have that.\nIf not, a good reason upgrade!\n\nI promise I will seed these torrents 24/7 until KDE 4.2 is out."
    author: "reihal"
  - subject: "Re: First post!"
    date: 2007-11-25
    body: "I've *never* had more than 768mb.\n:("
    author: "Chani"
  - subject: "Dugg"
    date: 2007-11-23
    body: "http://digg.com/software/Test_Latest_Builds_with_KDE4Daily_and_be_part_of_Krush_days"
    author: "Dolphin-fanatic :)"
  - subject: "Fantastic Work"
    date: 2007-11-23
    body: "Great work guys! Now I will have something to play with over the weekend. ;-)\n\nOn my desktop I'm used to VirtualBox. Is there any possibility to convert qemu into VirtualBox Files? Or will the qemu File work \"out-of-the-box\"?\n\n"
    author: "kiwiki4e"
  - subject: "Re: Fantastic Work"
    date: 2007-11-23
    body: "Hi there,\n\nThanks for the kind words!\n\nI've actually tried this myself, and it \"worked\" in that it created a bootable and runnable VirtualBox image.  However, I could not get internet access from within the VM in VirtualBox, and I'm not sure whether this is due to the image needing to be re-configured or whether VirtualBox needs some magic in order to allow the image to connect to the internet.\n\nAnyway, uncompress the .bz from the torrent, install Qemu if not already installed, and try:\n\nqemu-img convert kde4daily-0_0_1_r734472-qcow.img -O vmdk kde4daily-0_0_1_r734472-virtualbox.vmdk\n\nThis should give you an image that will work with VirtualBox, possibly without (essential :/) internet capability.  \n\nPlease give it a try and report your findings :)"
    author: "SSJ"
  - subject: "Re: Fantastic Work"
    date: 2007-11-23
    body: "Actually, there is a better and cleaner way to transform a qemu image to a virtualbox image:\nhttp://liquidat.wordpress.com/2007/11/23/howto-transform-a-qemu-image-to-a-virtualbox-image/\nAt the end of the text you also find a note about the network problem and how to fix it.\n\nBut the VirtualMachine is really a great thing, thx!"
    author: "liquidat"
  - subject: "Re: Fantastic Work"
    date: 2007-11-23
    body: "Liquidat has just blogged on precisely this topic - check it out!\n\nhttp://liquidat.wordpress.com/2007/11/23/howto-transform-a-qemu-image-to-a-virtualbox-image/"
    author: "SSJ"
  - subject: "Re: Fantastic Work"
    date: 2007-11-23
    body: "Hi SSJ!\n\nYes it worked for me too! I converted the image with your command described above and just made a \"ifup eth0\" and \"dhclient\" as root in the KDE4 session under VirtualBox without any problems.\n\nThanks for this great piece of Software. You rock! :-) "
    author: "kiwiki4e"
  - subject: "the release candidate ..."
    date: 2007-11-23
    body: "... needs more, umm, beta testers.  ;-)"
    author: "/me"
  - subject: "Use KVM to speed it up!"
    date: 2007-11-23
    body: "This is a brilliant idea. And you can always use kvm ( http://kvm.qumranet.com/ ) to speed things up!"
    author: "Amit"
  - subject: "A Klick (or Glick) Bundle would be even better"
    date: 2007-11-23
    body: "It would be cool if kde4 could be rolled up into a klick bundle that could then be run in a Xephyr window. This would cut the overhead of the emulation and give people a better idea as to the performance of the environment. Is this even possible? I remember a coulple of years ago, one of the nifty klick bundles was a standalone E17 desktop that would load up in Xnest. "
    author: "Joe Kowalski"
  - subject: "Re: A Klick (or Glick) Bundle would be even better"
    date: 2007-11-23
    body: "I don't have any experience with Klik, but the same KDE4 install that I provide (i.e. the contents of /storage/tmp/kde4dev) can be used with Xephyr (I've tried it).\n\nIf Klik could provide all the required dependencies, then I suspect such a thing should be possible :)"
    author: "SSJ"
  - subject: "Re: A Klick (or Glick) Bundle would be even better"
    date: 2007-11-23
    body: "Well its more like you'd have to provide all the dependencies to Klik. You could set it up to automatically build everyday. And then setup a script to run the app within Xephyr.\n\nSounds like a lot of work and bandwidth though. :)"
    author: "Ian Monroe"
  - subject: "Re: A Klick (or Glick) Bundle would be even better"
    date: 2007-11-26
    body: "Xephyr supports Composite, no? Do Kwin's effects work in Xephyr?"
    author: "scroogie"
  - subject: "Re: A Klick (or Glick) Bundle would be even better"
    date: 2007-11-26
    body: "AFAIK, only very recent snapshots of Xephyr support composite, unlikely it's already widely available in distros."
    author: "sebas"
  - subject: "Great!"
    date: 2007-11-23
    body: "Good idea, but why not make it a standard distro that you can install on your hard drive?"
    author: "k"
  - subject: "Re: Great!"
    date: 2007-11-23
    body: "Really, for convenience - most people probably don't want to install a whole new distro just to check out the latest KDE4 (or maybe they do - what do people here think?). \n\nIf anyone wants to step up and take a shot at doing this, please be my guest - there's not much different from a standard Kubuntu 7.04 server install + KDM3 - the list of installed packages can be found by\n\ndpkg --list\n\nthe KDE4 install itself is wholly contained in \n\n/storage/tmp/kde4dev\n\nand the scripts in \n\n~/kde4dailyupdater\n\nthe paths etc are set in the .bashrc/ .bash_profile.  The KDE4 .desktop file is a clumsy hack of an old one and can be found in /usr/share/xsessions.  That's probably the full list of all real differences.\n\nAnother thought that occurred to me is that it might be possible (if you have a big stack of CDs that you want to use up ;)) to use the image to generate your own up-to-date LiveCD everyday, for playing with \"natively\", as it were.  I have no idea how this could be accomplished, however, but suspect that it is possible in theory if anyone has a burning desire to give it a shot :) "
    author: "SSJ"
  - subject: "Re: Great!"
    date: 2007-11-23
    body: "And if the network isn't automatically configured for you, try the tip mentionned here:\nhttp://liquidat.wordpress.com/2007/11/23/howto-transform-a-qemu-image-to-a-virtualbox-image/\n\nThe 2 following lines got me a working network from the vmdk image:\n\nsudo ifconfig eth1 up\nsudo dhclient eth1\n"
    author: "dim"
  - subject: "Re: Great!"
    date: 2007-11-23
    body: "openSUSE is doing this already for some months:\nhttp://en.opensuse.org/KDE4"
    author: "Richard"
  - subject: "Re: Great!"
    date: 2007-11-24
    body: "unless i have missed something, they do not - they provide kde4 packages for opensuse and livecd (which is popular and very good), but this entry is about qemu virtual image.\n\nit is possible that the live cd could be booted in a virtual environment, but i\u00b4m not sure whether it supports the same way of updating this virtual image does."
    author: "Richlv"
  - subject: "Re: Great!"
    date: 2007-11-24
    body: "You can install openSUSE into qemu."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Great!"
    date: 2007-11-24
    body: "\"The KDE:KDE4 build service project offers SVN trunk snapshots updated usually weekly.\"\n\nWeekly being the key word there..."
    author: "Ian Monroe"
  - subject: "Wrong Reasoning"
    date: 2007-11-23
    body: "Quote: \"The KDE4Daily project was created to help people try out and test KDE4 without having to log out to boot from a LiveCD.\"\n\nA LiveCD can be run in a virtualizer so I see no advantage of providing a virtualizer specific image file."
    author: "Anonymous"
  - subject: "Re: Wrong Reasoning"
    date: 2007-11-23
    body: "The whole point is updating. LiveCD's do have creative ways to allow themselves to be upgraded, but Qemu seems like a much more straightforward solution."
    author: "Ian Monroe"
  - subject: "Re: Wrong Reasoning"
    date: 2007-11-24
    body: "Install from a live-cd to a virtual harddrive, works fine."
    author: "Chaoswind"
  - subject: "Re: Wrong Reasoning"
    date: 2007-11-24
    body: "Heh, or just download the virtual hard drive. Which is what this project is!"
    author: "Ian Monroe"
  - subject: "Advice"
    date: 2007-11-23
    body: "To quiet users' headache, please, follow this advice\n\n1) Distribute VM images compressed with 7z with at least 64MB (I suggest 96 or 128MB dictionary - but it requires a 64bit version of Linux) dictionary using Ultra compression mode\n\n2) Provide end users with xdelta binary diffs between daily releases.\n\n3) Remove all unnecessary stuff from the image. I cannot believe KDE4 image takes so much space (I can compress my Fedora 8 with entire KDE3.5.8 to 500MB image)\n"
    author: "Artem S. Tashkinov"
  - subject: "Re: Advice"
    date: 2007-11-23
    body: "Thanks for the advice, but the torrent is already active and it would be a huge pain to re-compress and re-upload :( If I do a \"refresh\" image down the line, I'll be sure to take your suggestion into account!\n\nThe image also contains KDM3 (I want the user to always be able to see a graphical login, even if the KDE4 install is broken), so this and its dependencies probably add a little to the bloat. "
    author: "SSJ"
  - subject: "New update - r740406"
    date: 2007-11-23
    body: "Ok, looking at my logs, it seems that a few people have dared the upgrade procedure and, since no one has appeared here to berate me, I assume they went absolutely, 100% perfectly ;)\n\nSo I'll make the latest prepared revision available, and get a new one building while I go to bed.  Enjoy!"
    author: "SSJ"
  - subject: "Torrent file brocken?"
    date: 2007-11-23
    body: "I have downloaded the file via torrent 2 times now, but unpacking with ark keeps failing with an error. Is the file brocken? Does anyone else have the same problem?"
    author: "Mark"
  - subject: "Re: Torrent file brocken?"
    date: 2007-11-23
    body: "Try using bunzip2 from the command-line.  Quite a few people seem to be using it successfully, so it should be fine.  The MD5 of the compressed file should be 3ea6287f5052d26645e83ea91e1555b3.  Hope this helps!"
    author: "SSJ"
  - subject: "Re: Torrent file brocken?"
    date: 2007-11-24
    body: "Thanks that worked. Seems like I have to report a bug against ark"
    author: "Mark"
  - subject: "Username and Password for login, anyone?"
    date: 2007-11-24
    body: "Username and Password for login, anyone?"
    author: "Mark"
  - subject: "Re: Username and Password for login, anyone?"
    date: 2007-11-24
    body: "found the problem: The keybord layout was the problem."
    author: "Mark"
  - subject: "why not use opensuse"
    date: 2007-11-24
    body: "why not just use opensuse.  Their kde 4 repository is updated daily, so people can test and report bugs better with the full install"
    author: "R. J."
  - subject: "Re: why not use opensuse"
    date: 2007-11-24
    body: "Basically, because I was completely unaware of this service :)"
    author: "SSJ"
  - subject: "Re: why not use opensuse"
    date: 2007-11-24
    body: "because it probably wouldn\u00b4t work that well for people using other distributions or even - windows ;)"
    author: "Richlv"
  - subject: "Re: why not use opensuse"
    date: 2007-11-24
    body: "You can install SuSE without problems, in a virtualized enviroment (VirtualBox for example)."
    author: "Chaoswind"
  - subject: "Re: why not use opensuse"
    date: 2007-11-25
    body: "Where can you get daily builds for OpenSuse from?\nI checked with their KDE 4 page, \nhttp://en.opensuse.org/KDE4\nthere you only find this link to the build service KDE 4 directory:\nhttp://download.opensuse.org/repositories/KDE:/KDE4/openSUSE_Factory/repodata/\nAnd the mentioned SVN version 738441 in the package number is a 6 days old KDE svn version:\nhttp://websvn.kde.org/?pathrev=738441"
    author: "liquidat"
  - subject: "Re: why not use opensuse"
    date: 2007-11-27
    body: "If you install the first two ymp files (KDE4-BASIS.ymp and KDE4-DEFAULT.ymp) from  the KDE4 directory that you mentioned then you will have a default KDE4 enviroment and you will be fine for future updates. "
    author: "Bobby"
  - subject: "Re: why not use opensuse"
    date: 2007-11-28
    body: "Yes, but that wasn't the point. The original statement was that there are daily updates for OpenSuse, and I can't find them."
    author: "liquidat"
  - subject: "Re: why not use opensuse"
    date: 2007-11-28
    body: "If you install the files that I mentioned then you will get a daily update. I do an update ever single day. Just use the Yast Software installer and search for KDE4 you will see the file that can be updated shown in blue. Simple click on one with the right mouse button and update all in the list."
    author: "Bobby"
  - subject: "Re: why not use opensuse"
    date: 2007-12-01
    body: "No, you don't get a daily update. The ymp-files only add\nhttp://download.opensuse.org/repositories/KDE:/KDE4/\nas a repository, which does not contain daily updates. Maybe weekly ones, but not daily ones. If you don't believe me, check the svn version of your installed KDE 4 files and compare it with the one from KDE SVN. The current KDE 4 packages in the OpenSuse repositories are for example of version 742646 which is already some days old."
    author: "liquidat"
  - subject: "Great work, one suggestion."
    date: 2007-11-24
    body: "Awesome work!\n\nA suggestion: I notice that primary partition is about 1gb and I think that too tight. I installed kernel-headers for compiling Virtualbox's Guest Additions and i almost run out of space.Any chance for getting this bigger?\n\nBtw, I've tested the update system and it works perfectly, great job :)\n\nBye."
    author: "Lucianolev"
  - subject: "Re: Great work, one suggestion."
    date: 2007-11-24
    body: "Eep - yes, I've had second thoughts about the partition sizes in the two weeks since I released the image, and think both / and /home are too small.  \n\nI think the only way to handle it is to grow the virtual disk image itself (I believe Qemu has tools to do this) and then try and use qparted to resize the partitions :/\n\nSorry about that :("
    author: "SSJ"
  - subject: "Not able to start up"
    date: 2007-11-24
    body: "Hi! Downloaded this yesterday, uncompressed and started with qemu. But all I get is this: \n\nhttp://www.flickr.com/photo_zoom.gne?id=2058716127&size=l\n\nCan anyone help me? thx. "
    author: "ossi"
  - subject: "Re: Not able to start up"
    date: 2007-11-24
    body: "I've heard this being reported for users of(K)Ubuntu Gutsy (7.10).  If this is the distro you are using, please search the KDE4Daily page (http://etotheipiplusone.com/kde4daily/docs/kde4daily.html) for \"Gutsy\" to find instructions for how to resolve it.  Good luck!"
    author: "SSJ"
  - subject: "Re: Not able to start up"
    date: 2007-11-24
    body: "Thx for the quick answer. That solved the issue. I got a running session now. But my mouse is not working in qemu. Besides the keyboard layout ist not working, but that's not such a big problem. Without a mouse it is hard though. I did some surfing with new Konqueror. Anyway a nice project ;-)"
    author: "ossi"
  - subject: "Re: Not able to start up"
    date: 2007-11-26
    body: "Can someone please post the magic hint here? The kde4daily homepage is still down.\nThanks,\nRolf"
    author: "Rolf"
  - subject: "Re: Not able to start up"
    date: 2007-11-26
    body: "Should be back up now, but for future reference, see here:\n\nhttp://ubuntuforums.org/showpost.php?p=3660695&postcount=10"
    author: "SSJ"
  - subject: "Re: Not able to start up"
    date: 2007-11-27
    body: "I had the same problem on Debian Etch.\n\nFixed installing bochsbios_2.3+20070705-2 from Debian Lenny."
    author: "Quique"
  - subject: "KVM"
    date: 2007-11-24
    body: "On machines with a recent AMD or Intel CPU, the use kernel virtualization brings significant speed improvements. This is true at least for my machine equipped with an AMD Athlon X2 5600+ and running Ubuntu Gutsy.\n\nA HOWTO for Ubuntu can be found at:\n\nhttps://help.ubuntu.com/community/KVM\n\nAfter completing the installation steps, run the kde4daily image:\n\nkvm -m <vramsize> kde4daily-0_0_1_r734472-qcow.img "
    author: "akonrad"
  - subject: "Nah..."
    date: 2007-11-24
    body: "I'd prefer a normal distro for speed and the compositing features. This could also be installed in a virtual machine if so desired.\n\nReally. What would be wrong with this approach? Daily updates via a normal repository should not be a problem either."
    author: "anonymous"
  - subject: "Re: Nah..."
    date: 2007-11-24
    body: "Nothing wrong with that approach too, and you can certainly do it by installing any openSUSE distribution (including KDE Four Live, though that would only really be suitable in a VM atm), which contain quite regular KDE4 package updates (and via 1-click-install). See http://opensuse.org/KDE4"
    author: "apokryphos"
  - subject: "Re: Nah..."
    date: 2007-11-24
    body: "Yeah, but weekly updates are not quite the same as daily updates. Especially for bug hunting.\n"
    author: "anonymous"
  - subject: "Re: Nah..."
    date: 2007-11-26
    body: "> What would be wrong with this approach?\n\nNope it isn't wrong. It just creates an unnecessary barrier for people to participate with testing. The more ways people can test, the more people will test.\n\nPackages in repositories is good for a certain group of people, VM's are good for an other set of people."
    author: "Diederik van der Boor"
  - subject: "Non-BT download?"
    date: 2007-11-24
    body: "I would love to try this out, but I am on a university campus network which breaks Bittorrent, presumably because they think it's illegal.\n\nIs there anywhere this can be downloaded without Bittorrent?"
    author: "Ben Morris"
  - subject: "Re: Non-BT download?"
    date: 2007-11-24
    body: "Unfortunately, I am unable to host this myself, and I'm not sure if the KDE servers have recovered from RC1 :)\n\nIf someone else has some bandwidth to burn, an HTTP download would be very much appreciated!"
    author: "SSJ"
  - subject: "Re: Non-BT download?"
    date: 2007-11-24
    body: "Thankfully, \"sandsmark\" has stepped up and provided this direct download - sincere thanks, Martin!\n\nURL is here; please only use if you can't use the torrent:\n\nhttp://iskrembilen.com/kde4daily-0_0_1_r734472-qcow.img.bz2"
    author: "SSJ"
  - subject: "Re: Non-BT download?"
    date: 2007-11-25
    body: "or just go to http://kde4.iskrembilen.com/"
    author: "martin"
  - subject: "Re: Non-BT download?"
    date: 2007-11-25
    body: "Also, Kiyoshi Aman has provided this mirror - much obliged :)\n\nhttp://kde4.aerdan.org/"
    author: "SSJ"
  - subject: "Re: Non-BT download?"
    date: 2007-11-26
    body: "I didn't expect a link to the main page, so I apologise for everyone who went there and got a 404 from my [typo'd] URL. It has been fixed, however."
    author: "Kiyoshi"
  - subject: "Remarks and questions"
    date: 2007-11-24
    body: "Hi and thank you for this great work!\n\nIf you issue an update of the disk image here are some suggestions:\n\n - remove the CDROM line in /etc/apt/sources.list\n - apt-get update, dist-upgrade, clean\n\nIn about 5 minutes of use I got 3 backtraces.  Now the questions:\n\n - Is there an automated procedure to submit them to the KDE bugtracker (a bit like apport submits Ubuntu bugs to launchpad.net, or reportbug submits bugs to the Debian BTS)?\n\n - Should I submit them manually?  If so, how do I get the exact revision number?\n"
    author: "Laurent Bonnaud"
  - subject: "Re: Remarks and questions"
    date: 2007-11-24
    body: "\"In about 5 minutes of use I got 3 backtraces\"\n\nNice work - thanks for putting up with the slow backtrace generation!\n\n\"Is there an automated procedure to submit them to the KDE bugtracker\"\n\nI don't think so, no - sorry :/\n\n\"Should I submit them manually? If so, how do I get the exact revision number?\"\n\nThat's probably best.  I've just added your revision number question to the FAQ; here's the answer:\n\n\"Your current SVN revision number is stored in the text file:\n$KDE4DAILY_UPDATER_DIR/data/kde4revision.txt\"\n\nYou are very welcome to come and help out in #kde4-krush, too!\n\nhttp://techbase.kde.org/Contribute/Bugsquad/KrushDays\n"
    author: "SSJ"
  - subject: "Re: Remarks and questions"
    date: 2007-11-24
    body: "Oh, and thanks for the CD-ROM suggestion!"
    author: "SSJ"
  - subject: "Running under VMWare"
    date: 2007-11-24
    body: "If somebody is interested, I am running the image under VMWare Server.\n\n- The image was converted using the 'qemu-img convert' untility.\n\n- I had to remove the shipped xorg.conf file. Autodetection of the X server works well enough. Then use krandrtray to switch resolution.\n\n- To get network enabled, I had to replace 'eth0' with 'eth1' (bridged networking) in /etc/network/interfaces"
    author: "Bernd"
  - subject: "Re: Running under VMWare"
    date: 2007-11-24
    body: "I'm very interested :) Can I link to your post in the KDE4Daily homepage?"
    author: "SSJ"
  - subject: "Re: Running under VMWare"
    date: 2007-11-24
    body: "Sure, go ahead. Thanks for all the work, it is great to explore the upcoming KDE4 with KDE4Daily."
    author: "Bernd"
  - subject: "Re: Running under VMWare"
    date: 2007-11-25
    body: "what are the parameters for qemu-img? I did not make it work. I have tried:\n\nqemu-img convert  -f qcow2   kde4daily-0_0_1_r734472-qcow.img -f vmdk kde4daily-0_0_1_r734472-qcow.vdmk\n\nBut this only brings the error:\n(VMDK) image open: flags=0x2 filename=kde4daily-0_0_1_r734472-qcow.img\nqemu-img: Could not open 'kde4daily-0_0_1_r734472-qcow.img'\n"
    author: "gttt"
  - subject: "Re: Running under VMWare"
    date: 2007-11-25
    body: "The second \"-f vmdk\" is wrong. The right operand is \"-O vmdk\"\n\nAnd the first \"-f qcow2\" is not really neaded - it is guessed correctly.  so the command should look like:\n\n$ qemu-img convert  kde4daily-0_0_1_r734472-qcow.img -O vmdk kde4daily.vmdk\n\nhth\nDaniel"
    author: "Daniel W"
  - subject: "Re: Running under VMWare"
    date: 2007-11-26
    body: "Ok, thank you, this works.\n\nNow I do have the problem that I cannot get it to work in vmware. I thought I could use the vmware-player, but this requires a \"vmx\"-virtual-machine file. The vmware-workstation seems not to be free what I always thought it would be."
    author: "gttt"
  - subject: "Re: Running under VMWare"
    date: 2007-11-26
    body: "The VMWare Server is also free (as in free beer). It is much nicer than the VMWare player, you can even create you own virtual michines.\n\nIf you want to use the VMPlayer, you might have luck by taking an existing image (google tells me that http://www.thoughtpolice.co.uk/vmware/ looks promising) and then replacing the vmdk HD image file. But I never did that, so it is all just guessing.\n"
    author: "Bernd"
  - subject: "Re: Running under VMWare"
    date: 2007-11-28
    body: "You can use http://www.easyvmx.com. Just go for the \"Super Simple\" option and select Ubuntu as your virtual operating system. You should check the .vmx file in the ZIP-archive, because it refers to a hard disk file. You can either change the .vmx file or rename your hard-disk image."
    author: "Maarten Th. Mulders"
  - subject: "A bit homogenous, perhaps?"
    date: 2007-11-24
    body: "Don't you think that provides a bit homogenous environment and thus, well, rather poor coverage?\n\nI wish there were .debs for Debian testing/unstable... or that the KDE packages compiled cleanly. Really, *always* when I've tried to compile it, I've got some mysterious (to a programmer) compiler errors. A bit frustrated, I admit I didn't always help by reporting the errors..."
    author: "SLi"
  - subject: "Re: A bit homogenous, perhaps?"
    date: 2007-11-24
    body: "\"Don't you think that provides a bit homogeneous environment and thus, well, rather poor coverage?\"\n\nThat's definitely a concern, and I've tried to compensate by compiling the apps with as many optional components stuffed in as I can - it's a homogeneous environment, but quite a big and rich one, and there seem to be plenty of \"global\" bugs that have been discovered to date."
    author: "SSJ"
  - subject: "runs on Windows XP!"
    date: 2007-11-26
    body: "Amazing, a full Linux KDE desktop with Plasma running within a Windows desktop.\n\nI ran it with QEmu and kqemu, using\nqemu.exe kde4daily-0_0_1_r734472-qcow.img -m 384 -L .\n\nThe KDE4DailyUpdater took a while to download 73MB, but it completed."
    author: "skierpage"
  - subject: "Re: runs on Windows XP!"
    date: 2007-11-27
    body: "Qemu and vmware should have no problems running Linux and KDE 4 in Windows.\n\nI'm curious to see the first Windows-native builds of KDE 4.  Now that Amarok uses libplasma, what does that mean for Plasma on Windows?  I thought the KDE devs said the desktop components would not get Windows ports.\n\nPersonally, the moment that KOffice 2 is released multi-platform, and it supports MS Office imports/exports as well as OpenOffice, I'm dropping OpenOffice all together."
    author: "T. J. Brumfield"
  - subject: "Re: runs on Windows XP!"
    date: 2007-11-27
    body: "I used kdewin-installer (see chehrlic's blog) to download all the pieces and got some KDE4 apps stumbling natively on Windows, details at http://techbase.kde.org/Projects/KDE_on_Windows/Installation  It's amazing that they run at all, but KDE4 on Windows seems pre-alpha to me.\n\nI also tried using the emerge system to build from source on Windows, see http://techbase.kde.org/Getting_Started/Build/KDE4/Windows/emerge .  After downloading hundreds of MB and compiling for hours, I kept running into Python \"file in use\" errors and finally admitted defeat.  There are helpful people making it work better on irc #kde-windows."
    author: "skierpage"
  - subject: "Re: runs on Windows XP!"
    date: 2007-11-27
    body: "> Now that Amarok uses libplasma, what does that mean for Plasma on Windows?\n\nNothing. libplasma is cross-platform.\n\nOnly the workspace (/usr/bin/plasma) won't be portable, as it relies on X11 heavily."
    author: "Diederik van der Boor"
  - subject: "Re: runs on Windows Vista too"
    date: 2007-12-01
    body: "Confirmed working on Vista 32-bit as well.\n\nDownloaded QEmu 0.90 (w 8/27/07 update) and Kqemu then used the above cmdline. I upgraded to today's version and it's working great.\n\nGREAT WORK SSJ!\n\n-T\n"
    author: "Tukon"
  - subject: "Hosting Down"
    date: 2007-11-26
    body: "Hi all, \n\nMy main webhost is currently down, so updates will not work.  A workaround is to enter\n\nexport KDE4DAILY_SUBSTITUTE_MIRROR=http://home.kde.org/~kde4daily/\n\nbefore trying\n\nkde4daily-update\n\nWe are currently at revision 741659 which is approx 6 hours old.  \n\nApologies for any inconvenience!"
    author: "SSJ"
  - subject: "Re: Hosting Down"
    date: 2007-11-26
    body: "Well I just run the update before reading this and it worked fine so I guess it's back up again.\n\nShould I change to that mirror anyway to spare some of your bandwidth?"
    author: "Nosemann"
  - subject: "Re: Hosting Down"
    date: 2007-11-26
    body: "Thanks for your consideration, but the main hosting only serves up small pieces of info (hashes, etc) so is not bandwidth intensive, and the KDE hosting usually only contains bulky stuff (but no info, hashes, etc), so it won't really work from day to day.  The workaround is just for emergencies - and won't work well in the (hopefully long) times of non-crisis :)"
    author: "SSJ"
  - subject: "Patch in order to update when behind a proxy"
    date: 2007-11-29
    body: "Hi all,\n\nTo update the kde4daily when behind a proxy, you need to change the \ndownloadutils.rb to use NET::HTTP::Proxy instead of NET::HTTP\n\nYou also need to define the proxy host and the proxy port.\n\nHere is a patch to the downloadutils.rb (#SCRIPT_REVISION:50) \n\n--- cut here ---\n7a8,12\n> $proxy_host = nil\n> $proxy_port = nil\n> # uncomment these lines if behind a proxy\n> #$proxy_host = 'proxy.name.or.ip'\n> #$proxy_port = 'proxy_port'\n56c61\n<                       Net::HTTP.start(@host, @port) do |http|\n---\n>                       Net::HTTP::Proxy($proxy_host, $proxy_port).start(@host, @port) do |http|\n63c68\n<                       Net::HTTP.start(@host, @port) do |http|\n---\n>                       Net::HTTP::Proxy($proxy_host, $proxy_port).start(@host, @port) do |http|\n144c149\n<                       Net::HTTP.start(@host, @port) do |http|\n---\n>                       Net::HTTP::Proxy($proxy_host, $proxy_port).start(@host, @port) do |http|\n180c185\n< #SCRIPT_REVISION:50\n---\n> #SCRIPT_REVISION:50a\n--- end cut ---\n\n"
    author: "Famelis George"
  - subject: "Re: Patch in order to update when behind a proxy"
    date: 2007-11-29
    body: "Some other things needed when behind a proxy:\n\nIf you need to use apt-get to install some pkgs\nyou have to create a new file named 02proxy in\n  /etc/apt/apt.conf.d with contents\n\n--- cut here (/etc/apt/apt.conf.d/02proxy) ---\nAcquire::http::Proxy \"http://proxy.name.or.ip:port\";\n--- end cut ---"
    author: "Famelis George"
  - subject: "Plasma panel clock"
    date: 2007-11-29
    body: "Whoops! I removed my plasma panel digital clock. Any ideas which config file I can tweak to get it back?\n\nPepe"
    author: "pepe"
  - subject: "Can't perform update"
    date: 2007-12-02
    body: "Always I get error with filehash. Like:\nfilehash = <bla-bla-bla>\nrequired = <another bla-bla-bla>\nupdate aborted, could not download <URL to diff> - hash failed.\n\nIs there another way to download update and apply him?"
    author: "Andrew"
  - subject: "Re: Can't perform update"
    date: 2007-12-02
    body: "Hi! I need more info: What revision are you currently on (contents of $KDE4DAILY_UPDATER_DIR/data/kde4revision.txt), what mirror are you using, and what URLs are failing the hash check?\n\nCheers!\n\nRemember, there is a support channel on #kde4daily on irc.freenode.net."
    author: "SSJ"
  - subject: "Re: Can't perform update"
    date: 2007-12-03
    body: "Sorry, it seems that I have problems with my hardware."
    author: "Andrew"
---
We are now on the home stretch of the road to KDE 4.0, but KDE still needs extensive user testing to make sure everything arrives in the best possible shape for the release.  There is a pressing need for users to be able to get hold of very up-to-date builds of KDE, especially if they want to participate in <a href="http://techbase.kde.org/Contribute/Bugsquad/KrushDays">Krush days</a> and pick up last-minute regressions, confirm proposed fixes, and avoid re-reporting recently fixed bugs, preferably without having to wait for their chosen distro to provide packages.  <a href="http://etotheipiplusone.com/kde4daily/docs/kde4daily.html">KDE4Daily VM</a> aims to provide such a service.





<!--break-->
<p>KDE4Daily is an experimental attempt at accomplishing the above goals using the Qemu virtualisation technology.  End-users <A HREF="http://linuxtracker.org/torrents-details.php?id=4855&amp;dllist=1#seeders">download the initial KDE4Daily 0.0.1 image</a> (containing the now somewhat dated revision 734472), run it in Qemu and use the built-in installer scripts to upgrade the KDE 4 install to the latest revision provided, so that they can always test-out bleeding edge revisions pulled straight from Subversion.  Upgrades are intended to use up relatively little bandwidth (20-50MB for a day's worth of updates is typical; bridge updates which condense a week's worth into one package bring this down to an average of 10MB or so) and be quick to apply (5 minutes per update is common, although bridge updates can take 10-15 minutes).</p>  

<p>Because Qemu is distro-agnostic, you do not need to worry about distros or libraries or dependencies or suchlike; in fact, you can even test out KDE 4 while running Windows!  The downside is that eye-candy such as KWin's new Composite-based effects will not be testable as Qemu does not support hardware graphics acceleration, and everything will generally feel a lot more sluggish than is the case with a native install.</p>

<p>An extensive FAQ is provided at the KDE4Daily homepage, above; please feel free to ask any further questions in the Dot comments section. Also, note that KDE4Daily has not yet had any real testers apart from myself, so please be prepared for "teething trouble" such as botched upgrades and bandwidth issues! Enjoy, and remember that the more people test, the better KDE 4.0 will be.  As an added incentive for trying it out, you can try out the rapidly-developing Plasma as it progresses too!</p>




