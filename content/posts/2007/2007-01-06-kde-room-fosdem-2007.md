---
title: "KDE Room at FOSDEM 2007"
date:    2007-01-06
authors:
  - "iwagener"
slug:    kde-room-fosdem-2007
comments:
  - subject: "Edu?"
    date: 2007-01-07
    body: "I already talked to various people in order to get a slot to talk about Education but I am unsure on what to do in order to make that happen. I'd like to invite all developers interested in Education (Debian-edu people, Bruno Coudoin from GCompris...) in order to start a better collaboration (data standards, sharing data, ...). Key speech at FOSDEM is about One Laptop Per Child project thus very edu-oriented. We could make this slot edu-oriented and in a second part sharing ideas and knowing each other better between projects. How do I go from there? I never went to FOSDEM thus it's quite difficult to understand it all. This article talks about \"the legendary pre-FOSDEM gathering at Le Roi D'Espagne\": when? link to a map? What do you define as \"KDE group\"? Is it only for lodging? Do I need to subscribe to the kde-events-benelux mailing list? \nAnne-Marie"
    author: "annma"
  - subject: "Re: Edu?"
    date: 2007-01-07
    body: "The legendary pre-FOSDEM gathering at Le Roy D'Espagne takes place the friday evening before Fosdem starts. It takes place at the Roy D'Espagne which is at the Grand Place in Brussels, not so far away from central station. Surely there will be more precise announcements in a few weeks on the Fosdem list: http://lists.fosdem.org/mailman/listinfo/fosdem"
    author: "Fred"
  - subject: "Another interesting room"
    date: 2007-01-07
    body: "If you are planning to be at FOSDEM, don't forget to drop by the Research Room.  Interesting discussions on Software Quality and OSS, with strong KDE emphasis, are going to be held there. "
    author: "Giorgos Gousios"
---
<a href="http://www.fosdem.org/2007/">FOSDEM</a> is yet another one of those catchy acronyms that stands for nothing less than "Free and Open source Software Developers' European Meeting". KDE will again be hosting a room dedicated to talks and chat about the free desktop. These annual meetings are organised by volunteers, free of charge and generally recognised as one of the most productive gatherings available on the European stage.  This year it will be held on the weekend of 24/25th February 2007 on the ULB Campus Solbosh in Brussels, Belgium.






<!--break-->
<div style="float: right; border: thin grey solid; margin: 1ex; padding: 1ex">
<img src="http://static.kdenews.org/jr/fosdem.png" alt="FOSDEM logo" width="270" height="80" />
</div>

<p>We are now looking for KDE contributors to talk about what they are working on.  Talks in previous years have included PyKDE, Krita, KDE Marketing, KDevelop and Context Linked Desktops.  We want to hear from all parts of KDE in the devroom, so do not think your area of work is too insignificant.  We also need people to man our stall, one of the busiest stalls at FOSDEM, selling KDE merchandise and chatting to our users about our future plans.  Sign yourself up on the <a href="http://wiki.kde.org/tiki-index.php?page=FOSDEM2007">KDE at FOSDEM 2007 wiki page</a>.</p>

<p>Outside of KDE the programme is packed with high profile speakers, stands, and events for all the major free software with 'devrooms' for focused software and distribution meetings. This year there will be a large X.org presence, making it an ideal forum for the exchange of ideas for the future of the Free desktop.</p>

<p>If you want to come to the conference together with the KDE team, please let the people at <a href="https://mail.kde.org/mailman/listinfo/kde-events-benelux">kde-events-benelux</a> know so they can add you to their plans. Details of our accomodation are also on the wiki page, let us know by Friday 12th if you want to stay with the KDE group.</p>

<p>Information on how to get there and where to stay is available on <a href="http://www.fosdem.org/2007/">the FOSDEM website</a>. Registration is not necessary, but will help give the organisers some idea of the attendance so they will not be overwhelmed on the day. We will be meeting at the legendary pre-FOSDEM gathering at Le Roi D'Espagne featuring (almost) free beer and as many hackers are can be squeezed into one pub attic.  Hope to see you there!</p>






