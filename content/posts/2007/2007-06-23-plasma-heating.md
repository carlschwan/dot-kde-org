---
title: "Plasma is Heating Up"
date:    2007-06-23
authors:
  - "jriddell"
slug:    plasma-heating
comments:
  - subject: "I regret"
    date: 2007-06-23
    body: "Hello,\n\nI regret having made comments related to plasma that were a critique to Aaron for capturing all the hype without delivery.\n\na) There is delivery, I have run it, looked at the design and source and am impressed.\nb) Talking about visions is really big part of making them come true. Or else nobody will develop the plans.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: I regret"
    date: 2007-06-24
    body: "There's a show of good character!\n\nKudos :)"
    author: "Anon"
  - subject: "Re: I regret"
    date: 2007-06-25
    body: "Same here. I tought Plasma was going to stay vaporware until KDE 4.1. Even that I think the full power of plasma won't the explored in KDE 4.0 it's good to see I was wrong."
    author: "Iuri Fiedoruk"
  - subject: "the kicker must die, just when !!!"
    date: 2007-06-23
    body: "ok i know it will die, but can you tell us when, with the next alpha to be published soon ( 27 octobre), can we expect to have the new kicker and raptor in place !!.\n\neh bye the way, did i say, you guys rock.\n"
    author: "djouallah mimoune"
  - subject: "Re: the kicker must die, just when !!!"
    date: 2007-06-24
    body: "Actually, it's 23rd of october, and it's not the next alpha, it's the final version. \nAccording to the release schedule, the first beta is supposed to be tagged tomorrow, and probably released some days later. \n"
    author: "AC"
  - subject: "Re: the kicker must die, just when !!!"
    date: 2007-06-24
    body: "What was planned as the \"first beta\" is now the \"second alpha\".\n(No schedule changes other than that, there's simply one more alpha and one less beta, making the API freeze data a later one.)"
    author: "Kevin Kofler"
  - subject: "Plasma and KWin"
    date: 2007-06-24
    body: "I was wondering if there was ever any discussion about having kwin merge with plasma, perhaps becoming a plasmoid of sorts.  Plasma would then be manager for the 'whole desktop' experience.  It's not something I'm necessarily suggesting be done, I'm just interested if anyone knows of a link to any discussions about such a move?"
    author: "Daniel"
  - subject: "Re: Plasma and KWin"
    date: 2007-06-24
    body: "\"I was wondering if there was ever any discussion about having kwin merge with plasma, perhaps becoming a plasmoid of sorts.\"\n\nI don't think its a good idea to make plasma act as a windowmanager. \nWhy turn the whole graphical environment into 1 monolith application?\n"
    author: "whatever noticed"
  - subject: "Re: Plasma and KWin"
    date: 2007-06-24
    body: "I agree, though on a similar note I also wonder how easy it would be to integrate Plasma into apps.  The most obvious reason would be to gain the power of plasma - an easy scripting language that can be integrated into sidebars.  Another would be to remove duplication so, for example, the Amarok play sidebar could be put on the desktop easily.\n\nApart from that imagine an Unreal Tournament like desktop in a game where the desktop is plasma and the community can make plasma applets for it.  I think there's lots of potential there."
    author: "strider"
  - subject: "Re: Plasma and KWin"
    date: 2007-06-25
    body: "Don't confuse Kross (multi-language scripting support) with Plasma (desktop interaction) ;)\n\nDanny\n"
    author: "Danny Allen"
  - subject: "Re: Plasma and KWin"
    date: 2007-06-24
    body: "At some points, the idea is'nt so bad. A fully scriptable enviroment, from the desktop over the icons to the applications up to the windows itself, it would be a dream to manage every aspect by the user itself, if want. The developer of a application, would have less work, and the features could gain rapidly from the community itself.\n\nWell, but on the realistic side, this is actually only a dream. The Performance for this would lack like hell, and the design of the security and interfaces, need years of experience and work. Actually for example, kde is missing important global components. \n"
    author: "Chaoswind"
  - subject: "Re: Plasma and KWin"
    date: 2007-06-24
    body: "I would not care, as far as I can continue to KDEWM=fvwm\n\nKeep up the good work :)"
    author: "Jes\u00fas Guerrero"
  - subject: "Re: Plasma and KWin"
    date: 2007-06-24
    body: "What can FVWM better than kwin?\nWhat about netwm/those window-attributes that kdes support via a kcontrol-gui?"
    author: "Chaoswind"
  - subject: "Re: Plasma and KWin"
    date: 2007-06-25
    body: "I am not going to argue about that. I can decide what suits me and what doesn't.\n\nIt can do far more than you seem to think it can do. But anyway that is totally off-topic. It is just my desire to do so. There is no greater reason to do something.\n\n-- Jesus Guerrero"
    author: "Jesus Guerrero"
  - subject: "The Plasma site should become relevant again"
    date: 2007-06-24
    body: "\nIt would be great if the Plasma site got Updated, adding all the Links to articles, moving all of the video's and podcasts or links to them on to it. Screen shots of what is going on. maybe some exclusive content.  There is a plethora of info spreading out all over the net concerning Plasma. It would be great if it was funneled through the Plasma site maybe with an RSS feed as well. I have no clue how to even write a simple bash script much less create a website so I can only hope that someone out there with not only the desire but the ability could make it happen. If I could help in any way feel free to contact me. I have no problem doing what I can :)\n\n \n"
    author: "madpuppy"
  - subject: "How long until it's usable?"
    date: 2007-06-24
    body: "So, the important question is: When could we start to code our own plasmaoids using kross and favored language? First Beta? Second Beta? After the first Final?\n\nAnd when come the first tutorial or showcast for this?"
    author: "Chaoswind"
  - subject: "Re: How long until it's usable?"
    date: 2007-06-25
    body: "You can code up plasmoids in c++ currently, and other language support is coming."
    author: "Thomas Georgiou"
  - subject: "Thank you Plasma Team"
    date: 2007-06-24
    body: "It is really great to see progress in plasma project. For the past few weeks commit digest carries updates regarding plasma which itself shows how active the project is. Now I would like to request plasma team for an updated plasma website with progress of the project and also the roadmap. Thank you plasma team for your great work."
    author: "Swaroop"
  - subject: "Re: Thank you Plasma Team"
    date: 2007-06-24
    body: "Yes, and I want an updated Oxygen website."
    author: "EMP3ROR"
  - subject: "One more thing :P"
    date: 2007-06-24
    body: "I was looking around the Plasma site and noticed that some of the links and the sites that they link to don't exist anymore....what ever happened to the appeal site? I remember going to that site a few times, I think the icon was a hand fan or something maybe it was supposed to be a peacocks tail but, it was like all gray. \n\nsomeone mentioned this back in February:\n\n>> On Monday 12 February 2007 17:52:42 Tim wrote:\n>> You probably already know this, but the appeal.kde.org site redirects you  \n>> back to the kde home page. Is it supposed to do that?\n\n>AFAIK, the Appeal webmaster is aware of it, and it's being fixed.\n\n>Thanks for the report, though.\n-- \n>sebas\n\nDid appeal go the way of the dodo? \n\n"
    author: "madpuppy"
  - subject: "Re: One more thing :P"
    date: 2007-06-24
    body: "Last I heard - yes, it has.  Appeal was a project that in itself did not have any code in it - but rather was more of an organizational thing that had other components doing the work.  We mostly just dropped the organizational overhead and work directly on the components at this point.  At the time of inception, however, it was important to visualize these components working together."
    author: "Troy Unrau"
  - subject: "zoom, zoom"
    date: 2007-06-24
    body: ">>According to Seigo, \"By zooming out, users can get an overview of all the object groupings that they have made. These groupings may reflect the projects they are working on, be ways to keep different sets of files organized, etc. By hovering or clicking on one of these groups when zoomed out, users can either get a preview/snapshot of what is in the grouping, or zoom in on that grouping so that it is displayed full size on the physical screen. Thus we can offer much greater flexibility on the desktop layers while giving users visual context.\n\nThat sounds really cool.\n"
    author: "ac"
  - subject: "website"
    date: 2007-06-24
    body: "an updated website or wiki or a plasma that is ready for 4.0, you decide.\n\ni'll get to the website as soon as i can, but the code is my priority right now. sorry, but the code is just slightly more important right now ;) it would be nice if the website wouldn't require any of my input/involvement to get up to date, but it's not rational to expect someone else to be able to know what to put there without input and consultation."
    author: "Aaron J. Seigo"
  - subject: "Re: website"
    date: 2007-06-24
    body: "I think you have made a wise decision ;)"
    author: "Joergen Ramskov"
---
Linux.com reports on <a href="http://www.linux.com/feature/114560">progress in Plasma</a>, the new KDE 4 desktop.  "<em>If you visited the <a href="http://plasma.kde.org/">Plasma project's outdated website</a> in past weeks, you might have got the impression that the team behind the project to revitalise the KDE desktop has not been up to much these past months. Delve into KDE's SVN repository, mailing lists, or the mind of lead developer Aaron Seigo, however, and you'll find a more exciting story.</em>"
<!--break-->
