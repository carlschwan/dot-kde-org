---
title: "Ars Technica: A First Look at KDE 4 Beta 2"
date:    2007-09-05
authors:
  - "tunrau"
slug:    ars-technica-first-look-kde-4-beta-2
comments:
  - subject: "Theme"
    date: 2007-09-04
    body: "I just want to point something, don't take this as a flame or bashing into the new style authors, please.\n\nOne of the things I always disliked in Linux/Unix UI is that you can't adjust spacings and marginx. You know, in windows 95 you could change the window title bar to being very thin, the same goes for buttons.\nEvery new style I see people proposing is fatter, more spacing wasted and more margins. This new theme is a nice idea, but needs do adjusts sizes (way to big for now) and contrast in my opinion. To my eye there is too much empty spaces, look the buttons.\nSure you could argue that's the better for msot people, but what about giving the user a way (hidden please, it can be a XML file for example) to define those settings?\n\nOverall KDE4 is looking really good, but I'm still not convinced by that Plasma stuff. For example changing a mature techonology like Kicker for a panel Plasmoid that isn't very good until beta2 (remember the original schedule was to release it in two months from now) dosen't seem to be a wise idea. But as a replacement for Kdesktop, icons and Karamba it looks very promissing and sharp :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Theme"
    date: 2007-09-04
    body: "> One of the things I always disliked in Linux/Unix UI is that you \n> can't adjust spacings and marginx. You know, in windows 95 you could \n> change the window title bar to being very thin, the same goes for buttons.\n\nI'm not sure which UI you are talking about, but that is certainly not true of the KDE window decorations and styles that I know of.\n\n"
    author: "Robert Knight"
  - subject: "Re: Theme"
    date: 2007-09-04
    body: "I think the original poster is not familiar with the gazillion configuration option offered KControl/System Settings (on Kubuntu) for window decorations!"
    author: "backtick"
  - subject: "Re: Theme"
    date: 2007-09-04
    body: "No, those kind of settings depend on the actual style implementation (widget or window decoration styles). Some provide them, some don't.\nI agree with the GGP's point. The current default style on KD3 (Plastik) is very compact and wastes minimal space. Unfortunately the Qt counterpart (Plastiq) is not and neither are the other available styles, including Clearlooks and Oxygen (which means that for some -including me- the transition won't be painless).\nNevertheless I'll save my final opinion for when things are settled. In the mean time, keep it up!"
    author: "attendant"
  - subject: "Re: Theme"
    date: 2007-09-06
    body: "Eh, no Plastik is quite big compared to Keramik which was quite big compared to Highcolor. Plastik is exactly what he is talking about when saying every new style is more bloated that the previous."
    author: "Carewolf"
  - subject: "Re: Theme"
    date: 2007-09-06
    body: "Are you kidding?  Keramik is the one with huge over-gradiented buttons and huge menus that won't scale down.\n\nI'm using Lipstik, which is basically Plastik.  It doesn't shrink anything down anymore than Plastik does and I can definitely tell you it is a very compact theme.  Here's a screenshot:\n\n"
    author: "me"
  - subject: "Re: Theme"
    date: 2007-09-06
    body: "I agree with you on Plastik/Lipstik, but I can't stand the Tahoma font."
    author: "Amy Rose"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "Yes, that's the case.\nAnd yes, I know the zillion options on kcontrol, but options for better sizing of elements (tabs, buttons, window titles, etc) are provided only by the themes (and only a few of them), it's not a configuration of qt or KDE.\n\nTake gtk themes for example, you can create a theme with very small buttons, but can I change button sizes on a theme? Only if I edit the theme, that in the case of a gtk theme is ok, but what about KDE themes that are C++ code compiled? You would have to edit the source and compile your own version.. what a mess.\n\nWindows allow you to change some sizes.. well tomorrow I'll take a screenshot to show you, now I'm away from a windows machine :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "All shipped decoration allow some tweaking of the border and titlebar size.\nExcept Oxygen, which still does not allow that. I suppose it's a matter of time. That is the case since KDE 3.2\n\nThe title bar size is tied to the title bar font height, and I think that's enough flexibility, without requiring a separate option (which would need tweaking if you change the window title font).\n\n"
    author: "Luciano"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "Take a look at thatr screen:\n\nhttp://protomank.googlepages.com/adjust_sizes.png\n\nLook how windows allow to adjust the titlebar and scrolling bar sizes.\nPlus, what I wished was for adjusting spacing in the button (see the red markings)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "If I understand how it works, I think the css styles for widget should allow this kind of configuration, and more. But there is no GUI to tweak the default attributes for the widgets at the moment. A programmer with enough interest in this feature could implement this feature.... ;)"
    author: "Luciano"
  - subject: "Re: Theme"
    date: 2007-09-04
    body: "The new, huge, buttons remind me of Word Perfect 5.1 for Windows! :-)\n\nDoes anyone else remember that? They had not yet abandoned their own printer drivers (which used to be a major selling point in the DOS version) so there were two ways to talk to the printer. Either using the Windows driver, or the Word Perfect built-in driver. In the latter case, you could not use the Windows fonts.\n\n[Oh. I wandered seriously off topic. Sorry!]"
    author: "Martin"
  - subject: "Re: Theme"
    date: 2007-09-04
    body: "Oh, yes... The Official MS Style Guide for Windows 3.0 prescribed big buttons with a but of text underneath. Which WordPerfect and MS Help faithfully followed, only to be wrongfooted by the next generation of Microsoft Windows applications."
    author: "Boudewijn Rempt"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "Out of curiosity, why are we going back to something almost 20 years ago? :)"
    author: "Henrik Pauli"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "Wordperfect for Windows was also horribly, and I mean, horribly slow.  Wordperfect was king once upon a time, and Word was the underdog.  Wordperfect kept the same key-strokes from a DOS/console based word processor, while Word focused on the GUI interface, and having the keystrokes mirror other Windows apps.\n\nWe all know how that turned out.\n\nMaybe there is a relevant object lesson here.  If your default UI reminds people of Wordperfect for Windows, something needs to change."
    author: "T. J. Brumfield"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "I loved the DOS WordPerfect, and I do wish they had managed to keep some ground.  As a word processor, they were vastly superior to anything else around the time..."
    author: "Henrik Pauli"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "Because 'new' doesn't necessarily mean 'better', seeing as the design of the human eye and brain did actually not change all that much over the last few decades. :)\n\nAnd as it happens, yeah, I understand there's pretty much a consensus that big icons with text underneath is what makes for the most comfortable toolbars for the users as a whole. Which doesn't mean that using big icons + text will /automatically/ make your UI good if there are other issues, mind (like too much wasted space where there shouldn't); it's just one part of a whole that's made of a sum of tiny fiddly details."
    author: "Balinares"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "Dunno, but I find icons only (granted the icons make sense and are recogniseable -- which of course is another issue altogether) superior.  It not only takes much less screen estate, but it's also not less understandable than the text-aided toolbar.  Another problem of mine with the text+icons toolbars is how Up takes much less space from Back and Forward, and makes the icons look unaligned and carelessly thrown at the toolbar."
    author: "Henrik Pauli"
  - subject: "Re: Theme"
    date: 2007-09-05
    body: "Kicker, however, was always a resource hog, especially for older systems.  Merging it together makes the memory management much simpler, and its progress to replace it instead of just porting it to Qt4.  It'll be better in the long run, hopefully."
    author: "Jordan Klassen"
  - subject: "KDE4 looks like KDE3"
    date: 2007-09-04
    body: "... ok, this could be changed, as Qt4 comes with the beautiful Clearlooks Theme. But does it feel like KDE3? That would be sad... hopefully, it can be configured to feel a bit GTK'ish ;)"
    author: "anonymous"
  - subject: "Re: KDE4 looks like KDE3"
    date: 2007-09-04
    body: "Don't get me wrong, I like Cleaklooks a lot but what the heck are you talking about? How do you mean \"KDE4 looks like KDE3\"? Assuming you're talking about style, then well you didn't actually experience Oxygen, did you? It's looks fresh, very polished and very distinct from styles usually shipped with KDE 3 (Plastik, Polyester, Keramik and the now popular domino)"
    author: "A KDE Advocate"
  - subject: "Re: KDE4 looks like KDE3"
    date: 2007-09-05
    body: "Good to have a GTK'ish KDE?\nCan I have some of that grass?"
    author: "nap"
  - subject: "Re: KDE4 looks like KDE3"
    date: 2007-09-05
    body: "Compared to the breath of fresh air that is Oxygen, the plastik-clone Clearlooks is imho pretty dull and boring..."
    author: "jospoortvliet"
  - subject: "Re: KDE4 looks like KDE3"
    date: 2007-09-07
    body: "\"\"Compared to the breath of fresh air that is Oxygen, the plastik-clone Clearlooks is imho pretty dull and boring...\"\"\n\nPlastik clone ? Clearlooks is a loose descendant of Bluecurve, introduced in Red Hat 8 as a unifying theme between Gnome and KDE. \nYou KDE users had by default the ugliest widget set the mind could conceive (Keramik) while we had our Bluecurve cups of STFU.\n\nPlastik is a pile of trash put together with ducktape. "
    author: "Anon"
  - subject: "Re: KDE4 looks like KDE3"
    date: 2007-09-08
    body: "> Plastik is a pile of trash put together with ducktape. \n\nNo it isn't, and I think you mean \"duct tape\"."
    author: "Paul Eggleton"
  - subject: "Re: KDE4 looks like KDE3"
    date: 2007-09-08
    body: "You, damn useless brat, should know better.\nThe two ways are right. Duck Tape is the most used nowadays, even though in the beginning it was wrong, it even has its brand. ( http://www.duckproducts.com/products/subcategory.asp?CatID=1&SubID=1 ) \nhttp://en.wikipedia.org/wiki/Duct_tape\n\nNow get off my lawn."
    author: "Anon"
  - subject: "Re: KDE4 looks like KDE3"
    date: 2007-09-06
    body: "I can't stand GTK, but to each their own."
    author: "T. J. Brumfield"
  - subject: "New folder icon"
    date: 2007-09-04
    body: "The new folder icon doesn't look really good. I hope they will change it before final release. The previous one(and the one before the previous one) were much better."
    author: "anon"
  - subject: "Re: New folder icon"
    date: 2007-09-04
    body: "It's infinetly better than the previous Oxygen folder icon. That one was ugly beond belif, but I hoped it was just a placeholder. \n\nOn the last screenshot, compare the bin and Desktop(yuch) icons. It's much better. But I kind of think the old(kde3) approach looks better than the straigth from the front look."
    author: "Morty"
  - subject: "Re: New folder icon"
    date: 2007-09-04
    body: "I didn't like the last one, either, but I wouldn't call it ugly - it was more \"completely unlike a folder\" than anything else :)\n\nI still like the one before that one the best."
    author: "Anon"
  - subject: "Re: New folder icon"
    date: 2007-09-04
    body: "You mean almost, but not quite, entirely unlike folder"
    author: "Ady"
  - subject: "Re: New folder icon"
    date: 2007-09-04
    body: "Come on guys, all the folder we did were good. Link to me your perfect folder for your favorite theme, that way I can understand what you guys like."
    author: "David Vignoni"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "The biggest problem with those folder icons are that they are way to glossy. And from the screnshot. The old icon used for the Desktop are too flimsy, all told it does not relly look very much like a folder.\n\nThe other look more solid and like real folders so they are an improvment. One problem I think is the single color used, using a base color for all folders and a ribbon to indicate folder color like with the KDE3 icons may work better.\n\nAnd simply the perspective used on the KDE3 folder gives a much better look.\n\nThe folders in the nouveXT2 iconset are quite good, and the perspective used on those works better than the Oxygen approach too.\nhttp://kde-look.org/content/preview.php?preview=2&id=62630&file1=62630-1.png&file2=62630-2.jpg&file3=&name=nuoveXT+2"
    author: "Morty"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "You understand the folder in NuoveXT2 are just a rip-off from Windows Vista folders?\n\nAs a reference this is the Windows Vista folder : http://www.rw-designer.com/res/vista-folder-32.png\n\nI can't take into consideration your suggestion. \n\n"
    author: "David Vignoni"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "As I don't use Vista I didn't know that :-). Rip of or not, you asked for a folder that I thought looked better. \n\nBut as said, the texture and the way the colors are done is waht makes it look better than the current Oxygen ones. Not that you should copy it outright. \n\nThe perspective are not good however, I still find the old way better. A slight tilt to the folder breaks the squareness of it and makes it look more natural. I don't think copying the perspective from the Vista look, or Tango(as the current Oxygen does) for that matter are a good way to go. "
    author: "Morty"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "There's nothing wrong with the folder icon, or any of the Oxygen icons (except for the one that is being used from KDE3, the one to clear the search field). Anyway, I think it's a step in the right direction, I honestly feel that at this stage it is on par with Leopard's icons (which I didn't like as much as Tiger, but time will tell if the entire icon set is as good). The Oxygen style and icon theme really is a breath of fresh air among the themes normally associated with KDE3, which no offense to anyone who likes them, were ugly, boring and horribly designed (with the exception of Domino). Though I'm not a designer I could point out many major flaws in many of the KDE3 visual styles (especially Plastik and Keramik). Let's try to make KDE more appealing for those used to top-notch visuals from OS like Leopard and Vista - and Oxygen is that style, and Plastik is at the opposite end."
    author: "kishiboota"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "Well thanks, always reading bad comments on folder etc is really depressing sometimes. Glad to see somebody likes the work we have been doing."
    author: "David Vignoni"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "I like it too! More subtle than the last one (the desktop icon) which is important 'cause it's probably the icon you have to see most often.\n\nI understand the difficulty in creating an icon that's original and still very pretty, I can't think of anyway you could do it better."
    author: "chair"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "I think the new folder is really good. Those of us who like what we're seeing are actually the silent majority :)"
    author: "AC"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "David, the new folder icons rock :-) Most of my development time I'm spending on Dolphin for KDE4.0 and if something would be wrong the the folder icons, I would be the first one who complains about them.\n\nNo complaining, just enjoying the progress of the Oxygen icons week after week :-)\n\nBye,\nPeter"
    author: "ppenz"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "And thanks for your work on Dolphin. I'll admit I wasn't convinced when the idea of using dolphin as default was first suggested - although I just took the view that I'd change my default back to konqueror and so didn't feel the need to bitch about it on the dot ;-)  But trying dolphin in the beta I've really liked it and I can see myself using both dolphin and konqui, probably even dolphin more than konqui"
    author: "Simon"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "Yeah Peter, Dolphin it's awesome"
    author: "David Vignoni"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "Ah, and I was sure since the beginning it was going to be a good thing :D"
    author: "David Vignoni"
  - subject: "Re: New folder icon"
    date: 2007-09-06
    body: "I'm with Simon on Dolphin. It's actually been nice to be able to configure Konqueror to behave more like a web browser (especially with opening clicked links from other apps in a new tab in an existing Konqueror window rather than in a new one all the time, since I kept having problems with that feature opening my web pages in file browser windows...)\n\nI just hope I can continue to customize the Dolphin toolbars because I cannot live without my Up button. :D (It's one reason I like to use Konq as my web browser)"
    author: "Amy Rose"
  - subject: "Re: New folder icon"
    date: 2007-09-21
    body: "To each his own. I like Konqueror because, through KParts, I can have everything except my text/source/document editing (Kate or LyX, as appropriate) in tabs of the same tool.\n\n(eg. double-click a video file and the tab is repurposed to the KMplayer KPart rather than a new tab or window being opened)\n"
    author: "Stephan Sokolow"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "Sorry to see people's work being put down. \nI really like what you do, although I can't see why some people get all fussy over a --> folder icon <--. \nDespite being a long-time linux user, I like the bling of today's desktops (yes, I already use KDE4.0's bling!). But still I can't see any cause to prefer one folder icon over (almost) any other. \n(Perhaps my wife's right and I don't have any understanding of aesthestic...)\n\nHmm, a compliment from an aesthetic analphabet might not be of the greatest worth, but there you go: \nI _really_ like the oxygen icon set!\n\n"
    author: "Tom Vollerthun"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "guys, I love your work. it's just way to dependent on taste :("
    author: "jospoortvliet"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "Cold not said better.\n\nAlso, it's just me or KDE4 is getting more Apple-like? (in general terms please, don't answer as if I'm talking only about icons or style, try to see the big scene).\n\nHonestly, running from a Windows look to Apple look dosen't seem a good idea to me. One thing I disliked the icons is removing dark(er) borders from icons, it's only good for big icons and good LCD monitor :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "Well thanks for the support, it's a great responsibility being the art director of the icon project and sometimes encouraging messages are really a great antidote to continuing doing my best. Thanks, we are in actually doing a lot of work making fixing all the inconsistencies and improving all the actions metaphors etc. Also the applications icons are finally shaping up ... it will be very good for the release."
    author: "David Vignoni"
  - subject: "Re: New folder icon"
    date: 2007-09-06
    body: "guys, I love your work. it's just way to dependent on taste :("
    author: "jospoortvliet"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "i like them too :-) keep up the good work"
    author: "Beat Wolf"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "I prefer the old oxygen folder. That was great.\nExcept for the folder, the oxygen icons are great and a huge improvements for kde4."
    author: "Coward"
  - subject: "Re: New folder icon"
    date: 2007-09-08
    body: "Except the folder icons, i think the oxygen-icons are stunning.\nCongratulations for your good work!\n\nIn smaller sizes the folder-icons get a little bit \"blurry\" and you cannot identify them as folders anymore. \nI think the icon needs to look more like a real folder with a better sharpness of edges to give the eye a better chance to resolve it easiely. \nFurthermore their bright blue color is too obtrusive. A more unobtrusive blue color woud be important, because the folder icons reign with their extrem colors the whole desktop.\n\nGreets\n"
    author: "Flo"
  - subject: "Re: New folder icon"
    date: 2007-09-21
    body: "I have a similar opinion. I like subdued colors and while the rest of Oxygen is beautiful, that blue folder icon is still borderline too bright for me. (Several years ago, before the presence of Baghira widgets+borders and OS-L icons and the allure of KIO and KParts drew me back, the fact that KDE hurt my eyes was enough to drive me to GNOME and then to Xfce once I discovered it.)\n\nOh well, not too difficult to pull it up in an editor and adjust the color a bit. At least it's not a glossy mess.\n\nNow all I need is a window border theme which doesn't play \"guess where the border ends\" and a widget theme that'll return the widget borders that the one in the screenshots stole/hid/erased.\n\nOf course, for all I know, I may not mind so much once I actually get a chance to test it out. Unlikely though."
    author: "Stephan Sokolow"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "Please everyone stop just a moment and look to XP folder icons. Do you think, a part from the colour, that this rapresent a paper folder?\nBut we are used to them, we identify them with \"folders\" so we think they are \"natural\".\n\nHonestly, Oxy's folders are probably a little worse than that (too gloosy, as Morty said) but once you get used to them, it's not that bad."
    author: "Vide"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "Hey David, I didn't see the last folder revision, remove everything I said about \"too blurry\", it's really cool!\n\nChe figata! :)"
    author: "Vide"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: "I saw the new folder icon for the first time with this screenshot http://www.rw-designer.com/res/vista-folder-32.png\n\nIn beta1 which I was testing there ware old icons for folders.\nI really like new icon. It's much better :)"
    author: "patpi"
  - subject: "Re: New folder icon"
    date: 2007-09-05
    body: ";/, wrong link, http://arstechnica.com/journals/linux.media/kde4-beta2-programs.png\nthis one is correct. Folders are nice"
    author: "patpi"
  - subject: "space"
    date: 2007-09-04
    body: "new white style is really very nice, especially smooth curves, but i completly agree with man who posted before me, that there is too much waste of space. E.g. screenshot with KWrite dialog, space between buttons and text forms at the bottom of the dialog is really too big. But I think that this will be configurable in some way, or i hope so at least."
    author: "nardew"
  - subject: "Re: space"
    date: 2007-09-04
    body: "it's actually not the whitespace, but the alignment of various elements in that dialog that aren't correct. whitespace is a very useful and important design element, and not to blame for your (understandable) reaction to that dialog."
    author: "Aaron J. Seigo"
  - subject: "Re: space"
    date: 2007-09-05
    body: "Either way, it seems like the buttons and toolbar are stealing a good deal of the screen real-estate.  If you're juggling multiple windows, or just want as much of your document/data in view as possible, I like to keep buttons and toolbars pretty tight.  I imagine it can be changed out with a style, so I'm not sweating it too much, but I think it is the biggest drawback from the default appearance."
    author: "T. J. Brumfield"
  - subject: "Re: space"
    date: 2007-09-05
    body: "UI did a fast mockup, compare okular spacings: (yes it's a bad drawning)\nhttp://arstechnica.com/journals/linux.media/kde4-beta2-programs.png\n\nhttp://protomank.googlepages.com/kde4-beta2-programs.png (new spacing in some parts)\n\nBTW, I think that maybe the problem has to do with artists using good and big LCD screens?\nYou know, my old 15\" CRT dosen't like wasted space, and I belive my girlfriend with hers 14\" also dosen't :)"
    author: "Iuri Fiedoruk"
  - subject: "When beta 2 will be released?"
    date: 2007-09-04
    body: "Will the team release also beta2 version of kde-four-live?"
    author: "david"
  - subject: "Re: When beta 2 will be released?"
    date: 2007-09-05
    body: "Isn't it possible to provide nightly builds?"
    author: "bert"
  - subject: "Re: When beta 2 will be released?"
    date: 2007-09-06
    body: "Today: http://www.kde.org/announcements/announce-4.0-beta2.php"
    author: "Fran"
  - subject: "No need for space between scollbar and main window"
    date: 2007-09-04
    body: "It would look a lot better if there was no space between the green scrollbar and the content window to its left (in this case, the content window shows the Ars Technica homepage):\n\nhttp://arstechnica.com/journals/linux.media/kde4-beta2.png\n\nHere's how it should look:\n\nhttp://upload.wikimedia.org/wikipedia/en/2/2d/MH_Mail_%28email_client%29.png\n\nIn the above example, the Mac OS X application's blue scrollbar actually _touches_ the list of email subjects to its left (ie. the orange highlighting). \n\nI would also suggest to our Oxygen artists and coders that there be _no space_ to the right of the scrollbar either. So if the right side of the window touches the right-most edge of the desktop, the scrollbar itself should be the right-most thing on the screen. This again is shown on the Mac OS X screenshot."
    author: "AC"
  - subject: "Re: No need for space between scollbar and main window"
    date: 2007-09-04
    body: "I totally agree.\n\nApple is doing a great job in getting most out of the screen you have."
    author: "Bernhard Rode"
  - subject: "Re: No need for space between scollbar and main wi"
    date: 2007-09-05
    body: "The weird thing is that I think in that screenshot he is using KDE 3 and building KDE 4.  However on principle, I think the design needs to be tightened up quite a bit with regards to white-space."
    author: "T. J. Brumfield"
  - subject: "Re: No need for space between scollbar and main window"
    date: 2007-09-05
    body: "I must admit that I do agree here - I always find it a way frustrating if there is space between the scroll bar and the right border because I sometimes click&drag the border instead of the scroll bar :/"
    author: "liquidat"
  - subject: "Re: No need for space between scollbar and main wi"
    date: 2007-09-05
    body: "Agreed.\nAs you have said, placing borders everywhere isn't only a visual, but also usability problem."
    author: "Iuri Fiedoruk"
  - subject: "Re: No need for space between scollbar and main window"
    date: 2007-09-05
    body: "I must say I really like the way the scrollbars look in Oxygen. They could just ensure the widget reacts on the whole width and keep the look..."
    author: "jospoortvliet"
  - subject: "Correction?"
    date: 2007-09-04
    body: "You say about the kdelibs: \"These libraries include many of the main KDE widgets,\".\n\nI was under the impression that all GUI stuff was moved off the kdelibs and into kdebase, so kdelibs can actually be built without linking to QtGui.\n\nAlso, I agree that the current Oxygen style isn't really good, but remember that other widget styles are available, after all, this is kde =)"
    author: "AC"
  - subject: "Re: Correction?"
    date: 2007-09-04
    body: "kdelibs is a module in SVN, sometimes resulting in one binary package.\n\nIt contains several libraries, including libkdeui, which contains GUI.\nYou were probably thinking about libkdecore, which is also part of kdelibs\n"
    author: "Kevin Krammer"
  - subject: "Hide UTF-8 option from Save File dialog"
    date: 2007-09-04
    body: "http://media.arstechnica.com/journals/linux.media/kde4-beta2-filedialog2.png\n\nKwrite is a general-purpose text editor, not a source code editor (that's kate). It is very rare that you have to change the encoding of a text file. Most users have no idea what's the difference between UTF-8 vs ASCII vs who knows what. And even if they do need to change the encoding to support some special character in their language, it would probably have to be done only once, in the configuration dialog and _not_ in the Save As dialog. Once the change has been made it should remain in place until changed later by the user.\n\nOne could argue that it is up to the distro installer to choose the right encoding based on what language the user specifies during installation.\n\nIn any case, the file encoding type has no place in the Save As dialog."
    author: "AC"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-04
    body: "It's very often that I have to change the encoding in the menu, and be able to specify encoding for document opening and saving. Not every one in the world uses English only - the option is very needed and should be left in the dialog."
    author: "Sergey"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "> Not every one in the world uses English only - the option is very needed and should be left in the dialog.\n\nExcuse me? That's exactly why we have UTF-8 (and not ASCII, etc.) So it supports just about any language.\n\nAnd I think there are good reasons for insisting that UTF-8 is used. Would you prefer to choose between CP-1251, KOI8-R, KOI8-U, etc. just to use Cyrillic alphabet? Oh, and what would you do if you needed to use some non-Cyrillic and non-English letter - in someone's name, etc.?"
    author: "Dima"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "There are cases where you have to deal with old encodings.  That's where you need to choose.  Obviously we all would prefer UTF-8, but sometimes we can't use it for some reason.\n\nA project I was involved in used Windows-1250 (not Latin-2 for the reason that at least we get proper quotation marks -- this part was in fact the result of me pushing for it), and I had no luck at the time (around 2002-2003, the web was already ready for UTF-8 and that's all we needed) to get the project to use Unicode."
    author: "Henrik Pauli"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-04
    body: "I on the other hand am very glad it is there. I have had to work with some files from other sources in all kinds of encodings, and I was very happy to be able to just Save As them in UTF-8. "
    author: "Andre"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-04
    body: "> I have had to work with some files from other sources in all\n> kinds of encodings, and I was very happy to be able to just\n> Save As them in UTF-8.\n\nWell then why not encode all files in UTF-8 and forget about this setting?"
    author: "AC"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "Probably more meant like \"very happy to be able to just Save As them in UTF-8, or whatever else was needed\".\n\nI too am for this option -- but yes, definitely keep it off kwrite, the average user probably doesn't need it; kate on the other hand might benefit of it.  I do very appreciate being able to choose (or at the very least see) the file\u0092s encoding at saving, and I always rely on it on Windows in EditPlus for example.\n"
    author: "Henrik Pauli"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "> Well then why not encode all files in UTF-8 and forget about this setting?\nBecause not every tool in the world can handle that. If you need to share your data, you can't always insist on a single file format, even if you'd want that."
    author: "Andr\u00e9"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "I also think that ASCII/UTF-8 is very geeky. Most people don't even know what it means. Why not just set UTF-8 as the default encoding everywhere, and have a config option for default.\n\nKWrite is the general text editor. For those who need advanced features of all sorts can have a specialized editor."
    author: "anonymous"
  - subject: "do you have to edit in non-English locales?"
    date: 2007-09-05
    body: "try Russian sometimes - we have 4 different encodings. This may change your prospective whether this feature is \"geeky\" or something you can't survive without. My only complain is it not prominent enough - the combobox just says \"UTF-8\", it also should have a label \"Encoding\" with proper keyboard shortcut. KDE5 perhaps?"
    author: "Sergey"
  - subject: "OK, but give the dropdown a sensible label"
    date: 2007-09-05
    body: "OK, so if languages with non-Latin alphabets use this option extensively, then leave it in. But I agree with Sergey: at least give the dropdown menu an understandable label, and not just the cryptic UTF-8."
    author: "AC"
  - subject: "Re: OK, but give the dropdown a sensible label"
    date: 2007-09-06
    body: "It's not just non-Latin alphabets but rather non-Latin-1 languages. If you use English exclusively, you don't need to care. For Latin-1 languages with diacritics you only need UTF and 8-bit. For Latin-2, you can definitely save all your files in UTF-8, but you absolutely need at least three encodings if you want to be able to exchange text files with others: UTF-8, ISO-8859-2 and WIN-1250. And you can still come across legacy encodings such as CP852 (DOS), MacOS CE, KEYBCS2, KOI-8, UCS-2 ... \nJust because average users somewhere never heard about encodings should mean that average users elsewhere should be forced to manually convert most of the files they send or receive with iconv?"
    author: "giahra"
  - subject: "Re: do you have to edit in non-English locales?"
    date: 2007-09-05
    body: "It's good that you insist on having and improving this option, I like that. :-)\n\nBut does it belong in the save dialog? Why isn't it something that is a (guessed, I think Strigi was said to be able to do that) property of the document once loaded, that can be changed?\n\nI think it was done like that in Emacs.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "I don't generally support removing options because end-users are too stupid to understand what they are.  That is the primary thing that turns me off in regards to Gnome design.  I think the option to select text-encoding should exist within the application, however I'm not sure it needs to be in the \"file save\" dialog, which I'm assuming is going to be fairly universal in KDE.  If it needs to be in the dialog, so each application designer doesn't need to place it elsewhere in the app, then simply tone down the size of that option.  If it is smaller, it isn't as likely to confuse or attract people who shouldn't change it, while not removing it completely."
    author: "T. J. Brumfield"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "I think it'd only make sense in text editors' save file dialogs -- it should be completely hidden in every other case, including saving anything from Konqueror (who wants to transcode a file when they save it off the web? probably a very small minority who can live just fine with iconv anyway).  So I guess it could be an option for the programmer, available inside the API (a simple flag for the save dialog), and a sensible programmer will offer it to its users (or get told to offer it, ehehehhe)."
    author: "Henrik Pauli"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "right now this option is present only in text editors - you don't have it in file dialogs of image editors, for example. This feature is very useful, imo. Perhaps, the combobox should be next to the filter combobox, and have a label. "
    author: "Sergey"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "<quote>Kwrite is a general-purpose text editor, not a source code editor (that's kate).</quote>\nWhere did you get this? KWrite is SDI and Kate is MDI, but they are pretty much the same in every other aspect.\n\n<quote>In any case, the file encoding type has no place in the Save As dialog.</quote>\nThat's your opinion (and I'm guessing you never had to deal with multiple encoding environments). I think encoding has an important place in a general-purpose text editor save dialog and other applications \"agree\" with me: gEdit and EditPlus (on Windows) have that option... heck, even Notepad has it."
    author: "attendant"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "In my not so humble opinion, this is rather official stuff:\n\nhttp://www.kate-editor.org/kwrite\n\nQuote:\n\n---\n\nKWrite is a simple text editor application, allowing you to edit one file at the time per window.\n\nKWrite is using the KTextEditor interface to let users decide which editor component they want to use. The default text editor component in KDE is the KatePart.\n\nKWrite simply provides the selected editor compoenent with a window frame, and lets you open and save documents. Being a KDE application, KWrite is network transperant \u0097 you can open files via any protocol supported by your system.\n\nIf you use the default KatePart component, KWrite shares all features the KatePart provides, look here to get some overview.\n\n---\n\nSo more or less Kwrite is indeed a single window shell for KatePart, like Kate is one for multiple windows. But projects, etc. cater to developers methinks, not?!\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "\"Automatically select filename extensions\"-- this should be a general setting and not displayed. the most important thing is to have a huge main window with all the files."
    author: "bert"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "That just sounds to me like you have nothing to complain, so you're complaining about things which everyone has agreed on for years. What people agreed on for years, can not be that bad."
    author: "Stefan"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-05
    body: "My very first reaction when seeing that dialog was the opposite; \"Yes! That's the way the encoding option should be presented! I'm so glad it is there!\"\n\nThen again, I might be a geek. On the other hand, and this is not an attack on the parent poster, I seem to have noticed that, often, suggestions that involve the phrase \"most users\", are perhaps not as well thought through as they could be?"
    author: "Martin"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-06
    body: "Full ACK. \"Most users\" is a myth. My mom wouldnt even notice nor think about an option she doesnt understand. She doesnt understand about 90% of all options, buttons, menus. Nevermind. She opens a file. Writes something. Prints. Closes it. That's all. You could put the whole screen with UTF-8, ASCII, whatever drop-down boxes. She wouldnt even care. That's why it is so exceptionally stupid to remove options for \"most users\" like Gnome does. This alleged user group doesnt even exist! BTW: I bet a proposal to remove that option can only come from someone who speaks English natively. It's the same people we can thank for having sth. like ASCII, 7-bit-Mails, Win-1252 in the first place. Absolutely beyond their understanding is that someone actually speaks two or more different languages and has to work with files encoded differently every day, in Cyrillic and German for instance. Google now gives different results when querying in German or English BTW. If you understand both languages (like most non-English-speaking-people do at least to some degree) you must now query 2 pages to get all relevant results. Grrrrr.\n"
    author: "Michael"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-06
    body: "Well, on one hand, I can see why the Gnomes are trying to clean up the interface. And to a certain extend, I think it really does make a difference. But I think it hurts far more to have a feature NOT there when a user needs it, than to have a feature there and ppl don't need it."
    author: "jospoortvliet"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-08
    body: "Agree 100%, not just because it makes sense, but also because I see the same thing from my family. If they don't understand something they leave it."
    author: "riddle"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-09
    body: "OFF:\nGo to Google (English)/Preferences/Search language\nSelect the languages you want."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Hide UTF-8 option from Save File dialog"
    date: 2007-09-10
    body: "why not add to the preferences dialog the option (as a checkbox) to display the encoding options?\nsomething like this:\n* Display character encoding options in the Save As dialog\nby default, this would be disabled, cause most users don't need it. those who do need it have the option to turn it on permanently.\nadd a drop-down menu after that that says:\nDefault character encoding: <>\nthe <> mark means a drop-down menu. by default, the default encoding is utf-8.\nyou could also have\n* Remember the last used encoding throughout session\n\nthis means that if you change the encoding, it stays the default till the end of the session, making it easier to work with a large number of files that use an exceptional encoding without changing the default. of course, at the end of the session you go back to the \"real\" default, that is defined in the drop-down list above.\n\nI'm not familiar with KWrite, so I don't know how suitable what I wrote is."
    author: "yman"
  - subject: "All white"
    date: 2007-09-04
    body: "I really hope that this confusing and ugly \"all white\", mixing Toolbar, menu and title bar is just a work in progress, not the final design.\n\n"
    author: "Julio"
  - subject: "Re: All white"
    date: 2007-09-04
    body: "Well, it's definitely not the final design."
    author: "Hans"
  - subject: "Re: All white"
    date: 2007-09-05
    body: "All drafts I've seen of Oxygen seem to suggest it will be very \"clean\", minimalist, and focus heavily on white.  Given how popular OS X clones are on kde-look.org, I firmly expected KDE 4 to look more like the Milk/Baghira clones out there."
    author: "T. J. Brumfield"
  - subject: "Re: All white"
    date: 2007-09-05
    body: "No its not all white, the problem is the color pallete being broken and we are currently fixing it.\nThe contrast issues has alot to do with that big problem that the developrs are fixing.\nhttp://www.nuno-icons.com/images/estilo/ \nHere you will find most of the work mocks we are using for oxygen style has you can see its not all that white and i will make color corretions wen the pallete is fixed so it fits well enough on not so well tuned tft screens."
    author: "pinheiro"
  - subject: "Re: All white"
    date: 2007-09-06
    body: "These mockups look really great.  Keep up the good work, I hope KDE4 can look like this."
    author: "joe"
  - subject: "Re: All white"
    date: 2007-09-06
    body: "I've seen these mock-ups before, and I think they are gorgeous.  If the final product looks like that, I'll be very happy indeed, but none of the actual screenshots I've seen look like that."
    author: "T. J. Brumfield"
  - subject: "Re: All white"
    date: 2007-09-06
    body: "In those mockups:\n\nhttp://www.nuno-icons.com/images/estilo/version%203%20tabs%20and%20version%2021%20scrolloptional.png\n\nYou have some sort of doc with the KMenu in the middle, sys tray on the right, and open windows on the left.  Is there a program/method for me to do that right now?\n\nI just love how that looks."
    author: "T. J. Brumfield"
  - subject: "Re: All white"
    date: 2007-09-06
    body: "It's cool.\nbut the bottom panel is somewhat bigger for old small CRT.\nI think it may add a button like '-' at top-right of panel,\nto lay down the panel.\nI mean when user pushes the button,\nthen the panel become a slim bar,\nand reverts it when pushes again."
    author: "Alex"
  - subject: "Re: All white"
    date: 2007-09-05
    body: "Oh dear, I was hoping the default windeco would be a green gradient to match that cute green arrow widget rather than the present monotonous gray/white with such awful contrast (no offence meant to those who came up with the theme - it's just that the buttons don't really stand out against the pale background). Oh well, one hopes that it is still a work in progress, rather than the final product."
    author: "Lee Grant"
  - subject: "Re: All white"
    date: 2007-09-05
    body: "I know this mockup is old but I find it accurate :\n\nhttp://www.kde-look.org/content/show.php/Kde4+Mockup?content=28476\n\nSimple and clear with sharp edge and few spaces."
    author: "Batiste"
  - subject: "Re: All white"
    date: 2007-09-05
    body: "That mock up is going to make a lot of people very disappointed.\n\nBut, in the end someone will create a theme that looks like that and post it on \nkde.looks.org.\n\nBy the way, where can I find that wallpaper?"
    author: "reihal"
  - subject: "Re: All white"
    date: 2007-09-05
    body: "As KDE4 will support creating themes with SVG, I belive we'll be seeing nice themes like that very soon :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: All white"
    date: 2007-09-05
    body: "Perhaps you should take a look at the artists' mockups which have a bit more contrast. The final design might look like that:\n\nhttp://bp3.blogger.com/__JNFVYfijS4/RnLgXQrK8JI/AAAAAAAAAGc/5dWv2oYi9aM/s1600-h/rect3364.png"
    author: "donju"
  - subject: "Re: All white"
    date: 2007-09-05
    body: "Yep, please add color to the titlebar with focus. Everthing I've seen ultil now about Oxygen style made me belive the final version is intented to be all white yes."
    author: "Iuri Fiedoruk"
  - subject: "On the Theme"
    date: 2007-09-05
    body: "White on white.  I can't even see where a window begins and where it ends.  Or where a widget begins and ends.  It's kinda mystery meat to me.  Other than that, it's okay, I like the shapes and all.  Definitely more contrast please."
    author: "Henrik Pauli"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "you *NEED* to use composite with window Shadow !!! with the theme , you will see the edges beautifully..."
    author: "composite with window Shadow "
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "Some of us can't use compositing. Or, is that part of your point?"
    author: "Soap"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "Composite has issues on most of the computers I have access to.\n\nA default theme should look decent in all reasonable configurations.\n\nI really hope the final version will take this problem into account, as well as being more accessible (all other window decorations allow for larger border sizes, for example, to make window handling easier).\n"
    author: "Luciano"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "Can't use it.  Please tell AMD/ATi to make a good driver.\n\nEven if I could... I'm not sure anyone should be required to.  That's kinda cutting off a huge amount of people from using the default (emphasis: default) theme, which is just plain silly."
    author: "Henrik Pauli"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "Amazingly AMD/ATi just announced they're finally gonna give proper support to AIGLX in the next driver version (not the one thats supposed to come out in a day or two, but next month I think).  Unfortunately that could be good or bad for those of us that use the FLOSS ati driver (it could make the developers less motivated, or it could give the developers a better source to get information from).\n\nI'm glad that KWin will be able to fall back to no-composite mode, since right now composite would kill my laptop (hell, just typing that last sentence the desktop wasn't refreshing fast enough to follow what I was typing... and still typing!)"
    author: "Sutoka"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "Colored pixels cost extra money."
    author: "T. J. Brumfield"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "Hahaha"
    author: "Coward"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: ":D\n\nThat was a good one :)))"
    author: "Henrik Pauli"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "Totally agree.\n\nThere is no contrast at all.\n\nI am  sure there will be a configurable outer liner for the final windeco.\n\nI hope, there will be a highlighted menubar.\n\nI hope there will be more contrast and more courage in using more colors."
    author: "Sebastian"
  - subject: "and what's up with ugly green scrollbars?"
    date: 2007-09-05
    body: "entirely out of place and have absolutely no connection with the rest of the style. Plus, they look like a blatant ripoff of macosx/vista."
    author: "Sergey"
  - subject: "Re: and what's up with ugly green scrollbars?"
    date: 2007-09-05
    body: "Second that."
    author: "Divide"
  - subject: "Re: and what's up with ugly green scrollbars?"
    date: 2007-09-05
    body: "Well, for sure they are temporaries to show it's work in progress!"
    author: "Luciano"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "I think KDE artist team wants to economize energy on LCD screens as Google does :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: On the Theme"
    date: 2007-09-05
    body: "On the last moths people have been complaining a lot About the lack of contrast and the all white issues on the oxygen style, i have said also lots of times that it is mostly a pallet issue. But the message is not getting out, so... i will do what i always do... Show pretty pictures.\n\n http://nuno-icons.com/images/estilo/\n"
    author: "pinheiro"
  - subject: "Re: On the Theme"
    date: 2007-09-06
    body: "Don't worry, the message willget out fast as lightning when it's actually fixed :) Just don't get too upset, you guys are doing a great job, I already love the icons!"
    author: "bsander"
  - subject: "Borders vs No Borders"
    date: 2007-09-06
    body: "Anyone remember all the bitching about how KDE 3 had \"extra\" borders and people demanded that they cut down on borders?\n\nYou now have a clean look without borders and everyone is demanding that borders be put back in."
    author: "T. J. Brumfield"
  - subject: "Re: Borders vs No Borders"
    date: 2007-09-16
    body: "Took me a while to see the comment and you'll likely never see this reply, but here goes:\n\nThose borders were about frames inside the application, which do sometimes stack really horribly.  I was talking about the window border for first, and for example button frames (which is a bit different from having a frame in a frame in a frame, like some KDE3 apps do) -- though, yeah, the second one probably wouldn't fit all that well into this theme.  But I sure hope I will be able to see at least a little where my LineEdits and ComboBoxes begin and end."
    author: "Henrik Pauli"
  - subject: "An idea"
    date: 2007-09-05
    body: "How hard would it be to universally remove the menu bar and compress it into one tool bar button? Kinda like what IE 7 can do (but not so chaotic). Similar to KDE3 where you can stick the menu bar up top Mac style, you could have an option to remove the menu bar and put it into a button at the beginning of the tool bar (with the application's icon, and a label \"Menu\").\n\nIt'd save a lot of space, look cleaner and only cost one click to any action that's in the menu. I don't know, maybe it's a bad idea, but I think it'd be a cool option."
    author: "chair"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "It's not a terrible idea, but not something that would likely ever become the default. It's probably not even that difficult to implement if someone was willing to take a crack at it.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "Yeah I'd give it a try but I'm only new to programming (did a semester of Java and I'm doing C right now). But I want to get into KDE eventually, when I have more time.\n\nThanks Troy, and thanks for all the work you do to keep us informed :)"
    author: "chair"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "Probably not hard at all, Amarok already do this. However, it would be great if you could move/remove the toolbar button (like any toolbar items)."
    author: "Hans"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "Great idea! If you put in a wishlist item in kde bugzilla, I vote for it!!"
    author: "cossidhon"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "O, I forgot to mention. Doesn't OS-X has something similar? I has, if I remember correctly, buttons in the title-bar with a hide/show functionality for either the menu-bar or the tool-bar (or both).\n\nYeah, that's neat: Create an extra button in the titlebar; left mouse-click on it shows/hides the menu-bar, right mouse click on it shows/hides the tool-bar, And maybe: middle mouse click shows/hide statusbar. Put in a little animation for the wow factor. This way you can really save on screen real estate and still keep total control.\n\nAnd I agree, I think it's easy to implement. Most of the code most be already there, just implement the extra button."
    author: "cossidhon"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "This could probaby be done, since it can be considered a variation of the top-level (macos like) menu bar. Accessing menus would be a click further away, though. I find the toplevel menus are easier to use. Last time I tried there were problems with using them in KDE4, but I hope that will be fixed before the release."
    author: "Luciano"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "In fact, I believe all that hide/show functionality is already in the \"window\" menu in the upper left corner, but i'm not sure, I can't check right now. So it would only boil down to a \"shortcut\" button in the title bar, And if you don't want it, remove it using the \"windows decoration kcontrol\".\n\nI say it again, I think it's a GREAT idea!!"
    author: "cossidhon"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "The same debates pops up again and again.\n\nExample: http://dot.kde.org/1173332156/1173340543/\n\nWaste of screen space, low contrast icons and widgets, artsy fartsy fear of colour.\n"
    author: "reihal"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "Maybe because artists aren't taking notes and changing what people thinks is wrong?\nOr maybe because they are doing so, but not right now?\n\nWho knows? :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: An idea"
    date: 2007-09-06
    body: "the reason is that it's just not ready... pretty simple. They have mockups, go check them out, there you see the target look.\n\nhttp://pinheiro-kde.blogspot.com/2007/09/ok-its-time-to-show-somthing-more.html"
    author: "jospoortvliet"
  - subject: "Re: An idea"
    date: 2007-09-06
    body: "I did, before posting here in the dot I posted in Pinheiro's Blog and liked his answer a lot (basically the problem is that there are too much hardcoded coloring and other style parts)."
    author: "Iuri Fiedoruk"
  - subject: "Re: An idea"
    date: 2007-09-05
    body: "The code for the top-level menu bar is still from Mosfet a bit buggy.\n\nHowever, it works for most of us. The real problem is that there is no way to use GTK applications or QT ones and have a top-level menu bar. I think a top-level menu bar is very convenient if it is supported."
    author: "bert"
  - subject: "Re: An idea"
    date: 2007-09-06
    body: "I already use an extension to that in firefox. It realy save screen space, but in some apps that I use heavily the menu I would like the defaut menu."
    author: "Manabu"
  - subject: "what's the point?"
    date: 2007-09-05
    body: "They hack a new KDE and the discussion goes about the *eyecandy*. do you buy a new car depending on its colour? I think there will be a lot of themes and icons and klicki-bunti-stuff. If you do not like the default look, there is kde-look.org for you pleasure.\n\n"
    author: "name"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "KDE3's poor aesthetics and generally bad first impression are often cited as one of the reasons why so many people are now switching to GNOME.  Sure, people willing to overlook the rough edges might be willing to go over to kde-look.org and compile a new theme(!), but this certainly doesn't apply to the majority of potential users.  It would be a huge shame not to fix this defect for KDE4."
    author: "Anon"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "To me \"poor aesthetics\" is an issue of interface layout, and not of theming.\nThe real problem is overall, control panel use different convention with regard to aligment and size of common widgets (combo boxes  and color boxes too long is my biggest annoyance, though I've learned to ignore the problem with time) and that can't be fixed with a theme."
    author: "Luciano"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "KDE comes with more than just one theme installed, so the users are not forced to compile anything, they can just click on the decoration they want. \n\nI mean that functionality and ease of use is more important than eyecandy. On the other hand (or eye ;-)) I understand that eyecandy will impress Windowsusers more than a naked and effective commandline. \n\n\"why so many people are now switching to GNOME.\"\n\nYou think they do it because Gnome has more bling? "
    author: "name"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "\"You think they do it because Gnome has more bling?\"\n\nI've read hundreds - possibly thousands - of testimonies from GNOME lovers (or KDE haters ;)), and mostly they cite GNOME's clean-looking widgets, its uncluttered and carefully-designed UIs of both the DE and core apps, its consistency across applications (a result of GNOME's far more detailed HIG) and just general the general look and feel of GNOME/ Gtk.\n\nTechnically, they seem to like GNOME precisely because it has *less* bling, but what it has is judged to be well-chosen.\n\nThey dislike KDE because it is judged to be ugly in terms of default themes, windecs and icons (remember: the KDE guys once thought it was perfectly acceptable to choose *Keramik* as the default style!), loud, flashy, blingy, \"in-your-face\" and cluttered - basically, they view it as something that was, from a UI point of view, thoughtlessly thrown together by people with poor, or no, taste.\n\nPersonally, I don't care about looks (I'm much, much more interested in KDE's functionality, excellent technology, and progressive, vital and helpful dev community), but a *very* substantial portion of people prize it above all else.  \n\nWhat I found the most interesting from my researches is that surprisingly few people cite confusingness or lack of usability of KDE as a reason they don't like it - it's nearly all fastidious exclamations of distaste."
    author: "Anon"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "I agree with you. I've always sticked with KDE for the technical reasons. It's way better \"architected\". But for looks? No. KDE 3.5.7 can be made good looking, but looks is a taste thingie, and people tend do disagree on that a lot :-). Most people believe however KDE doesn't look as good as Gnome on first impression, and, as I said in previous posts, first impression matters a lot! I'm sure however, that KDE4 will make a marvelous first impression, even with 4.0 (it better be!!)"
    author: "cossidhon"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "It was the other way around. Gnome was brown, boring and ugly. They branded it as \"for the industry\" while KDE had the bling. finally they kind of fixed the file dialogue, well, not really.\n\nNow: Honestly themes and DE are two entirely different issues. \n\nGet Abiword with Crystal Icons and it will \"feel KDE\". Get a QT application as Scribus and the Ubuntu guys will say that it \"integrates with Gnome\".\n\nForget the superficial stuff. Theme design is an issue you can hand over to a designer but a designer cannot solve a cluttered library. If you like gnome Themes go an port them to KDE. I really hope that on the long run both desktop environments will kind of merge.\n\nLook carefully at Windows applications and you will get that there are several different generations of graphical interfaces and API. But users don't really notice it. Similar a user should not even mention if a GTK application is running under KDE or a KDE application under Gnome."
    author: "bert"
  - subject: "Re: what's the point?"
    date: 2007-09-07
    body: "\"Look carefully at Windows applications and you will get that there are several different generations of graphical interfaces and API. But users don't really notice it. \"\n\nThat's exactly why KDE exists under the unix world and what they tried to prevent.\nhttp://kde.org/announcements/announcement.php\n\nThere's a reason Mac OSX is so highly praised. Even when different widgets libraries are used, they all try to follow the Apple guidelines as much as they can and there's no competing operating system that can give you the same feelings.\nWindows is a mess of different GUI behaviors between the software just like the old unix days. \n\nWe are lucky there's only QT and GTK that got some use under UNIX-like oses. (and some Motif)"
    author: "Anon"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "> do you buy a new car depending on its colour?\n\nActually, I think it's a very important part for most people. \nI think Henry Ford was proven wrong quite a few years ago."
    author: "G\u00f6ran Jartin"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "The point is, that if KDE4 is deemed crap by the new users due to the issues with the eyecandy (which is part look-good, part make-things-useable, the second one being more important), then Houston, we've got a problem.\n\nThis doesn't mean that I'd think the Oxygen style is crap -- far from it, I think it's nice.  Except for that it's \"unreadable\" and therefore hard and/or uncomfortable to use as a widget theme.\n\nA software, especially a whole desktop environment is very different from a car, and unfortunately its looks is much more important than the colour of a car.  Well, actually, you are making a bad point.  People like to buy cars that look pleasing to the eye (whereby I mean not the colour, but the shape), and don't like to buy cars that are ugly.  And some people will criticise a car for not being streamlined or things around the steering wheel not being at a correct place, just as we will criticise a widget theme that's not particularly easy on the hands/eyes/reflexes/whatever."
    author: "Henrik Pauli"
  - subject: "Re: Ok then"
    date: 2007-09-05
    body: "When more style brings more users to OpenSource, than more style is needed."
    author: "name"
  - subject: "Re: what's the point?"
    date: 2007-09-05
    body: "Yes"
    author: "Coward"
  - subject: "Well done"
    date: 2007-09-05
    body: "I want Thank you for your great works and efforts.\n\nOxygen theme and Icons team: Really, You have a good taste!!\nPlasma team: keep it up!! You are doing so great.\nKDE developers: Well done and thank you.\n\n"
    author: "zayed alsaidi"
  - subject: "Re: Well done"
    date: 2007-09-05
    body: "Finally, out of all the above \"critical\" posts,\nI see someone actually appreciate all the work\nthat went in to this release.\n\nI would also like to take a moment to appreciate\neveryone who is working on KDE 4 and would like\nto say that all of you are doing a marvelous job.\nOxygen looks great, and Plasma seems to be heading\nKDE into the right direction for the 21st century.\nAfter reading all the posts above, I see that many\npeople are forgetting that this actually IS the\n21st century, and that Gnome is not sufficient for\nevery-day use anymore, unless you are a system\nadministrator or a power user.\n\nKeep up the good work! KDE 4 will be a very big hit!"
    author: "Rus"
  - subject: "Re: Well done"
    date: 2007-09-05
    body: "I think Troy should also be added to the list (he wrote the article), along with Danny (commit digest guy) and a lot of people that don't really fall into any of the other categories exactly. I'm really liking the look of the plamsa-kicker, especially how the colors from it work with the default wallpaper, and the interesting contrast with the very bright windows.\n"
    author: "Sutoka"
  - subject: "Responsive?"
    date: 2007-09-05
    body: "A question that can't be answered from screenshots - how responsive does KDE4 feel? Do nested menus spring up quickly, or are there pauses? How quickly do applications launch? How does it feel when you grab the scrollbar on a 100 page pdf document and flick it up and down - does it need time to think about things?\n\nResponsiveness is the killer for me - I find it completely different when apps take 10s instead of 2s to start, or everything takes a few 10ths of a second to respond when clicked. I'm not looking for numbers, I'm happy to take opinions!"
    author: "Andy Allan"
  - subject: "Re: Responsive?"
    date: 2007-09-05
    body: "Let's hope the new fancy themes and Plasmoids dion't kill the gain of speed and memory we got with Qt4."
    author: "Iuri Fiedoruk"
  - subject: "Re: Responsive?"
    date: 2007-09-05
    body: "Imho, it's bad. Not extremely, but I hope the final version will be a lot better...\n\nOf course, it might have to do with all the beta stuff, unoptimized and all. And because of KWin not being really fast, and the debug stuff in there. So I have good hope."
    author: "jospoortvliet"
  - subject: "What about the dialogs..."
    date: 2007-09-05
    body: "Hi,\n\nIn the middle of all that discussion I cannot read (or see) anything about the dialogs, specially the file ones (copy/move/open/save, etc). In all the screenshots, the dialogs are the same of the KDE 3 series, heavily inspired by Windows (2000).\n\nSorry to say, but I really dislike all that dialogs (they are too big sometimes), especially the copy/move one, because it displays too much information, making it difficult to find the what you want.\n\nAre you planning something different for KDE4? Or the dialogs will remain the same?\n\nAs a old MacOS user (mostly versions 7, 8 and 9) I suggest that you make the dialog simple, with less information but with a \"drop arrow\" that expand the dialog to show more information, only when needed.\n\nBye, and keep your good work."
    author: "kde.fan.from.brasil"
  - subject: "Re: What about the dialogs..."
    date: 2007-09-06
    body: "some work has been put in these dialogs, but more has to be done yet. I think most of that will be 4.1 stuff."
    author: "jospoortvliet"
  - subject: "One thing i'd like to see in KDE4"
    date: 2007-09-06
    body: "I'm sure i had this option a long time ago but i may have been dreaming.  i would like to see a \"pin\" on the title bar of each window to allow me to keep a window on top instead of 3 clicks via the advanced menu.  Is it configurable or theme specific or i was dreaming?\nI do a lot of cut and paste between spreadsheets and documents and dont have the luxury of dual screen.\n\nps: and will okular print collated for X copies of a multipage document? KPDF can't manage it, it always prints all the page 1's then page 2's etc then i have to collate it myself \n\nI am really looking forward to KDE4 (except that theme in the article - the \"save as\" dialog looks awfully busy with no borders separating the constituent parts) \n\nKeep up the great work, it is appreciated."
    author: "Ian"
  - subject: "Re: One thing i'd like to see in KDE4"
    date: 2007-09-06
    body: "Hi Ian,\n\nIf I remember correctly (I'm sitting on a Windows machine at the moment), you have to go to the \"Window decorations\" settings in the Control Center. The second tab allows you to add and remove buttons in the title bar. You can probably also get to that setting dialogue by right-clicking on the bar and choosing the settings menue entry."
    author: "pin-king"
  - subject: "Re: One thing i'd like to see in KDE4"
    date: 2007-09-06
    body: "And if you don't want to use the mouse at all, you can set a shortcut key in KControl/System Settings."
    author: "Hans"
  - subject: "Re: One thing i'd like to see in KDE4"
    date: 2007-09-07
    body: "THanks Hans, \n\ni'll try and sort that out over the weekend - always good to have more than one way to skin a cat.\n\nregards\n\nIan"
    author: "Ian"
  - subject: "Re: One thing i'd like to see in KDE4"
    date: 2007-09-06
    body: "hi pin-king\n\nThat worked a treat once i stopped using the Suse decorations.\nThanks for that.\n\nregards\n\nIan"
    author: "Ian"
  - subject: "Already supported in 3.5 . . ."
    date: 2007-09-10
    body: "In 3.5:  kcontrol->Appearance & Themes->Window Decorations->Buttons.  Click \"Use custom titlebar button positions\" and drag and drop \"Keep Above Others\" to your titlebar."
    author: "Tom"
  - subject: "What really matters?"
    date: 2007-09-06
    body: "I think most Linux users are pleased with a well-integrated and responsive shell, for Windows users you must provide an as simple and less confusing structure, the windows key has to open the menu, for Mac users eye candy and usability. All this is no problem if it just work. That was always just a matter of configuration. But what really matters is unique stuff.\n\nPlasma' vision looks to me a lot like these Microsoft Silverlight Demos from Miguel. http://tirania.org/blog/pic.php?name=screenshot46.png&caption=Halo3%20Site%20on%20Linux.\nIn fact it is a Halo3 frame. \n\nwhat I find more important is technology leadership. Does KDE support VOIP calls? Video Conferencing with my friends? Collaborative editing? \n\nCan I record and edit my videos?\n\nEasy upload of your ripped media as torrents?\n\nDoes Konqueror support all relevant Ajax applications?\n\nCan you easily record the screen?\n\nCan you easily upload your KOffice slides to slideshare?\n\nDoes your application choice the right executor for alien files such as .exe (mono, wine, dosbox, ..). Is context menu configuration intelligent?\n\nIf your scanner doesn't work will your system help you to contact the right forum, find the right help page?\n\nAre all unnecessary popups eliminated?\n\nDoes the environment enable and facilitate user contributions to extend the system? E.g. a dictionary: adding new words and uploaded them.\n\nIs code reviewed on a regular base to avoid kludge?\n\nIs upcoming hardware such as USB and wifi telephones supported and integrated?\n\nSpeech and speech recognition.. we know it is a long way, but its important for accessibility.\n\nDoes it enable me to print easily birthday invitations? \n\nDoes it simplify my ebay accounting and home banking?\n\nCan I manage my small business or my local firefighters club with KDE applications?\n\nAre my files deleted safely?\n\nWill web radio just work? Will I also be able to easily record it?\n\nWill KDE excite my MEP because it also supports our regional language?\n\nWill the new KDE excite my girlfriend?\n\n...\n\n\n \n\n"
    author: "Martin Ronde"
  - subject: "Re: What really matters?"
    date: 2007-09-06
    body: "Most of these things can be answered with either \"yes\" or \"we are working on that, hold on until 4.1\" so rejoice."
    author: "jospoortvliet"
  - subject: "Re: What really matters?"
    date: 2007-09-06
    body: "Will it make coffee the way I like it?\n\nWill it massage my neck while I'm using the computer?\n\nDoes it leave a delicious peppermint taste in my mouth?\n\nCan it automatically locate and download porn to match my fetishes?\n\nIf not, you can keep that crap.  I think my expectations are reasonable. :-)"
    author: "Louis"
  - subject: "Re: What really matters?"
    date: 2007-09-06
    body: "If not, you can keep that crap. I think my expectations are reasonable. :-)\n\n---> Very good proposals ;-) Peppermint may take some time, you know the /dev/mezmer support."
    author: "Martin Ronde"
  - subject: "Re: What really matters?"
    date: 2007-09-07
    body: "Will it make focus follow mind?\n\nWill it supply the monkeys with bananas?\n\nWill it make annoying trolls disintegrate?\n\n\n"
    author: "reihal"
  - subject: "One thing KDE developers to has to understand!"
    date: 2007-09-06
    body: "One thing KDE developers to has to understand is what is meant by ugly. Solly ..."
    author: "AC"
  - subject: "Re: One thing KDE developers to has to understand!"
    date: 2007-09-06
    body: "What?"
    author: "Anon"
  - subject: "Object oriented desktop environment."
    date: 2007-09-06
    body: "I just hope KDE will some day become a fully object oriented desktop environment. Just the way OS/2 was."
    author: "szlam"
---
At <a href="http://arstechnica.com/">Ars Technica</a>, I have put together an article <a href="http://arstechnica.com/journals/linux.ars/2007/09/04/testdriving-kde-4-beta-2-or-thereabouts">detailing my impressions</a> of KDE 4 Beta 2 (more or less, my source checkout is from within 24 hours of beta 2 being tagged last week). An official beta 2 announcement should be arriving shortly as the distros have been packaging it this last week. I am happy to say that beta 2 has made significant progress since beta 1.



<!--break-->
