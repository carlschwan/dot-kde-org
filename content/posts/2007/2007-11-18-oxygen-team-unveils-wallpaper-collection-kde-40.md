---
title: "Oxygen Team Unveils Wallpaper Collection for KDE 4.0"
date:    2007-11-18
authors:
  - "dvignoni"
slug:    oxygen-team-unveils-wallpaper-collection-kde-40
comments:
  - subject: "I just hope distros use them"
    date: 2007-11-19
    body: "Since most distros apply their own branded wallpaper. Aesthetics in KDE4 get better every day, although I do wish it was an open poll. "
    author: "Skeith"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "compared to the results when we have done open polls, i think we got better and more consistent results from having a jury of artists. it's fun to participate in polls to pick things, granted, but i'd rather have high quality results for something like this ... \n\nthanks to everyone who contributed wallpapers. they rock =)"
    author: "Aaron J. Seigo"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "Totally agree on both counts."
    author: "Dado"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "I don't, I hope distro's vote no confidence in these and keep their own.  I find the wallpapers to be very boring, nothing there reaches out and say's KDE, actually they all remind me of Vista. and XP.  I wonder how many people use any of the themes, wallpaper, etc that comes with KDE, I know I don't, and it appears a lot prefer to download their desktop look.  Perhaps the KDE team should not focus on themes, wallpaper etc, and leave that up to those that contribute ate kde-look.org, always far better stuff from contributers, than what is released with KDE.\n\n"
    author: "dreamer"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "Sure, and those artists would then automagicly transform into coders who can be assigned to any other part of KDE, right?\n\n*Sigh*"
    author: "Andr\u00e9"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "Since kde-look.org already are supportet through the GetNewStuff(?) functionality, that part of it arre already covered. \n\nBut IMHO it's better to also supply some high quality defaults like this, rather than base it on whats avalible on kde-look.org. And to be frank, there are lots of rubbish on kde-look.org. Specially when it comes to walpapers. "
    author: "Morty"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "Sorry but where do you see a similarity with Windows Vista? It is a common but wrong conception to argue that KDE is too Windows-like (and by the way the most Windows like element - Kicker - is gone in KDE 4!).\n\nKubuntu did apply a theme that resembles a bit to Windows Vista (window controls and some brilliance highlights) but this very theme has nothing to do with the KDE project itself (and I personally stick to Plastik for KDE 3).\n\nAnd furthermore did you compare KDE with Keramik theme to Windows Vista? There are some subtle interesting similarities (e.g. at brilliance highlights, whereas Plastik doesn't use them). And guess which theme is much much older than Vista? Right Keramik. And it is a known fact that Microsoft evaluated KDE the days it used Keramik as default theme.\n\nAnd where do you see any similarity between Vista's and KDEs wallpapers? They both partially make use of HDR imaging, a careful adjusted dynamic range and a high color saturation (whereas KDE 3 wallpapers were much more \"flat\" and \"dirty\"). Shall KDE people avoid these common techniques of professional photo artwork in order to please you? Just have a look at professional art color photos and you will notice that this has nothing to do with Vista but with images looking good under any circumstances (ever wondered why your private images look good in very narrow screen configurations only?)\n\nSo quite the contrast: Even default KDE 3 doesn't look very much like Windows XP or Vista and KDE 4 will even look more different cause of different GUI elements (not only replaced Kicker) and cause Oxygen theme has absolutely nothing in common with Vistas theme (I bet after KDE 4 is out random people will complain that Oxygen icons and KDEs interface are too GNOME like which is much more likely :p).\n"
    author: "Arnomane"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "I would agree if kde-look wasn't infested by kids that vote their\ntrashy entry and their friends , that vote them too.\nThis issue is particularly visible on linux game tome and freshmeat,\nbut it pests kde-look too...."
    author: "nae"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "Wallpapers are meant to be 'boring'.\n\nAnd what's boring about nature? Are you sure you're human?"
    author: "Blah"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "What is Nature? Everything?\n\nBecause you know, just showing some plants isnt \"nature\" per se. Its just a tiny fraction of what beauty you can find, yet these wallpapers almost exclusively focus on plants ...\n\nPot smokers among the jury?"
    author: "she"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "I assume you send in better suggestions? Besides, nobody forces you to use any of these wallpapers, so what do you care?"
    author: "Andr\u00e9"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "Exactly why those themes remind you of Vista or XP? just because they are \"photographies\" of \"things\"?"
    author: "I\u00f1aki Baz"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-20
    body: "I guess you feel if the KDE team didn't hold this contest KDE 4 would be all done and then you could then rant about how much you think the distros should mess with it because, well... you would never use the default look, you'd prefer to download your desktop look, etc... insert more unproductive negativity here... more bs vista comparisons... it's a photograph, certainly vista has the capability to display photographs as a wallpaper...give me a break.. jump in a lake.\n\nim sure your response would have been different if they chose your kde wallpaper full of kde gears and the colour blue, whatelse? oh.. the dragon and oh.. tux because that would.... make no friggin sense... at least with all that BS it wouldnt be vista.."
    author: "Mike"
  - subject: "Re: I just hope distros use them"
    date: 2007-11-19
    body: "Ouhm, with 1900 Wallpapers?? That would be lucky ;-)"
    author: "Lukas Appelhans"
  - subject: "Thank you"
    date: 2007-11-19
    body: "They look great and the KDE4 is shaping up very nicely.\n\nThanks again "
    author: "oLdsk3wL"
  - subject: "O_O"
    date: 2007-11-19
    body: "The 'Wow' Starts Now (c)\n;)"
    author: "shamaz"
  - subject: "Re: O_O"
    date: 2007-11-19
    body: "hahaha.. oh no, don't jinx us! ;)"
    author: "Aaron J. Seigo"
  - subject: "Realy cool"
    date: 2007-11-19
    body: "Cool wallpappers! espatialy \"Ladybuggin\"\n"
    author: "Dumas"
  - subject: "Re: Realy cool"
    date: 2007-11-19
    body: "It's my least favorite... probably has something to do with the fact that this part of Missouri is getting swarmed with ladybugs currently."
    author: "Ian Monroe"
  - subject: "Wow!"
    date: 2007-11-19
    body: "The Wallpapers are simple breathtaking. I just can't wait to see a finished KDE4!"
    author: "Bobby"
  - subject: "Great!"
    date: 2007-11-19
    body: "These are really looking great. Once the artistic design decisions are taken out of the hands of the coding geeks, Open Source can actually look beautiful ;-) If this really is too much for you - go here:\n\nhttp://www.dotproject.net/\n"
    author: "Michael"
  - subject: "Re: Great!"
    date: 2007-11-21
    body: "We actually use dotproject at work...\nI think its ugly.. but it works.. :p"
    author: "Mark Hannessen"
  - subject: "Animating?"
    date: 2007-11-19
    body: "Love the wallpapers, top job!\n\nI'm just wondering whether KDE4 will allow animating wallpapers like Vista Ultimate (it calls them Dreamscapes - but I believe it's just playing avi movies). I know some will complain and say it's a resource hog, but there is something both relaxing and beautiful about having swaying grass or running water on the desktop. So, how about it?"
    author: "Andy"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "Fancy writing a plasmoid to do it?\n\nThe Plasma framework is designed so that you can really have any background you can code.  It's just another applet.  You may not get it by default in 4.0, but I have no doubt that such backgrounds will appear."
    author: "Alex Merry"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "I was actually interested in taking it one step further - 3D backgrounds.  We have the whole cube thing, so you can rotate etc.  So why not have a 3D background that rotates with the cube? :)"
    author: "John Tapsell"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "Cool idea!\nIt could help you orient yourself while you are roting the cube. How about employing KStars to render a 3D starchart behind your cube that rotates behind the cube as if not the cube is rotating, but you are roting around the stationairy cube? Hmm... Sounds cool!"
    author: "Andr\u00e9"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "It is really very cool feature. I am not well aware of what plasma can do and I think that it is more kwin issue. But if it is possible to create 3D backgrounds it will rock :)"
    author: "Plamen Terxiev"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "background have nothing to do with kwin (well, other than respecting the \"this window is the desktop\" flag) and everything to do with plasma. 3d background effects can be written in openGL even as plasma supports opengl plasmoids."
    author: "Aaron J. Seigo"
  - subject: "Re: Animating?"
    date: 2007-11-20
    body: "Wow. It sounds really great. I knew that plasma is very powerfull but this overpass my expectations what plasma is capable of. In this case maybe I will try to make a 3D background plasmoid in OpenGL when the KDE 4 is released."
    author: "Plamen Terxiev"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "While I like the idea, I also have a long-standing problem with the cube in that it feels rather limited to just four desktops.  While it's fine that there are people that stick with that default and are perfectly capable of getting work done, I am not among their number."
    author: "Wyatt"
  - subject: "Re: Animating?"
    date: 2007-11-28
    body: "i find using two or three monitors works best for me. The whole buggy cube thing is next to useless. I wish linux would go back to the people who it was meant for...\nintelligent people"
    author: "Shaun"
  - subject: "Re: Animating?"
    date: 2007-12-10
    body: "I use two monitors and four virtual desktops, so I see what you mean 50/50.\n\nBy your comment about the cube, I'll assume you've tried Compiz/Beryl. Have you tried the \"Desktop Wall\" plugin instead of \"Desktop Cube\"? Perhaps with the \"Expose\" plugin? I find that to be very comfortable.\n\n(A quick Super+Arrow to switch desktops with the keyboard... or just mouse to the top-right corner to zoom out and then click to pick)\n\nI'd be using a mix of KDE and Compiz Fusion parts right now if Compiz Fusion didn't have so many irritating rough edges and limitations."
    author: "Stephan Sokolow"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "Haven't had a look at plasma yet but I guess this could be done using a plasmoid."
    author: "Christian Nitschkowski"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "This has been possible since the early days of KDE and even earlier.\n\nThere is an advanced option that lets you select a program for drawing the background. you can select a video player and have it play avi files.\n\nPerhaps a plasmoid that is fully integrated into KDE, with easily downloadable animated backgrounds would be the more polished solution, though."
    author: "KOffice fan"
  - subject: "Re: Animating?"
    date: 2007-11-19
    body: "Enlightenment dr17 can do that"
    author: "Nsm"
  - subject: "Re: Animating?"
    date: 2007-12-27
    body: "and so for more than a year... "
    author: "somekool"
  - subject: "Looks great"
    date: 2007-11-19
    body: "Just a little bit strange that 5 out of 15 wallpapers are from jury member.\n\nBut i can't realy complain, they all look great! :-D"
    author: "Beat Wolf"
  - subject: "Re: Looks great"
    date: 2007-11-29
    body: "There are some comments and replies on that in the blog entry (the jury members were not allowed to vote for their own contribution, voting was secret and the entries were anonyminiezd), but it's a bad habit at all to let jury members take part in a competition.\n\nAnyway, as there haven't been any prices (except the winning itself) and the collection is OK (too much green for my taste and a bit too 'conservative') I don't care much about that."
    author: "furanku"
  - subject: "Kenneth Wimer.."
    date: 2007-11-19
    body: "I love the wallpaers \"Summer\" and \"Fields of Peace\", both make me feel refreshed and relaxed. wonderful wallpapers.. you rock Kenneth! thanks :)"
    author: "fast_rizwaan"
  - subject: "image ratio"
    date: 2007-11-19
    body: "And what about the differents screen ratio for the user ?\nWhen you use a 1024x768 screen (ratio 4/3) or a 1440x900 screen (ratio 16/10) the result is very different !\nThe solution is to use a scalable wallpaper (SVG) and NOT a jpeg wallpaper."
    author: "egan"
  - subject: "Re: image ratio"
    date: 2007-11-19
    body: "You can also just use a different cut out for those. SVG wallpapers are quite a different catogory than photography, resulting in very different kinds of images. You may prefer the one or the other, but that does not mean that KDE should limit itself to only one category. "
    author: "Andr\u00e9"
  - subject: "Re: image ratio"
    date: 2007-11-19
    body: "That's not true. SVG is designed for vector graphics, not photos. JPEG is the correct choice. I assume you weren't implying that people shouldn't use photos as backgrounds!\n\nAs for different aspect ratios, the only sensible solution is to crop the image."
    author: "Tim"
  - subject: "Re: image ratio"
    date: 2007-11-19
    body: ">>> As for different aspect ratios, the only sensible solution is to crop the image.\n\nSo for using these 1280X800 KDE wallpapers on my 1600x1200 screen i have to :\n\n1) Expand the wallpaper (distortion, big pixels =>ugly)\nor\n2) Leave the small wallpaper lost in the middle of my screen (ridiculous)"
    author: "egan"
  - subject: "Re: image ratio"
    date: 2007-11-19
    body: "No, you should just use the 1600x1200 resolution image when it becomes available."
    author: "Andr\u00e9"
  - subject: "Re: image ratio"
    date: 2007-11-20
    body: "For photos I agree raster is king (until some fancy-pants raster->vector conversion techniques get devoloped, maybe) but wouldn't PNG or TGA beat out JPEG for this kind of thing? The desktop isn't the web, after all."
    author: "Matthew"
  - subject: "Re: image ratio"
    date: 2007-11-20
    body: "PNG results in pretty ridiculous file sizes for photographic content. A high-quality JPEG can get very close while still keeping a manageable file size."
    author: "Hector Martin"
  - subject: "Re: image ratio"
    date: 2007-11-19
    body: "Well frist we made versions for most comon screen ratios.\nSecondly i work with svg all day long its a format i love. But its dead wrong for walpaper, sacaling an svg is not simple at all, it may seem simple but its not, example the floppy disk in devices has a svg version for every size. \nnormaly wen I do a svg wallpaper I end up rendering it in a big size and then croping its to each sub screen ration needed.\nAnother huge problem is that normaly my svg wallpapers take about 10 minutes to render, I gess a single gradient dosent work very well for me as an wallpaper :)   "
    author: "nuno pinheiro"
  - subject: "Gorgeous Pictures"
    date: 2007-11-19
    body: "In my humble opinion the artistic results of the contest are absolutly stunning. Thank you very much to the Oxygen team.\n\nHowever, I still disagree with the decision to allow jury members to submit images to the contest. It's even worse that so many of them \"won\" the \"contest\". I strongly suggest that this practice is changed in the future.\n\nYou could have released the Oxygen Wallpaper Set and then have a contest to  create the 'Oxygen Contributers Set 2007'.\n\nHad I taken part in the contest, I would have felt cheated...\n\nMind you, this is purely a criticism of the process, not of the result."
    author: "bangert"
  - subject: "Re: Gorgeous Pictures"
    date: 2007-11-19
    body: "You should read Riccardo's blog before complaining..."
    author: "Robin"
  - subject: "Re: Gorgeous Pictures"
    date: 2007-11-19
    body: "what makes you think that i hadn't read it?\n\ngreat if that solves your concerns. i still think the process is flawed and dishonest...\n"
    author: "bangert"
  - subject: "Re: Gorgeous Pictures"
    date: 2007-11-19
    body: "Well... if the artists aren't allowed to take part in this, we'd have less good wallpapers...\n\nI don't think it was unfair... it just proves that the kde artists are very talented ;)"
    author: "Robin"
  - subject: "Re: Gorgeous Pictures"
    date: 2007-11-19
    body: "Frist the one suposedly mine is not mine at all its realy from Jim (code poet).\nSecondly wen we strated and it was originaly my idea IIRC, the iddea was to device a way to get some more wallpapers to had to the ones I had made, and Ken 2, ken is a kick ass photografer and we are lucky to have him. \nIn the end none of my wallpapers made the final cut :)\nAhs for riccardo he went in a taking pictures frenzy he worked like hell and honestly only in the end did I realized so many of his pictures made the final cut. I meen After watching 2000 pictures they all look abit the same.\nYou can beleve us that we tried our best to get the best pictures we could, im very soory for all the people that did not made the final cut, but we tried to be very honest.\nExample on of the images that end up on the final 15 that is from ricardo is only there couse we got no anser from the outhor of the picture that did so we wre not sure we could lgpl it and has ricardo was the next runer up....\n"
    author: "nuno pinheiro"
  - subject: "Re: Gorgeous Pictures"
    date: 2007-11-19
    body: "I don't think you guys need to defend yourself. If it were a serious contest with huge prizes to be won, people might have a point. As it is, you asked for contributions to select a set of stunning images to use as backgrounds. I think the artists community around KDE has every right to participate in that. In fact: I am very glad they do!\n"
    author: "Andr\u00e9"
  - subject: "BUG!"
    date: 2007-11-19
    body: "There's typo in links to VladStudio"
    author: "Stalwart"
  - subject: "Can't wait to turn all these..."
    date: 2007-11-19
    body: "into a screensaver slideshow!"
    author: "jasper"
  - subject: "Green Concentration"
    date: 2007-11-19
    body: "The link to Green Concentration points to a broken image, as only the top half seems to have the image data.\n\nGuys, these are beautiful images, by any standards. Fantastic work to authors and organisers alike!"
    author: "markrian"
  - subject: "Summer is great"
    date: 2007-11-19
    body: "I finally tried running a full KDE 4 session just this last week. I just have to say that the Summer background (the default) is just fantastic and fits in well with the rest of the schema. "
    author: "Ian Monroe"
  - subject: "Pretty!!"
    date: 2007-11-19
    body: "First of all, to get things straight, KDE 4 will have the best artwork, of any KDE so far! This is really great work! These pictures are overall pretty awesome! And I mean it, despite my comments below!! It's critics on a high level ;)\n\nFor the next contest I'd like to make some suggestions however to keep in mind:\n\n- Out of those 15 pictures, 10 are nature shots and of those 7 are leaves, also only 2 are non photographic pictures. IMHO it would have been better to have a bit more diversion, thematically. Maybe another non photographic background in the style of EOS could still be added.\n\n- I'm personally a bit bored of those overdone macro shots, like Red Leaf or Fresh Morning. I think it's sometimes a bit too much macro, bluring the background a bit too much, losing cohesion between the macro object and the background. It's as always, less is often more ;)\n\n- Green Concentration is IMHO a bit too intensive for a default background picture, it's pretty blinding if have fullscreen..."
    author: "Psychotron"
  - subject: "Re: Pretty!!"
    date: 2007-11-19
    body: "I agrea in most acounts, in the end its a mater of the quality of the abstract non photografic images we recived not being good enough, I should say so, I comited quite a few of those :)\n\nAgain this were the best one in our opinion.  "
    author: "nuno pinheiro"
  - subject: "my preferences."
    date: 2007-11-19
    body: "please, my it be \"Colorado Farm Oil Paint\". I always have problems with bright backgrounds making the icons less readable, and this one is dark. feels darker than just about all of them, in any case. it also looks stunning, which helps.\n\nif it has to be abstract than I sure hope it's \"Emotion\", because I don't like \"Eos\" at all.\n\nthe rest of the wallpapers would be OK, I just think their nothing special."
    author: "yman"
  - subject: "Nice but not too similar???"
    date: 2007-11-19
    body: "Hello... just my 5 cents.. nice are all very nice.. but are not all too much similar???\nI would have expected more etherogeneity of subjects..."
    author: "Sylvaticus"
  - subject: "Re: Nice but not too similar???"
    date: 2007-11-20
    body: "I agree, and it seems every image is either predominantly green or blue.  Not only are the subjects of the images similar, but the colors."
    author: "T. J. Brumfield"
  - subject: "Headache"
    date: 2007-11-19
    body: "Sorry to say that, but most of this pictures make me headache. Too blurry, too unsharp. This abrupt switch from sharp to unsharp looks on ome of them very artificial.\n\nI would not want such pictures on my screenbackground.\n\nSorry.\n\nBoronk"
    author: "Boronk"
  - subject: "Re: Headache"
    date: 2007-11-19
    body: "lol unsharp?"
    author: "Garza"
  - subject: "Re: Headache"
    date: 2007-11-19
    body: "I agree. Plus they are very similar to each other. I would prefer a bit more diversity.\n\nThey are nice pictures generally speaking, but not so great for the screen background.\n\nCheers!"
    author: "SK"
  - subject: "Re: Headache"
    date: 2007-11-20
    body: "Hell, you obviously didn't try them.\n\nPlease Oxygen team, do not even bother whit those trolls."
    author: "Luis"
  - subject: "Re: Headache"
    date: 2007-11-20
    body: "Yeah, your rite. All those pictures suck. I hate them all!"
    author: "A Hater"
  - subject: "amazing"
    date: 2007-11-19
    body: "these are all amazing.  Also the work of the top 100 artists should be posted later and available for download too.  I mean the top 100 wallpapers!\nO and the down part is that these things just add to the unrest to see the finished KDE4.  I really can't wait anymore :(  it's just so hard!"
    author: "feng shaun"
  - subject: "miss"
    date: 2007-11-20
    body: "miss some walls about konqui....and kde4, i like nature...but is kde4, the most expected DE, and not wallpaper about kd4 or konqui ???\n"
    author: "Rudolhp"
  - subject: "Wallpapers?"
    date: 2007-11-20
    body: "Nice pictures but I don't think they are good wallpapers - they contain too much contrast.\n\nLet me qoute the openSUSE artwork guidelines ( http://en.opensuse.org/Artwork:Guidelines ):\n\"Wallpapers \n\nThe goal of the default wallpaper graphic is to be unobrtusive and allow the items on the desktop to be seen. It should not hinder the overview of user's desktop. \n\nWhile a solid color background seems to be a perfect fit based on the goal above, the wallpaper also shouldn't be completely dull and boring. \n \nIt is very likely that a user will customize a wallpaper and even go against the outlined goal, putting a photo of his beloved one as the background. The default graphic however should only be some sort of abstract shape with low contrast of the texture. Abstract shapes work best for two reasons. First it doesn't make the human visual system focus on the background, but the items on top of it and also it avoids cultural or religious specifics. Using a picture of an animal might have such implications and in worst case scenario insult the user.\"\n\nI agree with them and I don't think these pictures match them."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Wallpapers?"
    date: 2007-11-20
    body: "My thoughts exactly, well said. "
    author: "MOP"
  - subject: "I submitted some wallpapers to the contest..."
    date: 2007-11-20
    body: "Now that the contest is over, I feel free to submit them to KDE Look. If all other contestants did the same, we would have 1900 new wallpapers.\n"
    author: "Paul Leopardi"
  - subject: "Xinerama wallpapers"
    date: 2007-11-20
    body: "Will these wallpapers be available in dual-screen resolutions as welll?"
    author: "Henrik"
  - subject: "Re: Xinerama wallpapers"
    date: 2007-11-20
    body: "Why ? You can set a wallpapers for each screen, which IMO looks better. My problem is that the slideshow screensaver of kde3 cannot handle the two screens of xinerama as two. So the slideshow is just unuseable and ugly.\n\nGreetings\nFelix"
    author: "Felix"
  - subject: "No KDE logo"
    date: 2007-11-20
    body: "These are nice pictures but I was expecting wallpapers with KDE4 logo. None of them has it. They look like ordinary pictures not wallpapers."
    author: "parsek"
  - subject: "Re: No KDE logo"
    date: 2007-11-21
    body: "there is no need for a kde logo on the wallpaper "
    author: "pete"
  - subject: "its vladstudio"
    date: 2007-11-21
    body: "not valdstudio lol\n\nhttp://www.vladstudio.com"
    author: "pete"
---
The <a href="http://blog.ruphy.org/?p=23">Oxygen team is happy to present the final selection of 15 wallpapers</a> that will be released with KDE 4.0. It took longer than expected to manage the 
contest due to the large number of entries (around 2000), but it was our first time organising such a contest. Read on for details of the winning entries.




<!--break-->
<p>The Oxygen team are overjoyed at the response and want to thank everyone who took part. 
Every wallpaper will be available in several screen resolutions for both wide and regular display. 
We believe the selection provides a beautiful, colorful, peaceful yet not distracting collection of free wallpapers. We're sure you'll find one you like.</p>

<p>All of these wallpapers are being released under the LGPL version 3 license. For more insights by the jury, check <a href="http://blog.ruphy.org/?p=23">Riccardo's blog</a>.</p>

<p>
<strong>Code Poet's Dream</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/code-poets-dream.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/code-poets-dream.jpg" alt=""/> </a> <br />
By <a href="http://www.flickr.com/people/alphageek/">Jim &ldquo;code poet&rdquo;</a> and <a href="http://www.nuno-icons.com">Nuno F. Pinheiro</a>.
</p>

<p>
<strong>Colorado Farm</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/colorado-farm.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/colorado-farm.jpg" alt=""/> </a> <br />
By <a href="http://www.flickr.com/people/scingram/">Scott Ingram</a>.
</p>

<p>
<strong>Curls On Green</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/curls-on-green.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/curls-on-green.jpg" alt=""/> </a> <br />
By <a href="http://blog.jciv.com/">Joseph Connors</a>.
</p>

<p>
<strong>Fields Of Peace</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/fields-of-peace.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/fields-of-peace.jpg" alt=""/> </a> <br />
By <a href="http://sinecera.de/">Kenneth Wimer</a>.
</p>

<p>
<strong>Fresh Morning</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/fresh-morning.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/fresh-morning.jpg" alt=""/> </a> <br />
By <a href="mailto:kapuxino@gmail.com">Pablo León-Asuero Moreno</a>.
</p>

<p>
<strong>Golden Ripples</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/golden-ripples.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/golden-ripples.jpg" alt=""/> </a> <br />
By <a href="mailto:josephconnors@usa.net">Joseph Connors</a>.
</p>

<p>
<strong>Green Concentration</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/green-concentration.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/green-concentration.jpg" alt=""/> </a> <br />
By <a href="http://blog.jciv.com/">Joseph Connors</a>.
</p>


<p>
<strong>Ladybuggin'</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/ladybuggin.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/ladybuggin.jpg" alt=""/> </a> <br />
By <a href="mailto:rhanley002@aol.com">Rick Hanley</a>.
</p>

<p>
<strong>Leaf's Labyrinth</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/leafs-labyrinth.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/leafs-labyrinth.jpg" alt="Leaf's Labyrinth"/> </a> <br />
By <a href="http://www.ruphy.org">Riccardo Iaconelli</a>.
</p>

<p>
<strong>Emotion</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/emotion.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/emotion.jpg" alt=""/> </a> <br />
By <a href="http://www.valdstudio.com">Vlad Gerasimov</a>.
</p>

<p>
<strong>EOS</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/eos.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/eos.jpg" alt=""/> </a> <br />
By <a href="http://www.valdstudio.com">Vlad Gerasimov</a>.
</p>

<p>
<strong>There's Rain On The Table</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/rain-on-table.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/rain-on-table.jpg" alt=""/> </a> <br />
By <a href="http://www.ruphy.org">Riccardo Iaconelli</a>.</p>

<p>
<strong>Red Leaf</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/red-leaf.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/red-leaf.jpg" alt=""/> </a> <br />
By <a href="http://blog.jciv.com/">Joseph Connors</a>.
</p>

<p>
<strong>Skeeter Hawk</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/skeeter-hawk.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/skeeter-hawk.jpg" alt=""/> </a> <br />
By <a href="mailto:tsprader@gmail.com">Tracy Sprader</a>.
</p>

<p>
<strong>Summer</strong> <br />
<a href="http://oxygen-icons.org/wallpaper-contest/summer.jpg"> <img src="http://oxygen-icons.org/wallpaper-contest/t/summer.jpg" alt=""/> </a> <br />
By <a href="http://sinecera.de/">Kenneth Wimer</a>.
</p>

A big thanks for all the participants and congratulations to the winners by the <a href="http://www.oxygen-icons.org">Oxygen team</a>.




