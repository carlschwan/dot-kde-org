---
title: "KDE Games Taking Shape for KDE 4.0"
date:    2007-05-05
authors:
  - "acid"
slug:    kde-games-taking-shape-kde-40
comments:
  - subject: "Systematically remove of ugly, redundant borders"
    date: 2007-05-05
    body: "KDE needs a way to _systematically_ get rid of the ugly, redundant borders that spoil the visual aspect of many of its applications. Just compare the double border (BAD!) on the KReversi screenshot:\n\nhttp://static.kdenews.org/dannya/kreversi4.png\n\nwith the clean, borderless (GOOD!) play are of KBounce:\n\nhttp://static.kdenews.org/dannya/kbounce4.png"
    author: "AC"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-05
    body: "I totally agree. \n\nAlso, imho, some of the options in the Lipstik style for KDE 3 should be default in the default KDE 4 style (oxygen?), such as not drawing seperators and handles, and not drawing a status bar frame. This along with application creators cleaning up the toolbars would give KDE a much cleaner look. \n\nJust my two cents. And if it's not default, atleast it's customizable. Thats what I love about KDE. Keep up the good work! :)"
    author: "AC"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-06
    body: "Yup, the customizability is a winner. Especially that I like the borders. Gimme more of them in KDE4, for a nicer look! :)"
    author: "zonk"
  - subject: "Re: Systematically remove of ugly, redundant borde"
    date: 2007-05-06
    body: "This is getting a little off-topic, but speaking of aesthetics...\n\nIs there any convenient way to make a group of Qt/KDE toolbar buttons (I wouldn't know if KDE provide their own toolbar/tool button widgets) all take the width of the largest button in the group?\n\nGiven that most toolbar buttons are quite narrow anyway, this wouldn't usually increase the width significantly. I just find that a group of buttons of slightly different widths tends to stand out in a very bad way - does anyone else feel this way?"
    author: "Jeff Parsons"
  - subject: "Re: Systematically remove of ugly, redundant borde"
    date: 2007-05-06
    body: "I agree.  It's a small point, and I'm sure it could already be done on a case-by-case basis, but it would be neat to have this happen automatically.  Probably quite tricky, too..."
    author: "Adrian Baugh"
  - subject: "Re: Systematically remove of ugly, redundant borde"
    date: 2007-05-06
    body: "\"Also, imho, some of the options in the Lipstik style for KDE 3 should be default in the default KDE 4 style (oxygen?), such as not drawing seperators and handles, and not drawing a status bar frame. This along with application creators cleaning up the toolbars would give KDE a much cleaner look.\"\ntotally agree !!"
    author: "elo"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-05
    body: "I agree, specially when scrollbars are involved."
    author: "C2H5OH"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-05
    body: "there is no automatic way to make these borders gone. i'm quite sure the borders in this article are mostly added by developers on purpose.\n\nthe real problem is that currently every game in this article has its own way to do the borders ;). so thats something for the hig.\n\nhowever, i hope thats because the apps are still work in progress, and that the kde-games team aims for a consistent look of their module, and kde in general.\n"
    author: "ac"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-05
    body: "> i'm quite sure the borders in this article are mostly \n> added by developers on purpose.\n\nNo, that is not the case.  The explanation here is that the view in KReversi is housed in a scroll area, and scroll areas have borders by default.  You have to add additional code ( not very much, only one line ) to turn it off.  I just committed a patch to do that.  In fairness to the developer(s), they are spending their time on the important functional points of the application that matter - in this case the game logic and artwork.  These little fit and finish tweaks are a good opportunity for new coders to contribute.  You don't need a very detailed understanding of C++ either.\n\nUsing the Cleanlooks style provided with Qt4 ( a copy of the Gnome Clearlooks style ) produces a more attractive interface.\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-06
    body: "i see you've made 16 commits so far today providing such fit and finish improvements to the games. awesome! thanks robert =))"
    author: "Aaron J. Seigo"
  - subject: "Re: Systematically remove of ugly, redundant borde"
    date: 2007-05-06
    body: "> scroll areas have borders by default.\n\nWould it be possible to change the code of the scroll area class so that that it only shows the border _if_ the scroll itself is visible? If this behavior occurs by default, the application that incorporates the scroll area doesn't have to care about when to place the border (although it would still be able to override the default behavior if it wants)."
    author: "Vlad"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-05
    body: "Removing borders is not hard. It's something _you_ can do yourself. Just run through the kde4 applications, and whenever you notice an ugly border add the single line of code that turns the border off and send of a patch to the maintainer. It's a great way to get involved and grow beyond an anonymous coward into a valued member of the community."
    author: "Boudewijn Rempt"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-05
    body: "Borders should NOT be removed\n\nAt least not like that. Don't go about making such decisions in the application as it affect every style.\n\nInstead ask the style developer of your favourite style to not draw borders in the scrollview. That way every program will be fixed at one, and you you don't remove borders from styles that actually want it."
    author: "Casper Boemann"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-05
    body: "The problem is that some scroll views need borders, others look better without.  This has to be decided on a case-by-case basis.  \n\nMy gut feeling is that trying to do this in an automated way in the styles will not produce the best results."
    author: "Robert Knight"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-06
    body: "we already have proof for this in kde3 where various styles try various things with borders resulting in various apps looking alternately bad or good depending on which app and which style. =/ so i have to agree with you here."
    author: "Aaron J. Seigo"
  - subject: "Re: Systematically remove of ugly, redundant borders"
    date: 2007-05-09
    body: "Wow that's nit-picky, the kde 4 screenshot for reversi is a vast improvement over the previous version."
    author: "Kyle Goddard"
  - subject: "KBounce"
    date: 2007-05-10
    body: "I like the fact the KBounce is not resizable and the overall new layout is nicer. but the artwork is really uglier and darker IMO. I wish the same artwork as 3.x would be available in SVG.\n\nplease\n\nthe dark and roundly shaped background is horrible"
    author: "Mathieu Jobin"
  - subject: "Chess"
    date: 2007-05-05
    body: "What is about Knights?"
    author: "Karsten"
  - subject: "Re: Chess"
    date: 2007-05-06
    body: "As far as I know, Knights is not a part of KDE Games.  It is a great chess game though, and I hope the Knights team does a conversion.  Does anyone have any inside info on this one?"
    author: "Louis"
  - subject: "Re: Chess"
    date: 2007-05-06
    body: "Looks like Knights is almost dead, seems like the last activity was from 2005. This is a real pitty, it was a nice chess game.\n\nhttp://knights.sourceforge.net/news.php"
    author: "TMG"
  - subject: "ksudoku?"
    date: 2007-05-05
    body: "What is about ksudoku? I thought someone wanted to shape it up for KDE 4?"
    author: "liquidat"
  - subject: "Re: ksudoku?"
    date: 2007-05-05
    body: "It's been moved to kdereview to be tested etc. If all goes well, it will be moved to the kdegames module in time for KDE 4.0"
    author: "Matt Williams"
  - subject: "Re: ksudoku?"
    date: 2007-05-05
    body: "Thanks for the answer."
    author: "liquidat"
  - subject: "Congratulations"
    date: 2007-05-05
    body: "I'd like to congratulate the kdegames team on their work for KDE 4 so far.\n\nAlong with many of the kdeedu applications, the KDE 3 / KDE 4 difference is night and day.  \n\nThe swish new artwork in particular looks great.\n\nIf I may insert a request, it would be helpful to have an overlay or some other quick-to-access help screen which explains what a game is and how to play.  Opening up and reading the manual is a bit of a chore to do when you are after a ten-minute distraction.  Thanks again :)\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Congratulations"
    date: 2007-05-06
    body: "iirc, there is a tutorial mode for one of the games and the intention is to add tutorials to the other games over time as well. this should do what you want and would be available from the same opening screen overlay that offers to start a game =)"
    author: "Aaron J. Seigo"
  - subject: "Nice Article "
    date: 2007-05-05
    body: "Hey, nice article. *virtual high five*\n\nI like when modules are cleaned up (waves at dannya and his kdeartwork efforts) so the improved quality of the kdegames module is good news for KDE in general, as it's one of the first things new KDE users will look at, especially if they are not net connected.\n\nCheers"
    author: "Troy Unrau"
  - subject: "ktron"
    date: 2007-05-05
    body: "I find myself playing ktron a lot.  It's a good distraction that is over quickly.  Hopefully a maintainer could be found, but if not it's understandable to me, as I wouldn't have the time or motivation to do it myself."
    author: "MamiyaOtaru"
  - subject: "Re: ktron"
    date: 2007-05-07
    body: "IMHO armagetron is a lot nicer I would rather they took the time to work with armagetrons developers to properly intergrate and update an existing game than waste efforts on an older title that needs far more work.  I'm sure if you look around you can find many fitting games that could be worked on and then shipped with kde. "
    author: "matthews"
  - subject: "Re: ktron"
    date: 2007-05-08
    body: "Sure armagetron looks nicer.  But it misses the feature of ktron that attracts me: over quickly.  Armagetron is a bit more involved, and not as good a way to blow off a little steam for a minute or two (or 30 seconds).  Navigating armagetron's menus takes almost that long ;)"
    author: "MamiyaOtaru"
  - subject: "No maintainer for klickety??"
    date: 2007-05-05
    body: "It's too bad that there seems to be too little interest in Klickety. It's the game my gf loves about Linux..."
    author: "WPosche"
  - subject: "Re: No maintainer for klickety??"
    date: 2007-05-05
    body: "That's funny, Klickety is the only game my wife cares about. :-)"
    author: "Knut"
  - subject: "Re: No maintainer for klickety??"
    date: 2007-05-29
    body: "AFAIK Klickety will survive in KDE 4.0 as a theme in KSame, which is a very similar game.  There was a thread about this recently in the KDE Games mailing list."
    author: "Ian Wadham"
  - subject: "Re: No maintainer for klickety??"
    date: 2007-05-06
    body: "That's really really sad. Klickety is also one of my favourits (if not even THE favourite). Hopefully somebody can and will take care of it.\n\nKind regards\n\nesomer"
    author: "esomer"
  - subject: "Re: No maintainer for klickety??"
    date: 2007-05-06
    body: "Ksame is a very similar game, and that is alive and well for KDE 4. \nThere will be no klickety for 4.0 unless a maintainer appears out of nowhere in the next couple of days and starts getting it into shape. And that's pretty unlikely I'm afraid."
    author: "Paul Broadbent"
  - subject: "Re: No maintainer for klickety??"
    date: 2007-05-07
    body: "But maybe someone wants to do that =]]]"
    author: "Jan Vongrej"
  - subject: "KDE Games"
    date: 2007-05-05
    body: "Same tired lame ass games now with a pretty face.\n\nLinux gameing is never going to go anywhere with these passing as \"games\""
    author: "Klatu"
  - subject: "Re: KDE Games"
    date: 2007-05-05
    body: "Well, it sure beats minesweeper and/or solitaire:)"
    author: "Martin"
  - subject: "Re: KDE Games"
    date: 2007-05-20
    body: "actually, I miss minesweeper. there must be a linux version somewhere... time to find it!"
    author: "Chani"
  - subject: "Re: KDE Games"
    date: 2007-05-05
    body: "... was this lame ass troll meant to pass as a \"comment\"?"
    author: "Torsten Rahn"
  - subject: "Re: KDE Games"
    date: 2007-05-06
    body: "these are amusements and distractions, not \"games\" as used in the context of \"the gaming industry\". there are a couple types of games: simple, fun things to play and full-on entertainment. the latter are more akin to movies and novels, the former are more like solitaire, soduko and crosswords. in fact, that's often where the source material comes from. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE Games"
    date: 2007-05-06
    body: "yeah.. the gaming industry is not motivated to do something about Linux. I could not care less, because 95% of those \"games\" are like glossy paper. Looking shiny but little substance.\nTry playing bzflag with your mates. Trashy GFX, looks like crap actually, but loads of fun for hours. I don't want the game to provide complexity, I want the game to be made of simple rules to built the basis for some complex gameplay."
    author: "Thomas"
  - subject: "Games release"
    date: 2007-05-05
    body: "As I understand all that is needed is a KDE4 development plattform. But as long as  pre-alpha nothing will happen. How does porting Games from KDE3 to KDE 4 work? What are the differences and what time does it take per game?"
    author: "Bert"
  - subject: "Re: Games release"
    date: 2007-05-06
    body: "Many distributions already have KDE 4 packages for developers, and when alpha 1 is released, many more will do so (I happen to know a bit about that ;-)).\n\nAbout the porting of the games, my impression is that it's some work, but not too much. It seems pretty doable, which makes sense, as the games aren't that big. Help of artists is important, though, as you will need some help in the art area for nice, scalable graphics."
    author: "superstoned"
  - subject: "Re: Games release"
    date: 2007-05-06
    body: "The biggest work needed to port games from KDE 3 to KDE 4 is changing them to use Qt 4 rather than Qt 3. Other than that there are a few other upgrades which are being added to all appropriate games like resizable, scalable interfaces and theme support.\nWith regards to time, it really depends on the complexity of the game. The change to how Qt 4 handles graphics is quite significant."
    author: "Paul Broadbent"
  - subject: "Block appearance"
    date: 2007-05-06
    body: "Whenever I see preview pics of KDE4's new game graphics, the one thing that keeps bugging me is the blocks, particularly when they're in large groups.  Maybe it's the dark edges/color, the identical lighting, or the way they leave gaps when stacked, but in a word, they don't look \"real\" at all.  It's kind of distracting..."
    author: "MechR"
  - subject: "Re: Block appearance"
    date: 2007-05-06
    body: "I agree with you, I don't like very much the blocks in kbounce (my favorite game of kdegames) and katomic. I think thee border of they could be thiner..."
    author: "Joseph"
  - subject: "Re: Block appearance"
    date: 2007-05-06
    body: "This is one place to help improve KDE where neither programming or creative artistic skill are needed. The graphic are just SVGs and as good as finished, only minor modifications needed. So why not try tweaking it yourself and apply that last bit of polish making it better. That way the games developers/artist can consentrate on more pressing matters, like porting the rest of the games. While you improve it from decent to perfect :-)"
    author: "Morty"
  - subject: "Kolf"
    date: 2007-05-06
    body: "One of my favorite KDE3 games is Kolf. Could anyone post a screenshot of the KDE4 version?"
    author: "Peter"
  - subject: "Re: Kolf"
    date: 2007-05-06
    body: "No :)\nThe new Kolf artwork is currently \"programmer art\" made by me, so although it looks better than KDE 3 kolf it has yet to had much work on it from anyone with any real artistic skill. Don't worry, this will happen before the KDE 4 release, but there is little point showing you a current screen shot as there are likely to be big changes between now and October.\nOf course if you are desperate to see it you could drop me an email, but I don't want to post anything public yet."
    author: "Paul"
  - subject: "Re: Kolf"
    date: 2007-05-06
    body: "Ok, I'll be patient :). Rock on, though, Kolf is much-needed diversion ;-)"
    author: "Peter"
  - subject: "for KDE 4 games"
    date: 2007-05-06
    body: "KDE is a great Desktop Environment!!!!\nAbout the games:\nI think, it's better to include small games equivalent of Sven and Moorhuhn ( http://www.phenomedia.de/ ). This games are very amusement and relaxation,but to my regret, they are for Windows and are not \"open source\"....."
    author: "Stoner"
  - subject: "Re: for KDE 4 games"
    date: 2007-05-07
    body: "Both games are not \"politically correct\", but it's exactly this reason why they are so popular.\n\nBut I agree that such games make a lot of fun ;-)\n\nWill be very hard to get such games into the standard KDE environment."
    author: "Philipp"
  - subject: "why waste time on games? "
    date: 2007-05-06
    body: "I use KDE at home and work. My home office is KDE only. \nIMHO KDE team should not waste their valuable time on games. \nGames are the least important parts of KDE.\n\nPlease spend your time on more important parts of this wonderful OS. :) \nCheers! \n\n"
    author: "Brrrr"
  - subject: "Re: why waste time on games? "
    date: 2007-05-06
    body: "If you don't like it maybe you could hire them to work full time on KDE4? I hear the plasma dev's have not even written any code yet so they could use a hand.\n\nIt's my guess they would still do kde games in their spare time though ;)"
    author: "Matt"
  - subject: "Re: why waste time on games? "
    date: 2007-05-08
    body: "> I hear the plasma dev's have not even written any code yet \n\nIf you haven't heard, then you should start checking again. There's work being done on the library side, there's krunner already in. It's not finished, but it's definitely under way. Stating this kind of FUD is putting of developers, so please educate yourself before doing so.\n\n> so they could use a hand.\n\nAbsolutely. =)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: why waste time on games? "
    date: 2007-05-08
    body: "It was a joke ;)"
    author: "Matt"
  - subject: "Re: why waste time on games? "
    date: 2007-05-06
    body: "Since most of the KDE contributors are not paid for what they do they will work on whatever they like (which is probably their own applications).  It's their decision.  They are not resources that can be reassigned by some KDE project leader.  \n\nKDE as a project just doesn't work like that.\n\n"
    author: "cm"
  - subject: "Re: why waste time on games? "
    date: 2007-05-06
    body: "> Please spend your time on more important parts of ...\n\nThese cool guys are not \"movable resources\" that can be assigned to a different task.. ;-) Likewise, putting all GNOME developers at KDE code won't make KDE4 development twice as fast.\n\nSo understand: if these cool guys were not working on KDE games, they likely wouldn't work on KDE at all. Keep the thumbs up for them!"
    author: "Diederik van der Boor"
  - subject: "Re: why waste time on games? "
    date: 2007-05-06
    body: "I would say, do more work on games. Games are a ideal start for new developers, as they are small, don't depend on other more complex projects, but allready take advantage of all the kde-core technologies. And many of the devs working on games, also contribute to other projects.\n\nSo if nobody works on improving games which attracts new developers, that might result in less work being done on other modules."
    author: "Josel"
  - subject: "Indeed"
    date: 2007-05-06
    body: "Its kdegames, not a zero-sum game. :)\n\nI really hate it when people say \"stop working on this, work on xyz instead.\" It shows complete ignorance of open source and a certain level of just being a jerk. Note that I don't mind someone simply saying \"please implement xyz\", user requests are an important part of open source development. Or for that matter \"please don't  implement xyz, I think its a bad idea\" is great as well. But the notion that the thread parent has that people's free time is something they have the right to redistribute is just disrespectful."
    author: "Ian Monroe"
  - subject: "Re: why waste time on games? "
    date: 2007-05-08
    body: "\nI don't think astronauts should waste their time in space while I have pot holes on my street.  They should fix the pot holes and other things first.\n\nPoint: Game programmers are game programmers...that is what they do."
    author: "Henry S."
  - subject: "Re: why waste time on games? "
    date: 2007-05-08
    body: "Well, if I didn't spend time on kdegames, I would use the extra time to play World of Warcraft, not to work on Plasma. So, it's not like resources can be simply relocated..."
    author: "Henrique"
  - subject: "What about KMahjongg and KShinen-Sho?"
    date: 2007-05-06
    body: "Those are two games which I would definitely _hate_ to lose. While I don't have the time to play them regularly, I get into a frenzy every few months..\n"
    author: "RichiH"
  - subject: "Re: What about KMahjongg and KShinen-Sho?"
    date: 2007-05-06
    body: "I wouldn't worry about them. They are alive and well and the new graphics are absolutely beautiful :)"
    author: "Paul Broadbent"
  - subject: "Re: What about KMahjongg and KShinen-Sho?"
    date: 2007-05-06
    body: "Just a reminder, KDE3 games will continue to perfectly run under KDE4. They may just look a bit outdated...\n\nYours, Kay"
    author: "Debian User"
  - subject: "Decent"
    date: 2007-05-06
    body: "Looks decent but not very eye-candy :P"
    author: "Yoyo"
  - subject: "Games"
    date: 2007-05-06
    body: "I think the games in KDE4 are getting way too much attention. The games are nice and all, but are an extra. A way to entertain people.\n\nBut where is plasma? Where are the great features the plasma project was talking about? Where's the \"arthur\" eye candy in the theming engine for KDE4? Where is the task-based approach everyone was talking about. \n\nPlasma seems to become vaporware. Please don't let it come that far and focus on that what needs to be done before we find ourselfs playing games in a QT4'th KDE3."
    author: "Dennie"
  - subject: "Re: Games"
    date: 2007-05-06
    body: "Sigh... stop being so negative. Do you realize what you say will just discourage the developers? If you think something has been overlooked, please help. Otherwise, post *constructive* criticism, politely.\nMessages like these are useless, because they help neither the developers nor the community."
    author: "Luca Beltrame"
  - subject: "Re: Games"
    date: 2007-05-06
    body: "Well, I'm afraid he's right. Plasma *is* vaporware. There used to be a great forum on http://www.kde-artists.org/ with sooo many good ideas about plasmas...now it's all gone. Also, there were tons and tons of mockups. There was so much talk about Plasma and how it will be the most important thing in KDE4 and redefine what a desktop experience is about...\n\nhttp://www.golem.de/0507/39114.html\n\nWhat's left of all that? Nothing. Vaporware...  :("
    author: "hummingbird"
  - subject: "Re: Games"
    date: 2007-05-06
    body: "B?ll?cks.  Plasma was never going to be the most important thing about kde4.  Perhaps nepomuk, perhaps the various improvements that get made daily to kdelibs.  Quite possibly the qt4 goodness.  But to fall into the trap of putting eye candy at the top of the agenda... hey, anyone heard of the enlightenment windowmanager recently?"
    author: "Adrian Baugh"
  - subject: "Re: Games"
    date: 2007-05-06
    body: "I don't think anyone among the KDE developers (if anyone can confirm, please post) put eye-candy on top of the agenda. Look at Solid, Phonon, Decibel, Strigi, Nepomuk... nice technologies that don't bring eye-candy at all, but they're extremely useful. not to mention the huge work that went into kdelibs (and I thank  aseigo for posting those reports on \"kdelibs mondays\" on his blog). \n\nI think that most of the people who call Plasma \"vaporware\" confused the goal of the project with its visions, or have unrealistic expectations."
    author: "Luca Beltrame"
  - subject: "Re: Games"
    date: 2007-05-07
    body: "> confused the goal of the project with its visions\n\ni think so, yes.\n\non the other hand, i'm now spending my time on the workspace instead of the libs. so .. maybe we'll start to see results ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Games"
    date: 2007-05-07
    body: "Whatever becomes of plasma in the end, you have already done a tremendous amount of work on kdelibs and KDE coordination and promotion in general.\n\nThanks a lot Aaron. You rock.\n\n"
    author: "The Vigilant"
  - subject: "Re: Games"
    date: 2007-05-07
    body: "Well, as an user, when you read this for example:\n\nhttp://plasma.kde.org/cms/1029\n\n...then you think about the uber-desktop, packed with eyecandy and useable for just everyone :)\n\nNo criticism, but i think that this sets the users expectations for KDE 4.0 very high, and now there are some people a little disappointed by the news etc, even if they have seen nothing of KDE 4 yet :)\n\nI`ll take it easy and trust the KDE developers. KDE 3 is already a beast of a desktop and i think KDE 4 will be even more powerful.\n\nSo thanks to all KDE Developers making it possible, you rock !"
    author: "Jan"
  - subject: "Re: Games"
    date: 2007-05-08
    body: "Correct, the user's expectations for 4.0 might not be met, most of the exciting stuff will be in KDE 4.0 however. It will take some time to make it into various applications, that will happen in the lifecycle of KDE 4 (so in 4.1, 4.2 and so on).\n\nWe (the Marketing Team) are trying to match those expectations to the reality, to make the difference between 4.0 and KDE 4 more clear. \n\nWe cannot expect users to stop dreaming of course. :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Games"
    date: 2007-05-08
    body: "> Perhaps nepomuk\n\nMaybe not vaporware, but the importance of it is definitely overestimated. Praise a bit more and I'll call it hype."
    author: "Carlo"
  - subject: "Can't lose ktron or ksnake"
    date: 2007-05-06
    body: "Those two are the games that save my rainy afternoons, along with wesnoth.  Please don't lose these for KDE4.  If you need some help, then I could but I'm a web programmer, not a GUI programmer.  With some patience I could learn but I really would love for those games to stay."
    author: "lapubell"
  - subject: "Re: Can't lose ktron or ksnake"
    date: 2007-05-07
    body: "Well, the real shortage seems to be with graphic artists, not programmers. Though I may be wrong, correct me if it is so. :)"
    author: "zonk"
  - subject: "Re: Can't lose ktron or ksnake"
    date: 2007-05-07
    body: "Well, as is always the case in open source development, we have a shortage of everything :). But the main reason we've dropped these games is because there was no-one maintaining them. Some of them were introduced many, many years ago and the person who originally wrote them is long gone or is working on more important things in KDE.\n\nThe main thing thing we need for each game if we want to see it make a comeback in KDE 4.1 is a programmer to say, \"I'll maintain this game and get it up to date.\" and since this announcement was made we've had a number of people come forward. We'll have to wait a while to see what sort of results we get but if any of you want to help out, join the kde-games-devel@kde.org mailing list and come see us on #kdegames in irc.freenode.net. We're all very friendly :D and we'll help you with getting started."
    author: "Matt Williams"
  - subject: "Generic SVG tileset"
    date: 2007-05-08
    body: "Looking at KBounce and KSquares I notice that they all use the same SVG block images and these images could be used further in games like KSirtet. This is a good thing and makes me wonder whether there is any merit in developing a generic 'game imageset' in SVG, or whether one exists already. "
    author: "Wilfred"
  - subject: "Re: Generic SVG tileset"
    date: 2007-05-08
    body: "Well actually, KSquares doesn't use SVG at all :D. It's just drawn programatically using Qt.\n\nA unified art theme would have both advantages and disadvantages. While it would mean less art had to be created and that there would be a consistent look across the games, it would also mean that all the games would look the same. It would also mean that games would not be able to be themed individually as the theme would have to replace the monolithic SVG file. Admittedly this is mostly a technical problem but I don't feel that it would be worth all our effort :)\n\nThe simplest way is probably to simply decide on common elements and create all the individual themes is a similar way."
    author: "Matt Williams"
  - subject: "More Games, please!"
    date: 2007-05-08
    body: "I know i know, games are time wasters etc.. and since this is \n\"kde games\" there probably wont be huge kde games released, \nat least not as part as \"kdegames\" 4.0 or whatever the next \nupcoming version will be.\n\nBut but but hopefully! smaller games can be done,\nmore smaller games, more more MOOOOOORE games :)\n\nI can never have enough games to showcase what can be \ndone. Think of many old amiga games, they were simple enough \nbut attractive - and worthwhile!"
    author: "she"
  - subject: "unified highscores environment"
    date: 2007-05-09
    body: "i know that these games mostly are 'distractions', but on a longer run, it is really interesting to compare highscores for different people.\n\nfor quite a long time, i have wished for a unified highscores framework for kdegames, which would allow for games to simply call some functions and save their scores to a centralised location, in an unified manner.\n\nthis would make it easy to move scores between user profiles, machines - and also would ease a lot the process of unifying highscores from different locations.\n\nthat would also lift some burden from game authors, allowing them to work on the game itself, not caring much about mechanism to save highscores :)\n\ni have no programming knowledge at all, so there is no way i could do anything in that area - but maybe somewhere somebody has thought about this, too ?\nany bugzilla wish item or something..."
    author: "richlv"
  - subject: "Re: unified highscores environment"
    date: 2007-05-10
    body: "I have been working on this for KDE4. There is tutorial on using the high score system at http://techbase.kde.org/Projects/Games/Highscores. It doesn't yet provide a way to save the highscore to a system-wide location but that is planned.\nCurrently there is a push to convert as many of the current games to use this new framework in order to provide a consistent style."
    author: "Matt Williams"
---
On May 1st, the <a href="http://games.kde.org/">KDE games</a> developer community held its monthly IRC meeting. This time the major topic was discussing which games would stay in the kdegames module for KDE 4 and which ones would have to be removed because they don't meet our self-imposed quality standards. Read on for a discussion of this decision.









<!--break-->
<p>
Our quality standards focus on having resizable, scalable interfaces as modern computers
now feature high resolution screens, and having non-resizable games decreases their usability, so now most KDE games use SVG graphics or similar techniques to achieve this functionality.
</p>

<table width="100%">
<tbody>

<tr>
<td align="center">
<a href="http://static.kdenews.org/dannya/katomic3.png"><img src="http://static.kdenews.org/dannya/katomic3-thumb.png"></a>
</td>
<td align="center">
<a href="http://static.kdenews.org/dannya/katomic4.png"><img src="http://static.kdenews.org/dannya/katomic4-thumb.png"></a>
</td>
</tr>
<tr>
<td align="center">
KAtomic <i>(KDE 3)</i><br /><br />
</td>
<td align="center">
KAtomic <i>(KDE 4)</i><br /><br />
</td>
</tr>

<tr>
<td align="center">
<a href="http://static.kdenews.org/dannya/kreversi3.png"><img src="http://static.kdenews.org/dannya/kreversi3-thumb.png"></a><br />
</td>
<td align="center">
<a href="http://static.kdenews.org/dannya/kreversi4.png"><img src="http://static.kdenews.org/dannya/kreversi4-thumb.png"></a>
</td>
</tr>
<tr>
<td align="center">
KReversi <i>(KDE 3)</i><br /><br />
</td>
<td align="center">
KReversi <i>(KDE 4)</i><br /><br />
</td>
</tr>

<tr>
<td align="center">
<a href="http://static.kdenews.org/dannya/kbounce3.png"><img src="http://static.kdenews.org/dannya/kbounce3-thumb.png"></a>
</td>
<td align="center">
<a href="http://static.kdenews.org/dannya/kbounce4.png"><img src="http://static.kdenews.org/dannya/kbounce4-thumb.png"></a>
</td>
</tr>
<tr>
<td align="center">
KBounce <i>(KDE 3)</i><br /><br />
</td>
<td align="center">
KBounce <i>(KDE 4)</i><br /><br />
</td>
</tr>

</tbody>
</table>

<p>
Over the KDE 4.0 development timeline, KDE games has welcomed two new additions, <a href="http://milliams.com/content/view/18/42/">KSquares</a> a
KDE implementation of the paper "squares" game and <a href="http://home.gna.org/kiriki/">Kiriki</a>, a Tali dice game.
</p>

<table width="100%">
<tbody>

<tr>
<td align="center">
<a href="http://static.kdenews.org/dannya/ksquares.png"><img src="http://static.kdenews.org/dannya/ksquares-thumb.png"></a>
</td>
<td align="center">
<a href="http://static.kdenews.org/dannya/kiriki.png"><img src="http://static.kdenews.org/dannya/kiriki-thumb.png"></a>
</td>
</tr>
<tr>
<td align="center">
KSquares<br /><br />
</td>
<td align="center">
Kiriki<br /><br />
</td>
</tr>

</tbody>
</table>

<p>
Unfortunately, there are around 10 games that did not made the cut, mostly because
no maintainer was found to work on the KDE 3 to KDE 4 transition.
<ul>
<li>Atlantik</li>
<li>KFouleggs</li>
<li>Klickety</li>
<li>KPoker</li>
<li>Kenolaba</li>
<li>KAsteroids</li>
<li>KSnake</li>
<li>KSokoban</li>
<li>KJumpingcube</li>
<li>KTron</li>
</ul>
If any of these games is on your list of favorite games, <a href="https://mail.kde.org/mailman/listinfo/kde-games-devel">join us</a> and start working on it so it makes a star comeback in KDE 4.1.
</p>

<p>
For more details, <a href="http://johann.pwsp.net/2007/05/02/which-games-are-moving/">read this commentary</a> from the kdegames module release co-ordinator.
</p>


