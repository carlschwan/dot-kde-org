---
title: "aKademy 2007 Sponsors Announced"
date:    2007-06-25
authors:
  - "jriddell"
slug:    akademy-2007-sponsors-announced
comments:
  - subject: "Thanks!"
    date: 2007-06-25
    body: "A huge thanks to all the sponsors for making Akademy 2007 possible!\n\nI look forward to hearing a lot about Akademy 2007 - no doubt there will come a lot of good stuff from it!\n\nHave fun KDE contributors!"
    author: "Joergen Ramskov"
  - subject: "Google and KDE 4?"
    date: 2007-06-25
    body: "\"We also expect to have some exciting news regarding Google and our 4.0 release soon.\"\n\nI long to hear more."
    author: "A. Frolenkov"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-25
    body: "It's not about code :)"
    author: "boo"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "Google will be the default search engine for Konqueror ;)"
    author: "bsander"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "Perhaps a I want more than a release party.\nBut that's just the beginning. I'm longing for world domination ;)\n\nNoticeable excludes regarding the sponsors:\nIBM, Intel and Nokia are AWOL."
    author: "Phase II"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "You missed the fact that Intel is a Patron of KDE, and thereby supporting aKademy indirectly (and the KDE community directly). IBM and Nokia are platinum sponsors of the Linux Foundation, so they're sponsoring aKademy indirectly through this organisation."
    author: "sebas"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "Well, correction: Intel and IBM are platinum sponsors of the Linux Foundation, Nokia is Silver Member."
    author: "sebas"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "Google will open up Google Desktop Search for KDE4?\n"
    author: "Jos"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "Two-way synchronization between Gmail Calendar and KCalendar?"
    author: "Darkelve"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "Or just google adding a KHTML recognition to Gmail and co ?"
    author: "kollum"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-27
    body: "Oh, add me in!\n\nThe 'basic html view' in Konqueror for Gmail and Yahoo is annoying.\n\nThat, and the 'strange' shortcut combinations (I mean Ctrl+Alt+Tab just to go to the next tab, wth...) are some of the things that are keeping me with Firefox. \n\nEven though Konqueror's integration with KWallet is soooo useful.\n\nI for one, will be glad when Konqueror is a separate browser,\nhopefully that can improve things a bit.\n\n"
    author: "Darkelve"
  - subject: "Re: Google and KDE 4?"
    date: 2007-07-02
    body: "you can change the browser identification to firefox or IE and you can get the 'real' gmail. it works just fine after that."
    author: "hvm"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "Maybe Konqueror is going to start getting some kickback from Google.  Last I heard, Konqueror is identified by Google as Safari, and Apple is getting our juice.  I could be wrong."
    author: "Louis"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-26
    body: "I bet it's something lame like GMail will finally work with Konqueror without it having to masquerade as Firefox.\n\nSome kind of funding would be brilliant, though, but I'm not going to get my hopes up :) Imagine what KDE could do with just a handful of extra full-time devs in KDE-PIM, or anywhere else, for that matter."
    author: "Anon"
  - subject: "Re: Google and KDE 4?"
    date: 2007-06-28
    body: "It's Google Toolbar for Konqueror.\nObviously."
    author: "reihal"
  - subject: "Novell? And Microsoft?"
    date: 2007-06-26
    body: "After what Microsoft is doing I don't think it's a good idea to have Novell as sponsor for akademy, what happen next year? You can ask directly to Microsoft and give them all KDE source code to create Windows Vista 2 - the next generation..."
    author: "pablo"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "Since KDE is an open source project, they already have the source code, you muppet :)\n\nIt would be nice if people didn't just knee-jerk \"Novell is teh Evil!1\" every time its name is mentioned.  Without them employing several key KDE developers, the KDE project would lose some of its momentum."
    author: "Anon"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "The KDE people was employed by SuSE when Novell was not in there. Novell is not so transparent in their choices..."
    author: "pablo"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "You are wrong: three of five current \"KDE team\" people were hired by Novell."
    author: "binner"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "Just for interest, who is in the 'KDE team' - I know many of you employed mainly in other roles also contribute a lot to KDE, but I didn't know Novell had five people working specifically on KDE."
    author: "Simon"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "The formal \"KDE team\" are Coolo, me, Dirk, Will and Lubos. For the larger \"KDE team\" see http://opensuse.org/KDE_Team (btw the authors of KPowersave and NetworkManager-kde are missing there).\n\n> I know many of you employed mainly in other roles also contribute a lot to KDE\n\nAnd inverse the KDE team members also contribute to other parts of the distro (like pushing the one CD installation media forward)."
    author: "binner"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "It is interesting to read the Blog of the Novell guys and see how they  implement Microsoft technology. Just look at the recent idea.opensuse.org site and look how many KDE projects are mentioned there. Suse was the best KDE distribution.\n\nSuse was taken over by hostile radicals. Our advantage is that the community spirit of KDE remains clean. We don't have these corporate bastards involved. Rather the winners of design beauty contests (trolltech) and true SME members of the community. That is corporate support which is not destructive.\n\nBut the problem is that SuSE was a really nice KDE distribution. We need a Gnome-pollution-free, Novell-patent free fork of SUSE. Or at least a bit of YaST for Debian.\n\nMore:\nhttp://www.hula-project.com/\nhttp://tirania.org/blog/archive/2006/Nov-04.html\nhttp://www.go-mono.com/monologue/\nhttp://www.planetsuse.org/"
    author: "Andre"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "Your facts here are pretty misinformed. \n\n> Just look at the recent idea.opensuse.org site and look how many KDE projects are mentioned there.\n\nYes, openSUSE's KDE interest must be dying down or not significant. Or perhaps not? SUSE employ more KDE developers to work directly on KDE than any other distribution. I mean, 3 of the 7 guys on KDE's technical board are employed by SUSE. Not exactly insignificant, is it? SUSE are constantly driving a lot of innovations in KDE; KNetworkManager, Kickoff, KDE4 porting, working on a plethora of parts of KDE (kdepim, kdenetwork, kdelibs, kdebase... you get the idea?) They're constantly sponsoring KDE in many ways (developer meetings, akademy, other ways that may come to light later, etc). \n\nJust in case you thought it was lacking then on the user side here, take a look at the http://files.opensuse.org/opensuse/en/4/49/OpenSUSE_102_survey.pdf, the openSUSE user survey -- one of the biggest Linux distribution surveys ever. 72% of KDE users. That is far, far more than any other major distribution ([k]ubuntu, Mandriva, Fedora, Debian), so it quite evidently has a KDE-rich community.\n\n> Suse was taken over by hostile radicals. Our advantage is that the community spirit of KDE remains clean. We don't have these corporate bastards involved. Rather the winners of design beauty contests (trolltech) and true SME members of the community. That is corporate support which is not destructive.\n\nIt is a strange phenomenon that certain persons like feeling persecuted because of the evil \"system\" etc. \n\n> But the problem is that SuSE was a really nice KDE distribution. We need a Gnome-pollution-free, Novell-patent free fork of SUSE. Or at least a bit of YaST for Debian.\n\n(i) it's SUSE or openSUSE (not SuSE, that changed years ago), (ii) it is still a wonderful KDE distribution (by far the best, IMO). SUSE is hardly 'polluted by GNOME'. I mean, YaST is Qt and pretty much the only reason you need Gtk on a KDE install is for Firefox, which isn't even default in a KDE installation. \n\nThe reason planetsuse has more GNOME bloggers' blogs in the last few days is because... the KDE guys at SUSE blog less. You're really going to start making generalisations based on blog topic numbers? IME, the GNOME developers blog a lot more (than the KDE guys). I'd be interested in seeing statistics from other distributions."
    author: "Francis"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "It sounds like Trolling but as a user I just saw suse messed up with RedCarpet, no one requested it, it was a ximian product, what surprise. then we had suse products, what did they do: Hula. And later Hula vanished into thin air. Suse created a new menu, very nice but with beagle dependencies. beagle? a yes! Surprise. It requires mono. You see the path. Now they want to implement Gnome button orders in YaST as their project.\n\nAnd a company which makes Nat Friedman to its desktop strategist? We all thought it was a joke but then you know, suddenly a Novell enterprise desktop was released based on Gnome, then Suse was spread as a free opensuse product, no gnome default yet because users didn't want it. Nice trick, Gnome for Money, KDE for free. I remember very well how he used the 3d desktop internal development to give Gnome an unfair advantage.\n\nthe problem I have with these guys is not Gnome or KDE but that they infect software with their favourite libraries and new developments, which then need to be finished or fixed by others. You cannot rule out that Mono has patent problems. In fact that is very likely. So why did they have to implement beagle with mono which puts us all at risk. You cannot tell me that c# is required or efficient for that task and you can't do it in C or c++. And part of the dirty fighting is also the planned incompatibility. Why doesn't GTK integrate into the KDE global menu bar which makes the setting virtually useless? Why do GTK applications not support the KDE file dialogue when run under KDE? Why doesn't OpenOffice and firefox integrate well into KDE? It is not a matter of developer skills but political. Sure, we will see that a Gnome developer will use the Freedesktop initiative to declare his own product a universal standard.\n\nAnd now the patent agreement with Novell that excludes everything that matters.\n\nIt is all about ethics. I want good and mature code, cooperation between developers, true and fair views about developments. I can accept crappy solutions but developers shouldn't try to tell me immature products are ready. I want reliable APIs, no Gstreamer madness. I want a community of good developers with good faith, an open environment. If developers use a library just because it is theirs, that is bad. I want them to do what is best. Kludge puts a high burden on others. Developers who push their technologies on users are ignorant.\n\nEverybody who learns to know QT gets how beautiful it is designed. It is that culture of beauty. I personally don't care about theming or blue or brown desktops. But code quality and community ethics are very important for me."
    author: "Andre"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-26
    body: "> suse messed up with RedCarpet, no one requested it, it was a ximian product, what surprise. then we had suse products, what did they do: Hula. And later Hula vanished into thin air.\n\nRefraining from that ZMD and RedCarpet are not the same, shouldn't you be happy seeing these projects \"fail\", having been made optional and removed in future versions then? Or is it just bitching for because of bitching?\n\n> Suse created a new menu, very nice but with beagle dependencies.\n\nYou talk about an *optional* plugin which extends the menu search functionality. Don't like it? Don't use it.\n\n> Now they want to implement Gnome button orders in YaST as their project.\n\nMore trolling based on wrong \"facts\": did you actually ever use SUSE? Did you notice that YaST dialogs have \"Cancel\" at the left and \"Accept\" at the right? Looks more like GNOME button order to me and the project is about having KDE button orders when YaST runs under KDE.\n\n> a Novell enterprise desktop was released based on Gnome [..] Gnome for Money, KDE for free.\n\nMore wrong \"facts\" at both ends: the enterprise desktop includes KDE as well and your \"free opensuse product\" is sold for money in shops.\n \n> he used the 3d desktop internal development to give Gnome an unfair advantage.\n\nHow so? Did he prevent access of the KDE team during the \"internal development\"? No. I regularly tested the stuff early last year (and found it as too immature for SUSE 10.1 efforts).\n\n> So why [..] Why doesn't [..] Why do [..] Why doesn't\n\nYou seem to blame Novell for a lot of stuff nobody/upstream has not done. And ironically the pieces of integration into KDE that OpenOffice.org has was done by a Novell employee.\n\n> It is all about ethics. I want\n\nAnd I don't want trolling, FUD and wrong facts...\n\n> Everybody who learns to know QT gets how beautiful it is designed.\n\nQuickTime? :-)"
    author: "binner"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-28
    body: "\u00abWhy doesn't GTK integrate into the KDE global menu bar which makes the setting virtually useless? Why do GTK applications not support the KDE file dialogue when run under KDE? Why doesn't OpenOffice and firefox integrate well into KDE? It is not a matter of developer skills but political.\u00bb\n\nSome of these stuff would result in making Gnome, KDE and other facilities dependent on each other (which results e.g. on longer loading times), which users wouldn't like. This is also not trivial stuff; I'm sure that if someone comes up with a nice way to do these, OpenSuse would bundle those features. Its not like other distributions have those going...\n\nAnyway, I agree there is some political influencing of why distributions allocate resources to some areas and not others (I would call them business influences though). It is my perceptions that distributions prefer to have their work force on distro-specific technology (especially integration work, like system config tools, menu/search/etc bundles, etc), because it makes it harder for their work to be simply \"adopted\" by other distributions. So the techs they come up with either discourage 3rd parties from using them, or at least forces others to spent resources on it, which means bugs fixed, features added - which they benefit from.\n\n\u00abWhy doesn't OpenOffice and firefox integrate well into KDE?\u00bb\n\nThis is simply wrong however.\n\nAs far as I know, OpenOffice integrates just as good in Gnome as it does in KDE; it translates high level drawing to their drawing engines, and uses their filepicker/print dialogs. (However, for technical it seems they don't cope very well with each other, so you need to have either one integration facility installed. OpenSuse installs the appropriate according to your choice to KDE or Gnome in the installer.)\n\nAbout Firefox, Netscape guys meant for Mozilla to be the development branch of the browser suit, and wanted to be able to pick its sources, polish it and release it close sourced to make some bucks. They already had a good platform infrastructure for Windows, they just needed some better base library for X11. Qt requires royalties for little benefits to them, since they already had an incredible stack of code to handle the interface (they pretty much just use GTK+ for drawing - which is reflected on the GUI - you can see that widgets don't quite look or feel like those from ordinary GTK+ programs).\nHaving Firefox using Qt for the interface rendering would be quite some work, but far from impossible. Its just that nobody really bothers (especially with gtk-qt-engine there is little reason for it; glitches that you may notice are because the interface is already glitched with GTK+, not because of that indirection layer)."
    author: "fast penguin"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-28
    body: "From a capitalist perspective it is perfectly fine to buy the leading KDE distribution and do with it whatever you want. \n\nHowever, from a market perspective it does not really make sense to invest 80% of the resources into what 20% of the users want.\n\nWe all want to play nice and be big buddies. So we notice some Stockholm behaviour. As a KDE user I am not pleased with Novell's corporate policy. Not at all.\n\nRegarding the global top menu bar. Where is it implemented in KDE? \n\nI noticed that the \"saving window\" often uses the \"open window\" terminology.\n\nFrom my own perspective GUI toolkits should be irrelevant. We will end up with a mixed desktop. Toolkits should perfectly adapt to the environment they are run in, be it windows, MAC OS or KDE or Gnome.\n\nMaybe a GTK app would need some KDE dependencies but I don't see why KDE would get GTK dependencies. Anyway, where is the relevant code of KDE located for that functionality?"
    author: "Andre"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-29
    body: "\u00abFrom my own perspective GUI toolkits should be irrelevant.\u00bb\n\nI don't think anyone disagrees with that, and there's plenty of people that actually do something about it, and work on writting standard specifications and on their implementations. Look at freedesktop.org. Of course, this is a lot of work, and you always work with technology of one generation behind, that is stable  and common between the desktops (for instance, nobody will work in integrating Plasma with Gnome just now).\n\n\u00abwhere is the relevant code of KDE located for that functionality?\u00bb\n\nAre you referring to the option to put the menu on the top of the screen, like a Mac? I dunno; if you want to work on that, I would start checking the KMenuBar code. I tried it outside of KDE, and it also works, so I guess it just makes use of an external XWindow. One issue with this is that in GTK the menu bar is layouted just as any other ordinary widget, but I guess with some ability you could work around that.\n\nOh, you probably also to implement MacOS menu bar for Qt-pure programs. And let's not forget the widely used Firefox and OpenOffice programs (that pretty much feature their own toolkits)."
    author: "fast penguin"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-27
    body: "novell is a sponsor that consistently quite gives to the project. they have a number of people who work on kde related things on paid time and they continue to support a truly large kde user base as well as the growing community around opensuse.\n\nat the same time kde is not managed, controlled or otherwise influenced by their ethics or politics. we also have a number of other financially and developmentally active backers preventing us from being a one legged project.\n\nit's pretty much that simple.\n\ni am not happy with how novell violated the social (though not the legal) contract with the free software community, but there are other venues to get that point across. companies bigger than small tend to be mottelled creatures of many hues; the kde guys tend to be of the bright, trustable and very worthy colours."
    author: "Aaron Seigo"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-27
    body: "+1\n\nI don't care who employs someone if they personally make a positive contribution.\n\nI don't care who donates money as long as they don't get control in return (and thanks to the nature of KDE organisation, they don't)."
    author: "Simon"
  - subject: "Re: Novell? And Microsoft?"
    date: 2007-06-28
    body: "Don't worry Tom Green! (Kidding)  There are just a lot of paranoid people in the world.  Perhaps Novell did cross the line.  But I don't think they are evil, have a hidden agenda, or are \"out to get us.\"  I'm sure they are trying to do the best they can and at worst made a wrong choice.  If I had a nickel for every time I accidentally did something stupid or made a bad choice, I'd have 15 cents. Just kidding, I'd be a millionaire.\n\nI think the world would be better off if we spent more time thinking about what is right with the world than what is wrong with it.  In that spirit I will say, I love KDE and I sincerely thank all the developers, users, educational entities, businesses, and even governments who have supported KDE.  Long Live KDE!\n"
    author: "Henry S."
  - subject: "Requesting for Sponsors"
    date: 2007-07-03
    body: "CMR College of Pharmacy sponsored by the M.G.R. Educational Society and is established in the year 2005 with the objectives of providing quality education in the field of Pharmacy to the students of rural and urban areas. The college accorded the approval from the All India Council for Technical Education and Pharmacy Council of India, New Delhi and affiliated to Jawaharlal Nehru Technological University, Hyderabad for an annual intake of 60. \n \nWe are happy to inform you that we are going to organize a two day seminar on Bulk Drug Industries - Opportunities & Challenges\u0094 in India (Theme: Save Life - Save Environment ) on 28th and 29th of July 2007. In this connection  we are looking forward for sponsors who extend the financial assistance to organize the seminar in a grand manner.\n \nThanks and Regards\n\nDheeraj(Student)\nraj_19875@yahoo.com\n\nMahalakshmi (Member ,organizing committee)\nPh no:9392351567.\n"
    author: "Dheeraj"
---
This Friday will see KDE contributors and our friends arriving from around the world to take part in the <a href="http://akademy2007.kde.org">KDE World Summit</a> in Glasgow.  It costs a lot of money to host a conference of this size, but as in previous years our industry partners have stepped up and made it possible through <a href="http://akademy2007.kde.org/sponsors/">generous sponsorship</a>.  Read on for the full list.















<!--break-->
<p align="center"><img src="http://static.kdenews.org/jr/akademy-2007-logos-montage.png" width="555" height="418" alt="sponsors logos montage" /></p>

<p>We are very pleased to welcome <a href="http://www.trolltech.com">Trolltech</a> as our Platinum sponsor.  Trolltech makes the Qt toolkit without which KDE would not exist.  Other products from Trolltech include their Java bindings Qt Jambi and Qtopia, a free software environment for mobile and other embedded devices.  Trolltech will also be sponsoring the buffet at our reception with Glasgow's Lord Provost next Monday.</p>

<p>This year we increase our list of gold sponsors by welcoming <a href="http://www.hp.com">HP</a>, one of the world's largest IT companies.  HP supports free software in numerous ways, take a look at their <a href="http://www.hp.com/go/opensource">hp.com/go/opensource</a> page for details.  <a href="http://kubuntu.org">Kubuntu</a> is back as a gold sponsor thanks to the continued support from <a href="http://www.canonical.com">Canonical</a>.  Kubuntu, one of the official Ubuntu distributions, has 24/7 support options available from Canonical.  The newly re-formed <a href="http://www.linux-foundation.org">Linux Foundation</a> is our third gold sponsor, showing the continued support from  <a href="http://www.linux-foundation.org/en/Members">their members</a> for the free desktop.  </p>

<p><a href="http://www.amd.com">AMD</a> is alphabetically our first silver sponsor.  Makers of CPUs and graphics chips for many of the world's PCs, AMD supports free software which has been known to work on their chips long before proprietary desktops can.  <a href="http://www.google.com">Google</a> supports KDE not only through their generous <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007">Summer of Code</a> programme but now also with sponsoring aKademy too.  We also expect to have some exciting news regarding Google and our 4.0 release soon.  <a href="http://www.mandriva.com">Mandriva</a> have been releasing their distribution with KDE as default for almost a decade.  We should have a very special gift to every Akademy attendee from Mandriva.  <a href="http://www.novell.com">Novell</a> becomes a silver sponsor this year.  With their <a href="http://www.opensuse.com">openSUSE</a> distribution and employer of more KDE developers than anyone else, they are among our most important industry partners.</p>

<p>Our Bronze sponsorship sees two new supporters of aKademy.  The <a href="http://B2BSoftware.com">B2BSoftware.com</a> portal from <a href="http://botw.org/">Best of the Web</a> offers a convenient place for IT managers and administrators to find software suppliers.  <a href="http://www.codeyard.net/">CodeYard</a> brings together free software and education and is sponsoring our <a href="http://akademy2007.kde.org/codingmarathon/schoolday.php">Schools and Edu Day</a>.</p>

<p><a href="http://www.linux-magazine.com">Linux Magazine</a> returns as our media partner.  Their magazine is where many of our contributors started and continue to learn about this Linux thing.  The whole event is hosted by <a href="http://www.cis.strath.ac.uk/">The Department of Computer and Information Sciences, University of Strathclyde</a>, and is being run jointly by <a href="http://ev.kde.org/">KDE e.V.</a> and <a href="http://kde.org.uk/">KDE GB</a>.</p>

<p>Many thanks to our wonderful sponsors, we could not run the event without their support.  Remember, you need to <a href="http://kde.org.uk/akademy/">register for aKademy</a> if you want to attend.  See you at the weekend.</p>















