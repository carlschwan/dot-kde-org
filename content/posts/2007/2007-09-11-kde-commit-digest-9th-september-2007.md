---
title: "KDE Commit-Digest for 9th September 2007"
date:    2007-09-11
authors:
  - "dallen"
slug:    kde-commit-digest-9th-september-2007
comments:
  - subject: "Lots of exciting updates!"
    date: 2007-09-11
    body: "Glad to see work on KRecipes resumed.  The new icons look great, and the new splash is stylish, but \"Recipes\" is spelled wrong.  Hope it's easy to fix.\n\nThanks Danny (though I was getting a bit worried :-)"
    author: "Louis"
  - subject: "Crisis averted."
    date: 2007-09-11
    body: "When the mail carrier doesn't deliver mail one day, I'm indifferent.  When I don't hear from my friends for a while, I don't get too worried.  When Newegg delays shipping, I get over it.\n\nWhen the Digest is late, I develop a nervous twitch and check the site compulsively, like a jealous boyfriend obsessing where his girlfriend is.\n\nThanks Danny - crisis averted!"
    author: "Wade"
  - subject: "Re: Crisis averted."
    date: 2007-09-11
    body: "I coudln't have said it better !\n\nThanks Danny for saving my day :-)"
    author: "sim0n"
  - subject: "Re: Crisis averted."
    date: 2007-09-11
    body: "Yeah, it was mostly finished last night, but you know what they say about the last 5%...\nNow, that last 5% on a horrible internet connection and we have a tale of woe and delay ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Crisis averted."
    date: 2007-09-11
    body: "Awesome work man.  One of a kind amongst open source projects really (as far as I know)."
    author: "Leo S"
  - subject: "Re: Crisis averted."
    date: 2007-09-11
    body: "You are showing that one does not have to write code to work for a software project. Thank you!"
    author: "Stefan"
  - subject: "Re: Crisis averted."
    date: 2007-09-11
    body: "Haha. I felt exactly the same - I ended Monday without reading the digest. Well, it's a great way to start your day, so I won't complain.\n\nOn a side note, Plasma has also got a pager now. Joy!\nhttp://hanswchen.files.wordpress.com/2007/09/snapshot2.png"
    author: "Hans"
  - subject: "how about Korundum?"
    date: 2007-09-11
    body: "it's nice to see progress on PyKDE4. Anyone knows what's up with korundum-Ruby and KDE4?"
    author: "Patcito"
  - subject: "Re: how about Korundum?"
    date: 2007-09-11
    body: "It is at about the same stage as the PyKDE4 bindings - it builds fine and you can write apps with the kdelibs classes. It could do with more documentation, more examples, cmake integration and so on, but the basic bindings are there. The Ruby Plasma bindings need a bit more work, but I think that will be a fun way of getting started with KDE4 Ruby programming."
    author: "Richard Dale"
  - subject: "Re: how about Korundum?"
    date: 2007-09-11
    body: "That's about right - I'm working on PyKDE4 docs, examples and related stuff and have been for a few weeks. I think Simon Edwards got the cmake integration taken care of and is keeping up with SVN changes, and also implemented a  new pykdeuic.\n\nI've been working with PyKDE4 for a few weeks on an application, and it's been very stable (as has the underlying kdelibs - not many changes lately). I haven't looked at plasma and Python yet, as I'm doing KDE4 from packages, and plasma still doesn't work with the packages I'm using. That should be fixed this week I've heard."
    author: "Jim Bublitz"
  - subject: "Re: how about Korundum?"
    date: 2007-09-12
    body: "\"It could do with more documentation\"\n\nCould the KDE team provide room for a wiki about ruby+kde/qt?\nA wiki is a lot better than static documentation because the \nusers can share their knowledge there as well.\n\nThe wiki is one of the reason I chose ruby-gtk back then, but I looked \nat the kde/qt examples for qt4 and i really *liked* it a \nlot. Looked actually better than ruby-gtk\n\nI'd happily contribute to a central wiki too!"
    author: "she"
  - subject: "Re: how about Korundum?"
    date: 2007-09-12
    body: "Here it is on the kde techbase wiki, feel free to help :-)\n\nhttp://techbase.kde.org/Development/Tutorials/Qt4_Ruby_Tutorial"
    author: "Patcito"
  - subject: "Re: how about Korundum?"
    date: 2007-09-12
    body: "Yes, please do, any help is very welcome. I've been meaning the convert the kde developers corner ruby bindings to the wiki for a while:\n\nhttp://developer.kde.org/language-bindings/ruby/index.html\n\nWe could start by moving that stuff to the wiki and updating for Qt4/KDE4 - a lot of it shouldn't need changing that much."
    author: "Richard Dale"
  - subject: "valgrind"
    date: 2007-09-11
    body: "Cool. Valgrind helped find a bug in KCachegrind. KCachegrind is a program to visualise output from (primarily) Valgrind profiling tools. They are both very cool."
    author: "Martin"
  - subject: "What about job progress plasmoid?"
    date: 2007-09-11
    body: "Just wondering if any more progress has happened with a plasmoid to show what tasks are in progress (e.g. what CDs are burning, files being downloaded thru Firefox/Konqueror, page priniting progress, mail being received, etc.) Someone even made a mockup on kde-look at http://kde-look.org/content/show.php/Tasks+Info+in+Less+Windows+%28mockup%29?content=33673&PHPSESSID=336c8b884f6a19f8916dfa02d9c4099a\nand Troy covered it in one of his articles: http://dot.kde.org/1169588301/\n\nSo how's it progressing? Has the info been abstracted into a date engine? It'd be great to see this in kde 4.0 plasma, but it'd be understandable if time ran out."
    author: "Alan Denton"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "\nHm... I see more and more discrete windows beeing added - I hope the developers did not forget about the main principle - making the desktop cleaner and more usable.\n\n<rant>\nSure - its nice to have 26 versions of the download view ;), but \n1) be serious - no one needs more than a tinny progressbar for 99.999% of his time using the computer\n2) PLS, dont repeat the situation when we have 10 applets serving the same purpouse and none of them is usable.\n</rant>"
    author: "Anon Ymous"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "Yes, I noticed these were starting to proliferate too.  It would be nice to see all progress dialogs from now on changed to tasks in the systray queue, with just an initial notification when the task goes into the queue.\n\nAnother important part of that idea was the ability to pause background tasks when you need the power for something else, to change their priority relative to each other, etc.  In particular, strigi's searches could be in there."
    author: "Lee"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "Sorry, not searches, I mean indexing."
    author: "Lee"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "You raise interesting points, but I should reply:\n1)Rather than have 26 different views for different tasks, you can have them all in one place. Doesn't seem too overwhelming. Of course, with KDE's fantastic configurability you'll be able to banish it from your sight if you so desire.\n2)The point with plasma is you only need to implement the data stuff once and it is reusable.\n\nBut I agree with you that a clean & usable desktop is crucial for KDE 4. This idea doesn't seem to detract from that, but I respect your differing view."
    author: "Alan Denton"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "> making the desktop cleaner and more usable.\n\nOMG p0nies, it completely slipped my mind. thank the goddess you were here to remind us. ;)\n\nseriously, though, we haven't forgotten. if you feel you too have a good handle on the concepts and ideas (which is turning out to be a relatively rare thing, btw), i recommend trying your hand at creating something with the tools at hand ... you may surprise even yourself."
    author: "Aaron Seigo"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-12
    body: ">> making the desktop cleaner and more usable.\n\n> OMG p0nies, it completely slipped my mind. thank the goddess you were here to \n> remind us. ;)\n\nHey, I know that all of you have the poster \"Keep it simple, stupid\" above your desks ;). Still, I care to much to leave everything for you, so I decided to act as a simple \"reminder\";) Don't be offended just treat all of us - whiners - as kind of \"Feedback/QA\". All knows We arn't perfect, but who is nowadays."
    author: "Anon Ymous"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "> Has the info been abstracted into a date engine?\n> It'd be great to see this in kde 4.0 plasma,\n> but it'd be understandable if time ran out.\n\nWhat I understand of Plasma now it would be possible to implement such thing. Don't be disappointed though if it's not available with 4.0 yet. I think the plasma developers are working their ass off to get Plasma ready for KDE 4.0. This means getting all basics right. All the potential and foundations will be in 4.0, but it could be that nice applets like a Job overview will emerge with 4.1 instead."
    author: "Diederik van der Boor"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "I'm really not familiar with the KDE API at all but I think to remember something like a KJob class. Maybe they just need a KJob DataEngine to be able to implement something like that mockup?"
    author: "Coward"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "Yes, but remember KDE is in \"bug-fixing\" mode now. Developers are working towards a 4.0 release. They would keep adding features forever, but at some point you'll have to fix what you have built and release that. That's the plan for the short-term now."
    author: "Diederik van der Boor"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: " in my previous comment: replace \"would add\" with \"could add\". I should have used the preview button. ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "a) plasma features are still going in for 4.0\n\nb) i will be doing monthly releases of plasma (the library, the app, the engines, the applets, etc) after 4.0, so you won't have to wait for 4.1 if you don't want to."
    author: "Aaron Seigo"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "Just one doubt based on this screenshot: http://kde-look.org/content/preview.php?preview=1&id=33673&file1=33673-1.jpg&file2=&file3=&name=Tasks+Info+in+Less+Windows+%28mockup%29&PHPSESSID=336c8b884f6a19f8916dfa02d9c4099a\n\nWill it possible to embed Plasmoids, or even part of a Plasmoid as it where a kpart?\nI ask because the screen have the nice idea of a download in konqueror being integrated in the main window. It could (mayeb) be done by using a Plasmoid inside it as a kpart.\n\nAs the success of the tabs and integrated search in Firefox has already proven, more windows is more annoyance, and getting rid of then when possible is a good idea. :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-12
    body: "That is a mock-up, and not a screenshot.\n\nI wish it were a screenshot."
    author: "T. J. Brumfield"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-12
    body: "Yes, sorry about that, typo or distraction error :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-12
    body: "I also think about that mockup and how to implement it with Plasma. I think it would be great to have a plasmoids-bar witch can be placed where ever you want.\n\nOn this bar you only see a button (in your picture the \"Tasks\"-Button) and when you click at it the whole plasmoid appears...."
    author: "Volker"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "There was an article about it long time ago http://dot.kde.org/1169588301/\ndunno it's current status"
    author: "Coward"
  - subject: "Re: What about job progress plasmoid?"
    date: 2007-09-11
    body: "It currently works (functions) as a system tray applet, which is even somewhat functional in plasma always. In addition, there is or was some work started on a Data Engine for the jobs, but as of yet, there is no real plasma applet to take advantage of that data engine that I'm aware of :)\n\nCheers"
    author: "Troy Unrau"
  - subject: "KTorrent"
    date: 2007-09-11
    body: "The KGet plasma applets look really nice. Hopefully a similar applet will be available for KTorrent! Keep up the great work guys!"
    author: "DITC"
  - subject: "Re: KTorrent"
    date: 2007-09-11
    body: "There is going to be an applet for KT. I have already experimented with plasma a month or so ago. Currently I'm busy with other stuff for KT, I'm hoping to get the plasma applet going again next month.\n\nNot quit sure how it is going to look like, but it will probably have a chunkbar like in the status tab and show some information about a torrent. Or maybe we can show a graph with the current speeds and stuff like that."
    author: "Joris Guisson"
  - subject: "Re: KTorrent"
    date: 2007-09-11
    body: "as Alan Denton (the previous comment on this level) points out; it would be nice to have a list of all 'jobs' available in KDE4. this way one could see the progress on all jobs (regular downloads, torrents, copy actions, cd burning, etc) in a singe (plasmoid) dialog.\n\n_c."
    author: "cies breijs"
  - subject: "Re: KTorrent"
    date: 2007-09-11
    body: "Indeed. Having KGet and ktorrent duplicate each others functionality is bad enough, now each duplicating the progress thing instead of using the general progress report technology in KDE 4 sucks even more..."
    author: "jospoortvliet"
  - subject: "Re: KTorrent"
    date: 2007-09-11
    body: "To my mind, as long as KTorrent and KGet both feed into the KJob system there's no reason why you couldn't have both - the general KDE progress applet would show percent complete for all jobs including torrents/KGet downloads, but if you want the detailed chunk display then you can use the specific applets."
    author: "Paul Eggleton"
  - subject: "typos"
    date: 2007-09-11
    body: "there are a couple of typos:\n\nin KGet Plasmoid\n\"tow different graphs\" --> \"tow different graphs\"\n\"downloads represented by his size\" --> \"downloads represented by their size\"\n\nwell but other than that it sounds very exciting. hope those typos are easy to fix! keep up the great work!!!\n\nGian Luca"
    author: "Gian Luca Bellini"
  - subject: "Re: typos"
    date: 2007-09-12
    body: "The first one is very easy to fix, indeed. I don't even have write permission, but I already fixed it several times. :-)"
    author: "Martin"
  - subject: "Konstruct?"
    date: 2007-09-11
    body: "Shouldn't there be a page here?\nhttp://techbase.kde.org/Getting_Started/Build/konstruct\n\nand while the difference is small, there is something here:\nhttp://techbase.kde.org/Getting_Started/Build/Konstruct\n\nHowever, in the latter link there's nothing do download. I was expecting a \"konstruct.tar\" or similar somewhere.\n\nOscar"
    author: "Oscar"
  - subject: "KDE Display Manager"
    date: 2007-09-11
    body: "Hello, love everybody's work. Just wonder if many people still work on kdm, as it is a bit behind GDM feature-wise e.g. no support for failsafe server. This means features like ubuntu's upcoming bulletproof-x (https://wiki.ubuntu.com/BulletProofX) can't be used for KDE, which is shame. So do people still work on KDM, cause I notice security fix in this digest but nothing else :( "
    author: "Parminder Ramesh"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-11
    body: "Interesting point, but maybe it just proves that *buntu as a whole doesn't take KDE as seriously as the competition :( Adept is also lagging behind synaptic. Why don't they hire more KDE developers?"
    author: "Charles Hinchy"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-11
    body: "\"Why don't they hire more KDE developers?\"\n\nBecause they are a GNOME-based distro, through and through.  Kubuntu and Xubuntu and mere afterthoughts, and it really, really shows."
    author: "Anon"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-11
    body: "> Because they are a GNOME-based distro, through and through.\n> Kubuntu and Xubuntu and mere afterthoughts, and it really, really shows.\n\nHave you seen the following comment at the Ubuntu pages? (https://wiki.ubuntu.com/BulletProofX)\n\n> A pre-requisite to this [Kubuntu / KDE support] is to have a gdm-style trigger for\n> going into failsafe mode. I checked with the KDM guys at UDS, and they confirmed\n> it lacks this ability, and it didn't sound likely that it would be added in time\n> for Gutsy, so we may need to defer supporting this for now.\n\nI didn't bother finding the related threads at the mailing lists, and the KDM developers probably have a good reason for deferring it. Don't use the \"Jump to Conclusions\" mat too fast, most things are not what they appear to be at first sight. ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-11
    body: "They talk about deferring it, but that's only for Kubuntu, I'm sure. So they will ship Ubuntu with it, and Kubuntu without it.\n\nSure, they contacted the KDM dev's, but I bet THEY wrote the GDM stuff themselves..."
    author: "jospoortvliet"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-12
    body: "GDM had fail-back feature already and it was just utilized for this purpose. Ubuntu developers only took KDE guidance back-end and developed GTK front end for Display in GNOME and this is also used as front end for new BulletProofX"
    author: "Lure"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-13
    body: "I stand corrected ;-)\n\nBut I do wonder, though, why GDM supported a feature only usable with the latest X.org released just a week ago?"
    author: "jospoortvliet"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-15
    body: "Has anyone an idea what the plans are for kdm in kde4 ? Is it already ported, exists a roadmap, or any other information ?"
    author: "Frank"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-11
    body: "That's true Canonical is delegating Kubuntu to a second plane, if you take a quick look to the new Ubuntu Gutsy features, most of them are really nice but some of the Kubuntu ones a really stupid and non-polished.\n\n- The artwork does not look as professional as Ubuntu (the first things I do after a Kubuntu install is to set up to normal the original KDE settings and remove the Trash applet in kicker)\n- How do you compare Synaptic with the buggy Adept (anyway is doing a good progress)\n- Beagle vs Strigi?\n- Compiz Fusion still sucks in KDE because the lack of integration with the virtual desktops\n- A virtual keyboard is a major feature?\n- A kde 3 version of Dolphin by default? perhaps the kde 4 version is quite estable and featured but the one included... why don't let Konqueror as usual?\n- Knetworkmanager never worked for me in Kubuntu but in Ubuntu perfectly\n- Please don't add patches to KDE as the launch notificacion icon (like Mac OS X do)\n- You need some tweaking until fonts look good, in Gnome they look crisp and readable\n- Add multi input (i.e Japanese) in Kubuntu is a mess, in Ubuntu is only one click in language preferences panel)\n- The redesign of the control panel is a good idea but is not well done. Always with sudden resizes and... very chaotic... hope will be fixed for KDE 4.\n\nIn conclusion... I'm a bit dissapointed with the lasts Kubuntu releases, Ubuntu is really nice with Gnome, for a distribution with KDE use Mandriva or Suse are a much better solution.\n\nSorry about the post, but I need to give a bit of feedback (also do in launchpad)."
    author: "biquillo"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-11
    body: "Hi\nthe list is not what is BEING done on Kubuntu Gutsy, I really agree that Kubuntu is not a first class citizen of canonical, and I agree that Kubuntu Feisty was not as good as Ubuntu, but in Gutsy, things are changed, Kubuntu has Restricted-Hardwares and Restricted-Multimedia manager and many more things, Adept, works well, I cannot agree that its Buggy, but it needs a complete ui redesign.Artwork is just good on KDM theme and Splash Screen's Kicker looks like crap, Icons should be changed, Crystal SVG is great but we need another icon theme, but I think Kubuntu is the best KDE Distro out there for Desktop, while its not as good as Ubuntu.\nsome of the points you said are KDE based not Kubuntu, and some other are about bugs...\nCompiz-Fusion is not nice on Kubuntu because they created to be on Gnome and gtk+, then KDE things came on for KDE (like aquamarine) and it still hasnt any KDE/QT settings manager (AFAIK), what you except from Kubuntu guys? they cannot do everything...\nStill I want to stick this with KDE4, KDE's development is on 4.x for a few years (at least 1-2), so Kubuntu uses KDE which is 'outdated', I hope KDE4 will be a step forward for KDE Distro's and in a few years, KDE Based distro's will have a better chance to attract normal people."
    author: "Emil Sedgh"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-11
    body: "Let's talk about Adept. I'm very sorry for the Adept developer (he even posts on PKO, maybe he's reading here) but this deserves a rant: how the hell is it possible to thik, imagine, draw and eventually implement such an horrorific, inusable, complicated, bad-looking user interface? Was the \"just copy Synaptic UI\" too difficult? NIH syndrome anyone? For example, why every fucking apt source has to get its own line with its own % bar, with line jumping in and out, bars scrolling and ground-shaking when I want to update the repos? A single, nice, simple, stupid, tiny, little window with only a general progress bar and maybe single text line dispaly the current repo, was it too difficult? And this is just an example, Adept is full of this crap.\n\nI'm so sorry because I do love KDE and I am a Kubuntu user, but Adept just sucks and stinks. Fullstop. "
    author: "Vide"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-12
    body: "Well, I have to agree. It made good progress, and lets not forget that before it the debian landscape was even worse for KDE-based software managers.\n\nFeature-wise it was getting there. But suddently Peter Rockai disappeared (and stopped blogging) and Adept got no better. Still that horribly slow filtering for example.\n\nOf course the GUI is terrible, and always was.\nBy the way you didn't mention the confusion there is in Kubuntu with the three different Adepts: adept_updater, adept proper, and adept_installer. What a mess.\nAnd Adept updater is fugly as hell. Compare it with the Ubuntu updater. And why does it have to be named \"Adept updater\". What is it updating? couldn't it be called just update notifier, like in Ubuntu?. I already suggested this long ago, but to no avail.\n\nFor Gutsy it seems Kubuntu gets single-click deb package installation. That's great. But the ugliness in the adept family remains.\n"
    author: "Vigilant"
  - subject: "Re:Adept & KPackage"
    date: 2007-09-12
    body: "So why can't we just combine the best featurees from adept and kpackage, to reduce duplication amongst distros? and why did they ever drop kynaptic to make adept?"
    author: "semi-noob"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-12
    body: "I have to disagree. \n\nI like Adept, even though filtering is slow. \n\nI quickly find apps, it works just nice, and the integration with adept updater is great. \n\nI can nicely search for apps and find new ones I might think interesting. \n\nFor me, there's not much to complain about, and the GUI looks nice. \n\nThe only thing keeping me from using it is, that i use Gentoo as main system, so sadly there is no apept for me, but I showed it to my wife and she quickly understood the GUI. \n\nIt will need updating to the new KDE4 GUI Guidelines, though, but that doesn't affect my vry positiv impression of its current GUI (slowness aside). \n\nWishes, \nArne"
    author: "Arne Babenhauserheide"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-12
    body: "Sidenote: My wife uses Kubuntu, but I think you guessed it :) "
    author: "Arne Babenhauserheide"
  - subject: "Re: KDE Display Manager"
    date: 2007-09-14
    body: "Man forget the individual progress bars for each repo line.  The main interface is far from optimal.  \n\nSynaptic is great with its combobox for all/installed/not installed/upgradable/scheduled changes/broken/etc.  Adept does the same thing with checkboxes, and does so in a rather confusing way.  For example, to get the list of upgradable packages, I have to uncheck \"not installed\" and \"installed\" (wtf.. updgradables are by definition installed).  \n\nThe \"preview changes\" taskbar button can be duplicated with more checkbox fun.  Check \"installed\", \"not installed\" and \"upgradable\" and on the next line unchecking \"no changes\" and checking \"install\" \"removal\" and \"upgrade requested\".  Or more properly, the preview changes button duplicates that task.  The second line of checkboxes is pretty much obsoleted by the \"preview changes\" button.  Of course, \"preview changes\" uses the whole window for its list with no way to search.\n\nSynaptic has it right with the combobox for various situations.  One could see various lists of packages like all installed, all that are upgradable (and synaptic didn't try to pretend there was no overlap between those two groups) and scheduled changes, all while maintaining the search widget to quickly find a package no matter which list one was looking at.\n\nSynaptic has a separate area to show the details of a package, including dependencies, reverse dependencies etc.  In Adept, very basic info is shown *in the package list* once the arrow next to the package is expanded.  I suppose it's nice to see basic info for more than one package at a time, but it makes the list look like a mess.  For anything about dependencies or other info though, one must click the \"details\" button that then replaces the normal GUI wholesale.  One might argue that it's nice to have the whole window available for information, but synaptic showed it just wasn't necessary.  And even with all that extra room, Adept manages to not show reverse dependencies *at all*.\n\nNow, newer versions of Synaptic have done away with the combobox for all/installed/etc and use a list to pick from.  Still easier than the checkbox puzzle of adept.  Newer Synaptic has also done away with the quick search bar, and I find that less than useful.  Synaptic < 0.5 was just fine, and I don't know why they've futzed with it so much, but it's *still* better layed out than Adept.  List of package states to choose from > checkboxes that don't make sense.  Separate info area > hijacking the whole window to show info for a package.  Showing scheduled changes in the same list area > hijacking the whole window to do so.\n\nAdept is workable, and what I use and I'm glad for a competent KDE apt front end.  I would love to see the UI redone (see above) but I am still grateful that Adept exists, so I apologize for the bitter tone."
    author: "MamiyaOtaru"
  - subject: "Adept and unneeded dependencies"
    date: 2007-09-15
    body: "One feature that Adept needs (and that Synaptic/Apt/Aptitude all have) is the ability to remove all dependencies of a package once the packages is removed. This saves a ton of disk space."
    author: "Bill"
  - subject: "unmaintained applications ?"
    date: 2007-09-11
    body: "Where can one finds  more information about the applications that have been removed ? There's only a quick sentence in the introduction about this, and the line \"Move all application which are listed on techbase' blackhole page to this area.\", with some links, most of them either dead (\"Sorry, this page is not available.\") or with some errors (\"unknown location, HTTP Response Status\n404 Not Found, Python Traceback..\").\n\nThanks for the commit digest, i read it every week."
    author: "orzel"
  - subject: "Re: unmaintained applications ?"
    date: 2007-09-11
    body: "http://websvn.kde.org/tags/unmaintained/"
    author: "Anon"
  - subject: "Re: unmaintained applications ?"
    date: 2007-09-11
    body: "I've seen that kooka is unmaintained, but is there a scanner engine on kde4?"
    author: "weintor"
  - subject: "Re: unmaintained applications ?"
    date: 2007-09-11
    body: "Yes, libkscan:\n\nhttp://websvn.kde.org/trunk/KDE/kdegraphics/libkscan/"
    author: "Anon"
  - subject: "Re: unmaintained applications ?"
    date: 2007-09-11
    body: "All the commits to move the applications were done by Tom Albers and are covered in the selections section of the Digest (the actual apps moved can be deciphered from the changes section):\nhttp://commit-digest.org/issues/2007-09-09/moreinfo/709924/#changes\nhttp://commit-digest.org/issues/2007-09-09/moreinfo/709937/#changes\nhttp://commit-digest.org/issues/2007-09-09/moreinfo/709938/#changes\n\nDanny"
    author: "Danny Allen"
  - subject: "extragear great news!"
    date: 2007-09-11
    body: "Seems like a KPlayer port to KDE4 is under way!\n\nhttp://websvn.kde.org/trunk/extragear/multimedia/kplayer/\n\nI'm yet to try to compile it though, but still, this is awesome news!"
    author: "aha"
  - subject: "Re: extragear great news!"
    date: 2007-09-12
    body: "yeah, cool it will be a real test for phonon, let's hope the next time they will change the name from kplayer to something more appealing "
    author: "djouallah mimoune"
  - subject: "usability"
    date: 2007-09-11
    body: "Nikolaj Hald Nielsen committed changes in /trunk/extragear/multimedia/amarok/src:\n\"By applying the highest levels of advanced user interface design and usability knowledge, it was decided that the back button should like, you know, skip to the previous track, which it now does\"\n\nSee? See? KDE actually DOES care about usability!!!\n\n\n\n:D"
    author: "jospoortvliet"
  - subject: "Re: usability"
    date: 2007-09-11
    body: "And another proof of that, this time slightly more serious:\n\nPino Toscano committed changes in /trunk/KDE/kdegraphics/okular:\n\"Remove the \"Show Search Bar\" option, mostly unuseful.\"\n\nI love this. Remove options nobody needs... KDE needs more of this. See the work on KGet, pretty neath as well!"
    author: "jospoortvliet"
  - subject: "Re: usability"
    date: 2007-09-11
    body: "What - search bar in kpdf/okular was removed? I am using it all the time :(\n"
    author: "m."
  - subject: "Re: usability"
    date: 2007-09-11
    body: "no, the option to show it or not. Of course the bar is still there :)"
    author: "hmmm"
  - subject: "Re: usability"
    date: 2007-09-11
    body: "What a relief :)\n\nThanks."
    author: "m."
  - subject: "Re: usability"
    date: 2007-09-11
    body: "My guess is the option to disable the little search bar on the thumbnails list was removed (not the search bar itself, just the option to hide it)."
    author: "Sutoka"
  - subject: "Re: usability"
    date: 2007-09-11
    body: "You are guessing right. It was removed as we went through the settings and looked for options that are no longer needed or infrequently used. We had to decide between\n\na) keeping the setting and extending it to include the filter lines of all four tabs\nor\nb) removing it\n\nWe opted for b) since we want to keep things simple and clear"
    author: "Florian Graessle"
  - subject: "Re: usability"
    date: 2007-09-12
    body: "And you're doing a grat job with Okular, indeed.\nOkular, Kget and Dolphin (if it doesn't get too crowded :P ) will be for sure the gems of KDE 4.0. Go on!"
    author: "Vide"
  - subject: "Bugfixes"
    date: 2007-09-12
    body: "It seems like a whole lot of old bugs got fixed. Impressive."
    author: "Dima"
  - subject: "Akonadi?"
    date: 2007-09-12
    body: "What's the status on Akonadi, and KDE PIM in general?"
    author: "User"
  - subject: "Re: Akonadi?"
    date: 2007-09-13
    body: "Work is going on, but it'll be 4.1 material."
    author: "jospoortvliet"
  - subject: "KMLdonkey 2.0"
    date: 2007-09-14
    body: "Yeah! I thought I would have to start using the web interface while switching to KDE 4, it's great to see it has been ported. I hope they also took the chance to fix some of the important bugs like when you search for something and you see the same file repeated many times. You rock!"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Konqueror?"
    date: 2007-09-15
    body: "I see news about what seems like every other piece of KDE I love being improved for 4.0. But I haven't heard much about Konqueror \n\nIs webkit being used? or is khtml staying\n"
    author: "Skeith"
  - subject: "Re: Konqueror?"
    date: 2007-09-15
    body: "Konqueror is such a complex app that's there's basically not all that much that *can* be done with it, to be honest, so I don't see much scope for improvement to the Konqueror shell from here on in.  Even adding a feature like Undo Close Tab - which should be largely trivial - has proven to be a huge pain in the ass.\n\nIt will certainly be using the KHtml KPart for KDE4.0, with the possibility of a switch to a WebKit KPart at a later date.  Personally, I'd prefer to see a dedicated, KDE-integrated, Webkit-based browser at some point (a kind of \"Dolphin for the Web\", if you will :)), with Konqueror left as the swiss-army knife tool of power users (like me ;))."
    author: "Anon"
  - subject: "Re: Konqueror?"
    date: 2007-09-16
    body: "I heard a lot of talk about just that. People wanted to make a browser akin to Dolphin for web browsing. But I can't find anything it because its name lead to such rage (people defending and attacking it) that it seems to have vanished. They wanted it to be named Napoleon (the little conqueror)."
    author: "Skeith"
---
In <a href="http://commit-digest.org/issues/2007-09-09/">this week's KDE Commit-Digest</a>: Colour Picker and Welcome applets appear for <a href="http://plasma.kde.org/">Plasma</a>. Many bugs fixed, especially through the merge of the <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> project "<a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=9064143E62AF5BA6">KRDC Revamp</a>". A KPart created, amongst other improvements in <a href="http://edu.kde.org/marble/">Marble</a>. Support for <a href="http://freedesktop.org/wiki/XesamAbout">XESAM</a> UserLanguage queries in <a href="http://strigi.sourceforge.net/">Strigi</a>. More work, especially in playlist handling, for <a href="http://amarok.kde.org/">Amarok</a> 2.0. Improved search interface in KSystemLog. A return to work on <a href="http://krecipes.sourceforge.net/">KRecipes</a>. <a href="http://edu.kde.org/kvoctrain/">KVocTrain</a> is renamed Parley. Restart of development on a successor to the <a href="http://eigen.tuxfamily.org/">Eigen</a> math library, Eigen2. Start of a port of <a href="http://www.kmldonkey.org/">KMLDonkey</a>, a file sharing frontend, to KDE 4. Parts of the Cokoon decorator infrastructure ported from Python to C++. Security fixes in KDM. Work on page effects in <a href="http://koffice.kde.org/kpresenter/">KPresenter</a>. <a href="http://kross.dipe.org/">Kross</a> bindings for the <a href="http://www.falconpl.org/">Falcon programming language</a>. Import of PyKDE4, new Python bindings for KDE development. KDE SVN housekeeping sees the move of a variety of unmaintained applications to more relevant locations with regard to the KDE 4 release.


<!--break-->
