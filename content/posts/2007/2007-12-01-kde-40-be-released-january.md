---
title: "KDE 4.0 to be Released in January"
date:    2007-12-01
authors:
  - "sk\u00fcgler"
slug:    kde-40-be-released-january
comments:
  - subject: "API/ABI freeze?"
    date: 2007-12-01
    body: "How does this affect the API/ABI freeze? There are a couple of interesting changes floating around (to KConfig, mainly) that would be beneficial to KDE4, but which are ABI-incompatible, so if they are not added before the freeze, we will have to wait until KDE5.\n\nIs there a list of proposed BIC-changes anywhere and their work/ benefits analysis anywhere? KConfig is pretty core, and I think it would be nice to spend a short time getting it nearer to \"perfect\" than to spend the next 5+ years using ugly workarounds."
    author: "Anon"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-01
    body: "There are no ABI/API changes planned right now."
    author: "sebas"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-01
    body: "I'd like the kcolorpicker to handle transparency.."
    author: "Greg"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-01
    body: "Well QColor handles transparency, so I don't see why making kcolorpicker handling transparency is not possible while keeping API/ABI ? And I also would like to point that API/ABI stability doesn't mean you can't add new function and functionnalities to a class."
    author: "Cyrille Berger"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-01
    body: "Could presumably go into 4.1 in 6-12 months' time though?  I don't see why it would have to wait for kde5..."
    author: "Adrian Baugh"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-01
    body: "The proposed changes were binary incompatible, and KDE is committed to API/ABI compatibility throughout its major versions (this is how the KDE versioning system works).  A binary incompatible change thus cannot occur after the API/ABI is frozen (this is certainly frozen at the release of 4.0, and I believe even before 4.0) and before 5.0."
    author: "Anon"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-02
    body: "Ow, I might add the question:\n\nWhich changes are in your opinion needed to prevent those ugly workaround? Until now, for the release team, there's no visible reason to break BIC. Actually, no one has pointed out that there's a particular problem, why it would need a BIC exemption. So that's why I said that nothing's planned. Now if someone comes up with a good reason and explanation, we will seriously consider it, but until that does happen, we assume binary and source compatibility."
    author: "sebas"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-02
    body: "Well, it's entirely possible that my info is out of date :) It's based on your original delay proposal on the Release Team mailing list.\n\nSee if you can find Oswald Buddenhagen (ossi), Thomas Braxton)(?) or Andrea Pakulat (apaku) and ask if there are any BIC KConfig updates that they still want to add.  I think ereslibre and dfaure should know about the KJob stuff.\n\nHopefully, all this has gone in already and I'm worrying needlessly :)\n\n"
    author: "Anon"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-02
    body: "I'm aware of both of these, but it's not clear to me that those are really urgent. And neither David or Oswald did point out that this change is really a good idea to put in, they didn't make clear that it's both necessary changes or punished with 5 years of ugly workarounds.\n\nI assume that all of the above people are aware that they should point it out. So yes, there were possible changes, and it has been brought up within the Release Team, but nobody came up with good reasons to break BIC (yet). If there are those issues, do contact the Release Team and ask for a BIC exemption, however."
    author: "sebas"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-02
    body: "Ok, sounds like it's not as urgent as I feared - you can ignore this whole thread, after all ;)"
    author: "Anon"
  - subject: "Re: API/ABI freeze?"
    date: 2007-12-02
    body: "AFAIK all the KConfig changes that have even been discussed are to non-exported classes, so there shouldn't be any BIC changes."
    author: "Thomas Braxton"
  - subject: "Disappointing"
    date: 2007-12-01
    body: "oh well...\n\nguess i am disappointed. no xmas kde!\n\nThis is probably the better solution, to fix all the outstanding issues without over-pressuring the developers or burning them out, but ... i cant help myself... i am slightly disappointed :("
    author: "she"
  - subject: "Re: Disappointing"
    date: 2007-12-01
    body: "I 2nd this. I am slightly disappointed, too. But I think it is definitely the better choice to release a good KDE4 in january than an clearly unfinished one in december.\n\n"
    author: "me"
  - subject: "Re: Disappointing"
    date: 2007-12-01
    body: "Well, I was so looking forward to being able to install KDE4 for Christmas and I thought there would probably be another RC but I really hoped that the release would still have been this year. Anyway, my mind also tells me that this is probably the best solution considering the current state of development."
    author: "WPosche"
  - subject: "Re: Disappointing"
    date: 2007-12-02
    body: "Well, you can always install an SVN snapshot and follow the progress towards 4.0 in more detail. It's quite well-documented, and I wouldn't consider it hard. You set up the development environment once, then it costs you two commands to get an updated 'latest KDE'. How about that?\n\nIt's actually really exciting to observe the progress on KDE 4.0 these days..."
    author: "sebas"
  - subject: "Re: Disappointing"
    date: 2007-12-01
    body: "I think this is a good thing. The only difference is the release that you will be installing will be the latest RC instead of a final release. The release will be just as stable as it would if they just called it the final release but this is more fitting with the standard versioning scheme. I think any outstanding issues should be fixed before it is considered a final release.\n\n"
    author: "CptnObvious999"
  - subject: "Re: Disappointing"
    date: 2007-12-01
    body: "was counting down the days to installing the final, waaaaaaaa, I want a refund ;)\n\nGood decision, best to release something that you are happy to release, than release something that makes people run to the hills, aka Vista."
    author: "Richard"
  - subject: "Re: Disappointing"
    date: 2007-12-02
    body: "as odd as this may seem, and this surprised me as well,  I never really had any problems with Vista. I mean, it is an inferior product in most parameters, such as software installation, management and updates. It's  less secure and less stable.  a lot of things aren't as well organized as in my distribution and simply don't make sense, and there are regressions from Windows XP. on the other hand, aside from poor design choices, I didn't really encounter any problems. (aside from malware infections, but who counts those nowadays?)\n\nfrom the article:\n\"...the KDE community hopes to have a KDE 4.0 that will live up to the high expectations for it.\" - I assume this is talking more about stability than feature completeness?"
    author: "yman"
  - subject: "Re: Disappointing"
    date: 2007-12-01
    body: "If you can't wait, there's ways to try out KDE 4 in its current state right now:\n* openSUSE has been preparing KDE Four Live CDs for a while already.\n* The Debian KDE team is now offering something similar.\n* Fedora Rawhide (i.e. what will become Fedora 9) will include KDE 4 in a few days (the import has already started). There will also be live images, at least for the 3 test releases (alpha, beta, RC), probably also for some inbetween snapshots.\n* Then there's the add-on repos for openSUSE, Kubuntu and probably others.\n* And finally, you can always build from source, that's what kdesvn-build and the snapshot tarballs are for.\n\nAs others have already said, calling the release \"4.0 final\" sooner is not going to make it any less buggy, so waiting for it to actually be releasable before labeling it that way makes sense entirely. And it wouldn't significantly change your options to get KDE 4 anyway, distros aren't going to ship a version with KDE 4 as default the day KDE 4.0 is released, and I think this slip isn't impacting any distribution's schedule in any significant way (if you're from a distro which is impacted by this, feel free to prove me wrong though)."
    author: "Kevin Kofler"
  - subject: "Re: Disappointing"
    date: 2007-12-01
    body: "> for the 3 test releases (alpha, beta, RC)\n\nTo clarify: I mean the 3 test releases of Fedora 9 here. (Compared to earlier Fedora releases, they have been renamed test1->alpha, test2->beta and test3->RC.) I know KDE 4.0 is in RC already. ;-)"
    author: "Kevin Kofler"
  - subject: "Re: Disappointing"
    date: 2007-12-02
    body: "> guess i am disappointed. no xmas kde!\n\nWhat would have been \"KDE 4.0\" will simply be RC 2.  Just install RC 2 and pretend it's KDE 4.0.  It will be the exact same code.  Calling it \"4.0\" doesn't magically fix bugs.\n"
    author: "Jim"
  - subject: "Re: Disappointing"
    date: 2007-12-04
    body: "Amin to that, brother! xD"
    author: "Dread Knight"
  - subject: "Re: Disappointing"
    date: 2007-12-12
    body: "I was a little disappointed too, i was studying for exams these past two days as was thinking yesterday \"kde4 is out\" then i saw this today, oh well... as someone stated before its better for them to iron out the kinks and push the release date back rather then release buggy software and meet their deadline (ala microsoft)"
    author: "Brando"
  - subject: "how do you say that in english?"
    date: 2007-12-01
    body: "I think you made the right decision, because \"Gut Ding will Weile haben\"."
    author: "kdeuser"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-01
    body: "Please watch your language!  This site is also read by children.\n"
    author: "KDE User"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-01
    body: "Hey, calm down.\n\nAfter all, he didn't say \"Gut Kind will Keile haben.\""
    author: "bash"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-03
    body: "Indeed, after all \"schei\u00dfe und kartoffeln\"."
    author: "NabLa"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "\"Gut Ding will Weile haben\" is a german \"Volksweisheit\", something like \"farmer's wisdom\". The german \"Ding\" means \"thing, something, Object\" and not what it means in English! The word by word translation would be \"Good things need time to maturate/ripen/age\".\n\nThe intention is like: \"Rome wasn't built in a day\"."
    author: "Another KDE User"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "LOL, but your German is almost as good as my Spanglish.\nEin bisschen Spa\u00df muss sein :)"
    author: "Bobby"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "Really? Children read the dot? Hmmm ... I'm not to sure about that. \n\nBut I remember an English girl which couldn't drive on german highways without laughing whenever she saw the sign saying \"Ausfahrt\", which means \"exit\" in German. Should we better take them down? Now, do you know how the hungarian word for \"vegetables\" sound to germans ... ?\n\nNix f\u00fcr ungut!\n\n\n"
    author: "furanku"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "Like I sometimes make fun with the word fast food, fast means almost or nearly in German so \"fast food\" would be like almost or nearly food."
    author: "Bobby"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-03
    body: "seems like a pretty accurate translation actually ;)"
    author: "Borker"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "It's even funnier in dutch. Aus = uit, fahrt = vaart, so a literal translation would be \"uitvaart\", the dutch word for funeral. "
    author: "walter"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-04
    body: "It's funny that in Latvian \"Vista\" means hen or chicken. So much for Microsoft's best operating system..."
    author: "Janis"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-16
    body: "Microsoft's BEST operating system?  You mean 3.1? Ja, weil das Spa\u00df macht!  Ich spielte SkiFree, und DOS war... oh yeah... gut times.  "
    author: "Pacz"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-21
    body: "In Italian sVista means error or mistake......  I think that the name is right for the best windows ever made :)"
    author: "zp"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-01
    body: "I'm german too but I'd say:\n\nGood things need some time (to get them done).\n\nI think it's a really good decision to delay the release because if the actual desktop (plasma) isn't polished enough the people will say that KDE4 isn't good, even when the underlying libaries and everything else is rock-solid."
    author: "Bernhard"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-01
    body: "If Google translator is to be trusted, and if I guessed correctly that that phrase is German, then it would be \"Good things take time\".\n\nAnd I agree.  Good things do take time, and this is probably a good decision.  'Course it won't stop many of us from simply running the RCs while waiting :)"
    author: "mactalla"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "You guess correctly that phrase is meant to be German."
    author: "MamiyaOtaru"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "Well, it probably is somewhere.  Just not the type I know.  But I'm not the repository of all knowledge."
    author: "MamiyaOtaru"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "I don't speak germen, but I do agree that all good things take time.  I am a heavy KDE user, so I look forward to the final release.  "
    author: "Jason"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-01
    body: "The equivalent saying in English would be \"Good things come to those who wait\" i think :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "You could say \"Rome wasn't built in a day\" or \"Haste makes waste\".\n\nSee more solutions at dict.leo.org:\n\nhttp://tinyurl.com/ynnbm5"
    author: "gissi"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-02
    body: "I'm Slovene but i think:\n\nGood things will have awhile\n\nand in my language:\n\nDobre stvari trajajo.\n"
    author: "mihas"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-03
    body: "In Brazil we say \"N\u00e3o coloque a carro\u00e7a na frente dos bois\".\nMeaning \"Don't place the wagon before the bulls\". :)\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-03
    body: "Bulls don't pull carts. Oxen ( former bulls ;-) do."
    author: "Roberto Alsina"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-03
    body: "I speak Portuguese.  I think you'll be happy to hear that the expression \"N\u00e3o coloque a carro\u00e7a \u00e0 frente dos bois\" does literally mean \"Don't place the wagon before the oxen\".  The portuguese word for \"bull\" is \"touro\", not \"boi\", like the original poster suggested.\n"
    author: "Cultural Sublimation"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-04
    body: "This thread makes me smile with all the languages being tossed around.  Shows how diverse we as a community are.  Even if we do use English as the common tongue, we're here representing many more backgrounds."
    author: "mactalla"
  - subject: "Re: how do you say that in english?"
    date: 2007-12-14
    body: "Sorry for that. I didn't know that word :)"
    author: "Iuri Fiedoruk"
  - subject: "Awesome"
    date: 2007-12-01
    body: "You go guys, who cares if it takes longer, I run KDE4 for almost all my critical apps and find it very easy to use, I would prefer a late clean product than a rushed on time one. Keep up the good work guys!!!!"
    author: "Quentin"
  - subject: "Re: Awesome"
    date: 2007-12-02
    body: "Exactly. No need to rush, better deliver a better product. The reputation of the project will only be greater because of it; people will quickly forget about the delay if the product really kicks ass."
    author: "Michael G.R."
  - subject: "Seemed pretty far off"
    date: 2007-12-01
    body: "This week I tried RC1 from Ubuntu packages, but it seemed more like a beta than close to release:\n\n*) The screen locker crashed.\n*) Konqueror wouldn't accept cookies.\n*) The system tray was in the center of the screen, always, and on top of everything.\n*) When you tell it to shut down, it asks again what you want to do.\n*) When I zoomed out and then back in, the wrench at the top right was partially off the screen. \n\nI am not sure where to report these bugs - I put the crash into bugs.kde.org, but I am not sure if that's the right thing to do at this stage of development."
    author: "Mitch Golden"
  - subject: "Re: Seemed pretty far off"
    date: 2007-12-01
    body: "RC1 is over 5000 revisions out of date, so verify the bugs with a more recent version, first.\n\nbugs.kde.org is fine; there is also the Krush wiki:\n\nhttp://techbase.kde.org/Contribute/Bugsquad/KrushDays\n\nand IRC channel, #kde4-krush on irc.freenode.net"
    author: "Anon"
  - subject: "Re: Seemed pretty far off"
    date: 2007-12-02
    body: "All but one of those issues are fixed already.  The one that's not fixed yet is that when logging out from the menu is chosen, it pops up the logout dialogue instead of directly logging out, hardly a serious issue IMO."
    author: "sebas"
  - subject: "Re: Seemed pretty far off"
    date: 2007-12-03
    body: "> This week I tried RC1 from Ubuntu packages\n\nThere is one issue too: Ubuntu's KDE 4 still has packaging bugs. So the bugs you see are not nessecairy KDE bugs. It's a bit unfortunate, since it gives KDE a bad name."
    author: "Diederik van der Boor"
  - subject: "Re: Seemed pretty far off"
    date: 2007-12-03
    body: "Ubuntu gives Linux a bad name on the whole, if you ask me."
    author: "T. J. Brumfield"
  - subject: "Re: Seemed pretty far off"
    date: 2007-12-05
    body: "Why? can you support your opinion in-depth?\nAnyone can criticize, but opinions is better in pair with supporting explanations imho."
    author: "Larpon"
  - subject: "Good decision"
    date: 2007-12-01
    body: "I think this was a good decision. I checked out openSUSE packages yesterday and it still has many issues."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "the opensuse packages are really out of date... the panel isn't working correctly, the systray also not, on my system it has no background (yes i've cleared my .kde4 folder) ... i would recommend to try the KDE4 vm image to get a good overview of the progress KDE4 is making. You'll be suprised!"
    author: "Bernhard"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "I'm using the KDE 4 repo for openSUSE (and get updates nearly every day), and all of these issues are fixed. I sometimes switch my session from KDE 3 to 4 by pkilling Kdesktop, replacing KWin3 with KWin4, and starting Plasma. I'm not pkilling Kicker, this causes issues with the tray icons of KDE 3 apps, but beyond that KDE 4 is working quite well."
    author: "Stefan"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "Someone asked me for the repo URL. It is\n\nhttp://software.opensuse.org/KDE:/KDE4/openSUSE_10.3/\n\nThe current version is 3.96.2-8.2 (compared to the old-as-the-hills 3.93.0.svn712059-6 in the default repo)\n\nI think this is the right time to give a big virtual hug to the packagers at openSUSE. You deliver me a great Linux experience!"
    author: "Stefan"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "The service is very good but some of the packages don't work properly."
    author: "Bobby"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "I have the same problems that you mentioned."
    author: "Bobby"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "I think the same. Quality should be the highest priority and it would be very sad if kDE shouldn't live up to it'S name by releasing an unfinished \"product\".\nThe openSuse packages have some issuses that are related to SuSE and not necessarrily KDE."
    author: "Bobby"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "> The openSuse packages have some issuses that are related to SuSE and not necessarrily KDE.\n  \nLike?"
    author: "Anonymous"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "There are missing plasmoids, my background was missing for almost a week before it reappeared after a few updates and some things don't work as well as those that are compiled from source. Sometimes KDE4 on openSuse doesn't even start after an update so one has to wait for the next update etc.. "
    author: "Bobby"
  - subject: "Re: Good decision"
    date: 2007-12-03
    body: "There are rather few plasmoids contained in kdebase, for more plasmoids you have to install the extragear-plasma and playground-base packages."
    author: "Anonymous"
  - subject: "Re: Good decision"
    date: 2007-12-04
    body: "I installed the plasma-extragear but there were still missing plasmoids. However I didn't install the playgroud-base packages, now I followed your advise and those plasmoids that I missed are there again. Thanks :)"
    author: "Bobby"
  - subject: "Good Call"
    date: 2007-12-01
    body: "This was obviously a good call. It's also obvious that it's nonsense to name these Release Candidates! Everyone knows they are not ready for release, so they're Betas. In any case, not much to do about that right now, good luck guys and thanks for all your work."
    author: "KBX"
  - subject: "Re: Good Call"
    date: 2007-12-02
    body: "You know what a release candidate is good for as well? Testing if it is a release candidate!\n\nOnly now it's obvious that deadlines were missed and a schedule extension is necessary. Notice how the delay announcement is shortly after the release candidate has been found incomplete. \n\nBut you will never know, if you are not making deadlines, what people have sitting in their checkouts, and another Beta wouldn't be a hard enough deadline, or else things would already be there.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Good Call"
    date: 2007-12-02
    body: "> Only now it's obvious that deadlines were missed and a schedule extension is necessary. Notice how the delay announcement is shortly after the release candidate has been found incomplete.\n\nNo, I think it was plainly obvious beforehand.  Look at the list of current showstoppers.  Plasma:\n\n> goal: A basic panel is there; current work is to flesh it out into a more capable panel system. System tray and taskbar both need work as does a desktop containment with legacy support for the Desktop folder.\n\nThis is clearly not ready for release, you don't need millions of testers to realise that.  It is not the surprise that you are making it out to be.\n\n> But you will never know, if you are not making deadlines, what people have sitting in their checkouts, and another Beta wouldn't be a hard enough deadline, or else things would already be there.\n\nNobody is arguing against deadlines, only against unreasonable ones that don't reflect the state of the code.  And the idea that a release candidate is meant to prompt people to commit code is ludicrous.\n"
    author: "Jim"
  - subject: "Re: Good Call"
    date: 2007-12-03
    body: "> And the idea that a release candidate is meant to prompt people to commit code is ludicrous.\nWell, so is reality. Actually It is quite clear that the code would have been far less ready if the original schedule stated the January release date in the first place. Honestly, who cares about the integrity of the term 'release candidate' and its correct usage. Whatever - let's rather finish this thing. Then we can call it 4.0-stable."
    author: "eMPee584"
  - subject: "Thanks for resisting peer pressure and waiting"
    date: 2007-12-01
    body: "Thanks for waiting with the release. I prefer a stable KDE, over a rushed one. \"Gut Ding braucht (wirklich) Weile\" - Colloquial German for \"Good things take time\" \nIf I want rushed incomplete software I'll just stick with Micro$oft products. -That was a low blow :D \n\nSeriously though. Please resist peer pressure and make it stable. The Google release party isn't till January anyways. :p This way it's more likely that KDE 4.x will make it to Kubuntu 8.04.\n\nAlso please make sure that KDE plays nicely with \"eye candy\" apps like Compiz fusion, etc. This will be the future in years to come, so if it works nice now and is future proof, there will be less struggles later..\n\nPlease make sure that the \"desktop paradigm\" is innovated further. Don't be afraid to be a step ahead of the competition, in the end, that's what makes open source great.\n\n"
    author: "Max"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-01
    body: "Compiz-fusion is not the future...\n\n* Compiz and Beryl are the past.\n* Compiz-fusion is the present.\n* KWin >= 4.0 with builtin support for compositing and effect plugins is the future."
    author: "Kevin Kofler"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-02
    body: "Yes, but we're not at the future yet.  With the bugs and speed issues of Kwin at the moment, I think many will be using compiz-fusion at launch.\n\nI still think they should have worked to incorporate existing compiz-fusion core into Kwin, and allowed Kwin to just use compiz-fusion plugins."
    author: "T. J. Brumfield"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-02
    body: "If you watch the talks given the last two years at Akademy by Lubos Lunak, he explains why this was not done."
    author: "Paul Eggleton"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-02
    body: "Could someone please sum up these statements?\n\nLiving in the US, I can't make it out to Akademy."
    author: "T. J. Brumfield"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-02
    body: "Neither can I, but you can do what I do, which is to watch the videos recorded at the conferences:\n\nhttp://home.kde.org/~akademy06/videos/\nhttp://conference2007.kde.org/conference/programme.php\n\n(some of them have some encoding problems, you just have to put up with the frame freezing)\n\n"
    author: "Paul Eggleton"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-02
    body: "Basically Beryl/Compiz Fusion just exposes its internals for plugin writers (instead of having a good API), therefore re-using its plugins it's not possible.\n\n"
    author: "Luca Beltrame"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-03
    body: "Some reasons not to use Beryl/Compiz:\n* The \"API\" for plugins was using internals directly. So it's not usable outside Beryl.\n* Effects happen to be a small portion of the window manager code. (like 10% for the total code)\n* Window management is quite a black art. It was better to add effects then to throw all good code away.,"
    author: "Diederik van der Boor"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-03
    body: "I didn't suggest that you throw out Kwin.  I suggested you include the 10% of effects code from Compiz-fusion into Kwin, and try to stabilize a plugin API that both Compiz-fusion and KDE can use."
    author: "T. J. Brumfield"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-03
    body: "Frankly, re-implementing the effects would be far easier than a) persuading the Compiz Fusion guys to re-architect everything so that they have a neutral API and port all their plug-ins to it just so KDE can use them and b) actually doing the changes once you've somehow managed to persuade them  and c) porting KWin and all its effects to the new API."
    author: "Anon"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-03
    body: "I read here on the dot (I believe) some time ago that kwin implemented compiz like functionality in a trivial amount of code. Compiz is something like research and kwin is something like realistic implementation of said research. Compiz is huge and buggy whereas kwin has all the kinks worked out, is lean, and is fast. This is mainly because it has been around for over a decade (right?). Last I checked the devs did make sure it works with 4.0 anyway ;) Guess that goes along with the whole \"future proofing\" idea as well."
    author: "Nobbe"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-01
    body: "I don't think the kde devs have as a high priority making kde work with compiz. KDE 4 will have its own composite manager that we hope will be competitive with compiz.  Furthermore, compiz is very gnome centric and doesn't play very well with kde.  Finally, the responsibility is on the maintainers of compiz to make it work well with kde, not the other way around."
    author: "Level 1"
  - subject: "Ditto"
    date: 2007-12-02
    body: "I know people are eager to get their hands on \"KDE 4.0\", but if you don't care about stability then you can use betas (which will be exactly the same code as what the \"KDE 4.0\" was going to be, regardless of what it is named), and if you do care about stability, then you should welcome this delay.\n\nI think this delay is a big positive step for the KDE project and I'm very glad they did it.\n"
    author: "Jim"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-02
    body: "\" If I want rushed incomplete software I'll just stick with Micro$oft products. -That was a low blow :D \"\n\nor if you want to be a permanent beta tester using sub quality software stick with open source software.\n"
    author: "Nathan"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-03
    body: "And if you want to be a troll, keep hanging around and post this kind of useless comments."
    author: "Andr\u00e9"
  - subject: "Re: Thanks for resisting peer pressure and waiting"
    date: 2007-12-04
    body: "Andr\u00e9, the truth hurts doesn't it?"
    author: "Ouch"
  - subject: "A good decision"
    date: 2007-12-01
    body: "I think this is a good decision and I would encourage to delay it even further, if necessary. At the moment especially the desktop experience with hardly any configuration options, does not seem suitable for daily use. I hope that can be resolved and that also katapult will be available (the new menu really, really sucks, sorry for that)."
    author: "Mark"
  - subject: "Re: A good decision"
    date: 2007-12-01
    body: "I do not like the \"new\" menu either and prefer the \"90's\" one. For a simple reason, you cannot see what is under the submenus while moving the mouse. I do not like the search text box (it should be an option), because I use either the mouse or the keyboard and I use only the mouse when navigating under the menu."
    author: "Kde 4 user"
  - subject: "Usability nightmare!"
    date: 2007-12-01
    body: "Someone please tell me that this horrible piece of unusability I just installed is the result of Kubuntu's mucking about, and NOT what I have to look forward to with KDE4! If this is what KDE4 is going to be, I'll switch to plain vanilla blackbox.\n\n1) Konqueror is nearly unusable. Typing in the text field is painfully slow. Resizing fonts with Ctrl+ or Ctrl- results in garbled rendering and the inability to use this edit box. I am using Firefox just to post this.\n\n2) I added the analog clock widget to my desktop. Stupid me. It's stuck in the middle of the screen with no way to move it. No combination of keys/mousebuttons can get it to move anywhere but the center of the desktop. The rotate (circular arrow) button does nothing. Is it supposed to?\n\n3) I can find no way to change the wallpaper. It is missing from system settings. Neither is there a desktop menu of any kind. I tried dragging an image to the desktop, but all that did was result in an immovable widget (see above).\n\n4) Even more lack of configuration! There are so many configuration settings missing that I feel like I'm using a GNOME parody instead of KDE. If non-newbies are expected to hand edit configuration files, the least KDE could do is document them!\n\n5) And the worst, that stupid menu! I have to keep scrolling up and down, and clicking back and forward, to find anything. And since the \"Favorites\" is directly above the K menu button, it's nearly impossible to avoid moving the mouse over it. And true to form, there is no way to configure this menu. Do the developers really expect me to type in the search field every time I want to run an application? Once again, the default menu listing is item description followed by item name, leading so such bizarre results as \"Board Game\" (In larger boldface font) as the name of game. At least they could have put \"Board Game\" under \"Board Games\" instead of \"Strategy & Tactics\". Geez.\n\n7) There's lots of little stuff, but I'm going to write them off as merely known bugs waiting to be fixed, or additional Kubuntu shenanigans.\n\nOn the bright side, Dolphin4 isn't the piece of crap the Dophin3 preview was.\n\nI hate being so negative about a desktop I love, but this RC1 is a complete load of crap. Can we at least stop pretending that this will be suitable for use by non-developers?"
    author: "David Johnson"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "i also think, that RC1 doesn't look or behave like a Release Candidat, it still should be called \"beta\", in my opinion, but maybe the backends and interfaces are already very stable, so that developers can say, \"we are close to get something what can be named a release candidat\"."
    author: "opensuse10.3"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "well... they have a release party planned so you can bet it'll be called 4.0 even if it's still really a beta. i'm not going to touch it until 4.1"
    author: "bobo"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "sounds like what my professor said about Vista, that he won't touch it until SP2. I don't think this reflects positively on KDE."
    author: "yman"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Do you take what some anonymous poster on a website says as absolute truth? Or do you make your own mind up?"
    author: "Paul Eggleton"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "I'm not saying the KDE 4.0 would come out bad, or at least I didn't mean to. if you'd notice what I said in a different comment on this page (I don't really expect it, but then you might be a better person than myself), you'd notice that I do not think Vista came out nearly as bad as people make it sound like, although I wrapped it in a lot of talk about how I recognize that Vista is inferior to my distribution. what I meant to say, and I guess I phrased it poorly, is that it reflects badly on the public perception of KDE, making it sound as bad as the image of Vista. if things like that keep being said, people might think of KDE as being as buggy, incomplete, and poorly designed as they think Vista is. I'm talking not about the state of KDE, but about KDE's public image. I didn't try KDE since the second beta, so I don't know what it is really like at the moment."
    author: "yman"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "He is wrong, then. Vista is ready now, as long as you don't need software that doesn't work well with it (quite rare these days)"
    author: "Anonymous"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Yeah, and if you don't need your resources for actually running apps instead of just the OS.  And if you don't need your battery life.. But hey aside from that it's great!"
    author: "Leo S"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "This is really not the place for this kind of discussion, but anyway... my battery lasts as long as it did under XP. About system resources, well, usually those who moan more about those are precisely those who have more to spare. RAM is damn cheap these days, and besides, when mid-range PCs are coming out equipped with 2GB anyway, no one sane should really give a damn about that.\n\n"
    author: "Anonymous"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-06
    body: "And what if you get sent something like big.jpg, that consumes 1.75GB of your RAM? I'd upgraded my RAM to 2GB a matter of days before getting sent that. Good thing I use an OS that doesn't grab huge amounts of my RAM, eh?\n\nTyped on a laptop with 256MB RAM, using 138MB with stuff like Kontact open."
    author: "cloakable"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "Comparing KDE4 to Vista ?\n\nO_o\n\nIts not even finished yet !! You know the team are doing the best they can to make the best software they can. You know they do it with passion. You know its not going to be like Vista and yet you spout this kind of crap. Its a complete waste of space. Seriously, you find KDE4 isn't ready for general release yet, that it has some issues and suddenly \"oh gee its like Vista !!\". I deal with enough idiots at work so its disappointing to find them on dot.kde.org as well... Try saying something constructive or go elsewhere you dumbass nubs."
    author: "Jeremy"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-09
    body: "I wasn't talking about the real state KDE 4.0, I was talking about the public perception of it's quality, and made that quite clear 24 hours before you wrote this impolite reply. my original comment was not clear, but you should have read my clarification.\n\nI'm quite certain that when KDE 4.0 comes out it will be top notch quality within the features it offers, although from what I've heard there will be regressions in that regard as well as new features that were planned but whose implementation has been delayed to the 4.1 release.\n\nMy first major problem with Vista occurred on Friday. my computer became almost completely non-responsive for approximately 15 minutes. before that I was quite pleased with the implementation of Vista, although I think it's design is far from perfect. I experienced no performance problems what so ever, except for randomly and mysteriously not being able to use my computer for about 15 minutes."
    author: "yman"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "The only mistake that was made is to try and \"sell\" this as an \"RC1\" version. (Remember, release dudes: \"RC\" is means \"Release Candidate\". And a Release Candidate of a software is meant to say to its prospective users: \"We, the developers aren't aware of any major bugs any more [apart from the one or two we've documented in the release notes, and which we will fix before the '$Final']. Now you, dear users, tell us what we overlooked so far. If you don't, we'll release as-is [plus, the bug-fixes we promised in the release notes]. And, users! Be damned if you miss to tell us the remaining bugs in time...\". Why, oh why does this primitive release naming rule need to be hammered home to the KDE release dudes??)\n\nOther than that, David: this isn't 'crap'. It's just a technology preview. Something that soon may be called an alpha or even a beta. And after some more time, even a \"Release Candidate\". It's not crap, it's the best what can be expected. And the result of hard work of many developers. And something that is the basis to grow a magnificent KDE4 platform from, no doubt about that!"
    author: "ac"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "> The only mistake that was made is to try and \"sell\" this as an \"RC1\" version.\n\nWhat do you think how many time is left for \"real\" release candidates!? ;-) Even the postponed release date in january doesn't match my idea of a decent (pre-)release cycle, given the current state. I think the dot zero release will actually be the release candidate."
    author: "Carlo"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "Yes, it is the \"RC1\" part that causes me the frustration. I would have been much more lenient if it has been a tech preview or even a beta. But this is supposed to be a *release* candidate, which means the core developers think it's ready.\n\nI went and rebuilt last night on ArchLinux (so as to avoid any kubuntu weirdness), and while some of the minor issues had been fixed, it larger usability issues are still there. In addition, core components were crashing every couple of minutes."
    author: "David Johnson"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-05
    body: "> And something that is the basis to grow a magnificent KDE4 platform from, no doubt about that!\nWell, up to know, from what I've seen I'd like to complain about things here and there and remain sceptic, especially about the default interface. But saving my energy for actually contributing to 4.1, I have to totally agree with this statement. KDE4 is about to be the best desktop ever done, whatever its current state. Let's go and do this."
    author: "eMPee584"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "1) - 3) are already solved, 5) is work-in-progress, the other two are useless rants (useless as in too vague to do something about it, or just not above 'eh sucks' level of quality). Also, calling it crap and the apologizing for being negative, and then calling it  crap again doesn't score you any credibility points.\n\nSo you switch to blackbox, and we get _productive_ people to use bugs.kde.org instead of ranting on the Dot. Makes both of us happy. And as a 'side-effect', those that *really* care will be more considerate towards those that put lots of work and energy into KDE.\n\nReally, if those people that are so negative would:\n\n- think twice about how they say things (Hint: Imagine someone comes with the exact same wording ranting about your work, what would you think?)\n- Channel their stop energy into assisting developers to fix problems\n- Get their hands dirty and just start fixing things\n- or at least try to explain the problem in detail, along with (a proper bugreport) so there's a straightforward way to fix it\n\nSo yeah, you can all complain because you fear that your pet peeve is not addressed, that something doesn't match your taste or that things are simply a bit different, but at the end of the day, it counts how you communicate that. \n\nRanting drives people away, it kills motivation to fix bugs. Constructive feedback, using the right channels (BKO for example!) and being actually interested in fixing stuff and not just venting some rants is what makes the release stable and users happy.\n\nSo stop pissing off people, start doing something for \"a desktop you love\".\n"
    author: "sebas"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "I personally think what drives people away is the attitude that all comments must be positives, slap on a fake smile, and pretend comments, and that anything that isn't positive is point down.  There was nothing wrong with the original posters comment except people like you go and have a little tantrum if the post isn't kissing your butt.\n\nYou can have real posts or you can fake the world and only have positive posts.\n\nAs for reporting bugs, I would place money that the vast majority of KDE users now, who are not geeks or nerds, but have come over from windows, know nothing of the bugs reporting method as all they are used to is windows.  It's prolly something that dot could have an article on and explain to people. Instead of people like you getting all anal when some one makes a post like above.  "
    author: "Richard"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Positive and constructive are two very different things."
    author: "bsander"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Actually any criticism on the dot has been uniformly blasted for months that I've seen."
    author: "T. J. Brumfield"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "See, I don't mind people pointing out problems, I *do* mind how we communicate with each other. That's why I'm pointing out the problems of the tone of the original post. And it's not just me, it's about being nice in general. An important point of our community is 'being nice to each other', 'being productive and getting things done' is another attitude you encounter very often within the KDE community.\n\nOn the other hand, I think calling our work 'crap' repeatedly is something that just does not cut it when it comes to 'being nice'. So you enter the KDE community via the Dot, then you see people insulting other people's work, this does not reflect what KDE is. It's fine if you as a poster don't care about the wording, but it's important to respect other's feelings and how you represent (as a poster on the Dot) the KDE community."
    author: "sebas"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "You started this by communicating a release that wasn't an RC as an RC.  Do you know what RC is?  Send the right message in the first place and maybe you'll get better response...  basic marketing, ya?"
    author: "KDE User"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Except that if it hadn't been labeled RC, the amount of testers would have been much less. Note, I don't know if that was the rationale for choosing it, but I think it's the most logical (and understandable) ne.\n\nP.s.: Care to vaoid such messages? I don't think anything productive will be gained by this attitude."
    author: "Luca Beltrame"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "> Except that if it hadn't been labeled RC, the amount of testers would have been much less.\n\nLying about the stability of the code to trick people into testing it for you is hardly good communication.\n\nThere are lots of people willing to test something that is almost finished to sweep up the remaining minor bugs.  Presenting them with something that is half-finished only alienates them and makes it far less likely that they'll be willing to test release candidates in future.\n"
    author: "Jim"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "The release has been delayed now, ie, you got what you wanted. Can't you just leave the developers to their work? What more do you want?"
    author: "Paul Eggleton"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Huh?  Luca made a silly excuse and I disagreed with it.  Am I not allowed to talk?  How am I stopping the developers from working?\n"
    author: "Jim"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "People are beating the same dead horse over and over again here about stuff which is now past history. If you keep doing this you are just sapping people's motivation to work on the project.\n\nI am singling you out, by the way, because you continue to try to cast a bad light on this RC naming issue. There was no conspiracy to deceive and nobody lied. Not to mention that naming it an RC did achieve an actual result - it became clear that it wasn't ready for release and therefore the release date was moved.\n\nDo we really need to discuss this any further?"
    author: "Paul Eggleton"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "> People are beating the same dead horse over and over again here about stuff which is now past history.\n\nImagine that; a story about the release being postponed, and people are discussing why it happened.  How outrageous!\n\n> I am singling you out, by the way, because you continue to try to cast a bad light on this RC naming issue.\n\nSure I do.  I think the release schedule for KDE 4.0 has been abysmal, it has burned a lot of bridges, disappointed a lot of people and resulted in nothing but pain.  The KDE project has used up a lot of good will and it will be difficult to regain that trust.\n\nAnd if I see people making excuses for it, damn straight I'll reply to disagree.  If you don't like that, don't make excuses for it.  You can't express your opinion and deny me the right to mine.\n\nHopefully lessons will have been learnt from this and future releases will not become the debacle this one has.\n\n> There was no conspiracy to deceive and nobody lied.\n\nLuca said that it was the most logical and understandable rationale for what happened.  I replied saying why I thought that was bad.  If it's your opinion that this didn't happen, then simply disregard my comment, I was talking to somebody who does think that trying to maximise testers in this way is a good approach, so my point stands regardless of whether or not it actually happened.\n\n> Not to mention that naming it an RC did achieve an actual result - it became clear that it wasn't ready for release and therefore the release date was moved.\n\nIf you think this wasn't clear already, you are kidding yourself.  At no point have the developers considered Plasma-based components feature complete, it's *still* listed as \"work to do\".  The developers already knew that RC1 wasn't a release candidate.\n\n> Do we really need to discuss this any further?\n\nApparently so, because you are still playing apologist.  If you don't want to discuss it any further, stop making poor excuses that don't hold up to the slightest scrutiny.  But don't expect to say what you like without any kind of rebuttal.\n"
    author: "Jim"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "OK, Jim, let me put this to you another way: either you are trying to help, or you are just trolling. If you are trying to be helpful, then let me tell you directly: you are not helping. Right now you are not saying anything that nobody else hasn't already said _including yourself on multiple occasions_. So please, just stop."
    author: "Paul Eggleton"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "> ither you are trying to help, or you are just trolling.\n\nOr I'm thinking about what people say and responding with my opinion.\n\n> Right now you are not saying anything that nobody else hasn't already said _including yourself on multiple occasions_.\n\nSo?  I am limited to saying something once now?  Just because I've said it before, it doesn't mean that Luca or anybody else reading this thread has read it.  What you and Luca have said is also unoriginal, but that didn't stop you or Luca, did it?  If Luca repeats something people have already said that I disagreed with, I'll repeat what I said when I originally disagreed with it.\n\nStop trying to shut me up by calling me names and guilt-tripping me.  It won't work.  What you seem to want is a place where people can defend this atrocious situation ad nauseum, but anybody who disagrees should just be quiet.  That's not going to happen.\n"
    author: "Jim"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "++ from me.\n\nWe're seeing some weird behaviour on the Dot of late: first of all, there was the Rise of the Whinging Critic, where there was a sudden surge of people throwing tantrums and making threats to stop using KDE(!) unless they got their way: rightly, people discouraged this kind of behaviour, as it is wholly unconstructive and just pointless.\n\nBut now pendulum has swung too far, and some people seem to find that any dissent at all - no matter how civilly expressed, or well-argued - also needs to be squashed and the devs \"shielded\" from it, and so we get the bizarre apologism of Paul et al.\n\nHopefully once 4.0 has been released (and probably mildly publicly savaged by reviewers, both professional and armchair) and the devs start hacking on the rough edges and integrating everything for a much, much improved 4.1 we can look at 4.0 as what it really is - the difficult *beginning* of an awesome KDE4 cycle, of historical interest only, and get some semblance of sanity here on the Dot."
    author: "Anon"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "> first of all, there was the Rise of the Whinging Critic\n\nYou know what, I'm usually one of the first to argue against whinging.  I do that with other projects.  I used to do it with KDE.  I'm certainly no fan of it.  I think the \"Rise of the Whinging Critic\" is in large part a direct response to how more reasonable criticism has been handled throughout the latter part of KDE 4.0's development.\n\nPeople like to say that there was whinging for previous major releases, but it was absolutely nothing compared with this.  Sure, there will always be whingers regardless of what you do, but the sharp increase in whinging didn't come out of nowhere, and some of the KDE developers and fans seem to be writing off this criticism without looking at the causes behind it, and if you try to talk about the causes you get flamed.  Sometimes it feels like people need to praise the  developers if they want to talk about problems without being called a troll.\n\nI had hoped that lessons would be learnt for the next release, and the delay is an encouraging point in that respect, but the attitudes of some people make me worry that exactly the same thing is going to happen in future.\n"
    author: "Jim"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "And you started saying that KDE is not in RC state, when *most* of it, really is. Sure, Plasma is not as mature as the rest of KDE, and is the most visible part (because is the first thing you see), but the libraries, and many many many applications are in a really good shape."
    author: "suy"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "You are correct. The libraries seem to be in great shape. But I titled my post \"Usability nightmare\". The \"visible part\" is indeed relevant."
    author: "David Johnson"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Get real.  There is nothing wrong with negative comments if they are constructive, and I saw nothing in the response that I would call a rant.  On the other hand, the original poster was definitely ranting.  Calling someone's work \"crap\" isn't constructive, and it isn't merely negative.  It is downright disrespectful.\n\nBut since you don't see a problem with the original poster calling the hard work of the developers crap, it shouldn't bother you much when I say your post was crap."
    author: "cirehawk"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Dude, come on.  David is a long time poster here.  I totally trust his judgment and input.  Instead of getting angry about it, you should read what he is writing and his frustration.\n\nYou're a marketing a guy, so I'll put this in marketing terms -- You opted to call this an RC.  This is the result.  Call something that should be alpha an RC just for publicity and you'll put yourself under scrutiny.\n\nDavid did the right thing.  I'm glad he voiced his frustrations because I don't want KDE4 to be a piece of garbage like GNOME2 was."
    author: "KDE User"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "I'm sorry, but in no way he should be allowed to call it \"crap\" and hoping to get away with it, long time poster or not. \n\nPeople, do you understand you're bashing the work of volounteers? There are other ways to express criticism without affecting morale."
    author: "Luca Beltrame"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "I'm sorry, but in no way should we call it an RC and hoping to get away with it, long time project or no.\n"
    author: "KDE User"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Calling something \"RC\" instead of \"Beta\" has no negative meaning. \"Crap\" on the other hand, does have a disrespectful meaning. "
    author: "Luca Beltrame"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "\"Calling something \"RC\" instead of \"Beta\" has no negative meaning.\"\"\n\nWhat? Really?? What about misleading the user base and raising confusion among the developers on the intent of the release? Was it meant as a release candidate, or as a beta build?\n\nBut sure, I guess this is one of those things that have negative implications if Microsoft does it to push a product. Then it's always wrong if they call a beta a RC. Always."
    author: "Jugalator"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "the fact you consider it Alpha and we consider it RC doesn't make anyone wrong. You just seem to have a different standard here. We've tried to communicate that everything except a few parts of KDE is ready for testing. Has been for a while, but the users didn't seem to test it so we didn't get much useful feedback (besides from ppl calling it crap, but that's pretty useless feedback). Now we decided 95% of KDE is in RC mode, so we released an RC. And some feedback came, though still mostly 'it sucks'. At least we got SOME bugreports, besides all the time-wasting ranting..."
    author: "jospoortvliet"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "I've been using KDE as my primary desktop since 0.99. I did not use the word \"crap\" lightly. In retrospect I agree that it was too provocative a word. But that does not diminish the fact that this \"RC1\" looks and feels like a pre-alpha snapshot, with many poor or questionable usability decisions."
    author: "David Johnson"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "It all depends on what part you are testing. As has already been said, 95% of KDE4 is quite RC quality. I am posting this from Konqeror 4 and have a quite good experience.\n"
    author: "Michael"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "He may have done a pretty bad job of communicating, but I agree with his overall point.  KDE4 is not of \"Release Candidate\" quality, and I think the KDE community is doing itself a disservice (and disappointing a lot of users) by calling it so.\n\n\"Release Candidate\" is supposed to imply feature-complete and mostly bug-free.  Even aseigo notes that Plasma is not feature-complete, and Plasma is a pretty central part of KDE4.\n\nI've been trying to use KDE4 (built from SVN) as my desktop on and off for the past few days.  Each time I've given up and gone back to KDE3 after about 15 minutes.  Contrast this with the KDE3 RCs, which I was using stably and productively for days at a time.\n\nFor example, Plasma crashes constantly (there are a few specific cases -- I'm still trying to pin them down and convince gdb to give me a usable backtrace so I can file a proper report).  Konqueror still doesn't work on a number of websites (in particular, it can't view any LiveJournals -- again, going to try to narrow it down).  The IMAP ioslave \"quits unexpectedly\" when I try to get my mail with KMail.\n\nWhen I have time, I'll be tracking these down and filing bug reports (or generally helping to get them fixed).  I'll do my best to provide productive feedback.\n\nBut the simple fact remains that KDE4 RC1 is just not of Release Candidate quality.  And I think a lot of us (or at least, I do) feel misled by the developer community for calling KDE4 something it's not.\n\nSo I would urge you and the rest of the developers not to take criticisms like these personally -- ranting right back at him isn't at all useful."
    author: "Des"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "For people like me that read the Dot and the Planet, the fact that this RC1 is in fact a \"Beta 5\" was not a surprise but instead a well established fact.  I therefore didn't approach RC1 as release candidate at all, and I fully expected things to break here and there.  I was therefore not disappointed.\n\nHowever, this does not detract from the fact that calling this an RC was SERIOUS BLUNDER.  In fact, I expect that most of the \"bad vibes\" that've been circling around the dot and the Planet are in large part because of this.\n\nKDE is an open-source project, for goodness sake!  It should be ready when it's ready (within reasonable limits, of course!).  Though I understand that the release team would like for things to be on schedule, sometimes that's just not possible.  I am confident that people would've been understanding if the release team would have chosen to issue still a couple of more betas and to delay the RC until it was in fact a RELEASE CANDIDATE!  By calling the current version a RC, you have made a mockery of well established practices and lost a lot of good will from the community.\n"
    author: "Cultural Sublimation"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "Point is that most of KDE (eg 95%) is in RC mode, just some parts aren't. And we need feedback. So we had to call it RC to get ppl to seriously try and test it... The mockery is only that 5%, and frankly, I don't care about that. If we would have decided to call it another beta, there wouldn't be as much feedback, and we would just be wasting time for all those apps which are ready and need some user input!!!"
    author: "jospoortvliet"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "> Point is that most of KDE (eg 95%) is in RC mode, just some parts aren't.\n\nThat in my eyes does not merrit it calling a RC, unless those 5% are well hidden and not likely to be encountered. No, I take that back. If those 5% are well known, then unless you plan to release those the way they are, it should not be called RC.\n\n> And we need feedback. So we had to call it RC to get ppl to seriously try and test it...\n\nAargh. I seriously ponder now if I should try and test it again at all. After all, by testing I am also volunteering time. And seriously, I do not like it if people try to trick me into it by labeling things differently than they are. I might just decide to not trust them anymore and spent my time on something different. And by reading the dot, I do get the impression I am not the only one not liking this form of marketing. You might - for a single time - trick some more people into testing it. At the same time you might alienate a large part of those gained (there might be a reason why they did not test it before) and loose them for the future. Being honest usually is the better thing in the long run.\n\nAnd something else about the missing feedback:\nI have been compiling the source once a week for maybe half a year by now. I always did so with the hones wish to test it and report things I find. Each and every time until very recently, I was just driven away by the state of plasma and oxygen at once. If I stayed a little longer, I usually found that\nthe application I am usually using were in a pretty bad state as well (from konqueror, konsole, kmail, kopete, akregator and knode only konsole worked nice from the beginning, the others all had issues a week ago that would prevent me from calling them a release candidate). Since there were a lot of really visible problems I usually wondered what I should actually try to report, as chances were pretty high that I would report a dupe. Thus I was driven away from further testing.\n\nAnd I guess I am not the only one driven away by the state of the desktop itself. It is unfortunate, but I guess if kwin and plasma (I do not know if the overall slowness is a kwin or a plasma problem) would have been in a better state earlier, you would most likely have had a lot more people actually testing kde4.\n\nIt is a long time since then and my expectations might be different now but trying and using kde 2.0 betas did not seem to be nearly as bad. In the past I always switched to the new kde version somewhere in the betas and never went back. Now, for the first time ever I have not even done so with the first RC, the desktop just feels to much like an obstacle. I did not (and do not) mind beta applications crashing or a glitch here and there, but windows opening below others (even some which I needed to access next to continue working with that application), a slow desktop (I get 5-6 fps with glxgears, as opposed to 9800 at my kde3.5 install), invisible boxes (within konqueror), nonconfigurable taskbar (and no selfexplanatory way to have the finally working pager in there) just does not do it for me.\n\nSeriously though, calling something that is not quite ready (and known to be so) a RC just in order to get more users testing - that seems quite disrespectful to me, if not lying. And it is quite discouraging. Did you even consider the idea that the lack of testing might have some other reasons than the name of the release?"
    author: "smorg"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-04
    body: "I agree that using the word \"crap\" was rather harsh. However I am a long time <1998> kde user and I simply love the project, I just don't have enough skills to contribute any meaningful code but I have done my share of converting many a user and company to linux/kde. Recently however I don't know what has happened to kde I call it gnomification coz its beginning to feel a hell of a lot like gnome - restrictive and unacrobatic, taking away from the original beauty of kde. For exapmle I have tried hard but unsuccessfully for a long time to convince myself of the wisdom of creating dolphin as a default filemanager  and  okular as documnent viewer instead of adding enhancements to konqueror - new kde users just love konqueror's abilities, and love the single app that can make their lives so much simpler. kde3 has a beautiful philosophy, why reverse this unique philosophy in kde4?"
    author: "kaydi"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "> Hint: Imagine someone comes with the exact same wording ranting\n> about your work, what would you think?\n\nthat I must have done something really wrong if he is so pissed off ...\n\nbut no, this idea does not ever come to your head, you just say that he is impolite and uncostructive so it is not worth to take his opinion into account, while you are the perfect one, sigh"
    author: "kavol"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "This is why it is delayed. (Btw some of the problems are already fixed, try a more recent build.)"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "For the love of God can some take that menu setup out back and shoot it.\n\nKDE does rock, and thanks"
    author: "Yo"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Who really cares about the menu anymore? The run dialog has been beefed up. I doubt I'll find myself clicking that blue K more than once per month.\n\nIt's so much faster to hit a few keys instead of hunting through menus."
    author: "simon"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "But they did a usability study!\n\nI think the menu is terrible, and so do most people who use it, but everyone throws that study out there.\n\nThe study was a joke.  I'm just glad we'll have menu alternatives in time."
    author: "T. J. Brumfield"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Is the problem that most people think that it is terrible, or is it the minority of people who think it is terrible are really vocal? How many times do people go around posting what they *do* like vs posting what they don't like."
    author: "Henry S."
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "criticizing is more fun!\n\nbut seriously, when people point out the bad, I think the hidden assumption is that everything else is OK, which is why I would count any post of a person who tried out the release candidate and complained about something but didn't  complain about the menu as someone who at least doesn't find it a notable or bothersome problem and can live with it when using KDE. I think maybe there should be a poll of what is the all-time favorite KDE menu that people want to see in their KDE. maybe include main menus frome other desktops as well in the poll."
    author: "yman"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "The majority of people think it is bad. They just do not post about it. Fact is in the middle of trying to post about how bad it is most people just sigh and give up. It is like they have lost the will to live. Please bring us back the 90s menu we all so love. Please."
    author: "ne..."
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "... just after they got their 90s menu back, they started to grow fur and climbed back on their trees.\nHappy to stay there, waiting for the things to come and complain about"
    author: "Christian Nitschkowski"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "I, personaly, will just wait and see. I'll try the new menu for a month or so and if i cannot get used to it i'll install the classic-menu-port wich most likely is created by someone by then. I might even attempt to port it myself :D\n\nSeems like all the critique is either \"the new menu sucks\" (should be fixed within a month or so after the release, if it turns out to be a problem), \"the RC was a really beta\" (marked UNFIXABLE) and random bugs already fixed."
    author: "R. Bloemen"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Logically, how can something that hinders productivity be a boon for usability?\n\nIt is slow, cumberbsome, and difficult to navigate.  Everyone says, \"but it has search!\"\n\nSo add search to Kmenu like Suse did, or develop a new menu with search.  But Kickoff, while it looks good in a screenshot, is an absolute pain to use."
    author: "T. J. Brumfield"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "I agree 100%.  In fact I hereby ask the Kickoff supporter's to STOP saying that its design has been validated by user testing.  It hasn't.  The tests that were done have ZERO statistical significance, and the methodology used has serious flaws too.  The problem with developers repeating the \"it has been tested for usability\" mantra is that they come across as a) not really knowing much about usability testing, b) not knowing much about statistics, and c) not having a mind of their own.\n\nIt would actually be very helpful to this discussion if a new (independent!) usability test could be done on Kickoff.  And remember kids: if you want people to take you seriously instead of just coming across as a cargo-cult usability tester, make sure you read on your statistics!\n\nHaving said that, I actually think there is some goodness in Kickoff.  It's only that insane navigation menu that kills the whole thing.  It honestly feels like I am playing Doom inside the menu, navigating through mazes like a mad man looking for the exit!  (and obviously often getting lost in the process).  Have you at least considered adding a couple of demons to shoot?  That would make the menu navigation in Kickoff slightly less boring.\n"
    author: "Cultural Sublimation"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: ">>Have you at least considered adding a couple of demons to shoot?\n\nHA!  I laughed out loud on that one.  I made the very same \"maze\" point a while back.  It seems, though, that this menu business has become a sensitive issue.  Any time somebody complains about it, the defenders use one of the following two arguments:\n\n1.  But there was a usability study, so it must be better.\n2.  Why don't you just use Alt+F2 or Search to type the command, you barbarian?!\n\nI'll let others argue about the questionable usability study.  I want to point out that I really don't want to type commands to start programs.  Sometime back in the 80's, I discovered this new technology called the \"Graphical User Interface.\"  It brought with it an amazing innovation called a \"mouse.\"  Shortly after that, I got lazy and no longer wanted to type commands to start programs like in the old TRS-80 and Commodore 64 days.  Two decades of mousing have caused my typing skills to atrophy.  I'll take my menu, thank you very much :-)\n\nSince I love KDE so much, and look forward to KDE4, I've tried to hold my criticism  in check until things solidify more.  I'm patient enough to wait for an alternative menu.  I do have some fear that Kickoff being the default will turn some people off, and that's based only on my totally-non-scientific personal experience."
    author: "Louis"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "I'm sure if anyone writes a normal, 90's KDE menu quickly, and it's stable, it will get included. But currently, nobody did anything except complain, so there simply IS NO KDE 3.x MENU IN A SHIPPABLE STATE. Again, if there was, it would be included. So we're not trying to force anything on anyone, we just don't have anything else."
    author: "jospoortvliet"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "Thanks for your comment Jos.  It is actually quite a relief to hear that.  I just wish this rationale was made clearer: 4.0 is using Kickoff not because it is deemed to be the best, but because it's what is ready right NOW.  There is no shame in admitting that.  I think Kickoff detractor's would be a lot less whining if this situation were explained properly, without any attempts to camouflage the real issue.\n"
    author: "Cultural Sublimation"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-03
    body: "Jos, I know the whole story; I've been reading Planet and Dot for a long time now.  I hate to even complain because I'm not gonna write a menu.  However, the menu, for most people, is one of the most primary parts of the user interface.  It's something that they use all the time.  My worry is KDE 4.0 will be released with the current menu, and the reviews will start rolling out all over the web, and reviewers will be complaining about how difficult it is just to navigate to programs.  I, myself, can deal with it, since I've been using KDE for a long time, and know what I'm doing.  But first impressions matter, and since the menu is probably the first thing people will hit right after bootup, I wonder what kind of impression it will leave when people have to click in and out, up and down, to see what programs are there.  It seems that the menu deserved more attention for that very reason.  I hope the extra month gives somebody time to provide a menu that's easier to navigate / more discoverable.  Thanks to all."
    author: "Louis"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "You know, about 30 seconds before I read your comment I was thinking the same thing. The unfortunate thing is that the focus is now on Windows users and trying to adapt to them. In my view, this is just so wrong. The focus should be on KDE users. Others should adapt to KDE not the other way round."
    author: "ne..."
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "That's nonsense, KDE 3.x focussed on Windows users and that was the right thing.\nKDE 4.x is currently an unusable joke.\n\ntake for example the start menu.\nthe 3.x menu wasn't nearly as comfortable as the XP one, but it was usable, the 4.x thing is poor.\n\nLook at how XP or Vista provide a professionell start menu and try to imitate that.\nThat would be the right thing.\n\nThat means..\n- easy configuration with drag 'n drop\n- two mouse clicks to reach all relevant items (in the kde 4.x version... forget it)"
    author: "abc"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "This is basically the same menu that's in OpenSuse, right? Well, i can say after using it for awhile, it is addicting. I really like it. It's a huge improvement. Probably it would be nice to have an 'old-menu' option for people who can't get used to it. KDE is about choice, right?"
    author: "walter"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Yes, one need some time in getting used to a new thing. I remember the XP menu initially gave me trouble and I always switched back to the classic menu. However, after a few tries, I started liking the XP style menu and now I am using it since last couple of years.\n\nHowever, i tried the same thing with the Kickoff (in KDE 3.5.x even). However, I still do not feel comfortable. Too much of clicks and unexpected hover as I mentioned in another post.  "
    author: "oc"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "I thought rc1 had been released in october... I don't think it's a good idea to base all your comments upon a deprecated release...\n"
    author: "fken"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-02
    body: "Personally, I didn't like the new menu. Too much of clicks (reminds me of Windows 3.x Program Manager) and unexpected hover effects (tabs getting activated upon hover). Also, the hovers does not work always. It may be a problem in my setup however as I do not see anybody having the same problem. Why stuff everything in a small scrollable window when there is enough desktop space? A click to get to a subcategory and another if u wanted to come back (a few pixel wide place with an arrow). A typical menu would have been better after 1st level. Also, five tabs to start with!!!\n\n  \n\n\n\n"
    author: "oc"
  - subject: "Re: Usability nightmare!"
    date: 2007-12-04
    body: "I'm using Dolphin3 and while there are some issues like when renaming a file (konqueror did not select the extension, only the part after the dot in the name) and some blinking when a change a directory I find it's nice.\nSo what can you tell us about Dolphin4 that is better than Dolphin3?"
    author: "Iuri Fiedoruk"
  - subject: "Good decision"
    date: 2007-12-01
    body: "for me, there is only linux and kde. when i upgrade to kde4, i don't want to be disappointed at all. i definitly can wait a bit longer and get a strong and powerful kde4 :)"
    author: "opensuse10.3"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "so wait for 4.2 ??"
    author: "Emil Sedgh"
  - subject: "Re: Good decision"
    date: 2007-12-02
    body: "no, i believe and hope, that 4.0 will be finished and very useful in January. otherwise i stay with stable 3.5.8+ "
    author: "opensuse10.3"
  - subject: "Problems updating KDE4DAILY"
    date: 2007-12-02
    body: "Sorry for posting this here but I thought I'd get a quicker response.\n\nI'm trying to update my system with the built-in updater. Since the main site isn't working I tried switching mirrors like SSJ said to do in http://dot.kde.org/1195829316/1196069183/ with \n\nexport KDE4DAILY_SUBSTITUTE_MIRROR=http://home.kde.org/~kde4daily/\n\nbut as its going along I get the following error(PIC attached)."
    author: "josh"
  - subject: "Re: Problems updating KDE4DAILY"
    date: 2007-12-02
    body: "Hi Josh,\n\nPlease see the original thread in the KDE4Daily topic:\n\nhttp://dot.kde.org/1195829316/1196069183/1196105080/1196108595/\n\nThe mirror is only used for emergencies, and won't work at all other times.  Please use the normal settings instead :)"
    author: "SSJ"
  - subject: "Re: Problems updating KDE4DAILY"
    date: 2007-12-02
    body: "Thanks, I switched back to the original URL and its working fine now."
    author: "josh"
  - subject: "Sonnet?"
    date: 2007-12-02
    body: "I think it was wise to move the release to January to ensure the loose ends are tied up properly, even if it means no shiny finished 4.0 for Christmas.\n\nHow is Sonnet going since poor Jacob Rideout disappeared off the face of the KDE planet? Are others working on it so its original goals will be achieved by 4.0/4.1?"
    author: "Concerned gearhead"
  - subject: "Summary"
    date: 2007-12-02
    body: "I think I can sum up every post in this thread with one word.\n\nWhew.\n\nThe current RCs are not Ready. We would rather have it late and right than on time and soil the good name of KDE.\n\nI use KDE 3.5x daily, and have since 3.1. I like the direction we are going, but I urge the main developers to listen to the critics, harsh as some might be, they are KDE users, they are coming at it from a direction far more kind than a new user would. If they are this harsh, think how badly KDE will be blasted from non KDE users/Lovers. \n\nI welcome the idea of developer releases, and I think this should be pointed out in BOLD 26Point Letters on the next few releases. Many are of the mistaken idea that they can get a usable productive KDE4 desktop from a Release Candidate, and are severely disappointed to find what they get. \n\nOverall, Great progress, Keep polishing, and can we rethink the menu and clock please.\n"
    author: "MrCopilot"
  - subject: "Re: Summary"
    date: 2007-12-02
    body: "Why do people keep talking about RC's???  That was so last month.  Maybe for 7 days after an RC is released, you are allowed to critic it.  But KDE4 is moving at a lightning pace.  Developers need us to weed out the *hidden* bugs...not the big glaring, in your face GUI issues from last month.  The big glaring things are usually there because developers spend most of their time dealing with the thousands of behind-the-scenes stuff.\n\nWe should be more worried about hard-to-reproduce issues, security issues, translations and documentation...instead people only care that the system tray was floating about.  Which of these items do you think takes the most work?\n"
    author: "Henry S."
  - subject: "I second that."
    date: 2007-12-03
    body: "Everything is in the title..."
    author: "Richard Van Den Boom"
  - subject: "Nice."
    date: 2007-12-02
    body: "Nice to hear we're giving more time for KDE contributors to finish implementations, polish and iron our problems. :D"
    author: "Schalken"
  - subject: "Glad to hear it"
    date: 2007-12-02
    body: "I'm also very glad that the release is postponed. KDE is my favorite desktop for Linux (making it my favorite desktop for any computer system), and I don't want a new major version to be released with less functionality than the previous one. I really like the direction that 4 is headed, and I like the way that the new modules (Phonon, Solid, Plasma, etc) have been designed. However, the current state of the \"RC\" version feels more like an alpha. I know that it's a lot of work to reimplement all the functionality of kicker and the old desktop, and for that reason it should not be rushed. I wouldn't mind if the release were postponed even until the summer if all the features of KDE 3.5 were all implemented on top of the new framework. I know that everyone is looking at KDE 4.1 as being the release where all the cool new KDE 4 features are brought into the desktop, but I still think that a reliable, stable, and functional desktop is essential to the 4.0 release. In conclusion: Thanks for all your hard work, and great progress so far, but please please don't release until everything works like it should."
    author: "Andrew"
  - subject: "Re: Glad to hear it"
    date: 2007-12-02
    body: "It's not going to be reliable and stable until lots of people are using it.  And lots of people aren't going to use it until it is called 4.0.  That doesn't mean that 100% of the people have to use it.  If 15% of the people use it, that'll still be lots of people.\n\nIf it makes you feel better, when 4.0 comes out, pretend that it is a release candidate.  When 4.1 comes out, tell yourself that it is 4.0.  I say this because it seems what bothers people is the name, because the development pace will pretty continue at the same pace no matter what.  If you want to use it in January or next summer...it doesn't really matter what it is called."
    author: "Henry S."
  - subject: "Re: Glad to hear it"
    date: 2007-12-02
    body: "That is true, but with the current state of rc1 you don't need a lot of people testing to tell you it isn't ready. Give any user 5 minutes on RC1 and he will give you a list of reasons that it feels like a beta or even alpha release.\n\nI understand 4.0 will not be bug free, how can it be without a larger testing base. But it certainly should not feel like beta software to a user. Think of it as Debian's unstable branch. Yes, I expect things to break every now and then but it is perfecly fine for daily use.\n\nYou only get one chance to make a first impression. Once 4.0 is released it will be reviewed by countless sites. It would be a shame if people feel that it was released prematurely. \n\nThe general concencus here seems to be ready rather than early. And I agree.\n\nAnd thanks to all the KDE devs for their hard work."
    author: "Abdulla Bubshait"
  - subject: "Re: Glad to hear it"
    date: 2007-12-02
    body: "I must aggree. The latest releases shouldn't be called a \"release candidate\", because a lot of people feels that this is still beta stage. So please, do not call it a rc. I think every KDE addict is keen on having a new version, but the current state is IMAO not ready to be used in a daily productive work envroiment. Lot's of feature does not exist or feels \"strange\". For example, it looks like last week the first context menu operations on the \"kicker\" pabel was applied. The first time, I was able to switch application to another pager screen. But we had a RC already! Still I am missing lot's of configuration option for the panel and ... yes, it is told that they will be added soon.\n\nThis does not mean that the developers had done a bad job. Many applications does already looking very promising and solid. The backend also got lot's of love. It is quite easy to write own plasma applets and KDE 4 will be a great thing. So please, don't silenece every voice telling that KDE 4.0 is not ready yet. This call is IMAO very good, because it allows to add missing features + adding some more days to polish. KDE people waited a very long time for 4.0 and most of us will be waiting even more... but please made the new release shiny and not looking like a step back (even, if it is promised that 4.1 will add missing stuff). The real weak point is plasma and oxygen and the last weeks plenty of work had been done to deal with this issues.\n\nSo no tears on the new schedule, don't take it personal, if people are not satiesfied with the current state. IMAO naming it RC1 and trying to implement some important features within the last days, made more troubles than are actually in the back end."
    author: "Phobeus"
  - subject: "Re: Glad to hear it"
    date: 2007-12-03
    body: "You think it's not RC just because Plasma some other parts of KDE aren't RC. 95% actually IS RC quallity, and in desperate need of feedback. As the feedback didn't come during the Alpha and Beta cycles, we decided to release an RC. It's not like the users/potential testers gave us much choice..."
    author: "jospoortvliet"
  - subject: "Re: Glad to hear it"
    date: 2007-12-03
    body: "I really understand and agree with that, but that 95% is hard to get at if your desktop keeps on freezing after a minute or five of playing with it (latest Suse live CD). It may be a minor thing causing it, but the only way I found out was restarting X. That is simply not acceptable to a majority of users, and makes the whole of KDE 4 feel unfinished. Yes, that is an unfair assessment, I know, and I am confident KDE 4 will eventually rock, but it is not right now. It is also not like you can for now keep the oldfashioned, stable desktop and start playing with all those great, RC quality applications..."
    author: "Andr\u00e9"
  - subject: "Re: Glad to hear it"
    date: 2007-12-03
    body: "I do."
    author: "Louis"
  - subject: "Someone who sees the bright side"
    date: 2007-12-02
    body: "Developers: You're doing a great job indeed! Don't let some people get you down. If you follow the dot and planetkde, then you'll see that the people only complain about some GUI stuff and RC1, they don't even take the time to install a recent build where most of the problems are already fixed. \n\nAnd a positive point to end my post with: The new menu is very good(favourites, search, history). Way more features than the old 90's menu. The critics of the new menu are actually arguing that they want LESS features. Who understands them?? :-p\n"
    author: "Tom"
  - subject: "Re: Someone who sees the bright side"
    date: 2007-12-02
    body: "my favorite menus are the GNOME menu and the Windows XP menu. I never really tried other menus except the Windows 98 style menus in Windows, GNOME and KDE.\n\nI think menu preferences are probably a matter of habit. new users want their menus from the previous environment, and in KDE 4.0, everyone is basically a new user, except those who regularly used the development releases. I'm not saying that's everyone. many users welcome change and like new features, but many would rather stick with what is familiar, or works for them, or like new stuff but determined that this particular new stuff isn't for them."
    author: "yman"
  - subject: "Re: Someone who sees the bright side"
    date: 2007-12-02
    body: "I agree, it is a habit, but by trying out KDE4, you're trying something new, so I think you should be consistent and keep an open mind then on everything that is presented to you, including the menu.\n\nIf the argument is: I want to stick with what is familiar, then I wonder why they don't stick with KDE3 then. Apparently they don't really want to stay with what is familiar. \n\nIt's the same argument about the look of KDE4... I'm often wondering what it is that people don't understand when our developers say it is FULLY themable.\n\nMy post was just to let developers know that there are users who respect and understand that the foundation is layed now for the future.\n\nAnd to end my post another time with a positive note: KWin is really better than compiz-fusion, the default configuration is just what you expect from it, it is usable, not just flashy! \nThanks for reading."
    author: "Tom"
  - subject: "Re: Someone who sees the bright side"
    date: 2007-12-03
    body: "I'll say why I prefer the old menu: speed. Kickoff simple gets in the way. It requires one to repeatedly move the mouse from one side to another of a rather large menu. The use of mouse-over buttons results in accidental switching of menu mode. Most people put shortcuts to stuff like My Computer, /home, etc on the desktop, not in the menu. Assuming Plasma sticks with the usual desktop by default (it's already much more powerful), then having it in the menu is stupid and repetitive and a waste of space.\n\nLet it be noted that this is a fairly minor problem (nowhere near any kind of showstopper) it is an annoyance that should be a configurable option (FYI, KDE4RC1 is already my day-to-day desktop).\n"
    author: "Michael"
  - subject: "Re: Someone who sees the bright side"
    date: 2007-12-09
    body: "personally, I see the desktop as the place for all the stuff I'm currently working on. the main menu is to me where I find places, applications, and system configuration. I really like my desktop to be  completely clear of anything but the files I'm working on and files I can't place anywhere else.\n\nthat's just my personal way of working.\n\nI guess you use the desktop for links to your most used apps and to some of your files, and directories you frequent. that's all dumped into the Kickoff menu, which really inconveniences you. after all, the desktop is meant for your favorite apps, not the menu, and they are more quickly accessed from the desktop. in fact, what advantages does the kickoff menu have?\n\nFavorites:\nthe desktop is better for that.\n\nHistory:\nyou already have recent documents, and you can add recent applications to the kmenu.\n\nComputer -> Programs:\ncovered by \"System\", \"Settings\", and \"Utilities\".\n\nComputer -> System Folders -> home folder:\nalready on the desktop.\n\nComputer -> System Folders -> document folder:\nalready on the desktop, or can easily added to it. besides, who needs it?\n\nComputer -> System Folders -> remote folders:\nalready on the desktop, or can easily added to it (I think).\n\nComputer -> Media:\nit's already on the desktop as \"Computer\", right?\n\nApplications:\nthats the main function of the kmenu!\n\nLeave:\ncovered by \"Logout\" and \"Lock Screen\".\n\nthe only thing thats added is search.\nof course, to many people, perhaps me included (never tried kickoff), this menu might offer a better solution, but for others, this is an unnecessary and inconvenient restructure that only inconveniences and doesn't really improve anything. plus, it can clash with their philosophy about how desktops should be used. switching a menu might require changing the whole way you work and think and that's a huge change. I'm really glad that there is now a KMenu for Plasma, even though I don't expect to ever use it.\n"
    author: "yman"
  - subject: "Re: Someone who sees the bright side"
    date: 2007-12-11
    body: "I dunno. I do it completely differently. I completely cleared out my desktop and disabled all icons. Furthermore, I hardly ever use the K menu; I launch everything with Katapult. I have never tried KDE 4, but if the new menu is set up to operate like an improved Katapult; I would celebrate it."
    author: "alphamugwump"
  - subject: "Better served cooked, than half cooked"
    date: 2007-12-02
    body: "I was very worried about the quality of KDE 4.0. I think now KDE is on the right track. You should have gone for many betas. At least go for few RCs. Served it when cooked, not half cooked."
    author: "Sagara"
  - subject: "The price of success"
    date: 2007-12-02
    body: "Nice to hear that the realease of KDE 4.0 was postponed, it's not yet stable as many (the majority?) users expect it, IMHO.\n\nIn my opinion kde developers must take more into account that when KDE 4.0 will be released it will be reviewed from many people that do it just for sport and not to contribute to it. What they will do is judge it, not contribute to it.\nAnd if KDE4.0 will be released with some unfinished stuff they will say that it sucks.\n\nSo if you are planning to release it early just to get a wider user base to test it, think to it twice, the risk is that you get only unconstructive critics, i.e. what you don't like and doesn't deserve.\n"
    author: "KDE User"
  - subject: "Re: The price of success"
    date: 2007-12-02
    body: "I agree.\n\nThe kde \"users\" community is much wider now than at the time of KDE2.0.\nBut there are many kind of users, the developers acting as users, the power users, the new users and the normal users.\n\nI think that after KDE-4.0 \"developers platform\" we need a KDE-4.0 \"Enthusiasts Edition\" one that explicitly targets users willing and able to contribute to the KDE development with constructive feedback and that don't mind the fact that they can't remove yet their beloved kde-3.5.x.\n\n"
    author: "A lazy KDE user"
  - subject: "Re: The price of success"
    date: 2007-12-05
    body: "Currently that is KDE4 Daily."
    author: "Glen Kirkup"
  - subject: "Small Incremental Releases"
    date: 2007-12-02
    body: "KDE 4.0 at it current state is a mess.\nTo avoid future disasters like this you might consider instead of trying to build a complete new version 3.X -> 4.0 instead going in small incremental steps.\nTo achive this: Become independent from Trolltech: Fork QT. Make QT a part of the KDE source tree and maintain it along side with KDE. Throw out all unnecessary stuff not related to KDE.\nTry to remember some forgotten values like: simplicity, correctness and performance.\nFight your egos and listen to the flaming crowd. \n"
    author: "anon"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "This comment is a disaster!\n\nKDE-4.0 is going in the right direction. Only requires more time to be finished and a strategy to be released to an incremental user base and not to the whole public.\nThe first step was the release targetted to the developers users.\nNow it's time for power users, users interrested in the KDE advancement.\nThen will come the others.\n"
    author: "A lazy KDE user"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "> This comment is a disaster!\n\nNo, it's preposterous - a troll's rotton breath."
    author: "Carlo"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "Reading this comment, I'm reminded of Pauli's (in?)famous remark to a student: \"This isn't right.  It's not even wrong\".  I honestly don't know where to begin critiquing it, so I guess I'll just say that Qt currently has over 40 full-time very talented staff working on it, so \"forking\" Qt would be about the worst possible drain on manpower imaginable."
    author: "Anon"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-03
    body: "Not to mention that some highly valued KDE contributors are actually on Trolltech's payroll (cough! *aaron* Cough! *Thomas* cough!) and that agreements are in place to secure the availability of Qt for KDE in the future.\n"
    author: "Andr\u00e9"
  - subject: "kde4"
    date: 2007-12-02
    body: "Well i installed it through kubuntu hardy... I use it now everyday but I am retired so I have time to play :)\nAs I couldn't get coming changes I compile svn and apart few things which aren't working I think kde4 should be quiete interesting in january :)\nThe problem is that 3.5.8 has so many enhancing that it will be hard to get them straight away on kde4 but they plasma on kde4 which will make the difference... Tanks all for your good job :)\nNow I can even use kdevelop to develop my small examples :)\nriri\n"
    author: "riri"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "Sorry, I find your comment a little bit biased, your arguments are .... *well* .. nevermind. \n\nOn track, to bad the final 4.0 release of KDE is delayed. But it's better then have a rushed-out release. I'm using the kde4daily image and I see major changes after every svn update. And to be honoust I think the end result will be a close finish anyways.\n\nGood luck to the KDE guys which are working their arses off to get a new exciting product released.\n\nCheers!"
    author: "Gerwin"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "I wholeheartedly disagree. Small incremental changes are nice to maintain stuff, but for leaps forward you need a major code rehaul. There comes a point in any software projects life where it would be easier to do a complete rewrite rather than maintain it with small patches. The switch to KDE4 gives us this chance. We should give the devs the time they need to produce a proper KDE4."
    author: "Abdulla Bubshait"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "Hey! We've got an expert here! I name you brand new \"KDE Project Manager\" from now on! All hail the new KPM!"
    author: "Vide"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "Why do you think forking QT is 'the way'?"
    author: "oc"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "I will make it even more simple.\nfork KDE! You will save the world from the blight that is KDE 4"
    author: "pharaoh"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "ROFLC0PTER"
    author: "Ian Monroe"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-02
    body: "Sorry, but forking QT just doubles your work load (now you have to develop the fork, and KDE) while not adding any benefit.  In fact, it hurts you since you lose all the benefit you currently have from the professional QT development that is on-going.\n\nEvery once in a while, you really need to rethink thinks, and redevelop from the ground up.  I'm sure the KDE 4 series will be absolutely great in time."
    author: "T. J. Brumfield"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-03
    body: "KDE 4 is not a complete rewrite. It is a massive overhaul, but lots of existing pieces of code are \"just\" ported and refactored. \nHaving said that: exactly what reason do the developers have to \"listen to the flaming crowd\"? If you, as the flaming crowd, know so much better, why don't you do the work then?"
    author: "Andr\u00e9"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-04
    body: "Exactly, speaking as a developer, I tend to ignore people that aren't friendly enough for my sense of humor du jour. Odds that someone who asks for something in a friendly way is ignored are very slim, however."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-03
    body: "I have ported several Qt3 apps to Qt4. The mistake KDE made was subtle, and one many others have made. Simply put, they ported at the same time they redesigned. This kept the software in an unusable state for far too long. It would have been better (IMHO) if they had first ported to Qt4 and released a Qt4-based KDE3.5, and then redesign from a solid stable base. This approach does take *longer* overall, but you will have the benefit of a good interim release.\n\nNo need to fork Qt."
    author: "David Johnson"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-03
    body: "Almost but not quite... The port was done as a separate, but it never really stabilized.\n"
    author: "SadEagle"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-03
    body: "True, but kde3.5 is good enough to survive one other year, so no real need for an interim release, imho. I prefer to wait a little bit longer and get a next generation KDE rather that an improved one and wait again (and this time for a long time) for the next version.\n"
    author: "A lazy KDE user"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-04
    body: "I may be wrong, but I think that'd kill binary compatibility.  Qt3 is not BC with Qt4, and KDE also guarantees BC throughout the major versions, so by porting to Qt4, KDE is no longer BC with the previous version.  If they released that, then there would be another BC break (or severely limit what refactoring they can do) when they redesign.  KDE is the framework so many applications are built on.  I think in the long run, and for the sake of those other developers, doing both at the same time (meaning no intrim release) was the right way to go."
    author: "mactalla"
  - subject: "Re: Small Incremental Releases"
    date: 2007-12-14
    body: "we'll get it done.  however, you're not wrong.  more incremental releases would actually be a load off on everyone. i also agree on the QT thing. alas, this one is quite the pickle. better leave it be.\n\nguess who.\n\n\n\n\n"
    author: "psigh"
  - subject: "Windows Vista:"
    date: 2007-12-02
    body: "Hi folks,\n\nWill KDE 4.0 be any better graphicaly than KDE 3.XX and how does it compare to Vista's Aero interface. Which is better? Will KDE work on Windows Vista in place of the Aero interface?     "
    author: "JimmyX"
  - subject: "Re: Windows Vista:"
    date: 2007-12-03
    body: "That is is a very subjective question.\n\nI'm sure someone will make a Vista theme on kde-look.org to make KDE 4.0 look like Vista if you like.  And there will be people who skin it to look like OS X, and such.  I'd say that KDE is much more versatile in its appearance than Vista.  What it looks like will largely be up to you.\n\nI don't believe that KDE 4 on Windows however will use Plasma.  Last I heard, the desktop portions of KDE are not being ported to Windows, just the applications."
    author: "T. J. Brumfield"
  - subject: "Kde 4 RC1+ "
    date: 2007-12-02
    body: "Kde4 apps is thanks to Qt4 and core tehnologies much better, but desktop (desktop & settings & panel) agrrf - gnomish. Oxygen - icons profesional, sound  \"smells awfully\", windec dead (in other words - too much grey - i want plastic like (blue & grey) with oxygen buttons and domino corners), ....\n\n- Desktop of Kde Four Live RC1+ stil looks like 80s blackbook. I think that settings corner is a big mistake. There isn't any gradients. Simply too ugly. And you can acess to these functions with right click too.\n- Walpaper config is there, but simply too simply for me. Where are now Position options (scaled, centred, tiled), Colors options (various gradients),  Blending?!?! And how can I set 1 wallpaper for desktop 1, secound for D2, etc.???\n- What about runing KDE3 aplications in KDE4 desktop? \n- What about Raptor (kbfx for 4 generaton?)\n- What about option for KDE classic menu, from 3.x generation (many there think that it is much much better than Kickoff)\n- What about new splash screen?\n- What about current app menu bar (MacOS style)\n- Icons on desktop looks bad, but they are very usuable. We need it (and please  simple .desktop files - not plasmoids)\n \nGreetings, Miha"
    author: "mihas"
  - subject: "freeze status?"
    date: 2007-12-02
    body: "I'm utterly confused what this means regarding the freeze status. I guess API and ABI will need to remain compatible. But does the delay give us another chance for message string changes, for instance?"
    author: "tfry"
  - subject: "Re: freeze status?"
    date: 2007-12-03
    body: "Replying to myself, in case anybody else is wondering about this: The freeze remains in effect. See http://lists.kde.org/?l=kde-release-team&m=119669157907550&w=2 ."
    author: "tfry"
  - subject: "One month"
    date: 2007-12-02
    body: "One month isn't enough. If KDE 4 wants to live up the expectations at leasts two months is needed, but 4 months would be better. \n\nI mean, can anybody give me a link to the requirements + estimates used for this descision? "
    author: "Dennie"
  - subject: "Re: One month"
    date: 2007-12-03
    body: "I agree.. I would love to see KDE 4 really ready and perfectly running on April...\n\n"
    author: "Somebody from somewhere using KDE"
  - subject: "Re: One month"
    date: 2007-12-03
    body: "I hope they will release bugfix releases frequentely do that you can switch to, say, 4.0.5 in April if you find it stable enough."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: One month"
    date: 2007-12-03
    body: "Two months ago it was delayed by two months. Now by one month. I think if it won't be stable enough in January, it will be delayed by an other month."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: One month"
    date: 2007-12-03
    body: "Now, that is _exactly_ the problem I'm addressing. Nobody can count on anything. Basically this way it is done when it's done. The question is: when is it done? You can only tell that by defining clear functional requirements, defining the number of hours a developer can work on their project and ask those developers to give estimates on the work they're doing. Now we have a good view on when things are done. Add an additional 10% overhead and we're not telling the community farytails anymore.\n\nAnother thing is: Developers are adding functionality like crazy. When is this beta or even alpha tested? The thing is: KDE 4 is -not done-. So please stop this and begin with a good solid release plan. \n\nAnother thing I hate is that some developers say: No, 4.0 is more like an intermediate release for developers and early adopters. NO! That's what alpha's and beta's are for. "
    author: "Dennie"
  - subject: "Re: One month"
    date: 2007-12-03
    body: "> I mean, can anybody give me a link to the requirements + estimates used for this descision?\nSure. Check out http://techbase.kde.org/Schedules/KDE4/4.0_Release_Beta_Goals.\nCurrent showstoppers are plasma and konqueror FM (web browsing? I'm posting this from Konqeror 4).\n"
    author: "Michael"
  - subject: "Finally using SVN"
    date: 2007-12-02
    body: "Well, I've decided I can't wait that long, so I'm installing from the SVN now. Maybe I'll actually get my act together, and contribute somehow."
    author: "Soap"
  - subject: "Re: Finally using SVN"
    date: 2007-12-03
    body: "That's the spirit!  Together we'll beat the bugs.  \n\nMaybe we can get the dot admins to hang some old propaganda posters up in here to create a feeling of solidarity and progress ;).\n\n"
    author: "Bille"
  - subject: "Re: Finally using SVN"
    date: 2007-12-05
    body: "If you find it's too much hassle, as I did, I'm currently using KDE4 Daily and it's a wonderfully streamlined process for debugging."
    author: "Glen Kirkup"
  - subject: "4.0 Final in one month?"
    date: 2007-12-03
    body: "I've no doubt the plasma developers are pretty good at what they're doing. But I really wonder about the state of the desktop. To deal with the plasma applets is a mess. What about that horrible button in the upper right desktop corner? How can I move to and integrate the pager applet in the bar at the bottom (whatever it's current name is - formerly kicker)? Application windows easily hide applets - I can't find a way bring the applets into the foreground like a normal application window (by clicking it in the taskbar).\n\nI know, all is easy to implement and so on. 4.0 is not 4, and it's meant to be for developers only. You'll tell me about the preliminary state and so on. Perhaps you'll ask what I contribute instead of critizising. Anyway, it needs a bit more to convince me. All I can see is 3.95 up to now."
    author: "Andy"
  - subject: "Re: 4.0 Final in one month?"
    date: 2007-12-03
    body: "Plasma already has dashboard support.\n\nEhm, just drag'n drop the pager applet to the panel, it may give you some issues at that time, but if logged out and in, it'll be just nice (at least that is what happens here).\n\nBTW: I can't find any key binding to call the dashboard, but you can put a show desktop plasmoid on the desktop or the panel. "
    author: "Luis"
  - subject: "Re: 4.0 Final in one month?"
    date: 2007-12-03
    body: "Ehm, a show-desktop plasmoid on the *desktop* is just a non-sense ;P"
    author: "Vide"
  - subject: "Re: 4.0 Final in one month?"
    date: 2007-12-03
    body: "Yes, I know, but there is a little problem if you put it in your panel, once the dashboard is active, it's impossible to click, so you have to put one on the desktop XD."
    author: "Luis"
  - subject: "Re: 4.0 Final in one month?"
    date: 2007-12-04
    body: "CTRL+F12\n\nTry other CTRL+F?? combos, and you see more of the fancy things in KDE4 :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: 4.0 Final in one month?"
    date: 2007-12-03
    body: "3.95 AKA beta 4 is more than a month old."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: 4.0 Final in one month?"
    date: 2007-12-05
    body: "Did you run out of figures? Beta6 were more appropriate than RC1 for what I can see ATM. If you feel you need to release something, then go and release the libs. Releasing the \"desktop\" in its current state (and probably the next 4 weeks won't change it too much) will cost you more respect than a delay of another quarter."
    author: "Andy"
  - subject: "KDE4's kmenu"
    date: 2007-12-03
    body: "Don't know if this screenshot of the kde4 menu is a joke, a beta test a skin or a plugin, \n\nhttp://kde.rinet.ru/announcements/announce_4.0-beta4/kde4.0-beta4.png\n\nbut if not, I hope there will be an option in order to revert back to the KDE3 menu."
    author: "Meghazi Fabien"
  - subject: "Re: KDE4's kmenu"
    date: 2007-12-03
    body: "I think this deserves a huge red text in every release announcement:\n\nThere won't be any \"old style\" KMenu unless someone provides one. "
    author: "Luca Beltrame"
  - subject: "Re: KDE4's kmenu"
    date: 2007-12-03
    body: "I'm not going to do it on my own, but if someone wants to join me, I am seriously considering writing a new menu for KDE4. It definitely won't be ready for January, but maybe we can have something for 4.0.1. I'm leaning towards a classic menu. Email me if you are interested (remove the \"no.spam\" from the address)."
    author: "David Johnson"
  - subject: "Want to help"
    date: 2007-12-04
    body: "I want to help. I am almost ashamed that I am unable to do so!\n\nDonating money at this time probably will not help to make this a great release either."
    author: "Daniel Aleksandersen"
  - subject: "Re: Want to help"
    date: 2007-12-04
    body: "Grab a distro that offers up-to-date versions of KDE4 like OpenSUSE or KDE4Daily, and get filing bug reports - it's easy and very useful!"
    author: "Anon"
  - subject: "Re: Want to help"
    date: 2007-12-04
    body: "Also, if you've got the time, participate in the Krush Days on saturdays, to help, triage and fix many bugs. "
    author: "Luca Beltrame"
  - subject: "Re: Want to help"
    date: 2007-12-05
    body: "Anyone can help with KDE4 Daily, it's impressively simple."
    author: "Glen Kirkup"
  - subject: "I love KDE - but this is (/%/($&("
    date: 2007-12-04
    body: "I have been using KDE sce beta 2 - before version 1.0 was ready. It was great. I have compiled KDE for countless hours and have even added minuscule portions of its code... I have reported and tested hundreds of bugs and have defended KDE against unfair attacks many times.\n\nBUT - the release management of the KDE4 branch is the worst I have seen. I can only compare it to Gnome-1.0, which also sucked bad. How do you call this development snapshot RELEASE CANDIDATE? No packaging is available, only live cds because noone wishes to ruin a real system with this snapshot. All the live cds I tested were not usable as desktop - less so than any extremely simple WM like icewm or the like. FVWM is more usable than the RC1.\n\nPlease do take time until spring, add a number of real release candidates, when the developers run out of immediately obvious bugs. At the moment there are glaring bugs in the entire environment. To call this release candidate reminds me of the WOW effect of Vista... For the first time in years I have checked out gnome (still behind KDE3, fortunately).\n\nPlease take your time, package the stuff to get real users using it on their desktop machines (e.g. debian packages) help the packagers with their packages, wait until the flood of bugs decreases and then call it a release candidate - when it is ready. Stop the MARKETING CRAP - it's lying to people. Gnome is better at that game.\n\n\nPlease let me know when there are DEbian packages of KDE-4.0 that can be installed in parallel to the KDE3 desktop... You will then get as many bug reports as you like from me.\n\nBest regards\n\nMoritz\n"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: I love KDE - but this is (/%/($&("
    date: 2007-12-04
    body: "Moritz,\n\nAgree with you on most points, however, I have just installed Ubuntu Gutsy Gibbon (7.10) and of course the KDE front end (Kubuntu).\n\nI've always prefered the KDE environment, maybe because I was a mac & windows boy for a decade or more, however I think the KDE branch has lost significant ground to the GNOME environment...........\n\nTo the extent that I doubt I'll be returning to KDE as long as GNOME (in it's Ubuntu flavour anyhow) is as good as this.\n\nfor me, KDE has always been about attractivenes as much as usability - it's just as easy to acomplish tasks in GNOME, it's just slightly prettier in KDE.\n\nOr at least it was.\n\nA few mouse clicks (okay, I used a terminal, but you can use mouse clicks) and you've got the amazingly configurable Compiz-Fusion, and whadda you know?\n\nKDE may have 'nicer buttons', but GNOME (ubuntu's version anyway) is now incredibly stable, as clean or as whizzy as you want, and visually stunning.\n\nControversial as it may be, I find myself thinking, \"what's the point of KDE?\"\n\nKubuntu's KDE is buggy and, well, so last year.\n\nI strongly disagree that GNOME is still behind KDE3 - in my opinion it is light years ahead.\n\nSo if KDE want to remain relevant, I suggest taking AS LONG AS IT TAKES to come back with something with genuine wow factor (and not the 'wow, this is crap' I've just had trying to run the live RC cd).\n\nAlso, as an aside, I know it's fashionable in KDE circles to denigrate GNOME, and especially a 'for the kids' distro like ubuntu, but have a go with 7.10 and see if the prejudice remains..............\n\n\n "
    author: "FatFox"
  - subject: "Re: I love KDE - but this is (/%/($&("
    date: 2007-12-06
    body: ":/ You can put Compiz Fusion in KDE too. Hell, you can put kwin into GNOME.\n\nTell me when GNOME has their own windowmanager, okay :)\n\n(And no, metacity is just a window manager, and works perfectly well without GNOME - doesn't even depend on the GNOME libraries, and before Metacity, GNOME used Enlightenment then Sawfish).\n\nI use KDE for the sheer power it gives me over my system. If it can be done, there's probably a configuration option that can do it. I've got my tabs at the bottom of Konqueror, for example."
    author: "cloakable"
  - subject: "Re: I love KDE - but this is (/%/($&("
    date: 2007-12-07
    body: "\nYeah, I know - about the compiz-fusion on Kubuntu.\n\nHave you tried it?\n\nBuggy as hell - seems to be fully integrated into ubuntu, and positively poisonous to kubuntu.\n\nStrange, I know.\n\nBut then again ntfs read/write support works better in GNOME as well (Ubuntu will write to my ntfs external hard drives, Kubuntu won't) - challenging question, but (usability aside, which is where I'm actually coming from) - what can you do in KDE that can't be done in GNOME?\n\nI run many K-programs in gnome (& vice-versa) - they just don't always work so well as in the 'native' environment.\n\nI used to prefer KDE, now I prefer GNOME - if KDE works for you, and you prefer it, great - that's the beauty of linux & open source in general, we're not all herded into 'my way or the highway' Windows, check out Mint, PCLOS, SUSE - there's a flavour for pretty much anyone.\n\nMy point was that GNOME seems to have taken a huge leap forward in aesthetics & usability, & KDE (for me anyhow) has missed a step.\n\nI don't care what you use on your system - if it works & makes you productive (& happy!) then more power to you.\n\nI only made the above comments as an ex-KDE fan - it was (in my opinion) way ahead of GNOME, and it seems to be lagging at the moment.\n\nJust my opinion - & I'm waiting to see what KDE comes up with next (competition is always better from an evolutionary point of view!)\n"
    author: "FatFox"
  - subject: "Re: I love KDE - but this is (/%/($&("
    date: 2007-12-04
    body: "\"Because no one wishes to ruin a real system\"?\n\nYou're going far with assumptions, I see. If you actually read around in the MLs you'll see that is mostly because stuff had to be adjusted in order to be co-installable. Aside that, that's the distro's responsiblity.\n\nFinally, like other people on the Dot, why on Earth are you being so disrespectful? You can say \"marketing-wise, I think that calling it RC may have been a mistake\" instead of \"Stop the MARKETING CRAP\". It takes seconds to write a calm and polite reply that may disagree but not be on the borderline of offensive.\n\nIs it that hard?"
    author: "Luca Beltrame"
  - subject: "Marketing Crap"
    date: 2007-12-04
    body: "The people who wish to test betas of desktop environments invest time and effort and are mostly not stupid. To think that the wrong label (\"release candidate\") for an early development snapshot would lead to more testers and to better bug reports is insulting and not very smart. Or to call it short \"CRAP\". \n\nStuff like this did not happen for KDE 1.0, KDE 2.0 and KDE 3.0.\n\nIt is purely marketing driven and it is crap, so I think marketing crap is still a polite way to express myself."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Marketing Crap"
    date: 2007-12-05
    body: "Right. As much as I hate to say it as a devoted KDE fan, but it seems to me that some developers have developed a certain kind of arrogance. We all know that they spend hundreds of hours per year without pay doing their best to improve KDE and deserve respect for that and everything, but they seem to forget that the devoted users, and not just those who are directly contributing through active testing, but also those who help to spread the word, are just as critical for the success of the desktop. And I think that we have a right to be heard too. This \"you don't have to pay for it, so stop complaining\" - attitude doesn't h help anyone. And marketing something as RC1 that obviously has alpha quality at best (read: not even close to feature complete) IS downright disrespectful towards potential testers that the developers are trying to attract.\nSo the bottom line is: Dear KDE devs, I say this with all love and respect, but please please admit that you completely screwed things up when it comes to dealing with your user community, and try to set things right again."
    author: "chris.w"
  - subject: "Re: Marketing Crap"
    date: 2007-12-05
    body: "I fully agree. Go and set things right. Take another quarter to finish the desktop before releasing it. And don't take the easy route and call everybody criticizing the release cycle a troll.\n\n(Just to make it clear: I love KDE, too. I'm using KDE for almost 10 years. I'm using it for more than 7 years daily in the office. I'd contributed some bugfixes and a few features over the last years. I've recommended it to numerous people.)\n"
    author: "Andy"
  - subject: "Re: I love KDE - but this is (/%/($&("
    date: 2007-12-04
    body: "KDE 4 is packaged for openSUSE (and I think other distros also) so that one can install both kde 3 and kde 4."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Packages?"
    date: 2007-12-04
    body: "I wanted to try KDE 4 since its Beta days, but I can't because there are no packages for my distribution (Slackware). How do you want us to test the software if we cannot have it in our computers? Building from source 1) takes days, and 2) completely messes up the administration of my workstation.\n\nCan you get a way to provide packages for the most popular distros, please?\n\nThanks,\n\nEduardo"
    author: "Eduardo Sanchez"
  - subject: "Re: Packages?"
    date: 2007-12-06
    body: "Install it to /opt/kde4, then there should be no problems with messing anything up.\nYes, you have to build Qt4 first.\nThe cmake coming with Slackware 12 is recent enough, dbus too, not sure if I had to install any other additional packages.\n\nAlex\n"
    author: "alex"
  - subject: "Oh! Come on!"
    date: 2007-12-04
    body: "For all people crying about RC, just get over it!\n\nKDE 4.0 is in a much better shape now, Do you know why? Because a lot of people test RC and submit bug reports, that was the plan, the idea behind the label, thanks to that KDE 4.0 improve a lot.\n\nAnd, they didn't do anything to majority of their user base, people reading the dot, planetkde and testing alphas, betas, and RCs aren't the big portion of the user.\n\nAny way, cheers to all developers.  "
    author: "Luis"
  - subject: "Re: Oh! Come on!"
    date: 2007-12-04
    body: "\"RC\" stands for \"Release Candidate\". That means the developers think it is ready for release, mthough it might still have a few known minor bugs. The purpose of a release candidate is to expose the software to a wider audience in the hopes of shaking out a few more bugs. In no case should a release candidate be offered if there are any known major or critical bugs. In every case the candidate should be fully usable by the target audience. Such was *NOT* the case for the RC1.\n\nSome people above argued that the RC was justified in that the libraries were ready for release. But it wasn't just the libraries that got the RC tag, it was everything else as well. Putting the RC on the desktop meant that someone thought the desktop worthy of the RC tag.\n\nTelling me to \"get over it\" is doubly insulting. Has KDE become too sacred that we're no longer allowed to criticize?"
    author: "David Johnson"
  - subject: "Re: Oh! Come on!"
    date: 2007-12-04
    body: "I speak for myself, as a user, and not for anyone else. \"Allowed to criticize\" does not mean posting stuff like \"this is crap\", and negative and unconstructive comments or \"RC was a Beta!\". \nThe RC tag was not meant only for user's testing, but also to get most of the developers into \"release mode\", i.e. to start fixing showstopper bugs, since after two years of \"freedom\" there was the need to shift focus on to the upcoming release."
    author: "Luca Beltrame"
---
The KDE Release Team has decided to release KDE 4.0 this coming January. The release was originally planned for mid-December. The KDE developers want to solve a couple of essential issues before releasing. Having solved some of those issues, among which were glitches in the visual appearance, and in Konqueror, the KDE community hopes to have a KDE 4.0 that will live up to the high expectations for it. Read on for more details.





<!--break-->
<p />
Meanwhile, the progress towards KDE 4.0 is astonishing. Most parts, such as the KDE Development Platform and a lot of applications are considered stable and well-usable.
<p />
Some parts of the desktop experience do not yet meet the KDE community's quality standards and expectations for a stable release. There are also some issues which need to be addressed upstream, for example a bug in certain codecs of xine that cut off audio fragments prematurely. The developers are confident to be able to release a more polished and better working KDE 4.0 desktop in January. The changed plans involve releasing on January 11th, 2008.
<p />
At the same time, the release team's call for participation is repeated. To make KDE 4.0 a success, your effort is needed. An overview of current showstoppers can be found on <a href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Beta_Goals">Techbase</a>, KDE's knowledge platform.
<p />
This is also a call to the wider Free Software community, and also to companies working with KDE. If you have the resources to contribute, assistance in fixing the remaining bugs is most welcome.




