---
title: "KDE Commit-Digest for 15th April 2007"
date:    2007-04-16
authors:
  - "dallen"
slug:    kde-commit-digest-15th-april-2007
comments:
  - subject: "eh can you update the wiki for kplato "
    date: 2007-04-16
    body: "hello\n i know this is not the right place to speak about it, but i am too newbie to post in a mailing list, i tried even to contact the author, but i guess, i scared him with my poor English language.\ni noticed that kplato are getting more features, it seems now he can even draw EV charts( ok this is a premiere for open source project management software)\nso please give some love to documentation to reflect the actual progress.\n\nfriendly mimoune\n\nps: waiting for koffice under windows ;)"
    author: "djouallah mimoune"
  - subject: "Oh my gosh..."
    date: 2007-04-16
    body: "...I was afraid it wasn't coming. :-)  Thanks, Danny!"
    author: "Louis"
  - subject: "Kollision"
    date: 2007-04-16
    body: "Sounds like a good game. Anyone who knows where I can find the flash game? There are some things I want to try out. I also have some ideas, if the author of the game is interested; however, I would like to try it out first (I don't run SVN)."
    author: "Lans"
  - subject: "Re: Kollision"
    date: 2007-04-16
    body: "Good game? Its tones of fun, if its based on the old Koules games:\nhttp://happypenguin.org/show?Koules\n\nYou have to push a bunch of enemies against the walls. They explode and become smaller (but more in number), until they get so small they die, and you advance level. Bigger ones are stronger, but slower at accelerating. I don't remember if there were other types of enemies, objects or popups... You could also player with other humans. Tons of fun.\n\nAccording to Koules, its an original idea: \u00abKoules is an original idea. First version of Koules was developed from scratch by Jan Hubicka in July 1995.\u00bb Would be nice to credit the guy."
    author: "blacksheep"
  - subject: "Re: Kollision"
    date: 2007-04-16
    body: "Yeah, koules is a lot of fun. I still play it regularly.\nAnd the story is also very funny (and weird)."
    author: "infopipe"
  - subject: "Re: Kollision"
    date: 2007-04-16
    body: "It's not based on Koules, but on this flash game by Matteo Guarnieri: http://www.ragdollsoft.com/particles/\n"
    author: "Paolo Capriotti"
  - subject: "Re: Kollision"
    date: 2007-04-17
    body: "Ah, I see. You really should look at Koules; the engine should probably be similar, but the gameplay is so much more fun."
    author: "blacksheep"
  - subject: "Re: Kollision"
    date: 2007-04-17
    body: "YES! \n\nMy brother and I played koules for hours on end. The different enemies are incredibly fun and varying (my favorite are those sticky-kind). Getting a ton of strength balls so as to knock the big balls with ease is fun as well. \n\n-Sam"
    author: "Samuel Weber"
  - subject: "Re: Kollision"
    date: 2007-04-17
    body: "That kind of sounds like \"Beast\" back in the day of the IBM computer with 3,5 floppy disks and monochrome screens."
    author: "Darkelve"
  - subject: "Re: Kollision"
    date: 2007-04-17
    body: "Do you mean this Beast game:\nhttp://en.wikipedia.org/wiki/Beast_%28video_game%29\n\nIf it is, I agree my description may sound similar, but it has no resemblance with it. Check it out; it's worth the pain to compile it. ;)"
    author: "blacksheep"
  - subject: "Welcome screen"
    date: 2007-04-16
    body: "Ooo, looks very nice indeed.  \n\nThe one problem with this sort of thing is that people like it then quickly suggest tons of more things to do with it :-)\n\nSo I suggest also adding a help button and have the help text also show in a similiar way, instead of the kde help app?\n\nAlso maybe show the paused state in the same way?  (When applicable)."
    author: "JohnFlux"
  - subject: "Re: Welcome screen"
    date: 2007-04-16
    body: "I would be delighted if the KDE games got rid of \"Configure shortcuts\" and other \"KDE-ish\" thinks, like toolbars etc.\n\nHowever, that's probably not going to happen. But a similar paused state should be asking for too much, is it? :)"
    author: "Lans"
  - subject: "Re: Welcome screen"
    date: 2007-04-17
    body: "Why? They are some of the best bits! I know where to look to see what shortcuts are available and where to re-bind them; I know where things will be, so I am far more productive than with other applications where everything is in some arbitrary place. "
    author: "Cerulean"
  - subject: "Re: Welcome screen"
    date: 2007-04-17
    body: "Please have a look at my screenshot: http://img238.imageshack.us/img238/3701/kdegamesqm3.png\nDisadvantage with the current method:\n\n- If I'm going to play the game, I want to know the controls to play the game. Right now it's a mess with all different kind of actions.\n\n- I don't think most new users would go to \"Configure Shortcuts\" if they want to reassign the keys or just see the controls.\n\n- Let's say I don't like the current controls so I want to reassign all keys for Player 1. How do I do that in a fast way? Alt+U isn't obvious to all users, so they end up overusing the mouse. Also, some get confused when they try Escape to cancel, or when pressing Return.*\n\nTo sum everything up, it just doesn't behave as a game in my opinion. Many users don't care much about the shortcuts* in other applications, let's say Konqueror, as long as they can use the mouse. Maybe they know ctrl+c and ctrl+z, but the default shortcuts are good enough; no need to change them. \nHowever, when you play a game, most times you have to know the controls. Sometimes you also have to change them. But the most important thing, the controls in the game have to be presented in a \"simple\" way; the first impression I got from KDE games when trying to look the controls up was \"wow, too much information here; which is the relevant? Which do I actually use in the game?\"\n\nI wonder if there are any plans to change (or let's say improve) this in KDE4 games. Or is it only me who dislike the current approach?\n\nHope this will turn out to be an interesting discussing, I look forward to hear other users and devs thoughts.\n\n* Note: I don't have any test results or such to verify this statement, it's only that I personally have experienced when seeing other people use computers."
    author: "Lans"
  - subject: "Re: Welcome screen"
    date: 2007-04-19
    body: "The problem here is you are advocating against one of the core ideals of a DE like KDE. That ideal is consistency. Every KDE app has these things in common. That is the point. If you know how to change key assignments in one app you know how to do it in all of them, including the games. "
    author: "MrGrim"
  - subject: "Re: Welcome screen"
    date: 2007-04-21
    body: "Yes I know. However, I don't think Shortcuts and game controls are the same; you can usually access the shortcuts in other ways, like from menus, but that's not always the case with game controls.\n\nAt least, I think it can be greatly improved (see my complain in previous post). Still I prefer \"traditional\" way of changing and viewing game controls."
    author: "Lans"
  - subject: "who is -Thanomsub Noppaburana- ?"
    date: 2007-04-17
    body: "looks like a top commiter this week after never sleeping Laurent"
    author: "funnyfanny"
  - subject: "Oxygen window decoration?"
    date: 2007-04-17
    body: "Are the second two screenshots using the Oxygen window decoration? IF not, what window decoration are they using?"
    author: "Bill"
  - subject: "Re: Oxygen window decoration?"
    date: 2007-04-17
    body: "I don't believe so, it looks like the default Kubuntu (at least for Edgy) window decoration (don't recall the name for it)."
    author: "Sutoka"
  - subject: "Re: Oxygen window decoration?"
    date: 2007-04-18
    body: "Hmm... It doesn't look like the default in Kubuntu Edgy or Dapper to me."
    author: "Bill"
---
In <a href="http://commit-digest.org/issues/2007-04-15/">this week's KDE Commit-Digest</a>: The Summer of Code begins, with <a href="http://dot.kde.org/1176336589/">40 KDE projects</a>. <a href="http://dot.kde.org/1176397897/">Registration opens for Akademy 2007</a>. <a href="http://dot.kde.org/1176543400/">Hosting proposals invited for Akademy 2008</a>. Further progress in the KBattleship rewrite with sounds and network play integrated, and theming support added to the Bovo game. More work on <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> file analysers. Drag-and-drop and porting work in <a href="http://www.mailody.net/">Mailody</a>. A new CVS plugin for <a href="http://www.kdevelop.org/">KDevelop</a> 4. KSquares moves to kdegames. A new game, Kollision, is imported into playground/games.
<!--break-->
