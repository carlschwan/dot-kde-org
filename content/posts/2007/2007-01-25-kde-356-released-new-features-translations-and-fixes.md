---
title: "KDE 3.5.6 Released with New Features, Translations and Fixes"
date:    2007-01-25
authors:
  - "sk\u00fcgler"
slug:    kde-356-released-new-features-translations-and-fixes
comments:
  - subject: "Thanks!"
    date: 2007-01-25
    body: "Just want to say thank you for your good work. You make so many happy!"
    author: "Mike"
  - subject: "Re: Thanks!"
    date: 2007-01-25
    body: "Yes, KDE is a great project with a good community. I enjoy following the progress. I am looking forward to KDE4."
    author: "Samba"
  - subject: "Re: Thanks!"
    date: 2007-01-26
    body: "Great job folks. I love KDE.\n\nThis is a great project, at the future the next generation will pray for our souls due this great job."
    author: "V. B. from Brazil"
  - subject: "Fedora packages"
    date: 2007-01-25
    body: "KDE-RedHat (http://kde-redhat.sf.net) provides packages for Fedora, Red Hat and CentOS. In fact, this packages has been available since last week (!)."
    author: "Christian"
  - subject: "Re: Fedora packages"
    date: 2007-01-25
    body: "Then be careful. The KDE sources have been updated since last week to fix last-minute issues."
    author: "Thiago Macieira"
  - subject: "Re: Fedora packages"
    date: 2007-01-26
    body: "Updated packages has been released today :)."
    author: "Christian"
  - subject: "Re: Fedora packages"
    date: 2007-01-27
    body: "Thanks to Rex for it!\nfrom a user who still wonders RH does not sponsor the work done by Rex\n"
    author: "ac"
  - subject: "Re: Fedora packages"
    date: 2007-01-31
    body: "I am/will-be integrally involved in Fedora's KDE packaging from F7 onward.  See also: http://fedoraproject.org/wiki/Releases/FeatureFedoraKDE"
    author: "Rex Dieter"
  - subject: "Re: Fedora packages"
    date: 2007-02-13
    body: "The official FC6 packages (maintained by Than Ngo) got updated yesterday."
    author: "Kevin Kofler"
  - subject: "Compiz fix?"
    date: 2007-01-25
    body: "Anyone knowing what the compiz fix is supposed to do, cause both pager and taskbar doesn't work as with kwin in 3.5.6 with Beryl."
    author: "Rocco"
  - subject: "Re: Compiz fix?"
    date: 2007-01-25
    body: "The taskbar and pager now work for me with beryl, when they didn't before. Perhaps you need to upgrade to the latest beryl from svn?"
    author: "Jacob Rideout"
  - subject: "Re: Compiz fix?"
    date: 2007-01-26
    body: "I have installed it in Kubuntu using latest Beryl from Trevi\u00f1o's repository and the taskbar still shows windows from all desktops. Pager was working before KDE 3.5.6 (Kubuntu included a patched pager) so I don't know what does Kicker include to support Compiz (well, I'm using beryl instead)."
    author: "Jose"
  - subject: "Re: Compiz fix?"
    date: 2007-01-26
    body: "I agree that the pager was broken, but the taskbar has always worked under beryl for me..."
    author: "Ben Morris"
  - subject: "A big thanks for KDE folks"
    date: 2007-01-25
    body: "Thanks again for a good work. I enjoyed every release of KDE since its first steps."
    author: "Ignacio Monge"
  - subject: "Kubuntu dapper packages, anyone?"
    date: 2007-01-25
    body: "Has anyone built Kubuntu packages for dapper? I don't quite feel like upgrading to edgy for a while...\n\nthanks,\nrg0now"
    author: "rg0now"
  - subject: "Re: Kubuntu dapper packages, anyone?"
    date: 2007-01-26
    body: "+1!"
    author: "Ljubomir"
  - subject: "Re: Kubuntu dapper packages, anyone?"
    date: 2007-01-26
    body: "I just asked Jonathan Riddell, and he said he doesn't plan to build packages for Dapper.\nHe pointed out that it's quite easy to apt-get source the kde packages and run debuild to compile.\n\nAdd the deb-src for the 3.5.6 edgy archive, and run:\naptitude update\naptitude install devscripts build-essential\napt-get source <package> && apt-get build-dep <package> && debuild <package>*.dsc "
    author: "Quique"
  - subject: "Session Management / Konqueror"
    date: 2007-01-25
    body: "Session Management for browser tabs in Akregator is nice, as Akregator still crashes for me under certain circumstances (certain sites; navigating back and forth..)\n\nNow I only hope for session management in Konqueror (yes I know about profiles, but it's nowhere near a real session behaviour), this could also pave the way for recovering of closed tabs.\n\nSince Konqueror got much faster with many tabs opened, it is on its way to be as usable as Opera for me.. only some finetuning on interface and handling is needed, technical foundation and integration with KDE is already at a high level.\nOnly the somewhat better mouse gestures, clicking on tabs to minimize it, and some other Opera niceties prevent me from using Konqueror as my main browser. A nice plugin interface could work wonders for Konqueror as a webbrowser and raise it's popularity, just like with KDE's servicemenus.\n\nbtw: Does anyone know a way to close tabs with the third mouse button? Couldn't find an option in the preferences.\nCurrently third mouse button drags tabs, while left mouse button copies the link from one tab to another (which is imho quite unexpected and not really often used).\nI would propose to bind (pressed) left mouse button to dragging of tabs and to use the (clicked) third button to close the tab (right button is for the tab options). This is how  it works in Opera and Firefox (by default? not sure). The usability should be much higher."
    author: "Phase II"
  - subject: "Re: Session Management / Konqueror"
    date: 2007-01-26
    body: "Here ya go :\nhttp://wiki.kde.org/tiki-index.php?page=Secret+Config+Settings#id638229"
    author: "Dgege"
  - subject: "Re: Session Management / Konqueror"
    date: 2007-01-26
    body: "Wow, that's a very useful page... I just used 4 tricks, stuff I had always wanted done, but had no idea how to do !\nThanks !"
    author: "chris"
  - subject: "Re: Session Management / Konqueror"
    date: 2007-01-26
    body: "i have a closebutton on the tabs which shows when you mouseover the tab. and i love the drag'n'drop of tab's between and within konqueror windows, why would we want to loose that feature? you can even drag a tab on the kicker or on the desktop so it forms a link to an application. you can drag it to a document and insert the link, or to the run command applet for kicker... \n\nand middlemouse moves tabs, works fine for me...\n\nand you seem to confuse session support in KDE with opera's session support. In KDE, it means if you log out and come back, your session is restored. akkregator didn't support that (but konqueror always has). you can't save a session from a single app and restore it when you restart the app - would be cool, but it's not there yet."
    author: "superstoned"
  - subject: "Re: Session Management / Konqueror"
    date: 2007-01-27
    body: "The close button is in the options, but it takes the website icon away. Opera/Firefox have the close button additionally on the right side of the tab. Would be a good start to do that.\nThe drag and drop of tabs is useful, that's why I think it should be on the left mouse button. Left mouse button currently copies the link from one tab to another so you got the same page twice.. which is like duplicating the tab. I really don't know what that is meant for.\nPerhaps a smart behaviour could integrate both: dragging on the tab pane moves the tab, draggin the tab somewhere else, e.g. to the desktop or a file view copies it. That's also what I do quite often, I just don't think copying the link to another tab is really useful.\nJust in case it was not clear, the proposal was do reassign the different available functions to other input methods (other mouse buttons) so it works more like the other browsers. When work is being done on Konqueror webbroser interface, it will hopefully get streamlined and adjusted to behave like the other browsers (and be configurable with a GUI).\n\nI really was not clear with the sessions and mixed up three different features. KDE-wide session support doesn't help you when Konqueror crashes during a session, so your opened sites are gone.\nThat's why I'm hoping for recovery after crash, reopen (recover) closed tabs (like the trash icon or CTRL-Z in Opera) and management of sessions (save/open opened tabs to reopen them later). The last one is already there behind the profiles, but it's not that obvious."
    author: "Phase II"
  - subject: "Re: Session Management / Konqueror"
    date: 2007-01-27
    body: ">The close button is in the options, but it takes the website icon away\n\nYes, but that was not what parent said. He said \"i have a closebutton on the tabs which shows when you mouseover the tab\". Or if you use the \"official\" and more descriptive name, hover close button. See  http://bram85.blogspot.com/2006/06/hidden-settings-in-kde.html\n\n\n>Konqueror crashes during a session, so your opened sites are gone.\n\nDon't need to be, recovery after crashes has been there for a long time. Open Settings->Configure Extensions select Tools and enable Crashes Monitor. Next time Konqueror crashes, open Tools->Crashes (It's a plugins, so you may need to install it from the kdeaddons package)."
    author: "Morty"
  - subject: "anchors in Konqueror"
    date: 2007-01-26
    body: "I just updated my kubuntu edgy with sources from:\ndeb http://kubuntu.org/packages/kde-356 edgy main\n\nNow the konqi doesn't jump to a given anchor anymore, but always starts on top of the page.\n\nAnybody else with the same problem?\n"
    author: "tracer"
  - subject: "Re: anchors in Konqueror"
    date: 2007-01-26
    body: "If you mean that whenever you click a link konqi will briefly jump to the top of the page before displaying the intended page, then yes I have the same problem."
    author: "cirehawk"
  - subject: "Re: anchors in Konqueror"
    date: 2007-01-26
    body: "No, not only brief.\n\nIt stays the forever.\n\nBut, when I copy the URL, and open a new Konqui, paste the URL, it goes to the right place.\n\nHard to give an example, as it works with a fresh pasted URL. :(\n"
    author: "tracer"
  - subject: "Re: anchors in Konqueror"
    date: 2007-01-29
    body: "Let me guess: It works for visited sites, but not for unvisited ones."
    author: "Leo Savernik"
  - subject: "Re: anchors in Konqueror"
    date: 2007-01-29
    body: "Nope.\n\nIt's really strange.\n\nEven if I reload the page, it won't go to the right place.\n\nJust if I paste the URL into a fresh Konqui, it works.\n\nI'll try do downgrade back to 3.5.5"
    author: "tracer"
  - subject: "Re: anchors in Konqueror"
    date: 2007-01-28
    body: "I do have the same irritating problem."
    author: "me too"
  - subject: "Thanks for mail templates"
    date: 2007-01-26
    body: "Thanks for mail templates. This is an incredibly time-saving feature. I write daily about 100-200 mails with the BAT, most of them in under 2 seconds. Thanks to what BAT calls \"Quick Templates\", for offers, standard answers or customer information."
    author: "Stefan"
  - subject: "Yeeeeaaaahhhh"
    date: 2007-01-27
    body: "Hmmmm. Is that known as spam? If so, please stay with the BAT. It is one of the items that I filter out as it is the main one for sending spam (other than outlook). "
    author: "a.c."
  - subject: "Re: Thanks for mail templates"
    date: 2007-01-28
    body: "The changelog mentions a template language.  Where would I find documentation on this so I can learn how to utilize them?"
    author: "cirehawk"
  - subject: "Kontact"
    date: 2007-01-26
    body: "I've recently been trying to use kontact has my personal information manager, and am unable to send e-mails using my gmail account, I've followed the instructions in gmail help site with all the correct port, username (with @gmail.com) TSL (or something called like that), but for some reason  I never can send an email, only receive.\n\n\nThank you so much"
    author: "dan"
  - subject: "Re: Kontact"
    date: 2007-01-26
    body: "Same problem here with some mail browsers. Though I can use my educational email account and prentend being somebody else... Wonder if I went wrong in configuration. It always sayd \"no connection to computer mail.xxx.de\"."
    author: "A/C"
  - subject: "Re: Kontact"
    date: 2007-01-26
    body: "Best place for this kind of comment / question is on the kdepim-users@kde.org mailing list, which is specifically for user questions about KMail and Kontact and whatnot."
    author: "Adriaan de Groot"
  - subject: "Re: Kontact"
    date: 2007-01-28
    body: "I have it set-up and working here.\n\nGo to: Settings > Configure KMail... > Accounts > Sending > Add > SMTP\n- General Tab -\nName: Any name you want\nHost: smtp.gmail.com\nPort: 25\n\n[x]Server requires authentication\nLogin: Your full email address\nPassword: Your password\n[x]Store SMTP password\n\n- Security Tab -\n(x)TLS\n(x)PLAIN\n\nThat's it. You can make it default if you want, to make sure it is used when you sent a new mail."
    author: "Hellblade"
  - subject: "Re: Kontact"
    date: 2007-02-02
    body: "Well, it's still not working but thanks for the help, I'll contact the emails put a bit before here.\n\nDan"
    author: "dan"
  - subject: "..."
    date: 2007-01-26
    body: "thank you! kde is great!"
    author: "martin"
  - subject: "Slackware packages"
    date: 2007-01-26
    body: "Sorry guys,I will not release packages for Slackware this time.\nNo time, and no computer.\n\n"
    author: "JC"
  - subject: "Great!"
    date: 2007-01-26
    body: "Looking good.  Everything seems a little snappier for me, but that may be just my enthusiasm.  Mail templating is a good idea.\n\nNow to really improve Konqueror one method is to go and browse Digg and find out what it is that slows my machine to a crawl whenever I'm on there.  :)"
    author: "Steve"
  - subject: "Re: Great!"
    date: 2007-01-27
    body: "adding digg.com/img/main-back.gif to konquerors adblock filters list improves scrolling in digg.\n\ntaken from http://element14.wordpress.com/2007/01/05/tweaks-for-speed-in-konqueror-ipv6-and-diggcom-tweaks/\n\n"
    author: "dr"
  - subject: "signing Kmail messages"
    date: 2007-01-27
    body: "well, I have been trying to sign my sending messages with Kmail but it never worked. WITHOUT asking for any password in order to use appropriate keys, it is always showing the message \"wrong pass-phrase\". I have both the pinentry and pinentry-qt installed. I am using FC-6.\n\nPlease suggest me if there is some workaround!\nThanks!!\n"
    author: "chandra"
  - subject: "Re: signing Kmail messages"
    date: 2007-01-27
    body: "Two things that you should check: \n\n1. Is the gpg agent started, and in a fashion that the KDE programs can \"see\" it?  \n\nThe variable GPG_AGENT_INFO has to be set correctly, you can check this using the following command from a konsole window: \nexport | grep GPG_AGENT_INFO\n\nYou should see a line like this if everything is correct: \ndeclare -x GPG_AGENT_INFO=\"/tmp/gpg-AhIqwl/S.gpg-agent:3526:1\"\n\n(the actual value is different every time the agent is started)\n\nFor a complete KDE session a good place to start the daemon is ~/.kde/env/gpg-agent.sh . Here are the contents of my file: \n\n#!/bin/bash\neval \"$(gpg-agent --daemon)\"\n\n\n2. You said that pinentry-qt was installed.  But is the gpg-agent configured correctly so it finds it?\n\nFrom my ~/.gnupg/gpg-agent.conf:\npinentry-program /usr/bin/pinentry-qt\n[...]\n\nMake sure the path you have in there is correct for your setup.\nTyping \"which pinentry-qt\" may help you here.\n\n"
    author: "cm"
  - subject: "Re: signing Kmail messages"
    date: 2007-02-06
    body: "pinentry-qt on my kubuntu edgy crashes ( a dialog \"flashes\" then disappears ). On googling this bug seems to be known for years but somehow remains. You may want to try \npinentry-program /usr/bin/pinentry-gtk-2\ninstead."
    author: "pacesie"
  - subject: "Re: signing Kmail messages"
    date: 2007-02-06
    body: "My bad. On checking again, pinentry-qt works fine. "
    author: "pacesie"
  - subject: "Re: signing Kmail messages"
    date: 2007-02-06
    body: "... and has for years for me.  I had problems back when the GPG support in KMail was still in early development.  But not now."
    author: "cm"
  - subject: "And KDevelop?"
    date: 2007-01-27
    body: "Why there are no references to KDevelop?\nThis news, the KDE announcement and the KDE changelog, all does not mention KDevelop. Why?\nIt should have been upgraded from 3.3.x releases to 3.4.x, with _a lot_ of new features and bug fixes.\n\nThe main ones are:\n- much better code-completion\n- better debugger support\n- improved support for Qt4\n- much enhanced support for qmake projects\n- improved C++, Ruby and PHP support\n- changed default User Interface\nFor a better list, see http://www.kdevelop.org/index.html?filename=3.4/features.html\n\nIt's a very big change!\nWhy it was forgot?\nProbably there is a reason fot this, does someone knows it?\n\nBy the way, also KDevelop main page (www.kdevelop.org) does not mention the new release, only the changelog page has been updated (www.kdevelop.org/index.html?filename=3.4/changes.html)."
    author: "Alessandro"
  - subject: "Re: And KDevelop?"
    date: 2007-01-27
    body: "I guess the reason is simple:  KDevelop is not part of KDE 3.5.6.  It's separate.  \n\nBut here's the release announcement: http://dot.kde.org/1169902218/"
    author: "cm"
  - subject: "Re: And KDevelop?"
    date: 2007-01-27
    body: "Why then there is a empty reference to it in the KDE changelog?\nhttp://www.kde.org/announcements/changelogs/changelog3_5_5to3_5_6.php#kdevelop\nAlso the link \"all SVN changes\" shows almost nothing."
    author: "Alessandro"
  - subject: "Re: And KDevelop?"
    date: 2007-01-27
    body: "Ah, I see what you mean.  The changelogs for 3.5.5 and 3.5.4 didn't have that section at all though.  Dunno what that means. \n\nAnyway, you have the link to the announcement now. IMHO it highlights the changes in kdevelop better than a subsection in the KDE 3.5.6 announcement would have. \n\n"
    author: "cm"
  - subject: "Re: And KDevelop?"
    date: 2007-01-27
    body: "Yes, it's better a dedicated announcement!\nWhen I wrote my first message, I hadn't seen it yet.\n\nRemains the strange section in the KDE changelog and the little list of changes in the SVN changes.\n\nSo KDevelop and KDE come out in the same day, but are separated. They could release KDevelop also in different dates!\n\nAnyway, thank you for the answers!\n\nAlessandro"
    author: "Alessandro"
  - subject: "Re: And KDevelop?"
    date: 2007-01-27
    body: "Moreover all past releases of KDevelop, come out with KDE."
    author: "Alessandro"
---
The <a href="http://www.kde.org/">KDE project</a> today <a href="http://kde.org/announcements/announce-3.5.6.php">announced the immediate availability of KDE 3.5.6</a>, a maintenance release for the latest generation of the most advanced and powerful <em>free</em> desktop for GNU/Linux and other UNIXes. Significant features include additional support for <a href="http://www.go-compiz.org/">Compiz</a> as a window manager with Kicker, session management for browser tabs in <a href="http://akregator.kde.org">Akregator</a>,
  templating for <a href="kmail.kde.org">KMail</a> messages, faster frame rates with video chat in <a href="http://kopete.kde.org">Kopete</a> and new summary menus for 
  <a href="http://kontact.kde.org">Kontact</a> making it easier to work with your appointments and to-dos.




















<!--break-->
<p>
  This release includes a number of bugfixes for KHTML, <a href="http://kate.kde.org">Kate</a>, 
  Kicker, KSysGuard and lots of other applications.  Translations continue as well, with the most improved coverage coming from the <a href="http://l10n.kde.org/team-infos.php?teamcode=gl">Galician</a> translation team.  KDE now
  supports 65 languages, making it available to more people than most proprietary   software and can be easily extended to support others by communities who wish
  to contribute to the open source project.
</p>

<p>
  For a more detailed list of improvements since
  <a href="http://www.kde.org/announcements/announce-3.5.5.php">the KDE 3.5.5 release</a>
  last October, please refer to the
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_5to3_5_6.php">KDE 3.5.6 Changelog</a>.
</p>

<p>Packages are available for <a href="ftp://ftp.archlinux.org/extra/os/i686">ArchLinux</a>, Debian Experimental, <a href="http://kubuntu.org/announcements/kde-356.php">Kubuntu</a>, <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.6/Pardus/">Pardus</a>,  <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/">openSUSE</a> and several other distros.  See the <a href="http://kde.org/info/3.5.6.php">3.5.6 info page</a> for the source or use <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> to compile it yourself.</p>
















