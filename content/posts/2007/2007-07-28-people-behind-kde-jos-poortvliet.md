---
title: "People Behind KDE: Jos Poortvliet"
date:    2007-07-28
authors:
  - "dallen"
slug:    people-behind-kde-jos-poortvliet
comments:
  - subject: "About Jos..."
    date: 2007-07-28
    body: "Apart from conducting this interview, i've met Jos in person - we spent a lot of time in Glasgow working on Akademy articles, and Jos very committed, enthusiastic and professional in his work. It was a real pleasure to meet him!"
    author: "Danny Allen"
  - subject: "Re: About Jos..."
    date: 2007-07-28
    body: "I second that!"
    author: "Ariya"
  - subject: "Re: About Jos..."
    date: 2007-07-28
    body: "you're one hell of a writer yourself, Danny ;-)\n\nI said to him (and others must have said that before as well, I'm sure) he should fill in the questions himself, and get it online - after all, dear Danny might very well be the brightest star on the promotional sky at KDE :D\n\nSo, everybody who's with me on this, help me persuade him!"
    author: "jospoortvliet-former-superstoned"
  - subject: "what's with the changed name?"
    date: 2007-07-28
    body: "why not superstoned anymore? did you get sued for it like puff daddy in the UK?"
    author: "Patcito"
  - subject: "Re: what's with the changed name?"
    date: 2007-07-28
    body: "na, the time has come to start looking for a job, and I'd rather have a potential googling employer not to find references to weed and such all over the web ;-)\n\nOf course, nothing can be done about that (the web doesn't forget), but in time, the connection will loosen a little and they might consider it co-incidental or outdated...\n\nYes, it sucks. I've also cut my hair, I look very corporate now :("
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: what's with the changed name?"
    date: 2007-07-28
    body: "My embarrassing moment is that when I see you in real life I know you only as \"superstoned.\"  So I support 100% people having nicks related to their real name, makes life for folks like me, who can't remember names very well, twice as easy."
    author: "Ian Monroe"
  - subject: "Re: what's with the changed name?"
    date: 2007-07-28
    body: "I know what you are talking about... Not all companies are like that - that being said, you still don't want to reduce the set of possible employers unnecessarily. Of course."
    author: "the other guy"
  - subject: "Re: what's with the changed name?"
    date: 2007-07-28
    body: "Sure, not all companies are like that. But in the business I might end up in, they are ;-)\n\nRemember, I'm no IT guy..."
    author: "jospoortvliet-former-superstoned"
  - subject: "Re: what's with the changed name?"
    date: 2007-07-29
    body: "Maybe you should remove the magic mushrooms background and rename wietbak also?"
    author: "Debian"
  - subject: "Re: what's with the changed name?"
    date: 2007-07-30
    body: "Background? On the picture? I was in a zoo ;-)\n\nAnd yeah, wietbak might have to change as well ;-)"
    author: "jospoortvliet"
  - subject: "Jos has changed my life !!!"
    date: 2007-07-28
    body: "at least my experience with kde ( the information about opensuse was really invaluable), and i would like to thank you for the akademy reports, they were really very informative.\n\nand sorry for your girlfriend, don't worry u'll get another ;-)\n\nfriendly "
    author: "djouallah mimoune"
  - subject: "Re: Jos has changed my life !!!"
    date: 2007-07-28
    body: "Wow, that was more than I could ever hope for ;-)"
    author: "jospoortvliet-former-superstoned"
---
For the next interview in the <a href="http://behindkde.org/">People Behind KDE</a> series, we travel to the Netherlands to meet a KDE promoter and meeting organiser, someone who helps the international community to experience KDE events, even if they were not in attendance - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/jos/">Jos Poortvliet</a>.
<!--break-->
