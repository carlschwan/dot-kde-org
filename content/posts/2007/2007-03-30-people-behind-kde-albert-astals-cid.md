---
title: "People Behind KDE: Albert Astals Cid"
date:    2007-03-30
authors:
  - "dallen"
slug:    people-behind-kde-albert-astals-cid
comments:
  - subject: "RocK"
    date: 2007-03-29
    body: "Your program rocks. :)\n\nNice interview, it's always fun to know the people behind KDE better."
    author: "Lans"
  - subject: "Re: RocK"
    date: 2007-03-30
    body: "Your program rocks. :)"
    author: "zvonsully"
  - subject: "Your program rocks!"
    date: 2007-03-30
    body: "nice interview :-)"
    author: "MK"
  - subject: "Scrabble!!!"
    date: 2007-03-30
    body: "I can't wait for the Scrabble game (I think it's called Kombination?).  Scrabble is a favorite of mine.  It will be great to play it over the home lan with my son!  Keep up the great work, Albert!  Thanks also to Danny for keeping the news and interviews flowing."
    author: "Louis"
  - subject: "Funny :-)"
    date: 2007-03-30
    body: "As usual, a very nice interview. And yes, kpdf really rocks - it's one of the most useful program in the last few years!\n/goran j"
    author: "G\u00f6ran Jartin"
  - subject: "Hi Albert"
    date: 2007-03-31
    body: "Hi Albert! KPDF is great... By the way, you are a cutie!"
    author: "AC"
---
For the next interview in the fortnightly <a href="http://www.behindkde.org/">People Behind KDE</a> series we travel to Spain to meet a focused developer who does not go off at a tangent.  Someone with flashes of brilliance you may miss if you <a href="http://edu.kde.org/blinken/">Blinken</a>. With plans in abundance, tonight's star of People Behind KDE is <a href="http://kpdf.kde.org/">KPDF</a> maintainer <a href="http://www.behindkde.org/people/aacid/">Albert Astals Cid</a>.



<!--break-->
