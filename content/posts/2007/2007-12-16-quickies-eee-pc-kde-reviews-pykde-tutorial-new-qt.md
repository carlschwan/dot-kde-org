---
title: "Quickies: Eee PC, KDE Reviews, PyKDE Tutorial, New Qt"
date:    2007-12-16
authors:
  - "jriddell"
slug:    quickies-eee-pc-kde-reviews-pykde-tutorial-new-qt
comments:
  - subject: "GPL Exception"
    date: 2007-12-16
    body: "So the CDDL gets added to the GPL Exception, and the Eclipse Public License still hasn't been?  What's the deal, where's the holdup?"
    author: "David Walser"
  - subject: "Re: GPL Exception"
    date: 2007-12-16
    body: "Isn't the Eclipse license a BSD like license? In that case it will never get added, just like the lgpl will never get added.  (oh, and if you need to ask why, you haven't thought about it long enough)"
    author: "T"
  - subject: "Re: GPL Exception"
    date: 2007-12-16
    body: "http://trolltech.com/products/qt/gplexception/?searchterm=GPL%20Exception\n\nEclipse Public License is one of the few popular licenses not on the exception list actually, BSD MIT and LGPL are all present."
    author: "Skeith"
  - subject: "Re: GPL Exception"
    date: 2007-12-16
    body: "No, in fact, the EPL is a newer version of the Common Public License (the difference between the two being very minor), and the CPL is in the GPL Exception!"
    author: "David Walser"
  - subject: "Re: GPL Exception"
    date: 2007-12-16
    body: "Did you RTFException?\nhttp://doc.trolltech.com/4.3/license-gpl-exceptions.html\n\"Eclipse Public License  1.0\""
    author: "Kevin Kofler"
  - subject: "Re: GPL Exception"
    date: 2007-12-16
    body: "Nice catch, that wasn't there yesterday.  That's very good news."
    author: "David Walser"
  - subject: "Re: GPL Exception"
    date: 2007-12-17
    body: "Wow... does this mean that SWT could finally get a Qt backend?"
    author: "chris.w"
  - subject: "Re: GPL Exception"
    date: 2007-12-17
    body: "No, it means that bringing up licences as an excuse will finally not be possible any more.\n\nThe QPL, one of the licences of Qt/X11 has always made it possible to do an SWT backend implementation, but it has been a convenient excuse to \"forget\" this option and just point out the incompatability with the GPL option."
    author: "Kevin Krammer"
  - subject: "Re: GPL Exception"
    date: 2007-12-17
    body: "I had waited for something like this for a long time, but the wait was so long (and is unlikely to be over, since an actual SWT/Qt would still have to be coded and released) that I've switched to QtJambi.  It gets me that native LAF I missed in KDE, but is nicely cross platform as well, completely obviating my need for SWT.  \n\nI'd still like to see it for those apps that are written with SWT, but I have no need of it for future projects of my own."
    author: "MamiyaOtaru"
  - subject: "frak facebook, frack orkut!"
    date: 2007-12-16
    body: "(off topic)\nFacebook and Orkut are just massive scams to collect data.\nIt is funny (no, it's actually scary) how people are willing to give up so much information about themselves and friends to advertisers and spammers (Orkut->Google, Facebook -> Tucows. etc)\n\nGet a life, waste less time on \"social\" networks! \n\n"
    author: "orga"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-16
    body: "Sounds like someone has been watching Battlestar Galactica ;)! (\"Frak\" was used to death in that series)\n\nAnyway, as biased as your post may seem, I totally agree with you.  All the passive reading I've done on the web (and in magazines like Wire) seems to confirm this.  I don't trust big corporations, no matter how noble or moral some actions may seem, any company is gonna want something in return for these actions..."
    author: "Link"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-16
    body: "Why scams? Yes they collect data, and in return they provide a service. Actually a pretty damned good one. Social networks are part of life, just like email, they make it easier to keep in contact with people you know. I am more than willing to let them know which music and books I like, I trust them email as much as I trust Google, and yes they also know the city where I live in, Amazon and a bunch of other online shops know my full address. What is the big deal..."
    author: "KBX"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-16
    body: "yeah... the big deal is ..putting all the little pieces of the puzzle together into one big picture... the big deal is __to combine__ the data. Your order some meds online, you leave traces in social networks, you order books online, you store your mail on gmail. As much as we don't feel it right now we're shifting from consumers/buyers to \"targets\"."
    author: "Thomas"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-16
    body: "I think the correct word is \"mark\"."
    author: "reihal"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-16
    body: "At some point, there will be a time where many people will get some backlash of putting so much of themselves publically. Of course, they'll complain but that'll be too late and honestly, the only ones they should blame in that case is themselves. There has already been reports of people who were discarded from jobs because of some things they did put on Facebook and like. You may feel sour about the way companies may have disregarded you, but in the end, you shouldn't have made public information that may threaten you in the first place.\nMoreover, democracy is a moving target and no one can assure that any democratic country won't go a tyranny at some point in the future. The Weimar Republic was a rather good democracy and still, the Nazis got to power within it. You never know what use could be made of any government / company of the information you put on display on these websites.\nInternet is a public place, not a private living room. Too many people actually forget about that and will learn it the hard way, I think.\nMail adresses are perfectely enough to keep contact with friends. I don't understand the need for these social networks."
    author: "Richard Van Den Boom"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-17
    body: "The Weimar republic was nowhere near being a good democracy. It was terribly immature. There were hundreds of rivaling parties, and worst of all, it was a constant battle between extreme ideologies. If the Nazis wouldn't have won, some other Fascists or Marxists would have. Well, probably every other option than the Nazis would have been the lesser, but the chances for a true, lasting and stable democracy under post-WWI conditions were extremely low to begin with.\nJust my \u0080 0.02 :-)"
    author: "chris.w"
  - subject: "Weimar"
    date: 2007-12-18
    body: "Nah. You are basing this entirely on extrapolation backwards from the known outcome. It is the kind of wishful thinking you see on e.g. a skydiving forum. As soon as an accident is reported everyone wants to know what exact make the equipment was. This is a perfectly legitimate question, except that some people, mostly those new to the sport, only want to be told that the equipment was different from their own, so that they can convince themselves that it couldn't happen to them and continue to feel safe.\n\nI agree with grandparent that Weimar was perfectly good as democracies go. From a political perspective it was certainly better than certain stale, contemporary two-party systems. The problems were more in the economy and in the sociological situation. But let's not blame the outcome on Weimar, or even on the Communists. (That was already done, by you-know-who.) And most of all, let's not convince ourselves that everything was different back then, because Weimar was \"immature,\" and that such things could therefore not happen in the \"modern\" world."
    author: "Martin"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-17
    body: "Some may well be putting their lives on these networks - others use them to co-ordinate social events.  Idiots do idiot things. \n\nThings my profile doesn't have:\n\nbank account\nsocial security number\nwhere I work \nhome address \nmother's maiden name \n\nThings my profile does have:\n\na few private invitations to parties\na few private photos of previous parties\nlots of photos of black helicopters :)  \n\n\n"
    author: "Gerry"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-17
    body: "Yeah stop being paranoid. Unless you are stupid and allow everyone to see your profile, then there is little risk of people finding out secret information. Besides apart from your data of birth, and perhaps address and phone number there isn't really any useful information there anyway!\n\nNow consider how many online shops you have given your address, date of birth, credit card details, etc. etc. to. I think you just want to seem superior in your non-use of facebook.\n\nOn the other hand, many people have social lives and find it useful and fun to use facebook to organise events, look at photos of people, and basically stalk your friends. If the stupid applications/idiot-spam* continues I may have to leave it though. Why oh why did they open it up to everyone?!\n\n*Trying to think of a good name for that 'send this to 10 people' spam. Anyone?"
    author: "Tim"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-18
    body: "Idiot spam is good. ;) I do usually have a dozen or so \"application notification\" things that I clean up once every couple of months. \n\nLuckily Facebook does categorize everything really well, so for instance I've never gotten any spam on the Facebook Messaging system. Which is really just amazing given that any Facebook user can message me, they must have sophisticated spam-fighting measures to keep spam bots from signing up."
    author: "Ian Monroe"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-18
    body: "<i>Now consider how many online shops you have given your address, date of birth, credit card details, etc. etc. to. I think you just want to seem superior in your non-use of facebook.</i>\n\nI don't know how you're handling this, but for me there applies a simple rule... i don't give an online shop personal information that isn't actually needed. If amazon for example knows my adress and name that's enough to get the book or CD or whatever i bought to my house. I pay per bank transfer and that's it. Why would you provide them with correct date of birth, credit card and other stuff they don't need? Don't tell me you also answer all these questions about household income, personal interests and so on, too."
    author: "Marc"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2007-12-23
    body: "I see your point (although off topic) and there are many ways the targeting you talk about to be blocked.\n\nI think that the only way to prevent such \"social\" networks, and huge information aggregators from targeting you (even if you use them) to behave illogically, here are few ideas how:\n* click on random banner/ad in a new tab (then close the tab) - this way those random \"buyers\" of you as a target will have no ROI\n* don't click any banner/ad\n* if you find some banner/ad interesting then instead of clicking the ad, search it on yahoo or google\n* spread the word, tell your friends to behave this way to avoid becoming victims of targeting\n* don't provide information, don't participate in polls etc.\n* use different systems (social networks / search engines)\n* use popup blockers, ad blockers, flash blockers etc (e.g. adblock plus)\n* anything else you can guess that can mess up the \"logical\" user behavior\n\nAll this systems are based on statistics, the more you mess up their stats, the less ROI their advertisers will get. In the end if you can just join \"for pay\" service where you will never get annoyed or targeted from any demographically or statistically based popups, ads or flashes.\n\nHappy Holidays!\n\nPS: Is there something like adblock plus (firefox addon) for konqueror?"
    author: "Anton Velev"
  - subject: "Re: frak facebook, frack orkut!"
    date: 2008-01-02
    body: "> PS: Is there something like adblock plus (firefox addon) for konqueror?\n\nYes. Newest Konqueror has AdBlocK under Settings->Configure Konqueror->AdBlocK filters. Check the \"Enable filters\".\n"
    author: "Michael"
  - subject: "Koffice"
    date: 2007-12-17
    body: "KWord 2 looks real nice.\n\nWill be interesting to try it out."
    author: "Axl"
  - subject: "A Good KDE Distro? Recommendations, please!"
    date: 2007-12-19
    body: "This is just a general question for KDE people not really related to today's articles on the \"Dot\"...  \n\nI am a long-time GNOME user looking to move to KDE.  I won't mention the reasons why here, as I definitely do not want to start a flame war.  In the past, I have always used Fedora as my distro of choice and it is an excellent GNOME-based distro.  However, most people seem to think that the implementation of KDE on Fedora is far from the best available. Since I haven't used other distros (at least, not for years) I don't really have a yardstick to go by.\n\nI would therefore like to ask what distro most KDE people use.  Which distro has the best default installation of KDE?  I must confess that I started to play with OpenSuSE a few nights ago and wasn't really that impressed by it, despite its good reputation.  Not to be too negative but I really could not take to the Start Menu and the distro seemed to be lacking a lot of tools needed whilst others did not work (for example, kppp was missing but there was another network tool which did not agree with my modem). I obtained OpenSuSE from a magazine cover DVD, so perhaps not all the software was on there???\n\nI am really only looking for a general purpose distribution.  I would prefer something I can download from FTP in ISO format. I do not want a commercial distro that I have to pay for.  I would class my Linux experience level is \"moderate\".  My only real pre-requisite is that it have excellent hardware support and configuration tools.\n\nAny suggestions?  As I said before, I do not want to start a flame war, so please do not be too critical or condemnatory of other distros.  Only positive suggestions are needed!  Cheers and thanks in advance."
    author: "Kerr Avon"
  - subject: "Re: A Good KDE Distro? Recommendations, please!"
    date: 2007-12-19
    body: "Well, I predict you will get told to ask this on another list/forum but don't take it personally.  I too, am a relative newcomer but I have to say that I would vote for openSuse.  It has good configuration tools (Yast) and I would say the most polished and cutting-edge packages (eg for kde4 and backports)  this is mostly due to several of the higher-ups being prominent kde code contributors. Regarding your dislike of that menu (I like it) you can just right-click on it to switch it to a more traditional tree view menu.  And if you can get hold of the DVD version of 10.3 you should have all the packages you'll need and can usually get the rest from their build service (http://software.opensuse.org/search?baseproject=openSUSE:10.3). I'll stop now lest I be accused of being an eevil MS-lovin novell astroturfer.  \n\nNote I found kubuntu to be in many ways just as good but the default desktop was a little bit less 'sharp' and they annoyed me by being petty and removing the terminal shortcut from konqueror. On the other hand 'buntu seems to be popular and has healthy forums.  I have not (but plan to) tried fedora but hear good things about it and I believe it also has up to date kde packages which are actually a focus of the project going forward (I think I read?).\n\nI would expect all the latest distros to have very similar levels of hardware support so I would get some livecds and play with them over a free weekend.\n\nGood luck."
    author: "anon"
  - subject: "Re: A Good KDE Distro? Recommendations, please!"
    date: 2007-12-19
    body: "I use Mandriva right now for KDE with great success, however there are a great number of good KDE distros. Immediately, I think of OpenSuse, PCLinuxOS, Kubuntu, and even the Fedora KDE-spin is fairly well received. I'd even recommend Slackware if you're a power user that just wants a \"virgin\" KDE :) \n\nThere are many good ones out there too if you are looking for something that is more localized, depending on your language: for example Red Flag Linux is a KDE distro that is popular in China; there's ALT Linux in Russia; in India there are several good KDE distros too. These distros might give you a better experience in your language, depending on where you are from...\n\nOf course, most BSD's also have pretty good KDE support (at least in 3.x, I'm not sure how well 4.x is doing in the *bsd world these days...)\n\nHave fun :)"
    author: "Troy Unrau"
  - subject: "Re: A Good KDE Distro? Recommendations, please!"
    date: 2007-12-19
    body: "> However, most people seem to think that the implementation of KDE on Fedora\n> is far from the best available.\n\nDon't believe these people, stay on Fedora. :-)\n\nNo matter what rumors run around (mostly due to old gripes about Red Hat Linux 8 which are entirely obsolete these days), we're hard at work bringing you a nice KDE experience, see:\nhttp://fedoraproject.org/wiki/SIGs/KDE\nFor Fedora 9, expect KDE 4.0 with Plasma, composite-enabled KWin and all the other KDE 4 goodness. On Fedora 8, we have KDE 3.5.8 and kdepim-enterprise, and we'll bring you further upgrades from the 3.5 branch as official updates (not in some backport repository like most other distributions)."
    author: "Kevin Kofler"
  - subject: "Re: A Good KDE Distro? Recommendations, please!"
    date: 2007-12-19
    body: "Well, I really appreciate the addition of Scribus recently but why was k3b dropped from the installation disk?  It is painful for those of us who only have dial-up internet access to obtain it from a repository.\n\nAlso are there more KDE packages available on the new KDE live spin then there are on the \"main\" Fedora DVD?\n\nThanks."
    author: "Kerr Avon"
  - subject: "Re: A Good KDE Distro? Recommendations, please!"
    date: 2007-12-20
    body: "> why was k3b dropped from the installation disk?\n\nThe DVD contents are now determined differently. Prior to Fedora 7, it was easy to determine: there was Core and there was Extras, the DVD simply contained everything in Core and nothing from Extras. Now that we have one big collection, the contents of the DVD have to be actually specified. The problem is that K3b is listed as an \"optional\" rather than a \"default\" component (\"optional\" means \"optional and not installed by default\", \"default\" means \"optional, but installed by default\") of the \"Sound and Video\" group, so it isn't dragged in automatically by the reference to that group, nor is it listed explicitely in the DVD compose configuration. I'll ask the folks responsible for the DVD if K3b can be made available on the DVD in future releases (Fedora 9 and later).\n\n> Also are there more KDE packages available on the new KDE live spin then\n> there are on the \"main\" Fedora DVD?\n\nYes, there are more KDE packages on the KDE live spin than on the DVD. K3b is included on the KDE live spin."
    author: "Kevin Kofler"
  - subject: "Re: A Good KDE Distro? Recommendations, please!"
    date: 2007-12-20
    body: "I just asked on the fedora-devel-list about K3b on future DVD spins:\nhttps://www.redhat.com/archives/fedora-devel-list/2007-December/msg01283.html"
    author: "Kevin Kofler"
  - subject: "Re: A Good KDE Distro? Recommendations, please!"
    date: 2007-12-20
    body: "Thanks for doing that for me.  On forums like OSNews.com, in the past, I have read some people citing k3b as THE \"killer\" KDE application, so it was strange to see it dropped from the install DVD.  It would be great to have it back. \n\nIt is excellent that Amarok, Digikam and Scribus are now on the DVD though. As I said, my main problem is I am on a slow network connection so downloading things from repositories is a problem. However, I think for other people, especially Linux newcomers, the DVD should showcase the best applications available and cover a wide range of areas as first impressions do count. Therefore, if we show them applications like Scribus, without them needing to fossick in the respositories to find them, they can see the potential for Linux as a desktop publishing system straightaway.  Although Scribus may not yet be of the standard of professional industry level applications like Adobe or Quark at least it covers home user needs, so that is one more niche that desktop Linux has covered.\n\nThanks again for your response and following this up on your mailing list for me.  Great to see a community that listens to the needs and concerns of humble users like myself!  :-)"
    author: "Kerr Avon"
  - subject: "Re: A Good KDE Distro? Recommendations, please!"
    date: 2008-01-02
    body: "The best KDE distribution I've tested is Kubuntu. It comes with a good set of packages, good hardware detection, and a good community."
    author: "Michael"
  - subject: "also,"
    date: 2007-12-19
    body: "try out Mandriva, this distribution doesn't get as much attention as a few years ago, but it's stable, it's user-friendly, it has a nice community and a huge repository, it's available for for free and last but not least, it has top notch KDE integration.\n\nGreetings...\n"
    author: "ypsilon"
  - subject: "Re: also,"
    date: 2007-12-19
    body: "this should have been a reply to \"Kerr Avon\" actually...."
    author: "ypsilon"
  - subject: "Re: also,"
    date: 2007-12-19
    body: "Thanks to all of you for your thoughts. I'd prefer not to use Slackware, though I have heard it is extremely stable.  Since it has only one maintainer, do they have any contingency plan if something happens to him? Also, I'd probably opt for something a little more user-friendly at this stage.\n\nKubuntu is tempting but I am not sure if it would have a wide-enough range of packages on the install disk.  I am only on dial-up in my location, so downloading from online repositories is a pain in the rump. \n\nI might play with Open SuSe a bit more and also look at Mandriva.\n\nI am definitely interested to hear the Fedora person respond to my extra questions that I posted above. Yes, if they provide good KDE support, then there is little point in moving to another distro as they have good community support and excellent hardware detection (at least for my set-up).  \n\nThanks again."
    author: "Kerr Avon"
  - subject: "Re: also,"
    date: 2007-12-30
    body: "<I>\nThanks to all of you for your thoughts. I'd prefer not to use Slackware, though I have heard it is extremely stable. Since it has only one maintainer, do they have any contingency plan if something happens to him?</I><P>Not really; I imagine someone would probably step up though. You might want to look at vector linux, a derivative maintained by more of a community. (But I very much recommend actual slackware)"
    author: "m50d"
---
LinuxLinks has another <a href="http://www.linuxlinks.com/article/20071204141457291/Asus-701-Introduction.html">review of the KDE using, Eee PC</a>. *** ZDNet <a href="http://resources.zdnet.co.uk/articles/tutorials/0,1000002006,39290839,00.htm">introduces the KDE 3 desktop</a>. *** EarthWeb IT Management <a href="http://itmanagement.earthweb.com/osrc/article.php/12068_3709846_1">looks at KDE 4 Beta 4</a> while Ars Technica <a href="http://arstechnica.com/news.ars/post/20071213-afirst-look-at-kde-4-0-release-candidate-2.html">takes a first look at RC 2</a>. *** Logs are available from <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Kubuntu Tutorials Day</a> including an introduction to PyKDE 4. *** Trolltech <a href="http://trolltech.com/company/newsroom/announcements/press.2007-12-05.0846999955">released Qt 4.3.3</a>. *** Support your favourite open source organisations on Facebook. <a href="http://www.facebook.com/profile.php?id=6344818917">KDE</a> and <a href="http://www.facebook.com/profile.php?id=5927854727">Amarok</a> are now on facebook - tell your friends!






<!--break-->
