---
title: "People of openSUSE Bring You Stephan Binner"
date:    2007-10-29
authors:
  - "rjohnson"
slug:    people-opensuse-bring-you-stephan-binner
comments:
  - subject: "nice interview =)"
    date: 2007-10-29
    body: "it's classic Binner, the guy we all know and love in the community. favourite quotes: 'An AQAM (automatic questionnaire answer machine).' and 'I would answer &#8220;kded&#8221; - serving mostly in the background but without it much would not work.' sums up a lot about Stephan right there ...\n\nhe does a lot of great things in the community and at SUSE; i think he deserves a bit more recognition than he gets. his distro-patches-to-kde extraction project and the kde live cd's alone are huge, let alone his code contributions. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: nice interview =)"
    date: 2007-10-29
    body: "Fully ack. Binner is doing a great job, and his livecd's alone are probably a big source of testers for KDE 4. I'm going to specifically mention them in the next release announcement, lets hope ppl take it up and start helping report bugs."
    author: "jospoortvliet"
  - subject: "Re: nice interview =)"
    date: 2007-10-29
    body: "Yes indeed, a nice interview. I haven't had the opportunity of meeting Stephan in real life yet, but I think I do like his sense of humour. His family on the sofa, that's hilarious!\n\nAnyway, I've only recently switched to openSUSE, and I'm rather pleased with the work they've done with KDE. In my previous distribution (guess which), I was using plain KDE compiled from source, and while everything worked okay, it was indeed rather unpolished when compared to the openSUSE setup. Thanks for the good work!"
    author: "Just me"
---
"<em>Born last millenium</em>", KDE and openSUSE's very own <a href="http://news.opensuse.org/?p=476">Stephan Binner gets interviewed</a> for this week's People of openSUSE. Stephan talks about his beginnings starting with a Commodore 64 with Ghostbusters, to today's hacking on KDE and openSUSE. "<em>During my studies I maintained the KDE installation on the faculty&#8217;s Solaris network (most played day-time game then was XBlast) and started in 2001 to directly contribute to KDE (C++ programming and other stuff)</em>." 

<!--break-->
