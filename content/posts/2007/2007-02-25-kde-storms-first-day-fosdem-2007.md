---
title: "KDE Storms First Day of FOSDEM 2007"
date:    2007-02-25
authors:
  - "jpoortvliet"
slug:    kde-storms-first-day-fosdem-2007
comments:
  - subject: "videos are available."
    date: 2007-02-25
    body: "http://ftp.belnet.be/mirrors/FOSDEM/2007/"
    author: "noname"
  - subject: "Re: videos are available."
    date: 2007-02-25
    body: "The server is very slow.\nCan somebody put them on bittorrent please?"
    author: "Viesturs"
  - subject: "Strigi video & presentation"
    date: 2007-02-25
    body: "Would it be possible to have the strigi video and presentation, and more generally, all other kde ones ?\nThanks!"
    author: "gaboo"
  - subject: "Re: Strigi video & presentation"
    date: 2007-02-25
    body: "Working on it ;-)\n\nFirst, recovering from FOSDEM, tomorrow I'll finish the second-day report and start bugging everybody for their slides..."
    author: "superstoned"
  - subject: "Re: Strigi video & presentation"
    date: 2007-02-26
    body: "You've been there? Nice :)\nWhere will you release your information and experience reports?"
    author: "liquidat"
  - subject: "Re: Strigi video & presentation"
    date: 2007-02-26
    body: "Well, you just read his report for the first day (assuming you RTFA :p), and he said the second day's report is comming next."
    author: "Narishma"
  - subject: "Nepomuk status report"
    date: 2007-02-26
    body: "For more information on the Nepomuk-KDE status, read this document. It goes into a lot of detail on the various parts of KDE.\n\nhttp://nepomuk.semanticdesktop.org/xwiki/bin/download/Main1/D7-2/D7.2_v11_NEPOMUK_KDE_Community_Involvement.pdf"
    author: "Jos"
  - subject: "Re: Nepomuk status report"
    date: 2007-02-26
    body: "In the early nineties I ran the UK R&D programme \"SALT\" (Speech And Language Technology) programme and other stuff.  I'm absolutely not a semantics, natural language expert but I hung out a lot with those that were. \n\nMy overall feeling then, not supplemented since by any news of serious breakthroughs, is that all of this is much more difficult than it looks.  All progress has been incremental and definitely \"northwards\" rather than \"true north\" as it were.\n\nI've just got a reply from one of such academics regarding NEMOPUK.  The opinion received was that in academic research circles this was all quite fashionable in mid-1990s but never seemed to cut it \"...but NEPOMUK seems to be implying that the structure will be more sophisticated about e.g guessing what should go to whom, or 'learning' who is entitled and interested to be copied in on what...\" \n\nAre there any underlying academic papers that underpin this project? (the references in the URL seemed all to be NEMOPUK deliverables).  I'm genuinely curious.\n\n"
    author: "Gerry"
  - subject: "Re: Nepomuk status report"
    date: 2007-02-26
    body: "A quick reply: check out the beagle++ website for some papers."
    author: "Jos"
  - subject: "Thanks KDE-NL!"
    date: 2007-02-26
    body: "Impressive and amazing KDE-NL team! They did a very good job preparing FOSDEM for KDE. The KDE booth was a success, Wendy also organized the wiki and lodging, nice to see new people getting in KDE so easily! Thanks Bart for preparing the KDE room track. And Jos, superb presentation and awesome report! \nI had a very very good time with you all and I am so happy to be able to put faces on names now! I'll send my pics to the FOSDEM team and I'll post a link on my blog when they are uploaded.\nYour \"teacher\" ;-)"
    author: "annma"
  - subject: "Re: Thanks KDE-NL!"
    date: 2007-02-26
    body: "Yes, thanks for the great preparation, and especially for the beds in the youth hostel. It worked great! :-)"
    author: "Sven Krohlas"
  - subject: "Re: Thanks KDE-NL!"
    date: 2007-02-26
    body: "here's a nice picture of the KDE booth\n\nhttp://www.flickr.com/photos/duncanmac-vicar/402662463/in/set-72157594557555344/\n\n"
    author: "Pieter Vande Wyngaerde"
---
The first day of the annual Free and Open Source Developers' European Meeting in Brussels was very busy for the KDE team: attending talks by other talented hackers, hosting KDE related talks in the developer room, representing KDE at the booth, mingling with other hackers, bug hunting and work on new features. KDE had a strong presence this year, at least twice as many KDE people attended including a very strong showing from the Amarok developers. Speakers in the KDE developer room included Jos van den Oever, Stephan Laurient, Flavio and Sander Koning.








<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right">
<img src="http://static.kdenews.org/jr/fosdem-2007-stephan-jos.jpg" width="250" height="188" /><br />
The NEPOMUK Talk
</div>

<p>Our FOSDEM presence in Brussels started with the traditional 'Friday Beer Event' at Le Roy d'Espagne in the Grand Place. The KDE stall on Saturday was very crowded with both curious visitors and long time KDE people. The staff sold almost 500 euro worth of merchandise! Unfortunately we won't be able to match that total on Sunday, we are simply out of T-shirts, badges, bags and mugs. Many people moved to the developer room for the NEPOMUK talk, and made themselves comfortable behind their laptops.</p>

<h2>NEPOMUK and the Semantic Desktop</h2>

<p>The first talk was about the NEPOMUK project and its integration into KDE. Jos van den Oever of Strigi fame and Stephan Laurient from Mandriva gave the talk, unfortunately Sebastian Trueg was ill. Jos kicked the talk off by explaining to an intrigued audience the concepts behind NEPOMUK. NEPOMUK is doing researching into the Semantic Desktop. They are trying to enable knowledge to be processed by the computer, allowing applications to extract more information from files and letting the user add labels and tags to his data. The second step is sharing this knowledge with other users and desktops. Jos showed the structure of the framework and how it works. NEPOMUK provides a set of standardised programmer interfaces, ontologies and applications. They aim to integrate their technology in KDE, as the tight integration between the applications makes it easier for the developers to leverage the many capabilities NEPOMUK will offer. Then Stephane took over and explained the project's grand vision and the people behind it. NEPOMUK gathers researchers, industrial software developers and users together to develop a collaboration environment which supports both personal information management and the sharing across social or organisational relationships. He showed off some screenshots of the current status of tagging support in NEPOMUK and the integration in KDE, and what to expect the next two years.</p>

<h2>Strigi</h2>

<p>Flavio Castelli told us about using and integrating search engine Strigi in the desktop. He explained how we are having more and more trouble finding our files and documents, and Strigi offers a solution here. Applications can easily ask Strigi through the DBus interface for information like files containing a certain string or of a certain type. Flavio showed us how to use Strigi in applications, what interfaces to use and gave examples how to improve an application by using Strigi's features.  They hope to work on integrating Strigi into core KDE components such as the file dialogue in the near future.</p>

<h2>Documentation and Handbooks</h2>

<p>Sander Koning gave the last talk of the day. He told us about all the coordination work going on behind the scenes for the handbooks and documentation in KDE. Sander explained the process and tools used for producing documentation, translating it, and getting it on the end user's desktop. He then turned to the future and explained what they are planning to do. More documentation of course, which means more people working on it. Offering the documentation via a wiki might help in this area. Documentation also needs screenshots in all of the different languages. Sander has been working on this problem, and has developed a tool which automatically takes screenshots of applications after a documentation update. Sander then discussed some of the new ideas for the context ("What's this") help, for KDE 4, and the implications for the documentation writers. People in the room wondered if it would be possible to link to a future documentation wiki from within the documentation itself.</p>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left">
<img src="http://static.kdenews.org/jr/fosdem-2007-amarok.jpg" width="250" height="188" /><br />
Discussing Amarok 2.0
</div>
<p>During all these talks, some busy bees were hacking on different KDE related projects. Niels van Mourik noticed that KPat's demo mode is very cool and really shows off Qt 4's new graphics support. The only problem being that once the demo is over you have to start it again by hand. So Max Kossick (one of the Amarok hackers) jumped in and fixed KPat to play the demo continuously in the KDE booth</p>

<p>All in all, the day was fruitfull and fun. At the end of the day, the hackers spread out all over Brusselles, looking for food or returning to their hotels. And of course preparing for tomorrow!</p>






