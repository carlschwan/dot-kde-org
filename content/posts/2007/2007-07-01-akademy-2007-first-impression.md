---
title: "aKademy 2007: The First Impression"
date:    2007-07-01
authors:
  - "jallen"
slug:    akademy-2007-first-impression
comments:
  - subject: "finally ;-)"
    date: 2007-07-01
    body: "BTW tomorrow we will have more pictures and more articles ;-)\n\nPlease, if you got nice foto's, or a good story, a cool quote, or some nice progress/hack/piece of code in an application, PLEASE, talk to Danny and me, or get to room 13.02 tomorrow!!!!"
    author: "jospoortvliet"
  - subject: "Re: finally ;-)"
    date: 2007-07-01
    body: "Some photos here... http://tumblelog.sorn.net/"
    author: "Sandy Dunlop"
  - subject: "Homeward Bound!"
    date: 2007-07-01
    body: "Well, just a quick note from the train back to London to say what a great weekend it's been.  Some fantastic talks, well organised (shame about the network), and I'll be wearing my t-shirt with pride :-)  Great to finally get to meet people I only knew from their hackergotchis and nicks, and to lay plans for cool new features...\n\nEnjoy the hackfest, guys and gals!  I wish I could be there, but work demands and all that.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Homeward Bound!"
    date: 2007-07-01
    body: "P.S. The goodie bags rocked!!!\nP.P.S  The train sucks, screaming kids, cramped seats, wifi costing 10 quid and not working under linux..."
    author: "Odysseus"
  - subject: "Hooray again"
    date: 2007-07-01
    body: "Quite some new faces - some grew their hair, others lost it, a few gained weight. But despite the changing faces I am glad to see him agin: the waving man in the top right corner, you gotta love him!\n\nCompare to http://static.kdenews.org/jr/akademy-2006-group-photo.html :)"
    author: "me"
  - subject: "Greets to Glasgow"
    date: 2007-07-01
    body: "\nfrom the rainy Germany. Sorry, we weren't able to make it this year. Hopefully next time.\n\nJ\u00f6rg & Valerie"
    author: "J\u00f6rg Hoh"
  - subject: "Re: Greets to Glasgow"
    date: 2007-07-02
    body: "Greets back from a rainy Glasgow!"
    author: "Sebastian K\u00fcgler"
---
<a href="http://akademy2007.kde.org/">aKademy 2007</a> has started! Saturday, the first day of the conference, brought us many talks about various topics, ranging from very technical to more practically oriented. These talks are so content-rich that our coverage of the user conference will require several consecutive articles. Read on for the first aKademy 2007 Report, the First Impression.







<!--break-->
<h3>Kickoff</h3>

<p>The conference was kicked off by Adriaan de Groot, who introduced Lars Knoll of <a href="http://www.trolltech.com/">Trolltech</a> and our shiny new KDE-branded coffee mugs, which we received in our 'aKademy bag'. The bag was filled with other goodies as well, like a 2 GB bootable USB stick courtesy of Mandriva, and some live CD's.</p>

<br />

<table align="center" border="0" cellpadding="2"><tbody><tr><td><a href="http://static.kdenews.org/danimo/akademy07/group-photo.html"><img src="http://static.kdenews.org/dannya/akademy_group-photo_small.jpg" alt="aKademy 2007 Group Photo" title="Click to join names to faces!"></a></td></tr><tr><td align="center"><i>Click to join names to faces (<a href="http://static.kdenews.org/dannya/akademy_group-photo.jpg">full size version</a>)</td></tr></tbody></table>

<h3>Organisation</h3>

<p>During the talks, it was discovered that there was no internet. Indeed, 200+ geeks at a university, without network access. The organisation has quite some trouble getting us online. So this report is fairly late, and you probably noticed the almost-full radio silence. But the network is already up in one of the buildings, and we're expecting connection any time now. Besides, the lack of network has some upsides. It ensures most hackers spend their time actually listening to the talks. Aside from this one glitch, the organisation has been excellent so far. They even arranged food and beer last night, organising a social event which we enjoyed greatly. Most hackers spend their evening time in pubs and clubs, but some were already up-and-running before 8 in the morning on Sunday. On Monday, the group will attend another social event, where we will meet the Lord Provost of Glasgow in the rather fancy setting of the public chambers.</p>

<h3>Arrangements</h3>

<p>The t-shirts this year - which are fair trade produced! - are just amazing, and have been selling like crazy. Now more and more people are wearing these new t-shirts, decorated with Konqi taking the form of the infamous Loch Ness Monster. All in all, the arrangements are very nice here. Glasgow is a great city, and our hosts make sure things run smoothly.</p>

<p>Sunday will bring some more talks, which we will report to you soon. Then the hackers will start doing what they do best: code, from dawn to dusk. We will bug them about the things they are working on, and conduct some interviews, so expect more news from aKademy 2007!</p>

<p><i>Read our reports from the <a href="http://dot.kde.org/1183385741/">Keynotes</a> and the <a href="http://dot.kde.org/1183388210/">Tracks (KDE 4 Pillars and Bindings)</a></i></p>






