---
title: "The Road to KDE 4: Full Mac OS X Support"
date:    2007-01-16
authors:
  - "tunrau"
slug:    road-kde-4-full-mac-os-x-support
comments:
  - subject: "coolness"
    date: 2007-01-16
    body: "but beside the apple look the kde look does look not good :(\nthe icons in the apps shown are not the oxygen icons, are they?\n\n"
    author: "chri"
  - subject: "Re: coolness"
    date: 2007-01-16
    body: "Oxygen icons are entering kdelibs in SVN today, unless something changed... so future shots should show the new icons..."
    author: "Troy Unrau"
  - subject: "Re: coolness"
    date: 2007-01-16
    body: "Digg it!\n\nhttp://digg.com/apple/The_Road_to_KDE_4_Full_Mac_OS_X_Support"
    author: "ac"
  - subject: "Re: coolness"
    date: 2007-01-16
    body: "dugg."
    author: "Mark Kretschmann"
  - subject: "Re: coolness"
    date: 2007-01-16
    body: "Yay, it's on digg frontpage now :)"
    author: "Mark Kretschmann"
  - subject: "Excellent dot"
    date: 2007-01-16
    body: "Great summary, a very interesting read!  Good to see things are progressing so nicely.\n\nAs for KDE Workspace -- well, while getting just the KDE Applications over to those systems is already a really kool thing, I'd personally welcome an explorer.exe shell replacement.  Very much even :)"
    author: "Ralesk"
  - subject: "yeah lol give us mutant kde with win32 inside"
    date: 2007-01-16
    body: "you're right though the explorer thing just is not up to _anything_ with konqueror. it has more serious security issues however so it's leading somewhere.\nanyways i myself will try and look how to get this kde windows stuff to work under wine properly. that'll be phun. foss rulez so waaaay more."
    author: "eMPe"
  - subject: "Re: yeah lol give us mutant kde with win32 inside"
    date: 2007-01-16
    body: "explorer.exe as in the Windows shell, not as in iexplore.exe the browser.  Even if KDE Workspace isn't ported, Konqueror will be just as available (at least very likely, anyway), as it is on KDE/OSX.\n\nI like my Windows for its hardware and multimedia support and gaming (not that that happens too often!) and for some applications that I prefer over there, but I love my KDE desktop (and several KDE apps too (hi, eric3 and amarok!)) -- much enough in fact that I'm usually on linux and rather put up with the hell that's sound support in this OS."
    author: "Ralesk"
  - subject: "Re: yeah lol give us mutant kde with win32 inside"
    date: 2007-01-16
    body: "Don't forget Reactos"
    author: "Jim"
  - subject: "Re: yeah lol give us mutant kde with win32 inside"
    date: 2007-01-20
    body: "It's a very sweet project and I really hope they succeed in creating an open source Windows clone; and I'm more than willing to give it a spin!\n\nBut how does that help me (and others who are in the same boat), with having a KDE desktop and running Windows software I adore (irfanview and xmplay, in particular (okay, the first one runs with Wine), but there are a bunch of others I occasionally need as well), and not having to deal with Linux's inferior hardware and multimedia support? -- because I doubt ReactOS will have kdesktop and kicker :P"
    author: "Ralesk"
  - subject: "Re: yeah lol give us mutant kde with win32 inside"
    date: 2007-01-29
    body: "http://sourceforge.net/forum/forum.php?forum_id=507276"
    author: "Mats"
  - subject: "Re: Excellent dot"
    date: 2007-01-16
    body: "I agree that an explorer replacement would be a great idea. It would be a wonderful step for people migrating to OSS, as they could have a full K Desktop Environment, but be able to easily start their remaining native Windows apps as needed, and everything would appear consistent.\n\nHowever, while KDE will have a much superior taskbar, menu and file manager to Windows there would be a problem with providing all the random dialogs and stuff that Explorer is supposed to provide. The ReactOS Explorer (which can be installed on Windows XP; much better for slow machines) seems to load the libraries that the original explorer would load (I have no idea how it works internally, but it certainly produces some windows which look just like the equivalents from the normal explorer). That would be a whole lot of extra work above just making things work on Windows.\n\nAlso, will Vista allow the shell to be changed? It's coming out very soon and everyone is going to \"upgrade\", regardless of the problems/benefits."
    author: "Ben Morris"
  - subject: "Awesome review!"
    date: 2007-01-16
    body: "Troy, you are doing an awesome job with this \"Road\" series! I really love them, and they show what is indeed happening in the KDE development. It gets more people interested in KDE, and it finally shows the first real impressions of KDE 4 - some KDE users need this while they are waiting for KDE 4 ;)\n\nI'm really looking forward for your next article."
    author: "liquidat"
  - subject: "Re: Awesome review!"
    date: 2007-01-16
    body: "++"
    author: "Joergen Ramskov"
  - subject: "kstars screenshot"
    date: 2007-01-16
    body: "Yay, Winnipeg!"
    author: "Gabriel Hurley"
  - subject: "OSS on none OSS platforms"
    date: 2007-01-16
    body: "I would content the more application (like firefox and OpenOffice) you can get people to use, the less reason they have to hold onto a closed source system. \n\nIf you want to build a wall and not let any closed source users in, you could certainly do that to your own detriment. I would contend this wouldn't be useful in the long term. \n\nYou should let you work stand on its merits not on it association. \n"
    author: "James Brown"
  - subject: "Re: OSS on none OSS platforms"
    date: 2007-01-16
    body: "I agree with your post, I would also like to add 2 points:\n\n1) KDE already ran on closed source systems: UNIX. It has ran in solaris way before it was open sourced, and it runs on other UNIX variants. If indeed they wanted to prevent KDE from running in closed sourced systems, these \"ports\" should be killed as well.\n\n2) Why is it KDE's goal to promote free software? KDE goal should be to promote KDE, which happens to be free software. Porting it to as many platforms as possible is a great way to make kde more popular."
    author: "Paul Pacheco"
  - subject: "Re: OSS on none OSS platforms"
    date: 2007-01-16
    body: "You would be wrong.  While the idea of porting open source applications to closed source environments DOES sound like it would attract people to OS platforms... there is overwhelming statistical evidence to show otherwise.  \n\nFor example, the easiest way to get OS applications on a non-OS platform is by running OSX.  By your theory, Apple users would be some of the quickest converts to open source operating systems.  But the reality is that people are LEAVING open source to use OSX; in fairly sizable numbers in fact.  When polled, the most significant factor in moving from Linux to OSX was \"availability of both Mac and Linux applications on OSX.\"  \n\nIn this case cross-platform applications actually facilitated people moving AWAY from open source.\n\nAnother example is Firefox and OpenOffice.org themselves.  Their availability on Windows has caused NO (as in none, zip, nada) statistically significant movement to open source operating systems.  While there are plenty of people switching to Linux from Windows... the fact that they had Firefox and OpenOffice.org available to them on Windows doesn't seem to be a factor in their decision.\n"
    author: "Bobby"
  - subject: "Re: OSS on none OSS platforms"
    date: 2007-01-17
    body: "do you have a source for those statistics?"
    author: "otherAC"
  - subject: "Re: OSS on none OSS platforms"
    date: 2007-01-17
    body: "\"In this case cross-platform applications actually facilitated people moving AWAY from open source.\"\n\nAre you saying cross platform open source applications facilitate people moving away from open source? Would you care to explain what exactly you mean here?\n\nIf you think that a few applications being available on other platforms caused people to move away from Linux, I think you either delusional or intentionally avoiding the real cause of that migration.\n\nHaving those applications readily available might make a transition easier but they would not be the impetus for such a move. I would guess that these people either had a compelling reason to migrate to another OS or they found Linux frustrating to use."
    author: "Ari"
  - subject: "Re: OSS on none OSS platforms"
    date: 2007-01-17
    body: "I am not so sure about this. My first experiences with linux were caused by my desire to get PostgreSQL running (and I haven't been able to use PostgreSQL on windows back then). Only after being somewhat forced to use it, I stayed on board and kept using it more and more - until linux finally has become my preferred OS.\n\nI guess this is kind of typical for a lot of developers. They start using linux (or the BSDs or OpenSolaris) as their local server (for files, webapps, databases) and _might_ just stick with it because it simply works and it is finally a system where you know what actually is happening.\n\nThis is of course different for non-developers (sometimes referred to as users - as if we wouldn't use the systems ourselves). If there is a huge pool of free apps available that are actually used, they of course could switch faster. But I guess this is only relevant (as of now) at work where IT departments in conjunction with the execs of the company recommend which OS (and other software) to use. Whether they suggest (or even evaluate) a free OS or a proprietary one is among other things influenced by the availability of the used apps.\n"
    author: "rittmey"
  - subject: "Converting Users To KDE?"
    date: 2007-01-16
    body: "After reading this and the new KDE definitions, I'm a little confused as to the benefit of KDE on other platforms.  From what I can understand of the definitions, KDE Workspace is the real meat of what I understand KDE to be.  Without the KDE Workspace running on MacOS or Windows, what we are really talking about is KDE Applications running on the other platforms, correct?  If so, I don't have an issue with it.  But I fail to see how it will help convert users over to Linux.  KDE Workspace is how they would see the benefit of KDE itself, but they won't see that part.  So really all they get are the cool KDE apps native on their platform but no incentive to switch.\n\nAm I missing something?  Someone more knowledgeable, please enlighten me."
    author: "cirehawk"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "Why should the goal of the KDE project be to convert people to linux? What does the kernel have to do with a window manager/GUI toolkit? Are you opposed to KDE running on FreeBSD?"
    author: "Ari"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "I'm not opposed at all to KDE running on FreeBSD.  It is an open system and as I understand it, the full KDE system runs on it.  You ask why should the goal of the KDE project be to convert people to linux?  That's not my goal per se, but I thought it was stated by some KDE developers that part of the reason for running KDE on Windows and MacOS was to make it easier for windows developers to develop for Linux, thus pulling in more Windows users.  If that is a goal, then I was trying to get clarification (or more like education) on how this strategy would do that."
    author: "cirehawk"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "I agree Troy's explanation wasn't very clear.\n\nPutting aside \"the convert people to Linux\" part, there is one very simple benefit, and it is not \"making it easier for windows developers to develop for Linux\". It is Windows and Mac developers coding for KDE applications, and in time even kdelibs! They don't need to switch to Linux in order to be beneficial for KDE.\n\nThat means, for example, a physicist liking KStars so much, that he decides contributing that missing feature preventing him from dropping his commercial planetarium solution. Same with other apps. The source code improvements come from three operating systems now, Windows, Mac and Linux (and Unixes, of course). Everybody wins."
    author: "tobami"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "Ok, now that I understand.  So the benefit is that KDE apps get a lot more potential developers who can improve the app.  A \"side\" benefit is that some may convert to Linux/KDE, but that isn't necessarily the main objective."
    author: "cirehawk"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "also some users who need or like to run mac/windows may like to use good software on that platform too. For example me likes to use KMail and amarok while I am on windows (which I need to use for my win=>linux porting jobs :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "Many people find themselves tied, not to a particular platform but to a set of applications. If the applications they use everyday can be found on a different platform, wouldn't that make the switch easier for them? It did for me, when I started using FLOSS apss instead of proprietary ones on Windows. Not only makes it easier to keep using your computer as you did before switching, but also dispels any doubt you might have about the quality of open source software.\n\nI had to use GNOME apps like GAIM or GIMP on windows. Just imagine people using Kopete or Krita... Using a Linux/KDE desktop would make much more sense to them after using kde apps. The learning curve would be way less steep, as they would only have to get used to the desktop per se, and kde can behave very much like windows if configured to. The key is making it easy for people making the switch one step at a time."
    author: "Fede"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "On the other hand, KDE apps (and also Gnome/GTK+ apps) looks alien in Windows and Mac environment. And while regular Windows users can accept such things, I guess Mac zea^H^H^H users won't, screaming that they want true Mac OS apps and not foreign shoehorned apps, just like Linux users complain when company X releases a Wine-based Linux version of their products.\nI guess a proper iconset could help, yet other issues (HIG) are harder to conciliate.\nSo, open-minded people who uses multiple platforms at once (specially Unix/Linux people using Mac because its Unix fundation) can get interest on KDE/Mac, but getting hardcore Mac users interested in using KDE apps into the OSX desktop is not a task I believe possible."
    author: "Shulai"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "As a Mac developer, a Linux developer, and a Windows developer, I think the \"OS-centric\" viewpoint is dated. No one cares about the OS, they care about the applications.\n\nTHAT is what is important.\n\nAs a developer I see this as significant because a) wxWidgets reallys sucks and b) QT is pretty nifty (yes, I've used them both).\n\nAs for reconciling the HIG with KDE - let me clue you in that there are a LOT of OS X apps out there that do NOT follow HIG. \n\nAs for OS X developers, frankly, I would rather write in a real OO language than C++. Sorry, but them's the reality. So, no, you probably won't see folks switch. What you WILL see is more people write cross-platform apps.\n\nTHAT is what is important here."
    author: "feloneous"
  - subject: "what is important"
    date: 2007-01-16
    body: "What about this:\n\nKDE is good because it has many apps that are well-integrated together and to the underlying operating system (kio, dbus, ..).\n\nBy making it more abstract and by porting the apps on other desktops the efforts are put on dis-integrating the whole desktop.\n\nThe identity of the project is now split between those who only care about their apps, and those who want a functional desktop.\n\nPorting to other platforms is an interesting thing if done afterwards, not first.\n\nIt is not because something can be done that it must be done.\n"
    author: "bah"
  - subject: "Re: what is important"
    date: 2007-01-16
    body: "Porting Kwin/Kdesktop&Kicker(now: plasma)/etc. over would mean writing replacements to the explorer shell and equivalent on the mac.  Though if you really want to work in a KDE workspace, just grab a PC-BSD or Kubuntu disk and go nuts.  And the argument that you can't install a different OS at work is probably void anyway, considering that you'd be wanting to replace the explorer shell.  If you admin lets you do that, (s)he'd probably just let you use unix anyway :)\n\nAll of the important bits of KDE cross-application integration will still be there.  You can still save to the fish:/ ioslave in the KDE file dialogs, and such, or write scipts to control applications via dbus.  Things like KOffice scripting using Kross should be platform agnostic.  Other than a few utilities, KWin/Plasma are the only two substantial applications that won't make the journey.  KWin is totally redundant on non-X11 platforms, and Plasma is mostly redundant.  The only reason one would want to use it is for the little plasma applets (plasmoids), but again, if they're that important to you, use Unix/X11.\n"
    author: "Troy Unrau"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "Speaking of what you call a real OO language, is it possible to write KDE apps with ObjC?"
    author: "Leo S"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "There used to be Objective C bindings but nobody used them and they were dropped from active maintenance."
    author: "Boudewijn Rempt"
  - subject: "This is a good thing"
    date: 2007-01-17
    body: "I want to second Fede's point. Most people care much more about their applications than their operating system. If people get accustomed to using KDE applications that run on Windows, Mac, and Linux, then they will care a lot less what operating system they're using. And at that point, the advantages of Linux (and other free systems like BSD) will become more compelling and people will start switching when they get tired of paying for Microsoft's cosmetic upgrades and bad security.\n\nOver the short term, I think this will pull more people to Macs from both Windows and Linux. But over the long term we may see more of a shift to open source systems from both Windows and Mac. That's especially true if Mac and Windows developers do contribute to KDE and further improve the applications."
    author: "johnrobert"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "Actually, I think that there are several KDE applications offering great functionality on KDE, why should they be confined to the KDE Workspace only? I have a hard time finding a Latex editor on par with Kile and an email application with the features of KMail on the Mac. I am pretty sure that the developers of those apps will be very happy when they receive their first bug report from a Mac user :-)\n\nKDE/Mac guys, keep up the good work! "
    author: "Giorgos Gousios"
  - subject: "Re: Converting Users To KDE?"
    date: 2008-01-03
    body: "Just a question, have you tried TexShop?  It works very well for me.  http://www.uoregon.edu/~koch/texshop/."
    author: "Robert G."
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "Simple - exposing developers and users to free (as in freedom) software, will benefit the free software community of users if they are accepted and promoted by these new users."
    author: "More free (as in freedom) software"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "Every license out there puts limits on the freedom of others. Please stop misusing the word freedom. Open Source software licenses are merely more permissive in that they provide access to the source and the permission to redistribute derived works under the same license to other people. The GPL is one of the more restrictive licenses in that it restricts the freedom of developers to make use of the code in only GPL licensed projects. It is viral in nature and because of that, it actually can strip the rights of other developers if they do not educate themselves of all the terms and conditions before they touch the code."
    author: "Ari"
  - subject: "Re: Converting Users To KDE?"
    date: 2007-01-16
    body: "Yeah, and that's intended and works toward keeping the end user's freedom, as opposed to the programmer's freedom which is maximized by using a BSD style license.\n\nCan we please omit this discussion? I can't hear it anymore. Just accept that there isn't only one definition of freedom, and never assume that you know what's right and the other one is wrong. It's just a point of view.\n\nThanks for stopping this part of the thread."
    author: "Jakob Petsovits"
  - subject: "Great!"
    date: 2007-01-16
    body: "Troy, you.... ROCK!\nYour articles are simply wonderful! Is tomorrow Monday again? Uh?!?! Well... I'm definitely looking forward your next article!\nYour feature lists make me desire KDE 4 NOW!!! Damn, it's going to be sooo coool and soooo great, absolutely the greatest DE in the world. I think the portability to Mac and Win is a killer-feature: we will have great free applications working anywhere, well, this is a dream becoming true.\n\nKeep working on, The Road to KDE 4 is amazing!"
    author: "Nightfall"
  - subject: "Re: Great!"
    date: 2007-01-16
    body: "Seriously.  The only thing wrong with The Road to KDE4 is that it is posted so soon after This Week in SVN, so that I have to wait a whole week for my KDE news fix :)  Any chance of moving it to, say, Thursdays?"
    author: "Anon"
  - subject: "Re: Great!"
    date: 2007-01-16
    body: "i can only agree.... :D"
    author: "superstoned"
  - subject: "Re: Great!"
    date: 2007-01-16
    body: "Love the Road to KDE4 articles. Great work Troy!"
    author: "Lans"
  - subject: "Great article, great technology"
    date: 2007-01-16
    body: "Hi Troy,\n\nThank you for the great article! I'm perfectly fine with people using other operating systems and hardware, as long as they use KDE as their desktop environment/GUI ;-)"
    author: "Claire"
  - subject: "great"
    date: 2007-01-16
    body: "So the smart guys started wasting time porting their selfish little apps to more platforms instead of finishing KDE4. Does Qt4 suck or what ? :-)\n\nThis said, i am glad to see better integrated apps on the Mac, the ok/cancel buttons are finally on the right side.\n\nPS: no puppies but kittens\n"
    author: "\\o/"
  - subject: "???"
    date: 2007-01-16
    body: "KDE is based on Qt. Also on Mac. How do you distill any qt-suckage out of this article?"
    author: "/o\\"
  - subject: "Re: great"
    date: 2007-01-16
    body: "\"So the smart guys started wasting time porting their selfish little apps to more platforms instead of finishing KDE4. Does Qt4 suck or what ? :-)\"\n\nHey, developing for KDE4 base system is a totaly different work than building packages. Would you have complain if the article were about packaging KDE4 to Ubuntu ? Canonical doesn't develop KDE4 (they might help), but you can install somme of the 4th serie on your ubuntu OS.\n\nThanks to Qt, It's Nearly the same to make an Ubuntu .deb that a OSX .cmg or a windows .exe.  (exept low level system specific library and file system and ???)\nJust look these as two _huge_ new distrib suported by KDE."
    author: "kollum"
  - subject: "Re: great"
    date: 2007-01-18
    body: ">Just look these as two _huge_ new distrib suported by KDE.\n\nExcept these two huge new \"distrubitions\" (win/osx) are 100% closed source, 100% propietary and patented to hell and back. x.x\n\nljones"
    author: "ljones"
  - subject: "Re: great"
    date: 2007-01-22
    body: "OS X is not 100% closed source. OK it's mostly closed, but hell, the kernel is open.\n\nWindows OTOH doesn't deserve KDE.\n"
    author: "Simon Roby"
  - subject: "Re: great"
    date: 2007-01-22
    body: "What the hell? KDE News is retarded. I entered my email address so I can get a notification, I certainly did NOT want my email address to be visible by anyone (especially harvesters)."
    author: "Simon Roby"
  - subject: "But how will the integration be?"
    date: 2007-01-16
    body: "One of the most attractive features of KDE, in my opinion, is how all my apps work so nicely together.  If I right-click an album in Amarok, I can tell it to burn it to CD.  K3B pops up with the tracks ready to burn.  If I go to a web page that needs a login, my Wallet pops up and delivers my credentials.  The list goes on and on.  I can't imagine that integration like this will be so good on other platforms (maybe I'm wrong).  For that reason KDE Workspace will always be my preferred environment.  That said, I have to use Windows at work, and it will be nice to have Kontact and Konqueror there, even if the integration is not as good.  Troy, another great article.  Thanks."
    author: "Louis"
  - subject: "Re: But how will the integration be?"
    date: 2007-01-16
    body: "I belive it will be the #1 reason for a win/mac KDE-apps fan to switch to an open-source UNIX.\n\nKDE ads guy may have some work in this area."
    author: "kollum"
  - subject: "KDE/target"
    date: 2007-01-16
    body: "If I understand it correctly KDE 4 applications for MAC and WIN will be ready and functional before the K Desktop Environment 4.0 release. That leads to the absurd situation that KDE4 applications can already be developed for and run on other plattforms but not for Linux which has to stay with the 3.5.x release circle, right? Actually it is a good idea to have a working playground for programs, so KDE4 application development is not restricted by the wait for KDE4.0. It is always a bit strange to develop applications for an environment which is not already in productive use. "
    author: "Jim"
  - subject: "Re: KDE/target"
    date: 2007-01-16
    body: "You misunderstand.  KDE/Mac is using the same source code as KDE/X11.  They are at the same stage of development.  The applications I took screenies of do exist and run just as well on X11.  The stable versions will be released concurrently, barring any packaging delays.\n\nYou can get KDE 4 development snapshots for Linux, depending on your distro, or compile them yourself (which is what I do for my articles).  All RangerRick did is build some snapshots of the current KDE 4 source code for the Mac, since not everyone has the patience for building their own code.  Kubuntu does the same thing.\n\nThe KDE 4 APIs have not yet stabilized, so I would suggest that developers refrain from writing their comp sci thesis programs on it yet, at least until we start getting some official beta releases."
    author: "Troy Unrau"
  - subject: "Re: KDE/target"
    date: 2007-01-16
    body: "Of course, you can get and develop for KDE on Linux just as well, if not better than on Mac OS and Windows. In fact, most of the KDE developers are developing KDE on Linux. It's just that current KDE 4 is still pre-alpha, which means that there are hardly pre-built packages out there so you have to compile it by yourself. (But then, openSUSE already has got a KDE 4 development snapshot packaged, and Kubuntu 7.04 is planning to do the same.)\n\nGetting and running KDE 4 requires a bit of hassle everywhere, not just on Linux. If you want to develop for KDE 4, you need to set it up from its Subversion repository anyways, so no way going with easy pre-built installable packages. Well, yeah, you can stay with one of the occasional snapshots, but they are pretty much outdated in 2 or 3 weeks or so, because the libs are still in flux."
    author: "Jakob Petsovits"
  - subject: "What About Open/Save/Print Dialogs?"
    date: 2007-01-16
    body: "I have a question.  Is it possible to use the native Open/Save/Print dialog\nboxes?  I think it is a little weird to show the KDE dialogs on the Mac.  Of\ncourse this is the same problem you run into when running KDE apps in Gnome...\nwhich means the answer would probably be the same, which is to use the Portland project's intended solution.\n"
    author: "Henry S."
  - subject: "Re: What About Open/Save/Print Dialogs?"
    date: 2007-01-17
    body: "Or Gnome apps on KDE -- but yeah, you're right and I think it's indeed favourable to have native open/save windows!"
    author: "Ralesk"
  - subject: "Print / Scan Solutions"
    date: 2007-01-17
    body: "I've been wondering how we intend to deal with Print and Scan on non-nix platforms, especially Windows?  The standard kprinter and libkscan only support CUPS and SANE, which while they work under Windows are not exactly optimal from a user, support or dependency/installation standpoint.  Will Solid be providing some level of abstraction for these to use the native Windows systems and drivers?\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Print / Scan Solutions"
    date: 2007-01-17
    body: "kprinter supports every available printing subsystem onder unix, not only cups.\nUnder windows, it should just hook into the printing subsystem of that operating system ;)\n"
    author: "otherAC"
  - subject: "Re: Print / Scan Solutions"
    date: 2007-01-17
    body: "CUPS works on Windows???? You're telling news to me..."
    author: "ac"
  - subject: "Re: Print / Scan Solutions"
    date: 2007-05-14
    body: "CUPS is the standard printing backend on OS X, so KDE should be pretty set in that respect."
    author: "Tanner Lovelace"
  - subject: "re: full mac os support"
    date: 2007-01-18
    body: "Isn't this a bad idea overall?\n\nIf some part of kde or (even worse kdelibs) were developed on windows or OSX then surely some programmer somewhere could incert specific code ment only for windows or OSX into KDE? What if - say - a programmer decided he or she would like an activex control somewhere in kde or kdelibs? The result -- a program which is tied only to the ms platform.\n\nThis idea surely has the potential to ruin KDE? Programs which will run on one system, but not another .... wouldn't KDE on gnu/linux just get left behind?\n\nljones"
    author: "ljones"
  - subject: "re: full mac os support"
    date: 2007-01-19
    body: "We have self-controlling policy, not mentioning the Technology Working Group that controls that. Moreover, it's open source what prevents such threats. Portability is our preference.\n"
    author: "Jaroslaw Staniek"
  - subject: "re: full mac os support"
    date: 2007-01-19
    body: "Well, policies can change -- if it's a policy isn't it within reason that KDE could go from open source to a mozilla style licence, or even a propietary licence? \n\nPersonally I find the argument that the technology working group and portability a little bit weak to be honest. The technology working group could disagree or approve ms/mac specific code at some future point. And portability can never really be achived, for some things (for example) with windows you *need* propietary stuff to do some things (or they can't be done at all), eg with directx.\n\nLet me tell you what I fear. That KDE ends up just as a \"ooh, a cool looking win/osx gadget/window set\" with the linux version left forgotten. \n\nDoes KDE care about OSS/Linux, or is the future roadmap a propietary one eventually with a pricetag?\n\nljones"
    author: "ljones"
  - subject: "re: full mac os support"
    date: 2007-01-20
    body: "> Well, policies can change -- if it's a policy isn't it within reason \n> that KDE could go from open source to a mozilla style \n> licence, or even a propietary licence? \n\nShort answer:  No, it is not within reason.\n\nLong answer:\n\nThe copyright over the KDE code is not assigned to any central body.  Rather, the copyright for each source file belongs to the people who wrote them.  Changing the license would, in theory at least, require the consent of at least every major KDE contributor whose code is still in existence. \n\nIt would be easier to change GCC or Emacs to a non-free license than KDE, since there the copyright is assigned to the Free Software Foundation.\n\n> The technology working group could disagree or approve ms/mac \n> specific code at some future point\n\nThere is already Windows and Mac specific code in KDE, there has to be since the one goal of the KDE libraries is to hide the differences in underlying platforms and present a common interface to the developer using the libraries.\n\nWhat you are asking is \"will there be an MS/Mac specific feature\" at some point.  Quite possibly, a developer can write an interface and provide the Windows code to implement that interface and not the Linux code to do so.  But as long as the majority of KDE users work primarily with free operating systems, those versions of KDE are likely to provide the best experience.\n\n> Does KDE care about OSS/Linux, or is the future roadmap \n> a propietary one eventually with a pricetag?\n\nKDE is a community of people, not a company, and it must be treated as such.  There is no roadmap.  The future is determined by the people who develop it.    "
    author: "Robert Knight"
  - subject: "Hallelujah"
    date: 2007-01-19
    body: "Since I'm a new Mac convert, I'm totally thrilled by KDE/Mac as I've been a KDE/Linux user for about seven years now. And there is only one thing that I really, really miss on my Mac: KMail. And KDE/Mac means I just have to be a little patient and my prayers for a (seamless) KMail on my Mac will come true ! This is too good to believe :-)"
    author: "DarkDust"
  - subject: "When can we use KDE to replace the finder"
    date: 2007-01-20
    body: "I would love if we could use KDE to replace the finder and the Doc. We should have an option to use KDE/MAC without Finder and Doc on. I know people will be pissed that I suggested this--but I really hate the MAC OSX DOC/Finder. "
    author: "paperclip"
  - subject: "Re: When can we use KDE to replace the finder"
    date: 2007-01-20
    body: "Why don't you just install linux then?"
    author: "Troy Unrau"
  - subject: "Looking forward to it."
    date: 2007-01-21
    body: "I've a macbook and I'm really looking forwards to this, as there are several applications I'm missing from my stationary Gentoo Linux PC with KDE3.5. \n\n- A really great music player and manager, iTunes is IMO just terrible, so I hope the amaroK team will make amaroK 2.0 available for OS X.\n- kopete, for multi protocol IM.\n- A good filemanager with network (fish, ftp etc) capacity (konqueor) so I can use easily transfer files over network.\n- And a lot of small games to pass the time in the two hours I spend on train each day to and from work.\n\nAnd of cause I prefer to have the same applications on both my laptop, work pc and home pc. ;)\n\nI'm currently using the snapshots of KDE4 from http://ranger.users.finkproject.org/kde/index.php/Home\nto have a glimpse of the future of KDE4 and to help find bugs in the applications so I can report them."
    author: "m_abs"
  - subject: "Re: Looking forward to it."
    date: 2007-01-21
    body: "I have some even more exciting news for you: http://gentoo-wiki.com/HARDWARE_Apple_MacBook"
    author: "Erik"
  - subject: "Re: Looking forward to it."
    date: 2007-01-21
    body: "Yeah, that's nice too.\n\nHowever I do like OSX, just missing a few high quality apps ;)\n\nBut maybe you can point me to the one feature I actually needs from OSX, encrypted home-dir (in apple speak, FileVault). All apps I need is available under Gentoo, and it will make my job as a developer a lot easier since we develop on Linux in-house. \n\nFileVault is a feature that mounts an encrypted image-file containing my home-dir when I login, inaccessible without the correct password, on logout it is of cause umounted.\n\nI know that Linux can mount encrypted images/partitions, but I need something that works just as well as FileVault. I think that pam can do it with the right extensions, but I've failed to find a guide for setting it up."
    author: "m_abs"
  - subject: "Re: Looking forward to it."
    date: 2007-01-22
    body: "<i>A really great music player and manager, iTunes is IMO just terrible, so I hope the amaroK team will make amaroK 2.0 available for OS X.\n- kopete, for multi protocol IM.\n- A good filemanager with network (fish, ftp etc) capacity (konqueor) so I can use easily transfer files over network.\n- And a lot of small games to pass the time in the two hours I spend on train each day to and from work.</i>\n\nMusic player: If not iTunes then maybe Songbird, Cog or maybe VLC with a cool skin?\nIM: Why Adium of course! There are even ports of Adium skins and extras to Kopete  :)\nFile magager: Well... For FTP there's allways Cyberduck. I'ts agreed that the built in File manager in OS X is outdated. Let's hope for a new one in OS 10.5!\n\nCheck out this list for other open source mac apps: http://www.opensourcemac.org/\n"
    author: "daniel.bagge"
  - subject: "Fantastic!"
    date: 2007-01-22
    body: "I think its fantastic that KDE applications be portable to Windows and Mac OSX, because at the moment if someone considers making an application for Linux/Unix, it means it (generally) won't be available on more popular platforms without adding some layer of abstraction like XUL or actually fork/port it. Now that will no longer be true! This is therefore a huge stepping stone in promoting software development on open platforms.\n\nI just hope its easy to make sure a KDE app is sexy and integrated on all three platforms. This raises a few other questions as well. What happens to KControl and the configuration that it sets on these platforms? Will the printing and file management interfaces still work with native dialogs while appearing as one to the developer?"
    author: "Schalken"
  - subject: "this is a good step toward linux KDE adoption "
    date: 2007-03-14
    body: "If people become comfortable using the greatness that is KDE apps under OS X they are more likely to try it out on their PC. Also I must say this made my day. I love kde apps like digikam and amarok and I use them religiously on my linux/gnu desktops. But to put it simply os x has better wireless support for when I'm on the move. I've been wanting to port, or see some one port these apps to OS X, because I hate the X11 sweet for the mac platform. Keep up the good work I'm looking forward to KDE 4."
    author: "devon gleeson"
  - subject: "Kile"
    date: 2007-04-27
    body: "Any chance of Kile being ported. Honestly, I can't live without it."
    author: "Iano"
  - subject: "Re: Kile"
    date: 2007-06-11
    body: "I, too, would really appreciate having Kile ported. For me, it is really the only reason to run KDE on my Mac. Kile is also really the most useful of the TeX/LaTeX editors that is out there, having features that its little brother TeXmaker doesn't have. I have tried to get it going myself, but I don't have the Mac  programming skills to succeed."
    author: "david Waites"
  - subject: "Re: Kile"
    date: 2007-06-19
    body: "Another guy who thinks Kile is the best LaTeX IDE and can't live without it :|"
    author: "iStarTAC"
  - subject: "Re: Kile"
    date: 2007-09-03
    body: "you have a kde3 port of kile in mac fink unstable packages, however it still lacks some integration with my latex installation.\n\n\n"
    author: "Hugo Pacheco"
  - subject: "Re: Kile"
    date: 2008-03-24
    body: "Also the clipboard integration doesn't work pre-KDE4.\n\nSo copying and pasting text between OS X applications and X applications, and consequently between OS X and KDE3 does not work. This is a huge pain with Kile.\n\nI'm under an impression that KDE4 should fix the clipboard integration, since it is not based upon X11 anymore.\n"
    author: "Tero Keski-Valkama"
---
Just because KDE has been designed to be portable across Linux, 
<a href="http://www.freebsd.org/">FreeBSD</a> and other UNIX/<a 
href="http://x.org/">X11</a> environments for an age now, doesn't 
mean we aren't up for the occasional challenge. With version 4, <a 
href="http://www.trolltech.com/">Trolltech</a> released Qt for the 
Mac, Windows and now even embedded environments under the GPL.  
Since Qt is the base upon which KDE is developed, KDE is now free to 
offer native support for these platforms. Today I am focusing on the KDE/Mac developments for KDE 4. Read on for the details.






<!--break-->
<p>Before I begin, I'd like to discuss some branding issues that KDE has had to face in previous releases. With KDE 3, does the term "KDE" refer to the Desktop Environment (KWin, Kicker, kdesktop, etc.) and if so, would it still be reasonable to refer to the <a href="http://en.wikipedia.org/wiki/Mac_OS_X">Mac OS X</a> port as "KDE" even if it was without these key desktop components? Or, is "KDE" a reference to the project as a whole, in which case we can still call Konqueror a "KDE application" whether it is running on Mac, Windows, or in <a href="http://www.enlightenment.org/">Enlightenment</a>...</p>

<p>These are some branding issues that have been <a href="http://spreadkde.org/branding_meeting/branding_kde4">discussed for KDE 4</a>. The resulting decision has been to refer to "KDE" as the umbrella that refers to all things KDE. So, as a result, we have KDE Applications, the KDE Development Environment (libraries and technologies), and the KDE Workspace (this consists of KWin, <a href="http://plasma.kde.org/">Plasma</a>, etc.) that refer to the three main components that make up the collection of KDE software. When referring to just "KDE" by itself, you therefore refer to all things KDE.</p>

<p>This also solves some issues with alternate release schedules for some applications. For example, <a href="http://amarok.kde.org/">Amarok</a> has often released on a different schedule from KDE, and so some people have not considered it as part of KDE proper. Amarok for KDE 4 will be more clearly labelled as a "KDE Application" without the related implications of it being restricted to a given desktop environment. In KDE 4, Amarok is a KDE product, even if it is based on its own release cycle. As Amarok lead developer Mark Kretschmann says, "If Amarok convinces more people to use KDE technologies, that's perfect. If they use Amarok under other desktop environments like GNOME or Mac OS X, that's still nice for us."</p>

<p>Since this article deals with KDE on other platforms than X11, we need a way to distinguish between KDE for X11 and KDE for Mac. So, with the approval of some of the developers, I'm going to use the following terminology: KDE/X11 will refer to all KDE Applications that run on X11, the Development Environment on X11, and the KDE Workspace on X11. Likewise, KDE/Mac will refer to KDE applications that run on the Mac, the KDE Development Environment on the Mac, and since the workspace is not required on the Mac, it is not included here. Ditto for Windows. It is important to realize, however, that these distinctions are based on a subset of features that are available on each platform, and that for the most part the <a href="http://websvn.kde.org/">source code</a> is identical, and there are not separate source trees for each platform. There are no forks or haphazard ports.</p>

<p>New KDE Development Environment technologies like <a href="http://phonon.kde.org/">Phonon</a> and <a href="http://solid.kde.org/"> Solid</a> will help ease this transition, since the platform-integration work will happen at the library level. KDE Applications need not be aware of the differences that might exist in the OS platform.</p>

<h3>What is KDE/Mac?</h3>

<p>KDE/Mac is the collection of KDE Applications that run natively on the Mac, including the underlying technologies, libraries, etc. that make these KDE Applications work. There are only a few differences between KDE/X11 and KDE/Mac. Most notably, the KDE Workspace elements, such as KWin and Plasma are not present for the Mac. The reason for this is that the functionality of KWin and Plasma already exist in OS X in one form or another, and reimplementing them would not allow for close integration of KDE Applications alongside existing Mac Applications. So instead, KDE has elected to not port the KDE/X11 Workspace to the Mac.</p>

<p>Since the beginning of KDE, its applications have been designed in such a way as to peacefully co-exist in other UNIX Desktop environments. Initially this meant <a href="http://www.windowmaker.info/">Window Maker</a>, and then later GNOME and Enlightenment. The KDE Applications use shared standards (such as those crafted at <a href="http://freedesktop.org/">FreeDesktop.org</a>), capable of sharing clipboard data, system tray icons, and so on without any issues on these compliant platforms. Now, because of the additional portability that Qt 4 offers, this same peaceful co-existence will also be true for non-X11 environments, such as the Mac.</p>

<p>KDE apps have been able to run on the Mac before, using <a href="http://www.apple.com/macosx/features/x11/">Apple's X11</a> server layer that is built into OS X - however, KDE was still using Qt/X11 and as such, these applications simply looked identical to how they would look in good old KDE/X11 proper. The fact that they ran at all is thanks to the excellent efforts of the <a href="http://fink.sourceforge.net/">Fink</a> project. If you're interested in running other UNIX programs on OS X, check this project out.</p>

<p>(For a time there was also a Qt/Mac Freeware edition which allowed some experimentation with a true KDE/Mac experience within the KDE 3.x series. However, The KDE/X11 version in Fink was usually used due to its stability.)</p>

<p>Here's a screenshot of a KDE application as it looked using KDE 3.5 and the Fink project's packages. Click for the full version.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol3_3x_large.png"><img width="350" height="263" src="http://static.kdenews.org/dannya/vol3_3x_small-wee.png" alt="KDE 3.5.4 on Qt/X11 for Mac"></a></p>

<p>Because the above was developed for Qt/X11, the whole KDE environment was able to run. The downside, however, was that it did not integrate well with the Mac, so it felt like you were running two totally different computer systems on one screen. Of course, KDE is all about the integration...</p>

<br>

<p>KDE 4 brings great advances to the Mac porting effort, thanks mostly due to Qt 4, and the new KDE build system, based on <a href="http://www.cmake.org/">CMake</a>. For KDE 4, the KDE/Mac applications are downloadable as standard issue, Mac .dmg files, available from the <a href="http://ranger.users.finkproject.org/kde/index.php/Home">KDE on Mac OS X</a> website. The KDE/Mac development snapshots are among the easiest to use for any platform, thanks mostly due to the great work done by KDE/Mac guru Benjamin Reed (a.k.a. RangerRick). Visit #kde-darwin on irc.freenode.org to help report and fix anything you see that is broken. This is pre-alpha KDE 4, so it's likely to crash until more development has taken place.</p>

<p>Once the downloaded packages are unpacked and installed, KDE/Mac applications can be launched using OS X's Finder as shown here:</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol3_mac_finder.png"><img width="350" height="189" src="http://static.kdenews.org/dannya/vol3_mac_finder-wee.png" alt="KDE 4 apps listed in Finder"></a></p>

<p>As you can see, quite a wide variety of applications are already available for the Mac, including most of the official KDE applications. Because this is a development version, some things are still broken (like any apps using <a href="http://en.wikipedia.org/wiki/SSL">SSL</a>) and some things are just plain ugly. But the same is also true of many KDE 4 apps on KDE/X11 at this point, so expect both offerings to improve throughout the KDE 4 development timeframe.</p>

<p>At the same time, some very important things have occurred in KDE/Mac integration. For example, the clipboard works. Keyboard shortcuts work. Extended language input works. Some things, like drag-and-drop remain sketchy. The KDE/Mac developers would welcome any fresh talent that knows a little about KDE and Mac technologies to help solve smaller issues like this.</p>

<p>Here's what you've been waiting for: the screenshot tour of a few KDE 4 apps that are working on the Mac to show off some of the progress so far. Some of these apps will surely be greatly appreciated by Mac users.</p>

<br>

<p>Since we've covered SVG in the <a href="http://dot.kde.org/1167723426/">first issue</a>, I figured I would show a shot of SVG-goodness in action on the Mac. Below is Shisen Sho, a tile game that shares loadable SVG tilesets with KMahjongg, which was previously featured. This game looks beautiful on the Mac, and not at all out of place.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol3_mac_kshishen.png"><img width="350" height="283" src="http://static.kdenews.org/dannya/vol3_mac_kshishen-wee.png" alt="KDE 4 game Shisen Sho on Mac OS X"></a></p>

<br>

<p>One of the main questions asked last week about KOffice was support for other platforms. I'm pleased to say that KWord, KSpread, and the rest of the KOffice suite should run on KDE/Mac just fine. In the development version I tested, KWord performed pretty much as the KDE/X11 version from <a href="http://dot.kde.org/1168284615/">last week</a>. Additionally, I tried loading a few other KOffice applications to see if they work. Here's a shot of KSpread's Wizard, and the KDE 4 file dialog on the Mac.</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol3_mac_kspread.png"><img width="350" height="293" src="http://static.kdenews.org/dannya/vol3_mac_kspread-wee.png" alt="KOffice 2 app KSpread on Mac OS X"></a></p>

<p>Also notice how the KSpread icon shows up in the OS X dock. This didn't previously happen with KDE on Fink (The icon to the left of it is for kded, one of the KDE background processes, which will eventually be adjusted to hide itself when running on the Mac).</p>

<br>

<p>Of course, the real question everyone is asking is: does <a href="http://www.konqueror.org/">Konqueror</a> work? Yes. The KDE 4 version of the Konqueror application is mostly a port of Konqueror from KDE 3.5 at the moment, but the backend libraries such as the <a href="http://khtml.info/">KHTML</a> rendering engine and Javascript support have seen a lot of fixes and improvements. On the Mac, due to the OS X user interface styling in Qt 4, we get centred tabs during tabbed browsing, as seen below:</p>

<p align="center"><a href="http://static.kdenews.org/dannya/vol3_mac_konq.png"><img width="350" height="248" src="http://static.kdenews.org/dannya/vol3_mac_konq-wee.png" alt="KDE 4's Konq on Mac OS X"></a></p>

<br>

<p>The Mac has made a name for itself as a premier Desktop for graphical and multimedia applications. Unfortunately, the KDE Graphics package was not included in this snapshot so I can't show it off.</p>

<p>However, another niche that the Mac proudly occupies is that of education. Below are two of the great applications from the <a href="http://edu.kde.org/">KDE-Edu project</a>: <a href="http://edu.kde.org/kalzium/">Kalzium</a> and <a href="http://edu.kde.org/kstars/">KStars</a>. New features that are showing up in KDE-Edu will be featured in a later article... for now, it's just important to note that these superb applications will be available and fully functional for KDE/Mac.</p>
<p align="center"><a href="http://static.kdenews.org/dannya/vol3_mac_kalzium.png"><img width="350" height="208" src="http://static.kdenews.org/dannya/vol3_mac_kalzium-wee.png" alt="KDE-Edu's Kalzium on Mac OS X"></a></p>
<p align="center"><a href="http://static.kdenews.org/dannya/vol3_mac_kstars.png"><img width="350" height="361" src="http://static.kdenews.org/dannya/vol3_mac_kstars-wee.png" alt="KDE-Edu's KStars on Mac OS X"></a></p>

<br>

<p>And now that the tour is done, a couple of other things that come up whenever KDE is discussed on other platforms.</p>

<p>While I was researching this article, I encountered some people that object to running KDE apps on a non-free platform, with sentiments on IRC alike to "Anytime you run OSS on a non-free platform, God kills a puppy. A cute puppy."</p>

<p>But KDE has good reasons for supporting other platforms: attracting developers, and fostering interoperability and standards. There are a huge number of Mac and Windows developers out there and therefore a large amount applications that can take advantage of KDE technologies. The best example of a KDE technology benefitting from exposure to other platforms is KHTML/<a href="http://webkit.org/">WebKit</a>. As a side effect, since there are so many users of KHTML-based browsers now, websites have had to improve their standards compliance in order to be more compatible, which means more websites work in Konqueror. This will hopefully also happen with OpenDocument and <a href="http://www.kolab.org/">free software groupware systems</a> as KOffice and <a href="http://www.kontact.org/">Kontact</a> push onto other platforms.</p>

<p>In the meantime, have a look at <a href="http://commit-digest.org/issues/2007-01-14/">yesterday's KDE Commit-Digest</a> for some information on the progress of Windows platform support.</p>











