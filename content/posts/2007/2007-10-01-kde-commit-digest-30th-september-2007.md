---
title: "KDE Commit-Digest for 30th September 2007"
date:    2007-10-01
authors:
  - "dallen"
slug:    kde-commit-digest-30th-september-2007
comments:
  - subject: "KSensors anyone?"
    date: 2007-10-01
    body: "It seems like this project is completely abandoned and it'll be pity if KDE4 lacks such a useful and important application.\n\nSo, who will volunteer porting KSensors to KDE4?"
    author: "Artem S. Tashkinov"
  - subject: "Re: KSensors anyone?"
    date: 2007-10-01
    body: "well.. just a guess... but probably there'll be some plasmoids for stuff like this?"
    author: "Thomas"
  - subject: "Re: KSensors anyone?"
    date: 2007-10-01
    body: "yeah, with the help of solid, this might not be too hard..."
    author: "jospoortvliet"
  - subject: "Re: KSensors anyone?"
    date: 2007-10-01
    body: "I just hope people wont forget on it :)\n\nIts what makes KDE also great... the small and useful apps!"
    author: "she"
  - subject: "Re: KSensors anyone?"
    date: 2007-10-01
    body: "Solid and Plasmoids. Problem solved."
    author: "Segedunum"
  - subject: "Kickoff"
    date: 2007-10-01
    body: "well, how does it come that I most of the usability \"improvements\" look to me like obstructions?\n\nI just looked at the Kickoff page and the video ... a bit of criticism (I know this is not the right place to discuss, but as I see aKademy 200_6_, I guess the battle is lost already ...)\n\n- I do not understand the \"favorites\" improvement - why should I click some menu, when I can have my favourite applications on panel, accesible via 1 click? (or, at the desktop, which is the thing that 90% GUI users I know prefer ...)\n\n- accessing \"non-favorite\" applications is horrible! ... after clicking menu, you have to click that you want to really select the menu, then if you are not sure in which category the application is, you are clicking forth and back like an idiot to browse it, instead of simply pointing the cursor and watching the menus unroll (still seeing the previous menu levels unrolled), just like with the current style\n\n- grouping session options (logout etc.) is a good idea, but once again: why to force additional clicking?"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> - I do not understand the \"favorites\" improvement - why should I click some\n> menu, when I can have my favourite applications on panel, accesible via 1\n> click? (or, at the desktop, which is the thing that 90% GUI users I know prefer ...)\n\nAre you saying you believe that having a new menu is incompatible with having the favourites on panel or at the desktop? I wonder who taught you the basics of logic.\n\nIn the first screenshot on the link page I see the ordinary panel with shortcut buttons. This menu is just a new menu."
    author: "Eeli Kaikkonen"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "thankyou for the invective - and I wonder who taught you to read ...\n\nI am not saying anything about those two methods of accessing favourites being incompatible; I just say that I do not need them both at once, and the slower one of these two should be dropped (er, not developed at all), especially if that method slowers my work with normal applications menu"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "I've been using kickoff for quite a while now, and what you may not understand from a video is that it's a one-click solution for every nearly item in the menu. When you hover over the kicker button it opens, and when you hover over the tabs they change, so you only need to click on the final app/action. Only within the \"All Programs\" menu you need to click on the submenus, so that's exactly where the favorites tab comes in handy :)"
    author: "bsander"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> When you hover over the kicker button it opens\n\neven worse than I expected ...\n\n> Only within the \"All Programs\" menu you need to click on the submenus,\n> so that's exactly where the favorites tab comes in handy :)\n\nbut you cannot (well, theoretically you can but it is impractical) have everything within favourites - it's like saying \"oh, look at this shiny new car you can buy for commuting, the only thing is that it does not go, but you can move closer to your workplace, so you do not need the car to move\""
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-03
    body: "Come on genius, favorite menu, it's just for your most use application. In most cases they don't exceed 8 applications, so the favorite menu is more than enough, and when you want some non favorite app, you just search it on the SEARCH BOX or browse it, if you like it that way.  "
    author: "Luis"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> and when you hover over the tabs they change, \n> so you only need to click on the final app/action\n\nNot for me on my openSUSE system. The only thing that opens without clicking on it is the menu itself, and that doesn't even do it consistently (seems to mostly do it when I don't mean to click on it actually...\n\nWeird, it suddenly started switching tabs without me clicking on anything (after about 5 tries of seeing if I was just crazy or not), but it still wouldn't go into any submenus without clicking. Maybe it's just a bug on my system, but it acts very weird and unfriendly for me..."
    author: "Sutoka"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> I do not understand the \"favorites\" improvement - why should I click some\n> menu, when I can have my favourite applications on panel, accesible via 1\n> click? (or, at the desktop, which is the thing that 90% GUI users I know\n> prefer ...)\nBecause you can't have 12 applications in the panel (that is the number of\napps I have in my favourites) unless you have a gigantic screen. The one click\nargument is flawed as well: most of the time you will have one maximized\nwindow or windows covering the desktop icons, this means that to get to the\ndesktop you need to minimize/move the windows which is done by at least one\nclick. Besides that, you don't even need the click to open the menu, just move\nyour mouse in the lower right corner (assuming the menu is there) and it will\npop up immediately.\n \n> accessing \"non-favorite\" applications is horrible! ... after clicking menu,\n> you have to click that you want to really select the menu, then if you are\n> not sure in which category the application is, you are clicking forth and\n> back like an idiot to browse it, instead of simply pointing the cursor and\n> watching the menus unroll (still seeing the previous menu levels unrolled),\n> just like with the current style\nThis is why the menu has a nice big search bar, which has focus all the time\nso you can simply start typing. I don't recall the last time I had to \"click\nback and forth\" to find something.\n \n> grouping session options (logout etc.) is a good idea, but once again: why\n> to force additional clicking?\nBecause you need this about once every 8 hours, the additional click is not\ngoing to hurt you. What would be more annoying is to have to skip over these\n4-5 buttons every time you want to access some other icon. If you don't like\nthis behaviour, just add them to your favourites and they will be just one\nclick away. Also again, the \"click\" is not really needed to open the\nmenu/submenu, just move your mouse over it and it will pop up instantly.\n\nHave you actually ever used the menu, or just looked at the video? I was\nskeptical at the beginning too, but after 1 minute spent adding favourites and\nanother hour getting used to the new menu I turned into a raving fanboy, this\nthing is (almost) the best thing since sliced bread!"
    author: "Luca"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "I agree completely. Since I set up my \"Favorites\", I have *never* used the \"Applications\" area.  I love the \"History\" and \"Computer\" tabs!!  When I need an application that is not in one of these areas, I always use the search.\n\nFor the people who like their menu to look like Windows 95, I'm sure you will be able to revert to the old menu as in Suse.  The rest of us are living in 2007 and also want our computers in 2007.\n\nThere are always gonna be people who prefer the old way of doing anything, especially \"Old Dogs\".  There is nothing you can do about that.  Just look at all the old unix people who still hate GUI's.  When I started using Linux in 2001, people were actually pushing \"Lynx\" because the only alternative was a buggy Netscape Navigator and the \"evilness\" of images.  When images caught on, people were told to disable Javascript because it was unnecessary.  HAHAHHAHAHA!!!\n\nBTW, I'm not trying to be critical of others.  Now that I've gotten older, sometimes I don't like to \"learn new tricks.\"  Just human nature.\n\n"
    author: "Henry S."
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "sorry, but you missed the point\n\nI am not talking about learning new things - I am talking about what it takes to do a simple action ... and from the presentation, the new system is much worse for that\n\nI do not have any objections to using whatever you like (favorites, search, javascript, whatever) as long as it does not make harder using other things - unfortunatelly, this is the case\n\nbtw,\n\n> Since I set up my \"Favorites\", I have *never* used the \"Applications\" area.\n> I love the \"History\" and \"Computer\" tabs!! When I need an application that\n> is not in one of these areas, I always use the search.\n \nok, that is your usage, but it does not suit my needs - a simple use-case:\n\nI have installed tenths of games\nit does not make sense having everything in Favorites - it would take quite a time to browse 50+ items\nsometimes, I want to relax and play \"something\" without knowing exactly what is going to attract me - so I cannot just type something in the search\nthen I use the applications menu and just browse until something catches my eye\n\nmaybe, when you get older a bit more, you will find that sometimes the good old things work best and do not need to be replaced just because we live in 2007\n\np.s. Windows 95? er, Apple menu does not count?"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "There is always a tradeoff.  Although I have no proof, from my observations it is much more common to launch the top 5 or so apps, so having that easily accessible is of higher priority than having an easily browseable menu.  While you might browse your menu once in a while, looking for an application that catches your fancy, I hardly think that's a common task.  \n\nThis gives me an idea though.  Is it possible to search for menu groups in kickoff?  So if you want to see all the games, can you enter \"games\" into the search bar or does it just search application names?"
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "well, as i said allready on roberts blog: search and favorites don't depend on a non-functional menu... you could add a real menu to kickoff just fine, without losing those new abilities.\n\n\nso, does it make sense to cripple normal-menu users, just because of nothing (or aesthetics)?\n\nwriting games into a textbox is still much slower than just clicking games in a menu. a list of 50+ games without any directories is also nothing a would like search to find what game i would like to play today...\n\n\nand i don't think using the menu for what its made for (browsing your applications!) is a minor usecase..."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> writing games into a textbox is still much slower than just clicking games in a menu.\n\nMaybe if you are a very slow typist.  With incremental search you'd probably not even have to type the whole word.  \n\n> and i don't think using the menu for what its made for (browsing your applications!) is a minor usecase...\n\nSince when is a menu made for browsing applications?  That is a one use case, but I doubt it is the primary one (more like launching applications).  Watch some regular users using the Windows start menu sometime.  Usually they know what they want to run before opening the start menu."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "thats interesting. didn't you say there are better ways to use kickoff if you allready know what you want to start? so what do you think is the application menu in kickoff good for?\n\n\nmaybe you should watch some regular users type \"games\" (ore \"gam\" for that matter, if you don't want to type the whole word ;))... for most users the keyboard is not of much use, except for typing longer text in it. thats why we use guis these days ;)."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "\"for most users the keyboard is not of much use, except for typing longer text in it. thats why we use guis these days ;)\"\n\nYeah but\n\na) I am also not a \"typical\" user\nb) I like to type things\n\nI use 9000 aliases in my shell. I use them to navigate to my \ndirectory structure with 1-5 chars, or to open documents and so \non and so on.\n\nFor me, konsole ROCKS. Thats why I dont care much about the GUI.\n\nBut my old parents, they really need to use the GUI, and for \nTHEM I do test things in the GUI world. \n\nWhy cant the GUI world *also* focus on \"power users\" \nthat DO have better ways? I dont like any attitude that \nsays \"we exist only for the stupid users\" .... :(\n\n\nPS: Just to make it clear I think KDE rocks."
    author: "she"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "maybe you should read my comments more carefully... i never said i want the search feature gone."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> didn't you say there are better ways to use kickoff if you allready know what you want to start?\n\nYes.\n\n> so what do you think is the application menu in kickoff good for?\n\nBrowsing apps if you don't know what you want to start.  Or launching apps if you prefer to use the mouse (although most of the time your most used apps will be in the favourites or history.  The menu of all applications is really just a last resort.\n\n> maybe you should watch some regular users type \"games\"\n\nI would hazard that even for hunt and peck typists, typing a few letters is faster than finding their application given a screen full of 50 apps and then selecting it.  You are right in that people aren't used to it, but I believe it is faster anyway.  Proof is another matter though."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "well, its faster if you can type fast. thats probably me and you, but not a normal user. and it shouldn't be. a search feature is all nice, but in many cases it just can't replace a fast menu. so why not have search AND a fast menu?\n\n\nbut your usecase is kind of scary anyway ;). no menu implementation will work good with a plain list of 50 applications. thats why many (not all, don't get me wrong again, please ;)) \"hardcore\" windows users don't use their start menu at all. all those installers creating new toplevel directories are a real pain."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> sometimes, I want to relax and play \"something\" without knowing exactly\n> what is going to attract me - so I cannot just type something in the\n> search then I use the applications menu and just browse until something\n> catches my eye\n\nWell, you can do that with the (IMHO wonderful) Application tab."
    author: "Capit. Igloo"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "> Well, you can do that with the (IMHO wonderful) Application tab.\n\nerr ... this is what I criticise about Kickoff - using Kickoff, I cannot access that menu directly (the first bad thing) and the new style is worse, since I have to click (or put the mouse on the right place if set to hover action) forth and back to browse the menu, I cannot simply move over categories to unroll them, to see it together\n\nbtw, the second thing is exactly the same problem as I see in the new SystemSettings interface - and I find it so annoying that I hired a programmer to port kcontrol to KDE4 (hope we will see it finished soon ;-)), because in kcontrol I have the overview and I do not have to click forth and back (imagine an example - you are doing something and with kcontrol style, you can have all the tools around you at the table, if you want to use something, you simply take it ... with SS style, you can only have one tool at a time, when you want to use another, you have to put the first tool to its place in the closet ... clear?)"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "> and I find it so annoying that I hired a programmer to port kcontrol to KDE4 (hope we will see it finished soon ;-))\n\nReally?  Awesome.  I also am not a big fan of system settings.  When you know what you want it's fine, but sometimes I don't know exactly where a setting will be and I need to open a few.  That's much slower with system settings.."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-03
    body: ">> and I find it so annoying that I hired a programmer to port kcontrol to KDE4\n> Really?\n\nyou can track the issue here: http://burza.m4r3k.org/view.php?id=10 ... if you understand Czech/Slovak ;-)"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> Because you can't have 12 applications in the panel\n> (that is the number of apps I have in my favourites)\n> unless you have a gigantic screen.\n\nactually, you can ... I've seen people with one half row full of application shortcuts (about 30), but this is matter of personal taste\n\nif 12 is too much for you, then you can replace it with one button and you will have the same functionality - no need to destroy applications menu because of this\n\n> The one click argument is flawed as well: most of the time you will\n> have one maximized window or windows covering the desktop icons,\n> this means that to get to the desktop you need to minimize/move\n> the windows which is done by at least one click.\n\nplease, do not confuse what I write - I did not say that desktop access is equal to quick launch in number of clicks ... I just said that I know a number of users who prefer to find the application icon on desktop then to use quick launch or menu (well, it's been years since we have the \"most used\" list ...) - some of them even minimise the applications one after another instead of clicking some \"show desktop\" button (!)\n\n> Besides that, you don't even need the click to open the menu,\n> just move your mouse in the lower right corner (assuming the\n> menu is there) and it will pop up immediately.\n\nwhich is even worse - so that the people who do not handle the mouse well(*) will have randomly appearing and dissappearing menu ... well, I guess this is going to be configurable since we are not talking about Gnome, but the default is what matters ...\n\n(*) or using notebook with broken touchpad ... on some models, only a little humidity (sweat) suffices to cause the cursor jump here and there\n\n> This is why the menu has a nice big search bar, which has focus\n> all the time so you can simply start typing.\n\noh great! - so I have to remember the name of each application (or have a crystal orb implemented in my brain so that I can prophesy what keywords had the author in mind) ... then why not simply hit alt+f2 and run the desired application directly, why to have any menu at all?\n\n> Because you need this about once every 8 hours,\n\nI wish I had _your_ certainity about what _I_ need ...\n\n> the additional click is not going to hurt you. What would be\n> more annoying is to have to skip over these 4-5 buttons every\n> time you want to access some other icon.\n\nno, once again you are misinterpreting ... if that is grouped, you need to skip one button, no 4-5\n\n> Also again, the \"click\" is not really needed to open the\n> menu/submenu, just move your mouse over it and it will pop up instantly.\n\nalso, again, this argument is silly, one reason stated before and the second one is that if you need to point you cursor somewhere and hold it there so that the system notices, then there is no difference if you need to click or not (from the \"obstructions\" point of view; the difference is that I do not want the computer to try to guess my thoughts or react on unintentional moves, I want it to react on clear order)\n\n> Have you actually ever used the menu, or just looked at the video?\n\nhave you actually tried to read and _understand_ my previous comment?"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Just so you know, most of your logic is wrong...but that would happen when criticizing something you've never used.  I doubt anything will convince you otherwise, just based on your tone.  But here it goes anyways:\n\n1.) Traditional menus use \"on hover\" to open submenus which often cause \"randomly appearing and dissappearing menu.\"  The Kickoff doesn't have \"randomly appearing and dissappearing menu\" because it doesn't have submenus...only the window contents change.  However, if you make a mistake, you can fix it instantaneously, no different than a regular menu.\n\n2.) There is no need to \"point you cursor somewhere and hold it there,\" it is actually instant where it is used, again, just like the \"on hover\" of any menu.  Think of the tabs as vertical menus and you'll see that it is really the same thing, just vertical and grouped differently.  The way the \"Applications\" menu works is the only fundamentally new interface concept.\n\n3.) I have a terrible, terrible touchpad on this laptop...Kickoff actually works better because there are fewer motions.  I've always had problems with the touchpad and trying to go perfectly horizontal into a submenu...cuz if you drift tot high or too low the submenu closes before you get there!  Pay attention to the horizontal motion required by a traditional menu and you'll see how it is actually tricky for motion impaired people and crappy touch pads!  You've just learned to deal with it, so it feels \"normal\" to you.  Kickoff actually has this same problem in that you have go stay within the main tab as you move vertically, otherwise one of the ones next to it may activate.  But it has much more tolerance than a normal menu because it is a big square instead of a thin row.\n\n4.) Regarding the comment about \"I wish I had _your_ certainity about what _I_ need ...\".  The people behind kickoff actually did studies to find how to reduce the number of clicks in the majority of cases for the typical user.  That means it is  weigted towards making the applications you use, say 90% of the time, available in a single click(Favorites, History, Leave), and the rest require a little more work.  No desktop software is designed to be perfect for _you_, they are designed to be great for the typical user...if you aren't typical, then KDE is very customizable.  However, if you used kickoff for awhile, it may turn out that other people did know what you needed better than you did (although you may not want to admit it).\n\n5.) As I said before, KDE is trying to move into the future.  It can't keep behaving like Windows 95 just because that was cool 12 years ago.  Don't get me wrong, I don't think we should change things for the sake of it.  I wasn't crazy about Kickoff until I actually used it.  That being said, some folks may not like it...you can't please everybody with _defaults_, some will have to settle with configuring some options.\n\nNone of my comments above are meant to address the popping up of the menu when hovering over the \"Start Button.\"  This part of Kickoff does actually require a \"hover and pause\" in the Suse 10.2 version...although you can choose to click if you want.  However, this isn't so much a feature inherent to Kickoff...I believe it should be used on all start menus.\n\nRegardless whether you agree to the details of Kickoff...just give it an honest try before criticizing it.  It is not perfect...but it is the best thing out there for most people.\n\n"
    author: "Henry S."
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "thankyou for your effort put in the answer, but I would be happier if you would rather put it in thoroughly reading what I have written and in trying to understand that\n\nthere is interesting topic about the mouse movement needed, what is easier to achieve for impaired, but this is in accordance with my thoughts about \"click to do action\"\n\nhowever, I cannot seriously answer a post which begins \"your logic is wrong\" without pointing to anything that can be undoubtedly denied ... \"just based on your tone\" you would not listen to anything which does not agree with your opinion, as you prove by repeating your \"Windows 95\" dogma although at another place, I tried to tell you that I do not just copy Windows 95 look\n\np.s. sorry for the mistakes in English, I am not sure about \"would\" and tenses usage; I hope the text above makes sense ..."
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: ">> Have you actually ever used the menu, or just looked at the video?\n \n> have you actually tried to read and _understand_ my previous comment?\n\nAnswer the question.  If you haven't used it, then why are you still trying to convince people that _have_ used it that it is bad?  You don't even know if it's bad or not, you are just guessing based on a video.  Try it, and if you still don't like it after giving it an honest shot (a couple days of use), then you have a case.  Until then it's just speculation."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: ">>> Have you actually ever used the menu, or just looked at the video?\n>> have you actually tried to read and _understand_ my previous comment?\n> Answer the question.\n\nI am convinced the answer can be deduced from my first post ...\n\n> If you haven't used it, then why are you still trying to convince people\n> that _have_ used it that it is bad?\n\nplease go back to my first post - does a sentence like <EM>'most of the usability \"improvements\" <STRONG>look to me</STRONG> like obstructions'</EM> say something about Kickoff being universal evil and everybody has to dislike it?\n\n> You don't even know if it's bad or not, you are just guessing based on\n> a video. Try it, and if you still don't like it after giving it an honest\n> shot (a couple days of use), then you have a case. Until then it's just\n> speculation.\n\nwell, I have seen a video of man falling of the roof (quite high) and he died ... is it a speculation that I'll be probably hurt a lot (or die) when I fall from some high place? - do I have to actually try it a few times?"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "So your answer is ...\n\n...\n\nNO.\n\nTry it. I found that it changed my way of accessing applications.\n\nI have rarely looked through the Applications tree. It is far simpler to type a search term. It does a search. You don't have to type the whole name.\n\nI still use application buttons on my taskbar, but quite a few less.\n\nMaybe you have some ideas. How do you handle the very large number of applications available, allow access to them all, at the same time not forcing people to scroll through hundreds of choices to find what they want?\n\nDerek"
    author: "D Kite"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "\"Because you can't have 12 applications in the panel (that is the number of\napps I have in my favourites) unless you have a gigantic screen.\"\n\n- this is not true, i have about 15 app icons on panel (3x5) and it automatically modifies some according to usage.. so i use menu only rarely\n\nis kickoff somehow related to kickers' new launcher? \n"
    author: "mdl"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "I, e. g., use two kicker bars (one on the top for the favourite applications & status information and one on the bottom (task bar)) and can have many, many applications in the bar.\nI also agree that there is really NO need to put favourite applications in the menu and hide all the programs behind a \"all programs\"-button like in windows xp. This REALLY sucks"
    author: "Bobby"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> Because you can't have 12 applications in the panel (that is the number of\n> apps I have in my favourites) unless you have a gigantic screen. \n\nOff course you can, just use smaller Icons. Also, Screenspace is'nt expensive any more. Today, every stupid Laptop has axis with 1200 and more pixels, enough for ~20 big Icons.\n\nAlso, if you use something like a taskdock, which reuse the icon to show the status of running applications, there wo'nt be any wasted screen.\n"
    author: "Chaoswind"
  - subject: "Re: Kickoff"
    date: 2007-10-03
    body: "I'm seeing very mixed opinions about this .. and I'm not very technically skilled so I don't know if this would be a big performance hit or anything.. but surely it's possible to have two separate buttons in the system tray? One that leads to a menu suited for browsing applications and another that functions more as a quick launcher? That way we're still not doing too much clicking and, if designed well, it can look quit nice too. \n\nJust a thought.. "
    author: "Rahul"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: ">I do not understand the \"favorites\" improvement - why should I click some menu, >when I can have my favourite applications on panel, accesible via 1 click? (or, >at the desktop, which is the thing that 90% GUI users I know prefer ...)\n\nI do not see how this perceived problem of yours is specific to Kickoff. It does apply to pretty much every menu out there and not everybody likes to have a crowded panel / desktop either. This can happen pretty fast if you have on of the \"old\" 4:3 monitors with a low resolution (for instance 1024x768) and the desktop is competing for space with applications like KNotes.\n\n>accessing \"non-favorite\" applications is horrible! ... after clicking menu, you >have to click that you want to really select the menu, then if you are not sure >in which category the application is, you are clicking forth and back like an >idiot to browse it, instead of simply pointing the cursor and watching the >menus unroll (still seeing the previous menu levels unrolled), just like with >the current style\n\nIf you know the name of the application you can use the integrated search to start the application, no need to bother with the application tab at all. In case you don't know what you're looking for the categories give you at least hints what to expect in each sub-level. If the package is not there complain to the package maintainer for not putting it under the right category.\n\nThe problem with menus unrolling is that it can get very crowded very fast, especially if you have a lot of applications installed (for instance, both GNOME and KDE desktop). In a way I speak from experience: My employer installs about 50 applications by default on our work laptops (don't ask). If the menu unrolls it takes up the whole vertical screen space and it takes ages to look for something.\n\n>grouping session options (logout etc.) is a good idea, but once again: why to >force additional clicking?\n\nWell, I guess it must be because the Kickoff has about half a dozen options for session management. It's a tradeoff between usability and accessibility."
    author: "Erunno"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> If you know the name of the application \n\nThe generic name and keywords are also searched.  I think the description may also be searched.  So you can find Amarok by typing in \"music\" and K3B by typing in \"cd\" or \"dvd\" or \"burn\".  \"card\" will find the available card games.\n\nThere isn't any mimetype searching yet but I think that would be a useful addition.  (eg. Find Amarok by searching for \"mp3\" or \"ogg\")"
    author: "Robert Knight"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "just a note ... Google is becoming useless for me, because it always finds zillions of totally irrelevant things - if you extend the search too much, you can easily get user confused with false hits"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Perhaps a way to solve that would be to weight search results.  Matches on name have higher weight (and thus appear earlier) than matches on thinks like mimetype (if it was implemented).  So typing in mp3 would get you \"MP3Blaster\" before \"Amarok\"."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> I do not see how this perceived problem of yours is specific to Kickoff.\n\nbecause Kickoff replaces the applications menu ... and it adds another Favorites implementation\n\n> If you know the name of the application\n\n... then I do not need any menu at all - do you see the point?\n\n> In case you don't know what you're looking for the categories give you\n> at least hints what to expect in each sub-level. If the package is not\n> there complain to the package maintainer for not putting it under\n> the right category.\n\nno, this is not fair - I cannot blame the package maintainer that he does not have an oracle to tell him where the users will look for something\n\n> The problem with menus unrolling is that it can get very crowded very fast\n\n- I do not understand, how it is solved by displaying a single menu level at a time?\n\nif the last menu level is displayed on top of all the other things, is it somehow less usable then if it is displayed in the same place as the previous menu level?\n\n>> grouping session options (logout etc.) is a good idea, but once again:\n>> why to force additional clicking?\n> Well, I guess it must be because the Kickoff has about half a dozen options\n> for session management. It's a tradeoff between usability and accessibility.\n\ner, it looks like my mistake how does it work; but sill I do not like that kind of division and the way of mouse movement or how to say ..."
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> ... then I do not need any menu at all - do you see the point?\n\nNo.  In the Run dialog, you need to know the exact name of the executable to launch a program (krunner excluded here for now).  So to run OpenOffice Writer, you need to type 'oowriter'.  This is not intuitive.  In kickoff, you need only know part of the name, or the type of application you are looking for.  So you can start typing openoffice, or writer, or document (perhaps, this is a guess) or other keywords pertaining to the application you want.  And because the search is incremental, you only need a few letters to find what you want.  This is not at all the same as the run dialog."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "obviously, you did not try KDE4 ;-) - http://upload.wikimedia.org/wikipedia/commons/d/de/Kde4Beta1.png\n\nyes, this is a bit different ... but I do not want to take your Favorites and Search field away, I just want them not to block (complicate) access to the \"normal\" application menu"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "> obviously, you did not try KDE4 ;-)\n\nThat's why I said \"krunner excluded\".  Krunner is what you see in the screenshot there.\n\n> I do not want to take your Favorites and Search field away, I just want them not to block (complicate) access to the \"normal\" application menu\n\nPerhaps you have a mockup...  Try uploading to kde-look and check the feedback.  Perhaps it will be better and eventually adopted.  I can't imagine a good solution at the moment, but that doesn't mean it doesn't exist."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-03
    body: "> That's why I said \"krunner excluded\". Krunner is what you see in\n> the screenshot there.\n\noops, sorry, it must have been temporary blindness :-)\n\n> Perhaps you have a mockup...\n\nwell, \"Tasty Menu\" looks like it is doing the right thing (I do not know this software, I just looked at the kdeapps page and the screenshot after it was mentioned within this discussion) - the key features that are highlighted in Kickoff are there: the Search and the Favorites, while still having easy access to the application menu tree (I am not sure about how it is displayed when there are more than two nested levels but it would not be hard to imagine to expand to more columns)"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "\"because Kickoff replaces the applications menu ... and it adds another Favorites implementation\"\n\nYou were questioning why menus where necessary at all by pointing out that favorite applications can be started via one click from the panel or desktop. Selective reading is unnerving.\n\n\"... then I do not need any menu at all - do you see the point?\"\n\nNo, actually I do not. If you don't need the menu for known applications why complain about it in the first place?\n\n\"no, this is not fair - I cannot blame the package maintainer that he does not have an oracle to tell him where the users will look for something\"\n\nIf KDevelop is filed under Games/Puzzle the package maintainer is *not* to blame?\n\n\"if the last menu level is displayed on top of all the other things, is it somehow less usable then if it is displayed in the same place as the previous menu level?\" \n\nOn top is even worse than besides the other levels as you'll have to take extra precaution to not click one of the underlying layers and therefore make all following ones collapse.\n\nI still do not see the issue to be honest. The only people browsing the application menu are first-time users who have no idea about the preinstalled applications and even that doesn't imply that this group of persons is hindered by Kickoff.. Each following application has to be installed consciously by the user (assuming an one user system) and is therefore *known* (name, functionality) to him beforehand."
    author: "Erunno"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "> You were questioning why menus where necessary at all by pointing out that\n> favorite applications can be started via one click from the panel or\n> desktop. Selective reading is unnerving.\n\nI am afraid that this is only _your_ selection from what I write :-p\n\nI was not questioning why menus are necessary, I was questioning why to hide \"normal\" menu behind _another_ favorites implementation, when there already exists comfortable favorites implementation - or, to be precise, \"something like favorites\" because quick launch depends on user to add the favorite applications (at the time, but this can be changed)\n\n> If KDevelop is filed under Games/Puzzle the package maintainer is *not*\n> to blame?\n\nthis is an obvious mistake - but what if I find some board game under Games/Puzzle when there is also Games/Board category or vice versa?\n\n> On top is even worse than besides the other levels ...\n\nplease, do not do what you criticise yourself - why do you catch a single phrase, abusing my lack of expertise in English, or do you really not understand that I have meant the default KDE 3(.5.7) menu behaviour?\n\n> Each following application has to be installed consciously by the user\n> (assuming an one user system) and is therefore *known* (name, functionality)\n> to him beforehand.\n\nbut my experience not just fail the \"one user\" assumption (I administer desktops for 5 people not counting myself and visitors, they are not root so they do not install applications), it even tells me that people (including me as an advanced user!) do not remember names of all the applications they occasionally use\n\nand of course, your argument fails my \"pseudo-random game\" use case mentioned above within the discussion"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "Why hide the normal menu?\n\nHow is the normal menu, or rather, by what measure is the normal menu useful?\n\nIt is a long, nominally sorted list of applications. It is only useful in the situation where I'm browsing through a list.\n\nOne use only. And a rare use. Anyone who has used the KMenu over distributions and releases have found things moved around. The descriptions have proven to be lacking. I have many times installed applications through distro packaging systems that end up somewhere on the menu, damned if I know where. My systems end up with a very large number of applications due to my usage patterns. The menus end up nesting and subnesting into an awful mess. A hierarchical menu system doesn't work for me.\n\nThe menu is an attempt to allow people to run applications they want to run. \n\nOne usage pattern is the common usage of [insert number] commonly run applications. Kickoff does this very well. It is easy to move an application from the hierarchical list to the favorites, or to the taskbar.\n\nAnother usage is to find the application you ran recently, but that isn't an everyday usage. Again Kickoff does this very well.\n\nAnother usage is finding something that does a specific task. The search bar works very well. Don't think that KRunner does similar. KRunner is a long way from being useful. Kickoff works amazingly well, and this functionality is so good it changes the way you use a menu. Just a note here. I recently shut off Beagle indexing due to overgrown and out of control cpu and disk usage. Kickoff without indexing becomes noticeably less useful.\n\nAnother usage is to logout, hibernate, switch users, etc. Kickoff works ok here. Too many mouse clicks. There are other situations where moving the mouse switches focus too easily, other situations where too many mouse clicks are required. It isn't perfect by any means, but it is a vast improvement on a hierarchical menu.\n\nYou are focussing one one usage, browsing through a long list of applications. That is only one usage pattern.\n\nDerek"
    author: "D Kite"
  - subject: "Re: Kickoff"
    date: 2007-10-03
    body: "> You are focussing one one usage, browsing through a long list\n> of applications. That is only one usage pattern.\n\nyes, I am - but, until now, I have not read any single good argument what is the reason for destroying easy access to and easy usage of it, why the good ol' applications menu cannot co-exist with the new features?\n\nyou are right - the application menu has a rare use (running the program via alt+f2 or typing it in console versus selecting from menu has a ratio about 50 to 1 in my case), but this does not justify making it harder to use\n\nbtw,\n\n> KRunner is a long way from being useful. Kickoff works amazingly well\n\ninteresting ... one would think that KDE is designed with modularity in mind, especially KDE 4, and you do not have to reinvent the wheel, i.e. two search fields with the same purpose should share the same engine ... so how that difference comes?"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "I prefer choice.  I am happy to see Kickoff ported to KDE4, but I hope in the end users will be able to choose from a standard KMenu, Kickoff and KBFX such as they currently can.\n\nKickoff is the best looking of the three, but I find it slows me down since it takes to long to navigate.  It got to be that I avoided the menu altogether when I had Kickoff installed, and just used Alt+F2 all the time."
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Tasty Menu?"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "thankyou, I did not know this exists :-) - looking at the screenshot, this is what I have in mind: there are the favorites and search (the key features of Kickoff) without destroying the application menu (although I do not like the interface but it is just cosmetics)"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Leaving aside discussion of what is better, I was wondering if maybe kickoff was placed in as a option in KDE 4.0, because the Plasma menu won't be enought mature to ship?\nYou know, there are a lot of development in Plasmoids, so it's probally hard for the few developers to focus very well in a complex part as the menu is, having also to code clock, taskbar, quick-launch, desktop, virtual desktop switcher...\nThere are a LOT of things for plasma to code, so it probally makes sense to offer alternatives while it reaches a good point of usability. And for what I know, kicker isn't being developed anymore."
    author: "Iuri Fiedoruk"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "I agree with all the critics about Kickoff, it's ugly and bloated beyond any limits. Why KDE need this thing of the past? It remembers me of Windows 3.1 with that software used for organize the menus (sorry but I don't remember the name, maybe Xerox something?).\n\nWhy not dump the Start menu concept, using the taskbar for application lunching and as a system tray (like apple does, if I'm not wrong) and creating something like 'media:/' for browsing the installed software inside a Dolphin window (applications:/), per example? Most of people I know (common users) usually don't use the start menu in Windows, or even the K-Menu.\n\nThanks. "
    author: "kde.fan.from.brasil"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "http://en.wikipedia.org/wiki/Image:Windows_3.0_workspace.png\n\nIt was called the Program Manager, though I don't think it is a far comparison to Kickoff."
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "No, I'm not talking about the default Windows 3.1 program manager, but to a replacement that I don't remember the name. That software organizes the groups into menus and adds some quick launch icons."
    author: "kde.fan.from.brasil"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> Most of people I know (common users) usually don't use the start menu in Windows, or even the K-Menu.\n\nAnd that's the problem with armchair usability anecdotes.  Most of the users I know use the start menu heavily, even though it is slow.  So who's right?  Neither of us has more than personal experience to draw on, so it is hard to tell.  Suse has done a usability study on Kickoff with good results.  Having conducted usability studies myself, I have a much more cautious view of their utility than many, but it is definitely a very valuable tool, and in this case spoke for the Kickoff design.  That's more than can be said for the alternatives like the old KMenu, or KBFX.  \n\nA folder of applications is not an alternative.  I always hated that most about OS X.   The only reason it is not a huge disaster is that Spotlight will let you launch apps quickly by searching for them.  Kickoff does that, while being much more sophisticated as well."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "In a way he is right. On windows i use launchy (http://www.launchy.net/) and when on linux/KDE i use katapult (http://katapult.kde.org/). So yes. I barely use the menu.\nThey are both excellent apps and i look forward to krunner doing what katapult now does for me."
    author: "kitts"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "Launchy is amazing.  I haven't been able to find anything as good on Linux unfortunately.  Katapult doesn't cut it because it only shows one match, which is often not enough.  Until krunner arrives, I just bound the regular run dialog to ALT-Space and start everything by its proper name.  Not as nice as launchy but its the best I can find."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> the Plasma menu\n\nthe what?"
    author: "Aaron J. Seigo"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "That plasmoid that is like a toolbar with a k icon on the left side? :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Kickoff is horrible, I tried using it for a couple of months and it just seems like a solution looking for a problem, specially the \"application browser\" a royal pain in the ass to navigate. \nBut I guess it is OK if you only use a couple of applications that can live comfortably in the favorites menu.\n\nThe history menu is nice tough."
    author: "zncdr"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "no need to be rude, now is it ? (i'm referring to your other post).\n\nas for the kickoff, it actually is pretty nice. favorites are cool.\n\nmy biggest complaint is that tabs change upon mouseover - this seems unintuitive for me even after using it for extended periods of time. tabs change for me all the time when i don't want that.\n\napplications access is a bit cumbersome (and that scrolling can be ugly slow on slow video adapters - read, with no adequate drivers), but i rarely use that part now - it's either in favorites, or i use quicksearch at the top of the menu.\n\nclick to expand is much easier to use for people who don't control mouse perfectly (imagine stray mouse move collapsing subitems now and then...) - but that also is one reason contributing to me not using it that much anymore."
    author: "Richlv"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "> no need to be rude, now is it ? (i'm referring to your other post).\n\nbecause I am not Jesus - if somebody throws a stone on me, he won't receive bread in return\n\nthankyou for your comment anyway - you, as the regular Kickoff user (if I get it right?), prove my theories deducted from the presentation, making an evidence that I do not have to try everything myself to make a valid opinion on it"
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> how does it come that I most of the usability \"improvements\"\n> look to me like obstructions?\n\nthis is why people do lab testing on actual people to determine usability and consult design experts rather than rely on the armchair perspective. kickoff actually went through quite a bit of user testing."
    author: "Aaron J. Seigo"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "so, because there war lab tests, we are not allowed to not like it?\n\nthis is feedback. there obviously ARE people who don't like it. are there enough to add at least a few more options to kickoff? i don't know. but just dismissing all negative comments because there where lab tests is not the right way..."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "there will always be ppl who don't like something. Listening to them only is useful to a certain extend. And we're way past that point, imho. There are several menu projects for 4.0, but I don't think any of them will be very good when 4.0 should be ready. Kickoff might be the most stable one, and I'm all for shipping something stable over something probably better..."
    author: "jospoortvliet"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Especially when there is no evidence that the competing implementations are better."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "but aaron never said he doesn't consider these comments because of the imminent release.\n\ni understand there is no time anymore. i even understand there is a majority who likes the menu (concept) like it is right now.\n\nwhat i don't understand is aarons position to dismiss feedback because of lab testing."
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Please, stop throwing around phrases like \"most users\" without having *any* means to back up your words. Just be content that you like the other menu implementation better and don't try to give your claim weight by pretending to speak for a majority.\n\nI think Robert Knight still had the best explanation for not changing Kickoff too much: This is a proven concept and changing it might break it. Nobody will stop you to use Raptor or any other menu implementation. If you're right and Kickoff is universally hated it shouldn't take too long until someone decides to develop an \"old style\" menu."
    author: "Erunno"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Why not retain the old style menu? Is it too much of work? Just like Kickoff & Old style menu coexist in PCLOS, not sure about other distros."
    author: "VJP"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Read it:\nWhy not retain the old style menu 'also'?\n\n\n"
    author: "VJP"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "I guess it boils down to lack of resources and the need for a KMenu replacement right now. Kickoff has been at least tested by usability experts. You'd think that people would be happy that KDE tries to improve their GUI by listening to experts but after all the moaning here because their pet peeve is not honoured enough by a design decision I'm starting to ask myself how GNOME could come up with a GUI which allegedly applies to a majority of users."
    author: "Erunno"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "what the hell!? what on earth got me such a response? are you able to read?\n\n> there obviously ARE people who don't like it. are there enough\n> to add at least a few more options to kickoff? _i don't know._\n\n> i understand there is no time anymore. _i even understand there\n> is a majority who likes the menu (concept) like it is right now_.\n\ni never said \"most users\" don't like kickoff. i said the contrary. so if you want to flame me, at least get your facts right, please!\n\n\nall i said was, there ARE people that don't like kickoff. if you look at this threat, all data is here. are they many? again, i don't know. but there are at least a few, so dissmissing all that posts just because there was some lab testing or to not add a few _options_ to not break the concept doesn't sound right to me at all.\n\nsure, there is no time for 4.0 right now, but that wasn't aarons argument, and im quite sure it wasn't roberts either. \nthats not what im arguing about, so stop posting things i didn't say!"
    author: "ac"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> this is why people do lab testing on actual people to \n> determine usability and consult design experts rather\n> than rely on the armchair perspective. kickoff actually\n> went through quite a bit of user testing.\nIn that case, why not just copy it from WinXP or Vista? They perhaps passed far more lab tests than everything else? If that is the reason?\n\n"
    author: "VJP"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "There's enough \"lol KDE == teh windoze!1\" FUD flying around without giving naysayers actual ammunition.  I'm sure XP and Vista have very usable menus, but it's nice to do something both usable and at least a little different."
    author: "Anon"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "No. I am not at all doing any KDE vs Vista. Read it carefully please.\n\nIs opinions are to made by usability experts how it should look & feel like, then why not follow the proven one?  "
    author: "VJP"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Where is the data for the study on the usability of the Windows start menus?  You only assume those studies were done, but without access to all the data, we can't be sure."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "> then why not follow the proven one?\n\n...like Kick-Off, which is used for a while already in openSUSE? :-p\n\nMicrosoft or Apple are not the ultimate answer to all usability problems. We can learn a lot from them, but they make their fair share of mistakes too. If you read the Kick-off study (yes it's public!) you'll find that the start menu of XP and Vista have issues too. Some tasks take too much time, or haven't gone through usability changes because no one could agree (e.g. the shutdown options in Vista).\n\nI agree that the navigation in kickoff could use some improvements. I like the favorites very much, but it takes relatively much time to find a non-favorite. OTOH, the slide-in/out menu requires fewer mouse movements to access the items because the menu stays at the same place (tic-tac-tic-tac-open). That's really cool and increases productivity. :)\n\nWe (programmers) have different requirements, since programmers are capable of handling more overview. They miss the big menu (I sometimes do too, so I'd love to see incremental improvements in kickoff). My friends were really excited about kick-off though, calling it brilliant! SUSE did a good job here. :)"
    author: "Diederik van der Boor"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "There were supposedly tests that led to Gnome changing the button order in dialogs with no option to change it back, and that is one of the reasons I don't use Gnome.  The moment some study tries to change years of habit and use with no way to change it back, I'm out.  As long as Kickoff remains the only option, I'll not use KDE4.  Everyone has different dealbreakers I'm sure, but that's one of mine."
    author: "MamiyaOtaru"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Bye!\n\nBut seriously.  TRY it before you freak out.  Delivering silly ultimatums on message boards is counterproductive."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "I had tried it.  You must hover for 2 seconds at each menu level, or click and wait for a slow transition.  The Windows 95 Start Menu is vastly more efficient for getting to the app you need.\n\nKickoff looks great, and visually I'm sure it will impress people, but I'm not sure how it can be rated high for usability, when all it does is turn me off from using a menu in the first place."
    author: "T. J. Brumfield"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "Mamiya, this is you...mmh fith or sixth rant \"I will stop use KDE\", so what's up? Why are you still here whining (blatantly) on the dot? If you like KDE, collaborate, criticize it with *usefull suggestions*. If you don't like KDE or its developers/community, then just ignore it.\nIt's so easy!"
    author: "Vide"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "> well, how does it come that I most of the usability \"improvements\" \n> look to me like obstructions?\n\nThe might be a lack of expertise in the field of usability, at least that's what I think after reading most of your comments.\n\nSee, the kickoff menu has been developed in close collaboration with usability experts. So if you want it changed completely, you have to have data from usability research which clearly indicate why the current version is flawed, and why your version is better (not that your comments indicate an alternative, they only criticise). Having actually used kickoff for a while would probably also be helpful for the quality of your opinions.\n\nSpeaking of alternatives, you're talking about kickoff and that it should work differently. That whole discussion is totally moot, unless you have working code, in time for the feature freeze, which is close.\n\nPlease try to keep your arguments at a factual level, and if you have real input, go to panel-devel or usability mailinglists and tell about it. Don't forget to provide both data, and code. Having this kind of discussion on the Dot almost always indicates that the poster isn't seriously trying to help, but only telling others how they should do their work. Often from a questionable background."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Damn sebas, you have been making all kinds of sense lately.  When that start happening ;)"
    author: "manyoso"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "Changing how my DE works and talking down to me when I object isn't going to get me to hold usability studies in any higher regard.  Maybe you should try for some consistency and change dialog button orders too.  Gnome and Apple will be happy to cite usability studies for you."
    author: "MamiyaOtaru"
  - subject: "Re: Kickoff"
    date: 2007-10-01
    body: "So instead KDE should...?\n\nCome up with a better alternative and your opinion would have much more weight.  If your alternative is \"port the menu from KDE3\", then you're going to have to do it yourself, because apparently no-one else has stepped up to do that.  Once your implementation is ready, it might be incorporated."
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "> The might be a lack of expertise in the field of usability\n\nyep, you are right - I do not do studies about it, I just use it ...\n\n> See, the kickoff menu has been developed in close collaboration with\n> usability experts.\n\nI've heard that about Gnome too - how many of us would like to use Gnome instead of KDE (3)?\n\n> not that your comments indicate an alternative, they only criticise\n\nmaybe I was not explicit enough - the alternative is to keep the old-style menu (along the new favorites/search/whatever you like)\n\nthe problem is not that you add features but that you modify feature, making it worse, but saying that the new implementation is a bless for all (once again, it reminds me of Gnome ...)\n\nnow, I have to explicitly say, that I am talking about applications menu, not the whole Kickoff (remember that for the next paragraph!)\n\nit is not a bless, and if the usability experts say otherwise then I doubt whether they are sane - you argue with usability studies, I can dispute using psychological literature saying that maze can be solved easier when you have overview of the crossings (i.e. see more menu levels at once) rather than when you see only one crossing at a time (i.e. one menu level replaces the another) (not speaking about the number of clicks - or mouse holds if set to hover)\n\nI have no problem admiting the fact that Kickoff may be a benefit for the majority of users - but there are also users that need the application menu; access to it is harder using Kickoff, and the menu usage is harder ...\n\nlook at the study - http://en.opensuse.org/Kickoff/Results_taskcompletion - there is only ONE task involving starting an application! (I do not know the exact task, but if URL should be opened in browser then probably the applications menu was not involved at all ... simply typing URL into appropriate box is enough for KDE to open it using the associated program)\n\nare you serious to use such a study as an argument against classical applications menu?!\n\n> Having this kind of discussion on the Dot almost always indicates that\n> the poster isn't seriously trying to help, but only telling others how\n> they should do their work. Often from a questionable background.\n\nvs.\n\n> That whole discussion is totally moot, unless you have working code,\n> in time for the feature freeze, which is close.\n\noh great, I have no code, and I realise that it is late (see my original post) so expressing my opinion on Dot means that I am totally lame troll seeking for flamewars ...\n\nI would like to help, but nobody asked my opinion before deciding that Kickoff is going to replace the old menu\n\nand please do not say that this is my problem that I do not know what is going on and if I knew, I could say my opinion earlier and it would be accepted - I do not have the time to read the developer conferences of all the software I use; in fact nobody would be able to do this, there are megabytes of text daily\n\nI would file a bug (enhancement request), but I have learned that when something \"is decided\" in KDE, no user feedback is going to change it (the third opportunity to remember Gnome), so all that is left is ranting here ..."
    author: "kavol"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "Hopefully at some point Kickoff can accommodate both approaches.  Perhaps the applications tab could be as it currently is, but instead of having one folder replace the top level folder, it would open a popup menu at that point.  Then the top level menu remains as is, but you get to see the child menus without obscuring the top level so you can jump around quickly.  "
    author: "Leo S"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "Can someone tell me, why, when I'm typing into the search, I can't use the up and down keys to navigate the search results?  The default search result is something like \"Look for other results of this type\" or something, not the application I need.  I have to move my hand from my mouse to the keyboard, type, observe that the result I want has appeared, the move my hand back to the mouse, and this is the worst part: as I move my mouse over the tab bar (which stands between the cursor and the search results), it switches from  search results to that tab!  Until this is fixed, kickoff is useless useless useless.  I'll be using raptor, lancelot, or kde 3 kmenu until this is fixed, because there is no reason why I would want kickoff in its current state."
    author: "Level 1"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "> Can someone tell me, why, when I'm typing into the search\n\nIf you're talking about the KDE 4 version, that is a bug.  It is a high priority item on my TODO list."
    author: "Robert Knight"
  - subject: "Re: Kickoff"
    date: 2007-10-03
    body: "Thanks Robert... sorry if I was a little strong, but it is frustrating, it really makes the search feature useless.  \n\nFor the record, I'm using a version of kickoff that was ported to kubuntu for kde 3.  I'm using gutsy."
    author: "Level 1"
  - subject: "Re: Kickoff"
    date: 2007-10-26
    body: "Where did you get that?\nI'm running Gutsy and I've been looking all over for a Gutsy version."
    author: "Scott"
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "That is such a minor detail.  All of the complaints against Kickoff are hypercritical exaggerations of details.  There are some issues, but it can evolve and improve, the details polished, and bugs fixed.  At the end of the day, I am sure the old style menu will probably be available, so I don't see what the big deal is."
    author: "Henry S."
  - subject: "Re: Kickoff"
    date: 2007-10-02
    body: "Without trying to be hyper-critical of Kickoff, though I'm sure it'll come across that way, it's one of the major reasons that I won't entertain OpenSuSE 10.2.\n\nBetween hovers that only sometimes work, to bloody awful grouping of applications, to what feels like endless drill down, to menus appearing and disappearing when the mouse is only nudged slightly and not even off the menu window it takes longer to launch an app with Kickoff than the current menus.\n\nThe presentation is far from intuitive and heaven help the user if they somehow lose the menu that's presented when they get close to what they want because they have to drill down again.  Not to mention the chewing up of screen real estate as you drill down.\n\nAt the very least how it works in OpenSuSE 10.2 reveals it as half cooked and not ready for prime time regardless of how much research went into the concept.  Like the GNOME desktop developers do have to realize that there comes a point where ease-of-use becomes treat-the-user-like-an-idiot and ceases to be about actual usability.  Currently Kickoff lacks both usability and ease of use.\n\nThat's the big deal, since you asked.\n\nLike it or not users are conditioned to a start menu working like Win95 or Mac OS 9.  That's the expectation.  Kickoff breaks that expectation without being close enough to it for users to get used to the power you claim is there.\n\nIt has promise though for the moment that's really all it has.  From a user perspective it needs tons of work or, simply put, it won't be used.\n\nttfn\n\nJohn "
    author: "John W"
  - subject: "Re: Kickoff"
    date: 2007-10-03
    body: "Only long-time Linux users are used to the start menu working like Win95 or Mac OS 9, because that is the style we had when we left Windows.  We are behind the times.  Current windows users are used to the Start Menu working like XP or Vista, which is closer to Kickoff than Win95.\n\nIf the Application area is the one that people hate the most (even I dislike it), then it can be changed to work like the \"All Programs\" menu in Windows XP and Vista.  When I see a problem, I look for solutions.\n\nBTW, The grouping of applications has nothing to do with the style of menu.  \n"
    author: "Henry S."
  - subject: "Well, personnaly I'm interested"
    date: 2007-10-02
    body: "I'm not sure I'm fond of the Application browser but I'm still interested in testing getting used to Kickoff. Sure you can have an app panel, but having it on the menu saves space, so I could actually prefer this solution.\nI don't think it's such a big deal as to get into uproar because of it, though."
    author: "Richard Van Den Boom"
  - subject: "PlanetKDE"
    date: 2007-10-01
    body: "Heheh good point at the end of the Digest:\n\n> It seems crazy to me, but I guess it's possible that readers of the Digest could\n> not be aware of the existence of PlanetKDE, the aggregation of blogs from KDE\n> contributors around the world. For those of you who are interested in KDE\n> development and community and are not yet reading PlanetKDE, it really is the\n> ideal counterpart to this publication!\n\n(link: www.planetkde.org)"
    author: "Diederik van der Boor"
  - subject: "thank you danny :)"
    date: 2007-10-01
    body: "Nice to see some serious work on konqueror :)"
    author: "shamaz"
  - subject: "Games"
    date: 2007-10-01
    body: "Games are somewhat of a low priority, but I'd like to make two requests to the wonderful KDE coding community out there.\n\n1 - http://en.wikipedia.org/wiki/Nine_Men%27s_Morris\n\nIt is a fairly simple game, but there aren't many computerized versions of the game.  A few digests back I saw that someone was making a backend to develop board games, and I'm not sure if that could be used or not.  But I thought it would be a nice addition to the KDE-Games, and something new for people to play since so very few people have heard of it.\n\n2 - I'm a big fan of rougelikes, and games like Nethack/Slash'em have a whole slew of graphical options when playing the game, such as using SDL/X11/GTK/QT/Allego/etc, but I was hoping someone would update the QT portion of the code to use QT4/kdelibs and add support for scalable SVG tiles."
    author: "T. J. Brumfield"
  - subject: "Re: Games"
    date: 2007-10-01
    body: "1. given that it seems a normal board game, you can offer the developers of tagua some help... you don't need coding skills but rather knowledge of the rules of a boardgame to make tagua support it, so no reason why you can't do that yourself."
    author: "jospoortvliet"
  - subject: "More game requests"
    date: 2007-10-01
    body: "I'd like to see some \"Tower defense\" game. There are many, but the best I've seen so far are:\n- Desktop tower defense: http://www.handdrawngames.com/DesktopTD/game.asp\n- Vector tower defense: http://www.vectortd.com/play/\n- Budapest defenders: http://alt.tnt.tv/tntoriginals/thecompany/budapestdefenders/index.htm\n\nI think the ratio \"easiness of programming\"/\"fun\" is quite high for these sort of games. Even it would be possible to use the same \"engine\" for all them and link it to \"Get hot new stuff\" to allow the people to create their own versions.\n\nRegards,\nDavid C\n"
    author: "David C."
  - subject: "Re: More game requests"
    date: 2007-10-02
    body: "I have the beginning of the start of almost the sketch of something close to a game that might become a TD-like game for KDE some day... (and you though kde4 betas were unstable...)\n\nI welcome any help..."
    author: "NamShub"
  - subject: "Re: More game requests"
    date: 2007-10-03
    body: "That's wonderful! I also fell in love with the TD games. Have you tried to contact the creators of those flash games? Maybe they can lend you some of their artwork."
    author: "Tower Defense"
  - subject: "Re: More game requests"
    date: 2007-10-03
    body: "Wow. I'm also working on a KDefense. A friend of mine is a good C++ programmer with interests in the low-level area, and I have good experiences with Qt. Today, we spent two hours on discussing the data structure holding the creeps' and towers' positions, and decided to implement a Kd-tree (http://en.wikipedia.org/wiki/Kd-tree) for good performance with collision detection, targeting and such. Another friend of mine is going to draw some SVG graphics."
    author: "Stefan"
  - subject: "Re: More game requests"
    date: 2007-10-02
    body: "I would like a version of Bezique, please!  \n\nDetailed rules:\n\n\nhttp://en.wikipedia.org/wiki/Bezique\n\n\n\nThere is a quite good shareware version here for Windows that could be used to give ideas:\n\nhttp://www.fortedownloads.com/Malcolm-Bain-Bezique/\n\nIt is a great game as it has more complex play than most card games and relies on BOTH skill and luck.  "
    author: "The Vicar"
  - subject: "Lots of good news this time"
    date: 2007-10-01
    body: "To name the ones I really liked:\n- The Noatun music player becomes a KPart (ok, I admit, I really hate noatun, if should have vanished a long ago IMHO)\n- Further work on Phonon, with developments on the GStreamer backend. (starting to look good, this adds a lot of video and audio support already)\n- A plan is hatched to get Kopete ready for the KDE 4.0 release. (shipping without a icq/msn/gtalk solution was really looking bad).\n\nI just hope that someday instant messaging clients for unix/linux will support audio and video for msn/gtalk.. well I can at least dream of it :)"
    author: "Iuri Fiedoruk"
  - subject: "Dubious about noatun"
    date: 2007-10-01
    body: "I don't think a lot of people use it at all. I would really prefer a lot that Codeine/Video player would be ported fully to KDE4, as it can play sounds just as well as noatun, except that it doesn't set itself in the System tray and is faster to start. It also remember where it was last time in the media file, which is nice.\nI really think only one simple media player should be installed by default with KDE4, and codeine seems to me the simpler and faster one. \nOf course, I do not mean by that that more complex apps with different goals like Amarok, Kplayer or Kaffeine should be discarded, just that we only need one simple app that does the job without providing any additional service.\nEspecially since it would help integration with Web browsers of this particular app, as there would be only one target."
    author: "Richard Van Den Boom"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "I'm curious what relation KDE4's noatun has with KDE3's noatun. It might be a totally different app (Amarok pretty much is code-wise)."
    author: "Ian Monroe"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "Well, if it's exactly like Codeine, it'll be OK. :-)\nBut if it's a completely different app, then maybe renaming it would be a good idea."
    author: "Richard Van Den Boom"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "Just like the previous poster, I've always hated noatun. It has never worked for me (as in: play a sound!), but it reliably gets in my way.\nI hate noatun. Thinking about it, if I had a kde-svn account, my first action would be to delete it :)\n\nNow seriously, can't it *please* be replaced by kmplayer, kaffeine/amarok/juk, ANYTHING?"
    author: "anonymous coward"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "First, please people calm down.\n\nSecond, I think you guys are being very rude.  \n\nThat said, the commit digest is incorrect.  Noatun has been removed from KDE's svn altogether.  Charles just committed this kpartified Noatun so he'd have it under version history should he ever decide to change his mind and unremove Noatun.\n\nBut as of right now, Noatun is no longer."
    author: "manyoso"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-02
    body: "Noatun is dead, all hail noatun!!\n\n(finally hehehe)"
    author: "Vide"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "You are really an \"anonymous coward\".\nIt's not surprising that you don't have a kde-svn account."
    author: "shamaz"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "Dear manyoso, dear shamaz,\n\nI don't think I'm rude. I expressed my opinion, and I know that this opinion is probably quite close to the negative end of the spectrum. I also could have written a lot more text to make sure noone feels offended.\n\nI have not. It's my opinion, and I'm fine with you disagreeing. I never attacked noatun's author (don't know who he is. Charles?), but simply expressed the fact that I hate this piece of software. Now you might argue this is unconstructive criticism. Point is, I don't even want noatun to be improved. There's a lot of good alternatives (which I happily use), so it'd be best for me if noatun simply disappeared from \"open with...\" menus and the like.\n\nDear Charles, I don't like your software. That doesn't mean I don't like you. How could I know?\n\nshamaz, could it be that you WANT to be offended?"
    author: "anonymous coward"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "Hmm, I don't seem to have noatun in the \"open with\" dialog, possibly because I don't have it installed.  If you have it there, you must have it installed.  This seems backward to me, considering your dislike of it and my indifference.  Instead of heaping curses upon it, you might be better off uninstalling it and then not worrying about it anymore.  imho"
    author: "Anon"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "Codeine is a bit basic, I'd rather see Koos morph KMplayer's standalone UI into the Codeine one, or see Kaffeine get some usability work (eg Codeine has exactly the functionality a video player needs). Kaffeine unfortunately still aims to be amarok^2 for video and audio. Pitty, it does work great...\n\nWe now have KMplayer, Kaffeine and Codeine. Codeine is unfortunately as far as I can tell unmaintained, though it's the best by far in terms of usability. Kaffeine and KMplayer are distinctively different, and codeine wouldn't be needed if these two would be a bit more focused on what they're supposed to do (play video).\n\nmeh, longtime frustration for me. I stayed with the 0.4.2 release of Kaffeine for a long time, now I use codeine. But I want SOME of the Kaffeine stuff (eg playing speed, better DVD support etc) - yet not it's bloath. meh meh meh :("
    author: "jospoortvliet"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "I don't know if they really have to converge.  I'm quite happy using Codeine for 99% of videos, and a more complex player (VLC for me) to get at the more obscure features that I need once in a while.  Some features (like playing speed) could get into Codeine as keyboard shortcuts, but I don't think it's possible to make Codeine full featured without losing the UI simplicity.  I don't really see that as a problem though. "
    author: "Leo S"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-01
    body: "And what does Noatun have to do with Codeine/Video player, it's applications for completly different use cases. Noatun is not supposed to be a simple player, never was. Noatun main use is as a playlist capable player, comparable to but using a different approach than Juk or Amarok(In many cases what some people complaining about Amaroks playlist should use).\n\nThe simple player you are talking abuot, comparable to Codeine/Video player with simple interface and fast start up is Kaboodle. "
    author: "Morty"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-02
    body: "You may be right but it seemed to me recently that Noatun was the default app for most media files in a default KDE installation.\nIt seems to me that it's logical to configure the mime types of media to be owned by the fast, simple app as mime-app link seems to me essentialy related to view/hear a file when clicking on it in the file manager or when downloading it from the net. I don't want for instance amarok to be started when I click on a mp3 file in Konqueror. I use amarok to organize the collection, not to have a fast view. The same way I won't use digikam as an image viewer, I prefer to have Kuickshow for that.\nIf there was any confusion to me, it probably comes from these settings. In any case, I don't think Kaboodle and Noatun, with all their merits, are used a lot now compared to other players. And that's probably for good reasons, so setting them aside seems logical to me.\n "
    author: "Richard Van Den Boom"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-02
    body: "I agree, the complex players like Noatun, JuK and Amarok should newer be the default applications for media files. But that's a configuration bug/issue, not really related to the applications at all. But it's a really huge missunderstanding of the applications capabilities and use cases, mostly from the distributions it seem(they have now started to use Amarok as default on mp3 etc, really annoying). This missconfiguring is the only reason Kaboodle has seen little use, it's a gem few know about.\n\nWhen I click on a media file I want it played, nothing more. I don't want to add it to my playlist/collection or open a huge playlist in case I want to listen to more media afterwards. If I want that I'll open the application holding the collection and add it, using add or drag and drop or whatever.\n\nWhen clicking I want something like Kaboodle/Codeine/Videoplayer to simply play it. "
    author: "Morty"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-02
    body: "Exactly.\nThat's a small suggestion for KDE4 to the devs : assign mime type of media files to quick, fast loading apps. :-)\n \n"
    author: "Richard Van Den Boom"
  - subject: "Re: Dubious about noatun"
    date: 2007-10-02
    body: "We're thinking of having a simple mode in Amarok that would be used when playing a file from Konqueror or Dolphin. Its not a use-case that Amarok was really meant for, but it'd be silly to ignore the fact that Amarok gets used this way.\n\nBut its one of those long-term goals, I wouldn't expect to see it anytime soon."
    author: "Ian Monroe"
  - subject: "Amarok and Noatun?"
    date: 2007-10-01
    body: "Amarok is great but I installed yesterday the Lsongs tool from Freespire on Feisty which wasn't further developed since 2005. Usability wise the playlist concept of iTunes is fantastic. One library for all files. And playlists. And ripping and playing CDs also works excellent. Music is here more similar to bookmarks. You create a playlist with 15 titles and just burn it. In a non-corporate context we could thin of other uses, such as export a playlist as a torrent, share files with a person, \n\nWhat annoys me in Amarok is the nested menus items for Internet Radio and so on. But I was amazed to see that Lsongs understands PLS streams. Although Amarok is so much more advanced I wonder why I still felt in love with the iTunes ripoff. The reason is of course that Lsongs copies 1:1 some things Apple just did right. For instance I enter a CD and Lsongs just displays it and allows 1 click ripping. Unfortunately Lsongs does not support podcasts yet. Linux largely did not notice the podcast phenonemon because available podcatchers were inconvenient.\n\nFeisty:\n-------\nFor your sources list: \ndeb http://apt2.freespire.org/CNRUbuntu skipjack-feisty main restricted\nFor your commandline\nsudo apt-get update\nsudo apt-get install lsongs"
    author: "Andre"
  - subject: "Random uninformed whinge for the week"
    date: 2007-10-01
    body: "Kmplayer using cairo rather than Qt?  Any particular reason?"
    author: "Odysseus"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "I was also wondering about this."
    author: "Anon"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "Dosen't Qt 4.1 support cairo in the same way it supports openGL?\nIf true, they could be actually using cairo throught Qt."
    author: "Iuri Fiedoruk"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "My understanding is that KMPlayer is slowly migrating away from the Qt/KDE land.\n\nNot a big loss since KPlayer has always been far better anyway."
    author: "hmmm"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "Makes sense to have a cross desktop solution, but if there is any dependency on Qt, using cairo doesn't make much sense.  It would only be beneficial if kmplayer doesn't link to Qt at all, otherwise you get cairo-equivalent features for free with Qt."
    author: "Leo S"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "I don't think that it's possible to create a KPart without Qt. And from the cross desktop perspective, trying to wrap the Qt API over another graphic library is just one big pain I guess."
    author: "Koos Vriezen"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "Sorry if I'm mistaken, but isn't that precisely what Qt does in Mac OS X and Windows? Wrapping its own API over the native painting engine?"
    author: "Anonymous"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "In my case, it's about cross Kde/Qt to Hildon/Gtk. I did wrap glib, curl, gvfs in Qt compatible API and initially also QPainter before reimplementing the painting code."
    author: "Koos Vriezen"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "I don't understand why some people can't see that kmplayer is a konquer plugin and keep on comparing the stand alone applications, which is explicitly positioned as _simple_.\nSecondly, I don't think KMPlayer is drifting away from Qt/Kde land. Remember it's a konqueror plugin, tightly bound to the KPart infrastructure."
    author: "Koos Vriezen"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "I like it very much.\n\nAs a plugin and as a stand-alone application. I especially like the fact that all mplayer shortcuts work as expected.\n"
    author: "Kevin Krammer"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "The component that is equivalent to Cairo in KDE is Arthur.\n\nPersonally, I've never had much luck with KMPlayer over the years."
    author: "Segedunum"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-01
    body: "Actually it replaced the Qt painting. IMO cairo graphics is superior for my usage in compare to Qt. However I have not investigated yet the improvements in Qt4, so this is purely based on Qt3.\nAnother good reason to chose a cross desktop painting library is because of the porting to the Maemo platform, which is Gtk based.\nWhat's the reason for asking, any problems with Cairo?"
    author: "Koos Vriezen"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "The graphics part has been a focus in the development of Qt4, so if you haven't seen it, you have definitely missed something.\nI think this benchmark still holds true:\nhttp://zrusin.blogspot.com/2006/10/benchmarks.html\nYes, Cairo has seen improvements since then, but so has Qt. And Qt is still multiple times faster. And Cairo is slow (not only in comparison. So no, this is not redundant information).\nIsn't it better/easier to use Qt's API if you use Qt anyway? Also, this way it's one dependency less.\n\nRegarding Maemo: Don't Qt apps run on it? And what about porting it to Qtopia?"
    author: "Anon"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "Up till version 0.10.0, released this week, KMPlayer uses Kde3/Qt3. It would be totally insane to try this to link to Qt4 for painting. Porting to Kde4 has just began, and thanks that all painting code is in one visitor class, it's actually relatively easy to move to another painting library.\nThe painting speed comparison is not really relevant to wherefore it's used in KMPlayer. It's fast enough right now and SMIL can't do complex drawings, like SVG can.\nDependency on cairo isn't really noticable. The libcairo.so exports 244 functions, and all being C, also lazy linked. Qt's libQtCore.so.4.3.1 OTOH exports 2505 symbols, libQtGui.so.4.3.1 exports 9805 (T ones) and not or hardly lazy. Which means that a very tiny performance hit on a Kde desktop, but a huge performance degrade on N800 when compiled with Qt4.\nQtopia, being a very nice platform for specialized and small memory devices, is IMO a huge step back on a consumer device like the N800 running a real X server. My contributions to Kde between 3.0 and 3.2 where on a PC that had less memory than the N800, to give an idea.\nIf there was a Qt port back when I ported it, I surely would have used it (at least tried it, a program loading time of more than a few seconds is deadly of course)."
    author: "Koos Vriezen"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "So you will eventually link to QtCore only and do the UI with cairo?\n\nI'm interested to see how that goes on the maemo platform.  It is a very enticing target."
    author: "Leo S"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "That would be the holy grail, but notice the smiley."
    author: "Koos Vriezen"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "Btw, lets not fall in the discussion that although we all love Qt, everything has to be implemented with Qt. In real live, one must choose the best tool for the job. Sometimes one is lucky it has a nice API, sometimes it takes a little more effort to master. That's how it goes."
    author: "Koos Vriezen"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "fine :-)\n\nWe appreciate your work!"
    author: "fine"
  - subject: "Re: Random uninformed whinge for the week"
    date: 2007-10-02
    body: "\"What's the reason for asking, any problems with Cairo?\"\n\nNope, not really, it just seemed to me that it would have been a conscious decision to move away from the Qt canvas, and I was just interested in the why part, it's always good to know peoples practical reasons for choosing one solution over another (thats just the architect in me, I guess).\n\nJohn."
    author: "Odysseus"
  - subject: "No new plasma eyecandy?"
    date: 2007-10-01
    body: "Curses! no new (plasma) eyecandy :(\n\nIt's gonna be another long week...\n"
    author: "Sad Coward"
  - subject: "Menu = mostly for show :)"
    date: 2007-10-02
    body: "For me the kickoff model is not very practical too. The classical model is as intuitive as it gets : choose the category of the program you want to run vertically and move horizontally to get all the available relative applications. It really looks old but it can't get any simpler. The favorites section is a good idea, but still it is also implemented in the old menu. However, I really don't care what the menu structure will be. Alt + F2 + app name = just like shooting. Pointing and trying to figure out where the application lies is nowhere as fast ..."
    author: "Nick L."
  - subject: "Re: Menu = mostly for show :)"
    date: 2007-10-02
    body: "Sorry, opened new topic by mistake ..."
    author: "Nick L."
  - subject: "Re: Menu = mostly for show :)"
    date: 2007-10-03
    body: "It has a learning curve, like anything.  But don't mistake your trained convenience for actual convenience.  The two major difficulties KickOff solves are scalability of the vertical/horizontal motion profile (i.e. nesting depth and screen size) and accessibility of function.  This is simplification, not for the GNOME model of hiding function, but for improving access to the existing functionality.\n\nFirst off, if you have seen the demo/usability study, you will notice that the functionality you describe, the simple nested menu method, is implemented.  If you've ever used an iPod or similar device, you will notice the form-factor-related constraint on the display of the menu is similar.  The user-friendliness of limiting selection distance should not be underestimated.  Less motion is required to perform the same action.\n\nSecond, just because it's a visual change, doesn't mean it's just for show.  This is a visual interface, in case you forgot.  The nesting of functions is a necessity in any system over a certain size.  Classification of items by function and type is the most basic heuristic, implemented in the existing menu, and most others.  KickOff doesn't abandon this heuristic - in fact, it refines its use, attempting to better separate disparate functions and group items better.  The separation of applications, devices, and system functions into equally close tabs, rather than grouping them together on one long menu, presents an arguably more accessible visual interface."
    author: "archangel"
  - subject: "K3B"
    date: 2007-10-02
    body: "What is happened to K3B? There is no news about it. I hope there is something new for KDE 4.0. In general it needs to redesign its interface (frankly I hate the file browser in main window of K3B). Also, K3B need to rework the workflow."
    author: "Zayed"
  - subject: "Re: K3B"
    date: 2007-10-02
    body: "According to lead developer Sebastian Tr\u00fcg in a recent interview (at http://dot.kde.org/1177530148/), he's focussing on the 1.1 alpha. After 1.1 is released, then he plans to port K3b to the KDE 4 libraries."
    author: "Alan Denton"
  - subject: "Re: K3B"
    date: 2007-10-03
    body: "Ah K3B.  What would I do without it.  Personally, I think the window is OK, meaning it's nice and simple.  I could, however, understand how some people would dislike it, and novices might benefit from a wizard or some such.  I still love it, though."
    author: "Louis"
  - subject: "Re: K3B"
    date: 2007-10-04
    body: "will K3B get also ported to windows? I'm missing a really good opensource burning application... imo nero is much too overloaded and expensive and I don't like deepburner."
    author: "LordBernhard"
  - subject: "Re: K3B"
    date: 2007-10-05
    body: "Yes, this is much needed. Of course, theres a lot of hardware considerations that need to be fixedcd-- burners are accessed differently on linux as on windows; its different than KOffice or Amarok.  But the cd burners for windows suck more than the rest of the OS."
    author: "Level 1"
  - subject: "And what about kolourpaint?"
    date: 2007-10-02
    body: "I love that program, it's quick and simple to use for some minor tasks that don't require complex editing. Last I heard on the program's website, there was a problem with the KDE 4 port (e.g. the toolbox was in two rows rather than two columns), but I guess it's been progressing well? Any updates?"
    author: "Alan Denton"
---
In <a href="http://commit-digest.org/issues/2007-09-30/">this week's KDE Commit-Digest</a>: Beginnings of a list view, and an applet browser integrated into <a href="http://plasma.kde.org/">Plasma</a>. Optimisations in <a href="http://www.konqueror.org/">Konqueror</a>. More work, including image practice support in <a href="http://edu.kde.org/parley/">Parley</a>. XMP metadata support in <a href="http://www.digikam.org/">Digikam</a>, with new splashscreens announced. Work on playlists in <a href="http://amarok.kde.org/">Amarok</a> 2. The <a href="http://noatun.kde.org/">Noatun</a> music player becomes a KPart, with musings on its KDE 4 future. Further work on <a href="http://phonon.kde.org/">Phonon</a>, with developments on the <a href="http://gstreamer.freedesktop.org/">GStreamer</a> backend. <a href="http://en.opensuse.org/Projects/KNetworkManager">KNetworkManager</a> is ported to work with NetworkManager 0.7. Deep refactoring in the <a href="http://eigen.tuxfamily.org/">Eigen</a> 2 library rewrite. <a href="http://en.opensuse.org/Kickoff">Kickoff</a> is ported to KDE 4 as a candidate menu replacement option. A plan is hatched to get <a href="http://kopete.kde.org/">Kopete</a> ready for the KDE 4.0 release. Import of the KBreakout game to playground/games in KDE SVN. Final moves in the recent KDE SVN reorganisation effort. The <a href="http://bugs.kde.org/">KDE Bug Tracker</a> starts to be upgraded to Bugzilla 3.0.

<!--break-->
