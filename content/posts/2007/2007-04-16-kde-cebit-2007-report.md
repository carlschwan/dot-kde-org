---
title: "KDE at CeBIT 2007 Report"
date:    2007-04-16
authors:
  - "dneundorf"
slug:    kde-cebit-2007-report
comments:
  - subject: "Resolution"
    date: 2007-04-16
    body: "Nice article. \nThe pictures jumps around though if you have a screen resolution of over 800x600 and I expect most people do."
    author: "pascal"
  - subject: "Re: Resolution"
    date: 2007-04-16
    body: "800x600 most ??? :DDD you mean 1920x1080 have the most (hdtv res)"
    author: "asda"
  - subject: "Re: Resolution"
    date: 2007-04-17
    body: "Learn to read!!!"
    author: "Marc"
  - subject: "Re: Resolution"
    date: 2007-04-18
    body: "This has nothing to do with resolution, would it be 800x600dpi\u00b2 or whatever.\nWhat you are referring to is the width of the view pane of the browser. Quite something different."
    author: "testerus"
  - subject: "Pleasant reading"
    date: 2007-04-17
    body: "It's always good to read about co-operation between Gnome and KDE people.\n\nThe photo \"crowded_booth\" has an interesting caption. The guy in the centre looking at the laptop screen with his hand on his forehead doesn't seem too eager to see KDE 4... Maybe he just had a headache. :)"
    author: "Anon"
---
KDE was present at <a href="http://www.cebit.de">Cebit 2007</a> in Hannover, the world's largest IT fair. The booth was located inside the <a href="http://www.linux-events.de/LinuxPark_2007">LinuxPark</a> in Hall 5, where Linux New Media had given us and other open source projects the opportunity to present their work. Alexander Neundorf, KDE buildsystem maintainer and the booth manager in charge for large parts of the event, considers this year's CeBIT "<i>a very successful event for KDE</i>". Read on for his report.














<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex; width: 300px">
<a href="http://static.kdenews.org/danimo/cebit_2007/messeturm.jpg">
<img src="http://static.kdenews.org/danimo/cebit_2007/messeturm_small.jpg" border="0" width="300" height="225" /></a><br />
<small>The tower at the fairgrounds in Hannover.</small>
</div>
<p>On Thursday, when the
exhibition started we occupied booth G64, directly next to the Debian
booth. Unfortunately, only wifi was available, which not all of our demo boxes had support for, but the friendly Debian guys were so kind to let us connect to their wired ethernet switch, so we finally had network. We attracted a lot of visitors and it turned out that the booth was a bit small for us.</p>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex; width: 300px">
<a href="http://static.kdenews.org/danimo/cebit_2007/crowded_booth.jpg">
<img src="http://static.kdenews.org/danimo/cebit_2007/crowded_booth_small.jpg" border="0" width="300" height="225" /></a><br />
<small>Despite the tiny booth, everybody is eager to see KDE 4.</small>
</div>

<p>So our Debian friends arranged that we could exchange the booth with Franz
Schmid, who presented <a href="http://www.scribus.net/">Scribus</a>. So starting from Saturday we had a bigger
booth available with two demo points.</p>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex; width: 300px">
<a href="http://static.kdenews.org/danimo/cebit_2007/team_1.jpg">
<img src="http://static.kdenews.org/danimo/cebit_2007/team_1_small.jpg" border="0" width="300" height="225" /></a>
<small>
The team for the first day of CeBIT (ltr):
Joost Sander and Sabina Weiland (both Kubuntu),
Andre Wöbbeking (long time KDE developer),
Melissa Steinhagen (first time contributor),
Alex Neundorf (long time KDE developer)</small>
</div>

<p>We had the Gnomes as direct neighbors. This turned out to be a lot
of fun and we helped each other where we could. The days of animosity
between KDE and Gnome developers (if there ever was this) are long gone.</p>

<p>Thanks to our great community the KDE booth was always very well staffed,
both by experienced KDE contributors and our friends in the Kubuntu and
Fedora communities, but also the first time contributors Melissa
Steinhagen, Tobias Pfeiffer and Steven van der Vegt. It's nice to see such
enthusiastic new contributors coming to KDE :-)</p>

<p>We had a lot of interesting chats with visitors. There were very different
groups of visitors. Some of them were experienced KDE users and maybe we
were able to motivate some of them to contribute to KDE themselves. Then
there were also visitors completely new to Linux, we could show them that
nowadays Linux with KDE is a viable if not even more powerful alternative
to the commercial offerings. I guess the most asked question was "what
will KDE 4 be like?". For these visitors we could show some bleeding edge KDE 4 applications.</p>

<p>During the event, members of the KDE projects gave a couple of talks in the <a href="http://www.linux-events.de/LinuxPark_2007/LinuxForum/vortraege.html">Linux Forum</a>. On Saturday, Jürgen Gerner talked about KDE in everyday business while on Sunday, Torsten Rahn talked of the current state of KDE and gave a glimpse of KDE 4.0. On Wednesday, Daniel Molkentin of KDE PIM fame was a guest in a public round table on groupware systems, which targetted medium-sized businesses.</p>

<p>Due to the generous support from Novell, Canonical and Fedora we were able to give away a lot of CDs/DVDs to visitors so they could get a real-life impression of the power of KDE. Thanks also go to all those who staffed the booth, especially all first-time contributors and the people from the <a href="http://www.Kubuntu-de.org">German Kubuntu team</a>.</p>
