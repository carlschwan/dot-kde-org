---
title: "KDE Commit-Digest for 2nd September 2007"
date:    2007-09-03
authors:
  - "dallen"
slug:    kde-commit-digest-2nd-september-2007
comments:
  - subject: "Thanks"
    date: 2007-09-03
    body: "Thank you for yet another informative Digest! Your work is really appreciated, Danny!"
    author: "Andr\u00e9"
  - subject: "Re: Thanks"
    date: 2007-09-03
    body: "I have to agree. It is actually my favourite post of all in akregator :)\n\nCheers!"
    author: "Thomas"
  - subject: "Re: Thanks"
    date: 2007-09-03
    body: "It's not nice, but I hope he won't finish his study soon and get a job, as he probably won't have time to continue this by then ;-)\n\n(yeah, we love your work, Danny!)"
    author: "jospoortvliet"
  - subject: "Re: Thanks"
    date: 2007-09-03
    body: "(yeah, we love your work, Danny!)\n\nand yourself of course, which does make us all want you to finish your study on time... but..."
    author: "jospoortvliet"
  - subject: "Re: Thanks"
    date: 2007-09-04
    body: "I certainly look forward to it every week."
    author: "T. J. Brumfield"
  - subject: "Plasma"
    date: 2007-09-03
    body: "Plasma is shaping up pretty nicely; it has now a panel which reminds of Kicker, and the taskbar works pretty good now.\nOh. I like the idea of a \"Welcome Plasmoid\". :) Just hope to see some text under/[random preposition] the icons."
    author: "Hans"
  - subject: "Re: Plasma"
    date: 2007-09-03
    body: "> Plasma is shaping up pretty nicely; it has now a panel which reminds\n> of Kicker, and the taskbar works pretty good now.\n\nplease can somebody tell me how to get this (Plasma) working?\n\nI am trying KDE4 from SVN (using the Gentoo KDE overlay ebuilds), ocassionally deleting ~/.config and ~/.kde just to be sure there is no cruft causing misconfiguration\n\nnow it's over two weeks that I am getting a panel at bottom over the kicker, and on that panel there are messages like \"cannot create object\", no taskbar, no K-menu, no appplets ...\n\nso my favourite command of the last days is \"killall plasma\" - then that panel disappears, thus revealing kicker so I can work normally\n\nbtw, it's over month now that the desktop clock plasmoid has messed graphics on my system, it worked ok before\n\nbut I do not feel like filing any bugs before rc1 ;-)"
    author: "kavol"
  - subject: "Re: Plasma"
    date: 2007-09-03
    body: "As of yesterday night, plasma started to work a bit better again (but kwin doesn't do alt-tab for me).\n\nAnd no, don't fill bugs yet, unless you are capable/willing to help fix them (the 'willing' is more important than the 'capable', btw)"
    author: "jospoortvliet"
  - subject: "Re: Plasma"
    date: 2007-09-03
    body: "> and on that panel there are messages like \"cannot create object\n\nUsually this happens if you are missing the applets from playground, check out and build everything in trunk/playground/base/plasma/"
    author: "Robert Knight"
  - subject: "Re: Plasma and code in Branches :/("
    date: 2007-09-03
    body: "why is this developed in playground ? so it wont get tested by the other dev's :(. kde4 is pre-alpha anyway so you can code in normal trunk anyway."
    author: "asbest"
  - subject: "Re: Plasma and code in Branches :/("
    date: 2007-09-03
    body: "\"why is this developed in playground ? so it wont get tested by the other dev's :(\"\n\nHuh? Anyone can checkout and test playground."
    author: "Anon"
  - subject: "Re: Plasma and code in Branches :/("
    date: 2007-09-03
    body: "True, but I have to agree that it would probably not get as much exposure as it would when being developed in kdebase or something like that. "
    author: "Andr\u00e9"
  - subject: "Re: Plasma and code in Branches :/("
    date: 2007-09-03
    body: "They were started in /playground as prototypes.  They should be moved to kdebase soon."
    author: "Robert Knight"
  - subject: "Re: Plasma and code in Branches :/("
    date: 2007-09-03
    body: "It really should be moved ASAP until people get it from KDE Four Live or their distro Packages and give their ideas to Dev's.This is how FreeSoftware works..."
    author: "Emil Sedgh"
  - subject: "Re: Plasma"
    date: 2007-09-04
    body: "thankyou for the explanation ... so, I'd rather wait until it is moved (hope this happens soon)"
    author: "kavol"
  - subject: "Re: Plasma"
    date: 2007-09-03
    body: "Out of interest: What's the state of the menu replacement, Raptor? Is it already enabled and usable?"
    author: "Anon"
  - subject: "Re: Plasma"
    date: 2007-09-03
    body: "no, not at all, it is just a proof of concept and animation."
    author: "Emmanuel Lepage Vallee"
  - subject: "Re: Plasma"
    date: 2007-09-04
    body: "Then what will be the menu that will be in 4.0 or 4.x? The old k-menu?"
    author: "Anon"
  - subject: "EXCELLENT!!"
    date: 2007-09-03
    body: "Good work, as informative as ever, keep on it KDE team!!"
    author: "Quentin"
  - subject: "sonnet"
    date: 2007-09-03
    body: "does that mean sonnet is still being developed and will appear in the .0 release?"
    author: "eol"
  - subject: "Re: sonnet"
    date: 2007-09-03
    body: "I think you misunderstood the commit, the commit is to integrate sonnet's spellchecker engine to koffice, not to continue work on sonnet in kdelibs or to continue what was going to be implemented by Jacob Rideout. And AFAIK, the spell engine in sonnet is still like in the KSpell2."
    author: "ac"
  - subject: "Re: sonnet"
    date: 2007-09-04
    body: "I wasn't sure that Sonnet was finished on any level.  There was a lot of great promise with the project, but I thought it was dormant these days.  I wonder what condition Sonnet is in, and if it will be picked up again.\n\nFrankly, I think that is a technology that will touch most every user, and one that will help with how global of a product KDE is."
    author: "T. J. Brumfield"
  - subject: "Sonnet?"
    date: 2007-09-03
    body: "In what state is Sonnet, considering the disappearance of developer Jacob Rideout?\n\nAlso, the Welcome stuff was actually done by siraj, IIRC, not Ruphy :)\n\nOne of the kget devs made a nice screencast of KGet and the KGet progress Plasmoid last week, which would have made a nice addition to last week's digest, I think: does anyone know who that was, and what happened to it?\n\n"
    author: "Anon"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "What happened to J Rideout?"
    author: "foo"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "Nobody knows; he stopped committing and no longer replies to email. I have not been able to track anyone down who knows of his whereabouts and his phone numbers either are not answered or are somebody else's voicemail."
    author: "Adriaan de Groot"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "He is grounded."
    author: "Blitz"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "Do you say that because you know this for a fact? He's a college boy, how can he be *grounded* ? I've been assuming he's dead, actually, but I'd welcome any alternative explanation."
    author: "Adriaan de Groot"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "Well he might have suffered information overload and simply tunned out the online world, which is quite easy to do as people from the community ussually don't visit eachother in person. Sometimes people need to push an entire project out of sight, they may still have a positive attitude and will add todo's when you contact them even though they will never get around to them yet are unwilling to create closure because they still have high hopes and don't want to shatter any dreams yet.\n\nIt's good to see the work was evolved far enough for inclusion. And I thank Rideout for his efforts to bring it this far."
    author: "Dennis"
  - subject: "Re: Sonnet?"
    date: 2007-09-04
    body: "My guess and hope is that he was buried with other projects, or had to step back for a while because of real life issues.  With volunteer projects, people's interest will often spike and wane at odd times.\n\nI'm grateful that the code is out there, and I hope he returns someday to work on it some more.  In the mean time, perhaps someone else might be able to continue it."
    author: "T. J. Brumfield"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "where did you see the screencast?"
    author: "Bar"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "On irc, but I can't remember the dev's name, or the URL.  I do actually have a local copy of the screencast, but wouldn't want to host/ publish it anywhere without his permission."
    author: "Anon"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "You can see KGet plasma the screencast here\nhttp://linkalltheweb.com/temp/plasma_kget3.ogg\nhttp://linkalltheweb.com/temp/plasma_kget4.ogg"
    author: "jgoday"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "That's the one - pretty neat, I thought :)"
    author: "Anon"
  - subject: "Re: Sonnet?"
    date: 2007-09-04
    body: "I don't understand why kget is different from all otehr copy/move/etc jobs. Isn't this what the UIServer was created for? How do these two relate?\n\nIs there are reason why stuff like kget, ktorrent, kio (ftp, http, file, smb etc.) and all their similar friends are NOT using the same UI, the same plasma widgets etc?\n\nthanks for any explanations!"
    author: "anonymous coward"
  - subject: "Re: Sonnet?"
    date: 2007-09-04
    body: "+1"
    author: "G\u00fcnter"
  - subject: "Re: Sonnet?"
    date: 2007-09-03
    body: "You are right - the video was made by Siraj Razick. I have updated the text to reflect this.\n\nAbout possible extra Digest content: I can only include stuff that I am aware of! I welcome any submissions to the Digest email address, which is easy to find.\n\nDanny"
    author: "Danny Allen"
  - subject: "gwenview, kde and resizing"
    date: 2007-09-03
    body: "hi, do you think to add a support like this in gwenview ? http://bugs.kde.org/show_bug.cgi?id=149485\nmaybe at this point would be nice to implement it directly somewhere in kde so can be used from any application without reimplement it evewrytime, what do you think?"
    author: "xdmx"
  - subject: "Re: gwenview, kde and resizing"
    date: 2007-09-03
    body: "Yes, having a resizer that works this way would be too cool to handle with bare hands... "
    author: "Andr\u00e9"
  - subject: "Re: gwenview, kde and resizing"
    date: 2007-09-03
    body: "yes... i hope this gets into kde4 as a general picture widget and into krita, specialy the feature where you can mark the portion of the image you want to cut out. The algorithm seems quite easy to implement!"
    author: "Beat Wolf"
  - subject: "Re: gwenview, kde and resizing"
    date: 2007-09-03
    body: "What about **hint** SoC 2008 **hint** ? =)\n\nSeems like a perfect thing for a SoC."
    author: "AC"
  - subject: "Re: gwenview, kde and resizing"
    date: 2007-09-03
    body: "No... you dont need SoC for that"
    author: "she"
  - subject: "Re: gwenview, kde and resizing"
    date: 2007-09-04
    body: "I was especially amazed at the very last example, how you could add negative weight to an area, and then remove seams to very easily remove an object from an image, and yet morph the entire image as to make it seem like nothing was there in the first place.\n\nI am very curious to see how it would automate retargeting, and how it would guess how to correctly crop."
    author: "T. J. Brumfield"
  - subject: "happy to see Mr andersen back"
    date: 2007-09-03
    body: "happy to see that kplato is getting some attention again, recently their was a release of an open source project management software, openproj, but unfortunately they choose another license CPAL which is incompatible with GPL, so no code sharing  \"sigh\" "
    author: "djouallah mimoune"
  - subject: "Re: happy to see Mr andersen back"
    date: 2007-09-03
    body: "Yeah, Dag is committing on and off every now and then. Much like most open source developers he sometimes puts in lots of patches and then nothing for a while.\n\nI think what this mostly shows is that we need more people to help with KPlato, its already a great piece of code but its pretty big for one coder in his spare time.\n\nIf anyone here is willing to take a look at dialogs and clean them up; or work on any other part that you feel needs work in KPlato, please do join the mailinglist and see what there is for you!"
    author: "Thomas Zander"
  - subject: "[Little OT] Question in KDE4 install"
    date: 2007-09-03
    body: "Hi,\n\ni installed kde4 in ~/kde , but kde reads some stuff from /usr/share/... in kubuntu - things like start menu entries. They messed up the startmenu completly with kde3 , gnome and kde4 stuff.\n\nis there a way to completly ignore these system settings ?"
    author: "lukilak@we"
  - subject: "Re: [Little OT] Question in KDE4 install"
    date: 2007-09-03
    body: "Unless you want no mimetypes (thus KDE non even starting), no."
    author: "Pino Toscano"
  - subject: "Re: [Little OT] Question in KDE4 install"
    date: 2007-09-03
    body: "I've worked around this by installing freedesktop.org's shared-mime-info into ~/kde as well. "
    author: "Troy Unrau"
  - subject: "non-modal"
    date: 2007-09-03
    body: "Arnd Baecker (abaecker) committed a change to /branches/extragear/kde3/graphics/digikam/digikam/searchfolderview.cpp:\n\nAdd Yes/No Warning when deleting a search to avoid accidental removal of complex searches.\n\n---------\n\nIt would be great if a little dialog on the bottom of the screen would pop up with the option to undo the removal, instead of forcing the user to click Yes..."
    author: "jospoortvliet"
  - subject: "Re: non-modal"
    date: 2007-09-03
    body: "I think you know this is a bad forum for such feedback ;)"
    author: "Thomas Zander"
  - subject: "digikam"
    date: 2007-09-03
    body: "\"Download/Delete Selected\" and \"Download/Delete All\".\n\nWouldn't be \"Move\" more obvious/common/shorter?\n:)\n\nKeep up the good work!"
    author: "miro"
  - subject: "Blank Space"
    date: 2007-09-04
    body: "I'm not sure if that is actually the design of KDE4/Oxygen, or simply due to Oxygen still being worked on, but most KDE4 screenshots I've seen have a great deal of empty space.  Buttons are large, with plenty of white space around the text and icons.  Is this how the release will look?\n\nI'm just curious.  I know that in the end we'll be able to swap out widgets and styles, so I'm not sweating it too much either way, however for what it is worth, I think it is a bit too much white space, and it wastes valuable screen real estate."
    author: "T. J. Brumfield"
  - subject: "Re: Blank Space"
    date: 2007-09-04
    body: "I agree with you in that there is too much empty space.\n\nMaybe some people like it, good for them. I for myself like to use my screen for more than just emptyness... I hope configuration permits to have a more compact layout :-)"
    author: "Yves"
  - subject: "Re: Blank Space"
    date: 2007-09-04
    body: "It's not \"blank space\".\nIt's called \"whitespace\" and it's sooo arty.\n"
    author: "reihal"
  - subject: "Re: Blank Space"
    date: 2007-09-04
    body: "Ditto. I like to have the toolbars, buttons, etc. take up as little room as possible, so that the document is the focus of my work. They have to still be usable, of course."
    author: "Soap"
  - subject: "Release Date"
    date: 2007-09-04
    body: "Last week I asked for the team to move the release date back two months.  I threw out two months as a somewhat arbitrary guess of a time frame to better test and polish KDE 4.0\n\nI'm going to assume my suggestion wasn't exactly a catalyst, but merely more of a coincidence.  Either way, I'm extremely happy the release date is being pushed back.  While the digest suggested some might see this as a negative thing, quite frankly those who want it in October (or even now) can do so with beta releases.  I'm a big fan of release early, release often, however that is more fitting for gradual, incremental changes.  Basically, the entire KDE project was refactored, and it is hard to release that early.\n\nI often love playing with beta projects and accept the lumps that come with them, but I believe in the long run, a more polished official release will be seen as a a positive, and reflect well on the KDE team.  When you literally spend years working towards such a large goal, it is always nice to put your best foot forward with the release."
    author: "T. J. Brumfield"
  - subject: "Games?"
    date: 2007-09-05
    body: "am &#305; the only one wonder&#305;ng why so much work &#305;s go&#305;ng &#305;nto port&#305;ng and &#305;mport&#305;ng and develop&#305;ng games?\ndoes anyone know &#305;f there actually &#305;s a userbase for these games?\n&#305; dont want to d&#305;scourage anyone, but &#305; never reallz l&#305;ked any of the kde-games..."
    author: "Hannes"
---
In <a href="http://commit-digest.org/issues/2007-09-02/">this week's KDE Commit-Digest</a>: <a href="http://plasma.kde.org/">Plasma</a> continues to take shape. Continued improvements in KGPG and <a href="http://www.kdevelop.org/">KDevelop</a>. More KVTML format conversion work across <a href="http://edu.kde.org/">KDE-Edu</a> applications. Theme improvements in KDE Games. A new game, KSimili, is imported into playground/games. Initial work on a <a href="http://edu.kde.org/kalzium/">Kalzium</a> KPart for 3d molecular viewing. A redesigned configuration module for colours in KDE. Support for autodetection of gphoto2 cameras using <a href="http://solid.kde.org/">Solid</a> in <a href="http://www.digikam.org/">Digikam</a>. Annotation DRM support in <a href="http://okular.org/">okular</a>. Work on threading in <a href="http://www.mailody.net/">Mailody</a>. Orca screenreader support through Kross scripting in <a href="http://www.koffice.org/kspread/">KSpread</a>. Continued development on <a href="http://koffice.kde.org/kchart/">KChart</a> 2. Initial work on a Sonnet-based spellchecker for <a href="http://koffice.org/">KOffice</a>. Development on <a href="http://eigen.tuxfamily.org/">Eigen</a> 2 is restarted to follow a different implementation strategy. <a href="http://sourceforge.net/projects/qimageblitz">Blitz</a> is renamed QImageBlitz. The release schedule for KDE 4.0. is officially pushed back two months. KDE 4.0 Beta 2 tagged for release.

<!--break-->
