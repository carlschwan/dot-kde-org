---
title: "KDE and Distributions: ALT Linux Interview"
date:    2007-10-24
authors:
  - "Wade"
slug:    kde-and-distributions-alt-linux-interview
comments:
  - subject: "We need more"
    date: 2007-10-24
    body: "This is what users need. Get more GNU/Linux to schools and companies, we need free world where no one is there to tell users what they can use or what they can do.\nOf course, this dosn't mean that there would not be any windows, it's just one choise among others, but it should be ended now to force users to buy expensive hardware just to run their OS if that can be easily otherwise. \n\nAll what we need is that users can select any computer part what they want and they know it will work with GNU/Linux too. They know that they can go and buy ANY OEM PC with or without OS.\n\nI'm very happy about this Russian choise and i hope ALT will be chosen (and not *buntu ;-)) so free world can come true. And after all, this will be very big impact to Microsoft market share after 7-15 years...\nLet's just make clear for PC part manifactures that we need their support for other OS's as well and they can gain more market share too (and keeping open API's and open specs)!"
    author: "Fri13"
  - subject: "Re: We need more"
    date: 2007-10-25
    body: "Really, look at Alt and SXDE installed and compare them. KDE in Alt and Gnome in SXDE, financial accounter Ananas in Alt (is it really necessary for schools?) and Netbeans in SXDE, ay, some winmodems supported by Alt. Russian docs available in Alt that are partly outdated, partly inconsistent with current version. Slangy and inconsistent localization in Alt and less slangy, more classic Debian one in SXDE, both equally incomplete. AND THESE SEEM TO BE ALL DIFFERENCES EXISTING from the point of view of GUI-based user. Can we call it \"convergence\"? Maybe. But not competition, I think. Simply MS is aside -- and ALL other systems are alike, be it Alt, Hurd, MacOS or Solaris (ay, REactOS is free but like MS:) )."
    author: "worrabay"
  - subject: "More attention to KDE Education project"
    date: 2007-10-24
    body: "This is why all these Linux and open source educational projects, like KDE Education, deserve as much attention as possible. If they are made the best and teachers know about them then there will be a higher chance that they will use them and at the same time see that they don't have to spend so much money for proprietary solutions. And the more young people come in touch with Linux and open source and their principles (which also have high educational value) the higher the chance they will also keep it in mind when they buy computers at later time. So do help FLOSS educational projects as much as you can. Test the software, write articles about it, translate them into your native language, tell other parents and teachers about it..."
    author: "Neja"
  - subject: "Re: More attention to KDE Education project"
    date: 2007-10-25
    body: "It's nice to see FOSS software in schools however when 99 percent of the desktops run Windows or OSX, what will these kids do when they leave school and look for careers? Knowledge of Windows is pretty much a job requirement for any position these days.\n"
    author: "Gizmo"
  - subject: "Re: More attention to KDE Education project"
    date: 2007-10-25
    body: "> Knowledge of Windows is pretty much a job requirement for any position\n> these days.\n\nSo all the elementary schools running OSX are dooming our kids to a life of unemployment?\n\nLook at how much Linux adoption has increased in the last 5 years alone. By the time today's schoolkids will be looking for a job,  do you really believe that desktops will be only 1% Linux? Or that Windows-then will look like Windows-now, more so than the differences between Windows-now and KDE-now? Even if Linux stays in the minority, do you think there aren't \"niches\" in the computer ecosystem? Finally, if a bunch of kids grow up on Linux, don't you think the world might start to change once they start making the decisions?\n"
    author: "T"
  - subject: "Re: More attention to KDE Education project"
    date: 2007-10-25
    body: "Linux has been around for what, 15 years? In that time Microsoft has released some real bombs, up to and including Vista, yet Linux still hovers around 1 percent of desktop market share. I've been hearing for years how 'this is the year of Linux on the desktop\" and in truth it has not happened.\nIs Linux a good idea in education?\nMaybe, but all the good educational software, SAT prep, LSAT prep, K-8, etc software is written for either Windows or Macintosh.\nHaving kids play little match the number box games at the kindergarten level is one thing. Having a complete SAT prep course, language course etc at the seconday school level (Windows/Mac) is a completely different animal.\n\nSo if schools wish to save money, Linux might be a good choice.\nIf schools are intereseted in commercial level applications that tutor and enrich, Linux is a terrible choice.\n\n"
    author: "zip"
  - subject: "Re: More attention to KDE Education project"
    date: 2007-10-25
    body: "\"I've been hearing for years how 'this is the year of Linux on the desktop\" and in truth it has not happened.\"\n\nWhat does \"Year of Linux on the desktop\" really mean? Does it mean that in that year, 50% of windows users switch to GNU/Linux? Does it mean that 50% of commercial software makers starts making applications to GNU/Linux?\nIf it does mean that GNU/Linux is easy enough to be used by avarage joe and avarage Lisa, then it has already happend on 2006. We dont anymore see \"This is year of Linux\" because it has already happend. Now we just need to keep supporting that it will not go \"It was year of Linux few years ago\". \n\n\"Linux still hovers around 1 percent of desktop market share.\"\n\nThen that 1% desktop market share must come from our country (Finland) alone... because GNU/Linux is in media everyday, it gets installed everyday to new users in big installation rate, you can go to PC store and get GNU/Linux distro. You can almoust go any school (any level) and ask how many use GNU/Linux at home on PC and you get 30% hands getting up.\nOnly place where you have problems to see GNU/Linux is in big corporations what still hesitate to move. If those big corporations would start developing open softaware from those needed apps, it would pay itself back in 10 years and after that, it would be free from Microsoft.\n\nAnd it's just bulls't that GNU/Linux has only 1% marketshare. By tought estimate it is now over 5-7%\n"
    author: "Fri13"
  - subject: "Re: More attention to KDE Education project"
    date: 2007-10-29
    body: "Get a life. Your fud machine is getting rusty.\n\nLook, revolutions in IT are evolutionary tales whose real impact is only felt after the fact and with a lot of hindsight. If you cannot look back to the mid 1996-1997 when kde started and say that what has happened is revolutionary, you are blind.\n\nIn a decade we have built an amazing desktop that is localized into hundreds of languages and created the foundations for the future.\n\nThe proprietary applications will come. SPSS just released a Linux version and makes it possible to deploy Linux in our sociology department. \n\nYou can choose to see what you want, but your brand of bullshit is getting old."
    author: "Anonimos Anonimos"
  - subject: "Re: More attention to KDE Education project"
    date: 2007-10-25
    body: "Well, when I was at scool, there were no lesson on computer, but if it had been, I would have been teached comand line mono task things.\n\nAnd now, I am geting ready to work, what am I likely to work on ?\nVista/XP (not that much), or KDE (hopefully) or *cauth* CDE (because it seems HP-UX+CDE is widely used on the workstation of my targeted companies...)\n\nAnd last but not least : How long dit it take me to swich from WinXP to KDE the first time ? About one week to get used to it. Yay, so about 3% of my traineeship I may be a little disapointed by the new UI."
    author: "kollum"
  - subject: "Re: More attention to KDE Education project"
    date: 2007-10-26
    body: "Please read the following as a disagreement on what schools teach, not as a Microsoft vs Linux flame:\n\nThe purpose of computers in most public schools isn't to train kids to be Quickbooks gurus or Microsoft Access gurus, it is usually to teach them to type, read, or do math.  The only general software *most* kids learn in school is word processing and spreadsheets, and they only do basic stuff which is the same regardless of system.  The more advance computing in schools is usually done in programming classes...and for that, they probably should be using Linux.\n"
    author: "Henry S."
  - subject: "Re: More attention to KDE Education project"
    date: 2007-10-26
    body: "Knowledge of MS is vital?\n\nReally, the basic things someone does there is just click-click\n\nWhat kind of difficult knowledge is that???"
    author: "she"
  - subject: "ALT is not for schools yet"
    date: 2007-10-27
    body: "ALT Linux was my second Linux distro (the first was RedHat 7.1 from LinuxInk), and I used several releases of it for a fairly long time. What distinguished ALT in my opinion from others was the \"just works\" factor - they ensured that most of the hardware worked out of the box and distributed non-free drivers and plugins with their CD for seamless experience.\n\nHowever, on modern desktop Linux distribution standards, I wouldn't rate ALT Linux so high. I actually bought ALT Linux Desktop 4.0 (as I did with most of the previous versions), and it failed to displace Ubuntu Gutsy (dual-booted with Windows XP) on my laptop. I didn't even try to install it, just ran it in the LiveCD mode and it discouraged me from proceeding further. What I encountered was bloated, multi-level application menu with inconsistent, geeky localization, differences in font display (hinting) between GTK+ and Qt apps, sluggish, ugly and somewhat buggy (in its network-related part) ALT Linux Control Center, a sound of glass being smashed (ouch!) used as a system error sound, and the inability to connect to WiFi network (neither automatic connection nor a discoverable means to manually establish a connection was present). The documentation that come in the form of a brochure with the DVD was rather comprehensive, but written by techies for techies. That is not the kind of documentation that is usable at schools.\n\nTherefore, instead of going with ALT Linux, I would take some other prominent distro and have it localized by a team of paid professional (!) technical translators and writers - not some volunteer geeks. (The current Russian localizations in all distros I tried leave much to be desired.) OpenSUSE and Kubuntu are strong candidates here, but Pardus is IMHO the most clean and consistent KDE distribution out there - if you prefer KDE."
    author: "Artem"
  - subject: "Re: ALT is not for schools yet"
    date: 2007-10-28
    body: "Well, if you ever supported any noticeable Linux deployment then you must know already that both initial package base (and overall distro) quality are very important, but being able to fix what comes out broken in yet another particular situation is even more important.  That, in its turn, depends on the level of competence (or profanity) of those who would typically know the distro nearby and can provide the support needed.\n\nSo far I personally have varied experience with varied distro users with ALT users being mostly competent (or at least curious) and Ubuntaries (sorry!) tend to be dummies, way too often aggressively pushing their dummy agenda on others (just like sectants) but being unable to solve real-world problems.  Of course it's just my own observation but I communicate with hundreds of different people weekly.\n\nBTW your rants are partly OK (our menu currently sucks; wifi needs some serious work as well), partly invalid (I'd argue Ubuntu localization is way worse regarding consistency) and partly a matter of personal taste (like sound theme).\n\nThose that are OK are known bugs in work ;-)\n\nPS: I've done mid-sized migrations and projects (typically involving one to a few hundred systems), just in case you wonder.  I also participate in ALT since 2001 and seriously considered Ubuntu in 2005 but had to reject it for they were *much* worse than ALT, technically, back then -- and they still generate more noise than result till now.  I tend to avoid projects with bad SNR and faulty PR."
    author: "gvy"
  - subject: "MS will dump price close to 0 to win that tender!"
    date: 2007-11-09
    body: "\"Russian Ministry of Education and Ministry of Information Technology and Communications will soon announce a public tender for development and deployment of a GNU/Linux distribution for Russian schools. According to the government plans, this distribution will be installed on all the school computers in 65,000 schools in Russia (up to 675,000 computers). We will certainly take part in this tender.\"\n\n-----\n\nThat's nice to know. However, your Big $$$ Opponent will pull all strings against you, and use their accumulated wealth to \"sell\" their software dirt-cheap, for pennies and cents, when it comes to that tender.\n\nSee what a ZDnet blog has to report: http://blogs.zdnet.com/open-source/?p=1655\n\nSee what happened to Mandriva in Nigeria. \n\n(Update: now one high official apparently is going to stop that turnaround. He's going to make sure he'll get some money too. This is what ComputerworldUK says:\n\n[quote from http://www.computerworlduk.com/management/government-law/public-sector/news/index.cfm?newsid=6124\n]\n\"Nigeria's Universal Service Provision Fund (USPF) wants to keep Mandriva Linux on the Classmate PCs\", said an official who identified himself as the programme manager for USPF's Classmate PCs project.\n\n\"We are sticking with that platform,\" said the official, who would not give his name.\n\nThe organisation reserves the right to choose whichever platform is best for Nigerian students, which could also include Microsoft's software in the future, said the official.\"\n[/quote]\n\nThis sounds like he was overlooked when it came to the bribes, and now he makes sure that Microsoft will go make him an offer as well...)"
    author: "Kurt Pfeifle"
---
As part of our <em>KDE and Distributions</em> series (<a href="http://dot.kde.org/1153960955/">1</a>, <a href="http://dot.kde.org/1150244865/">2</a>, <a href="http://dot.kde.org/1152563989/">3</a>, <a href="http://dot.kde.org/1162426636/">4</a>) KDE Dot News spoke to representatives from <a href="http://www.altlinux.ru/">Alt Linux</a>. Russia recently announced plans to include GNU/Linux in <a href="http://eng.cnews.ru/news/top/indexEn.shtml?2007/09/14/266177">every school in the country</a>, and ALT Linux hopes to be the chosen distribution.  Below CEO Alexey Smirnov and Andrey Cherepanov answer our questions about their relationship with KDE.



<!--break-->
<h2>Past</h2>

<h3>Can you tell us about the history of ALT Linux? What
were your initial goals?</h3>

<p>ALT Linux was established in 2001 as a result of merger between IP Labs
Linux Team and Linux RuNet, both involved in creation of GNU/Linux-based
solutions since 1997. The primary focus of the company lies in the
sphere of development of Free Software and GNU/Linux-based solutions.</p>

<h3>Why did you choose KDE and which version of KDE did you first implement?</h3>

<p>Our first distribution included KDE 2.1. At that time we inherited
Mandrake's tradition of having KDE as the default environment.</p>

<h3>How did you find initial support for a new distro?  Is ALT Linux based
on another existing distro?</h3>

<p>ALT Linux started with tailoring of Mandrake Linux for the Russian
market. However, it soon became evident that full-fledged technical
support and more extensive customization were impossible without having
a package repository and establishing the distribution development cycle
of our own. That is why we created Sisyphus, which is now one of world's
5 largest Linux-based Free Software repositories.</p>

<h3>What could KDE have done better to help new distros use KDE?</h3>

<p>KDE was (and still stays) an attractive environment for both newcomers
migrating from Windows and experienced users. The primary factors that
determined our choice were attractive look-and-feel, customization
features and a rich pack of applications.</p>

<h3>What were your first impressions about KDE's documentation and community?</h3>

<p>KDE had decent documentation, and its community treated companies and
individual users in an equally friendly manner.</p>

<h2>Present</h2>

<h3>It is an exciting time for ALT Linux.  Can you share your opportunities
and recent news?</h3>

<p>In mid-2007 we launched a new 4.0 branch of ALT Linux distributions. The
first was ALT Linux 4.0 Server with support for virtualisation and our
own browser-based management framework named ALTerator. By the end of
August we announced a desktop version ALT Linux 4.0 Desktop with KDE as
default environment.</p>

<p>Now we are working on ALT Linux 4.0 Desktop Lite, a distribution for
less powerful computers and on ALT Linux 4.0 Small Business, which will
include WINE@Etersoft Local, a tool that allows to run accounting and
legislation database software particularly widespread in Russia.</p>

<p>Since 2003 we release distributions targeted at schools and are now
working on a new version of ALT Linux 4.0 Junior for Russian schools
that will include the full set of KDE Edu applications and other Free
educational programs.</p>

<p>Russian Ministry of Education and Ministry of Information Technology and
Communications will soon announce a public tender for development and
deployment of a GNU/Linux distribution for Russian schools. According to
the government plans, this distribution will be installed on all the
school computers in 65,000 schools in Russia (up to 675,000 computers). 
We will certainly take part in this tender.</p>

<h3>Do you feel that you have a good relationship with the KDE community?</h3>

<p>Yes, we maintain a close relationship with the KDE community, particularly
in the area of the Russian localisation. One of the key members of the
Russian KDE translation community, Andrey Cherepanov, is currently
employed by ALT Linux.</p>

<h3>What are things that KDE can do to help ALT Linux with all of this
activity?</h3>

<p>We believe that KDE team should pay more attention to the quality of
applications and their usability (maybe interfaces need to be simplified
a bit without sacrificing their functionality). Special attention needs
to be drawn to the needs of enterprise users, development of deployment
and management tools.</p>

<h3>Do you have any user feedback mechanism? If so, what feedback do they
have about ALT Linux and KDE?</h3>

<p>Our users communicate with developers directly by means of mailing
lists. We have bugzilla for tracking of bugs and feature requests on our
products. The mailing lists and bugzilla allow users to communicate in
Russian, which brings down the language barrier for many of them.</p>

<h3>In what ways do you customise KDE?  Any changes that are moved back
upstream to KDE?</h3>

<p>We do not change KDE deeply as Novell/SUSE does -- it is nice enough in
its original implementation. However, we include a patch for quick
search in KDE menu and also use a unified icon theme based on Tango and
a unified widget style QTCurve, which allows us to make consistent the
look-and-feel of KDE and GNOME/GTK+ applications.</p>

<h3>What KDE applications are the most popular among your users?</h3>

<p>Konqueror, Kontact, Kopete, Amarok.</p>

<h2>Future</h2>

<h3>What challenges does ALT Linux have with all of these opportunities?</h3>

<p>We will continue developing quality GNU/Linux distributions for servers
and workstations. We are ready to offer our package repository management
technologies to any interested party.</p>

<h3>What are your thoughts on KDE 4.0 and how it pertains to ALT Linux' goals?</h3>

<p>We hope that KDE 4.0 will increase users' productivity and will offer
developers a powerful environment to realise their ideas. Both KDE and
ALT Linux seek to make user experience in GNU/Linux environment really
comfortable.</p>

<h3>Any other plans for your distro in the future?</h3>

<p>We plan to take an active part in the programme of deployment of GNU/Linux
on all computers in schools and government. We hope that a new support
infrastructure for GNU/Linux across the whole Russia will be created
within this project, which will make the migration possible not only for
schools, but also for IT infrastructure of government agencies, business
and home users. The Russian government expects to migrate up to 50% of
computers within the government sector to Free Software within the next few
years.</p>



