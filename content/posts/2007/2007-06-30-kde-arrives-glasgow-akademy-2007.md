---
title: "KDE Arrives in Glasgow for aKademy 2007"
date:    2007-06-30
authors:
  - "jriddell"
slug:    kde-arrives-glasgow-akademy-2007
comments:
  - subject: "Flickr Tags"
    date: 2007-06-30
    body: "They should probably upload them to some online image sharing site and tag them consistently (akademy / kde4 / akademy2007) for grouping. :-)"
    author: "Roy Schestowitz"
  - subject: "Re: Flickr Tags"
    date: 2007-07-02
    body: "And don't forget a license. Journalists that want to report about aKademy will also want to use photos of us cute gearheads. Having some good photos available under an unrestrictive license helps them covering is."
    author: "Sebastian K\u00fcgler"
  - subject: "Hey, I know that guy in the middle!"
    date: 2007-06-30
    body: "Man I wish I was there! Next year work for you Jonathan? :) Have fun everyone, and make sure you leave me a little bit to work on!"
    author: "nixternal"
  - subject: "Will there be videos of the conferences?"
    date: 2007-06-30
    body: "I'm really sorry I can't attend this year's Akademy. Does anybody know if there will be videos and where will be hosted?\n"
    author: "Albert"
  - subject: "Re: Will there be videos of the conferences?"
    date: 2007-06-30
    body: "Really hoping the audio will be better this year :/"
    author: "Peter"
  - subject: "Re: Will there be videos of the conferences?"
    date: 2007-06-30
    body: "Explanation: Audio issues last year\n-Clipping so bad that you couldn't understand the voice: watch the VU meter and listen to the recording occasionally. Clipping could happen earlier in the chain.\n-Sometimes the microphone was too far away from people who talked\n-Line noise: filter it out using notch filters."
    author: "Andreas"
  - subject: "Re: Will there be videos of the conferences?"
    date: 2007-07-01
    body: "The talks are recorded on video so they will be available some time later."
    author: "binner"
  - subject: "Dammit"
    date: 2007-06-30
    body: "I wish I had known about this, I live in glasgow and would have loved to have seen it."
    author: "Pazy"
  - subject: "Re: Dammit"
    date: 2007-06-30
    body: "Oh well you could just drop by, I doubt anyone would complain."
    author: "Mark Kretschmann"
  - subject: "did I miss something"
    date: 2007-06-30
    body: "Milestone: Alpha2 Release\n\nDate: 27 June 2007\n\n\nehemm, where is the second alpha?\n"
    author: "bert"
  - subject: "Re: did I miss something"
    date: 2007-06-30
    body: "Check http://techbase.kde.org/Schedules/KDE4/4.0_Release_Schedule\n\n'tagging' is different from a release. It usually takes at least a week to roll the tagged snapshot into a release. This time is used for some testing and other preparations (packagers like to have some time as well)."
    author: "Sebastian Kuegler"
  - subject: "Re: did I miss something"
    date: 2007-06-30
    body: "The Alpha2 will be source only -- without translations.\n\nBut all KDE releases are source-only releases? At least officially??"
    author: "semsem"
  - subject: "Re: did I miss something"
    date: 2007-07-01
    body: "They are. The non-\"source-only releases\" are about waiting one week to give packagers time to create packages. Alpha 2 is waiting for a good announcement day (which week-ends are simply not)."
    author: "binner"
  - subject: "Re: did I miss something"
    date: 2007-06-30
    body: "http://lists.kde.org/?l=kde-release-team&m=118296200931273&w=2"
    author: "Emil Sedgh"
  - subject: "Why not at a sunny tropical beach?"
    date: 2007-06-30
    body: ":-)\n\nNetherlands -> Aruba, Curacao, Bonaire\n\nFrance -> Polyn\u00e9sie fran\u00e7aise\n\nSouth Africa -> Kwazulu-Natal, Buffels Bay\n\nBrazil -> Paraty, Porto de Galinhas, Fortaleza \n\nAustralia -> Scarborough Beach (the best white sand surf beach in the world)\n\n:-)"
    author: "I don't know my name"
  - subject: "Re: Why not at a sunny tropical beach?"
    date: 2007-07-01
    body: "If you think Scarborough Beach is the best white sand surf beach in Australia you really need to take a trip to Queensland :)  Why not take a tropical beach that's in the tropics?"
    author: "strider"
  - subject: "ooops"
    date: 2007-06-30
    body: "german only... but it seems there might be some problems at the airport...\n\nhttp://www.spiegel.de/panorama/0,1518,491617,00.html\n"
    author: "me"
  - subject: "Re: ooops"
    date: 2007-06-30
    body: "English:\n\nhttp://news.bbc.co.uk/1/hi/scotland/6257194.stm\n"
    author: "Anon"
  - subject: "Re: ooops"
    date: 2007-07-01
    body: "i dont think the english people memorize they are in war...\n\nyou cant kill people somewhere and go sleeping @ home without getting it back.. here you get it :)\n\nso taking the dot to a politicaly theme.. :/"
    author: "getit"
  - subject: "Re: ooops"
    date: 2007-07-01
    body: ">here you get it :)\n\nIt seems you are smiling, having fun, eh?\nLet's hope none of your relatives gets hurt in a crossfire of \"get them - get them back - get them hard - get them harder.\n\n"
    author: "getthemback"
  - subject: "Information wanted"
    date: 2007-06-30
    body: "Would be great if the presentations could be put online timeley. I'm dying for information, and there's no blog so far either!"
    author: "Phase II"
  - subject: "Re: Information wanted"
    date: 2007-07-01
    body: "that's actually an awesome idea! i second that!"
    author: "me"
  - subject: "Re: Information wanted"
    date: 2007-07-01
    body: "does anyone know why no one blogs?"
    author: "me"
  - subject: "Re: Information wanted"
    date: 2007-07-01
    body: "no network until now... :D"
    author: "jospoortvliet"
  - subject: "any influence of the airport attack in Glasgow"
    date: 2007-07-01
    body: "Just as the title.\nGod bless!!!!!!!!!!!!!!!!"
    author: "yuanjiayj"
  - subject: "Re: any influence of the airport attack in Glasgow"
    date: 2007-07-01
    body: "At least on some people: http://troy-at-kde.livejournal.com/3469.html\nMakes me wonder if the aKademy is actually happening at all, but let's hope it's only a minor disturbance and most things go all right. "
    author: "ode"
  - subject: "Re: any influence of the airport attack in Glasgow"
    date: 2007-07-02
    body: "We actually worried a bit about Troy, and it's a good thing that he's alright. It seems no other KDE people were affected by the incident, at least not at arrival. Those who depart today are still likely to get some nice delays, but aKademy is running great - we even have internet now :)"
    author: "Jakob Petsovits"
---
This evening KDE developers from around the world arrived in Scotland's largest city Glasgow for their annual KDE World Summit.  The week long meeting will see over 250 delegates from KDE and our partners discuss and hack on the world's original Free Software desktop.  Tonight the local team have been busy settings up the network, videos and other infrastructure for the attendees who are busy in the student bar below the building.  The first photos have been arriving on the internet.




<!--break-->
<p><img src="http://static.kdenews.org/jr/akademy-2007-livingston-tower.jpg" width="400" height="179" /><br />
Livingston Tower in the centre back, the week's venue.</p>

<p><img src="http://static.kdenews.org/jr/akademy-2007-neil-jonathan-kyle.jpg" width="300" height="225" /><br />
Some of the local team.</p>




