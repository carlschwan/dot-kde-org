---
title: "KDE Commit-Digest for 28th October 2007"
date:    2007-10-30
authors:
  - "dallen"
slug:    kde-commit-digest-28th-october-2007
comments:
  - subject: "Lancelot"
    date: 2007-10-30
    body: "Lancelot looks a lot like KickOff to me while raptor seems to be different with new ideas. Let's see how they turn out :)"
    author: "Patcito"
  - subject: "Re: Lancelot"
    date: 2007-10-30
    body: "I haven't seen raptor, but I think all of them are presenting cool ideas... I have no idea which one will be my final choice, but its awesome that we moved from one mediocre menu to three great sounding menus.\n\nActually, now I think I remember raptor... not sure if I'm a fan, but I'll keep an open mind."
    author: "Level 1"
  - subject: "Re: Lancelot"
    date: 2007-10-31
    body: "Lancelot acknowledges the flaws of Kickoff, that by limiting the display to a few options at once, and requiring clicking through a level of menus, you slow down the task of starting the app you need.\n\nVisually, Lancelot looks like Kickoff, and I think both look sharp.  Conceptually, the two are quite different, which is a good thing.\n\nI can't wait to try it out."
    author: "T. J. Brumfield"
  - subject: "Re: Lancelot"
    date: 2007-10-31
    body: "I thought Raptor was just fluff and mock-ups so far.\nWhen I tried KBFX, which is/was developed by the same guys that are making Raptor, afaik, what put me off was the sheer amount of bugs on all levels. Animations didn't work properly, integration didn't work properly, \"skinning\"/themes didn't work for me at all, etc. Granted, it's almost half a year since I tried it, but still."
    author: "martin"
  - subject: "No KOffice this time"
    date: 2007-10-30
    body: "Just to make things clear: there is no koffice 1.9.95.  KOffice passed this time, but will be back in force for the RC1 release."
    author: "Inge Wallin"
  - subject: "Sonnet"
    date: 2007-10-30
    body: "Nice to see that something happened with Sonnet again. Zack already mentioned that he thinks that Sonnet could be ready at shipping date.\nI wonder if that means that we will have a working live-spell checking system in all of KDE's text edit fields? In KDE 3.5 this is/was possible with kspell (and kmail, kopete, konqueror, etc.) and was a huge benefit compared with other OS."
    author: "liquidat"
  - subject: "Re: Sonnet"
    date: 2007-10-30
    body: "I hope so.  Spell checking in KDE 3 was ok, but not great.  No context menu for misspelled words was really annoying.  The \"check spelling\" dialog is quite outdated.  Most of the time I just played around with variations until the word turned black, or checked the spelling with google instead of going through the dialog."
    author: "Leo S"
  - subject: "Re: Sonnet"
    date: 2007-10-31
    body: "There is a dictionary Plasmoid, but I was really excited about Sonnet."
    author: "T. J. Brumfield"
  - subject: "Okteta?"
    date: 2007-10-30
    body: "KHexEdit is being renamed to Okteta???\n\nI thought renaming programs to obscure names that ensure that the mapping (task -> program) is as impossible as the mapping (program -> task) had fallen out of fashion? That seems so first-half of 2007 to me. I mean, khexedit is a perfect name. Three scenarios:\n\n(1) \"I wonder if there is a KDE hex editor on this machine?\" ... khex<TAB> ... \"Yup! There it is.\"\n\n(2) \"I need a hex editor!\" ... aptitude <slash> hexed ... \"There we go!\"\n\n(3) \"I wonder what this program does?\" ... \"No, actually I don't.\" ... \"It's called KHexEdit and I'm not an idiot after all.\""
    author: "Martin"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "From the digest, it sounds like Okteta is a new replacement hexeditor, not a rename of KHexEdit."
    author: "Lee"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "(1) that's why alt+F2 search in description now\n(2) use the search facilities of your package manager\n(3) read the description"
    author: "Cyrille Berger"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Is there some reason that couldn't have been implemented while still retaining descriptive app names?  \n\nThis is a retarded trend."
    author: "MamiyaOtaru"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Well, we could name all apps with a K in fromt of it, like Khexedit, Kwebbrowser, KMail, KWrite etcetera - but then everyone will say we are childish (after all, nobody else does that - there is no Gedit, or iTunes etc).\n\nSo we can just use Webbrowser, Email, Texteditor - and we'll be clashing with Gnome and other apps who can do the same.\n\nSo we must invent good names. You can't always make them descriptive, and if you try, ppl complain. Like they do if you don't, of course.\n\nAll in all, developers went their own way. Listening to the users doesn't always work, they will complain whatever you do. And having a name like Dolphin at least is more fun than Kfilebrowser."
    author: "jospoortvliet"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Not to mention that names like Dolphin and Okteka work way better for text based launchers such as Krunner.  (Just think how many matches there would be for K, versus D or O)"
    author: "Leo S"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Yeah, that's been discussed extensively. :-)"
    author: "Louis"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Don't want to nag the developers, but I was thinking exactly this when I read the overview of the commit-digest: I know \"khexedit\" doesn't sound as cool as \"Okteta\", but the former is much easier to remember than the latter."
    author: "[Knuckles]"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Neither is Nero named WinBurn, and Gimp isn't named GPhotoShop. Nobody complains about Amarok, Banshee or Exaile. MS Excel, Powerpoint and Access don't communicate a lot of meaning either, and QuickTime isn't the most descriptive name for a media playback solution.\n\nSo what? Restricting app names to their exact purpose is boring, and why shouldn't have developers the freedom to assign a witty yet applicable cool name to their application? Right. So, get over it."
    author: "Jakob Petsovits"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "I think that some distinction must be made between major applications that well be come well known in their own right such as Konqueror and the bunch of utilities that ship with KDE.  The major apps should probably have distinctive names but the little utilities should have somewhat generic names starting with 'K'.  I see this as basically a usability issue.\n\nNote for the record that GIMP is an acronym and acronyms such as KATE are perfectly acceptable for both classes of apps.\n\nI realize that there is an issue when a utility is replaced.  But, if another suitable generic K-name can't be though of, I would suggest that we use the old name.  One also has to ask why these utility apps are being replaced rather than just repaired/rewritten.  I see that as part of the problem."
    author: "James Richard Tyrer"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "> One also has to ask why these utility apps are being replaced rather\n> than just repaired/rewritten. I see that as part of the problem.\n\n+1 ... seeing the evolution of KDE (and other opensource projects), I have a bad feeling that a lot of people are doing their playground of it, rather than trying to really cooperate\n\nsometimes, it is good to throw away the old code, all the nasty hacks, and start from scratch (possibly using the experience from the old mistakes) - but most of the time I see (the details of) such changes, it's just laziness to learn about the old code ... just glueing together new bits is soooo easy, and the new computers are so fast and have too much RAM so that one does not have to care about writing nice code and saving resources ...\n\np.s. please do not associate this post directly with Okteta - it's just a common complaint; as far as what I know about Okteta, this is the case of using experience from the old mistakes"
    author: "kavol"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: ">> Note for the record that GIMP is an acronym and acronyms such as KATE are perfectly acceptable for both classes of apps.\n\nJust because you know it's an acronym doesn't mean any normal user will.  Gimp and Kate are usually written in lower case, and people will just read them as words.\n\n>>  One also has to ask why these utility apps are being replaced rather than just repaired/rewritten.\n\nMostly because you didn't step up to repair/rewrite it.\n\nSeriously though, it's the developer's choice to write a new app, or name it Wrzlbrmpft.  Contextual search launchers make this issue more or less irrelevant."
    author: "Leo S"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: ">> Note for the record that GIMP is an acronym and acronyms such as KATE are perfectly acceptable for both classes of apps.\n\nJust because you know it's an acronym doesn't mean any normal user will.  Gimp and Kate are usually written in lower case, and people will just read them as words.\n\n>>  One also has to ask why these utility apps are being replaced rather than just repaired/rewritten.\n\nMostly because you didn't step up to repair/rewrite it.\n\nSeriously though, it's the developer's choice to write a new app, or name it Wrzlbrmpft.  Contextual search launchers make this issue more or less irrelevant."
    author: "Leo S"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "> Neither is Nero named WinBurn, and Gimp isn't named GPhotoShop.\n> Nobody complains about Amarok, Banshee or Exaile. MS Excel, Powerpoint\n> and Access don't communicate a lot of meaning either, and QuickTime\n> isn't the most descriptive name for a media playback solution.\n\nyou missed one thing - all (AFAIK all) these names existed from the first public appearance of that software, no renaming (which is the thing that is questioned at the beginning of this thread, and to which the post you react to adds)\n\nto rename software that exists for years just because the name is not free-cool-and-in enough is simply stupid, IMHO\n\n(note: Okteta is a brand new program, I do not object to it's name but rather to the arguments used in discussions)\n\np.s. the full name is \"Nero Burning Rom\", so \"WinBurn\" would not say anything new about the purpose of the program - the fact that you are so lazy to use the correct name or so historically illiterate that you do not know that emperor Nero was famous for _burning_ Rome is another issue\n\nbtw, there exists \"Nero Wave Editor\", \"Nero ImageDrive\" etc. so\na) you really should use the whole name\nb) you've selected a really bad example :-p"
    author: "kavol"
  - subject: "Re: Okteta?"
    date: 2007-10-31
    body: "Having a name like Kate Text Editor would look pretty stupid with the description behind it, like in the KDE menu... Don't you think?\n\nWith the newer menu systems it's less of a problem, but for the KDE 3 series - this has once been discussed, and obviously dismissed."
    author: "jospoortvliet"
  - subject: "Re: Okteta?"
    date: 2007-10-31
    body: "You know you can set K-Menu et al to just show the descriptions? Then you don't have to see the silly (in your humble opinion) names the developers choose."
    author: "martin"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "I could not agree more. I start having difficulties to read the commit digest because the application name say nothing about what the application does. I believe that the trend towards meaningless names harms when people try to use the desktop and also from a branding perspective. Best example is this library, whose name I have forgotten, that always comes with a description how to pronounce it. Java, Firefox, Eclipse can do that... They are big... But not a hex editor or a library."
    author: "Dominic"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Do you mean khalkhi? I remember the name, but I have forgotten its purpose...  I also have difficulties reading the commit digest because the application names are meaningless.  In the case of Okteta it is not so bad, because the name contains \"octet\" which is related to \"hex\", but e.g. khalkhi and parley are more difficult.  I agree that for the small utilities such as KHexEdit, KWrite, KCalc, KFontView, KColorChooser, KSnapshot, ... it is better to keep the descriptive name with the K in front (the K shows immediately that it is a KDE application) and for larger projects such as Konqueror, Amarok, KWord, Okular, Kontact, K3b, ... it is nicer to have fancy names.  If there are fancy names only for the large projects, then you have to remember less names and you can select the program from the menu much faster.  The argument \"press Alt+F2, type hexedit and the dialog will give you Okteta\" is not valid, because not everyone might know the shortcut Alt+F2 and even if people know it, they may try to find the correct application by browsing the menu instead.  And when you browse the menu, it is easier to find the correct application if the titles give you all the information you need (for larger well-known projects the fancy name gives you all the information because the name is well-known, for smaller utilities the name gives you all the information only if it is descriptive)."
    author: "anonymous"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Kickoff displays not only the name of the application but also the description, and you can also configure the traditional K menu this way."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Parley is an awesome name.  Sounds like the french word for \"speak\" so it perfectly conveys the meaning of learning a foreign language (unless you're french I guess).  The previous name was KVocTrain, which I think was terrible.  Although the purpose can be figured out from the name, it requires quite a bit of effort, and the result is not a word or easy to say.\n\nAnd searching is also available in the menu, so you don't need to know about Alt-F2\n\n"
    author: "Leo S"
  - subject: "Re: Okteta?"
    date: 2007-11-06
    body: "And if you start your aptitude and update the softwarelist, you can't see the \"k-tools\" on the first view. With the old k-names, you was interesting in every tool with a k as first letter. Now you must read all software descriptions."
    author: "vicbrother"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "See my other comment:\n\nWell, we could name all apps with a K in fromt of it, like Khexedit, Kwebbrowser, KMail, KWrite etcetera - but then everyone will say we are childish (after all, nobody else does that - there is no Gedit, or iTunes etc).\n\nSo we can just use Webbrowser, Email, Texteditor - and we'll be clashing with Gnome and other apps who want to do the same.\n\nSo we must invent good names. You can't always make them descriptive, and if you try, ppl complain. Like they do if you don't, of course.\n\nAll in all, developers went their own way. Listening to the users doesn't always work, they will complain whatever you do. And having a name like Dolphin at least is more fun than Kfilebrowser. "
    author: "jospoortvliet"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "As someone that actually has GNOME installed (well part of it), I would like to clarify this.\n\nI presume that your saying that nobody has an app: Gedit was sarcasm.  That is the name of the GNOME editor.  GNOME actually has names for their applications, however the Name= in the 'desktop' file is often not the application name but rather a generic name.  So, Gedit (or gEdit) shows up as \"Text Editor\" in the menu (I don't exactly like this).  If we are going to use these totally non-descriptive names, we are going to have to do that also or at least have the menu default to having the description first."
    author: "James Richard Tyrer"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "The current method of showing Name (Description) is fine.  Showing description only sucks, because then you can't keep programs straight.  \"Text editor\"? Is that Gedit, kedit, kwrite, kate, or something else?  Not everyone is a new user."
    author: "Leo S"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "But people hate names like Kgolf, Kplayer, Kaudio, etc... \"Amarok\" is better then former \"amaroK\". So changing names in KDE4 is good IMO"
    author: "Kolo"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Indeed. Esp if the new names are smart, like Okteta, Parley, Decibel... I like those a lot."
    author: "jospoortvliet"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "> But people hate names like Kgolf, Kplayer, Kaudio, etc...\n\nthanks for noticing me that I am not human, since I do not hate these names ... I did not realise before 'cause my appearance etc. is so similar to that species\n\n... now, I only have to figure out what kind of creature am I"
    author: "kavol"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "dude....\nyou are human\nyou are not an average of all humans\nthere, wasn't too hard :/"
    author: "logixoul"
  - subject: "Re: Okteta?"
    date: 2007-10-31
    body: "Yet I think he was rather funny ;-)"
    author: "jospoortvliet"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Just like \"Amazon.com\" is a terrible name because it should have been \"Internetbookstore.com\"?\n\nPersonally, I think that having a distinct name actually helps people in growing a closer bond/association with an application.  Naming an application exactly for its task -- without any room for artistic license -- makes it sound like the \"generic toothpaste\" Euroshopper products...\n\nMoreover, the issue of not being immediately obvious is moot both for beginners and experienced users.  The former will see the description of the application in the menu, or use the search capabilities of their package manager's GUI.  The latter will simply issue \"apt-cache search kde hex editor\" (or whatever their distro equivalent) from the command line.\n\nMoral of the story: Okteta is a wonderful name.  It sounds good, fresh, and it reflects its task in a witty way.  I like it!\n"
    author: "dario"
  - subject: "Re: Okteta?"
    date: 2007-10-30
    body: "Projects need a distinctive name.\nAfter all, it's not the only KDE hexadecimal editor, let alone the only hexadecimal editor.\nApplications need a distinctive name.\n\nBut at the same time, people have hard time telling what a poetic application name means.\n\nThat discussion was raised a long time ago here:\nhttp://blogs.qtdeveloper.net/archives/2005/08/10/on-naming-of-kde-applications/\n\nAnd it's also why I named my application with both a strong-identity-name and a descriptive-name: BasKet Note Pads and Kirocker Music Display.\n\nI'm strongly in favour of using those naming, and in any place (menu, application, help, website...):\n- Okteta Hexadecimal Editor\n- Dolphin File Manager\n- Konqueror Web Browser\n- Akregator News Reader\n- Kopete Instant Messenger\n- Amarok Music Player\n- Konversation IRC Client"
    author: "S\u00e9bastien Lao\u00fbt"
  - subject: "Re: Okteta?"
    date: 2007-10-31
    body: "OK. So the KDE menu would look like:\n\nOkteta Hexadecimal Editor (hexadecimal editor)\nDolphin File Manager (filemanager)\nKonqueror Web Browser (webbrowser)\nAkregator News Reader (news reader)\nKopete Instant Messenger (chat application)\nAmarok Music Player (music player)\nKonversation IRC Client (chat application)\n\nWell, nicely done. You've really improved the KDE menu. Any more ideas?\n\nSorry for being so harsh, but hey, I'm right, aren't I? THIS IS NO ISSUE in linux. It only is in windows, where you only see folders in the Start menu with undescriptive names and links to executables in them with the same issue."
    author: "jospoortvliet"
  - subject: "Re: Okteta?"
    date: 2007-10-31
    body: "Jos, don't be ridiculous! The description in the brackets is not even required. You can change your settings to choose any combination of App name, description, or both in the KDE menus.  I really do think that his suggestion would look nicer than the description in brackets.\n\nSometimes I wonder if you even know your own desktop configuration settings :P"
    author: "Troy Unrau"
  - subject: "Re: Okteta?"
    date: 2007-10-31
    body: "Although you are not all englo-american people, you tend to forget that not every KDE user on the planet can read english (and even if they do, KDE aims to be usable by anyone).\nFor people who do not speak english, khexedit is not necessarily more obvious than okteta.\nIronically, for a frenchman, both names are quite equivalent, since \"octet\" is the french word for byte (and \"hexadecimal editor\" is \"\u00e9diteur hexad\u00e9cimal\").\n\nI think one should not rely on the application name to relate it to its function, but rather on localizable metadata (generic name, description, etc.). Anyway searching in metadata as easy as searching in names (excepted for the bash autocompletion... )."
    author: "Aldoo"
  - subject: "Just Work?"
    date: 2007-10-30
    body: "I'm wondering if this comment\n\n\"Like the most things related to Plasma, I intend to make Lancelot just-work(TM) for 4.0, and to make it Work-As-I-Think-It-Should(TM) for 4.1.\"\n\nIf perhaps KDE4 shouldn't be delayed until everything works like those writing it think it should.  I'm not sure I want to upgrade to KDE4 if things just work.  I'd rather wait i think."
    author: "Richard"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "Most people are going to want to wait.\nKDE 4.0 is going to be about the libs.\nTHe desktop is going to be torn apart and glued back together for 4.1, so this is probably going to be the release you should aim for to use."
    author: "Dan"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "> desktop is going to be torn apart and glued back together for 4.1\n\nwhat?\n\nlibplasma will be having internal changes, yes, and some applet will need to be updated because of this  .. but .. torn apart and glued back together? nothing quite that drastic."
    author: "Aaron Seigo"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "Actually, that sounds worryingly like the GNOME philosophy (do things one \"right\" way, anddon't give user options) to me!"
    author: "Lee"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "Don't be so alarmist.  Do we have to keep wheeling out the OMG-terrible GNOME bogeyman every time a comment can be tortured into reading as \"I intend to deprive users of all choice - muahahah!\"? It's getting tiresome."
    author: "Anon"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "It's Halloween-time.  The terrible Gnome Boogeyman will come out, remove all configuration code from svn, and turn KDE4 into QtGnome.  He will transform Dolphin into QtNautilus, Okular into QtEvince, ...  He will introduce plenty of bugs into the things that would make KDE4 better than Gnome (actually everything, but in particular Plasma).  He will then release a buggy alpha-quality KDE 4.0 in December so that the poor KDE users will be living a \"Nightmare before Christmas\".  And while he is at it, the Gnome Boogeyman will release a version of Konqueror with the evil QtWebkit instead of our beloved KHTML (and of course remove the option to use KHTML)."
    author: "anonymous"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "...And turn Qtwenview into Gwenview!  Oh wait...\n"
    author: "dario"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "Three menus (kickoff, lancelot, raptor) isn't enough options for you ? (especially considering that KDE3's menu didn't have that many options anyway)"
    author: "Cyrille Berger"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "> especially considering that KDE3's menu didn't have that many options anyway\n\nbut it had one option that those three are missing - usable application menu :-p\n\n... however, there should be one more alternative which will fix this for us evercomplaining; God(*) bless Robert Knight if he holds his promise\n\n(*) whichever he prefers"
    author: "kavol"
  - subject: "Re: Just Work?"
    date: 2007-10-30
    body: "Yep, and Dolphin is a Nautilus clone.\n\nNo intend to offend you, but these kind of comments are just nonconstructive considered as flaming by me.\nIn this context, I don't even see how you make that connection."
    author: "Hans"
  - subject: "Not this again :("
    date: 2007-10-30
    body: "Then please wait and maybe run 3.5 and 4.X on the same system until its the right time to switch for you.\n\nDelaying 4.0 until everyone is happy to switch from 3.5 is probably the slowest way of getting 4.X into shape. "
    author: "Matt"
  - subject: "Re: Not this again :("
    date: 2007-10-31
    body: "Matt, I tend to think when someone says it will just work, that perhaps it should be delayed until it works the way they want it to work, rather than putting something out that isn't what they want, but is what they have to do to meet the release schedule.  Just works put into my mind memories of the many press launches microsoft has held and had the blue screen of death appear. I only hope that when KDE offical launch this at the google launch, it is something they can be proud of, and that will draw users to it, rather than something that \"just works\"  \n\nIt should be delayed until things work right, and not just work, which just reflects badly on the kde teams if they release crap that is full of bugs and not doing what it should do.  I can go into any distro forum and ask if I should upgrade when kde4 is released, and always the same reply is that after the last major release people should wait til 4.1 because the last one was a nightmare full of bugs.  Is that the kind of press kde4 wants if it just works?"
    author: "Richard"
  - subject: "Starting to come together"
    date: 2007-10-30
    body: "The Trunk builds will have quite a few more deb depends for KDE 4 but with those accounted the SVN build process is starting to go a lot smoother. It's nice to see it come together.\n\nI'm looking forward to it's release."
    author: "Marc J. Driftmeyer"
  - subject: "Worst thing in 4.0"
    date: 2007-10-30
    body: "While there are some much great stuff in upcomming release, the Kickoff seems to be THE most disgusting one. Seriously. \n\nIf in 4.0stable it is only one i can choose, i will end up in switching back to fluxbox till Raptor or Lancelot gets stable. IMO Kickoff aint improvment .. its a step back =/"
    author: "mefisto"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-30
    body: "Your post is amazingly negative and at the same time uninformative.\n\nDo tell *exactly* why kickoff doesn't work for you and it can be fixed. Don't do it (like you just did), and you're a lame troll demotivating those that do actual work.\n\nI showed kickoff to my girlfriend yesterday, and she was really impressed (as I am, now I've given it some time). It looks good, performs rather well and you can find applications easily. (Type in \"cd\" if you want to burn a CD, for example, hoppa, there's k3b popping up. That's making the desktop and application accessible for those that didn't use it for years and happen to know about those app names.\n\nThose that do not use the KDE 4 desktop on a regular basis should, at this point just shut up."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-30
    body: "I'n not really fond of Kickoff either, mainly because browsing the applications is a pain. But then I have to admit that I've grown up with the old Windows menu, without being able to search etc - it's just about getting used to it, like everything else. And if you still don't like it, alternative menus are being developed.\n\nNow back to my main point: [@mefisto] If Kickoff really is so \"disgusting\" as you describe it, and many agree with you, surly there has to be someone who can \"port\" the old KDE menu to Plasma? I don't know much about coding, but in my ears it doesn't sound too hard."
    author: "Hans"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-30
    body: "> it's just about getting used to it, like everything else\n\nI do not agree, as for browsing applications ... but I would not repeat myself ;-)\n\nthe strenght of Kickoff is in reducing the need for browsing the applications - but for us used to fire up Krunner via alt+f2, kickoff brings no advancements in that field, so the ugly applications menu is all what is left ... and it is terrible to find out that the browsing experience is much worse than with classical KMenu\n\n> surly there has to be someone who can \"port\" the old KDE menu to Plasma?\n\nthere should be reimplementation available soon - see http://bugs.kde.org/show_bug.cgi?id=150883"
    author: "kavol"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-30
    body: ">> it's just about getting used to it, like everything else\n>I do not agree...\n\nWell, you should agree, as it is common consensus that a new way of doing something also means that you have to get used to it?!??! Very strange statement.\n\nI also want to point out a point in favour of the oh-so-bad old application start menu: I could simple structure it the way I want to have it, and design and use it almost exactly the same way I structured and used the Windows-menus I unfortunately have to use at work.\nTherefore I'm simply used to this kind of start menu both in work and at home, and I'm quick at using it.\n\nThis is not meant to troll about Kickoff, just expressing my hope that for dinosaurs like me a menu that is the same as it always has been will sooner or later be included in KDE4 ;)."
    author: "Peter Thompson"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-31
    body: "> Well, you should agree, as it is common consensus that a new way\n> of doing something also means that you have to get used to it?!??!\n\nok, let me explain:\n\nlet's pretend that I never used application menu before\n\nnow I am going to open 10 applications using the classical menu\n\nthen I am going to open 10 applications using the applications menu tab of Kickoff\n\nrepeat those steps 100 times - by that time I am \"used to\" Kickoff as well as to classical menu\n\nbut still, navigating applications menu in Kickoff is slower - so, it's not just the matter of \"getting used to it\""
    author: "kavol"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-30
    body: "I don't want to type in anything as my hand is on the mouse now I have to pick it up and use the keyboard??? I want a simple menu that is fast and is easy to find stuff. The classical Windows/KDE menu is that. Kickoff is far worse to find applications, you have to click all around multiple times to do something simple. I know Kickoff came from Suse and they supposedly did a usability study, I also know that one of the head people there is the original Gnome person. Would he not like to sabatoge KDE to make Gnome look like a better option, because Gnome is there long term plan, why else would you spend all those man hours porting YAST.\n\nThis is change for the purpose of change, not because something is better or more usable. "
    author: "none"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-31
    body: "Your right!!! [sic] And the government is trying to hush it all up."
    author: "Just me"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-30
    body: "I'm not mefisto, but i'm working on KDE4, as many here, and i'm partialy using KDE4 and Suse with KDE 3.5 with kickoff to be familiar with this menu concept (even if it isn't the same code as the KDE4 one). \n\nAs i understand the several posts (here, ml, irc...), there are several way to use this kind of menu. Some guy likes type a search or the apps name, others are using always the same apps, so a dynamic \"favorite\" is perfect. There are also the perfect beginner, who doesn't know what to use... And there some people, and i'm part of them (maybe because i learnt computer with win95, 98, KDE...  i don't know), which use and like a sorted apps menu to launch an app, with a very fastly mouse-over and 1 click, and no keyboard at all.\n   \nAnd kickoff, for us, kind of people which are maintly using the apps menu, is really a severe regression, when it is an improvement for others typing names or using favorite. We need 2 to 5x more time and click to launch an app. And with \"severe regression\", i mean it is really a pain to use.\n\nThat said, we can all give many big thanks to Robert Knight, because the fact are simple: Raptor or Lancelot will be not ready for 4.0. And because of him, and his really great work, we now have a working menu, and the 4.0 release will be possible.  He deserve our total respect. \n\nBut KDE4 is not KDE4.0, the futur is not writen. (is it?):\n- Raptor is developping interesting concepts, has a great dev&artists team...,  planned usability tests\n- Lancelot seems also developping interesting concepts, based on plasma (theme...)\n- Kickoff could also be improved during the now->4.1 lifetime, taking care of the positive/negative feedback, and perhaps also put its best ideas in Lancelot and Raptor, which are using plasma technologies.\n\nThis doesn't deserve flamewars for me, because there are no choice, and Kickoff is a good tmp fix for a fucking big issue, before to see something better (Raptor? Lancelot? Kickoff v2?) for defaut, like Plastik replaced Keramik some time ago for theme. \n\n\n"
    author: "Johann Ollivier-Lapeyre"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-31
    body: "Plenty of people have complained specifically about why Kickoff doesn't work for them (slow, needing to click several times, burying apps, etc) and they are rebuffed with \"experts say it is better!\"\n\nI'm not opposed to including Kickoff, or porting it.  I am opposed to it being the only option.  Please give users the options for a base KMenu as well in KDE 4.0, and hopefully we'll have Raptor and Lancelot as well for 4.1"
    author: "T. J. Brumfield"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-30
    body: "> the Kickoff seems to be THE most disgusting one\n> IMO Kickoff aint improvment .. its a step back =/\n\nplease point your browser here: http://bugs.kde.org/show_bug.cgi?id=150883\n\ndo not be so negative and just tell Robert that you prefer classical menu, and so you are glad that he is going to include it - I guess he will be pleased that users praise his work ;-)"
    author: "kavol"
  - subject: "Re: Worst thing in 4.0"
    date: 2007-10-30
    body: "Dude... why switch to fluxbox?  It's not like the developers are going to come knocking on your door to confiscate all copies of kde 3.  You are allowed to just keep using kde 3 until kde 4 becomes settled enough for you (which may be 4.1 not 4.0).\n\nBut aanstelligen like you never seem to rely on common sense to inform their arguments..."
    author: "Adrian Baugh"
  - subject: "Shredding functionality removed from KGPG"
    date: 2007-10-30
    body: "Why?\nDoes that mean it is is removed because KGPG should not be able to shred files?\n\nGreetings\nstrahler"
    author: "strahler"
  - subject: "Re: Shredding functionality removed from KGPG"
    date: 2007-10-30
    body: "\"This feature was never related to GPG at all, is removed from KDElibs and had some security implications. Since it will not work at all from now on better completely remove it before users start to argue.\"\n\nAdvantage for those that bother to actually RTFA. :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Shredding functionality removed from KGPG"
    date: 2007-10-30
    body: "And to explain the issue a bit better: this shredding doesn't work on modern filesystems, so it's useless anyway. Better not let the user have the illusion his files are shredded..."
    author: "jospoortvliet"
  - subject: "Re: Shredding functionality removed from KGPG"
    date: 2007-10-30
    body: "Could you define \"modern\"? On what filesystems does shredding actually work?"
    author: "ac"
  - subject: "Re: Shredding functionality removed from KGPG"
    date: 2007-10-31
    body: "Fat16...\n\nWon't work on any serious linux filesystem. There is no guarantee the FS will overwrite the file, if you write zero's into the file it's absolutely possible they will be written on an entirely different physical location - so you think you overwrote the file, but you have not."
    author: "jospoortvliet"
  - subject: "Re: Shredding functionality removed from KGPG"
    date: 2007-10-31
    body: "what about the 'shred' command?\nshred (1) - overwrite a file to hide its contents, and optionally delete it\n"
    author: "AC"
  - subject: "Re: Shredding functionality removed from KGPG"
    date: 2007-10-31
    body: "shred(1) has the same problem - relies on overwriting in place. If you need this functionality, you'd better shred(1) whole partitions."
    author: "tadeusz"
  - subject: "Little gems"
    date: 2007-10-30
    body: "> i need a librarian to wander around shushing my classes up.\n> \"Ssshhhhhh! This is a library! There are other classes here\n> trying to study!\" you know, the kind with the sorta geeky glasses,\n> hair bun and conservative clothes ... but cute.\n>\n> until then ... less debug output, please.\n\nlol :)"
    author: "Diederik van der Boor"
  - subject: "Application documentation for KDE4?"
    date: 2007-10-30
    body: "What's planned for KDE4 application documentation?\nWill there be a wiki of some sort to help improve it?"
    author: "AC"
  - subject: "Re: Application documentation for KDE4?"
    date: 2007-10-30
    body: "Hear! Hear!\n\nKDE4 wiki of any sort right from the start (or even earlier) would be great improvement in communication and gethering momentum."
    author: "Dawid Ci&#281;&#380;arkiewicz"
  - subject: "KDE-PIM bugs?"
    date: 2007-10-30
    body: "Hi.\nBeing a kubuntu user I tend to report my bugs at launchpad.net. If I report them there, but they need to get fixed upstream, do you guys who do the actual (excellent) work get to see the bugs?\n\nI've reported a few.\n\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/156606\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/155676\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/153810\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/152935\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/152916\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/151303\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/128685\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/150555\nhttps://bugs.launchpad.net/ubuntu/+source/kdepim/+bug/150536\n\nWhile I could probably report the bugs in kde.org too, it's quite tedious to try to figure out where to report the bug just to be told that this isn't the right place. Ideally the distro should forward the bugs to where they should be if they can't handle them themselves.\n\nAgain, thanks for all your great work. When I get my paypal account I'll be sure to send some money your way.\n\nOscar\n"
    author: "Oscar"
  - subject: "Re: KDE-PIM bugs?"
    date: 2007-10-30
    body: "All kde apps have a Help/Bug report menu item."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: KDE-PIM bugs?"
    date: 2007-10-30
    body: "Yes they do. And the first thing that happens when I click on it is that I must register my email to be able to submit a bugreport. \n\nThe problem isn't that I don't know how to do it, the problem is that I don't know if a bug that I see in my Kubuntu implementation of KDE is a kubuntu bug or a KDE bug. \n\n"
    author: "Oscar"
  - subject: "Re: KDE-PIM bugs?"
    date: 2007-10-31
    body: "> Ideally the distro should forward the bugs to where they should be if they can't handle them themselves.\n \nThis done for the distro by people like you who work on reporting bugs they find, and once they are familiar with the system, help sift through existing bugs and report them upstream if necessary (as well as other tasks).\n\nThere are several ways to know if a bug is an upstream bug or a distro bug. The easiest (but of course least accurate) is to use your intuition. The best is probably to compile KDE from SVN and compare.  You can also try it in another distro.  Or you can download the source deb and look at what patches kubuntu adds to see if there's anything in there that could be causing the bug (this also might help narrow down where the bug might be).\n\nReporting bugs on launchpad and on bugs.kde.org is fairly easy, and you can pretty much use the same text for both, so I don't think it's particularly tedious to report it in both places (at least not for you, the reporter ;) ).  Just be sure to link the bugs to each other."
    author: "Yuriy Kozlov"
  - subject: "Re: KDE-PIM bugs?"
    date: 2007-10-31
    body: "Oscar, as far as I can tell, there is a way to link the launchpad bug to the KDE bugzilla somehow and have the bug automatically sent upstream once it's confirmed to be an upstream issue.  However, while the ubuntu people have ensured me that this feature does exist somewhere, I'm not sure how to set it up.\n\nCheers"
    author: "Troy Unrau"
  - subject: "Re: KDE-PIM bugs?"
    date: 2007-10-31
    body: "Troy,\nThis is exactly what I wanted to hear. Thank you. Now I'll go \"bug\" some Ubuntu devs to get them to show me how I send my bugs upstream. \n\nI do want to help. I'm just a bit lazy so I don't want to exert myself doing it. I'm hoping a little help with bug-reporting is better than nothing at all. "
    author: "Oscar"
  - subject: "Yay KStars"
    date: 2007-10-30
    body: "Yay for Kstars with the big fat telescope !!!!!!!!!!!!!!\n\nprofessional APP!"
    author: "jumpingcube"
  - subject: "Re: Yay KStars"
    date: 2007-10-30
    body: "I agree - that is an amazing hack!"
    author: "Joergen Ramskov"
  - subject: "Re: Yay KStars"
    date: 2007-10-30
    body: "Hear, hear, I thought no-one was going to mention it.  Jasem, that seriously kicks ar$e, just about the ultimate KDE hack in my book :-)  Today, Jasem's big telescope, tomorrow mpynes nu-cu-lear submarine: world domination is within our grasp!"
    author: "Odysseus"
  - subject: "plasma"
    date: 2007-10-30
    body: "Hello all,\n\nthe svn builds are getting better, plasma seems to be falling in place :) great work. There is one annoying problem with plasma, if one of the plasmoids segfaults due to a missing .so due to the plasmoid being unstable, plasma gets killed! plasma needs a bit more resilience from bad behaving plasmoids\n"
    author: "anoni"
  - subject: "Re: plasma"
    date: 2007-10-30
    body: "+1"
    author: "Kolo"
  - subject: "Re: plasma"
    date: 2007-10-30
    body: "(frequently) compiling from SVN and watching the incredible rate of change has been a privilege! For a long time, every build I did contained an incredible number of new features and toys to play with. Now, things are getting more and more solid (the description of stable, not the KDE4 hardware system ;) and particularly since the last time I built everything is noticeably faster. I too have had plasma implode on occasion but I'm sure we're not alone in that and that a fix is on the way. Great Stuff!\n\n\nPS. for those doing frequent rebuilds like myself, one thing i find handy is to occasionally nuke the ~/.kde4 dir (obviously as this is build from svn stuff there is nothing of permanent value in here for me at this stage) as it seems to help to rollback all my fiddling around that has accumulated in app settings etc and get a nice fresh set of default settings. YMMV"
    author: "Borker"
  - subject: "Re: plasma"
    date: 2007-10-30
    body: "Borker great tip about refreshing the .kde4 directory :) I actually cleanup all the .* files from the kdedevel before trying out the next build. btw it still seems oxygen is not the default theme/style, make it default so more oxygen bugs/improvements can be suggested, not that important though as i change it after the settings get wiped off :)"
    author: "anoni"
  - subject: "Re: plasma"
    date: 2007-10-30
    body: "first, it doesn't segfault due to a missing .so. it fails to load the object and says so.\n\nhowever, a c++ based plasmoid going under will take plasma with it ... and plasma will attempt to restart itself at that point. there is little that can be done about that since running the plugins out of process is simply not an option.\n\nthis is why we will be encouraging people to write plasmoids in non-c++ languages as much as possible."
    author: "Aaron Seigo"
  - subject: "Re: plasma"
    date: 2007-10-31
    body: "about non-c++ languages: is it currently possible to write a plasmoid in ruby? i checked the techbase, but couldn't find anything about the current state of the ruby plasma bindings. am i missing something?"
    author: "alterego"
  - subject: "Re: plasma"
    date: 2007-11-01
    body: "The Ruby plasma bindings aren't usuable yet. The ruby version of the clock runs for about two minutes fine, and then a virtual method callback like Plasma::Applet.contentSizeHint() will fail. That throws a Ruby exception which isn't handled, and it causes Plasma to crash.\n\nSo the apparent memory corruption problem that causes the virtual method callbacks needs to be fixed. I not sure what is causing it because it doesn't happen when QtRuby or Korundum apps are run in stand alone mode. Secondly, the bindings runtime needs to handle Ruby exceptions more gracefully, and only terminate the Ruby applet and not the entire Plasma process.\n\nMaybe the Ruby version of the Kross api will end up working better in practice, and there will be no need for a more complete implementation of the entire C++ api in Ruby. I haven't an opinion either way, but I do think it is very important to be able to write Plasma applets in Ruby."
    author: "Richard Dale"
  - subject: "Re: plasma"
    date: 2007-10-31
    body: "That's really bad news, even more that the idea is that not only KDE guys, but others write plasmoids.\nNow think about installing a nice and fresh weather plasmoid and suddenly you loose the panel, application menu, launcher, desktop wallpaper and icons...\n\nTo me, it sounds a bad idea all those fundamental items depend on plasma, I always thought from the start one plasmoid dying woouldn't kill others (in any case, users don't care if plasmoids are python, c, php or c++). :-(\n\nBut I hope the users pressure will eventually make things different."
    author: "Iuri Fiedoruk"
  - subject: "Re: plasma"
    date: 2007-10-31
    body: "As Aaron said, people will be encouraged to use scripting languages for their Plasmoids (which gives the additional advantage of being easily installed via GetHotNewStuff) as these will not have this problem, or will have it to far a lesser degree.  Using C++ plasmoids should be no less stable than using C++ applets in the old kicker.\n\n\"But I hope the users pressure will eventually make things different.\"\n\nSince this is a very fundamental design choice, I wouldn't get your hopes up :) Running stuff out-of-process gobbles up a lot more RAM, too."
    author: "Anon"
  - subject: "Re: plasma"
    date: 2007-11-01
    body: "I may be mistaken, but say a Plasma applet on the desktop crashing won't take out the Panel (and thus the Application menu) or the launcher (KRunner) because they're separate processes (but use libplasma internally).\n\nI think there will be three processes:\n1.) The desktop/wallpaper (i.e. KDesktop now)\n2.) The panel/application menu (Kicker now)\n3.) The 'launcher' aka KRunner(alt+f2 dialog on steroids + lock screen + maybe more?)\n\nSo if anything, you'll be in a better situation than in KDE3 (KDesktop going down would take the Run Command dialog down with it currently)."
    author: "Sutoka"
  - subject: "kuiserver"
    date: 2007-10-30
    body: "****** Apply patch by ereslibre to remove the \"all network operations in a single window\" option, currently broken anyway since kuiserver isn't even called anymore. (so much for all the hype around kuiserver...)\n\nKuiserver, is that that thing which was supposed to show all 'progress' things going on in the whole system, allowing to have them aggregated in one place? I was looking forward to that, but it seems dead... KGet does everything by itself (even has a plasma applet), and it seems the most important user of such a thing. Pitty, it sounded a sane solution to all the separate progress viewing/reporting/notification implementations (kget, ktorrent, k3b, ark etc etc). What happened with this plan/app/tool?"
    author: "jospoortvliet"
  - subject: "Re: kuiserver"
    date: 2007-10-30
    body: "I'm also interested in this - I thought it was hooked into KJob, so that all KJob-using classes benefitted from it \"automagically\", as it were?"
    author: "Anon"
  - subject: "Re: kuiserver"
    date: 2007-10-30
    body: "Amarok at some point used it, and now it seems gone. I wonder what the outcome will be..."
    author: "Ljubomir"
  - subject: "Re: kuiserver"
    date: 2007-10-31
    body: "That was a feature I was looking forward to, what a shame"
    author: "Skeithy"
  - subject: "Re: kuiserver"
    date: 2007-11-01
    body: "Hi guys,\n\nFirst of all, I have to say I'm sorry because of this. We (or I?) simply ran out of time for 4.0. I preferred to develop a quality stuff rather than releasing a feature that wasn't to be as good as I expected.\n\nNow, I did this because I wanted an easy way of swapping between trackers (kuiserverjobtracker, kwidgetjobtracker...) that before wasn't and I had to do some adaptations on KIO code for that. Now it should be easy to adapt code to just set one or another.\n\nI listened to some constructive criticism and it shined me. It was getting so much space, and we also wanted to make the kuiserver inside plasma or something. There were too many things on the air.\n\nWith Qt 4.4 we have the ability of adding actual widgets to plasma, so I will work on this stuff hard to make it a success for KDE 4.1.\n\nIs just a matter of quality, and I hope you will like it when seeing it as a new feature.\n\nThanks for all your polite comments and ideas, you really rock guys."
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Re: kuiserver"
    date: 2007-11-01
    body: "That's great that it will continue to be worked on and appear in the future - it's an immensely popular feature! Thanks for taking the time to keep us informed, and set out minds at ease :)"
    author: "Anon"
  - subject: "Re: kuiserver"
    date: 2007-11-02
    body: "I'm very glad that this idea wasn't droped completely and forever. It's definitely one of the coolest features KDE 4 will have.\n\nThanks for the information - i'm looking forward to 4.1 impatiently. :-)\nStevie"
    author: "Steve Peters"
  - subject: "Re: kuiserver"
    date: 2007-11-20
    body: "Rafael, I browsed your blog every week to get informations about kuiserver. It's so revolutionary... we could get rid of the system tray with it.\nThink also to all the application stdout, not only the progress. I dream of a kuiserver with progress, stdout, events and commands... with such a thing we wouldn't use the taskbar or the system tray anymore.\nKeep on the good work!"
    author: "Murdock"
  - subject: "[FEATURE REQUEST] Sonnet multilanguage spelling"
    date: 2007-10-30
    body: "Sometimes KDE developers seem to neglect the existence of their own bugzilla, so I'm posting this wish here:\n\nPlease, make it possible to spellcheck two languages at once. I often have to make posts or write e-mails in two languages (Russian and English) and there's no easy way to spellcheck them."
    author: "Artem S. Tashkinov"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spelling"
    date: 2007-10-30
    body: "Wasn't that one of the killer features of sonnet? \n( http://liquidat.wordpress.com/2007/01/09/kde-4-sonnet/ )"
    author: "AC"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spellin"
    date: 2007-10-30
    body: "Sonnet is MIA unfortunately. :("
    author: "Ian Monroe"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spellin"
    date: 2007-10-30
    body: "MIA?"
    author: "Dolphin-fanatic :)"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spellin"
    date: 2007-10-30
    body: "Missing in Action - my guess."
    author: "Hans"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spelling"
    date: 2007-10-30
    body: "The main Sonnet developer, a linguist named Jacob Rideout, disappeared from the face of the earth sometimes at the end of February. As far as I know Zack Rusin took over the development of Sonnet but I don't know how much time he invested into this project or if he even has the necessary theoritical background. Zack claimed that Sonnet will be ready for 4.0 but how many of original planned features will be realized remains to be seen."
    author: "Erunno"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spelling"
    date: 2007-10-30
    body: "> Sometimes KDE developers seem to neglect the existence of their \n> own bugzilla, so I'm posting this wish here:\n\nWhich totally gives you the right to nag them everywhere you want.\n\nThis is Free Software, people (those that are actually informed) work on what they deem important, or for people that pay them (or are generally nice).\n\nShouting that they're ignoring you (while, with a bit of research, you would've found that Sonnet does just that) and demanding your right for a feature is the best way to be ignored in the future. And rightfully so.\n\nYou either have to send a patch, or be patient and nice. That's the game."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spelling"
    date: 2007-10-31
    body: "> Shouting that they're ignoring you (while, with a bit of research, you would've found that Sonnet does just that) and demanding your right for a feature is the best way to be ignored in the future. And rightfully so.\n \n> You either have to send a patch, or be patient and nice. That's the game.\n\nSo, you forbid us, mere mortals, to post bug reports and wishes via your bugzilla? ;-)\n\nDo I get you right?"
    author: "Artem S. Tashkinov"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spellin"
    date: 2007-10-31
    body: "this isn't bugzilla. it was perfectly clear what was meant."
    author: "Borker"
  - subject: "Re: [FEATURE REQUEST] Sonnet multilanguage spelling"
    date: 2007-10-31
    body: "If users aren't allowed to express feedback or give opinions on the dot, then disable commenting.  When you allow commenting, you are expressly implying that people have a right to do so."
    author: "T. J. Brumfield"
  - subject: "Commit-Digest Feed broken"
    date: 2007-10-30
    body: "The Commit-Digest rss feed is broken for me, since last week already.\nThe address being http://www.commit-digest.org/updates.rdf"
    author: "Anon"
  - subject: "Menus shouldnt contain buttons"
    date: 2007-10-30
    body: "Don't put Buttons, Lists or any other Widgets like that into menus!!!\nNO NO NO!\nIt confuses people it is horrible usability wise. It should be possible to use a Menu by keeping LeftMouseButton pressed and releasing on the menuItem that you wish to select.\n\nALL the menus I have seen for KDE4 suck big-time, really. And I don't want to flame. I really like KDE and I think that new aproaches like Katapult are great, BUT don't mix the stuff, just push a totally keyboard-controlled App-starter and keep the old KMenu as alternative for people wo want a menu.\n\nDont windowsify all the stuff, don't add widgets to places they don't belong. Dont add Mouse-over operations to the UI. JUST KEEP IT SIMPLE.\n\nAt least thats what I think!\nBut of course you can include anything in KDE if you think the people like it, as long as I can deactivate it ;)\n(which I will always be able to do since its free, I know...)"
    author: "Hannes Hauswedell"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-10-31
    body: "Took me a while, but you're talking about Kickoff, aren't you? Yeah, sorry about that. Nobody ported the current K-menu to KDE 4, and this piece of junk, written by Novell after extensive usability research, kindly contributed to KDE, is all we have. Poor users, how could that thing ever be usable.\n\n\n(sorry for the sarcasm, but I've heard enough complaints about Kickoff. If anyone sends a patch, fine, it will most likely be included. Otherwise, please either help us (eg TEST beta 4) or leave us.)"
    author: "jospoortvliet"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-10-31
    body: "/me hands Jos a KDE-flavoured chill-pill :)"
    author: "Troy Unrau"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-10-31
    body: "I really like KDE, like I said, and am using it for quite some time now.\nI also do some Qt/KDE-coding so I actually thought about making a menu myself, but prosponed the idea because of lack of time (and I really only use katapult and the quickstarter applet nowadays).\n\nI am looking forward to KDE4 and a lot of the new stuff, but I think there have not been many discussions about the KMenu. It looks like everybody was unhappy with the old one and just took what ever someone else brought up.\nSince Windows Vista has something similar it appears KDE wants to have a new one, too, although from my experience with both skilled and unskilled users these \"big menus\" are not received very well.\n\nI just hope someone comes up with a plan, because just offering three different menus that are similar in style, but unusable to most users isnt good.\n\nMy simple thought about Application starting is:\n* Make something like Katapult that is only invoked and controlled by the Keyboard.\n* Make something like the Original KMenu that is fully controlled by the mouse.\nand let the user choose.\n\nThe later could be more \"flashy\" than the old KMenu (bigger Icons, descriptions, whatever...) but should not contain non-Menu Widgets (NO Buttons, NO Scrollbars, NO filter Boxes) and should have a CLEAR and SIMPLE structure.\nAND No Menu-Items that change (e.g. no Favorites). Especially for unskilled users it is important to always find something in the same place.\n\njust my 2\u00a2.\n\nKeep up the good work!\nLooking forward to KDE4 :)\n\nP.S.: I might look into actually developing sth. like that if noone else does, but I won't start before KDE4.0-Release, since I have no time right now, especially not to make KDE4-betas run on FreeBSD ;)"
    author: "Hannes Hauswedell"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-11-01
    body: "The feedback that we got from users, especially non-geek users, was that they found deep hierarchical (old KMenu) menus hard to navigate and discover.  They just don't maintain the mental model that you call a 'clear and simple structure'. So we designed  Kickoff as a multi mode system with quick, linear access to a set of favourites, broad search, and the existing hierarchy.  Adding a hover to change modes makes it possible to use all the modes with the same number of clicks as the old KMenu."
    author: "Bille"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-11-01
    body: "Lots of people bashing the kickoff menu. I'm perhaps not the most sophisticated user, but nor am I \"your grandma,\" and I really dig the kickoff menu. There are only two things about it that I find annoying, and I hope they are fixed.\n\nFirst, I wish the activate-on-hover feature was configurable. I have the menu in the upper left corner of my screen and I sometimes hit it when I'm trying to close another window or something.\n\nSecond, the focus when searching is a bit messed up. While you can just pull up the menu and type a search term in, when you move your mouse to select the menu item that your search has returned, I often end up getting myself into another part of the menu (ie, my results disappear). I have to then point the mouse at the search field and move it vertically up to get to the search results. I think the behvior should be as follows: when you start typing, automatically move the mouse pointer so it hovers on the first result of your search.\n\nAnyway, I consider these issues to be fairly minor. I use the menu to launch most programs I use: they generally reside in the favorites section, but I sometimes do use the \"all programs\" tab to browse to certain programs, such as openoffice, kile, kbib, tork, konversation, and other less-used programs. I dig the kickoff menu, and I'm glad it will be included in KDE4. Thanks for the good hard work."
    author: "jml"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-10-31
    body: "Ok. Goodbye."
    author: "reihal"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-10-31
    body: "Somebody seems to work on it: http://bugs.kde.org/show_bug.cgi?id=150883"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-10-31
    body: "I agree that Kickoff is terrible, but accusing it of \"windowsify\"-ing KDE comes across as trolling."
    author: "T. J. Brumfield"
  - subject: "Re: Menus shouldnt contain buttons"
    date: 2007-11-01
    body: "yeah, the correct accusation would be of \"macosifying\" KDE ;-)"
    author: "kavol"
  - subject: "kdesvn-build"
    date: 2007-10-31
    body: "Is there somewhere a .kdesvn-buildrc around that is recommended for building KDE4 trunk?\n\nFlo"
    author: "Florian"
  - subject: "Re: kdesvn-build"
    date: 2007-10-31
    body: "just use the example (kdesvn-buildrc-sample) supplied along with the kdesvn-build. from http://kdesvn-build.kde.org/\n\nits basically modifying the modules listed here:\n# This subroutine assigns the appropriate options to %package_opts and the\n# update and build lists to build a default set of modules.\nsub setup_default_modules()\n{\n    @update_list = qw(qt-copy kdesupport kdelibs kdepimlibs kdebase kdeartwork\n                      kdemultimedia kdepim kdeutils kdegraphics kdenetwork\n                      kdeutils playground extragear);\n\ndelete the list of kde modules from above i.e., the ones you dont want to compile :)\n\nhth"
    author: "anoni"
---
In <a href="http://commit-digest.org/issues/2007-10-28/">this week's KDE Commit-Digest</a>: Further XMP tag support in <a href="http://www.digikam.org/">Digikam</a>. Beginnings of a <a href="http://plasma.kde.org/">Plasma</a> lock/logout applet and a weather applet, to display data from the existing weather data engine. Continued work on the new Plasma-based KNewsTicker applet. Continued work and development ideas in <a href="http://edu.kde.org/parley/">Parley</a>. More various developments and optimisations in <a href="http://en.wikipedia.org/wiki/KHTML">KHTML</a>. Jamendo album download support in <a href="http://amarok.kde.org/">Amarok</a> 2.0. Interface usability work in <a href="http://kopete.kde.org/">Kopete</a>. Shredding functionality removed from <a href="http://developer.kde.org/~kgpg/">KGPG</a>. Fixes in the Sonnet spellchecking solution for KDE 4. Work on the foundations of <a href="http://koffice.kde.org/kchart/">KChart</a> 2 in <a href="http://koffice.org/">KOffice</a>. <a href="http://ivan.fomentgroup.org/blog/2007/10/27/lancelot-revealed/">Lancelot</a> and Raptor develop as alternatives to the <a href="http://en.opensuse.org/Kickoff">Kickoff</a> menu of KDE 4.0. Okteta, a planned successor to KHexEdit, uploaded to playground/utils. KOffice 1.9.95 and KDE 4.0 Beta 4/KDE Platform RC1 are tagged for future release.

<!--break-->
