---
title: "KDE Commit-Digest for 7th January 2007"
date:    2007-01-08
authors:
  - "dallen"
slug:    kde-commit-digest-7th-january-2007
comments:
  - subject: "Technical issues with this digest..."
    date: 2007-01-08
    body: "There are a few technical issues with this digest (namely the lack of extended statistics and revision information pages/visual changes/etc.) that I will fix Monday morning when I wake up!\n\nThanks for your patience,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Technical issues with this digest..."
    date: 2007-01-08
    body: "Also, some characters are badly encoded: \"Rafael Fern&#65533;ndez L&#65533;pez\" instead of \"Rafael Fern\u00e1ndez L\u00f3pez\". The special characters are in ISO-8859-1, however the page is sent as UTF-8. Conversion from the original to UTF-8 is faulty."
    author: "NabLa"
  - subject: "Re: Technical issues with this digest..."
    date: 2007-01-08
    body: "Wow... sorry for the mess. Sorry Danny... xD\n\n/me wonders why his name has strange characters :P"
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Re: Technical issues with this digest..."
    date: 2007-01-08
    body: "All the technical issues with this digest should now be fixed.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Technical issues with this digest..."
    date: 2007-01-08
    body: "Thanks, Danny :)"
    author: "NabLa"
  - subject: "uiserver"
    date: 2007-01-08
    body: "I really really dig what is going on with the uiserver!"
    author: "slougi"
  - subject: "Plasma"
    date: 2007-01-08
    body: "Wouldn't it make sense to make a plasma component to show the uiserver information? There could be a component in the panel to discretely indicate activity that would expand into a pane on the desktop containing the progress information and the interaction buttons when clicked. It could also have mouseover information.\n\nThe uiserver information is some kind of system state that could perhaps be more logically presented in this way than in a regular window. This would of course be optional so you could still have the traditional interface if you prefer or if you don't run Plasma."
    author: "Martin"
  - subject: "Re: Plasma"
    date: 2007-01-08
    body: "> Wouldn't it make sense to make a plasma component to show \n> the uiserver information?\n\nYes.  But in order to write and display a plasma component, you need the Plasma application and libraries.  "
    author: "Robert Knight"
  - subject: "Re: Plasma"
    date: 2007-01-08
    body: "one also needs all the uiserver bits and knobbles first anyways. not to mention we need this as a backup in case the user *gasp* isn't running the plasma workspace ;)\n\nwe discussed how to handle this gracefully last week actually"
    author: "Aaron J. Seigo"
  - subject: "Re: Plasma"
    date: 2007-01-08
    body: "I realise that you would still need the normal implementation and it makes perfect sense to start with that. Hence the last sentence in my post above. My suggestion was in no way meant as criticism of the current work. \n\nIt's great that you are already thinking about how to incorporate this component into a seamless desktop environment!"
    author: "Martin"
  - subject: "What is the uiserver doing at all?"
    date: 2007-01-08
    body: "Could someone please explain to the non-C++/Qt-fluent audience here what the uiserver's basic task and role for a KDE desktop is?"
    author: "Kurt Pfeifle"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-08
    body: "The non-technical name for the uiserver makes things clearer.\n\nThis is the \"KDE Task Manager\".  It keeps track of all the long running tasks, such as downloading files, burning CDs or fetching emails which are in progress.  It provides a central place to see the progress of these tasks and in some cases control them ( pause a download or cancel a CD burn for example ).  \n\nAt the moment it only shows tasks which are in progress.  In the future it may also show tasks which have been completed."
    author: "Robert Knight"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-08
    body: "Task manger is unfortunatly a very bad name for this, since users from Windows know \"Task Manger\" as a tool ekvivalent to KDE System Guard. \n\nSince it keep track of the progress of running task, I think Progress Manger vould be a better description:-)  "
    author: "Morty"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-08
    body: "Probably is not the best name. It is not the name that will be on the kde 4 release... I think your suggestion is OK.\n\nSince I haven't committed yet I can update it.\n\nThanks."
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-08
    body: "My native language is not English, but IMHO it sounds a bit weird that one wants to manage the progress, and not the process that is progressing. What do you think about \"Background Task Manager\"?\n\nThinking about it, the term \"Background\" could be mislead non-technical users. Maybe something like \"Autonomous\" would be better?"
    author: "MM"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-09
    body: "How about \"progress monitor\"  or something then?  Maybe even something like \"The Progressor\" :-D"
    author: "JohnFlux"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-09
    body: "Progress Monitor is okay if one can only monitor the progress but not e.g. stop the process.\n\n\"The Progressor\" sounds good...\n"
    author: "MM"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-12
    body: "I like Task Manager.\n\nOr maybe Task List, Job List, etc."
    author: "Tim"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-12
    body: "I like Task Manager, too, but the post by Morty on Monday 08/Jan/2007, @06:02 remains valid, though...\n\n"
    author: "MM"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-23
    body: "The Progressor sounds stupid.  Progress Monitor is self-descriptive...at least much more so than \"The Progressor\".  Sheesh."
    author: "R gillen"
  - subject: "Re: What is the uiserver doing at all?"
    date: 2007-01-09
    body: "Thanks for the explanation, Robert.\n\nAnd this thingie also already had/has an incarnation that is in KDE3? Is it showing itself in the popping up progress bars once you start downloading a file or two?"
    author: "Kurt Pfeifle"
  - subject: "cooperation"
    date: 2007-01-08
    body: "\"[...] to see if we can co-operate and co-ordinate, letting Mathusalem list KDE jobs, and then of course, enabling uiserver to list GNOME jobs. This is possible because Mathusalem (and GNOME) uses D-Bus too.\"\n\nYes! Finally, cooperation. As I read about KDE's great features, I often think, \"What about non-KDE apps? I do use some - and they won't be able to use any of this.\" But this is good news. And, it's great that D-BUS is proving that it can in fact unite different desktops.\n"
    author: "Dima"
  - subject: "Re: cooperation"
    date: 2007-01-08
    body: "> I do use some - and they won't be able to use any of this.\n\nSure they can, the libraries are available for all applications to use.\n\nThere is no technical reason why an app can't use this stuff."
    author: "AC"
  - subject: "Re: cooperation"
    date: 2007-01-08
    body: "More important for this project should be to coordinate with the KGet developers. Since there is lots of functinality overlap between the two. \n\nCooperation inside of KDE is increadibly more important than possible cooperation with random non KDE projects. It's a case of having the left hand know what the right does etc. "
    author: "Morty"
  - subject: "Re: cooperation"
    date: 2007-01-08
    body: "likewise, i guess kget and ktorrent dev's should talk, as kget is rumored to get torrent support (and copying ktorrent would be a waste of time, right?)."
    author: "superstoned"
  - subject: "Re: cooperation"
    date: 2007-01-08
    body: "Comunication in a project is always good, and the waste of time comment I agree completly on. \n\nIn my oppinion the simplest and correct solution is to not implement torrent support in KGet at all. Since KTorrent already does the job quite well, and more importantly torrents does not fit into the usage case of KGet. KGet should download the qued items and quit, and allowing for pausing of downloads and restarts of broken connections. Manging upploads, share ratios, trackers and seeding does not fit into this. It will only give rise to (unneccesary) more complex UI and code for no real gain.\n\nThen again KTorrent should obviously have an option to provide progress indication using kio_uiserver.\n\n"
    author: "Morty"
  - subject: "Re: cooperation"
    date: 2007-01-08
    body: "I think bittorrent in kget will be useful only if it will be part of metalink ( http://www.metalinker.org/ ) support."
    author: "vrabcak"
  - subject: "Re: cooperation"
    date: 2007-01-08
    body: "finally?\n\nd-bus, mimetype specs, etc, etc haven't been enough for you? ;)"
    author: "Aaron J. Seigo"
  - subject: "Commit-Digest and KDevelop"
    date: 2007-01-08
    body: "Danny: just wanted to say that like the \"new\" format (I hadn't looked at the digests for a while, so I'm not sure how old it is)\n\nI'm really looking forward to the KDevelop developments.  Depending on the project, I use either Kate, KDevelop, Visual Studio, or Eclipse for programming.  Eclipse has some really nice ideas, but its C++ support is poor, and its support for embedded development is miserable, but Visual Studio and KDevelop aren't usable for my embedded work at all, so Eclipse is the only choice.  Visual Studio + Cygwin offers the most efficient programming environment by far in my experience, but Windows itself is no good for development.  KDevelop is the IDE I want to like the most, so I'll be watching with much anticipation to see whether v4 manages to achieve a well-integrated IDE for C++.  The ideas sound promising! :)"
    author: "Ellis Whitehead"
  - subject: "Re: Commit-Digest and KDevelop"
    date: 2007-01-08
    body: "Ellis. Could you please describe your embedded development workflow in our wiki page http://www.kdevelop.org/mediawiki/index.php/KDevelop_4_Proposals ? If you have ideas/suggestions on what features you'd wish for that, please add them also. We need to know what is needed because embedded development have never been our target.\n"
    author: "Alexander Dymo"
  - subject: "Re: Commit-Digest and KDevelop"
    date: 2007-01-08
    body: "Ok, but I'll have to give it a try again first to remember exactly which things don't function properly.  Part of the problem was related to the sequence of MI commands needed for remote+embedded debugging...  I'll try to look at the problems again soon and then add a suggestion to the wiki."
    author: "Ellis Whitehead"
  - subject: "Re: Commit-Digest and KDevelop"
    date: 2007-01-08
    body: "Thanks Ellis - 40 weeks now, since 2006-04-09 ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "KRunner"
    date: 2007-01-08
    body: "I was wondering why does KDE need a replacement for the \"run command\" program. I find it just perfect. What does KRunner offer to KDE than its predecesor didn't?"
    author: "NabLa"
  - subject: "Re: KRunner"
    date: 2007-01-08
    body: "Well, if it runs as a separate process, maybe it can run with higher priority, giving it higher responsiveness when your system is under heavy load. Also, when kdesktop / kwin locks, you will still be able to launch programs without resorting to the command line."
    author: "Constantin"
  - subject: "Re: KRunner"
    date: 2007-01-08
    body: "yup, and kdesktop is not available for kde4, so the 'run command' of kdesktop needed to come back in some way, either glued to another process or as a seperate process"
    author: "otherAC"
  - subject: "Re: KRunner"
    date: 2007-01-08
    body: "Quote from Aaron J. Seigo:\n\"--- \nthe new skills of this successor include being able to have multiple consumers of the entered text each of which can offer possible matches (think: search). it will also include such things as access to servicemenus (of konqi's action context menu fame). and it already does things kdesktop fails to do such as restart reliably in the case of a crash =)\""
    author: "Lans"
  - subject: "Re: KRunner"
    date: 2007-01-08
    body: "there is no \"run commmand\" program in kde2/3. it's a dialog inside of kdesktop. if kdesktop is to be replaced, we need to put that dialog (and the screensaver stuff) somewhere. i don't want it as part of another app like plasma since that raises the possibility that the user can lose access to the run command feature in case something goes bad with plasma. this happens with kdesktop already when it crashes; in fact, just this past weekend someone asked on irc how to get the run command dialog in kde3 after they'd accidently killed kdesktop by clicking on it after pressing ctrl-alt-esc ;)\n\nin addition to practical requirements (kdesktop going away) and finding a safer place for it to live, it is also getting new features.\n\ndoes that answer your question?"
    author: "Aaron J. Seigo"
  - subject: "Re: KRunner"
    date: 2007-01-08
    body: "ctrl+alt+backspace is always your friend after successfully killing kdesktop using ctrl+alt+esc."
    author: "asdf"
  - subject: "Re: KRunner"
    date: 2007-01-08
    body: "hell, no, why not use kicker or any other running thing to restart kdesktop?!?!?"
    author: "superstoned"
  - subject: "Re: KRunner"
    date: 2007-01-09
    body: "indeed, just start a konsole, open yakuake or whatever, type kdesktop & and your back online ;)\n"
    author: "otherAC"
  - subject: "Re: KRunner"
    date: 2007-01-11
    body: "throw out the baby with the water or what"
    author: "ac"
  - subject: "Re: KRunner"
    date: 2007-01-10
    body: "Indeed :) Thanks Aaron."
    author: "NabLa"
  - subject: "Re: KRunner"
    date: 2007-01-12
    body: "Won't that make it quite slow? Starting KColorChooser (simplest app I could think of) takes about 1 second on my system, but alt-F2 takes about half a second. Quite a difference.\n\nBesides if plasma crashes shouldn't there be some thing running to restart it like windows does with the shell?"
    author: "Tim"
  - subject: "KDevelop"
    date: 2007-01-08
    body: "KDevelop 4 looks soooo promising. I hope version 3.4 will be released soon, and work on version 4 can be done with full pace. And I want to use the (big) improvements of 3.4 ;).\nThe KDev team did a really nice job with 3.4 - I'm confident KDevelop 4 will be the best C++ IDE on all supported platforms :)\n"
    author: "Birdy"
  - subject: "Re: KDevelop"
    date: 2007-01-08
    body: "We plan to release it together with KDE 3.5.6. The estimated date of KDE 3.5.6 is January 23rd."
    author: "Alexander Dymo"
  - subject: "Re: KDevelop"
    date: 2007-01-08
    body: "Thanks! Nice to read that.\nI'm looking foward to a really great release."
    author: "Birdy"
  - subject: "Re: KDevelop"
    date: 2007-01-09
    body: "Ooh, that's my birthday :)\nThat'll be a nice birthday present!"
    author: "Matt Williams"
  - subject: "Amarok 1.4.5?"
    date: 2007-01-08
    body: "Uhm...so they said out in '06 for sure in the IRC chan...what happened? I see the changelog is huge and it's been some time...why not release? Erm... o.O"
    author: "fish"
  - subject: "Re: Amarok 1.4.5?"
    date: 2007-01-08
    body: "It's really quite simple - we wants 1.4.5 to be just right, because it looks like there is concensus that it will be the last release before development starts on 2.0. This is still up for debate, of course, but so far people seem to be leaning that way. So - it is not yet released, because it MUST be perfect. :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Akregator will be able to manage enclusures?"
    date: 2007-01-08
    body: "I saw the svn repository of the new syndication library, and it manages enclosures and podcasts. Can we hope a new akregator wich can manage enclosures (like RSSOwl, Feed'n Read, liferea... )?"
    author: "capit. Igloo"
  - subject: "Okular vs. Ligature"
    date: 2007-01-08
    body: "Seeing that Okular continues to improve quicly, with both improved featues and bug and mem leak fixes, and Ligature's development seems very slow at the moment (about 6 commits in one month), would not it be time to simply replace Ligature by Okular in kdegraphics4? I know this is an emotional subject for some people, but just looking at the facts, it seems to be clear to me which should be done now..."
    author: "Fred"
  - subject: "Re: Okular vs. Ligature"
    date: 2007-01-09
    body: "Its a bit odd to base the inclusion of an application on the commits of one month don't you think?\n"
    author: "otherAC"
  - subject: "Re: Okular vs. Ligature"
    date: 2007-01-09
    body: "But it's not at all \"odd\" to choose the the default application based on good and active maintainership.\n\nSee also\nhttp://cia.navi.cx/stats/project/kde/okular and http://cia.navi.cx/stats/project/kde/ligature\n\n(Not only look at the difference in activity, also look at the kind of changes being made and the number of developers having committed to the project, in the left box)\n\nI can only conclude that Okular is really actively maintained, while Ligature currently is almost dead."
    author: "Fred"
  - subject: "Re: Okular vs. Ligature"
    date: 2007-01-09
    body: "I believe Ligature is currently developed under the completely brainfucked name UNNAMED_READER - http://cia.navi.cx/stats/project/kde/UNNAMED_READER - which still has got much less commits than Okular, but seems far from being dead.\n\nWhat the Ligature devs (or rather, dev, as Stefan Kebekus seems to be the only one improving the app at the moment) are lacking is good promotion work, and getting others to join the project as developers or at least as supporters. Something that gets the masses interested. Something that makes people want this application.\n\nSo, we know that KPDF rocks and Okular is a solid improvement to KPDF. We have read that Okular supports a wide range of documents, and there's even a support chart on its web site: http://kpdf.kde.org/okular/formats.php ...where is Ligature's web presence? We have come to know that Okular uses poppler for its PDF support (there's also a link on their site), where do I get to know if Ligature uses it too, and how good its support is? Which improvements are in it for me if I use \"UNNAMED_READER\" instead of Okular?\n\nThe only thing that I always hear is that Ligature's DVI support is much better, which is due to its KDVI heritage. This is great, and I liked KDVI much better than KPDF when I wrote my bachelor thesis in LaTeX. But you won't win the battle for the universal page viewer with good DVI support. You'll win it with good usability, interesting features, speed with viewing and scrolling, more developers (who cooperate with other projects) and most of all perfect PDF support. Because as much as I like DVI, it'll always be a niche format while PDF is everywhere.\n\nFrom what I read on the dot, and on k-c-d, it always seems to me that the Ligature devs are only in a defensive position. There's a call for Okular, which is opposed by the Ligature devs listing a few areas where Okular still falls back, then the Okular devs respond with their app's advantages, and then everyone cuts each other into pieces with mutual insults. Or at least, that's how I perceive it.\n\nIf \"UNNAMED_READER\" is to be successful, there's really just two options:\n\n- Be vastly better than Okular. Simply being on par won't suffice for inclusion in main KDE4. The current state is that everyone, including important core developers, prefer Okular over Ligature as the universal page viewer, so you have to give them a reason why they should choose Ligature instead of Okular. Not the other way round, believe me when I say that won't work. Going down this path includes better promotion (probably a blog with new Ligature features, and working on a \"we are a friendly development team with a welcoming attitude\" image), working together with the usability team, and (have I mentioned it already?) exceptional PDF support.\n\n- Or, do one job well and concentrate on plain being the DVI viewer for Kile. Provided Okular's DVI support is really not good enough, going down this road will likely get you a safe place for Ligature where it doesn't have its existence questioned again and again.\n\nCurrently Okular is outside of KDE4's main branch and Ligature is inside of it, but you really shouldn't give a damn about this fact. Almost everyone wants Okular in, and if the Ligature devs don't improve a) their communications, b) their commitment to fast development, and c) their vision of what Ligature should be and not be, then Ligature will be out of there by the time KDE4 is released.\n\nDraw your conclusions.\n\nOh right, and have some announcement about what it is with UNNAMED_READER. I'm sure it's a nice in-joke and makes sense somehow, but it definitely confuses the common interested user."
    author: "Jakob Petsovits"
  - subject: "Re: Okular vs. Ligature"
    date: 2007-01-10
    body: "I have been told that UNNAMED_READER is not Ligature, so that's an error on my side. Please think \"Ligature\" everywhere I wrote \"UNNAMED_READER\". Sorry for the incorrectness, it's just that so much hints were pointing at the two apps being the same."
    author: "Jakob Petsovits"
  - subject: "Re: Okular vs. Ligature"
    date: 2007-01-10
    body: "well, if you vote for ligature in stead of okular, we'll forgive you :o)"
    author: "ac"
  - subject: "Geography"
    date: 2007-01-08
    body: "Thank you for the report!\n\nI wondered how many contributions came from the main countries. Germany and France are the sole countries above 10%, but how much exactly for each?"
    author: "Hobbes"
  - subject: "Re: Geography"
    date: 2007-01-08
    body: "Here is the breakdown for the top 5 of this week:\n\nde: 19.8\n(unknown): 19.2\nfr: 13.0\nnl: 7.75\nit: 4.73\n\nI plan to expose these more detailed statistics in the digest at a later date.\n\n\"(unknown)\" are due to cases where I haven't collected data for those developers yet - so, all KDE SVN account holders please go to http://commit-digest.org/data/ ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Geography"
    date: 2007-01-10
    body: "i went there, filled in my svn account, but got no response :o)\n"
    author: "Rinse"
  - subject: "Re: Geography"
    date: 2007-01-10
    body: "Ah, i was too unpatient :o)\n"
    author: "Rinse"
---
In <a href="http://commit-digest.org/issues/2007-01-07/">this week's KDE Commit-Digest</a>: Sonnet, the natural language checker, continues to develop and can now discriminate between more than 70 different languages. More work on the "konsole-split-view" branch to add split/merge functionality to the KDE 4 console. Support for filesystem labels in the "mountconfig" <a href="http://kde-apps.org/content/show.php?content=18703">Guidance</a> configuration module. Large developments in the "mailtransport" <a href="http://pim.kde.org/">KDE-PIM</a> work to enable code sharing between users of the common "emailing" action. Support for background text colours in <a href="http://konversation.kde.org/">Konversation</a>. Further work in the "Papillon" MSN Messenger connection library, with support for <a href="http://www.icq.com/xtraz_devcenter/">Xtraz</a> status and notifications in <a href="http://kopete.kde.org/">Kopete</a>. Gradient editing tool introduced across <a href="http://koffice.org/">KOffice</a>. Better support for PDF presentation files in <a href="http://okular.org/">Okular</a>. Improved AI in the recently-imported game KSquares. "Sublime", the new user interface library for <a href="http://www.kdevelop.org/">KDevelop</a> 4 is imported into KDE SVN. The initial code for KRunner, the KDE 4 replacement for the "Run Command" dialog, is imported into KDE SVN. The RSS Konqueror sidebar plugin is removed from KDE SVN, along with dcoprss and librss, which will both be replaced by libsyndication in KDE 4.

<!--break-->
