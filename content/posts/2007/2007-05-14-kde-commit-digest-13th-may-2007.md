---
title: "KDE Commit-Digest for 13th May 2007"
date:    2007-05-14
authors:
  - "dallen"
slug:    kde-commit-digest-13th-may-2007
comments:
  - subject: "Missing screenshots..."
    date: 2007-05-13
    body: "I'm aware that there is a gap where the screenshots for Galmuri are supposed to be - the result of an *ahem* \"email mishap\" (read: I shift-deleted them ;))\n\nI'll add them tomorrow after they are resent to me, Korean time :)\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Missing screenshots..."
    date: 2007-05-14
    body: "Huh, is this showing the first public oxygen widget style?\n\nTo be honest, I am not yet so much impressed by it, I *love* the style of the QFrames, but the buttons and text boxes still don't look really right. Especially since the textboxes are larger than buttons right now should be the other way round.\n\nBut well, that style is still far from perfect yet, just giving my feedback what an ordinary user like me likes and doesn't like."
    author: "Martin Stubenschrott"
  - subject: "Re: Missing screenshots..."
    date: 2007-05-14
    body: "I think it's peachy.  Better than any widget style currently available.\n\nThe only bit that jars my eye is the blue underline in the text boxes, which seems heavier and less illusionistic than the rest.  By \"illusionistic\" I guess I mean things like the glowing text boxes in OSX or the glossy buttons here."
    author: "Horris"
  - subject: "Re: Missing screenshots..."
    date: 2007-05-15
    body: "I fully agree with you. I _very_ much love the borders around the QFrames, although I think it might look better if they were closed at the bottom - dunno. The disabled buttons look promising imo, but the black list view headers remind me a bit of Vista / Windows Media Player 11. I don't want to say it's bad, I like the design of the WMP11, but it's not too original.\n\nI've just watched http://media.ereslibre.es/2007/05/kpluginselector4.mpeg  aswell, the new oxygen style is shown there, too. As far as I can tell from this video, the oxygen style seems too glossy for me. I like the scrollbar, even the round buttons at the bottom, and I love the checkboxes, but I don't like the tabs and buttons.\n\nWell, this is all work in progress, I know, and the first time a screenshot/video of oxygen was released, and I really don't want to be that destructive. I think it's good work, but a little more \"serenity\" wouldn't hurt. It's a GUI, not a game ;-)."
    author: "Thomas"
  - subject: "Re: Missing screenshots..."
    date: 2007-05-15
    body: "Based on the video, I agree that the tab background is a bit shiny and the black list headings and bottom gradients are maybe too intense, but those are (very) minor quibbles.\n\nWhat I like best is how subtle the gradients are - the slight emphasis on the highlighted button, the horizontal sweep across the whole frame, and so on.  Things like the scrollbar stand out just enough to be seen easily but not draw attention.  It's flat and shiny!\n\nAnd the video shows all the fancy glow/fade effects when switching tabs or checking a radio button.  Pretty fly."
    author: "Vanya"
  - subject: "Re: Missing screenshots..."
    date: 2007-05-15
    body: "indeed, the style is not ready yet. I have quite a lot of comments as well (like mose-overs not being clear enough sometimes) and bugs (inactive tab's do still have an incomplete mouseover effect) but we better wait voicing them until the authors starts asking for comments."
    author: "superstoned"
  - subject: "Re: Missing screenshots..."
    date: 2007-05-15
    body: "ow, and to add: I really love many elements of the style, like the gradients & animations. So I DO have very high expectations of future incarnations..."
    author: "superstoned"
  - subject: "Re: Missing screenshots..."
    date: 2007-05-16
    body: "Yep, me too, especially because the icons became really awesome!"
    author: "Thomas"
  - subject: "Potatismannen"
    date: 2007-05-14
    body: "Hooray for KTuberling! I love the Swedish sounds, which are done by a little girl. I hope they can be saved as well."
    author: "Martin"
  - subject: "Re: Potatismannen"
    date: 2007-05-15
    body: "I don't think we'll find time (or man) to change them, so yes, actual ktuberling's  sounds will be kept .\n\n\n\n"
    author: "Johann Ollivier Lapeyre"
  - subject: "KChart"
    date: 2007-05-14
    body: "I thought KDChart was a commercial program?\n\nThat would be awesome anyway. A really powerful KChart component!. The actual version can't do a lot of things and Openoffice is not very good at it either. That would be another area where KOffice would be clearly ahead of Openoffice.\n\nI can't wait for it. I will surely help testing at the very least.\n"
    author: "The Vigilant"
  - subject: "Re: KChart"
    date: 2007-05-14
    body: "Our (KDAB's) components are usually dual-licensed, like Qt itself, which means that there is a GPL license, and a commercial one. The Qt3 versions of KDChart, KDGantt and other bits and pieces have been used in KDE for a long time, and the new Qt4 versions will likely also be used, at least in kdepim and koffice.\n\nSide note:\n\nThe opposite of Free Software is not commercial software, it's proprietary software. KDAB and other companies work on and with Free Software and release products as Free Software or dual-licensed and make good money doing so. There is a lof of commercial (for profit) work going into KDE, by distributions, integrators, consultants, etc. and without those profit-driven contributions KDE would surely not be where it is today. KDAB and the Kolab Konsortium for example have put countless person-days of development and QA work into kdepim, paid for by our customers, which gets you features such as quota support or d'n'd in the folder tree in kmail , just to name two recent examples. Commercial Free Software is a reality, and a good thing, in my opinion, and that of the FSF. ;)"
    author: "Till Adam"
  - subject: "Re: KChart"
    date: 2007-05-15
    body: "Thanks for the explanation, it is now clear to me.\n\nWhere has the Qt3 version of KDChart been used in KDE?\n"
    author: "The Vigilant"
  - subject: "Re: KChart"
    date: 2007-05-15
    body: "KChart is the GPL variant of the Qt 3 version of KDChart."
    author: "Mirko Boehm"
  - subject: "Re: KChart"
    date: 2007-05-15
    body: "Er, not really.  KChart 1.x uses, i.e. embeds, KDChart 1 the same way that KChart 2.x will embed KDChart 2.x. The added value of KChart over KDChart is:\n * KPart for preview of documents in konqueror.\n * Embedding into KOffice, e.g. kspread\n * Configuration Dialog\n * Creation wizard\n * (1.6 and later only): rudimentary ODF support\n"
    author: "Inge Wallin"
  - subject: "Re: KChart"
    date: 2008-04-04
    body: "KCharts\u00a92007 - 2008  by Karriger Eng. & Mfg., Inc.\n\nthought you might like to know that another company has tried to copyright your name"
    author: "Ken Kuper"
  - subject: "Re: KChart"
    date: 2007-05-16
    body: "KChart needs an xy-scatter chart. Preferably in the next month or two, since I'll need it for a couple course projects :)"
    author: "Soap"
  - subject: "Re: KChart"
    date: 2007-05-16
    body: "Ah! You've got an itch -- now you just know you need to scatch it :-). Come over to #koffice @ freenode or the koffice-devel mailing list!"
    author: "Boudewijn Rempt"
---
In <a href="http://commit-digest.org/issues/2007-05-13/">this week's KDE Commit-Digest</a>: The <a href="http://dot.kde.org/1179060633/">KOffice ODF weekend sprint takes place in Berlin</a>. <a href="http://opensource.bureau-cornavin.com/ktuberling/">KTuberling</a>, the much-loved "potato man" game, is saved for inclusion in kdegames for KDE 4, with the start of porting to SVG and other general improvements. Rewrite of KPoker replaces the previous implementation. Xinerama improvements in the KWin window manager. Continued work on <a href="http://konsole.kde.org/">Konsole</a>. Usability and other improvements in <a href="http://developer.kde.org/~kgpg/">KGPG</a>. More progress in the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9B5F3C70D02AA396">Music Notation Flake shape</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> project in <a href="http://koffice.kde.org/">KOffice</a>. Version 2 of the KDChart library imported into KDE SVN to allow <a href="http://koffice.kde.org/kchart/">KChart</a> of KOffice 2 to be based upon it. The "systemsettings" set of utilities begins to be ported to KDE 4. Printing and Exporting functionality added to the KDE frontend to Marble. User documentation handbooks started for Marble and KAlgebra. KAlgebra, Marble and <a href="http://kdebovo.googlepages.com/">Bovo</a> move into the kdereview module for final review before moving into their respective home modules for KDE 4. KLatin is moved out of the kdeedu module for KDE 4.
<!--break-->
