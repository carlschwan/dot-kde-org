---
title: "KDE Commit-Digest for 10th June 2007"
date:    2007-06-11
authors:
  - "dallen"
slug:    kde-commit-digest-10th-june-2007
comments:
  - subject: "kdict"
    date: 2007-06-11
    body: "bah, I find kdict very useful, however crude it may be. Perhaps in the next release ..."
    author: "lynx"
  - subject: "Re: kdict"
    date: 2007-06-11
    body: "Removed from the kde-network part hopefully doesn't mean removed from KDE, but moved to extragear or something..."
    author: "superstoned"
  - subject: "Re: kdict"
    date: 2007-06-11
    body: "BTW I use it a lot as well, I esp loved the kdict applet in the kicker."
    author: "superstoned"
  - subject: "Re: kdict"
    date: 2007-06-11
    body: "It has been deleted in trunk. It is still in 3.5 branches.\n\nThe trunk version worked not at all. Feel free to grab it and make it working! (It's probably even better to start from scratch...)"
    author: "Urs Wolfer"
  - subject: "Re: kdict"
    date: 2007-06-11
    body: "i don't think there is a question as to weather a dictionary application is useful or not. it's that kdict wasn't ported, wasn't maintained and reimplemented all sorts of things that we have perfectly great APIs for now. it was written back when none of these things existed.\n\nyou could probably reimplement all of kdict in a couple 100 lines of code these days =)"
    author: "Aaron Seigo"
  - subject: "Re: kdict"
    date: 2007-06-11
    body: "That would make a sweet Plasmoid."
    author: "Louis"
  - subject: "Re: kdict"
    date: 2007-06-12
    body: "press ALT+F2, then type dict:blah if you're looking for a definition of \"blah\". this is the way I use dict currently. however, before I noticed that shortcut, I was using (and still sometimes) kdict alot. to sad to read this \"news\" '("
    author: "Christian Parpart"
  - subject: "Re: kdict"
    date: 2007-06-12
    body: "Yep, the shortcut is alright, but kdict pulls definitions from various sources and doesn't open a web page full of ads.  It hope somebody gets the motivation to port it.  I wonder why kdict never got integrated into, for example, KWord (right-click, Define Word) and the rest of KDE similar to the way KSpell works.  Just curious.\n\n"
    author: "Louis"
  - subject: "Re: kdict"
    date: 2007-06-13
    body: "because kspell is a library and kdict is a stand alone application."
    author: "Aaron Seigo"
  - subject: "Re: kdict"
    date: 2007-06-13
    body: "Right, I guess that's kinda what I meant; why not librari-tize kdict and make it available to all apps.  Seems like it would be a very handy right-click item in a lot of situations, and you say it would be a very small amount of code."
    author: "Louis"
  - subject: "Folding in KListView?"
    date: 2007-06-11
    body: "Well, the video is nice, but i ask me, how useful KListView will be. Has KListView something like folding? So the user could hide those categories, he does'nt need?"
    author: "Chaoswind"
  - subject: "Re: Folding in KListView?"
    date: 2007-06-11
    body: "And will there be more categories? Would be pretty cool to have files grouped by file type. Videos on one, music in another, executables in another, etc."
    author: "fast penguin"
  - subject: "Re: Folding in KListView?"
    date: 2007-06-11
    body: "Yes, it will be possible to group all kind of sortings (file type, name, date, permissions, group, ...). Currently the implementation is done only for file names, but it will be more a straight forward task to implement the other kind of categories later.\n"
    author: "Peter Penz"
  - subject: "Re: Folding in KListView?"
    date: 2007-06-11
    body: "Try posting your suggestions directly on his blog: \nhttp://www.ereslibre.es/?p=51\nhe might not look here.\n"
    author: "Coward"
  - subject: "Re: Folding in KListView?"
    date: 2007-06-11
    body: "Good point. I like the fact you select files by clicking the top line, but normally, one would expect it to fold..."
    author: "superstoned"
  - subject: "Plasma question"
    date: 2007-06-11
    body: "I seem to remember discussion a long time ago about how Plasma would work in multiple 'layers'; e.g., it would be not only the Desktop, but also Dashboard. Am I remembering correctly? If so, is it still planned?"
    author: "illissius"
  - subject: "Re: Plasma question"
    date: 2007-06-11
    body: "Dashboard?"
    author: "Carsten Niehaus"
  - subject: "Re: Plasma question"
    date: 2007-06-11
    body: "If you mean like on top of windows, I think the answer is 'yes'. However, AFAIK, it's still not implanted."
    author: "Lans"
  - subject: "Re: Plasma question"
    date: 2007-06-11
    body: "> If so, is it still planned?\n\nyes =)"
    author: "Aaron Seigo"
  - subject: "Re: Plasma question"
    date: 2007-06-15
    body: "This is really great... different layers like the applets, the desktop etc. rising up an coming down again, wow. OSX has the dashboard, true, but the concept has so much more potential!\n\nCan't wait to see where kde is going, but i know you guys have to take your time. Keep up the awesome work"
    author: "Phil"
  - subject: "Re: Plasma question"
    date: 2007-06-12
    body: "When i first heard of Plasma, i thought of some featuers i'd seen on OSX Leapard is there any similar features in the new proposed taskbar? http://www.apple.com/macosx/leopard/features/desktop.html\n\nthx\n\nSam"
    author: "Samuel Mukoti"
  - subject: "Re: Plasma question"
    date: 2007-06-12
    body: "What's so new at leopards taskdock? A little Eyecandy and the stack for folders, but what more? KDE has some (more or less) similar docks, and i think they there will be more coming with plasmas easy handling of such things. In fact, i wrote myself my personal taskdock just with pyqt/pykde and it's an easy piece of work (until i come to the artwork ^_-).\n\nOn the other side, i think apples taskdock-implementation sucks. There is to much bloat and it miss important functions. So i think kde4's community will have at the final of there development, some really nice solution itself :)"
    author: "Chaoswind"
  - subject: "Dolphin"
    date: 2007-06-11
    body: "Now I understand why it is named \"Dolphin\". \n\nhttp://www.gutenberg.org/files/11409/11409-h/11409-h.htm"
    author: "Andre"
  - subject: "Dolphin embeded in Konqueror"
    date: 2007-06-11
    body: "Now thats an interesting thing ...."
    author: "zvonsully"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-11
    body: "I'm very sad that nearly nothing is going on in Konqueror. Only because the all new Dolphin is coming around, everyone is putting one's power in it. The KListView looks very good, OK, and it is easier to use, agreed. But was there no possibility to do this in Konqueror?\n\nI would like Konqueror to be overworked for 4.1 or higher (or even reimplemented, if necessary) to incorporate the features that Dolphin has brought to us. For example, there is an urgent necessity for a more dynamic interface (I want a combined web browser, file viewer, etc.; but I need different functions for all of these).\n\nI hope that I do not tell new things."
    author: "stefanm"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-11
    body: "I agree! I wish also for an advanced konqueror, where  can link the signals of one kpart to slots of some other kparts, or where i can have as much tabbed-areas as i need (splitting is nice, but lacks important informations for navigation).\n\nBut, IIRC was one reason for dolphin, the lack of developer for konqueror. And also, as far as possible, the new features from dolphin will come to konqueror also, sooner or later."
    author: "Kami"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-12
    body: "this is not fair at all. there was no development on konqueror before dolphin even existed.\n\nbut more important: you allready get most of dolphins features in konqueror when the embeding of the dolphin main view is done.\n\nso, stop complaining!"
    author: "ac"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-12
    body: "I wish the insiders could post an explicit report on the state of Konqueror in KDE 4, here on the dot.\n Are they expecting Safari on KDE, so they can ditch Konqi? What's going on?"
    author: "reihal"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-12
    body: ">>I wish the insiders could post an explicit report on the state of Konqueror in KDE 4, here on the dot.\n\nit is already stated that konqueror will stay in kde4 as an advanced filemanager/webbrowser/fileviewer."
    author: "whatever noticed"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-13
    body: "Yes, we all know by now that it will stay. That wasn't my question.\n\nI want to know about KDE 4 specific changes and plans for the future, if any.\n\nWill Apple port Safari to KDE as thanks for the loan of KHTML? If so, then what?"
    author: "reihal"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-13
    body: "look in playground/libs/webkitkde."
    author: "Aaron Seigo"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-13
    body: "> I'm very sad that nearly nothing is going on in Konqueror\n\nyou shouldn't be sad; konqueror has changed more for 4.0 that it did for 3.5.\n\n> The KListView looks very good, OK, and it is easier to use, agreed. But was\n> there no possibility to do this in Konqueror?\n\nKListView is in kdelibs. it's used by konqueror today.\n\ncan i please suggest that instead of getting concerned about issues which are probably not graspable unless you either see the apps in action or understand how the code goes together beneath that, that you just wait for 4.0 to come out and try things out then?\n\n> incorporate the features that Dolphin has brought to us\n\nwhich would those be, exactly?\n\n> there is an urgent necessity for a more dynamic interface\n\nif there's an urgent need then someone will at some point step up and write the code."
    author: "Aaron Seigo"
  - subject: "Re: Dolphin embeded in Konqueror"
    date: 2007-06-16
    body: "You can't really embed Dolphin in Konuqeror.  There are two questions.\n\n1.  There are UI features that could be added to Konqueror.\n\n2.  There are new display methods that could be made into KParts so that they could be used in Konqueror.\n\nThese are both ideas with some merit.\n\n-- \nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Kopete Icons"
    date: 2007-06-11
    body: "Man, that avaible icon os VERY ugly in my opinion, and dosen't represent well what's going on (what's with that lock?). Is it just me to think it?\n\nSometimes it's better to use a simpler icon than something more realistc, because too much details make the idea do be lost."
    author: "Iuri Fiedoruk"
  - subject: "Re: Kopete Icons"
    date: 2007-06-11
    body: "They are not icons. They are Mac wanna be, italian design, photo-realisticly rendered pretty pictures.\n\ntackat and Everaldo made icons.\n\nThere, I said it.\n"
    author: "reihal"
  - subject: "Re: Kopete Icons"
    date: 2007-06-11
    body: "There, you are trolling. Post something constructive instead of just expressing distaste that will not help anyone and will just make contributors feel sour.\nIcons are a matter of personal preference, I admit, although I really like Oxygen over the overly-cartoonistic Crystal.\nBesides Crystal had license issues for a while (more like, a lack of license) and difficulty in obtaining sources. There are more advantages with Oxygen even from this point of view."
    author: "Luca Beltrame"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "And good trolling it is. \nIcons should be cartoonistic to be visible, you seem to object to them being \noverly so. Exactly how is Everaldos Crystal SVG icons too much cartoonistic?\n(And please, no gnome-talk about licenses)"
    author: "reihal"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "They're rather childish-looking, in my opinion (probably a better adjective than \"cartoonistic\"). Not to mention that as far as I can tell the set hasn't been updated in a long time (but feel free to correct me if I'm wrong).\n\nI prefer the Oxygen approach, which IMO is also quite polished (and we see progress every day) and also more \"transparent\" because we see what is going on, even though there is an unavoidable personal touch in these things.\n\nLicenses *are* a problem when you want to reuse someone else's work, and Crystal did not have any for a long time. Not to mention that the original SVGs were hard to obtain."
    author: "Luca Beltrame"
  - subject: "Re: Kopete Icons"
    date: 2007-06-15
    body: "We should try to avoid what appears to be trolling because there are valid issues here.\n\nIt is a valid criticism of CrystalSVG that there were usability issues.\n\nIt is a valid issue that icons which are too realistic are not easily visible -- especially in smaller sizes.\n\nI agree with the opinion that icons need to be cartoonistic to be visible.  So, I started an icon theme to illustrate that point:\n\nhttp://www.kde-look.org/content/show.php/USR+HiColor?content=27410\n\nThis has only received a 40% good rating.  I don't know if this means that people don't think that they are good or simply if they wouldn't use this style.  So, I didn't continue to make a full theme.  OTOH, if I knew that 40% of the KDE users value usability over eye-candy, then having such a theme is needed."
    author: "James Richard Tyrer"
  - subject: "Re: Kopete Icons"
    date: 2007-06-11
    body: "Yes you are right!\n\nThey are not just icons, They are Mac wanna be, italian design, photo-realisticly rendered pretty pictures...\n\n... they are wonderful! ^^\n"
    author: "Mercurio"
  - subject: "Re: Kopete Icons"
    date: 2007-06-11
    body: "> Mac wanna be, \n\nif by \"mac\" you mean \"sophisticated and elegant\", then .. sure. that's what makes the \"mac look\", well, the \"mac look\". while i'm certain that there are people who don't like such an approach (no visual approach pleases everyone) it does tend to be more widely appreciated.\n\n> italian design,\n\ngood job on the nationalistic stereotyping there, buddy. *sigh*\n\n> photo-realisticly rendered pretty pictures.\n\nactually, only the physical device icons are photorealistic. the rest are not photorealistic at all, but simply high quality art that emphasizes beauty over starkness. starkness is useful in small form factor design or for serving the needs of, for instance, low vision users (we have the accessibility pack for that). given that we have a solution for accessibility and that the icons will be used overwhelminginly on 1024x768 and above displays (though they look great on 800x600 too; i test at that res regularly) it makes sense.\n\nbtw, if you're wondering what the definition of photorealistic is, you should check out this article: http://en.wikipedia.org/wiki/Photorealistic"
    author: "Aaron Seigo"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "Hey! I kinda' thought that the \"Italian design\" comment was more of a complement than anything else! after all, architecture, automobile design, food, wine, pottery, glass work.......well you see what I'm getting at. not that I am saying that these things are lacking anywhere else in the world but, c'mon! Italy!\n\nAll joking aside, the Oxygen Icons are different, different gets people saying things that are insulting and and of course there are the Mac trolls comparing everything that the KDE dev's and artists create as poor copies of their preferred OS. same with the MS trolls and the Gnome trolls. you cannot do anything about trolls I guess. \n\n"
    author: "madpuppy"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "\"Italian design\" was neither nationalistic stereotyping nor a complement.\n\n\"Design\" in the esthetic sense of the word, indicates a tendency for \"form before function\"."
    author: "reihal"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "And what does \"Italian\" indicate? ^^ \n\nCuriously the same English word \"design\" is used in Italian also, but I never found it be used with that meaning.\n\nNationalistic or semantic stuff aside, usability is the main concern of Oxygen artists, you can easily see it reading their blogs, and I'm sure they will welcome your contribute.\n\nOn the other hand they can't make a graphic style that can please all, and it seems most people like the current, Portuguese-design (^^), Oxygen's one.\n\n"
    author: "Mercurio"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "Batch process the lot with more contrast, thats my contribution.\n\n(Lets avoid this quagmire of semantics for now)"
    author: "reihal"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "\"mac look\" appeals to artsy-fartsy types. Are we not nerds? Yes, and nerds demands nerdy icons!\n\n\"Italian design\" is a concept and a multi billion $ industry. Here's nationalistic stereotyping, buddy:\n\"Stupid canucks. I hate them so much\" - Homer\n\nYou avoided to diss tackat and Everaldo, and rightly so. I remember tackat made an explicit point in \nhaving a black outline on the icons to make them visible.\nThe main problem with the Oxygen icons is low contrast, which make them look fuzzy.\nThe larger the display is, the fuzzier will the small icons be.  \n"
    author: "reihal"
  - subject: "Re: Kopete Icons"
    date: 2007-06-13
    body: "> Are we not nerds? Yes, and nerds demands nerdy icons!\n\nwell, i hate to break it to you, but ...... most of our users aren't nerds, and not all (or even perhaps most) nerds like (let alone demand) \"nerdy\" icons.\n\nthe defaults are for the majority, the mainstream. you can find alternatives in kdeartwork, including ones that probably work as \"nerdy\".\n\n> \"Italian design\"\n\nah, so you aren't refering to the artists involved, some of whom are actually Italian. \n\n> Stupid canucks\n\nindeed, screw them. especially the ones who actually have a clue about the topics at hand. damn them!\n\n> You avoided to diss tackat and Everaldo\n\nwhy would i? this isn't about taking sides; i don't have to \"diss\" one person to applaud another. tackat and everaldo are both good artists.\n\n> The larger the display is\n\nto get nerdy here ;) it's really the higher the resolution the fuzzier they'd be. and at a fairly decent resolution here they look just fine. but hey, being a canuck maybe i'm just not capable of being nerdy enough to notice these fine points ;)"
    author: "Aaron Seigo"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "Arrgh, good looking mac icons, shodder. \n\nHope the good old KDE2 icons will still be provided.\n\nI wonder why no one aims for the enlightment look"
    author: "max"
  - subject: "Re: Kopete Icons"
    date: 2007-06-13
    body: "> Hope the good old KDE2 icons will still be provided.\n\nlook in kdeartwork/IconThemes.\n\n> I wonder why no one aims for the enlightment look\n\nprobably because so few people actually like it.\n\np.s. thanks for joining the positivity brigade."
    author: "Aaron Seigo"
  - subject: "Re: Kopete Icons"
    date: 2007-06-13
    body: "Hey! I didn't expect that kind of trolling from you!\nEnlightenment Desktop Release 17 is amazing (in a lot of aspects better than KDE). People dosen't know it, that's a different history.\n\n(I love e17 mixed with KDE apps)"
    author: "Luis"
  - subject: "Re: Kopete Icons"
    date: 2007-06-13
    body: "He's not trolling. He wasn't referring to e17, but to the general, overall negativity of certain posts. In the past weeks there have been way too many."
    author: "Luca Beltrame"
  - subject: "Re: Kopete Icons"
    date: 2007-06-14
    body: "i didn't say that e17 was bad, just that the reason is probably that most people don't like its style. being hard to procure doesn't help, either. who was it that said that real products ship?\n\nif you like it, that's GREAT. i like a lot of things that most people don't like. i've gotten comfortable with that fact.\n\nwhat i'm trying to get across to some of the people in our community is that you really ought to try openning your mind to a vision that is bigger than just you. why? because when we have a wider view we include more people. that includes you.\n\nthe downside is that when we do that we also tend to find solutions that work for most people by default while providing for the minority through options and customization.\n\nyou talk about e17. what about those who don't or can't appreciate its stylings? yes, they are SOL. for the e team, that's no big deal. they have a focus point that is pretty constrained and are comfortable with that.\n\nbut kde isn't e. the goal many of us have is to allow for things like e (as well as in the other direction), but provide defaults that work for more people. this is an amazing engineering challenge, but one that we are getting closer to achieving year over year.\n\nsadly, it means we take flack from time to time from those who concentrate on their own needs. be they people who love e or people who think the world should be painted in simplified brown.\n\npersonally, i'm after world domination. e and simpleBrown are just customizations of that. ;)"
    author: "Aaron Seigo"
  - subject: "Re: Kopete Icons"
    date: 2007-06-15
    body: "You're answering me something I didn't say. \n\nYou said this:\n\n\"probably because so few people actually like it\"\n\nThat's not true, people dosen't know it, but you can't say they don't like it, if I am wrong and you made an experiment with people to see what they think about e, ok, then you can speak with facts, otherwise, it's trolling as far as I can see.\n\nBy the way, I didn't try to tell KDE to follow enlightenment look, that will be terrible. KDE it's KDE and shouldn't follow anyone else, it have his own style, so as e17, gnome, fluxbox (etc.) have their own. \n\nIn fact KDE 4 is one of the most exciting projects out there, I'll hate it if it just was some new e17, or GNOME. \n\nI apologize in advance for my bad enligsh."
    author: "Luis"
  - subject: "Re: Kopete Icons"
    date: 2007-06-15
    body: "Have we developed a plan to rename the icons in KDEArtWork-4?\n\nYes, I will volunteer to work on it."
    author: "James Richard Tyrer"
  - subject: "Re: Kopete Icons"
    date: 2007-06-11
    body: "Useless reply : I just love the new iconset :)"
    author: "Heller"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "> They are not icons. They are Mac wanna be\n\nYou know most toolbar icons in the mac don't have colors??\n\nOnly the application icons have realistic colors."
    author: "Diederik van der Boor"
  - subject: "Re: Kopete Icons"
    date: 2007-06-11
    body: "I agree.\n\nFor instance, at 16x16:\n\nA) up and down arrows for webcamrecieve and webcamsend are unrecognizable;\nB) the mic in the headset for voicecall blends in with the shadow;\nC) the tag for available looks more like an old mouse.\n\nI'd rather there not be tag/door knob hanger/whatever in the available icon.\n\nThe set status message icon seems pretty clear, though."
    author: "Soap"
  - subject: "Re: Kopete Icons"
    date: 2007-06-12
    body: "agreed as well.  So many of these look terrible at 16x16.  22x22 isn't much better."
    author: "MamiyaOtaru"
  - subject: "Re: Kopete Icons"
    date: 2007-06-11
    body: "I partially agree for the Kopete available icon. It's not very easy to recognize. And the \"lock\" or whatever it is doesn't symbolize \"available\" to me. A light bulb or just a green check mark or sth. like that would be better IMO. But beyond that: I've been very critical of Oxygen in its infancy and didn't like most of the icons. After many revisions of these icons that has changed a lot. You can see that lots of work has gone into these icons and especially on higher resolution screens which are more and more common these days they're just superb barring some minor exceptions like this one. "
    author: "Michael"
  - subject: "Re: Kopete Icons"
    date: 2007-06-11
    body: "Yep same here!\nI forgot to say that I really like most of new revisions on Oxygen. \n\nThey just have to think a little less like Apple in some cases, and use something more representative.\nI would LOVE a operating system with a style like google talk: clean, easy and very intuitive."
    author: "Iuri Fiedoruk"
  - subject: "so what about a rating system?"
    date: 2007-06-12
    body: "I think each icon should have an option to rate any individual revision of it, so the most popular ones could get into an 'extended' version of Oxygen packing in alternative icon subsets.. like 'I want to have all my folder icons have *that* kind of folder, but all my media player buttons like *that*... I think this would be the one solution to please them alll.. and no as sorry I am I cannot code it, busy with soc---- anyways regards"
    author: "eMPee"
  - subject: "Re: so what about a rating system?"
    date: 2007-06-12
    body: "Yes, some kind of democracy for icons. \nAt least some kind of voting for icons HIG would be VERY welcome. :)\n\nThere are some icons in Oxygen that are really good, but a few just dosen't follow the quality or just don't transmit the idea they should."
    author: "Iuri Fiedoruk"
  - subject: "umbrello & kdevelop integration"
    date: 2007-06-11
    body: "Can we expect to see an integration with Umbrello and KDevelop?  What would be nice is starting a KDevelop project, where an Umbrello view opens first.  You then design your model there.  When you build your project, KDevelop causes Umbrello to do a code generation.   \n\nEven nicer, is if in KDevelop, you edit the generated source files, Umbrello \"sees\" the changes, and incorporates them into the model.\n"
    author: "Rick Wagner"
  - subject: "Re: umbrello & kdevelop integration"
    date: 2007-06-12
    body: "Usually, when I open a project my idea is not to design it, I already have my idea and I want to put it in code.\n\nThe second idea is nice, maybe could be a kdevelop plugin of software (they have import from C++ I think, shouldn't be that difficult)."
    author: "apol"
  - subject: "Re: umbrello & kdevelop integration"
    date: 2007-06-12
    body: "Integration? Better good cooperation. That is no integration on the userinterface level. Netscape comes to my mind. that was in the days on the integration fashion. You get overcomplex applications which confuse people and are buggy."
    author: "Andre"
  - subject: "speaking of Kopete... and dataengine"
    date: 2007-06-12
    body: "Does someone think it will be possible to provide the IM informations (state, avatars...) through a dataengine in plasma (in order to make some cool plasmoid to interact with my contacts (as a picture of my girlfriend in a buble on my desktop which become grey or colored according when she is connected, etc...)\n\nwell, maybe not, I'm not sure to understand what a dataengine can do :p"
    author: "DanaKil"
  - subject: "Re: speaking of Kopete... and dataengine"
    date: 2007-06-12
    body: "This is a nice ideia for a data engine, no aseigo?\nmaybe for a 4.1 version?"
    author: "cOWARD2"
  - subject: "Re: speaking of Kopete... and dataengine"
    date: 2007-06-12
    body: "Why not? Maybe something with Decibel? </guess>"
    author: "Anonymous"
  - subject: "Re: speaking of Kopete... and dataengine"
    date: 2007-06-13
    body: "I think there is technically no problem of implementing something like that. However, someone has to write the DataEngine and the Plasmoid."
    author: "Anonymous"
  - subject: "Konqueror vs Dolphin"
    date: 2007-06-12
    body: "To me, one obvious future path for Konqueror is to start a new browser-only project, from scratch and based on Webkit, with a similar philosphy to Dolphin but strictly orientated towards rendering web pages, and then create a new Konqueror4 project that simply uses this Webkit project and Dolphin togerther as kparts (or whatever the KDE4 equiv is) as the new NG Konqueror. The current Konqueror code remains in the 3.5 branch. The new Konqueror looks and behaves like the current one but is really only a simple wrapper around the Webkit engine and Dolphin. No need to \"pollute\" KDE4 with ancient Konqueror code and half the new Konqueror is already done. We'd also end up with a Dolphinesque trimmed down dedicated web browser. KDE4 Konqueror users would be happy, webkit browser-only users would be happy, Dolphin-only advocates would be happy.\n\nNo, even if other people agreed with this approach, I have no skills to implement something like this. I do wish I could though."
    author: "markc"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-12
    body: "i think this idea was around for some time, but currently there are no developers left do start yet another project. everyone is busy getting kde4.0 ready.\n\nthough i think a dedicated kde-based webbrowser with more plugin/extension support would rock. maybe for kde 4.2 =)."
    author: "ac"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-12
    body: "That's what KHTML already is.  I assume the Unity project (which is porting changes in from WebKit) would produce a KPart for Konqueror and whoever else needs it."
    author: "anonymous"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-12
    body: "khtml is no webbrowser. you can't use it without konqueror as an application."
    author: "ac"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-12
    body: "It's available as a KPart, though.  You can build a functional web-browser around it with the bare minimum of effort."
    author: "Anon"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-12
    body: "developing a _good_ webbrowser is no bare minimum effort. even when getting the content and displaying it is allready done for you..."
    author: "ac"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-12
    body: "Why should it be a great deal? Just create an application with the absolut minimal necessary of features, add a extension-interface (via kross) and let the community create the rest, and let the user decide what bookmark-system they want, if there need a wallet, and so on... In the end, 2-3 years later, the kde-project can bundle the most useful extensions and add them to the default-installation.\n\nOr something like that..."
    author: "Chaoswind"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-13
    body: "yeah! that will get us a completely unusable mess! why didn't i think of that before?!\n\nusability is very important for a webbrowser. you can't just \"attach\" every feature somehow to the app. currently kde doesn't even have a real tdi-widget. a tabbed webbrowser without drag'n'drop hardly qualifies for the attribute good..."
    author: "ac"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-13
    body: "Gossip. Firefox proofs that it's possible."
    author: "Chaoswind"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-14
    body: "nice try ;).\n\never looked at the firefox code? if you call that \"bare minimum effort\", than i finally understand Anons post. though someone of us should look that up in a dictionary :P.\n\n\nalso, firefox doesn't bundle any extensions (well, the javascript debugger is one, but...). they simple reimplement cool features of extensions."
    author: "ac"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-14
    body: "You miss the important points. What Firefox has in there code, is unimportant. Firefox' extensions are mostly working, despite the unholy plattform. So, why should an clean environment, which brings the most features for free, be inferior ? And at least, there is already an KDE-Browser, which must only charged up, and lose some functions."
    author: "Chaoswind"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-12
    body: "ac is correct.  KHTML is a great renderer, and obviously good for a real browser, but a renderer/page retriever is pretty much all it is.  It can almost certainly render forms, but until you add code to make KHTML do it, there are no forms.  There's no history, no bookmarks, no drag and drop, no loading images or movies using other KParts, etc.  For real utility in apps, we could use a much higherlevel KonqiPart, not just KHTML."
    author: "Lee"
  - subject: "Re: Konqueror vs Dolphin"
    date: 2007-06-14
    body: "History = Strigi + cached page URLs, bookmarks = technorati et al, drag n drop is only needed for the Dolphin kpart, image and form rendering needs adding, and launching other kparts when needed shouldn't that hard using plasmoid concepts. Carving out a KonqiPart, from what's available now and for where a \"WebUI\" wrapper concept could lead, shouldn't be all that hard in general. The sort of KonquerorNG functionality I would like to see is something like a RMB menu edit option on any image in a web page and it's instantly inside a Krita kpart (if such a thing exists, just an example) ready to edit and auto upload the changed image (if appropriate) etc. This WebUI interface could perhaps wrap around and manage *anything* that one would want to do on the net and, to me, rethinking Konqueror along these lines could eventually lead to much more powerful and usable interface than the kurrent Konqueror+Quanta.\n\nThe basic point is that a potential WebUI wrapper interface could make use of almost otherwise standalone parts rather than a single separate codebase... and, because there is no obvious Konqueror4 devel then it might be possible to end up with a WebUI wrapper instead, which could end up a lot more powerful than the current single codebase Konqueror ever could be... if the right framework is put in place right about now. "
    author: "markc"
  - subject: "why digiKam is in kdegraphic ?"
    date: 2007-06-12
    body: "If i set \"Statistics in Each Section\" = 20 in \"Number Of Entities Displayed\" from Options panel, i can see:\n\nProgram               Buzz\ndigiKam (kdegraphics) 1443\n\ndigiKam is not in kdegraphics, but in extragear...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: why digiKam is in kdegraphic ?"
    date: 2007-06-12
    body: "I'll fix it ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Cammanlinescreenshots"
    date: 2007-06-13
    body: "Is there any commandline tool to find out what windows are up and running and where? "
    author: "max"
  - subject: "save KPF"
    date: 2007-06-13
    body: "It's quite sad to see kpf go away. It was a nice way to quickly make files available to friends. (Especially since I've often hat problems with kopete's filesharing abilities.)"
    author: "Anonymous"
  - subject: "Re: save KPF"
    date: 2007-06-13
    body: "Unmaintained and abandoned, there was no other choice possible.\nEspecially that is was a panel applet, thus it would had required some major effort in porting to the plasma API, not just some occasional porting."
    author: "Pino Toscano"
  - subject: "Sugestions"
    date: 2007-06-13
    body: "I must suggest organizing by file types in KListView. So in the words used in video, we should have categories by file type as mp3, avi, etc... we could even expand this and also have by multimedia, office types, simple types... that would be great and very usable!\n\nAs to kopete, I really hope they could make a switch to enable/disable video capture as I don't use it right now because when I'm watching TV, kopete messes my signal!"
    author: "jcp"
  - subject: "Re: Sugestions"
    date: 2007-06-13
    body: "http://dot.kde.org/1181568739/1181571377/1181580671/1181591195/\n\n:)"
    author: "Lans"
  - subject: "some thoughts for KDE 4"
    date: 2007-06-14
    body: "I really do hope that efforts are being put into the window manager. I really want tabbed window managing and other features in ion3 for Kwin. \n\nAlso notice how Vista has sorting by selection on it's column header in a detail file list view. For example you can mask using the the file type header. Select a few file types and the rest of the file formats are masked out of the view. That sorta stuff is the quirks I really want in Dolphin.\n\nKDE 3 is already there; I'm not nearly as interested in the new technology (although I am; particularly Solid and Phonon) as I am in the intricate details and polishing of KDE 4. \n\nThat reminds me of a question; will Phonon have intelligence to fade out amarok or any music program when say a system notification is played (or vice versa)? Say if I switch between VOIP and the music player will their relative volumes adjust accordingly."
    author: "DaveD"
---
In <a href="http://commit-digest.org/issues/2007-06-10/">this week's KDE Commit-Digest</a>: <a href="http://uml.sourceforge.net/">Umbrello</a> gets a code generator for the <a href="http://en.wikipedia.org/wiki/D_programming_language">D programming language</a>. Further work in <a href="http://plasma.kde.org/">Plasma</a>. Initial work to allow the <a href="http://enzosworld.gmxhome.de/">Dolphin</a> file view component to be embedded into <a href="http://www.konqueror.org/">Konqueror</a>. More work in the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=5D66C1579098460C">KOrganizer Calendar</a> and <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9064143E62AF5BA6">KRDC</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> projects, with the start of the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=1EF6392A4C8AEADD">Icon Cache</a>, <a href="http://code.google.com/soc/kde/appinfo.html?csaid=2FD0CEF20B077C6C">TextTool Plugins in KOffice</a> and <a href="http://code.google.com/soc/kde/appinfo.html?csaid=F3389CEA023E0E9D">Kopete Messenger update</a> projects. Start of a <a href="http://solid.kde.org/">Solid</a> interface in <a href="http://amarok.kde.org/">Amarok</a>, with breakthroughs in support for the Jamendo music service. <a href="http://www.kdevelop.org/">KDevelop</a> begins to be ported to the KDevPlatform structure. A return to progress on the Cokoon SVG window decoration engine, with the addition of PyQt4 bindings. A streamlined command-line screenshot utility, kbackgroundsnaphot, is created. The <a href="http://gwenview.sourceforge.net/">Gwenview</a> image viewer moves to the kdegraphics module, whilst KPF, KWifiManager and KDict are removed from the kdenetwork module for KDE 4.
<!--break-->
