---
title: "KDE Commit-Digest for 5th August 2007"
date:    2007-08-06
authors:
  - "dallen"
slug:    kde-commit-digest-5th-august-2007
comments:
  - subject: "Phonon Out"
    date: 2007-08-06
    body: "Seems like the predictions saying KDE 4.0 will be more like a port to Qt4, with new things coming on 4.1 or 3.2 where mostly right :)\n\nPS: I always said Plasma wasn't going to be on 4.0, now looks I'm wrong but other parts will be missing. I know saying that it irritates some people, more than once I was called troll, but it's better in my opinion to be realistic and don't create false expectations.\n\nKDE 4.0 won't be *that* great and will miss a solid Plasma, Phonon and other things, but remember always 2.0 and 3.0, it weren't anything *that* good, but it leaded to a series of great successes.\n\nJust porting to Qt4 and adding some minor features takes a lot of time, so be patient, we'll get there."
    author: "Iuri Fiedoruk"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "Phonon isn't out, one of the back-ends are out, namely NMM. There is still Xine afaik. And as KDE 4 matures there will probably be a lot more :)"
    author: "AC"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "but nmm ist the coolest backend !! :'-("
    author: "srettttt"
  - subject: "Re: Phonon Out"
    date: 2007-08-07
    body: "when was the last stable release of nmm and which distros ship it?"
    author: "Aaron J. Seigo"
  - subject: "Re: Phonon Out"
    date: 2007-08-08
    body: "That some unfortunate and disturbing question you have to ask, but still NMM is the coolest alternative comparing the current backends. \n\nThe other backends don't really bring anything new, while both the technology and possibilities NMM brings are much more exiting. The vision of network-integrated multimedia are quite compelling."
    author: "Morty"
  - subject: "Re: Phonon Out"
    date: 2007-08-08
    body: "While it may be great technology; you can hardly expect KDE to depend on unreleased versions of software.  Think of the riots that would cause! :)\n\nIt may be useful if you can ask the nmm makers if there is something to release after 2 years since their latest release."
    author: "Thomas Zander"
  - subject: "Re: Phonon Out"
    date: 2007-08-08
    body: "I was not expecting KDE to depend on it, even if I find the ideas behind NMM cool :-) \n\nAnd thanks to Phonon, it's even possible to make NMM the recommended backend in the future when/if the NMM people gets their act together:-) "
    author: "Morty"
  - subject: "Re: Phonon Out"
    date: 2007-08-14
    body: "It seems there is.  They just anounced the release of the 1.0.0 version: \nhttp://lists.kde.org/?l=kde-multimedia&m=118708663022124&w=2\n\nDoesn't magically make the phonon backend complete though. \n\n"
    author: "cm"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "Except you are still wrong.\n\nPhonon-xine and phonon-gst still exist.  nmm was just one of the phonon backends.  Plasma is also there.  Solid is also there.  Oxygen is also there.  Odd how that works."
    author: "Dan"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "> Seems like the predictions saying KDE 4.0 will be more like a port to Qt4,\n> with new things coming on 4.1 or 3.2 where mostly right\n\nthen you evidently haven't used the beta much. marble, okular, krdc, the games, dolphin, system settings ..... all apps that are pretty well if not completely new in kde. kwin has seen tremendous work .. i could go on. the icon/pixmap cache, seeking in kio, solid and so many more additions to the frameworks which apps are already taking advantage of are also there right now.\n\nso.. yeah. it's a lot more than a port to qt4.\n\n> will miss a solid Plasma, \n\nwhat you'll miss in plasma in 4.0 are the larger number of extensions that inevitably will only follow post-4.0 and the more advanced things like the networking layer which haven't been on the 4.0 target zone since i started planning what would and wouldn't be in 4.0.\n\nwill plasma be better in 4.1 and 4.2? well .. yeah. we're going to continue to develop it and do more cool and crazy things, but what is there will be solid and it will take care of desktop needs.\n\n> Phonon \n\nuhm.. what? mkretz and a handful of TT engineers sat around for the last 1+ week and did a thorough going over of the phonon API (again!) which will be merged into trunk this coming week. it's more solid and ready for production use than it has ever been.\n\nthe nmm backend was moved to playground because the *nmm* backend isn't near ready. the xine backend is quite there and the gst one, while needing some work, is also there.\n\na feature that has been postponed to 4.1 is video effects. that is due to reworking how the pipeline for those works in 4.0 so that we can actually do them properly in 4.1 (which means having to rework the effects stuff, which won't be there in time for 4.0). in other words, it's been made a *more* solid framework at the expense of having video effect plugins for 4.0 (which wouldn't have worked out all that great with the old api anyways =).\n\nso i think you perhaps misread or misunderstood something there.\n\nnow.. all THAT said:\n\n> remember always 2.0 and 3.0, it weren't anything *that* good,\n> but it leaded to a series of great successes\n\nthis we agree on. 4.0 will be a lot like 2.0; much more than it will be like 3.0, actually. the number of new apps (inc the new file manager and desktop workspace, stuff that didn't get revamped as radically since 2.0), the immense number of important new frameworks and the likeliehood of 4.0 not being as stable as the later 3.x releases (for obvious reasons, e.g. the maturity that comes from being banged on by users and developers for years) are all ways it is similar.\n\nand yes, 4.1 will have a lot of stuff over 4.0. ditto for 4.2. kde4 is going to have a long succession of great releases, each better than the last. but while you're concerned about setting 4.0 expectation too high, i'm concerned that you're shortchanging us all here.\n\ni'm looking forward to 4.1 as much as they next guy, but 4.0 is going to be a damn sight better than you seem to consider."
    author: "Aaron J. Seigo"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "> and yes, 4.1 will have a lot of stuff over 4.0. ditto for 4.2. kde4 is going to have a long succession of great releases, each better than the last. but while you're concerned about setting 4.0 expectation too high, i'm concerned that you're shortchanging us all here.\n\nLately everyone just takes everything I say here on the bad side...\nI just wanted to express that I tough  some people (me including) wanted *everthing* done in KDE 4.0 and that's not the reality.\nYes, I war wrong about Phonon, thanks my poor english and eyes hurting today for that, sorry.\n\nI will not post here anymore, just read the news and posts. \nSorry everybody for being a problem for you all and posting foolishes, bye."
    author: "Iuri Fiedoruk"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "\"I will not post here anymore, just read the news and posts.  Sorry everybody for being a problem for you all and posting foolishes, bye.\"\n\nDon't stop posting altogether, dude - just take the extra time needed to verify that whatever criticisms you make are accurate, and haven't been done to death already ;) Fresh criticisms are always welcome e.g. see http://dot.kde.org/1185753836/1185808183/1185884790/, and the (only one, admittedly!) reaction to it :)"
    author: "SSJ"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "> seeking in kio\n\nHmm... How many KIO slaves actually support seeking?"
    author: "AC"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "Good question. If its just HTTP and kio_file, I'll be happy."
    author: "Ian Monroe"
  - subject: "Re: Phonon Out"
    date: 2007-08-07
    body: "Well, it's biggest advantage was supposed to be that the network KIOslaves wouldn't have to download a whole movie to watch it, so I suppose they do support seeking. But maybe just nobody got around implementing it..."
    author: "jospoortvliet"
  - subject: "Seeking in KIO Slaves"
    date: 2007-08-07
    body: "Some HTTP servers don't allow seeking, if I remember correctly, but most do. I think it would be useful for desktop users to have seeking support for SFTP and SMB KIO slaves so as to make network access more transparent."
    author: "AC"
  - subject: "Re: Seeking in KIO Slaves"
    date: 2007-08-08
    body: "In addition to file, sftp and smb are the kio slaves that do support seeking at the moment."
    author: "cloose"
  - subject: "Re: Seeking in KIO Slaves"
    date: 2007-08-08
    body: "Excellent! Thanks for the info."
    author: "AC"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "I remember someone once saying the cd-ripping IO slave (can't remember what it is called) wasn't as good as it could have been due to KIO's lack of seeking support, so no doubt it is useful."
    author: "Tim"
  - subject: "Re: Phonon Out"
    date: 2007-08-07
    body: "Oh Aaron, you and your \"facts\". Wild speculation is much more fun for these armchair quarterbacks who don't code and only troll. Don't ruin their fun by actually informing them of the truth. And how would you know anyway? Oh, you coded those things? Oh, hmmmm."
    author: "Joe"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "So things like the whole kde-games now using SVG rather than pixmaps. The whole slew of new features in okular compared to KPdf. Advanced composite support in KWin. The optimizing and rework done to KSysGuard. Inclusion and use of new frameworks like Solid and Phonon, change from KSpell to Sonnet. New applications like Dolphin and Marble. And lots of improvements and added features to many applications compared to their 3.5 versions. Are they just ports?\n\nAnd if you bother to look on the statistics you'll see numbers indicating more than 5k lines modified and 1.5k new files, every week for months.  I'd say that sums up to more than a port to Qt4."
    author: "Morty"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "You definitely have no idea about the entire development of KDE 4.0! And no, Phonon is not out, just the nmm backend. But the Xine backend is in place and working. Don't complain about things you don't understand.\n\nThe only things which will be not ready until KDE 4.1 are Decibel and maybe Sonnet - the last point is still not decided yet (afaik) and the first one was clear quite early.\nThe other technologies and especially the applications are in place already.  And that the development will go on after KDE 4.0 is typical, everything else would be quite strange.\n\nSo why do you state things you have no idea about? This is exactly what trolls do."
    author: "liquidat"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "Akonadi won't be ready either. And didn't Decibel already support KPhone? I didn't pick up that it won't be ready..."
    author: "jospoortvliet"
  - subject: "Re: Phonon Out"
    date: 2007-08-06
    body: "AFAIK Kcall was the first application which used Decibel and Kopete will use Decibel in 4.1\n"
    author: "Emil Sedgh"
  - subject: "Re: Phonon Out"
    date: 2007-08-07
    body: "kcall was ported to Decibel, but Decibel itself won't be ready afaik, let alone kopete.\n\nAnyway, Akonadi won't make it? That's new to me, do you have any link or something? I thought I will see kdepim released with KDE 4.0?"
    author: "liquidat"
  - subject: "Re: Phonon Out"
    date: 2007-08-07
    body: "There was some discussion a while back on the kde-pim mailinglist about Akonadi being targeted at the 4.1 release. And releasing a kdepim based on the current backend for 4.0. Did a quick search, but didn't see any decision or a current status on the issue. \n\nI would think they would do much the same for Kopete, they will release. And do the Decibel later, when it's ready. At least that sounds like a reasonable way to do it."
    author: "Morty"
  - subject: "Lancelot?"
    date: 2007-08-06
    body: "Any link for the concept behind Lancelot? What's so \"next generation\" at this application launcher?"
    author: "Chaoswind"
  - subject: "Re: Lancelot?"
    date: 2007-08-06
    body: "Hmm, the name not starting with a K isn't next gen enough??"
    author: "Thomas Zander"
  - subject: "Re: Lancelot?"
    date: 2007-08-06
    body: "Not for my curiosity concerning rivaling solutions ;)"
    author: "Chaoswind"
  - subject: "Re: Lancelot?"
    date: 2007-08-06
    body: "It wasn't supposed to go to the news... ever :) \n\nLancelot was supposed to be a testbed for no-click interface application launching, not (as everybody would think from this digest) a kmenu replacement..."
    author: "Ivan &#268;uki&#263;"
  - subject: "Re: Lancelot?"
    date: 2007-08-06
    body: "like katapult !!!"
    author: "djouallah mimoune"
  - subject: "Re: Lancelot?"
    date: 2007-08-06
    body: "Well, then, without the keyboard too :)"
    author: "Ivan Cukic"
  - subject: "Re: Lancelot?"
    date: 2007-08-06
    body: "You're working on KBFX/Raptor?"
    author: "Ivan Cukic"
  - subject: "Re: Lancelot?"
    date: 2007-08-06
    body: "No. I have a personal project. Actualy it's a PyQt-Application. I will port it to plasma (with some new features) in some weeks. It's nothing like an application-menu, more like apples taskdock, with some tabbar-magic, launcher-features and more efficient."
    author: "Chaoswind"
  - subject: "Re: Lancelot?"
    date: 2007-08-07
    body: "Looking forward to it ;-)"
    author: "jospoortvliet"
  - subject: "Re: Lancelot?"
    date: 2007-08-07
    body: "Me too!"
    author: "Ivan Cukic"
  - subject: "Re: Lancelot?"
    date: 2007-08-07
    body: ":( Yet another missing K\n\nI really liked the ability to quickly know an app intergrates with KDE> "
    author: "Ben"
  - subject: "Re: Lancelot?"
    date: 2007-08-07
    body: "+1\n\nIn the long list of new frameworks and apps I can remember by now (Plasma, Phonon, Decibel, Sonnet, Strigi, Dolphin, ...), only Akonadi, Nepomuk, and Okular have a \"K\"."
    author: "Stefan"
  - subject: "Re: Lancelot?"
    date: 2007-08-08
    body: "It's a PR thing to get companies to choose KDE ratehr than anything else. \n\nI miss the K, too, but there were people who thought it was becoming silly, and they seem to have had a majority (at least in voices). \n\nAs far as I remember, there was a simple guideline: \n\n* If your app is a smaller library, use the k to show that it is mostly part of KDE and a seperate lib after that. \n\n* If your app is strongly user-visible, don't use a K or don't put much focus on it (like amaroK went to Amarok). \n\n* If your app is a main system component, don't use a K. \n\n\nI would more vote for using ingenious K, like in Amarok, Akonadi, Nepomuk, and Okular. The K makes an app easily searcheable on search-engines. \n\nFor Plasma, Phonon and Solid it sounds OK, because they are the big backends. \n\nFor strigi, it's also OK, because it need not be KDE-only (no need to keep Gnome-devs from directly using it, just because tehre's a prominent K in the App), but if something depends on KDElibs, I think that it is very nice to have a k in it. \n\n\nIt gives the nice and warm feeling of home. "
    author: "Arne Babenhauserheide"
  - subject: "okular vs kdpf"
    date: 2007-08-06
    body: "Any news on okular being able to display either pdf or ps files before 4.0?  If not I assume kpdf and kghostview will still be supported?  Loving all your work guys and looking forward to contributing in the future (especially using the python bindings)"
    author: "betang"
  - subject: "Re: okular vs kdpf"
    date: 2007-08-06
    body: "huh, for me, PDF support doesn't compile, but it's there in the code. I guess some incompatibility or bug. But Okular sure begun life as a PDF viewer, and I think it's highly unlikely (eg: NO WAY) that it won't be able to view them...\n\nActually, one of the commits talks about supporting PS and PDF in compressed (tar.gz, bz2) form. Ow, and this support being tested. So it'll work, don't worry."
    author: "jospoortvliet"
  - subject: "Re: okular vs kdpf"
    date: 2007-08-06
    body: "> Any news on okular being able to display either pdf or ps files before 4.0?\n\nOkular has been supporting PDF for a very long time now, and that support will be included in KDE 4.0. Okular will be a significantly better PDF viewer than KPDF (interface and performance improvements, better text selection, lots of other stuff). "
    author: "Eike Hein"
  - subject: "Re: okular vs kpdf"
    date: 2007-08-06
    body: "Awesome. :) kpdf is probably my favorite \"core\" KDE app already."
    author: "Ian Monroe"
  - subject: "Re: okular vs kpdf"
    date: 2007-08-06
    body: "I've been using okular as default doc viewer (pdf, djvu...) for some time now. The only format I do not use it with is chm."
    author: "Ivan Cukic"
  - subject: "Re: okular vs kpdf"
    date: 2007-08-07
    body: "I think my problem is too many alpha/betas then.  I try to use okular (beta) on opensuse 10.3 alpha7 and it doesn't show pdfs at all.  Guess it would be a bit rich to complain given those versions :)   I'm happy to wait as 3.5.7 is rock solid."
    author: "betang"
  - subject: "Re: okular vs kpdf"
    date: 2007-08-07
    body: "> I think my problem is too many alpha/betas then. I try to use okular (beta) on opensuse 10.3 alpha7 and it doesn't show pdfs at all.\n\nI think your problem is not the alpha/beta/rc/whatever, it's just a distro issue, see http://www.kdedevelopers.org/node/2918"
    author: "Pino Toscano"
  - subject: "been on kde4 svn for few days now, and counting..."
    date: 2007-08-06
    body: "Did plunge into the kde4 svn via Gentoo's overkay (kde with 9999.4 ebuilds) and the thing that was very surprising to me was that when I made my initial merge in the 2nd of Aug last week, the amount of stuff that did not work started to work after doing daily svn rebuilds within some odd hours after I even noticed them... Amazing how the builds morph constantly at lighting speed, I can almost use the desktop already (heh, stuff like the legacy kicker eats 100% of cycles on one core while don't even start the apps from the menu, so I really hope that will be dropped and replaced with that Plasma version soon) and the only option is to start apps by \"run command\" via 2nd mouse button.\n\nAnd of course Konqueror is somewhat useless, not only it stucks after the initial page load, but need to disable javascripting so it even renders the pages, but hey, that's life at \"Alpha3\" stage. :-)\n\nLuckily there are apps that can take care of all the stuff I need using \"the other toolkit\".\n\nI just wish either the overlay adds kdesupport/kdeplayground/kdeextragear to it's ebuilds, and/or stuff start to crawl to the kde's main packages soon. ;-)\n\nAnyhow, the base structure in Qt4/kdelibs really do work like a dream (and stuff like konsole is way better than it's kde3 version).\n\nPs. and I hope kdenetwork starts to compile soon, haven't been able to compile it once, always stucks at \"[ 43%] Building CXX object filesharing/advanced/kcm_sambaconf/CMakeFiles/kcm_kcmsambaconf.dir/joindomaindlg.moc.o\".\n\nPps. oh yeah, and for some reason the kdegraphics doesn't find my exiv2-0.15 installation (installed as normal on /usr/include and /usr/lib64 dirs) so can't yet play with the cool gwenview that I had on my kde3 setup earlier.\n\nPpps. I'll start bugging the bigs.kde.org soon, just have to play around for a while first. :-)\n\nPppps. and sorry for the rant, just can't irc yet. ;-)"
    author: "anon"
  - subject: "Re: been on kde4 svn for few days now, and counting..."
    date: 2007-08-06
    body: "I broke KIO for a while. KDE 4.0 beta 1 cannot download anything properly. It corrupts the downloads.\n\nIt's been fixed already. Just upgrade again."
    author: "Thiago Macieira"
  - subject: "Re: been on kde4 svn for few days now, and countin"
    date: 2007-08-07
    body: "Actually after looking into why the kdegraphics doesn't locate my Exiv2 installation noticed that the top kdegraphics/CMakeLists.txt does have the check in place, but for some reason there is no actual check for it in kdegraphics/cmake/modules/, maybe there should be something like FindExiv2.cmake or something? Or is it intentionally left out to disable gwenview compilations (not ready yet)?\n\nOther thing I noticed that is that the http://techbase.kde.org/Schedules/KDE4/4.0_Module_Status is way too lagging, maybe someone could check that out? And possibly adding ETAs for the stuff coming from playground/extragear to base KDE soon, like the Raptor (and kicker removal of course) and Plasmoids?"
    author: "anon"
  - subject: "Re: been on kde4 svn for few days now, and countin"
    date: 2007-08-07
    body: "For example kdelibs has perfectly working kdelibs/cmake/modules/FindExiv2.cmake, so it has to be either a intentionally deleted from kdegraphics, or is just forgotten from moving gwenview to kdegraphics?"
    author: "anon"
  - subject: "Domino"
    date: 2007-08-06
    body: "Is it just me, or is the new oxygen style looking a lot like a the Domino KDE3 style (especially the one supplied with KDEMOD in Arch Linux)?\nMaybe it's just me, and me being so used to the Kubuntu default look, that everything different looks the same.\nLook at the screenshots on http://kdemod.ath.cx/ to see what I mean."
    author: "Martin"
  - subject: "Re: Domino"
    date: 2007-08-06
    body: "Hmmm, domino can look vaguely alike, but I think Oxygen is pretty original..."
    author: "jospoortvliet"
  - subject: "Re: Domino"
    date: 2007-08-06
    body: "Now it must be said that Domino is very tweakable (just look at the screenshots on kde-look.org) and can look like almost anything (custom gradiants is a stroke of pure genius, imho). And the Domino theme KDEMOD ships with is very pleasant to use, so I surely wouldn't mind if the Oxygen style borrows some ideas from them (you shouldn't sacrifice prettiness and usability for originality, imho)."
    author: "Martin"
  - subject: "Re: Domino"
    date: 2007-08-07
    body: "No it's just you. I checked the screenshots on that so-called KDEMOD site and I actually use Domino. You need to check with a medical professional about your eyesight."
    author: "backtick"
  - subject: "KRDC and NX?"
    date: 2007-08-06
    body: "Quote: \"Planned features include (for example) NX support\".\nWow! It will be very nice!"
    author: "Dmitry"
  - subject: "Re: KRDC and NX?"
    date: 2007-08-06
    body: "I can only agree on this and would really love to see this feature (nice looking to Urs ;-)."
    author: "Mario Fux"
  - subject: "Re: KRDC and NX?"
    date: 2007-08-07
    body: "Can't you open an NX connection via the shell?\n\nEither way KRDC having this is good, its more intuitive than the shell version. "
    author: "Ben"
  - subject: "KViewShell"
    date: 2007-08-06
    body: "Some time ago tere was still some tension between KPDF (no Okular) and KViewShell.  So what happened to the latter?"
    author: "someone"
  - subject: "Re: KViewShell"
    date: 2007-08-06
    body: "It's called ligature nowdays."
    author: "Cyrille Berger"
  - subject: "Re: KViewShell"
    date: 2007-08-08
    body: "It moved to extragear: http://websvn.kde.org/trunk/extragear/graphics/ligature/"
    author: "cloose"
  - subject: "Amarok is monopolizing all the love !! "
    date: 2007-08-06
    body: "\ni hope i am wrong, but it seems that the video players for Kde are falling behind,  nearly all the commits are either for Amarok or phonon thing, what's about kaffeine, kplayer etc...\ni keep watching for extragear, and playground and no sign of them. too sad\n\n"
    author: "djouallah mimoune"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-06
    body: "It seems to me that codeine is already ported.\nI suppose the other ones will get ported now that kdelibs API and Phonon is stable."
    author: "Richard Van Den Boom"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-06
    body: "Kaffeine4 gets love. It lives in /branches/work/kaffeine4\n\nAdmittedly I haven't been keeping my [pet project|plan for world domination] Video Player up to date with kdelibs API, I should update it sometime. I bet it works a lot better now then it did in May thanks to the work on Phonon."
    author: "Ian Monroe"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-06
    body: "Yes please do, I really like codeine speed to load and simplicity and would like to see it in KDE4. :-)"
    author: "Richard Van Den Boom"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-06
    body: "Actually Ian Monroe started to port Codeine to KDE4 a while ago (and renamed it to Video Player). He's currently busy with Amarok's playlist code though.\n\nMaybe afterwards he's gonna hack on it some more, or maybe I'll help out a bit too, as I love Codeine very much. Or maybe we can motivate Max Howell to hack on it (he's the original author). That would be cool.\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-06
    body: "Codeine is amazing.  Simple, yet just works every time.  It should really be included in the default KDE install to increase awareness."
    author: "Leo S"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "+1"
    author: "Richard Van Den Boom"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "Yes, Codeine is by far the best video player KDE has. It does what it's supposed to do, and it does it smart. Much better than the other players... Kaffeine tries to do way too much (I hate the concept of mediaplayers, word processors should not be combined with cdburn applications, why should videoplayers be merged with audioplayers?). And KMplayer is technically great, works fine for embedded stuff, but is just a bit awkward to use."
    author: "jospoortvliet"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "Actually, there's a significant problem here.  Some \"container formats\" can contain video (without sound), audio, or both.  Telling which it is before you open the file is difficult.\n\nThat said, I agree with your general statement that video players and audio players should be separate.  It'd be nice to see a mime-like smart file identification system (or better, some sort of file attribute tracking from downloads etc.) which knows what a substreams are, and lets you specify the file handler system-wide."
    author: "Lee"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-06
    body: "Codeine is a great application. And in my opinion a similar simple application should also exist for sound. Hope somebody brings Kaboodle back, or write something like it. \n\nThe need for a simple click-and-play soundapplication is real, when you only want to check out a sound file there is not need for playlist and collections. And it should always be default for playing those files, not Amorok, JuK, Noatun or any other playlist touting player. When I click on a sound file I simply want it played, not added to some collection or playlist. Always a drag having to change the preferred action for the mimetypes back to Kaboodle, after installing Amorok and similar."
    author: "Morty"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-06
    body: "Personally I hate to have bunch of application just for one thing.Kaffeine could play your soundfiles when you DoubleClick on it...Why Kaboodle should be included? Amarok Can play It too!\n\nAnd guys, Amarok didnt enter the kdemultimedia and is still in Extragear...\nWhich logic means that one of the most loved KApplications, should not be in kde default applications?\nnow that Applications couldnt enter for 4.0, I really hope and wish to see AmaroK in kdemultimedia for 4.1"
    author: "Emil Sedgh"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "Amarok chooses not to be in kdemultimedia so it can release when it chooses to rather than when the rest of kde is ready to release."
    author: "Dan"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "Yes I know that, but thats really not a good reason...\n"
    author: "Emil Sedgh"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "It is a good reason. The amarok PPL don't want to be tied to the irregular, 8-10 month KDE schedule, but release way more often. What's wrong with that? K3B is in extragear as well as Digikam..."
    author: "jospoortvliet"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: ">Why Kaboodle should be included? Amarok Can play It too!\n\nIt's rather simple, but you seem to miss the point. \n\nNot only are Amorak and other complex players big applications with long startup times, they will also add the file to your playlist etc. Which is most likely not the wanted result when clicking on a random sound file in your filemanger. Adding to your playlist is better done from within the application, by drag and drop or by using right click and a servicemenu option. \n\nKaboodle provides the play once functionality, for the use case for files you are not interested in adding to your collection/playlist. You now for those music files from your little sister you just \"has to hear\", funny soundfiles mailed from friends or interviews of KDE developers. Generally stuff you check out, but don't want messing up your playlists.\n"
    author: "Morty"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "I rather agree with this : I like to have one simple app to play file from the net or from the file manager, and one more complex managing file lists for my collections of music or films. A bit like having a fast image viewer and Digikam to organize your photos is nice too.\nAs you said, the main reason is startup speed, but also avoiding things like addition to playlists, etc.\nActually, Codeine fits well here. Though intended as video player, it perfectly plays stand-alone sounds, including from internet, with its xine background.\nThe point of Emil, I think, is avoiding to have noatun, kaboodle, and similar apps all provided in the standard package for the same task basically. And I agree with it. I think most people would have enough with codeine/video player for all one-shot media play (without having the app in the task bar, etc.), something like kaffeine for movie collections and amarok for music collection."
    author: "Richard Van Den Boom"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "I dont know about Codeine, But know Kaffeine supports Audio Playback, you could doubleclick on your MP3 File and Kaffeine opens, Plays it easily.\nI would Prefer that Kaffeine gets some usability love instead of having another Player, Kaboodle, Noatun, Juk or...."
    author: "Emil Sedgh"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "But then Kaffeine should remove 80% of what they added since their 0.4.2 release. I think they went in the totally wrong direction with all their playlist bullshit. Actually Codeine is what Kaffeine should be: an actual video player.\n\nI think the parent has a great idea: use Codeine for quick and simple video and audioplaying. If you want to have things like playlists and coverart for audio, go for Amarok. and if, for some weird reason, you want those for video, use Kaffeine.\n\nBTW and I'm all for removing Kaffeine from the default KDE. It's not that usefull for normal users. It doesn't do video well (way to complex) nor music (both Amarok and Juk do a better job)."
    author: "jospoortvliet"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "But I Completely Disagree...\n\nApplication Developers work hard to add features to their applications and you say the Full Featured Applications get of the KDE?\nI Think Kaffeine could be better if just gets some usability Love, Im confused with the GUI too...But I never want this full featured application out!"
    author: "Emil Sedgh"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-11
    body: "100% agree with you, Kaffeine was a really good program, but it's latest revisions \"destroyed\" almost all the good work done (there are comments from me on kde-apps when Kaffeine 0.5.0 was released, dated 2004).\n\nAnyway, I suggest the Amarok people to rename (once again :PP) Video Player to... Player, add minimal support to sound file (well, I think it's already present), put it into kdebase and ship it as the default program associated with every multimedia file in a standard KDE 4.0 installation. "
    author: "Vide"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "For that kind of situation, I'd rather let Konqueror open it. Konqueror can open sound files inside itself using kio or kparts, or send it to an external application depending on configuration. You could set it up so left-clicking on the file opens it in konqui, and middle clicking sends it to the external app (e.g. amarok). With KDE defaulting to Dolphin on the 4 series it will be a bit different, I guess."
    author: "Fede"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "yeah, Dolphin won't use kparts. Yet, the preview pane could show a play button for audio and video - problem solved. No separate app needed."
    author: "jospoortvliet"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "Open the sound file in the filmanger is no good either, since it will lock up your filemanager while playing. And most cases you are interested in continuing to work with it while the soundfile plays. Since those files are usually more than a few seconds, this becomes impractical."
    author: "Morty"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "> Kaboodle provides the play once functionality, for the use case for files you are not interested in adding to your collection/playlist.\n\nFor this case, I'm looking forward to Plasma, when you're able to define multiple default options for files which are symbolized by small dots around the file icon when hovering it. (It was in one of the screencasts some weeks ago.) This way, you can define \"Play Audio File\" and \"Add to Amarok Playlist\" and get both with one click."
    author: "Stefan"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "will this also be possible in dolphin and therefore konqueror? (the thing with the icon-edges ^^). i also didn't unterstand from the comments above if dolphin will be able to play videos and sound files directly or if it needs to open a 3rd party app (or konqueror ^^). now that konqueror also uses the dolphin part.. is konqueror capable of doing this? (with the dolphin view). could someone please explain this to me?"
    author: "LordBernhard"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-07
    body: "Its likely that Amarok (and some of the other neat KDE extragear apps) will have their last stable version tagged and then that will be released along with the next KDE release.\n\nAll the \"good\" KDE distros ignore the kdemultimedia, kdepim etc. and just pick and choose what KDE apps they want to be installed anyways."
    author: "Ian Monroe"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-10
    body: "I agree when you say: \"The need for a simple click-and-play soundapplication is real, when you only want to check out a sound file there is not need for playlist and collections.\"\n\nMaybe you like the mockup I did some time ago:\n\nhttp://kde-look.org/content/show.php/Mini+Audio+Applet?content=34884"
    author: "I\u00f1aki Baz"
  - subject: "Re: Amarok is monopolizing all the love !! "
    date: 2007-08-25
    body: "Have you read about the new Job Progress Interface in KDE4? If not, have a look at [http://dot.kde.org/1169588301/]. It would be a suitable interface for simply playing a sound file and showing how much of it has been played already. As far as I understand, that interface even supports buttons, such as pause and seek (see for example the \"Pause\" and \"Cancel\" buttons in the dialog for the download of \"install-x86-universal-2005.1.iso\" in the screenshot)."
    author: "Erik"
  - subject: "Hardware reqs"
    date: 2007-08-06
    body: "With all the new desktop stuff going on (Plasmoids etc, enhanced Kwin) are we looking at increased memory/swap usage?  Are we going to need more than 512mb of memory for example?  New gfx cards?  How much disc space for a \"standard\" kde 4 install?  Any noticeable enhancements using dual-core cpu's?\n\nI'm not trolling, just asking :-)  Also hoping we're not going down the road known as Vista :-|\n\nPhil."
    author: "Phil"
  - subject: "Re: Hardware reqs"
    date: 2007-08-06
    body: "> With all the new desktop stuff going on [] are we looking at increased memory/swap usage?\n\nIn contrary to what people commonly say; new versions of software in KDE typically use LESS resources than the previous ones.  This is because we keep on working on the code whereas companies releasing software tend to focus only on new features not touching the old stuff because they are afraid it would break.\n\nWhat this effectively means is that new features and new versions really don't have to take more memory.  And in KDE4, you'll actually see that the memory usage of a typical desktop is lower than in a previous version.\nNaturally we provide a lot of cool new features that will gladly eat all your memory and GPU cycles if you let it.\n\nI hope you understand that providing more optional goodies to waste your computing resources on is an entirely different thing then what you are asking.  Note the optional part in my sentence again :)\n\nIn other words; if you use the exact same feature set as you used in kde3, a good memory count (hard to get under unix) is going to tell you that you are not using more memory.  And its likely going to get even better when KDE4 matures.\n\nCheers!"
    author: "Thomas Zander"
  - subject: "Re: Hardware reqs"
    date: 2007-08-07
    body: "That's good to know.  TBH the plasmoids and eye-candy really don't do a lot for me, 99% of my windows are maximized anyway so I rarely see my desktop.  About the only thing I have that's non-standard is Liquid Weather for SuperKaramba.  Dont' get me wrong, a little desktop plasmoid to control Amarok or something would be fun but it certainly isn't the reason I use KDE and I don't want to have to spend $$$ upgrading my machine to run KDE 4.\n\nOn a side issue, thanks to all the KDE-Devs for your hard work and dedication, I'm looking forward to seeing KDE 4 released and using it.  I'm glad there's guys like you giving us an alternative to Microshaft.\n\nPhil."
    author: "Phil"
  - subject: "Re: Hardware reqs"
    date: 2007-08-07
    body: ">  99% of my windows are maximized anyway so I rarely see my desktop\n\nYou know, Aaron has explained several times, quite likely a couple of times more often than he expected to, that Plasma is not just about the desktop. The desktop is only one possible \"container\" for Plasma applets.\n\nYour current setup probably has one or more panels, maybe autohiding, etc. Better think of Plasma as something around (in 3D) your windows, including above and below (where \"below\" would be the traditional desktop).\n"
    author: "Kevin Krammer"
  - subject: "Re: Hardware reqs"
    date: 2007-08-09
    body: "Just switching to Qt4 lowers the memory consumption, I believe.\nKDE SVN runs pretty fast on my really old ThinkPad, and things are far from optimized at the moment."
    author: "Lans"
  - subject: "Re: Hardware reqs"
    date: 2007-08-07
    body: "i wonder about the cpu performance.. i mean now that kde is going to use svg.. everytime you resize something you need much cpu power? the iconcache doesn't solve this problem if i'm right.. it only provides icons at a specific size. (afaik you aren't able to use normal pngs from a default kde setup)"
    author: "LordBernhard"
  - subject: "Thank you"
    date: 2007-08-06
    body: "I've been reading the comments over at Slashdot and OSNews following KDE related articles and must say that I don't know how you guys and gals do it, keep your spirits up in spite of all the trolling you keep running into.\n\nI have some experience with such problems (not only are you uncredited for your hard work, you get put down for it and have to defend it) and this is just a simple \"You KDE-ers rock, thanks for all your hard work\", FWIW.\n\nLong-time KDE user and (hopefully) contributor-to-be. :)"
    author: "Dado"
  - subject: "Re: Thank you"
    date: 2007-08-07
    body: "Yep! I second that!"
    author: "Richard Van Den Boom"
  - subject: "Re: Thank you"
    date: 2007-08-07
    body: "Thirded :)"
    author: "Lee"
  - subject: "Thanks you to all the KDE contributors"
    date: 2007-08-07
    body: "I'm sending out Big amounts of love to all the KDE contributors!"
    author: "Vlad"
  - subject: "SSH support for KRDC"
    date: 2007-08-10
    body: "It's exciting to see that NX is planned for KRDC, but I think it would also be cools to see SSH support. SSH support for forwarding ports, setting up VNC/SSH, or even just plain old SSH would be useful IMO."
    author: "RyRy"
---
In <a href="http://commit-digest.org/issues/2007-08-05/">this week's KDE Commit-Digest</a>: Work in <a href="http://plasma.kde.org/">Plasma</a>, with extra sources for the Weather data engine, work on the applet browser, and the start of SystemTray and <a href="http://www.rsibreak.org/">RSIBreak</a> plasmoids and a "next generation" application launcher, named Lancelot. Cut-down versions of Korundum and Smoke libraries for writing scripted Plasma applets. More interface work for <a href="http://amarok.kde.org/">Amarok</a> 2. More work on <a href="http://xesam.org/">XESAM</a> (a shared metadata specification) integration in <a href="http://strigi.sourceforge.net/">Strigi</a>. An <a href="http://pim.kde.org/akonadi/">Akonadi</a> resource for Facebook information. Support for compressed documents, and more work on DjVu support in <a href="http://okular.org/">okular</a>. Several new features in the <a href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=9064143E62AF5BA6">KRDC</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> project, including bookmark support, sound output, and toolbar options. Custom text shaping in <a href="http://koffice.kde.org/kword/">KWord</a>, and significant progress in the colour mixing capabilities of <a href="http://www.koffice.org/krita/">Krita</a> in <a href="http://koffice.org/">KOffice</a>. Various optimisations in KBounce, KPixmapCache, <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://edu.kde.org/marble/">Marble</a> and KOffice. KOffice 2.0 Alpha 2 (1.9.92) is tagged for release. Beginnings of a D-Bus interface in <a href="http://ktorrent.org/">KTorrent</a> for KDE 4. KNotes and Kompare begin to be ported, Kenolaba completely ported to KDE 4. Reworkings in <a href="http://phonon.kde.org/">Phonon</a>, with the Phonon-NMM backend moved to playground/multimedia, as it is not ready for KDE 4.0. The Kaboodle music player is removed from the kdemultimedia module, whilst kaudiocreator moves to extragear for KDE 4.0.
<!--break-->
