---
title: "KDE Commit-Digest for 23rd September 2007"
date:    2007-09-24
authors:
  - "dallen"
slug:    kde-commit-digest-23rd-september-2007
comments:
  - subject: "Thank you!"
    date: 2007-09-24
    body: "..for yet another commit digest. We all truly appreciate it!"
    author: "kmare"
  - subject: "Wow at Akonadi!"
    date: 2007-09-24
    body: "I love all the work done by KDE developers, but certainly the news of supporting Exchange (which has been a bugbear on free systems) is a massive step forward. How will this compare with Evolution's Exchange Connector (that uses the Outlook Web Access method I believe)?\n\nIt's great to see KDE continually progressing by leaps and bounds; there's so much to be excited about, from Akonadi to Plasma to Amarok to Okular to Dolphin to Koffice2 to K3b to Kross to all the other projects that make KDE such a fantastic platform."
    author: "Alan Denton"
  - subject: "Re: Wow at Akonadi!"
    date: 2007-09-24
    body: "Apparently Evolution's solution is slow because it doesn't use the native MAPI protocol, but uses the web interface to work its magic. This seems like a much better solution."
    author: "Anonymous Braveheart"
  - subject: "Re: Wow at Akonadi!"
    date: 2007-09-24
    body: "Yes, that is great news to me, too. Will there be RPC over https support, too? This is right now the only non-gaming related reason for me to keep a windows installation alive on my private desktop machine...\n\nRegards\nErik"
    author: "Erik"
  - subject: "Re: Wow at Akonadi!"
    date: 2007-09-24
    body: "But be aware that Akonadi won't show up before KDE 4.1. And the PIM applications may be ported even later (I hope not, but I wouldn't be surprised).\n\nAnyway - this is great news :-)\n"
    author: "Birdy"
  - subject: "Re: Wow at Akonadi!"
    date: 2007-09-24
    body: "I join the excitement from having an fully working Exchange solution in KDE. I'm forced to work with Evolution because of the current luck of such solution. Not fun :(.\n"
    author: "yuval"
  - subject: "Re: Wow at Akonadi!"
    date: 2007-09-24
    body: "Why not use Kontact with Exchange?\n\nWe are doing that with our Exchange 2000 server. Mail and calendar on openSUSE 10.2\n\nNow we are moving to Lotus Domino8 , and that will be interesting. Anyone know from the top of their head if ther is support for that in Kontact?\n\ncu"
    author: "birger"
  - subject: "Re: Wow at Akonadi!"
    date: 2007-09-24
    body: "Depends on what plugin/protocol Lotus Domino 8 supports. dIMAP, Groupdav should work ok, for example."
    author: "liquidat"
  - subject: "Re: Wow at Akonadi!"
    date: 2007-09-24
    body: "An alternative to Evolution's exchange connector will be very useful.\n\nI've yet to get Evolution to connect to my employer's Exchange server.  I'm met with an uninformative error message so I can't tell if it's my fault, Evolution's or the configuration of Exchange.\n\nI've no idea if it really is a NTLMv2 issue or something else.  Nor do I know if this affects lots of people or just a handle with unusual Exchange setups.\n\nhttp://bugzilla.gnome.org/show_bug.cgi?id=323619\n\nFingers crossed that the OpenChange project will interoperate more easily with Exchange."
    author: "Paul Fee"
  - subject: "Re: Wow at Akonadi!"
    date: 2007-09-27
    body: "Not surprising that bug report, the Exchange connector is a piece of crap that never worked properly but only for a subset of Exchange users, and more often than not, it would make Evolution crash big time."
    author: "NabLa"
  - subject: "More KDE SVN reorganisations"
    date: 2007-09-24
    body: "does this mean we'll soon switch to Git? :-D"
    author: "Patcito"
  - subject: "Re: More KDE SVN reorganisations"
    date: 2007-09-24
    body: "No it means applications are moved to the proper modules and therefore packages."
    author: "Diederik van der Boor"
  - subject: "Samba?"
    date: 2007-09-24
    body: "With Samba adopting GPLv3 while KDE is still under v2, wouldn't that be a problem when using OpenChange? \n\n"
    author: "AC"
  - subject: "Re: Samba?"
    date: 2007-09-24
    body: "If you read the article, you would have seen they talked about that.  It will only be released as source code for now, and they're hoping that by 4.1 the licensing issues will take care of themselves."
    author: "Matt"
  - subject: "Re: Samba?"
    date: 2007-09-24
    body: "What's the latest from the Trolls on GPLv3? Last I heard they were still considering their options..."
    author: "hmmmm"
  - subject: "Re: Samba?"
    date: 2007-09-24
    body: "Wait for at least 3 months before asking again ;-)\n\nLawers need to think about this, not hackers. So it'll take time..."
    author: "jospoortvliet"
  - subject: "Re: Samba?"
    date: 2007-09-24
    body: "Not really.  The GPL has always been very clear, that when you include GPL code in your project, you need to stick with the GPL.\n\nThey are quite adamant that GPLv3 may only be licensed GPLv3 or later.\n\nHonestly, I'm not sure what new code exists in Samba since the Samba GPLv3 split was fairly recent, but if KDE wants to include it and remain GPLv2, the only solution I see is to take a fork of the last GPLv2 Samba, and then rewrite any new code from the GPLv3 branch that they require."
    author: "T. J. Brumfield"
  - subject: "Re: Samba?"
    date: 2007-09-25
    body: "But nobody is going to have the time to do such non-sense. I am quite sure, that KDE will simply wait for Trolltech to allow go GPLv3. To them it makes a lot sense, so I bet they are simply currently evaluating how to do it.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Thank you!"
    date: 2007-09-24
    body: "> Aaron J. Seigo committed changes in /trunk/KDE/kdebase/workspace/libs/plasma:\n> lock screen and log out ... this makes things slightly more bearable for me\n> on a day to day basis ;)\n\nGreat to hear. :-) I was missing this already :p"
    author: "Diederik van der Boor"
  - subject: "Re: Thank you!"
    date: 2007-09-27
    body: "wait.. what? I've been able to log out for ages, I just hit ctrl-alt-del and the lock/logout thing comes up. ...except when the keyboard shorcuts got broken, that is :)"
    author: "Chani"
  - subject: "Completely unrelated, but..."
    date: 2007-09-24
    body: "Do we have any news from Jacob Rideout? Has Sonnet been abandoned?"
    author: "Balinares"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "no - no."
    author: "jospoortvliet"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "*zzzz* Does not compute.\n\nI thought that Jacob was the main Sonnet developer. Is he still committing without actually commenting on his progress or has somebody else picked up his work?"
    author: "Erunno"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "I know that Zack Rusin (yep - *that* Zack Rusin!) has been doing some work on it, but I'm not sure to what degree - it could be anywhere from \"keeping it compiling\" to \"actively developing\"."
    author: "Anon"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "personally, im worried about jrideout, not sonnet, sonnet is just a software, somebody will do it, whats important about this issue is Jacob, I hope that we will get some good news from him..."
    author: "Emil Sedgh"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "Well said. (Do I have to mention that I completely agree?)"
    author: "Hans"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "I think a lot of people have come to the conclusion that Jacob is dead, as no one can seem to find any trace of him at all even after putting out a plea for help.  The other option is that he's just taking a break from all things online, but it's been long enough without any notice that this is whats happened that it seems unlikely."
    author: "Matt"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "I've seen people disappear online for literally years at a time.  Sometimes people step away from one community simply because they decided to refocus on other areas of their life.\n\nIt happens, so I wouldn't assume death."
    author: "T. J. Brumfield"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "m....t"
    author: "anonymous coward"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-24
    body: "a few weeks ago i searched for dead people named rideout, I even found a Jacob Rideout, but he was dead about one hundred years ago...\ni didnt find anything about 'Mountaint Goat Programmer', the KDE Developer, Jacob Rideout...\nso i hope that he is just 'Away from our community'\n\nand a little suggestion, its not too bad to ask for a little news in theDot asking for anybody who knows jrideout or has any news about him?\nKDE is a community, its members are more important than its technical parts (at least, I think)"
    author: "Emil Sedgh"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-25
    body: "I prefer to think of other, less tragic possibilities, such as mental illness,  physical disability, financial ruin, on the run from the law, etc, that at least hold some hope for him.  But the total lack of contact I think rules out the 'time-out' option, as I think he seem's the sort of guy who would at least have posted a 'sorry, back later' note (he's a member of a lot of online groups like kde and ubuntu).\n\nIt's certainly made me wonder, if I was hit by a car if my family would be able to access my digital legacy or at least let my online friends know?\n\nThere are options for finding out if he is dead, the obvious being the Colorado vital records registry, but we have no guarantee he was in Colorado at the time (maybe CA).  While certificates are only issued to involved parties, it's possible the index is publically accessable.  Their web site http://www.cdphe.state.co.us/certs/birth.html is not clear on this, someone local would need to visit or ring to ask.  Another option would be to search the newspaper death listings and police reports, again something someone local would have to do at the library.\n\nSomeone has previously mentioned he's a member of the Ubuntu Colorado LoCo team (https://wiki.ubuntu.com/ColoradoTeam), they might be able to provide the shoe leather.\n\nBasic known facts:\n* last blog 10th Feb 2007\n* last ubuntu launchpad contribution 14th Feb 2007\n* last kde commit 18th Feb 2007\n* last kde-devel post 19th Feb 2007\n* last digg on 28th Feb 2007\n* whois gives details as\n      PO Box 1760\n      La Jolla, CA  92037\n      US\n      720-252-9928\n(was some talk of writing there, not sure if anyone did, but CA adds another place to search)\n* www hosted at rimuhosting.com (good sign as still up so still paying bills, or just in advance?)\n* his very old stumbleupon page (http://jrideout.stumbleupon.com/) puts him as 20 in about 2004, so would be 22-23 now.(perhaps contact his listed friends?)\n\nHe seems to have had a couple of freelance companies, such as R Wilde & Associates (http://www.technical-outsourcing.com/profile.php?id=16948), but in a more recent post mentions a colour-blind secretary complaining about some software their company had written, so employment situation is not clear.\n\nOn his last blog, there's a comment in May from a Tim Dinsmore seeking contact, perhaps try drop him a line?\n\nSee http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/428-Where-in-the-world-is-Jacob-Rideout.html for a couple more links.\n\nThere's a youtube user at http://youtube.com/jrideout who last logged in 1 day ago, but he's a Joe Rideout."
    author: "Odysseus"
  - subject: "Re: Completely unrelated, but..."
    date: 2007-09-26
    body: "Perhaps he's found a girlfriend!"
    author: "cupid"
  - subject: "To-Do list"
    date: 2007-09-24
    body: "I also want to say thanks for another great digest.  I love catching up with it, even though I also hear the main points on planetkde.\n\nBUT... it's occurred to me for a while that there are far too many announcements in Free Software for things like \"improved exchange support\".  Every time I hear somethign like that, i think, \"Oh, it must be fairly complete by now.\"  Then, six months later, or more, I see something like \"Better exchange support.\"  Not to belittle the improvements, which are great, but... it's a serious feeling of deja vu, and such announcements undermine my confidence in Free Software, rather than boosting it, sometimes.\n\nHow about, instead of (or as well as) normal feature announcements, having \"progress announcements\" and status reports that show how complete the support for something is, and what features have been improved?  Then we could just look at apps and see, \"OK, this does what I want\", or \"OK, this doesn't do what i want, but at the current rate, it'll be there by the time my project is launching, and I can always help out...\"\n"
    author: "Lee"
  - subject: "Re: To-Do list"
    date: 2007-09-24
    body: "Read the commit-digest, and you'll see the words \"There is still a very long way to go, and (as always) more help is needed in many areas\", following that up with a TODO list.\n\nSo in short, read the article itself before suggesting what's already been thought of by others. :P"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: To-Do list"
    date: 2007-09-24
    body: "That is a great idea. Where is your web site with the test results?\n\nHave you ever thought that these announcements are in fact hints that testers and developers are welcome to look at the code, test, add finishing touches, etc?\n\nFree software is a process, not a product.\n\nDerek"
    author: "D Kite"
  - subject: "Re: To-Do list"
    date: 2007-10-02
    body: "Of course I've thought about contributing to free software.  I do it all the time.  You could also take my posts here as a contribution to the process --- if you so chose."
    author: "Lee"
  - subject: "Re: To-Do list"
    date: 2007-09-25
    body: "As far as I can tell, Evolution unfortunately doesn't share the exchange stuff by using OpenExchange, but does it all by itself. Not sure, though, can't find clear info about that..."
    author: "jospoortvliet"
  - subject: "Re: To-Do list"
    date: 2007-09-25
    body: "That is probably changing, as the openexchange people are working on an evolution plugin: http://www.openchange.org/index.php?option=com_content&task=view&id=65&Itemid=74"
    author: "Alan Denton"
  - subject: "Re: To-Do list"
    date: 2007-09-25
    body: "This is true. Note that there is a potential issue with GPLv2 (Evolution) and GPLv3 (Samba). I'm not sure how many people have copyrights on Evolution outside of Novell, or how that relicensing is going."
    author: "Brad Hards"
  - subject: "Re: To-Do list"
    date: 2007-09-25
    body: "That's very good, as Evolution is burning through a lot of support from Novell and others to get good Exchange support. If that support would be shared through openExchange, that'd be great."
    author: "jospoortvliet"
  - subject: "Re: To-Do list"
    date: 2007-09-25
    body: "You are mixing things up, but it's not your fault that these names are almost identical.\n\nOpenExchange is a groupware server product, OpenChange is, right now, a Free Software reimplementation of the Microsoft Exchange protocol and its API (called MAPI).\n\nOpenChange, according to its website, aims at providing a full server replacement at a later stage though."
    author: "Kevin Krammer"
  - subject: "Re: To-Do list"
    date: 2007-09-26
    body: "yes, you're right. i meant OpenChange, it was a slip of the keys :)"
    author: "Alan Denton"
  - subject: "Re: To-Do list"
    date: 2007-09-25
    body: "It is hard to estimate how \"complete\" it is, or how long it will take to be \"done\". \n\nWe don't really know how hard it is going to be (there might be a lot of problems, or it might be fairly smooth sailing), we don't know if there are going to be major setbacks that require new architecture or rework, and we don't know if anyone is going to lend a hand.\n\nRight now, I'm the only one working on the OpenChange plugin. I'm not especially motivated (I don't need it, I just think it is an important thing for KDE to have), and I'm reliant on work by both the OpenChange developers (actually, most of the work is done by just one person) and Akonadi developers. \n\nI'd say the OpenChange plugin is \"maybe 10%\" done. Certainly much less than half. I'm not sure how much work is left on OpenChange libraries or Akoandi, but certainly a lot."
    author: "Brad Hards"
  - subject: "Re: To-Do list"
    date: 2007-09-28
    body: "Well open source is inherently openly developed, its kind of hard to avoid this phenomenon. "
    author: "Ian Monroe"
  - subject: "Gwenview"
    date: 2007-09-24
    body: "Does anyone knows if gwenview will start shipping with a kipi plugin for picasaweb?\nI know there is one in development sinse july or early."
    author: "Iuri Fiedoruk"
  - subject: "Re: Gwenview"
    date: 2007-09-25
    body: "WEll, if the plugin makes it into the kipi plugin suite, gwenview will support it..."
    author: "jospoortvliet"
  - subject: "Road to KDE?"
    date: 2007-09-24
    body: "What happened to all those \"Road to KDE\" articles?\nIt's been a long time since the last one was published and they we're really interesting..."
    author: "Coward"
  - subject: "Re: Road to KDE?"
    date: 2007-09-24
    body: "I'm sorry - I've been busy. *sigh* I've had material to write at least two of them, but never got around to actually writing them. (I'm also a full time student, and hold some other jobs on the side, so writing KDE articles isn't always the best way to relax)."
    author: "Troy Unrau"
  - subject: "Re: Road to KDE?"
    date: 2007-09-25
    body: "It is nothing to be sorry about, we really love what you have done already. It's truly amazing that you and all the other KDE guys offer up your free time to help us enjoy KDE even more. \n\nThanks!\n\n"
    author: "Mythor"
  - subject: "Re: Road to KDE?"
    date: 2007-09-27
    body: "Don't be sorry. If anything, we're grateful. You're doing a tremendous job. Thank you!"
    author: "Derek"
  - subject: "KTabedit"
    date: 2007-09-24
    body: "It's cool to see KTabedit being worked on. I wonder if there would be any overlap there (eg synergy possible) between that app and the notes thing in KOffice?!?"
    author: "jospoortvliet"
  - subject: "Kolourpaint"
    date: 2007-09-24
    body: "And I see Clarence wonders if he can get Kolourpaint finished in time... bleh, as Kolourpaint rocks ;-)\n\nI mean, imagine you use Gnome. And you want to quickly edit a picture. YOU HAVE TO START THE GIMP! That's horrible, even if you want to do something complex ;-)\n\nIn KDE, you have Krita for the complex stuff, Kolourpaint for the basics and Showfoto (from Digikam) for Photo retouching. We got it all covered very neath."
    author: "jospoortvliet"
  - subject: "Re: Kolourpaint"
    date: 2007-09-24
    body: "While I appreciate your recent work for the KDE Ministry of Propaganda I don't see how this inflammentory posts you keep occasionaly churning out helps the relationship between the communities. Ah well, must be that I was always sitting on the fence between my brain telling me to use KDE and my guts to go for GNOME so I never actually felt negative feelings towards Smallfoot. ;-)"
    author: "Erunno"
  - subject: "Re: Kolourpaint"
    date: 2007-09-24
    body: "++\n\nJos's KDE advocacy is helpful, but his off-topic side-swipes at GNOME are childish, irritating, and are really putting people off the KDE project.  As one person in this article ( http://osnews.com/comment.php?news_id=18662 ) just remarked:\n\n\"If you think that your bashing to GTK+ applications will makes us switch to Krita or KDE think again, it really sucks that a good project like KDE is infected with trolls like you. \"\n\nPlease, Jos - knock this off.  KDE has plenty of merits of its own to talk about without resorting to belittling the \"competition\"."
    author: "Anon"
  - subject: "Re: Kolourpaint"
    date: 2007-09-24
    body: "I think it was a rather funny joke actually and I think gnome devs/users would also think it's funny (or at least wouldn't mind the joke)."
    author: "anon2"
  - subject: "Re: Kolourpaint"
    date: 2007-09-24
    body: "The problem is, of course, that this sort of joke works for about 5 percent of Internet readers. Not necessarily because the rest is to thick for that, more because the average Internet forum has far more trolls than it has comedians."
    author: "Just me"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "Meh, I get a bit carried away, that's for sure. Sorry. Though, in that thread, I thought the 'I only start gimp if I need a good laugh' actually was a good joke... And aside from the joke, who would disagree?"
    author: "jospoortvliet"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "I would. I actually use Gimp on a day-to-day basis, because it's the only Free Software program that actually does the job well enough for me. Krita is just not there yet although I have high hopes that some day it will. Krita needs more polishing, it's somewhat slow in some operations and it doesn't support all the things I need (think of guides, for example).\n\nOther than that, let's just all keep in mind that we're not competing against GNOME, but against Windows (and to a lesser extent MacOS)."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Kolourpaint"
    date: 2007-09-27
    body: "I don't agree with you Sebastian. \n\nWe are of course competing with other open source applications. But as with all competition, it shall be fair competition. No bashing.\n\nWindows is of course the main competitor on the desktop space, and I agree that most of our efforts should go towards that competitor.\n\nIt must not come to the situation that it is politically incorrect to be an advocate for the solutions one believs is the best in the long run.\n\nBirger"
    author: "birger"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "Jos made a good point in that having to use a complex graphic tool like GIMP for making minor changes to a photo is crazy. Look at the Windows world: there's good old MS Paint and then there's Adobe Photoshop. Targeted for different tasks and different user-skills. KDE's got it right, IMHO.\n\nAs for the assertion that GIMP's UI is horrible, well... I think plenty of people would agree with that, even GNOME users."
    author: "Scott"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "i would think that's a fairly safe assumption - witness the current drive towards a GUI redesign for the GIMP :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "Well since we are on a KDE board, let's worry more about Krita's interface.\n\nLike for example, is there a way to open more than one image at a time without starting multiple Kritas yet?"
    author: "hmmm"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "Sure you can. You can do that already. But all images will get into a separate window. And unless KOffice 2.something-but-not-0-or-1-or-even-2-most-likely will decide that having a tabbed interface is a good thing, it will stay that way. We won't do the Photoshop-on-windows-which-sucks-for-any-real-Photoshop-user way of having a master window with multiple child windows.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "Hi Boudewijn,\n\nI was wondering - GIMP is obviously the GNU *Image Manipulation Program*, whereas Krita bills itself more as a painting app which integrates with KOffice.  Are you planning to focus more on the \"painting\" side, or more on the \"image manipulation\" side as time goes by? Put more simply - do you ever intend for Krita to be a direct competitor with the GIMP, with the same abilities and use-cases, or is Krita really a different sort of application with different goals to those of the GIMP?\n\nCheers!"
    author: "Anon"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "As far as I understand the whole issue, drawing/painting and image manipulation aren't an either-or thing. So Krita does want to do what Gimp does (and as far as I can tell, it already can - Gimp probably only has more plugins)."
    author: "jospoortvliet"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "> As far as I understand the whole issue, drawing/painting and image\n> manipulation aren't an either-or thing.\n\nThis is not entirely true. Photoshop and the GIMP have some painting / artistic creation related functionality, but there are applications that are more geared toward such tasks (Painter: http://en.wikipedia.org/wiki/Corel_Painter, which attempts to better simulate different painting media, for example). In a broad sense they are all raster image editors (in contrast to vector graphics programs like Inkscape or Illustrator), but certainly they specialize in different areas under that umbrella.\n\nI would love to see Krita go more in the drawing/painting direction than photomanipulation, since the GIMP is definitely better suited to the latter. It is also an area that the free software ecosystem could stand to have an app specialize in... ;)"
    author: "Matthew Kay"
  - subject: "Re: Kolourpaint"
    date: 2007-09-25
    body: "I want Krita to be a Corel Painter competitor: that includes a hefty amount of image manipulation, including vector and text objects (thanks to KWord and Karbon based flake shapes), including 16 and 32 bit/channel image hacking. But it also includes dynamic, natural media paint simulation. Like we got this summer: natural paint mixing, complex brush loading, programmable brushes.\n\nRight now, if you want to manipulate your 16 bit/channel raw image in the L*a*b colourspace you've got no free software choice but krita; but we're not good enough yet. I know that. My personal goal (because, yes, there's some rivalry, but keep this in mind: I really like and respect the gimp people, they are great guys and gals to meet, and there's lots to learn from their work) is to release a Krita with natural media simulation and dynamic everything layer stacks before Gimp 2.6 is released.\n\nBut my main point of reference is Corel Painter: if I can follow the tutorials in the main Corel Painter magazine in Krita, not to the letter, but to the intent, and with more ease than would have been the case had I used Corel Painter, I'll be a happy man, and feel I can stop hacking and start painting again."
    author: "Boudewijn Rempt"
  - subject: "Re: Kolourpaint"
    date: 2007-09-26
    body: "Great; thanks for the detailed and comprehensive response - Krita is in very good hands, I think :)"
    author: "Anon"
  - subject: "Re: Kolourpaint"
    date: 2007-09-26
    body: "If it's inspired by Corel Painter, does that mean it won't compete with e.g. Photoshop or Pixel? Will artists use the two programs together (e.g. krita for art creation, then gimp/photoshop/pixel for manipulation)?"
    author: "anon3"
  - subject: "Re: Kolourpaint"
    date: 2007-09-26
    body: "My point was that pixel manipulation is simply needed in a painting application, so there is nothing in the gimp/photoshop that Krita won't need to do.\n\nDrawing/painting is EXTRA on image manipulation. So Krita can do stuff Gimp and Photoshop can't but there is no reason the opposite should be true."
    author: "jospoortvliet"
  - subject: "What about BasKet?"
    date: 2007-09-25
    body: "Just wondering if anyone was found to help out with that program. It looked very promising and it's a shame the developer had to leave..."
    author: "Darryl Wheatley"
  - subject: "Re: What about BasKet?"
    date: 2007-09-25
    body: "Others have taken over, I believe, though progress appears to be very slow.  Development is not carried out in KDE's svn, unfortunately, so it's hard to say how it's going and even harder for KDE devs to contribute to it.  I wish the basKet devs would import it into main KDE SVN so that everyone can have a shot at it - I bet someone would have ported it to KDE4 and translated it into dozens of languages already if they had ..."
    author: "Anon"
  - subject: "And Scribus?"
    date: 2007-09-26
    body: "I realise Scribus is a Qt-based app and independent of KDE, but does anyone know how it's going? I heard that with the port to Qt4 they may start integrating even better with KDE."
    author: "scribus fan"
  - subject: "Re: And Scribus?"
    date: 2007-09-27
    body: "Given the way KDE4 is getting ported to Win32 and OSX, this might become a possibility, but finishing the port and the new text engine are priorities. That does not mean we do not want to have the option, but it is just not yet the highest priority.\n\n"
    author: "mrdocs"
  - subject: "Dolphin/Konqueror and ACL manipulation?"
    date: 2007-09-27
    body: "It is realy great to see all these improvemnts comming in to Dolphin(and Konqi).\n\nI tried to search for acl handling and Dolphin , but came up with little. Any information on Dolphin in that area?\n\nWill it be possible to manipulate ACL entries? Any possibility of reading user and group names from authentication sources? AD/LDAP++?\n\nRegards Birger"
    author: "birger"
  - subject: "Re: Dolphin/Konqueror and ACL manipulation?"
    date: 2007-09-27
    body: "> Will it be possible to manipulate ACL entries?\n\nThis is already implemented (long time ago). Just mount a partition with ACL option and open Konqueror. In \"properties\" you can handle ACL permissions perfectly. ;)\n\n\n> Any possibility of reading user and group names from authentication sources? AD/LDAP++?\n\nThat is already possible and it's not depending on user desktop, but in libnss-ldap package and nsswitch.conf configuration.\n \n"
    author: "I\u00f1aki Baz"
---
In <a href="http://commit-digest.org/issues/2007-09-23/">this week's KDE Commit-Digest</a>: A security fix developed for KDM, covering KDE 3.3.0 to 3.5.7. A KioBrowser data engine, HDD monitor applet, and general layout work in <a href="http://plasma.kde.org/">Plasma</a>. More refinements in <a href="http://edu.kde.org/parley/">Parley</a> (formerly KVocTrain). GeoData subproject in <a href="http://edu.kde.org/marble/">Marble</a> to support popular geographic data formats. An AI player added to Kombination. Development renewed on the KPicross game. Basic printing support in <a href="http://gwenview.sourceforge.net/">Gwenview</a>. Improved mimetype detection, as per the cross-desktop specifications. More work on text highlighting in <a href="">Kate</a>. Continued developments and optimisations in <a href="http://pim.kde.org/akonadi/">Akonadi</a>, including the OpenChange (Exchange) connector. Further work on the GStreamer <a href="http://phonon.kde.org/">Phonon</a> backend. Colourspace work in <a href="http://www.koffice.org/krita/">Krita</a>, greater definition given to <a href="http://koffice.kde.org/kchart/">KChart</a>2. File management part in <a href="http://www.konqueror.org/">Konqueror</a> is replaced by a shared <a href="http://enzosworld.gmxhome.de/">Dolphin</a> part usage. More KDE SVN reorganisations.

<!--break-->
