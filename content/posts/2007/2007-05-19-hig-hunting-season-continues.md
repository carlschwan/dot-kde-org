---
title: "The HIG Hunting Season Continues"
date:    2007-05-19
authors:
  - "oschmidt"
slug:    hig-hunting-season-continues
comments:
  - subject: "How ironic"
    date: 2007-05-19
    body: "I made a typo in this story, which is about finding typos - and no, I don't mean the intentional mitsake."
    author: "Olaf Schmidt"
  - subject: "nice goal, you'll never reach it."
    date: 2007-05-19
    body: "They really think they can fix KDE's HIG issues before the 4.0 release? I have my doubts. What I'm guessing is going to happen, they'll say they fixed the HIG issues in 4.0, but when compared to Gnome and OSX it'll be substandard."
    author: "Steve Warsnap"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-19
    body: "Nice trolling.  But it's better than gnome already (a drool-proof interface isn't the same as good usability) and while OSX itself is pretty good HIG-wise Apple seem to have lost the way a bit recently, with the various experimental interfaces they release.  I don't say 4.0 will be perfect, but hey - nothing is."
    author: "Adrian Baugh"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-19
    body: "How well does it scale? It is good to start a formal HIG review process. The same needs to happen in terms of security review, and the best method is of course to automatise quality testing as much as you can. EBN is a fantastic tool.\n\nBtw: with Feisty's KDE 3.5 pasting text from Mozilla Firefox (an url) to the console (e.g. for wget) does not work, but when you past it to kate and repaste it to the console it does work. Interesting it also works with the console context menu but not when pasted with the mouse. Kate? Ah he: On the console kate throws kate: WARNING: Pixmap not found for mimetype application/vnd.oasis.opendocument.presentation hm?? How can this be checked and tested? Use cases? Most problems we have with KDE have to do with these interfaces. When you look at the forums you see that people have always the same problems. And you see that this is no inner-KDE issue. When just testing KDE it does not occur as you would use Konqueror. Hmm, how come that someone wanted to use Mozilla? Problems usually occur at the interfaces or when real world data is fed. For version 4.0 talkback versions should to be provided, that is if an application in use throws warnings that needs to be fed back to the developers. Quality wise an application is mature when it takes more than 5 minutes for you to find a bug. \n\nThird party problems really need review. When we think of fonts, then it should be possible to use the fonts but when certain features or needed codesets are not supported by the font file I would like to know it before I chose to use the font, so I don't have to wonder why it does not work. Is there a font testing environment, to test for KDE complicance?\n\nSame for icon sets, currently there is no way for a user to find out how complete an icon set is. If you had tests you could say: this icon set is complete for x. And get a kind of KDE certificate which motivates third party providers to make it complete. A, B, C compliant. Same for languages, we have some languages which are in a beta stage completeness and quality wise. http://l10n.kde.org/stats/gui/stable/ Here we have a bar but users don't get informed, and some languages e.g. nds just don't work, a problem of Feisty or KDE? Don't know. \n\nAnd in terms of fonts: just take the most common third party fonts files and test them. Else you have the same problem as with printer drivers. You haven't tested them and chose a random one. If it works its fine for you. but there may be better ones with other cryptic names and features you don't even want to know. Third party files, fonts, icons, hardware, plugins, mp3s whatever should not be able to corrupt KDE and if so you need to know before that the file is only for beta or alpha use."
    author: "Andre"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-20
    body: ">  with Feisty's KDE 3.5 pasting text from Mozilla Firefox (an url) to the console (e.g. for wget) does not work, but when you past it to kate and repaste it to the console it does work\n\nHow did you copy it? Did you properly select it with the mouse (for pasting with the mouse) or did you do a copy action, e.g. context menu, CTRL+C?\n\n"
    author: "Kevin Krammer"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-20
    body: "select- left click - copy \nand paste with middle mouse button.\n\nmaybe no KDE problem because you have the same problem with Mozilla. As if there were two different clipboards."
    author: "Andre"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-20
    body: "That's because there *are* two different clipboards in X - thus people who are expecting a Windows/Mac style clipboard get what they want, and people who expect a Unix style clipboard get what they want also. Just don't try to mix the two."
    author: "Paul Eggleton"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-20
    body: "Hmm, well, true.\n\nBut in this case it should have worked. After selection the URL text with the mouse it should be in the one \"clipboard\" for middle mouse paste and after doing the copy action it should *also* be in the other clipboard.\n\nA problem would be if one has a link with a text that it is not the link's URL, i.e. the part inside the \"a\" tag is not the text in the \"href\" part.\n\nIn this case one can only copy the link to the \"action clipboard\" and not to the \"selection clipboard\", thus cannot paste it with middle mouse click.\n\nHowever I am not sure if an application is or is not allowed to set the \"selection clipboard\" as well if it doesn't have an explicit selection.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-22
    body: "I just tried that on Feisty from Firefox into a konsole window and I can't reproduce this issue. I have Feisty 64bit edition. This kind of stuff makes it very very hard to find and fix bugs since you have to first be able to replicate the bug. I see a lot of this kind of stuff in the software I write, a user will have a strange bug that can't be replicated and nobody else experiences, most we track down end up being something the user did or some screwed up settings on their machine. \n\nComputers are very complex and expecting to find and fix all bugs is just unrealistic."
    author: "Kosh"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-19
    body: "If you've read the previous posts, no-one on the usability team has claimed that all the HIG issues will be fixed, or even that the HIG will be complete, before 4.0.  This is an ongoing process.\n\nI'm not going to comment on comparisons with Gnome or OSX because I don't use either of them enough to compare them."
    author: "Alex Merry"
  - subject: "Re: nice goal, you'll never reach it."
    date: 2007-05-21
    body: "\"I have my doubts. What I'm guessing is going to happen, they'll say they fixed the HIG issues in 4.0, but when compared to Gnome and OSX it'll be substandard.\"\n\nAnd so it starts. HIG issues in KDE aren't HIG issues in the same way as OS X or certainly Gnome. It appears that there is going to be some invites for help, and some debate, rather than a top-down autocratic approach where someone has opened some usability book, has read various chapters and think they know it all. Don't kid yourself - Gnome in particular has a ton of usability, interface and especially consistency issues, mostly because their applications don't have a common library and a concept of inheritance.\n\nI've said before that I really wonder what many people will complain about as KDE 4 moves through its development cycle. What we'll probably get are various comments such as \"Oh, this is an unachievable goal - you'll never get there\". Well, if you're going to troll about it then I would say that Gnome doesn't have a snowball in hell's chance of creating the technical underpinnings and infrastructure to create a great desktop and applications that are sustainable long-term."
    author: "Segedunum"
  - subject: "opera 9.2 scaling is very good for text and images"
    date: 2007-05-19
    body: "opera 9.2 has good scaling shortcut \"ctrl++\" will maximize everything in the window to the visible area.\n\nKDE 4.x should include such a nice feature of text and picture scaling for better visibility and accessibility."
    author: "fast_rizwaan"
  - subject: "EBN"
    date: 2007-05-19
    body: "No trolling intended, but I wonder if (theoretically) these HIG-checks can be automated and for example be integrated in EBN? Somehow it doesn't feel right to do this manually. Thanks!"
    author: "LB"
  - subject: "Re: EBN"
    date: 2007-05-19
    body: "I think you're overestimating the mighty powers of the EBN :)\n\nIt's probably pretty hard to detect HIG violations (which are, of course, contraventions of *human*, aesthetic sensibilities of the running user interface) from static analysis of the program code.  Maybe I'm wrong, though."
    author: "Anon"
  - subject: "Re: EBN"
    date: 2007-05-19
    body: "> No trolling intended, but I wonder if (theoretically) these HIG-checks \n> can be automated and for example be integrated in EBN?\n\nSome can be automated, and the EBN already has a few basic user interface tests which look for overpopulated menus and spelling errors.\n\nThe font and spelling checks probably can be mostly automated, but we don't have that technology yet - so doing it by hand makes sense.  Even in areas which the EBN does cover, such as spelling errors, it cannot really prioritise them.  That is, it doesn't know the difference between a spelling error that shows up in 15 point text in the main window from a spelling error which only shows up in an obscure dialog which is rarely used.  \n\nFinally of course, writing automated tests to answer more subjective questions is hard.  Is this dialog intuitive to use?  Is this warning message understandable?  Does the application make me feel stupid?  Does the application look pretty?\n\n> Somehow it doesn't feel right to do this manually.\n\nI disagree.  This is about human interaction a computer, as such it isn't a technical problem, so trying to solve it completely using technology would be a mistake.  \n\n\n\n\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: EBN"
    date: 2007-05-19
    body: "> The font and spelling checks probably can be mostly automated, \n> but we don't have that technology yet - so doing it by hand makes sense.\n\nSorry, correction:  We don't have the technology to do the font checks yet.\n"
    author: "Robert Knight"
  - subject: "Re: EBN"
    date: 2007-05-19
    body: ">>Somehow it doesn't feel right to do this manually.\n \n>I disagree. This is about human interaction a computer, as such it isn't a >technical problem, so trying to solve it completely using technology would be >a mistake.\n\nThanks, for your views, I understand you and agree with you."
    author: "LB"
  - subject: "Re: EBN"
    date: 2007-05-19
    body: "I'm pretty sure that the EBN could easily check for hard-coded colors and fonts pretty easily (it seems like a fairly simple task), but I wouldn't know where to begin writing such a beast."
    author: "Anonymous Coward"
  - subject: "Re: EBN"
    date: 2007-05-19
    body: "But hard-coded colours are not always bad. It depends how they are being used, and scripts from EBN would have a hard time inferring that. Hard-coded font sizes/families are less easily justifiable ;-)."
    author: "Paul Giannaros"
  - subject: "Re: EBN"
    date: 2007-05-20
    body: "Hardcoded colors are not bad if you do not care for accessibility. Otherwise they are a real problem.\n\nThemes like high-contrast do not work as required if you do not use system colors, for example, in the white-on-black mode, hard-coded black text will not be visible. \n\nThen there is the problem with color blindness, where color blind people may want to adjust colors so that, say warning/OK colors are easy to distinguish.\n\n"
    author: "Luciano"
  - subject: "Re: EBN"
    date: 2007-05-20
    body: "\"in the white-on-black mode, hard-coded black text will not be visible.\"\n\nsomeone was complaining about exactly this in #kde today. he had a transparent kicker and a black background, iirc, and the text of programs was shown in black when they started up. :)"
    author: "Chani"
  - subject: "Re: EBN"
    date: 2007-05-19
    body: "there are approx. thousands of different tasks which would need testing. when you have a clean environment people will not pollute the environment. The real point is to prevent that developers mess around. The earlier issues are caught the better.\n\nHIG means for me \n1. dont's use popups\n2. minimize clicks\n3. ensure everything can be done with key board (plug out your mouse)\n4. Think about contrast\n5. change the screen solution\n6. Remove all unnessasary text, menu entries\n\nIn general look at all popup windows invocations in the source and rethink if they are needed. The mozilla search dialogue is a perfect example how to do it right. Same can be done for password field. As a educatory means just combine your personal popup dialogue with a really annoying sound, you really want to get rid off it.\n\nI am still a bit surprised about the placement of the \"kill process\" button.\n\n"
    author: "Ben"
  - subject: "Kopete"
    date: 2007-05-20
    body: "Kopete in 3.x is a good example. Even if you turn off all configurable notifications, or set them to amodal, it *still* pops up dialogs where you have to click \"OK\" to close them. This happens whenever it experiences any of a number of problems in its communication to the various servers. After you click OK, you have to manually restore whatever connection went down.\n\nHere is how it should work:\n1) Remember the user's desired connection state for each protocol.\n2) If there is a problem, by all means indicate that by a warning triangle or so on the system tray icon.\n3) Then periodically try to restore the connection state that the user has requested.\n4) When successful, remove the warning triangle. By default don't pop up any message at this point either; frankly I don't care that much that jabber.org is back online.\n\nThis constitutes a relationship of trust between me and the application. I tell it what to do and then trust that it is doing what it can to make it so. It need not come running back to me when it has a problem; I trust it do do whatever can be done to fix the problem."
    author: "Martin"
  - subject: "Re: Kopete"
    date: 2007-05-20
    body: "Kopete is a very good example. Indeed. kopete usually is run at startup and annoys me with a popup dialogue everytime I started KDE."
    author: "Ben"
  - subject: "Re: Kopete"
    date: 2007-05-20
    body: "I completely agree.  This is especially annoying since jabber.org goes down so often.  Same goes for AIM and MSN, but not as often.  Automatic reconnect without complaining would be an enormous improvement for Kopete!"
    author: "Matt"
  - subject: "Re: Kopete"
    date: 2007-05-20
    body: "Working on it, for KDE 3."
    author: "Bille"
  - subject: "Re: Kopete"
    date: 2007-05-21
    body: "http://konversation.kde.org/screenshots/konversation10_1.png\n\nE.g. why to we have two windows here? Don't you think it looks ugly?"
    author: "Ben"
  - subject: "Automated dialog screenshots"
    date: 2007-05-20
    body: "I wonder if the review process could be made a bit more efficient with some simple tools. Is it for example possible to automatically open configuration dialogs in a program? Since I believe it's possible to regenerate the screenshots that are used in KDE documentation, one could also write a script that makes screenshots of configuration dialogs at a number of different resolutions, font sizes and colour schemes. Most common problems could easily be detected by just looking at the screenshots.\n\nI know that some other problems with dialogs can't be seen from a screenshot, but such a tool would at least provide an efficient way to find some common problems."
    author: "Jonathan Brugge"
  - subject: "Re: Automated dialog screenshots"
    date: 2007-05-20
    body: "Maybe a modified special version of KSnapshot, where you take the shots of the dialogues and then say, fill in Kbugbusterstyle some remarks and submit it to a hig mailing list or upload your report to bugzilla or whatever.\n\nthe problem currently is that you watch a problem, you make a shot, then you invoke bugzilla, then you type down, then you attach the screeshot. this could be made easier with a hacked version of Ksnapshot for testers. If you watch something suspicious press \"Print\" and submit.\n "
    author: "Andre"
---
The <a href="http://dot.kde.org/1178743323/">HIG Hunting Season</a> for KDE 4 continues. This week we focus is on the written word with a new <a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Text_and_Fonts">checklist on text and fonts</a>. Are you impatiently waiting for KDE 4? Would you like to help KDE make this release a full success? Then get involved! Read on for more details.

<!--break-->
<h2>Spelling</h2>
<p>"If you find typing mitsakes, then you can keep them", many email signatures say. For a software release such as KDE, it is far more important to have correct and consistent spelling. And finding spelling or capitalization mistakes early is helpful for translators. Otherwise the translation will need to be submitted again whenever a typo is fixed.</p>

<h2>Font Settings</h2>
<p>Some people use KDE with two large screens, others with a single, small laptop screen. Some users do not mind reading extremely tiny text, while most partially sighted users need huge font sizes (up to 25 or 30 points) to see the screen content at all. All of these people are important to us. For this reason the font size in KDE configurable.</p>

<p>Your participation in the HIG Hunting Season helps ensure that all KDE applications respect these settings.</p>

<p>The procedure for reviewers is pretty simple:</p>
<ol>
<li>You pick an application which is included in <a href="http://dot.kde.org/1178891375/">KDE 4 Alpha</a>, but not yet reviewed by another person. <a href="http://wiki.kde.org/tiki-index.php?page=hig_hunting_season">See this wiki page for reference</a>.</li>
<li>You open a checklist - today's focus is Configuration Dialogs - and go through the checklist items.</li>
<li>For each infringement you find, post a bug. In the title, write "HIG" and the number of the checklist item which is not met (e.g. CL2/1.1).</li>
<li>On the wiki page of the checklist, add a section for the application you reviewed and link all bug reports you have created.</li>
</ol>

<p>And while you are at it, you could also have a look at <a href="http://dot.kde.org/1178743323/">last week's checklist</a>. Many applications have not been checked yet. Happy Hunting!</p>

