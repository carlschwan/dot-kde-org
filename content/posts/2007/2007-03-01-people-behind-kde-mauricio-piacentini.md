---
title: "People Behind KDE: Mauricio Piacentini"
date:    2007-03-01
authors:
  - "dallen"
slug:    people-behind-kde-mauricio-piacentini
comments:
  - subject: "rocking!"
    date: 2007-03-01
    body: "I really enjoyed your people behind article, Mauricio. Keep rocking! And the same goes for the team behind behind.\n\nI did know that Free Software is rocking in Brazil, but didn't know that it's that KDE-centric. Really nice to read!"
    author: "Sebastian K\u00fcgler"
  - subject: "He rocks"
    date: 2007-03-01
    body: "The personality of Mauricio fills you with enthusiasm... After i met him at Akademy, i wanted to involve myself in KDEgames... He rocks."
    author: "Johann Ollivier Lapeyre"
  - subject: "none"
    date: 2007-03-01
    body: "digg this story!  ok, just kidding"
    author: "mvd"
  - subject: "you guys are great"
    date: 2007-03-04
    body: "a year ago I had never heard of linux and now I'm a dyed-in-the-wool convert, and I love KDE. keep up the great work; i have high hopes for kde4!\n\nzcp"
    author: "zach powers"
---
For the next interview in the fortnightly <a href="http://behindkde.org/">People Behind KDE</a> series we switch continents, travelling to Brazil to meet a cool-headed draftee with a 'cooler' pet, someone who keeps it in the family and who loves to both play <i>and</i> create games - tonight's star of People Behind KDE is <a href="http://behindkde.org/people/piacentini/">Mauricio Piacentini</a>.
<!--break-->
