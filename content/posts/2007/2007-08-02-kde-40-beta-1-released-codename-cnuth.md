---
title: "KDE 4.0 Beta 1 Released, Codename \"Cnuth\""
date:    2007-08-02
authors:
  - "sk\u00fcgler"
slug:    kde-40-beta-1-released-codename-cnuth
comments:
  - subject: "Sounds like 'snut'"
    date: 2007-08-02
    body: "In danish 'Snut' and means sweetie.. \n\nDamn polar bears"
    author: "Allan Sandfeld"
  - subject: "Gentoo ebuilds"
    date: 2007-08-02
    body: "For all other Gentoo'ers out here, there are SVN ebuilds avaible with which you can get the most recent version, and if you pull them now, you'll get the beta (Please bear with the overhear. If there's at least one visitor who didn't know, the last sub-sentence was useful :) )\n\nhttp://overlays.gentoo.org/proj/kde/wiki\n\nBest wishes, \nArne, now waiting anxiously for the KDE4 beta to finish compiling. "
    author: "Arne Babenhauserheide"
  - subject: "Re: Gentoo ebuilds"
    date: 2007-08-02
    body: "One thing to bear in mind if you intend to keep using your KDE 3 install is to follow the advice here\n\nhttp://overlays.gentoo.org/proj/kde/wiki/CommonProblems\n\nimmediately AFTER building kdelibs. Otherwise, all your menus will go bye-bye when you install the rest of the ebuilds."
    author: "Addicted to Gentoo"
  - subject: "Re: Gentoo ebuilds"
    date: 2007-08-02
    body: "Just too bad it's so sadly configured that you need to mess around with multiple package.masks, now that it's on Beta1 (+Alpha3 apps, heh) maybe M~ or even ~arch (ok, maybe not) ebuilds would make it simpler for people to start breaking their systems... :-)\n\nIt's way too simple to just cp -a the system to other partition anyway. ;-)"
    author: "anon"
  - subject: "Re: Gentoo ebuilds"
    date: 2007-08-02
    body: "I was updating my Gentoo KDE4 svn with this ebuild (last build, 1 mounth ago).\nkdelibs compile fine, but kdebase complains that no libsolid.so.5 is installed.\n\nIn fact I noticied that kdelibs install \"libsolid.so.4\" !!! Where is the error?"
    author: "Minkiux"
  - subject: "Re: Gentoo ebuilds"
    date: 2007-08-02
    body: "Will this replace my current KDE or install it into a new slot?"
    author: "Dire Dicker"
  - subject: "Re: Gentoo ebuilds"
    date: 2007-08-03
    body: "Install in a new slot"
    author: "Minkiux"
  - subject: "I'll stick with daily snv builds"
    date: 2007-08-02
    body: "My installation of this version is not much better than most of my daily builds. I'll keep on using SVN for a while. :)\n\nIt's nice that KDE4 is getting usable though. I removed KDE 3.x a while ago and I can live with the current KDE4 quality. Kwin and Konsole (the most important apps) are working fine and I can use non-KDE apps where things get fucked-up.\nFurthermore, Juk (running on Phonon-xine) and Kopete have gone into a pretty good shape the last couple of weeks. The only thing I'm really missing is a working system tray. This seems broken for Kicker and not implemented for Plasma."
    author: "Kurt"
  - subject: "Re: I'll stick with daily snv builds"
    date: 2007-08-02
    body: "there is a system tray applet in playground already, but don't know if it works"
    author: "djouallah mimoune"
  - subject: "Re: I'll stick with daily snv builds"
    date: 2007-08-02
    body: "It doesn't in the build from a few hours ago ;-)"
    author: "jospoortvliet"
  - subject: "Odd press release"
    date: 2007-08-02
    body: "Is marble really the most important architectural feature in this Beta? Where are Plasma, Phonon, NEPOMUK and the other components that are supposed to be the foundation of the new desktop?\nPeople who have been tracking KDE4's development may be up to speed on these issues. But if I were someone who was supposed to be informed by this press release, I would have thought this Beta to be in very poor shape."
    author: "Elad Lahav"
  - subject: "Re: Odd press release"
    date: 2007-08-02
    body: "most of them are not actived by default.you have to enable them to test.but it doesnt mean they doesnt exist.\nand what you read in the official page, I think it was a visual changelog."
    author: "Emil Sedgh"
  - subject: "Re: Odd press release"
    date: 2007-08-02
    body: "they are all activated by default. however, it was decided to focus on the apps that were most ready and stable for this beta release. why? to showcase what is currently at the best working state and to show that app devel is indeed very possible with the current libs."
    author: "Aaron J. Seigo"
  - subject: "Re: Odd press release"
    date: 2007-08-02
    body: "There simply was no really big, major news since Alpha 1 on those. So when I wrote this announcement, I simply choose not to talk about them... They are there, under the hood. Dolphin has progressed in integrating NEPOMUK, but isn't ready yet, nor is it enabled. I did talk about plasma, Phonon did improve, but mostly under-the-hood again - so the most visible things are those that are mentioned (though I might have left some out, of course)."
    author: "jospoortvliet"
  - subject: "Snut"
    date: 2007-08-02
    body: "In swedish \"Snut\" is slang for \"cop\" or \"police officer\"\n\n( This is just so the interests club can take a note )"
    author: "M4d Swede"
  - subject: "Re: Snut"
    date: 2007-08-02
    body: "I love that literal translation:\n\n\"the interests club takes notes\" = \"intresseklubben antecknar\" = \"thanks for the information, but nobody is likely to care\"\n\nWhat would be the corresponding expression in English, I wonder?"
    author: "Martin"
  - subject: "Re: Snut"
    date: 2007-08-03
    body: "Well, to take a crack at it: \"So Long, and Thanks for All the Fish\" is about the most that I can make out of it. :)."
    author: "a.c."
  - subject: "Oh My God"
    date: 2007-08-02
    body: "It's the worst realease, even worst than alpha1...\nLet me say I tried 3 different setups: gentoo overlay, kde4live and then,\nbelieving I choosed some wrong options, again kde4live with different settings\nat boot.\nGentoo ovelay emerged but doesn't even start.ugh.\nAfter downloading kde4live I rechecked if I had downloaded the right\nversion dated 1 aug:\nNothing is stable, I got a crash of the mixer before starting, even konsole is unusable, amarok showed a nice screen but just hitting a button crashed it,\nI tried to change some setting in systemsettings and no joy, can't even change\ndecorations or style.\nThe menu is ridiculous: one submenu for the calc alone, and most submenu have\nless than 4 items inside, in the worst windows style. Kde 3 is much better!\n\nThat's a nightmare to me, I hope you're joking, this is not a beta, and\nyou're not wasting all that time on koffice (without fixing the only part of koffice which is really needed, the file format filter), I won't believe that,\ntell me it's just a nightmare."
    author: "nae"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "Yes, it is. This is a beta, no one promised stable throughout. Some applications are further along, some are crashy. That's life.\n"
    author: "Thiago Macieira"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "Easy dude, beta status does not mean \"mostly works\" as you seem to think. Beta normally means \"this is it, we won't be adding any more features, from now on we focus on making things work\". So beta 1 in fact means the very first release after they did the feature freeze. KDE has always been of the higest quality, I don't expect it to be any different now. Start yelling when they say they are close to release and you still feel this way. Have faith! ;-)"
    author: "Quintesse"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "Actually it's just an API lib freeze, apps are still getting features."
    author: "Patcito"
  - subject: "Re: Oh My God"
    date: 2007-08-03
    body: "> Easy dude, beta status does not mean \"mostly works\" as you seem to think. Beta normally means \"this is it, we won't be adding any more features, from now on we focus on making things work\".\n\nNo, that's an alpha.  It normally goes like this:\n\nAlpha: Architecture finished, everything major is in place, mostly operational.\n\nBeta: Ready for testing by end users.\n\nRelease candidate: Should actually be finished at this point, will be the final release if no major bugs are found.\n\nThere's a growing tendency to play cowboy and just release whatever you feel like in order to meet the deadlines, but that's the usual distinction between alpha/beta/rc that responsible software engineers use.\n"
    author: "Jim"
  - subject: "Re: Oh My God"
    date: 2007-08-03
    body: "Hasn't the Open Source mantra always been: release early, release often? I'm not sure if calling it \"beta\" is the smartest thing to do, and maybe alpha 2 would have been more appropriate, but the name tag doesn't really matter to me. I'm going to install it over the weekend and have a go at it."
    author: "Andr\u00e9"
  - subject: "Re: Oh My God"
    date: 2007-08-03
    body: "i like your definitions. please, release your software using those guidelines and i'm sure you'll feel very satisfied.\n\nour definitions are slightly different (well, not for RC ...). we do want people, particularly developers, testing things with the beta, particularly the libraries. your definitions are made for a different of software development model from another time. kde is a huge and dynamic project developed in an open, cooperative model. this change in development model (which you probably have noticed) results in needed changes in the traditional release models.\n\nwait, wasn't that one of the most basic tenets of open source in the first place?\n\nso yeah, please chill out, relax and think about why it was released. and no, it wasn't so we could listen to whinging. ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Oh My God"
    date: 2007-08-05
    body: "Yes that's a different definition. Now that I know, as end-user, I'll never \ntry to test and bugreport KDE betas anymore and I'll always wait to..\n(gamma ?) RC as you teach me.\n\nSorry for whinging, now that I know I'll never waste mine and your time anymore, and I'll warn other end-user too that kde betas are not for \nbugreports of users but only for bugreports of developers."
    author: "nae"
  - subject: "Re: Oh My God"
    date: 2007-08-07
    body: "You should be sorry for whinging...\nInfact you should stop it completely.\n\nWhich part of \"we do want people, particularly developers, testing things with the beta, particularly the libraries.\" didn't you get?\n\nOf course they want other experienced developers to test their code at this stage.\nHow on earth would you test new a new library as an end user who presumably only know how to operate things though a GUI, when there are no GUI's for libraries (in far the most cases)?\nAs a developer you'd like -particularly- other developers to test your library by using calls or functions in it.. which makes it hard for end users if they don't know how to write code which uses the library...\n\nThe corner stones and most fundamental things like the libraries, has to be _very_ functional before you can even think about building an application, hence GUI, on top which people like you can fiddle around with and deliver your optional bug reports.\nAaron even said they want people to test it and give feedback.\n\nBut my guess is that the devs will get more bug reports on crashes in the early GUI, from end users, which aren't the focus point and probably are just there as a placeholder for some other GUI. The more fundamental things like libraries are probably more important at this stage in development as I see it.\n\nTherefore you really need to relax and take a deep breath before crying out about crashes or missing stuff in the graphical user interface (referring to the Great Great Great Grandparent Post here).\n\n...And chill.. no one said they didn't want your end user bug reports? a developer bug report is maybe just a bit more specific on the exact problem at this point?"
    author: "Larpon"
  - subject: "Re: Oh My God"
    date: 2007-08-04
    body: "Ok, that's how, for example, Microsoft works. This is opensource, this is KDE, so this works differently"
    author: "Vide"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "No it's not a nightmare.\nSo throw in the towel cause kde4 ain't shit \n or better yet, take that towel and hang yourself with it."
    author: "Bob"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "Well, the things you are looking at, like Amarok and the desktop or the almost-gone kicker, simply are so much in flux it's not funny. For me, Konsole works fine, though it's also under heavy development. Plasma is mostly nice to look at, but unusable for anything real. Okular is fine, though PDF's don't work here... etc. Some apps work fine (all games and edu, most graphics & network, dolphin...), others are way too much in development.\n\nSee all this in a positive light - the more unstable the app, the more in development it is ;-)"
    author: "jospoortvliet"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "> For me, Konsole works fine, though it's also under heavy development\n\nKonsole is now feature complete and should be stable and usable enough for day-to-day work, with the notable exception of missing input method support, which I will try to do in time for the next beta.\n\nIf it is crashing or doing something else wrong, please file a bug report.\n\n"
    author: "Robert Knight"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "... and the winner of the Most Hilariously Melodramatic and Incoherent post goes to ..."
    author: "Anon"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "I had a similar experience (gentoo overlay not starting, then kde4live)\nan altough I don't think absolutely that kde4_beta1 is ready to be inserted\nin gentoo portage, I've enough faith that beta1 is not the final product\n(yes, a submenu for just 2 painting apps is nasty, makes me do in 2 clicks\nwhat before I did with just one) and there's still time to improve, in those\n2 months and half.\nBetter wait the realease candidates to have a much clear idea of what it will be."
    author: "mat"
  - subject: "Re: Oh My God"
    date: 2007-08-02
    body: "This reminds me of the first beta of KDE 2.0 that I tried. It was so horrendously slow and crashy that I thought it would never become usable. However, shortly thereafter, 2.0 was released and it was much faster, much more stable, and much prettier.\n\nPatience! :)\n\n\n\n"
    author: "Apollo Creed"
  - subject: "Re: Oh My God"
    date: 2007-08-03
    body: "The problem is that then there was approximately twice as much time between the beta1 and the release as now."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Oh My God"
    date: 2007-08-03
    body: "Honestly, I expect the release date to get postponed if it should turn out that the new version is not ready for release. This has happened before."
    author: "reading the dot at work"
  - subject: "Re: Oh My God"
    date: 2007-08-08
    body: "Well, I'm pretty sure this will run on 64-bit at the moment, though I haven't tested it with the 64-bit that KDE 2's betas wouldn't run on, alpha. (Actually it took until about beta5 for it to work on Alphas.)\n\nFun little crash there, I and a friend could fix it some, but either the WM would work and nothing else, or the other way around. Finally someone else much more familiar with alphas fixed it.\n\nBut that's the way of betas, getting things like that figured out and fixed before .0-rc or .0"
    author: "James L"
  - subject: "Re: Oh My God"
    date: 2007-08-03
    body: "Flamebait!\n\nGnome troll"
    author: "CeVO"
  - subject: "Re: Oh My God"
    date: 2007-08-04
    body: "I blame google for making people think that \"beta\" = \"ready for normal use\". \n...heck, has any google project ever come out of beta?\n\nalso: it's the framework that's beta. most of the apps are still alpha, I think.\n\nwhy am I feeding the trolls?"
    author: "Chani"
  - subject: "Sounds like \"Knut\""
    date: 2007-08-02
    body: "Sounds like \"Knut\", the little polar bear of the Berlin Zoo.\n\nhttp://en.wikipedia.org/wiki/Knut_(polar_bear)"
    author: "Martin Bober"
  - subject: "Codename"
    date: 2007-08-02
    body: "Am I the only one to nerdy to realise the code name is a reference to Donald Knuth?"
    author: "Keegan"
  - subject: "Re: Codename"
    date: 2007-08-02
    body: "Blast, beat me to it. That's what happens when I start to reply, then get caught up in work."
    author: "Tryke"
  - subject: "Re: Codename"
    date: 2007-08-02
    body: "Yes, yes you are :)\n\nDamn polar bears"
    author: "Carewolf"
  - subject: "Re: Codename"
    date: 2007-08-03
    body: "> Am I the only one to nerdy to realise the code name is a reference to Donald Knuth?\n\nI did, too.\n\nActually, I'm very surprised others didn't recognize the fine joke on Knuth and the traditional K* KDE naming scheme. I find it not only funny, but also touching. Kudos to the guy who came with the idea.\n"
    author: "Anonymous"
  - subject: "Re: Codename"
    date: 2007-08-05
    body: "I think only some ppl got the joke, eg those who where involved in the huge 'goto is or is not evil' discussion on KDE-DEVEL ;-)"
    author: "jospoortvliet"
  - subject: "its only a beta"
    date: 2007-08-02
    body: "Patientia est virtus"
    author: "Jesus R. Acosta"
  - subject: "Re: its only a beta"
    date: 2007-08-03
    body: "Patientia virtus est"
    author: "Youssef"
  - subject: "Re: its only a beta"
    date: 2007-08-03
    body: "Sickos are virtually at eastern standard time?"
    author: "reihal"
  - subject: "Re: its only a beta"
    date: 2007-08-03
    body: "Google said:\n\n\"Patientia virtus est\" :: 36 Results.\n\"Patientia est virtus\" :: 661 Results.\n\nGoogle gives me the reason ;)"
    author: "Jesus R. Acosta"
  - subject: "Sounds like \"Knuth\""
    date: 2007-08-02
    body: "I guess we ran out of 'K's and had none to spare?\n\nDonald Knuth is the guy that wrote \"The Art of Computer Programming\".\nhttp://en.wikipedia.org/wiki/Donald_Knuth"
    author: "Tryke"
  - subject: "Re: Sounds like \"Knuth\""
    date: 2007-08-07
    body: "I'm not sure whether releasing a version with so many bugs to shake out is the nicest way to pay a tribute to someone who built a reputation by releasing bugless versions of TeX..."
    author: "Veig"
  - subject: "kcontrol, systemsetting, raptor, lancelot !!!!"
    date: 2007-08-02
    body: "of course i can get the answer just after less then a month, the time for the next beta, but as i am impatient, i wanted it now ;-)\nwho will  be shipped in kdebase, kcontrol or systemsetting !!! and i thought that  raptor is the official application launcher, so what's about lancelot and kickoff  and one more thing; did you know the name of ( to be coded) replacement of kicker !!!\n\nthanks \n\n     "
    author: "djouallah mimoune"
  - subject: "Re: kcontrol, systemsetting, raptor, lancelot !!!!"
    date: 2007-08-04
    body: "The Kcontrol thing can be answered: no Kcontrol, so its gonna be systemsettings. Raptor, Lancelot an Kickoff: it's not decided. We'll see which one will be best when KDE 4.0 releases ;-)"
    author: "jospoortvliet"
  - subject: "Re: kcontrol, systemsetting, raptor, lancelot !!!!"
    date: 2007-08-07
    body: "Damn, I knew I shouldn't commit Lancelot to the svn... now everybody expects a kmenu/raptor/kickoff type of thing... as I've already mentioned in another .kde post, Lancelot is not planned to be a kmenu replacement - just a testbed for no-click interface for launching apps (and only for that)"
    author: "Ivan Cukic"
  - subject: "Improved font rendering?"
    date: 2007-08-02
    body: "While the press release talks about improved font rendering, the corresponding screenshot didn't impress much: the \"blackness\" of characterier is irregular: the 'y' in Kylie looks darker than the other character.. :-(\n\nI hope that's because the screenshot was here to demonstrate the 'flakes' and that the font rendering was correctly set.."
    author: "renoX"
  - subject: "Re: Improved font rendering?"
    date: 2007-08-02
    body: "was correctly set --> wasn't correctly set."
    author: "renoX"
  - subject: "Re: Improved font rendering?"
    date: 2007-08-02
    body: "I think what the press release intended to say was better text layouting.  The actual rendering of individual characters is still problematic as you can see (that is, they do not look sharp as they should).\n\nThe layouting is hugely improved over KWord 1.x with characters not jumping around all over the place any more.  In fairness this was always a slightly system and font specific issue with some fonts working pretty well and others producing very strange results.  \n\nFor the fonts I have tested, which includes the selection of free fonts that comes with Kubuntu, Nimbus and Gentium (my personal preference) and of course Microsoft's standards (Times New Roman, Arial etc.) the layouting of characters seems to behave much more consistently.   "
    author: "Robert Knight"
  - subject: "Re: Improved font rendering?"
    date: 2007-08-03
    body: "To be honest, only the text in the last screenshot looks a little strange. The fonts for the dialogs everywhere look very good...\n\nWe'll see when screenshots of KWord 2 arrive...."
    author: "boemer"
  - subject: "Don't forget : SVN + Cmake"
    date: 2007-08-02
    body: "KDE did a complete Switch from CVS to SVN ---- and from autotools to Cmake from kde 3 to kde 4\n\nthis was major work !!!"
    author: "chhchhchh"
  - subject: "Re: Don't forget : SVN + Cmake"
    date: 2007-08-02
    body: "and DCOP to DBUS, and QT3 to QT4...\n\n"
    author: "Emil Sedgh"
  - subject: "Re: Don't forget : SVN + Cmake"
    date: 2007-08-03
    body: "Well, they switched to SVN a while ago.  I think it was sometime during 3.5, but it might have been earlier."
    author: "Matt"
  - subject: "oh no :("
    date: 2007-08-02
    body: "According to the screenshot they moved Konsole tabs to the top like GNOME.  What next, switching Yes, Ok, Cancel buttons?  :("
    author: "Anonymous"
  - subject: "Re: oh no :("
    date: 2007-08-02
    body: "> According to the screenshot they moved Konsole tabs to the top like GNOME.\n\nThat is actually an old screenshot.  The tabs are currently on the bottom as per KDE 3.5\n\nFor users who prefer to have the tab bar at the top, or prefer not to have a tab bar at all there is an option for that as well."
    author: "Robert Knight"
  - subject: "Re: oh no :("
    date: 2007-08-03
    body: "Thanks for clarifying!!  It's looking great otherwise... especially search highlighting. :D"
    author: "Anonymous"
  - subject: "Re: oh no :("
    date: 2007-08-03
    body: "don't know if the button to create tabs in konsole is already there again (left corner on the bottom) (wasn't able to follow svn for a time).. but i really hope that it'll be there again... i really liked it (kde 3.5)"
    author: "LordBernhard"
  - subject: "Re: oh no :("
    date: 2007-08-02
    body: "God, you people complain about the lamest things, and your slippery slope scenarios are hilariously dumb.  \"I tripped over my cat this morning.  What's next - WORLD WAR 3?!\".\n\nAnyway, Konsole from SVN today had tabs along the bottom, just as it has had for the last few months.  So I guess your apocalyptic \"KDE is turning into teh GNOME!1\" scenario will be delayed for at least a couple of weeks longer.\n\nBy the way - I would like to formally coin \"Anon's Law\", as of this moment:\n\n\"In any discussion of the progress of KDE, the probability of an (apparently unflattering) comparison to GNOME approaches 1\".\n\n\n\n\"You know who else had tabs on the top of their terminal emulator, don't you? That's right!\" \n\n\n "
    author: "Anon"
  - subject: "Re: oh no :("
    date: 2007-08-02
    body: "hmmm, Godwin's law but gnome in lieu of nazi, good idea to begin a flame war,            i am just amazed at the idea if this discussion was on slashdot ;-)\n\ni just like you guys. "
    author: "djouallah mimoune"
  - subject: "Re: oh no :("
    date: 2007-08-02
    body: "The button order can already change depending on the theme ;) for example the cleanlooks qt theme rearranges the standard buttons"
    author: "Anonymous"
  - subject: "Re: oh no :("
    date: 2007-08-02
    body: "You can move them... at least, in KDE 3 you can, and I doubt you wouldn't be able to in 4. Just right-click on the tab bar."
    author: "LiquidFire"
  - subject: "Nice work"
    date: 2007-08-02
    body: "My only words are\n\nthanks, thanks and thanks "
    author: "vincent"
  - subject: "Re: Nice work"
    date: 2007-08-02
    body: "Yep I have to echo those sentiments. Thanks guys for all the hard work, I'll be trying out Beta1 as soon as possible. Cheers ;)"
    author: "bigtomrodney"
  - subject: "Re: Nice work"
    date: 2007-08-03
    body: "++\n\nI haven't tried the beta and I'm not even sure I will, but I really look forward to kde4, with all the cool new stuff it will have it can only rock!"
    author: "Joergen Ramskov"
  - subject: "This isn't perfect right this second..."
    date: 2007-08-03
    body: "...because this software isn't perfect right this second, and because it isn't everything I expected, then my life isn't worth living.  Even though this is free software, and I haven't contributed anything, I am entitled to demand that the KDE team cater solely to my desires.\n\nHow dare they not create the perfect (in my opinion) OS over-night?\n\nIn fact, the KDE fairy should have created packages and installed them on my computer for me.  I can't believe they expect me to get packages from my distro, or compile from source!\n\nI'm going to go complain on Myspace at how betrayed and disappointed I am.  I'm not sure I'll ever recover from this set back."
    author: "T. J. Brumfield"
  - subject: "Re: This isn't perfect right this second..."
    date: 2007-08-03
    body: "Nice fake :)\n(I mean, I really like it, it's hilarious hehehe)"
    author: "Vide"
  - subject: "Kubuntu packages"
    date: 2007-08-03
    body: "Errr, I think the link is premature, cause\n1) the story isn't officialy out on Kubuntu blog\n2) the packages in backports are simply not there...\n\nI think we should wait some more days before we can update to beta1"
    author: "Vide"
  - subject: "Re: Kubuntu packages"
    date: 2007-08-03
    body: "yeah, that bug me too!\n\nhttp://www.kde.org/info/3.92.php clearly states:  \nCurrently pre-compiled packages are available for:\n    * Kubuntu\n    * openSUSE\n    * Mandriva\n\nBut you're stuck with the alpha packages (at least for kubuntu).\nHow about announcing binary packages when they're ready?\n\nbeeing just a little impatient..."
    author: "anonymouse"
  - subject: "Re: Kubuntu packages"
    date: 2007-08-03
    body: "Beta packages are available for Kubuntu Gutsy.  Unfortunately the feisty-backports are being notably slow in the build daemons, but they really should be available this European evening.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Kubuntu packages"
    date: 2007-08-04
    body: "Thanks Jonathan! apt-getting right now on Feisty! :)"
    author: "Vide"
  - subject: "Koffice on win, mac"
    date: 2007-08-03
    body: "\"Due to Qt4, KOffice will have a native look and feel on all platforms\"\n\nYes, great!"
    author: "Acke"
  - subject: "Re: Koffice on win, mac"
    date: 2007-08-03
    body: "Haha, I knew that would please some people. :-)"
    author: "Inge Wallin"
  - subject: "quote"
    date: 2007-08-03
    body: "\"After recently having started over with coding of the Oxygen style, things are progressing rather well. The old code of the style was ditched mainly due to negative feedback from code tested and reviewed at Akademy. Thomas L\u00fcbking, the original developer then (quite understandably in my opinion) got up and left. Unfortunately, many of the Akademy reviewers were reluctant to actually help in the effort of improving the code, and so the style was essentially dead in the water.\"\n\nOxygen - first the vision, then the excitement, then someone actually implements something and when the beta stage of KDE4 is reached someone admits a core component was \"essentially dead in the water\".\n\nMaturity by curiosity does not work. Release early, release often. Make evolutionary changes.\n\nA wonder if the same applies to the new plasma kicker. Finally someone will admit that there is no revolutionary concept and reveal a suse++ version.\n\nAh, didn't I read it: \"..., and the Kickoff menu start to be ported to KDE 4.\"\n\nAy-he. "
    author: "bert"
  - subject: "Re: quote"
    date: 2007-08-03
    body: "Kickoff is not the official KMenu replacement, Raptor is (look for more info on it on Techbase). Since Oxygen had visible results for a long time calling it \"dead in the water\" is kind of a far-fetched statement but the quality of the underlying code seems to have been problematic as far as I understand it and warranted a rewrite to ensure it's maintainable in the future."
    author: "Erunno"
  - subject: "Re: quote"
    date: 2007-08-04
    body: "Raptor isn't there yet, and I wonder if that one will be the final one. I'd say nothing is set in stone, and if Kickoff works better, it'll be the default for 4.0."
    author: "jospoortvliet"
  - subject: "Re: quote"
    date: 2007-08-04
    body: "This would be a welcome development. KDE is often attacked for its perceived lack in the UI department and I'm still confused to a certain degree why Kickoff was met with a lot of hostility although Novell really tried hard to back up their design decisions with publicly available usability studies.\n\nAfter all, it would seem more logical to me to use a scientifically investigated menu and build upon it in future releases then introducing a totally unproven and untested application like Raptor is at the moment. I still have gripes due to the decision to develop Raptor in secrecy."
    author: "Erunno"
  - subject: "Re: quote"
    date: 2007-08-05
    body: "To be honest, I agree. I personally prefer the current KDE menu over Kickoff, but also over raptor. And it just seems Kickoff is developed more in the open, and is really usability-tested."
    author: "jospoortvliet"
  - subject: "Some have said it above but I like to repeat"
    date: 2007-08-03
    body: "Can we cut the doom & gloom crap now?\nI understood that this beta was released because kdelibs API are mostly frozen and because apps dev need to see where things are going. This is not for end users to have fun with it as far as I understand and has never been advertised as such.\nKDE is getting a nearly full overhaul and this is obviously quite a bit of work, the less the devs need is people whining and complaining.\nWait for final release before complaining : this is not libquicktime or mplayer where a beta can be enough to get the job done, and that is not really surprising.\nAs for undelivered promises, even if they do occur, there's no one to blame : having great visions for a new version of a desktop is normal and the contrary would be weird to say the least. But it may happen that the vision is a bit too big for the decided time frame, so you have to fall back to Plan B for the time being. This does not mean that the whole vision is flawed, just that it will need a bit more time to mature. So what?\nI'm really absolutely amazed by many comments in this news : watch your manners, please."
    author: "Richard Van Den Boom"
  - subject: "Re: Some have said it above but I like to repeat"
    date: 2007-08-03
    body: "Right.\n\nAnyone not happy with the progress of KDE at this point is free to switch to Windows Vista.  I mean, with their thousands of full-time, paid programmers, MS was able to deliver on all the promises they made, right?  Oh wait, they cut most of the innovative new features out before release.\n\nIt is possible to be constructively critical without being a dick."
    author: "Louis"
  - subject: "Re: Some have said it above but I like to repeat"
    date: 2007-08-04
    body: "Yes, it is possible to be constructively critical, but also to be \nthat way and get \"do-it-yourself-or-turn-around\" answers\n\nIn OSS most \"in\"famous cases are:\n1)gimp and the layer styles/adjustment layers (lots of flame wars there)\n2)mythtv and an option to disable the timeshift features\n-(the funny about this is that devs sometimes say \"it's the gazzillion time\nsomeone ask this, but it wouldn't be anymore a PVR!\" , without ever thinking\nthat if a gazzillion people need that, they'll move to elise or freevo just\nbecause you can't hear....)\n3)koffice and the file format filters, from time to time someone asks why\nanything else is developed but this, when almost anyone has the need of this.\n\nAlso, please be serious, linux folks know that there are alternatives to KDE\nwithout moving to Micros**t, like gnome, xfce,fluxbox...."
    author: "nae"
  - subject: "Re: Some have said it above but I like to repeat"
    date: 2007-08-05
    body: "I dunno about the first, but the third is simply about priorities and interest of developers. Getting the MS import filters perfect would mean a huge effort, and developers rather work on things THEY consider more usefull and fun."
    author: "jospoortvliet"
  - subject: "Re: Some have said it above but I like to repeat"
    date: 2007-08-03
    body: "Yep, a lot of people still seem to have problems grasping the difference between KDE 4.0 and the whole KDE4 lifespan. A lot of features where promised and from past experiences I'm reasonably sure that they will find their way into future feature releases in one way or another."
    author: "Erunno"
  - subject: "Re: Some have said it above but I like to repeat"
    date: 2007-08-03
    body: "I promise I won't start bitching about KDE 4 until 4.1 is out."
    author: "reihal"
  - subject: "UK accessibility exhibition"
    date: 2007-08-03
    body: "http://www.techshare-expo.com/\n\nOrganised by RNIB, (charity for blind), and Abilitynet. Opportunity for KDE4? Just a thought."
    author: "Gerry"
---
The KDE Community is happy to <a href="http://www.kde.org/announcements/announce-4.0-beta1.php">announce the first
Beta release for KDE 4.0</a> is available now. This release marks the beginning of the integration
process which will bring the powerful new technologies included in the
now frozen KDE 4 libraries to the applications. Simultaneously <a href="http://www.koffice.org/">KOffice</a> have released <a href="http://www.koffice.org/announcements/announce-2.0alpha2.php">the second Alpha</a> of KOffice version 2. Highlights are improved text rendering and layout and the new Flake library. Read on for more details.
<!--break-->
<p>Almost two months after the foundations of KDE 4 have been laid with the first Alpha, KDE enters the stage of a full freeze of the library interface. From now on, the applications will focus on integrating the new technology refined during the last months, and the library developers will try to fix all bugs found during this process. No new applications will enter the official KDE modules and usability and accessibility work is an ongoing process. In the following weeks, KDE developers will be able to add features to their applications until the next Beta is released and the application features will be frozen as well.</p>

<p>The <a href="http://www.kde.org/info/3.92.php">info page</a> has the download links, including packages for Kubuntu, Mandriva and openSUSE.</p>






