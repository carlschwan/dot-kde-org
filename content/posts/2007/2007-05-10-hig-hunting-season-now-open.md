---
title: "HIG Hunting Season Now Open"
date:    2007-05-10
authors:
  - "ereitmayr"
slug:    hig-hunting-season-now-open
comments:
  - subject: "KDE's wiki is outdated, use TechBase"
    date: 2007-05-10
    body: "KDE uses an outdated Wiki version that is difficult to navigate and has a lot of un-needed features. Use KDE TechBase to store the information:\n\nhttp://TechBase.KDE.org"
    author: "AC"
  - subject: "Re: KDE's wiki is outdated, use TechBase"
    date: 2007-05-10
    body: "Post it as a HIG breaking bug."
    author: "zonk"
  - subject: "Re: KDE's wiki is outdated, use TechBase"
    date: 2007-05-11
    body: "+5 funny"
    author: "NabLa"
  - subject: "Re: KDE's wiki is outdated, use TechBase"
    date: 2007-05-11
    body: "No, techbase is a collection of approved articles with an editorial process. Don't abuse it for random collaboration. (Aaron, is there a techbase page describing what is supposed to go in to techbase and how the editorial process works? A statement of purpose?)\n\nOn the other hand, I'm all for replacing the KDE wiki with one that is less crufty -- and to do coordination and collaboration work in that."
    author: "Adriaan de Groot"
  - subject: "Re: KDE's wiki is outdated, use TechBase"
    date: 2007-05-11
    body: "> Aaron, is there a techbase page describing what is supposed to go in to\n> techbase and how the editorial process works? A statement of purpose?\n\nthe naming discussion page actually had that. hm. yeah, we should probably put a revised version of that somewhere; good idea, ade =)\n\nthat said, there is a projects area on techbase for projects that need help to collaborate on technical issues. that might be appropriate for this, but only if it is kept in the projects area, is technical and is watched over"
    author: "Aaron Seigo"
  - subject: "Where to download?"
    date: 2007-05-10
    body: "Is there any place I can download KDE 4 binaries for the review? Maybe some LiveCD? I would be glad to check some application, but I don't have time to compile the whole KDE myself - and since this is not even beta I expect problems during compilation..."
    author: "DNx"
  - subject: "Re: Where to download?"
    date: 2007-05-10
    body: "I hope the article about 'getting alpha 1' will be released today, and you will have all the answers you need ;-)"
    author: "superstoned"
  - subject: "Re: Where to download?"
    date: 2007-05-10
    body: "Beineri has conscientiously been putting together LiveCDs.  The most recent one was a couple of weeks ago but I expect he'll do one for the upcoming Alpha 1.\n\nSee his blog at http://www.kdedevelopers.org/node/2785> for details."
    author: "Bille"
  - subject: "Re: Where to download?"
    date: 2007-05-10
    body: "I built KDE 4 from source - both trunk and alpha1 - and it was relatively painless, especially with some help from http://techbase.kde.org/Getting_Started/Build/KDE4_Alpha_1 \n"
    author: "Dima"
  - subject: "Great idea"
    date: 2007-05-10
    body: "I think this is a very good idea to get HIG violations ironed out early. However, one crucial point is missing (at least I am missing it...) how do you go about testing KDE4 applications? It would be much easier if there was a LiveCD to download, a WMware image, an open NX server people could connect to, whatever.\n"
    author: "ac"
  - subject: "Re: Great idea"
    date: 2007-05-10
    body: "today KDE 4 alpha 1 is supposed to be released with an article about 'where to get it'."
    author: "superstoned"
  - subject: "Bugzilla versions"
    date: 2007-05-10
    body: "Applications Authors!  Get out and update your application's version number in bugs.kde.org so that HIG Hunters can make sure they are not reporting against older KDE 4 versions.\n\nI've done Kopete already :)"
    author: "Bille"
  - subject: "Nice"
    date: 2007-05-10
    body: "Are vertical tabs on the list too?\n\nDon't know if these are infringing, but they DO annoy me to no end! \n\nAnd the worst of it is that they show up in all of my favorite applications such as Amarok (left pane) or Konqueror (left 'sidebar') and there is no way to have a horizontal layout!"
    author: "Darkelve"
  - subject: "To clear something up:"
    date: 2007-05-10
    body: "By 'vertical tabs' I'm referring to these -usually narrow- tabs with vertical text in them."
    author: "Darkelve"
  - subject: "Re: To clear something up:"
    date: 2007-05-10
    body: "There are people (like me) that like them very much, and wish that they were used in more places! Though given your opinion, I think in most places there should be an option to switch to other layout..."
    author: "zonk"
  - subject: "Re: To clear something up:"
    date: 2007-05-11
    body: "Sophie book uses vertical text tabs intensively -> see attached screenshot"
    author: "anonymous"
  - subject: "Re: To clear something up:"
    date: 2007-05-11
    body: "Obviously some people (like me) have no trouble reading vertical text (or struggle once and thereafter simply remember the position or the icon of the tab) and really appreciate the improved layout it allows.\n\nOther people (I've seen that complain many times) find it hard to read however, so there should be an alternative solution for them. An option to use horizontal tabs ? A horizontal tooltip ? Write horizontal letters, one above the other ? Text at a 45 degree angle ? Apple-dock-style zooming icons, showing the horizontal text only when zoomed ?"
    author: "Moltonel"
  - subject: "Re: To clear something up:"
    date: 2007-05-10
    body: "I think this is a very good initiative!\n\nI agree about the tabs, and the worst thing about them is that (afaik) you can't use the keyboard to navigate between side tabs.\n\nThis is also the reason I don't use Amarok, you need five clicks with the mouse just to show your collection in the playlist.\n\nI hope those accessibility issues will also be solved."
    author: "kill an app"
  - subject: "Re: To clear something up:"
    date: 2007-05-11
    body: "vertical tab is a nightmare to read (like in Amarok).\n\nLook at Windows Media Player (see attach), and you will see that horizontal tab makes things crystal clear."
    author: "Horizontal tab"
  - subject: "Re: To clear something up:"
    date: 2007-05-11
    body: "Well, it's not really the idea in something like Amarok that you should know what it says (that is simply a hint for the first and second time user to spark the memory) - the position of the tabs is what is important. As their relative position on the program's interface is always the same, your muscle memory will remember for you what the tabs say."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: To clear something up:"
    date: 2007-05-11
    body: "There's a pretty simple reason you need that many clicks with the mouse to make Amarok work like iTunes and XMMS (collection-is-playlist). This is NOT how you are supposed to be using Amarok, so obviously it's not easy :) That is how you're supposed to use JuK, so well... you'll find that more to your liking i suspect.\n\nWe have spent a HUGE amount of time researching the sidebar tab system, active research with the people in #amarok on freenode back around the 1.2 release cycle. We tried just about every option available to us, and the current solution (text shown, tabs taking up an equal amount of height of the sidebar, the colour animation designed to show what will happen if you click somewhere and so on) is the best thing we could come up with.\n\nTo reply to your comment on keyboard navigation - this is not in fact true in all apps. In some it is, sure (report bug on those), but taking your own example of Amarok again you can use the same shortcuts as you would in Kontact (that is, [ctrl] plus number of tab in top-down order) to switch between the browsers."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Checklists"
    date: 2007-05-10
    body: "In my opinion, you need to get rid of those smiley faces in the checklists; they are very confusing. For example:\n\n\":-) Does your application have less than 5 menus?\n :-( Does your application only look good in resolutions above 800x600?\"\n\nRemove the smiley faces and write all the checklist items as positive statements and it will be much easier to read and then scan the application for problems. e.g.\n\n\"The application should:\nhave less than 5 menus\nlook good in resolutions 800x600 and higher\"\n\nBy the way, where are the rest of the HIG documents? Most of them on the wiki are blank. I'm starting to get worried that KDE4 will not have great usability because you're deciding on the HIG at too late a stage. :-("
    author: "Bob"
  - subject: "Re: Checklists"
    date: 2007-05-10
    body: "We have been working on the HIG for three years, so I don't think it is too late a stage. It looks like more than half of the topics on http://wiki.openusability.org/guidelines/index.php/Main_Page are in the review stage or later."
    author: "seele"
  - subject: "Re: Checklists"
    date: 2007-05-10
    body: "I totally agree about the smiley faces. It means you have to read every entry at least twice to work out what it means.\n\nI assume you've found http://wiki.openusability.org/guidelines/index.php/Main_Page. AFAIK, this is the only public HIG documentation. Yes it is unfinished but it's also being worked on all the time. I guess someone on the HIG team can give a better answer on this."
    author: "Matt Williams"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "FYI, I think you have it wrong. The bullet points you give:\n\n\":-) Does your application have less than 5 menus?\n:-( Does your application only look good in resolutions above 800x600?\"\n\nare translated into:\n\n\"The application should:\nhave less than 5 menus\nlook good in all resolutions, not only those 800x600 and higher\"\n\nWhich illustrates why the smileys are confusing!"
    author: "D"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "I changed everything to positive statements.\n\nBetter this way?\n;-)\n"
    author: "Ellen Reitmayr"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "Sad to say, no. I usually run KDE on my TV, thus 720x576, meaning I can't use kcontrol and most configuration at all. if this dude read things right, you need to change it to be all resolutions and not just 800x600 and up. at least make sure that they get a scrollbar in < 800x600, a whole lot of things in kde3 fail on that. and that is more annoying since it completely disables me to change things. (the apply button becomes unavailable, I'd have to tab and hope I press space on the right one.)\n\nAlso, dialog windows (or maybe any window) has a tendency to not wanting to appear outside of the screen. when a window can't be smaller than a given size (has  aminimum  size) it has to be allowed to moved around so you can see the part of it you need to.\n\n\"Re: Checklists\n by D on Thursday 10/May/2007, @15:33\n \n   FYI, I think you have it wrong. The bullet points you give:\n \n \":-) Does your application have less than 5 menus?\n :-( Does your application only look good in resolutions above 800x600?\"\n \n are translated into:\n \n \"The application should:\n have less than 5 menus\n look good in all resolutions, not only those 800x600 and higher\"\n \n Which illustrates why the smileys are confusing!\""
    author: "Jonas Lihnell"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "I run KDE on my 320x240 IBM VGA monitor from 1992. Please update all your configuration dialogs to fit my screen. Thanks."
    author: "Davide Ferrari"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "Funny... yeah update every single thing so this one dude can use a monitor with a lower screen resolution than my cell phone.\n\nGuys, make KDE compile on my Razr so I can use Kopete and have a phone call at the same time as using KMail and KOffice to do all my work.  I really need this. :-)"
    author: "Phil"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "> look good in all resolutions, not only those 800x600 and higher\n\nThat's not really practical. Even making 800x600 work ok is difficult, making all resolutions work is basically impossible. We also don't want to make things worse for the majority for the small number of people who run at such low resolutions.\n"
    author: "Richard Moore"
  - subject: "Re: Checklists"
    date: 2007-05-20
    body: "when kubuntu messes up monitor detection, it'll force you into 640x480. config dialogs *have* to be usable at this resolution - even if it's ugly and a pain in the ass, it needs to be possible, so that I can get out of that awful resolution as fast as I can."
    author: "Chani"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "About too big dialogs - I've had that problem many times with my old monitor.\nQuick fix is to move dialog window to reach controls you need (you can do this even if you don't see title bar by holding ALT and dragging any point of dialog).\n\nStill - it's very frustrating when you have to do that, and I think 800x600 is a good limit.\n\nSorry for my English."
    author: "ajuc"
  - subject: "Re: Checklists"
    date: 2007-05-14
    body: "800x600 is the smallest officially supported resolution for kde.   Sorry.\n\nNote that we are not against making kde work on your TV, but it is not a supported platform.   All we can do is provide scrollbars, which are ugly and hard to use (in this context).   Since this is not a supported platform, you will have to do a lot of work yourself.\n\nHowever my general feeling (and I suspect many people share this) is that 800x600 is as small as you can get and still get any advantage from a general purpose windowing system.   If you need to go smaller adds are you don't want a general purpose system anyway, instead you are looking for a specific purpose that useally doesn't involve much input.   (watch movies, play games, or some such) In any case, you are going to have to run everything full screen anyway, so why not work with applications designed for that environment."
    author: "Henry Miller"
  - subject: "Re: Checklists"
    date: 2007-05-13
    body: "Greetingz,\n\nI have an idea for a possible solution for the\n 'screen size to small' annoyance.\n\nHave Kded (or whatever) check the display size on startup,\n and if its smaller then a set minimum size,\n place the entire desktop inside a scrollable frame.\n\n also, have a option to force the scroll frame at startup.\n\nZarantu \n\nPS.. Drop the smilies, use:\n \"+\" it's good\n \"-\" it's bad\n \"?\" undecided\n "
    author: "Zarantu"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "Should be no big deal negating all the :( Questions.\nWould anyone be mad, if anyone did it? (I would do it, when I get idle within the next days)"
    author: "Malte"
  - subject: "Re: Checklists"
    date: 2007-05-11
    body: "better late than never. and while i'm sure a lot of things will be caught for 4.0, there is aproximatley zero reason it can't be continued in 4.1, 4.2, etc. until it's \"perfect\" (you know, in 4.10 ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Checklists"
    date: 2007-05-13
    body: "Isn't it possible to check automatically or enable runtime talkbacks."
    author: "Benjamin"
  - subject: "IRC channel?"
    date: 2007-05-10
    body: "Do we have an IRC channel for this hunting season where HIG people hang out to answer our questions (say, #hig-hunting)? For small problems, IRC is much easier than email for small issues."
    author: "Matt Williams"
  - subject: "Re: IRC channel?"
    date: 2007-05-11
    body: "Hey,\ni like to use akregator for reading feeds. All but the KDE Dot News Feed shows the main Content of the Article in the Window of akregator. The Feed from KDE Dot News only shows a link to \"Read full\".\nI wanted to say that anyway and now i felt this might be a good thread to post. So, maybe the KDE Dot News people can make the feed so that akregator displays them in its own window (rather then just the link)? Would be very neat.\n\n-- \nThank you\nManuel"
    author: "Manuel"
  - subject: "Re: IRC channel?"
    date: 2007-05-11
    body: "i was given a good tip by someone in a previous thread on here which was equally off topic with regards to the thing it was actually replying to ;)\n\nThe tip was quite simple: right-click on the Dot feed and choose Edit Feed..., then go to the Advanced tab and select Load Website (or however that's written). Works a treat (and also, since the feed is generated pretty slowly, you get the first couple of comments as well ;) )."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: IRC channel?"
    date: 2007-05-11
    body: "Hey  \nthank you, though this is not the best solution it works. \nBut still, wouldn't it make sense to have a feed behave like it should? Or am i missing the point what a feed should behave like !?\n\nCheers\nManuel\n\n"
    author: "Manuel"
  - subject: "Re: IRC channel?"
    date: 2007-05-11
    body: "The problem you have is with the word \"should\", you see ;) There are good reasons why some places decide only to send out title, date/time and url for their entries in feeds - in this case it's because the community is as important as it is... You don't get comments with the feed, however it's made, and this way you do :) One thing that would be interesting, though, would be if Akregator recognised this and acted accordingly - that is, if there is no content field in the feed, the default could be to load the website rather than just show an empty field :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: IRC channel?"
    date: 2007-05-11
    body: "You can ask questions in #openusability.\n\nHowever, speaking for myself I'm very busy with work at the moment, so I only can answer questions in the evening hours or during the weekend. sorry...\n\n\n"
    author: "Ellen Reitmayr"
  - subject: "Stuff like this"
    date: 2007-05-11
    body: "What about confusing stuff like this here (screenshot) in the 3.5.x release?\n\nHow can HIGs be formulated to avoid that?\n\n- avoid shortcuts?\n- mind the poor translator?\n- think?\n- talk with users?\n- ...\n\nI am not sure formal HIG would help. What is really needed is a formal review process where real world persons meditate on each dialogue and find out what's wrong.\n\nAs of the screenshot a simple: A question as 'What does the user want from this module?' would lead to a complete redesign."
    author: "hag"
  - subject: "Can't this be done automatically?"
    date: 2007-05-12
    body: "Why all the manual labour? Extend the EBN to scan for such things. It's more reliable, easier to adjust and can't be interpreted differently. Somehow doing this manually doesn't feel like it's 2007.\n\nhttp://www.englishbreakfastnetwork.org/\n"
    author: "LB"
  - subject: "german button labels too long"
    date: 2007-05-12
    body: "i never use any button labels in toolbars. the labels are always too long !\nlike in Kmail: \"Vorherige Ungelesene Nachricht\" , \"Filter als nicht Spam klassifizieren\" and so on. it belongs the whole KDE TOOLBARS, you can find too long labels everywhere. It doesn't even make any difference, if you place the labels beside or below the Symbol in the toolbars. THEY ARE JUST TOO LONG.\nTranslating rules should follow usability as well.\n\nIt would be also nice and very useful, if the custom toolbars in Kicker can optional show a personal label.\nExample:\n- \"show desktop Applet\" is placed on the kicker. but the icon is very small.\nAdding a label (an applet label, or better custom toolbar that called \"Show Desktop\" beside the icon "
    author: "suse"
  - subject: "Re: german button labels too long"
    date: 2007-05-13
    body: "A matter of translation quality. the translations you quote are horrible."
    author: "Benjamin"
  - subject: "german button labels too long"
    date: 2007-05-12
    body: "i never use any button labels in toolbars. the labels are always too long !\nlike in Kmail: \"Vorherige Ungelesene Nachricht\" , \"Filter als nicht Spam klassifizieren\" and so on. it belongs the whole KDE TOOLBARS, you can find too long labels everywhere. It doesn't even make any difference, if you place the labels beside or below the Symbol in the toolbars. THEY ARE JUST TOO LONG.\nTranslating rules should follow usability as well.\n\nIt would be also nice and very useful, if the custom toolbars in Kicker can optional show a personal label.\nExample:\n- \"the show desktop Applet\" is placed on the kicker. but the icon is very small.\nAdding a label (the applet's label, or better custom toolbar which can optional show a label) called \"Show Desktop\" beside the icon would be usable !\nit is possible in WinXP too, you can try it there !"
    author: "suse"
  - subject: "100 % true!!!"
    date: 2007-05-12
    body: "Remove those persistent labels. pop them up if needed, make them appear \"on demand\" !!!"
    author: "veton"
  - subject: "Re: 100 % true!!!"
    date: 2007-05-14
    body: "that's called a tooltip, and it's there. The problem is that having labels works quite OK in english, but most other languages (esp german) are much longer. So there you get into trouble. We'll see what happens, at least for now the labels ensure the app dev's try to get the toolbar as clean as possible..."
    author: "superstoned"
  - subject: "Scroll Bars"
    date: 2007-05-18
    body: "I'va also had some problems getting to the apply button when the screen are set 640x480. This makes it a bit difficult to set the resolution if you don't know how to edit the X config file. I think it would help a lot of users to add more scroll bars to apps."
    author: "scorpking"
---
In the scope of the KDE 4 Usability Review Cycle that started on May 9, the KDE Human Computer Interaction (HCI) Working Group hereby announces the HIG Hunting Season to be open. Read on for more details.

<!--break-->
<p>The HIG Hunting Season is an experiment to include the community into the search for obvious infringements of the KDE Human Interface Guidelines. As those are not fully finished yet, the HCI working group is currently preparing HIG checklists that help to uncover small potatoes that disturb a seamless use experience, such as inconsistencies among applications, incomplete keyboard access, missing feedback, or overloaded configuration dialogs, toolbars or menus. In short: We are asking the community to report user interface and interaction issues that can be stated like bugs.</p>

<p>Applications can be analysed through these checklists by anyone who is testing KDE 4 - users, technical writers, translators, artists, accessibility evangelists, developers, usability people - anyone! Infringements are reported in the bug tracking system and tagged with the attribute "HIG", so it is easy for developers to search and fix them during the usability review cycle.</p>

<p><b>Today's Checklist: Configuration Dialogs</b>
<br><br>
Today's checklist is about <a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Configuration_Dialogs"><b>Configuration Dialogs</b></a> - we plan to announce more lists every three or four days, for example toolbars, menus, context menus, color settings, keyboard access, feedback dialogs, and more.</p>

<p>The procedure for reviewers is pretty simple:
<ol>
<li>You pick an application which is already in trunk, but not yet reviewed by another person. See <a href="http://wiki.kde.org/tiki-index.php?page=hig_hunting_season">this wiki page</a> for reference.</li>
<li>You open a checklist - today's focus is <a href="http://wiki.openusability.org/guidelines/index.php/Checklist_Configuration_Dialogs">Configuration Dialogs</a> - and go through the checklist items.</li>
<li>For each infringement you find, post a bug. In the title, write "HIG" and the number of the checklist item which is not met (e.g. CL1/1.1).</li>
<li>On the <a href="http://wiki.kde.org/tiki-index.php?page=Checklist+1%3A+Configuration+Dialogs">wiki page of the checklist</a>, add a section for the application you reviewed and link all bugs you have created.</li>
</ol>
</p>

<p>For developers, this will probably mean a lot of work. However, many of the HIG bugs can be fixed easily by referring to the checklist, others will be harder. Regarding configuration dialogs, bugs that concern the dialog navigation or size of dialogs can be fixed easily, problems concerning the categorization or grouping require further analysis. Please write an email to <a href="https://mail.kde.org/mailman/listinfo/kde-usability-devel">kde-usability-devel</a> for HCI support.</p>

<p><i>Note that this is an experiment, and we hope that many members of the community will contribute to make KDE 4 a consistent and compelling user experience!</i></p>


