---
title: "KDE at FOSDEM This Weekend"
date:    2007-02-22
authors:
  - "sk\u00fcgler"
slug:    kde-fosdem-weekend
comments:
  - subject: "Desktop search"
    date: 2007-02-22
    body: "I'd love to know to what degree desktop search is usable in Linux, now. When I was busy looking at this stuff in December 2005, there didn't appear to be a properly working desktop search engine yet.\n\nI'm currently using the Google Desktop on Windows XP, and it's such a shame Google hasn't got a version of it for Linux - even if it wouldn't index anything, I'd still be able to easily search my existing Windows index for all the files I've had since 2004, and all the pages I visited since years ago as well.\n\nI guess I should try it with Wine soon, I hear it's really coming along nicely, but I've never actually tried to get a Windows app working with it. Anyone have experience with Google Desktop on Wine?"
    author: "TMaster"
  - subject: "Re: Desktop search"
    date: 2007-02-23
    body: "Have you ever tried Beagle with the Kerry front-end? Looks like what you want. I haven't tried it out myself yet, though."
    author: "Chris"
  - subject: "Re: Desktop search"
    date: 2007-02-23
    body: "Works fantastic in Kickoff, the new startmenu of opensuse 10.2"
    author: "otherAC"
  - subject: "Re: Desktop search"
    date: 2007-02-25
    body: "I'll see if I can install it on Kubuntu 6.06. The only Desktop Search-like thing I remember using on Linux was something - that kept crashing - called Kat.\n\nThanks!"
    author: "TMaster"
  - subject: "Re: Desktop search"
    date: 2007-02-23
    body: "Who cares about Google search. Kerry is very usable (at least in Suse)."
    author: "blacksheep"
  - subject: "Re: Desktop search"
    date: 2007-02-25
    body: "I'll just say it again:\n\n\"I'd still be able to easily search my existing Windows index for all the files I've had since 2004, and all the pages I visited since years ago as well.\"\n\nI use my index a lot. Using any different desktop search, even on Windows, would mean losing my index, or having to search both indexes with 50% of all desktop queries I'm doing.\n\nBut I welcome you to make sure Kerry is compatible with Google Desktop's index! :-)"
    author: "TMaster"
  - subject: "Re: Desktop search"
    date: 2007-02-24
    body: "Yes, beagle is real crap compared with google desktop search. But strigi also points into the right direction."
    author: "Bert"
  - subject: "Metalink talk at FOSDEM 2007"
    date: 2007-02-23
    body: "Check out the Metalink ( http://www.metalinker.org/ ) lightning talk on Sunday at 11:20. It's an XML list of mirrors and other stuff for downloads. openSUSE uses it, and it will be supported in KDE4's KGet.\n\nhttp://www.fosdem.org/2007/schedule/events/70"
    author: "Ant Bryan"
---
This weekend, the <strong>F</strong>ree and <strong>O</strong>pen <strong>S</strong>ource <strong>D</strong>evelopers <strong>E</strong>uropean <strong>M</strong>eeting will be held at the Université Libre in Brussels, Belgium. <a href="http://www.fosdem.org">FOSDEM</a>, being one of the biggest European Free Software events usually attracts several thousand people each year. The <a href="http://www.fosdem.org/2007/schedule/devroom/kde">schedule for the KDE devroom</a> is now published. Highlights this year include talks about the <a href="http://nepomuk-kde.semanticdesktop.org/">semantic desktop</a>, a workshop on <a href="http://edu.kde.org">educational software</a> and naturally an overview of the status of KDE 4.  For the first time we are running a series of <a href="http://www.fosdem.org/2007/schedule/devroom/crossdesktop">cross desktop talks</a> together with Gnome where you can <a href="http://www.fosdem.org/2007/schedule/events/crossdesktop_strigi_internals">hear how desktop search engine Strigi works</a>.  KDE and friends also make appearances in the <a href="http://www.fosdem.org/2007/schedule/tracks/lightningtalks">Lightening Talks track</a> with topics including CMake, OpenWengo and Amarok.  If you are planning to visit FOSDEM, add your name to the <a href="http://wiki.kde.org/tiki-index.php?page=FOSDEM2007">FOSDEM 2007 Wiki Page</a>.






<!--break-->
