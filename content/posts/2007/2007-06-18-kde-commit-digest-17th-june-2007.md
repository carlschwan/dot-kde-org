---
title: "KDE Commit-Digest for 17th June 2007"
date:    2007-06-18
authors:
  - "dallen"
slug:    kde-commit-digest-17th-june-2007
comments:
  - subject: "\"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "Is a replacement planned for Kooka?\nOtherwise I'm curious how one should scan under KDE 4."
    author: "Evil Eddie"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "all the important bits are in libkscan which is still in kdegraphics. various applications use this already, such as kword and krita. the idea is to scan from the app you will be using the results in.\n\nif someone writes a new app that acts as a shell around libkscan, much as kwrite is around katepart, that could be cool. but we already have scanning available without kooka in both kde3 and 4."
    author: "Aaron J. Seigo"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "I agree, an app which wraps around libkscan would be nice. It's nice to have an app just for scanning, when all you want to do is scan images and save them."
    author: "Kyle Williams"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "then again, i'm not sure why krita wouldn't fit that bill pretty well?"
    author: "Aaron Seigo"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "> then again, i'm not sure why krita wouldn't fit that bill pretty well?\n\nFor the same reason I don't open want to open Digicam to just copy pictures from my camera to /mnt/data-dump/pictures/ I just want to get the job done, not open a fully fledged painting application. Following your logic, KSnapshot can be removed too in favor of krita, but this doesn't make sense to me either.\n\nIMHO, it's newbie friendly to have a simple wizard with (1) scan preview > (2) select bounds, rotate > (3) scan > (4) save as, send by e-mail or post to flickr. Windows XP has a nice type of application for this, and it seams to be used a lot because it's simple."
    author: "Diederik van der Boor"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "for everyone commenting about how badly this is needed, please read the whole thread.\n\nnote that i first say that a little scanner shell would be cool. then i say that we have something good enough for now. put the two together and you get:\n\nit would be nice to have a little scanner shell, but we are ok for now even if we don't."
    author: "Aaron J. Seigo"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-19
    body: "All your required functionalities (and much more) are already\navailable as kipi-plugins (and therefore in gwenview\nshowimg, kphotoalbum, digikam and ksquirrelmail)\n\nAs gwenview will be part of KDE4 maybe an new option\n\n   gwenview --plugins scan\n\nthat start the plugin on gwenview startup ,wrapped in\na scan-images.desktop is all you want ;)\n\nAchim"
    author: "Achim Bohnet"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-19
    body: "~> gwenview\nQGDict::hashAsciiKey: Invalid null key\nKCrash: Application 'gwenview' crashing...\n\n~> gwenview\nlibkscan: WARNING: Trying to copy a not healthy option (no name nor desc)\nlibkscan: WARNING: Trying to copy a not healthy option (no name nor desc)\nKCrash: Application 'gwenview' crashing...\n\nStill needs some work i guess :o)\n\nIf it is reproducable, i'll post a bug report about it"
    author: "whatever noticed"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-22
    body: "For all who are not aware of it:\nI recently added support for scanning into kolourpaint (in KDE3 and KDE4 of it)\nkolourpaint is a nice, compact application which every normal user should be able to deal with."
    author: "Martin Koller"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-29
    body: "It is a nice app, but it isn't a scanning app.  It's a doodling app, and not the first place someone will look for to scan something, and it contains a lot of functionality unneeded by someone who wants to scan something.\n\nDon't get me wrong, I think it's nice that kolourpaint can scan, but it's hardly a replacement for a simple scanning app.  For all the talk of making things easier and simpler for newbies, lumping functionality many people consider unrelated into now larger and more complex apps is precisely what one doesn't want to do."
    author: "MamiyaOtaru"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "Well maybe you don't want to open a large and powerful graphics application just to scan a picture. I would rather open up digiKam, since it has the basic functions you're likely to do on the image before you save it, i.e. rotate, crop, fix colours, but not do complex painting.\n\nOn the topic of scanning, any word on a KDE4 frontend for performing OCR? I hear tesseract-OCR is a fantastic library."
    author: "Schalken"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "Wonderful.  In the same way Windows users open up Photoshop to scan things.  That's the first place I'd look.\n\nLuckily everyone has koffice (with krita) installed instead of OOo and Gimp.  "
    author: "MamiyaOtaru"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-21
    body: "That's *****!\nAlmost nobody uses Koffice cause it's barely compatible with ODF and not\ncompatible at all with MS files (and most of us need to communicate even with\nnon-linux users). Krita sadly isn't still 50% as useful as gimp.\nSo most (90%?) people uses OOo and gimp, and will continue to. \nThese apps are not really mature, and you know. Givin\nscanning functionality only to Koffice users will not force users to use\nkoffice, most of them will just move to other scanning solution,\nor will to others DE if they badly need integration."
    author: "matte"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-23
    body: ">>So most (90%?) people uses OOo and gimp, and will continue to.\n\nYou can also scan images with OOo and Gimp.\n"
    author: "whatever noticed"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-29
    body: "I'd heard there were still a few people around who had no concept of sarcasm.  What a wonderful sighting this has been.. like visiting primitive tribes in the rainforest.  Thank you for this."
    author: "MamiyaOtaru"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-29
    body: "Text messages are not an ideal medium for transporting sarcasm and/or irony.\n\nThere are two reasons why sarcasm messages can fail: the reader and the writer..."
    author: "MK"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: ">then again, i'm not sure why krita wouldn't fit that bill pretty well?\n\nKrita isn't a scanning application, and it is not part of KDE itself.\nNovice users that just want to scan a document will be looking for a scanning application, not for an image editor to do the job.\n\nA simplified digikam could do the job, as it has the necessary abilities to order scanned images and editing features to scale, crop and rotate images and to change the file format.\n\n"
    author: "whatever noticed"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "I'd be very keen to see good scanning functionality built into digikam anyway - not all my photos are taken using a digital camera and it would be nice to be able to scan slides / negs / prints and bring them straight into my digikam workflow."
    author: "Adrian Baugh"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-19
    body: "install kipi-plugins and you'll find in digikam:\n\n    Album -> import -> scan images"
    author: "Achim Bohnet"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "I agree with the others on this one - having a separate scanning application is immensely preferable.  Kooka has served KDE very well for a number of years, and I have found it particarly useful.  Inserting scanning into an application such as Krita makes a simple task rather complicated for a number of users.  This seems especially to go against the grain of the Unix philosophy of providing simple utilities, and also decisions such as to separate off file management into Dolphin.  Please retain Kooka!"
    author: "David Joyce"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "> Please retain Kooka!\n\nI'd go for a alternative with much more friendly UI... (the wizard I described somewhere above). for the remaining, you've complemented the other arguments nicely.. :-)"
    author: "Diederik van der Boor"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "Actually, I don't use Krita to scan images myself. I haven't even tried it -- not even now that I have a scanner again. I actually had forgotten that Krita was supposed to be capable of directly scanning images in. And I'm the maintainer... But I tried Kooka and went to using XSane myself, because it does 16 bits. I'm not sure whether libkscan supports 16 (or rather 12) bits per channel at all.\n\nBut it should be easy enough to write a standalone scan application using libkscan -- you can probably do it yourself, in Python or Ruby or C++.\n\nThere's just one thing I'm certain of: nobody actually working on KDE4 has time to start or restart a dedicated scanning application. Kooka has been seriously unmaintained for ages. So -- if you want a scanning application, if you think it's important that there is a scanning application, then you need to act. Stop pleading. Stop hoping. Follow the instructions on techbase and start a development environment. Start developing. Become a widely-admired hero!"
    author: "Boudewijn"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "> I'm not sure whether libkscan supports\n\n... i'd be surprised if it doesn't as it uses sane, so should follow what xsane can do.\n\n> nobody actually working on KDE4 has time to start or restart\n\nwhich is exactly what i was thinking when suggesting krita is good enough for now. due to having ways to scan there's no big impending doom cloud hanging above if there isn't a little scanner shell in 4.0.\n\n> Kooka has been seriously unmaintained for ages.\n\nit would need a pretty big rethink/rewrite in any case as the UI really needs to be completely rethought. given that most of the logic (except the ocr stuff) is in libkscan, it makes the choice even more obvious.\n\n> Start developing. Become a widely-admired hero!\n\nindeed."
    author: "Aaron J. Seigo"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-18
    body: "<i>it would need a pretty big rethink/rewrite in any case as the UI really needs to be completely rethought.</i>\n\nI'll second that :)"
    author: "forrest"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-19
    body: "<i>I'll second that :)</i>\n\n<p>Could you please elaborate? What do you not like about the current user interface, and what ideas do you have to make it better? It would be great if we could collect some ideas here, so that the soon-to-be-widely-admired hero knows what to do..."
    author: "ac"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-19
    body: "my main problem with the current interface is that you can close the views in the main screen, but cant get them back in the same fashion.\nMost novice users i work with come to me with an empty kooka screen and the only way to restore it to its default layout is by removing ~/.kde/share/config/kookarc.\n"
    author: "whatever noticed"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-19
    body: "more than anything else, keep it simple. it doesn't need docks, it doesn't need galleries, it doesn't need a separate image viewer space ... simplicity. if the user wants all of those things, they can use gweview or krita. preview, crop/select, rotate and save: that's all that's needed.\n\nsecond, do it fast. there were a number of things kooka tried to automate or handle \"smartly\" that made the app a bit slow.\n\nthird, don't try and manage the image results automagically. kooka tried quite bravely to keep automated galleries around. imho the scanner app should treat the scans as documents (think kate, kwrite, kword, etc) and not try and auomatically mange them.\n\nthat's just mho though, and to be fair i've used kooka for years and have got a lot of utility out of it. so it's a good app. but we can probably do even better by learning from it."
    author: "Aaron Seigo"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-29
    body: "OK, I'll happily admit this is better than whining about kooka disappearing.  If a better scanning app comes out of this it's a win."
    author: "MamiyaOtaru"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-19
    body: "We've been over this ground a few times now, haven't we??? :-)\n\n>> I'm not sure whether libkscan supports\n\n>... i'd be surprised if it doesn't as it uses sane, so should follow what \n> xsane can do.\n\nHaving trawled through libkscan in the past, I can say there's a lot of stubs in there where the author intended to support all the features in SANE, but didn't in the end.  It's also very messy.  I'm of the opinion that libkscan needs a full re-write for the post-Solid & Windows/Mac world that finally does support all my scanners features :-)  I have some code that speaks to my scanner OK, but I'm a bit like the artilleryman in War of the Worlds...\n\nEverything else is a +1 from me.  Kooka tries to do too much, we already have great image management apps.  Instead its widely agreed that a beefed up libkscan in kdegraphics and a couple of new smaller apps outside libs is the way to go.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-19
    body: "Aaron & Boudewijn,\n\nStart developing - perhaps this is an invite I can't resist.  With the amount of spare time I have, it will have be a _slow_ process!\n\nI'll try and get in touch with you to see what I can do.  The UI suggestions posted seem very sensible - I've always thought the gallery pane, and that sort of stuff, added undue complexity to the whole scanning process.\n\n - David\n\nps. best leave the hero bit out, I think... :-)\n"
    author: "David Joyce"
  - subject: "Re: \"Kooka and kmrml are removed completely.\""
    date: 2007-06-20
    body: "Sure. Feel free to contact me, either by email or on #koffice."
    author: "Boudewijn Rempt"
  - subject: "Caches"
    date: 2007-06-18
    body: "I hope that KDE will at least create a cache API and make it possible to slip in caches via a quick configure. In particular, I see the icon cache and of course there is the html cache. It would be nice to skip all that, and have something like squid serve the needs. Yes, I know that icon cache is meant for local caching, but it seems that a small generic cache that interacts with squid (or other network cache) would be great for schools, departmental servers, businesses, etc. KDE can use more networking."
    author: "a.c."
  - subject: "Re: Caches"
    date: 2007-06-18
    body: "the point of the icon cache is to speed things up. throwing them on a machine somewhere on the network seems ... counter productive.\n\nsquid helps with web browsing because it caches remote content on a system that is closer to the destination (you). integrating the icon cache with something like squid would be doing exactly the opposite, and therefore have exactly the opposite effect (e.g. slow things down).\n\nadditionally, the sort of on-disk strategies used by web cache apps are not necessarily the greatest for other work loads such as icons due to differences in access patterns. so even if squid was running locally it probably wouldn't be the best idea.\n\ndirect access to a shared local file that is tuned for icon access patterns is going to be hard to beat. and i'm really not sure what benefits a network cache would have.\n\ni can see sharing cache files on a system serving x clients over the network (e.g. thin client setups) could be nice for disk space and disk cache friendliness ... but that's also local (to the app) and not on the network."
    author: "Aaron Seigo"
  - subject: "rounded selction"
    date: 2007-06-18
    body: "Finally dolphin has rounded selected item.\nI thought this features was abounded. "
    author: "terran"
  - subject: "KDE4 porting progress"
    date: 2007-06-18
    body: "Danny, thanks for again delevering on my monday morning habit of reading the commit-digest. Would it be possible to somehow get statistics on de progress of the porting effort to KDE4? It would give a good feeling where we are right now."
    author: "cossidhon"
  - subject: "Re: KDE4 porting progress"
    date: 2007-06-18
    body: "well, porting is done, as is improving the libraries (mostly). They're working on features now."
    author: "superstoned"
  - subject: "Re: KDE4 porting progress"
    date: 2007-06-18
    body: "Well, it would be pretty hard to get definitive statistics on the porting effort, as it is not something that is easily quantified - really, many files may be in different stages of porting, etc. And it is not as if there is some register of ported files :)\n\nI really wouldn't know where to start in getting such a statistic. However, you might be able to form an impression through the Krazy code quality checker at http://www.englishbreakfastnetwork.org/krazy/ (where a reduction in issues over time would somewhat indicate progress in porting).\n\nDanny"
    author: "Danny Allen"
  - subject: "plasma icons"
    date: 2007-06-18
    body: "Hello,\n\nwill those special icon actions also be available from dolphin and konqueror ? or only  on the desktop ?\n\n"
    author: "anon"
  - subject: "Re: plasma icons"
    date: 2007-06-18
    body: "let's hope they won't be plasma-specific ;-)"
    author: "superstoned"
  - subject: "Re: plasma icons"
    date: 2007-06-18
    body: "The quick icon actions yes I do think should be available in Dolphin under icon view.\n\nHowever, other parts of Plasma such as icons having the nice rounded border and the ability to move icons around should probably remain Plasma's expertise.\n\nIt is useful to distinct the use of icons on the desktop (fewer large icons, move them around) from icons in the file manager (large number of icons, must reduce clutter)."
    author: "Schalken"
  - subject: "Re: plasma icons"
    date: 2007-06-18
    body: "I disagree.  I think that an icon should behave exactly the same everywhere - anything else is confusing to users as they have to figure out where the different behaviours apply and why, when they drag an icon from a dolphin window onto the desktop, it begins to behave completely differently."
    author: "Adrian Baugh"
  - subject: "Re: plasma icons"
    date: 2007-06-18
    body: "which is why icons on toolbars behave exactly like icons in sidebars, the file dialog and the file manager? ;)\n\nicons are context sensitive. i agree that things should generally work similarly, i also think (and have done various bits of user testing and observation to base this on) that the \"desktop is just a file manager view\" concept is completely and utterly broken.\n\none of my stated goals is to break this errant connection by 4.1 so we can get on with using the desktop and panels as something that is actually useful in the general case."
    author: "Aaron J. Seigo"
  - subject: "KOrganizer layout"
    date: 2007-06-18
    body: "I like the new layout of KOrganizer a lot :-) (http://commit-digest.org/issues/2007-06-17/files/korganizer_themed_decorations.png) The \"details of selected event\" makes it much friendlier to use too.\n\nNot sure about the background stuff, but well I don't expect it will be mandatory to use a background. :-p"
    author: "Diederik van der Boor"
  - subject: "Re: KOrganizer layout"
    date: 2007-06-18
    body: "Well, the layout in http://commit-digest.org/issues/2007-06-17/files/korganizer_themed_decorations.png is more of a mockup. But the \"details of selected event\" part is available in KOrganizer 3.5 (in fact, it's a quick cut-and-paste).\nThe only difference that may appear in KOrganizer 4 is that the position of the panel may become configurable (left or right), and the to-do view is being changed a little. Here's a screenshot of today's KOrganizer (still a work-in-progress):"
    author: "Lo\u00efc Corbasson"
  - subject: "Re: KOrganizer layout"
    date: 2007-06-18
    body: "What are these not-useful pictures at the bottom of each day?\nAre they the \"new\" features?\n\nKorganizer is a pain in usability temrs, and the solution isn't adding \"cool\" pictures for each day.\n\nPlease, use Google Calendar for a while and understand how simply to use it can be. For example:\n\n- Google Calendar: After selecting a time range INMEDIATELY does appear a ball to create the new task/event.\n- Korganizer: After selecting a time range you must press right buttom for context menu to appear.\n\nHope Korganizer will be improved in real terms."
    author: "I\u00f1aki Baz"
  - subject: "Re: KOrganizer layout"
    date: 2007-06-18
    body: "Hi!\n\nI'm the creator of the not-useful feature you just saw :)\n\nI'm a student working this summer (Summer of Code) to add some theming support to KOrganizer, to make it more appealing to users that think computer-based calendars are too \"unhuman\" and boring, so mainly home users still using paper-based calendars instead of KOrganizer and also companies which may want to customize the calendar layouts for the employees/customers/...\n\nThe \"cool\" pictures are Wikipedia's Pictures of the Day, and while this may seem unuseful, this is commonly found on paper calendars which are very successful, so maybe it's not that unuseful after all?\n\n(You sure have noticed that the goal is <I>not</I> to improve usability with this feature, but attracting new use(r)s.)\n\nNow, concerning the GUI in general, I won't say KOrganizer's GUI is perfect, and I think that your example is good at showing this, but please notice two things:\n- my Summer of Code project is about KOrganizer <I>theming</I>, so I won't be able to revolutionize the GUI in terms of buttons/actions/etc.; I'll work on themes and decorations to allow customization of the calendar views;\n- we lack developers for KDE-PIM (KOrganizer being one of the components of the PIM suite) so of course you would be very welcome to join us make it better: just email kde-pim@kde.org if you'd like to (this would be greatly appreciated!). Changing the GUI requires time = people = new developers, and you'd be very welcome to help us on this!\n\nNow, on the usability front, I personally do not think KOrganizer is a pain, and I think it is comparable to most (ok, not all) other calendar apps out there, free or not. This of course does not mean it could not be improved, and I for sure understand that some apps' functions are easier to use. If you want to help, please feel free to do a usability study or code new ideas, this again would be very appreciated.\n\nAnyway, thanks for the feedback :)\nFeel free to contact me / post on my blog (http://blog.loic.corbasson.fr/) if you'd like to add something on the theming possibilities themselves -- thanks in advance!\n\nLo\u00efc"
    author: "Lo\u00efc Corbasson"
  - subject: "Re: KOrganizer layout"
    date: 2007-06-19
    body: "Ok, I understand the goal of your project.\nJust let me explain that when I saw these images I though there were more important things to fix about Korganizer, most of them about usability, and because I expected to see a new and cool designed Korganizer I just saw the same as now but with some pictures.\n\nJust it, I've nothing against you project, it could be cool, but I hope more improvements for a better calendar app.\n\nAnyway I'll try to compare various calendar program to test Korganizer \"bugs\".\n\nRegards."
    author: "I\u00f1aki Baz"
  - subject: "Re: KOrganizer layout"
    date: 2007-06-20
    body: "> I've nothing against you project\nNo problem, I didn't take it personally :)\n\n> Anyway I'll try to compare various calendar program to test Korganizer \"bugs\".\nThanks for having a look at this -- if you need some info or anything else, feel free to post to kde-pim@kde.org. This is appreciated!\n\nBest regards,\n\nLo\u00efc"
    author: "Lo\u00efc Corbasson"
  - subject: "Re: KOrganizer layout"
    date: 2007-06-19
    body: "Hi.\n\nAbove those Image-of-the-day pics there are two numbers.\n\nThey are supposed to be the number of days in the year before THIS day and after THIS day?\n\nWell the sum is allways 362 + THIS day would be 363, but the year 2007 has 365 days as most years.\n\nThe first number includes THIS day (31+28+31+16=106 for Apr.16), while the second number is 3 days less then the remaining days of the year.\n\nJust wanted to mention it, in case you didn't already realize.\n"
    author: "Harald Henkel"
  - subject: "Re: KOrganizer layout"
    date: 2007-06-20
    body: "Hi,\n\nThanks for the feedback! In fact, the images in the digest are mockups. I didn't notice when I created them that the numbers I added manually did not fit. But in the real KOrganizer, they do :)\n\nThanks!\n\nLo\u00efc"
    author: "Lo\u00efc Corbasson"
  - subject: "Re: KOrganizer layout"
    date: 2007-06-22
    body: "You can also simply start typing after you selected a timerange, and automatically the \"new Event\" dialog appears. Alternatively, you can press the ENTER key after selecting a timerange, which also brings up the dialog.\nSo it's not really complicated."
    author: "Martin Koller"
  - subject: "Plasma icons!"
    date: 2007-06-18
    body: "I would just like to say that Plasma's icons look fantastic!\n\nThe faint white rounded border improves visibility/viewability/readability (?) greatly as icons now have a distinct border and 'area' that is that icon.\n\nAnd the quick buttons that appear in the corners of icons is both ORIGINAL and USEFUL.\n\nWell done Plasma team! :)"
    author: "Schalken"
  - subject: "Re: Plasma icons!"
    date: 2007-06-18
    body: "thanks for kinds words of encouragement; we've got a lot more coming, too, so fasten those seat belts. =)"
    author: "Aaron Seigo"
  - subject: "Re: Plasma icons!"
    date: 2007-06-18
    body: "While they certainly appear very cool and useful, I'm going to reserve judgement on those quick actions until I've seen some less experienced users interacting with them.  Sometimes making something too easy to access (a single click on an area of the icon) is setting up users for a lot of mistakes (performing an action when they just wanted to select).  Just like click to rename in Windows is a disaster for new users, or drag and drop in menus."
    author: "Leo S"
  - subject: "Re: Plasma icons!"
    date: 2007-06-18
    body: "why reserve judgement? test now.\n\ngoddess help us all if we just sit back and throw ui ideas into a pot. there has been some testing of the hover interface concept, and you'll find them increasingly appearing elsewhere in the software world as well.\n\nthat said, the version that was committed to svn was somewhat of a draft and not the version of these things that i had tested on people.\n\nthere's an easy way to fix that, but it doesn't involve reserving things =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Plasma icons!"
    date: 2007-06-19
    body: "Ah, but the best test is on real users.  I fully support putting this into KDE4, and in a few months it will be much clearer whether it is good or not, based on user feedback.  \n\nI'm running a full blown usability study on some of my software right now (20 non-disabled subjects, and 5-10 people with various disabilities), and it's not an experience I necessarily want to get into again without some serious consideration.\n\nWhile it can teach you a lot, it is also extremely difficult to get any sort of generalizable results from usability testing in a lab setting.  Either you control your environment so much that the results are very specific to your task, or you make the task more open and end up with confounds.  It's a delicate balance, and usually you are better off just creating the feature and trying it out on real people.  "
    author: "Leo S"
  - subject: "Re: Plasma icons!"
    date: 2007-06-19
    body: "agreed. here's what i've been doing for the last while:\n\n- the \"coffe house method\". take my laptop to a coffee shop or other relaxed and open environment. invite people to try out application X and accomplish a few tasks. this way real people are using real software in a real environment as they might if the machine was theirs and they were doing some work/play in a coffee shop. the results tend to be pretty accurate and revealing because of this, ime.\n\n- the \"jane goodall method\". i invade my friend's offices and sit behind them and observe them using their current computing system to see what fails them and what works. every so often i'll ask a quick question about what they are doing or why they did it that way. when doing toolbar research i actually managed to get people to log their toolbar icon usage for me. \n\nboth approaches involve real people in real settings and don't take up a lot of there or my time. it's amazing what people will do for you if it is for an ethical, not for profit global project.\n\nit's also a cool way to spread the word about kde, especially in the coffee house approach.\n\ni've also blown a few minds when something fails for the person in the coffee house method and i take the laptop back, type madly for 10 minutes or so then give them the laptop again to try with the changes. very few people have had a chance to see the software development practice up close so it's pretty neat to see the look on their face when you bring them changes for testing based on their experience and feedback from a bit earlier.\n\ni'm not a huge fan of lab based research, partly because it feels so boring but mostly because, as you pointed out, it's both labour intensive and of questional utility."
    author: "Aaron Seigo"
  - subject: "Re: Plasma icons!"
    date: 2007-06-20
    body: "Wow, that post was really useful to see how you do testing of new features, thanks for the info."
    author: "Martin Stubenschrott"
  - subject: "Screenshots?"
    date: 2007-06-18
    body: "\"The Oxygen window decoration and widget are both moved into kdebase\"\n\nIs there any screenshots or video? just to take a look\n\nBye"
    author: "me"
  - subject: "Next Alpha Build?"
    date: 2007-06-18
    body: "Can anyone tell me when there will be a new alpha build for KDE4. I would like to try it out with all the new eyecandy. Will there also be some packages for Kubuntu oder do I need to compile KDE on my own?\n"
    author: "DITC"
  - subject: "Re: Next Alpha Build?"
    date: 2007-06-18
    body: "Beta is planned for 25/06/07."
    author: "makosol"
  - subject: "Re: Next Alpha Build?"
    date: 2007-06-18
    body: "The SUSE KDE 4 packages are updated weekly from SVN. Also there is a new Live CD with snapshot of last week available."
    author: "Anonymous"
  - subject: "Re: Next Alpha Build?"
    date: 2007-06-18
    body: "Where can we grab those updated live-CD ?"
    author: "makosol"
  - subject: "Re: Next Alpha Build?"
    date: 2007-06-18
    body: "http://home.kde.org/~binner/kde-four-live/ - or you could hove googled kde4 live cd ;-)"
    author: "Simon"
  - subject: "Plasmoid packages"
    date: 2007-06-18
    body: "First of all, cheers for the entire KDE4 team: the developments are astounding!\n\nNow, I have a couple of questions concerning the plasmoid packages:\n\na) Have you discussed with distros about the integration with their own packaging solutions?\n\nb) Do plasmoids run in a sandbox of sorts?  I realise that the plasmoid packaging system will probably only cover scripts for which the source code is available (JavaScript, Python, Ruby, etc), but nevertheless, there are security implications if some malicious website lures users into \"download our awesome dancing hippos plasmoid!\"...\n"
    author: "dario"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "Hi\n\n\"a) Have you discussed with distros about the integration with their own packaging solutions?\"\n\nI think KDE has KHotNewStuff(2) to avoid that.these are Distro Independent, User Level stuff.\nfor example a user wants to add Cool plasmoids to his Desktop, should he have Root Password to install the Plasmoids Deb Packages?not really..."
    author: "Emil Sedgh"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "I think you should compare this with Firefox extensions. Users won't and shouldn't look for the 'firefox-webdeveloper' or 'firefox-firebug' extension in their package browser. Or a \"kdelook-wallpaper-N\" package. Each user has different preferences for their extensions, so this is a user-level thing, not system-wide."
    author: "Diederik van der Boor"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "True, but bear in mind that Firefox extensions *are* available as packages in at least some distros (like Debian et al).  It does make some sense to have a system-wide manner of updating them because of bugs, for example.\n\nIn any case, Firefox extensions provide a good reference point from which Plasma developers can design their packaging system.  I am thinking in particular of the problems related to security (will plasmoids be authenticated somehow?).\n"
    author: "dario"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "> but bear in mind that Firefox extensions *are* available as packages\n\njust as kicker applets are and, in the future, plasmoids will be. the concepts are parallel and don't really interact in the way you seem to be implying.\n\n> have a system-wide manner of updating them\n\nfor ones that are installed system-wide, sure. for things that the user installs themself, maybe not so much.\n\nwe do support versioning of the packages, however, so we could offer update notifications.\n\n> Firefox extensions provide a good reference point\n\nwhat would you suggest are the strong points to learn from?\n\nnote that plasmagik packages are already quite flexible (runtime definition of expected package structure), integrate with kde's plugin metadata system and there is a GUI to step one through the creation of a package so you don't have to do it by hand. plasmoid packages themselves may also come in a few different scripting languages.\n\n> will plasmoids be authenticated somehow?\n\nsee my other reply in this thread."
    author: "Aaron J. Seigo"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "> just as kicker applets are and, in the future, plasmoids will be.\n\nExcellent!\n\n> what would you suggest are the strong points to learn from?\n\nI was thinking of a) a trusted repository like the Mozilla Addons site (which Plasma will also have, as you mentioned in the other thread), b) a notification of when updates are available (which again Plasma will also have), and c) a straightforward way of installing/removing plasmoids (which I am sure is also in your plans).  In fact, you seem to have everything already covered!  Moving on...\n\n> we do support versioning of the packages, however,\n> so we could offer update notifications\n\nYeap, that would be very useful, particularly for those plasmoids that the user has privately installed, ie, those that are not system-wide.\n\nBut consider that the update mechanism will have to distinguish between packages that are installed system-wide and those that are privately installed.  Just imagine the following situation:\n\nThere is a \"Foo 1.0\" system-wide plasmoid installed, and the user has a private installation of \"Bar 1.0\" (there are no dependencies between the packages).  Now, a new version 2.0 is released for both Foo and Bar.  The plasmoid updater will of course notify the user that the new versions are available.  In the case of Bar, upgrading is straightforward, but what to do with Foo?  One option would be for the plasmoid updater *not* to upgrade and instead to inform the user that they should use whatever mechanism is available in their distro to perform a system-wide upgrade.  This has the disadvantage that the distro's update for the Foo plasmoid could take a while before it is available from their repositories.  Another option would be for the plasmoid updater to upgrade anyway, installing version 2.0 of Foo as a private installation (which supersedes the system-wide package).  But then you have the question of what happens when the system-wide package is also upgraded: will you end up with two copies of the same package, or will the plasmoid updater be smart enough to remove it when no longer needed?\n"
    author: "dario"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-19
    body: "i've actually thought about the issues you bring up in your last paragraph. for now, i'm punting on it but will revisit it for 4.1. it is a non-trivial problem, but we do have all the pieces we need to create a proper solution so i'm not worried. it's just doing the rather detailed work.\n\nright now i'm concentrating on getting the Big Basics together for 4.0... the cuter stuff, like managing differences between local and system wide installs of plasmagik updatable packages, will come after that"
    author: "Aaron Seigo"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-19
    body: "> it is a non-trivial problem\n\nIndeed.  And it gets all the more non-trivial once you start taking package dependencies into account...  But anyway, you are right that it is perhaps a bit too soon to be worrying too much about it.\n\nBut when that time does come, be sure to have a chat with the Debian guys about this issue; I wouldn't be surprised if they had already encountered it and given it a lot of thought...\n\n"
    author: "dario"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-21
    body: "dependencies are pretty much a non-issue. we're talking about leaf-node add-ons here."
    author: "Aaron J. Seigo"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "I also have security concerns here. Aaron said in an interview that the user would be able to download and run plasmoids with just one click. That would be awesome in an ideal world, but in the real world it actually frightens me a bit!"
    author: "Jack H"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "ghns allows one to authenticate packages using gpg signing and we will control the main repository.\n\ni don't see people freaking out about grabbing rpm's or deb's from central repos for these very reasons.\n\nheck, i didn't see gentoo people freaking out when they didn't even have gpg signed packages. (has that even been fixed yet?)"
    author: "Aaron J. Seigo"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "Concerning the security of plasmoids, having a main KDE repository will go a long way towards allaying security concerns.  As you mentioned, users already install binaries from distro repositories without second thoughts because they explicitly trust their distribution maintainers.  And ditto for Firefox extensions available from the Mozilla site.\n\nI am sure the same will happen with plasmoids coming from a KDE repository: after all, we already trust you guys with our entire desktops!\n\nIn any case, are plasmoids plenipotent?  Even if the code is trusted, it would still make sense to have some degree of sandboxing to protect from potential exploits.\n\nAnd yes, I realise the same argument could be used for any application in the system, but bear in mind that plasmoids will use languages such as Javascript, which are far more brittle.\n"
    author: "dario"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-18
    body: "Sorry Dario, IMO the difference is simply \"executed embedded in a remote page\" or \"executed locally after being downloaded\". Plasmoids AFAIK fall in the 2nd category"
    author: "Vide"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-19
    body: "I agree with you, but that was not the reason why I suggested that some sort of sandbox would be a welcome feature for plasmoids.  Yes, you could make the traditional assumption that \"if the user has willingly downloaded it, then they must accept the consequences\".  However, the fact is that many plasmoids will be built using loosely-typed languages with no compile-time checks (Javascript springs immediately to mind).  This means they will be far more prone to bugs and exploits.  To compound it, many plasmoids are likely to be network aware, and will therefore receive data from outside sources.  That alone is an exploit waiting to happen.  Are you willing to trust the integrity of your system to a Javascript programme running outside a sandbox?  Personally, I would have my doubts.\n\nI know that some might still argue that once you trust an outside programme, it shouldn't matter if it's made in C++, Javascript, or whatever.  But it does matter: due to language and compilation constraints, a C++ programme is far more solid than a Javascript one.  (And for those that still insist that all programming languages are equally error-prone, then you must have been using all the wrong ones!)\n"
    author: "dario"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-19
    body: "Dynamically typed languages are less prone to certain classes of security related bugs than certain statically typed languages.\n\nI'd be far more willing to trust a Javascript program than a C or C++ one.\n\nThe idea that dynamically typed languages are automatically buggier than statically typed languages is absurd.\nIn something like Javascript, if you pass the wrong type, you'll get a well defined runtime error when the receiving code tries to execute a method or access a property that doesn't exist.\n\nIn C, if you pass the wrong type, sure the complier may warn you (in the simplest case, but in a large number of cases you'll never notice because you've probably cast it, or are using void * as a polymorphism mechanism), but it will often still compile, and then at run time rather than throw an error, it'll attempt to execute some random code - which will _probably_ just cause a crash, but which also may expose a security problem.\n\nC++ is a little better on that front, but mostly because there's less cause to cast and use void pointers, not because such things are impossible.\n\nIn practice, passing the wrong type just doesn't happen very often - not anywhere near as often as off-by-one errors, and going off the end of arrays, neither of which are catchable at compile time, even in the strictest of statically typed languages."
    author: "mabinogi"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-19
    body: "> Dynamically typed languages are less prone to certain classes\n> of security related bugs than certain statically typed languages.\n\nI agree.  But you do realise that your \"certain\" qualifiers weaken your entire argument?  You said it yourself: \"less prone to *certain* classes of security bugs\".  Because there are *other* classes of bugs far more common in dynamically typed languages.  Likewise, \"than *certain* statically typed languages\" betrays another weakness: C is hardly a good example of strong, statically typed language. Even C++ is a borderline case!\n\n\n> The idea that dynamically typed languages are automatically\n> buggier than statically typed languages is absurd.\n\nI didn't say \"automatically\".  I said they are far more bug-prone, which is hardly controversial.\n\n\n> In something like Javascript, if you pass the wrong type,\n> you'll get a well defined runtime error\n\nDude, there lies the crux of the matter: I would rather have bugs being caught at compile-time than at runtime!  If your language allows a programme to even start running when there are such gross type inconsistencies, then that language  is prone to being abused by lazy programmers.\n\n\n> In C, if you pass the wrong type, sure the complier may warn you\n\nWell, C is a straw-man, isn't it?  It has such a weak type system that it doesn't deserve to be lumped together with the strong, statically typed languages.\n\nIn any case, having a compiler warn the programmer at compile time is far better than getting runtime errors!\n\n> In practice, passing the wrong type just doesn't happen very often\n\nYes it does.  You can have type inconsistencies even in a C++ programme that compiles with no warnings.  You never realise those type inconsistencies because the compiler is not smart enough to spot them, and the syntax of language is not expressive enough in the first place.  But if you tried the same algorithm in a *really* strongly typed language such as Haskell or Ocaml, you would see just how many type inconsistencies go unchecked in more conventional languages.\n"
    author: "dario"
  - subject: "Re: Plasmoid packages"
    date: 2007-06-19
    body: "\"...we will control the main repository.\"\n\nAlright, good enough for me. I wasn't freaking out, I was just worrying if it would be too easy to download and run malware by accident. :-S"
    author: "Jack H"
  - subject: "Filenames and Thumbnails"
    date: 2007-06-18
    body: "Hi\nWhere will Filenames go, and will they support Thumbnails?\nThanks."
    author: "Emil Sedgh"
  - subject: "Re: Filenames and Thumbnails"
    date: 2007-06-18
    body: "On top of this: I know that Aaron has put a lot of emphasis on making sure all Plasma elements are scaleable (in terms of physical size, that is).  Will icons be resizable, and will the thumbnails adapt to the new size if the user resizes them, even to very large sizes?"
    author: "Anon"
  - subject: "Re: Filenames and Thumbnails"
    date: 2007-06-18
    body: "Hi\nand as I know, SVG is resizable.are the Preview's SVG graphics?\nIf yes, that could be easy, if no, Im thinking about Mime icon, which Thumbnail Preview is placed on Top of it, if we resize Plasmoid, Mime Icon became bigger (and visible) but Thumbnail Preview stays small.\nMaybe Im wrong, but Thumbnail Previews are one of the biggest helps while finding Files between lots if others.\nand...Another question, what happens when you create an Icon in the Desktop? the file copies in the ~/Desktop Folder?do we have suck folder with Plasma??"
    author: "Emil Sedgh"
  - subject: "Re: Filenames and Thumbnails"
    date: 2007-06-18
    body: "> are the Preview's SVG graphics?\n\nno work has yet been done on this. we'll find out when someone does something ;)\n\n> the file copies in the ~/Desktop Folder?\n\nright now you just get a link to the file or resource. we don't actually store copies of the file right now. this will likely become an option in the future, but i really find \"~/Desktop\" to be one of the most clumsy ideas around. why can't i have multiple folders that show? why treat the desktop layer as a dumbed down file manager? etc...\n\n> do we have suck folder with Plasma??\n\ni hope to allow easy access to 0, 1 or many."
    author: "Aaron J. Seigo"
  - subject: "Re: Filenames and Thumbnails"
    date: 2007-06-20
    body: "Apple had a nice idea with 'stacks' in there WWDC Keynotes video.\n\nIt could go like this: if you download something to you're desktop, there would be one stack-icon, with the last thing you downloaded in front. When you move over it it would show in historical order everything that is downloaded. \n\nThis would make the desktop a little less cluthered... And ofcourse this idea could be used in other cases also.\n\n"
    author: "boemer"
  - subject: "Re: Filenames and Thumbnails"
    date: 2007-06-21
    body: "\"stacks\", you mean the thing we discussed and documented openly at the first appeal meeting some 2 years ago?\n\nyes, they beat us to the implementation, but i'm seeing more and more of our openly discussed ideas find perch in industry. which is great.\n\nof course, it jobs&co. decide to start patenting some of this stuff, i'll be the first to show up at his house with my pitchfork and torch.\n\npersonally, i've had enough of their \"we can invent cool stuff\" when really they've invented very, very little combined with their \"we can patent it too!\" position. oh wait, i just described most proprietary software shops!"
    author: "Aaron J. Seigo"
  - subject: "Wow"
    date: 2007-06-18
    body: "The rate of progress is really astounding. I'm sure to an insider it was also fast back in the \"rewrite the library\" days, but now it's much more visible to the rest of us. At this rate, I bet Danny's having a hard time keeping up with it all! Good work all round.\n"
    author: "Ted"
  - subject: "Re: Wow"
    date: 2007-06-18
    body: "Yes, it is really great to see how all small steps\nin the past are accumulating to wonderful effect.\n\nKOrganizer styling isn't maybe the most important thing\nbut undeniably improves personal feeling of application.\nI wonder if it would be possibly to get \"Quote of the Day\"?"
    author: "m."
  - subject: "Re: Wow"
    date: 2007-06-19
    body: "True! Sometimes one tends to forget how much work it must be to plan, program and maintain such a big project like KDE and its applications is. Software Projects tend to fail at some point, but the good projects always evolve. KDE is among them and has a very cood code base as far as I can judge.\n\nThanks to all the involved folks that do all the documentation, developing and translations!!\n\nSpecial thanks to the writers of the techbase tutorials, that make it so easy to get a KDE 4 development environment up and running. This also attracts new contributors and is very important I think!\n\nKeep it up!!\n\n"
    author: "David"
  - subject: "Re: Wow"
    date: 2007-06-20
    body: "Agreed, but I would like to add that I think KDE4 has been progressing fast for a long time, all the work until recently have just been \"under the hood\" and you haven't really been able to see all the work that has been done there. Now the ground work has been done and the developers are able to work on all the nice visible stuff like plasma. "
    author: "Joergen Ramskov"
  - subject: "Oxygen windeco and widget"
    date: 2007-06-18
    body: "Hi, i just updated my kde4 desktop to the lastest svn and i deleted the .kde from my testing account but the default style and windeco is plastic. How can i set oxygen? (kcontrol is not working so please dont answer this).\n\nAnd an other question, last years i read that plasma was ready to support apple dashboad widget, but i cant not load them, is it still planed to support those  widget?"
    author: "Emmanuel Lepage"
  - subject: "Re: Oxygen windeco and widget"
    date: 2007-06-18
    body: "Just open a Konsole and enter\n\nkcmshell style\n\nAnd don't forget to post screenshots ;-)"
    author: "Anonymous"
  - subject: "Re: Oxygen windeco and widget"
    date: 2007-06-19
    body: "Thanks!\n\nHere are the screenshot\n\nAs you can see, the windeco dotn really work and crash after few sec and the style load only for the style windows (it is not the demo, the style is loaded, but not on other windows, that strange)"
    author: "Emmanuel Leoage"
  - subject: "Re: Oxygen windeco and widget"
    date: 2007-06-19
    body: "The screenshot did not upload...\n\nhere it is:\nhttp://img361.imageshack.us/img361/319/capture24iv2.png\n\ni will add that today windows efects dont work"
    author: "Emmanuel Leoage"
  - subject: "Re: Oxygen windeco and widget"
    date: 2007-06-19
    body: "> last years i read that plasma was ready to support apple dashboad widget\n\nno, zack said it -would- support such widgets. this is among my 4.1 targets and relies on a few things happening first, including the completion of the kde/webkit part.\n\nremember that some dashboard widgets include coacoa bundles which we will have no way of supporting, so it is only http+css ones that can work.\n\nalso, by 4.1 we may find we have equal or better replacements for all the dashboard widgets and so this support would be unnecessary."
    author: "Aaron Seigo"
  - subject: "Sonnet"
    date: 2007-06-19
    body: "Is there still development going on with sonnet (spellchecker)? There hasn't been any news about it for months..."
    author: "Jonathan"
  - subject: "Re: Sonnet"
    date: 2007-06-19
    body: "it's stalled at the moment, but that happens fairly regularly with open source components so it's not a reason to get overly concerned. i'm sure it'll pick up again at some point. =)"
    author: "Aaron Seigo"
---
In <a href="http://commit-digest.org/issues/2007-06-17/">this week's KDE Commit-Digest</a>: Work on engine configurability, data management, a packaging system for Plasmoids and themes, and new refinements in desktop icon interaction in <a href="http://plasma.kde.org/">Plasma</a>. The <a href="http://oxygen-icons.org/">Oxygen</a> window decoration and widget are both moved into kdebase. Further work in the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=1EF6392A4C8AEADD">Icon Cache</a>, <a href="http://code.google.com/soc/kde/appinfo.html?csaid=F3389CEA023E0E9D">Kopete Messenger update</a>, <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9064143E62AF5BA6">KRDC</a> and <a href="http://code.google.com/soc/kde/appinfo.html?csaid=A605EEA634DA5998">Context Help</a> <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> projects. Improved highscore handling and network management across kdegames. New keyboard engine becomes live in <a href="http://edu.kde.org/ktouch/">KTouch</a>, whilst the <a href="http://stepcore.sourceforge.net/">Step</a> physics simulation package receives support for annotations. Support for many new text styling options in <a href="http://koffice.org/">KOffice</a>. Further work towards <a href="http://amarok.kde.org/">Amarok</a> 2.0, including work on the context view and the display of lyrics. More recent and precise elevation data added to <a href="http://edu.kde.org/marble/">Marble</a>. KColorScheme colour roles are added to aid usability. User documentation is started for <a href="http://enzosworld.gmxhome.de/">Dolphin</a>. More work in <a href="http://strigi.sourceforge.net/">Strigi</a> and <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>. Work on vector selections and a smoothing algorithm for drawing implemented in <a href="http://www.koffice.org/krita/">Krita</a>. Many improvements in the KMix sound management utility. <a href="http://www.digikam.org/">Digikam</a> begins to be ported to KDE 4. Large scale reorganisation in the kdegraphics module: KColorEdit, KIconEdit, KPovModeller, Kuickshow and Ligature move to extragear/graphics, whilst Kooka and kmrml are removed completely.
<!--break-->
