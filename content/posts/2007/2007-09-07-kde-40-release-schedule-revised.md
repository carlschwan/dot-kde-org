---
title: "KDE 4.0 Release Schedule Revised"
date:    2007-09-07
authors:
  - "sk\u00fcgler"
slug:    kde-40-release-schedule-revised
comments:
  - subject: "Very good!"
    date: 2007-09-07
    body: "i think it's a very good strategy to wait for a really final and stable release for december and to release a developer version earlier. I update my opensuse packages of KDE4 every day and I'm always happy when I see new things which work... so move on with the great work!\n\nBye\n\nBernhard"
    author: "LordBernhard"
  - subject: "Explanation of the Development Platform"
    date: 2007-09-07
    body: "We're not releasing a \"development version\" (well, we do, it's called \"Beta\" and \"Release Candidates\"). The Developer Platform however does *not* contain the desktop, nor applications. It's \"merely\" a set of libraries and tools that you need to *write* KDE 4 applications. Those parts are considerable further down the road than the desktop itself (no surprise there), so we want to give third party developers the opportunity to work with it while we're still polishing the desktop itself."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Explanation of the Development Platform"
    date: 2007-09-07
    body: "I think this is a really good move! Compiling all of KDE still seams like a daunting task to me. If I can install a \"SDK\" to start coding directly, I'd really use that instead.\n\nLikewise, the KDE4 packages for openSUSE made me consider to try it out now. I don't feel like compiling KDE every week again. ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: Explanation of the Development Platform"
    date: 2007-09-07
    body: "oh.. I'm really sorry for this mistake.. I haven't had enough time at work to read the whole text so I just flew over it... nonetheless it's a great decision!\n\nI've got one last question: will there be enough documentation ready in the techbase (until the developer platform gets released) to start from scratch with KDE4 programs? atm there are wonderful categories but most of them are empty or really basic. I'm really interested in this because I'd like to write a little systemcenter for KDE and it should also have support for plasma/plasmoids and therefore I'd need good documentation of plasma/solid and some other stuff."
    author: "LordBernhard"
  - subject: "good move"
    date: 2007-09-07
    body: "Good move guys.  Eager as I am to get my hands on a finished KDE4, I'd ratehr wait until it's REALLY finished, and everyone else (even people who've never tried KDE before) can realise its potential too.  Well done :)"
    author: "Lee"
  - subject: "Bad"
    date: 2007-09-07
    body: "You REALLY had change the release date of KDE 4.0 to when I have all my tests, aight? ;)\n\nNo seriously, I think it sounds great. So, is the codename going to be \"Khristmas\"? Oh wait, forgot that you change all 'K's to 'C' nowadays.\n\nKeep up with the great work!"
    author: "Hans"
  - subject: "Re: Bad"
    date: 2007-09-07
    body: "*had change = had to change"
    author: "edit"
  - subject: "Re: Bad"
    date: 2007-09-07
    body: "Why did they have to change Ks to Cs"
    author: "ben"
  - subject: "Re: Bad"
    date: 2007-09-07
    body: "Don't worry, they won't change to CDE or Copete. ;)\nSee http://dot.kde.org/1189078926/1189105274/1189107719/"
    author: "Hans"
  - subject: "Re: Bad"
    date: 2007-09-08
    body: "Isn't the CDE name more or less available now? I searched for \"cde\" on a well-known search engine and the top (related) hit was the following page: http://www.opengroup.org/cde/\n\nwhere the FAQ contained questions such as:\n\n\"What is the right porting path for me if I am using Motif 1.2.4 right now? Should I expect to be using CDEnext as of January 1, 1997? How do I get there from here?\"\n\nWho is for a name change!? CDE 4.0 - the cool desktop environment :-)"
    author: "Martin"
  - subject: "Re: Bad"
    date: 2007-09-08
    body: "community desktop environment? :)"
    author: "Chani"
  - subject: "Re: Bad"
    date: 2007-09-08
    body: "Hear, hear.  We may as well, so that we don't have to explain to all of the hoards of newbies what the \"K\" stands for..."
    author: "David"
  - subject: "Re: Bad"
    date: 2007-09-08
    body: "If you're going for a name change might as well drop the nonsense that is \"desktop environment\"."
    author: "Reply"
  - subject: "Re: Bad"
    date: 2007-09-11
    body: "I agree. Acronyms are so 90's."
    author: "Tim"
  - subject: "Re: Bad"
    date: 2007-09-08
    body: "The point of the excursion into Latin is that K,C and Q are pronounced the same and can be used as you like."
    author: "reihal"
  - subject: "String freeze?"
    date: 2007-09-07
    body: "The string freeze will occur in October 19th also?\n\nThank you."
    author: "kde.fan.from.brasil"
  - subject: "Bugreports"
    date: 2007-09-08
    body: "> Start using the current SVN version and help us fixing bugs.\n\nI think this means \"help coding\" right? Just having svn running here, and it looks like it is to early to report bugs ;)\n\n\nGreetings\nFelix"
    author: "Felix"
  - subject: "Re: Bugreports"
    date: 2007-09-10
    body: "Are there nightly builds?"
    author: "Martin"
  - subject: "Translation freeze?"
    date: 2007-09-08
    body: "I don't see a translation freeze in the schedule.\n\nWich is the final data for translations?\n"
    author: "Rinse"
---
The KDE Release Team has revised the <a href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Schedule">release 
schedule for KDE 4.0</a>. The first visible bits of KDE 4.0
will be the <strong>KDE Development Platform</strong> release on October, 30. This Development Platform release 
consists of bits and pieces needed to develop KDE applications. It includes kdesupport, kdelibs, kdepimlibs 
and kdebase/runtime. The purpose of the <em>KDE Development Platform</em> release is to make it 
easier for third party developers to port their applications to KDE4 technology and to start developing 
new applications.  The final and long awaited release of the <strong>KDE Desktop 4.0</strong> is planned for <em>
December, 11th 2007</em>, well in time to be a Christmas present for everyone who 
has been longing for KDE 4.0.




<!--break-->
<p>The <em>revised release schedule</em> looks like this:</p>

<ul>
    <li>
        October, 2nd: KDE 4.0 Beta 3
    </li>
    <li>
        October, 19th: Total Release Freeze, only critical bugfixes are allowed to get in. 
        If in doubt, ask the release coordinator.
    </li>
    <li>
        October, 30th: KDE 4.0 Release Candidate 1
    </li>
    <li>
        November, 14th: KDE 4.0 Release Candidate 2
    </li>
    <li>
        December, 11th: KDE 4.0
    </li>
</ul>

<p>Tagging of those releases takes place on the Wednesday before the release, so if you are a
developer, make sure to have your code in by the following dates:</p>

<ul>
    <li>
        September, 26th: Tagging Beta3
    </li>
    <li>
        October, 19th: Total Release Freeze, critical bugfixes only from this date
    </li>
    <li>
        October, 24th: Tagging RC1
    </li>
    <li>
        November, 14th: Tagging RC2
    </li>
    <li>
        December, 5th: Tagging 4.0
    </li>
</ul>

<p>The Release Team would like to stress that developers should be in 'release mode' by now,
concentrating on making KDE4 usable as quickly as possible. Start using the current SVN version and
help us fixing bugs.</p>



