---
title: "KDE Commit-Digest for 21st January 2007"
date:    2007-01-22
authors:
  - "dallen"
slug:    kde-commit-digest-21st-january-2007
comments:
  - subject: "Timezone?"
    date: 2007-01-22
    body: "What timezone are you posting this, Danny?\nWe currentyl have 00:14 here, seeing your article\n\ngreat btw. :>"
    author: "chri"
  - subject: "Re: Timezone?"
    date: 2007-01-22
    body: "I'm in GMT, so I published this digest about 22:50 (I generally try to get them done before Midnight ;))\n\nCheers,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Timezone?"
    date: 2007-01-22
    body: "Impressive work again -- thanks a lot!"
    author: "MK"
  - subject: "french grammar checking?"
    date: 2007-01-22
    body: "does anyone know if sonnet will have support for french grammar checking which is very complicated? this is the only thing I miss from MS Word. Even OpenOffice doesn't have it so it would be great to have it Kword and KDE in general."
    author: "Patcito"
  - subject: "Re: french grammar checking?"
    date: 2007-01-22
    body: "I'm currently not aware of a mature open source grammar checker for French. However, I do know of a few projects that have it on their agenda. When they mature enough to be useful, KDE can include plugins enabling their use."
    author: "Jacob Rideout"
  - subject: "Re: french grammar checking?"
    date: 2007-01-22
    body: "Give http://www.danielnaber.de/languagetool/ in OOo a try. But it's definetly not yet mature (but you can help ;) ). As far as I know was Daniel Naber also developing something for KOffice but this happened ages ago as a part of his diploma thesis (-> http://www.danielnaber.de/languagetool/download/style_and_grammar_checker.pdf)\nHowever, excellent things he is doing."
    author: "pete"
  - subject: "Re: french grammar checking?"
    date: 2007-01-22
    body: "I use Antidote. I now it's not OpenSource, not even free, but it works very well.\nhttp://www.antidote.info"
    author: "Nonymous"
  - subject: "Re: french grammar checking?"
    date: 2007-01-22
    body: "Work has also been done on a French port of Gramad\u00f3ir (http://borel.slu.edu/gramadoir), although I'm not sure how current that is.  Certainly worth looking into, though."
    author: "Kevin Donnelly"
  - subject: "konsole"
    date: 2007-01-22
    body: "konsole gets improved? I hope someone will finally fix the configuration settings. Can't it be one \"configuration dialogue\" instead of popup submenus which clutter half of your screen with the option to set your language to farsi (\"code\" menu) and chinese and steel the focus when you scroll the menu or the offer of 20 colour schemes which look equally boring?"
    author: "Fiasma"
  - subject: "Re: konsole"
    date: 2007-01-22
    body: "Hahaha i see what you mean\n\nIf I hit encoding, I get a huuuuuuuuuge list.\n\nThe only problem is, I will at best need only like 2 or \nthree of these encodings ever :)\n\nMaybe a sensible default would be good there, with an extra \n\"button\" that reads like \"expand for extra encodings\"\nor so, and if this is clicked, it lists really many \noptions..."
    author: "shev"
  - subject: "Re: konsole"
    date: 2007-01-22
    body: "I think gnome-terminal has a good solution here.  It only shows the current locale by default, with an \"Add/Remove...\" menu option below.  Selecting that brings up a dialog which allows the user to choose which encodings (from a long list) are shown in the menu).\n\n"
    author: "Robert Knight"
  - subject: "Re: konsole"
    date: 2007-01-22
    body: "Gnome and configuration... Ha ha ha, this IS a good joke ;) \n\nBut yes, you are right, there is no need to show all the options at once. "
    author: "asdasd"
  - subject: "Re: konsole"
    date: 2007-01-23
    body: "It would have been a joke if the GNOME application wasn't better designed in this particular case. ;)"
    author: "Reply"
  - subject: "Re: konsole"
    date: 2007-01-23
    body: "The funny thing: there is already an ugly configuration dialogue. You find it below the list, there you can define more of the settings you never need to define. Must be a hostile takeover hack for those who switch konsoleconfigs twice a day and CONTROL the konsole\n\nWhy not \"simple theming\": where \"konsole theme\" is a module in the Kontrolcenter.\nCant's codepages be switched on the console with a simple command? Do we need a menu entry?"
    author: "ugla"
  - subject: "Re: konsole"
    date: 2007-01-23
    body: "> Cant's codepages be switched on the console with a \n> simple command? Do we need a menu entry?\n\nA menu entry has two important advantages:\n\n- Menu entries are easier to discover than shell commands.\n- Consistency with other applications which provide menu options for encoding selection\n\nRegarding the settings dialog and general handling of preferences, I hope to make some improvements in this area for KDE 4.  I won't promise anything specific because it isn't done yet."
    author: "Robert Knight"
  - subject: "Open source or free software?"
    date: 2007-01-22
    body: "on http://decibel.kde.org/ it says at the bottom:\n'... it's an open source desktop project!' linking to kde.org which says: 'KDE is a powerful Free Software graphical desktop environment'.\nIt would be less confusing  if all kde projects could define themselves as either OS or Free Software but not both, don't you think?"
    author: "Patcito"
  - subject: "true lol"
    date: 2007-01-22
    body: "you're right os and fs don't have anything in common actually they are mutually exclusive...\nclown punk"
    author: "eMPe"
  - subject: "Re: true lol"
    date: 2007-01-22
    body: "well, there are some fundamental issues here, but even if there wheren't and they where exactly the same, it is still confusing to use both.\n\ni prefer to change the plasma & other sites to show the KDE link as 'it's a Free Software Desktop Project' instead of Open Source, but hey, it's not my call :D"
    author: "superstoned"
  - subject: "Re: true lol"
    date: 2007-01-22
    body: "Those 2 (\"Free Software\", \"Open Source\") are _important_ descriptions and are NOT the same. "
    author: "qwertyui"
  - subject: "Re: true lol"
    date: 2007-01-22
    body: "Yes, they are strongly different and \"free software\" should be used whenever GPL or LGPL is used as a license. \"Open Source\" is much more limited then this."
    author: "Andrei"
  - subject: "Re: true lol"
    date: 2007-01-23
    body: "You're right that they're very different, but \"Open Source\" may also be applied to software under the GPL. For example, Linux is defined by its creator and main developers as \"open source\", and it's available under the GPL. (Linus has even said the ideas promoted by the FSF are idiotic).\n\nSee the list of approved licenses by the Open Source Initiative and the GPL is right there.\n"
    author: "Reply"
  - subject: "Commit digest 'lines' graph broken..."
    date: 2007-01-22
    body: "It looks as though it's calculating it's percentage of the top developer (in terms of number of commits) rather than the top number of lines added."
    author: "Henry"
  - subject: "Grama is a bad name for an application in Italian"
    date: 2007-01-22
    body: "\"Gramo\" (Feminine: grama) in Italian means \"Bad\", \"Hard\", \"Difficult\", \"Wretched\".\n\nA typical expression is \"Vita grama\", meaning \"Wretched life\"...\n\nOn the other hand, DBModeler gives an idea on what the application is meant to do, why would you change it?"
    author: "Luciano"
  - subject: "Re: Grama is a bad name for an application in Ital"
    date: 2007-01-22
    body: "DbModeler is a typo for DBModeller, I choose it only because I needed a name to the directory where to put the files in =].\n\nIn portuguese it means \"grass\" or \"gram\".\n\nWell... I have until KDE4 release date to choose a name... and most important... finish basic features that still missing and release a usable version somewhere."
    author: "Hugo Parente"
  - subject: "Re: Grama is a bad name for an application in Ital"
    date: 2007-01-23
    body: "What kind of modelization do you plan to support ? Merise ? Have an eye on analysesi which has an excellent mcd/mld support and can generate the sql output out of it."
    author: "Morreale Jean Roc"
  - subject: "KPlayer"
    date: 2007-01-23
    body: "I guess the digest doesn't cover extragear news, but still it is worth mentioning that a new version of the best multimedia player on any platform, KPlayer 0.6, was released this week."
    author: "hi"
---
In <a href="http://commit-digest.org/issues/2007-01-21/">this week's KDE Commit-Digest</a>: <a href="http://edu.kde.org/khangman/">KHangman</a> becomes the latest application to migrate to SVG-based scalable interface rendering. KOpenBabel is merged and the beginnings of a 3d navigation system in <a href="http://edu.kde.org/kalzium/">Kalzium</a>. Work expands in the <a href="http://uml.sourceforge.net/">Umbrello</a>/<a href="http://koffice.kde.org/kplato/">KPlato</a> <a href="http://dot.kde.org/1165100724/">Student Mentoring program</a>. Support for the <a href="http://en.wikipedia.org/wiki/CDisplay_RAR_Archived_Comic_Book_File">ComicBook Archive</a> and other improvements in <a href="http://okular.org/">okular</a>. Work on Picture, Video and <a href="http://www.koffice.org/krita/">Krita</a> "<a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a>" shapes in <a href="http://koffice.org/">KOffice</a>. Improvements in both the KDE 3.5 and 4.0 versions of <a href="http://konsole.kde.org/">Konsole</a>. Language detection in Sonnet continues to mature. Import of concept code demos in <a href="http://decibel.kde.org/">Decibel</a>. "Simple-search" user interface work, and support for indexing binary data fields in <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>. "<a href="http://kde.ground.cz/tiki-index.php?page=KDE%204%20GUI%20framework">liveui</a>" moves back into kdelibs. dbmodeler, a database schema modelling application (part of the <a href="http://developer.kde.org/seasonofkde/">Season of KDE</a>), is renamed "<a href="http://developer.kde.org/seasonofkde/project.php?database_modeller.xml">grama</a>".
<!--break-->
