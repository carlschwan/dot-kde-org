---
title: "Sebastian Tr\u00fcg on K3b 1.0 and More"
date:    2007-04-26
authors:
  - "gventuri"
slug:    sebastian-trüg-k3b-10-and-more
comments:
  - subject: "speaking of cd-writing.."
    date: 2007-04-26
    body: "Relatively recent LG Writer, verbatim DVD-R dual layer media yields (growisofs/k3b):\n:-[ READ DVD STRUCTURE #10h failed with SK=8h/ASC=ABh/ACQ=80h]: Input/output error\n\nIt's good that these error messages are well thought out :)). Google tells me that drive would not be OK with this media, but .. it used to burn it just fine before :/\n"
    author: "foo"
  - subject: "Thank you for bringing us K3B"
    date: 2007-04-26
    body: "Sebastian, thank you for K3B!\n\nNow all I need is Amarok being able to use it for burning audio CD's without starting up the \"complete\" K3B program... (yes, us users always want more :p)"
    author: "Darkelve"
  - subject: "Re: Thank you for bringing us K3B"
    date: 2007-04-26
    body: "If I understood some mails/messages somewhere correctly, Sebas wants to turn K3B into a library, so this will actually be possible, though it'll most likely be post KDE 4.0/amarok 1.0 :D"
    author: "superstoned"
  - subject: ":)"
    date: 2007-04-26
    body: "F\u00e9licitations pour ton mariage Sebastian"
    author: "shamaz"
  - subject: "Re: :)"
    date: 2007-04-26
    body: " quoi il es marri\u00e9 d\u00e9ja, j'ai cru comprendre qu'il a dit girlfriend ? "
    author: "djouallah mimoune"
  - subject: "Video DVD authoring"
    date: 2007-04-26
    body: "I would rather keep it seperated and have here a loose or tight intergration with e.g. kdenlive.\n\nFor this kdenlive shoult also be part of KDE extragear, but anyway this is the responsibility of the kdenlive devs."
    author: "Philipp"
  - subject: "k3b - cool"
    date: 2007-04-29
    body: "Many thanks to Sebastian for k3b. Now my wife can burn disks from my machine and I did it using k3b too without constructing command-line for growisofs. K3b also have a cute interface!"
    author: "alogic"
  - subject: "hi"
    date: 2007-04-29
    body: "hi John,\n\n\nthanks for telling me this.  I have never had an email before.\n\nisn't kde the best.\n\nplease let me know.\n\nnatalya "
    author: "natalya"
---
Today we talk with the author of the K3b Project, the well known application that lets you burn CDs/DVDs and that lets you rip music from CD audio and films from DVD Video. We are going  to talk with Sebastian about his story: when he started using KDE, when he started to create K3b and to talk about
his plans in KDE 4 with a new KDE 4 project.  This interview was <a href="http://www.kde-it.org/index.php?option=com_content&amp;task=view&amp;id=108&amp;Itemid=1">originally released</a> for <a href="http://www.kde-it.org/">KDE Italia</a>.
<!--break-->
<p><b>Hello Sebastian. What did you think when I asked you an interview for KDE Italia?</b><br/>
Oh man, yet another interview? ;) No, really, I am honored to be of interest to you as an interview partner.</p>

<p><b>Can you introduce yourself to the KDE Italia readers? What did you study? What is your day job?</b><br/>

Originally I come from the north of Germany where I was raised in rainy weather. Once I finished school I "fled" to the south of Germany to study computer science in Freiburg. It is hard to go more south than Freiburg if you want to stay in Germany. Since August of last year I am working for Edge-IT, a subsidiary of Mandriva. I am still living in Freiburg working remotely from home. It is great. I finally can do what I like best and get payed for it: working on KDE software. In particular the <a href="http://nepomuk-kde.semanticdesktop.org/">Nepomuk-KDE project</a> which is part of Nepomuk, the European semantic desktop initiative. 

<p><b>When did you hear of KDE for the first time?</b><br/>
I think it was in 1997. When I came to Freiburg a friend "introduced" me to SuSE 5.something or 4.foobar. It came with KDE 1 and Qt 1.4. In the beginning I was not really interested since I did no programming yet. Then I started to learn Qt development in a software project we did at the university and I got hooked.</p>

<p><b>How and when did you get involved in KDE?</b><br/>
My interest in Qt started with the software project mentioned above. If I recall it correctly that was the time when I really began to try Linux/KDE. It was still KDE 1.4 back then and the desktop was by far not as powerful and nice as it is today.<br/>
I wanted to see if I could do everything I normally did on my Windows system (which was not much ;) on Linux. The first thing that did not work as I wanted it to was CD burning. Especially Audio CD burning was near to impossible with the existing tools. I wanted to drag my mp3 files somewhere and click a few buttons, not manually decode the files using mpg123 and then burn the CD on the commandline or use something like xcdroast or kcreatecd. These tools simply did not provide what I needed.<br/>
So I asked myself: "Why don't you try it yourself?" And I did. The first version of K3b I released was 0.3.1 and with it the at times strange K3b versioning system started. It was very well received and I just kept going.</p>

<p><b>What makes you develop for KDE instead of the competition?</b><br/>
Easy: Qt and the KDE libs. IMHO Qt is the best widget-library there is. It's easy, it's powerful, and it looks good. And the kdelibs are very well designed and solve so many standard problems so as a developer I can focus on the really important stuff in my application. And in the end "real programming" can only be done with C++. ;)</p>

<p><b>Can you say that programming for KDE was an investment? You got C++/Qt programming experience that helped you in enriching your personal skill set. Can this be of help getting a job? Could it be a good attribute to take to a job interview with a software company?</b><br/>
K3b was without a doubt my ticket into the professional world. It built a nice reputation and raised a lot of interest. I surely would not have my current job if I would never have done K3b.</p>

<p><b>Can you explain what "DVD authoring" is? Some users said K3b does "DVD authoring". You are the K3b author so no one better than you can tell us.</b><br/>
This is a really strange question. You must be talking about Video DVD because otherwise it would just be burning DVDs. And authoring Video DVDs means preparing the movie material, cutting it, adding subtitles, creating a menu. K3b can do none of those things. So, no, I am not the best one to talk to about Video DVD authoring. Maybe I will be some day when I start implementing it in K3b but that is nearly as much work as all of K3b was until now.</p>

<p><b>What are the more important new K3b features shipped with the version 1.0?</b><br/>
The most important new feature for me is the K3b MediaCache. It knows at all times which medium is in which drive and thus, provides a basis for a much improved user feedback and GUI adjustments. One example is the CD copy dialogue: K3b now only shows the settings suitable for the inserted source medium. If you copy an audio CD you only get the audio copy options. It also provides a means to automatically adjust the project size to the inserted writable medium.<br/>
Then there is the new Video DVD ripping. It provides a very easy way to encode Video DVD titles to MPEG4 (for example Xvid) movies. The user interface is quite similar to the audio CD ripping and is focused on simplicity. It is by far not as powerful as for example DVD:Rip but it is easy to use.</p>

<p><b>Was in the past K3b part of the standard KDE modules?</b><br/>
K3b was once in the kdemultimedia package but only for a very short time. It was never released with kdemultimedia. Frankly. I am glad we moved it out of there and into the Extragear because I could never have kept up with the kdemultimedia release cycle. K3b needs many little bugfix releases due to the many different CD and DVD devices that all behave a little different and I cannot test all of them because I don't have them all (actually I tried getting testing devices from many companies, but apart from LG I never received any). So Extragear is actually the perfect place for K3b.</p>

<p><b>Is it true that some people wanted K3b in KDE 4 standard modules? Is this possible or not?</b><br/>
I never heard anyone ask K3b to become part of KDE4. In theory it is possible but I really don't want it to. I like the freedom I have with K3b. This way I have my own release cycle and I can change whatever I want, whenever I want. After all it is not clear which advantage would come from putting K3b into KDE4 itself. A tighter integration with other applications can also be achieved with K3b being a separate package. KDELibs and KDE-Parts bless.</p>

<p><b>When do you think to release next K3b versions (1.0.2, ...)? When do you start KDE 4 porting?</b><br/>
I am now preparing 1.0.2 which will fix all the bugs the "real" users find in 1.0 and 1.0.1. In the end all those previews and release candidates have only been tested by geeks like me and we are not the best when it comes to exploiting bugs. After K3b 1.1 I will start porting K3b to KDE 4 libraries.</p>

<p><b>Are you being paid to work on KDE?</b><br/>
Until last August when I started working for Edge-IT I did all the work on K3b in my spare time except for a three-month internship for SuSE. Now, however, I am working full-time on KDE, namely the Nepomuk-KDE project as mentioned before.</p>

<p><b>I've seen in your commit and from some news around the web you are working on a new KDE project. It's related to KDE 4. Nepomuk. Can you talk about it?</b><br/>
Sure. Nepomuk-KDE is a subproject of the European research project Nepomuk. The goal is to implement the standards and technologies defined there on the KDE desktop. I will not try to explain the whole Nepomuk project here but focus on the part that I do for KDE at the moment.<br/>
The keyword is metadata. At the moment the only metadata handled in KDE is what you can extract from files, for example id3 tags in an mp3 file, and maybe some tags and ratings specific to certain applications. Nepomuk-KDE (Hopefully it will soon get a fancy new name) aims to unify the access to metadata for all applications. Applications will have the possibility to create and retrieve metadata from one central storage. Imagine for example that you want to find all resources related to a certain person. With Nepomuk-KDE you will then get the KAddressbook entry, all the emails the person has written, the emails you wrote to the person, maybe the images the person took, or those that show the person and most importantly second-degree resources like the pdf document that person sent you.<br/>
The idea is that metadata (and I am not talking about the metadata already contained in files which is extracted and indexed by systems like Strigi or Beagle) is created by basicaly all KDE applications and interconnected automatically or semi-automatically.</p>

<p><b>How would you define your experience with KDE and KDE developers? Did you meet any of them personally or you just know them by emails?</b><br/>
The first time I met some of the KDE guys was at last year's Akademy. Before that all contact was through email, I never even went in to the IRC. Today I know that you have to do that in order to stay connected. And now that I know some of the nice KDE faces personally chatting became much more interesting.</p>

<p><b>Have you ever stayed at an Akademy? If so can you tell us briefly how it was? What did you do? Do you think a KDE user/developer has to participate at least once to the KDE developers conference (Akademy) in his/her life?</b><br/>
As I said: I participated in last year's Akademy. It was a very nice experience and I think that it makes a lot of sense to go there every year if you want to be part of KDE. Working together is much easier if you know the people personally. This year, however, I won't be able to attend the Akademy since I am getting married at the exact same time in France.</p>

<p><b>Oh, than many wishes :D . Do your parents and friends use Linux and KDE?</b><br/>
My girlfriend surely uses KDE. She actually uses it in a way that she misses a lot of the great functionality when she sits in front of a Windows machine.</p>

<p><b>What could be your slogan to attract people to KDE? Can you give also some "reasons to stay with *nix/KDE"?</b><br/>
"Come on, use KDE, please.... PLEASE!" (Man, I should have gone into advertising!)</p>

<p><b>If, one day, you stopped working on KDE, what could be the reason? Too much time to dedicate to a new job, to your family or what else? Or, simply, your passion for KDE is spent and so you leave the KDE team? What would you miss of the KDE experience? (Obviously we hope you can work in the KDE team for at least the next 30 years :))</b><br/>
How should I know what I will be doing in the future. But I would probably miss creating something. When I was a child I created stuff with Lego, now I am a programmer. Actually, I don't think it is important what I create. I just need to create something that is usable and impresses people. When I was little my Lego models were usable too since I played with them and my parents were impressed by them. Today the software I write is usable to a lot of people and many are impressed by it.</p>

<p><b>How much time do you usually spend on KDE?</b><br/>
All of it. ;) Well, probably 8 hours a day.</p>

<p><b>What was your first Linux distribution? Did you try many ones before you found the right one?</b><br/>
I started with SuSE. Later I tried many more. I actually have installations of many different for testing purposes.</p>

<p><b>Which distribution do you use now? Why?</b><br/>
On my laptop I am running Mandriva Cooker and not only because they pay me to do so. It actually works perfect and as far as rpm goes I know of nothing fancier then urpmi. My big machine at home runs Gentoo because I like compiling stuff and having an optimised system.</p>

<p><b>What is your favourite place in the world?</b><br/>
I haven't found that yet (No, it just can't be in front of the computer screen! ;)</p>

<p><b>Have you ever been in Italy? If not, do you plan to come here and visit our country?</b><br/>
Sure I have been to Italy. Many times.... ok, once, when I still was in school. But I loved it and who wouldn't? It is warm almost all the time.</p>

<p><b>Thank you for your time and good luck with K3b and Nepomuk. I think they're two of the best apps written for KDE.</b><br/>
Thanks a lot for the interview.</p>