---
title: "Polish KDE Community Announces KDE.org.pl Web Site"
date:    2007-05-14
authors:
  - "jstaniek"
slug:    polish-kde-community-announces-kdeorgpl-web-site
comments:
  - subject: "Good luck!"
    date: 2007-05-14
    body: "It is always nice to see people active all around the World. Good luck!"
    author: "Hobbes"
  - subject: "What CMS?"
    date: 2007-05-14
    body: "Hello,\nfirst of all good luck. It's hard to manage a KDE related web site. What CMS are you using?"
    author: "Giovanni Venturi"
  - subject: "Re: What CMS?"
    date: 2007-05-14
    body: "It's customized Mediawiki, with template based on techbase.kde.org.\n\n"
    author: "Jaroslaw Staniek"
  - subject: "KDE.pl once again?"
    date: 2007-05-14
    body: "I hope it will be more interesting than kde.pl website..."
    author: "Romanujan"
---
The Polish community of KDE is growing year after year. In association with <a href="http://ev.kde.org/">KDE e.V.</a> we're proud to announce the launch of the <a href="http://kde.org.pl/">KDE.org.pl</a> web site, with ambitions of becoming the starting point for the KDE element of Poland. Read on for details.


<!--break-->
<p>Alongside local articles, KDE.org.pl contains content from the main <a href="http://kde.org/">kde.org</a> web site, such as KDE history, release plans, interviews with KDE people and information about KDE applications. Polish localization and local events, as well as the forthcoming KDE 4 release has been a central focus.</p>

<p>The knowledge that can be acquired on the new web site is largely operating system and vendor independent. This covers not only KDE as a graphical environment but also as a strong development platform, and as a set of office, scientific and educational applications, all consisting of Free and Open Source software components.</p>

<p>KDE.org.pl has been established with a small editorial staff, an extension of the "KDE Press Team PL" established earlier this year to improve communications and contact between KDE and the Polish media. Everyone is invited to join.</p>

<p><i>Regards,<br><a href="http://kde.org.pl/">The KDE.org.pl Team</a></i></p>

<p>Contact: <a href="http://kde.org.pl/Kontakt">http://kde.org.pl/Kontakt</a> (pl)</p>

