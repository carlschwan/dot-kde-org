---
title: "Deadline for Google's Summer of Code 2007 Approaching"
date:    2007-03-23
authors:
  - "sk\u00fcgler"
slug:    deadline-googles-summer-code-2007-approaching
comments:
  - subject: "KDE-related opportunities in other projects"
    date: 2007-03-23
    body: "There are also many KDE/Qt development opportunities in other projects, \nso I've quickly skimmed for KDE relatedness, \nfor whomever might find his/her future project there.\nThis is what I found. You might want to check that out.\n\nAbiSource: Implement KDE's Sonnet in AbiWord (Gtk word processor)\nhttp://www.abisource.com/twiki/bin/view/Abiword/SummerOfCode2007\n\nBazaar: Work on the GUI (QBzr?)\nhttp://bazaar-vcs.org/SummerOfCode\n\nBoost: A graphical front-end to Boost.Test\nhttp://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Google_Summer_Of_Code_2007\n\nBZFlag: Enhanced server listing for this game + GUI. Time to revive Kqf for the gui part \n(http://websvn.kde.org/trunk/playground/games/kqf/)?\nhttp://my.bzflag.org/w/Google_Summer_of_Code\n\nCLAM: Qt based interfaces for CLAM Network based VST plugins\nhttp://iua-share.upf.edu/wikis/clam/index.php/SoC_ideas\n\nCreative Commons: Desktop Integration for finding cc content in KDE\nhttp://wiki.creativecommons.org/Summer_of_Code\n\nCrystal Space: Level Editor in C++ (->KDE/Qt)\nhttp://trac.crystalspace3d.org/trac/CS/wiki/SoC%20Ideas\n\nFANN - The Fast Artificial Neural Network Library: Native GUI (You know which toolkit to write that with ;))\nhttp://leenissen.dk/fann/index.php?p=gsoc.php\n\nFreeBSD: KDE front-end to the freebsd-update(8) utility\nhttp://www.freebsd.org/projects/summerofcode.html\n\nGenMAPP: IDEA 2: Innovative & Intuitive Interfaces. They want a non-clunky GUI -- and they're Java. \nQt-Jambi to the rescue?\nhttp://conklinwolf.ucsf.edu/genmappwiki/Google_Summer_of_Code_2007\n\nGNU Project: work on gnunet-qt?\nhttp://www.gnu.org/software/soc-projects/ideas.html\n\nHandhelds.org: Make opensync work better with Opie; co-sponsorship possible with KDE\nhttp://www.handhelds.org/moin/moin.cgi/SummerOfCode\n\nHaskell.org:Haskell Qt binding generator\nhttp://hackage.haskell.org/trac/summer-of-code/query?status=new&status=assigned&status=reopened&group=topic&type=proposed-project&order=priority\n\n\n#####Some extra space for this:\nhugin / panotools: Intuitive yet powerful GUI for panorama creation. \nThey really want a nice Qt (KDE) GUI for good and consistent look and feel on all platforms. \nHelp them and us with a cool GUI for this panorama photo maker!\nhttp://wiki.panotools.org/SoC2007_projects\n\n\nLanka Software Foundation: They offer mentorship for KBFx and KDE enhancements. \nGood possibility to let the the official Raptor start menu be mentored?\nhttp://www.opensource.lk/\n\nLiblime: Modify KDE's Kartouche translating tool and modify it to their needs.\nhttp://wiki.liblime.com/doku.php?id=googlesummerofcodeideas\n\nLispNYC.org: CFFI+QT: Expand/improve existing work on creating QT bindings in Common Lisp.\nhttp://lispnyc.org/soc2007.clp\n\nMetaBrainz Foundation: Work on their PicardQt tagger\nhttp://wiki.musicbrainz.org/SummerOfCodeIdeas\n\nMixxx: e.g. Improve library/playlist interface for this cool Qt DJ application\nhttp://mixxx.sourceforge.net/wiki/index.php/GSoC_2007\n\nOpenICC: Control Panel for Colour Management. The KDE desktop needs integration of colour management settings in desktop configuration UI's.\nLProf - various tasks for this ICC profiler (it's a KDE application)\nhttp://freedesktop.org/wiki/OpenIccForGoogleSoC2007\n\nPHP: Debugger Frontend for Xdebug. Write a simple KDE/Qt GUI application.\nhttp://de.php.net/ideas.php\n\nScribus Development Team: There are several tasks for this Qt page layout program.\nhttp://wiki.scribus.net/index.php/SoC2007_ideas\n\nScummVM: Tools: Write a portable GUI for the tools. Qt-ify their command line tools!\nhttp://wiki.scummvm.org/index.php/OpenTasks\n\nSwarm Development Group: front end to Swarm to do model visualization with a native GUI - Qt/KDE.\nhttp://www.swarm.org/wiki/Swarm:_IdeasList\n\nSwathanthra Malayalam Computing: Fix Malayalam Rendering in Qt\nIntegrate Dhvani text to speech engine with libraries (Qt)\nVoice recognition support for Free Software Desktops (KDE)\nhttp://fci.wikia.com/wiki/SMC/SoC/2007\n\nThe Electronic Frontier Foundation: Write a / work on one of the TOR GUI clients (there do exist several for KDE).\nhttp://tor.eff.org/volunteer.html.en#Coding\n\nThe gEDA Project: Several tasks with no special GUI toolkit requirement.\nhttp://geda.seul.org/gsoc/projects.html\n\nThousand Parsec: Create a \"Ruleset\" development Environment (Scheme, Qt)\nhttp://www.thousandparsec.net/tp/google-summer-of-code-2007.php\n\nVideoLAN: VLC media player as a Back-End for Phonon\nhttp://wiki.videolan.org/SoC_2007\n\nWinLibre: WinLibre Control Center GUI. Why not with Qt?\nhttp://www.winlibre.com/wiki/doku.php?id=winlibre_soc_2007:proposals_for_the_google_summer_of_code_program_2007\n\nX.Org: Port Qt to XCB\nCreate GUI or textual tool for assisted editing of XKB configuration database.\nhttp://xorg.freedesktop.org/wiki/SummerOfCodeIdeas\n\nXMMS2: XMMS2-specific GUI toolkit widgets for Qt\nhttp://wiki.xmms2.xmms.se/index.php/Summer_of_Code_2007/Proposed_projects\n\nXMPP Standards Foundation: Implement Jabber technologies like \nJingle VoIP, Video, file transfer or encryption in clients like Kopete (or Psi).\nhttp://wiki.jabber.org/index.php/Summer_of_Code_2007\n\n\nI hope this gives some of these projects some exposure (with their respective KDE development opportunities). \nAlso, several projects might help you to actually get one assigned, so why not stick with KDE with your other proposals, too ;)"
    author: "Phase II"
  - subject: "Re: KDE-related opportunities in other projects"
    date: 2007-03-23
    body: "great work o_o;"
    author: "shamaz"
  - subject: "XGL + KDE 3.5"
    date: 2007-03-23
    body: "KDE 4 is still a long way to go. How about better integration between XGL stuff (Compiz/Beryl) and KDE 3.5. I love eyecandy."
    author: "anonymous"
  - subject: "Re: XGL + KDE 3.5"
    date: 2007-03-24
    body: "KDE4 should be this autumn, so it is quite close... \n\nRelease Schedule: http://dot.kde.org/1174481326/"
    author: "Arne Babenhauserheide"
  - subject: "Re: XGL + KDE 3.5"
    date: 2007-03-25
    body: "you mean 4.0 , hrhr , fucking version numbers"
    author: "wingsofdeath"
  - subject: "How about a library for MS office export/import?"
    date: 2007-03-25
    body: "It strikes me that if somebody comes up with a library that can do import/export for\n1. Ms Word\n2. MS Excell\n3. MS PowerPoint\n4. MS visio\n5. MS project\n\nthen it can be used by ALL of the oss world and perhaps the closed source as well.\nIf adapted and MS is not able to be imported/exported by ALL of the apps, then it will be considered MS's fault, not the rest of the world. In addition, it will most likely lead to 1 library that is maintained by all of the users and that is pretty powerful.  \n"
    author: "a.c."
  - subject: "Re: How about a library for MS office export/import?"
    date: 2007-03-25
    body: "If I remember correctly, a couple of years ago this was about to happen. Werner Trobin, at that time KOffice developer, started designing and implementing a library called wv2, a successor to the library AbiWord uses for MS Word conversions.\n\nHowever, the project came to a halt when the AbiWord developers decided not to help on the library (as the initially indicated).\n\nSo while the idea is all nice and shiny, it actually requires developers working on it."
    author: "Universe"
  - subject: "Re: How about a library for MS office export/import?"
    date: 2007-03-25
    body: "Yes, and KWord still uses libwv2 -- it's pretty good, actually. Only one kind of image cannot be loaded. The big problem is that KWord 1.x is constitutionally unable to render the document. Mainly because of some difference in design philosophy, but also because of a broken table implementation.\n\nIn KOffice 2.0 it will be quite easy for someone who's interested in that kind of thing to write a table shape that works like the text shape but for tables. It can be a completely stand-alone project with a high prestige associated :-)."
    author: "Boudewijn Rempt"
  - subject: "hmm"
    date: 2007-03-26
    body: "Getridoffpopups-project"
    author: "Andy"
---
As we <a href="http://dot.kde.org/1173949256/">reported last week</a>, KDE will be taking part in Google's Summer of Code again. The deadline for student applications is approaching now, so be quick sending in your ideas before March 26th. If you are a student and would like to spend the summer on a cool Free Software project, do apply for one of the scholarships.  For more information, please refer to the following pages: <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Participation">how to participate with KDE</a>, <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Ideas">KDE SOC ideas page</a>, <a href="http://code.google.com/soc/">Google Summer of Code page</a>.




<!--break-->

