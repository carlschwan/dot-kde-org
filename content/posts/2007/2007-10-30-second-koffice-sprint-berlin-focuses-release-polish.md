---
title: "Second KOffice Sprint in Berlin Focuses on Release, Polish"
date:    2007-10-30
authors:
  - "irempt"
slug:    second-koffice-sprint-berlin-focuses-release-polish
comments:
  - subject: "Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "It would be cool if 3D effects could be added to KPresenter such as these:\nhttp://movies.apple.com/movies/us/apple/mac/iwork/2007/features/iwork_features_newtransitions_20070807_r640-10cie.mov\nhttp://movies.apple.com/movies/us/apple/mac/iwork/2007/features/iwork_features_smartbuilds_20070807_r640-10cie.mov\n\nAnd we do have the technology :)\nhttp://labs.trolltech.com/page/Graphics/Examples\n\nIf anybody has the C++ knowledge and time to work on it, please do so :)\nIt would really make Koffice stand out (beside all the great work that's already been done), that's the kind of stuff like Compiz/Fusion that gets people attention.\n"
    author: "Patcito"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "i actually sat down (ok, i couldn't do much else. i was in an airplane over the atlantic and my laptop battery was dead...) and calculated how many hours in a year i spend twiddling with slides.\n\ni just about puked, and i'm not one who gets airsick.\n\nmy wish for kpresenter is an app that puts the workflow of creating a presentation as the primary feature: outliner, notes, templates and selectable dynamic layouts. done right, you could get a very nice effects system on top of that with probably even less work.\n\nbut yeah, i'm pretty much ready to never manually position another item on a slide every again in my life. bleh."
    author: "Aaron J. Seigo"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "I agree. Presentation software centers on the WYSIWYG view mostly out of habit... mostly I want to write an outline and then spend 5 minutes at the end tweaking with the look.\n\nThough I suppose my vote for flashy effects would be to have Beamer-like presentations without needing to know LaTeX. :)"
    author: "Ian Monroe"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "Added to http://wiki.koffice.org/index.php?title=JJ. It should be pretty easy to do, actually, even if KOffice 2.0 i really big on manually putting stuff in place."
    author: "Boudewijn Rempt"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "While I agree with the advantages of content-based document creation (it's why I use LaTeX to write papers), for presentations I'd guess it would be most helpful for text-heavy presentations. Most computing presentations seem to fall in this category, but certainly there are many other fields (e.g., biology) where most of the content is graphical (figures, images, etc.). Those are areas where manual placement of material can really help to achieve things that a more rigid LaTeX-like layout engine has a hard time handling. I tried using LaTeX for my presentations for a while, but ended up fighting it more than benefiting from it. (Even the LaTeX manual, IIRC, says LaTeX is not always the best choice in such cases.) \n\nSo, thanks for the current manual-positioning capabilities! If a more structured mode gets added, I hope that the ability to twiddle doesn't go away.\n\nI also use only boring transitions (so the special effects don't end up distracting people from the content of my presentation), but I fully agree that they'd add a wow factor that might impress new users, and be useful in some circumstances.\n\nSadly, I still use PowerPoint under Wine, because I haven't yet found an alternative that integrates multimedia (sound & video). I don't know, but I'm hoping that this is one of the things on the slate for KPresenter 2.0. If so, I'm looking forward to it!\n\nTo all KOffice developers: thanks for the outstanding work!\n"
    author: "T"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "I use videos in OO.o... works reasonably well. Maybe worth a try?"
    author: "jospoortvliet"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "I would not call videos working reasonably well in OpenOffice (version 2.3, Ubuntu Gutsy Gibbon). As far as I know, there is no possibility to start videos during presentations by clicking on them in slides. Instead, OpenOffice starts videos automatically when entering new slides. Also I don't think that it is possible to have videos looping endlessly."
    author: "tuxo"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-31
    body: "Hmm, that's true. they basically work, but not reasonably well ;-)"
    author: "jospoortvliet"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "Have you seen Beamer presentations though? They are always a higher quality then other presentations. Personally I rather dislike LaTeX, but if you write your papers with it..."
    author: "Ian Monroe"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "yes, i have even used LaTeX Beamer. the problems are this:\n\n- the process is still more complicated than it should be for such a thing. yes, i want point-and-click here, if only for all the others marooned on this same island of \"i hate the powerpoint model\".\n\n- the slides are too monotonic in look, and the resulting presentations are often pretty boring looking even if they can be elegant.\n\n- as someone noted above, they are best for text heavy presentations\n\nit is completely possible to provide a templating system that takes into consideration graphics and other media on slides, effects and other eye candy and general jazz in a point and click interface.\n\ni actually sat down and design such a system on paper once (during that same air flight ;) i just don't have the time right now to write another application (gotta finish the ones i've currently got started.. you may have heard about them)"
    author: "Aaron J. Seigo"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "> i actually sat down and design such a system on paper once\n\nPlease do digitize them :)\nJust scanning (or send them to me for scanning) would do.  But you can use wiki.koffice.org if you want.\n\nI'd love to see your views on this subject as someone that both knows design and actually eats this dogfood a lot :)"
    author: "Thomas Zander"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-30
    body: "Yeah! We want to see it!"
    author: "Boudewijn Rempt"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-11-02
    body: "You might want use latex with the latest version of powerdot. You may like it."
    author: "Ask"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-31
    body: "I think this is a sign that the idea communication paradigm needs to be changed. There should be a connection from the content to the presentation...\n\nRight now I'm being fairly conceptual but I think one should be able to write all the content go from the content to automatically generating a visual wiether it is a poster for an event [along with a distrubtion list], to a presentation [based on an academic or white paper]etc.\n\nDo we know how and why people use office tools? I really like the idea of a koffice workspace. It begins to introduce the idea that documents have a contextual grouping. Its a different concept to doing computer based work. \n\nI think microsoft is trying to touch on this with Sharepoint, Infopath, and Smarttags..not just individual tools but ways to communicate connected ideas. \n\nAre there any other thoughts?"
    author: "steven"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-10-31
    body: "Slides presentations are overrated, anyway.  Effects designed by George Lucas cannot save a poor presentation.  A really great presentation requires a speaker who can keep the audience interested while presenting the ideas at hand.  I've seen plenty of presentations with fancy (but pointless and distracting) graphics and animations, that otherwise sucked because the speaker was just reading slides to me.  If you have to show me diagrams or screenshots or some such visual aid, fine, but I can read for myself.  Sadly, it seems that presentation software has become a crutch for people that otherwise suck at public speaking.  (Not referring to anyone here in particular, just generalizing based on my personal experience.)  Sorry for the rant.  My point is simply that fancy effects should be a secondary concern."
    author: "Louis"
  - subject: "Re: Keynote like effect for Kpresenter"
    date: 2007-11-01
    body: "I hope kpresent could use semantik mindmap software somehow as preferens. It is great application when sitting on lessons and taking notes what belongs to where and even greater when you just want fast type something what is in your mind and later do somekind presentation from it..."
    author: "Fri13"
  - subject: "Where is the KOffice chicks?"
    date: 2007-10-30
    body: "KOffice boy's club?"
    author: "Johanes"
  - subject: "Re: Where is the KOffice chicks?"
    date: 2007-10-30
    body: "This is an open source phenomenon, KOffice isn't unique."
    author: "Ian Monroe"
  - subject: "Re: Where is the KOffice chicks?"
    date: 2007-10-31
    body: "Here you go: http://openclipart.org/media/files/johnny_automatic/4986"
    author: "a.c."
  - subject: "Re: Where is the KOffice chicks?"
    date: 2007-11-01
    body: "Are you shure that Inge is a Boy?\n\nKaa"
    author: "Kay"
  - subject: "Re: Where is the KOffice chicks?"
    date: 2007-11-01
    body: "He is actually a full grown man. Typical Scandinavian, blond, tall, drinking lots of beer, carrying a Viking battle axe around.\n\nWell, no battle axe at KOffice sprints or KDE conference, since air travel safety restrictions do not allow declaring battle axes as hand luggage.\n"
    author: "Kevin Krammer"
  - subject: "Not presentations?"
    date: 2007-10-30
    body: "What do you mean... KPresenter not in the release?"
    author: "Axl"
  - subject: "Re: Not presentations?"
    date: 2007-10-30
    body: "It's more Kivio which might not make the cut and might not be in next release. On a side note, even if kpresenter doesn't make it in 2.0, it will be back in 2.1, and in the mean time, if you need kpresenter you still can use the 1.6 version. There is no point in delaying the release for just one application."
    author: "Cyrille Berger"
  - subject: "kexi - calculation fields"
    date: 2007-10-30
    body: "Hi guys,\n\nWhat I want is calculation fields in Kexi.  This is ALL I want.  Just let me, as in Filemaker, put a field in a form, and have that field be the result of arithmetic operations on other fields.\n\nLike, I have 10k records of sales transactions.  They have an item number, description and a date and category of item.  I just want to make a summary, showing total sales by month by item.  I need to do ifs as well.  So I need to be able to show book sales by month in total - not just the sales of each book, but the total for the category book.\n\nI know this would take me 5 mins in any version of filemaker after 2.0.  Well maybe earlier even.  I have no idea how to do it in Kexi.  Please help!  Or if it is easy already, put it in the docs how to do it.  If only I were a programmer at the level of you guys, I would get the source and add it.  But I am not.  It cannot take you more than a couple of days.  Just do it.  We will be eternally grateful, make large contributions to KDE, tell all our friends how nice you guys are, promise never to complain about bouncing progress indicators or horrible pastels again....  or anything else you ask!\n\nBest wishes,\n\nPeter"
    author: "peter"
  - subject: "Re: kexi - calculation fields"
    date: 2007-10-31
    body: "Hi Peter,\nThe dot is not a place for such detailed reports or wishes. If you indeed wash to participate in the project, please use bugs.kde.org and dicuss on kexi@kde.org mailing list.\n\n"
    author: "Jaroslaw Staniek"
  - subject: "Chicks are too busy"
    date: 2007-11-02
    body: "Women will always be just very few.\n\nMany grown women like to have stability, a career and childrens. \n\nAnd a mom will never be a hacker, this take to many time.\n\nTop-notch developers spend their lives on development, \n\nIf some of the people who readme have worked with a women, remember: she have a total passion or dedication to development or prefer a more diversified lives?\n\nI think this is the real reason: free software is a men's club\n"
    author: "Marian"
  - subject: "Re: Chicks are too busy"
    date: 2007-11-02
    body: "> And a mom will never be a hacker\n\nNonsense, Anne-Marie Mahfouf is both"
    author: "Kevin Krammer"
---
This weekend, ten KOffice hackers congregated once again in the
hospitable Berlin KDAB headquarters. KOffice has come a long way in
six months: all the groundwork has been laid for the new version, KOffice 2.0. From
Krita to KPresenter, KWord to KSpread, KChart to Karbon, KPlato to Kexi, and from KFormula to Kivio, the big underlying frameworks are ready. This meeting was called to decide on common look
&amp; feel issues and a release plan and schedule.











<!--break-->
<p>For a long time, the KOffice team has worked towards a release
around the new year 2008. The meeting decided that it was a good idea
to aim for a slightly later release near the end of Q1. This gives us
time to work with the distributions to ensure that their needs are
met, and also to make KOffice take a more prominent role in some of
them.</p>

<p><a href="http://static.kdenews.org/dannya/koffice_sprint2/groupphoto.jpg"><img src="http://static.kdenews.org/dannya/koffice_sprint2/groupphoto-medium.jpg" alt="" width="640" height="480" /></a><br />
Happy hackers: Jaroslaw Staniek, Marijn Kruisselbrink, Sven Langkamp, Inge Wallin, Mirko Boehm, Boudewijn Rempt<br />
Martin Pfeiffer, Johannes Simon, Sebastian Sauer, Thorsten Zachmann</p>

<p>Given that all the big architecture work has been done, it was also
decided to try and release every six months after that, keeping
synchronized with Kubuntu and possibly other distributions releasing
semi-annually.</p>

<p>Unfortunately, the first release in the KOffice 2 series might not
contain all the old KOffice applications. There remains a lot of work
to be done, especially on KPresenter and Kivio! More help is
definitely needed here. The <a
href="http://www.koffice.org/getinvolved/junior-jobs.php">Junior
Jobs</a> page has been resurrected and will be filled in the coming
week with enticing little tasks any aspiring hacker might be proud to
attempt. This has been shown to be a great way to get into KOffice hacking
in the past, so do not hesitate to contact us either at
koffice-devel@kde.org or #koffice on irc.kde.org.</p>

<p>On the tangible results front, lots of work has been done on
identifying issues where the KOffice applications duplicate code. For
<a href="http://create.freedesktop.org">Create Project</a> resources
like patterns, gradients and colour palettes, a common loading
infrastructure is already in development. And a KOffice 2 Application
Style Guide, an extension to the <a
href="http://wiki.openusability.org/guidelines">OpenUsability
KDE HIG</a> has been started to aid developers in doing the right
thing.</p>

<a href="http://static.kdenews.org/dannya/koffice_sprint2/tabletcomputer.jpg"><img src="http://static.kdenews.org/dannya/koffice_sprint2/tabletcomputer-medium.jpg" alt="" width="640" height="476"></a>

<p>Of course, the intangibles are just as important: meetings like
this are great opportunities to meet new KOffice developers in real
life. Since the KDE e.V has the rule that each meeting should also
feature 10% new developers, this virtually ensures that new people
will be taken into the community in an exciting way. This year's new
developer was Johannes Simon, who has done work on KChart and the
libraries. Another developer who took the chance to meet his online
friends in person was Sven Langkamp, who is a long-time Krita
developer, but had never met the other KOffice hackers.</p>

<p>Finally we would like to thank again our sponsors: <a href="http://ev.kde.org/">KDE e.V.</a> for taking care of the travel and hotel costs for eight hackers, and <a href="http://kdab.net/">KDAB</a> for the
wonderful hacking location and the great food. Thank you!</p>










