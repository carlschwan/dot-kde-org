---
title: "LinuxWorld.com.au: KOffice 2 on Windows"
date:    2007-10-10
authors:
  - "rjohnson"
slug:    linuxworldcomau-koffice-2-windows
comments:
  - subject: "Heartening"
    date: 2007-10-10
    body: "It is heartening to see the work going into KOffice at the moment.  In some ways, this is more exciting than KDE 4 itself. \n\nPorting to Windows will hopefully gain the suite more developers and also strengthen the OASIS format's position. Now that the suite's architecture is coming together, there should be a platform to build upon.\n\nAs per my other recent comments, I hope that the KOffice developers look seriously at the innovations that were in Lotus Smartsuite, both in terms of features and usability. If they can build something as elegant, responsive and intuitive as Lotus, they will gain users from OO.org and, far more importantly, from Microsoft. \n\nGo for it, Thomas Zander and team!  :-)"
    author: "The Vicar"
  - subject: "Re: Heartening"
    date: 2007-12-20
    body: "I know this is wayyyy late to mention, but I couldn't agree more.\n\n\nGO KOFFICE GO!"
    author: "JayBee"
  - subject: "does that...."
    date: 2007-10-10
    body: "\"potential to challenge its dominance\"... does this means we'll finally\nsee some more compatibility with odf and windows formats?"
    author: "mark"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "As per my previous comment, I certainly hope it means compatibility with ODF.  If OO.org, the new IBM suite (which I understand is based on OO.org), Google and KOffice all start to really support ODF, it will help to undermine Microsoft's proprietary formats, especially if Governments and archive offices start demanding it more and more as well.  The latter will point to those suites already using ODF and pressure MS to follow suit."
    author: "The Vicar"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "IBM's new office suite is their own codebase but with OOo import/export filters for ODF. It's not really much of OOo."
    author: "Matt"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "Thanks for explaining, Matt."
    author: "The Vicar"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "Actually it is a fork of OOo version 1, with support for ODF, but the default format is a new proprietary format they invented.  However the code on the whole is still largely the OOo 1 base.\n\nI really want to see the Lotus Symphony UI ported to the OOo 2.3 codebase.\n\nI really want to see KOffice get better filters for MS Office.  That is the deal breaker for me."
    author: "T. J. Brumfield"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "One more proprietary format is exactly what we do NOT need.  What is their rationale behind this?  Are they trying to undermine Sun's success?  I can't imagine IBM being a serious challenger to Microsoft by introducing a new proprietary standard.\n\n\nBy the way, why don't IBM open source Lotus Smartsuite or at least recommence development of it?  Are there technical issues with legacy code or are there licencing problems?  It was the best office suite I have ever used.  From a personal user perspective, Smartsuite '97 is still ahead of OO.org in 2007.  All I would like to see added to it is ODF support and a clean up of some of stability bugs."
    author: "The Vicar"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "IBM plans to be a major contributed to OOo, so I'm hoping many of the Lotus \"improvements\" make it way into upstream OOo, and that we won't need Lotus and the new proprietary format.  I think it exists to make IBM money."
    author: "T. J. Brumfield"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "As I understood it, IBM contributed a little code, but they're not really planning on contributing a lot at all. Which makes sense, as they must've seen how hard it is to get working on the spagetti mess OO.o's codebase is, and how difficult it is to get code accepted by Sun."
    author: "jospoortvliet"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "Who knows, they might give KOffice a chance as a platform to contribute to then. At least it's code is readable and it's developers welcoming contributions."
    author: "Andre"
  - subject: "Re: does that...."
    date: 2007-10-14
    body: "That's not true. IBM promised a team of decent size working full-time in OpenOffice as of OOcon in September (the number was said to be around 30 developers). They haven't yet started doing *any* work though."
    author: "Brush"
  - subject: "ooxml vs odf"
    date: 2007-10-15
    body: "currently we see the struggle for the ISO standardization and IBM plays a strong role here.\n\nEssentially converters to or from ODF shall be independent so that all office apps can use and benefit from them and take ODF as their own language."
    author: "Andy"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "ODF is the standard fileformat from KOffice, and though support in the 1.x series isn't perfect, it's getting much better... Actually, I'd even worry more about OO.o supporting ODF properly, they're rather stubborn when it's about interpretation of the spec (they're kinda \"my way or my way\").\n\nAnd don't count on more support for the proprietary formats, it's a lot of work and nobody in the current KOffice team is willing to do that. So unless you or someone else wants to step up and waste your time, it's not gonna happen anytime soon."
    author: "jospoortvliet"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "I was with you until you said \"waste your time, ...\".  I know that some people think it's a bad idea to support proprietary formats, and I know that most current KOffice developers don't want to do it.  However, calling it a \"waste of time\" is to go too far. I think it would benefit KOffice very much in terms of user base if we got better support for the MS Office file formats.\n\nIf somebody wants to do the work to improve the MSO filters, I for one will welcome it very much."
    author: "Inge Wallin"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "Actually the legacy MS Word file format import filter isn't at all bad, actually, it supports nearly everything. The big problem has always been that KWord didn't support the features encoded in those files, like tables or images well enough. "
    author: "Boudewijn Rempt"
  - subject: "Re: does that...."
    date: 2007-10-11
    body: "This depends on your definition of \"not bad at all\" :\nIf you're meaning \"it just read text as it would do with a plain text file\"\nI can agree, if instead you were meaning \"it can import and export in a\nfairly decent way\" then you must have a rare version of KOffice-4 got\nback from the future, here even the OOo level is sci-fi for KOffice."
    author: "nae"
  - subject: "Re: does that...."
    date: 2007-10-12
    body: "The import filter supports everything except for one type of image for word versions up to 2000. It's just that, as I said, KWord cannot display the imported content very well. You don't have to agree with me, you just have to read the libwv2 code and the filter code in KOffice."
    author: "Boudewijn Rempt"
  - subject: "Re: does that...."
    date: 2007-10-14
    body: "Just import an odf file you wrote in OOo, then save it.\n(or just wait for autosave)...\nOften it's completely scrambled, almost always you need\nto reopen the original OOo file if you were so wise to\ndo a backup."
    author: "nae"
  - subject: "Re: does that...."
    date: 2007-10-10
    body: "\"waste your time\" probably refers to the fact that such work is hard and never done. So you work and work and only get a tiny fraction closer to compatability, and then MS invents a new format :("
    author: "Carewolf"
  - subject: "Koffice is my favorite office suite, but it needs"
    date: 2007-10-10
    body: "improvement yet\n\nKoffice has a lot of things going for it. Its lightweight, integrated, and fast but still has many features I need. Its a one stop shop for documents and images, but some parts of it still need some improvement."
    author: "Skeith"
  - subject: "But I can't get used to it"
    date: 2007-10-10
    body: "Kword orgonizes the document structure as frames and pages, not sections and subsections. This very 'feature' has prevented me from using it effectively, for a long time."
    author: "yzhh"
  - subject: "Re: But I can't get used to it"
    date: 2007-10-10
    body: "Yes, it is trying to incorporate some lightweight DTP features, which are not necessary for general writing, as opposed to document production.\n\nI guess you are aware of LaTeX and graphical front-ends such as Lyx.  They are outstanding for writing academic documents but you may find they are overkill.  There is a learning curve involved as well."
    author: "The Vicar"
  - subject: "Re: But I can't get used to it"
    date: 2007-10-10
    body: "Yes, you are right, for many pages/frames are not useful.  We wont be removing them as others find that useful but we will focus on sections / subsections more in a future release.\n\nMore TeX like thinking is on my roadmap for KWord, but not for KOffice 2.0 (the first of the KOffice2 releases) just yet.\n\nCheers!"
    author: "Thomas Zander"
  - subject: "Re: But I can't get used to it"
    date: 2007-10-10
    body: "Awesome!  A TeX approach sounds great, Thomas.  I am becoming more and more encouraged about this project all the time. Quality typesetting printed output would be great, too."
    author: "The Vicar"
  - subject: "Semantic mode"
    date: 2007-10-10
    body: "How about a semantic mode? The idea is that you press a button, and all the raw formatting controls (\"Bold\", \"font X\", \"indent this paragraph 2 mm\", ...) disappear from the UI, leaving only semantic controls, such as \"Section\", \"Quotation\", \"Emphasised\". Just like in LyX, and very clean! What exactly qualifies as semantic vs. raw formatting should be configurable (though there should be sensible defaults). Actually, I would imagine that you would tie this setting to document templates, so when you load the \"corporate letter\" template, only the relevant formatting options are shown in the UI (\"Sender address\", \"Letter body text\", etc. in this case, and perhaps even \"Bold\", if the template creator had poor taste).\n\nIf you really need to, you can always \"cheat\" and use raw formatting, by temporallily turning off semantic mode. But the raw formatting should then be highlighted in the document when you switch back to semantic mode, and you should optionally (depending on the template settings) be warned when saving the document. This would help keep documents clean and machine parseable (which may be important in an organisational setting) and also help when collaboratively editing a document; no more ever expanding lists of styles (\"Normal\", \"Normal1\", \"plain text\", \"untitled\", ...) that seem to creep into a document when it has been passed back and forth a few times between editors, and no mysterious formatting left in by others.\n\n(All right, I have suggested this before on the dot.)"
    author: "Martin"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "If a LaTeX approach is taken to typesetting, there would be occassions when explicit codes would be needed, just as in typing raw LaTeX into vi or emacs.  There would be occassions when the user would want to turn ligatures for \"ff\" and \"fi\" off.  Maybe some kind of character palette could be used, just as it is already for accented characters, anyway.\n\nOnce the document is written, it could be parsed through LaTeX itself and output as PDF or DVI (to enable onscreen print preview) and then printed, all within KWord itself (using suitable Kpart viewers.)  Is this a feasible approach?"
    author: "The Vicar"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "If you could, I'd appreciate if you can satisfy my curiosity as your example made me curious :)  Why would you want to turn ligatures off for a section of text? (KWord2 automatically generates them)\n\nThanks!"
    author: "Thomas Zander"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "There are certain circumstances where it is not warranted and you can switch them off in LaTeX. I really cannot think of any examples at present.  I'll try to find an example and return to you."
    author: "The Vicar"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "I think I remember reading that the convention was that ligatures should not be used for compound words where the boundary would be on an f. I can't recall any anyway. And I don't know if it is a strong requirement, or convention used only in English typography or if it is applied elsewhere."
    author: "Luciano"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "I think I remember reading that the convention was that ligatures should not be used for compound words where the boundary would be on an f. I can't recall any anyway. And I don't know if it is a strong requirement, or convention used only in English typography or if it is applied elsewhere."
    author: "Luciano"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "Found this in Wikipedia's \"Typographical Ligatures\" article:\n\nSometimes, a ligature crossing the boundary of a composite word (e.g., ff in \"shelfful\"[2]) is considered undesirable, and computer softwares (such as TeX) provide a means of suppressing ligatures.\n\nFurther down in the article:\n\nTeX is an example of a computer typesetting system that makes use of ligatures automatically. The Computer Modern Roman typeface provided with TeX includes the five common ligatures ff, fi, fl, ffi, and ffl. When TeX finds these combinations in a text it substitutes the appropriate ligature, unless overridden by the typesetter. Opinion is divided over whether it is the job of writers or typesetters to decide where to use ligatures."
    author: "The Vicar"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "You can easily suppress a specific ligature for such words already: by adding a ZWNJ (zero width non-joiner, Unicode code point: U+200C) between the two letters where no ligature may be formed. So you can enter: s+h+e+l+f+ZWNJ+f+u+l and you get the result you want.\n"
    author: "Eimai"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "Sure, you can do that. But very few people will know how to typeset the word \"correctly\". Maybe an exception dictionary may be used to make the right choice. How do you enter a zwnj anyway?\n"
    author: "Luciano"
  - subject: "Re: Semantic mode"
    date: 2007-10-11
    body: "In Gedit you have a context menu for example to input these special characters. It's also present in the Qt4 TextEdit demo program, and I hope it will become available across KDE4 text input boxes. If there isn't such a context menu available you can copy the glyph from KCharSelect: U+200C.\n\nAnyway, a dictionary may be too hard to do, since it would probably be never complete, and there's the added complexity for languages like Dutch or German that can assemble different words into one.\n\nChances are more likely that if you're that conscious about the typography in your text, that you know about ZWNJ.\n"
    author: "Eimai"
  - subject: "Re: Semantic mode"
    date: 2007-10-13
    body: "\"In Gedit you have a context menu for example to input these special characters.\"\n\nYou can also press ctrl+U200C and then let go ctrl."
    author: "xyz"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "German for instance has infinite possibilities of compound words. Sometimes a ligature between the last character of the first word and the first character of the second word shouldn't be a ligature, especially if the charcaters before are already a ligature. A common example (new German ortography) is \"Sauerstoffflasche\" (oxygen cylinder). \"Sauerstoff\" has a ligature at \"ff\" and the third \"f\" belonging to the next sylable is spelled separately and therefore should be separated."
    author: "Arnomane"
  - subject: "Re: Semantic mode"
    date: 2007-10-11
    body: "It's ff, not fff - unless you are hold to write \"Schlechtschreibreformdeutsch\". ;) Repeating a consonant three times in a row is such a nasty looking, damn stupid idea..."
    author: "Carlo"
  - subject: "Re: Semantic mode"
    date: 2007-10-11
    body: "That's a misconception actually... \"Sauerstoffflasche\" was always supposed to be spelled with a triple \"f\" and is unaffected by the German spelling reform, just like \"Balletttruppe\" and \"fetttriefend\" (triple consonant followed by another consonant). The change only affects such words as \"Stoffetzen\" (now \"Stofffetzen\") or \"Brennessel\" (now \"Brennnessel\") and of course the famous \"Schifffahrt\" as in \"Donaudampfschifffahrtsgesellschaft\"...\n\n</off-topic>"
    author: "Nils"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "If you have a section written in Turkish for example, or any other language with dotless i. The difference between f+i and f+dotless i is too small when the dot is merged with the f, so in Turkish they don't use that fi ligature.\n\nBut the better option for that is to just let the user tell the program which language he's writing in (or better: a desktop input language switcher, see http://fedoraproject.org/wiki/SIGs/Fonts/Dev/LanguageAwarenessProblem), and let the font do its OpenType magic (and if it's a good font, the ligatures will be disabled for those languages).\n"
    author: "Eimai"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "I'd love you to try out one of the KOffice2 alpha versions (alpha 4 coming out next week together with KDE4-beta3) and tell me what you think about it.\n\nThe reason I ask is because there is a side-panel that already does most of what you seem to want.  The text tool has a sidepanel with 2 tabs that are all about styles. You can ignore the first tab if you want ;) There are paragraph and text styles which come from the document-template.  So if you want 'Sender Address\" etc. you can create those styles and I hope you will share the document with others so they don't have to go through the style-making process (this remind you of TeX yet? :)\n\nSome KOffice users recently started; http://koffice.wikidot.com I think it would be great if people that figure out this stuff write tutorials / hints etc on that wiki. And maybe even upload docs there (or point to them).\n\nCheers!"
    author: "Thomas Zander"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "Sounds good. I'm trying to finish a post-grad thesis at the moment, so I won't have time to play with it much until the end of the year, though.  :-(\n\n\nWhat about typesetting printed output with nice ligatures and fonts.  Is this on your agenda?"
    author: "The Vicar"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "Yes it is (well, its on the agenda of Trolltech, the makers of Qt, which I recently joined). And in fact it already works in a large set of cases. But could certainly be improved.\n\nLigatures, professional printing (WYSIWYG) and various DTP like features are already working.  There are known issues with reading of certain font-files and you'll see so called 'kerning issues' mostly because of fonts not getting read properly by the common libraries."
    author: "Thomas Zander"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "Great!  You are giving me all good news today, Thomas!  :-)"
    author: "The Vicar"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "Thats a reward for me waking up to the first comment on this story which has my name in it :) :)"
    author: "Thomas Zander"
  - subject: "Re: Semantic mode"
    date: 2007-10-10
    body: "Glad to be of service! :-)"
    author: "The Vicar"
  - subject: "Re: But I can't get used to it"
    date: 2007-10-10
    body: "I don't like the page structure but I love frames. I just wish they were steroid-laden frames like in Framemaker. Heck, I just wish Framemaker were available for free *nix. "
    author: "David Johnson"
  - subject: "Re: But I can't get used to it"
    date: 2007-10-10
    body: "One thing I really liked in Framemaker were the abundant options in anchored frames.  I made a LOT of those available in KOffice2, the need some more UI tweaking, but the actual layouting code is in place already.\n\nThis means that you can anchor a frame in text and have it aligned on the binding-side of the page. Which is great for advanced layouted books :)"
    author: "Thomas Zander"
  - subject: "waste of time"
    date: 2007-10-10
    body: "I personally consider the porting of Koffice to windows, and even it's development to be a complete waste of time.  It is something that I consider to be the downfall of the linux community.  And that is, instead of working together on one project, they have multiple of projects focusing on the same thing.  The Koffice team would be better working with OpenOffice, or finding another project.  I'd rather have one application that works bloody good, than a multiple of projects creating the same sort of application, but are all mostly useless and crap.  Come on people, find a strong project and work on it, rather than trying to progress likewise projects.\n\nI will personally never use Koffice, I find OpenOffice to be far better."
    author: "Richard"
  - subject: "Re: waste of time"
    date: 2007-10-10
    body: "First, development of KOffice is not a waste of time, why?\n1. KOffice was started before When Sun released open source version of StarOffice - or OpenOffice.org.\n2. OpenOffice.org's codebase is far more complex to a new developer to start  (You can imagine, it's codebase started somewhere in 80's with Star Office). Many parts of KOffice 2.0 are rewritten from scratch and hence it provides even more structured code of KOffice.\n3. OpenOffice.org is nowhere near lightweight, and some people really need lightweight office suite (here KOffice comes).\n\nand second, you can't ask people to work on a specific open source project because they work on their free time.\n\nthird, if KOffice (and other KDE 4 applications in general) is available on Windows / other platforms, it will attract more developers and maybe in few years we can have enough developers to catch with OOo ;)"
    author: "fyanardi"
  - subject: "Re: waste of time"
    date: 2007-10-10
    body: "Ditto what fyanardi states above.  There is also the argument about putting all of one's eggs in one basket.  From what I understand, the OO.org code is so convoluted that not many people really grasp its full complexity.  If it no longer suited Sun to sponsor development, the project would be in huge trouble.\n\nI don't mind multiple approaches to the problem of developing an office suite by different teams.  Yes, Koffice lacked momentum before but I hope it will gain traction now. My only request is that all different open source office projects should standardise on using ODF as the default format.  It is open, accessible and relatively well documented and is not controlled by any one company.  By all means, start as many office projects as you like, but please use this to save your document files in."
    author: "The Vicar"
  - subject: "Re: waste of time"
    date: 2007-10-10
    body: "And fourth: Never say never!"
    author: "Volker"
  - subject: "Re: waste of time"
    date: 2007-10-11
    body: "Ever"
    author: "Kane"
  - subject: "Re: waste of time"
    date: 2007-10-13
    body: "Amen!"
    author: "Ivan"
  - subject: "Re: waste of time"
    date: 2007-10-10
    body: "OOo has performance issues, and includes Java for no good reason.  The two major benefits of OOo is multiplatform support, and decent MS Office support.\n\nKOffice is getting multiplatform support, and if improves on MS Office filters, it will surpass OOo in almost all regards.\n\nAnd if I were a dev, I'd be more interested in working on KOffice because of the way Sun handles OOo development.  They're driving away good developers and good contributions."
    author: "T. J. Brumfield"
  - subject: "Re: waste of time"
    date: 2007-10-10
    body: "Fortunately what you do personally does not have any impact on KDE and KOffice. Personally I seldom use any office tools except to write the odd letter to the administration twice a year. That does not prevent me to have a broader vision of office suites. I tried a small laptop designed for emergent countries, the Classmate PC. It runs KDE but as office suite has OOorg. Let me say that starting this beast is very very slow. Schools (teachers and pupils) do not need a full featured suite, they need a lightweight one. Using ODF will anyway allow to share documents between all compatible suites if needed. \nMost average users will also need only basic features in an office tool. A light suite with strong base working features has a huge market share waiting for it.\nWait, probably you don't use KDE anyway as it's yet another desktop & window manager in Linux world? ..."
    author: "annma"
  - subject: "Re: waste of time"
    date: 2007-10-10
    body: "\"I will personally never use Koffice, I find OpenOffice to be far better.\"\n\nHAHAHAHAHAHA :)\n\nYou made my day, troll!\n\nI am the first to ackknowledge that everything has drawbacks etc..\nbut whoever just blindly shouts in favour of OO just gets my laughs\n\nHAHAHAHAHA :)\n\nActually, his post is a good reason FOR doing exactly that.\nGaining a larger userbase will enable to make KDe more important afterall as well"
    author: "she"
  - subject: "Re: waste of time"
    date: 2007-10-12
    body: "Ack.\n\nGuide him to MS Office, no need to do anything for such an attitude.\nIf such persons prefer the best of all, just tell them to pay for it.\n\nNot even for working for OOo makes sense here as MS Office is better than OOo. And it runs via Wine on Linux as well."
    author: "Philipp"
  - subject: "Re: waste of time"
    date: 2007-10-10
    body: "> I personally consider the porting of Koffice to windows, and even it's development to be a complete waste of time.\n> It is something that I consider to be the downfall of the linux community. And that is, instead of working together on one project,\n> they have multiple of projects focusing on the same thing. The Koffice team would be better working with OpenOffice,\n> or finding another project.\n\ner, sorry you're missing a point from the article here. And I find it rather rude to demand developers to what you propose, and saying it's a downfall. Would it really be a downfall if we have KOffice as alternative (backup), or would it be a downfall if OpenOffice was \"the only horse to bet on\" and something went wrong? It's code is so complex only few want to volunteer working on it. OpenOffice depends on companies to pay developers working on it. From this perspective, KOffice has much a lot of potential.\n\nSo I ask to to consider, is it really a downfall to have an alternative? I agree the community shouldn't have 5 alternatives, but having only 1 option is worse in the long term. You *as user* might find OpenOffice superior, but people *as developers* might think different about this for the above reasons. And without developers we'll be... well you get it. :-)\n\nA similar thing happens with Konqueror (KHTML) vs Firefox (Gecko). Did KDE have to ditch KHTML when Gecko got 10% market share? Because Firefox superior in the eyes of users? Apple evaluated KHTML and Gecko, and choose KHTML as their base for Safari/WebKit. Nokia did the same, and choose Webkit. Trolltech is looking for a HTML engine and chooses WebKit. They see potential in KHTML/WebKit because of the clean code base. In the short term Firefox is superior. In the long term I expect WebKit-based browsers to take over the world, not Gecko ones. Now compare OpenOffice vs KOffice again ;-)"
    author: "Diederik van der Boor"
  - subject: "not only a waste of time"
    date: 2007-10-10
    body: "This also counter productive, and scattering the devs across incompatible platforms:\nhttp://blogs.cnet.com/8301-13505_1-9793052-16.html?part=rss&tag=feed&subj=TheOpenRoad\n\nWell done Koffice team!\n\n"
    author: "***"
  - subject: "Re: not only a waste of time"
    date: 2007-10-11
    body: "> This also counter productive, and scattering the devs across incompatible platforms:\n> http://blogs.cnet.com/8301-13505_1-9793052-16.html?part=rss&tag=feed&subj=TheOpenRoad\n> Well done Koffice team!\n\nOff course Ballmer doesn't mind Open Source on top of Windows. Especially if the Open Source product supports Windows only. Microsoft is good at building roads that lead to Windows, while making the way out harder or next to impossible. (i.e. a better import then export function).\n\nI wonder if Ballmer will still be saying the same after KDE supports Windows. KDE doesn't offer a lock-in strategy to enter Windows, but makes it easier to opt-out from Windows. It removes more barriers to get off Windows.\n\nKDE, Qt and KOffice do this by offering a cross-platform framework that isn't controlled by a Microsoft lock-in agenda. So other software vendors can join in and build their software with the same frameworks. Which leaves their options open to switch to an other platform (like Linux) later. How bad is that? :-)"
    author: "Diederik van der Boor"
  - subject: "Re: not only a waste of time"
    date: 2007-10-11
    body: "> How bad is that? :-)\n\nBoth open-source and commercial software run on top of Windows, while only open-source software run on Linux. For example, the amount of commercial games on Linux is ridiculously low compared to the amount of titles for win32.\n\nAs a result, Windows remains the richest platform in terms of commercial and non-commercial offers. Businesses keep on making better software for win32, open-source developers get divided across platforms, and most computer users keep the standard, less risky platform choice.\n\nThe best bet is not to divide the efforts in porting, but to bring commercial attention (as in testing, developers, education, advertising, services) to the open platforms.\n\nUnfortunately, the development is ego-driven, and most efforts are being thrown into fancy and unreal goals instead of the realistic and useful ones.\n"
    author: " "
  - subject: "Re: not only a waste of time"
    date: 2007-10-12
    body: "We might not agree, but just something that did occur to me:\n\n> The best bet is not to divide the efforts in porting, but to bring commercial\n> attention (as in testing, developers, education, advertising, services)\n> to the open platforms.\n\nThis sounds like the classic problem of \"listen first, speak later\". We're standing on a soap box advocating, but not listening. Only speaking how everyone should be doing stuff our way, but not listening to the needs of the people who'd love to join but can't make that switch yet. If we don't reach out, how can they ever reach out to us?"
    author: "Diederik van der Boor"
  - subject: "Re: not only a waste of time"
    date: 2007-10-14
    body: "Correct me if I'm wrong but wasn't half of the point of (and work that has been done on) KDE4 to create an API layer which basically creates a platform independent foundation for applications to run on top of, so it will not be development for incompatable platforms (apart, obviously, from the development of the API layer which will have to be platform dependent but provide the same hooks for programs on all platforms to use)\n\nSo it follows that if this is the case then when writing code for Koffice on QT4 running on windows you are writing code that will also run for Koffice on QT4 running on Linux.\n\nNow I'm not a programmer so I may have got all of this wrong and if so I apologise but that is my understanding of the goals of KDE4, and of Koffice. "
    author: "Pob"
  - subject: "Re: not only a waste of time"
    date: 2007-10-14
    body: "\"Now I'm not a programmer so I may have got all of this wrong and if so I apologise but that is my understanding of the goals of KDE4, and of Koffice.\"\n\nNo, you are precisely correct :) In theory, this is how it will work, but in practice, there may be some platform-specific quirks that slip through the net, so there may indeed be some \"porting\" required, but it should be fairly trivial."
    author: "Anon"
  - subject: "Re: waste of time"
    date: 2007-10-10
    body: "Sun also require's most all contributors to OpenOffice to transfer the copyright to Sun, so that they can release StarOffice as closed source. Theres been some controversy over that recently as well, with Sun wanting to rewrite some code someone else wrote (under the same license that OO.org is released under) because Sun wanted to be able to relicense it and the person didn't want to let them."
    author: "Sutoka"
  - subject: "Re: waste of time"
    date: 2007-10-12
    body: "yep, worth a read about this issue\nhttp://www.gnome.org/~michael/activity.html#2007-10-02"
    author: "Dolphin-fanatic :)"
  - subject: "Re: waste of time"
    date: 2007-10-11
    body: "Youre absolutely right. The only app thats worth something is Kword.. the others are and have allways been in an embarassing state. Koshell has been ridiculous for years.. its it still in 2,0? "
    author: "Jandersen"
  - subject: "Re: waste of time"
    date: 2007-10-11
    body: "\"I will personally never use Koffice, I find OpenOffice to be far better.\"\n\nWhen KOffice gets better than the competition, I doubt that you can still keep your words!"
    author: "jasper"
  - subject: "Re: waste of time"
    date: 2007-10-12
    body: "Open Source is all about choice !! That's why you'll always find many different programs performing the same tasks "
    author: "George"
  - subject: "Re: waste of time"
    date: 2007-10-12
    body: "I wonder - does the DOT finally got its own troll?\nI think there have been similar posts recently in such stupid directions.\n\nWow, that would again mean that the DOT has become quite important (because otherwise the Troll wouldn't bother). Nice :D"
    author: "Hm...."
  - subject: "Now also covered in Sweden"
    date: 2007-10-10
    body: "An article at idg.se, http://www.idg.se/2.1085/1.124616 , refers to the australian article, so now we see a little bit of coverage for Koffice in Sweden as well=)"
    author: "Peppe Bergqvist"
  - subject: "Re: Now also covered in Sweden"
    date: 2007-10-10
    body: "There is a story on Slashdot as well right now, linking to a Computerworld article.  So there is plenty of exposure today.\n\nOddly, none of the articles prominently pimp flake support, the new music notation, etc."
    author: "T. J. Brumfield"
  - subject: "Re: Now also covered in Sweden"
    date: 2007-10-10
    body: "I read all the comments to that article, and I must say that I'm a bit surprised.  It seems that the knowledge about KOffice is much bigger than I thought. None of the comments asked what KOffice was, and nobody dismissed it out of hand."
    author: "Inge Wallin"
  - subject: "Re: Now also covered in Sweden"
    date: 2007-10-10
    body: "aaah, yes, but KOffice is rather old. And I think it used to have more users and mindshare, before OO.o became FOSS."
    author: "jospoortvliet"
  - subject: "2.0 - Worthy of a try"
    date: 2007-10-10
    body: "At the time I knew what was going on KO 2.0 during \"the road to KDE 4\", I saw some exciting features in KO. By then I was deciding to try 2.0 once it is release. Good job! KDE developers!!"
    author: "rockmen1"
  - subject: "MS Office filters"
    date: 2007-10-10
    body: "I hope that KOffice 2.0 somehow integrates the MS Office 2007 converters from http://odf-converter.sf.net.\nThey already run on Linux and other platforms as stand-alone apps.\n\nIn the beginning of the KOffice 2 lifecycle shipping these Mono-dependant apps might be better than nothing. For later releases KOffice should use the converter Sun is currently developing in C++."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: MS Office filters"
    date: 2007-10-10
    body: "Actually very early in the KOffice 2.0 development cycle Sebastian Sauer made an import script that would use OpenOffice to convert any fileformat to ODF and then load that into KOffice. It's slow, of course, but also convenient. And we don't have to waste time anymore developing support for Microsoft OfficeOpen XML. (Which could easily occupy all our development effort, of course)."
    author: "Boudewijn Rempt"
  - subject: "Re: MS Office filters"
    date: 2007-10-11
    body: "It's just that the Mono-based converters have no or very little dependencies (except Mono, of course). KOffice only has to add the code to run the odf-converter as external application and then load the converted ODF file. I don't think that's lots of work for somebody who knows how to launch an external app via C++."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: MS Office filters"
    date: 2007-10-11
    body: "Do not compromise KDE in any way with disease called MONO!\nThe reason why I am not using GNOME anymore is cos' MONO is integrated in so many applications that it is almost impossible to avoid in GNOME and future looks even worse ...\n\nLeave MONO disease out of KDE realm. Linux community needs one strong desktop cos' GNOME will fail sooner or later under M$crosh$t sword."
    author: "Damijan"
  - subject: "No Mono please"
    date: 2007-10-14
    body: "The Mono should be avoided all costs. If MS is serious about their \"Standard\", they should release a non Mono version. Using Mono or requiring Mono may have very serious consequences on all platforms.\n"
    author: "Ilgaz"
  - subject: "Re: No Mono please"
    date: 2007-10-14
    body: "Yes, not having dependancy on Mono is a major feat of KDE."
    author: "mark"
  - subject: "Re: No Mono please"
    date: 2007-10-14
    body: "I was just talking about an *optional* Mono-based component. Nobody should be forced to use it. It's just that many people need to open and save MS Office files.\nOnce OpenOffice's C++-based converter is ready (OO 3.0), the Mono-based one could be replaced."
    author: "yxxcvsdfbnfgnds"
  - subject: "Re: No Mono please"
    date: 2007-10-21
    body: "> I was just talking about an *optional* Mono-based component. \n\nYep, keep talking. We're not listening."
    author: "Tuju"
  - subject: "Its crap"
    date: 2007-10-15
    body: "It is crqp; does not work; a technology preview, a proof of concept"
    author: "Andy"
  - subject: "What about Pivot tables and Pivot charts?"
    date: 2007-10-10
    body: "OOO can only do pivot tables, known as \"data pilot\". Will these features be supported in Kspread 2? "
    author: "Darryl Whheatley"
  - subject: "Re: What about Pivot tables and Pivot charts?"
    date: 2007-10-11
    body: "I dare to say yes, but I'm also certain it's not going to happen in 2.0."
    author: "Inge Wallin"
  - subject: "How much co-operation is there with GNOME Office?"
    date: 2007-10-11
    body: "Just wondering, do Koffice and Abiword/Gnumeric devs work together on a common backend or must features be implemented independently? e.g. using same architecture for spreadsheet formulae, etc.\n\nAlso, what happened to KGraph, which when looking at an ancient review I saw mentioned? Was it merged into KChart?"
    author: "Parminder Ramesh"
  - subject: "Re: How much co-operation is there with GNOME Offi"
    date: 2007-10-11
    body: "You have used 'backend' term but it is not a silver bullet, I am afraid. There is no clear separation between internal structures and application/gui framework. Amount of work to set standards related to backend-frontend communication would be so huge..."
    author: "Jaroslaw Staniek"
  - subject: "Re: How much co-operation is there with GNOME Offi"
    date: 2007-10-11
    body: "Oh, it's a shame that it couldn't be easier to have a common backend:(\nStill, I guess both camps are doing a good job independently and hopefully there are enough developers to go round for all projects. I already think Koffice is a joy to use for many tasks, especially the zippy speed - excellent!"
    author: "Parminder Ramesh"
  - subject: "Good Work!"
    date: 2007-10-12
    body: "I use KOffice a lot, KWORd mainly (more than I use OOo), and I am satisfied with it because it does what it's purpose is. It is simple (ok, more or less ;-)) and functional. \nThank you four work.\n"
    author: "Me"
  - subject: "Re: Good Work!"
    date: 2007-10-12
    body: "Seconded. It's true that freedomware developers only get about 0.03% of the praise they deserve. Keep up the great work everyone!"
    author: "Alan Denton"
  - subject: "KSpread and Solver"
    date: 2007-10-12
    body: "Does KSpread have a tool similar to the \"Solver\" in MS Excel?"
    author: "Axl"
  - subject: "Re: KSpread and Solver"
    date: 2007-10-13
    body: "There seems to be some code in http://websvn.kde.org/trunk/koffice/kspread/plugins/solver/\n\nBut I don't know of the state of it + I guess a full solver would need quit a lot of code and therefore time which is very limited :-(\n"
    author: "Sebastian Sauer"
  - subject: "Re: KSpread and Solver"
    date: 2007-10-15
    body: "If you want an equation solver, look at Qalculate. It's really amazing!"
    author: "."
  - subject: "Kspread"
    date: 2007-10-16
    body: "Makieng kspread do what you want is next to impossible. I mean, koffice is great and all, but does anybody REALLY use it for serious jobs? I once tried, and I have to say that it was impossible - OO was barely usable, and MS Office, as an only sensible MS app, was perfect."
    author: "szlam"
  - subject: "Re: Kspread"
    date: 2007-10-16
    body: "Yes, they do. We did a survey two years ago and we found hundreds of small-to-medium size companies using KOffice to handle all their needs, from writing catalogs and tenders to producing invoices. We found even more individuals using KOffice to produce their school or university papers, their home budgets, their presentations -- everything.\n\nMyself, and I have to admit I'm biased, because I'm a KOffice developer, did depend heavily on Kivio and KPresenter in my previous job. I used KSpread for planning my home renovation. I've written theology papers using KWord and I'm having fun sketching and painting with Krita.\n\nSo, yes. People are really using KOffice for real jobs, inconceivable as it may be to you."
    author: "Boudewijn Rempt"
---
<a href="http://www.linuxworld.com.au">LinuxWorld.com.au</a> spoke with our very own Sebastian Kügler, and is reporting about the future of porting KDE 4 and its applications to Windows in an article titled <a href="http://www.linuxworld.com.au/index.php?id=1596080362&rid=-50">KDE's Windows Weapon: KOffice 2.0</a>. "<em>With OpenOffice.org receiving a lion's share of commercial support and market awareness for a free office suite, KOffice 2.0 has the potential to challenge its dominance with innovative features and a leaner code base</em>." The article discusses the pros and cons of porting free software to proprietary platforms noting, "<em>there is a community building around KDE on Windows and KDE e.V. sponsored a meeting to help people get the port to Windows going.</em>"

<!--break-->
