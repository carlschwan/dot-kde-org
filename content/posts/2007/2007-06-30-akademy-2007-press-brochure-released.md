---
title: "aKademy 2007 Press Brochure Released"
date:    2007-06-30
authors:
  - "vblanton"
slug:    akademy-2007-press-brochure-released
comments:
  - subject: "press link wrong?"
    date: 2007-07-01
    body: "Over here on kde 3.5.7 I have to delete \"www\""
    author: "Gerry"
  - subject: "Typo on page 12"
    date: 2007-07-01
    body: "There is an unprofessional typo on page 12. Under \"Recent KDE Press Coverage\" the first entry lies in the future. It should be \"11th June 2007 - HIG Hunting Season: Icons\" (instead of 11th July 2007 - HIG Hunting Season: Icons).\n\nApart from that: good work so far :)"
    author: "me"
  - subject: "weirdness on page 9?"
    date: 2007-07-01
    body: "Konqueror: Advanced web browsing suite. This where Apple got the\nengine for Safari and Adobe the HTML engine for Apollo.\n"
    author: "Patcito"
  - subject: "Typo"
    date: 2007-07-01
    body: "Page 2: \n\n\"Suppport Page 4\"\n\nThe spell checker does not help with style, but it has its own advantages :-)\n\n"
    author: "outolumo"
  - subject: "About free"
    date: 2007-07-02
    body: "This is a really cool thing to see, and very professional!! :-D\n\nOne thing occurred to me: one question the press document raises is \"why are you doing this for free?\". Perhaps some background about FLOSS and community motivations would be nice to include. :-)"
    author: "Diederik van der Boor"
---
With aKademy 2007 now here, we'd like to announce the immediate availability of the 2007 aKademy KDE Conference Press Brochure available at <a href="http://akademy2007.kde.org/press/">http://akademy2007.kde.org/press/</a> (<a href="http://akademy2007.kde.org/press/aKademy-2007-pressbrochure.pdf">direct link</a>).  The Press Brochure is available for any news organizations covering our annual contributors conference. Read on for more details.


<!--break-->
Inside the brochure is information detailing:
<br>
<ul>
<li>KDE History</li>
<li>KDE 4</li>
<li>KDE e.V.</li>
<li>aKademy schedule</li>
<li>aKademy sponsors</li>
<li>aKademy support/contact</li>
<li>Recent aKademy/KDE press coverage</li>
</ul>
<br>
Additional coverage from the conference will appear on <a href="http://dot.kde.org/">KDE Dot News</a> at regular intervals throughout the week.

