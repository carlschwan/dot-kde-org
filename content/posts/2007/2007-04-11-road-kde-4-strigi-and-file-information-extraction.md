---
title: "The Road to KDE 4: Strigi and File Information Extraction"
date:    2007-04-11
authors:
  - "tunrau"
slug:    road-kde-4-strigi-and-file-information-extraction
comments:
  - subject: "Thank you"
    date: 2007-04-11
    body: "Thank you Troy for another great article about interesting technology behind KDE4.It has become a habit to read these series every week, and I was very happy to see this new article about Strigi today.\n\nOnce again, thank you, and keep up the good work."
    author: "Lans"
  - subject: "Re: Thank you"
    date: 2007-04-11
    body: "Yes thank you!\n\nThis is kind a more abstract information than usual,\nbut to me it is one of the coolest aspects of the coming KDE4! B)"
    author: "Darkelve"
  - subject: "Re: Thank you"
    date: 2007-04-12
    body: "Yeah, I actually moved during the break here, and haven't had a net connection until recently... no net means no SVN builds... therefor I had to choose a more abstract topic.  Thankfully Jos was very helpful in answering all the questions I had while preparing the article :)\n\nI can't guarantee it'll be weekly articles (at least for the next few weeks as I'm now in my exam block at the uni) but I'll try to keep 'em rolling...\n\nYou're welcome :)"
    author: "Troy Unrau"
  - subject: "Re: Thank you"
    date: 2007-04-12
    body: "\"I can't guarantee it'll be weekly articles (at least for the next few weeks as I'm now in my exam block at the uni) but I'll try to keep 'em rolling...\"\n\nWell, good luck on the exams."
    author: "Darkelve"
  - subject: "No mix of files and dirs please."
    date: 2007-04-11
    body: "Hi there,\n\nplease don't mix files and directories. You will create tremendous confusion. \n\nI do see that I should be able to use tar://path/to/tarfile and file://path/to/tarfile and it sure would be nice, if there were a way to find their relation by means open a double-click, open action in Dolpin of KDE4.\n\nIf done correctly, up of the file browser Dolphin would switch to the file:// protocol again.\n\nUnsolved, forever, is the nesting of IO-Slaves, isn't it? What if want to do tar IO-slave over ssh? fish://path/to/tarfile, can't be browsed with tar:// can it? The chaining of IO-Slaves would be nice.\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-11
    body: "Strigi partially solves this problem. You can do e.g.\n jstreams://message.eml/data.zip/familytree.tar.gz/mother.jpg\n\n"
    author: "Jos van den Oever"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-11
    body: "What does it mean, partially? IOW what are the limits of that concept using Strigi? If done correctly we might finally fix bug 73821, which would be really cool.\n"
    author: "Michal"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-11
    body: "Strigi reads files as a stream. This fits very well with kioslaves. On its own, Strigi can read embedded files from other files. To read files embedded in other files that are read over a network protocol like ftp is a bit more tricky. There would need to be a way to really nest kioslaves to make that possible."
    author: "Jos van den Oever"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-12
    body: "I don't get it. I can do this stuff already without using Strigi on KDE 3? It sounds like nothing new at all. KIO slaves have rocked a long time, and being able to navigate tar's and other archives right from your trusted Konq is a very old feature. Works like a charm."
    author: "Andr\u00e9"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-13
    body: "Yes and no.  You can do some of what Strigi does in KDE 3, but it's slower than using Strigi, and you can't extract the same detail of information (the infrastructure is not there).  For example, you can navigate a tarball in KDE 3, but you can't pull embedded thumbnails out of images when browsing within a tarball...  Strigi can do that, and fast..."
    author: "Troy Unrau"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-11
    body: "Please, don't bring up the Dolphin debate again.  Let's focus on fixing Konqueror first."
    author: "KDE User"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-11
    body: "I used Dolphin (the IO-Slave shell) to specifically express that I regard the job as different from what Konqueror (the IO-Slave and KPart shell) would do, like using a KPart for tar and an IO-Slave for the ssh.\n\nThat said I didn't think of bringing up the pointless debate. Which is btw mostly pointless, because it's about a decision already done, and only about people not understanding what it is.\n\nAnd other than that, the debate is by itself not bad. I think it helps to show the developers that the \"people\" (the mass of slightly informed users) really appreciate Konqueror and want it to stay and want to see continuation of this success story. \n\nSo please don't police my use of \"Dolphin\". While I feel that it was not well communicated by the developers, I do feel and appreciate the role of it. And my wish is exactly a point where Konqueror and Dolphin should behave different. In Konqueror browsing a tar should open a complex KPart with all the details, and in Dolphin it should be like browsing the files inside the tar.\n\nBah, I really am annoyed to see this police style of comment.\n\nThanks,\nKay"
    author: "Debian User"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-12
    body: "Sorry, not getting it.  Why should Konqueror get a complicated KPart for this and Dolphin get the polished one?  Who wants a complicated KPart exactly?  I want Konqueror to be awesome, not complicated."
    author: "KDE User"
  - subject: "Re: No mix of files and dirs please."
    date: 2007-04-12
    body: "du... man complex != complicated.\n\nkonqu may load Ark as a kpart (which translates to \"embed ark in konqu\")\n\ndolphin can _not_ load any kpart, so it's limited to the one file-browsing interface hardcoded into dolphin. Still it uses the same kio-slaves like konqu or all other kde-apps can use (making it possible to dive into a tgz-file e.g.)"
    author: "Thomas"
  - subject: "Comparison"
    date: 2007-04-11
    body: "See the following comparison how efficient Strigi is compared to Beagle:\nhttp://www.kdedevelopers.org/node/2639"
    author: "Diederik van der Boor"
  - subject: "Re: Comparison"
    date: 2007-04-11
    body: "That comparison is old enough that I'm not sure it really still applies, especially given that it exposed some bugs that were causing Strigi to return fewer results than the competition.\n\nStill, everything I've seen regarding performance has been very impressive."
    author: "Matt"
  - subject: "KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-11
    body: "> For example, if you're browsing with Konq, and click on a \n> file within a tarball, and you want to open it in the Gimp,\n> well passing that sort of directory would obviously break the\n> Gimp.\n\nYou can easily view all KIO slaves in non-KDE apps (such as GIMP, Firefox, OpenOffice, even commandline utilities) through KIO-FUSE. It works by mounting remote locations (or tar archives, in your example) into the root filesystem hierarchy:\n\nhttp://kde.ground.cz/tiki-index.php?page=KIO+Fuse+Gateway"
    author: "Bill"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-11
    body: "FUSE is Linux-only (ok, + some BSDs and a hackish Mac OS X-Part). \nKDE, is not."
    author: "Kami"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-11
    body: "Except that of course FUSE is not the solution, what gives?\n\nI already today can use OpenOffice to open files via IO-Slaves. It just takes a temporary file, created behind my back. And why not monitor that file for changes and push these backto the IO-Slave where it came from?\n\nAdmited, a lame work-around, but with inotify its going to work nicely.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-12
    body: "> I already today can use OpenOffice to open files via IO-Slaves.\n> It just takes a temporary file, created behind my back. And\n> why not monitor that file for changes and push these backto\n> the IO-Slave where it came from?\n\nBecause when OpenOffice crashes or misbehaves it leaves your /tmp directory with Gigs of orphaned temporary files.\n\nIt's a pain to make OpenOffice and other non-KDE applications aware of IO slaves, and it's outright impossible to do so in closed-source apps. With FUSE, they don't have to be recompiled or modified at all - they see remote files as a normal local files. So it's great for backward compatibility."
    author: "Bill"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-12
    body: "this already works in KIO, it CAN create a temporary file, monitors it for changes, and uploads the changed file back to the original location. I agree FUSE is cool, but not the cross-platform solution we need."
    author: "superstoned"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-12
    body: "Using FUSE to create a temporary mountpoint is a lot cleaner than creating temporary files and transmitting the changes back. Especially if you're working on a very big file.\n\nBut FUSE isn't on all of KDE's platforms. I think the best solution would be to have FUSE where you can, and temporary files when its not an option."
    author: "ben"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-12
    body: "> I think the best solution would be to have FUSE where you can,\n> and temporary files when its not an option.\n\nExactly!!!!"
    author: "Bill"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-12
    body: "> FUSE is Linux-only (ok, + some BSDs and a hackish Mac OS X-Part).\n> KDE, is not.\n\nFuse already works in Linux, FreeBSD, DesktopBSD and Solaris. I don't see why the users of these systems should be dragged down by the ineptitude of some of the more obscure OS's."
    author: "Bill"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-12
    body: "It doesn't work on Mac OS X and Windows, thus it's still not an option."
    author: "superstoned"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-12
    body: "Will KIO slaves will ever run on Windows/OS X?\n\nimho this is not neccessary.\n\nApplications (Amarok, Krita, Quanta, ...) yes - but KIO-slaves?"
    author: "niko"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-12
    body: "Getting the KIO-Slaves to work on the other platforms should be much easier than getting the applications.  Especially on OS X where the primary differences between OS X and FreeBSD (or OS X vs Darwin+X) exist only with regards to the GUI."
    author: "Sutoka"
  - subject: "Re: KIO-FUSE accesses KIO slaves in non-KDE apps"
    date: 2007-04-16
    body: "> Will KIO slaves will ever run on Windows/OS X?\n\n> imho this is not neccessary.\n\n> Applications (Amarok, Krita, Quanta, ...) yes - but KIO-slaves?\n\nhttp is a KIO-Slave too. Do you want Konqueror on MacOSX or Windows?\n\nGetting the KIO-Slaves to work on *all* the platforms KDE support *IS* necessary."
    author: "Pino Toscano"
  - subject: "io-slave design-problem"
    date: 2007-04-12
    body: "I really like where KDE and its libraries are going. Except for the ioslaves.\n\nIt is becoming very obvious that ioslaves do not belong into kde/gnome/openoffice, but at least one layer deeper than that. They should be part of linux, available everywhere, so that commands like \"less http://kdenews.org\" become possible.\n\nI guess that there is some good reason why this has never happened, but the current state really sucks!"
    author: "ben"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-12
    body: "The main problem is that it gets a lot more difficult to implement at a deeper layer. Even now ioslaves have some complex issues to solve.\n\nOne of them is that most interresting ioslaves may require user interaction.\nlets say \"less http://kdenews.org\" actually works but the given URL requires authentication. \"less\" obviously can't handle that, so what will?\n\nWhat about progress bars? Even small files may take a long time to access. And in many cases \"download, work locally, upload\" is the only reasonable approach to work with a file. \n\n"
    author: "michael"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-12
    body: "I'm pretty much sure all of the issues can be solved.\n\nIt would just need a deeper interaction between a generic library and the above DE / applications. Even the mentioned interactions can be done.\n\nFor sure it is a huge task getting it done in a way every DE / app is accepting it and the flexibility in adding new / adjusting current IO-slaves from one to another KDE version is gone. I.e. it needs to be at least pure LGPL so it cannot be done with Qt.\n\nBut I don't see a fundamental reason why it shouldn't be possible."
    author: "Philipp"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-12
    body: "> I'm pretty much sure all of the issues can be solved.\n\nThere has been at least one attempt for a unified VFS implementation (look for D-VFS in the archives of the xdg mailinglist on freedesktop.org).\n\nHowever, main author/developer got lots of negative feedback from people not associated with any desktop project and gave up.\n\n> it needs to be at least pure LGPL so it cannot be done with Qt\n\nThat's not an issue. Any such system would be out of process, i.e. a single daemon or multiple daemons (one for each connection like KIO slaves, or one for each protocol or one for each host, etc).\n\nSince communication would happen through a specified protocol (also the way KIO slaves work), the licence of each side of the communication does not influence the licencing options of the other."
    author: "Kevin Krammer"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-12
    body: "\"less http://kdenews.org\" IS possible (at least in my gentoo build with some lessopen-trickery).\n\nBut I think you're right, I would also prefer it to have things like that in an DE-independend way."
    author: "Martin Stubenschrott"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-12
    body: "<i>I guess that there is some good reason why this has never happened</i>\n\nYes, it needs kernel support from BSD, Solaris, HP-UX, Linux and Win32. That's a bit challenge to get right. So that's why the KIO slaves are still implemented at a higher level.\n\nPerhaps some of the recent user-space filesystems (FUSE) open a new way to implement this."
    author: "Diederik van der Boor"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-12
    body: ">Yes, it needs kernel support from BSD, Solaris, HP-UX, Linux and Win32.\nIf this would really be the case, then KDE would not be at all possible on these platforms.\nIf you can do it within KDE, then you can do it seperately as well."
    author: "Philipp"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-12
    body: "what?\n\nits possible in kde on all platforms because kde is programmed to do it on all platforms. sure, you can do this with every application, but that means you need to change the code. thats the problem - you just can't change the code of all applications for half a dozen platforms...\n\nso if you want something like kio that works with all applications, you need kernel support."
    author: "ac"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-13
    body: "Kernelsupport? Nonsense. This is mostly pure userspace.\n\nFor sure some support is in the kernel, as some basic network- and filesupport is there for other reasons, but why on earth needs a kernel knowledge about IMAP or EXIF?\nThe kernel doesn't need to know all these different things.\n\nWhat you want is a functionality available on Linux platforms and this can be done with a library as well.\nYou are worried it cannot be used in the bash? For sure bash needs then to be adjusted as well.\n\nYes, we can't change the code of all applications on all platforms. But if you keep it in KDE alone and in Gnome alone and in other Apps, it for sure will never happen.\n"
    author: "Philipp"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-13
    body: "you should read the whole of the thread... \n\njust putting ioslaves into another library will not do anything. noone is going to rewrite their software just because some geeks like to really use networks and such.\n\nthe only way to make ioslaves fully work for non-kde software now (not in 10 years...) is to use kernel extensions. not because the kernel needs to know about imap or whatever, but because its the only common api for filesystems access every application uses. thats what fuse is about, to make things like that easier.\n\n\nrewriting the ioslaves in pure c, with a minimum of depedencies - so that maybe someone else would use it, is not a good idea. the kde devs allready have enough work ahead. and you still won't get less-over-http anytime soon, because these apps arn't maintained anyway."
    author: "ac"
  - subject: "Re: io-slave design-problem"
    date: 2007-04-17
    body: "Since when has KDE limited itself to being only a Linux desktop?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Fear of KDE 4"
    date: 2007-04-11
    body: "With so many changes for KDE 4 I wonder how long will it take to make KDE 4 stable/usable. \nI stopped using Konqueror because it crashes a lot when dealing with embedded multimedia. Kaffeine since a couple of months crashes when I open the playlist and a video is running... I hope we get a stable KDE 4 before jumping to an even cooler KDE 5.\n"
    author: "Gummi Bear"
  - subject: "Re: Fear of KDE 4"
    date: 2007-04-11
    body: "please see this:\nhttp://kaffeine.sourceforge.net/index.php?page=news&details=22\n\nit really was not konquerors' fault. \nmy suggestion for you:\nkplayer , it embeds cleanly - no crashes for me since i used it, caused by kplayer.\n\nfor embedding videos into firefox, my suggestion for you is mplayerplug-in:\nhttp://mplayerplug-in.sourceforge.net/\n\nall in all, that nasty crash when viewing embedded videos with kaffeine (which i personally don't like (ohh, see how lame it is)) was not konquerors fault but a misuse of Xlib in means of multithreading.\n\nhappy hacking"
    author: "litb"
  - subject: "Re: Fear of KDE 4"
    date: 2007-04-12
    body: "Nice of them to have fixed it.  Unfortunately I only use Kaffeine 0.4.3.  After that, I hate the UI.  (Old UI: http://img64.imageshack.us/my.php?image=kaffeine7gu.png )  To prevent the crashes, a small patch of Konqueror is sufficient.  That's obviously not the \"right\" answer, fixing Kaffeine was, but this is nice for those of us using older versions.\n\nThe differences amount to\n+#include <X11/Xlib.h>\nand\n+  XInitThreads(); // fix for kaffeine\n\n(patch file attached)\n\nAgain, not so useful now that Kaffeine is fixed, but it allows me to continue using my preferred 0.4.3"
    author: "MamiyaOtaru"
  - subject: "Re: Fear of KDE 4"
    date: 2007-04-12
    body: "I started to use Codeine after Kaffeine decided to bloath their UI with all kinds of useless stuff..."
    author: "superstoned"
  - subject: "Re: Fear of KDE 4"
    date: 2007-04-11
    body: "Hmmm... That may be caused by poor packaging (if you used binary packages) or extreme CXXFLAGS and LDFLAGS (if you compiled KDE from source). Also, if many othert programs start segfaulting with no aparent reason, check your computer - this may be a hardware failure (faulty cooling, dust, bad capacitors etc) -- but this is less likely."
    author: "Gleb Litvjak"
  - subject: "Re: Fear of KDE 4"
    date: 2007-04-11
    body: "Odd you should say that, mine also did a lot but since Kubuntu edgy I haven't seen that problem at all; if you point out a website and it crashes for me I shall file a bug report."
    author: "David Taylor"
  - subject: "Use KPlayer instead"
    date: 2007-04-12
    body: "Everyone is switching to KPlayer it seems. It's so much more stable, and the default interface is more simple and easier to use. But it also lets you get under the hood and tweak things the way you like them. And it has a multimedia library to organize your stuff. And it uses MPlayer, which means more compatibility with formats, codecs, and so on."
    author: "forget Kaffeine"
  - subject: "new nepomuk name"
    date: 2007-04-11
    body: "i, for one, think the name Kumopen would be great. and it starts with a k \\o/"
    author: "somecoward"
  - subject: "Re: new nepomuk name"
    date: 2007-04-12
    body: "What does Kumopen stand for?"
    author: "Sebastian Tr\u00fcg"
  - subject: "Re: new nepomuk name"
    date: 2007-04-12
    body: "Just Nepomuk backwards.  \n\nAnd the same kind of marketing blunder as the name Kant instead of Kate would have been, BTW. \n\n\n"
    author: "cm"
  - subject: "Re: new nepomuk name"
    date: 2007-04-12
    body: "Nepomuk is not a kde technology, so no point in using a K in the name of it. \nNepomuk is an acronym for \nNetwork Environment for Personal Ontology-based Management of Unified Knowledge"
    author: "whatever noticed"
  - subject: "Re: new nepomuk name"
    date: 2007-04-13
    body: "Hu?  What does that have to do with what I said?  \n\nI only explained how the grandparent poster came up with \"Kumopen\" given the name Nepomuk, \nand said that that would not be a good idea.  \n\nNot that I think the suggestion was meant seriously in the first place ... \n\n"
    author: "cm"
  - subject: "Re: new nepomuk name"
    date: 2007-04-12
    body: "I'm not sure where the name NEPOMUK originated, but looking at it the only thing I could think of was that it read as \"Kumopen\" backwards."
    author: "Anon"
  - subject: "Re: new nepomuk name"
    date: 2007-04-12
    body: "Network Environment for Personal Ontology-based Management of Unified Knowledge"
    author: "whatever noticed"
  - subject: "support Aaron"
    date: 2007-04-11
    body: "i support Aaron on including Nepomuk in Kde 4.0 already - please see the thread on kde-core-devel\n\nhttp://lists.kde.org/?t=117613635500003&r=1&w=2\n\ni does not need to freeze the api , but please include it mandantory, not matter if it works on windows or not."
    author: "funnyfanny"
  - subject: "Try it out in Kubuntu !"
    date: 2007-04-12
    body: "If you are using Kubuntu, go ahead install it and give it a shot. I just did. It looks very, very promising (and already useful)\n\nGreat work!"
    author: "KubuntuUserExMandrake"
  - subject: "kde innovation"
    date: 2007-04-12
    body: "this is a real nice kde innovation. this application nicely bridges the command line and the desktop.\n\n\ni sincerely hope this can become a standard for all unix desktops as desktop search is getting more and more important.\n\nhoooray for kde."
    author: "cies breijs"
  - subject: "Re: kde innovation"
    date: 2007-04-12
    body: "<i>this is a real nice kde innovation. this application nicely bridges the command line and the desktop.\n \n \n i sincerely hope this can become a standard for all unix desktops as desktop search is getting more and more important.</i>\n\nGreat for us KDE users and nice to see them doing this as Apple is updating its technologies for Cocoa devs."
    author: "Marc Driftmeyer"
---
After a short delay due to a heavy dosage of Real Life(tm), I return to bring  you more on the technologies behind KDE 4. This week I am featuring  <a href="http://strigi.sf.net/" title="Strigi">Strigi</a>, an  information extraction subsystem that is being fully deployed for KDE 4.0. KDE has previously had the ability to extract information about files of various types, and has used them in a variety of functional contexts, such as the Properties Dialog. Strigi promises many improvements over  the existing versions. Read on for more...

<!--break-->
<p>Strigi is a library that sits at a lower level than KDE. It is written in C++, and is designed to present a series of generic calls that a program can use to find more information about a given file or files. It is in no way tied to KDE except that the <a href="http://websvn.kde.org/trunk/KDE/kdesupport/strigi" title="development version">development version</a> lives in KDE's SVN repository. It also has search capabilities, which are not really the focus of this article.</p>

<p>The Strigi libraries are used to get information from within files, such as the dimensions of an image, or the length of an audio clip, embedded thumbnails, number of lines in a log, source code licensing info or just to search a text file for a given string. Strigi has other advantages, as it can work inside compressed files, archives, and so forth seamlessly. In fact, it ships a few useful utility programs, called deepgrep and deepfind. These useful command line programs allow you to search for information within binary file formats as easily as using grep or find on plain text files. KDE is inheriting the same libraries, so we also get this unique advantage of being able to pull information out of files that are buried within binary formats, such as .tgz files. There is a prototype kio_jstreams powered by Strigi that treats archives like local folders, allowing you to visit <i>/home/user/tarball.tar.gz/icons/</i> for example... This works great when you are using solely KDE integrated applications, but there are currently problems when mixing with other programs. For example, if you're browsing with Konq, and click on a file within a tarball, and you want to open it in the Gimp, well passing that sort of directory would obviously break the Gimp. So for the time being, this mode of operation is an experimental io_slave only, and will continue to be until these sorts of problems are solved. (The other problem is making a tgz or odp file behave as both a file and a directory simultaneously.)</p>

<p>There are many useful ways that Strigi can return data, once a query has been performed. For example, Jos notes: <i>"The program xmlindexer is useful for extracting data from files in a very efficient manner. Because it outputs xml, it is easy to use from any program. Other search projects such as Beagle and Tracker would greatly benefit from using xmlindexer."</i> The xmlindexer program is a binary, so programs can easily call it externally without having to link to Qt or use C++. That said, there are many ways to directly use the Strigi libraries...</p>

<p>The KDE libraries have had methods of extracting information (such as meta data via KFileMetaInfo) from files before, but in many cases they were either slow, or of limited functionality. With Strigi, we have seen as much as a several-fold increase in speed for extracting data from PNG files. I am not aware of any other speeds tests actually being performed, but the general impression is that it is much faster at retrieving file data than most of the previously existing methods.</p>
  
<p>So in KDE, there are not really any good screenshots to show Strigi in action, as it's really just a library. That's not to say that its effects will be invisible though, as things like the File Properties dialogs are already taking advantage of the Strigi backend to pull the data that was previously provided by KFileMetaInfo. Also, for things like thumbnail and other metadata that is being displayed in the file browsers, Strigi is planned to be used (or already in use in some cases) and preliminary results show massive speed improvements. But so far, this has had little effect on the actual KDE experience to the end user, at least in a visual sense. However, as more KDE subsystems become aware of Strigi, we should start to see more unique and useful uses for all the features that Strigi supports.</p>

<p>For example: One of the biggest benefactors of the Strigi work is <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a>. According to Jos: "Nepomuk is a big European research project on enhancing computer applications to make them semantic and connected.  <a href="http://nepomuk-kde.semanticdesktop.org/">Nepomuk-KDE</a> is the work on a KDE implementation of the standards and ideas that come out of that project. I work together with the people of Nepomuk and especially Sebastian Trueg of Nepomuk-KDE to make sure our work fits together. At the moment Sebastian is writing [an] additional index implementation for  Strigi that is better able to work with semantic data." This project uses a lot of metadata and other file contents (like the text of IRC logs, for example) to provide a easy to use search system for the desktop. NEPOMUK will undergo a name change before its final implementation is set.</p>

<p>So while Strigi does the actual digging through the data, other applications such as the Dolphin/Konqueror, the File Properties Dialog or NEPOMUK are the applications that will see the fruits of this work. At the moment, however, work is mostly focused on porting the previously existing KFilePlugins to use the new backend classes. For status reports on this effort, check out the <a href="http://wiki.kde.org/tiki-index.php?page=Porting+KFilePlugin+Progress" title="Porting KFilePlugins Progress">Porting KFilePlugins Progress</a> page on the kde wiki.</p>

<p>To learn more about Strigi, visit the  <a href="http://strigi.sf.net/" title="website">website</a> or join #strigi on irc.kde.org.</p>

