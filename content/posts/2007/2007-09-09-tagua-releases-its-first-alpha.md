---
title: "Tagua Releases its First Alpha"
date:    2007-09-09
authors:
  - "jpoortvliet"
slug:    tagua-releases-its-first-alpha
comments:
  - subject: "Chess!"
    date: 2007-09-08
    body: "That's great news!. Congratulations to all involved. I hope to see a lot of good looking games based on Tagua in the future.\n\nAnd thanks for the games you chose to implement first. KDE was so desperately in need of a Chess game. Great!\n"
    author: "tobami"
  - subject: "great work!! bravi ragazzi!!"
    date: 2007-09-09
    body: "i'm looking forward to play with these cool kde4 chess game :)"
    author: "killer"
  - subject: "Me too"
    date: 2007-09-09
    body: "Whooho!! Expecially this paragraph made me really happy:\n\n> And boy, does it look good. Everything is animated smoothly,\n> pieces explode, and the move history is very fun to play with.\n\nWhoohoo!! That's what I was hoping for. This rocks, and blows everything else away. I'm really curious how users will experience this kind of game interactivity. I hope more games get animations too (e.g. kbattleship)."
    author: "Diederik van der Boor"
  - subject: "The good news keeps coming"
    date: 2007-09-09
    body: "I like playing Chess on boards once in awhile, but a good looking online chess game with KDE4 might pull me away from them. KDE4 development is just a stream of good news."
    author: "Skeith"
  - subject: "Re: The good news keeps coming"
    date: 2007-09-09
    body: "You should still use a board and make the movements you see on the screen on the board if you don't want to *think differently*."
    author: "J. Pablo Fernandez"
  - subject: "Looks great"
    date: 2007-09-09
    body: "I love the way the board looks!  Great job!  Now if only we can get some artists to make nice looking chess pieces for this...  :)"
    author: "solid_liq"
  - subject: "Re: Looks great"
    date: 2007-09-09
    body: "I've always like the \"Symmetraced\" pieces from Knights.  I wonder if it's possible to \"port\" them somehow.\n\nI was looking at the planned games and was surprised to see that Checkers (English Draughts) wasn't there.  Seems easy compared to chess."
    author: "Louis"
  - subject: "Re: Looks great"
    date: 2007-09-09
    body: "I forgot to mention how great this looks.  Thanks, guys!"
    author: "Louis"
  - subject: "Re: Looks great"
    date: 2008-11-16
    body: "Draughts game is available go to.\nhttp://rucko.com/draught/draught-6.2.tar.gz\nhttp://rucko.com/draught/draught-6.2.lsm"
    author: "Michael Rapaport"
  - subject: "Re: Looks great"
    date: 2008-11-16
    body: "Draughts  game (JAMAICAN Style) is available go to\nhttp://rucko.com/draught/draught-6.2.tar.gz\nhttp://rucko.com/draught/draught-6.2.lsm"
    author: "Michael Rapaport"
  - subject: "No need to list \"Undo/Redo\" twice; Connect button"
    date: 2007-09-09
    body: "There's no need to to list Undo/Redo twice (once under Move List and once in the menu bar at the top). Just leave the one in the menu bar.\n\nAlso, consolidate the Connect/Disconnect button into one button. When the player is already connected, the button should say Disconnect. When the player is not yet connected, the button should say Connect.\n\nIt's finishing touches like these that all KDE applications are in dire need of."
    author: "AC"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "Yeah, finishing touches. Sure. In a first Alpha. Thank you for your comment. \n\nWell, of course you're right to a large extent -- the reduplication of the Undo/Redo buttons is unnecessary, and having one button that toggles the online state is not a bad idea at all.\n\nHowever, the keen observations visible in your last sentence, your astute power of reasoning, your marvelous ability to see the large picture behind all KDE applications toss your whole post into the bin marked \"Trash\"."
    author: "AC too"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "The last part is indeed true. KDE developers (and open source developers in general) are often unpaid coders who don't have a team of usability experts and artists to guide the design of the applications. And even if the programmers understand the deficiencies, they don't do anything about them because the application \"is good enough for them\"."
    author: "AC"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "Now what kind of nonsense is that.\n\nKDE has a usability team that has more requests that i can provide help. For a first Alpha, I am sure these are not entirely grave problems to find.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "Of course, KDE has some competent usability experts, but I feel they don't have much say in application development. Coders tend to stick to their own geekish ways of doing things (read: hundreds of irrelevant config options and redundant buttons) while pooh-poohing the usability experts. Coders should _internalize_ good design practices so that features exhibit good design from their inception and so that user-friendliness is not an afterthought. \n\nMany coders lose interest in applying finishing touches because the element of novelty disappears once a feature is (however sloppily) implemented. That's why it's imperative for good usability practices to be followed from the beginning. First alpha is no excuse - it's better to have a few well thought-out features than than dozens of half-backed, overly complex ones."
    author: "AC"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "\"Coders tend to stick to their own geekish ways of doing things (read: hundreds of irrelevant config options and redundant buttons) while pooh-poohing the usability experts.\"\n\nFor what it's worth: I've hardly seen any thing like that in KDE - if an app looks like that, then it likely hasn't seen any usability review at all.  From what I've seen, devs seem to be quite happy to implement the suggestions given in a usability report (if one exists), although they don't necessary implement every single one of the usability guy's suggestions."
    author: "Anon"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-10
    body: "Just to remain stick to the OT, Tagua has two great developers which have written much code just in order to have a polished and well thought design from the start.\n\nIf one makes me notice that there are seemingly redundant buttons, I would blindly assume there is a reason for it. Maybe it is because a generic Undo/Redo is not the same as a move Undo/Redo.\n\nI bet Tagua 1.0 will rock."
    author: "iax"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "\"Coders tend to stick to their own geekish ways of doing things (read: hundreds of irrelevant config options and redundant buttons) while pooh-poohing the usability experts..\"\n\nAnd what, genius armchair pundit, are your qualifications in application experience and UI design, since you are saying that kde developers don't know what they're doing?\n\nIf you don't actually have any sort of track record, do you mind being less rude."
    author: "Richard Dale"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-11
    body: "I understand that there are better, less rude ways to put forward a point - but I fail to understand your perspective on this issue as well. As harsh as his criticism sounds, it is very true of the state of design that KDE3 is in. Even though many of the end-users are not designers, if they are pointing out basic flaws within designs then that should clue you in that something is very wrong. After all, the applications are designed with the end-user in mind and many of them have been complaining about design issues like this for very long. Now I doubt the final version of Tagua will be like this (likely very much improved) but mistakes like this are very indicative of KDE3's design. I'm sorry but there's just no defending the design of a desktop environment having no uniform or constructed HIG. I hear KDE4 is going to implement one finally, so let's see how far they go with that concept - but it's obvious that KDE has a lot of catching up to do in the design department."
    author: "Yashutooki"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-11
    body: "I'm not complaining about constructive criticisms the KDE UI at all - ones like yours above are very welcome. Only about people being rude for no reason, and making incorrect assumptions about the abilities of people who contribute to the project.\n\nI have worked on KDevelop 3.x and the interface is really messed up by the huge number of toolbar buttons, and excessive menu options because it comes with so many plugins by default. \n\nYou would think it would be a simple job to remove totally pointless buttons like 'enlarge font size' and 'reduce font size'. In fact it is really hard because of the way the KParts component model works - you get all those toolbar buttons from the Kate part whether you like it or not. There are options in the api which are supposed to allow you to control the buttons, but it doesn't seem to work in KDE 3.x. Similarly, every KDE app tends to have about 3-4 settings menus which I find really confusing. Again that is a function of the development framework - you end up with all those menus whether you would prefer them consolidated into one or not. \n\nFortunately, because of the excellent framework you can edit and customise toolbars in any KDE, and the problem with KDevelop can be worked around by doing that.\n\nSo my point is - feel free to comment on the UI of applications, but don't just assume we are idiots are are desperate to clutter up UIs with useless buttons and menus at the first opportunity. Often programmers do have a very good sense of how to do a nice UI design and aesthetics in general. \n\nThe KDE philosophy is that with the right toolkit framework design there is much less need for HIG documents and help from User Interaction experts. Of course if the framework is a bit sub optimal as described above, there isn't much application programmers can do about it.\n\nFinally, the word 'coder' is insulting because it describes a job where someone relatively unskilled, takes a very detailed specification from an 'analyst' and just turns it into code. Nobody in the KDE project works like that, and so they are much more accurately described as 'programmers' or 'developers' as those terms don't have any derogatory connotations."
    author: "Richard Dale"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-11
    body: "I'm sorry if my suggestions/comments seemed rude. My intention was to succinctly point out what I perceived to be deficiencies in UI design and approach to application development.\n\nIt's a natural tendency to compartmentalize to doing what one is best at (eg. coding), and I sometimes fall in that trap too. But instead of getting mad at people who point out this general trend, I had hoped to encourage those KDE developers who read this thread to start to do a little more UI planning (and consulting with others) before going headfirst into coding.\n\nRichard, I'm glad you're already aware of the button, toolbar and setting mess in KDE 3.x. As a user, I would have preferred more time being spent designing a KParts component model where useless features could be easily removed. Even if that means less time spent on developing the said features (fun as that may be).\n\n> There are options in the api which are supposed \n> to allow you to control the buttons, but it doesn't seem\n> to work in KDE 3.x.\n\nGiven that Kate parts are used extensively in a lot of text editors, this is a serious bug.\n\n> Fortunately, because of the excellent framework\n> you can edit and customise toolbars in any KDE\n\nI've found this to be extremely buggy - what is listed in the settings window is rarely what appears on the toolbar (there are some buttons on the toolbar that don't appear in the settings, and vice versa)."
    author: "AC"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-11
    body: "> There are options in the api which are supposed \n > to allow you to control the buttons, but it doesn't seem\n > to work in KDE 3.x.\n \n\"Given that Kate parts are used extensively in a lot of text editors, this is a serious bug.\"\n\nYes, I think there might have been some way of getting it working, but I never managed it.\n\n\" I've found this to be extremely buggy - what is listed in the settings window is rarely what appears on the toolbar (there are some buttons on the toolbar that don't appear in the settings, and vice versa).\"\n\nEditing toolbars has worked well for me. I did have a problem with my customised KDevelop toolbar, where the 'delete file' button would change into something else every now and then.\n\nI needed to customise all the kde edu apps for use with small school kids, and we completely customised the toolbars, made  the icons large 48x48 sizes and put the whole setup in an easily managed kiosk mode profile. Most people wouldn't even know that is possible.\n\nWe'll have to see how well it works in KDE4, but I think the screen shot of Tagua looks pretty good. It doesn't have loads of tiny icons in the toolbar, just a few big clear ones and the whole of KDE4 should look like that. The way the undo/redo buttons and menu options work is governed more by how KActions work than how the application programmers have written the app. You write a tiny amount of code and all that stuff 'just works'. So the advantage of having it in the framework, instead of code duplicated in each app is that you fix it in one place in the library, and it's fixed in all apps.\n"
    author: "Richard Dale"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-11
    body: "It's very comforting to know that the KDE developers realise the issues within the design of KDE3 and are working towards solving it. For what it's worth, I just happened to watch Aaron Seigo's presentation called \"Beautiful Features\" during the last aKademy online and he really has the issues that the project is facing nailed, and his solutions point in the right direction. What only time will tell is how far the audience of that presentation will embrace the concepts he put forward, because if the entire community of KDE developers were to adapt to that - so many people wouldn't be put off by KDE's design. What's very encouraging is that these problems are being recognized and rectified - although I use Gnome most of the time I realise that many of their devs would never stray from their path of development even from criticisms within the community like KDE has done. It's only because KDE4 has made the progress in design as it did that I'm following it so closely.\n\nThe fundamental problem with KDE's application design has always been redundancy in presentation of information, non-descript icons in toolbars which are stacked on top of each other etc. From what I have seen, these elements have already been made obsolete by more thoughtful interface design for KDE4. The important thing that I honestly found that KDE3 developers did not take into account is that when you have a lot of information to present (as do many KDE apps with the loads of features they provide), then a streamlined interface is an absolute must. In any case, there have been a few KDE apps that have had beautiful design and I have no qualms with (like Amarok, Konversation and K3B). Let's see if KDE4 can bring the consistency of well-designed apps like the ones I mentioned to the entire application base - it's no easy task, and however far the KDE devs come it will be a commendable task, so no worries :) "
    author: "Yashutooki"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-12
    body: "How comes that Tagua has anything to do with KDE3?\n\nMoreover, it will have nothing to do with KDE4.0 either.\n\nOn the other hand, it is to receive such suggestions that alphas are published. Just go to the developers and tell them.\n\nFinally, thinking that misplacement of buttons in an alpha is a design issue means not knowing anything about programming. I currently have Tagua here without those \"horrible, evil, astonishingly duplicated buttons\"...\n\nThe screenshot, which contains also the whole change I had to make:\n\nhttp://linuz.sns.it/~iazzi/imgs/tagua_movelist.png\n\nDesign means that new games can be added in a few hours if you keep a good programming pace, and does not involve touching the UI. Tagua's design is wonderful.\n\nThe original post and all others alike are pure crap."
    author: "iax"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "Would you mind, anonyomous coward, giving us your name so we know who is trying to insult us? And perhaps something about your qualifications, if any?"
    author: "Boudewijn"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "Seems like if he/she had the balls to do that, he/she wouldn't have use the 'anonymous coward' alias in the first place.  Just like it says; \"coward.\""
    author: "Louis"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-09
    body: "I entirely agree with you : this is the way most GTK and gnome apps are coded after all. FROM THE BEGINNING, FROM THE ALPHA, THE GUI IS DONE RIGHT. By right, i don't mean feature complete, or even useful. But the GUI only has what it needs, nothing more. Take a look at apps like Transmission, or Sound Converter.\n\nhttp://soundconverter.berlios.de/#screenshots\nhttp://transmission.m0k.org/screenshots.php\n\nThose are GUIs that were done right from the beginning. No useless bloat, no duplication of buttons, menus that does the same thing.\nThis is also the way most OS X cocoa applications are done. This is why beta version of Mac OS X software can get a truly growing base. The GUI is always thought first, sometimes before even writing the backend. "
    author: "Anon"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-10
    body: "Gimp. The original GTK application.  ROTFLOL."
    author: "KDE User"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-10
    body: "Hahahaha. You made my day. GIMP is really one of the applications with the worst GUI ever. Also on my list of the hall-of-shame: phpMyAdmin.\n"
    author: "Michael"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-10
    body: "Oh, yes, the ever so useful Transmission. Thats why I use ktorrent in XFCE and Azureus in OSX. Transmission is useless garbage and is extremely basic. Sure, it looks pretty, but that doesnt help me download linux isos faster.\n\nAnd speaking of beta versions of Mac OS X. how is it supposed to get a growing base when Apple sued the people that posted the Tiger beta online?\n\nFunctionality and openness are far more important aspects of software than pretty looking toolbars.\n\nAnd when you have apps like Tagua which are open, functional, innovative AND look pretty, the developers have really struck a home run."
    author: "DR"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-11
    body: "I wasn't talking of Beta version of the OS itself, but beta version of regular apps. Many popular cocoa apps began their life at the beta stage. \n\nFor transmission, maybe you are the problem. It downloaded all the torrents i gave it without any hassle and a very low footprint. "
    author: "Anon"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-12
    body: "Yes, but ktorrent also has a low footprint, and it supports \"advanced\" features like DHT and web UI. For my uses, Transmission is basically useless. \n\nAnd OS X betas are like Google Betas. In KDE, we have svn and developers are much more open. They release unfinished stuff during the beta stages to get feedback from the community. Tagua is not actually meant to be used to play chess at this point."
    author: "DR"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-10
    body: "Yeah, let's go and tell people who donate their free time to write software we all can use to do that exactly according to our wishes. Sure... I take it you do a better job at this?\n"
    author: "Andr\u00e9"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-11
    body: "No, let's guide these developers who write this software as their hobby for other people and themeselves to make better applications so people can appreciate their work more. Don't look at criticisms so negatively, much that has been said against KDE has been very valid."
    author: "Yashutooki"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-10
    body: "Why would this have to erupt in such a flamewar... Our AC maybe didn't put it very nicely, but he has valid points. The authors of this app are still thinking about this, and some clear suggestions like these should make their life easier.\n\nKDE simply DOES need finishing touches like these, and we have a huge lack of usability ppl willing and capable of suggesting them. Users who are helping should be thanked. The comment by AC seems rude because he's only criticizing, but you could also see that he cares. He thought about it, suggests improvements which are simple to implement. That's good."
    author: "jospoortvliet"
  - subject: "Re: No need to list \"Undo/Redo\" twice; Connect but"
    date: 2007-09-10
    body: "yes, but there are at least two very stupid assumptions in his post, which made it sound like \"flame me\".\n\nOne: this is just not the place to report bugs or ask things, given the developers are not even bound to read this. This is news, not bugzilla.\n\nTwo: I know everyone is used to Google-style betas, where they give a finished product and use \"beta\" to avoid guaranteeing that it will work, but this is an alpha meant _exactly_ to correct big design mistakes and get suggestions from users while the codebase is still changing. So he should just tell his suggestions (or patches...) to developers, not complain as if this was the 1.0.\n\nmy 2c\n"
    author: "iax"
  - subject: "Included in KDE-Games?"
    date: 2007-09-09
    body: "As this is in Alpha, I realise it is probably too late for 4.0 but will it be included in KDE-Games later in the 4.x series?  Chess is definitely the one game I was missing the most from KDE, so I am very grateful for this.  :-)"
    author: "David"
  - subject: "Cool!"
    date: 2007-09-09
    body: "hope chinese chess support is coming soon"
    author: "Cavendish Qi"
  - subject: "Re: Cool!"
    date: 2007-09-09
    body: "it had, for a while. I think it was still kboard at that time.\n\nSeems that they did not have time to port the code, so they stripped it. But the ease of adding new games should make it trivial to recover support for all the games it had some time ago.\n\nThere was also Reversi and Win4 once. While that looks like it's a bit too far, it shows how powerful this tool is...\n\nIf you know the rules and have time to make a good looking theme you can make it yourself."
    author: "iax"
  - subject: "I disagree"
    date: 2007-09-09
    body: "I disagree!\n\nThere have been around boards for chess - even networked - for years! Just try looking after Knights (http://knights.sourceforge.net/) or Eboard (http://www.bergo.eng.br/eboard/).\n"
    author: "Gnupf"
  - subject: "Re: I disagree"
    date: 2007-09-09
    body: "Yes there have been \"boards for chess\", but they either have primitive looking interfaces or are unmaintained, or are not KDE versions.\n\nKnights has not been updated for about 2 or 4 years! and Eboard is GTK+ for starters (and so KDE needs a chess game is still valid), and though actively developed, doesn't look very modern at all.\n\nThere are of course others, like XBoard, glchess and more. But the fact still remains that kdegames needed a modern, good looking chess game.\n\nAnd the great thing is that Tagua should help create all popular board games that kdegames still lacks, and do so in a consistent, beautiful way.\n\nCheers\n"
    author: "tobami"
  - subject: "Can computer strength be adjusted?"
    date: 2007-09-09
    body: "The one feature I missed (or just didn't find?) in Knights was to adjust the strength of the computer opponent. (Beginners hate to loose too often.)\nIs something like that available or intended for Tagua?"
    author: "Joachim"
  - subject: "Re: Can computer strength be adjusted?"
    date: 2007-09-09
    body: "tagua uses external chess engines like Gnuchess or Crafty.\n\nIf those don't have setting for strength, tagua won't probably have, I don't know. Anyway, as the configuration dialog for engines becomes complete, some tweakings should become possible, like deepness of thought.\n\nRight now I can see no tweakings configuration, but the possibility to edit the command line for engines could be more than enough."
    author: "iax"
  - subject: "Re: Can computer strength be adjusted?"
    date: 2007-09-09
    body: "At least Gnuchess does have some options which make it less challenging and also faster. (You can cap the number of nodes searched, the lookahead depth and I think also the time.) It might make sense to show some \"levels\" in the UI which are actually presets for those tweaks."
    author: "Kevin Kofler"
  - subject: "Re: Can computer strength be adjusted?"
    date: 2007-09-09
    body: "yes that is a must. because i had to search on the net to find a realy bad chess engine (actualy it's a flash game), and even that one beats me in 9 of 10 games"
    author: "Beat Wolf"
  - subject: "Re: Can computer strength be adjusted?"
    date: 2007-09-10
    body: "Then it is possible.\n\nIt is up to the guys there to make it possible that you set the engine X with levels N, instead of choosing the engine \"X_N\" from a huge list. But this is usability and will surely come at some point.\n\nWhich reminds me that you could also get hints from the computer, for learning. For now it isn't coded yet, but it is a \"must have\" so this will come too."
    author: "iax"
  - subject: "Re: Can computer strength be adjusted?"
    date: 2007-09-12
    body: "Also - a \"must have\" is the ability to \"take a move back\".  I know \"real\" chess players don't do such novice things - much less against the rules - but those who are trying to learn to play - it is a MUST = should be fairly straight forward to go back and replay the move.  \"Hints\" should be fairly straight forward - the engine plays your turn but puts the result to be displayed as the \"Hint\". Everything that makes good chess programs \"good\" are all the things that teach chess - not that win the game = that is the chess engines responsibility (programmers actually :).  Novice players HATE to loose to the point they quit ever trying to play.  The real challenge is not in winning but in making the game a challenge.  It has already been proven that computers can beat humans - so what - I can't calculate PI to the nth degree in my head but that doesn't make me a looser. But to lose incessantly - over and over - it is beyond the human capability to be STUPID to play a game that can't be beat.  Why try?  So the challenge is not to beat the human (once the engine is programmed to be good enough) the challenge is to make it a challenge. - That is a harder part in programming - random loosing - 90% good moves on 100% wins.  Not 100% calculated moves on 100$ wins. - perhaps the variables need to be changed but a 2nd best or 3rd best move to let the other player capitalize on the mistake.  Otherwise, the player never - NEVER - gets to see opportunities of a missed move.  The most important part is constructive feedback - that is totally lacking in GnuChess.  There is nothing but win or lose and mostly - no always for new players - lose.  That is not trivial but hard to program.\n\nCheers!\n\nHating GnuChess\nBen "
    author: "EzwareZ"
  - subject: "Re: Can computer strength be adjusted?"
    date: 2007-09-17
    body: "there is full history support (see in the screenshot on the left side, you can click on any of these moves and go back)."
    author: "jospoortvliet"
  - subject: "Re: Can computer strength be adjusted?"
    date: 2007-11-23
    body: "> \"take a move back\". I know \"real\" chess players don't do such novice things\n\neven the \"real\" chess players sometimes do not handle the mouse good enough to make the intended move ... so yes, this is a must for any such game engine ;-)"
    author: "kavol"
  - subject: "animations.."
    date: 2007-09-09
    body: "Hi, I like playing KMahjongg and Shisen-sho, so I've been following their KDE4 evolution with anticipation. The new versions already look great, what with the SVG evolution and all... However, a lot of people will compare it to Vista's \"Mahjong Titans\" and it's catchy startup animation and the small effects when matching tiles are found. Is that already in the current KDE4 versions and, if not, in how far could Tagua provie this functionality? (What I'd love is to see a Youtube video showing the effect currently present.\nI know, pretty shallow looking only at eye-candy, but hey, that's what people want these days....\n"
    author: "Uglymike"
  - subject: "If Only...."
    date: 2007-09-10
    body: "Now that my desire for chess is satisfied, I hope some kind KDE card game developers will create a version of Bezique for me....  :-)"
    author: "The Vicar"
  - subject: "Re: If Only...."
    date: 2007-09-17
    body: "If you know the rules it's not that hard to make Tagua support this. Maybe contact the developers and get it going..."
    author: "jospoortvliet"
  - subject: "Is Chess the only boardgame?"
    date: 2007-09-10
    body: "When you mentioned the word \"boardgame\" my eyes went immediately wide open. I thought about the thousand boardgames that are listed in boardgamegeek.com . I thought about playing \"Settlers of catan\", Carcassone, and others, like in www.brettspielwelt.de . But you only think of chess and checkers! Well, it's a beginning, though it could get better. *Much* better."
    author: "David C."
  - subject: "Re: Is Chess the only boardgame?"
    date: 2007-09-10
    body: "You made me gasp with The Settlers. Would definitely be cool."
    author: "Hagai"
  - subject: "Re: Is Chess the only boardgame?"
    date: 2007-09-11
    body: "There is pioneers (I think pioneers.sf.net) but the AI isn't great and there aren't ever any people online to play with (probably because it is linux only)."
    author: "Tim"
  - subject: "Settlers"
    date: 2007-09-11
    body: "I must be the only one on the planet who finds Settlers extremely disappointing.  Other than the fact that it allows me to shout \"I have wood for sheep!\" it is too simplistic for my taste.  I might prefer network play, but when you play with your buddies, you learn their strategies (who goes for longest road, who tries for armies, etc) and what you should or shouldn't trade them.\n\nPersonally, I'd love to see huge, extremely dynamic games like Talisman get ported someday."
    author: "T. J. Brumfield"
  - subject: "Re: Settlers"
    date: 2007-09-11
    body: "Surely their strategies depend on the game situation? Maybe you should get some better friends!"
    author: "Tim"
  - subject: "GGZ compatible ?"
    date: 2007-09-10
    body: "Is this project aiming to be compatible with GGZ  http://www.ggzgamingzone.org/ ?\n\nI hope it grows to be as varied as the now defunct GTKBoard.\n\nhttp://gtkboard.sourceforge.net/indexold.html\n\n\nBest of luck to the developers."
    author: "Senthil"
  - subject: "to when a Game of Go for KDE4 ?"
    date: 2007-09-10
    body: "qgo (http://qgo.sourceforge.net/) is nice but far from the KDE4 style."
    author: "St\u00e9phane Magnenat"
  - subject: "Re: to when a Game of Go for KDE4 ?"
    date: 2007-09-11
    body: "tagua will do it, probably. every PvP information complete board game could finish there. classical ones like Go are the most probable.\n\nThey are actually changing the code to be less Chess-specific, and accomodate other types of game. Remember that at one time they implemented Chain Reaction (like KJumpingCubes) and four-in-a-row..."
    author: "iax"
---
<a href="http://www.tagua-project.org/">Tagua</a>, a generic boardgame for KDE, is approaching version 1.0, and the developers decided it's time to get the word out on this exceptionally cool application by <a href="http://trac.tagua-project.org/wiki/Release1_0_alpha">releasing a first Alpha</a>. Read on for more!



<!--break-->
<p>A little less than two years ago, Paolo Capriotti started developing KBoard, a generic application supporting all kinds of board games. His intent was to create a definitive platform that would make it easy to develop board games and variants. After some time, Maurizio Monge joined the project, contributing with exceptionally innovative ideas. Development got a <a href="http://pcapriotti.wordpress.com/2007/07/18/kboard-an-update/">big boost</a> after <a href="http://akademy2007.kde.org/">aKademy 2007</a>, and currently it doesn't even seem to compare to the old KBoard anymore. The name was thus changed to the more original and less boring "<a href="http://en.wikipedia.org/wiki/Tagua">Tagua</a>".
<br>
<div style="margin: 1ex; padding: 1ex; float: right">
<a href="http://static.kdenews.org/dannya/tagua-chess-big.png"><img align="right" src="http://static.kdenews.org/dannya/tagua-chess-small.png" width="344" height="258"  /></a><br />
<center>Tagua smoothly in action</center>
</div>
</p>

<h3>The current status</h3>
<p>The Free Software world doesn't have a decent, good looking Chess game which is properly network aware, so Tagua is focusing heavily on good looks and online play. Being a generic board game, this of course also benefits all other games built upon it. And boy, does it look good. Everything is animated smoothly, pieces explode, and the move history is very fun to play with. The theme engine is very powerful, so hopes are high that artists will create some great looking things.</p>

<p>Currently it features 2 games (Chess and Shogi) and some variations on them, like 5x5 chess and simpleshogi. But it is very easy to write new games (the 5x5 chess is just 20 lines of simple code) so this is expected to change as well.</p>

<p>Online playing is possible, but some work is still to be done on the usability front. And playing against the AI is already implemented, it just lacks a GUI. So, we can expect some improvements still, but the game is already very playable, and it's time for users to start submitting new themes and implement new games! The developers have promised that the best ones will be shipped with KDE 4, so <a href="http://www.tagua-project.org/wiki/Development">go<a> <a href="http://tagua.ath.cx/doxy/en/html/index.html">nuts</a>.</p>


