---
title: "KOffice 2 Alpha 5 Released"
date:    2007-11-23
authors:
  - "iwallin"
slug:    koffice-2-alpha-5-released
comments:
  - subject: "ODF Library"
    date: 2007-11-23
    body: "I am sooo looking forward to having at least basic ODF I/O capabilities available in a common library.\n\nODF spreadsheet tables instead of all the parsing, escaping/quoting and type guessing currently involved with CSV I/O. *sigh*"
    author: "Kevin Krammer"
  - subject: "Re: ODF Library"
    date: 2007-11-23
    body: "I think that Qt should have ODF classes."
    author: "anonymous"
  - subject: "Re: ODF Library"
    date: 2007-11-24
    body: "> I think that Qt should have ODF classes.\n\nWhy?\n\n\nIsn't a KOffice LGPL library good enough? And if so, why not?"
    author: "Thomas Zander"
  - subject: "Re: ODF Library"
    date: 2007-11-25
    body: "In a way the advantage would be to use simple Qt widgets to modify ODF files\n\nthink of a small pygtk app for example\ni think it would not be a bad idea to consider"
    author: "she"
  - subject: "Re: ODF Library"
    date: 2007-11-26
    body: "Why does py_gtk_ have to do with this?"
    author: "Anonymous Coward"
  - subject: "Re: ODF Library"
    date: 2007-11-26
    body: "He was using that as an example, not literally. Don't be so harsh."
    author: "Ruben Schade"
  - subject: "Goal of Krita"
    date: 2007-11-23
    body: "\"The Krita team is working hard to make Krita the best free pixel based image editor in the world.\"\n\nI propose to change the goal to make Krita simply the best pixel based image editor in the world."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Goal of Krita"
    date: 2007-11-23
    body: "Especially since personally I think Krita 1.6 already was the best free pixel image based editor in the world."
    author: "Ian Monroe"
  - subject: "Re: Goal of Krita"
    date: 2007-11-23
    body: "To be sure Krita is awesome, but the gimp still has more mindshare and features. Still, knowing the impressive team behind Krita, this will not be true for long!"
    author: "Some perspective"
  - subject: "Re: Goal of Krita"
    date: 2007-11-23
    body: "Well yea I did say personally... I can't stand the Gimp's interface."
    author: "Ian Monroe"
  - subject: "Re: Goal of Krita"
    date: 2007-11-25
    body: "The GIMP team had some setbacks, especially with GEGL being dormant so long after the original developer vanished and the code cleanup that lead to 2.0 taking so long. But at least they have even gotten better at listening to their users as the reviews on gimp-brainstorm.blogspot.com demonstrates. \n\nThere are still problems though and the main problems with the GIMP are:\n\nA) That developers have come up with some use cases that don't include original art creation even though if you go to www.gimpusers.com or look at some of the feature requests you find there are  a lot of current GIMP users doing exactly that.\nB) The long time between releases, especially considering how little changes between releases.\n\nKrita 2.0 has some nice features that make it interesting for art creation instead of just photo manipulation and if the GIMP wants to continue playing on that field it will have to add some extra features."
    author: "Imos Anon"
  - subject: "what about tables"
    date: 2007-11-23
    body: "in koffice1 working with tables is really painfull, what about koffice2 ???"
    author: "Rudolhp"
  - subject: "Re: what about tables"
    date: 2007-11-23
    body: "Unless someone steps up and creates a table shape plugin, tables will not be in KOffice 2.0."
    author: "Boudewijn Rempt"
  - subject: "Re: what about tables"
    date: 2007-11-23
    body: "WHAT? There will be KSpread, right?"
    author: "anonymous coward"
  - subject: "Re: what about tables"
    date: 2007-11-23
    body: "Yes, KSpread is in excellent shape. "
    author: "Boudewijn Rempt"
  - subject: "Re: what about tables"
    date: 2007-11-23
    body: "Will it be possible to include a KSpread-shape anywhere in a document? Doesn't this make the traditional \"Table\"-feature (like in MS Word) obsolete?"
    author: "Axl"
  - subject: "Re: what about tables"
    date: 2007-11-23
    body: "Yes, it will be possible to include a kspread shape and no, it doesn't make the standard text shape obsolete.  The reason for that is that a spreadsheet is very oriented towards calculation and a text processor table is oriented towards organizing text.  Right now, we don't have advanced handling of text properties in the spreadsheet cells, although this will possibly change later.  Spreadsheets also don't have things like reoccuring header cells on several pages, which are important in text tables."
    author: "Inge Wallin"
  - subject: "Re: what about tables"
    date: 2007-11-26
    body: ">>The reason for that is that a spreadsheet is very oriented towards calculation and a text processor table is oriented towards organizing text.<<\n\nMicrosoft would disagree with you here: they saw a huge adoption of Excel when they focused on table as a way of organising text..\n\n"
    author: "renoX"
  - subject: "Re: what about tables"
    date: 2007-11-23
    body: "Too bad. I guess I am stuck with bloated OpenOffice forever."
    author: "taxi"
  - subject: "Re: what about tables"
    date: 2007-11-23
    body: "Yeah, well... We can't build Rome overnight. It's likely that we will have a table plugin for 2.1 or 2.2 -- but right now we simply don't have enough time."
    author: "Boudewijn Rempt"
  - subject: "Re: what about tables"
    date: 2007-11-23
    body: "I see. Thanks anyway Boudewijn and all the other Koffice developers for all your efforts!"
    author: "taxi"
  - subject: "Q Note."
    date: 2007-11-23
    body: "This version introduces so called  Chinese Brushes and a new Pat...\n\n\nSo called? Bad choice of wording here Inge. Makes it sound ... hyped ... fake ..."
    author: "AnonymityIsGreat"
  - subject: "Re: Q Note."
    date: 2007-11-23
    body: "of course it is a fake... it's not a real chinese brush .. "
    author: "Thomas"
  - subject: "Re: Q Note."
    date: 2007-11-23
    body: "Well, it could be because I am not an English native speaker.  In Swedish the equivalent of 'so called' here would not have connotations of hype, just a statement of fact that this is what they are called.\n\nOn the other hand, and as another comment stated, they are in fact not real, only simulated or virtual.  Would that have been a better word to use?"
    author: "Inge Wallin"
  - subject: "Re: Q Note."
    date: 2007-11-23
    body: "Don't sweat it.  I am a native speaker and I have no idea what he's talking about."
    author: "MamiyaOtaru"
  - subject: "Re: Q Note."
    date: 2007-11-23
    body: "\"so-called\" at least in my country carries connotations of disputed legitimacy (see eg http://www.thefreedictionary.com/so-called).  A less ambiguous phrase (though more wordy) is \"This version introduces support for Chinese Brush  paint-stroke simulation\""
    author: "picky"
  - subject: "Re: Q Note."
    date: 2007-11-25
    body: "agreed."
    author: "Chani"
  - subject: "Re: Q Note."
    date: 2007-11-25
    body: "Not agreed. Look at your own link again and then at point 1. It doesnt carry any connotations - this is the first and main meaning. There are other meanings apart from that but - at least for me - they dont necessarily imply a \"connotation\", especially when the context is completely clear. What you are hinting at seems to me like interpreting \"The pizza has hot chili on it\" meaning \"The chili on the pizza has a very high temperature\". It's just weird, especially given the somewhat informal nature of many publications on the internet."
    author: "Michael"
  - subject: "Re: Q Note."
    date: 2007-11-29
    body: "Virtual Chinese Brushes might have worked better.  In fact, the clarification behind the Chinese Brushes being virtual and not physical might have been unnecessary given the subject of the article (it would be implicitly assumed)."
    author: "Matt"
  - subject: "Alpha Version"
    date: 2007-11-23
    body: "Thanks for giving it a realistic release state label."
    author: "Anonymous"
  - subject: "Re: Alpha Version"
    date: 2007-11-23
    body: "Don't worry that will soon change."
    author: "Cyrille Berger"
  - subject: "Release notes?"
    date: 2007-11-23
    body: "It looks like very impressive work. I know my kids will benefit from the Kids Office.\n\nFor me, the link to the release notes doesn't work.\n"
    author: "T"
  - subject: "Re: Release notes?"
    date: 2007-11-23
    body: "Right, there is a bug there.  The release notes can be found at http://www.koffice.org/releases/2.0alpha5-release.php, but there isn't too much there."
    author: "Inge Wallin"
  - subject: "Re: Release notes?"
    date: 2007-11-23
    body: "Fixed now in the article."
    author: "Inge Wallin"
  - subject: "Kids are the future"
    date: 2007-11-23
    body: "Honestly, I believe this Kids Office will help KOffice as a whole, improving its usability.\n\nOften a very simple interface, for a very different mindset of people, helps a whole class of applications to reach a breakthrough.\n\nSome legends tell that GUIs, mouses and OOP(to code them) where developed for kids use."
    author: "Bruno Laturner"
  - subject: "Re: Kids are the future"
    date: 2007-11-23
    body: "Much of the GUI interface we see today was created a Xerox PARC for SmallTalk. The language targeted kids. The basic philosophy of Alan Kay was to build an environment that kids would be able to learn programming. \n\nhttp://gagne.homedns.org/~tgagne/contrib/EarlyHistoryST.html\n\nThe group at PARC was known as the 'Learning Research Group' and was heavily influences by LOGO a language designed bat MIT by Seymour Papert for teaching kids programming. \n\nSo you are right - looking at the world for a kids perspective forces you to think in pedagogic terms, to simplify, to link related concepts, introduce information in terms that can be easily learnt. Doing those things introduces a clarity that benefits not just kids but all users. I hope KDE in its entirety will take that approach.\n\nKDE is trying to do a wonderful thing, but it is not the most attractive desktop in the world, I am sorry to say it is still rather ugly but powerful. Teat us all as kids and we will thank you for it."
    author: "Nurul Choudhury"
  - subject: "Re: Kids are the future"
    date: 2007-11-23
    body: "However...don't do like GNOME and treat us like we're id10t's.  Linus had a good point there.  That's why I, too, stick with KDE, because it's full featured, but yet it doesn't try too hard to hide the power of the system from me.  For example, tweaking MIME types on GNOME is a royal PITA (everything I've found says you've got to go to a terminal window to do it).  By contrast, doing it on KDE is a snap and quite intuitive.\n\n--SYG"
    author: "Sum Yung Gai"
  - subject: "Re: Kids are the future"
    date: 2007-11-23
    body: "I agree GNOME has gone too far when it comes to 'dumbing down' the interface, but on the other hand they have a more attractive 'IMHO'. But perhaps the system to learn form is OS X. The beauty and simplicity of the interface is astonishing. Let us not be too proud that we do not learn from other. Yet I do not suggest we blindly copy them. \n\nDesign the interface to be simple for kinds to understand yet powerful enough to satisfy the most demanding user. SmallTalk achieved that and more, let us make KDE do the same and we will have a system that is the envy of all and a gift to the world. Keep up the good work and I hope that the comments that I have written will ring a bell somewhere.\n\n - without beauty we are nothing\n - without power we are a slaves\n - without simplicity we are lost\n"
    author: "Nurul Choudhury"
  - subject: "Re: Kids are the future"
    date: 2007-11-24
    body: "Problem, of course, is that the beauty thing is subjective. Personally, I like the Ubuntu look and prefer it over Kubuntu. But I prefer Oxygen over both, and I think KDE 4 looks awesome. Apparently, you disagree."
    author: "jospoortvliet"
  - subject: "Re: Kids are the future"
    date: 2007-11-25
    body: "I have to be honest the only version of KDE4 I have tried is the Debian live CD for KDE beta4. I do not think that it had Oxygen theme in these. I am sure the Oxygen theme improves the look but that is not what I am talking about. It really has to do with fonts, layouts and the general feel of the environment. \n\nI am not saying in any way the KDE is bad, far from it I think the system is an amazing achievement it terms of capability offered to end users. The presentation of KDE really does look like 'developers' put the visuals together and not visual designers. It is not a criticism of 'developers' - I have been one for more years than I care to remember. It is that you would not use a doctor to do the job of a lawyer would you? \n\nOne of the most interesting aspects of KDE is Plasma. The most interesting aspect is that it gives visual designers to create the interface and the look of the applets. For KDE to really reach new aesthetic heights there has to be a way for all applications to have their interface look and layout be created by graphic artists and interface designers. I wish I could tell you how that would be done; but making that a stated goal of KDE 4.x would bring far smarter minds than mine to the issue of creating a visually beautiful interface as well a an ergonomic one.\n\nOpen source has come a long way in this arena.\n\nInstallation was difficult in the past \u0096 now it is simply amazing\nConfiguring the system was difficult \u0096 now it get getting easier and easier\nDrivers and hardware interface was a challenge \u0096 now it is very good\nFonts were ugly \u0096 it has been steadily improving\n\nWhat I am saying is the open source has met and excelled at every challenge thrown at it yet many more remain. Ultimately it is the \u0091Red Queen\u0092 problem, as you get better the bar keeps rising \u0096 welcome to the real world.\n"
    author: "Nurul Choudhury"
  - subject: "Thanks, KOffice team, for your continued work"
    date: 2007-11-23
    body: "Hello KOffice team,\n\nI am currently a major user of OpenOffice.org, chiefly because of the MS Office file format compatibility issues that I have to deal with at work.  Yes, I actually do like OO.o.  However, at home, I've started to use KOffice here and there, and due to the shared library nature, it is far less memory-intensive than OO.o.  I have run it on a 400MHz Power Mac G3 with 256MB DRAM, and it's actually quite functional (Kubuntu 6.06 \"Dapper Drake\") and snappier than I had expected.  The PDF editing functionality of KWord already has proved handy at least a couple of times.\n\nThe OpenDocument support is, as far as I'm concerned, a major plus.  It's taking a while for GNU/Linux to show lots of numbers on the desktop, sure.  Note my choice of words: \"show\" lots of numbers.  However, it's happening, and thus truly open standards like ODF will have to be paid attention to; the days of \"Microsoft Word required\" or \"submit resume/proposal in MS Word format only\" are numbered.  Your work helps to further this goal.\n\nNot that I care for MS Windows, but I've also come around to the decision that it was a good idea for KDE to support MS Windows with KDE 4.  Here's why.  If we can get folks to actually run KDE on MS Windows, then they'll have KOffice, among many other things (KTorrent, AmaroK, K3b, etc.).  This is yet another vector to make it easier for folks to migrate to Free platforms like GNU/Linux and *BSD.\n\n--SYG"
    author: "Sum Yung Gai"
  - subject: "\"KOffice for Bairns\""
    date: 2007-11-23
    body: "Rofl... :-)"
    author: "Odysseus"
  - subject: "Kids Office"
    date: 2007-11-24
    body: "Wanted to say thanks for this, it's sorely needed... and sometimes adults prefer simple interfaces too.\n"
    author: "Christie Harris"
  - subject: "Remember Creative Writer 2?"
    date: 2007-11-26
    body: "I remember when I was going to primary school in the 90s we used Microsoft Creative Writer 2 in the labs to type up our letters, reports and other projects, it was a lot of fun. I think this Kids primer interface will help to do the same thing: help make computers more accessible and fun to people at an earlier age.\n\nThis could also mean that for some when they're old enough they'll start demanding KDE based machines instead of Windows ones, which can only be a good thing right :)?"
    author: "Ruben Schade"
  - subject: "Re: Remember Creative Writer 2?"
    date: 2007-11-26
    body: "the creative writer 2 - idea is interesting - you'll still find a demoversion at:\nftp://ftp.microsoft.com/deskapps/kids/cw2install.exe\n"
    author: "doesn't matter"
  - subject: "Formating icons"
    date: 2007-11-26
    body: "I hope those formating icons wont be so huge on Koffice2. I understand those should be big on kids version but c&#8217;mon, for normal usage those are HUGE sized. \n\nI loved smaller ones on wich came with KDE4 RC version but when i opened formation panel, i got almoust heart attack because they toke almoust all the space what my 1280\u00d7800 monitor allows on vertical space. \n\nSo do we get them smaller size and im just missunderstand or will they stay as that? \n\n1/4 size of those could be ideal&#8230;\n\nI liked version what came with Mandriva 2008 cooker but there is so much missing so i cant get good touch for it. \nThere are many things what i would like to see on final, but i dont know are they currently left away (like many things are because there is missing so much menu entries) or has them disabled or leaved."
    author: "Fri13"
---
Immediately after the release of KDE 4.0 RC1, the KDE office suite KOffice today <a href="http://www.koffice.org/announcements/announce-2.0alpha5.php">announced the release of version 2.0 Alpha 5</a>. As with the previous alpha versions of 2.0, this is a technology preview more than a version for users to test out.  Nonetheless there are some exciting new features and developments here.  Read on for more information.











<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex;">
<a href="http://static.kdenews.org/jr/koffice-alpha-5-kids.png"><img src="http://static.kdenews.org/jr/koffice-alpha-5-kids-wee.png" width="300" height="205" /></a><br />
KOffice for Bairns
</div>

<p>In addition to enhancements in all the applications themselves which are too many to mention here, there are a few particularly important improvements.</p>

<h3>More Shared Infrastructure</h3>
<p>
The general libraries saw several enhancements.  First, there is improved sharing by making resources like gradients and strokes shared.  All applications are now using the same implementation of these features, something that will reduce bugs and heighten the users feeling of a unified application suite.  Another change is that the basic routines for handling the OpenDocument Format have been broken out into its own library.  This is not a user-visible change, but important nonetheless because it is this library that was suggested will be made <a href="http://dot.kde.org/1179144063/">available for other KDE developers in kdelibs 4.1</a>.
</p>

<h3>Simplified Word Processor: Kids Office</h3>
<p>
Over two years ago, <a href="http://ingwa2.blogspot.com/2005/09/koffice-kids-office.html">Inge Wallin proposed a simplified word processor</a> to be used in school for kids.  Thomas Zander, the KWord lead developer, <a href="http://labs.trolltech.com/blogs/2007/11/10/koffice-in-educational-settings/"> made a proof of concept of this using the infrastructure of KOffice 2</a>.  This proved simpler than even Thomas would have believed, and KOffice 2.0 Alpha 5 now contains a first version of the KOffice for kids.  Note that only the GUI is simplified, and that it still contains the full power of KOffice. This means that it can save and load the OpenDocment Format, which will make it easy to interact with other users of OpenOffice.org or the full KOffice suite.
</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex;">
<a href="http://static.kdenews.org/jr/krita-koffice-alpha5.png"><img src="http://static.kdenews.org/jr/krita-koffice-alpha5-wee.png" width="300" height="225" /></a><br />
Krita's Chinese Brush
</div>

<h3>New Drawing Tools in Krita</h3>
<p>
The Krita team is working hard to make Krita the best free pixel based image editor in the world.  This version introduces so called <a href="http://www.valdyas.org/fading/index.cgi/hacking/krita/wacoms.comments"> Chinese Brushes</a> and a new Path Tool.  It also now has all the filters that Krita 1.6 had again, as well as a <a href="http://www.valdyas.org/fading/index.cgi/hacking/krita/livepreview.comments"> first-cut implentation of on-canvas filter previews</a>.
</p>

<p>More information about this release can be found in the <a href="http://www.koffice.org/announcements/announce-2.0alpha5.php">full announcement</a> or the <a href="http://www.koffice.org/releases/2.0alpha5-release.php">release notes</a>.</p>










