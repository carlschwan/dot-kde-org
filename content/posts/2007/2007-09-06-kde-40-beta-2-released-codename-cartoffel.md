---
title: "KDE 4.0 Beta 2 Released, Codename \"Cartoffel\""
date:    2007-09-06
authors:
  - "jpoortvliet"
slug:    kde-40-beta-2-released-codename-cartoffel
comments:
  - subject: "THANKS"
    date: 2007-09-06
    body: "And thanks to everybody who helped to get this online ;-)\n\nThose writing the code, obviously. And those who added stuff at http://techbase.kde.org/Schedules/KDE4/4.0_Announcements \nAnd Wade, Steve, Joseph, Danny and the others for the review. Sebas for fixing what I did wrong, and telling me how to do better in the future... What, I should ignore all the cool stuff going on but not in the beta?!?! ;-)\n\nA extra tnx! for Wade. Just because he's insane."
    author: "jospoortvliet"
  - subject: "Re: THANKS"
    date: 2007-09-06
    body: "And I want to mention to everybody that:\nDanny, Riddel, Sebas and others are doing an amazing job on getting articles on the dot. If you see how the software works, you'd be surprised that anything ever gets online ;-)\n\nAnd they could use some help. If you have time (no really skills needed, just some time and willingness) you can make a difference here.\n\nSame with some other administrative stuff, we can use some help there as well.\n\nAnd you want to write articles? magazines often ask us for ppl who want to write (it can even make you money), join kde-promo@kde.org and offer help to those asking for it ;-)\n\nBoy, there is so much non-coding work to do..."
    author: "jospoortvliet"
  - subject: "Re: THANKS"
    date: 2007-09-06
    body: "Come on, how hard is it to post and approve your article on the dot?  There's no embedded images or anything..."
    author: "Navindra Umanee"
  - subject: "Re: THANKS"
    date: 2007-09-06
    body: "Aaah, indeed, a simple announcement isn't that hard. But a longer article is much more work than it should be -- again, from what I've seen..."
    author: "jospoortvliet"
  - subject: "Re: THANKS"
    date: 2007-09-06
    body: "Wait...is that officially a back-handed compliment, a compliment and an insult, or two compliments?\n\nIrregardlessably, as I've stated in a recent blog entry, Jos did a great job of creating a thoughtful and detailed press release that puts other larger companies to shame.\n\nSide note: We have screenshots and plenty of links; just remember that if you see a press release or story like this pointing to images or URLs on your server, don't be devious and replace the image with an elephant's butt or a goatse image.\n\nLinks and images are only useful if they stay up for some amount of time.  If you don't want press releases or stories pointing to your images and therefore servers, please let us know.  We may not have the Slashdot effect but we have the Dot effect."
    author: "Wade Olson"
  - subject: "Quality?"
    date: 2007-09-06
    body: "What's the quality intended to be for this release?  Is it meant to be functional?  The last beta, people complained that it was incomplete and non-functional, and they were told to stop whinging and that it wasn't *meant* to be complete or functional.\n\nI'm quite happy to test this if it's actually beta quality, but I don't want to waste my time on something that isn't even supposed to work right yet.  I am happy tracking down bugs, but want something that is actually operational.  \"Time to start polishing\" and \"significant improvements\" could mean absolutely anything in this regard.\n"
    author: "Jim"
  - subject: "Re: Quality?"
    date: 2007-09-06
    body: "Some say it's basically usable. I wouldn't use it myself yet, but we're indeed close to usable already. Next beta will most likely be good for daily usage."
    author: "jospoortvliet"
  - subject: "Re: Quality?"
    date: 2007-09-06
    body: "The quality is actually quite good: most applications don't crash anymore if you start them and the games and kde-edu are both really good.\nTo be honest, most applications seem quite good already: dolphin p.e. (though I might have to get used to it :)\n\nOne problem I have encountered is the \"accelerated\" bling: performance goes immediately down and I can almost put my finger on the screen to follow the mouse-pointer with my finger.\n\nAnd I think it's really a pity that they (we?) didn't get raptor out of the door, so that console-challenged users still can get along without kicker: At the moment, there's simply no menu if you kill kicker.\n\nOh and for the whole fun of it, run \n nepomukdaemon\nwith Alt+F2 (or konsole) - then you'll have the beginning of the meta-information goodness of dolphin.\n\nStarting strigidaemon (the same way) starts the indexer, (first time is slow!) which then enables you to search through stuff it has indexed (normally $HOME) using Alt+F2, too.\n\nAll in all I think it's a felicitous tech-preview of what can be expected in the next few months. \n\nStop! Did I say _tech_ preview? Oxygen is almost complete: although the window decorations are nowhere close to what they might become next week, the icon set seem to be almost finished - at least in dolphin and most other obvious places (don't start systemsettings or the window-effects-dialog if you're afraid of questionmark-icons :)\n\nGet it, try it (no: don't use it fulltime, yet) and get busy making it better: \n- Documentation is needed - see #kde-docs (nixternal has the list?)\n- Translations have already started - get on board while some of the easy strings are still to be got!\n- Bug the developers with bug reports - if you don't tell it, it (probably) won't be fixed!\n- Show it to your friends - if they laugh, tell them off and explain the difference between an open and a closed development-cycle!\n\nGet it."
    author: "Tom Vollerthun"
  - subject: "Re: Quality?"
    date: 2007-09-06
    body: "> I'm quite happy to test this if it's actually beta quality\n\nApplications from kdebase, kdegames, kdeedu, kdegraphics, kdesdk are, on the whole, functional and stable enough to be worth testing.  The window manager works, I haven't used it extensively.\n\nThe desktop itself (including the panel) is still way behind where it needs to be.  The widget style and window decoration still need work, you will probably find it easier to use one of the ported KDE 3 styles for the time being.\n"
    author: "Robert Knight"
  - subject: "Re: Quality?"
    date: 2007-09-10
    body: "Sorry, I don't have an answer for you, but your question was very good and just wanted to let you know. I felt exactly the same. \n\nI used the beta-versions of previous releases often long before release in my production environment. And know I downloaded two Live CD and even there, most applications won't even start. \n\nI am very sorry, I could not help test kde4 'til now. "
    author: "thorsten"
  - subject: "typo?"
    date: 2007-09-06
    body: "Why not Kartoffel (german word for potato) ?"
    author: "Benoit Jacob"
  - subject: "Re: typo?"
    date: 2007-09-06
    body: "because this is what everyone would expect... :-)"
    author: "MS"
  - subject: "Re: typo?"
    date: 2007-09-06
    body: "it's a joke we came up with in a kartoffelhaus one night over dinner and brainstorming about names for betas. it was suggested that kartoffel was an great name, and the follow up suggestion was to misspell it with a 'c' as a self-referential jab at our historical k-ification.\n\nthis is what happens when you get a bunch of geeks together thinking of code names for stuff. ;)\n\n\"one night in darmstadt and the world's your kartoffel...\""
    author: "Aaron Seigo"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "I love it! Spelling it with a 'c' is going make all the K hating trolls (Slasdot trolls, not the Norwegian trolls ;) go up in flames...or something :p"
    author: "Joergen Ramskov"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "I think in Rome C was pronounced K in Latin.\nCeasar and Cicero was pronounced Keasar and Kikero, not Seasar and Sisero.\nVery sophisticated joke."
    author: "reihal"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "Uh,well... not really!\nIn Latin C was pronounced C just like in Cicero, Ciceronis... OR K as in \"Alea icta est\". It all depends on what letter is after the C...... "
    author: "ZIOLele"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "Hmm... What about the ruler of the Holy Roman Empire?\nDerived from Ceasar, it was the Kaiser, not the Saiser.\n"
    author: "reihal"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "Hmmm... What about the ruler of Imperial Russia, who saw themselves as the legitimate inheritants of the Eastern Roman Empire? Derived from Caesar, it was the Tsar, not the Kar.\n\n;-)"
    author: "Emperor"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "Ok... but lets leave Charlemagne out of this."
    author: "reihal"
  - subject: "Re: typo?"
    date: 2007-09-10
    body: "No, on second thoughts, lets do the reverse.\n\nMay I propose that the last KDE 4 RC be codenamed \"Karl\" in honor of\n the father of both Germany and France.\nIf you look at the map of contributions to KDE 4 you will agree that it is appropriate. \nYou can make a joke about it and use the old English version \"Churl\".\n(\"Charon\" is not appropriate)\n"
    author: "reihal"
  - subject: "Re: typo?"
    date: 2007-09-09
    body: "It's the ending \"sar\" bit in \"Caesar\" that turned into \"tzar\", that's all."
    author: "kolla"
  - subject: "Re: typo?"
    date: 2007-09-09
    body: "It's not Ceasar, dummy, it's Caesar, or even C\u00e6sar."
    author: "kolla"
  - subject: "Re: typo?"
    date: 2007-09-10
    body: "Yes, you spotted the typo! You win a beautiful Lounge Suite!\n\nAnd they pronounced it Kaisar!"
    author: "reihal"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "\"The first duty of a man is the seeking after and the investigation of truth.\"\n-Markus Tullius Kikero"
    author: "reihal"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "It really depends on the dialect. Classical latin (the one I learned in highschool) is always a hard C (like in cake). The other dialect (the name escapes me) has a soft C in some circumstances."
    author: "Soap"
  - subject: "Re: typo?"
    date: 2007-09-07
    body: "English perhaps?"
    author: "reihal"
  - subject: "Re: typo?"
    date: 2007-09-09
    body: "It's quite common in all languages derevied from Latin (+ English, which is just a Nethergerman/Norse mix with fancy words, ironically imported from the always behated French) that c before a, o, u and other consonant is pronounced k, while c before e i and y is pronounced as a what you call \"soft\" (s/sh-like or guttural). I'd be happy to know about words of latin languages that break this rule, I cant think of any myself."
    author: "kolla"
  - subject: "Re: typo?"
    date: 2007-09-10
    body: "there was a city named named Keisaria, after Caesar."
    author: "yman"
  - subject: "New desktop"
    date: 2007-09-06
    body: "Is that the new desktop?\n\nDefinitely have to try it when it's out. :-) \n\nLooks interesting."
    author: "Axl"
  - subject: "Rotation"
    date: 2007-09-06
    body: "You always see this rotated green/grass image on the plasma desktop. I wonder what it is good for. And even more interesting: Can you do that to any plasmoid? That would be really great if I could freely rotate things..."
    author: "Michael"
  - subject: "Re: Rotation"
    date: 2007-09-06
    body: "> rotated green/grass image on the plasma desktop\n\nwe're working on standardized resize and rotate for objects,  yes."
    author: "Aaron Seigo"
  - subject: "Re: Rotation"
    date: 2007-09-07
    body: "When you say \"objects\" you mean plasmoids or any kind of object that lies on the desktop. The idea of this rotated/resized image would be great for open windows if they could be rotated on the z axis. Then you have a practical way to leave open windows on the desktop that don't take much space but you can still see what each one of them is."
    author: "Nick L."
  - subject: "Re: Rotation"
    date: 2007-09-07
    body: "Aaron's talking about Plasmoids - rotating windows is not within the area of competence of the Plasma application. That said, kwin could rotate windows on any axis you want right now, it just wouldn't be particularly useful, as X does not yet have generic support for input transformations, i.e. translating input events into a transformed (rotated) windows' reference frame. Work on this is ongoing in the X.org project, and will eventually land on your desktop. And KDE 4 will have the technological infrastructure to make use of it. \n\nRight now, you can look at Mandriva's \"Metisse\" environment for a case-specific implementation of an OpenGL-based composited desktop with support for input transformations to get a feel for what it enables."
    author: "Eike Hein"
  - subject: "Themes"
    date: 2007-09-06
    body: "Just a short question:\n\nWhich popular icon-themes and widget-themes from kde-look.org etc. will be included into KDE 4 (maybe to replace some not-so-popular and outdated icon- and widget-Themes)?\n\nIf the answer is \"we do not intend to include any community work from kde-look.org\" then the question is \"why?\".\n\n"
    author: "Max"
  - subject: "Re: Themes"
    date: 2007-09-07
    body: "\"If the answer is \"we do not intend to include any community work from kde-look.org\" then the question is \"why?\".\"\n\nWell most likely the reason would be that none of them are made for KDE4 (as in the widget themes are KDE3 themes and the icons don't use the new FD.o naming spec).\n\nNote:  I haven't looked at kde-look.org to check, and I have no clue whether anything from kde-look.org is being included or not so, as well as having nothing to do with any of that stuff."
    author: "Sutoka"
  - subject: "Re: Themes"
    date: 2007-09-07
    body: "> Which popular icon-themes and widget-themes from kde-look.org etc. will be included into KDE 4\n\nI can't provide you will a full list, but for one, the very popular \"Nuvola\" icon set has joined the kdeartwork module. \n\n\n> If the answer is \"we do not intend to include any community work from kde-look.org\" \n\nThat's definitely not the answer. After all, essentially all of KDE is made up of community contributions, in a way: It's a community-driven project."
    author: "Eike Hein"
  - subject: "Re: Themes"
    date: 2007-09-07
    body: "Nuvola has always been my favorite :) Something simple and easy about it."
    author: "Samuel Weber"
  - subject: "Re: Themes"
    date: 2007-09-07
    body: "I've actually seen a couple on kde-look myself.\nhttp://www.kde-look.org/index.php?xcontentmode=9\n\nAll those styles are KDE4 ones... and I dont see why a couple of them cant be included as \"lightweight\" alternatives to oxygen. I especially like \"float\". Its nice a polished, and would be a great addition to the default list.\n\nOn the icon side, I think crystal-project has a good case for inclusion. It just doesn't seem like KDE without a crystal icon theme! :)\nhttp://www.kde-look.org/content/show.php/Crystal+Project?content=60475\n\nOh, and the mockup style on the Crystal Project screenshot looks mad :)"
    author: "lexxonnet"
  - subject: "Re: Themes"
    date: 2007-09-07
    body: "Float and Bespin both looked pretty nice to me.  Apparently Bespin is based off the old Oxygen code that got replaced, so it is somewhat similar."
    author: "Matt"
  - subject: "confusing links to 3.9x info pages"
    date: 2007-09-07
    body: "This article's \"The _info page_ has more information\" links to http://www.kde.org/info/3.93.php.  It took me a while to realize that a 3.9x release is in fact a 4.0 beta, I think this will confuse other n00bs.  Could the 3.93 info page say in big letters that it's effectively 4.0 beta 2, and maybe explain the numbering scheme?\n\nAlso, the Download section of the left-hand navigation on info pages like http://www.kde.org/info/ only has a link for \"KDE 4.0 Beta1\", maybe replace that with \"KDE 40 Beta 2\".\n\nCongratulations!"
    author: "skierpage"
  - subject: "openSUSE packages"
    date: 2007-09-07
    body: "I've tried to install the openSUSE packages from http://en.opensuse.org/KDE4, but many plasma applets don't load. It seams I'm missing the libplasma_*.so libraries. Can someone tell me which package contains those for openSUSE? They sure splitted the packages a lot: http://download.opensuse.org/repositories/KDE:/KDE4/openSUSE_10.2/i586/ :-|"
    author: "Diederik van der Boor"
  - subject: "Re: openSUSE packages"
    date: 2007-09-07
    body: "you gotta ask the suse ppl about this..."
    author: "jospoortvliet"
  - subject: "Re: openSUSE packages"
    date: 2007-09-07
    body: "Some mix up with the kdebase split and Plasma and Kicker (not yet completely) being removed happened in Beta 2: some parts of Plasma like the tasks data engine, which is used by the taskbar plasmoid, still depend on Kicker parts (actually Kicker is still started behind Plasma). When you build from SVN kicker gets build but due to a bug it doesn't anymore in the official Beta 2 kdebase-workspace release tarball from which the current openSUSE packages were built. Next week these problems will be fixed in the openSUSE packages."
    author: "Beineri"
  - subject: "could not start kstartupconfig.check your instal.."
    date: 2007-09-07
    body: "hi, i got a mandriva spring (2007.1). i have updated kde from the kde mirror from beta 1 to beta 2, but when i start the session i receive this message and i can't access the desktop. what can i do?\nthanks\nMarcello"
    author: "killer"
  - subject: "Re: could not start kstartupconfig.check your inst"
    date: 2007-09-07
    body: "You gotta ask the mandriva ppl about this..."
    author: "jospoortvliet"
  - subject: "Re: could not start kstartupconfig.check your instal.."
    date: 2007-09-21
    body: "I\u00b4ve installed KDE4 Beta1 using meta packages from Yast (Suse), and i used the kde4 like a charm. But when i updated to KDE4 Beta 2 i prompted for this error too, when using the same user from beta 1 session. I created a new user too. Did you found any trick for workaround or fix this? thanks in advance."
    author: "Jose Carlos"
  - subject: "broken link in the release announcement"
    date: 2007-09-07
    body: "Not sure where to report this but there's a broken link in the release announcement:\n\n\"...the KDE Remote Desktop Client is finished as well. Making use of libraries ensures the...\"\n\nthe 'libraries' link goes to: http://libnvserver.sourceforge.net/ which is broken, I think it should be http://libvncserver.sourceforge.net/\n\nApologies for this very minor nitpick.\n\nCheers for all your hard work everyone :-)\n\nLanroth"
    author: "lanroth"
  - subject: "Re: broken link in the release announcement"
    date: 2007-09-07
    body: "Fixed, thanks."
    author: "Sebastian K\u00fcgler"
  - subject: "Live CD - Beta2"
    date: 2007-09-07
    body: "as usual a KDE4Live CD can downloaded from \n\n<A href=\"http://home.kde.org/~binner/kde-four-live/\">Download</A>\n"
    author: "Cougar"
  - subject: "Re: Live CD - Beta2"
    date: 2007-09-07
    body: "Thanks a lot for your work. Isn't it possible to make a torrent though? would be great."
    author: "Jonathan"
  - subject: "Re: Live CD - Beta2"
    date: 2007-09-07
    body: "Installation hangs on a ATA Raid system. Or whatever the reason is.\nNo workie!"
    author: "reihal"
  - subject: "Re: Live CD - Beta2"
    date: 2007-09-07
    body: "Er, I mean loading not installation."
    author: "reihal"
  - subject: "Re: Live CD - Beta2"
    date: 2007-09-07
    body: "Can anyone can post some screenshots?"
    author: "Coward"
  - subject: "Re: Live CD - Beta2"
    date: 2007-09-07
    body: "Official:\n\nhttp://vizzzion.org/stuff/screenshots/kde-4.0-beta2/\n\nUnofficial:\n\nhttp://www.jarzebski.pl/read/kde-4-rev-705212.so"
    author: "Anon"
  - subject: "Re: Live CD - Beta2"
    date: 2007-09-08
    body: "deja vue?\n\nhttp://www.slax.de/img/e17-solaris.png\n"
    author: "Martin Ronde"
  - subject: "Cartoffel"
    date: 2007-09-07
    body: "'Cartoffel' means 'potato' in russian"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: Cartoffel"
    date: 2007-09-07
    body: "The german word is \"Kartoffel\". This would even fit better to the C-K-twist that was in the name of the first beta \"Cnuth\"."
    author: "Michl"
  - subject: "Re: Cartoffel"
    date: 2007-09-08
    body: "Hahaha, so I'm not the only one who noticed it!\n\nI really laughed. =)"
    author: "A. Frolenkov"
  - subject: "Re: Cartoffel"
    date: 2007-09-08
    body: "German language. It's the same in the Polish language. \"Kartofel\" for a spud."
    author: "szlam"
  - subject: "beta2 mandriva 2007.1 issues"
    date: 2007-09-08
    body: "I installed the packages from kde.org mirror. When I log into kde4, I don't see much. The panel on the bottom has two boxes with the message that this object could not be rendered. The mouse doesn't do anything on the desktop.\n\nThere is one item names \"desktop toolbox\" on the desktop. It is iconless but hovering over it presents a menu. With that, I can 'run' the konsole command and from there, run some apps.\n\nAll in all, from the desktop itself, I cannot do much. Am I missing some packages ?"
    author: "vm"
  - subject: "Re: beta2 mandriva 2007.1 issues"
    date: 2007-09-08
    body: "Yeah I get the same shit on kubuntu and according to the forums opensuse has the same issue as well. The livecd also has the problem. Seems like the panel wasn't tested that much before being included into beta2. Unless I'm totally missing something.\n\n"
    author: "Heretic"
  - subject: "Re: beta2 mandriva 2007.1 issues"
    date: 2007-09-08
    body: "For me it seems that released beta2 tarballs from kde.org are incomplete or broken. Missing kopete in kdenetwork, kicker is also missing etc. SVN tarballs are O.K."
    author: "Poborskiii"
  - subject: "Re: beta2 mandriva 2007.1 issues"
    date: 2007-09-08
    body: "For people that have no idea what it looks like:\n\nhttp://pix.nofrag.com/1/c/c/27c7aedf2edfd6a7497824761e22f.html\n"
    author: "Heretic"
  - subject: "Re: beta2 mandriva 2007.1 issues"
    date: 2007-09-10
    body: "Yes I can confirm that I am having the same issue as you and I am running on kubuntu feisty after I installed the kdebase-workspace package from the backports server.\n\nNow this is a sad day for me, since I had the exact same issue with beta 1. I'd really love to play with plasma etcetera... for the love of all things good (ergo, kde) please have this fixed!\n\n:)"
    author: "Iamn00b?"
  - subject: "Re: beta2 mandriva 2007.1 issues"
    date: 2007-09-13
    body: "i have the same problem with beta2 and openSuse. Is there any workaround or do i have to wait for beta3?"
    author: "Joe"
  - subject: "Re: beta2 mandriva 2007.1 issues"
    date: 2007-09-10
    body: "Seems like who ever packaged this seems to have left out playground for some reason. Any one know if this will be corrected or is this a \"Just deal with it\" scenario?"
    author: "Heretic"
  - subject: "Kubuntu packages - where is plasma?"
    date: 2007-09-08
    body: "On the Kubuntu packages page, it says that plasma should work, but I can't find it in the kde4-* packages they provide (gutsy). How do I get it/start it?"
    author: "Mike"
  - subject: "Re: Kubuntu packages - where is plasma?"
    date: 2007-09-09
    body: "It should be in kdebase-workspace."
    author: "Kevin Kofler"
  - subject: "Re: Kubuntu packages - where is plasma?"
    date: 2007-09-09
    body: "Thanks, I found them now. I was only looking for kde4*-packages..."
    author: "Mike"
  - subject: "Ugly"
    date: 2007-09-08
    body: "Sorry, but KDE4 looks soooooooo ugly and 'simple'.\n\n"
    author: "Bart"
  - subject: "Re: Ugly"
    date: 2007-09-08
    body: "Thanks for the detailed and useful feedback!"
    author: "Anon"
  - subject: "lost startkde script"
    date: 2007-09-09
    body: "pls post file startkde for kde 4.0 beta 2(3.93) or give me a link where i can find it. if you have the beta 2 installed pls look in /usr/bin/kde4/bin/startkde an post the content."
    author: "illu"
  - subject: "Gentoo ebuilds"
    date: 2007-09-10
    body: "Also there are Gentoo ebuilds in the kde overlay: \n- http://overlays.gentoo.org/proj/kde/wiki"
    author: "Arne Babenhauserheide"
  - subject: "Not yet Beta quality"
    date: 2007-09-14
    body: "Sorry, guys, but this release still hasn't even reached alpha quality in my opinion. As eager I am to see KDE 4 released, please take your time. And stop releasing something as Beta that's barely testable."
    author: "chris"
---
The KDE Community proudly presents <a
href="http://www.kde.org/announcements/announce-4.0-beta2.php">the second Beta release for KDE
4.0</a>. This release marks the beginning of the feature freeze and the stabilization of the current
codebase. Simultaneously the <a href="http://www.koffice.org/">KOffice</a> developers have announced
their <a href="http://www.koffice.org/announcements/announce-2.0alpha3.php">third Alpha
release</a>, marking significant improvements in this innovative office suite. Both <a
href="http://behindkde.org/people/soc2007-one/">KDE</a> and <a
href="http://dot.kde.org/1188249220/">KOffice</a> have benefited from the <a
href="http://code.google.com/soc/2007/kde/about.html">Google Summer of Code</a>, as most resulting code has now been merged. Read on for more details.







<!--break-->
<p>Since the libraries were frozen with the <a
href="http://www.kde.org/announcements/announce-4.0-beta1.php">first beta</a>, KDE developers have been adding features and functionality to their applications. Now it is time to start polishing these features; writing and translating documentation, improving the usability, and completing the artwork. As KDE 4.0 is feature-frozen now and going full on into bugfixing mode, major ideas and changes will be held off until KDE 4.1. However, some KDE components, such as <a
href="http://plasma.kde.org/">Plasma</a>, are exempt from this freeze and will still see significant
improvements.</p>

<p>The <a href="http://www.kde.org/info/3.93.php">info page</a> has more information, including information on how to obtain KDE 4.0 Beta 2.</p>

<p>Packages are available for <a href="http://kubuntu.org/announcements/kde4-beta2.php">Kubuntu</a>, <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.93/Mandriva/">Mandriva</a> and <a href="http://en.opensuse.org/KDE4">openSUSE</a><!--, plus there's the <a href="http://home.kde.org/~binner/kde-four-live/">openSUSE KDE 4 Live CD</a>-->.</p>






