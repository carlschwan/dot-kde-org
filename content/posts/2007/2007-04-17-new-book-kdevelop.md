---
title: "New Book on KDevelop"
date:    2007-04-17
authors:
  - "pjung"
slug:    new-book-kdevelop
comments:
  - subject: "Will it be translated ?"
    date: 2007-04-17
    body: " It would be nice if this book got translated as I don't know German but would very much like to read it. Maybe there's some other books in English you could recommend me for learning KDevelop.\n Still maybe we'll have an English version soon."
    author: "Marius Cirsta"
  - subject: "Re: Will it be translated ?"
    date: 2007-04-17
    body: "I agree. English than all the other languages..."
    author: "Giovanni Venturi"
  - subject: "ESPERANTO must be the official language"
    date: 2007-04-17
    body: "Why not use esperanto as the official language for the FS community?\nWhy keep our minds under the control of \"The Empire\"?\n"
    author: "Joshua"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-17
    body: "Absolutely agree with you, Joshua.\nI'not a guru in Esperanto, but this lang is not difficult for learning. Also it is more simply to write auto translator from Esperanto to any lang..."
    author: "Alexander"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-17
    body: "What empire? There are so many countries that use English as their (primairy) language, it is absurd to claim it for any *one* \"empire\" now. To name a few: UK, USA, Canada, Australia, South Africa..."
    author: "Andr\u00e9"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-17
    body: "I believe he was referring to the British Empire that was..."
    author: "Troy Unrau"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-17
    body: "probably the real question was, why to use English or ESPERANTO if we have C++ :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-18
    body: "to match verbal buffer overflows?"
    author: "Carlo"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-17
    body: "Esperanto would certainly be a good choice, but before we can do this people must get more aware of the unfair system Global English represents and what immense benefits English native speakers have through their mother language (what is not their fault, but yet a fact). If people are informed and this action is supported, one can really consider it. One can start informing about this for example at http://www.nitobe.info \n\nA well-known swiss professor, Francois Grin, calculated the profit the UK is having out of the dominant position of English in the EU as 25 billion EUR, and he always took the most conservative estimations. http://lingvo.org/grin/ Again, this is just a privilege due to birth, not to any other merit.\n\nBtw:\nEsperanto estas la unu malfermfonta lingvo :)\nEsperanto was the first open source language\n\n since it was licensed under a kind of BSD-license in 1905 to mankind :)"
    author: "Felicx"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-17
    body: "\"since it was licensed under a kind of BSD-license in 1905 to mankind :)\"\n\nGiven the amount of forking Esperanto has been subject to, it might have been better for Zamenhof to have used a more restrictive license :-). Or to have embraced the first international auxiliary constructed language Volapuk, of course, which is much more difficult^Wchallenging^Winteresting to learn. (If you don't count Solresol and Leibnitz's symbolic language.) And Latino Sine Flexione has its points, although it's even more culturally limited than the already very limited Esperanto. Then there's Interlingua, which has its points, although they escape me for the moment. Lojban is very logical, which makes it nearly impossible to learn, and even more impossible to lie in. There are serious linguists who maintain that Silver Age Latin is a constructed language, as well as classical Sanskrit.\n\nAh -- you've set me off yearning for my conlanging days :-) (http://www.kdedevelopers.org/)"
    author: "Boudewijn Rempt"
  - subject: "INTERLINGUA must be the official language ;-)"
    date: 2007-04-19
    body: "Interlingua's main point is that it doesn't try to define a new concept of an \"international\" language, but instead takes what is *already* international (i.e.: the vocabulary of science, technology, religion, etc. that all the major world languages have in common and which is mostly Greek/Latin in origin), and derives a real language out of these pre-existing international components using a verifiable prototyping process. Its grammar is simpler than Esperanto's, and most of its words are immediately obvious to large segments of the world population, and certainly to anyone active in the free software community.\n\nThe curious may head over to www.interlingua.com and ia.wikipedia.org for live examples, and visit #interlingua on irc.freenode.net (active times are Wednesdays and Saturdays at night, European time) to chat in Interlingua. Have fun!"
    author: "Martijn Dekker"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-10-31
    body: "\"Lojban is very logical, which makes it nearly impossible to learn, and even more impossible to lie in.\"\n\nWhat a ridiculous assertion.  I must debunk it.\n\nLogical things are hard to learn:\nI suppose you weren't able to learn maths, since it's logical.  Oops, there goes all programming languages too!  I guess you won't be doing any Linux development anymore because you would be using a /logical/ programming language!  I've spent the last few months learning Lojban and I have found it extremely easy to learn, much more so than Japanese which I have studied for just over a year, and is far less logical than Lojban.  If what you said was true, it would be easier to learn illogical things as compared to logical things, which I think we can all agree is obviously false.\n\nIt's hard to lie in Lojban:\nIt's no harder than English.  do tugni mi = you agree with me.  But I'm fairly sure you don't, so it looks like I lied!  What made you think this?  Do you believe that you can't state a fact that is untrue using a logical language?  \n\nThis seems to originate from a misunderstanding on your behalf about the nature of Lojban.  It is not hard to learn, it is not unambiguous (in fact you can be incredibly ambiguous if you want), and there is certainly plenty of room for lies, trickery and deceit.  Lojban's logicality lies in its grammar and structure, in the language itself, not necessarily in its use.\n\nI believe Lojban would work extremely well as an official language since its extremely easy to learn (you can start conversing in it at a simple level within a few weeks) and its very culturally neutral (in fact, the English-speaking Lojban community actively discourages any use of Lojban that resembles English - they call it \"malglico\" which is a derogative way of referring to the English language).\n\nI suggest you learn a little more about the nature of Lojban before making such uninformed statements.  Some introductory material can be found here: \nhttp://www.lojban.org/tiki/tiki-index.php?page=Lojban+Introductory+Brochure&bl"
    author: "Matt"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-17
    body: "Esperanto estas la unuA malfermfonta lingvo :)\nEsperanto IS the first open source language\n\n(but I know in text I do more mistakes :)\n\nThis is not propaganda, but for me was very difficult read english documentation (documentation on russian is very poor). Soft for translation text from English to Russian like on clown (particularly if you do inverse translation). As I know in UNO was researches, where Esperanto became main lang for stored documents\nhttp://www.radicalparty.org/esperanto/ins_un.htm\n\nYou can represent when google bring me to chinese texts (try to find \"lzh\" for example :)\n\n"
    author: "Alexander"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-18
    body: "Well, learning english is quite an easy task compared to other languages, e.g. russian. And face it, the world won't switch to esperanto or any other artificial language for the next 100++ years, if ever."
    author: "Carlo"
  - subject: "Re: ESPERANTO must be the official language"
    date: 2007-04-18
    body: "I guess the ease of learning English is highly dependent on your own linguistic background. For other native speakers of (western) europe originated languages, it is not hard. But for people with a very different linguistic background, such as an asian language, it is much harder. There is the big advantage that English seems to be almost ubiquitous, so it is relatively easy to pick up and practise in dayly life. Unlike one of the artificial languages, which frankly I haven't found being used in the wild yet. The latter may be a result of my own limited experience, of course."
    author: "Andr\u00e9"
  - subject: "when can we get English version?"
    date: 2007-04-17
    body: "I and my friends almost can't understand German. Sigh."
    author: "Cavendish Qi"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "How bad is language-education in your country, or did it not interest you, when at school?\nI can read German, (not write it correctly), as should everyone well educated in Europe and the U.S. (IMHO).\nI am Dutch and Dutch is certainly not German,as it may look in first sight.\nPS. same holds for French and Spanish and maybe Russian.\nAlas, no classes in Spanish or Russian in my time."
    author: "Douwe"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "duh.. you know a lot of languages.\n\nI'm german, and I have to admit, that I only know English and French. From time to time I'm doing some business in the Netherlands (Hoogkarspel), but I have a hard time understanding Dutch. Reading it is a bit better, though..."
    author: "Thomas"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "I am Portuguese and only know English besides my own. There is a great focus on English on my country (currently, it is being taught at school from 8 years old, I believe). With English so spread out, it makes little sense for public school to waste taxes on other languages; we have lots of language schools for those that want to learn more languages."
    author: "blacksheep"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Why should anyone in the U.S. learn german?\nThey dont even live near it, most people in the U.S. wont ever need to know german."
    author: "br men"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "American people don't learn Spanish, don't learn French and don't learn Portuguese, is it right? This 3 languages you can find at America, is it right..?\n\nCanada - french\nBrazil - portuguese\nall the other countries - Spanish\n\nIs there any language abroad teached at US schools....???\n"
    author: "Joshua"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "> Is there any language abroad teached at US schools....???\n\nHow about \"taught\"?"
    author: "Evil Eddie"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Nearly every high school and college in the US has classes in Spanish, French, and German.   Most colleges require at least 2 years of a foreign language to be admitted.\n\nThe problem is nobody can practice.   If we forced French (like Canada) on everyone, then you could practice with your peers, or if you live in the south west you have neighbors who speak Spanish.   For most of the country finding Enough people fluent in some language that you can practice enough to learn it is impossible.  So they don't really learn to speak the language.  After a few years they forget what they did learn.   And so the cycle goes.\n\nIt isn't that we are too stupid to learn any language, it is that we don't have the opportunity to do it right.\n\nMost of Europe forces English, so you can practice with your peers.  English is also today's language to know, if you are in a foreign country and you want to talk to someone, you use English.  If I go to Germany I will speak English, and nobody will know that I took Spanish.   Of course if I knew German I'd use German, but nobody would think about trying to speak Spanish with someone in Germany when it is more likely that the other person speaks English.  \n\nThere is also the time argument.   You only live so long.  If you take the time to learn a language, that is time you can't devote to something else.  Music teachers (band and choir) in school have a big problem getting great musicians to take their class - students know that they MUST take language classes, and that often leaves no time at all for music classes.  "
    author: "Henry Miller"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Actually, our experience has been fairly different. When my wife lived in US (in a pretty wealthy area, so no excuses there), she was surprised by how badly foreign languages and foreign geography were taught. She registered to attend an advanced course in Spanish at SDSU and she actually dropped it after 2 lessons because it was so appallingly basic.\n\nI do not think people who live in the USA, in particular in the southern states, miss any opportunity to practise Spanish. It is spoken by a very large minority, but few of the people we met spoke it. Essentially, only people who had Latin American origin.\n\n(A short anecdote: We met at a party a secondary school teacher from Orange County who was actually convinced that the \"Basque countries\" were in fact in Italy, and she was obviously passing it on to the students, to our astonishment. If what they teach about foregin languages matches it ....)"
    author: "eeos communications support (Corrado)"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "This is nonsense, I'll cite a 1995 government report on the topic:\n\n\"Most foreign language study takes place in Grades 9-12, where over a third of the students study a foreign language. Spanish is the most popular language, studied by about 28% of all secondary school students, followed by French with 11%, and German with 3%. At the primary level, over 6% of the students study foreign languages, again with Spanish leading the list at 4.5% followed by French with 1.5%, and German and Japanese each with 0.2% of enrollments.\"(1)\n\n(1) http://www.ncela.gwu.edu/pubs/resource/foreign.htm"
    author: "Jason"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "Re-read what I wrote very carefully, as nothing I said contradicts anything you said.   Every high school I know of has foreign language classes.   They are however optional, so not all students take them.    Most colleges require a foreign language for admission (but you can get a waiver if you take those classes in college instead).\n\nSpeaking a foreign language with friends is nothing like speaking with a native.   You just step back to English (or whatever) anytime the subject gets complex, so you don't learn the language well.\n\nTime is always an issue.  I wanted to take a foreign language in high school.   I also wanted to take choir, and math.   There was only enough free time for 2 of the 3, and I believe math should be required, so that left a choice between choir and a language.   Language lost because I consider music more important to me.   (A choice that I still consider right - I have yet to meet someone who speaks French or German but not English.   When I was in Spain there were enough English speakers that I could get by without Spanish.  I didn't know in high school that my job would send me to Spain and not Germany or France, so even if I had chosen to learn a language I might have taken the wrong one.\n\nI'm not arguing that learning a foreign language is a bad idea.  I'm arguing that there are plenty of other things that you can do instead that are also equally good."
    author: "Henry Miller"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "Practice is not an excuse. I rarely practiced english with my peers and same goes for them too. You can watch movies, listen to music, check out foreign web pages and irc channels, and you got stuff like: http://www.mylanguageexchange.com to find budies to practice over skype. And while I understand your argument about english being a good language to know, where I come from (Portugal) it's compulsory to learn 2 foreign languages, not just English. "
    author: "someone"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "I think for the vast majority there's no real incentive in such a big country, spreading from one ocean to the other and the world learning english anyways."
    author: "Carlo"
  - subject: "Re: when can we get English version?"
    date: 2007-04-22
    body: "I think you'll find that the order of languages spoken in the world by numbers is \n1. Chinese\n2. English\n3. Spanish\n\nI know esporanto exists but I've never come across anyone who could speak/read it and as far as I know I've never overheard it spoken in any European countries i've visited.\n\nIf i knew any german apart from a few words, i'd help in attempts to translate it in collaboration with other people (chapter each perhaps) and pass the translation back to the authors to allow them to put it forward for publishing. "
    author: "Ian"
  - subject: "Re: when can we get English version?"
    date: 2007-04-22
    body: "China has several languages that could be referred to as 'chinese' (plus a bajillion dialects and so on, apparently). you might want to specify mandarin :) (iirc mandarin is the most common in china, but vancouver has mostly cantonese)"
    author: "Chani"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "For one, they might be able to enjoy reading a book on KDevelop written in German :-)"
    author: "Andr\u00e9"
  - subject: "A little respect please"
    date: 2007-04-18
    body: "German is far easier to understand for an English speaker since it forms our root (combined with latin). Back in first grade, I took a year of spanish (required in texas). Later, when I moved to Ill, my parents decided that we should learn some German, so I had a year of that (5th grade). Then in high shcool, we were required to take some foreign language. We had the choice of Spanish, French, or German. I chose Spanish, as it made good sense for America. But the other 2 would have helped in various areas. In my neighborhood, we had German speakers (straight from German post WWII). Some of their words were in German esp when they would talk about the war. As to French, it has its uses. In addition, if you wish to obtain a Chem. degree in the states, you are REQUIRED to have 2 years of college germain. Even to this day, most major chemistry journals are German.<BR><BR>While it is Good that we have (supposedly) a universal language (we do not), I think that every America should learn another language and  bit of their culture (beyond the beer fest combined with a bratwurst (besides there are better meats there ( but the blutwurst sux; far too bland) or a tortilla with a bit of tequila). And now, it would be good if we were teaching Arabic and perhaps some chinese in our schools."
    author: "a.c."
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "> How bad is language-education in your country, or did it not interest you, when \n> at school?\n> I can read German, (not write it correctly), as should everyone well educated in \n> Europe and the U.S. (IMHO).\n\nHow bad is the respect for other people?\nNot all the people have the time (or the willing, or what else you can't blame them for) to learn a new language. You can't pretend people do what you want them to do."
    author: "Pino Toscano"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "I do not think it is lack of respect.\n\nI think it is really a shame that open source is becoming so English centric nowadays (not even proper English). I live in UK and do not speak German myself, but that is my fault and fault of the education system. In Europe, English is the language of a minority, whilst German is the language of the relative majority, followed by languages of Latin origin (French, Italian, Spanish to name but a few). \n\nI am very happy that this book has been published *AND* that it has been published in German. It means that the German open source community is alive .... and kicking!\n\nKeep up the good work!\n\n-- \nTechnical Support\neeos uk ltd (www.eeos.biz)\nCompany No. 05765416"
    author: "eeos communications support (Corrado)"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "I disagree, I think it is a great thing that open source is becoming English centric.   One language that everyone knows makes it possible to communicate with everyone.   There are more than 100 languages used in the world.  There is not enough time to learn all of them fluently - at least not if you want to accomplish anything else in life (like earn enough money to eat).\n\nI also think it is great that KDE has a strong I18n/l10n community, and translations into many languages.   \n\nNote that I'm not claiming English was a good choice.  It may even be the worst choice.   However this is one case where any choice is better than none.  I'm personally happy it is my native language because I have trouble learning languages, but I recognize that English has many faults."
    author: "Henry Miller"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "That should not forbid other people to speak their language or to publish books in their language, I hope.\n\nThe idea we should mumble or complain because the book has been publish in German instead of English is ludicrous. What should people do, publish even local literature in English?\n\nInstead of complaining, I think we should congratulate the lively German community for the achievement as well as learn German or pay a translator if we want to read their books."
    author: "eeos communications support (Corrado)"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "Just for the record, I agree fully.  Actually I don't see anyone complaining about German.   Everyone seems happy enough that this books is published.\n\nI do see complaints about it being in a language that they personally don't know (German).  This is no surprise - the dot is written in English, and so we can assume that everyone reading here knows English.  \n\nI also see complaints about people who do not know EVERY language ever spoken.   This is a subtle root of what they are saying.  There are people people here who know 3 or 4 languages, but not German who may wish to read a book like this.   Yet they are also being picked on for not knowing German."
    author: "Henry Miller"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "It is always nice to stir things up a bit."
    author: "eeos communications support (Corrado)"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "> Note that I'm not claiming English was a good choice.\n\nIn comparison to my native language, English was pretty damn good choice. We use it for pretty much every term in CS; even for stuff we do. Our language is just \"too verbose\" for CS like a professor of mine likes to put it. :)"
    author: "blacksheep"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "Being german, I can tell you it's more likely to find a dutch or polish person to be able to read/speak german, than a German to read/speak either one. I mean, reading dutch sentences, I can see the similarity of some words and  sometimes can guess the meaning of some simple sentences, but that was it. Same for the (not so) mutual interest in the political and cultural environment of the relevant coutries. There's just more incentive in smaller countries to be interested, since the economic, cultural, political power/climate/prosperity of the big neighbour affects the smaller coutries much more, than a weakness of a smaller one, does affect the big one.\n\nI guess it's a benefit coming from the netherlands learning german and vice versa. ;-) If you don't have a german speaking neighbour, what's the benefit, though. And btw. Russian is really hard to learn. Six cases, no articles, weird language. Polish may be even worse."
    author: "Carlo"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Why not learn German. Its worth."
    author: "andre"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "> Why not learn German. Its worth.\n\nWell consider Chinese and Spanish too! :-p\nBoth are one the most spoken languages in the World ;-)\n\nNo, and even in The Netherlands where we all learn 5 languages I can only speak and read two well: Dutch and English. "
    author: "Diederik van der Boor"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Yes, and that is something to seriously considder. As well as updating my French a bit. Yes it costs time and effort, but it also brings considderable benefits. I am really happy now that I learned English, German and French at school (besides Dutch). Even knowing some tiny basics in other languages can be a real benefit. And as you learn more languages, they get easier to pick up. "
    author: "Andr\u00e9"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Actually, Mandarin (chinese) is the most spoken language... but it's hard to say if the second is hindi or spanish."
    author: "shamaz"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "I can't find the article to back it up, but there are more second-language English speakers than first-language English speakers in the world.   This is not true for Mandarin or Spanish or so forth.  So the most spoken language depends on whether you could only \"native speakers\" or everyone that knows the language... Mandarin wins for \"native speakers\", but English would be understood by more people.\n\nWhich sucks, because English really is a ridiculous language to learn :) Has anyone tried to teach non-native English speakers about idioms before? :P"
    author: "Troy Unrau"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "> Which sucks, because English really is a ridiculous language to learn :) Has anyone tried to teach non-native English speakers about idioms before? :P\n\nTry to learn German or Russian before saying learning English is hard..."
    author: "Carlo"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "Come on... Most of the English idioms have <i>some</i> sort of meaning behind them... You've not heard insane idioms until you've heard Danish ones ;) (as my ex loves to point out *giggles* funny stuff, seriously :) )"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Why not Portuguese? It is the main language of the biggest number of countries (granted, most of them are small ;)). Timor Lorosae, the newest country of the new century, saw the light and adopt it; why not you all? ;)"
    author: "blacksheep"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Because all the country's already have a language?\nI dont see any reason to make a complete country switch from language.\nEven if u'd try it wouln't work."
    author: "br men"
  - subject: "Re: when can we get English version?"
    date: 2007-04-17
    body: "Mark Twain (who tried to learn German during his trip through Europe): \"I rather decline two drinks than a German adjective!\" ;-)\n\nUwe\n(neither an American nor a Brit)"
    author: "Uwe Thiem"
  - subject: "Re: when can we get English version?"
    date: 2007-04-18
    body: "wunderbar :-)"
    author: "Carlo"
  - subject: "Ordered"
    date: 2007-04-17
    body: "Thanks! I ordered it instantly - Together with Danimos Qt4 Book :-)"
    author: "Birdy"
  - subject: "la lojban se finti lo merko"
    date: 2009-05-16
    body: "la lojban se finti lo merko prenu gi'e jikybebna.pe'a kulnu gi'eku'i melbi"
    author: "snan"
---
If you are able to read German and use or plan to use <a href="http://www.kdevelop.org/">KDevelop</a>, it is now possible to get the help of a newly published book. In <a href="https://www.opensourcepress.de/index.php?26&amp;backPID=178&amp;tt_products=108">"KDevelop - Einführung in die Entwicklungsumgebung"</a> KDevelop contributors Jonas Jacobi and Robert Gruber will not explain in great length that "The 'New File' menu entry opens a new file", but concentrate on less self-explatory topics like advanced code navigation, documentation with <a href="http://www.doxygen.org/">Doxygen</a> or using <a href="http://www.valgrind.org/">Valgrind</a> with KDevelop.  The book, published by Munich-based publisher <a href="https://www.opensourcepress.de">Open Source Press</a>, uses KDevelop 3.4 as a reference.



<!--break-->
