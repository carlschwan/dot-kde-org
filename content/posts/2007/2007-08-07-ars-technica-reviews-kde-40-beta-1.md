---
title: "Ars Technica Reviews KDE 4.0 Beta 1"
date:    2007-08-07
authors:
  - "jriddell"
slug:    ars-technica-reviews-kde-40-beta-1
comments:
  - subject: "Riddell, you make me smile..."
    date: 2007-08-06
    body: "Heh, \"lengthy review\"... :) It was basically a rehash of my own articles that were posted here first :) Also, \"from the un-rah-oo dept\"? Aahahah.\n\nFor the 4.0 release, expect a 10 page feature :P"
    author: "Troy Unrau"
  - subject: "Re: Riddell, you make me smile..."
    date: 2007-08-06
    body: "\"KWin has now seen testing for nearly a decade and pretty much every corner case has been solved.\" , hehe, i other kde-sub-projects this counts as \"old code\" which need to be rewritten, or replaced with a \"better\" api :-)\n\nlike compiz fusion ? :) , just joking...\n\narts was also tested for nearly a decade and many many many more which were replaced for kde4(.0)"
    author: "jumpingcube"
  - subject: "Re: Riddell, you make me smile..."
    date: 2007-08-06
    body: "There's a difference between keeping a good, working and usable technology than keeping something that is unmaintained, and no one properly understands :)"
    author: "Troy Unrau"
  - subject: "Re: Riddell, you make me smile..."
    date: 2007-08-06
    body: "I understand it, that poor, poor orphaned thing.\nSpaghetti has a soul too, you know!"
    author: "Martin"
  - subject: "Re: Riddell, you make me smile..."
    date: 2007-08-08
    body: "> \"KWin has now seen testing for nearly a decade and pretty much every corner case has been solved.\" , hehe, i other kde-sub-projects this counts as \"old code\" which need to be rewritten, or replaced with a \"better\" api :-)\n\n> like compiz fusion ? :) , just joking...\n\n\nThere are a couple of differences here specific to kwin:\n\n- kwin has actually been rewritten once before, i.e. it already went through a \"now we have hindsight; let's see what we can do better\" phase during its lifetime. And that rewrite has held up really well.\n\n- Unlike Compiz Fusion, kwin is capable of scaling from the non-composited case over 2D XRender-based compositing right up to 3D OpenGL-based compositing while offering a full feature set and consistent behavior in all of those cases. No other window manager on Linux/Unix is presently capable of this, i.e. kwin is very much at the top of its game."
    author: "Eike Hein"
  - subject: "beauty by default"
    date: 2007-08-07
    body: "It is mentioned some where in this screen-shot http://arstechnica.com/journals/linux.media/kdebeta1_plasma.png, \n\nthat not all that beauty is shipped by default! This is annoying because there is incredible beauty in that screen-shot.\n\nThis is in regard to the fonts where a default KDE install has always been lacking. As a Joe User, I'd also like to see widgets made a little smaller and smoother to increase screen real estate. "
    author: "cb"
  - subject: "Screenshots or mockups?"
    date: 2007-08-07
    body: "i wonder about these screenshots:\n\nhttp://www.nuno-icons.com/images/estilo/\n\nare those real ones or just mockups.. if they are real.. they are AWESOME!!!!!!\ngood bye kicker hello raptor.. or how it is called nowadays ^^"
    author: "LordBernhard"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-07
    body: "and if this aren't real screenshots.. will raptor look a bit like this? *looking to aseigo*^^"
    author: "LordBernhard"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-08
    body: "Awsome !!!\n\n"
    author: "jumpingcube"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-08
    body: "Amazing."
    author: "asdx"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-08
    body: "Oh!! they are so beautiful, that, simply, they can't be real! :-("
    author: "Me"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-08
    body: "The code is getting to it.\nI'm working on the slider now, that will be used also for krunner, but that's another chapther. =)\nThe rest is coming with the plasma panel."
    author: "Riccardo Iaconelli"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-08
    body: "Hi Ruphy,\n\nThanks for the info :) So is this mockup roughly what you guys are going to be aiming for for 4.0, or just one of many possible designs?"
    author: "Anon"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-10
    body: "Yes, I would like to know that, too, Ruphy.  Please tell us!"
    author: "David"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-09
    body: "The file menu looks nothing less than totally awesome. And I'm glad that the scrollbars are more visible now although green wouldn't be my first choice ;-)"
    author: "Erunno"
  - subject: "Re: Screenshots or mockups?"
    date: 2007-08-10
    body: "It's UNIVERSALLY beautiful. really. most beautiful style *ever*.\nI really do hope this is the target. It's way nicer and more original than the current implemented style.\nAlthough it could be named oxygen chlorophyl edition ;^) "
    author: "thom"
  - subject: "Stability"
    date: 2007-08-08
    body: "But, IMHO, yet again, the whole environment will become really stable around 4.0.8. KDE 3.5.0 -RELEASE  3.5.7 -STABLE (?). ;) :)"
    author: "szlam"
  - subject: "Re: Stability"
    date: 2007-08-09
    body: "And did you really expect it to be any different? \n\nNo major distribution that I know of will ship KDE 4 by default until several months into 2008. OpenSuse 10.3 and Kubuntu 7.10 could have chosen to go with KDE4 instead of 3.5.7, but they realize that it will still require more testing in the wild before it can be shipped as default. The reason why we have KDE 3.5 in the first place is because the developers knew that it will take a while before KDE 4 matures to the same level as KDE 3. Otherwise they would have cut it off at 3.4.3(or whatever it was) and started working on KDE 4 several months earlier."
    author: "DR"
  - subject: "Re: Stability"
    date: 2007-08-09
    body: "I understand Fedora Core 8 will be shipping it.  They are aiming for a November 8th release."
    author: "David"
  - subject: "Re: Stability"
    date: 2007-08-09
    body: "Well, of course we try to have 4.0 stable and usable... But besides being stable, there is more needed to have it really finished. User generated content, for example. When KDE 4.0 is released, there won't be many plasmoids, scripts, styles and backgrounds for download with GHNS2. That'll take some time."
    author: "jospoortvliet"
---
Ars Technical points out that <a href="http://arstechnica.com/journals/linux.ars/2007/08/06/the-first-kde-4-0-beta-hits-the-streets">the first KDE 4.0 beta has hit the streets</a>.  "<em>A major milestone release such as 4.0 is a long time in the making. Here follows a number of things to look forward to for those of you brave enough to try this early beta.</em>"  The lengthy review covers the portability improvements, new build system, new multimedia framework, new artwork from Oxygen, the new Plasma desktop and composite effects in KWin.

<!--break-->
