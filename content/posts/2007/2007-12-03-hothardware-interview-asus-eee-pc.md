---
title: "HotHardware Interview on Asus Eee PC"
date:    2007-12-03
authors:
  - "wolson"
slug:    hothardware-interview-asus-eee-pc
comments:
  - subject: "Eee + konsole"
    date: 2007-12-03
    body: "The software selection on the Eee *is* a little weird, IMO. In several places capable KDE apps were skipped in favor of others, which strikes me as a good way to waste memory by loading multiple toolkits. The use of KNotes as the notes application is kind of weird, but it does work well with the simple-and-lite UI the Eee provides by default. And the trick to getting konsole up there is File Manager, Start Console. Then it's a sweet ssh client :) I still need to figure out how to mount an SD card on /home for general use, though."
    author: "Adriaan de Groot"
  - subject: "Re: Eee + konsole"
    date: 2007-12-03
    body: "there's also a keyboard shortcut for a terminal window, ctrl+alt+t i think? i can't remember if that pops up konsole or something else (i don't have an eee pc in front of me right atm =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Eee + konsole"
    date: 2007-12-03
    body: "Yes it's Ctrl+Alt+t."
    author: "Torsten Rahn"
  - subject: "Re: Eee + konsole"
    date: 2007-12-03
    body: "Or Win+T\n\nIt brings up (x/a/whatever)term or something by default so if you want konsole you'll have to edit the icewm keyboard shortcuts."
    author: "Leo S"
  - subject: "Re: Eee + konsole"
    date: 2007-12-04
    body: "> Or Win+T\n\nI thought I read this laptop has no \"Windows(tm)\" key but one with a Home symbol instead? was that a false information?\n\n\n"
    author: "paul"
  - subject: "Re: Eee + konsole"
    date: 2007-12-04
    body: "Well, it doesn't have the Windows logo on it, but its the same key.  If you press it in the KDE shortcut assignment dialog it register as \"Win\""
    author: "Leo"
  - subject: "Re: Eee + konsole"
    date: 2007-12-04
    body: "Not on the subject above?! I could not help it. But just opportunity to ask Aaron if he probably install KDE4 on Eee PC. (After all he got one for p-man in his blogs). I just get mine this weekend and hoping KDE4 would on it. Visit http://www.eeeuser.com what folks hacking in this unit. Thanks.  "
    author: "NS"
  - subject: "we shouldn't advertize eee"
    date: 2007-12-04
    body: "First it ships with one of the most proprietary distro Xandros that sold out to Microsoft patent FUD deal, second it violated the GPL and third you can't change RAM without voiding the whole warranty. Give me a break, we shouldn't advertise evil products and companies that go against the whole Free Software community (yes the MS patent deal is hijacking the whole community) just because they ship KDE. As it is stated on kde.org, KDE is supposed to be the best *Free Software* desktop and Xandros is definitively not about that."
    author: "Paul"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "They didn't really violate the GPL. They overlooked providing the source on the website, but after being asked for it, they provided it in (I think) less than a weeks time. The GPL only requires that you provide the source upon request on a medium conventionally used for sourcecode.\n\nAnyway, they fixed their oversight pretty quickly. Seems far from evil to me."
    author: "anon"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "Blah blah blah.  Nothing about Xandros is particularly proprietary.  You can put any linux you want on the machine (I'm running debian sid on it).  Yes asus' handling of the source code for their kernel modules wasn't ideal, but cut them some slack, this is a first for them.  The EeePC is an amazing little machine for very little money, and it is really giving the KDE apps more exposure."
    author: "Leo S"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "Maybe a group of forum experts could post on who KDE should and shouldn't work with?\n\nPlease sort in descending order of software libre purity and denote clear demarcations where we should stop accepting funding for events, allowing e.V. members and upstream patches.\n\nThanks in advance.\n"
    author: "Wade Olson"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "Well if these companies want to fund KDE that's fine obviously. But promoting proprietary products on a free desktop website is not very consistent. It's like saying you should use KDE because it's great and free software oh by the way check out that proprietary OS it's really neat... Or a vegetarian fundation saying 'eating animals is bad! oh by the way did you check out the latest new hamburger from McDonalds? they're funding us so who cares, please get some experts to decides who we can promote' "
    author: "Paul"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "We deliberately LGPL our libraries to allow for proprietary products. It's you being inconsistent here, not us."
    author: "Richard Moore"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "Then blame Microsoft, not ASUS. Patent deals make companies nervous, nobody wants a lawsuit from Microsoft. Better safe then sorry as the saying goes. Secondly that was an oversight that they quickly fixed, thirdly...yes thats lame but you can't do much do electronics without voiding the warranty.\n\nStop raging against the machine, ASUS is another big company picking up Linux and hopes to sell millions. This is nothing but good."
    author: "Skeith"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "The code is available. If you unscrew the back, open the case, you void your warranty.\n\nDon't buy one then. I think you'll find the Eeepc one of the most hacked pieces of hardware in a very short time.\n\nDerek"
    author: "D Kite"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "Asus/Xandros did not violate the GPL. They put up a sources tarball even way _before_ the product was launched. As a Free Software evangelist who has contributed to Free Software since 10 years I'd say that gives some extra karma bonus already.\n\nThe fact that parts of the sources were missing for a few days can be attributed to tight schedules and organizational issues.\nI've been more or less involved with many projects already (and yes, our company credativ also has got Xandros as a business partner).\nAnd I have yet to see a company -- even one consisting of 100% Free Software advocates -- where minor human accidents like these would _not_ happen (In fact I'm pretty sure that if you dig deep enough on distrowatch about 75% of LiveCDs have similar \"source issues\" as well as several \"minor\" distributions).  \n\nThe EeePC is based almost exclusively on free software. That is already a very nice win for Free Software if you consider that this means that potentially a few million people will run happily some software that is closely where we'd like Linux to be.\nYes, personally I'd like to see the EeePC making even more use of Free Software, too. Ideally I'd even like to see companies being able to sell hardware with fully open and free specs.\n  \nThrowing stones however like you do doesn't really help to get anywhere near this vision. In fact it's harmful and it's in fact the worst Linux \"evangelism\" that one can think of. \n\n"
    author: "Torsten Rahn"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "\"Throwing stones however like you do doesn't really help to get anywhere near this vision. In fact it's harmful and it's in fact the worst Linux \"evangelism\" that one can think of.\"\n\nI suspect that people such as the person you are replying to like to think of themselves more as \"vigilantes\" than \"evangelists\".  "
    author: "Anon"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "I didn't expect the Spanish Inquisition!\nOh, come to think of it, in foss country I should.\nI think all religious people should be burned at the stake.\n"
    author: "reihal"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-05
    body: "Nobody expects the Spanish Inquisition!!!!! :D"
    author: "hansl"
  - subject: "Re: we shouldn't advertize eee"
    date: 2007-12-04
    body: "KDE is supposed to be the best desktop.\n\nFree as in freedom or beer isn't necessarily motivating every developer. Many, perhaps even most, but possibly not all.\n"
    author: "Brad Hards"
  - subject: "Re: we shouldn't advertize eee  . . ."
    date: 2007-12-09
    body: "ASUS is happy to send you an e mail telling you that you can indeed upgrade your RAM without voiding the warranty. I predict that in units with the empty mini PCI slot, you will be able to fill that too (as it is easily accessible).  And they are changing the sticker.  What will void your warranty is hacking the mobo or disassembling and modding the EEE.\n\nI am a research physicist and engineer, and have been happily using computers since the days of paper tape, and I currently use half a dozen of them, Mac OS X, Windows, Unix etc. The EEE absolutely fills the niche of the low weight portable you can take anywhere. And thanks to good design and use of LInux, performance in basic tasks (mail, word processing, spreadsheets)  not far of my Ibook G4, which weights 3x as much. I hook it up to a large LCD monitor and USB keyboard to use it at home. I predict it will be smashing success, and really puts the boot into $1000+ UMPCs.\n\nThey have indeed published their source code, so accusations of \"violating GPL\" are simply not valid. The \"easy mode\" interface is very good for use on the go, and you can also run most of KDE or windows. Compromises had to be made, but I find them reasonable for this kind of use---and this kind of price.  The hackers are hard at work coming up with how-tos for installing more software, other flavors of Unix, bluetooth, and so on. This--and the Walmart $200 Linux PC---are a big step toward bringing Linux to Everyman. Linux diehards should quit the whining about ideological purity and take pride--and get credit---for at what they have done is spreading computer literacy faster than ever. \n\nFor $399 from Amazon etc, this is a great deal. I  got mine on ebay with warranty for $370 incl shipping, and I feel very lucky indeed. \n\nvery best  regards\n\nJHH\n\n\n\n\n"
    author: "jhh"
  - subject: "Re: we shouldn't advertize eee  . . ."
    date: 2007-12-09
    body: "ASUS is happy to send you an e mail telling you that you can indeed upgrade your RAM without voiding the warranty. I predict that in units with the empty mini PCI slot, you will be able to fill that too (as it is easily accessible).  And they are changing the sticker.  What will void your warranty is hacking the mobo or disassembling and modding the EEE.\n\nI am a research physicist and engineer, and have been happily using computers since the days of paper tape, and I currently use half a dozen of them, Mac OS X, Windows, Unix etc. The EEE absolutely fills the niche of the low weight portable you can take anywhere. And thanks to good design and use of LInux, performance in basic tasks (mail, word processing, spreadsheets)  not far of my Ibook G4, which weights 3x as much. I hook it up to a large LCD monitor and USB keyboard to use it at home. I predict it will be smashing success, and really puts the boot into $1000+ UMPCs.\n\nThey have indeed published their source code, so accusations of \"violating GPL\" are simply not valid. The \"easy mode\" interface is very good for use on the go, and you can also run most of KDE or windows. Compromises had to be made, but I find them reasonable for this kind of use---and this kind of price.  The hackers are hard at work coming up with how-tos for installing more software, other flavors of Unix, bluetooth, and so on. This--and the Walmart $200 Linux PC---are a big step toward bringing Linux to Everyman. Linux diehards should quit the whining about ideological purity and take pride--and get credit---for at what they have done is spreading computer literacy faster than ever. \n\nFor $399 from Amazon etc, this is a great deal. I  got mine on ebay with warranty for $370 incl shipping, and I feel very lucky indeed. \n\nvery best  regards\n\nJHH\n\n\n\n\n"
    author: "jhh"
  - subject: "Re: we shouldn't advertize eee"
    date: 2008-02-12
    body: "Give us a break. They posted the source code and removing the sticker on the expansion bay does not void warranty. Asus will confirm that. The EEE is the best thing that could happen for Linux and KDE. Just know it."
    author: "Vernon"
  - subject: "On topic; about the Eee PC"
    date: 2007-12-04
    body: "It's the type of portable I have been waiting for.\nUnfortunately, the screen is too small, I need 800x600 resolution.\n\nAsus: If you read this, 512 MB RAM and 2-4 GB ROM is ok, add more pixels instead."
    author: "reihal"
  - subject: "Re: On topic; about the Eee PC"
    date: 2007-12-06
    body: "I have to agree.  I would like a small portable but need the screen resolution.  Perhaps a half screen portable (1024x384) or 3/4 screen (1024x576 [which is actually wide screen]) would be a good compromise.  Perhaps they will follow on with a slightly more expensive product directed at adults."
    author: "JRT"
  - subject: "Re: On topic; about the Eee PC"
    date: 2007-12-08
    body: "I disagree, both my wife and myself are ... adults I think ;-). We bought one and we are loving it. Everything works out of the box beautifully. WiFi, Function keys, suspend, etc. \n\nOn the screen, I agree, more resolution would be great, but I don't think it would be wise for them to sacrifice portability. Still, there is some room around the existing monitor. The speakers could be moved to the top, around the camera, and the rest of the space used for a slightly larger LCD, maybe an extra 2-3 cms. If that can make it a 912x600 or something, this would be an improvement.\n\nAlso, they will apparently release a 10 inch, _larger_ one. You may be interested on that one. \n\nBack to KDE, it only takes like five minutes to activate the \"advanced\", KDE  interface, which looks and works awesome!\n\nCheers!"
    author: "KubuntuUser"
  - subject: "Re: On topic; about the Eee PC"
    date: 2007-12-10
    body: "No, this model with a screen that fills the whole lid area.\nSacrifice the loudspeakers and the microphone, they are not needed.\nBluetooth for headphones would be nice instead."
    author: "reihal"
---
The new <a href="http://eeepc.asus.com/en/">Asus Eee PC</a> has been released to many positive reviews and great consumer interest.  A streamlined and customized <a href="http://www.xandros.com/">Xandros</a> and KDE interface combines with other free software applications, a slim form factor and an attractive price point.  HotHardware.com was one of the very first sites to <a href="http://www.hothardware.com/articles/Asus_Eee_PC_Full_Retail_review_Showcase/">
review</a> the Asus Eee and report back on this new device.  Read on for an interview with Editor-in-Chief Dave Altavilla and his thoughts on the Asus Eee PC and its value proposition.






<!--break-->
<h3>Please give us the history behind 
<a href="http://www.hothardware.com">HotHardware.com</a>. 
(Reasons for site creation, etc).</h3>

<p>That's a fairly loaded question there Wade but I'll try to
be brief.  HotHardware.com has been in existence now for
over 8 years.  My vision was fairly clear from the beginning
but certainly has evolved over the years a bit to what we're
all about today.  In the early stages, I felt passionate
about computing technologies (like many of our readers of
course) and in a general sense have always loved the
creative writing process.  Mix these two interests together
and you have the makings of a Tech Journalist I guess.  In
these earlier days of the net, things were just ramping up
so it took a while before some of the major OEMs saw the
value of Web Media.  Our industry has come a long way since
then; from our initial classification of being considered
just &quot;early adopters&quot; and &quot;enthusiasts&quot;, to now what many
major OEMs consider as valuable trend-setters and critical
&quot;user expert&quot; feedback resources, in addition to being very
influential members of the press.</p>

<p>About a year into it I met my good friend and now Managing
Editor of our site (aka the Backbone of HH), Marco
Chiappetta.  With Marco on board we gained what I would call
critical mass and it has been upward and onward ever since. 
We're currently one of the top sites in our field and are on
virtually every major first-round product launch roll-out to
the press with all of the top-tier OEMs and board partners. 
In short, life is pretty good!</p>

<h3>How does HotHardware differ from other hardware sites on
the web?</h3>

<p>We try to walk our talk, so to speak, to the best of our
ability.  We position ourselves at a consumer-targeted site,
whether you consider the enthusiast, novice or IT
Professional looking for valuable input.  Of course, since
you can&apos;t be all things to all people, we try our best to
take the middle ground and appeal to the broadest scope of
the available readership in our industry as possible.</p>

<p>As such, we offer detailed analysis and evaluation of
leading-edge products and technologies in computing and
adjacent markets, in what we like to think is cleanly
presented, easily digestible chunks.  That is to say that on
HotHardware, you will not see a horrendously long,
over-the-top with technical drivel style article, nor will
you see what is essentially a regurgitation of marketing
hype that amounts to nothing more than a fluffy commercial. 
Instead we strive to offer our readers a down-to-earth,
professional perspective on the &quot;out of the box&quot; experience
with any given product or technology.  OK, so this thing has
2X the memory bandwidth and 4 times the processing
horsepower, so what does that mean to the reader?  We try to
be very clear on the &quot;moral of the story&quot; and not stray too
far into minutia but at the same time provide thorough
coverage of the salient points, good or bad, of each
product.  We also try to hit this watermark in our breaking
industry news content as well.</p>

<h3>Who are the typical readers and forum posters for
HotHardware?</h3>

<p>Out audience is made up of Computing Enthusiasts, early
adopters of tech products and IT professionals.  We also
have a fair number of computer newbies that come to our site
to learn a few tricks and possibly get assistance on upgrade
suggestions and other buying decisions.</p>

<h3>Are hardware enthusiasts more likely to have an interest
in different operating systems and software?</h3>

<p>Absolutely, these people are not afraid to get down and
dirty with their setups.  If there&apos;s a perceived advantage
to a new part of their total system solution, they&apos;re going
to investigate it and prove it out.  We have lots of readers
that run dual boot setups of Windows and Linux packages.</p>

<h3>HotHardware.com 
<a href="http://www.hothardware.com/articles/Asus_Eee_PC_Full_Retail_Review_Showcase/">
recently reviewed</a> the new Asus Eee PC.  Can
you summarize your findings?</h3>

<p>The Asus Eee PC is a breakout product in many ways.  First,
of course it&apos;s a highly portable PC that has many of the
significant capabilities of a full sized notebook.  It has
the form factor of a UMPC, with the usability of and
functionality of a much more substantial machine.</p>

<p>In addition, the Linux-loaded version we tested has such an
amazing complement of software applications, that you get
the feeling the machine has all you need to utilize it to
the fullest of its capabilities.  It&apos;s stable, highly
functional and perfect for people on the go, students, or
even as a first kid&apos;s PC.  I think moving forward the
product will see some explosive growth in the end user
community, especially with the open source crowd and modders
that will do some impressive things with it.  There&apos;s a
Windows XP capable version in the works as well, so we&apos;re
told.</p>

<h3>In the review, you wrote "this product has been easily
the most researched and searched-upon product in the HotHardware.com
content database in many years."  Why do you think people
have been so interested and intrigued about this product in
particular?</h3>

<p>Well, primarily because it&apos;s an ultra low-cost,
ultra-portable PC that comes with a ton of software
pre-installed.  It&apos;s a full-up computing platform at a
killer price-point.  It also appeals to many user types,
from professionals looking for an unobtrusive machine to
take notes on in a meeting, to kids in grade school learning
the ropes for the first time on a PC.</p>

<h3>How does the Asus Eee compare to the <a href="http://laptop.org/">
OLPC XO-1 notebook</a> or the 
<a href="http://www.intel.com/intel/worldahead/classmatepc/">Intel ClassMate PC</a>?  
How do the different target markets for these devices reflect in their respective
hardware and software choices?</h3>

<p>This is definitely an interesting and debatable topic.  From
my early perspective, to me the XO-1 definitely caters more
directly to children.  That&apos;s no surprise obviously, since
that&apos;s the whole premise of the OLPC program.  The Intel
Classmate is similar in this regard, however it&apos;s much more
a kin to the Eee PC, architecturally with respect to
motherboard and processor design which, like the Eee PC, is
an Intel Mobile Celeron design.  Regardless, comparatively,
the XO-1 and Classmate PC look and feel much more toy-like
versus what Asus built, which is essentially a classic,
sleek design of an ultra-portable notebook.  The Eee PC
should have much more broad-market appeal as a result.</p>

<h3>With lower and lower price points on devices like these,
is freely available software becoming more important from an economic
standpoint?</h3>

<p>I think so.  At least it should be.  The pre-installed Open
Office, Firefox and other open source software products that
are on the Eee PC, are a huge advantage for the system. 
Integrating all that functionality for free should offer
much more competitive price points when you consider the
entire package and bundle being purchased.</p>

<h3>In the case of the OLPC XO-1 notebook and the Intel
Classmate PC, how important is it that the software is free from a
philosophical standpoint?</h3>

<p>I don&apos;t know about philosophies but both of those systems
are budget PCs target to kids and need to be very cost
efficient solutions over all.  Parents, schools etc don&apos;t
want to have to worry about buying and loading up additional
software to get their children up and fully functional on
the machines.  Obviously, it would be pretty pointless if
either OLPC or Intel were to develop a machine that would
have a total cost of ownership somewhat higher than the
initial price tag. Again, for kids in third-world countries,
the primary target market for these machines, you need a
shrink-wrapped total solution with as little configuration
and optional purchases required as possible.  It&apos;s common
sense really.</p>

<h3>Simplified desktop UIs are nothing new.  How does the
Asus Eee succeed and how does KDE play a part in that
success?</h3>

<p>To be perfectly honest, I&apos;m not an expert user by any
stretch of the imagination, when it comes to Linux
interfaces and distros.  The experience I had with the Eee
PC was fairly &quot;pure&quot; in that I was looking at the machine
with a fresh, somewhat novice end-user perspective.  I mean
we&apos;ve tested literally thousands of systems, notebook and
mobile devices at HotHardware, most all of which were
configured to run a Windows operating system of some sort.</p>

<p>As such, we&apos;re VERY comfortable and fluid power users with
Windows OS setups, so a KDE-based interface running Linux
was a bit of a departure for us.  In this regard, on a
certain level, we could have been very harsh critics about
the OS configuration, its performance and functionality on
the Asus Eee PC.  Regardless, we were all very impressed
with the streamlined, easily navigable KDE-based UI.  In
fact, it all seemed very natural and super intuitive.  KDE
and Asus developed the OS specifically for the Eee PC and
you can definitely see and feel the forethought  that was
put into it.</p>

<h3>Looking forward, what changes do you see in operating
systems and software based on your experiences with
HotHardware?</h3>

<p>I think, if there&apos;s one thing we&apos;ve learned over the years,
whether you consider open source OS solutions, Apple OS or
Microsoft, it&apos;s that end-user simplification of the UI needs
to continue to evolve.  If there&apos;s a shining example of
things to come, it&apos;s probably what Apple has done with the
iPhone and iPod touch.  The average end user doesn&apos;t want to
&quot;tinker&quot; like we like to do in the labs here at
HotHardware.com.  They want to just boot up (and quickly),
run their requested program and not worry about anything. 
In short, it just works and it doesn&quot;t take any learning
curve to work it.  That day is coming.  The question isn&quot;t
if but when.</p>

<h3>Any advice to the KDE community?</h3>

<p>Advice?  Me?  I wouldn&apos;t claim to be able to offer you any
words of wisdom but certainly words of encouragement are in
order.  Keep up the great work and keep doing what you&apos;re
doing.  When a product with such huge, sweeping appeal, like
the Asus Eee PC, is showcased with a KDE interface at the
helm, you know that&apos;s a lot of good exposure and there&apos;s
significantly more critical mass behind the community in
general.</p>



