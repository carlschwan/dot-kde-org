---
title: "Earthweb Tours KDE 4.0 Beta 2"
date:    2007-09-20
authors:
  - "sk\u00fcgler"
slug:    earthweb-tours-kde-40-beta-2
comments:
  - subject: "Performance problems"
    date: 2007-09-20
    body: "When opening this page, Konqueror freezes and then comes up with something like \"A script on this page slows down the rendering speed. [Abort|Continue]?\". (Using KDE 3.5.5 Release 45.4.)\n\nAre there people experiencing similar problems?"
    author: "Stefan"
  - subject: "Re: Performance problems"
    date: 2007-09-20
    body: "I use firefox but it's flash-ad."
    author: "Menne"
  - subject: "Re: Performance problems"
    date: 2007-09-21
    body: "No problems on Konqueror 3.5.7 (flash and js enabled)."
    author: "Sutoka"
  - subject: "Re: Performance problems"
    date: 2007-09-23
    body: "No problems with Konqueror 3.5.7. Javascript and Flash turned off."
    author: "yttrium"
  - subject: "Konqueror - again"
    date: 2007-09-21
    body: "Mr. Byfield says:\n\"If you want, you can still use Konqueror as a file manager, but much of that functionality has been removed, making it a much less cluttered application.\"\n\nI heard that Konqueror will not be \"dumbed down.\" So either he is wrong or it is \"dumbed down.\"\n\nHe also says:\n\"At any rate, Dolphin, which has been in development several years, has always been far more powerful than Konqueror as a file manager, with not only a default directory tree pane, but the ability to split and merge panels and to offer some innovatively useful views, including an indexed one.\"\n\nI have used Dolphin. It may be easier to use for your average user. Though I still consider Konqueror \"more powerful.\" I think that the KDE devs have decided to keep Konqueror for \"power users\" and Dolphin for \"average users.\"\n\nIs this not so?\n\nAlso he talks much about KDE 4 and KDE 4 beta. So I think many people still don't get the point of KDE 4.0 and KDE 4.0 Beta."
    author: "winter"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: ">> I heard that Konqueror will not be \"dumbed down.\" So either he is wrong or it is \"dumbed down.\"\n\nI think many things in Konqueror are broken at the moment. Otherwise I can't say what's been \"removed\". Maybe getting some love, thus reducing the \"clutter\".\n\n>> I have used Dolphin. It may be easier to use for your average user. Though I still consider Konqueror \"more powerful.\" I think that the KDE devs have decided to keep Konqueror for \"power users\" and Dolphin for \"average users.\"\n\nReally, I don't get why everyone thinks Dolphin is some kind of \"you're too stupid to use Konqueror; here, have a go with Dolphin\". Dolphin is a powerful file manager, but Konqueror is more powerful in the sense that it can handle more things - browse the web, use KParts etc. Konqueror is for people who like this approach, Dolphin a general (but still, in my opinion, powerful) file manager for everyone.\n\nDisclaimer: I haven't read the article yet. Everything I've written is only my interpretation."
    author: "Hans"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: ">> Really, I don't get why everyone thinks Dolphin is some kind of \"you're too stupid to use Konqueror; here, have a go with Dolphin\".\n\nPerhaps because that is what we were told. No?"
    author: "winter"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: "i'm going to have to go ahead and disagree with you there - i have certainly never seen that being said by anyone actually involved with creating the thing... So... citation needed? ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: ">> \"you're too stupid to use Konqueror; here, have a go with Dolphin\".\n> Perhaps because that is what we were told. No?\n\nNo, it was what the lot's of people said, except the developers themselves. The Dot has seen a lot of reactive complaints to the announcement; how \"Dolphin\" was missing features and that implied KDE was going towards GNOME's direction. Stuff like KIO support was not in Dolphin, tabs and treeviews were not in Dolphin. Those were valid points, but they missed the fact the developers planned to add those missing parts.. :-/\n\nThe truth is Dolphin was a good base to build a better file manager. I really like Dolphin in KDE 4. It looks awesome, works really well, much easier to configure, customize, nicer for the eyes and I can't say I miss anything anymore. That's the Dolphin we're talking about today!! :-) Not the version many still think Dolphin perceive to be. ;-)"
    author: "Diederik van der Boor"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: "What gets me is when people bemoaned the lack of tabs and such, the response was the Dolphin is supposed to be very simple, and light of features.\n\nIn all honesty, I have yet to really delve into it, and I'm waiting to switch to KDE 4, so I can't say from experience how powerful, or simple Dolphin is."
    author: "T. J. Brumfield"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: "I don't know where he's pulling the removal of functionality, or dolphin being more _powerful_ from.\n\nIf anything, konqueror's getting more functionality, because many things being developed for dolphin are being applied to konqueror. Dolphin isn't, and never really has been more powerful than konqueror for file management (konqueror has tree-view and split panels, as well as document viewers, a shell, and a ton of other stuff)."
    author: "Soap"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: "> Mr. Byfield says:\n> \"If you want, you can still use Konqueror as a\n> file manager, but much of that functionality has\n> been removed, making it a much less cluttered application.\"\n>\n> I heard that Konqueror will not be \"dumbed down.\" So either\n> he is wrong or it is \"dumbed down.\"\n\nNo functionality of Konqueror will be removed for KDE 4.0. Until now David Faure invested a lot of time getting the core libraries for Konqueror and Dolphin in shape for KDE 4.0. He did not have that much time yet for Konqueror, but the Dolphin KPart (which can be embedded in Konqueror) has already been written and (at least basically) integrated. As David is an incredible good programmer I have no doubt that he will bring Konqueror in good shape for KDE 4.0 :-)\n\nSo the file management part of Konqueror will keep its tab functionality, its unlimited split functionality, its custom context menu, ... - all the things why people love Konqueror. If one minor thing would not make it into KDE 4.0 (we are all human), we'll do our best for getting this into KDE 4.1.\n\n> I have used Dolphin. It may be easier to use for your average user.\n> Though I still consider Konqueror \"more powerful.\" I think that\n> the KDE devs have decided to keep Konqueror for \"power users\"\n> and Dolphin for \"average users.\"\n\nI fully agree that Konqueror is more powerful. Still this \"Konqueror is for power users\" and \"Dolphin for average users\" does not hit the point in my opinion... My vision of Dolphin was that it is a simple to use and fast file manager. With \"simple to use\" I never meant \"drop all features that might confuse average users\". E. g. we integrated an embedded console in Dolphin, which for sure is not something for a typical \"average user\".\n\nBut I differ between features that clutter the UI and make Dolphin less simple to use and features that are non-intrusive to the UI. The embedded console part or the tree view panel are features which are (nearly) non-intrusive and off per default. They can be turned on by selecting one menu entry and are useful for a lot of users out there. I also investigated a lot of time that the available features in Dolphin are highly configurable, so that the features can be adjusted to the needs of a broad user group (not only \"average users\").\n\nWhat I want to say is that I intended Dolphin to be a powerful file manager, but never at the costs of simple to use. As Dolphin exports all its file manager capabilities to Konqueror and Konqueror can do do a lot of more things too, Konqueror will always be more powerful than Dolphin. Still this does not mean that Dolphin is a file manager for \"dummies\" ;-)\n\nKonqueror and Dolphin are just 2 different approaches sharing a lot of things internally. I think the nice thing is that people can chose between the 2 applications no matter whether they see themselves as \"power users\" or \"average users\"...\n\nBest regards,\nPeter"
    author: "Peter Penz"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: ">> The embedded console part or the tree view panel are features which are (nearly) non-intrusive and off per default. They can be turned on by selecting one menu entry and are useful for a lot of users out there.\n\nCareful, or you'll get a bunch of feature requests about tabs in Dolphin again. ;)\n\nTalking about feature requests, I have an idea I want to share. Just need to make a mockup when I get my stationary back (still stuck with Photoshop, unfortunately)."
    author: "Hans"
  - subject: "Konqueror and Webkit?"
    date: 2007-09-21
    body: "\"Until now David Faure invested a lot of time getting the core libraries for\nKonqueror and Dolphin in shape for KDE 4.0. He did not have that much time yet for Konqueror\"\n\nWhere does this leave the transition to webkit?  Has that happened?  Konqueror in the betas seems to render a little nicer, but is that just Qt4's new font rendering?\n\nBasically... when is Konqueror going to be at using webkit, at or above the level of functionality it now has with KHTML?"
    author: "Lee"
  - subject: "Re: Konqueror and Webkit?"
    date: 2007-09-21
    body: "\"Basically... when is Konqueror going to be at using webkit, at or above the level of functionality it now has with KHTML?\"\n\nThere's absolutely no guarantee that it will, ever - all we have is some rumours, and a fairly inactive stab at creating a WebKit KPart."
    author: "Anon"
  - subject: "Re: Konqueror and Webkit?"
    date: 2007-09-22
    body: "Oh, really?  Hmm.. that's disappointing.  At least it clears things up though, thanks :)"
    author: "Lee"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: "I think the reason people get confused about the difference between KDE 4 and KDE 4.0 is that 4 and 4.0 generally mean exactly the same thing. I expect that if we called a pre-release of KDE 4.0, KDE 4.00, it would also confuse people.\n\nAnd at the risk of being annoying, I love Dolphin, despite being a 'power user'. The only thing I miss is tabs. (And I keep pressing Ctrl-N to get a new window and it makes a new folder, but I guess I can at least configure that.)"
    author: "Johhny Awkward"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: "that last thing is of course configurable, but I think it should indeed open a new window by default. Maybe post a bugreport/wish, or email the mailinglist..."
    author: "jospoortvliet"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: "> And I keep pressing Ctrl-N to get a new window and\n> it makes a new folder, but I guess I can at least configure that.\n\nIn Dolphin for KDE 4.0 Ctrl-N opens a new window like in Konqueror :-)"
    author: "Peter Penz"
  - subject: "Re: Konqueror - again"
    date: 2007-09-22
    body: "I have one question :\nIf I have a Dolphin window opened on a directory, a Konqueror window opened on an FTP site, can I drag a file from Dolphin to Konqueror and upload this file directly? The same with FISH, SMB, etc? And download the same way too?"
    author: "Richard Van Den Boom"
  - subject: "Re: Konqueror - again"
    date: 2007-09-23
    body: "Sure, this is no problem :-)"
    author: "Peter Penz"
  - subject: "Re: Konqueror - again"
    date: 2007-09-24
    body: "Cool!\nI know it's probably obvious, but since we're talking about two different apps, it seemed to me that making it work was not so trivial.\nWell, looking forward to it. :-)"
    author: "Richard Van Den Boom"
  - subject: "Re: Konqueror - again"
    date: 2007-09-21
    body: "Well, I don't know if I'm a power user or not, but while I was dubious at the beginning, I am now looking forward to see Dolphin in action.\nFor actually simple reasons : I like to have file manager windows rather small and I like to have open new windows in windows, not tabs. On the contrary, I like to open web pages on new tabs and I like my browser to open with a large window.\nSomewhat, I've never managed to get Konqueror to handle both ways properly, and having two different apps will hopefully fix it for me."
    author: "Richard Van Den Boom"
  - subject: "Sidebars"
    date: 2007-09-24
    body: "Is there any plan to unify the different sidebar solutions in KDE4?\n\nOkular uses big icons ( http://www.kdedevelopers.org/node/2921 ),\nAmarok it's current side-tabs-with-rotated-labels-on-all-tabs approach ( http://amarok.kde.org/blog/uploads/Amarok2svgsandservices.png ).\nI can only guess whether Konquis gonna keep its current side-tabs-with-rotated-label-only-on-activated-tab style because every time I try to toggle on the nav panel Konqui crashes.\nIf you arrange the different toolbars in Dolphin as tabs (by d&ding them on top of each other), they get arranged in standard at the top (was it bottom?) which doesn't really work at all.\nI also don't know whether kpdf's vertically-arranged-horizontal-tabs ( http://kpdf.kde.org/screenies/04.contents.png ) are still around.\n\nNow I'm all for using the best approach for the job at hand but I think using 3 or 4 (or 5 or 6) different ways of implementing very similar functionality is just inconsistent and unnecessarily confusing. Why not unify sidebar-tabs and make an option in kcontrol so we can choose our favorite solution for all KDE apps?\n"
    author: "ac"
  - subject: "Re: Sidebars"
    date: 2007-10-10
    body: "++"
    author: "riddle"
---
Earthweb takes a look at KDE 4 Beta saying that "<i>Few major pieces of free software are more eagerly awaited than KDE 4</i>". The article <a href="http://itmanagement.earthweb.com/entdev/article.php/11070_3696671_1">Touring the KDE 4 Beta</a> covers most of the important changes in KDE 4 and concludes "<i>[...] despite the complexity, to judge from the beta, KDE has a high chance of realising all the ambitions wrapped up in KDE 4."</i>



<!--break-->
