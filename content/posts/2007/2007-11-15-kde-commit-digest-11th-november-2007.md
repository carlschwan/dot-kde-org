---
title: "KDE Commit-Digest for 11th November 2007"
date:    2007-11-15
authors:
  - "dallen"
slug:    kde-commit-digest-11th-november-2007
comments:
  - subject: "Thanks Danny"
    date: 2007-11-14
    body: "Thanks, thanks, thanks !!!!!!!!!! "
    author: "Marc"
  - subject: "Re: Thanks Danny"
    date: 2007-11-14
    body: "Thank you Danny. Your work is very appreciated !"
    author: "Shamaz"
  - subject: "Re: Thanks Danny"
    date: 2007-11-15
    body: "Many thanks, Danny!"
    author: "mactalla"
  - subject: "Re: Thanks Danny"
    date: 2007-11-15
    body: "Thanks!!"
    author: "Dirk"
  - subject: "Re: Thanks Danny"
    date: 2007-11-15
    body: "Thanks!!!!!!!!!!!!!!!!!!!!!!!"
    author: "Fran"
  - subject: "Re: Thanks Danny"
    date: 2007-11-23
    body: "You reminded me of the mask salesman in Zelda: Majora's Mask when you said that :)"
    author: "Lee"
  - subject: "Thanx"
    date: 2007-11-14
    body: "Thanks for the Digest Danny but why do you always put the sunday date and publish it on Wednesday? I guess you don't have time to release it on Sunday and that's ok but then you should put the date of Wednesday no? :)"
    author: "Patcito"
  - subject: "Re: Thanx"
    date: 2007-11-14
    body: "It is about the commits until Sunday, isn't it?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Thanx"
    date: 2007-11-14
    body: "Thanks for Digest."
    author: "m."
  - subject: "Re: Thanx"
    date: 2007-11-15
    body: "I could be mistaken, but I think the digests used to come out on Monday, and eventually Tuesday.  I'm assuming Danny does these as an unpaid volunteer, and I certainly look forward to them each week.  Sure, I get a bit impatient when they aren't here, but I'm just grateful when they do show up and I get to read them.\n\nI'm assuming Danny (like many of us) is just busy."
    author: "T. J. Brumfield"
  - subject: "Re: Thanx"
    date: 2007-11-15
    body: "I thought the reason was obvious: the Digest is weekly publication which covers KDE commits in a set time period of one week (actually between Saturday and Saturday). For that reason, the Digest can never be more than a week \"late\".\n\nAs for releasing on Wednesday, circumstances change, and I can no longer be as rigid with my release schedule as I once was. Also, i'm sure you want a good read, so if the content for the introduction section is not ready, i'll wait for that.\n\nBut i'm still committed to producing the Digest, and I haven't missed a week out of the last 84...\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Thanx"
    date: 2007-11-15
    body: "So do you need at least some help from the community?\nwhich part of producing the Digest takes more time? tell us, maybe we could help?"
    author: "Emil Sedgh"
  - subject: "Re: Thanx"
    date: 2007-11-15
    body: "Thanks for so much effort and so many nice reading hours. You keep us (simple mortals) up to date every week. For me, each week is like when-I-was-a-child Christmas eve, -and you like Santa-. Thank you for making me feeling that way. Take you time, but -please- continue the good work."
    author: "Juan M. Caravaca"
  - subject: "Re: Thanx"
    date: 2007-11-15
    body: "I have to agree with that.\nI remeber my first commit digest beeing far less detailed as the ones we get today (I may have a bad memroy thought ;)\n\nBut if I have to wait 3 days more to get these extra, I'm more than willing to wait ( he, nice to see Kstars used in real life, or explanations about Raptor...)\n\nIt sometimes feel the commit digest is more than just the digest, but a couple of What's new in SVN and a What's new in the Komunity. And that's great."
    author: "kollum"
  - subject: "Give Kickoff a chance!"
    date: 2007-11-14
    body: "So, I've tried the new KDE4 beta, and I had the opportunity to use Kickoff for the first time.  First impression: it looks pretty.  But how does it fare when you actually try to use it?  Well, I think that many of its critics have been a bit unfair.  It's quite obvious that Kickoff is still a beta product, and it still has quite a few rough edges to soften.  Nevertheless, people seem to pounce on it and tear it to pieces as if it were a finished product -- give it a chance for goodness sake!\n\nOne example: the navigation between the tree of applications is still very awkward and confusing.  But don't you think the Kickoff guys know about it?  Give them some time, and I'm sure they'll improve it to the point where it is usable.  Also, I like the idea of the \"clickless\" switching of tabs.  At the moment it still causes a few problems -- let your mouse wander a bit too much and the tab switches unexpectedly -- but with some tweaks it can become really cool and usable.\n\nMy conclusion?  I think that Kickoff has quite a number of neat ideas and is showing a lot of promise.  My advice to its developers would be to get some of the usability people involved, because I'm sure they can give you lots of good advice (for instance, the navigation through the application tree seriously needs to be revisited;  I am sure those usability folks will have a heart-attack when they try that one...).\n\nAnyway, keep up the good work!\n"
    author: "Anonymous cow"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-14
    body: "Kickoff has been in openSUSE KDE 3 since 10.2 as default and it has the same problems. It didn't change at all between 10.2 and 10.3 so it seems that developers (at least at openSUSE) think it's perfect. This menu in KDE 4 is a full rewrite but looks and works almost exactly the same."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-14
    body: "Interesting. I didn't know that.  But it doesn't invalidate what I just said: Kickoff has a lot of neat ideas and potential, but it's quite obvious that it needs the eye of someone with a background in usability.  It's very tempting to implement all sorts of new ideas (such as Kickoff's method of navigating through the application tree) just because they are cool; however, you also need someone to tell you \"slow down; that may look cool but it's unusable by your target audience\".\n"
    author: "Anonymous cow"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-14
    body: "They did a lot of usability testing and they found that it was good. (More info: http://en.opensuse.org/Kickoff .), while many people (including me) find it unusable. The difference might be that in their test they used Kickoff for everything, including things that were not possible with the traditional menu, while I do these things with other tools and mainly use the menu for application browsing. I think the main problem with Kickoff (both the KDE 3 and KDE 4 versions) is that nothing can be configured: not even the default tab or whether there should be an animation, which is not usual in KDE."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-14
    body: "there will always be people that don't like something, you can't please them all. Personally I love kickoff and have no problems with it.\n\nThe only thing I didn't like about it in KDE4, and it is a beta so will change, was that I couldn't add things from the kickoff to the desktop.  But that will change, it's just a beta."
    author: "Richard"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I did not say that it is a problem that some people don't like Kickoff, as long as there is an alternative."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I think I read that someone is reprogramming the old KDE3 menu as a plasmoid."
    author: "Anonymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I do think that's because Plasma has no proper support for \"desktop icons\" plasmoids yet."
    author: "Anonymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "kickoff is quite ok (and i say that as an user of many console apps).\ni am using it on opensuse installs i have, but there are some problems with it, obviously.\n\nmost of those could easily be solved by adding more kde-style configuration (this is where ui config nazis clash on each other :) ).\n\n1. already mentioned animations - there should be a way to disable those (on some setups applications meny looks veeeery bad when animated).\n\n2. in suse-styled old kde menu there was a filter field which would disable meanus and entries not matching a filter - this allowed an easy way to find where the shortcut is located in the menu. in some cases, this is not shown in kickoff.\n\n3. tabs switch on mouseover, not click. this probably is the only thing i really hate about kickoff. if i could make them switch on click, that would make me much, mcu happier.\n\ni also understand that some usability study might suggest switch on mouseover is better for some groups - which i can happily accept. but this has gotten in my way too many times and has really pissed me off, thus i nominate this as the biggest headache.\nfirst two items are minor annoyances, third is ANNOYANCE ;)"
    author: "Richlv"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-14
    body: "> and mainly use the menu for application browsing.\n\nHow often do you browse through your 'all application' list? Are you really saying that you very regularly use an application that is not in the 'Favourites' tab? If so, why isn't it?\n\n> or whether there should be an animation\n\nWhat animation? The green geeko? Of course it is configurable. "
    author: "apokryphos"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I use other methods to start regularly used applications: I put icons on the kicker or on the desktop. Additionally, I have the Desktop directory as a menu on the kicker. For what I need a main menu is browsing all applications."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I agree. For starting my favorites I use Katapult (I hope that will still be in place in KDE4.0). If I need an application which I do not know by name or which is not found by Katapult, because it is newly installed, then I use Kmenu. I tried Kickoff the few months I had opensuse installed and also had a look on it using current beta-liveCDs. Application-browsing is rather a pain, the menu is not configurable. I really hope that KDE4.0 will also have Kmenu and Katapult."
    author: "Mark"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "The animation when I switch to a submenu."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "http://www.kdedevelopers.org/node/2620 and http://roderick-greening.blogspot.com/2007/02/kcontrol-updates-for-kickoff.html may provide a hint how to configure it. Through I don't know by myself about the state of a GUI to change such options.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "You think I complain about it?\n\nYou should hear my wife scream.  She thinks it is the worst thing ever invented.\n\nI can't understand how something so painfully slow is good for usability.  This has never once been explained.  This particular menu is so bad, it has me completely sworn off menus, and just using Alt+F2 all the time."
    author: "T. J. Brumfield"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "\"They did a lot of usability testing and they found that it was good.\"\n\nThe problem with this \"usability testing\" done by the developers themselves it's that it's not testing usability at all!  If I developed something, of course that I would think it was great and intuitive and all that.  But usability testing must be done with REAL users, meaning people who didn't develop the thing!\n\nI bet that if INDEPENDENT usability experts would take a look at Kickoff they would immediately come to the same conclusions as many posters in this thread: a) that the typical KDE user would find the application menu too slow to interact with; and b) though you think you might be helping users by hiding the tree structure, in fact you're only making people feel lost.\n\nNow, I have to insist on this usability thing because it's a common pattern that I see in many KDE apps: the developers themselves assume that if something feels intuitive to them it will feel the same to everyone else.\n\nI really like some of the ideas behind Kickoff, and I would love to see this project thrive.  I would really hate to see Kickoff fail simply because their menu structure is unusable and the developers were to proud to reconsider it.\n\nSo, Kickoff guys: please get some usability people involved ASAP!  We love your ideas, but you must also listen to REAL users.\n"
    author: "Anonymous cow"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "RTFA\n\"Central to this research is a usability study where about 30 users of different types...\""
    author: "Ian Monroe"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "But redding is teh hard!!11 I'd rather spout irrelevant drivel on the dot!\n\nLulz."
    author: "Erunno"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "It's not really relevant if he read it or not. As explained before by some other people: it tries to solve problems in the menu field that should'nt be in a menu. It might be the most optimised menu-solution to create the fastest way to do these things, when they all need to be done using a menu... which they don't: favorites in a menu? come on, one simple \"favorites\" plasma on the bar that behaves like the \"favorites\" list in kickoff would be a lot faster (you don't even need to press the kickoff menu... what a speed improvement). Put too manu things in a menu and you'll get some slow action.\nReplacing problems to another area and then screaming: we've found the best solution to these problems in this area without even considering that moving the thing was wrong to start with is not quite a good study."
    author: "Terracotta"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: ">>But redding is teh hard!!11 I'd rather spout irrelevant drivel on the dot!\n\nIrrelevant drivel ??\n\nFollowing your 'logic', people would have to read this 'study' in order to submit to their very basic ongoing frustrations.\n\nIt seems valid points are pointed out in this thread. Irrelevant drivel ??\n"
    author: "ac"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "do you consider this number of guinea pigs to be sufficient for such testing?\n\nif someone is doing public opinions poll within our small 10 million country, mathematicians laugh at him if he has less than thousands of respondents (carefully selected to cover all social groups)\n\n30? - it is far less than how many professions are listed in our bureaucratic forms ... could you imagine that one menu approach would fit the same to CNC programmer as to old-school secretary still missing her purely mechanical typewriter? how many other contrasting examples you can think about?\n\n... not talking about the fact, that the study does not consider what we all need the application menu for - starting an application (no, I really do not want to find my documents via \"start\" menu etc.)"
    author: "kavol"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I just don't believe the study.\n\nIt hides options, it forces you to click 5 times to get to one thing, and it is the slowest menu I've ever used.\n\nHow is that good for usability?  No one has ever answered that.\n\nIf it makes me CONSIDERABLY less productive, how is that good for usability?"
    author: "T. J. Brumfield"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Sorry, but using 30 random people hardly constitutes a proper usability test.  Besides, using \"different types\" is a fine way of trying to please everyone and ending up being hated unanimously.  And how many of those 30 were actually typical KDE users?\n\nI still maintain that Kickoff was never subjected to a proper usability test with members of its target audience.  That means typical KDE users, who on average are more technically minded than your regular folk.  Once someone picks, say, at least 30 such users and tests Kickoff with them, then you can claim that Kickoff has been properly tested.\n"
    author: "Anonymous cow"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Why should the target audience be current KDE users? And to your \"how many?\" question, how about actually reading the study/presententations done about it?"
    author: "Anonymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "> Why should the target audience be current KDE users?\n\nsince he considers a bad_thing(tm) betraying large user base of \"unix lovers\" just to attract a few Windows deserters who did not end up with Gnome for some obscure reason?\n\n> And to your \"how many?\" question, how about actually reading\n> the study/presententations done about it?\n\nwell, how about actually reading the question? - it is NOT answered within the study"
    author: "kavol"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "> it is NOT answered within the study\n\nSure it is, eg Akademy 2006 slide #20."
    author: "Anonymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-18
    body: "> Once someone picks, say, at least 30 [technical] users and tests Kickoff\n> with them, then you can claim that Kickoff has been properly tested.\n\nSo, you don't disagree the 30 is too low and you don't disagree that the test being done was bad.  Just that the people the test was run on doesn't fit what you think the user audience is.\n\nWell, I think thats a good thing then :)\n\nI mean; I actually do think the user audience being a random set of computer-users is perfectly ok.  If you don't think so, then we disagree on that.  But I'm very happy we agree that the test for the intended audience was good.\n\nCheers!"
    author: "Thomas Zander"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "> The problem with this \"usability testing\" done by the developers\n\nWho says that the usability was done by developers or with developers? Clear, get your facts straight."
    author: "Anonymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Where does one get the source for KickOff?  I use Gentoo:\n\n$ qsearch kickoff\n$\n\nThere's nothing in portage for it (unless it's called something else).  The page you link to doesn't have a link to the sources.  A quick check on Source Forge (just for grins) shows a Soccer Game and some Windows Logon Script something-or-other.\n\nAll I can find is an SVN link, but I honestly don't see anything list there for kickoff.  Am I just missing something, or is SuSE/Novel holing on to this one for themselves?\n\n\n\n"
    author: "Xanadu"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Learn to use Google before raising accusations because your distro being lame?"
    author: "Anonymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "learn to think a little more before calling a distro lame. did he offend yours? btw it used to be in the xeffects or sabayon overlays (i think). you might have to enable some USE flag for it to compile. i'm not sure though as I only use KDE4 live ebuilds at the moment..."
    author: "GD"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "oh yeah, just because the maintainers do not find your favourite toy so useful to be worth wasting their time by including it within the main package tree, you call the whole distro lame? - great flames for Friday morning ;-)"
    author: "kavol"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-24
    body: "it is included as patch for kicker in xeffects overlay\n\nadd xeffects overlay and emerge kicker with kickoff use flag"
    author: "evo"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "I just looked at the study, as far as it is presented at http://en.opensuse.org/Kickoff/\n\nThe sampling used is quite questionable if one wants to find out wether kickoff is more efficient than the classik kde menu.\n\nThey used 30 participants and divided those into 3 groups of 10 each: One using kickoff, one using the classic kde menu and one using the windows vista beta2 menu. According to SuSE, the groups consisted of the follwing (they considered some more factors that I omit here, I also omit the windows testing group):\n\nkde classic:\nby OS/Desktop:\nWindows: 7\nLinux/Gnome: 0\nLinux/KDE: 3\n\nby skills:\nbeginner: 9\nadvanced: 1\nexpert: 0\n\n\nkickoff:\nby OS/Desktop:\nWindows: 3\nLinux/Gnome: 4\nLinux/KDE: 3\n\nby skills:\nbeginner: 1\nadvanced: 6\nexpert: 3\n\nThen they compared those groups solving 13 tasks (of which I would not solve more than half by using the start menu at all) solely by using the menu. Basically they compared 9 beginners and 1 advanced user (kde classic) to 1 beginner, 6 advanced and 3 experts (kickoff group). Is anyone really surprised that kickoff seems more efficient when comparing those two that way? Not to mention that they used 7 linux users in the kickoff group, while they used only three linux users in the kde classic group. At least both groups included 3 kde users. :-)\n\nBottom line: while this study most likely had some usful results (eg. by watching the participants and their reactions), it is hardly suitable to compare kickoff to the classic kde menu, at least in terms of efficiency. Whatever the intention of this study was, it most certainly was something different."
    author: "smorg"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-14
    body: "Two misconceptions:\n(i) Kickoff did get some changes between 10.2 and 10.3 (like tab location), a few extra non-gui options\n(ii) the one in KDE 4 _is_ beta, everything has not been implemented into it yet.\n\n..and yes, it's been in openSUSE for some time, it's proven itself, and openSUSE users have been _very_ positive about it. "
    author: "apokryphos"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "\"openSUSE users have been _very_ positive about it\"\n\nAt least some of them. I am an openSUSE user and many other users do not like it after trying it, just see the comments on the dot."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "And then there seem to be the type of users who liked it and missed it on their distribution so that they made packages for their distribution (kde-apps.org shows Slackware, Fedora, Kubuntu and Crux). And last Mandriva has it in their distro package as option."
    author: "Anonymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Mandriva even iclude it by default, it's just a right click on the Kmenu, and choose the 'switch to kickoff menu' even from the liveCD.\n\nBut thanks for keeping old plain Kmenu the default :)"
    author: "kollum"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "\"Kickoff did get some changes between 10.2 and 10.3\"\n\nSorry, I just tried Kickoff in both versions, I didn't notice the change. Anyway, the application browser did not improve.\n\nWhat non-gui options did kickoff get in 10.3?"
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "See the comment of Sebastian, it has the links."
    author: "Anonymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Hate to be the one bringing this up, as it has been discussed over and over. Beta state should mean everything has been implemented and it's time to test, bug fix and optimize. "
    author: "MOP"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Yes , and many have probably switch back to\nthe old menu without make some noise ;-)\n\nat least my daughter , who is not a computer geek,\nscreams if put the new menu active.\n \n"
    author: "LucW"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-14
    body: "Yes, kickoff seems to be OK.  I haven't conducted a usability study so I really can't say if it is better than the large tree structure of KMenu in KDE3.\n\nOne thing which I really like is that it has room for larger type."
    author: "JRT"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-14
    body: "I wouldn't say that Kickoff is pretty but it's very usable, it's nicely structured and the built-in search function is just great.\nKickoff needs a little polishing and a few eye-candies and then it would be perfect for me and my wife :) I prefer it to every start menu that Windows has and even KBFX.\n\nPeople bash Kickoff unfairly like you pointed out and give Raptor hig ratings (I can't say anythig about it because I never used it) but I like a few  others, think that it would be a good idea if the Raptor crew would work together with the Kickoff guys to making ONE GOOD START MENU for KDE. We don't need a thousand! All we need is one good one that everyone can adjust and configure to fit his/her needs and taste. The Open Source people need to learn to work together more instead of scattering their talents all over the place. Together we are strong!"
    author: "Bobby"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I disagree strongly. \n\nFirst, an advantage of open source choice. There are a bunch of different solutions to choose from, free to try, and modifiable if I don't find something that suits me.\n\nSecond, Kickoff is essentially useless for me. I've tried it, and it gets in my way for what I use application launchers for. Although, I don't discount its use for others.\n\nThird, from what I can tell, raptor should fit me well. If the Raptor crew junked the project to work on KickOff, I'd be screwed, since I don't have the creativity to come up with something like raptor for myself.\n\nFinally, there are the cliches like \"too many chefs to a pot\". If everyone worked on one thing, it would be disorganized, and never get done, since everyone would have different ideas for it."
    author: "Soap"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "You are right about choice but sometimes I think that too many projects at the same time for the same thing without having one very good product isn't really good. \nLook at the Linux Kernel for example, it's only one Kernel that thousands of developers work on and it gets better and better everyday. I don't know if that would be possible with 100 Linux Kernels. Yes there are many distros which makes choice possible but in some cases also causes confusion for newbies.\n\nNow back to the start menu. Like i said, I can't say much about Raptor apart from what i read and the fact that it looks real cool (screen shots). It sounds like your have tried it the way you wrote. I would like to do the same in order to make a fair comparison. How can I install and use it in KDE4 (present beta)? I would appreciate a few tips. Thanks."
    author: "Bobby"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "\"One example: the navigation between the tree of applications is still very awkward and confusing. But don't you think the Kickoff guys know about it?\"\n\nNot everyone can simply assume this when looking at how long it has been like this in the openSUSE environment.\n\nIn the current vanilla kde 3 environment, when one clicks on the K, one can then simply look/hover around the application tree. So far I have not seen this very, very simple and effective mechanism replicated in any new start menu, which I believe makes some people somewhat nervous as in overlooking something relatively obvious. Note that this only about navigating that tree, not necessarily how you get to that tree in the first place. You can't blame people when, seemingly, already Kickoff has been developed in a vacuum of perceived perfection, all the while having this relatively obvious problem, at least to me.\n\nWell yes, I guess it will be solved, I guess, but how such a design survived for so long in the first place, is a bit of a mystery to me."
    author: "ac"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "> You can't blame people when, seemingly, already Kickoff \n> has been developed in a vacuum of perceived perfection\n\nThat is not the case.  The reason why Kickoff (in KDE 4) is essentially identical to the one in KDE 3 (at present) is very simple - it is just down to a lack of resources and time.\n\nThe original design was the result of an iterative process of refinement done by Novell engineers with the aid of a usability lab.  When I wrote the initial code for the KDE 4 version, the only tool I had was a laptop.  I didn't have any means to test whether major user interface changes cause usability 'breakage', other than basic heuristics.  Therefore I copied the KDE 3 design verbatim - aside from a few aesthetic changes and fixes for problems that could not be solved easily in KDE/Qt 3 for whatever reason.\nI personally do not encounter the problems described when using the Application tab, so without actually seeing video footage of a poor suffering user, it isn't obvious what is not working.\n\nAlthough I am no longer involved with it, discussion about Kickoff happens on panel-devel and kde-usability for those who wish to participate.\n\n"
    author: "Robert Knight"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Thanks Robert for having invested your time into this earlier, that's the least I should offer.\n\nI guess there is a good case for the way the new app. tree is to be browsed and 'my' way must have it's flaws too. The reason I fall over this is, for me it is related to speed and oversight. When I started kde4 beta with the kickoff menu, I found myself aiming left and right only to navigate the application tree. I lost speed and oversight in relation to the app. tree. The result was I could not quickly scan the available applications or get to them and ended having to iterate and click trough each category.\n\nReading bellow comments, I see there must be cases where this new approach actually solves problems, obviously it would solve problems in some cases. But still, I find the 'old' approach where you can simply with 1 single click quickly hover over each category and sub category and quickly get to where you want without having to aim for extra mouse clicks left and right to browse the tree, much easier and faster.\n\nIf I am a minority with this view, I suspect I am missing something, but I just find for myself the one click quick hover approach to be faster and less confusing. I don't have a video of myself or others struggling with this, but all I know is that I was struggling to get trough the application tree in that kde4 beta kickoff menu. All I thought was that it was supposed to be more easy, all I observed was a bit of frustration with it on my part.\n\nIf I have time, I will see if my experience here is shared by any others and do so on panel-devel and kde-usability as you suggested."
    author: "ac"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I totally agree with you.  This is exactly the problem I have with Kickoff's application browser.  While I applaud their effort to try something new in this regard, I also think they should have the courage to admit that the experiment failed.  Trying to hide the application tree does not make life easier for users   -- quite on the contrary: it makes them a) feel lost among the categories, and b) make access to applications so slow as to be frustrating.\n\nMoreover, I'm a bit suspicious of these so-called usability tests they supposedly performed.  I found a few guinea pigs around the office and gave them Kickoff to try out.  While they like the general look and feel and love having quick access to favourites and whatnot, everyone found the application browser awfully confusing.  Granted, these 3 people were all Windows XP users, so they might be more used to the more traditional tree-like approach, but still, I really would like the meet a person who thinks that hiding a tree structure makes life easier.\n"
    author: "Anonymous cow"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "\"I really would like the meet a person who thinks that hiding a tree structure makes life easier.\"\n\nThat seems to be the prevailing thought among people working on file browsers.  A tree view squeeked into Dolphin, but is surely off by default.  Hardly a surprise that the same thinking is now being applied to the application^H^H^Heverything menu."
    author: "MamiyaOtaru"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Sorry dude, but Dolphin does NOT try to hide the fact that you're navigating through a tree structure.  The \"bread crumbs\" are very visible.\n\nIn any case, you can hardly compare the file system tree to the application tree.  The former is orders of magnitude larger.  When dealing with trees of that size you need different metaphors.\n\nI still maintain that it's a mistake to hide the arboreal nature of the application tree.\n"
    author: "Anonymous cow"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: ">>If I have time, I will see if my experience here is shared by any others ...\n\nWhy?  The comments here and in previous dot articles should tell you straight away that your experience is shared by others.  Here's my best \"usability\" anecdote:\n\nI'm an IT Tech by trade, so my 8y/o son naturally gets lots of computer exposure, and is pretty savvy at this point.  When I first installed OpenSuse with Kickoff, I had to explain to him how to find the various programs that he uses.  Even after I showed him, he told me that it was like trying to \"find his programs in a maze.\"  I immediately switched back to the K menu (thanks for leaving it as an option, guys).  I gave it some thought later, and decided that his maze analogy was right on - try this path, no, backtrack, try that path, repeat.  It seems to me that Novell were trying to solve a problem that didn't exist.  Getting to my programs should be natural and unobtrusive.  K menu did that; Kickoff misses the mark for me and my 8y/o."
    author: "Louis"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "> If I am a minority with this view ...\n\nmaybe WE are minority, but it seems that we are being taken seriously ... once again, I have to advertise this wishreport:\nhttp://bugs.kde.org/show_bug.cgi?id=150883\n\n- unfortunately, I am struggling with getting KDE compiled from svn so I did not have a chance to try Robert's menu implementation yet :-/"
    author: "kavol"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-16
    body: "Thanks, I added my votes."
    author: "ac"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Whats wrong with the application browser?  a tree based menu is extremely annoying when using a touchpad: if you move it too far in the vertical direction, it switches to another part of the tree.  I've been using the kubuntu port of open suse's kickoff (kde 3) and I like it; the only problems I have with it is that its difficult to select a search option without moving across the tab bar (which of course navigates away from the search results); and that the search is very slow.  Otherwise it is a very nice product... \n\nas for raptor, I'm not sure where it's trying to go.  Lancelot, however, looks very very nice, so I'm keeping an eye on him."
    author: "Level 1"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "About the touchpad thingy, the more accurate solution is to use your arrow keys and you won't have problems anymore with the mouse pointer going too far :)."
    author: "Anne Onymous"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "I see your point, it is valid. Still, I think this only describes the problem of switching to another, unwanted part of the tree when your aim is off. However, it does not invalidate the approach itself - it only describes a problem when an aim is off. I think, if possible, it would be nice if the new kde application tree menu could offer the 'old' navigation mode as well. I find in this case the either or approach a bit too harsh, seeing also as how the 'old', or current approach is more then a decade old and works just fine for some people (while dealing with the moments the aim is off). Again, perhaps an either or approach is a bit too harsh.\n\nAt the same time I am aware how young these menu's are and am aware that over time what I find works much faster, to become dated - but it would have to earn that, prestige ;)"
    author: "ac"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "One improvement I think useful is to allow user to configure the order of tab. For instance, I'd like to put 'all aplications' as the first tab since I've almost never use favourites (and I even disable this feature in the classic KMenu)."
    author: "a"
  - subject: "Re: Give Kickoff a chance!"
    date: 2007-11-15
    body: "Nice sentence: I never use my favorites (==most used applications) :-)\n(but I know what you mean)"
    author: "Bar"
  - subject: "Digikam"
    date: 2007-11-14
    body: "Here is my use case for Digikam:\n\n1. Set up repositories on my laptop and on an external drive/network drive.\n2. As the laptop drive fills up, I move pictures from the laptop repository to the external repository, not necessarily from inside Digikam.\n3. Digikam does not delete the database information about the moved files, but notices that they are missing. Optionally it shows them as \"ghosts\" in the interface.\n4. When I plug in the drive/connect to the network, Digikam finds the moved pictures and associates them with the already present database information.\n5. Since Digikam could not know in step 3. whether the pictures were moved or deleted, there is a function that allows me to confirm expunging of pictures that were truly removed, to get rid of the ghosts.\n\nI suppose this use is not supported yet, but with the new feature we got a lot closer!\n\n(Identifying moved picture can be reliably done with checksums, but would primarily be done by looking at file sizes/names/dates and/or by a hidden file in each directory. I'm sure there is some synergy with Strigi and Nepomuk here?)"
    author: "Martin"
  - subject: "Re: Digikam"
    date: 2007-11-14
    body: "As far as I understand with those new features this scenario is working with exception of point 2. Such migrations should be done from managing program."
    author: "m."
  - subject: "Re: Digikam"
    date: 2007-11-15
    body: "I strongly disagree. Just because I sometimes use Digikam I should be chained to using only that program for my file management, or things break. There are several good reasons to want to be able to use e.g. Dolphin or the command line to move things around.\n\nThe first commandment only applies to Emacs after all. Not to other software. http://www.emacswiki.org/cgi-bin/wiki/faith.el"
    author: "Martin"
  - subject: "Re: Digikam"
    date: 2007-11-15
    body: "Amarok has always worked that if you move a file off and then back the statistics are kept. It's easy to do, you just don't bother cleaning the statistics table. :) Of course you don't want to show stuff that isn't there, and Amarok doesn't.\n\nNow we have more sophisticated stuff, it takes a hash of the first 4k of the file + the metadata. It works pretty well. "
    author: "Ian Monroe"
  - subject: "Re: Digikam"
    date: 2007-11-15
    body: "A similar feature that would be neat (and unfortunately quite complex i fear):\nsupport for creating albums on cds and dvds and then if that files are deleted from the hard disk, digikam keeps the info and the thumbnails in the db and the album is still available from the main view.\nwhen the user tries to open a photo from that album a message appears with something like \"please insert the disc labaled foo\""
    author: "Mart"
  - subject: "Re: Digikam"
    date: 2007-11-16
    body: "This is already in the bug tracker as a feature request.  Go vote for it!  :)"
    author: "A Debian User"
  - subject: "Thanks danny"
    date: 2007-11-14
    body: "Nice things to read. Nice work.\n\nI don't know what I would miss more if they didn't exist :\nthe \"much-wanted feature of multiple album root paths in Digikam\"(yay, finaly, that's great) or\nthe commit digest every week"
    author: "kollum"
  - subject: "Multiple root paths?"
    date: 2007-11-21
    body: "I use Digikam too... but, what does that actually mean, 'multiple root paths' ??"
    author: "Darkelve"
  - subject: "Thank you very much!"
    date: 2007-11-14
    body: "Can't live without commit-digest..."
    author: "Fuffo"
  - subject: "Right"
    date: 2007-11-15
    body: "That's the reason I secretly hope that KDE will be delayed for more than a year. Than I could read 50 more digests for sure..."
    author: "mathletic"
  - subject: "Re: Right"
    date: 2007-11-15
    body: "Have no fear, I think we can safely say that commit digests containing drool-inducing feature commits will continue after the 4.0 release :)"
    author: "Paul Eggleton"
  - subject: "Ligature"
    date: 2007-11-14
    body: "Has Ligature definitely been abandoned now? Could someone tell the story of what happened to it? Is our good friend Wilfried Huss, who kept us up to date in the past, still lurking on the Dot?\n\n(FYI: Ligature is/was the renamed KViewShell, an app that on the surface of it looks/looked almost identical to Okular, but has/had different internals. I don't know exactly what was unreconcilably different, since also the low-level libraries, such as Poppler for PDF, were shared, but supposedly Ligature is/was more tailored for DVI files. Its developers also claimed it to be faster.)"
    author: "Martin"
  - subject: "Konqueror suggestions"
    date: 2007-11-14
    body: "I'm not going to make any suggestions here.\n\nReading between the flames on the KDE-dev mailing list, I find that large changes are planed for Konqueror which I am not sure if users will like.\n\nSo, a simple question, where is the proper place for users to post their suggestions on how to improve Konqueror?"
    author: "JRT"
  - subject: "Re: Konqueror suggestions"
    date: 2007-11-15
    body: "Link(s) to discussion in the list archive? Thanks."
    author: "Martin"
  - subject: "Re: Konqueror suggestions"
    date: 2007-11-15
    body: "I believe there is a kfm-devel mailing list."
    author: "T. J. Brumfield"
  - subject: "Re: Konqueror suggestions"
    date: 2007-11-15
    body: "Not really intended for user input.  We shouldn't clog up a development list with a lot of user traffic.\n\nShould there be a non 'devel' kfm (or Konqueror) list if a list is the place for this?\n\nI specifically ask because there is a lot of discussion about Konqueror 3.x.y in BugZilla that really belongs in a suggestion box forum.  Bugs are intended to be about what is wrong, not long discussions about how to change something."
    author: "JRT"
  - subject: "Amarok Playlist should be bigger and in the middle"
    date: 2007-11-15
    body: "It seems that the Amarok playlist is getting bigger and bigger (as it should) -- now it's about a third of the window. Now let's move in to the middle!\n\nWhat's the point of having an empty blue screen in the middle, if this scrunches up the majority of the titles in the playlist and forces them to be hidden behind an ellipses symbol (...)? Let the user see what song is actually playing, and give the playlist the focus that it deserves ;)\n\nOverall, I like the fact that the various widgets are more compact, resulting in less wasted screen real-estate. I hope this trend continues."
    author: "Scott"
  - subject: "Re: Amarok Playlist should be bigger and in the middle"
    date: 2007-11-15
    body: "I think you are right. But let's give the devs some time to come up with really cool context widgets, to make good use of that empty blue space. If they fail to produce something that motivates its central position, it can always be moved back to the side later!"
    author: "Martin"
  - subject: "Re: Amarok Playlist should be bigger and in the middle"
    date: 2007-11-15
    body: "The center is a big blue Amarok logo obviously because the dev's need some way to promote their product."
    author: "Dan"
  - subject: "Re: Amarok Playlist should be bigger and in the mi"
    date: 2007-11-15
    body: "No, that big blue space is integrated with plasma, so the dev's can add cool plasmoids in there. Honestly I am not convinced that it will be as good as it sounds, because i haven't seen any use of this feature yet, but it has potential.\n\nMore on-topic, great digest Danny! I expect it every week =]"
    author: "Firmo"
  - subject: "Re: Amarok Playlist should be bigger and in the mi"
    date: 2007-11-15
    body: "Since Amarok is the only non-plasma user of libplasma, sometimes (often) libplasma breaks things for Amarok. It will all be sorted out in the end. But there are in fact several plamoids already developed, they just aren't always working."
    author: "Ian Monroe"
  - subject: "Re: Amarok Playlist should be bigger and in the mi"
    date: 2007-11-15
    body: "the only non-plasma user of libplasma outside of kdebase/workspace =)"
    author: "Aaron J. Seigo"
  - subject: "Selection in Amarok's tree view, compact scrollbar"
    date: 2007-11-15
    body: "I think only the letters in \"Interstate Medicine\" should be highlighted grey in Amarok's treeview, and not the space to the left (see attached screenshot). It's not supposed to highlight the vertical line representing the parent folder. This highlighting behavior in treeviews seems to be a problem throughout KDE 4, since Dolphin also extends the highlighting to the left.\n\nAlso, is there any way to eliminate the space between a scrollbar and the window it controls? (see red arrow on attached screenshot)."
    author: "Bob"
  - subject: "Re: Selection in Amarok's tree view, compact scrol"
    date: 2007-11-15
    body: "Yes I agree with you about highlighting.\n\nIt's too early to worry about pixels here and there, probably just some widget margins."
    author: "Ian Monroe"
  - subject: "Re: Selection in Amarok's tree view, compact scrollbar"
    date: 2007-11-15
    body: "Good observations."
    author: "T. J. Brumfield"
  - subject: "Print-protected PDFs"
    date: 2007-11-15
    body: "> KGhostView finally removed in favour of okular\n\nI use KPDF to read PDF files and I have no doubt that okular will be a great app but currently I need to use KGhostView in order to print print-protected PDF documents. Will okular allow me printing of print-protected PDF documents?"
    author: "Rub\u00e9n Moreno Montol\u00edu"
  - subject: "Re: Print-protected PDFs"
    date: 2007-11-15
    body: "Last I looked, there was a option in kpdf's configuration dialog to ignore the print-protected \"feature.\""
    author: "Ian Monroe"
  - subject: "Re: Print-protected PDFs"
    date: 2007-11-15
    body: "whoa! Thank you! Next time I will check more carefully the config dialog."
    author: "Rub\u00e9n Moreno Montol\u00edu"
  - subject: "Re: Print-protected PDFs"
    date: 2007-11-15
    body: "DRM in Okular is optional. When compiling Okular, set -DOKULAR_FORCE_DRM=OFF as a compiler parameter. Isn't OKULAR_FORCE_DRM set to OFF by default? This will allow you to have a GUI config where again you have to turn DRM off.\n\nThis is a pain - how about keeping the runtime config and scrapping the compile time one?"
    author: "Laurie"
  - subject: "Re: Print-protected PDFs"
    date: 2007-11-15
    body: "I totally agree. This should be a runtime configurable parameter or set to 'off' on compiling time by default."
    author: "Rub\u00e9n Moreno Montol\u00edu"
  - subject: "shadow bug"
    date: 2007-11-15
    body: "when you use compiste, the shadow in the desktop doesnt dissapers when you close kickoff, you need start a new window o aplicattion and the shadows go away, now im compiling from svn, but wanna now if is solved ???"
    author: "Rudolhp"
  - subject: "Amarok 2"
    date: 2007-11-15
    body: "I have to say when I first heard that Amarok 2.0 would be using covers in the playlist and compacting it I was more then a little scared. Being a huge fan of Amarok previously, I loved the playlist and didn't think that this would improve it all only make it more bloated. I was wrong, that video put into perpective how cool this will be. Thank you everyone for your hard work and innovation!"
    author: "CptnObvious999"
  - subject: "Re: Amarok 2"
    date: 2007-11-15
    body: "I really appreciate this comment. Despite what people sometimes seem to think when we are posting development screenshots ( Even though, I have to admit, it has not been that bad lately ), we are not out to destroy Amarok.:-) We are, more than anything else, all Amarok users ourselves.\n\nOn the other hand, we had also gotten to the point where 1.4.x, which, in its current incarnation, is a really great and solid music manager/player, can only be improved so much without compromising stability or simplicity. So we decided to \"go for broke\" and try something completely different, while keeping the Amarok \"Spirit\" intact. All this work is slowly starting to come together now, and I am more exited than ever about Amarok2.  \n\n"
    author: "Nikolaj Hald Nielsen"
  - subject: "Konqueror for 4.0"
    date: 2007-11-15
    body: "Aaron has an interesting post at (http://aseigo.blogspot.com/2007/11/oxygenation.html), where he shows the great work being done to oxygenify the beloved Big K. One thing that puzzles me however, is this: why was the search bar in the right hand side removed? I know you can do \"gg:\" for a Google search etc, but I thought the bar was a pleasant touch. Is there an option to re-enable it for those feeling sentimental? \n\nOh and thanks for your perseverance and durability in providing these great digests Danny & crew; your motivation and dedication to the cause is admirable!"
    author: "Darryl Wheatley"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "Both search bars: Web Search & Directory Filter, are totally configurable in Konqueror on KDE 3.5.8 -- you can add them to the Location ToolBar and remove them from it using: \"Settings -> Configure Toolbars\"  I would presume that this would also be the case in 4.0.0.  If they are missing, there are going to be a lot of disappointed users."
    author: "JRT"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "It's OK if they are configurable, but please make they appear in the default config. People always complain about the \"bad defaults\" in KDE. And if this is true (no google/wikipedia/youtube/etc bar), they are right this time.\nRemember that only a few will search in the configs dialogs to get the bar back, many will simply switch to Firefox"
    author: "Firmo"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "The search bar is the first thing I remove in any browser I use. I think the space is better used for the address bar, especially on lower resulution screens. Plus, with the \"gg:\" or \"g\" and other configurable keywords, I don't lose any functionality.\n\nOf course, you're probably right about most users."
    author: "Soap"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "ACK. That's actually my favourite personal complaint about \"bad defaults\" in KDE. I spend way too much time removing this IMO useless duplicate feature from konq's interface every time I get to a new KDE setup. (On the upside, I'm getting pretty good at removing it, these days :)\nI hope it won't get any harder to remove the search bar (or anything else in the toolbars) than it is now.\n\n"
    author: "stingraz"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "Agree, it's the first ting I remove too. I have never found Konquerors interface to be too cluttered, but that one is plain annoying."
    author: "Morty"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "I have be added to the list of the 'google and the likes' search bar.\n\nBut on the other side, the 'filter bar' is realy nice, and I can't find it on my mdv2008. I found it on a mandriva 2007 powerpack at scool, and it was a good surprise.\nI think this would have been a great default in filemanager mode. (I mean, it's the like of a 'ls|grep xxx', wich I used to open a konsole just to easily do that... F4 rox, but an integrated bar in konqui is so much easier...)"
    author: "kollum"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "wow, such a filter bar would be great!\n\nBut i dont agree with the searchbar. The searchbar has to be default, the user expects this, specialy new users. Advaced users can remove it with no problem, new users won't even know that it exists"
    author: "Beat Wolf"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "Little tip: you can actually use wildcards in the location bar in konq.\nSo type in *xxx* in there, and you see the stuff.\n\nYeah, not very discoverable :(\n\n"
    author: "SadEagle"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "Wow - I had no idea about this - that's great, thanks!"
    author: "Anon"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-16
    body: "I think the default interface should show most features of which the user can remove those they don't use. If there is only keyword search, the user will not even notice this feature, while they notice the search box with the Google icon."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-15
    body: "The searchbar has never been a part of konqueror, it's just a plugin.\nSo the screenshots with it \"removed\" simply do not have it installed."
    author: "SadEagle"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-16
    body: "That's a plugin that I can't live without, too.  Which reminds me, is Apple still making money off my Konq Google searches?  Also, how can KDE get a customized Google page like:\n\nhttp://www.google.com/firefox\nhttp://www.google.com/microsoft\nhttp://www.google.com/linux\n\nHas anybody checked into this?  Would be kool for Konq to have a unique Google page!"
    author: "Louis"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-16
    body: "Is/was Apple really making money off Konqueror Google seaches? That sounds bizarre. Any source on that?\n\nI do like your idea of a Konq Google page though."
    author: "Andr\u00e9"
  - subject: "Re: Konqueror for 4.0"
    date: 2007-11-16
    body: "I remember reading a while back that searches from Konq's Google toolbar were somehow identified by Google as coming from a Safari browser.  Therefore, searches that led to revenue-generating clicks were generating revenue for Apple.  I don't know the details, just that it got me a bit out of sorts back then.  Maybe somebody else here has details...?"
    author: "Louis"
  - subject: "Re: Konqueror for 4.0"
    date: 2008-07-14
    body: "AHHHH!"
    author: "blackbelt_jones"
  - subject: "Re: Konqueror for 4.0"
    date: 2008-07-14
    body: "The real significance of the filter bar was for cutting straight to the chase when trying to locate files.  It's completely essential to konqueror's function as a killer file manager.  I've been searching Konqueror for about ninety minutes now. The search bar appears to have been relocated.  It's just not possible that the search bar has been removed, not after all the assurances that Konqueror would not be tampered with.   "
    author: "blackbelt_jones"
  - subject: "Re: Konqueror for 4.0"
    date: 2008-07-14
    body: "Yeah, found the Konqueror plugins and got my filter bar.  TY so much!"
    author: "blackbelt_jones"
  - subject: "Re: Konqueror for 4.0"
    date: 2008-08-02
    body: "Hi, \nI just migrated to  4.1 from 3.5.9 and can't locate the filter bar :( Where did you find it ? \n\nIt seems i have the \"Directory Filter\" activated under Configuration/extensions, but it seems it can only filter by mime type, the input filter seems to have disappeared. Have you encountered the problem ? \n\nMany thanks, \nRomain."
    author: "Romain GUINOT"
  - subject: "Re: Konqueror for 4.0"
    date: 2008-08-04
    body: "Where did I find it?  In 3.5.9, I'm afraid.  If you want it in 4.x, it looks like you have to open ****ing Dolphin!  I just spent 40 minutes searching through all all   Came up on this page while the configure toolbar lists, and my hands are shaking from frustration.\n\nI think that this functionality will restored to Konqueror eventually.  I wish I knew for sure."
    author: "blackbelt_jones"
  - subject: "Re: Konqueror for 4.0"
    date: 2008-08-04
    body: "I finally got some satisfaction by using find to locate Konqueror 3.5.9 and putting the absolute path (/usr/bin/konqueror), in a link to the application in ~/Desktop, and then using folderview to put the link on the Desktop... and there it is, Konqueror, running in KDE 4.1!"
    author: "blackbelt_jones"
  - subject: "Re: Konqueror for 4.0"
    date: 2008-08-04
    body: "Where's the bug report? I'm not fully sure what feature it is that you want re-implemented, and a detailed bugreport would help."
    author: "Anon"
  - subject: "Re: Konqueror for 4.0"
    date: 2008-09-19
    body: "[quote]Hi, \n I just migrated to 4.1 from 3.5.9 and can't locate the filter bar :( Where did you find it ? [/quote]\n\nUh... in 3.5.9 :(\n\n"
    author: "blackbelt_jones"
  - subject: "KPPP"
    date: 2007-11-15
    body: "KPPP shouldnt be dead, its widely used in third-world countries who still use dialup...\nlast time i tried it wasnt working, is there anyone on it? what happened to its Solidifaction? in trunk, its still the kppp of kde3, but just not working..."
    author: "Emil Sedgh"
  - subject: "Re: KPPP"
    date: 2007-11-15
    body: "Well, if sth like Knetworkmanager can replace Kppp, I don't see why it should be ported. Kppp is needed for getting on the web through phone lines and people who are travelling a lot need this. So is there any replacement for it?"
    author: "Keba"
  - subject: "Re: KPPP"
    date: 2007-11-15
    body: "If Knetworkmanager or any already-ported application could do this there is no reason to fix kppp.but afaik, there is no application for Dialup connections except kppp for kde(4).so we need kppp...\n\nthe ideal thing is that kppp detects the modems automatically using solid and uses them, but its not changed from kde3 time, except that it doesnt works.gives errors when connecting, dialogs are messed up and...\nthis shouldnt be a hard job to fix the problems, kppp is ported already, just its buggy."
    author: "Emil Sedgh"
  - subject: "Re: KPPP"
    date: 2007-11-16
    body: "networkmanager a replacement for kppp?\n\nNetworkmanager barely works for simple configurations in my experience. Now to have to set up this monstrosity for dialup?\n\nDerek"
    author: "D Kite"
  - subject: "Kwin Composite"
    date: 2007-11-15
    body: "Is it possible to run Kwin Composite with ATI proprietary driver and XGL?\n\nI have tried a lot of things without success. \n\nAny help will be very appreciate :)  "
    author: "Luis"
  - subject: "Re: Kwin Composite"
    date: 2007-11-15
    body: "I'm not really an expert but my basic understanding so far is: The brand-new ATI proprietary driver (FGLRX) has built-in AIGLX. So you dont need XGL. XGL has proven extremely sluggish and unstable here but YMMV."
    author: "Michael"
  - subject: "Re: Kwin Composite"
    date: 2007-11-15
    body: "Yes, I know about it, but it has a terrible performance, no video playback, and some times frames appear and follow you mouse."
    author: "Luis"
  - subject: "Re: Kwin Composite"
    date: 2007-11-18
    body: "> and some times frames appear and follow you mouse\ntry inserting option \"XGL_NO_VISUAL_GLITCHES\" \"true\" into your xorg.conf ;)"
    author: "eMPee584"
  - subject: "Re: Kwin Composite"
    date: 2007-11-15
    body: "The word from X.org is that as of 7.3 that XGL isn't working yet.  The code doesn't even build.\n\nTherefore, consider using it experimental."
    author: "JRT"
  - subject: "Re: Kwin Composite"
    date: 2007-11-15
    body: "I use X.org 7.2, and XGL works well, I can run compiz, but, Kwin Composite refuses. "
    author: "Luis"
  - subject: "Re: Kwin Composite"
    date: 2007-11-15
    body: "And am I the only one who cannot do kwin opengl compositing with an Intel card and AIGLX (although Compiz works) ?"
    author: "Aldoo"
  - subject: "Re: Kwin Composite"
    date: 2007-11-15
    body: "I have it  working on a i965.  The key thing was to put\nexport LIBGL_ALWAYS_INDIRECT=1\nat the beginning of startkde.  I'm also using EXA, but I'm not sure if that makes any difference.\n"
    author: "mactalla"
  - subject: "k3b !"
    date: 2007-11-15
    body: "\"Sebastian Trueg committed changes in /trunk/KDE/kdebase/runtime: \n[...]next week K3b gets ported to KDE4![...]\"\n\nwonderful :)"
    author: "shamaz"
  - subject: "Re: k3b !"
    date: 2007-11-16
    body: "Yes, exciting!  Did anything ever come of the project to make a user-friendly wizard for K3B?  I remember reading some tidbits about it a while back, and then it disappeared.  I personally didn't feel it necessary, but it looked cool.  Anyone?"
    author: "Louis"
  - subject: "Kicker replacement"
    date: 2007-11-15
    body: "Months ago in the KDE-Look site someone posted a mockup of a kicker replacement idea (http://www.kde-look.org/content/show.php/Kde4+Mockup?content=28476). Does anybody know whether something in Plasma is moving toward that direction? I think the Create-Communicate-Configure thing and the task oriented menu is very original (no other OS's have it) and much more usable than the other kicker replacement idea I've seen. Will we ever see something like that?"
    author: "Manuele"
  - subject: "Re: Kicker replacement"
    date: 2007-11-15
    body: "\"Will we ever see something like that?\"\n\nDepends entirely on whether or not someone writes it ;) Plasma has the long-term goal of making the development and deployment of replacement panels, taskbars etc trivial, and something like this could probably be knocked in a few hours in Python or Ruby (which *hopefully* will be supported by Plasma, via Kross) or in slightly more time in Javascript, or slightly more time still in C++."
    author: "Anon"
  - subject: "serious security concerns"
    date: 2007-11-18
    body: "> Plasma has the long-term goal of making the development and deployment of\n> replacement panels, taskbars etc trivial, and something like this could\n> probably be knocked in a few hours\nYeah, and that's the problem. I hope someone takes the implicated security issues into concern: malware will be more easily developable and deployable with this, and that users could look at the source code to verify the authors intent doesn't improve on that in reality.\nIMHO it is very important that KDE accepts code only from kde community sites (<allowed>kde-look.org, kde-apps.org, kde.org </allowed>) and those put up a small verification/code review workflow for script snipplets like those for Plasma, else the ease of developing for KDE could seriously backfire, especially onto the new users that will pour in with the 4.0 release. regards."
    author: "eMPee584"
  - subject: "Re: serious security concerns"
    date: 2007-11-18
    body: "It's no more dangerous than Firefox extensions, to be honest (Firefox has a far, far larger installed base than KDE, and there is no epidemic of malware there), but I wouldn't be at all surprised to see whitelisting/ blacklisting/ signing implemented, if only because it would make a useful addition in a corporate environment."
    author: "Anon"
  - subject: "Re: Kicker replacement"
    date: 2007-11-21
    body: "I think it should be the default, not just an option. Having such a desktop metaphor would make KDE unique. It would not be Windows nor Macintosh nor Gnome, it would be KDE only. It could renovate the whole desktop thing. IMHO. :)"
    author: "Manuele"
  - subject: "Re: Kicker replacement"
    date: 2007-11-16
    body: "C'mon - you should have realized by now that plasma doesn't care about user opinions. They just want to develop a face-lifted piece of crap kde3 look-alike."
    author: "p0wer"
  - subject: "Re: Kicker replacement"
    date: 2007-11-16
    body: "And we have realized you are just trolling. Go away."
    author: "Luca Beltrame"
  - subject: "Re: Kicker replacement"
    date: 2007-11-16
    body: "Nuuh! Don't feed it! ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Kicker replacement"
    date: 2007-11-18
    body: "Agreed."
    author: "John G."
  - subject: "Re: Kicker replacement"
    date: 2007-11-21
    body: "It would be cool to know whether other paople liked that proposed solution? I personally loved it. But I do not know whether it's only me or others liked it too.\n\nI think the more we are in espressing our preference the more likely would be the KDE team makes it default :)\n\nAnyway, kudos to the KDE Team for providing us with such a great desktop! :)"
    author: "Manuele"
---
In <a href="http://commit-digest.org/issues/2007-11-11/">this week's KDE Commit-Digest</a>: Resurgent development work on <a href="http://www.kdevelop.org/">KDevelop</a> 4, with work on code parsing, code completion and the user interface. Support for converting the KVTML XML-based format to HTML in KDE-Edu. Support for the much-wanted feature of multiple album root paths in <a href="http://www.digikam.org/">Digikam</a>. Various continued developments in <a href="http://amarok.kde.org/">Amarok</a> 2. Multiple additional comic sources for the <a href="http://plasma.kde.org/">Plasma</a> Comic applet. Support for <a href="http://kopete.kde.org/">Kopete</a> plugins written in Python, Ruby, JavaScript and other supported languages through the <a href="http://kross.dipe.org/">Kross</a> scripting framework. A simple command-line application for playing media supported by <a href="http://phonon.kde.org/">Phonon</a>. WavPack, TrueAudio and Speex format support added to the TagLib support library used by JuK and Amarok. Audio device work (utilising <a href="http://solid.kde.org/">Solid</a>) in KMix. Work begins on KsCD by a team of French students. Various optimisations in Plasma and <a href="http://enzosworld.gmxhome.de/">Dolphin</a>, amongst other applications. <a href="http://okular.org/">okular</a> moves to a shared FreeDesktop.org library for PostScript format support. KGhostView finally removed in favour of okular for KDE 4.0. Code for supporting Apple OS X Dashboard applets via WebKit imported into playground (not for KDE 4.0!)

<!--break-->
