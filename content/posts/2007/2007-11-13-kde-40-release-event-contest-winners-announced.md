---
title: "KDE 4.0 Release Event Contest Winners Announced"
date:    2007-11-13
authors:
  - "wolson"
slug:    kde-40-release-event-contest-winners-announced
comments:
  - subject: "Congrats!"
    date: 2007-11-13
    body: "Congratulations, Aron and Kyle! Looking forward to meet you guys in Mountain View!"
    author: "sebas"
  - subject: "Re: Congrats!"
    date: 2007-11-13
    body: "Looking forward to meeting you too! Thanks!"
    author: "Aron Stansvik"
  - subject: "Re: Congrats!"
    date: 2007-11-13
    body: "I'm also looking forward to it!"
    author: "Kyle Cunningham"
  - subject: "Titles for KDE apps?"
    date: 2007-11-13
    body: "Excellent... and surely there are some KDE apps waiting to be named:\n\nKyle\n\nand\n\nstansviK\n\n???"
    author: "Peter Lewis"
  - subject: "Re: Titles for KDE apps?"
    date: 2007-11-13
    body: "I will neither confirm nor deny that we threw out any entries from contestants that didn't have K's in their name.  Or that Kyle had favoritism for having the name of one of the Gearheads.  Or that they offered the best bribes.\n\nIn all seriousness, thanks to all the participants and congratulations to the winners.\n"
    author: "Wade Olson"
  - subject: "Re: Titles for KDE apps?"
    date: 2007-11-13
    body: "Next time I'll call myself Klaudio. :)"
    author: "Taupter (Cl\u00e1udio Pinheiro - Kopete)"
  - subject: "Re: Titles for KDE apps?"
    date: 2007-11-13
    body: "Or, as the re-normified naming scheme goes, Cyle and Stansivik"
    author: "J Klassen"
  - subject: "Re: Titles for KDE apps?"
    date: 2007-11-13
    body: "There is a \"kile\" application, close enough for some."
    author: "David Johnson"
  - subject: "Re: Titles for KDE apps?"
    date: 2007-11-13
    body: "Yeah, it's the KDE Yntegrated Latex Environment.\n\nA prety good app, I have to say.\n\nCongrats to the winers, enjoy the meeting for all of the KDE comunity ( you'll need more than courage to drink the beer for all of the contest participants, but you may find new liver and stomach on the internet for cheap... KDE, the Komposited-Developer made Environement )"
    author: "kollum"
  - subject: "Thanks to donors"
    date: 2007-11-13
    body: "The thanks for being able to get the winners of our contest to California of course have to go to all the donors and supporting members who financially support the KDE e.V. You make the difference :-).\n"
    author: "Cornelius Schumacher"
  - subject: "How I would have run it..."
    date: 2007-11-14
    body: "If I were in charge of the contest, I'd pick two entrants from California, and saved me some dough.\n\nCongrats to both winners!"
    author: "T. J. Brumfield"
  - subject: "Re: How I would have run it..."
    date: 2007-11-14
    body: "It isn't your money, and you don't get to keep anything you save, so what would you have done with it?"
    author: "Hank Miller"
  - subject: "Oh wow"
    date: 2007-11-14
    body: "Hi Kyle! Hope you get your private/public file patch into Drupal 7 :D\n\nIt's really neat to see people somewhere where you hadn't expected them to be. Right? (Yeah, that goes out to you, MaxAuthority.)"
    author: "Jakob Petsovits"
  - subject: "Other applications"
    date: 2007-11-17
    body: "Doh, what kind of humorous submissions?"
    author: "Tuju"
---
On October 4, 2007 we <a href="http://dot.kde.org/1191506286/">announced a contest</a> regarding the KDE 4.0 Release Event  at Mountain View, California on January 17-19, 2008.  Participants were asked to answer the question: "Why should you be at the KDE 4.0 Release Event?" with the winner being flown out to the Release Event itself.



<!--break-->
<p>We received many great submissions from community members with very different backgrounds from around the globe.  Everyone captured the spirit of the contest with enthusiastic responses; some humorous and some serious, some brief and some reaching the submission limit on length.  In the end, with generous approval from KDE e.V. Vice President and Treasurer Cornelius Schumacher, we have decided to fly out two contestants:</p>

<p>Aron Stansvik: Aron is a small business owner that runs a book café in the town of Örebro, Sweden.  As with any new business, he found himself spread thin and scrambling to accomplish everything required of him.  A KDE user from the 2.0 days, what began as a useful tool for him became a passion of its own.  Aron represents entrepreneurial people that take chances and have free software spirit intertwined with other aspects of their lives.  He understands the reasons for and the efforts behind KDE 4.0, as well as the potential tipping point of KDE's momentum.  We'll get to discuss this further with Aron in just two months.</p>

<p>Kyle Cunningham: Kyle is a Computer Science and Computer Engineering student at the University of Minnesota.  Even though students have busy schedules, he manages to both contribute to Drupal (SoC student) and stay current with KDE development progress.  By working with multiple communities and showing his passion for software libre, Kyle embodies the spirit of community members and enthusiasts.  He understands all of the elements that comprise the KDE community and our motivations.  Kyle is hoping to meet some KDE community members for the first time and it looks like he's going to get that chance.</p>

<p>We would like to thank everyone for their entries and feel that we have chosen two individuals that capture the spirit of KDE and free software. Their entries were clear, concise and creative.  More importantly, they spoke from their hearts about their interests in KDE's future and how to become more involved.  Their responses are not published here, because we don't want to spoil the surprise:  Both will be speaking briefly at the Release Event and we'll have coverage for everyone.</p>

