---
title: "Final KDE e.V. Quarterly Report of 2006"
date:    2007-03-06
authors:
  - "jriddell"
slug:    final-kde-ev-quarterly-report-2006
comments:
  - subject: "@Jonathan: Typo, eplace \"Darmstradt\" with \"Darms "
    date: 2007-03-06
    body: "Typo: Replace \"Darmstradt\" with \"Darmstadt\""
    author: "Victor T."
  - subject: "Re: @Jonathan: Typo, eplace \"Darmstradt\" with \"Darms "
    date: 2007-03-06
    body: "Done, thanks\n"
    author: "Jonathan Riddell"
  - subject: "sixth"
    date: 2007-03-06
    body: "Jonathan: this is the sixth issue of the quarterly report."
    author: "Allen"
  - subject: "Re: sixth"
    date: 2007-03-06
    body: "Guessing he meant Q4.\n\nThanks again for the report Allen."
    author: "Wade Olson"
  - subject: "Re: sixth"
    date: 2007-03-07
    body: "yes, Q4 '06."
    author: "Aaron Seigo"
---
The <a href="http://ev.kde.org/reports/ev-quarterly-2006Q4.pdf">fourth quarterly report</a> from KDE e.V. is now available.  It covers the board meeting in Darmstadt, the fate of the technical working group and the status of the <a href="http://dot.kde.org/1161821085/">SQO-OSS</a> research project.  As usual there are reports from the working groups, including business cards, a branding meeting, an active HCI group and 27,478 commits.  New members and finances are also covered.  If you have been contributing to KDE for some time and want to get involved in the administrative side, do consider <a href="http://ev.kde.org/getinvolved/members.php">joining KDE e.V.</a>




<!--break-->
