---
title: "KDE Commit-Digest for 18th November 2007"
date:    2007-11-22
authors:
  - "dallen"
slug:    kde-commit-digest-18th-november-2007
comments:
  - subject: "Comit diggest!"
    date: 2007-11-22
    body: "Yay finally Commit Diggest time!"
    author: "Carutsu"
  - subject: "Re: Comit diggest!"
    date: 2007-11-22
    body: "Yes, thanks Danny!"
    author: "T. J. Brumfield"
  - subject: "KDEmod"
    date: 2007-11-22
    body: "On Arch Linux, I've been using KDEmod, which I love.  I used those patches on Sabayon and Gentoo as well.  Will any of these improvements ever find there way to KDE 4?"
    author: "T. J. Brumfield"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "what improvements are these?"
    author: "Aaron J. Seigo"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "http://kdemod.ath.cx/features.html\n\n\n# The pertty/KIP patchset, enabling the following features:\n\n    * A translucent selection rectangle, e.g. rubberband\n    * Rounded icon titles when icons are selected\n    * An improved konqueror sidebar, which saves its settings per profile\n    * The ability to open a new tab in konqueror that opens the default home page instead of a blank page\n    * A smooth logout fade effect\n\n\n# SuSE's Kickoff menu (patched for Arch Linux, optional package)\n# Support for LUKS-encrypted volumes in KDE\n# The Kopete Reloading Kit\n# Kopete support for uvc webcams\n# Logout Dialog Theme Support (ported to latest KDE)\n# Software Drop Shadows for KWin (ported to latest KDE)\n# Dunkelsterns improved Icon Execute Feedback effect\n# Selis improved KDE Xinerama support\n# A patch for Kmix to toggle muting with the middle mouse button\n# KDesktop transparency support\n\n# Various security patches & fixes from SVN that did not made it into the actual KDE release\n# Various patches for kdebase/kdelibs to improve the work with Kickoff and its beagle integration\n# Session manager improvements for Beryl/Compiz\n# An improved session manager (lock/logout) kicker applet\n# Superkaramba does not depend on XMMS anymore\n# Less verbal kdesu dialogs (still shows all important information)\n# Less verbal kwallet dialogs (still shows all important information)\n# Merged stop/reload and viewmode buttons for konqueror to save some screenspace\n# Improved sorting of systray icons\n# Much more patches and stuff, just take a look at our svn repository...\n# And at least... A customized theme based on the domino widget style"
    author: "T. J. Brumfield"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "* A translucent selection rectangle, e.g. rubberband\n\ndone\n\n * Rounded icon titles when icons are selected\n\ndone, they even animate in an dout\n\n * An improved konqueror sidebar, which saves its settings per profile\n\nnot done\n\n * The ability to open a new tab in konqueror that opens the default home page instead of a blank page\n\nnot usre about this one.\n\n * A smooth logout fade effect\n\ndone\n \n # SuSE's Kickoff menu (patched for Arch Linux, optional package)\n\ndone\n\n # Support for LUKS-encrypted volumes in KDE\n\nsolid supports encrypted volumes\n\n # The Kopete Reloading Kit\n\ndunno what that is =/\n\n # Kopete support for uvc webcams\n\nsolid supports webcam discovery, and kopete is using that now afaik, however we're bound to what v4l can do. so not sure if that fixes things for you or not\n\n # Logout Dialog Theme Support (ported to latest KDE)\n\nthe logout dialog is now themable, yes.\n\n # Software Drop Shadows for KWin (ported to latest KDE)\n\ndone\n\n # Dunkelsterns improved Icon Execute Feedback effect\n\nnot sure what that is\n\n # Selis improved KDE Xinerama support\n\nshould be in since he's the kwin maintainer ;)\n\n # A patch for Kmix to toggle muting with the middle mouse button\n\nthe kmix applet won't be there until 4.1\n\n # KDesktop transparency support\n \nkdesktop transparency? transparent to ...? (and by transparent we're actually meaning transluscent i take it0)\n\n # Various security patches & fixes from SVN that did not made it into the actual KDE release\n\nobviously those would be in trunk/\n\n # Various patches for kdebase/kdelibs to improve the work with Kickoff and its beagle integration\n\nwould have to be more specific as to what those are, but the kde4 kickoff menu is no longer hard coded to beagle\n\n # Session manager improvements for Beryl/Compiz\n\nnot sure\n\n # An improved session manager (lock/logout) kicker applet\n\nwhat's improved about it?\n\n # Superkaramba does not depend on XMMS anymore\n\nthat's a packaging issue\n\n # Less verbal kdesu dialogs (still shows all important information)\n\ni don't think we've done this in trunk/ yet. it's on my todo, and the generic Qt4 based animated hide/show widget is a step in the right direction (danimo did the original work on that)... unfortunately there are only so many usability and pixel crazy developers around kde, so our time is already spread thin.\n\n # Less verbal kwallet dialogs (still shows all important information)\n\nsee above\n\n # Merged stop/reload and viewmode buttons for konqueror to save some screenspace\n\nsome merging has already been done; not stop/reload though (personally i have issues with that one, but whatever)\n\n # Improved sorting of systray icons\n\nhonestly, i refuse to improve anything in the systray beyond where it is until the systray protocol in x11 is fixed to be something that isn't brain damaged. i tried a few years ago, but honestly most of the people who need to be onside for such big changes are way too shortsighted to even begin to understand the issues. thankfully Seli is not one of those people, but what i said about people who are forward thinking being rare and therefore our time being scant applies there as well; but yeah, Seli should have somethign someday (he and i sat down last year in norway and hashed it all out; i'm happy with what came out of that =). oh, and rasterman from enlightenment also gets it on this point.\n\n # Much more patches and stuff, just take a look at our svn repository...\n\nlacking time; why not try and triage the patches upstream? e.g. help us help you.\n\n # And at least... A customized theme based on the domino widget style\n\noxy has a lot in common with domino when it comes to clean looks and what not. so i consider this one \"done\" too\n\nso.. how are we doing for a dot-oh release? =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "Honestly, I assumed the devs were aware of the KDEmod team and their patches.\n\nI'm not sure why none of this is upstream, but they have a SVN repository.\n\nhttp://www.kdemod.ath.cx/svn/"
    author: "T. J. Brumfield"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "Hi there,\n\nmost patches we are using in KDEmod are from Gentoos KIP project or kde-look or other distros or just fix Arch specific stuff. The most work over the last months has been done on the theme-side as i simply cannot stand the default KDE3 look anymore :-) And our users seem to love the theme, i see many screenshots around with our default settings... Also consider that Arch is a very small distribution and we are only 2 people who package (split), modify and write patches. So far, we have a good workflow with me packaging, customizing and testing etc and Dunkelstern writing patches and packaging for x86_64, and we intend to continue this with KDE4 :-) And we are open for everyone who wants to help us, just to say that...\n\nIn the last months, we tried to patch up KDE3 even more, and Dunkelstern seems to be _very_ capable in writing eyecandy patches from my point of view. However, time is a factor, so our output is limited (although we could do a lot more, the motivation is there, absolutely)...\n\n\nHere are the \"KDEmod\"-patches that got created so far:\n\nIcon Execute Feedback: This is the icon effect also used in Kubuntu. Its nearly the same effect as in OS X, where the icons zoom and fade when you click on them:\n\nhttp://kdemod.ath.cx/svn/trunk/kdelibs/03_dunkelstern-execute_feedback.patch\nhttp://kdemod.ath.cx/svn/trunk/kdebase/19_dunkelstern-execute_feedback.patch\n\n\nShiny Kicker: This patch is not finished now. It will give you the ability to assign a pixmap to kickers translucency and add some additional filtering like blur. Its still a work-in-progress and has some bugs (Dunkelstern is currently fixing the patch, a first version should be there soon):\n\nHere is a small teaser: http://kdemod.ath.cx/temp/shiny.png\n(I dont want to show a complete screenshot because there are still some rendering/redraw issues :-) )\n\nHowever, as kicker is gone in KDE4, this will be a KDE3-only thing...\n\n\nThen, a patch for this issue: http://bugs.kde.org/show_bug.cgi?id=110318\nIt seems to work for us and our users, so far we had no problems with it. Its also posted on the KDE bugtracker:\n\nhttp://kdemod.ath.cx/svn/trunk/kdelibs/04_dunkelstern-async-configfile.patch\nhttp://kdemod.ath.cx/svn/trunk/kdebase/22_dunkelstern-async-history.patch\n\n\n\n@Aaron: You can find the kopete reloading kit here: http://www.kde-apps.org/content/show.php/Kopete+Reloading+Kit?content=49765 . It adds the ability to use themes on the contact list.\n\nThe kdesktop-transparency patch adds the ability to make the desktop wallpaper translucent when using compiz (so you can see the cube through it). Its located here: http://www.kde-apps.org/content/show.php/KDesktop+transparency+support?content=59864\n\nThe \"improved\" lock/logout session applet is just drawn prettier, e.g. the buttons look a little bit better. Its a patch from the openSUSE guys if i remember correctly...\n\nThe patches written by Dunkelstern were also posted on the KDE bugtracker, and the execute feedback one seems to be widely used among other distros already... \n\n\nAnd at last i want to thank all the people who are working on KDE, its a real pleasure to work with your software. And from what i have seen of KDE4, this will be even better in the future :-) \n\nGreetings \n\nJan\n"
    author: "Jan"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "Hi Jan,\n\nYou seem to have many ideas about how to improve KDE. I think that you should proactively send your idea to the relevant KDE mailing lists or to kde-devel@kde.org if you can't find a more specificly suitable list. Otherwise your ideas might be forgotten!\n\nBenoit"
    author: "Benoit Jacob"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "Many KDE devs don't like a lot of \"great new ideas\". I understand that. It's good to KISS. I guess a sort of experimental or modded version of KDE is a good thing for those of us who like to have some slick features that devs want to keep out of the \"vanilla\" version. After all, many distros mod KDE. I actually happen to like that. Because up until recently, KDE has had a few rough edges from an OSes GUI point of view. Or course that is to be expected. So the mods and tune ups are pretty cool IMHO.\n\nOf course KDE4 is supposed to be a whole new deal. So let's see what apps can be made."
    author: "winter"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "Just take a look at the pictures:\n\nDesktop transparency support (yes, it is transparency!):\nhttp://www.kde-apps.org/content/show.php/KDesktop+transparency+support?content=59864\n\nIcon execute feedback:\nhttp://www.kde-apps.org/content/show.php?content=52994\n\nThe Kopete Reloading Kit:\nhttp://www.kde-apps.org/content/show.php?content=49765\nSeems to be a GUI improvement (themable cointact lists etc.)\n\nI personally agree with the domino style. Though Oxygen is very stylish, I still prefer Domino. "
    author: "Sebastian"
  - subject: "Domino for KDE 4"
    date: 2007-11-22
    body: "I love Domino.  Will it be adapted for KDE 4?"
    author: "T. J. Brumfield"
  - subject: "Re: Domino for KDE 4"
    date: 2007-11-22
    body: "You'll have to ask the author:\n\nhttp://kde-look.org/content/show.php/Domino?content=42804"
    author: "Anon"
  - subject: "Re: Domino for KDE 4"
    date: 2007-11-23
    body: "Not by the original author. If you port it, that would be awesome."
    author: "logixoul"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "This is yet another reason why I really like open source. How often do you see developers take the time to answer such questions elsewhere? \n\nThanks a lot to all KDE devs for creating what looks to become a great new release!\n\nAlso thanks for yet another nice digest!"
    author: "Joergen Ramskov"
  - subject: "Re: KDEmod"
    date: 2007-11-23
    body: "# # Superkaramba does not depend on XMMS anymore\n \n# that's a packaging issue\n\nActually not as easy.\n\nXMMS support is a build time option, e.g. havng libxmms as an optional build dependency.\nPackagers could just choose between supporting or not supporting XMMS, not something like doing it as a recommended package.\n\nI personally wrote a patch for the Debian packagers so it can runtime detect libxmms availability by dlopen'ing it.\n \n"
    author: "Kevin Krammer"
  - subject: "Re: KDEmod"
    date: 2007-11-23
    body: "Well, we are using your patch :) Maybe the explanation on our site needs a little update..."
    author: "Jan"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "> http://kdemod.ath.cx/features.html\n\nWhat I don't get about this... what's stopping these people to get such enhancements in KDE itself? In the proprietary world people need to have separate add-ons because the original product can't be modified. In the open world such limitation does not apply. So why don't these people get a KDE SVN account, and get the changes right in KDE?\n\nIs is some kind of big-wall to enter KDE, or is there a pride involved to keep such a patchlist for ones selves? I really wonder what it is, but it should be fixed IMHO."
    author: "Diederik van der Boor"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "Most of these patches are already available since a long time, and some also never got accepted (the rubberband patches for examples, because qt has to be patched for them too). If i remember correctly, most of Gentoos KIP patches were posted on the KDE mailing lists years ago, but they got rejected because of various issues... I can only say that these patches are working _for_years_ now without problems for many people, so i would definitely be glad if they are included in a KDE 3.5.9 or something... (Check out the http://gentoo-xeffects.org wiki, they have a svn repo with their patches)\n\nThen there are patches made by people who are working on KDE (the openSUSE and Kubuntu guys for example), but they dont seem to have any interest to include them into KDE itself. And i like most of the stuff they produce, otherwise it wouldnt be included in KDEmod :-)\n\nAlso, the patches that are made by us either got posted on the KDE bugtracker or are available on kde-apps, so i dont understand the \"pride\" or \"big-wall\" thing you are talking about... The list on our site is only there to let people check whats included and give further background info... And this is something no one else does, they all use a lot of patches but no one (= users) knows what they do or who has written them initially... We simply want to be informative what users get if they install KDEmod :-)\n\nJan\n\n"
    author: "Jan"
  - subject: "Re: KDEmod"
    date: 2007-11-22
    body: "great work guys, kde devs and kdemodders too....\nI enjoy kdemod and kde4 very much!"
    author: "mangus"
  - subject: "Re: KDEmod"
    date: 2007-11-24
    body: "no big wall, these patches often don't get into KDE because 3.5.x is supposed to be stable. Some might get in, some day, but that'll take some time."
    author: "jospoortvliet"
  - subject: "Re: KDEmod"
    date: 2007-11-25
    body: "Are you suggesting people haven't been contributing improvements during the 3.5 cycle?\n\nFrom 3.5 to 3.5.8, there have been numerous new features, improvements, etc.\n\nI'm not sure why these patches specifically have been left out."
    author: "T. J. Brumfield"
  - subject: "Now it all makes sense,"
    date: 2007-11-22
    body: "That explanation of Containment sure cleared a few things up on just how Plasma works. Now that 4.0 looms the development pace seems to be getting faster every day, hats off to the KDE devs.\n\nCommit-Digests never disappoint, thanks for all the hard work putting these together."
    author: "Skeith"
  - subject: "Re: Now it all makes sense,"
    date: 2007-11-22
    body: "The explanation of Containment was very nice, and the thought of being able to nicely bring the desktop into the foreground (along with the icons!) may actually cause me to (possibly ;) use the Run Dialog less (then again, KRunner may cause me to use it more... which creates quite a conundrum!)\n\n>Commit-Digests never disappoint, thanks for all the hard work putting these together.\n\nI can't second this enough!"
    author: "Sutoka"
  - subject: "errors in commit-digest.org"
    date: 2007-11-22
    body: "the errors:\n\n\n\nWarning: file(): Unable to access /var/network/www/docs/commit-digest.org/issues/2007-11-18/introduction.txt in /var/network/www0/docs/commit-digest.org/content.inc on line 101\n\nWarning: file(/var/network/www/docs/commit-digest.org/issues/2007-11-18/introduction.txt): failed to open stream: No such file or directory in /var/network/www0/docs/commit-digest.org/content.inc on line 101\n\nWarning: array_search(): Wrong datatype for second argument in /var/network/www0/docs/commit-digest.org/content.inc on line 118\n\nWarning: array_search(): Wrong datatype for second argument in /var/network/www0/docs/commit-digest.org/content.inc on line 177"
    author: "david"
  - subject: "Re: errors in commit-digest.org"
    date: 2007-11-22
    body: "Fixed, thanks!\n\nNow that the fire is out, i'll pass out (or go to sleep :)).\n\nDanny"
    author: "Danny Allen"
  - subject: "Marketing guys really forked over the big cash"
    date: 2007-11-22
    body: "I think it was worth it, though it was probably pretty expensive to hire Tom Green to do the Plasma screen-cast."
    author: "T. J. Brumfield"
  - subject: "Re: Marketing guys really forked over the big cash"
    date: 2007-11-22
    body: "Well, Chuck Norris was already taken, so Plasma got the next best thing ;)"
    author: "Marketing guru"
  - subject: "Re: Marketing guys really forked over the big cash"
    date: 2007-11-22
    body: "hahaha thumbs up!"
    author: "miro"
  - subject: "Good stuff"
    date: 2007-11-22
    body: "Thanks for the digest Danny!\n\nThe plasma video is pretty cool, lots of effects I'm looking forward to..  \n\nMy only wish is that someone would make that volume on-screen display pretty (end of the video).  It's still just as terribly ugly as in KDE3 and really no longer fits.  \n\nThe rest looks top notch. :)"
    author: "Leo S"
  - subject: "Re: Good stuff"
    date: 2007-11-22
    body: "\"My only wish is that someone would make that volume on-screen display pretty (end of the video). It's still just as terribly ugly as in KDE3 and really no longer fits.\"\n\nhttp://vir.homelinux.org/blog/index.php?/archives/73-volume-change-popup.html"
    author: "Anon"
  - subject: "Re: Good stuff"
    date: 2007-11-22
    body: "Yeah, doesn't look like it'll be in KDE though.  It doesn't need to even be that fancy, even if it was resized to be not massive it would be 100 times better.  The amarok OSD volume is nice and discrete for instance."
    author: "Leo S"
  - subject: "Re: Good stuff"
    date: 2007-11-23
    body: "That is the kde 3 volume control.  Aaron was running his screencast from within an embedded X session called Zephyr (at least I assume).  The kde 4 one will certianly be different, if not better."
    author: "Level 1"
  - subject: "Re: Good stuff"
    date: 2007-11-23
    body: "full disclosure: i was running a full kde4 env, but recordMyDestkop and opengl based compositing don't get along. that and my display resolution is a bit big for these things.\n\nso i turn off kwin composite, start a *second* kde4 session in a xephyr window, turn composite on in that session and record that window.\n\nfreaking cool.\n\nand yeah, that sound control popup is the kde3 one. i have kde3 also installed on my devel system so any apps for which there aren't kde4 versions installed just get filled in by the kde3 versions. the volume popup is one such app that i don't have the kde4 version for on that system.\n\nkde3 apps do look like kde3 apps, as you can see. but they run perfectly. i've used kde3 version of konqueror, kaffeine, ktorrent and others in my kde4 session (which i have been running as my full desktop session for a while now) and it works impressively well. as in.. it just works."
    author: "Aaron J. Seigo"
  - subject: "New \"about\" pages"
    date: 2007-11-22
    body: "They are really good and oxygenish. But IMHO the black plasmoid-like rectangle in the center looks too contrast and unnatural in otherwise low-contrast konqueror window. It attracts all attention from other important parts of the window. Probably the same plasmoid-like border but light-grey background will be better ?\n\nThanks a lot for all you hard-work which made KDE4 possible !"
    author: "Anonymous"
  - subject: "Re: New \"about\" pages"
    date: 2007-11-22
    body: "it's planned to eventually looks like this, btw:\nhttp://img137.imageshack.us/img137/9743/bg3tf9.png\n"
    author: "logixoul"
  - subject: "Re: New \"about\" pages"
    date: 2007-11-24
    body: "wow ;-)"
    author: "jospoortvliet"
  - subject: "App launchers in container"
    date: 2007-11-22
    body: "One thing I was wondering from the screencast is how easy it'd be to set up application launchers in the container.  So instead of having a quick-launch bar, you just bring the container to the front and have a grid of your selected applications to choose from, in nice big icons."
    author: "dooberry"
  - subject: "Re: App launchers in container"
    date: 2007-11-22
    body: "Yeah, exactly, just get rid of the popup type of reaction of the k-button, and put the thing that pops up(kickoff) in a plasmoid itself. now the placing of the kickoff popup is apparently always above the k-button, even when adding a k-button plasmoid and moving it to the top. That way, your menu is always \"open\", and you put it anywhere you like, even rotate it etc, and plasma does all the layout. You can apparently easily make the plasmoids visible, so it would be very quick to use, and you don't have to start over from the beginning when selecting an application, it would still be open from last time you used it. That sure would be nice."
    author: "Tom"
  - subject: "Re: App launchers in container"
    date: 2007-11-23
    body: "unfortunately, kickoff's menu won't be available on the plasma canvas until kde 4.1 when we can use qt 4.4's widgets-on-canvas support. what we have in 4.0 is just the basics (buttons, line edits, etc..) so we could get going.\n\nbut yes, in 4.1 what you describe will totally be possible."
    author: "Aaron J. Seigo"
  - subject: "Re: App launchers in container"
    date: 2007-11-23
    body: "i think it will be really interesting to see how people use plasma. i honestly can only guess and the coming years will provide the real answers.\n\nthe goal is to make the desktop useful again, and i expect people will find all sorts of interesting ways to take advantage of the functionality plasma offers towards fulfilling that goal.\n\nthe idea of launchers on the desktop being easily accessible like this is one of the possible use cases we've considered. i think more than a few people will find it useful ;)\n\nremember that for plasma, 4.0 is just the skeleton upon which we will drape the real flesh in 4.1 and beyond. i hope that, along with the rest of the plasma team, we will dazzle people anew with every release. i already have big plans for 4.1, but it's feeling so good to get this close to 4.0 =)"
    author: "Aaron J. Seigo"
  - subject: "Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-22
    body: "Why there are so many borders is the every Dolphin panel? It's a (Oxygen) style problem or a application one?\n\nLook at the screenshots, the places panel have a border around it, the name 'places' (and the buttons) are almost over this line. And appears to me that every panel have it's border, and vertical and horizontal \"splitters\". The information panel (2nd screenshot) it's pretty overcrowded, so full of stuff that it's difficult to find the information that you need.\n\nhttp://img164.imagevenue.com/img.php?image=30400_7_122_1144lo.jpg\nhttp://img192.imagevenue.com/img.php?image=30406_sreencast_122_505lo.jpg \n\nPlease remove all that borders you can. Sorry to say that, but in this shape Dolphin looks a lot like a KDE 3 application running a modded Plastik theme.  \n\nAnother problem is the green progress bar. It's pretty strange, too big, with a weird proportion. I suggest that you create a small contour around it and make it a little smaller (in the vertical).\n\nThanks and sorry for my bad english."
    author: "kde.fan.from.brasil"
  - subject: "Re: Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-22
    body: "I don't think there are borders enough. The borders help separate distinct functionality and interfaces. The lack of borders between menus and toolbars is bad enough, because they operate completely different."
    author: "Carewolf"
  - subject: "Re: Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-22
    body: "I don't agree.\n\nLook at the MacOS 10 UI screenshots below. They prove that a cleaner UI, with a lot less borders and splitters, works very well, better than any Linux UI (including the Oxygen one).\n\nhttp://www.guidebookgallery.org/screenshots/macosx103"
    author: "kde.fan.from.brasil"
  - subject: "Re: Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-22
    body: "well, there is a border around the places and one around the main view - just like in dolphin.\n\nactually, borders are used quite extensivly on osx. though their theme uses very thin borders, which makes it hard to resize them.\n\nhttp://img.presence-pc.com/news/l/e/leopard_scrn.JPG"
    author: "ac"
  - subject: "Re: Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-23
    body: "Yeah, you are correct, Leopard now have a new look."
    author: "kde.fan.from.brasil"
  - subject: "Re: Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-23
    body: "That wasn't a shot of Leopard."
    author: "MamiyaOtaru"
  - subject: "Re: Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-23
    body: "No?! :-(\n\nSorry for the mistake. Leopard screenshots are in the link below:\n\nhttp://www.apple.com/br/macosx/features/desktop.html"
    author: "kde.fan.from.brasil"
  - subject: "Re: Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-22
    body: "+1"
    author: "anonymous coward"
  - subject: "Re: Why so many \"borders\" (at least in Dolphin)?"
    date: 2007-11-22
    body: "An error occurred while loading http://img164.imagevenue.com/img.php?image=30400_7_122_1144lo.jpg:\nCould not connect to host img164.imagevenue.com."
    author: "logixoul"
  - subject: "Re: Progress bars"
    date: 2007-11-22
    body: "I second the thing about the progress bars... they look clunky and nothing like the elegant and light bars hovering over the other elements with a soft shadow in nuno's mocks. I'd really love to see that again in 4.0, together with the awesome oxygen dropdown-menu style; the actual one is not an eye opener i fear.\n\nSorry guys, but watering our mouths with such beautiful mocks make me complain there a bit ... oxygen is really nice and clean, but lacks just this last \"wow\". Pleeease, remember it'll be christmas soon ;-)"
    author: "Marc"
  - subject: "Re: Progress bars"
    date: 2007-11-23
    body: "The menus will not be done for 4.0. But the mocks will probably not be possible to implement when we actually try to :(\n\nAs for the progressbar - it is not done like we want, but we are running out of time, so maybe this will be postponed for a later release too.\n\nBesides using a progressbar to show disk usage in dolphin is conceptually wrong in the first place, though that is besides the point that the progressbar is not done."
    author: "Casper Boemann"
  - subject: "Re: Progress bars and Tabs"
    date: 2007-11-23
    body: "I was wondering if the tabs will look like it is now in RC1... It was SOOOO very beautiful in the first version of Oxygen (Beta 1, the old version of Oxygen - With Colorful \"background\", like in \"Option 1\" in the Nuno's preview) \n\nhttp://nuno-icons.com/images/estilo/new%20style%20tabs.png\n\nThat was one of the aspects that really showed me somenthing completely new and awesome! Please, please artists and coders of KDE, consider bringing it back!!\n\nAnd the progress bars are also very diferent from Nuno's mockups.\n\nI know you're all running out of time, but it would be great to see those implementations on next releases. Now, Oxygen theme is not as good as Oxygen icons (but I know that's just a momentary issue).\n\nBespin (from the old coder of Oxygen) seems more finished (at least for what we can actually see - the theme), so it is good to take already an alternative at the moment of 4.0 release.\n\nKeep up the good work guys! I know you'll make KDE the best desktop enviroment ever!"
    author: "Augusto"
  - subject: "Re: Progress bars and Tabs"
    date: 2007-11-24
    body: "I'd love to see a theme that more closely matches those mock-ups, and offers the flexibility that Domino provided."
    author: "T. J. Brumfield"
  - subject: "Re: Progress bars and Tabs"
    date: 2007-11-24
    body: "point is: they WANT the theme to be as close to the mockups as possible, but they don't have the manpower to do it."
    author: "jospoortvliet"
  - subject: "Much Loved Features - Not There Yet, or Dropped?"
    date: 2007-11-22
    body: "Here's a couple of KDE3 features that I (and many other people) really love, but which are not yet available for 4.0.  It would be nice to know whether they simply haven't been ported yet, or whether they have been purposefully dropped, for usability or other reasons.  This is an important distinction as I know that I can write a patch for the former and have it accepted, but if for the latter I know I'd be wasting my time[1].\n\n1) Thumbnailing in File Dialogues.  Note that I'm *not* talking about the individual file preview that can be summoned by pressing F11, but the \"icon view\" that thumbnails *all* files in the current directory (even over remote kio_slaves like ftp!) so you can see previews of all of them at once without having to hover over each one individually.\n\nI remember my 58-year-old mum went bananas over this when she saw me attach an image in KMail - she'd been using Thunderbird whose GTK file dialogue does not have this feature, and finding the right image she wanted to attach out of her hundreds was really laborious.  But with KMail, she could see all the previews at once in the file dialogue itself and just scroll through, finding the one she wanted almost straight away.  It's a fantastic usability enhancement for power-users and newbies alike, and is often trumpeted as being one of the top reasons why KDE's file dialogues are so much better than GNOME/GTKs.  Hopefully it just hasn't been ported yet - the idea of such a great labour-saving feature being dropped just for some \"less is more\" aesthetic makes me queasy :/\n\n2) Thumbnail + info when you hover over a file in Konqueror.  This is a great feature because you do not need a separate panel taking up space all the time - just hover over the file you want information/  thumbnail of, and up it pops! Dolphin does not have this feature, relying on the aforementioned panel to display the info, which is one of the reasons why I'll be using Konqueror in KDE4 (no disrespect to Peter and Dolphin - having Dolphin as the default file manager was the Right Choice as far as I'm concerned, as long as Konqueror remained as feature rich as it is now).\n\nUnfortunately, KDE4's Konqueror now embeds Dolphin, so we lose this handy and space-efficient feature.  Any chance of having it return if, say, the File Information panel isn't currently enabled by the user?\n\n3) File Size View in Konqueror.  That awesome little plug-in that shows you what's taking up all the space in your current directory and subdirectories by showing all files as blocks geometrical physical size reflects their on-disk size.\n\n\n--\n\nIncidentally, maybe it would be worth setting up a \"Things That Didn't Make it (But Will)\" page on techbase or the like, detailing all the features that are planned but didn't make it for 4.0 - things like ereslibres excellent progress viewer, for example.  Whenever a feature goes astray, there's often a huge outcry on the Dot about \"teh GNOME-ification!1\" of KDE/ threats to leave KDE or, if it was a feature that was hyped a while back (as was Raphael's progress viewer, in the Road to KDE4) people will scream \"KDE4 is all hype!1\", etc.  While such tantrums can usually be ignored, it's nice to avoid them in the first place, and a page such as this would go a long way towards accomplishing this and quelling whiner's complaints or just putting dedicated fan's (like me!) fears to rest.  What do people think of this idea? Does anyone who has used a recent KDE4 have any items they would add to my list?\n\n[1] There's nothing more demoralizing than this.  I remember when someone (not me!) actually spent time on and submitted a patch to add the oft-requested tabs to Dolphin, only to have it rejected.  Peter was very nice about the whole thing and was very apologetic and clearly conflicted, but it would have been better, I think, to have a clearly publicised statement saying \"We know this feature is really desired by end-users, but we have no plans to include it for the following reasons:\""
    author: "Anon"
  - subject: "Re: Much Loved Features - Not There Yet, or Droppe"
    date: 2007-11-22
    body: ">> 2) Thumbnail + info when you hover over a file in Konqueror\n\nCouldn't agree more: a nice, simple and elegant solution.\n\nI find that feature of G'd old Konqy surprisingly helpful when browsing through loads of archived media (that and the icon file previews, which you also mentioned).\n\nDolphin's preview pane is clunky and space-consuming. IMO, previews and all kinds of metadata -fstat, extattr, Nepomuk, ID3, EXIF, whatever- should be displayed on tooltips and edited from the File->Properties dialog.\n\nLeave those \"all in your face\" design ideas for M$. Surely they'll know how to capitalize on them.\n\nAll said, thanks Dan for you amazing work and happy Thanksgiving to those of you who live in the US!"
    author: "tecnocratus"
  - subject: "Re: Much Loved Features - Not There Yet, or Droppe"
    date: 2007-11-22
    body: "I actually prefer Dolphin's preview pane to tooltips. Less cognitive overhead, less clicks, smoother..."
    author: "logixoul"
  - subject: "Re: Much Loved Features - Not There Yet, or Droppe"
    date: 2007-11-22
    body: "Both should be available, so that the user can enable the one he likes."
    author: "anonymous"
  - subject: "Re: Much Loved Features - Not There Yet, or Droppe"
    date: 2007-11-22
    body: "So we will be in the YAO (yet another option) tunnel again and again. No, thanks."
    author: "Vide"
  - subject: "Re: Much Loved Features - Not There Yet, or Droppe"
    date: 2007-11-23
    body: "That's nice.  But as was mentioned, some people using _Konqueror_ might not want such a pane displayed.  They are using Konqueror, not Dolphin.  \n\nKonqi users who are unhappy about Dolphin replacing it as a filemanager are always assured \"konqueror is still there\".  This obviously isn't entirely correct if Konqi is embedding Dolphin for the file view and missing functionality."
    author: "MamiyaOtaru"
  - subject: "Re: Much Loved Features - Not There Yet, or Dropped?"
    date: 2007-11-22
    body: "\"Incidentally, maybe it would be worth setting up a \"Things That Didn't Make it (But Will)\" page on techbase or the like, detailing all the features that are planned but didn't make it for 4.0\"\n\nYeah, I was actually thinking we need something similar, a series of \"Truth In Advertising\" articles on the Dot going through each core area or module with highlights of what's in, what's out, and what's still to come in 4.1.  Expectation management, if you will.\n\nAnother good series of articles would be \"How Do I...\" which would show people how to master the basics of the new features, e.g. a walk-through of the new file dialog.\n\nAnyone from the publicity department care to comment on what communication / education is planned in the lead-up to The Big Release?\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Much Loved Features - Not There Yet, or Dropped?"
    date: 2007-11-23
    body: "> going through each core area or module with highlights of what's in,\n> what's out, and what's still to come in 4.1.\n\nlet's start with this truth: we have enough to do leading up to 4.0. if we want to also do a full examination of kde 3.5 vs kde 4.0 components and schedule / project for each feature not there in 4.0 we'll need many more people on board doing the work.\n\n> Expectation management\n\nwe'll be showing exactly what 4.0 is capable of, as well as some technology previews at the release event. i think that will help a lot already.\n\n> Another good series of articles would be \"How Do I...\" \n\ni agree. and i bet that you or someone like you could write them =) remember, we aren't a staff of people, we're a community of contributors. i'd love to see a techbase like wiki set up for this kind of content for the community to rally around add to, much like the gentoo, ubunto or other such \"how to\" wikis.\n\nit would take a couple of people to appear on the scene willing to put in the effort to coax the process along. we can provide hosting, the wiki software, etc... but it would take people to make it happen.\n\nperhaps one of the kde forums out there would like to take this on, even, and direct their community towards a new set of possibilities in sharing.\n\np.s. wiki.kde.org could become that place as well, but again .. it would take some strong stewardship to get that sorted out so we have someting akin to techbase but for users."
    author: "Aaron J. Seigo"
  - subject: "Re: Much Loved Features - Not There Yet, or Dropped?"
    date: 2007-11-24
    body: "I agree that it would be nice to see:\n  Feature X was intentionally changed and/or removed.\n  Feature Y didn't make the cut, but should in the future\n  Feature/App Z is in limbo do to lack of a maintainer.  Please volunteer."
    author: "T. J. Brumfield"
  - subject: "Re: Much Loved Features - Not There Yet, or Dropped?"
    date: 2007-11-22
    body: "File Size View is there: it is another kpart so select View/View mode (first menu entry)/File Size View. Not a nice interface though."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Lost"
    date: 2007-11-22
    body: "I've been trying to use kde 4 RC1 these days. But I miss a guide or manual that tells me what I should test, what should be working. Because there are lots of things that are obviously not working. So, should I post a bug because of something that simply isn't ready? Kopete, for example, doesn't store all options of it's settings and some they aren't applied. The plasma replacement of kicker is also missing usability: I don't know how to add or remove plasmoids in it."
    author: "Danilo Luvizotto"
  - subject: "Re: Lost"
    date: 2007-11-22
    body: "Have to agree with this. I've used linux\\kde on and off for a few years and am keen to help with testing on KDE 4. There don't appear to be any release notes to accompany each release - I would expect these to summarise known errors and direct people to where the testing effort is required. Just releasing something and asking people to 'test it' doesn't seem the most efficient or effective way of utilising your testing resource..."
    author: "nic"
  - subject: "Re: Lost"
    date: 2007-11-22
    body: "Common sense applies. If you find a very obvious lack, such as adding plasmoids to the panel, then move on -- it's simply not done. For quirkier things like kopete not storing settings file a bug in bugs.kde.org.\nAlso join #kde4-krush on Saturdays to do intensive testing and collaboration."
    author: "logixoul"
  - subject: "Re: Lost"
    date: 2007-11-23
    body: "actually, you can add them to the panel. drag&drop is your friend :)\nI know this isn't obvious, but nobody's had time to make things more easy to use - making stuff work at all is first priority."
    author: "Chani"
  - subject: "Re: Lost"
    date: 2007-11-24
    body: "I know perhaps this isn't the right place to ask.... but exactly how you drag and drop them to the panel? I've been trying with a very recent build (yesterday) with no luck. If I drag applets over the taskbar, they go under it. If the function is not yet implemented, I apologize for the noise."
    author: "Luca Beltrame"
  - subject: "Re: Lost"
    date: 2007-11-22
    body: "You should test stuff you use every day, and report any bugs you see.\nIt's a lot better for us to have duplicate reports than to have no idea \nthat something isn't working; which is possible since people have very \ndifferent work habits.\n\nA flipside, though: when you submit bugs, please take a look at the duplicate suggestions the bug wizard provides.\n\n\n\n"
    author: "SadEagle"
  - subject: "about containers and the video "
    date: 2007-11-22
    body: "SO the screencast looks fantastic, but I am not sure how the containers work yet. I am wondering if there are plans for the user to be able to zoom in and out using the keyboard. I am not sure the best way is to click on the toolbox.I have to say kde4 it looking better and better everyday."
    author: "tikal26"
  - subject: "Re: about containers and the video "
    date: 2007-11-23
    body: "we don't have a keyboard shortcut for zooming yet... one zooming is more feature complete (there are a couple key things missing to really make it what it needs to be) then we'll come back to the keyboard shortcuts for it =)"
    author: "Aaron J. Seigo"
  - subject: "Plasma"
    date: 2007-11-22
    body: "Can't wait to final! It is do good and ...\n\n\nBut I have some questions in relationship with plasma?\n- what about support for icons on desktop\n- what about compatibly .desktop files both for KDE3 and 4\n- links that i have made in KDE3 don't work in KDE4\n- what about configuring wallpaper? I still can't configure it. It would nice to see options from KDE3 conf dialog - gradients, etc.\n- what about superkaramba support for plasma? And, for example I write plasmoid, how can I run it - or better how can I add it too dialog and new ...\n- also what about apple dashboard support\n- panel is not configurable\n\nI think that KDE have to be higly costumable as KDE3 is - maybe idea for 4.1 - old kcontrol from 3.5 for example - not kubuntu systemsettings!!!. \n\nI know that there are many more important things but please include walpaper option.\n\n"
    author: "Miha Simonic"
  - subject: "Re: Plasma"
    date: 2007-11-22
    body: "> what about support for icons on desktop?\nplanned, not done.\n\n> what about compatibly .desktop files both for KDE3 and 4\nyes they are compatible.\n\n> what about configuring wallpaper?\nyou now can. update.\n\n> It would nice to see options from KDE3 conf dialog - gradients, etc.\nsure. but it's not done yet.\n\n> what about superkaramba support for plasma?\nthere is a plasmoid for superkaramba compatibility. I guess it's still in playground somewhere.\n\n> And, for example I write plasmoid, how can I run it?\nnot sure. you are supposed to use \"Plasmagik\". Also see `plasmoidviewer`.\n\n> also what about apple dashboard support\nit has to wait for QtWebKit.\n\n> panel is not configurable\nyes, known.\n\n> I think that KDE have to be higly costumable as KDE3 is\nit will be loads more so, with all the SVG theming, etc.\n\n> old kcontrol from 3.5 for example - not kubuntu systemsettings!!!\nit's been decided in a developer meeting to use systemsettings by default. why do you want kcontrol?"
    author: "logixoul"
  - subject: "Re: Plasma"
    date: 2007-11-22
    body: "I don't know the original poster's opinion, but I like kcontrol better because it has a tree showing all the available pages.  In kubuntu systemsettings, you have to press the \"Overview\"-button before you can choose another page.  In kcontrol, you select the new page directly in the tree, which is faster.  I also hate the fact that in systemsettings the overview disappears when you select an item."
    author: "anonymous"
  - subject: "Re: Plasma"
    date: 2007-11-22
    body: "yes, same reason. kcontrol is more usuable, but not userfriendly. It should be option. "
    author: "Miha Simonic"
  - subject: "Re: Plasma"
    date: 2007-11-22
    body: "I agree, it is really annoying. I agree that the old kcontrol a bit like a mess, but the systemsettings in kubuntu is worse. Luckily you can switch back in kubuntu, but if kde adopts this as a standard I fear this will not be possible."
    author: "Mark"
  - subject: "Re: Plasma"
    date: 2007-11-22
    body: "Also in kcontrol, if a module does not fit in the window, and needs to be scrolled, Apply, Default etc. buttons at the bottom are always there. In systemsettings you have to scroll to apply."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Plasma"
    date: 2007-11-22
    body: "This is a \"bug\" that could be fixed without dropping systemcontrol as a whole.\nMoreover, the real problem is that many KCM modules doesn't follow the 800x600 rules. Fix the module first, than the container."
    author: "Vide"
  - subject: "Re: Plasma"
    date: 2007-11-23
    body: "what a clusterf."
    author: "MamiyaOtaru"
  - subject: "Re: Plasma"
    date: 2007-11-23
    body: "Couldn't agree more.\n\nkcontrol: select category, click option, select another category, click option...\n\nsystemsettings: click advanced, select category, click option, go back, click general, select category, ugh wrong category, go back, select right category, click option...\n\nIs this an improvement? What should I expect in KDE5?"
    author: "vf"
  - subject: "Re: Plasma"
    date: 2007-11-23
    body: "But how many different settings does someone regularly change at the same time?\n\nA recent message on a list points out the \"people I know\" issue but this can't even fall into that category because it's obvious most people don't complain about not being able to easily change a bunch of settings at the same time. This should be something for everyone not the minority power user or old school \"I love the 90's\" person. ;)\n\nI'm looking at the kubuntu system settings right now. 18 icons in the general category and 9 in the advanced category. I open kcontrol on the same system and I have at the most 9 categories I can click on before I can click on something within one of those categories that will let me change a setting. Some of those I expand only to find that there are still settings within something else that needs to be expanded. *If* everything was expanded and it didn't require a huge windows and still didn't require some scrolling it *might* be faster to change different settings in different modules but it isn't.\n\nThe biggest problem system settings has and related dialogs is that the user doesn't get a sense of any forward or back action. When you click it instantly goes to a different view. If this had an animation it would give that 'feel' of what just happened and a person's instinct can kick in when they see that it \"came from over there\" so they need to \"go back\". They have the related items on the same line so you could push that horizontal line to the side vertically and it would give quick access to similar settings and provide the animation that gives the user a sense of 'where they are' at the same time.\n\n"
    author: "fuegofoto"
  - subject: "Re: Plasma"
    date: 2007-11-23
    body: "get together 2-3 people and start developing kcontrol4. the code is already ported but has some bugs and what not to bring it up to speed. we can host it in extragear and then everyone can be happy with their preference.\n\nbut to support multiple apps for each category requires a similar multiple in the number of people involved. simple math =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Plasma"
    date: 2007-11-23
    body: "logixoul already answered most of the queries, but i'll add a couple notes:\n\n- what about support for icons on desktop\n\nyou can already have icons on your desktop. they need a bit of final touch up love, but they exist and essentially work. what isn't there is loading the contents of the desktop folder. honestly, i think that's a very, very broken concept that needs to be shot in the head and never heard from again. however, given how much so many people have built their workflow around that very, very broken concept we'll have support for it. it will be possible to disable, however, for those who wish to adventure forward a bit. =)\n\n- links that i have made in KDE3 don't work in KDE4\n\nwhich links?\n\n- It would nice to see options from KDE3 conf dialog - gradients, etc.\n\nerg. i really hope we don't duplicate that nightmare. there are tons of fiddly that few to no one uses (when was the last time you saw someone use the desktop pattern + blending option?) .. moreover, containments give us the ability to provide multiple options for background rendering separate from the core. so if someone steps up to create the ultimate mind bending set of options galore for blending patterns, colours and pixmaps with 12 different scaling algorithms ..... we can provide that in extragear very easily while keeping the default clear and simple.\n\nthis is rather similar to the approach firefox has taken for many such features, if you think about it =)\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Plasma"
    date: 2007-11-23
    body: "Ok, i understand. You want to make KDE cleaner and more userfriendly.\nBut look people says that Gnome is userfriendly! OK. It is! But what can you do with it? Nothing (i think). \n\nAnd what about colorizing knotes? - don't like yellow so much. \n\nPlease provide options in extragear.\n"
    author: "Miha Simonic"
  - subject: "Re: Plasma"
    date: 2007-11-24
    body: "Plasma will make things so much more flexible. \n\nChoices don't have to be expressed as just micromanaging options. :)"
    author: "Ian Monroe"
  - subject: "Re: Plasma"
    date: 2007-11-24
    body: "screnshoot from my kde-devel\n\nthere are 2 bugs:\n- icons looks very bad while reising - where is now SVG?\n- systemtray isn't on dockbar\n\nvery good work have done in UI - looks very professional, clean... \n\nwhat about moving items from desktop to panel. it workn't for me. \n"
    author: "Miha Simonic"
  - subject: "Re: Plasma"
    date: 2007-11-25
    body: "What about this http://www.kde-look.org/CONTENT/content-files/40702-40702-koledar.gif"
    author: "Miha Simonic"
  - subject: "Kwin Composite"
    date: 2007-11-22
    body: "Finally I got Kwin Composite working whit ati, aiglx and the new driver.\n\nAnd, I liked a lot some of the effects, as the parent dialog, however, it's a lot slower than Compiz Fusion, and it doesn't appear to offer all the effects.\n\nIt felt like using xcompmgr. Is it really using 3D accel? or is it just using 2D accel?\n\nThanks, and cheers."
    author: "Luis"
  - subject: "Re: Kwin Composite"
    date: 2007-11-23
    body: "Ok, I mess even more whit my xorg.conf, they work much much better know, however, it's still not as fast as Compiz, though it could be fglrx"
    author: "Luis"
  - subject: "The launcher button is not in the corner anymore"
    date: 2007-11-22
    body: "The new panel look is awesome. However, there is a slight usability problem with it: the launcher button is not in the corner anymore and is therefore not as easy to hit.\n\nMaybe some special rule could be devised where when you click on the side borders of the panel, the clicks would register as clicks on the leftmost/rightmost contained plasmoid."
    author: "Antoine Chavasse"
  - subject: "Re: The launcher button is not in the corner anymo"
    date: 2007-11-23
    body: "+1 !"
    author: "Askrates"
  - subject: "Re: The launcher button is not in the corner anymore"
    date: 2007-11-23
    body: "If you make it the full width of the screen then the side borders dissappear, as Aaron explained in his screencast."
    author: "Leo S"
  - subject: "Re: The launcher button is not in the corner anymore"
    date: 2007-11-23
    body: "omg! someone was actually listening! ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: The launcher button is not in the corner anymore"
    date: 2007-11-23
    body: "That must feel nice :)  Thanks for explaining it."
    author: "MamiyaOtaru"
---
In <a href="http://commit-digest.org/issues/2007-11-18/">this week's KDE Commit-Digest</a>: A Calculator and Show Desktop Plasmoid, units conversion and contacts "runners", enhanced composite-based effects, a "dashboard" view and applet hover handles in <a href="http://plasma.kde.org/">Plasma</a>. Updated artwork for "about" pages (like the one present in Konqueror upon application startup). Support for quick user switching in <a href="http://en.opensuse.org/Kickoff">Kickoff</a>. Continued development progress in <a href="">KDevelop</a> 4. Work begins on resurrecting <a href="http://edu.kde.org/keduca/">KEduca</a> for the KDE 4.1 timeframe. New imagery for KTuberling and KMahjongg. Foundations laid for "undo close tab" in Konqueror. OSS device hotplugging in KMix. A bandwidth scheduler plugin in <a href="http://ktorrent.org/">KTorrent</a>. Interface work, including per-protocol UI specification in <a href="http://kopete.kde.org/">Kopete</a>. Hardware database for an enhanced audio device experience in <a href="http://phonon.kde.org/">Phonon</a>. Continued KDE 4 porting in <a href="http://k3b.org/">K3b</a>, with the integration of <a href="http://solid.kde.org/">Solid</a> and Phonon for device and media management. KDE 3.96 tagged, comprising Release Candidate 2 of the development platform (hopefully final), and Beta 5 (or Release Candidate 1) of the Desktop.

<!--break-->
