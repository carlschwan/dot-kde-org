---
title: "KDE-Forum Romania Launched"
date:    2007-04-17
authors:
  - "calexandru"
slug:    kde-forum-romania-launched
comments:
  - subject: "Excellent News!"
    date: 2007-04-17
    body: "Imi pare bine ca a aparut un forum pentru a discuta KDE in Romana. Stiti daca exista vreo companie care vinde calculatoare gata instalate cu Linux si KDE in Romania?\n\nTranslation: I'm glad there's now a forum to discuss KDE in Romanian. Do you know if there is a company that sells computers pre-installed with Linux and KDE in Romania?"
    author: "Vlad"
  - subject: "Re: Excellent News!"
    date: 2007-04-17
    body: " There are several companies that sell computers preinstalled with Linux in Romania but unfortunately mostly do so only to make them cheaper and have no Windows tax. It's unfortunate but people later delete Linux and pirate Windows. You can tell that the preinstalled  Linux OS presents no interest by the fact that it only mentions it's Linux in the adds, not even what distro. I wonder if there's even a manual or something to get you started with Linux and KDE/Gnome.\n Anyway this is a subject we should maybe discuss in the new Romanian KDE forum. See you there people."
    author: "Marius Cirsta"
  - subject: "Re: Excellent News!"
    date: 2007-04-17
    body: "Cheaper and no windows tax is not that bad.\n"
    author: "someone"
  - subject: "Re: Excellent News!"
    date: 2007-04-17
    body: "Well, but at least, the machines should work well with Linux? Otherwise customers could rightfully protest that they are not working properly.\n\nSo even if it is sort of a trick, it could make things move in the right direction. \n\nAnd there may be someone that keeps Linux along, too.\n\nI have heard that in Brasil, 20% of the state-subsided machines sold with Linux are actually kept as shipped.\n\nSo, Linux is gaining new users anyway, even if not as many as the linux-bundled pcs shipped.  "
    author: "Luciano"
  - subject: "despre knoppix(KDE)"
    date: 2007-04-17
    body: "salu romania si tuturor celor care utiliz Linux\nfolosesc de ceva timp linux si cred ca la ora actuala gratie celor care posteaza pe net Linux este concurent puternic pe piata softwar!Folosesc dvd live Knoppix si sunt multunit (care are inclus KDE)Este stabil si intuitiv \nsper sa se dezvolte o comunitate puternica in ro ptr ca suntem baieti destepti \n,in adev sens al cuv\nnumai bine si traiasca LInux\n\n\n\n\n\nadded by ryks cta romania"
    author: "sal all "
  - subject: "Domeniul este liber"
    date: 2007-04-17
    body: "De ce nu se numeste kde-forum.ro"
    author: "VisezTrance"
  - subject: "Re: Domeniul este liber"
    date: 2007-04-17
    body: "Cumpara tu domeniul moshule"
    author: "zvonsully"
  - subject: "Re: Domeniul este liber"
    date: 2007-04-17
    body: "cred ca problema mai mare este hostingul. dupa un timp site-ul va deveni probabil destul de folosit si destul de greu de intretinut. domeniul poate fi cumparat destul de ieftin, mai ales daca este disponibil."
    author: "hvm"
  - subject: "Re: Domeniul este liber"
    date: 2007-04-17
    body: "Il compar eu si, probabil daca lucrurile merg bine, ii gasesc si hosting."
    author: "BogDan"
  - subject: "Re: Domeniul este liber"
    date: 2007-04-18
    body: "wow, it looks more like latin than italian itself, it's wonderful :) (I'm italian, and I did know that romanian is a latin language, but never thought it could be like *this* :)"
    author: "Vide"
  - subject: "Latin"
    date: 2007-04-18
    body: "I was very pleased reading your opinion about romanian language!\nI am very fond about my latins ancestors, it is a historical fact, and I became furious when some stupid people call ALL romanians \"gypsies\"!!!!\nHistory and politics are two bitches, see Yalta Agreement :(\nYou are open minded and intelligent! \nYours respectfully,\nCaius\nP.S. yes, and the names in my family, the REAL ones, are from latin, my father is Virgiliu, my mother Aurelia, my wife Diana and my daugther Iulia!"
    author: "Caius"
  - subject: "Nu va faceti probleme."
    date: 2007-04-17
    body: "Nu va faceti griji. O sa iau masurile sa cumpar KDE-Forum.ro. Momentan n-am finantele disponibile, insa cu cresterea comunitatii acest lucru o sa devina realitate."
    author: "Catalin Zamfir Alexandru"
  - subject: "Re: Nu va faceti probleme."
    date: 2007-04-17
    body: "How much would be the kde-forum.ro domain name?"
    author: "Andras Mantia"
  - subject: "Re: Nu va faceti probleme."
    date: 2007-04-17
    body: "a .ro domain is under 50 usd. But take note, this is a permanent domain; no yearly payment. "
    author: "VisezTrance"
  - subject: "Re: Nu va faceti probleme."
    date: 2007-04-17
    body: "Then I see no problem buying this domain if needed. I can also help with some money if needed (and the forum gathers a community around it)."
    author: "Andras Mantia"
  - subject: "speak romanian on .ro"
    date: 2007-04-17
    body: "It's nice that you guys got a Romanian site where you can speak Romanian, but this is an international site, it wouldn't make sense for me speak Danish here either.\n\nTak for kaffe.\n\n//Pascal"
    author: "pascal"
  - subject: "Forums for countries or languages?"
    date: 2007-04-17
    body: "I note the announcement is for a 'Romania' forum, not 'Romanian'. From now on are we going to have separate forums for each country, instead of one per language?"
    author: "John Karp"
  - subject: "Re: Forums for countries or languages?"
    date: 2007-04-17
    body: "Probably some combination thereof.  For example, there exists a KDE Chile, but that doesn't mean it's useless to other Spanish speakers..."
    author: "Troy Unrau"
  - subject: "Opensuse problems"
    date: 2007-10-22
    body: "I tried to mount partition c,and i did it. But when i did it for partition d, i followed the same steps,one thing i forgot: to edit file /etc/fstab .\nAnd so now when i start computer and boot opensuse, it stocked and so i have to restart and boot windows. Can you help me? what should i do?"
    author: "Ligia"
---
After the rise of <a href="http://www.kde-forum.org">KDE-Forum.org</a>, and <a href="http://www.kde-forum.de">KDE-Forum.de</a>, Romanians wanted a forum of their own, and <a href="http://kde-forum.bluepink.ro">KDE-Forum Romania</a> was born. Destined to unite all Romanians under one roof, KDE-Forum Romania is going to join its brothers on the boat for the upcoming KDE 4 release.


<!--break-->
