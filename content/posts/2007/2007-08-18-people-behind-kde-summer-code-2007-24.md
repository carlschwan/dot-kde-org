---
title: "People Behind KDE: Summer of Code 2007 (2/4)"
date:    2007-08-18
authors:
  - "dallen"
slug:    people-behind-kde-summer-code-2007-24
comments:
  - subject: "man in the garden"
    date: 2007-08-18
    body: "that man in the garden looks really scary to me"
    author: "nick"
  - subject: "Re: man in the garden"
    date: 2007-08-18
    body: "As a fellow bearded man, I resent the comment. \n\n"
    author: "Ian Monroe"
  - subject: "Re: man in the garden"
    date: 2007-08-18
    body: "As _the_ fellow bearded man, I've had that comment before!"
    author: "Gavin Beatty"
  - subject: "Re: man in the garden"
    date: 2007-08-19
    body: "I think he was referring to the merciless use of barbecue sauce, not the beard.\n"
    author: "Jake"
  - subject: "Thank you all"
    date: 2007-08-18
    body: "Thanks to all the students participating in this special Behind KDE series!\n\nYour interviews will help people remember that starting to contribute to KDE is not as scary as it might look at the first glance.\n\nUsually our most visible contributors are people who have been with the project for quite some time, so it is probably easy to forget that most of us did not start as gurus. Seeing what you all managed to accomplish in in this relatively short period of time, will hopefully encourage others to give it a try themselves.\n\nAnd obviously also many thanks to Danny, for handling all the work behind (pun intended) the scenes.\n\n"
    author: "Kevin Krammer"
  - subject: "IM app idea"
    date: 2007-08-18
    body: "\"An idea my mentor, Cristian Tibirna, had about IM apps\"\n\nwhat is that about? :)"
    author: "Patcito"
  - subject: "Thanks"
    date: 2007-08-19
    body: "Thanks to google for providing us with Pierre such a great ODF-hacker! :)\n"
    author: "Sebastian Sauer"
  - subject: "Re: Thanks"
    date: 2007-08-19
    body: "I must say that I'm really pleased to hear about his work. I suppose hacking input/output filter must be kind of boring, and having someone focusing on this is extremely good news. I'm looking forward to KOffice 2.0, it seems full of promises. "
    author: "Richard Van Den Boom"
  - subject: "Windows Users"
    date: 2007-08-19
    body: "I find the interview with Carlos Licea specially interesting. It seems he's one of the few that comes almost directly from the Windows world, and I'm sure he'll stay with our community for quite some time.\n\nWe'll probably get used to this as soon as KDE 4 starts working nicely on Windows, but it's even better when new comers to Linux start contributing so early. Surely they'll find a community they couldn't even imagine on the propietary world!"
    author: "Albert Cervera i Areny"
---
<a href="http://behindkde.org/">People Behind KDE</a> releases the second interview in its series of four interviews with students who are working on KDE as part of the <a href="http://code.google.com/soc/kde/about.html">Google Summer of Code 2007</a> - <a href="http://behindkde.org/people/soc2007-two/">meet Bertjan Broeksema, Carlos Licea, Pierre Ducroquet and Gavin Beatty!</a>


<!--break-->
