---
title: "KDE Commit-Digest for 8th April 2007"
date:    2007-04-09
authors:
  - "dallen"
slug:    kde-commit-digest-8th-april-2007
comments:
  - subject: "dolphin and http://"
    date: 2007-04-09
    body: "it would be cool if typing http://www.kde.org in dolphin could automatically open konqueror or firefox with http://www.kde.org. I think that's the default behavior of vista's explorer when using firefox as the default browser.\n\n"
    author: "Patcito"
  - subject: "Re: dolphin and http://"
    date: 2007-04-09
    body: "good idea but definitely use KDE's default browser rather than firefox ;)"
    author: "ben"
  - subject: "Re: dolphin and http://"
    date: 2007-04-09
    body: "It would be nice if it is the \"default browser\", and not just hardwired for Konqi. Funny enough, I will be using only konqi, but I think that choice is a good thing."
    author: "a.c."
  - subject: "that's already in kde3"
    date: 2007-04-09
    body: "In KDE3 you arelady kcontrol you can configure which is your default browser, etc. So no surprise here: it should be the same in KDE4."
    author: "Eduardo Robles Elvira"
  - subject: "dolphin and tabs"
    date: 2007-04-09
    body: "it would be nice to have tabs in dolphin too cause splitting views is usefull when you need to copy/past but when you just need to have several repositories open, tabs are just better IMO."
    author: "Patcito"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "fully agree"
    author: "LordBernhard"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "Yes definitely. IMO that's certainly a killer feature of Konqueror at the moment, middle click on a folder and you get a nice new tab."
    author: "ac"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "...that is, if you have Konq configured to \"web behavior/open links in new tabs instead of new windows\". Otherwise, you get a new window with middle click. It would be nice to be able to configure middle click behavior and intercepting new window opening separately... Not that configuring middle-click on a folder under \"web behavior\" is sensible, either."
    author: "slacker"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "I like tabs in konqueror. It could be implemented in Dolhin but embeded Konsole is more important IMO :) And as Peter said it is going to be implemented :D jupi! \nhttp://bugs.kde.org/show_bug.cgi?id=143024"
    author: "elo"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "Ditto. I couldn't agree more. People don't need to use them if they don't want (so no necessary visual clutter beyond a couple more menu items), and surely it couldn't be a hard to implement given Dolphin already has the split view (i.e. it's already comfortable handling multiple views)?\n\nI really love where Dolphin is going, and this would make me even happier; I hope the devs feel the same. :)"
    author: "Jeff Parsons"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "personally, I think they shouldn't add tabs. You should just use konqi, if you want such features (I'll be doing that anyway) and we want Dolphin to be as simple as possible, for ppl who need that..."
    author: "superstoned"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "Agreed. I don't think the argument \"if you don't want it, don't use it\" is so good; or should I request a Tetris clone in Dolphin? Exaggerated, but you see my point.\n\nDolphin should be kept simple, and as far as I know, the most users wouldn't use tabs in a file manager. Based on my experience, so I could definitely be wrong. But if you like tabs, and other \"advanced\" features, why not stick with Konqueror? (This is not a rhetorical question, I want to hear your reasons.)"
    author: "Lans"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "I do not think tabs are \"advanced\" features just for power users. They can be useful for everyone. I think they can provide a similar way of working that the one achieved in internet browsers, and know almost all of then have choosen tabs as the way to go. I still can remenber when interner explorer users say that they do not need tabs...\nAnother point is to keep Dolphin interface as simple as possible. But, Do you think adding a couple of options to open a new location in a new tab is going to clutter the interface?, I do not think so."
    author: "beetle"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "I don't think tabs are advanced either, that's why I had rabbit ears (\"\") around the word.\n\nI use tabs when surfing on the Internet, but very seldom when browsing files. Why? I don't know myself, but some reasons I use tabs are:\n- If there are many links I want to check, I usually open them in tabs. After I've read the first one, I can close the tab and have the second one ready.\n- When searching for example information, it is good to have many pages loaded in tabs.\n- If you're doing something, for example writing a comment, and suddenly want to check something (google something for example), I prefer to open a new tab rather than a new window.\n\nI just don't see the same use for a file manager. Even if I consider myself as a \"power user\". The most people (non-Linux) I've asked wouldn't use tabs even if they were available.\n\nBut maybe it's just about preference and habits. I'm not saying absolutely no to tabs in Dolphin, but right no I haven't found any really good arguments to add them."
    author: "Lans"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "Tabs can be useful during file browsing.  For example, in krusader, I have three tabs in each pane for a total of six open folders.  They're useful for short-attention-span people.  When I all of a sudden want to copy pictures from my digital camera, but I also want to keep an eye on my dcc folder for irc, and I also want to remember to watch a movie I downloaded, I just open new tabs for each one, so they can all be done at once.  Tabs only make krusader better, and would probably do the same for dolphin."
    author: "spielburg"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "Enabling tabs and Dolphin being simple are not opposite. The good thing with tabs is that it can be hidden that they are possible until you use them. So if you don't want to use them, you don't see them (aka there's no tabbar).\nAnd when the user discovers the benefits of tabs, they're available for him to use."
    author: "Phase II"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "I disagree.  Dolphin is supposed to be the file manager for the average user, not power users.  Split view is easier to visually follow so is perfect for non-techie users.  Things like tabs and embedded konsole should be left for konqueror.  If you want those features set the default file manager to konqueror instead of dolphin.  Just my two cents."
    author: "Carl"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "Dolphin should have tabs, they should just be easy to understand.\nYour trying to make the average user dumber than he/she is. I think Dolphin should be made for the average user not the super-simple user. \n\nIt's the same as saying that OO.o Writer should only have bold and italic, everything else confuses the user. All the power-users can use Latex.\nWhere's is the Average user?"
    author: "pascal"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "It is not about dumb or experienced users. It is about clean interfaces.\n\nThe unix philosophy was one tool one task. that works well.\n\nWhen your software can break x different ways it gets really complicated. \n\nWe don't want choice. We don't want cluttered software. \n\n"
    author: "bert"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "Where is the filemanager for power users?"
    author: "kolla"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-13
    body: "Krusader tries to fulfil this need. Judge yourself. ;-)\n\nhttp://krusader.sourceforge.net/\n"
    author: "dek"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "I fully disagree. Dolphin is the simple solution, yet it is turned again into something complex and complicated. I already wonder why the splitview is needed at all.\n\ndolphin reminded me how little features are really needed."
    author: "bert"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: ":D\n\nI agree with you. Konqi is for those who want features. The split-screen function isn't even in windows, mac or Gnome - why would a normal user need tabs? I do, sure, but most others don't..."
    author: "superstoned"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "Windows has the option to \"tile windows (horizontally/vertically)\" though- sme net effect, really.  Nautilus has tabs if you bother to use them.  Mac finder uses one part directory structure and one part arbitrary metadata for file management.  All three methods work and have been proven by long use in some form or another.  Why is it not a good idea to give people choice?  Depending on what your users actually DO, there are many things that could be considered simple. There are directories that I just keep open all the time (in tabs). Other times, I'm moving things around and organizing (split pane), and I'm very much looking forward to Nepomuk integration in KDE4 very much because metadata can be very useful for finding things quickly."
    author: "Wyatt"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "I wonder how tabs can be \"complex and complicated\"?\nThe tab feature gives you the possibility to middle click a folder and have it opened in a new tab, so you have more than one folder/location easily available at the same time, just like in any modern web browser. If you don't need that or open another folder, you don't even notice that it's there. Only if you actually open another folder, you have the tabbar with the folder names on it which you can navigate like your web browser. The user decides what he wants to see and use.\n\nMaybe you can elaborate on why you think the possibility would make Dolphin harder to use?"
    author: "Phase II"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "Yes I completely agree, Tabs make life easier :-) and if you don't need them,  they don't show up at all in the UI.\n\nI hope the developpers of Dolphin are reading these discussions and will develop a tabbed structure for dolphin as they have done and developped the tree view.\n\nThanks all the KDE developpers for your efforts "
    author: "landolsi"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-09
    body: "I fully agree.  And anyway, is Dolphin supposed to be \"simple\" (i.e. bare minimum of features), or merely \"specialised\" (i.e. could easily achieve file-manager feature parity with Konqueror, or even surpass it)? What's the goal of Dolphin? I know that \"usability\" is a goal, but I don't see that omitting *use*ful features is really in line with that."
    author: "Anon"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "When I hear \"usability\" and \"removing features\" in the same sentence, it reminds me of why I ditched Gnome in favor of KDE few years ago..."
    author: "slacker"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "Not removing features but hiding complexity. That is a difference."
    author: "bert"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "Should be a difference.  In practice it doesn't seem to be.  Since extra features gets equated with complexity, removing complexity means removing features.  Or hiding them, but how's that any different?"
    author: "Cliffton"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-11
    body: "If you call removing \"hiding\", and call features a \"complexity\", then you are basically saying the same thing..."
    author: "slacker"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-11
    body: "Splitview is great! I used it to copy files from a backup to my home directory, and vice versa. It let me decide what to do with a file/directory one at a time without having to go back and forth. Having more than one window open just has too many menus, toolbars, etc for me.\n\nI turn it off for regular file browsing, but any time I need to copy or move a file, I just open split view, go to the directory, and drag it over."
    author: "Soap"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-10
    body: "Even better, it would be cool to integrate tabs and split-views. I mean tabs behaving somewhat like those on the left of Konq sidebar - you click a tab, and it is shown in the sidebar instead of the previous one. BUT, right-click the tab-bar, check the \"multiple views\" option, and now clicking on a tab splits the view, showing BOTH the previous and the new tab."
    author: "slacker"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-11
    body: "As long as I can turn off tab browsing, I'm fine with that."
    author: "Soap"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-13
    body: "Oh, have you ever heard about \"reinventing the wheel\"?:-)\nI think that the basic purpose of Dolphin was to provide users with a SIMPLE file manager. I mean, there are many people, for whom Konqueror represents a total overkill - a bunch of features and flexibility they simply don't need...Dolphin should never be a Konqueror clone (and with those two coexisting, the choice is absolutely trivial:\n\"I want tabs, total web integration, configurability like I-want-this-precious-kpart-embedded etc.\" -> Konqueror\n\"I'm tired of \\\"mc\\\" and simply want a FILE MANAGER, not that overcomplicated Konq-thing.\" -> Dolphin)\nUnderstand?"
    author: "Tomas Trnka"
  - subject: "Re: dolphin and tabs"
    date: 2007-04-13
    body: "There's absolutely no reason why tabs should make things any more difficult for an end user, though - if need be, they can only be enabled if the config file is manually changed, and simply never seen otherwise.\n\nAlso, the mere addition of tabs will not turn Dolphin into a Konqueror clone - that would further require the ability to load arbitrary KParts and include the massive baggage of config options for cache, cookies, javascript etc.\n\nAs has been mentioned many times, tabs are a boon to File Management, not just web-browsing, so there is little reason not to include them, in my opinion."
    author: "Anon"
  - subject: "Good to see..."
    date: 2007-04-09
    body: "...optimizations in KJS. At the moment pages with much of JS\nfeel heavy in Konq.\n\nThanks HP :)"
    author: "m."
  - subject: "Re: Good to see..."
    date: 2007-04-09
    body: "Funny, in my experience Konqueror's Javascript is way faster than Firefox's.\nThough making Kjs even faster is very welcome, of course!...\n"
    author: "dario"
  - subject: "Re: Good to see..."
    date: 2007-04-11
    body: "I've always found KJS to be slow.  It loves to choke on scripts (this site gets it to tell me that a script is causing khtml to freeze for example: http://www.hardforum.com/showthread.php?t=1092402 )\n\nAlso, the javascript benchmark here:\nhttp://www.24fun.com/downloadcenter/benchjs/benchjs.html\nResults on my machine with Firefox and Opera are similar (Opera around 5 seconds (on Windows faster still :-/ , Firefox around 5.5 seconds) while Konqueror comes in at around 11 seconds.  Taking twice as long as the others is hardly a nice thing.\n\nSo yeah, some KJS speed improvemens would be hot.  I know it's hard to implement stuff from Safari, but they seem to have dealt with the speed issues somewhat.  I'll have to post a result for the benchmark with in Safari.\n\nOpera 9.2, Firefox 1.5.0.7 Konqueror 3.5.5."
    author: "MamiyaOtaru"
  - subject: "Re: Good to see..."
    date: 2007-04-12
    body: "OK, Safari on the same machine runs the benachmark in 2.5 seconds (!), even faster than Opera on Windows with 4.\n\nOpera on Linux with 5, Firefox Linux with 5.5 and Konqueror with 11 (!) are way behind.  Konqueror embarassingly far behind.  If whatever speedups were implemented in webcore can't be ported back to KHTML, would using webcore in KDE bring those sorts of speeds?  I'm really amazed."
    author: "MamiyaOtaru"
  - subject: "Incremeantal Build System QSA"
    date: 2007-04-09
    body: "Hi Devs,\n\nis there somewhere an incremental Build System for Kde4 Trunk ? I have seen this sometime around kde 3 but never seen it used.\n\nFor QSA, dont know if Cmake supports this : if a file changes , determine what parts need to rebuild and show the result on a Webpage + Mail the commiter.\n\ncan someone please post the url?"
    author: "inco"
  - subject: "Re: Incremeantal Build System QSA"
    date: 2007-04-09
    body: "Eh... surely any build system is incremental these days? Only the stuff that needs to recompile compiles (mostly). And yes, I think cmake can spit out a report of what needs to be rebuild, but why? What's the usecase?\n"
    author: "Esben Mose Hansen"
  - subject: "Re: Incremeantal Build System QSA"
    date: 2007-04-09
    body: "one usecase would be to ensure , every commit builds"
    author: "inco"
  - subject: "KPackage"
    date: 2007-04-09
    body: "\"KPackage starts to be ported to the SMART package management scheme.\"\n\nWow, that sounds pretty significant.  KPackage doesn't seem to have an official website, where can I go to find out more about this?"
    author: "Anonymous"
  - subject: "Re: KPackage"
    date: 2007-04-09
    body: "hmm... where we find information in XXI century? :)\nhttp://en.wikipedia.org/wiki/KPackage"
    author: "heh :)"
  - subject: "Re: KPackage"
    date: 2007-04-09
    body: "Maybe ask the person who did the work?!?"
    author: "superstoned"
  - subject: "Re: KPackage"
    date: 2007-04-10
    body: "Awesome. After Yast, apt* and many others, KPackages is still my main-stay for quick package management. Now, instead of just telling me a dependency is missing, it will pull it up. Cool."
    author: "Daniel"
  - subject: "Re: KPackage"
    date: 2007-04-10
    body: "urpmi ?"
    author: "bert"
---
In <a href="http://commit-digest.org/issues/2007-04-08/">this week's KDE Commit-Digest</a>: Bluetooth support in <a href="http://solid.kde.org/">Solid</a>. 'Breadcrumb" navigation widget from <a href="http://enzosworld.gmxhome.de/">Dolphin</a> is made more modular to allow use in other KDE contexts. Support for different caret (text cursor) styles in <a href="http://konsole.kde.org/">Konsole</a>. Various bugfixes in <a href="http://developer.kde.org/~wheeler/taglib.html">TagLib</a>. Better AIM protocol file transfer support in <a href="http://kopete.kde.org/">Kopete</a>. <a href="http://koffice.kde.org/kword/">KWord</a> gets the ability (through <a href="http://kross.dipe.org/">Kross</a> scripting) to use an OpenOffice.org instance to import from supported file formats. KPackage starts to be ported to the <a href="http://labix.org/smart">SMART package management scheme</a>. The beginnings of user documentation for the Bovo game application, whilst the initial draft of the <a href="http://www.mailody.net/">Mailody</a> handbook nears completion. <a href="http://www.kmobiletools.org/">KMobileTools</a> starts to be ported to KDE 4. Great reductions in the number of external dependencies for the kdecore library. The kplot library is renamed "plotting", plasmagik is renamed "uploader". <a href="http://okular.org/">okular</a> moves back from the playground to the kdegraphics module. <a href="http://home.gna.org/kiriki/">Kiriki</a> is moved to kdereview. Kremas, an experimental image viewer, is imported into KDE SVN.
<!--break-->
