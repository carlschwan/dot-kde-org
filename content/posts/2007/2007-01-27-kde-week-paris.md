---
title: "KDE Week in Paris"
date:    2007-01-27
authors:
  - "cmiramon"
slug:    kde-week-paris
comments:
  - subject: "wish i'll be there"
    date: 2007-01-27
    body: "hello all\n\ni wish to be there; but life is just unfair"
    author: "morphado"
  - subject: "Re: wish i'll be there"
    date: 2007-01-28
    body: "I agree....\n\n:-(\n\n\n\n."
    author: "from South America"
  - subject: "Paris?"
    date: 2007-01-31
    body: "in, aehm, Paris (Hilton) :>< , ehehehehe :>>>\n\nwould be fun !"
    author: "chri"
---
In partnership with the Qt based VoIP free software <a href="http://www.openwengo.org/">OpenWengo</a> and <a href="http://www.mandriva.com/">Mandriva</a>, <a href="http://www.kde-france.org/articles/divers/semaine-kde-a-paris.html">KDE-France is organising week of events in Paris</a> from January 29th to February 2 about KDE 4.   KDE will be present at the <a href="http://www.solutionslinux.fr">SolutionsLinux trade show</a>, the annual French professional meeting for Open Source software. We will have a booth and give two presentations during the show. Two open KDE 4 hacking events will sandwich Solutions Linux, <a href="http://www.kde-france.org/component/option,com_openwiki/Itemid,56/id,solutions_linux_2007/">one before at OpenWengo</a> and <a href="http://dot.kde.org/1168969796/">the Nepomuk workshop</a>, previously mentioned on KDE Dot News, organised after. 



<!--break-->
