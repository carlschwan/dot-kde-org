---
title: "KDevelop 3.4 Brings Many New Features"
date:    2007-01-27
authors:
  - "jriddell"
slug:    kdevelop-34-brings-many-new-features
comments:
  - subject: "You rock!"
    date: 2007-01-27
    body: "Hi,\n\nthe new Kdevelop is great.... thanks so much! I am excited about what is planed for Kdevelop 4...please rock on! The only thing I am worried about is a potential fork of the project \n\nhttp://lists.kde.org/?l=kdevelop-devel&m=116940111208187&w=2\nhttp://lists.kde.org/?l=kdevelop-devel&m=116939723404129&w=2\n\nplease don't do this because of a name!\n\n"
    author: "MK"
  - subject: "Re: You rock!"
    date: 2007-01-27
    body: "I +++++++++++++++++++++++++++++++++++++++++++++++++"
    author: "kollum"
  - subject: "Re: You rock!"
    date: 2007-01-27
    body: "Very good point. You guys have to work it out without causing any fork. Forks are OK when they are beneficial, in this case, I don't see any from renaming.\nHere is a mid solution which others already presented in the e-mails with slight modification.\n\nKDev for Kdevlop. indicative, meaningful, & short, \nKDevForm, KdevPlat, KDevBase, KDevPlatform for KDevelop Platform.\n\nGet a resolution without any forking PLEASE!!! Otherwise you are wasting your novel efforts for nothing."
    author: "Abe"
  - subject: "Re: You rock!"
    date: 2007-01-27
    body: "Wow, what a lame name. Koncrete. The IDE for 10-year-old-boys-who-enjoy-stupid-names. WTF?"
    author: "AC"
  - subject: "Re: You rock!"
    date: 2007-01-28
    body: "Yikes, it reminds me of this old joke:\n\nhttp://www.geocities.com/rcwoolley/\n\n:)"
    author: "Colin"
  - subject: "Re: You rock!"
    date: 2007-01-27
    body: "Please don't worry. If you look at the thread, you'll find out that nobody wants to fight to the death for just a name. We'll have the name issue sorted out as soon as we finish the rework of the core platform stuff. \"Koncrete\" wasn't the first proposed name and it won't be the last one.\n\nAbout the fork... Judging from the replies to the email and IRC conversations I can assure you that nobody from current KDevelop contributors supports the idea of a fork.\n"
    author: "Alexander Dymo"
  - subject: "Re: You rock!"
    date: 2007-01-28
    body: "I don't like the name Koncrete... it just doesn't sound any better than KDevelop. Why not try to come up with something like the other main KDE 4 projects: Plasma, Phonon, Solid, Decibel."
    author: "ac"
  - subject: "Name"
    date: 2007-01-29
    body: "How about \"Kodex\"?"
    author: "Daniel"
  - subject: "Re: Name"
    date: 2007-01-29
    body: "You know, that's actually pretty damn good. Drop the K, make it 'Codex', and I'm very much sold. :)"
    author: "A.C."
  - subject: "Great release"
    date: 2007-01-27
    body: "Thank you to all the people who continue to put work into this great software. Really great release."
    author: "ac"
  - subject: "Definitely!"
    date: 2007-01-28
    body: "Definitely true!\n\nThe configuration of this release just got soo clean.. :)\n\nI notice it in many places, the class browser with bold method names, the RMB settings of the output display, and global settings. It's great to see so many improvements there."
    author: "Diederik van der Boor"
  - subject: "Digg the story!"
    date: 2007-01-27
    body: "Don't forget to digg the story and help spread the good news:\nhttp://digg.com/software/KDevelop_3_4_Released"
    author: "Tsiolkovsky"
  - subject: "Re: Digg the story!"
    date: 2007-01-27
    body: "dugg"
    author: "Mark Kretschmann"
  - subject: "Loving the Qt4 support"
    date: 2007-01-27
    body: "The Qt4 support is great. I've just started working on a new Qt4 project and KDevelop is making it really easy."
    author: "James"
  - subject: "Support for interface/ source/ directories"
    date: 2007-01-27
    body: "It seems that the \"New class\" tool still do not allow to place .h and .cpp in two different directories. \nMaybe I should report to bugs.kde.org..\nAt least now importing a class with .h and .cpp in two different dirs does not produce two different entries in the class tree.\n\n\n"
    author: "and."
  - subject: "Great!"
    date: 2007-01-27
    body: "This looks very good. this is the first kdevelop version that actualy starts for me, lets me create a new project with a sample application and run it. all this withou crashing!! i always wanted to start coding for kde, but until now kdevelop was too buggy for me. now i will take a closer look at it"
    author: "Beat Wolf"
  - subject: "Re: Great!"
    date: 2007-01-27
    body: "Oops, honestly it should have worked before too ;) Don't forget to post a bug report if you see crashes in the future please. Our bug squad (Andreas and Jens) are eager to see new bugs :)"
    author: "Alexander Dymo"
  - subject: "Tanks..."
    date: 2007-01-27
    body: "for the best IDE on unix systems. \nKeep up the great work please!"
    author: "David"
  - subject: "Re: Tanks..."
    date: 2007-01-28
    body: "wow, you want to give them tanks? that may be dangerous if they start playing around :-)\n\nNevertheless, hooray for the kdevelop team!"
    author: "infopipe"
  - subject: "Re: Tanks..."
    date: 2007-01-28
    body: "Thanks too for such a great release!"
    author: "Sebastian Sauer"
  - subject: "Showcase"
    date: 2007-01-27
    body: "We also prepared a little presentation for some of the new features and improvements:\nhttp://www.kdevelop.org/3.4/showcase.pdf\nhttp://www.kdevelop.org/3.4/showcase.odp\n"
    author: "Alexander Dymo"
  - subject: "Re: Showcase"
    date: 2007-01-28
    body: "Thanks for the informative presentation, and keep up the great work! KDevelop is my favourite IDE thanks to you guys and girl(s)."
    author: "Claire"
  - subject: "Wow"
    date: 2007-01-28
    body: "500 bugs closed is impressive, and you managed to include quite a few useful additions while you were at it :) Good work guys, if only we had such a dedicated team ferretting out the bugs for other major KDE projects, we'd, well, be alot less buggy :D"
    author: "Ben Axnick"
  - subject: "Python"
    date: 2007-01-28
    body: "What about the Python support in KDevelop? How good is it? Usable?\n\nThanks,\n\nFlorian"
    author: "Florian"
  - subject: "Re: Python"
    date: 2007-01-28
    body: "For a good python IDE use eric3. KDevelop can't compete with it."
    author: "Andreas Pakulat"
  - subject: "Re: Python"
    date: 2007-01-28
    body: "\"For a good python IDE use eric3. KDevelop can't compete with it.\"\n\nI don't agree that we should leave PyKDE support to Eric3 (or Eric4). It doesn't have the concept of project types where you can create a standard sort of project such as a python KDE part or a python KDE application, in the way you can with KDevelop. I agree Detlev Offenbach is doing a brilliant job, but it would be very good if we could have some serious python support in KDevelop4 too. I believe there has been some separate work with python KDE project templates, such as python kparts or python kde kicker applets, but that has never been integrated with an IDE - and I believe it is really important that we should do something about that for KDE4."
    author: "Richard Dale"
  - subject: "Re: Python"
    date: 2007-01-30
    body: "I personally disagree. It's true that eric3 has a lot more features related to Python, but I still use KDevelop instead because eric has (IMO) a very cluttered interface that makes working with the program hard."
    author: "Luca Beltrame"
  - subject: "Re: Python"
    date: 2007-02-21
    body: "Since the ruby will be support seriously, why the python not?\n\nRails? Python have the similar things like dijango and turbogears. Of course python has PyQt like RubyQt. I don't know musch about ruby, but I think python is qt-friendly at least like ruby. maybe better: i use some native kde apps programed with python but seems no one is programed by ruby.\n"
    author: "JackPhil"
  - subject: "Its about time !"
    date: 2007-01-28
    body: "Boy, I have mixed feelings about the KDevelop team and KDevelop.\n\nI tried KDevelop for a project a couple years ago and it was very buggy.  Support and interest and dealing with these issues was non existant.  I wasted a ton of time with KDevelop !\n\nShortly thereafter I moved to Eclipse and I haven't looked back.  The only thing that Eclipse doesn't do for me that KDevelop does is visual Qt development, but that is a pretty major thing !\n\nI don't know what to think about this release.  I don't doubt they fixed 500 bugs because there was a ton of them !\n\nI'll have a look at the new release of KDevelop, but its going to have to really wow me to earn back my trust.  "
    author: "me"
  - subject: "Re: Its about time !"
    date: 2007-01-28
    body: "Well.. since you no doubt made bug reports for the issues you found (right?), it should be trivial for you to find out if the problems are fixed or not..."
    author: "teatime"
  - subject: "Re: Its about time !"
    date: 2007-01-28
    body: "I also got frustrated with KDevelop and tried eclipse (mainly for the code completion). However it was unbelievably slow! Also KDevelop 3.4 is significantly less buggy than previous releases (although a few new ones seemed to have creeped in).\n\nOn the plus side, the code formatter is a lot more useful, and the UI is a whole lot cleaner."
    author: "Tim"
  - subject: "Re: Its about time !"
    date: 2007-01-28
    body: ">a few new ones seemed to have creeped in\nWe'll have 3.4.1 to tackle those :)\n\nAs for code completion, as long as you use code completion databases only for libraries you use, it's amazingly fast. I prefer to have kdelibs, qt and libstdc++ completion only and experience no slowdowns.\n\nAs about completion quality, David Nolden put a lot of effort to make it complete every time and everywhere. So I highly recommend 3.4 as it is certainly better.\n"
    author: "Alexander Dymo"
  - subject: "No autocompletion?"
    date: 2007-01-30
    body: "I just installed new KDevelop 3.4 from Debian unstable. It autocompletes nothing.\n\nE.g.:\n\nMyClass o;\no.(nothing happens, Ctrl-Space works)\n\ncout.(nothing happens)\n\nstd::(nothing happens)\n\nstd::vector<int> v;\nv.(nothing happens)\n\n\nWhat's wrong? (I have CTags installed)\n\n"
    author: "zack"
  - subject: "Re: No autocompletion?"
    date: 2007-01-30
    body: "Make sure you created code completion databases as described in http://www.kdevelop.org/mediawiki/index.php/FAQ"
    author: "Alexander Dymo"
  - subject: "Re: No autocompletion?"
    date: 2007-01-30
    body: "Thanks. I had to manually create/add database from /usr/include/C++ to have completion for STL.\n\ncout.(still nothing)\n\nAnyway, thanks for this vast improvement over previous versions. However, in MSVC I do not need to maualy create any databases - it is being created automaticaly according to what headers are included in the project. Is it so difficult to parse the project and create needed databases automatically?\n\nAnd, IMO standard C/C++ library should be autocompleted by default."
    author: "zack"
  - subject: "Re: No autocompletion?"
    date: 2007-01-30
    body: "There's an option to parse the included headers for autocompletion, but that feature is very time and memory consuming in KDevelop3.\n\nFor KDevelop4 this is a major point that we want to tackle.\n\nLast but not least: Adding a database for the standard C++ lib is a bit of a problem, because its headers might lie anywhere on the system, for example here it is /usr/include/c++/<gcc-version>, on gentoo its somewhere deep under /usr/lib/"
    author: "Andreas Pakulat"
  - subject: "Re: No autocompletion?"
    date: 2007-03-24
    body: ">its headers might lie anywhere on the system\nIf the compiler can get the information about where to get the headers, then it should be no problem for the IDE as well."
    author: "Dom"
  - subject: "Re: No autocompletion?"
    date: 2007-03-31
    body: "It's necessary to close and reopen the file after auto-completion was enabled to make it trigger automatically. This is a bug that is fixed in svn."
    author: "David Nolden"
  - subject: "What about Mono and C# support?"
    date: 2007-01-29
    body: "Today, I can use MonoDevelop for Mono developing, but it is not good as KDevelop. "
    author: "Aceler"
---
<a href="http://www.kdevelop.org/index.html?filename=3.4/announce-kdevelop-3.4.html">KDevelop 3.4 has been released</a>, bringing many new features to KDE's Integrated Development Environment.  The first major release in over a year <a href="http://bugs.kde.org/buglist.cgi?product=kdevelop&amp;resolution=FIXED&amp;resolution=WORKSFORME&amp;bugidtype=include&amp;chfield=resolution&amp;chfieldfrom=2005-10-21&amp;chfieldto=2007-01-25&amp;order=Bug+Number&amp;cmdtype=doit">closes more than 500 bugs</a>.  There is an <a href="http://www.kdevelop.org/index.html?filename=3.4/features.html">impressive list of additional features</a> including improved Qt 4 support, new debugging abilities, more attractive default user interface layout and improvements for C++, Ruby and PHP support.  Packages are available for <a href="http://kubuntu.org/announcements/kde-356.php">Kubuntu</a> and <a href="http://software.opensuse.org/download/KDE:/Backports/">openSUSE</a> with unofficial builds for several others on the <a href="http://www.kdevelop.org/index.html?filename=3.4/download.html">download page</a>.  <strong>Update:</strong> The developers have put together <a href="http://www.kdevelop.org/3.4/showcase.pdf">a slideshow to showcase the new features</a>.









<!--break-->
