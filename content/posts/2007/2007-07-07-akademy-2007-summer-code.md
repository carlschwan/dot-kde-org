---
title: "aKademy 2007: Summer of Code"
date:    2007-07-07
authors:
  - "jpoortvliet"
slug:    akademy-2007-summer-code
comments:
  - subject: "Summer of Code"
    date: 2007-07-07
    body: "I'd like to thank Emanuele and Jos for this article. It has been a pleasure to work with the students and mentors so far. And I've been dutifully impressed by the presence of so many students at aKademy.\n\nThe two most important subjects we discussed at the BoF, in my opinion, were the promotion and the \"keeping students around\" ones. Though we also agreed that KDE is doing a good job promoting, because we are getting lots of submissions and high quality ones at that. We thought of preparing some material for universities, like an A4 folder and a presentation of \"KDE for Students\".\n\nAs for keeping students around, the best idea we got was to bring them \"into the fold\" quite early on, involving them with the KDE community and other projects. Bringing them to aKademy this year will hopefully help to motivate this year's students to continue hacking."
    author: "Thiago Macieira"
  - subject: "DANNY RULES"
    date: 2007-07-07
    body: "Wow, and again Danny takes up more work. That guy is a machine... Yeah, this is news for me as well - Danny often does heavy editing on articles. I'd say he should always be credited for each article which is put online by him, due to the sheer amount of work he does on layout, wording, spellcheck and of course handling the horrible DOT software."
    author: "jospoortvliet"
  - subject: "KDE usage at Universities"
    date: 2007-07-08
    body: "According to the Thin-client talk at akademy KDE lacks some must-haves regarding large, centrally administred installations, whereas Gnome (Sabayone) catches up.\n\nSo in order to attract more students to KDE it might be helpful to make KDE the first choice for large installations, such as Universities, and thereby attract new users/students automatically since more people would use KDE more often."
    author: "Sven"
  - subject: "Re: KDE usage at Universities"
    date: 2007-07-08
    body: "Well, about that, at college, we  have 3 rooms of 15 to 20 light clients connected to a server runing suse 9. We use them for our C and Java practice, with Kate or Nedit and Konsole or Other terminal. Konqueror is also available, but thats about all, with of course gcc and javac.\n\nSomethin strange is that if you load a KDE session, it may become slow as hell, but if you are in a MWM (I think) it will just be fast as it can be, even when the people next to you are wailting 10 seconde to see on screen what they just entered with keyboard.\n\nIt seems for this KDE use much more network than MWM. Well, I mean when running exactly the same apps, namely Kate, Konsole and Konqui. Nothing more, but the extra KDE descktop things. Kicker ? HAL ? DCOP ?\nI don't know, but there is obviously something in KDE witch use much of the network compared to a simple WM."
    author: "kollum"
---
On Tuesday, the Summer of Code BoF was held at <a href="http://akademy2007.kde.org/">aKademy 2007</a>. I conducted an interview with Emanuele Tamponi about the session and his first experiences of aKademy. Please read on for the article.

<!--break-->
<p>Emanuele Tamponi revealed many details about the meeting, as I had been unable to attend. Emanuele is from Italy, and was also involved in last year's Summer of Code (SoC). He got involved when he was asking for a Krita feature on IRC, and Bart Coppens suggested that he write it himself through a SoC project. He applied and was accepted, and wrote the feature.</p>

<p>He enjoyed the work and liked the community so he applied for a second SoC this year, and got accepted again. This is his first aKademy, and he has very much enjoyed the experience so far.</p>

<h3>The BoF</h3>

<p>The Summer of Code BoF, led by <a href="http://behindkde.org/people/thiago/">Thiago Macieira</a>, started with a question about the general status. Thiago responded that there were the expected delays, mostly due to many university exams, which are still ongoing in June in many locales. Google knows this, and the students expect to be able to catch up later on. Then Thiago asked how the students liked the project until now. Some students expressed concerns regarding the amount of communication with their mentors. One student from India has a mentor who lives in Los Angeles, so it is easy to understand these difficulties.</p>

<p>Apart from these small issues, students enjoy working with the community. There was some talk about applying with more sub-projects next year, like a separate KOffice entry, but this was not determined to be a good idea. Under the umbrella of KDE, the projects have good infrastructure, and slots are appointed based on the quality of submissions, rather than a fixed quota per project. The example given was Marble, which was awarded 3 slots, and which would not have been the case had Marble been an independent entity.</p>

<p>Last year, a big problem was that the trunk of KDE SVN was closed during development, thus the code could not migrate to its native location easily. This has led to the 'loss' of some code, and other code still has to be ported to trunk. Luckily, this year the situation is much brighter, and most code being written is immediately committed to trunk.</p>

<p>Emanuele and others expressed how they were impressed with the good organization of the Summer of Code within KDE. KDE has clearly done a better job compared to last year, and having 5 co-ordinators is really paying dividends.</p>

<p>The next topic was spreading KDE in the coming year. Especially at universities, some more promotion would be a great idea. Setting up a 'Students Behind KDE', similar to the <a href="http://behindkde.org">People Behind KDE</a> was proposed, and accepted by Danny Allen, editor of the series. Look out for this series in the near future.</p>

<p>The final discussion was of how to best keep students interested in KDE after their project has concluded. This is an important goal for the whole Summer of Code project, and we can do better here. The conclusion was that the mentors should try to keep in touch with the students after the SoC has finished. This has the best chance of keeping them involved.</p>

<p>Emanuele was happy to note the rest of the KDE community was as friendly as the Krita community was, and he is glad to be here at aKademy. I think I speak for all of KDE when I say we are glad to have both him and the other SoC students here!</p>
