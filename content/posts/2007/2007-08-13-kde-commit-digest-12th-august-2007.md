---
title: "KDE Commit-Digest for 12th August 2007"
date:    2007-08-13
authors:
  - "dallen"
slug:    kde-commit-digest-12th-august-2007
comments:
  - subject: "kde4 environment"
    date: 2007-08-13
    body: "anyone already live working in a kde4 environment for developing kde4 ?\n\n"
    author: "lukilak@we"
  - subject: "Re: kde4 environment"
    date: 2007-08-13
    body: "I've compiled KDE4 from svn. But not for development.\n\nIt compiles, starts. Very very slow, many crashes, many things not working or missing.\n\nI would not use it for development, working in kde3 and running it on another xserver screen would be best I think. Even Alt-Tab sometimes doesn't work here, kicker is not responding, konqueror has problems, kmail doesn't work at all with my imap server, kmahjjong is working ;)\n\nPersonally I would say it is in Alpha-stage.\n\nBut as a demonstration of the technology it is very good, you can see the kde developers really implemented new and very good ideas. Looks _very_ promising.\n\n\nGreetings\nFelix"
    author: "Felix"
  - subject: "Re: kde4 environment"
    date: 2007-08-14
    body: "I'm really wondering why hardly anybody is digging this:\n\n     http://digg.com/linux_unix/Photoshop_can_t_do_it_CorelPainter_can_t_but_Linux_with_Krita_can"
    author: "digger"
  - subject: "Re: kde4 environment"
    date: 2007-08-15
    body: "Because the Digg crowd is a bunch of arrogant asshats who think they know everything.\n\nThe previous statement would be a flamebait if it had a slight smell of doubt, but it's very true. Just go to that URL and read the comments yourself.\n\nPS: Please don't spam the dot with Digg links, Digg is shit and anybody with IQ > 70 will tell you that.\n\nPPS: I never liked all caps text but.... KRITA IS AWESOME!"
    author: "Nonymous"
  - subject: "Re: kde4 environment"
    date: 2007-08-15
    body: "Your language was a little harsh, but, wow, I can't believe how stupid those comments were. That was a new low, even for digg.\n\nThose people clearly can't remember fingerpainting when they were kids."
    author: "Soap"
  - subject: "Re: kde4 environment"
    date: 2007-08-15
    body: "As much as I like Krita - I Don't think it's a new low for digg or that they have a low IQ or whatever. They are just stating the facts:\n1) Not many people need this. Krita is missing MUCH more important features right now. I can't even edit GIF images from my web pages. I know this is QT and Ubuntu and whatever but sorry folks: ImageMagick and GIMP open and close them flawlessly and at least they should have implemented sth. to circumvent this. Further flawless with the resizing dialog (no center option the last time I checked) etc.\n2) There actually IS other software that can do similar things.\n3) If Krita is a painter app like it's always said then you cant really compare it to Photoshop. If it is a photo editing app then it's missing a LOT of necessary features still.\n4) If you don't have this feature you still can make a palette of, say, green colors anyway. You can even create a palette in PaintShop or another app that supports this and use that in Photoshop.\n\nSorry folks, but the digg folks are right. Krita is a promosing app dont get me wrong and this is a nice feature and I don't have a problem reading a feature about this on the dot BUT don't let me get started on features Photoshop has and neither Krita nor Gimp has...\n"
    author: "Michael"
  - subject: "Re: kde4 environment"
    date: 2007-08-15
    body: "Well, the text on digg was a bit over the top really. I don't think any Krita developer would put it that way. I'm trying hard to avoid comparisons to Photoshop myself -- I'd rather compare Krita with Corel Painter.\n\nAnd yes, we are aware that there is other software that mixes colors in something resembling a painter's palette. There's just no available software that does it as well as Krita -- Bill Baxter never released his Dab, Stokes or Impasto (all of which include similar technology, but Krita has advanced on his work in some areas).\n\nAs for gif -- if you want to edit indexed color images, use a tool that's good for that kind of thing. Kolourpaint or the Gimp. It's not what Krita is for. It's out of scope for us. We do lab, xyz, ycbcr, hdr, lms, kubelka-munk -- others do 255 colors in a table. Others do indexed palettes of various colors to pick from -- we have that as well. But we've also got something that makes the image creation experience a lot more fun and that lets people bring real-world skills to the computer.\n\nBut it the end, I simply cannot understand why people actually want to spend their time being so negative every time a nice new thing comes along. It's not cool, it's not sophisticated, it's not productive... What do those digg people get out of it?"
    author: "Boudewijn Rempt"
  - subject: "Re: kde4 environment"
    date: 2007-08-15
    body: "@ all parent posts:\n\nThink about Digg and its typical readers/commenters all you want (and you are probably right), but...\n\nHowever, once you hit one of the technology front pages of the Digg service, chances are that you'll find a dozen or so new readers/users who never had heard of your application. Forget about the dumbasses in between who shout loudest about their negative feelings.\n\nAlso, remember: \"Bad publicity is better than no publicity at all.\" There's more than a grain of truth in that saying...."
    author: "digger"
  - subject: "Re: kde4 environment"
    date: 2007-08-15
    body: "Sorry, but no; putting it on Digg will only bring asshats and trolls. And they the worst kind of asshats and trolls, they stick around and command the developers as if they were paying for it. And the only way to shake 'em off is to scrap the project.\n\nThose who ever heard of KDE will notice Krita sooner or later. Those who are interested in it will join the mailing-list and/or the IRC channel. Let's not lure the asshats here.\n\n\n\nYou might think that I'm bitter about Digg, and you bet I'm. I've been there before and all the \"news\" I see is brought to you by \"Apple worshipers\", \"Racist fascists\", \"Self-righteous heretics\", \"Potheads, er.. I mean Pot-heads\" and more fun types."
    author: "Nonymous"
  - subject: "Re: kde4 environment"
    date: 2007-08-16
    body: ">As for gif -- if you want to edit indexed color images, use a tool that's\n>good for that kind of thing. Kolourpaint or the Gimp. It's not what Krita is\n>for. \n\nMmm, I dont find this very convincing, really. It's more a programmers' thinking than a users'. And thinking like a programmer is quite often\na very bad choice for making user interfaces and implementing functions.\nI want to edit images, with seperate layer, use filters on them, rotate them, paint on them. The only thing is - I want to save them as GIF later. Sometimes. Sometimes not. And now I need a seperate app for this. Well, whatever...\n\n"
    author: "Michael"
  - subject: "Re: kde4 environment"
    date: 2007-08-13
    body: "I use it daily since a few months, rebuilding with kdesvn-build every day. It is quite stable.\nI'm using it to develop ksirk and kgraphviewer. Currently, I use kate+konsole instead of kdevelop4 is not usable (in my last tries).\nWhat I miss is an automatic start of nepomuk services and strigi daemon. Also, I'd like to know how to ignore the menus and other desktop entries filled by my distribution (and to only use those given by KDE4). I'm never sure if a menu entry is a kde4 one or one from my distrib (mandriva).\n"
    author: "Kleag"
  - subject: "Re: kde4 environment"
    date: 2007-08-13
    body: "I think on techbase they have a trick for that. Was something about XDG_* enviroment variables. Have a search there.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: kde4 environment"
    date: 2007-08-14
    body: "find out where vandenoever.strigi.service has been installed. Then make sure you define XDG_DATA_DIRS such that XDG_DATA_DIRS/dbus-1/services/vandenoever.strigi.service points to this file.\n\nIn the standard KDE4 environment you can add\n export XDG_DATA_DIRS=$KDEDIR/share\nto your .bashrc file.\n\nIf you want to define multiple directories in XDG_DATA_DIRS, separate them with colons.\n"
    author: "Jos"
  - subject: "some comments..."
    date: 2007-08-13
    body: "I wonder why both Digikam and Gwenview are busily working on thumbnail view. Improving performance seems to be the game, as I see Aurelien complain about KPreviewJob being slow. How much duplication is going on here, and how much could be shared? This goes for all these graphics viewers out there, of course. They each have the same or comparable effects (some shared with kipi-plugins, yes) and a lot other things in common.\n\nShowfoto, gwenview - digikam, kphotoalbum. a lot of similarities. Now I'm OK with having many apps doing the same, I know you can't force developers to work together anyway. But the annoying thing is that each of em does SOME things right, and others wrong... So they should at least watch each others apps more often, and steal (or share) more ;-)\n\nI esp want to mention digikam here - I love the app for it's features, but I loathe it for it's complexity. I think all tabs in there should be removed. Yes, all of them. That would force a huge redesign of the UI, but I think it's possible - and it would make it finally a lot more usable for mere humans ;-)\n\nOf course, lot of work. Maybe some usability guy can/wants to help, I'm not much in the usability area...\n\nSecond thing.\n\nMarble.\n\nWonderfull work going on!!!\n\n\nLast, congrats to Daniel (Mosfet) for his first commit in a long time ;-)\nWelcome back!"
    author: "jospoortvliet"
  - subject: "Re: some comments..."
    date: 2007-08-13
    body: "BTW I choose to ignore the whole colormixing stuff, not because it's not really amazing, but because I assumed many others would have said stuff in the time I wrote this mail. Not true, so:\n\n- great work, emanuele, on that! It's way cool to have stuff our proprietary competition doesn't have ;-)"
    author: "jospoortvliet"
  - subject: "Re: some comments..."
    date: 2007-08-13
    body: "Now I'm busy completing myself anyway, so here's another thing:\n\nCongrats, Bram, on your top location :D You're consistent and enduring bug killing spree has put you second, third and fourth a few times. Now it's your turn to shine on top :D\n\n@ Danny: he has 24, yet his bar shows zero?!?\n\n\nBTW about the whole digikam thing, love the work on it, Gilles, don't mistake my criticism for negativism. I really do appreciate the hard work you put into it (and the statistics show that work). I just wish all the app would feel more natural..."
    author: "jospoortvliet"
  - subject: "Re: some comments..."
    date: 2007-08-13
    body: "Jos, I think you need a cat to talk to ;)"
    author: "Thomas Zander"
  - subject: "Re: some comments..."
    date: 2007-08-13
    body: "didn't you know that he broke up with his girl friend !! "
    author: "djouallah mimoune"
  - subject: "Re: some comments..."
    date: 2007-08-14
    body: "freedom (including personal one) is above all"
    author: "shaforostoff"
  - subject: "Re: some comments..."
    date: 2007-08-15
    body: "hehe, he is right, I DO need a cat. That's how bad it is - I HAVE one already...\n\naaah, it'll be allright, don't worry, be happy..."
    author: "jospoortvliet"
  - subject: "colour mixing in Krita"
    date: 2007-08-13
    body: "While this is not something I'm likely to use much (I don't even use Krita very much as digikam is so good for photo manipulation nowadays and that's the limit of my artisitc activities) the colour mixing in Krita looks very cool. I can see that this could be great for someone trying to do some freehand artwork with a graphics tablet for example - just imagin how natural it would be to mix your colours over on the edge of the tablet (in a manner of speaking) to get the one you want"
    author: "Simon"
  - subject: "Re: colour mixing in Krita"
    date: 2007-08-13
    body: "I am thrilled to see ground breaking technology going into Krita. While I am not an  artist this sort of innovation will add momentum to KDE becoming a true platform of choice for artists. Well done!\n\nI can see I am going to have do some finger painting!"
    author: "Kevin Colyer"
  - subject: "Re: colour mixing in Krita"
    date: 2007-08-13
    body: "Its really an amazing work by Emanuele Tamponi  :)\nI never knew that color mixing would be so complicated. Thanks for this wonderful technology and also the main seeder of this idea :)"
    author: "Gopala Krishna"
  - subject: "Seen before?"
    date: 2007-08-14
    body: "Awesome work! Doing the hard research work, digging through the literature and doing the maths is the right way to go. Thank you!\n\nHowever, I think I've seen mixing colors like this before, but now I'm not sure anymore. Some time ago I played around with a program called ArtRage which -- I think -- also managed to create the feel of real paint and color mixing. Apart from confirming whether or not this awesome color mixing feature in Krita is a very first, it might give some inspiration for further projects, as well."
    author: "Zeog"
  - subject: "Re: colour mixing in Krita"
    date: 2007-08-14
    body: "Yes, artrage already has something much,much similar to this."
    author: "pak"
  - subject: "Re: colour mixing in Krita"
    date: 2007-08-14
    body: "Actually, no, not that similar. If you mix blue and yellow in Artrage you get purple, not green. Our approach is much more realistic."
    author: "Boudewijn Rempt"
  - subject: "Marble"
    date: 2007-08-13
    body: "Sweet!  Marble now supports the International Date Line.  I can finally try and get myself a nice Brazillian girl :)"
    author: "John Tapsell"
  - subject: "Re: Marble"
    date: 2007-08-13
    body: "yeah, i am not alone, the idea pop up in my head, when i read this commit ;-)"
    author: "djouallah mimoune"
  - subject: "Re: Marble"
    date: 2007-08-19
    body: "If the local village idiots do not like your fat ass, why do you think Brazillians do? Or are you supporting human trafficking?\n\n"
    author: "JKdsjjaoO0Ol"
  - subject: "Blast from the past!"
    date: 2007-08-13
    body: "Daniel M. Duley committed changes in /trunk/kdesupport: \n\nAdding Blitz, my first commit in many years :)"
    author: "Troy Unrau"
  - subject: "Re: Blast from the past!"
    date: 2007-08-13
    body: "Ah! :) Great! Welcome back Mosfet!"
    author: "Richard Stallman"
  - subject: "Re: Blast from the past!"
    date: 2007-08-13
    body: "behindkde.org to interview him?"
    author: "shaforostoff"
  - subject: "Re: Blast from the past!"
    date: 2007-08-13
    body: "I'm not sure I understand what Blitz is about. Is this to provide a library for graphical effects a la Beryl to KDE?"
    author: "Richard Van Den Boom"
  - subject: "Re: Blast from the past!"
    date: 2007-08-13
    body: "it's replacing kstyle, which simply is a mess. They want to replace it with quasar in the future, but that's not ready yet (Zack is on it). So as a intermediate solution, Blitz might be put in kdesupport. But it's not fully decided, yet."
    author: "jospoortvliet"
  - subject: "Re: Blast from the past!"
    date: 2007-08-13
    body: "That should be kdefx, not kstyle. Apart from that, true."
    author: "Jakob Petsovits"
  - subject: "Some ideas..."
    date: 2007-08-13
    body: "Some ideas for painting (brushes, etc) can be taken from here: http://www.pixarra.com/product.html\n\nBut as it seems from the screenshots, it does color mixing extremly bad )."
    author: "CHX"
  - subject: "Re: Some ideas..."
    date: 2007-08-13
    body: "Corel Painter could also be a source of inspiration: \nhttp://www.corel.com/servlet/Satellite/us/en/Product/1166553885783\n\nI remember that it was what professional graphic artists I worked with used when they needed a realistic painting simulation. "
    author: "MORB"
  - subject: "Re: Some ideas..."
    date: 2007-08-13
    body: "Oh, we have looked at pretty much every existing paint application in existence. Twistedbrush, Deep Paint, Artrage, Corel Painter -- there are many more, on Windows, but also on Linux. Applications like Gogh and Mypaint are not bad at all. And I know that the Mypaint author reads the Krita mailing list."
    author: "Boudewijn Rempt"
  - subject: "Re: Some ideas..."
    date: 2007-08-14
    body: "Maybe this truly innovative piece of software can be another source of inspiration:\nhttp://macenstein.com/default/archives/759\n"
    author: "Anonymous"
  - subject: "Re: Some ideas..."
    date: 2007-08-14
    body: "That video is VERY funny!\n\nCan we make this line thinner? No.\n\nHaha."
    author: "anonymous coward"
  - subject: "beta?"
    date: 2007-08-13
    body: "I love the commit digests. This looks good, but how this should be done in a few months time I'll never know.. :("
    author: "anona"
  - subject: "Re: beta?"
    date: 2007-08-13
    body: "I must say I agree with you. From what I'm seeing being discussed on the mailinglists still, that seems way too deep and pervasive. But we'll see."
    author: "jospoortvliet"
  - subject: "krita"
    date: 2007-08-13
    body: "The color-mixing looks quite interesting and cool. I'll have to compile krita myself to see how it works with not-so-uniform-brushes. But I do wonder why the author starts to paint his pictures IN THE COLORMIXER, and not in the image itself. Doesn't that show there's a problem with the UI/usability? Then again, it probably was just an early demo...\n\nElse, I'm very happy to see mosfet back in kde-land. Welcome back!"
    author: "anonymous coward"
  - subject: "Re: krita"
    date: 2007-08-13
    body: "The color model that enables this kind of mixing isn't yet available for the main image -- not a big issue, just not done yet. It's on Emanuele's todo, together with complex loading (that is, more than one color in a brush) and other stuff."
    author: "Boudewijn Rempt"
  - subject: "Re: krita"
    date: 2007-08-13
    body: "That's really an impressive work. I guess, once this can be applied to the image, that watercolor and things like that are not far away. :-)\nWould it be possible to have this behaviour as a property of a layer? It would probably be interesting for artists to have a background more or less unmutable, and to have the possibility of drawing on a calc/layer with color mixing, taking into account the background colours, but not modifying the background layer itself."
    author: "Richard Van Den Boom"
  - subject: "Re: krita"
    date: 2007-08-14
    body: "If the amount of white in the brush could be changed dynamically (like with pressure on a drawing pad)\nit would simulate water.\nYou could do a \"watercolour\" right there in the mixer."
    author: "reihal"
  - subject: "Re: krita"
    date: 2007-08-14
    body: "Well, watercolor is a bit more complex, since there is diffusion (not sure it's the english term, maybe more scattering) going on even after you actually painted, for a certain amount of time depending on the amount of water and of the type of paper. But having correct color mixing is probably a prerequisite, I suppose.\n"
    author: "Richard Van Den Boom"
  - subject: "Re: krita"
    date: 2007-08-14
    body: "That's right. Well, it's possible to have watercolor simulation without k-m color mixing, like we had in Krita 1.6, but with the right color mixing and hopefully a better approach to the physics filters it'll be much better in 2.0."
    author: "Boudewijn Rempt"
  - subject: "Re: krita"
    date: 2007-08-14
    body: "Diffusion is the right term."
    author: "Soap"
  - subject: "Re: krita"
    date: 2007-08-13
    body: "Could this be used to get a realistic silver/gold/etc  color?  Silver looks silver because it doesn't reflect light uniformally in all directions."
    author: "John Tapsell"
  - subject: "Re: krita"
    date: 2007-08-13
    body: "That's what Kubelka and Munk were originally interested in: calculating the reflectivity of metal flakes in a suspension. In a funny way it turned out to work for non-metallic pigments, too.\n\nI don't know of any work on simulating metallic colors on a computer screen using K-M, though. It should be an interesting research area. People have done a Phd on similar stuff."
    author: "Boudewijn Rempt"
  - subject: "Yet another very old bug smashed"
    date: 2007-08-13
    body: "Horray for solving this bug/feature:\nBruno Virlet committed changes in /trunk/KDE/kdepim/korganizer: \n  Possibility to add timelines for different timezones. \n  Bug 18726: korganizer multiple timelines for timezone differences \n\nIt means another very old bug (around 2400 days) has been solved.\nThe previous one that was about this old, had an age of (only ;) ) 2000 days,\nand was solved by Thomas Zander.\n"
    author: "Richard"
  - subject: "Any news about Quanta?"
    date: 2007-08-14
    body: "I haven't heard anything about Quanta (and Eric) for a very long time. What can we expect from it for KDE 4.x. Thanks!!"
    author: "LB"
  - subject: "Re: Any news about Quanta?"
    date: 2007-08-14
    body: "I second that. I hope a less clutered interface and perhaps integrated PHP debugging :D"
    author: "Mariano"
  - subject: "LinuxMCE Release Plans"
    date: 2007-08-14
    body: "hello!\n\ndoes anybody of you know about the release plans of LinuxMCE for KDE4? Wasn't it the plan at the beginning to deliver it LinuxMCE with KDE4.0? I hope someone could answer this question and the following one ^^:\n\nwill LinuxMCE use plasma? In my opinion this would be a really good idea and it would make the interface look more united into kde. At the moment the latest version (0704) isn't really stable and fast (and i've got a dual core cpu and 2gb ram).. so why is it so slow? (i've got the kubuntu+linuxmce dvd from the servers)\n\nhope you've got some answers for me\n\nBernhard"
    author: "LordBernhard"
  - subject: "Re: LinuxMCE Release Plans"
    date: 2007-08-14
    body: "I think you will hear more about this really soon..."
    author: "thom"
  - subject: "Re: LinuxMCE Release Plans"
    date: 2007-08-14
    body: "thx for the answer.. i'm dieing waiting for some news ^^"
    author: "LordBernhard"
  - subject: "Re: LinuxMCE Release Plans"
    date: 2007-08-14
    body: "Yeap, I believe Aseigo alluded to an upcoming announcement concerning LinuxMCE in a recent blog post of his.  Just stay tuned: I'm sure there'll be interesting news soon.\n"
    author: "dario"
  - subject: "Re: LinuxMCE Release Plans"
    date: 2007-08-14
    body: "yeah.. i've also read it... because of these announcements i can't wait ^^ always this little word... soon.. brr..\nwell.. to all guys who develop or review or anything else on kde: take the time you need and don't hear to me and the other guys who can't await anything ^^"
    author: "LordBernhard"
  - subject: "Re: LinuxMCE Release Plans"
    date: 2007-08-14
    body: "I'd like also to know if it's gonna using still vdr as tv backend or if\nit will have a standalone method (I hope as customizable as the one in freevo)\nto look and record TV shows. Not all of us are interested in using our MCE\nas a live-vdr thus recording continuously to hard-disk."
    author: "pak"
  - subject: "Re: LinuxMCE Release Plans"
    date: 2007-08-14
    body: "yes, i'm working on a story for theDot here. we got a submission but it was a bit on the short and boring side. unfortunately it arrived just as my semi-vacation was starting and then i ended up talking with the linuxMCE people which only brought in more information for me to assimilate =) but it'll be here.\n\nand no, the plan never was to bring it in for 4.0, but the work it in post-4.0 (whether that means an interim linuxMCE/KDE release between 4.0 and 4.1 or just releasing with 4.1 proper is yet to be confirmed). there is a prototype version available right now based on kde3, which is what the upcoming article will cover."
    author: "Aaron J. Seigo"
  - subject: "Thanks!"
    date: 2007-08-14
    body: "I literally wait and very passionately read commit digests! Keep the great work up!"
    author: "backtick"
  - subject: "Perceived konqueror speed (double-click on folder)"
    date: 2007-08-14
    body: "I've noticed something minor yesterday.  On the same machine, when you double-click on a desktop folder on Windows the explorer opens _instantly_.  When you try the same on KDE's desktop, even though I have set konqueror to be preloaded (and checked it shows on 'ps' listing and it shows on kdcop too), it doesn't feel instant.  It feels like it needs 0.4-0.7 seconds to come up.\n\nWhy is that?  No further optimization possible?\n"
    author: "Alex Argiropoulos"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-14
    body: "yeah.. i've got the same feeling about the performance of konqueror (maybe also dolphin.. can't remember atm ^^)"
    author: "LordBernhard"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-14
    body: "i'm experiencing this with all apps in linux.\n\nmaybe new CFS scheduler will change the situation?"
    author: "shaforostoff"
  - subject: "Re: Perceived konqueror speed (double-click on fol"
    date: 2007-08-14
    body: "Well, I don't know how preloading works with Konq, but I assume we can probably preload it to the point where it's just a matter of calling .show()..."
    author: "Troy Unrau"
  - subject: "Re: Perceived konqueror speed (double-click on fol"
    date: 2007-08-15
    body: "Yes, but AFAIK konqueror can only go as fast as to force some instance to create another window. And it takes some time.\n\nI have hacked some time ago something faster - spawning hidden instances of konqueror with prepared windows and just telling it to show() using DCOP.\n\nThe code (quick and dirty) is here:\nhttp://ola-os.com/inne/konqffm/konqffm-0.1.tgz\n\nUsage:\n- apply patch to konqueror, recompile, install\n- Start konqffm.py - it stays in system tray and manages hidden instances.\n- Install desktop files and in directory properties move \"Konqueror FFM\" to top, so that opening directories is handled by FFM code.\n\nDesktop files provide fallback to standard kfmclient if konqffm.py is not running."
    author: "Krzysztof Lichota"
  - subject: "Re: Perceived konqueror speed (double-click on fol"
    date: 2007-08-15
    body: "I don't think it's a matter of optimization.\n\nI'm currently on Windows, and each time I open up an Explorer window it instantly shows *without* contents (just the interface parts). It takes a bit less than a second to show the contents of the folder.\n\nShowing the UI right away is what makes Explorer feel so 'fast'."
    author: "Someone"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-15
    body: "It's a kernel issue. You not only need to preload konqueror, you also need to pin it in memory. The linux kernel doesn't preload pages the user might often need access to fast, and it doesn't provide hinting mechanisms for the desktop to suggest such things. The Linux kernel really isn't optimized nor designed for desktop usage."
    author: "Allan Sandfeld"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-15
    body: "sorry, but thats just spreading FUD from several kernel threads we had recently.  There is no truth in the above.  Simple test;  try the same on a machine with nothing in swap.\n\nI suggest profiling the stuff to see where the real time comes from and you will notice that a large chunk of waiting time is in X11.  Well, we know that X11 is a bit behind in advancing the state of the art ;)\nWe'll get there."
    author: "Thomas Zander"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-15
    body: "hm.. i don't know if it's just me but imho X11 seems to be the most unstable part of linux (distributions). at least it needs some improvements (hotplugging of devices (i know xrandr 1.2)) easier configuration (without xorg.conf) and improvements on switching between sessions also needs to be improved so that it doesn't flicker when xorg kills itself and starts itself and so on (4 times on my ati system :-( (isn't there a project which implements grafics support into the kernel to improves this situation).\n\nthis was just my subjective view of the things and i would appreciate if someone who really knows this whole stuff would correct me if i'm wrong."
    author: "LordBernhard"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-15
    body: "The project xorg is the better version of xfree which had some bad management and eventually died (as far as free software can die).  The result is that only some time ago (2 years?) xorg got new blood and started becoming alive.\n\nIn other words; its true that a lot of work needs to go into that code base. And if you are a developer you should consider joining them. For the benefit of all free desktops :)"
    author: "Thomas Zander"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-16
    body: "ok.. thx for that clarification. yes i'm a developer but sadly i haven't got time for these things atm :-( i need to work on 2 other projects atm and personally i would like it more to work on KDE4 ^^ but we'll see... maybe i've got time and skill enough to work on both *hope so* ^^\n\nfriendly greetings\n\nBernhard"
    author: "LordBernhard"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-16
    body: "I am not only talking about swap, I am also talking about buffers and cache, the whole interconnected shebang of VM. Konqueror loads various plugins and scans sycoca database. These are files that could be speculatively preloaded into cache, and are in all desktop versions of Windows (since W2K). Konqueror could be rewritten to not do this things and X.org could be rewritten to require fewer context switches, but in all cases that is just working around deficiencies in the kernel."
    author: "Allan Sandfeld"
  - subject: "Re: Perceived konqueror speed (double-click on fol"
    date: 2007-08-16
    body: "You know, when Windows desktop dies, the way I normally restart it, is getting the task manager and from the File menu choose 'New Task (Run...)' and type 'explorer'.  So it _seems_ like their desktop and explorer is actually the same thing.\n\nOn the other hand, we have two separate applications kdesktop and konqueror.\n\nI am not saying (in fact I can't say that due to lack of knowledge) that it's not a kernel deficiency, but it might be (also) a design issue.\n"
    author: "Alex Argiropoulos"
  - subject: "Re: Perceived konqueror speed (double-click on folder)"
    date: 2007-08-16
    body: "Yeah, I heard someone else push those patches based on the logic that in one usecase it makes a difference to get things from disk into mem before the user realizes he needs it.\nUnfortunately this conceptually can't scale.  You can't optimize for one usecase and not make a lot of other usecases get worse.  And that is why it didn't get into the kernel, not because someone thought the desktop is not cool or whatever.\nBad ideas die, fact of open source life ;)"
    author: "Thomas Zander"
  - subject: "NMM not in KDE 4.0 :-("
    date: 2007-08-14
    body: "its a pitty :\n\nhttp://www.golem.de/0708/54109.html\n\nsophisticated sound technology , cross plattform , ... :/ and not in kde 4.0"
    author: "getit"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-14
    body: "what OS includes NMM by default? and given the design of phonon, all it would take is for some even mildly motivated hacker to step up and complete the backend. that, really, is the problem here.\n\nsome people are sad that phonon-nmm is out in playground now, but that wasn't a centralized decision that was made. it was the direct result of virtually zero interest from *anyone* to work on it. which, to me, says volumes about the vitality of the nmm development community.\n\nyeah, it's a cool technology. but it takes more than cool technology. it also takes people doing that \"last mile\" of work to make sure it is available to people.\n\nhonestly, i can't really think of anything more useful the nmm development community could do in the effort to spread the usage of their technology than to get that phonon backend together. it's not a huge amount of work, particularly for someone already familiar with nmm, and would bring a large number of apps to nmm."
    author: "Aaron J. Seigo"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-14
    body: "I suspect it's also because aside from nebulous claims about it being cool and the integrated network being the future, NMM is doing a poor job of explaining why anyone should care about it.  \n\nI can't think of the last time I wanted to take advantage of or control \"remote multimedia devices or software components\".  Perhaps if I was building a dedicated multimedia machine, but for a normal desktop?  Am I missing something here?  Why should the average user care about NMM?"
    author: "Leo S"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-15
    body: "well, the average user shouldn't, because thats the job of the developers. nmm is about the \"future of entertainment\" (or electronics in general, everything is going to be connected somehow...). look at stuff like http://www.apple.com/appletv/ .\n\nthe point is, things like that only will happen of the infrastructure is there. if kde ships with nmm it would be a huge success for nmm. i don't know why they (the nmm developers, that is) don't put more effort in this... getting nmm on computers today will get you cool applications for it much sooner.\n\nnmm, kde and linuxmce collaborating would be really interesting, at least to me."
    author: "ac"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-15
    body: "who cares about nmm... or anything else really. the point about phonon is just that it has to work and play all the media files. the backend is really unimportant, or does anybody really think sound sounds better coming from nmm than from xine?\n\npersonally i am really fond of vlc (because it works and they decided to go for qt4 ;) ) and hoped for the vlc-backend-soc-projet to be accepted, but really it doesnt matter, as long as the thing works!!!\n\n"
    author: "Hannes Hauswedell"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-15
    body: "the point is, phonon will only do what its backend is able of. so nmm should be important to you, because it does much more than xine... maybe you should look up what nmm is about."
    author: "ac"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-16
    body: "Hehe, with the integration of phonon in Qt4.4, maybe VLC team will separate their soft in one UI and a phonon backend.\nwould be so nice."
    author: "kollum"
  - subject: "Re: NMM community"
    date: 2007-08-15
    body: "To me it looks like the NMM people have done a lot(1) getting Phonon backends  together, way more than any comunity from the other backends. Most files are copyright Motama, the guys behind NMM.\n\nAnd with 1.0 released yesterday, a guess may be they have been prioritizing getting it ready before concentrating on the Phonon backend. Much like how the Plasma developers have been concentrating on building the libraries rather than writing plasmoids :-)\n\n\n(1) Based on copyright in the backend files, the history is not avaliable in  websvn due to the move to playground. "
    author: "Morty"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-16
    body: "NMM can be cool, but at the moment it's just a toy. The last I tried it it was so damn hard to compile it I doubt any distributions will include it.\n\nThe way forward right now is Xine. It has been around for years and it works, despite the GStreamer coming we've all heard about."
    author: "Segedunum"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-16
    body: "Not only that, but it seems fairly simple to program an interface to xine, seeing the number of apps using it and the apparent ease of creating xine's phonon backend.\nActually, in the current state of things, I don't see Gstreamer or NMM bringing anything on the table which would be that interesting for a simple desktop.\nIf they bring in multimedia framework, allowing edition as well as playing, i.e. creating of powerful editing tools as well as media players, that would be interesting. But Im' not sure Phonon is ready for this either."
    author: "Richard Van Den Boom"
  - subject: "Re: NMM not in KDE 4.0 :-("
    date: 2007-08-16
    body: "Answering to myself : well, NMM could bring portability, which would be cool.\nSeems a Phonon backend is included in 1.0.\nIt is indeed interesting and certainly powerful, but I think its use is more in companies and bigger structures than the regular joe."
    author: "Richard Van Den Boom"
  - subject: "Unable to compile."
    date: 2007-08-14
    body: "Normally, each 2 week, i post some screenshot on the dot (in the digest news), but this week, i am unable to do it. If someone find how to solve my problem, i will post some screenshot again. Everythings compil well unless 43% in kdelibs\n\n[ 43%] Built target kcm_phonon\n/usr/bin/make -f solid/solid/CMakeFiles/solid_static.dir/build.make solid/solid/CMakeFiles/solid_static.dir/depend\nmake[2]: Entering directory `/home/kde-devel/kde/build/kdelibs'\nsolid/solid/CMakeFiles/solid_static.dir/depend.make:1: warning: NUL character seen; rest of line ignored\nsolid/solid/CMakeFiles/solid_static.dir/depend.make:1: *** missing separator.  Stop.\nmake[2]: Leaving directory `/home/kde-devel/kde/build/kdelibs'\nmake[1]: *** [solid/solid/CMakeFiles/solid_static.dir/all] Error 2\nmake[1]: Leaving directory `/home/kde-devel/kde/build/kdelibs'\nmake: *** [all] Error 2\nmakeobj[0]: Leaving directory `/home/kde-devel/kde/build/kdelibs'\n\nThe make file is binary, i cant edit it. And my hard drive died, i copied the kde account to the new drive, this can be the problem, but i did a chmod 777 -r on it, so it is not a permission error."
    author: "Elv13"
  - subject: "Re: Unable to compile."
    date: 2007-08-15
    body: "\"The make file is binary\" ?\nIt should be a text file, something like:\n# CMAKE generated file: DO NOT EDIT!\n# Generated by \"Unix Makefiles\" Generator, CMake Version 2.4\n\nkdecore/CMakeFiles/kdecore.dir/fakes.o: /home/alex/src/KDE/kdelibs/kdecore/fakes.c\nkdecore/CMakeFiles/kdecore.dir/fakes.o: /home/alex/src/KDE/kdelibs/kdecore/kdecore_export.h\nkdecore/CMakeFiles/kdecore.dir/fakes.o: /opt/qt4/include/QtCore/qconfig.h\n\nIf it's not, it has been damaged due to your disk problems.\n\nAlex\n"
    author: "Alex"
---
In <a href="http://commit-digest.org/issues/2007-08-12/">this week's KDE Commit-Digest</a>: Significant progress in Colour Mixing in <a href="http://www.koffice.org/krita/">Krita</a>. A new, more usable sidebar for <a href="http://okular.org/">okular</a>. International Date Line support, and the merge of <a href="http://code.google.com/soc/kde/about.html">Summer of Code</a> work in <a href="http://edu.kde.org/marble/">Marble</a>. Solid is used for hardware detection in <a href="http://www.digikam.org/">Digikam</a>. KRunner uses <a href="http://strigi.sourceforge.net/">Strigi</a> for filename-based searches. The ability to switch cursor themes without restarting KDE. Timelines for multiple timezones, rich-text support and other journal improvements in <a href="http://korganizer.kde.org/">KOrganizer</a>. Support for storing bookmarks in <a href="http://pim.kde.org/akonadi/">Akonadi</a>. Initial porting of the Kollision game to QGraphicsView. Support for KNewStuff2 in <a href="http://edu.kde.org/kwordquiz/">KWordQuiz</a> and <a href="http://edu.kde.org/kvoctrain/">KVocTrain</a>; KNewStuff2 support (and the spectrum viewer) removed in <a href="http://edu.kde.org/kalzium/">Kalzium</a> until KDE 4.1. Initial import of <a href="http://sourceforge.net/projects/qimageblitz">Blitz</a>, an improved graphical effect and filter library for KDE 4.0.

<!--break-->
