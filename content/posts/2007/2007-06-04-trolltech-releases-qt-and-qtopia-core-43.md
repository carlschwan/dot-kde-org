---
title: "Trolltech Releases Qt and Qtopia Core 4.3"
date:    2007-06-04
authors:
  - "jriddell"
slug:    trolltech-releases-qt-and-qtopia-core-43
comments:
  - subject: "Qtopia Core 4.3?"
    date: 2007-06-04
    body: "Why do the downloads for Qtopia Core Open Source Edition only show 4.2.3?\n\n"
    author: "autocrat"
  - subject: "Re: Qtopia Core 4.3?"
    date: 2007-06-04
    body: "I guess they haven't updated their website, but you can still find it here:\n\nftp://ftp.trolltech.com/qt/source/"
    author: "AC"
  - subject: "qtwin free patch"
    date: 2007-06-04
    body: "anybody managed to get the kde-copy qtwin patch for vs express to work?\nIt applies but the makefiles are somewhat errorenous and will fail before linking the first DLL (qtcore4)\n :("
    author: "katakombi"
---
<a href="http://www.blogistan.co.uk/qt/2007/05/qt_v430_released.php">Qt Blog reports</a> that Trolltech has released version 4.3.0 of Qt, its cross-platform development platform, and Qtopia Core, its basis for embedded application development. Major new features include QtScript, an ECMAscript standard application scripting engine, replacing QSA; SSL support; improved OpenGL engine; more flexible main window architecture; ability to both render and generate SVG images and a new font system.  
More on the <a href="http://trolltech.com/products/qt/whatsnew/whatsnew-qt43">new features from Trolltech</a> with <a href="http://trolltech.com/developer/notes/changes/changes-4.3.0/">full changelog available</a>.  Get it from <a href="http://trolltech.com/developer/downloads/qt/">Qt downloads</a>.

<!--break-->
