---
title: "KDE-NL New Year's Meeting Coming Up"
date:    2007-01-15
authors:
  - "sk\u00fcgler"
slug:    kde-nl-new-years-meeting-coming
comments:
  - subject: "Content of the afternoon"
    date: 2007-01-15
    body: "There is actual content, too. I think Jos will be doing a KDE4 talk. I will naturally have KDE4 under Linux and BSD to show off and KDE3 on a Mac (because I haven't started compiling KDE4 there yet). Tom Albers will be showing as much of Mailody as he can -- slightly difficult because it's an online IMAP client, and there's no network connection in the Town Shed in Lent. Sebas and I will have something to say about Open Source research.\n\nThe attendees list is pretty short, still, so folk in NRW, .nl and .be are cordially invited for beer, pizza and KDE."
    author: "Adriaan de Groot"
  - subject: "Online, shmonline."
    date: 2007-01-15
    body: "> Tom Albers will be showing as much of Mailody as he can -- slightly difficult because it's an online IMAP client\n\nActually it's rather trivial to set up a local (to his laptop) IMAP server.  It takes all of about 10 minutes of configuration effort..."
    author: "Scott Wheeler"
---
On Saturday, 20th January, the traditional <a href="http://www.kde.nl">KDE-NL</a> New Year's Meeting</a> 
will be held in Lent near Nijmegen in the eastern part of the country.

KDE-NL invites contributors, interested users and other affiliated people for the day to get to know each other in person and 
discuss all kinds of KDE-related things. If you want to join, send an email to the Dutch <a href="https://mail.kde.org/mailman/listinfo/kde-i18n-nl">kde-i18n-nl mailing list</a>. If you want to give a talk, please note that in your e-mail.

More information on can be found on this <a href="http://www.kde.nl/agenda/2007-01-20-nieuwjaarsbijeenkomst">webpage</a>.



<!--break-->
