---
slug: dot-closed-long-live-kde-planet
date: 2024-09-25
title: The Dot is closed. Long live KDE Planet!
---

As KDE grows, so does the interest in each of its projects. Gathering all KDE news in one place no longer works. The volume of updates coming from the KDE community as a whole has become too large to cover in its entirety on the Dot. With this in mind, we are archiving [the Dot](https://dot.kde.org/), but keeping its content accessible for historical reasons.

The news coming out of the community was curated and edited for the Dot. The current rate of news items being published today would've not only made that impractical, but would have also led to things being unjustly left out, giving only a partial view of what was going on.

But we are not leaving you without your source of KDE news! We have figured out something better: we have worked with KDE webmasters to set up a blogging system for contributors. You can now access [Announcements](https://kde.org/announcements/), [Akademy](https://akademy.kde.org/news/), the [Association news](https://ev.kde.org/news/), and the [news from your favorite projects](https://blogs.kde.org/) directly, unfiltered, unedited, straight from the source.

Or... If you want to keep up with what is going on in ALL KDE projects and news on a daily (often hourly) basis, use [the Planet](https://planet.kde.org/)! Access it on the web or add an [RSS feed](https://planet.kde.org/index.xml) to your reader. You can also follow KDE news as it happens in our [Discuss forums](https://discuss.kde.org/c/community/blogs) and talk about it live with the rest of the community. You can even follow [@planet.kde.org@rss-parrot.net](https://rss-parrot.net/web/feeds/planet.kde.org) on Mastodon to stay up to date.

If you just want the highlights, check out our social media:

* [Mastodon](https://floss.social/@kde) (recommended)
* [BlueSky](https://bsky.app/profile/kde.org)
* [Threads](https://www.threads.net/@kdecommunity)
* [Facebook](https://www.facebook.com/kde)
* [Instagram](https://www.instagram.com/kdecommunity/)
* [LinkedIn](https://www.linkedin.com/company/kde)
