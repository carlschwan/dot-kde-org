---
title: "KDE at Asian FOSS conferences"
date:    2017-07-03
authors:
  - "jriddell"
slug:    kde-asian-foss-conferences
---
It feels great to say that KDE has active contributors across the globe. Two KDE contributors recently presented talks in Asia about their work and encouraged new contributors to join us and get started.

<h2>Hong Kong Open Source Conference 2017</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/heena-hkoc.jpg" width="299" height="400" /><br /><figcaption>Heena in Hong Kong</figcaption></figure> 

Heena Mahour presented a talk on ‘KDE : Journey of a SoK student to GCI organization administrator’ at Hong Kong Open Source Conference 2017.
 
HKOSCon is an open source event held in Hong Kong which aims to showcase and promote open source projects and activities happening across the globe.
 
Heena discussed her experience as a Season of KDE student which was her initial project and how she continued contributing to KDE afterwards. She also talked about recent trends in the community outreach program Season of KDE and her insight into the Plasma projects as an example of the vastness of KDE.

Heena also discussed the contributions made by KDE in Google Code-In, encouraging the students to get started with it. She provided information about GCompris and Pairs and a word cloud from Season of KDE Twitter posts and listed the contributions she had made so far. 

It was amazing to help the new contributors join the KDE community in a similar fashion as to how her mentors helper her.
 
<br clear="all" />
<h2>FOSSASIA 2017</h2>
 
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/anufossasia.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/anufossasia-wee.jpg" width="400" height="207" /></a><br /><figcaption>Anu in Singapore</figcaption></figure> 

Anu Mittal presented a talk on ‘K’oding with KDE at FOSSASIA 2017, Singapore.

At the FOSSASIA conference, many communities showcased their hardware, designs, graphics, and software. Anu talked about KDE, what it aims at, the various programs that KDE organizes to help budding developers, and how they are mentored. She walked through all the steps to start contributing in KDE by introducing the audience to KDE bug tracker, the IRC channels, various application domains, and the Season of KDE proposal format. 

She also shared her journey in KDE, talked briefly about her projects under Season of KDE and Google Summer of Code. The audience were really enthusiastic and curious to start contributing in KDE. Overall her experience working in KDE has been very enriching. She wished to share her experiences to help budding developers get started.
 
It was interesting to see an emerging interest to participate and contribute to open source. KDE is one of the projects students are keen to contribute to. We do hope we, together as a community, keep it blooming more. 
<!--break-->
