---
title: "Kirigami 2.1 is Out"
date:    2017-04-28
authors:
  - "Paul Brown"
slug:    kirigami-21-out
---
<figure><img src="/sites/dot.kde.org/files/gemini_top.jpg" /></figure> 

<a href="https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui">Kirigami UI</a> lets you easily design and create convergent apps that work on desktop and mobile environments. Platforms supported by Kirigami UI include Windows, Android, and Linux. Kirigami is especially indicated for KDE's Plasma Desktop and the upcoming Plasma Mobile platform, KDE's graphical environment for mobile devices.  Apps developed with Kirigami will probably also work on MacOS X and iOS with minimal tweaking, although these platforms are not officially supported yet.<!--break--> 

In fact, today's release has benefited from the feedback from the <a href="https://subsurface-divelog.org/">Subsurface Mobile</a> community -- the most prominent users of Kirigami outside of KDE at the moment. The Subsurface app, originally created by Linux creator Linus Torvalds, has successfully been ported to both iOS and MacOS X.

Several new components have been added to today's release:

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><img src="/sites/dot.kde.org/files/Discover_home.png" />
<br /><figcaption>The new Discover, KDE's graphical utility for searching and installing for apps, displays a customized ListView with a background picture.</figcaption></figure> 
<ul>
<li>ItemViewHeader is the standardized title for ListViews and can be customized with a background picture that will scroll with a nice parallax effect when the header adjusts. You can configure it to follow several different behaviours.</li>

<li>ApplicationItem is a root element for the QML application. You can use it in applications that are a hybrid of QWidgets and QML. The main view for these applications will be either QQuickView or a QQuickWidget.</li>

<li>PageRow is now a public element and you can use it directly in any application and in any context.</li>
</ul>

Developers have also engaged in a comprehensive bug stomping campaign, correcting among other things:

<ul>
<li>The bug that affected the behaviour of the central scrollable column view</li>

<li>Spacing and margins, improving the sizing for bottom action buttons and drawer handles</li>
</ul>

Other fixes specific to applications running on a desktop system include:

<ul>

<li>The Desktop mode bar has been rewritten, improving the behavior of the pages when loaded on a desktop app.</li>

<li>Improvements to icon management for icons coming from the system-wide icon theme when the application is running on a desktop system</li>

<li>Better mouse wheel support in the main item views</li>

<li>Bugfixes in the behaviour of the central scrollable column view</li>
</ul>

To find out more about this release and learn more about Kirigami in general, visit our KDE <a href="https://techbase.kde.org/Kirigami">techbase website</a>. If you would like to get started developing your apps with Kirigami, visit the <a href="https://api.kde.org/kirigami/html/index.html">Kirigami2 API overview</a>.

You can also talk directly to the developers and become a part of the future of desktop/mobile convergence by visiting our <a href="https://forum.kde.org/viewforum.php?f=293">forum</a>, joining in the conversation on the Plasma <a href="http://webchat.freenode.net/?channels=plasma">IRC channel</a>, or hanging out in our <a href="https://telegram.me/joinchat/BbOuVj6l7b5aZ_WbupyFFw">Telegram group</a>.
<!--break-->
