---
title: "Akademy Awards 2017"
date:    2017-07-24
authors:
  - "jriddell"
slug:    akademy-awards-2017
---
Every year at Akademy we celebrate some of our hardest working achievers in the community.  The prizes are selected and awarded by the previous year's winners.  The winners this year are:

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kai-uwe.jpg" width="199" height="243" /><br /><figcaption>Kai-Uwe</figcaption></figure> 
<h2>Application Award</h2>
Kai Uwe Broulik for their valuable work on Plasma.

<br clear="all" />
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/cornelius.jpg" width="199" height="252" /><br /><figcaption>Cornelius Schumacher</figcaption></figure> 
<h2>Non-Application Contribution Award</h2>
Cornelius Schumacher for their long term contributions to KDE.

<br clear="all" />
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/olaf_schmidt.jpg" width="101" height="136" />&nbsp;<img src="http://dot.kde.org/sites/dot.kde.org/files/konold.jpg" width="101" height="136" /><br /><figcaption>Olaf and Martin</figcaption></figure> 

<h2>Jury Award</h2>
Martin Konold & Olaf Schmidt-Wischhöfer for their work on the <a href="https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a>.
Honourable mentions also go to Lars Knoll and Tuukka Turunen for their work on the Qt side of the foundation which controls Qt's licensing.

<br clear="all" />
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/organisers.jpg" width="400" height="158" /><br /><figcaption>Thanking the Akademy organisers</figcaption></figure> 

The organising team were also given a certificate and thanked for their hard work.  The organisation has been led by Kenny Duffus who has helped for over a decade making the events appear to run smoothly.  The local team have worked hard this year led by Rubén Gómez and Ismael Olea to bring us a fabulous event.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/vienna.jpeg" width="400" height="300" /><br /><figcaption>Lukas Hetzenecker</figcaption></figure> 

Akademy continues for another four days with meetings, workshops and hacking from teams within KDE to discuss their work over the forthcoming year. 

One final announcement was made at the end of the talks. The location for next year's Akademy was announced to be in Vienna, Lukas Hetzenecker introduced what he assured us was a beautiful and welcoming city.

<!--break-->
