---
title: "Tuesday Akademy Wrapup Session"
date:    2017-07-25
authors:
  - "sealne"
slug:    tuesday-akademy-wrapup-session
---
The second day of Akademy BoFs, group sessions and hacking has just finished. There is a wrapup session at the end so that what happened in the different rooms can be shared with everyone including those not present.

<video width="750"  controls="1">
  <source src="https://cdn.files.kde.org/akademy/2017/bof_wrapups/tuesday.mp4" type="video/mp4">
Your browser does not support the video tag, download <a href="https://cdn.files.kde.org/akademy/2017/bof_wrapups/tuesday.mp4">Tuesday wrapup video</a>.
</video> 
<!--break-->
