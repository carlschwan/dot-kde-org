---
title: "Akademy Monday Wrapup Session Video"
date:    2017-07-24
authors:
  - "jriddell"
slug:    akademy-monday-wrapup-session
---
Akademy has had its first full day of BoFs, group sessions discussing our plans for the next year.  The wrapup session has just finished so watch the video to find out about what Plasma devs are working on, what tutorials happened and how we avoided a fist fight to the finish.

<video width="750"  controls="1">
  <source src="https://cdn.files.kde.org/akademy/2017/bof_wrapups/monday.mp4" type="video/mp4">
Your browser does not support the video tag, download <a href="https://cdn.files.kde.org/akademy/2017/bof_wrapups/monday.mp4">Monday wrapup video</a>.
</video> 
<!--break-->
