---
title: "Wednesday Akademy BoF wrapup"
date:    2017-07-26
authors:
  - "sealne"
slug:    wednesday-akademy-bof-wrapup
---
Wednesday is the third and for many people last day of BoFs, as people start to head off home. However hacking and some smaller meetings will happen tomorrow between those still here

<video width="750"  controls="1">
  <source src="https://cdn.files.kde.org/akademy/2017/bof_wrapups/wednesday.mp4" type="video/mp4">
Your browser does not support the video tag, download <a href="https://cdn.files.kde.org/akademy/2017/bof_wrapups/wednesday.mp4">Wednesday wrapup video</a>.
</video> 
<!--break-->
