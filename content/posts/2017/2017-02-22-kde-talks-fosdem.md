---
title: "KDE Talks at FOSDEM"
date:    2017-02-22
authors:
  - "jriddell"
slug:    kde-talks-fosdem
---
KDE had 4 talks at this year's FOSDEM conference.  Here's the recordings.

<h2>From Gtk to Qt: A Strange Journey, part 2</h2>
The continuation of the original talk from Dirk Hohndel and Linus Torvalds about the port of Subsurface from Gtk to Qt, now with mobile in mind.

<div class="video">
  <video preload="none" controls="controls" width="700">
    <source src="https://video.fosdem.org/2017/K.4.401/desktops_from_gtk_to_qt_subsurface_mobile.vp8.webm" type='video/webm; codecs="vp8, vorbis"' />
    <object type="application/x-shockwave-flash" data="https://releases.flowplayer.org/swf/flowplayer-3.2.15.swf">
      <param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.15.swf"/>
      <param name="allowfullscreen" value="true"/>
      <param name="flashvars" value="config={'clip': {'url': 'https://video.fosdem.org/2017/K.4.401/desktops_from_gtk_to_qt_subsurface_mobile.vp8.webm', 'autoPlay':false, 'autoBuffering':false}}"/>
      <p>Video tag not supported. Download the video <a href="https://video.fosdem.org/2017/K.4.401/desktops_from_gtk_to_qt_subsurface_mobile.vp8.webm">here</a>.</p>
    </object> 
  </video>
</div>

<h2>Kube</h2>
The next generation communication and collaboration client

<div class="video">
  <video preload="none" controls="controls" width="700">
    <source src="https://video.fosdem.org/2017/K.4.401/desktops_kube_next_generation_communication_collaboration_client.vp8.webm" type='video/webm; codecs="vp8, vorbis"' />
    <object type="application/x-shockwave-flash" data="https://releases.flowplayer.org/swf/flowplayer-3.2.15.swf">
      <param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.15.swf"/>
      <param name="allowfullscreen" value="true"/>
      <param name="flashvars" value="config={'clip': {'url': 'https://video.fosdem.org/2017/K.4.401/desktops_kube_next_generation_communication_collaboration_client.vp8.webm', 'autoPlay':false, 'autoBuffering':false}}"/>
      <p>Video tag not supported. Download the video <a href="https://video.fosdem.org/2017/K.4.401/desktops_kube_next_generation_communication_collaboration_client.vp8.webm">here</a>.</p>
    </object> 
  </video>
</div>

<h2>Bundling KDE</h2>
Where does KDE land in the Snap and Flatpak world?

<div class="video">
  <video preload="none" controls="controls" width="700">
    <source src="https://video.fosdem.org/2017/K.4.401/desktops_bundling_kde.vp8.webm" type='video/webm; codecs="vp8, vorbis"' />
    <object type="application/x-shockwave-flash" data="https://releases.flowplayer.org/swf/flowplayer-3.2.15.swf">
      <param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.15.swf"/>
      <param name="allowfullscreen" value="true"/>
      <param name="flashvars" value="config={'clip': {'url': 'https://video.fosdem.org/2017/K.4.401/desktops_bundling_kde.vp8.webm', 'autoPlay':false, 'autoBuffering':false}}"/>
      <p>Video tag not supported. Download the video <a href="https://video.fosdem.org/2017/K.4.401/desktops_bundling_kde.vp8.webm">here</a>.</p>
    </object> 
  </video>
</div>

<h2>KDE SlimBook Q&A</h2>

<div class="video">
  <video preload="none" controls="controls" width="700">
    <source src="https://video.fosdem.org/2017/K.4.401/desktops_kde_slimbook_q_a.vp8.webm" type='video/webm; codecs="vp8, vorbis"' />
    <object type="application/x-shockwave-flash" data="https://releases.flowplayer.org/swf/flowplayer-3.2.15.swf">
      <param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.15.swf"/>
      <param name="allowfullscreen" value="true"/>
      <param name="flashvars" value="config={'clip': {'url': 'https://video.fosdem.org/2017/K.4.401/desktops_kde_slimbook_q_a.vp8.webm', 'autoPlay':false, 'autoBuffering':false}}"/>
      <p>Video tag not supported. Download the video <a href="https://video.fosdem.org/2017/K.4.401/desktops_kde_slimbook_q_a.vp8.webm">here</a>.</p>
    </object> 
  </video>
</div>
<!--break-->
