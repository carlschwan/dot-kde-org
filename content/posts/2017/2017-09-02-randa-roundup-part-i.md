---
title: "Randa Roundup - Part I"
date:    2017-09-02
authors:
  - "Paul Brown"
slug:    randa-roundup-part-i
comments:
  - subject: "Bugfixes"
    date: 2017-09-06
    body: "<p>Will the new version have bugs fixed? I still experience, in several versions I have used, the bug which does not put the time cursor on the spot where I click on the timeline. Also the presented time on the preview screen does not change. This makes it impossible to work with the program cause I can never cut and paste at the right spot. Otherwise it's a great program which I like to use very much, but at the moment my projects have come to a halt.</p>"
    author: "DeMus"
---
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kdenlive_0.png"><img src="/sites/dot.kde.org/files/kdenlive_0.png" alt="" width="420" /></a><br />Kdenlive will be easier to use by the end of the sprint.</figure></p>

<p>Our intrepid developers are getting <a href="https://randa-meetings.ch/">ready to make their way to Randa</a>, the location for <a href"https://dot.kde.org/2017/08/08/randa-meetings-2017-its-all-about-accessibility">KDE's autumn coding sprint</a>, and we are gradually finding out what they will be doing up there in the Swiss mountains.</p>

<p><a href="http://linuxgrandma.blogspot.com.es/2017/08/why-you-care-about-accessibility-and.html">As Valorie said in a recent blog post</a>, accessibility is useful for everybody at some point or another. Clear, highly contrasted icons, easy to reach keyboard shortcuts, and scalable fonts are things we can all appreciate most of the time, whether we have any sort of physical disability or not.</p>

<p>With that in mind, Jean-Baptiste Mardelle will be working on <a href="http://kdenlive.org/">Kdenlive</a>, KDE's video editing software. He'll be reviewing the user interface; that is, the different panels, toolbars, etc., to make it easier to use for people who start editing for the first time. He'll also be working on packaging - creating AppImages and Flatpaks - so the latest versions of Kdenlive can be installed anywhere without having to worry about dependencies.</p>

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kube.png"><img src="/sites/dot.kde.org/files/kube.png" alt="" width="420" /></a><br />Kube is the new app for email, calendars, tasks, <br />notes and more.</figure></p>

<p>Marco Martin will be working on <a href="https://techbase.kde.org/Kirigami">Kirigami</a>, the framework that helps developers create apps seamlessly for desktops and mobile phones. His accessibility work will also extend to Plasma Mobile. If he has time, Marco says he would also like to work on <a href="https://kube.kde.org/">Kube</a>, a new groupware client to manage your emails, contacts, tasks, calendars and so on.</p>

<p>When asked what they would be working on, Christian Mollekopf and Michael Bohlender both chanted "<em>Kube, Kube, Kube!</em>". Although the work they will be carrying out is not specifically related to accessibility, one of the main aims of Kube is to offer a friendlier, more intuitive and more attractive user interface, making it easier to use than its alternatives.</p>

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/KMyMoney02.png"><img src="/sites/dot.kde.org/files/KMyMoney02.png" alt="" width="420" /></a><br />KMyMoney will get consistent keyboard functionailty.</figure></p>

<p>Another dynamic duo, Thomas Baumgart and Lukasz Wojnilowicz, will be working on <a href="https://kmymoney.org/">KMyMoney</a>, KDE's personal finance manager. T &amp; L will be working on making KMyMoney's keyboard functionality more consistent. They will also improve porting KMyMoney to Windows, creating an opportunity for a larger audience to use the app.</p>

<p>And then, of course, there will be the invaluable work of the organisers. Mario Fux is the main coordinator of the event, and he will be making sure everybody is fed and watered during the meetings. Simon Wächter and Fox will be helping developers by catering to their needs, plying them with Swiss chocolate, and dispensing hugs for moral support when their code misbehaves.</p>

<p>Essential stuff.</p>

<p>You too can help make Randa 2017 a success -- <a href="https://www.kde.org/fundraisers/randameetings2017/">contribute to our crowdfunding!</a> A few euros go a long way to making KDE better for everybody.</p>

<!--break-->