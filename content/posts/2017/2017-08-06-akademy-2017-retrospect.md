---
title: "Akademy 2017 in Retrospect"
date:    2017-08-06
authors:
  - "Paul Brown"
slug:    akademy-2017-retrospect
---
<style>
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 50px;
  line-height: 1;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #55f;
  border-right: 2px solid #55f;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\201C"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 90px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:5px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>

The <a href="https://akademy.kde.org/2017">2017 edition of Akademy</a> was held in Almería, Spain. Starting officially on the 22nd of July and ending on the 27th, <a href="https://conf.kde.org/en/akademy2017/public/schedule/2017-07-22">the weekend was dedicated to talks</a>, as is customary. The rest of the following week, from <a href="https://community.kde.org/Akademy/2017/AllBoF">Monday to Thursday</a>, was dedicated to workshops and <I>BoFs</I>  —  <I>Birds of a Feather</I> — sessions in which community members interested in the same things meet and work together.

This year's event attracted over 110 attendees. Attendees traveled mainly from Europe, but also from North and South America, and Asia. Over the weekend, visitors were able to attend over 40 different talks on all kinds of topics, ranging from developing applications for mobile phones to best ways for collaboration between communities.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://devel-home.kde.org/~duffus/akademy/2017/groupphoto/"><img src="/sites/dot.kde.org/files/akademy2017-groupphoto_1000.jpg" width="420" /></a><br /><figcaption>Attendees of the 2017 Akademy. Click on the image<br />to see a larger, interactive version with<br />participants' names.</figcaption></figure></p>

From Monday to Thursday, Akademy was dedicated to BoFs and workshops where a specific topic or area is focused on. For most participants, this part is a primary motivation for attending Akademy, since it gives them the chance to sit down with their colleagues in the flesh. They can discuss and code together without having to relay messages over email or IRC. Each day attendees met, discussed, and worked side by side, pushing KDE forward.

One of the hottest topics was <a href="https://www.kde.org/plasma-desktop">Plasma</a>. Plasma is KDE's graphical desktop and mobile environment. Dedicating a large chunk of the meetings to Plasma makes perfect sense. Although most KDE apps work on a wide range of platforms (including Windows, MacOS and Android), the first platform KDE developers would want to target is their own. With as much time dedicated to mobile frameworks, such as <a href="https://techbase.kde.org/Kirigami">Kirigami and <a href="https://halium.org/">Halium</a>, as to Plasma on desktop computers, it is clear the developers are very seriously committed to the effort of taking over smartphones and breaking the Android/iOS duopoly.

KDE developers know very well that a rich software catalogue is essential to attract end users, hence many of the talks and BoFs where dedicated to app development. There were slots on how to port applications to the upcoming <a href="https://wayland.freedesktop.org/">Wayland</a> display server protocol which, like winter, is definitely coming someday. Aleix Pol dedicated time to explaining how developers could package apps for <a href="http://flatpak.org/">Flatpak</a>, a universal packaging system for all GNU/Linux distributions. Scattered throughout the week were also several sessions and talks about <a href="http://doc.qt.io/qt-5/qmlapplications.html">QML</a>, <a href="https://doc.qt.io/qt-5/qt3d-index.html">Qt 3D</a>, and other KDE-related technologies.

As for the steps the applications should actually go through  —  from concept to working utility on the desktop or your phone's screen  —  during Akademy 2017 the community reached an agreement on the new <a href="https://community.kde.org/Policies/Application_Lifecycle">Applications Lifecycle Policy</a>. The overhaul of this policy had already been discussed at some length on KDE's <a href="https://mail.kde.org/mailman/listinfo/kde-community">Community</a> mailing list, but the conversation hadn't reached a satisfactory conclusion. However, a few hours of face-to-face negotiation led to an acceptable solution which <a href="https://dot.kde.org/2017/07/26/wednesday-akademy-bof-wrapup">Jonathan Riddell announced on Wednesday in the half-day wrap-up session</a>.

<video width="750"  controls="1">
  <source src="https://cdn.files.kde.org/akademy/2017/bof_wrapups/wednesday.mp4#t=131,196" type="video/mp4">
Your browser does not support the video tag, download <a href="https://cdn.files.kde.org/akademy/2017/bof_wrapups/wednesday.mp4">Wednesday wrap-up video</a>.
</video> 

According to Jonathan, the new policy... 

<blockquote>
"[D]efines how projects enter KDE, which is either through <a href="https://community.kde.org/Incubator">Incubator for projects</a> which started outside KDE, or just starting it in Playground. It defines the sort of things that get reviewed in <a href="https://community.kde.org/ReleasingSoftware#Sanity_Checklist ">KDEReview</a> and it explains how to choose where to put the application, (<em>Frameworks</em>, self released, <em>Applications</em>, <em>Plasma</em>) which in turn defines how and when it gets released. Finally, it defines options when a project is no longer useful: either ask the Gardening team to update it or move it to unmaintained."
</blockquote>

On the non-technical front, there were all-important discussion on how to make KDE technologies more accessible to end users, and how to make the community more open to potential contributors. Improving communication aimed at non-technical users, reaching out and cooperating with other communities, and implementing policies that promote inclusiveness were some of the areas participants pledged to work on.

This was another solid Akademy. Knowledge was shared, agreements reached, and code got written. Even though the KDE community discussed a wide variety of topics, there was clearly a common underlying theme of how members of KDE want to shape the world of tech to their <i>vision</i> — the vision of a world in which everyone has control over their digital life and enjoys freedom and privacy.

After this year's hot Andalusian sun, Akademy will be moving next year to the heart of Europe: Vienna. See you there in 2018!

<h2>About Akademy</h2>

<p>For most of the year, KDE — one of the largest free and open software communities in the world—works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2017">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.</p>

<p>For more information, please contact the <a href="https://akademy.kde.org/2017/contact">Akademy Team</a>.</p>

<!--break-->