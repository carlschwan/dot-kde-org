---
title: "Robert Kaye -- Music Buff, Entrepreneur, Akademy Keynote Speaker"
date:    2017-06-18
authors:
  - "Paul Brown"
slug:    robert-kaye-music-buff-entrepreneur-akademy-keynote-speaker
comments:
  - subject: "Typo in the first line."
    date: 2017-06-21
    body: "<p>Hi, Awesome blog post!&nbsp;</p><p>&nbsp;</p><p>Just a nitpick, but there is a typo in the second sentence, it should be `ListenBrainz` instead of `ListenBrain`.&nbsp;</p>"
    author: "Anonymous"
  - subject: "Best interview"
    date: 2017-06-22
    body: "This is the best interview I've read in quite a while. Also, Robert - awesome work ;-)"
    author: "jospoortvliet"
  - subject: "Typo"
    date: 2017-06-26
    body: "Thanks! Corrected."
    author: "Paul Brown"
  - subject: "Great"
    date: 2017-07-01
    body: "<p>Thanks for that interview, thoroughly enjoyed all of it.</p><p>ElectronicBrianz sounds awesome.</p>"
    author: "Julius"
  - subject: "Amazing interview!"
    date: 2017-07-05
    body: "<p>I just wanted to say thank you both for an amazing, fun interview!</p><p>I didn't know most of these projects, looking forward to know more about them :)</p><p>&nbsp;</p>"
    author: "ricardo.barberis"
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><img src="/sites/dot.kde.org/files/serious-profile_protrait.png" /><br /><figcaption>Robert Kaye, creator of MusicBrainz</figcaption></figure> 

<i>Robert Kaye is definitely a brainz-over-brawn kinda guy. As the creator of MusicBrainz, ListenBrainz and AcousticBrainz, all created and maintained under the MetaBrainz Foundation, he has pushed Free Software music cataloguing-tagging-classifying to the point it has more or less obliterated all the proprietary options.</i>

<i>In July he will be in Almería, delivering a keynote at the <a href="https://akademy.kde.org/2017">2017 Akademy</a> -- the yearly event of the KDE community. He kindly took some time out of packing for a quick trip to Thailand to talk with us about his *Brainz projects, how to combine altruism with filthy lucre, and a cake he once sent to Amazon.</i>

<span style="color:#55f;font-weight:bold">Robert Kaye:</span> Hola, ¿qué tal?

<span style="color:#f55;font-weight:bold">Paul Brown:</span> Hey! I got you!

<span style="color:#55f;font-weight:bold">Robert:</span> Indeed. :)

<span style="color:#f55;font-weight:bold">Paul:</span> Are you busy?

<span style="color:#55f;font-weight:bold">Robert:</span> I'm good enough, packing can wait. :)

<span style="color:#f55;font-weight:bold">Paul:</span> I'll try and be quick.

<span style="color:#55f;font-weight:bold">Robert:</span> No worries.

<span style="color:grey;font-style:italic">* Robert has vino in hand.</span>

<span style="color:#f55;font-weight:bold">Paul:</span> So you're going to be delivering the keynote at Akademy...

<span style="color:grey;font-style:italic">* Robert is honored.</span>

<span style="color:#f55;font-weight:bold">Paul:</span> Are you excited too? Have you ever done an Akademy keynote?

<span style="color:#55f;font-weight:bold">Robert:</span> Somewhat. I've got... three? Four trips before going to Almería. :)

<span style="color:#f55;font-weight:bold">Paul:</span> God!

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><img src="/sites/dot.kde.org/files/metabrainz.png" />
<br /><figcaption>MetaBrainz is the umbrella project under which all other *Brainz are built.</figcaption></figure>

<span style="color:#55f;font-weight:bold">Robert:</span> I've never done a keynote before. But I've done tons and tons of presentations and speeches, including to the EU, so this isn't something I'm going to get worked up about thankfully.

<span style="color:#f55;font-weight:bold">Paul:</span> I'm assuming you will be talking about <a href="https://metabrainz.org/">MetaBrainz</a>. Can you give us a quick summary of what MetaBrainz is and what you do there?

<span style="color:#55f;font-weight:bold">Robert:</span> Yes, OK. In 1997/8 in response to the CDDB database being taken private, I started the CD Index. You can see a copy of it in the Wayback Machine. It was a service to look up CDs and I had zero clues about how to do open source. Alan Cox showed up and told me that databases would never scale and that I should use DNS to do a CD lookup service. LOL. It was a mess of my own making and I kinda walked away from it until the .com crash.

Then in 2000, I sold my Honda roadster and decided to create <a href="https://musicbrainz.org/">MusicBrainz</a>. MusicBrainz is effectively a music encyclopedia. We know what artists exist, what they've released, when, where their Twitter profiles are, etc. We know track listings, acoustic fingerprints, CD IDs and tons more. In 2004 I finally figured out a business model for this and created the <a href="https://metabrainz.org/">MetaBrainz Foundation</a>, a California tax-exempt non-profit. It cannot be sold, to prevent another CDDB. For many years MusicBrainz was the only project. Then we added the <a href="https://coverartarchive.org/">Cover Art Archive</a> to collect music cover art. This is a joint project with the Internet Archive.

Then we added <a href="https://critiquebrainz.org/">CritiqueBrainz</a>, a place for people to write CC licensed music reviews. Unlike Wikipedia, ours are non-neutral POV reviews. It is okay for you to diss an album or a band, or to praise it.

<span style="color:#f55;font-weight:bold">Paul:</span> An opinionated musical Wikipedia. I already like it.

<span style="color:#55f;font-weight:bold">Robert:</span> Then we created <a href="https://acousticbrainz.org/">AcousticBrainz</a>, which is a machine learning/analysis system for figuring out what music sounds like. Then the community started <a href="https://bookbrainz.org/">BookBrainz</a>. And two years ago we started <a href="https://listenbrainz.org/">ListenBrainz</a>, which is an open source version of last.fm's audioscrobbler.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><img src="/sites/dot.kde.org/files/Musicbrainz.png" />
<br /><figcaption>MusicBrainz is a repository of music metadata widely used by commercial and non-commercial projects alike.</figcaption></figure> 

<span style="color:#f55;font-weight:bold">Paul:</span> Wait, let's backtrack a second. Can you explain AcousticBrainz a bit more? What do you mean when you say "<I>figure out what music sounds like</I>"?

<span style="color:#55f;font-weight:bold">Robert:</span> AcousticBrainz allows users to download a client to run on their local music collection. For each track it does a very detailed low-level analysis of the acoustics of the file. This result is uploaded to the server and the server then does machine learning on it to guess: Does it have vocals? Male of female? Beats per minute? Genre? All sorts of things and a lot of them need a lot of improvement still.

<span style="color:#f55;font-weight:bold">Paul:</span> Fascinating.

<span style="color:#55f;font-weight:bold">Robert:</span> Researchers provided all of the algorithms, being very proud and all: "<i>I've done X papers on this and it is the state of the art</i>". State of the art if you have 1,000 audio tracks, which is f**king useless to an open source person. We have three million tracks and we're not anywhere near critical mass. So, we're having to fix the work the researchers have done and then recalculate everything. We knew this would happen, so we engineered for it. We'll get it right before too long.

All of our projects are long-games. Start a project now and in five years it might be useful to someone. Emphasis on "might".

Then we have ListenBrainz. It collects the listening history of users. User X listened to track Y at time Z. This expresses the musical taste of one user. And with that we have all three elements that we've been seeking for over a decade: metadata (MusicBrainz), acoustic info (AcousticBrainz) and user profiles (ListenBrainz). The holy trinity as it were. You need all three in order to build a music recommendation engine.

The algorithms are not that hard. Having the underlying data is freakishly hard, unless you have piles of cash. Those piles of cash and therefore the engines exist at Google, Last.fm, Pandora, Spotify, et al. But not in open source.

<span style="color:#f55;font-weight:bold">Paul:</span> Don't you have piles of cash?

<span style="color:#55f;font-weight:bold">Robert:</span> Nope, no piles of cash. Piles of eager people, however! So, once we have these databases at maturity we'll create some recommendation engine. It will be bad. But then people will improve it and eventually a pile of engines will come from it. This has a significant chance of impacting the music world.

<span style="color:#f55;font-weight:bold">Paul:</span> You say that many of the things may be useful one day, but you also said MetaBrainz has a business model. What is it?

<span style="color:#55f;font-weight:bold">Robert:</span> The MetaBrainz business model started out with licensing data using the non-commercial licenses. Based on "<I>people pay for frequent and easy updates to the data</I>". That worked to get us to 250k/year.

<span style="color:#f55;font-weight:bold">Paul:</span> Licensing the data to...?

<span style="color:#55f;font-weight:bold">Robert:</span> The MusicBrainz core data. But there were a lot of people who didn't need the data on an hourly basis.

<span style="color:#f55;font-weight:bold">Paul:</span> Sorry. I mean *who* were you licensing to?

<span style="color:#55f;font-weight:bold">Robert:</span> It started with the BBC and Google. Today we have <a href="https://metabrainz.org/supporters">all these supporters</a>. Nearly all the large players in the field use our data nowadays. Or lie about using our data. :)

<span style="color:#f55;font-weight:bold">Paul:</span> Lie?

<span style="color:#55f;font-weight:bold">Robert:</span> I've spoken to loads of IT people at the major labels. They all use our data. If you speak to the execs, they will swear that they have never used our data.

<span style="color:#f55;font-weight:bold">Paul:</span> Ah. Hah hah. Sounds about right.

<span style="color:#55f;font-weight:bold">Robert:</span>Anyways, two years ago we moved to a supporter model. You may legally use our data for free, but morally you should financially support us. This works. 

<span style="color:#f55;font-weight:bold">Paul:</span> Really?

<span style="color:#55f;font-weight:bold">Robert:</span> We've always used what I call a "<I>drug dealer business model</I>". The data is free. Engineers download it and start using it. When they find it works and want to push it into a product they may do that without talking to us. Eventually we find them and knock on their door and ask for money.

<span style="color:#f55;font-weight:bold">Paul:</span> They pay you? And I thought the music industry was evil.

<span style="color:#55f;font-weight:bold">Robert:</span> This is the music *tech* companies. They know better.

Anyways...

Their bizdev types will ask: where else can we get this data for cheaper? The engineers look around for other options. Prices can range from 3x to 100x, depending on use, and the data is not nearly as good. So they sign up with us. This is not out of the kindness of their hearts.

<span style="color:#f55;font-weight:bold">Paul:</span> Makes more sense now.

<span style="color:#55f;font-weight:bold">Robert:</span> Have you heard the <a href="http://boingboing.net/2013/12/04/charity-sends-amazon-a-cake-ce.html">Amazon cake story</a>?

<span style="color:#f55;font-weight:bold">Paul:</span> The what now?

<span style="color:#55f;font-weight:bold">Robert:</span> Amazon was 3 years behind in paying us. I harangued them for months. Then I said: "<I>If you don't pay in 2 weeks, I am going to send you a cake.</I>"

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><img src="/sites/dot.kde.org/files/cake.jpg" />
<br /><figcaption>Amazon got cake to celebrate the third anniversary of an unpaid invoice.</figcaption></figure>

"<I>A cake?</I>"

"<I>Yes, a cake. One that says 'Congratulations on the 3rd anniversary'...</I>"

They panicked, but couldn't make it happen.

So I sent the cake, then silence for 3 days.

Then I got a call. Head of legal, head of music, head of AP, head of custodial, head of your momma. All in one room to talk to me. They rattled off what they owed us. It was correct. They sent a check.

Cake was sent on Tuesday, check in hand on Friday.

This was pivotal for me: recognizing that we can shame companies to do the right thing... Such as paying us because to switch off our data (drugs) is far worse than paying.

<a href="https://metabrainz.org/finances/">Last year we made $323k</a>, and this year should be much better. We have open finances and everything. People can track where money goes. We get very few questions about us being evil and such.

<span style="color:#f55;font-weight:bold">Paul:</span> How many people work with you at MetaBrainz, as in, are on the payroll?

<span style="color:#55f;font-weight:bold">Robert:</span> <a href="https://metabrainz.org/team">This is my team</a>. We have about 6 full-time equivalent positions. To add to that, we have a core of contributors: coders, docs, bugs, devops... Then a medium ring of hard-core editors. Nicolás Tamargo and one other guy have made over 1,000,000 edits to the database!

<span style="color:#f55;font-weight:bold">Paul:</span> How many regular volunteers then?

<span style="color:#55f;font-weight:bold">Robert:</span> 20k editors per year. Más o menos. And we have zero idea how many users. We literally cannot estimate it. 40M requests to our API per day. 400 replicated copies of our DB. VLC uses us and has the largest installation of MusicBrainz outside of MetaBrainz.

And we ship a virtual machine with all of MusicBrainz in it. People download that and hammer it with their personal queries. Google Assistant uses it, Alexa might as well, not sure. So, if you ask Google Assistant a music-related question, it is answered in part by our data. We've quietly become the music data backbone of the Internet and yet few people know about us.

<span style="color:#f55;font-weight:bold">Paul:</span> Don't you get lawyers calling you up saying you are infringing on someone's IP?

<span style="color:#55f;font-weight:bold">Robert:</span> Kinda. There are two types: 1) the spammers have found us and are hammering us with links to pirated content. We're working on fixing that. 2) Other lawyers will tell us to take content down, when we have ZERO content. They start being all arrogant. Some won't buzz off until I tell them to provide me with an actual link to illegal content on our site. And when they can't do it, they quietly go away.

The basic fact is this: we have the library card catalog, but not the library. We mostly only collect facts and facts are not copyrightable.

<span style="color:#f55;font-weight:bold">Paul:</span> What about the covers?

<span style="color:#55f;font-weight:bold">Robert:</span> That is where it gets tricky. We engineered it so that the covers never hit our servers and only go to the Internet Archive. The Archive is a library and therefore has certain protections. If someone objects to us having something, the archive takes it down.

<span style="color:#f55;font-weight:bold">Paul:</span> Have you had many objections?

<span style="color:#55f;font-weight:bold">Robert:</span> Not that many. Mostly for liner notes, not so much for covers. The rights for covers were never aggregated. If someone says they have rights for a collection, they are lying to you. It's a legal mess, plain and simple. All of our data is available under clear licenses, except for the CAA -- "<I>as is</I>"

<span style="color:#f55;font-weight:bold">Paul:</span> What do you mean by "rights for a collection"?

<span style="color:#55f;font-weight:bold">Robert:</span> Rights for a collection of cover art. The rights reside with the band. Or the friend of the band who designed the cover. Lawyers never saw any value in covers pre-Internet. So the recording deals never included the rights to the covers. Everyone uses them without permission

<span style="color:#f55;font-weight:bold">Paul:</span> I find that really surprising. So many iconic covers.

<span style="color:#55f;font-weight:bold">Robert:</span> It is obvious in the Internet age, less so before the Internet. The music industry is still quite uncomfortable with the net.

<span style="color:#f55;font-weight:bold">Paul:</span> Record labels always so foresightful.

<span style="color:#55f;font-weight:bold">Robert:</span> Exactly. Let's move away from labels and the industry.

Though, one thing tangentially, I envisioned X, Y, Z, uses for our data, but we made the data rigorous, well-connected and concise. Good database practices. And that is paying off in spades. The people who did not do that are finding that their data is no longer up to snuff for things like Google Assistant.

<span style="color:#f55;font-weight:bold">Paul:</span> FWIW, I had never heard of Gracenote until today. I had heard of MusicBrainz, though. A lot.

<span style="color:#55f;font-weight:bold">Robert:</span> Woo! I guess we're succeeding. :)

<span style="color:#f55;font-weight:bold">Paul:</span> Well, it is everywhere, right? 

<span style="color:#55f;font-weight:bold">Robert:</span> For a while it was even in Antarctica! A sysadmin down there was wondering where the precious bandwidth went during the winter. Everyone was tagging their music collection when bored. So he set up a replica for the winter to save on bandwidth.

<span style="color:#f55;font-weight:bold">Paul:</span> Of course they were and of course he did.

<span style="color:#55f;font-weight:bold">Robert:</span> Follows, right? :)

<span style="color:#f55;font-weight:bold">Paul:</span> Apart from music, which you clearly care for A LOT, I heard you are an avid maker too.

<span style="color:#55f;font-weight:bold">Robert:</span> Yes. <a href="http://partyrobotics.com">Party Robotics</a> was a company I founded when I was still in California and we made the first affordable cocktail robots. But I also make blinky LED light installations. Right now I am working on a sleep debugger to try and improve my crapstastic sleep. 

I have a home maker space with an X-Carve, 3D printer, hardware soldering station and piles of parts and tools.

<span style="color:#f55;font-weight:bold">Paul:</span> Uh... How do flashing lights help with sleep?

<span style="color:#55f;font-weight:bold">Robert:</span> Pretty lights and sleep-debugging are separate projects.

<span style="color:#f55;font-weight:bold">Paul:</span> What's your platform of choice, Arduino?

<span style="color:#55f;font-weight:bold">Robert:</span> Arduino and increasingly Raspberry Pi. The Zero W is the holy grail, as far as I am concerned.

Oh! And another project I want: ElectronicsBrainz.

<span style="color:#f55;font-weight:bold">Paul:</span> This sounds fun already. Please tell.

<span style="color:#55f;font-weight:bold">Robert:</span> Info, schematics and footprints for electronic parts. The core libraries with KiCad are never enough. you need to hunt for them. Screw that. Upload to ElectronicBrainz, then, if you use a part, rate it, improve it. The good parts float to the top, the bad ones drop out. Integrate with Kicad and, bam! Makers can be much more useful. In fact, this open data paradigm and the associated business model is ripe for the world. There are data silos *everywhere*.

<span style="color:#f55;font-weight:bold">Paul:</span> I guess that once you have set up something like MusicBrainz, you start seeing all sorts of applications in other fields.

<span style="color:#55f;font-weight:bold">Robert:</span> Yes. Still, we can't do everything. The world will need more MetaBrainzies.

<span style="color:#f55;font-weight:bold">Paul:</span> Meanwhile, how can non-techies help with all these projects?

<span style="color:#55f;font-weight:bold">Robert:</span> Editing data/adding data, writing docs or managing bug reports as well. Clearly our base of editors is huge. It is a very transient community, except for the core.

Also, one thing that I want to mention in my keynote is blending volunteers and paid staff. We've been really lucky with that. The main reason for that is that we're open. We have nothing to hide. We're all working towards the same goals: making the projects better. And when you make a site that has 40M requests in a day, there are tasks that no one wants to do. They are not fun. Our paid staff work on all of those.

Volunteers do the things that are fun and can transition into paid staff -- that is how all of our paid staff became staff.

<span style="color:#f55;font-weight:bold">Paul:</span> This is really an incredible project.

<span style="color:#55f;font-weight:bold">Robert:</span> Thanks! Dogged determination for 17 years. It’s worth something.

<span style="color:#f55;font-weight:bold">Paul:</span> I look forward to your keynote. Thank you for your time.

<span style="color:#55f;font-weight:bold">Robert:</span> No problem.

<span style="color:#f55;font-weight:bold">Paul:</span> I'll let you get back to your packing.

<span style="color:#55f;font-weight:bold">Robert:</span> See you in Almería.

<i>Robert Kaye will deliver the <a href="https://conf.kde.org/en/akademy2017/public/events/385">opening keynote</a> at Akademy 2017</a> on the 22nd of July. If you would like to see him and talk to him live, <a href="https://events.kde.org/">register here</a>.</i>

<h2>About Akademy</h2>

<p>For most of the year, KDE—one of the largest free and open software communities in the world—works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2017">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities. Join us by <a href="https://events.kde.org/">registering for the 2017 edition of Akademy today</a>.</p>

<p>For more information, please contact the <a href="https://akademy.kde.org/2017/contact">Akademy Team</a>.</p>

<!--break-->