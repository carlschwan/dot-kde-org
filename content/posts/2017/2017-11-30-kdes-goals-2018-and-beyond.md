---
title: "KDE's Goals for 2018 and Beyond"
date:    2017-11-30
authors:
  - "Paul Brown"
slug:    kdes-goals-2018-and-beyond
---
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/road-368719_1280.jpg"><img src="/sites/dot.kde.org/files/road-368719_1280.jpg" alt="" width="300" /></a></figure></p>

<B>The KDE community has spoken and it has chosen the proposals which will define the general direction of the KDE project over the next three or four years.</B>

How does the KDE community decide where it wants to take the project? Well, every once in a while, we hold a <I>Request for Proposals</I>, if you will. All members of the community are encouraged to submit their grand ideas which will lay out long-term targets. Proposals are voted on democratically, again, by the community. This ensures it is truly the community that guides the KDE project to wherever the community wants it to go.

This year, the three most voted proposals have been:

<h3><a href="https://phabricator.kde.org/T6831">Top-notch Usability and Productivity for Basic Software</a></h3>
Nate Graham proposes improving the usability of KDE's software and making it more accessible and user-friendly for a wider variety of users. Nate argues that, although KDE apps and environments in general boast a "<I>long list of features that are genuinely useful for normal people's typical use cases</I>", small but noticeable inconsistencies and niggling usability issues sometimes mar KDE's semblance of maturity with casual users.

Nate reasons that focusing on irksome details of the most common and commonly used of KDE's software, such as Plasma, Dolphin, Okular and Discover, would be the first step towards polishing the whole. He mentions, for example, the annoying bug that makes Plasma require the Wifi password twice; or enhancements that can be made to Dolphin to support column view or colourised files and directories, like MacOS X's file browser sports; or improving Okular's stamp feature to make it suitable for digitally signing documents.

KDE's environments and applications are mature and usable to a great extent, but by getting small incremental improvements, we can nearly subliminally improve the overall feel of the project and increase its uptake with the general public.

<h3><a href="https://phabricator.kde.org/T7050">Privacy Software</a></h3>
In synch with <a href="https://dot.kde.org/2016/04/05/kde-presents-its-vision-future">KDE's vision</a>, Sebastian Kugler says that "<i>KDE is in a unique position to offer users a complete software environment that helps them to protect their privacy</i>". Being in that position, Sebastian explains, KDE as a FLOSS community is morally obliged to do its utmost to provide the most privacy-protecting environment for users.

<p><figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/plasmaphone.png"><img src="/sites/dot.kde.org/files/plasmaphone.png" alt="" width="200" /></a><br />KDE will bring privacy to all<br />your devices, including your<br />mobile.</a></figure></p>

This is especially true since KDE has been developing not only for desktop devices, but also for mobile - an area where the respect for users' privacy is nearly non-existent. Sebastian thinks that the intrusion on users’ personal lives is very dangerous. Users can have their livelihood and even personal safety put at risk when their confidential data makes its way into the hands of unscrupulous companies or hostile government agencies.

To make sure KDE environment and apps protect users’ privacy, Sebastian lists several measures that developers can implement. He proposes that applications not expose private data by default, asking the user for explicit consent before sending data to third parties. Other measures would involve apps using privacy-protecting protocols when communicating with the outside world; say, a widget should use Tor to collect weather information. Applications should also only require the bare minimum user information to operate and only when it is essential.

Finally, the proposal explains that KDE must provide the right tools to further safeguard users' privacy. These tools include email clients that offer easy ways of encrypting messages, privacy-protecting chat and instant messaging protocols and clients, and support for online services that can be implemented on personal servers.

<h3><a href="https://phabricator.kde.org/T7116">Streamlined Onboarding of New Contributors</a></h3>
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/latinoware02x1500.jpg"><img src="/sites/dot.kde.org/files/latinoware02x1500.jpg" alt="" width="300" /></a><br />Getting more people involved with the project<br />is crucial for the survival of KDE.<br />Photo CC By-SA by <a href="https://teia.bio.br/blog/">Teia</a></figure></p>
Although we have made many improvements to KDE's development infrastructure and tools over the years, there are still several things we can do to streamline the access for contributors, says Neofytos Kolokotronis. Thinking of ways to get more people involved in the development, Neofytos proposes measures to simplify newcomer participation within KDE.

KDE is a living community and, as such, it is threatened if new users do not become contributors and therefore do not join its ranks, bringing in new blood and fresh ideas. To solve this potential problem, Neofytos wants the community to look at methods of incrementing user involvement. This will require analysing the available resources, especially the people and time that they can invest in this effort.

He also proposes KDE improve and standardise protocols for accepting and mentoring new users, as well as correcting issues with documentation and tools used to receive new contributions, such as  <a href="https://bugs.kde.org/">KDE's bug tracking system</a>.

<hr />

The KDE community will start implementing the proposals into concrete actions and policies. The proposals will shape how the KDE community creates software and works with its members, as well as with outside users. In view of the winning ideas, we can expect a more polished KDE experience, enhanced privacy protection and a more accessible and welcoming community for years to come.

<I>To make sure all KDE's goals are met, we need your support. Head over to the End of Year fundraiser (coming soon!) and help us meet our yearly funding target. You can contribute to the success of KDE and we will also show appreciation with karma and gifts!</I>
<!--break-->