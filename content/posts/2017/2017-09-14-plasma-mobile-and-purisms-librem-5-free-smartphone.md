---
title: "Plasma Mobile and Purism's Librem 5 Free Smartphone"
date:    2017-09-14
authors:
  - "Paul Brown"
slug:    plasma-mobile-and-purisms-librem-5-free-smartphone
comments:
  - subject: "Shame Gnome's got in on the game..."
    date: 2017-09-25
    body: "<p>Would love to see Plasma shipping as the dafualt.</p>"
    author: "Nomen luni"
  - subject: "Why do we need an open source smartphone?"
    date: 2017-10-11
    body: "<p>I believe that the Librem 5 at its current stage and with these proposed specifications doesn't stand a chance against iOS or Android. but still, I think that we need it as a third option.</p><p>I have written a piece on why do we need an Open Source smartphone. Here's the link http://www.consumeit.io/librem-5/31005</p>"
    author: "Parth singh"
---
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/tilted.jpeg#overlay-context="><img src="/sites/dot.kde.org/files/tilted.jpeg#overlay-context=" alt="" width="300" /></a><br />Artist's impression of what Plasma Mobile<br />would look like on the Librem 5.</figure></p>

"<i>This was the plan all along</i>", said Todd Weaver during a conference call we held with him last week. He was referring to building a free, open, and privacy-respecting smartphone. 

[<b><a href="https://www.kde.org/announcements/kde-purism-librem5.php">Read the official press release here.</a></b>]

<a href="https://puri.sm/">Purism</a>, Todd's company, produces the Librem computers, laptops with components that, where possible, are guaranteed to be respectful of the user's privacy. Their covers sport two hardware kill-switches, for example. One shuts off the camera. The other closes down WiFi and Bluetooth. 

And, although not all components are open hardware, <href="https://puri.sm/learn/freedom-roadmap/">Purism is perfectly transparent about this</a>, recognizes it's not ideal, and aims to replace them when it becomes possible. Purism's ultimate aim is to achieve what they call <a href="https://puri.sm/learn/freedom-roadmap/"><I>Purism Purist state</I></a>, in which every single chip and board is totally free and open, with all the schematics published under a free licence.

Naturally, the Librem laptops come with GNU/Linux pre-installed.

Now, Purism has set its aims on the smartphone market. Unhappy with the dominance of a few gigantic (and gigantically powerful) multinational corporations that actively crush any competition and leech data from customers wholesale, Todd and his team are <a href="https://puri.sm/shop/librem-5/">raising money to fund a phone</a> that, like the Librem laptops, is as free and open as possible, and respects users' privacy.

This aligns well with  <a href="https://dot.kde.org/2016/04/05/kde-presents-its-vision-future">KDE's vision</a> of what software should do for the users, and we are actively developing Plasma Mobile, which right now is at a stage where the platform actually works. It seemed logical that we should team up with Purism and work towards the common goal of creating a free and open, commercially viable smartphone.

<iframe width="560" height="315" src="https://www.youtube.com/embed/auuQA0Q8qpM" frameborder="0" allowfullscreen></iframe>

It is true that Purism has not committed to any given platform yet. What they have done is agreed to help KDE adapt Plasma Mobile to their device, and for that they are committing resources, human and otherwise.

This is a win on both sides. KDE gets to try out Plasma Mobile on a device without having to go through all the guesswork of reverse engineering undocumented hardware. Purism gets to test-run Plasma Mobile on their device and help steer its development so it is fully supported. This gives Plasma Mobile a good chance of becoming the default interface for the Librem 5, although that decision is ultimately one Purism has to take.

However, our first step is to help make the Librem 5 a reality. The success of the <a href="https://puri.sm/shop/librem-5/">crowdfunding effort</a> will be a net gain for the Free Software community regardless of which environment finally gets to run on the hardware.

This is a step we cannot take alone. <a href="https://puri.sm/shop/librem-5/"">Support the crowdfunding campaign</a> and you won't only help us succeed, but you can also become part of the project: donate now and you can get your hands on developer kits and early-bird devices! 
<!--break-->