---
title: "Meet KDE at FOSDEM Next Month"
date:    2017-01-16
authors:
  - "jriddell"
slug:    meet-kde-fosdem-next-month
---
  <p>Next month is FOSDEM, the largest gathering of free software developers anywhere in Europe. FOSDEM 2017 is being held at the ULB Campus Solbosch on Saturday 4th and Sunday 5th of February. Thousands of coders, designers, maintainers and managers from projects as popular as Linux and as obscure as Tcl/Tk will descend on the European capital Brussels  to talk, present, show off and drink beer. 
  
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 327px;"><img src="/sites/dot.kde.org/files/101.jpg" width="327" height="829" /><br /><figcaption>You won't believe what the KDE community's next weird collaboration is about. Find out at FOSDEM.</figcaption></figure> 

<p> KDE will have a stall in building K where we will demonstrate our latest software including KDE neon running on Docker, the newest build of Plasma Mobile using Android Open Source Project, and a very exciting mystery announcement.</p>

Our Saturday parties have become legendary and this year's party has a new location at Le Magic Rubens in the city centre. Sign up on <a href="https://community.kde.org/Promo/Events/FOSDEM/2017">the wiki page</a> if you'd like to come.

We will be taking part in the <a href="https://fosdem.org/2017/schedule/track/desktops/">Desktop devroom</a> on Sunday where several presenters will give talks about KDE.

<h2>Bundling KDE - Where does KDE land in the Snap and Flatpak world? by Aleix Pol Gonzalez (apol)</h2>

<p>How we are integrating the Snap and Flatpak packaging systems into Plasma and what steps we've had to take to get KDE applications packaged and working on Flatpak and Snap.</p>

<p>KDE is present on different platforms, but most notably on GNU/Linux and it's here where we're seeing the most changes lately. In this presentation I'll explain how we are integrating the Snap and Flatpak packaging systems into Plasma and then what steps we've had to take to get KDE applications packaged and working on Flatpak and Snap.</p>

<h2>From Gtk to Qt: An Strange Journey, part 2 - The continuation of the original talk from Dirk Hohndel and Linus Torvalds about the port of Subsurface from Gtk to Qt, now with mobile in mind.</h2>
     by Tomaz Canabrava<br style="clear: both;" /> <img class="event-logo" src="https://fosdem.org/2017/schedule/event/desktops_from_gtk_to_qt_subsurface_mobile/desktops_from_gtk_to_qt_subsurface_mobile-35da113311a7015f8c499bd4ac14a41d9f677c54618ca567dc410d93ff90f5f8.jpg"  alt="" />

<p>As subsurface evolved from a Gtk Application to a Qt one, cutting a quarter  of the codebase while still gaining new functionalities, a new question arose: "How do we get this desktop based application and run it on  mobile, on a unified codebase?"</p>

<p>How do we take a kernel developer application writen in kernel-style code for the desktop and make it universal, able to run in any operating system be it mobile or desktop?</p>

<p>In this talk I'll present you piece-by-piece history on what we had when Subsurface started, the challenges that kernel hackers faced when creating a desktop application and why the choice was made to port away from Gtk into Qt -  even though the main developer of subsurface back then loathed C++.</p>

<p>After the initial port to desktop a new era began, the era of mobile applications, and Qt had launched it's new QML language that promised good integration on mobile and even desktop with minimal effort. We wanted to give it a try. At the same time the KDE hackers launched a new project "Kirigami", a library build on QML to simplify development of QML based software. Since we were already on the bleeding edge of things, why not give it a try?</p>

<p>This is the tale of Subsurface, From Gtk to Qt to Mobile, from one of it's main hackers.</p>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; width: 320px;"><img src="/sites/dot.kde.org/files/24599424582_2432be500a_n.jpg" alt="" width="320" height="240"  /><br /><figcaption>Last year's KDE @ FOSDEM party to launch KDE neon, what will this year's party
  launch?</figcaption></figure>

<h2>Kube - The next generation communication and collaboration client by Christian Mollekopf </h2>

<p>Kube is a beautiful, modern communication in a reliable, high-performance  native groupware application for your desktop, laptop and mobile devices.</p>

<p>Kube is a next-gen communication and collaboration client built with QtQuick on a high performance, low resource usage core. It provides online and offline access to all your mail, contacts, calendars, notes, todos and more.</p>

<p>With a strong focus on usability, the team works with designers and UX experts from the ground up, to build a product that is not only visually appealing but also a joy to use.</p>

<p>While the initial focus is on the Linux desktop, the platform is built to run on all desktop systems as well as on mobile devices.</p>

<p>This talk is giving an overview of what Kube is and strives to be, along with some history why this effort has been started in the first place. The talk will go into some technical detail, but is suitable for anyone interested in an alternative to the currently existing groupware clients.</p>

With over 8000 hackers attending to particulate in hundreds of lectures FOSDEM is one of the premier conferences to collaborate with other developers and be informed about the latest developments in the free software community. We look forward to seeing you there!

<!--break-->
