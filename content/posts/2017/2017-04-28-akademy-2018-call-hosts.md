---
title: "Akademy 2018 Call for Hosts"
date:    2017-04-28
authors:
  - "valoriez"
slug:    akademy-2018-call-hosts
---
<p><img src="https://akademy.kde.org/sites/akademy.kde.org/files/2016/akademy_website_header_blank_0.png" alt="" /></p>

<p>Akademy, KDE's annual conference, requires a place and team for the year 2018. That's why we are looking for a vibrant, enthusiastic spot in Europe that can host us!</p>

<h2>A Bit About Akademy</h2>

<p>Akademy is KDE's annual get-together where our creativity, productivity and love are at their peak. Developers, users, translators, students, artists, writers - pretty much anyone who has been involved with KDE will join Akademy to participate and learn. Contents will range from keynote speeches and a two-day dual track session of talks by the FOSS community, to workshops and Birds of a Feather (BoF) sessions where we plot the future of the project. Friday is scheduled for the KDE e.V. General Assembly and a pre-Akademy party/welcoming event. Saturday and Sunday covers the keynotes, talks and lightning talks. The remaining four days are used for BoFs, intensive coding sessions and workshops for smaller groups of 10 to 30 people out of which one day is reserved for a Day Trip of the attendees around the local touristic sights. Hosting Akademy is a great way to contribute to a movement of global collaboration. You get a chance to host one of the largest FOSS community in the world with contributors from over the world and be a witness to a wonderful inter-cultural fusion of attendees in your home town. You'll also get great exposure to Free Software. It is a great opportunity for the local university students, professors, technology enthusiasts and professionals to try their hand at something new.</p>

<h2>What You Need to Do</h2>

<p>Akademy requires a location in Europe, with a nice conference venue, that is easy to reach, preferably close to an international airport. Organizing Akademy is a demanding and a resource intensive task but you’ll be guided along the entire process by people who’ve been doing this for years. Nevertheless, the local team should be prepared to spare a considerable amount of time for this. For detailed information, please see the <a href="https://ev.kde.org/akademy/CallforHosts_2018.pdf">Call for Hosts</a>. Questions and applications should be addressed to the <a href="https://ev.kde.org/contact.php">Board of KDE e.V.</a> or the <a href="https://mail.kde.org/mailman/listinfo/akademy-team">Akademy Team</a>. Please indicate your interest in hosting Akademy to the Board of KDE e.V. by June 1st. Full applications will be accepted until 15th June. We look forward to your enthusiasm in being the next host for Akademy 2018!</p>
<!--break-->
