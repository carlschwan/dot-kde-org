---
title: "Announcing Season of KDE 2018"
date:    2017-11-19
authors:
  - "bgupta"
slug:    announcing-season-kde-2018
---
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/Mascot_konqi-app-presentation.png"><img src="/sites/dot.kde.org/files/Mascot_konqi-app-presentation.png" alt="" width="300" /></a><br />Season of KDE  encourages<br />everybody to help improve KDE.</figure></p>

<b>KDE Student Programs</b> is pleased to announce the <strong>2018 Season of KDE</strong> for those who want to participate in mentored projects that enhance KDE in some way. 

Every year since 2013, KDE Student Programs has been running <strong>Season of KDE</strong> as a program similar to, but not quite the same as Google Summer of Code, offering an opportunity to everyone (not just students) to participate in both code and non-code projects that benefits the KDE ecosystem. In the past few years, SoK participants have not only contributed new application features but have also developed the KDE Continuous Integration System, statistical reports for developers, a web framework, ported KDE Applications, created documentation and lots and lots of other work. 

For this year’s Season of KDE, we are shaking things up a bit and making a host of changes to the program.

<h3>Schedule</h3> 

The <strong>2018 Season of KDE</strong> will have more flexible schedule options for participants. They will now have the opportunity to choose between a shorter sprint project where the working period lasts 40 days or the usual full duration project with a working period of 80 days. 

The timeline is: 
<strong>1st - 26th December 2017:</strong> Participant and Mentor Application Period 
<strong>30th December 2017:</strong> Projects Announced 
<strong>1st January 2018, 00:00 UTC:</strong> Official SoK Work Period Begins 
<strong>9th February 2018, 23:59 UTC:</strong> End of Work Period (40 day projects) 
<strong>21st March 2018, 23:59 UTC:</strong> End of Work Period (80 day projects) 
<strong>25th March 2018:</strong> Results Announced 
<strong>31st March 2018:</strong> Certificates issued and sent out 
Beginning of <strong>Q3 2018:</strong> Merchandise and Schwag sent out by courier 

<h3>Teams</h3> 
For the first time, we are now welcoming applications from teams of up to 2 people to participate in the same project. Teams may only participate in full 80-day projects. Shorter sprint projects are still only open to individual participants. 

<h3>Cross-Organisation Projects</h3> 
Do you want to see KDE software work well on other operating systems? Do you want KDE applications to integrate better with another desktop environment? Do you want to see applications from elsewhere integrate better with KDE? 

In the 2018 Season of KDE, we are specially looking out for projects that can help integrate KDE better with other free software projects. We welcome mentors from other projects who’d like to help our participants in their efforts and encourage applications from participants who’d like to work on such a project. Participants will need to have a fairly reasonable grip on both KDE and the partner organisation’s projects, as well as a point of contact in the other organisation that can offer support throughout the duration of the project. 

If you are from another FOSS Project, have a concrete idea of something SoK participants can implement and would like to mentor them for it, please get in touch with us directly. 

<h3>A Grand Prize</h3> 
The 2018 Season of KDE will accept a maximum of 6 projects. We will score each project based on objective criteria, and after completion of the project, the one with the highest score will win this year’s Season of KDE. 

The participants from the winning project will have a chance to attend <a href="https://dot.kde.org/2017/11/17/akademy-2018-vienna-austria-11-17-august">Akademy 2018</a>, KDE’s annual world conference, which will be held in <strong>Vienna</strong> from <strong>August 11th - 17th, 2018</strong>. All travel and stay expenses will be paid for by KDE. At <strong>Akademy</strong>, you will have the chance to meet people from all around the world who make KDE possible, present your project to them, mingle with some of the brightest minds in the world of free software, and lose yourself in one of Europe’s historic centers of music and culture. 

<h3>Getting Started</h3> 
Prospective participants are advised to get in touch with us even before the application period begins to discuss possible projects. You can connect with us at <strong>#kde-soc</strong> on IRC, via our <a href="https://mail.kde.org/mailman/listinfo/kde-soc">mailing lists</a>, or contact the maintainer of an application you want to work on (or the specific team) directly. 

If you’re looking for project ideas, you can find some on our <a href="https://community.kde.org/GSoC/2018/Ideas"> Google Summer of Code 2018 Ideas Page</a>. Prospective mentors are requested to add ideas to this page, so that we have a central repository of project ideas that may be used for both the 2018 Season of KDE and GSoC 2018. 

Participants and mentors can apply <a href="https://season.kde.org">here</a> once applications open. 
<!--break-->