---
title: "KDE releases beta of Kirigami UI 2.0"
date:    2017-01-02
authors:
  - "thomaspfeiffer"
slug:    kde-releases-beta-kirigami-ui-20
comments:
  - subject: "android APK"
    date: 2017-01-13
    body: "<p>The techbase page has a link to an Android app on the play store, but it would be nice if you offer the APK directly for those who don't use the play store.</p>"
    author: "Shawn Rutledge"
---
<img src="/sites/dot.kde.org/files/kirigami.png" />

Today, KDE announces the beta release of Kirigami UI 2.0. 

Soon after the <a href="https://dot.kde.org/2016/08/10/kdes-kirigami-ui-framework-gets-its-first-public-release">initial release of Kirigami UI</a>, KDE's framework for convergent (mobile and desktop) user interfaces, its main developer Marco Martin started porting it from Qt Quick Controls 1 to Qt Quick Controls 2, the next generation of Qt's ready-made standard controls for Qt Quick-based user interfaces. Since QQC 2 offers a much more extended range of controls than QQC 1, the port allowed the reduction of Kirigami's own code, while improving stability and performance. Kirigami 2 is kept as close to QQC 2's API as possible in order to extend it seamlessly. 

Beyond the improvements that the port to QQC2 brings, further work went into Kirigami 2's performance and efficiency, and it also offers significantly improved keyboard navigation for desktop applications. On Android, Kirigami 2 integrates better visually with Material Design.

Of course there are also smaller improvements in various places, such as better handling of edge swipes in the <a href="https://community.kde.org/KDE_Visual_Design_Group/KirigamiHIG/CommandPatterns/OnDemandControls">SwipeListItem</a> or more reliable activation of the Overscroll / Reachability mode (which pulls down the top of the page to the center of the screen where it can be reached with the thumb).

Discover (Plasma's software center), a quite complex application, has already been <a href="http://www.proli.net/2016/12/31/discover-more-in-2017/">ported successfully to Kirigami 2</a> without much hassle, so we are confident that most applications can be ported easily from Kirigami 1 to Kirigami 2.  Since Kirigami 2 requires Qt 5.7, which is not available on all Linux distributions yet, Kirigami 1 is still maintained (receiving fixes for critical bugs) for the time being, but won't receive any new features or improvements.

Although Kirigami 2 has of course been tested internally, this beta release allows us to make sure that the final release contains no bugs which only surface under circumstances we haven't thought of. Therefore we're happy for developers who would like to try out Kirigami 2 beta in their application and report issues they might encounter through one of our various communication channels. You can find those channels, as well as the link to the source tarball, on <a href="https://techbase.kde.org/Kirigami">Kirigami's Techbase page</a>.
<!--break-->