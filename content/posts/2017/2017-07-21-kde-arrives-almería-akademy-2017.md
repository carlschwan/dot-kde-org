---
title: "KDE Arrives in Almer\u00eda for Akademy 2017"
date:    2017-07-21
authors:
  - "jriddell"
slug:    kde-arrives-almería-akademy-2017
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kde-flight.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-flight-wee.jpg" width="450" height="337" /></a><br /><figcaption>KDE Chartered Flight to Almería</figcaption></figure> 
We have travelled from across the globe to meet for our annual gathering where we plan and discuss the next year's activities creating free software to share with the world.  Almería is in the south east of Spain, a country which has long been a supporter of free software and collaboration with its creators.  The sun here is hot but the water is also warm for those who make it to the beach to discuss their work with a pina colada and a swim.  Over the last year KDE has run conferences in Brazil, India, Spain, Germany and sprints in Randa in Switzerland, Krita in the Netherlands, Marble in Germany, GSoC in the US, WikiToLearn in India, Plasma in Germany, Kontact in France, and sent representatives to OSCAL in Albania, FOSSASIA in Singapore, FUDCON in Cambodia, HKOSCon in Hong Kong and more.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/tapas.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/tapas-wee.jpg" width="450" height="337" /></a><br /><figcaption>Tapas y Sangria</figcaption></figure> 

Today we meet from around the globe, KDE contributors have flown in from Taiwan, US, all over Europe and British Isles, India, Brazil, Canada.

We have completed Akademy España, talks in Spanish to the community. 

We also met for the formality of KDE e.V. Annual General Meeting.

The Community Working Group reviewed issues they had to deal with and were pleased there were fewer firefighting issues than in previous years and they could concentrate on gardening community.
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/robert-k.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/robert-k-wee.jpg" width="450" height="253" /></a><br /><figcaption>We are KDE</figcaption></figure> 

Our outgoing treasurer Marta reported on the year's finances which were pleasingly balanced and with ample reserves.  The Financial Working Group reported how they had supported this and that their main task is to support the incoming new treasurer.

The Sysadmin Working Group reported on the pleasing developments retiring old machines, old software such as Drupal 6 and old operating systems.  They are moving towards Ansible for system deployment and were pleased at the new multi-platform CI system which is now running.

We heard from the Advisory Board Working Group who now have regular meetings with representatives from supporting companies and large deployments of KDE software.  

The KDE Free Qt Foundation controls the licencing of Qt with representatives from both KDE and Qt company.  In the last year they have concluded the relicensing of all Qt parts as Free Software. All parts of Qt are available under the GPLv3 or under a compatible license. Most parts are also available under the LGPLv3 and under the GPLv2. The last remaining code to be relicensed was the Qt Quick Compiler. This is now deprecated and replaced with an open source solution since the release of Qt 5.9.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/board.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/board-wee.jpg" width="450" height="253" /></a><br /><figcaption>Your new KDE e.V. board</figcaption></figure> 
Finally we voted on replacement board members for the three who's terms came to an end.  Marta the treasurer did not renew her term.  Holding one of the most important but least thanked tasks in KDE we owe her much gratitude for keeping out books balanced and our payments prompt.  Lydia Pintcher and Aleix Pol i Gonzàlez both stood for the board again and were re-elected for another three years.  And long term KDE developer Eike Hein was elected as a new board member, hoping to bring in more representation to the community from Korea where he lives.  We thanked the outgoing and new board members with the traditional thanks of a fancy dinner.

Tonight we drink sangria and wine under the stars at a welcome party meeting old friends and new, eating tapas of salmorejo, croquetas, tortillas and rice, looking forward to the week ahead.
<!--break-->
