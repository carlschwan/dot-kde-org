---
title: "Plasma 5.10, Simple by Default Powerful When Needed"
date:    2017-05-30
authors:
  - "jriddell"
slug:    plasma-510-simple-default-powerful-when-needed
comments:
  - subject: "Any way to disable \"Pause music on suspend\"?"
    date: 2017-06-07
    body: "<p>Hi,</p><p>I am wondering if there is a way to disable that new feature: \"Pause music on suspend\"?</p><p>Indeed, I am remote controlling a mpd instance (running on a headless server) from a laptop under Plasma. And now, since the Plasma upgrade, after launching music, if I close the laptop the music is paused... And I really would like to avoid that.</p><p>Anyway thanks for that nice release!</p>"
    author: "Fran\u00e7ois"
---
<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    text-shadow: 2px 2px 5px grey;
}
</style>


<main class="releaseAnnouncment container">

<div style="text-align: center">
	<figure class="videoBlock">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/VtdTC2Mh070?rel=0" allowfullscreen='true'></iframe>
	</figure>
	</div>

	<figure class="topImage">
		<a href="https://www.kde.org/announcements/plasma-5.10/plasma-5.10.png">
			<img src="https://www.kde.org/announcements/plasma-5.10/plasma-5.10-wee.png" width="600" height="338" alt="Plasma 5.10" />
		</a>
		<figcaption>KDE Plasma 5.10</figcaption>
	</figure>

	<p>
		Monday, 30 May 2017.		Today KDE has released Plasma 5.10 with new features across the suite to give users an experience which lives up to our tagline: simple by default, powerful when needed.	</p>

<br clear="all" />

<h2>Panel Task Manager</h2>
<figure style="float: right; width: 360px;">
<a href="https://www.kde.org/announcements/plasma-5.10/middle-click-task-bar.webm">
<video loop="true" autoplay="true" width="350">
<source src="https://www.kde.org/announcements/plasma-5.10/middle-click-task-bar.webm" type="video/webm">
[WebM Video not supported]
</video>
</a>
<figcaption>Middle Mouse Click to Group</figcaption>
</figure>

<p>Task Manager, the list of applications in the panel, has gained options for middle mouse click such as grouping and ungrouping applications.</p>

<p>Several other improvements here include:</p>
<ul>
<li>Places jump list actions in File manager launchers (e.g. pinned Dolphin in Task Manager now lists user places)</li>
<li>The icon size in vertical Task Managers is now configurable to support more common vertical panel usage patterns</li>
<li>Improved app identification and pinning in Task Manager for apps that rely on StartupWMClass, perl-SDL-based apps and more</li>
</ul>
<br clear="all" />

<h2>Folder View Is the New Default Desktop</h2>
<figure style="float: right; width: 360px;">
<a href="https://www.kde.org/announcements/plasma-5.10/spring_loading.gif">
<img src="https://www.kde.org/announcements/plasma-5.10/spring_loading.gif" style="border: 0px" width="350" height="192" alt="Spring Loading in Folder View" />
</a>
<figcaption>Folder on the Desktop by Default</figcaption>
</figure>

<p>After some years shunning icons on the desktop we have accepted the inevitable and changed to Folder View as the default desktop which brings some icons by default and allows users to put whatever files or folders they want easy access to.  Many other improvements have been made to the Folder View include:</p>
<ul>
<li><a href='https://blogs.kde.org/2017/01/31/plasma-510-spring-loading-folder-view-performance-work'>Spring Loading</a> in Folder View making drag and drop of files powerful and quick</li>
<li>More space-saving/tighter icon grid in Folder View based on much user feedback</li>
<li>Improved mouse behavior / ergonomics in Folder View for icon dnd (less surprising drop/insert location), rectangle selection (easier, less fiddly) and hover (same)</li>
<li>Revamped rename user interface in Folder View (better keyboard and mouse behavior e.g. closing the editor by clicking outside, RTL fixed, etc.)</li>
<li><em>Massively</em> improved performance in Folder View for initial listing and scrolling large folders, reduced memory usage</li>
<li>Many other bug fixes and UI improvements in Folder View, e.g. better back button history, Undo shortcut support, clickable location in the headings, etc.</li>
<li>Unified drop menu in Folder View, showing both file (Copy/Move/Link) and widget (creating a Picture widget from an image drop, etc.) drop actions</li>
<li>It is now possible to resize widgets in the desktop by dragging on their edges and moving them with Alt+left-click, just like regular windows</li>
</ul>

<br clear="all" />
<h2>New Features Everywhere</h2>
<figure style="float: right; width: 360px;"><a href="https://www.kde.org/announcements/plasma-5.10/lock-music.png"><img src="https://www.kde.org/announcements/plasma-5.10/lock-music-wee.png" style="border: 0px" width="336" height="329" alt="Lock Screen Now Has Music Controls" /></a><figcaption>Lock Screen Now Has Music Controls</figcaption><p>&nbsp;</p><a href="https://www.kde.org/announcements/plasma-5.10/software-centre-krunner.png"><img src="https://www.kde.org/announcements/plasma-5.10/software-centre-krunner-wee.png" style="border: 0px" width="350" height="57" alt="Software Centre Plasma Search" /></a><figcaption>Software Centre Plasma Search offers to install apps</figcaption><p>&nbsp;</p><a href="https://www.kde.org/announcements/plasma-5.10/plasma-pa.png"><img src="https://www.kde.org/announcements/plasma-5.10/plasma-pa-wee.png" style="border: 0px" width="350" height="345" alt="Audio Volume Device Menu" /></a><figcaption>Audio Volume Device Menu</figcaption></figure>

<p>There are so many other improvements throughout the desktop, here's a sample:</p>
<ul>
<li>Media controls on lock screen</li>
<li>Pause music on suspend</li>
<li>Software Centre Plasma Search (KRunner) suggests to install non-installed apps</li>
<li>File copying notifications have a context menu on previews giving access to actions such as open containing folder, copy, open with etc</li>
<li>Improved plasma-windowed (enforces applet default/minimum sizes etc)</li>
<li>'desktop edit mode', when opening toolbox reveals applet handles</li>
<li>Performance optimizations in Pager and Task Manager</li>
<li>'Often used' docs and apps in app launchers in addition to 'Recently used'</li>
<li>Panel icons (buttons for popup applets, launcher applets) now follow the Icons -> Advanced -> Panel size setting in System Settings again, so they won't take up too much space, particularly useful for wide vertical panels</li>
<li>Revamped password dialogs for network authentication</li>
<li>The security of the lock screen architecture got reworked and simplified to ensure that your system is secured when the screen is locked. On Linux systems the lock screen is put into a sandbox through the seccomp technology.</li>
<li>Plasma's window manager support for hung processes got improved. When a window is not responding any more it gets darkened to indicate that one cannot interact with it any more.</li>
<li>Support for locking and unlocking the shell from the startup script, useful especially for distributions and enterprise setups</li>
<li>Audio Volume applet has a handy menu on each device which you can use to set is as default or switch output to headphones.</li>
</ul>

<br clear="all" />
<h2>Improved touch screen support</h2>
<figure style="float: right; width: 360px;">
<a href="https://www.kde.org/announcements/plasma-5.10/lock-screen-virtual-keyboard.png">
<img src="https://www.kde.org/announcements/plasma-5.10/lock-screen-virtual-keyboard-wee.png" style="border: 0px" width="350" height="197" alt="Virtual keyboard on Log In and Lock Screen" />
</a>
<figcaption>Virtual keyboard on Log In and Lock Screen</figcaption>
</figure>
Touch Screen Support has improved in several ways:
<ul>
<li>Virtual Keyboard in lock screen
<li>Virtual Keyboard in the login screen
<li>Touch screen edge swipe gestures
<li>Left screen edge defaults to window switching
<li>Show auto-hiding panels through edge swipe gesture
</ul>
<br clear="all" />
<h2>Working for the Future with Wayland</h2>
<p>We have put a lot of work into porting to new graphics layer Wayland, the switch is coming but we won't recommend it until it is completely transparent to the user.  There will be improved features too such as KWin now supports scaling displays by different levels if you have a HiDPI monitor and a normal DPI screen.</p>
<p>Keyboard layout support in Wayland now has all the features of X11:</p>
<ul>
<li>Layout switcher in the system tray</li>
<li>Per layout global shortcut</li>
<li>Switch layout based on a policy, either global, virtual desktop, application or per window</li>
<li>IPC interface added, so that other applications can change layout.</li>
</ul>

<br clear="all" />

<h2>Plymouth Boot Splash Selection</h2>
<figure style="float: right; width: 360px;">
<a href="https://www.kde.org/announcements/plasma-5.10/plymouth-kcm.png">
<img src="https://www.kde.org/announcements/plasma-5.10/plymouth-kcm-wee.png" style="border: 0px" width="350" height="245" alt="Plymouth KControl Module" />
</a>
<figcaption>Plymouth KControl Module</figcaption>
</figure>

<p>A new System Settings module lets you download and select boot time splashes.</p>

<br clear="all" />

<h2>Bundle Packages</h2>
<figure style="float: right; width: 360px;">
<a href="https://www.kde.org/announcements/plasma-5.10/xdg-portal.png">
<img src="https://www.kde.org/announcements/plasma-5.10/xdg-portal-wee.png" style="border: 0px" width="350" height="204" alt="Selecting a file using file chooser portal, invoking openURI portal and notification portal" />
</a>
<figcaption>Flatpak integration with xdg-desktop-portal-kde: selecting a file using file chooser portal, invoking openURI portal and notification portal</figcaption>
</figure>

<p>Experimental support for forthcoming new bundle package formats has been implemented.  Discover software centre has gained provisional backends for Flatpak and Snappy.  New plugin xdg-desktop-portal-kde has added KDE integration into Flatpak packaged applications.</p>

<p>Support for GNOME’s <a href='https://odrs.gnome.org/'>Open Desktop Ratings</a>, replacing old Ubuntu popularity contest with tons of already existing reviews and comments.</p> 

<br clear="all" />

	<a href="https://www.kde.org/announcements/plasma-5.9.5-5.10.0-changelog.php">Full Plasma 5.10 changelog</a>
<!--break-->