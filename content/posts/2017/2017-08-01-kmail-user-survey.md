---
title: "KMail User Survey"
date:    2017-08-01
authors:
  - "dvratil"
slug:    kmail-user-survey
comments:
  - subject: "It may be more useful to"
    date: 2017-08-01
    body: "<p>It may be more useful to suvey non-users.</p>"
    author: "Stefan"
  - subject: "I would say both are useful.."
    date: 2017-08-09
    body: "<p>I would say both are useful... ;-)</p>\r\n<p>&nbsp;</p>\r\n<p>What I wonder is why there was no way to separate \"I don't know the feature\" and \"I don't need it\". Also, I discovered \"add follow-up reminder\" which I didn't know about, as 15 year KMail user. It shows ONLY when you rightclick in the content of an email not in the message menu or the right click on a email in the list... Took me a while to find it. But - useful! Just like the awesome sent later feature :D</p>\r\n<p>Just one tip/request: don't rewrite all of KMail from scratch. Incremental improvements is the way to go. Just see how far you got with incremental improvements already... Even if, sometimes, a big refactor is needed - just do the refactor. Not rewrite :D Please :D</p>"
    author: "jospoortvliet"
  - subject: "Reliability & recovery from errors"
    date: 2017-09-15
    body: "<p>It's a shame the KDE kmail/PIM survey has ended. For what it's worth I would concentrate on reliability. Kmail has plenty of features and doesn't need any more. Whats needed is to focus on improving the self recovery of the program from errors and prevention of errors in the first place. For instance the classic error when you click on an email and get the \"Retrieving Folder, Please Wait\" place holder. Often the only way to fix this is to wipe out the Akonadi database manually. Kmail should identify the root cause of this error and then recover from it gracefully.</p>"
    author: "Nick"
  - subject: "I don't use it for many"
    date: 2017-09-27
    body: "<p>I don't use it for many reasons. I would not be opposed to using it but the kwallet popup always annoys me.</p><p>It would be nice if there are alternatives to it.</p><p>I use gmail still but would like to stop using it ... due to google eating on the world being evil.</p><p>The thing is just that laziness and inertia is a huge reason for what I do or don't do - if something is</p><p>tedious, difficult, boring or cumbersome then I don't use it really. So please, simplify it! I don't</p><p>want to really click on much at all.</p>"
    author: "shevy"
---
<p>Do you use KMail or Kontact? The KDE PIM developers want to get more knowledge about how KMail is used so they can better know where they should focus and how they should evolve Kmail and Kontact. They want to make the best user experience possible and you can help by filling out a <a href="https://survey.kde.org/index.php/852475">short survey</a>.</p>

<p>The survey won't take more than 5 minutes and it will be a big help.</p>

<p>Please share the survey with your family members, friends and colleagues who use KMail too!</p>