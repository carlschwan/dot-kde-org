---
title: "KDE Plasma 5.9 Beta Kicks off 2017 in Style"
date:    2017-01-12
authors:
  - "jriddell"
slug:    kde-plasma-59-beta-kicks-2017-style
comments:
  - subject: "Git Stable or Unstable?"
    date: 2017-01-12
    body: "<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">I'm confused,</span></p><p>&nbsp;</p><p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">For this 5.9 beta test, do i download the Git Stable of the Git Unstable iso?</span></p><p>&nbsp;</p><p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Thank you very much.</span></p>"
    author: "Wilbert van Bakel"
  - subject: "New scrollbars"
    date: 2017-01-13
    body: "<p>I think the new scrollbars still need work. With the new ones you now have to click on a hidden area to scroll slowly. See my suggestion at&nbsp;https://bugs.kde.org/show_bug.cgi?id=375005 to also show the scroll arrows on mouse over.</p>"
    author: "Paul"
---
<div style='clear:both;'></div>
    <div id="module">
    </div>
<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<!--
<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/LgH1Clgr-uE?rel=0" frameborder="0" allowfullscreen></iframe>
</figure>
<br clear="all" />
-->

<figure style="float: none">
<a href="http://www.kde.org/announcements/plasma-5.9/plasma-5.9.png">
<img src="http://www.kde.org/announcements/plasma-5.9/plasma-5.9-wee.png" style="border: 0px" width="600" height="338" alt="KDE Plasma 5.9 Beta" />
</a>
<figcaption>KDE Plasma 5.9 Beta</figcaption>
</figure>


<p>
Thursday, 12 January 2017. Today KDE releases the beta of this year’s first Plasma feature update, Plasma 5.9. While this release brings many exciting new features to your desktop, we'll continue to provide bugfixes to Plasma 5.8 LTS.
</p>

<br clear="all" />

<h2>Be even more productive</h2>
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/spectacle-notification.png">
<img src="http://www.kde.org/announcements/plasma-5.9/spectacle-notification-wee.png" style="border: 0px" width="350" height="192" alt="Spectacle screenshot notifications can now be dragged into e-mail composers (including web mail)" />
</a>
<figcaption>Spectacle screenshot notifications can now be dragged into e-mail composers (including web mail)</figcaption>
</figure>

<p>In our ongoing effort to make you more productive with Plasma we added interactive previews to our notifications. This is most noticeable when you take a screenshot using Spectacle's global keyboard shortcuts (Shift+Print Scr): you can drag the resulting file from the notification popup directly into a chat window, an email composer or a web browser form, without ever having to leave the application you're currently working with. Drag and drop was improved throughout the desktop, with new drag and drop functionality to add widgets directly to the system tray. Widgets can also be added directly from the full screen Application Dashboard launcher.</p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/icon-dialog.png">
<img src="http://www.kde.org/announcements/plasma-5.9/icon-dialog-wee.png" style="border: 0px" width="350" height="327" alt="Icon Widget Properties" />
</a>
<figcaption>Icon Widget Properties</figcaption>
</figure>
<p>The icon widget that is created for you when you drag an application or document onto your desktop or a panel sees the return of a settings dialog: you can now change the icon, label text, working directory, and other properties. Its context menu now also sports an 'Open with' section as well as a link to open the folder the file it points to is located in.</p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/mute.png">
<img src="http://www.kde.org/announcements/plasma-5.9/mute.png" style="border: 0px" width="340" height="399" alt="Muting from Panel Task Manager" />
</a>
<figcaption>Muting from Panel Task Manager</figcaption>
</figure>

<p>Due to popular demand we implemented switching between windows in Task Manager using Meta + number shortcuts for heavy multi-tasking. Also new in Task Manager is the ability to pin different applications in each of your activities. And should you rather want to focus on one particular task, applications currently playing audio are marked in Task Manager similar to how it’s done in modern web browsers. Together with a button to mute the offending application, this can help you stay focused.</p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/kickoff-krunner-result.png">
<img src="http://www.kde.org/announcements/plasma-5.9/kickoff-krunner-result-wee.png" style="border: 0px" width="350" height="145" alt="Search Actions" />
</a>
<figcaption>Search Actions</figcaption>
</figure>
<p>The Quick Launch applet now supports jump list actions, bringing it to feature parity with the other launchers in Plasma. KRunner actions, such as “Run in Terminal” and “Open containing folder” are now also shown for the KRunner-powered search results in the application launchers.</p>

<p>A new applet was added restoring an earlier KDE 4 feature of being able to group multiple widgets together in a single widget operated by a tabbed interface. This allows you to quickly access multiple arrangements and setups at your fingertips.</p>

<br clear="all" />
<h2>More streamlined visuals</h2>

<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/breeze-scrollbar.ogv">
 <video width="350" height="240" autoplay="true">
  <source src="http://www.kde.org/announcements/plasma-5.9/breeze-scrollbar.ogv" type="video/ogg" />
 </video> 
</a>
<figcaption>New Breeze Scrollbar Design</figcaption>
</figure>
<p>Improvements have been made to the look and feel of the Plasma Desktop and its applications. Scroll bars in the Breeze style, for instance, have transitioned to a more compact and beautiful design, giving our applications a sleek and modern look.</p>

<br clear="all" />
<h2>Global Menus</h2>
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/global-menus-widget.png">
<img src="http://www.kde.org/announcements/plasma-5.9/global-menus-widget-wee.png" style="border: 0px" width="350" height="207" alt="Global Menus in a Plasma Widget" />
</a>
<figcaption>Global Menus in a Plasma Widget</figcaption>
<a href="http://www.kde.org/announcements/plasma-5.9/global-menus-window-bar.png">
<img src="http://www.kde.org/announcements/plasma-5.9/global-menus-window-bar-wee.png" style="border: 0px" width="350" height="258" alt="Global Menus in the Window Bar" />
</a>
<figcaption>Global Menus in the Window Bar</figcaption>
</figure>
<p>Global Menus have returned.  KDE's pioneering feature to separate the menu bar from the application window allows for new user interface paradigm with either a Plasma Widget showing the menu or neatly tucked away in the window bar.</p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/task-manager-tooltips.png">
<img src="http://www.kde.org/announcements/plasma-5.9/task-manager-tooltips.png" style="border: 0px" width="284" height="225" alt="Neater Task Manager Tooltips" />
</a>
<figcaption>Neat Task Manager Tooltips</figcaption>
</figure>
<p>Task Manager tooltips have been redesigned to provide more information while being significantly more compact. Folder View is now able to display file emblems which are used, for example, to indicate symlinks. Overall user experience when navigating and renaming files has been greatly improved.</p>

<br clear="all" />
<h2>More powerful Look and Feel import & export</h2>

<figure style="float: right; width: 360px;">
 <iframe width="350" height="197" src="https://www.youtube.com/embed/RX5HwumKPP8" frameborder="0" allowfullscreen></iframe>
<figcaption>Look and Feel Themes</figcaption>
</figure>
<p>The global Look and Feel desktop themes now support changing the window decoration as well – the 'lookandfeelexplorer' theme creation utility will export your current window decoration to the theme you create.</p>

<p>If you install, from the KDE store, themes that depend on other artwork packs also present on the KDE store (such as Plasma themes and Icon themes) they will be automatically downloaded, in order to give you the full experience intended by the theme creator.</p>


<h2>New network configuration module</h2>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/network-configuration.png">
<img src="http://www.kde.org/announcements/plasma-5.9/network-configuration-wee.png" style="border: 0px" width="350" height="309" alt="Network Connections Configuration" />
</a>
<figcaption>Network Connections Configuration</figcaption>
</figure>
<p>A new configuration module for network connections has been added to System Settings, using QML and bringing a new fresh look. Design of the module is inspired by our network applet, while the configuration functionality itself is based on the previous Connection Editor. This means that although it features a new design, functionality remains using the proven codebase.</p>

<br clear="all" />
<h2>Wayland</h2>
<figure style="float: right; width: 360px;">
<!--
<a href="http://www.kde.org/announcements/plasma-5.9/kwin-wayland.png">
<img src="http://www.kde.org/announcements/plasma-5.9/kwin-wayland-wee.png" style="border: 0px" width="349" height="107" alt="Plasma with Wayland Can Now Take Screenshots and Pick Colors" />
</a>
<figcaption>Plasma with Wayland Can Now Take Screenshots and Pick Colors</figcaption>
<br />
-->
<iframe width="350" height="197" src="https://www.youtube.com/embed/_kQwFpZoDZY" frameborder="0" allowfullscreen></iframe>
<figcaption>Pointer Gesture Support</figcaption>
<a href="http://www.kde.org/announcements/plasma-5.9/wayland-touchpad.png">
<img src="http://www.kde.org/announcements/plasma-5.9/wayland-touchpad-wee.png" style="border: 0px" width="350" height="352" alt="Touchpad Configuration" />
</a>
<figcaption>Wayland Touchpad Configuration</figcaption>
</figure>

<p>Wayland has been an ongoing transitional task, getting closer to feature completion with every release. This release makes it even more accessible for enthusiastic followers to try Wayland and start reporting any bugs they might find. Notable improvements in this release include:</p>
<p>An ability to take screenshots or use a color picker.  Fullscreen users will be pleased at borderless maximized windows.</p>
<p>Pointers can now be confined by applications, gestures are supported (see video right) and relative motions used by games were added. Input devices were made more configurable and now save between sessions.  There is also a new settings tool for touchpads.</p>
<p>Using the Breeze style you can now drag applications by clicking on an empty area of the UI just like in X.  When running X applications the window icon will show up properly on the panel.  Panels can now auto-hide.  Custom color schemes can be set for windows, useful for accessibility.</p>

<p><a href="http://www.kde.org/announcements/plasma-5.8.5-5.8.95-changelog.php">
Full Plasma 5.8.95 changelog</a></p>

<!-- // Boilerplate again -->

<h2>Live Images</h2>

<p>
The easiest way to try it out is with a live image booted off a USB disk. You can find a list of <a href='https://community.kde.org/Plasma/Live_Images'>Live Images with Plasma 5</a> on the KDE Community Wiki.</p>

<p>
<a href='https://community.kde.org/Plasma/Docker_Images'>Docker images</a> also provide a quick and easy way to test Plasma.</p>

<h2>Package Downloads</h2>

<p>Distributions have created, or are in the process of creating, packages listed on our wiki page.
</p>

<ul>
<li>
<a href='https://community.kde.org/Plasma/Packages'>Package download wiki page</a></li>
</ul>

<h2>Source Downloads</h2>

<p>You can install Plasma 5 directly from source. KDE's community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>. Note that Plasma 5 does not co-install with Plasma 4, you will need to uninstall older versions or install into a separate prefix.
</p>

<ul>
<li>

<a href='http://www.kde.org/info/plasma-5.8.95.php'>Source Info Page</a>
</li>
</ul>

<h2>Feedback</h2>

You can give us feedback and get updates on <a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a> or <a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a> or <a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>.
<p>
Discuss Plasma 5 on the <a href='https://forum.kde.org/viewforum.php?f=289'>KDE Forums Plasma 5 board</a>.</a>
</p>

<p>You can provide feedback direct to the developers via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>.  If you like what the team is doing, please let them know!</p>

<p>Your feedback is greatly appreciated.</p>
<!--break-->

