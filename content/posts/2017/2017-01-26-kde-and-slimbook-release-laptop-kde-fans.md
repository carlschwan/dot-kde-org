---
title: "KDE and Slimbook Release a Laptop for KDE Fans"
date:    2017-01-26
authors:
  - "thomaspfeiffer"
slug:    kde-and-slimbook-release-laptop-kde-fans
comments:
  - subject: "HW - Features"
    date: 2017-01-28
    body: "<p>Hi,</p><p>great idea, but I miss a little bit the links to the HW features (yes in the comment above you added it, but in the rest of the websites it's missing).</p><p>Secondary I miss details of the specification, like a link to the Intel description of the <a title=\"intel hd graphics 520\" href=\"http://ark.intel.com/search?q=intel+hd+graphics+520\">Grafik Controller and CPU</a> or at minimum more details (like speed grade from SD/MMC &nbsp;or HDMI interface).</p><p>In the future, it will be nice when USB 3.1 (Type C connector) will be used, with integrated possibility of an USB HUB. A nice implementation you can find at Intels <a title=\"Compute Stick 2m364\" href=\"http://ark.intel.com/products/91980/Intel-Compute-Stick-STK2m364CC\">Compute Stick 2m364</a>. The nice Powersupply which integrates the 2 times USB-HUB you can see at 0:43 on the <a title=\"Intel Video\" href=\"http://www.intel.com/content/www/us/en/compute-stick/intel-compute-stick-core-video.html\">Intel Video</a>. As last remark, it will be greate if a DP for e.g. <a title=\"EIZO EV3235\" href=\"http://www.eizoglobal.com/products/flexscan/ev3237/index.html\">EIZO EV3235</a> connector is available to have the option to use it with a 4k screen with 60Hz or via USB 3.1 Type C for use with e.g. <a title=\"EIZO EV2780-WT\" href=\"http://www.eizoglobal.com/products/flexscan/ev2780/\">EIZO EV2780-WT</a>&nbsp;and the <a title=\"EIZO EV2780-WT Video\" href=\"https://www.youtube.com/watch?v=lc0XZYiLpkw\">Video</a>.</p><p>&nbsp;</p><p>Hope the best for this project - regards<br />UbIx</p>"
    author: "UbIx"
  - subject: "Now, that's a laptop I can get behind"
    date: 2017-01-26
    body: "<p>It seriously makes me hesitate... even though I really wanted a <span style=\"text-decoration: underline;\">cheap</span> laptop this time. I wouldn't have minded a low-powered ARM laptop running KDE &amp; applications, with some modern technologies on board, for example.</p><p>However, is it just me, or the technical description is verry succint? No ports list, battery information, or side pictures. Even the screen details, webcam presence, etc... are unknown. Well, there's <a href=\"https://slimbook.es/en/characteristics\">this page</a>, but I am not sure if this is the same laptop, since it's not directly linked.</p>"
    author: "M@yeulC"
  - subject: "HW info"
    date: 2017-01-26
    body: "http://kde.slimbook.es/hardware"
    author: "tsdgeos"
  - subject: "403 Forbidden"
    date: 2017-01-26
    body: "<p>I get this error when trying to view the website for the KDE Slimbook</p>"
    author: "Yitzchak Schwarz"
  - subject: "So close..."
    date: 2017-01-26
    body: "<p>It's very much a personal preference, but at this point I can't bring myself to go back to a non-standard charger, so the lack of USB C is a letdown. Oh well! Maybe the KDE Slimbook 2 ;)</p>"
    author: "keithzg"
  - subject: "The spec is decent"
    date: 2017-01-26
    body: "<p>and the price is quite reasonable IMO. The customs duty here will press my wallet flat, though :p</p>"
    author: "Mario Ray Mahardhika"
  - subject: "Nice!"
    date: 2017-01-26
    body: "<p>Given all the hardware/driver problems I have with my laptop (unreliable wifi, nvidia driver crashes triggered by kwin, etc. etc.) I like very much the idea of a laptop who is known to work well with Linux and KDE software! Very nice.</p>"
    author: "David Faure"
  - subject: "Hi! Will it be sold in Russia"
    date: 2017-01-26
    body: "<p>Hi! Will it be sold in Russia? How can I get this notebook? :)</p>"
    author: "True Mike"
  - subject: "Money"
    date: 2017-01-27
    body: "<p><span style=\"font-size: medium;\">Let us talk about business.</span></p><p><span style=\"font-size: medium;\">Does KDE earn some money on that deal, like mint does with <span style=\"color: #000000; font-family: Verdana,sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fafafa; display: inline ! important; float: none;\">CombuLab.</span></span></p><p><span style=\"font-size: medium;\"><span style=\"color: #000000; font-family: Verdana,sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fafafa; display: inline ! important; float: none;\">For me it would be right if it is so, but it has to be transparend to the community.<br /></span></span></p>"
    author: "tuxnix"
  - subject: "Win-key.. Nice linux-way xD"
    date: 2017-01-27
    body: "<p>Win-key.. Nice linux-way xD</p>"
    author: "Prot"
  - subject: "Stealed design is not something to be proud of"
    date: 2017-01-27
    body: "<p>This keyboard layout and overall look tells the only message: no designer were involved. We can imagine how the internal design looks like, too. I'd not to trust, really.</p><p>\"Look, ma, I can do the same as Apple!\" <strong>but this is not Apple</strong>.</p><p>Older CPU, second-hand look, overpriced - nothing to be proud of, guys!</p>"
    author: "Alex12345"
  - subject: "Windows Logo"
    date: 2017-01-28
    body: "<p>Hello,</p><p>Why does it have a Microsoft Windows logo on the keyboard and not Tux or KDE Plasma logo?</p>"
    author: "Adomas"
  - subject: "I wonder what the \"1 x"
    date: 2017-01-28
    body: "<p>I wonder what the \"1 x drivers disk\" is?! For a Linux Laptop! Without a CD drive :)</p>"
    author: "Anonymous"
  - subject: "very nice notebook"
    date: 2017-01-28
    body: "<p>I really like the idea. I understand that Intel CPU are superior, yet I perfer to have AMD CPUs. I like the underdog. :)</p><p>Never the less, it is a great idea! Congratulations for all involved! Good work!</p>"
    author: "Michael"
  - subject: "cost?"
    date: 2017-01-28
    body: "<p>I assume they won't sell thousands of the notebooks, and small batch of replacement keys may not be feaseble. But I agree, a Penguin button would have been very stylish.</p>"
    author: "Michael"
  - subject: "Penguin buttons are available for that KDE laptop"
    date: 2017-01-28
    body: "<p>Penguin buttons are already available for that KDE laptop: https://slimbook.es/en/store/componentes/teclado-slb15tc-comprar</p>"
    author: "Ganton"
  - subject: "That in particular is very"
    date: 2017-01-30
    body: "<p>That in particular is very visible on the \u201cdesign\u201d choice that Fn is in the corner and Ctrl is one step inwards.&nbsp; Ctrl should always be in the corner (as is on desktop keyboards) :(</p>"
    author: "ralesk"
  - subject: "Penguin and German layout"
    date: 2017-01-30
    body: "<p>Is it available for the KDE notebook too?</p><p>&nbsp;</p><p>Unfortunately, the German layout lacks a button between the left shift and the Y (on the German) layout.</p>"
    author: "Michael"
  - subject: "Yay!"
    date: 2017-01-30
    body: "<p>That\u2019s great news! I hope many people buy it!</p>"
    author: "Arne Babenhauserheide"
  - subject: "DELL XPS Comparison"
    date: 2017-02-03
    body: "<p>Quite some folks balk at the price and compare it to the Dell XPS 13. <strong>WTF?</strong></p>\r\n<p>A Dell XPS i5 with 8 GB ram and 256 GB SSD costs.. <strong>1.309 \u20ac</strong>\r\nThe Slimbook i5 with 8 GB ram and 256 GB SSD costs... <strong>899 \u20ac</strong></p>\r\n<p>That is a crazy good deal. Yes, the Slimbook comes with a 6th generation Intel Core, Skylake, not the 7th generation, Kaby Lake. Kaby Lake is about 5-10% more power efficient and can play H256 with lower power usage. Otherwise, no difference - the architecture is the same, Kaby Lake is a super minor refresh. And the Slimbook comes with a huge 6800 mAh battery vs Dell's 6000 mAh so you can expect similar battery life.</p>\r\n<p>But it gets more interesting. You want a DELL with 16 GB ram? The CHEAPEST option available is <strong>1.559 \u20ac</strong>\r\nBut you are then also forced to buy a high resolution screen for your DELL, which cuts your battery life IN HALF. Down to 6-8 hours at best. Not with the Slimbook. You can get 16 GB ram for <strong> 1029 \u20ac</strong> and have HUGE battery life!</p>\r\n<p>I am strongly considering buying this as it is simply better hardware for a much better price than what DELL offers. If it had Kaby Lake, I would have decided already, simply because I'm a sucker for 'the latest and greatest'. Even if it barely brings any benefits. And I'd like to see a hardware review, or touch it myself. But it is still SUPER tempting.</p>\r\n<p>Oh and yes, I use KDE so yay.</p>"
    author: "jospoortvliet"
  - subject: "I just had this idea..."
    date: 2017-02-09
    body: "<p>It's weird... I was just thinking about making something like this and here is is. Interesting... It could have a better design though. I guess my idea isn't obsolete yet...</p>"
    author: "27"
  - subject: "love the idea but wish it were bigger"
    date: 2017-02-10
    body: "<p>I would love to see this in a 17 inch size, maybe even a 15.</p>"
    author: "jpj"
  - subject: "Intel CPU = Management Engine"
    date: 2017-02-15
    body: "<p>KDE, I am disappointed&nbsp;</p><p><br />I cannot understand that KDE Slimbook uses an Intel CPU without mentioning the Intel Management Engine anywhere. On the slimbook website it is clearly mentioned you strive for more privacy for your users, alas according to your hardware choices you do not take this seriously.</p><p>You, KDE, as part of the FOSS community, should have known that Intel is a hugely malicious company that handles according to the \"Security by obscurity\" principle and, moreover, uses this principle as an argument to put our privacy at risk. This becomes clear when we look at the libreboot FAQ page about the Intel Management Engine:&nbsp;https://libreboot.org/faq/#intelme. The most important paragraph on that page clearly states what is wrong with the majority of Intel CPUs: \"In summary, the Intel Management Engine and its applications are a backdoor with total access to and control over the rest of the PC. The ME is a threat to freedom, security, and privacy, and the libreboot project strongly recommends avoiding it entirely. Since recent versions of it can't be removed, this means avoiding all recent generations of Intel hardware.\".</p><p>I also cannot accept when you would make an argument like \"There are no other, better possibilities\", because both the Ministry of Freedom (https://minifree.org/) and Technoetic (https://tehnoetic.com) prove that this is not true.</p><p><br />I hope you'll reconsider these design flaws in the future</p>"
    author: "Wouter Franken"
  - subject: "Really?"
    date: 2017-02-27
    body: "<p>So you think 7 year-old refurbished laptops are a better choice than this one? So you think Intel is secretly watching your every move?</p><p>&nbsp;</p><p>Uh, right...thanks for the advice.</p>"
    author: "oshunluvr"
  - subject: "Website is down"
    date: 2017-03-04
    body: "<p>The website for the slimbook is down.</p>"
    author: "Gregg Housh"
  - subject: "\u0410\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0441\u0432\u0435\u0440\u043b\u0435\u043d\u0438\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0439"
    date: 2021-01-10
    body: " \r\n<a href=https://gendiam.ru/>\u0410\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0441\u0432\u0435\u0440\u043b\u0435\u043d\u0438\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0439 \u0432 \u0431\u0435\u0442\u043e\u043d\u0435</a> \u0432 \u0441\u043f\u0431 \u0438 \u043b\u043e \u043d\u0435\u0434\u043e\u0440\u043e\u0433\u043e."
    author: "gendiamlync"
  - subject: "\u0410\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0441\u0432\u0435\u0440\u043b\u0435\u043d\u0438\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0439"
    date: 2021-01-10
    body: " \r\n<a href=https://gendiam.ru/>\u0410\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0441\u0432\u0435\u0440\u043b\u0435\u043d\u0438\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0439 \u0432 \u0431\u0435\u0442\u043e\u043d\u0435</a> \u0432 \u0441\u043f\u0431 \u0438 \u043b\u043e \u043d\u0435\u0434\u043e\u0440\u043e\u0433\u043e."
    author: "gendiamlync"
  - subject: "\u041d\u0443\u0436\u043d\u043e \u043f\u0440\u043e\u0441\u0432\u0435\u0440\u043b\u0438\u0442\u044c \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0435?"
    date: 2021-02-10
    body: "\u041d\u0443\u0436\u043d\u043e \u043f\u0440\u043e\u0441\u0432\u0435\u0440\u043b\u0438\u0442\u044c \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0435? \u0442\u043e\u0433\u0434\u0430 \u0412\u0430\u043c \u043a \u043d\u0430\u043c \u041c\u044b \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u043c <a href=https://gendiam.ru/>\u0430\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0431\u0443\u0440\u0435\u043d\u0438\u0435 \u043b\u044e\u0431\u044b\u0445 \u0434\u0438\u0430\u043c\u0435\u0442\u0440\u043e\u0432</a>"
    author: "gendiamlync"
  - subject: "\u041d\u0443\u0436\u043d\u043e \u043f\u0440\u043e\u0441\u0432\u0435\u0440\u043b\u0438\u0442\u044c \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0435?"
    date: 2021-02-10
    body: "\u041d\u0443\u0436\u043d\u043e \u043f\u0440\u043e\u0441\u0432\u0435\u0440\u043b\u0438\u0442\u044c \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0435? \u0442\u043e\u0433\u0434\u0430 \u0412\u0430\u043c \u043a \u043d\u0430\u043c \u041c\u044b \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u043c <a href=https://gendiam.ru/>\u0430\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0431\u0443\u0440\u0435\u043d\u0438\u0435 \u043b\u044e\u0431\u044b\u0445 \u0434\u0438\u0430\u043c\u0435\u0442\u0440\u043e\u0432</a>"
    author: "gendiamlync"
---
<div style="float:right; margin-left:20px; "><a href="http://kde.slimbook.es/"><img src="/sites/dot.kde.org/files/cover.jpg" width="400"/></a></div>

Today KDE is proud to announce the immediate availability of the <a href="http://kde.slimbook.es/">KDE Slimbook</a>, a KDE-branded laptop that comes pre-installed with Plasma and KDE Applications (running on Linux) and is assured to work with our software as smoothly as possible.

The KDE Slimbook allows KDE to offer our users a laptop which has been tested directly by KDE developers, on the exact same hardware and software configuration that the users get, and where any potential hardware-related issues have already been ironed out before a new version of our software is shipped to them. This gives our users the best possible way to experience our software, as well as increasing our reach: The easier it is to get our software into users' hands, the more it will be used.

Furthermore, the KDE Slimbook, together with KDE neon, offers us a unique opportunity to isolate and fix issues that users have with our software. When something in Plasma, a KDE Application or some software using a KDE Framework does not work as intended for a user, there are at least three layers that can cause the problem:
<ul>
    <li>The KDE software itself</li>
    <li>The operating system</li>
    <li>The hardware or its drivers</li>
</ul>
Of course KDE always tries to reduce bugs in our software as much as possible. Problems can occur in any of the aforementioned layers, however, and often times it is difficult for us to pin-point exactly where things are going wrong. Last year, <a href="https://neon.kde.org/">KDE neon</a> joined the KDE community with the promise to give us control over the operating system layer. This does not mean we won't make our software available on other distributions or operating systems, of course, but it allows us to eliminate that layer as a possible source of a problem.

This left us still with one layer we had zero control over, though: The hardware layer.

Fast-forward to late last year, when the Spanish laptop retailer <a href="https://slimbook.es">Slimbook</a> approached KDE with the idea to offer KDE-branded laptops that come pre-installed with Plasma and KDE Applications. We were excited about the idea, and put our designers and developers to the task of creating a branding for such a device and making sure that KDE neon runs without any hardware-related issues on it.

<figure><div style="margin:auto; width:560px"><img src="/sites/dot.kde.org/files/machine.jpg" /></div></figure>

For now, the KDE Slimbook will always come pre-installed with KDE neon, but we are open to offering other distributions that come pre-installed with Plasma for customers to choose from.

The KDE Slimbook is for people who love KDE software, regardless of whether or not they are active contributors to KDE.

For more information, visit the <a href="http://kde.slimbook.es/">KDE Slimbook website</a>.

<!--break-->
