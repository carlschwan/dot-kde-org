---
title: "Randa Meetings 2017: It's All About Accessibility"
date:    2017-08-09
authors:
  - "Paul Brown"
slug:    randa-meetings-2017-its-all-about-accessibility
comments:
  - subject: "Qt cross platform accessibility "
    date: 2017-08-09
    body: "<p>I recently saw a post about problems with Qt accessibility across platforms. Just wanted to check that this sort of thing is on your radar. https://blind.guru/qta11y.html</p>"
    author: "sam2"
  - subject: "We'll take a look"
    date: 2017-08-10
    body: "Be sure that we're informed about this blog post and that we will work hard on accessibility in Randa but we mostly probably won't be able to work on the specific problem mentioned in this blog post as JAWS is quite expensive software and as far as I know none of the Randa Meetings participants will have it installed."
    author: "unormal"
  - subject: "Hello all KDE Developers"
    date: 2017-09-05
    body: "<p>Hello all KDE Developers</p><p>I hope u had a good time at Randa... I love since KDE 3.8 our Desktop..</p><p>I live by a friend at Findeln near zermatt... Thanks a lot for all the work.&nbsp;</p>"
    author: "Silvan Marty "
---
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/27766726455_b994731631_k.jpg"><img src="/sites/dot.kde.org/files/27766726455_b994731631_k.jpg" width="420" /></a><br /><figcaption>Attendees of the 2016 Randa Sprint in the Swiss Alps.</figcaption></figure></p>

<a href="https://dot.kde.org/2015/12/07/randa-meetings-2015-huge-success-again">Randa 2015 was about bringing touch to KDE</a> apps and interfaces. At <a href="https://dot.kde.org/2016/06/10/randa-meetings-2016-fundraising-campaign">Randa 2016, developers worked on building frameworks that would allow KDE apps to work on a wider range of operating systems</a>, like Windows, MacOS and Android.

Randa Meetings 2017 will be all about accessibility.

At KDE, we understand that using an application - be it an email client, a video editor, or even educational games aimed at children - is not always easy. Different conditions and abilities require different ways of interacting with apps. The same app design will not work equally well for somebody with 20/20 vision and for somebody visually impaired. You cannot expect somebody with reduced mobility to be able to nimbly click around your dialogue boxes.

This year we want to focus on things that have had a tendency to fall by the wayside; on solving the problems that are annoying, even deal-breaking for some, but not for everyone. 

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/GComprisLiveUserTesting.jpg"><img src="/sites/dot.kde.org/files/GComprisLiveUserTesting.jpg" width="420" /></a><br /><figcaption>Beta tester trying out new features during the 2015 sprint.</figcaption></figure></p>

To that end, KDE developers will be gathering in the quietness of the Swiss mountains and will push several different projects in that direction. David Edmundson, for example, plans to spend his time improving navigation on Plasma for those who prefer, or, indeed, <I>need</I> to use a keyboard over a mouse. This will help users with reduced mobility that find moving a mouse around cumbersome. And Adriaan de Groot will be working on Calamares, an application that helps install operating systems. Adriaan will make Calamares more accessible to visually impaired users by improving integration with the Orca screenreader.  Sanjiban Bairagya will be working on text-to-speech on Marble, KDE's mapping application. He wants to make the app's turn-by-turn navigation experience more intuitive by integrating Qt's Speech module.

Apart from the projects mentioned above, we will also have developers from Kdenlive, Kubuntu, KMyMoney, Kontact, Kube, Atelier, KDEEdu, digiKam, WikiToLearn and Krita, all working together, intent on solving the most pressing accessibility issues.

But to make Randa Meetings possible, we need your help. <a href="https://www.kde.org/fundraisers/randameetings2017/">Please donate to our funding campaign</a> so we can make KDE more accessible for everyone.

<!--break-->