---
title: "Akademy 2018 - Vienna, Austria - 11-17 August"
date:    2017-11-17
authors:
  - "tsdgeos"
slug:    akademy-2018-vienna-austria-11-17-august
---
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/wien.jpg"><img src="/sites/dot.kde.org/files/wien.jpg" alt="" width="300" /></a><br />A View of Vienna.<br />Source: <a href="https://upload.wikimedia.org/wikipedia/commons/d/dd/Wien_-_Stephansdom_%281%29.JPG">Bwag/CC-BY-SA-4</a>.</figure></p>

Vienna Calling! This is not only a song by the famous Austrian singer Falco, but could also be the motto for next years Akademy.

In 2018 <a href="https://akademy.kde.org/2018">Akademy</a> will be held at the <a href="https://www.tuwien.ac.at/en/">University of Technology</a> (TU Wien) in <a href="https://en.wikipedia.org/wiki/Vienna">Vienna</a>, Austria, from Saturday 11th to Friday 17th August.

The conference is expected to draw hundreds of attendees from the global <a href="https://www.kde.org">KDE</a> Community to discuss and plan the future of the Community and its technology. Many participants from the broad free and open source software community, local organizations and software companies will also attend.

Akademy 2018 is being organized with <a href="https://www.fsinf.at">Fachschaft Informatik</a> (FSINF). Apart from representing and counseling computer science students, FSINF engage in diverse political activities, such as FOSS activism, privacy and social justice, and so on.

<h2>Akademy 2018 Program</h2>

Akademy 2018 will start with a 2-day conference on Sat 11th of August and Sunday 12th of August, followed by 5 days of workshops, Birds of a Feather (BoF) and coding sessions.

<h2>Vienna and Akademy</h2>

Vienna, the capital of Austria, has around 1.8 million inhabitants. It is located in the middle of Central Europe, on the banks of the river Danube. With it's rich history that stretches back to Roman times. It was once the capital city of the Habsburg Empire ans is now a modern city, rated number one in diverse studies on quality of living.

<h2>TU Wien and Akademy</h2>
Almost all buildings of TU Wien are very close to the city center. From the venue a 10 minute walk will bring you directly to the inner city. Around 30,000 students study at Tu Wien, of which 6000 study Computer Science.

<h2>About Akademy</h2>

<p><figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://devel-home.kde.org/~duffus/akademy/2017/groupphoto/"><img style="float: center"  src="/sites/dot.kde.org/files/guille-akademy2017-groupphoto-teensie.jpg" /></a><br />Akademy 2017, Almería.</figure></p>

For most of the year, KDE—one of the largest free and open software communities in the world — works on-line by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact <a href="mailto:akademy-team@kde.org">The Akademy Team</a>.
<!--break-->