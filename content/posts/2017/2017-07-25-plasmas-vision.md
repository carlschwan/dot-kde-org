---
title: "Plasma's Vision"
date:    2017-07-25
authors:
  - "jriddell"
slug:    plasmas-vision
---
Sebastian Kügler <a href="https://vizzzion.org/blog/2017/07/plasmas-vision/">writes on his blog about Plasma's vision statement</a>, which names durabililty, usability and elegance as its corner stones.
