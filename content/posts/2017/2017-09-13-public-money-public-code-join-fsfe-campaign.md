---
title: "Public Money? Public Code! - Join the FSFE Campaign"
date:    2017-09-13
authors:
  - "skadinna"
slug:    public-money-public-code-join-fsfe-campaign
---
Public institutions spend millions of Euros every year for the development of new software that is specifically tailored to their needs. 

Unfortunately, most of this software is closed source. 

This means that your tax money is being used to pay for software that cannot be modified or even studied. Most public institutions pay to develop programs that they do not or cannot release to the public. When other institutions need to solve similar problems, they have to develop the same software again. And each time the public - including you - has to foot the bill.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/PMPC-Why.png"><img src="/sites/dot.kde.org/files/PMPC-Why.png" alt="" width="420" /></a></figure></p>

Paying a company to provide closed software also leads to vendor lock-in. Vendor lock-in is when an institution contracts a certain provider and later discovers it is very hard to switch to another one. 

Companies with a stranglehold on an institution can artificially restrict usage and features of their products. They can forbid you to install their programs on more than a handful of computers, disable saving your work in a certain format, or hike the prices of licenses for no reason.

The biggest problem, however, is the safety of your data. 

Closed software makes solving flaws especially hard and expensive. Even if you know how to solve its vulnerabilities, you would not be legally allowed to do so. Many branches of our public administration often have to keep running insecure software because they cannot afford to pay for the newer version. 

Furthermore, closed source providers often include in their software code that collects data they have no business in collecting. This data can end up in the power of foreign security agencies, sold to unscrupulous advertising companies, or worse. 

<strong>How can we put our trust in public bodies if they don't have full control over the software they are using?</strong> 
<strong>Shouldn't your money be used to develop software that benefits you and other citizens?</strong> 

The Free Software Foundation Europe (<a href="https://fsfe.org/">FSFE</a>) thinks it should - and we at KDE Community agree. 

That is why we are supporting the FSFE campaign called <a href="https://publiccode.eu/">Public Money? Public Code!</a>.
The campaign proposes that all software financed with public money should be distributed as Free Software. 

<iframe src="https://player.vimeo.com/video/232524527" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/232524527">Public Money? Public Code!</a> from <a href="https://vimeo.com/fsfe">Free Software Foundation Europe</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

Although publishing/sharing publicly funded software under a free licence generates great benefits for governments and civil society, policy makers are still reluctant to move forward with decisive legislation. The purpose of this campaign is to convince them.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/PMPC-Imagine.png"><img src="/sites/dot.kde.org/files/PMPC-Imagine.png" alt="" width="420" /></a>.</figure></p>

With Free Software, independent researchers can report earlier on errors, before even miscreants can use them. Experts from anywhere can provide solutions for applications because they can study the code. They can also audit the software to eliminate backdoors or other malicious code. 

By using Free Software, citizens' data is kept safer and the chances of successful attacks from criminals goes down. Free Software can also be used as the foundation for better applications, building upon it to create more efficient and safer programs.

In short, Free Software can help us build a better society for everyone. 

<h2>Join the Campaign!</h2>

More than 30 organizations and individuals have already endorsed the campaign, including Edward Snowden, President of Freedom of the Press Foundation.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/Snowden-PMPC.png"><img src="/sites/dot.kde.org/files/Snowden-PMPC.png" alt="" width="420" /></a></figure></p>

You, too, can join the "Public Money? Public Code!" campaign. Sign the <a href="https://publiccode.eu/openletter/">open letter</a> that explains to politicians and policy makers why using public money to fund public code is a good idea. FSFE will send it to political representatives several times over the next months. 

You can also share the link to <a href="https://publiccode.eu/">the campaign website</a> on social media and online forums. Send it to your friends and coworkers, and encourage them to sign the open letter. 

Spread the word about the campaign by writing about it on your website, or by contacting the media in your country. 

Show that you care about the future of digital infrastructure, because you will be paying for it one way or another.

<!--break-->
