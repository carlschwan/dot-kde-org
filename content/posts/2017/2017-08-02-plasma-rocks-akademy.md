---
title: "Plasma rocks Akademy"
date:    2017-08-02
authors:
  - "sebas"
slug:    plasma-rocks-akademy
---
KDE's yearly world conference - Akademy - was held last week in Almería, Spain. Lots of interesting things happened in the Plasma-verse during Akademy 2017.

<h2>State of the Union</h2>

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/plasma-state-of-the-union-sebas-marco.jpg"><img src="/sites/dot.kde.org/files/plasma-state-of-the-union-sebas-marco-thumb.jpg" width="420" /></a><br /><figcaption>Sebastian Kügler and Marco Martin.</figcaption></figure></p>

Right after the first day's keynote (<a href="http://vizzzion.org/stuff/plasma/akademy2017plasma-state-of-the-union.pdf">slides</a>), Marco Martin and Sebastian Kügler caught the general audience up in their presentation "Plasma: State of the Union". Sebas talked about how the architecture around Frameworks 5 and Plasma 5 worked out very well, and how the 5.8 LTS release was received positively, especially by the users who value stability and predictability.

Marco then presented a number of new features that have been added to Plasma since last year's Akademy and that are planned for the upcoming 5.11 release. There is the improved integration for web browsers, better support for touchscreens, enhancements in the taskbar, return of the App Menu (Global Menu), encrypted volumes through <a href="http://cukic.co/2017/03/09/vault-for-the-privacy-of-your-data/">Plasma Vault</a>, more elegant artwork, a redesign of the System Settings user interface, and many more.

The <a href="https://store.kde.org">KDE Store</a> has gained a lot of traction since <a href="https://dot.kde.org/2016/09/03/kde-software-store">its relaunch</a> at Akademy 2016, and now even allows its contributors to receive donations directly from happy users. (Full disclosure: Your editor was able to buy a pizza last week from donations coming through this new feature in the KDE Store).

Plans for the future of Plasma include getting the Wayland port ready for end-users, and thereby delivering on the promise of smoother graphics, a leaner graphics stack, better protocol semantics, improved high-DPI support and more features for touchscreens and convertible devices (such as edge-swipe support).

<h2>Plasma's Vision</h2>
Another talking point which deserves a special mention is <a href="https://community.kde.org/Plasma/Vision">Plasma's vision statement</a>. The statement had been discussed on Plasma's mailing list and on KDE's Phabricator collaboration tool over the last couple of months, and it is now finalized. Plasma's vision statement reads:

<blockquote>
"Plasma is a cross-device work environment by the KDE Community where trust is put on the user's capacity to best define her own workflow and preferences.
Plasma is simple by default, a clean work area for real-world usage which intends to stay out of your way. Plasma is powerful when needed, enabling the user to create the workflow that makes her more effective to complete her tasks.
Plasma never dictates the user's needs, it only strives to solve them. Plasma never defines what the user is allowed to do, it only ensures that she can.
Our motivation is to enable actual work to happen, across devices, across different platforms, using any application needed.
We build to be durable, we create to be usable, we design to be elegant."
</blockquote>

Its cornerstones are stability, usability (which includes features to make the users' lives easier) and elegance, meaning that the team aims to create stable software that helps users get their job done elegantly, but swiftly and with pleasure. For a more detailed explanation of Plasma's vision, head over to <a href="https://vizzzion.org/blog/2017/07/plasmas-vision/">sebas' weblog</a>.

<h2>Halium Unifies Mobile Linux</h2>

Bhushan Shah, maintainer of Plasma's user interface for mobile devices, talked in detail (<a href="http://blog.bshah.in/slides/akademy2017/">slides</a>) about the latest endeavour - <a href="https://halium.org/announcements/2017/04/24/halium-is-in-the-air.html">Project Halium</a>. Halium is a collaboration that aims to share the development effort among several groups. By creating a common base, Project Halium wants to make it easier to bring Plasma Mobile to a wider range of devices. In just a few months, Halium has not only allowed Plasma Mobile to run on its new reference device (the Google Nexus 5X), but has also provided the basis for ports to the Fairphone 2 and a few other handsets.

With other Free software mobile stacks (such as Ubuntu Touch and Firefox OS) being discontinued, Plasma Mobile positions itself as the best option to create truly Free and community-developed mobile devices. While its development is not very fast-paced, its continued improvements begin to bear fruit. This makes Plasma Mobile an increasingly attractive platform for people and companies that want to escape the Duopoly of today's smartphone market and bring something entirely new to the table.

<h2>Kirigami Makes Convergence</h2>
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/systemsettings.png"><img src="/sites/dot.kde.org/files/systemsettings-thumb.png" width="420" /></a><br /><figcaption>System Settings' new user interface.</figcaption></figure></p>

Most of today's applications are written using QWidgets, which is still the most powerful tool for certain kinds of applications. QWidgets does have some graphical limitations, which may sound bad, but this limitation leads to more consistent UIs across applications. Using QWidgets makes it difficult to deviate from the human interface guidelines (HIG) too much, thus enforcing visual consistency.

This is about to change. Qt is investing heavily into QML, which has fewer graphical limitations, putting more power into developers' hands. A side-effect, however, is that it makes it (too) easy to create applications visually and interactively inconsistent with each other.

Kirigami was born both as a human interface guideline (HIG) and a framework implementing it. Kirigami has been stabilized over the past year and will be released together with KDE Frameworks 5 starting August 2017.
Kirigami provides the skeleton for applications, allowing developers to write visually and interactively consistent applications with little effort. While it seems that Kirigami limits the endless flexibility that QtQuick-based UIs offer, it solves common problems of in-app navigation. Moreover, it solves the lack of consistency across applications, providing uniformity and reducing the need to 'learn' a new application. In order to think outside of the box, you need a box in the first place, and Kirigami provides that box.

One example of a Kirigami app is Koko, a simple QML-based image viewer. Although Koko never received any real designer input, it has been resurrected as a Google Summer of Code project by Atul Sharma, and has been <a href="http://atulsharma.me/2017/07/19/GSoC-Week-5-,-6-,-7/">redesigned</a> to be a textbook case for Kirigami. Other Kirigami-based applications are Discover (Plasma's software center), the new System Settings user interface (to be released with Plasma 5.11), and of course, Kirigami's first adopter: <a href="https://subsurface-divelog.org/2016/03/announcing-subsurface-mobile-for-android/">Subsurface Mobile</a>, an application for logging scuba dives. Kirigami provides a morphing UI, which allows creating applications that work equally well on desktop and mobile. With Kirigami, creating a convergent application is a breeze.

The main target platforms for Kirigami are Plasma Desktop and Plasma Mobile, with the convenient side-effect of working nicely on Android. While we care a lot about following the look of Material on Android, we're not interested so much in the feel. With Plasma Mobile, we want to innovate without the legacy that Android and iOS have on their top-heavy UIs. Such interfaces are very hard to use with a single hand on big screen phones, and this is something we want to avoid.

<h2>Kai Uwe Broulik wins Akademy Award for Plasma</h2>

Kai Uwe Broulik, a Plasma Hacker extraordinaire, was lauded an <a href="https://dot.kde.org/2017/07/24/akademy-awards-2017">Akademy Award</a> this year for his work on Plasma. As the developer behind features like the App Menu, web-browser integration, and many other cool features big and small, Kai is a tireless and positive force that many of his teammates follow as a role model. The award, which also reflects positively on the rest of the Plasma team, is well-deserved and received gratefully.

Swapnil Bhartiya interviewed Kai shortly after the Akademy Awards ceremony. In this interview, Kai reveals his ideas and thoughts on Plasma and the desktop in general. It's well worth watching.

<div style="text-align: center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/X843MCxw5TQ?rel=0" frameborder="0" allowfullscreen></iframe>
</div>

<h2>Birds-of-a-feather</h2>

On Monday, the Plasma team held a day-long <a href="https://dot.kde.org/2017/07/24/akademy-monday-wrapup-session">birds-of-a-feather (BoF) session</a>. BoFs are collaborative meetings where everyone with the same interest is welcome to participate. The sessions are focused on making plans and finding solutions for problems that need face-to-face interaction. Naturally, they are also a great opportunity to look into the developers' kitchen. Let's take a peek!

The Plasma team plans to improve support for convertible devices (laptops with touchscreens and detachable keyboards). In order to do that, some improvements to the virtual keyboards are planned.

Other aspects that need improvement include screen rotation, sensors support, and the appearance of the UI when used with one's finger. Furthermore, the team talked at length about structuring the (somewhat dynamically grown) battery of QtQuick imports in a more logical way.

Finally, a BoF session on Wayland resolved a number of problems in applications using Wayland, leading to direct results. For example, KMail will support the Wayland display server natively in an upcoming releases.
Changes planned for Plasma Mobile include a more efficient and clean implementation of the top panel, better backend-sharing between desktop components and mobile UIs, and better support for third-party applications, especially those left in the rain by the demise of Ubuntu Touch.

Akademy provided a great opportunity to prepare for the next big leaps in different areas of Plasma. As always, it was also a wonderful opportunity for the Plasma team to get together, socialize, and keep working as a team of friends.

<!--break-->