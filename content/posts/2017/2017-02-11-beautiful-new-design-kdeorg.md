---
title: "Beautiful New Design on kde.org"
date:    2017-02-11
authors:
  - "jriddell"
slug:    beautiful-new-design-kdeorg
comments:
  - subject: "Like it a lot"
    date: 2017-02-11
    body: "<p>Congrats to those responsible.</p>"
    author: "Darren"
  - subject: "Wonderful Redesign"
    date: 2017-02-13
    body: "<p>This redesign of KDE.org does looks stunningly beautiful indeed. Its clean, simple and efficient. Just what the project needed. Very nicely done. Keep up the good work.&nbsp;</p>"
    author: "Maxx"
  - subject: "KDE"
    date: 2017-02-15
    body: "<p>KDE is the most beautiful desktop environment i ever seen</p>"
    author: "Muhammad Yusup"
  - subject: "Colors"
    date: 2017-02-15
    body: "<p>Why do you make it colorful. You should make it black and white same as you make icons in plasma. Then it would match the latest trends.</p><p>I think black and white would be perfect.</p>"
    author: "komentator"
---
KDE's main website <a href="https://www.kde.org">www.kde.org</a> has gained a beautiful new design.

<div style="text-align: center;">
<figure style="text-align: center; width: 300px; padding: 1ex; margin: 1ex auto; border: 1px solid grey;"><a href="https://www.kde.org"><img src="/sites/dot.kde.org/files/webpagethumbnail_0.jpeg" /></a><br /><figcaption>www.kde.org</figcaption></figure> 
</div>

While in KDE we pride ourselves on making beautiful software our website has lagged behind modern requirements and trends.  <a href="https://vdesign.kde.org/">Visual Design Group</a> member <a href="https://kver.wordpress.com/">Ken Vermette</a> has quietly worked away with key stakeholders to create a design and update the content.  The new site uses correct HTML5 and is responsive to working on mobiles and tablets.  It includes an introduction to our products, community and how you can get involved.

The scope of KDE projects continues to grow as we evolve from the original desktop environment to become an umbrella organisation hosting projects as diverse as <a href="https://en.wikitolearn.org/Main_Page">WikiToLearn academic textbook collaboration</a> or <a href="https://store.kde.org/">KDE Store content distribution site</a>.  The new website reflects this change in direction while still focusing on our flagship Plasma desktop.

This change is only to the front pages and many more pages on kde.org still use the old theme but these will be transitioned over in the weeks to come.  Many other websites under kde.org are expected and encouraged to adopt the new theme.  

If you find problems please check for them and report them on <a href="https://bugs.kde.org/describecomponents.cgi?product=www.kde.org">our bug tracker</a> or discuss on <a href="https://mail.kde.org/pipermail/kde-www/">kde-www mailing list</a>.

<!--break-->
