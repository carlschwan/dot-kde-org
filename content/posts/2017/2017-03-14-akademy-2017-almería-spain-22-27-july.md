---
title: "Akademy 2017 - Almer\u00eda, Spain - 22-27 July"
date:    2017-03-14
authors:
  - "sealne"
slug:    akademy-2017-almería-spain-22-27-july
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/akademyLogo4Dot.png"/></div>

This year's <a href="https://akademy.kde.org/2017">Akademy</a> will be held at the <a href="http://www.ual.es/">Universidad de Almería (UAL)</a> in <a href="https://en.wikipedia.org/wiki/Almer%C3%ADa">Almería</a>, Spain, from July 22nd to 27th.

The conference is expected to draw hundreds of attendees from the global KDE Community to discuss and plan the future of the Community and its technology. Many participants from the broad free and open source software community, local organizations and software companies will also attend.
 <!--break-->

This year Akademy is being organized together with UNIA and HackLab Almería. Together they have organized various free software events including the successful <a href="http://2016.es.pycon.org/">PyConEs 2016</a>

<h2>Akademy-es</h2>
Akademy-es is the KDE event organized every year by <a href="http://kde-espana.org/">KDE España</a>, this year it is teaming up with the international event and <a href="https://www.kde-espana.org/akademy-es2017/">Akademy-es 2017</a> will also be held in Almería from 20th to 21st of July.

<div style="float: left; padding: 1ex; margin: 1ex;"><img width="225" height="300" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Almeria_Alcazaba_fcm.jpg/450px-Almeria_Alcazaba_fcm.jpg"/><br><small>Source: <a href="https://en.wikipedia.org/wiki/File:Almeria_Alcazaba_fcm.jpg">Wikipedia - CC SA 2.5</a></small></div>

<h2>Almería and Akademy</h2>
Almería a city of about 200,000 inhabitants located in the south-eastern corner of the Iberian Peninsula, it's a city that has attracted diverse peoples and cultures for thousands of years. The city has around 3000 hours of sun every year, so come prepared with lots of sun screen. It has nice sights such as <a href="http://www.andalucia.org/en/cultural-tourism/visits/almeria/monuments/alcazaba-de-almeria/">Alcazaba</a> and the <a href="http://www.andalucia.org/en/cultural-tourism/visits/almeria/monuments/catedral-de-almeria/">Cathedral</a>. Not far from Almería one can enjoy the <a href="https://en.wikipedia.org/wiki/Cabo_de_Gata-N%C3%ADjar_Natural_Park">Cabo de Gata-Níjar Natural Park (UNESCO Biosphere Reserve)</a> the driest location in Europe.

<h2>About HackLab Almería</h2>
<a href="http://hacklabalmeria.net/">HackLab Almería</a> is a collective of technological, social and creative experimentation.

At HackLab we all nurture everyone, sharing information and ideas. It is the perfect place to experiment and create new projects through working groups bringing together experts in different fields.

<h2>About UNIA</h2>
<a href="http://asociacion-unia.es/">UNIA (Universitarios Informáticos de Almería)</a> is an association that promotes participation and learning.

Founded in 1993, it is the oldest of the student associations of the University of Almeria. Of deep entrepreneurial spirit it has always aimed at enriching the university life in different areas. It founded the first UAL university newspaper, CAMPUS, a computer magazine and has organized events about Computer Science for several years.

<div style="padding: 1ex; margin: 1ex; width: 610px;text-align:center; "><a href="http://byte.kde.org/~duffus/akademy/2015/groupphoto/"><img src="/sites/akademy.kde.org/files/2015/akademy2015_wee_1.jpg" /></a><br /><em>Akademy 2015, A Coruña, Galicia, Spain</em></div>

<h2>About Akademy</h2><p>For most of the year, KDE—one of the largest free and open software communities in the world—works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2017">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact The <a href="https://akademy.kde.org/2017/contact">Akademy Team</a>.</p>