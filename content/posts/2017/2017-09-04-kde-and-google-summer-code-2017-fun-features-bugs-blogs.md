---
title: "KDE and Google Summer of Code 2017: Fun, Features, Bugs, Blogs"
date:    2017-09-04
authors:
  - "skadinna"
slug:    kde-and-google-summer-code-2017-fun-features-bugs-blogs
---
While you were enjoying your summer vacation, our <a href="https://summerofcode.withgoogle.com/">Google Summer of Code (GSoC)</a> students were working hard on their projects. They developed new features for KDE software, stomped bugs, wrote blog posts to report on their progress, and still managed to have fun while doing all that. With the final results announcement just around the corner, let's take a look at what the students accomplished in the past three months.
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/kde-gsoc-konqi_0.jpg" width="400" /><br /></figure>

This year, 24 students contributed to more than 20 KDE projects as part of GSoC. As you probably already know, GSoC is a yearly program organized by Google for students from all over the world. The aim of GSoC is to motivate young developers to join open source organizations, and those who successfully complete their project receive a stipend from Google. 

KDE has been <a href="https://community.kde.org/GSoC">participating in GSoC</a> since the very beginning in 2005, and we're proud to say that many of our students remain active contributors and members of the KDE Community.

If you haven't been following our GSoC students' blog updates (a mistake you can easily fix by subscribing to <a href="https://planet.kde.org/">Planet KDE</a>), here's a recap of their activities. Most, if not all of their work will show up as new and improved features in the upcoming versions of KDE software.

<h2>More Power to the Creatives</h2>

Digital artists will be happy to hear that Krita and digiKam received some power-ups from our GSoC 2017 students. <a href="https://anikethfoss.wordpress.com/2017/07/30/gsoc17-week-6/">Aniketh Girish</a> improved the user interface of Krita's Resource Manager, making it easier to create and edit bundles. He also created a dialog that enables interaction and content exchange with the share.krita.org website. 

<figure style="position: relative; left: 50%; padding: 1ex; margin-left: -231px; width: 464px; border: 1px solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-gsoc-krita-downloader.png" /></a><figcaption style="text-align: center; font-size: 9pt">Krita's new content downloader.</figcaption></figure>

<a href="https://akapust1n.github.io/2017-08-26-final-blog-gsoc-2017/">Alexey Kapustin</a> worked on a touchy subject - implementing telemetry into Krita. Of course, this feature will be completely opt-in, and the information collected will help Krita developers understand the behavior and needs of their users. 

<a href="https://tantsevov.wordpress.com/">Grigory Tantsevov</a> developed a watercolor brush engine that emulates the look and behavior of real watercolors, and <a href="https://eliakincosta.github.io/2017-08-28-gsoc-final/">Eliakin Costa</a> worked on making Krita more scriptable to save time on repetitive actions. <figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-gsoc-digikam-face-detection.jpg" width="400" /></a><figcaption style="text-align: center; font-size: 9pt">Choosing a face recognition algorithm in digiKam.</figcaption></figure> <p>Along the way, Eliakin also improved and developed several plugins, including the Document Tools Plugin, Ten Scripts Plugin, and the Last Documents Thumbnails Docker.</p> 

<a href="https://community.kde.org/GSoC/2017/StatusReports/ahmedfathy">Ahmed Fathy Shaban</a> worked on implementing a DLNA server directly into the digiKam core, and <a href="https://yjwudi.github.io/2017/07/26/dnn/">Yingjie Liu</a> achieved 99% face recognition accuracy in digiKam by adding new face recognition algorithms. 

Last but not least, <a href="https://community.kde.org/GSoC/2017/StatusReports/shazaismailkaoud">Shaza Ismail Kaoud</a> created a useful healing clone tool for digiKam. See the tool in action in <a href="https://www.youtube.com/watch?v=kyGM4X3Z-Zs&feature=youtu.be">this video clip</a>.

<h2>Boosting KDE Edu</h2>

Applications for all levels of education, from preschool to PhD, received a boost from GSoC students. Thanks to <a href="https://stefantoncu29.wordpress.com/">Stefan Toncu</a>, users of Minuet can now choose an instrument for exercise visualization, instead of always being stuck with the keyboard. 

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-gsoc-gcompris-family.png" width="400" /></a><figcaption style="text-align: center; font-size: 9pt">GCompris Family activity.</figcaption></figure>

<a href="https://divyam3897.github.io/2017-08-03-GSoC-Month2/">Divyam Madaan</a> and <a href="http://rudranilbasu.me/blog/">Rudra Nil Basu</a> added a bunch of activities to GCompris: Oware, Computer parts, Piano composition and note names, Pilot a Submarine, Family, and Digital Electronics. 

Deeper in the scientific territory, <a href="https://rish9511.wordpress.com/2017/08/27/gsoc-port-of-r-to-qprocess/">Rishabh Gupta</a> ported the Lua, R, and Qalculate backends in Cantor to QProcess, and <a href="https://krajszgsoc.blogspot.hr/
">Fabian Kristof Szabolcs</a> implemented support for live streaming data in LabPlot.

<a href="https://kecsapgsoc2017.blogspot.hr/2017/08/finalizing-gsoc-project-for-kstars.html">Csaba Kertesz</a> worked on modernizing the KStars codebase, and <a href="http://baldi.me/blog/summing-up-gsoc">Cristian Baldi</a> developed a responsive web app for WikiToLearn from scratch. His project also included building offline browsing right into the WikiToLearn website, and allowing Android users to install the website on their phone just like any other regular app. 

<h2>Kirigami Welcomes New Apps</h2>

Speaking of mobile apps, <a href="https://bernkastelsgsoc.blogspot.hr/">Judit Bartha</a> worked on the Android version of Marble Maps. Judit implemented bookmark management and redesigned the app to fit the Material Design guidelines using the Kirigami framework. <a href="https://www.mnafees.me/google-summer-of-code-2017-welcome/">Mohammed Nafees</a> worked on extending Marble Maps to support indoor maps, such as building floor plans.

<figure style="position: relative; left: 50%; padding: 1ex; margin-left: -231px; width: 464px; border: 1px solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-gsoc-marble.png" /></a><figcaption style="text-align: center; font-size: 9pt">Marble Maps for Android: old interface on the left, and the redesigned one on the right.</figcaption></figure>

<a href="http://atulsharma.me/2017/08/24/GSoC-Final-Week-Report/">Atul Sharma</a> ported <a href="https://userbase.kde.org/Koko">Koko</a>, a simple image viewer, to Kirigami. 

<h2>Chatting Anywhere, Anytime</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde-gsoc-ruqola.png" width="400" /></a><figcaption style="text-align: center; font-size: 9pt">Ruqola, the Qt-based client for Rocket.Chat.</figcaption></figure>

Chat applications keep multiplying, and users expect native clients for their Linux desktops. <a href="https://vasudhamathur.wordpress.com/">Vasudha Mathur</a> developed Ruqola, the first generic cross-platform interface to <a href="https://rocket.chat/">Rocket.Chat</a>. She used Kirigami and Qt technologies to shape the application for both desktop and mobile platforms.  

<a href="https://rivadavide.blogspot.hr/2017/08/brooklyn-02-released-ready-for.html">Davide Riva</a> developed a protocol-independent chat bridge that supports IRC, Telegram, and Rocket.Chat, allowing for future expansions thanks to its modularity. The bridge is called Brooklyn, and it is already on its 0.2 release. 

<a href="https://r3wired.blogspot.hr/">Vijay Krishnavanshi</a> and <a href="https://community.kde.org/GSoC/2017/StatusReports/paulolieuthier">Paulo Lieuthier</a> worked on Kopete. Vijay ported the remaining KDE4 parts of Kopete to KF5, and Paulo created a new plugin for chat history management. 

<h2>Making KDE Software Better</h2>

Plenty of improvements have been implemented across the KDE applications ecosystem. <a href="http://blog.chinmoyrp.com/gsoc%20kde/2017/07/24/kio-polkit-support-progress-so-far/">Chinmoy Ranjan Pradhan</a> worked on adding Polkit support to KIO, the system library used by KDE software to access and manage files. Polkit allows non-root users to perform file management operations that would usually require admin privileges. With this feature, opening Dolphin as root should finally become a thing of the past.

<a href="https://community.kde.org/GSoC/2017/StatusReports/LukasHetzenecker">Lukas Hetzenecker</a> examined HiDPI rendering issues in KDE applications (Gwenview, Spectacle, Okular) and set out to fix them. <a href="https://ematirov.blogspot.hr/2017/08/gsoc-week-12.html">Mikhail Ivchenko</a> focused on KDevelop, and worked on stabilizing the support for the Go programming language.

<h2>Looking Forward to Next Year</h2>

Taking part in GSoC is a great opportunity for professional development. In addition to expanding their programming skills, the students earn valuable project management experience, as they are expected to plan and report on every step of their project. 

Despite all those benefits, GSoC is not always so peachy for everyone. Sometimes students encounter code-shattering bugs, or have to rewrite entire software components in another programming language. This is where the mentors step in. Mentors offer guidance when students get stuck, and provide advice in making key decisions. Without their support, GSoC wouldn't be so successful, so here's a big "thank you" to all our GSoC 2017 mentors!

To all our students who passed the final evaluation: Congratulations! We're delighted to have been a part of this journey with you, and we hope you'll stick around in the KDE Community. And if you didn't pass, don't despair. We still greatly value your contribution and effort, and you're more than welcome to keep contributing to KDE.

It's never too early to start preparing for the next Google Summer of Code. If you're a student interested in Free and open source software, <a href="https://community.kde.org/GSoC">join us</a> today!
<!--break-->