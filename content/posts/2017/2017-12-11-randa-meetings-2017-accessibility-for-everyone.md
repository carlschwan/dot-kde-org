---
title: "Looking Back at Randa Meetings 2017: Accessibility for Everyone"
date:    2017-12-11
authors:
  - "skadinna"
slug:    randa-meetings-2017-accessibility-for-everyone
---
<style>
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 50px;
  line-height: 1;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\201C"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 90px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:5px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>

A software developer's life can get lonely. Developers spend a lot of time immersed in their code, and often don't get to see the direct impact of their work on the users. That is why events like Randa Meetings are necessary. They help build bonds within the community and show the developers that their work is appreciated.

<blockquote>Randa Meetings are crucial to the KDE community for developer interaction, brainstorming, and bringing great new things to KDE.  --- Scarlett Clark</blockquote>

Randa Meetings are a yearly collection of KDE Community contributor sprints that take place in Randa, Switzerland. With origins dating back to a Plasma meeting in 2009, Randa is one of the most important developer-related events in the community. 

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/randa-2017-5.jpg"><img src="/sites/dot.kde.org/files/randa-2017-5.jpg" alt="" width="400" /></a><br />View from Randa. Photo by Gilles Caulier, CC BY 2.0</figure></p>

This year, Randa Meetings were held from September 10 to September 16, and were centered around a meaningful theme - accessibility.

<h2>Accessibility in Free and Open Source Software</h2>

Accessibility is incredibly important, yet so often neglected in the process of software development, and implemented almost as an afterthought. Users with disabilities and impairments often find themselves excluded from using the software, and prevented from participating in activities that the rest of us take for granted. Essentially, the software makes them feel as if they don't matter.

To remedy this, many governments enforce laws requiring software to be accessible. There are also accessibility standards, guidelines, and checklists to help developers make their software accessible to all. 

FOSS communities and projects have the potential to play a major role in driving <a href="https://community.kde.org/Accessibility">software accessibility efforts</a> because of their open nature. People with disabilities can communicate directly with developers, report issues, and request features that they need. Proprietary products are rarely this open to feedback, not to mention the fact they are often very expensive. 

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/randa-2017-accessibility-settings.png"><img src="/sites/dot.kde.org/files/randa-2017-accessibility-settings.png" alt="" width="400" /></a><br />Accessibility Settings module in Plasma 5</figure></p>

Assistive technology covers a wide range of products and solutions: from screen magnifiers, screen readers, and text prediction methods to text-to-speech interfaces, speech recognition software, and simplified computer interfaces. There are also advanced solutions like 3D-printed prosthetic limbs, and those that allow controlling the mouse by moving the head or just the eyes.

The best thing about all this technology is that it benefits everyone. Although people usually associate the word "accessibility" with hearing or visually impaired people, assistive technology can make life easier for many other groups: dyslexic or illiterate people, cognitively disabled people, the elderly, anyone with limited mobility or just bad eyesight. 

The analogy is clear with wheelchair-accessible spaces, which are useful to parents with baby strollers, people with bicycles and shopping carts, and delivery drivers. Likewise, improving keyboard navigation, image and color contrast, and text-to-speech tools results in satisfaction among non-disabled users. Making software accessible means making software better. 

<h2>Making KDE Software More Accessible</h2>

Generally speaking, there are two ways to make software accessible: either by building special accessibility-focused tools from scratch, or by implementing accessibility features and improvements into existing applications. The latter is what the Randa Meetings 2017 were all about.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/randa-2017-1.jpg"><img src="/sites/dot.kde.org/files/randa-2017-1.jpg" alt="" width="400" /></a><br />digiKam with David Edmundson from Plasma in the background. <br /> Photo by Gilles Caulier, CC BY 2.0</figure></p>

The developers created a <a href="https://blog.qt.io/blog/2017/09/15/testing-applications-color-blindness/">useful KWin plugin</a> that can simulate different types of color blindness. This will help in all future accessibility efforts, as it helps developers understand what their color schemes will look like to visually impaired users.

KMyMoney was made more accessible via improvements to keyboard navigation. New keyboard shortcuts were added, and others simplified to make them easier to use.

Randa Meetings 2017 were made special by Manuel, a visitor from Italy who stayed at the Randa house during the sprint. Manuel is a deaf user, and he took the time to explain the problems that hearing-impaired users encounter with KDE software, and software in general. His feedback was extremely valuable in the context of the sprint's theme, and helped developers come up with accessibility-oriented solutions.

<blockquote>Meeting in Randa with other participants makes you aware of deficiencies, possibilities, and needs. For a newcomer, like myself, it was also a chance to meet some community members, see what kind of people build KDE software, and take a look behind the scenes. --- Lukasz Wojnilowicz</blockquote>

Apart from fixing individual applications, a <a href="http://blog.davidedmundson.co.uk/blog/plasma-accessibility-randa/">lot of work</a> was done on the Plasma desktop environment itself. Accessibility-related improvements include the ability to navigate the Plasma panel using voice feedback and the keyboard. The following video demonstrates this feature in action:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yhO17uzPqHE" frameborder="0" allowfullscreen></iframe>

KRunner was made <a href="http://notmart.org/blog/2017/09/accessibility-at-randa/">completely accessible</a>, and this change is visible in Plasma 5.11. The Orca Screen Reader works well with KRunner, and can read the entered query, as well as let the user know which element of the interface is currently focused.

There was also a round of discussions on <a href="https://blog.qt.io/blog/2017/09/14/accessibility-improvements-randa/">best practices</a> for accessibility in KDE software. When testing software, the developers should try to use it only with their keyboard, and then only with the mouse. Too much customization is not a good idea, so it should be avoided, especially when it comes to colors, fonts, and focus handling. 

Another good practice is to test the application with a screen reader. This experience should highlight potential issues for disabled users. In the end, it all comes down to empathy - being able to put yourself in the user's shoes, and the willingness to make your software available to as many people as possible, without excluding anyone.

<h2>More Plans and Achievements from the Randa Meetings 2017</h2>

Of course, the developers worked on so much more than just accessibility. The KDE PIM team discussed the results of their KMail User Survey, and tackled the most pressing issue reported by the uses - broken and unreliable search. They also ported the entire Kontact codebase away from the obsolete KDateTime component.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/randa-2017-2.jpg"><img src="/sites/dot.kde.org/files/randa-2017-2.jpg" alt="" width="400" /></a><br />Lukasz Wojnilowicz from KMyMoney and Volker Krause <br />from KDE PIM. Photo by Gilles Caulier, CC BY 2.0</figure></p>

KMyMoney saw some important changes. All plugin KCM modules were ported to KF5. The backup feature is, well, back up and available to use, and database loading was improved so as to prevent the incompatibility between new and old KMyMoney files.

After successfully participating in <a href="https://dot.kde.org/2017/09/04/kde-and-google-summer-code-2017-fun-features-bugs-blogs">Google Summer of Code</a>, the digiKam team gathered in Randa to further polish the new code contributed by students. They also worked on the media server functionality, which allows users to share their photo collections across different devices (smartphones, tablets, TVs...) using DLNA.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/randa-2017-4.jpg"><img src="/sites/dot.kde.org/files/randa-2017-4.jpg" alt="" width="400" /></a><br />Scarlett Clark (KDE CI), Emmanuel LePage (Ring), Simon Frei <br />(digiKam) and David Edmundson discussing Plasma Accessibility. <br />Photo by Gilles Caulier, CC BY 2.0</figure></p>

Marble Maps now has a new splash screen, and the entire interface of the Bookmarks dialog is responsive to touch. There are plans to <a href="https://sanjibandotme.wordpress.com/2017/09/20/randa-2017-report-marble-maps/">implement a better text-to-speech module</a> for navigation.

The Public Transport Plasma applet has been <a href="https://harishnavnit.wordpress.com/2017/09/16/plasma-publictransport-rewrite-part-ii/">completely rewritten as a Kirigami application</a>. The applet's UI is now much more flexible and easier to adapt to mobile devices.

The <a href="https://cmollekopf.wordpress.com/2017/09/20/kube-in-randa/">developers of Kube</a> worked on resolving an annoying issue with live queries which slows down email synchronization, especially when there are thousands of emails. They also discussed the possibility of implementing an HTML composer to edit emails in Kube, and made plans for GPG implementation. In collaboration with developers from other KDE projects, they explored the options for making Kube cross-platform, and looked for the best way to build Kube on macOS and Windows. Finally, they implemented a visualization in the configuration dialog which indicates when the user input is invalid in configuration fields.

Last but not least, Kdenlive received <a href="https://kdenlive.org/2017/09/randa-news-release-update/">color correction improvements</a>, and the developers worked on bringing back the popular TypeWriter effect. They also fixed the import of image sequences, worked on porting Kdenlive to Windows and macOS, and removed the warning about missing DVD tools that used to appear when starting Kdenlive for the first time.

<h2>Looking Forward to the Next Randa Meetings</h2>

With another successful Randa Meetings behind us, we can start planning for the next session. 

If you like the work our developers are doing, you can directly support it by <a href="https://www.kde.org/donations">donating to KDE</a>. You can also contribute to KDE and make an impact on the users by joining our mentored project called <a href="https://dot.kde.org/2017/11/19/announcing-season-kde-2018">Season of KDE</a>. 

Who knows, maybe in the future you too will attend the Randa Meetings!

<figure style="position: relative; left: 50%; padding: 1ex; margin-left: -231px; width: 464px; border: 1px solid grey"><a href="/sites/dot.kde.org/files/randa-2017-group.jpg"><img src="/sites/dot.kde.org/files/randa-2017-group.jpg" /></a></figure>

<!--break-->