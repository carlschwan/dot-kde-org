---
title: "Have You Heard? KDE Applications 17.04 and Plasma 5.9.5 Now Available"
date:    2017-05-02
authors:
  - "skadinna"
slug:    have-you-heard-kde-applications-1704-and-plasma-595-now-available
comments:
  - subject: "Okular improvements"
    date: 2017-05-02
    body: "<p>It's wonderful that Okular recieves some love and gets better.</p><p>Yet we still have the very big problem with Qt not supporting ppd-options for printing anymore!</p><p>This cuts off a very big part of using Okular... printing is \"crippled\" since Qt5. From my POV this is an essential part which is now defunct since a long time. I want to be able to choose from my printers ppd options for a print job (e.g. stapling... color options...etc...). It did work with Qt4, but somehow \"vanished\" with Qt5...</p>"
    author: "thomas"
  - subject: "Dual monitor setup improvements"
    date: 2017-05-02
    body: "<p>Having a Macbook pro laptop with a retina display and a 1080p or a 1440p external display I would love to have different DPI settings on each display. But I'm not sure if this fits KDE or QT!</p>"
    author: "Paulo Fidalgo"
  - subject: "Qt printing"
    date: 2017-05-03
    body: "<p>Yes, this is a known regression. We still hope someone finds the time to submit patches for QPrinter classes (and QLocale classes ;) because the maintainer unfortunately had to resign from the task.</p>"
    author: "cfeck"
  - subject: "Dolphin as root"
    date: 2017-05-03
    body: "<p>Good work, but how do you re-enable running Dolphin and Kate as root? Might be a security hazard but if you know what you're doing you should be able to do so. Browsing the filesystem and editing files can be a PITA otherwise if you have to perform administrative tasks. Thanks!</p>"
    author: "Mario"
  - subject: "Individual wallpapers for each desktop"
    date: 2017-05-04
    body: "<p>Is there any hope that this long-standing bug (a well-known and used KDE4 feature) will be fixed soon?&nbsp;https://bugs.kde.org/show_bug.cgi?id=341143</p><p>Hopefully the amount of feedback given by your users might show you that it's worth spending a little time to restore this feature we've missed for so long. Keep up the great work!</p>"
    author: "Maarten"
  - subject: "PPD print options are broken and nobody seems to care"
    date: 2017-05-04
    body: "<p>The standard answer would be \"file a bug\".<br /><br />Certainly -- but where? bugs.kde.org? Look at how many orphaned bugs and duplicates there are there. This looks like a serious project management problem. We have this: https://community.kde.org/Plasma/Bugtracker which was last updated in 2011. It appears that all the same issues still apply, and it's six years later. How do smaller projects manage this while KDE can't?<br /><br />Or how about over at Qt? https://bugreports.qt.io/browse/QTBUG-54464 - This bug is almost a year old. It's assigned, but there is no sign that any progress has been made on it. \"No work has yet been logged on this issue.\" I know we're all busy people, but seriously?<br /><br />And frankly, why are we still talking about this in 2017? This worked in 4! This functionality is so basic that it should be the first thing that gets addressed in any new design. It is absolutely staggering that this isn't high on the priority list. Printing from Plasma 5 is essentially useless.</p>"
    author: "rhombus"
  - subject: "no dolphin as root"
    date: 2017-05-04
    body: "<p>Really disappointed with this change.&nbsp; I have been using kde since back in the days of kde 3 {suse 8.1} and have now had to install some gnome programmes to edit system files.&nbsp; Please don't turm plasma into a locked-down MS Windows style o/s.</p>"
    author: "dth1"
  - subject: "Root access"
    date: 2017-05-04
    body: "<p>No more graphical root access: because the user is stupid, because the \"administrator\" is protected (he is Administrator, or what else?) again every possible error...</p><p>Why leave it easy, when making it complicated is possible?</p>"
    author: "Frank Einstein"
  - subject: "Allow operations as root"
    date: 2017-05-12
    body: "<blockquote><p><em><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">\"A significant change that affects not only Dolphin, but also Kate and KWrite, is that launching these applications as root on Linux systems has been disabled by default. The reason for this is that it is a safety risk to run GUI apps with root privileges in the X Window System (X11).\"</span></em></p></blockquote><p><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">This is terrible. There may be use-cases where people want to run a desktop as root. Or even \"kdesu dolphin\" or kate. From the post, I understand that there's nothing technical that prevents these applications from running with root privileges. Just that a policy decision has been imposed on the users. This is a terrible decision - along the lines of silly gnome designers. There are genuine use cases for running GUI applications as root. KDE shouldn't impose such restrictions on its users.</span></p>"
    author: "Syam Krishnan"
  - subject: "Okular"
    date: 2017-06-13
    body: "<p><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; background-color: rgba(255, 255, 255, 0.5);\">It's wonderful that Okular <a title=\"recieves\" href=\"unair.ac.id\" target=\"_blank\">recieves</a> some love and gets better.</span></p>"
    author: "sukiyoyo"
  - subject: "inattention to bug reports"
    date: 2017-07-17
    body: "<p>I couldn't agree more.&nbsp; There are a number of bugs in Kontact, and none of them is getting any serious attention from developers.&nbsp; Why can't KDE focus on attending to these problems with the Kontact suite before traipsing off to new projects?&nbsp; If Kontact were bug-free, it would be a much better sales agent for KDE than more bells and whistles on the desktop.</p>"
    author: "Ken Freeland"
  - subject: "Really though, sudo access for dolphin/kate is crazy useful."
    date: 2017-09-28
    body: "<p>Even if we have to go find a config file somewhere to allow starting dolphin as root, the option would be very much appreciated.</p>"
    author: "John"
  - subject: "Running dolphin/kate as root"
    date: 2017-12-15
    body: "<p><span style=\"font-size: small;\">See a few people asking about this, and when you have a few asking there are thousands looking, but no information forth coming.&nbsp; How can we run these programs as root?</span></p>"
    author: "Robert"
---
The last two weeks have been busy for the KDE Community. On April 20 we announced the release of <a href="https://www.kde.org/announcements/announce-applications-17.04.0.php">KDE Applications 17.04</a>, and five days later we released a new set of bugfixes and translations for Plasma, officially versioned <a href="https://www.kde.org/announcements/plasma-5.9.5.php">Plasma 5.9.5</a>.

Both new versions of our products have introduced several features and stability improvements to the overall KDE user experience. Here are some of the highlights from the latest KDE Applications and Plasma releases. As always, you can find a lot more information in their respective changelogs. 

<h2>What's New in File Management?</h2>

If <a href="https://www.kde.org/applications/system/dolphin/">Dolphin</a> is your file manager of choice, you will be happy to hear that it now allows you to interact with the metadata widgets in the tooltips. The Places panel now has better context menus, and opening a new instance of Dolphin using the "New Window" option will launch it in the same target folder as your current Dolphin window. 
<figure style="padding: 1ex; margin: auto; border: 1px solid grey; width: 530px;"><a href="/sites/dot.kde.org/files/dolphin1704.png"><img src="/sites/dot.kde.org/files/dolphin1704-small.png" /></a></figure> 

A significant change that affects not only Dolphin, but also Kate and KWrite, is that launching  these applications as root on Linux systems has been disabled by default. The reason for this is that it is a safety risk to run GUI apps with root privileges in the X Window System (X11). 

When it comes to viewing your files, <a href="https://okular.kde.org/">Okular</a> will be even better at it thanks to numerous improvements. You can now create bookmarks from the <i>Table of Contents</i>, resize annotations, and disable automatic search while typing.

Finally, <a href="https://www.kde.org/applications/utilities/ark/">Ark</a> - the application that lets you manage compressed files and folders - now has a handy plugin configuration dialog and a Search function to help you look inside your archives.
<figure style="padding: 1ex; margin: auto; border: 1px solid grey; width: 530px;"><a href="/sites/dot.kde.org/files/ark1704.png"><img src="/sites/dot.kde.org/files/ark1704-small.png" /></a></figure> 

<h2>What About Multimedia Applications?</h2>

The biggest improvements in the multimedia department will be visible in <a href="https://kdenlive.org/">Kdenlive</a>, KDE's video editor. The profile selection dialog has been fully redesigned, and it is now much easier to tweak the framerate, screen size, and other details of your project. Perhaps the coolest new feature in Kdenlive is the ability to play your video directly from the notification you receive when rendering is completed. 
<figure style="padding: 1ex; margin: auto; border: 1px solid grey; width: 530px;"><a href="/sites/dot.kde.org/files/kdenlive1704.png"><img src="/sites/dot.kde.org/files/kdenlive1704-small.png" /></a></figure> 

Other multimedia applications received some minor improvements, for example <a href="https://www.kde.org/applications/graphics/gwenview/">Gwenview</a> now lets you hide the status bar in the application window. 

<h2>Don't Forget About KDE Edu!</h2>

Our educational applications have seen some interesting changes. <a href="https://www.kde.org/applications/education/kalgebra/">KAlgebra</a> - the powerful graphing calculator and math-learning tool - has a new 3D back-end on the desktop, and its mobile version has been ported to <a href="https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui">Kirigami 2.0</a>. 

If you love music more than math, the new version of <a href="https://www.kde.org/applications/education/minuet/">Minuet</a> will delight you. The music education tool now comes with more scale exercises and ear-training tasks, plus an entire Test Mode for practicing and monitoring your progress. 
<figure style="padding: 1ex; margin: auto; border: 1px solid grey; width: 530px;"><a href="/sites/dot.kde.org/files/minuet1704.png"><img src="/sites/dot.kde.org/files/minuet1704-small.png" /></a></figure> 

<a href="https://www.kde.org/applications/education/kstars/">KStars</a>, our desktop planetarium, will now work much better on OS X, and <a href="https://www.kde.org/applications/education/kgeography/">KGeography</a> now includes a map of Ghana. 

<h2>New Members of the KDE Applications Family</h2>

We are happy to announce that <a href="https://www.kde.org/applications/multimedia/k3b/">K3b</a>, the disk burning software, is now part of KDE Applications. In other great news, several applications have been ported from their old kdelibs4 base to KDE Frameworks 5. The list includes KCachegrind, Kajongg, kde-dev-utils and kdesdk-kioslaves.

No longer included in KDE Applications is the unmaintained development tool <a href="http://kdewebdev.org/kommander/">Kommander</a>.

<h2>What About the New Plasma?</h2>

The most obvious changes introduced in Plasma 5.9.5 are related to window decorations and other visual tweaks. Themes in the System Settings module are now sorted, Plastik window decoration supports the global menu, and Aurorae window decorations support the global menu button. KWin will respect theme colors in buttons, and you will be able to edit the default color scheme of your Plasma Desktop.

Moreover, your Plasma session will correctly handle the event of disconnecting a primary screen and replacing it with a new one. The Media Controller Plasmoid has been fixed, and can now properly seek tracks longer than 30 minutes. 

<h2>Where Can You Get All These New Things?</h2>

Both KDE Applications 17.04 and Plasma 5.9.5 are available in <a href="https://neon.kde.org/download">KDE neon</a>. Linux distributions are expected to provide packages or update their existing ones in the coming weeks. Users of Arch Linux, Manjaro Linux, and Gentoo should already see our latest software in their repositories.

If you can't wait for your distribution's packages, you can always download our source code and compile it yourself. We provide build instructions for both <a href="https://www.kde.org/info/applications-17.04.0.php">KDE Applications</a> and <a href="https://www.kde.org/info/plasma-5.9.5.php">Plasma</a>. 

<h2>What's Next?</h2>

Plasma 5.10 is expected at the end of May. If you have been following our developers' blogs, you might be aware of some <a href="http://blog.broulik.de/2017/03/whats-up-for-5-10/">upcoming features</a>. Folder View will have a much <a href="https://blogs.kde.org/2017/03/01/plasma-510-folder-view-default-desktop-mode">more prominent role</a> on the Plasma Desktop, and it will include practical <a href="https://blogs.kde.org/2017/01/31/plasma-510-spring-loading-folder-view-performance-work">spring-loading navigation</a>.

A lot more is in the works, and we will reveal some of the novelties as the release date approaches. Make sure to follow us on <a href="https://twitter.com/kdecommunity">Twitter</a>, <a href="https://www.facebook.com/kde/">Facebook</a>, and <a href="https://plus.google.com/105126786256705328374/posts">Google+</a> to keep up with all the news. If you're planning to celebrate the release of Plasma 5.10 by hosting a release party, start preparing now! Our Community Wiki has some tips on <a href="https://community.kde.org/Promo/Events/Parties">how to organize a local KDE event</a>. 

In the meantime, let us know about your experience with KDE Applications 17.04 and Plasma 5.9.5 in the comments!
<!--break-->

