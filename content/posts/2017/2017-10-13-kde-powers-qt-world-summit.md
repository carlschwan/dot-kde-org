---
title: "KDE Powers up the Qt World Summit"
date:    2017-10-13
authors:
  - "Paul Brown"
slug:    kde-powers-qt-world-summit
comments:
  - subject: "Pinebook KDE"
    date: 2017-10-15
    body: "<p>Looks like great times!</p><p>Curious about what KDE distro you ran on the Pinebook. Did you have to do any further customization here to get it working smoothly?</p><p>Have got a Pinebook myself currently running Mate and would love to switch it over to KDE if I can.</p><p>Thanks to everyone there for all their hard work.</p>"
    author: "jim.campos"
---
<strong>The motto of our space at QtWS this year has been "Power up!". We put it into practice in more than one way and in the most literal of senses.</strong>
<br />
<iframe src="https://www.youtube.com/embed/h5D7V8Dmnwg" frameborder="0" width="560" height="315" style="float: center; clear: both;"></iframe>
<br />
<!-- <p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/Powerup.jpg"><img src="/sites/dot.kde.org/files/Powerup.jpg" alt="" width="300" /></a></figure></p>-->

First we designed our allocated space so that attendees could come, sit and relax, and recover their energies. We made sure there was ample sitting space with comfy cushions in an open and informal atmosphere.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/TeamKDE.jpg"><img src="/sites/dot.kde.org/files/TeamKDE.jpg" alt="" width="300" /></a><br />Team KDE.</figure></p>

We also wanted to make it easy for visitors to power up their devices, so we placed plugs and USB charging stations all over our booth. Our visitors came, sat, chatted, re-charged their bodies, minds and devices, while at the same time finding out why KDE is the driving force behind many a software project. This turned out to be winning idea. A lot of people came by the "Power up!" space, and the buzz gave us the chance to demonstrate exactly how KDE could also power up their software and hardware projects. Many still perceive KDE exclusively as the creator of a desktop, but, at the ripe age of twenty, KDE is much more than that.

<p><figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/USB.jpg"><img src="/sites/dot.kde.org/files/USB.jpg" alt="" width="200" /></a><br />Visitors could power up<br />in more ways than one.</figure></p>

<a href="https://20years.kde.org/book/">Twenty years of development</a> means that KDE has made many different kinds of software. Primary device UI, end-user apps, communication apps, business apps, content creation apps, mobile apps, and on and on. This means we have had to solve many problems and create many libraries in the process. Our libraries complement Qt and are very easy to use by any Qt-based application. Many have few or no dependencies aside from Qt itself. These libraries are free to use and licensed in a way that is compatible even with commercial apps. They also run on many different platforms.

To leverage all the libraries and frameworks we have created, we have also built many development tools, including a full IDE that supports both static and dynamic languages (<a href="https://www.kdevelop.org">KDevelop</a>), an advanced editor especially designed for developers (<a href="https://kate-editor.org">Kate</a>), debugging tools (<a href="http://www.kdbg.org/">Kdbg</a>, <a href="https://www.linux-apps.com/p/1127160/">Massif Visualizer</a>), etc. They all support Qt and C++ and again run on a variety of platforms.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/photo_2017-10-11_12-14-08.jpg"><img src="/sites/dot.kde.org/files/photo_2017-10-11_12-14-08.jpg" alt="" width="200" /></a><br />Plasma Mobile running on<br />Nexus 5x at QtWS 2017.</figure></p>

Our most valuable asset is our community. The KDE community is the real power behind KDE's projects. The community fosters personal and professional development, helping programmers become better Qt developers in a welcoming environment. Also, just by contributing to KDE, you get to help us decide where we should take our projects next and help us keep KDE code up-to-date and secure.

To prove our point, we had on display two examples of how KDE powers much more than desktop devices. We showed off the <a href="https://www.pine64.org/?page_id=3707">Pinebook</a> running Plasma Desktop. The Pinebook is a low-cost ultra-netbook (only $99 for the 14'' version) built around the Pine, an ARM-based 64 bit single board computer -- similar to a the Raspberry Pi, but more powerful. The Pinebook is not only a good example of a cheap machine you can take anywhere, but also of how KDE technologies can provide a full-fledged working environment on all sorts of devices.

To drive the matter home even more, visitors were also able to play with <a href="https://plasma-mobile.org/">Plasma Mobile</a>, our environment for smartphones. Plasma Mobile has been in the news recently thanks to the fact that Purism, manufacturers of high-end laptops that come with Linux pre-installed, and KDE have agreed to work together on the <a href="https://puri.sm/shop/librem-5/">Librem 5</a>, an open and privacy-respecting smartphone. As the Librem 5 hasn't been built yet, at QtWS 2017 we showed how Plasma Mobile works fine on an off-the-shelf device; in this case, a Nexus 5x. Plasma Mobile running on an actual device is living and breathing proof of the power KDE delivers to developers.

Thanks to <a href="https://halium.org/">Halium</a>, for example, you can sit different graphical environments (including Plasma Mobile) on top of an Android base, and Halium will manage communication between the graphical environment and the kernel. Then we have <a href="https://techbase.kde.org/Kirigami">Kirigami</a>, a framework that helps developers create apps that will work within all sorts of environments, not only on the Plasma Desktop. With Kirigami, you can deliver apps to the two Plasmas, Desktop and Mobile, Windows, MacOS X, Android, and iOS.

These powerful technologies are developed and maintained by KDE, and are examples of how KDE can power up your projects.
<!--break-->