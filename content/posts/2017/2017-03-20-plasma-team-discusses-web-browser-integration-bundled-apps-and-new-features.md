---
title: "Plasma Team Discusses Web-browser integration, Bundled Apps and new Features"
date:    2017-03-20
authors:
  - "sebas"
slug:    plasma-team-discusses-web-browser-integration-bundled-apps-and-new-features
comments:
  - subject: "Browser integration"
    date: 2017-03-21
    body: "<p>Thanks plasma heroes for your continuous efforts ! I wish you the best :)<br />About the browser integration, is it planned (or even feasible ?) to use native KDE Open/Save dialogbox in Firefox or Chrome ?</p><p>&nbsp;</p>"
    author: "Heller"
  - subject: "Compiz"
    date: 2017-03-21
    body: "<p>Still haven't found time to discuss the broken cube since KDE 4? You should probably fix that before anything</p>"
    author: "peeto"
  - subject: "Please fix problems in"
    date: 2017-03-21
    body: "<p>Please fix problems in current web browser plasmoid https://bugs.kde.org/show_bug.cgi?id=371023 - it is totally unusable in desktop!</p>"
    author: "Murz"
  - subject: "For Firefox on Ubuntu you can"
    date: 2017-03-21
    body: "<p>For Firefox on Ubuntu you can use Plasmazilla PPA https://launchpad.net/~plasmazilla/+archive/ubuntu/releases for add KDE dialogs in Firefox</p>"
    author: "Murz"
  - subject: "There\u00a0are native ones, but"
    date: 2017-03-21
    body: "<p>There&nbsp;<strong>are</strong> native ones, but GTK-native. Switch to qT dialogs would require Mozilla and Google to change their implementations.</p>"
    author: "marsjaninzmarsa"
  - subject: "Where are results of your discussion published?"
    date: 2017-04-11
    body: "<p>Hey guys,</p><p>&nbsp;</p><p>did you publish any of your discussion regarding browser integration? If so, where did you publish it? Thanks in advance.</p>"
    author: "rockiger"
  - subject: "Desktop"
    date: 2017-04-18
    body: "<p>To all the kde team, congratulations for the incredible work on plasma!</p><p>A desktop made for pleasure!</p>"
    author: "A.X."
---
In February, KDE's Plasma team came together in for their yearly in-person meeting. The meeting was kindly hosted by <a href="http://www.vonaffenfels.de/">von Affenfels GmbH</a>, a webdesign agency in Stuttgart, Germany. The team discussed a wide variety of topics, such as design, features new and old, bugs and sore points in the current implementation, app distribution, also project management, internal and outward-facing communication and Wayland.

<figure style="padding: 1ex; margin: auto; border: 1px solid grey; width: 530px;"><img src="/sites/dot.kde.org/files/plasma-team.jpg" width="530" height="358" /><br /><figcaption>Plasma team at the sprint: Clemens, Ronald, Martin, Kai Uwe, Sebas, Jonathan, Martin, David, Bhushan, Dan, Aleix, Roman, Ken</figcaption></figure> 

<h2>New features...</h2>

KDE is experimenting with new ways to <strong>deploy applications</strong>. Under consideration are technologies such as Flatpak, Snap and AppImage, which all have their distinct advantages. Support for bundled applications is being built into Discover, Plasma's software management center, and the <a href="https://store.kde.org">KDE Store</a>. An idea is to allow software developers more control over their applications' lifecycle, and to get updates shipped much quicker into the hands of users. Similar as with packages automatically created from our Git code repositories. This can dramatically cut down on the complexity of the deployment chain.

<strong>Browser integration</strong> in Plasma will be improved by integrating notifications and download progress and multimedia natively into Plasma by providing a browser extension that relays this information to the Plasma shell.

The Plasma team also discussed using <strong>touchpad gestures</strong> to control the window manager, so users can use specific multitouch gestures to trigger effects like the "desktop grid", "present windows" or swiping between virtual desktops.

<h2>Plasma Mobile Ported to Nexus 5X</h2>
Plasma Mobile, KDE's ongoing product to provide a Plasma implementation suitable for mobile phones was made to run on the Nexus 5X. The previous reference device, the Nexus 5 (sans "X") was getting a bit dated, and since it's not easily available on the market anymore, a new reference device that people can get their hands on was needed. Bhushan Shah solved the last problems keeping us from using this newer and faster device as a development platform. Images will be appearing shortly, and the team is looking forward to receiving (and addressing) feedback about Plasma on the 5X.


<h2>New Website</h2>

While not strictly Plasma, the team made a final push to getting KDE's websites at <a href="https://www.kde.org">www.kde.org</a> updated. A tireless effort by Ken Vermette with the help of Harald Sitter and a few more helping hands lead to the <a href="https://dot.kde.org/2017/02/11/beautiful-new-design-kdeorg">shiny new design</a> being revealed during the course of the sprint.

<h2>Soft Internals</h2>

On the less technical side, a sprint such as this is always a good opportunity to talk about how we work together, and how we present ourselves to the outside world. While we have made great strides to improve our software by applying more thorough review processes, continuous testing and integration and paying more attention to the wishes and problems of our users, we want to put more focus on stability. One way to achieve this is to move bigger feature merges more towards the beginning of a development cycle, thereby increasing the amount of time we have for testing and ironing out problems.

<h2>Thanks!</h2>

Sprints like this are only possible with the support of our community. We would like to thank the KDE e.V. for making this sprint (as many others before) possible. A special note of appreciation goes out to all those who <a href="https://www.kde.org/donations">donated to KDE e.V.</a>, without your support, we cannot get together in person to discuss and work. Personal interaction, while not necessary on a daily basis helps us to improve our collaboration, communication, team-work, and not at least the software we create for our users.

<h2>Linux Action Show</h2>
The Linux Action Show did an interview with the team at the sprint, watch this episode from 5 minutes in to meet the crew.
<video id="video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="none" width="854" height="506" poster="http://jb7.cdn.scaleengine.net/wp-content/uploads/2017/02/las456-v.jpg" data-setup='{ "plugins" : { "resolutionSelector" : { "default_res" : "SD" } } }' > <source src="http://www.podtrac.com/pts/redirect.webm/201406.jb-dl.cdn.scaleengine.net/las/2017/linuxactionshowep456-432p.webm" type="video/webm" data-res="WEBM" /> <source src="http://www.podtrac.com/pts/redirect.mp4/201406.jb-dl.cdn.scaleengine.net/las/2017/linuxactionshowep456-432p.mp4" type="video/mp4" data-res="SD" /> <source src="http://www.podtrac.com/pts/redirect.mp4/201406.jb-dl.cdn.scaleengine.net/las/2017/linuxactionshowep456.mp4" type="video/mp4" data-res="HD" /></video>

<!--break-->

