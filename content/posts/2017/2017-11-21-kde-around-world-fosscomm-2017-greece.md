---
title: "KDE Around the World: FOSSCOMM 2017, Greece"
date:    2017-11-21
authors:
  - "unknow"
slug:    kde-around-world-fosscomm-2017-greece
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href=""/sites/dot.kde.org/files/fosscomm-2017-intro.jpg"><img src="/sites/dot.kde.org/files/fosscomm-2017-intro.jpg" alt="" width="400" /></a><br />FOSSCOMM logo.<br />Source: Official FOSSCOMM <a href="https://www.facebook.com/pg/fosscomm2017">Facebook page.</a></figure></p>

On the 4th and the 5th of November, the FOSSCOMM 2017 conference took place at Harokopio University of Athens, Greece. The KDE Community had a presence at the conference. Our Greek troops gave a talk on Sunday about the past, present and future of KDE, focusing on the vision of the community.

<h2>What is FOSSCOMM?</h2>

<a href="https://www.fosscomm.hua.gr/">FOSSCOMM</a> (Free and Open Source Software Communities Meeting) is an annual conference about free software organized by the Greek Free Software community. The main purpose of the conference is the promotion of free software, as well as social interaction between community members. During the first weekend of November, a great number of free software contributors and advocates gathered in Athens to discuss many interesting topics related to free software and open standards. 

In addition, several Greek software communities promoted their work and values at the booths and by hosting workshops. The conference was a success, since a lot of people participated in various conference activities, confirming the significant impact of freedom and openness to the Greek society, especially among young people.</p>

<h2>KDE at FOSSCOMM</h2>

<figure style="float: left; padding: 1ex; margin: 1ex 1ex 1ex 0ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/fosscomm-2017-speaker2.jpg"><img src="/sites/dot.kde.org/files/fosscomm-2017-speaker2.jpg" alt="" width="300" /></a><br /></figure>

Throughout the weekend, our KDE Community members talked about KDE-related topics, especially at the Nextcloud booth. We also sought out actions that could promote the KDE Community in Athens.

On Sunday afternoon, we gave a presentation about KDE in the main amphitheater of Harokopio University. After listing the milestones from the 20 years of KDE history, we defined what KDE represents today, and briefly presented the software that the KDE Community creates.

Afterwards, we analyzed <a href="https://dot.kde.org/2016/04/05/kde-presents-its-vision-future">KDE Vision</a>. We discussed the reasons that led to its creation and the purpose that it serves, focusing on the social impact of our work and our community values. 

Then, after mentioning <a href="https://vizzzion.org/blog/2017/07/plasmas-vision/">Plasma’s vision</a> and familiarizing the FOSSCOMM audience with Plasma’s approach of “convergence”, we introduced <a href="https://plasma-mobile.org">Plasma Mobile</a> as well as Kirigami’s <a href="https://community.kde.org/KDE_Visual_Design_Group/KirigamiHIG">human interface guidelines</a>.</p>

You can find the <a href="https://share.kde.org/index.php/s/MVZriUwRO9P3rp1">slides from our presentation here</a>.

<h2>Creating a Telegram Group</h2>

Regarding our initiative to improve the impact of KDE in Greece, we created the <a href="https://t.me/kdeel"><strong>KDE el</strong> Telegram group</a> . Before concluding our talk, we suggested to everyone interested in KDE values and Plasma software to participate in the newly created group. 

We hope that this communication channel will constitute a starting point for the KDE Community in Greece to come together, communicate their contributions, and promote KDE and free software in general. 

If you speak Greek and you care about free and open source software, <a href="https://t.me/kdeel">you're welcome to join</a>!</p>

<h3>Important Update<h3>

Inspired by this post and by the creation of the Telegram group, several Greek-speaking KDE users and contributors decided to collaborate on developing a stronger KDE community among the Greek-speaking audience, and further promote KDE's work. After a fruitful discussion, the team decided to facilitate participation from additional media that correspond to our values, as this can help in reaching a wider community.

As a result, we created the 'kde-el' <a href="https://matrix.org/">Matrix</a> room. 
The existing kde-el Freenode IRC channel is now bridged to both the Matrix and the Telegram groups.

<strong>The Greek-speaking KDE team can now be reached via:</strong>

Matrix: #kde-el:matrix.org
ΙRC: #kde-el at freenode
Telegram:  <a href="https://t.me/kdeel">KDE el</a>

Just choose the one you prefer!

For more details, see <a href="https://opensource.ellak.gr/2017/12/14/kde-el-mia-kenouria-archi/">the official announcement</a> from our team.

<h2>Wrapping Up</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/fosscomm-2017-presentation.png"><img width="300" src="/sites/dot.kde.org/files/fosscomm-2017-presentation.png" alt="" width="600" /></a><br /></figure></p>

Although the turnout on Sunday was not as high as on Saturday, many students, contributors and free software enthusiasts attended our talk. Since the timeline was quite strict, there was no time for questions from the audience.

Nevertheless, the impact of our talk was quite positive, since several people approached us looking for more information about the material we presented. According to the feedback from attendees, the decision to introduce a privacy- and freedom-oriented mobile paradigm seems to be more than welcome. 

Participating in meetings like FOSSCOMM is quite helpful for KDE as well as for the free software community in general. Talking with people from different communities, getting introduced to interesting open projects, and envisaging a world of freedom is always a revitalizing and encouraging way to keep going! Finally, we would like to thank FOSSCOMM volunteers and organizers for being so helpful and managing to host quite a successful event. 

See you at FOSSCOMM 2018!
<!--break-->