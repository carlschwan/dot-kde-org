---
title: "What to Expect from Akademy 2017 on Day 1"
date:    2017-07-18
authors:
  - "Paul Brown"
slug:    what-expect-akademy-2017-day-1
---
<a href="/sites/dot.kde.org/files/banner2_1500.png">
	<img style="display: block; margin: auto;" src="/sites/dot.kde.org/files/banner2_1500.png" width="600" alt="I'm going to Akademy" />
</a>

There's less than a week until the beginning of <a href="https://akademy.kde.org/2017">Akademy 2017</a> (if you still haven’t registered, <a href="https://events.kde.org/">do so now</a>) and this is what you can expect from your <a href="https://conf.kde.org/en/akademy2017/public/schedule">first day at the event</a>:

<h2>Keynotes and Master Talks</h2>

Akademy opens on <a href="https://conf.kde.org/en/akademy2017/public/schedule/2017-07-22">Saturday, July 22</a> at 10 am with Robert Kaye, the brains behind Musicbrainz. <a href="https://dot.kde.org/2017/06/18/robert-kaye-music-buff-entrepreneur-akademy-keynote-speaker">We talked with Robert a few days ago</a>, and he will tell us all about his projects and how he managed to marry FLOSS activism with the pragmatism of having to make money in order to keep them alive.

<a href="https://conf.kde.org/en/akademy2017/public/events/381">Sebastian Kügler will follow with an overview of the most important things that happened over the last year in the development of Plasma</a>. He will talk about current features, future plans and goals, what to expect on your desktop over the next year, and how to help and get involved.

Meanwhile, in the next room, <a href="https://conf.kde.org/en/akademy2017/public/events/371">Jos van den Oever will examine Calligra and its native support for ODF</a>. He'll look at a number of areas of ODF and see how well they are supported compared to other office suites.

At 11:50, Mirko Boehm will review the governance norms applied in FSFE, KDE and Wikimedia in his talk <a href="https://conf.kde.org/en/akademy2017/public/events/369"><I>Why we Fight</I></a>. He will examine how the norms developed over time and how current debates reflect their evolution.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/slide1.png"><img src="/sites/dot.kde.org/files/slide1.png" /></a><br /><figcaption>Kdenlive, KDE's video editor, now comes with a new, re-vamped user interface.</figcaption></figure>

At the same time, <a href="https://conf.kde.org/en/akademy2017/public/events/335">Volker Krause will present the UserFeedback</a> framework, which provides ways to engage users from inside the application itself, including the collection of system or usage statistics, as well as asking an interested set of users that match a specific set of criteria to participate in an online survey.

Continuing on with a similar topic, at 12:30 <a href="https://conf.kde.org/en/akademy2017/public/events/354">Aleix Pol will talk about the challenge of developing for users employing bundled systems</a>. We'll see what impact shortening the path between the development and users being able to run the software will have.

At the same time, <a href="https://conf.kde.org/en/akademy2017/public/events/394">Emma Gospodinova will tell us how she plans to add support for Rust, the promisingly popular programming language, to KDevelop</a> during her Google Summer of Code project. Emma plans to include standard features any IDE should support for a language, such as semantic highlighting, code completion, refactoring, debugging and project management.

From there, we will move onto the light entertainment, which is movies. Or more like movie-editing. In <a href="https://conf.kde.org/en/akademy2017/public/events/374"><I>Kdenlive, rewriting the timeline</I></a>, Jean-Baptiste Mardelle will show us the new, polished Kdenlive 17.08, which now uses QML for many parts of the UI. 

On a more technical note, <a href="https://conf.kde.org/en/akademy2017/public/events/331">Ivan Čukić will talk about how functional programming can improve our day-to-day work</a>, make our code safer, cleaner and more correct.

<h2>Lightning Talks</h2>

After lunch, at 15:30, we'll have a bunch of lightning talks. <a href="https://conf.kde.org/en/akademy2017/public/events/334">The first one will be about Mycroft</a>, the Alexa-like AI, and Aditya Mehra will explain how you can turn it into a Plasma widget and really enhance your life by having something you can boss about.

<a href="https://conf.kde.org/en/akademy2017/public/events/336">Volker Krause will then take the stage and tell us all about KF5::SyntaxHighlighting</a>, a syntax highlighting engine that was originally tied to Kate, but can now be used anywhere. 

Then Albert Astals Cid is up, and <a href="https://conf.kde.org/en/akademy2017/public/events/379">he will explain the work being carried out on Clazy</a>, a compiler plugin which allows Clang to understand Qt semantics.

<a href="https://conf.kde.org/en/akademy2017/public/events/378">Marco Martin will then have ten minutes to explain how the feedback generated from the design and implementation of applications significantly improved the quality of Kirigami</a>, KDE's user interface framework for developing applications that work both on mobile and desktop computers.

Finally, <a href="https://conf.kde.org/en/akademy2017/public/events/350">Vasudha Mathur will talk about Ruqola</a>, the first generic chat application based on Rocket.Chat. Ruqola is a Qt/QML/C++ app and provides multi-platform portability. Ruqola will currently run on both desktop and mobile (Android) platforms.

<h2>... Back to Regular Talks</h2>

At 16:30, <a href="https://conf.kde.org/en/akademy2017/public/events/355">Sandro Andrade will be talking about preliminary implementation of a modular and flexible framework for building Qt mobile applications</a>. He will also explain how you can use code generators and a plugin-based architecture to automate the implementation of recurrent tasks.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/chromium_extension.png"><img src="/sites/dot.kde.org/files/chromium_extension.png" /></a><br /><figcaption>Babe allows you to add music from multiple sources, including YouTube.</figcaption></figure>

At the same time and next door, <a href="https://conf.kde.org/en/akademy2017/public/events/362">Camilo Higuita will be introducing Babe</a>, a contextual multimedia desktop app. Babe uses online resources and AI to find relationships between the music metadata and its context in order to generate personalized queries and suggestions.

Lydia Pintscher and the rest of the KDE e.V.  Board will then sit down for an <a href="https://conf.kde.org/en/akademy2017/public/events/333">Ask Us Anything</a> session with the audience at 17:10. If you want to find out what the board really gets up to and hear the plans for KDE as a community moving forward, here's your chance.

Meanwhile, <a href="https://conf.kde.org/en/akademy2017/public/events/329">Dmitri Popov will be teaching you how to take your digiKam skills to the next level</a> by mastering its advanced functionality. Dmitri's talk will introduce several useful features and tools, such as filtering, batch processing, and curve presets.

At 17:55, <a href="https://conf.kde.org/en/akademy2017/public/events/343">John Samuel will be talking about Wikidata</a> and how it can play an important role for the visibility of KDE applications. He will show how developers can build tools to integrate their applications with Wikidata to present an up-to-date view of their applications and their cool features.

At the same time, <a href="https://conf.kde.org/en/akademy2017/public/events/337">Arnav Dhamija will introduce you to the KIO (KDE Input Output) library</a>. KIO is what allows your KDE apps to access data from a number of different protocols, such as local file systems, ssh, https, samba shares, ftp, and network file systems. Arnav will explain the need for KIO, how KIO works, KIO slaves, and how to develop for the same.

At 18:35 <a href="https://conf.kde.org/en/akademy2017/public/events/349">Timothée Giet will be taking us down the long road to GCompris-qt 1.0</a>. GCompris, the collection of educational games and activities for children, has finally officially released the new Qt-based version. Timothée will show us the progress the team has made to get there, as well as some shiny new activities.

In the next room, <a href="https://conf.kde.org/en/akademy2017/public/events/380">David Edmundson will be explaining the <I>Binding loop detected for property "title"" error</I></a>, an annoying and cryptic error everyone developing QML has experienced at some point or another. He will talk about what this warning really means and how you can tackle even the most complicated loops.

<h2>... And a last Blast of Lightning Talks</h2>

At 19:15 we'll have the last three Lightning talks of the day. First up will be <a href="https://conf.kde.org/en/akademy2017/public/events/357">Agustín Benito with his <I>Opening new doors</i> presentation</a>, in which Agustín will explain why he thinks KDE should jump into the embedded-for-automotive fray. Should he have called his talk <I>Opening <B>car</B> doors</I>? Definitely.
 
Then <a href="https://conf.kde.org/en/akademy2017/public/events/375">Annu Mittal will talk about all the application domains and various programs currently running in KDE</a>, namely: Season of KDE, Summer of Code, and Outreach Program for Women. She will follow up by explaining the various ways you can get involved with KDE, both from the technical and non-technical point of view.

Finally, <a href="https://conf.kde.org/en/akademy2017/public/events/383">yours truly will help you look for love (for your projects)</a> by explaining in ten minutes flat three simple steps that will improve your communication and increase your audience's appreciation for your project.

... And that is just day one.

<a href="https://events.kde.org/">Register here and don't miss Akademy 2017</a>, one of the most important Free Software conferences this year.

<!--break-->