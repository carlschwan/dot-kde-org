---
title: "Private Internet Access becomes a KDE Patron"
date:    2017-10-16
authors:
  - "unknow"
slug:    private-internet-access-becomes-kde-patron
comments:
  - subject: "Nitpick: SUSE, not SuSE"
    date: 2017-10-17
    body: "<p>It hasn't been like that for a few years.</p>"
    author: "Luca Beltrame"
  - subject: "Would it be also possible"
    date: 2017-10-17
    body: "<p>Would it be also possible what support means exactly? Ok, I assume financial, but what about the KDE infrastructure? Any particular improvements?</p><p>&nbsp;</p><p>E. g. reading the above makes me wonder if KDE-vpn support would see improvements.</p>"
    author: "mark38"
  - subject: "re: Would it be also possible"
    date: 2017-10-17
    body: "<p>The KDE Board approves all financial expenditures, so infrastructure would be included in that, if needed.&nbsp;</p>"
    author: "Saline Coctails"
---
<a href="https://www.privateinternetaccess.com/">Private Internet Access</a> is joining <a href="https://ev.kde.org/supporting-members.php">KDE as a Patron</a> and pledges to support the work of KDE e.V. through the corporate membership program.

<p><figure style="adding: 1ex; margin: 1ex; "><a href="/sites/dot.kde.org/files/PIA_logo.png"><img src="/sites/dot.kde.org/files/PIA_logo.png" alt="" width="100%" /></a></figure></p>

"We are very happy to have the Private Internet Access/London Trust Media as a KDE Patron and KDE e.V. Advisory Board member. The values of Internet openness are deeply rooted in both organisations, as well as those of privacy and security. Working together will allow us to build better systems and a better Internet for everyone", said Aleix Pol Gonzalez, Vice-President of the KDE e.V.

"Private Internet Access is highly committed to giving back to those communities that have helped the brand and its parent company get to where it is today, and we are very much aware that vast proportions of the infrastructure we use on a daily basis, in the office and at home, is powered by Free and Open Source Software. We have made a pledge to show our gratitude by supporting FOSS projects to help encourage development and growth. We are proud to be supporting KDE and the crucial work that the project does for the Linux Desktop" said Christel Dahlskjear, Director of Sponsorships and Events at Private Internet Access.

Private Internet Access provides VPN services specializing in secure, encrypted VPN tunnels. Those tunnels create several layers of privacy and security for a more effective safety for users on the Internet. Private Internet Access's VPN Service is backed by multiple gateways worldwide, with VPN Tunnel access in 25+ countries and 37+ regions. 

Private Internet Access will join KDE's other Patrons: The Qt Company, SUSE, Google, Blue Systems and Canonical to continue supporting Free Software and KDE development through the KDE e.V.
<!--break-->