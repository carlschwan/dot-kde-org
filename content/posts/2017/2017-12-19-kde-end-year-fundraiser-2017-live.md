---
title: "KDE End-of-Year Fundraiser 2017 is Live!"
date:    2017-12-19
authors:
  - "skadinna"
slug:    kde-end-year-fundraiser-2017-live
---
<figure style="float: right; padding: 0ex 1ex 1ex 1ex; margin: 0ex 1ex 1ex 1ex"><a href="/sites/dot.kde.org/files/kde-fundraiser-charge.png"><img src="/sites/dot.kde.org/files/kde-fundraiser-charge.png" alt="" width="300" /></a></figure>

<strong>It's that time of the year again - the time for <a href="https://www.kde.org/fundraisers/yearend2017/">our End-of-Year fundraiser!</a></strong>

After an exciting and successful year, we give you all an opportunity to help us recharge our proverbial batteries. 

You've always wanted to contribute to a Free and open source project, right? Maybe you wondered how you could do that. 
Well, supporting our fundraiser is a perfect way to get started. Donations are also a great way to show gratitude to the developers of your favorite KDE applications, and to ensure the project will continue.

Besides, you know that this is a project worth backing, because we get things done. Since proof is in the pudding, let's take a quick look at what we did this year.

<h2>2017 Software Landmarks</h2>

<li>In 2017, we released <strong>3 major versions of Plasma</strong> - 5.9, 5.10, and 5.11</li>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/nMFDrBIA0PM" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
<br />
<li>KDE Applications also saw 3 major releases, with the last one <a href="https://www.kde.org/announcements/announce-applications-17.12.0.php">released just recently</a></li>
<li>There were <strong>2 big releases of KDevelop</strong>, with improved support for PHP, Analyzers mode with plugins like Heaptrack and cppcheck, and support for Open Computing Language</li>
<li>We kept pushing <a href="https://www.kde.org/products/kirigami/">Kirigami</a> forward with releases 2.0 and 2.1, and several applications newly ported to the framework. Thanks to the new Kirigami, even more apps can be ported to a wider range of desktops and mobile environments</li>
<li>There were <strong>4 releases of digiKam</strong>, the image management software, which also got a new, prettier website design</li>

<figure style="position: relative; left: 50%; padding: 1ex; margin-left: -365px; width: 730px;"><a href="/sites/dot.kde.org/files/screenshot-frontpage.jpg"><img src="/sites/dot.kde.org/files/screenshot-frontpage.jpg" /></a></figure>

<li>Krita continued to amaze everyone with its high-quality features, and it just <a href="https://krita.org/en/item/4-0-development-update/">keeps getting better</a></li>
<li>We welcomed a new browser Falkon (formerly known as <a href="https://github.com/QupZilla/qupzilla">Qupzilla</a>) into our KDE family. We were also joined by several new applications, including <a href="https://github.com/KDE/elisa">Elisa</a>, a simple and straightforward music player</li>
<li>Our developers focused on accessibility and <strong>made our applications more usable</strong> for everybody during the Randa Meetings developer sprint</li>

<h2>Into 2018 with You</h2>

We look forward to the new year with all its challenges and excitements, and we don't plan on slowing down. 

There will be new Plasma and KDE Applications releases, with a new Plasma LTS release (5.12) planned for the end of January. <a href="https://dot.kde.org/2017/11/19/announcing-season-kde-2018">Season of KDE</a> will bring a stream of fresh contributors. <a href="https://blogs.kde.org/2017/09/05/konversation-2x-2018-new-user-interface-matrix-support-mobile-version">Konversation 2.0</a> will present a completely redesigned interface, and you can be sure it's not the only application that will positively surprise you in 2018. 

We will spend a lot of time and effort on <a href="https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond">our long-term goals</a>, which include improving software usability, protecting your privacy, and making it easier to become a KDE contributor. And as always, we'll be on the lookout for more Free Software projects that we can bring under our wing and help the developers bring their ideas to fruition.

But we cannot do all this without you. The community - that is, people just like you - is what drives KDE forward. Your energy motivates us. Your feedback helps us improve. Your financial support allows us to organize community events and developer sprints, maintain our websites and infrastructure, and so much more. 

<figure style="position: relative; left: 50%; padding: 1ex; margin-left: -365px; width: 730px;"><a href="/sites/dot.kde.org/files/banner-v3.png"><img src="/sites/dot.kde.org/files/banner-v3.png" /></a></figure>

Help us build a bigger, better, more powerful KDE Community by donating to <a href="https://www.kde.org/fundraisers/yearend2017/">our End-of-Year fundraiser</a>. We appreciate every contribution, no matter how modest.

You can also support us and power up our fundraiser by posting about it on social media. Share the link, tell others about it, or write a post on your blog and share that. Tweet us a link to your blog post, and we will share it on our social media.

Let's empower each other!
<!--break-->