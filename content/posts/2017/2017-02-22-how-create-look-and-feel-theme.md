---
title: "How to Create a Look and Feel Theme "
date:    2017-02-22
authors:
  - "jriddell"
slug:    how-create-look-and-feel-theme
---
<a href="https://www.youtube.com/watch?v=Hac3c6fADQM">How to Create a Look and Feel Theme</a> video tutorial.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Hac3c6fADQM" frameborder="0" allowfullscreen></iframe>

Find out more <a href="https://community.kde.org/How_to_create_Look_and_Feel_Package">on KDE community wiki</a>.
