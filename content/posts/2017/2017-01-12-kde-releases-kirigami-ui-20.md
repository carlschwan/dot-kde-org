---
title: "KDE releases Kirigami UI 2.0"
date:    2017-01-12
authors:
  - "thomaspfeiffer"
slug:    kde-releases-kirigami-ui-20
comments:
  - subject: "F-Droid"
    date: 2017-01-14
    body: "<p>Great news. Would it be appreciated if the demo app was also available on F-Droid and not only from the Google store? If you could provide a direct source code URL I could announce it there.</p>"
    author: "cs1"
---
<img src="/sites/dot.kde.org/files/kirigami.png" width="560"/>

Today, KDE announces the public release of Kirigami UI 2.0 !

All issues that were identified during the ten days of beta testing have been fixed, and Kirigami 2.0 is deemed ready for general use.

<iframe width="560" height="315" src="https://www.youtube.com/embed/BxRw9YPTxTE" frameborder="0" allowfullscreen></iframe>

Soon after the <a href="https://dot.kde.org/2016/08/10/kdes-kirigami-ui-framework-gets-its-first-public-release">initial release of Kirigami UI</a>, KDE's framework for convergent (mobile and desktop) user interfaces, its main developer Marco Martin started porting it from Qt Quick Controls 1 to Qt Quick Controls 2, the next generation of Qt's ready-made standard controls for Qt Quick-based user interfaces. Since QQC 2 offers a much more extended range of controls than QQC 1, the port allowed the reduction of Kirigami's own code, while improving stability and performance. Kirigami 2 is kept as close to QQC 2's API as possible in order to extend it seamlessly. 

Beyond the improvements that the port to QQC2 brings, further work went into Kirigami 2's performance and efficiency, and it also offers significantly improved keyboard navigation for desktop applications. On Android, Kirigami 2 integrates better visually with Material Design.

Of course there are also smaller improvements in various places, such as better handling of edge swipes in the <a href="https://community.kde.org/KDE_Visual_Design_Group/KirigamiHIG/CommandPatterns/OnDemandControls">SwipeListItem</a> or more reliable activation of the Overscroll / Reachability mode (which pulls down the top of the page to the center of the screen where it can be reached with the thumb).

Discover (Plasma's software center), a quite complex application, has already been <a href="http://www.proli.net/2016/12/31/discover-more-in-2017/">ported successfully to Kirigami 2</a> without much hassle, so we are confident that most applications can be ported easily from Kirigami 1 to Kirigami 2.  Since Kirigami 2 requires Qt 5.7, which is not available on all Linux distributions yet, Kirigami 1 is still maintained (receiving fixes for critical bugs) for the time being, but won't receive any new features or improvements.

You can get Kirigami 2.0 via its <a href="https://techbase.kde.org/Kirigami">wiki page</a>, or from your distribution's repository as soon as it is packaged there.
If you want to try it out on Android, the Kirigami Gallery demo app is <a href="https://play.google.com/store/apps/details?id=org.kde.kirigamigallery">available on Google Play</a>.
<!--break-->