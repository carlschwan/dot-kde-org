---
title: "User Question: With Some Free Software Phone Projects Ending, What Does Plasma Mobile's Future Look Like?"
date:    2017-04-28
authors:
  - "Paul Brown"
slug:    user-question-some-free-software-phone-projects-ending-what-does-plasma-mobiles-future
comments:
  - subject: "Some updates, please"
    date: 2017-07-07
    body: "<p>I Just wish there would be some news, at least once a month. Even if it only contained the words \"Plasma Mobile is moving forward\"...</p><p>&nbsp;</p><p>We have no news neither from Plasma-Mobile (which seems pretty dead) nor from Halium :(</p>"
    author: "john"
---
<span style="background:red; color:white;font-weight:bold">Q:</span> <b>With some free software phone projects ending, what does Plasma Mobile's future look like?</b>

<span style="background:blue;color:white;font-weight:bold">A:</span> The future is rosy. While it is true that Plasma Mobile used to be built on the Ubuntu Phone codebase, <a href="http://blog.bshah.in/2016/05/02/plasma-mobile-new-base-system/">that was superseded some time ago</a>. The recent events at Ubuntu and other mobile communities have not modified the pace of the development (which is pretty fast) or the end goal, which is to build frameworks that will allow convergence for all kinds of front-ends and apps on all kinds of devices.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><img src="/sites/dot.kde.org/files/20170426QA_01.jpg" />
<br /><figcaption>The "converged" KAlgebra app running on an Android phone.</figcaption></figure> 

That framework for apps already exists. It is called <a href="https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui">Kirigami</a>. Usually an operating system gains traction because of its apps. Think back when Android was the underdog to iOS, what did Google do? Lower the bar and put in place incentives for developers to create apps for Android.

The plan is that Kirigami will make the underlying platform irrelevant. If developers can port their apps with minimal hassle, and users can run their apps the same on all platforms, including the desktop, the possibility of having a shot at grabbing a slice of the mobile market becomes much more realistic. Even for new players, the main hurdle at the point of entry, i.e. having a well-stocked app store, disappears.

In the last couple of weeks Plasma Mobile developers have been working with some other mobile communities and has now announced the <a href="http://halium.org/announcements/2017/04/24/halium-is-in-the-air.html">Halium</a> project. This project aims to develop a common free, open and community-backed base-layer for all GNU/Linux-based mobile operating systems, including Ubuntu Phone which lives on through the <a href="https://ubports.com/">UBports project</a>. This interface will allow all operating systems to interact with the Android subsystems that control hardware and other low level components.

As you can see, the Plasma Mobile developers are working on bringing a common framework both to the UI side front and to the base layer. Interestingly, they are doing this, not only for the benefit of Plasma Mobile, but, in true Free Software fashion, for every community with a mobile project. This was already the goal before what happened at Ubuntu, by the way.

So, as I said at the beginning, the future for Plasma Mobile is bright.
<!--break-->
