---
title: "How KDE's Open Source community has built reliable, monopoly-free computing for 20+ years"
date:    2017-08-05
authors:
  - "sebas"
slug:    how-kdes-open-source-community-has-built-reliable-monopoly-free-computing-20-years
---
Hostingadvice.com runs a story <em><a href="http://www.hostingadvice.com/blog/kde-community-delivers-open-source-monopoly-free-computing/">How KDE’s Vast Open-Source Community Has Been Developing Technologies to Bring Reliable, Monopoly-Free Computing to the World for 20+ Years</a></em>. The article gives background to the how and why of KDE, and includes an interview with KDE's <a href="https://vizZzion.org">Sebastian Kügler</a> for some more in-depth insights. From the article:
<blockquote>
At the time, Sebastian was only a student and was shocked that his work could have such a huge impact on so many people. That’s when he became dedicated to helping further KDE’s mission to foster a community of experts committed to experimentation and the development of software applications that optimize the way we work, communicate, and interact in the digital space.

“With enough determination, you can really make a difference in the world,” Sebastian said. “The more I realized this, the more I knew KDE was the right place to do it.”
</blockquote>