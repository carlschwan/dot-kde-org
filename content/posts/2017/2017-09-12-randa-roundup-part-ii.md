---
title: "Randa Roundup - Part II"
date:    2017-09-12
authors:
  - "skadinna"
slug:    randa-roundup-part-ii
---
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/Marble_Android.png"><img src="/sites/dot.kde.org/files/Marble_Android.png" alt="" width="260" /></a><br />Marble, KDE's mapping app, will get<br />better turn-by-turn navigation features.</figure></p>

The last time <a href="https://dot.kde.org/2017/09/02/randa-roundup-part-i">we wrote about Randa Meetings 2017</a>, preparations for the event were still in progress. The developer sprint is now in full swing. Everyone is settled in and ready to start improving, debugging and adding features to KDE's apps and frameworks. But what exactly will the developers work on during Randa 2017? Here are some more details.

As you're probably already aware, the theme of Randa Meetings 2017 is accessibility. This doesn't include only desktop software, but also extends to mobile apps. Sanjiban Bairagya is working on the <a href="https://play.google.com/store/apps/details?id=org.kde.marble.maps&hl=hr">Marble Maps Android app</a>, KDE's answer to Google Earth. His accessibility-related tasks include making the turn-by-turn navigation experience more visually intuitive in real-time. He will also be switching Marble to the Qt 5.8 Speech module instead of using Java for text-to-speech support in navigation. Another thing Sanjiban wants to do is find a way to let users add notes to any place on the map.

Bhushan Shah will mostly focus on Plasma in all its shapes and sizes. During Randa 2017, he will work on making Plasma even better and snappier with Wayland, as well as on making PIM apps work better on Plasma Mobile.

Plenty of new things are in store for <a href="https://www.digikam.org/">digiKam</a>. Simon Frei will work on improving the user interface, as well as the way digiKam handles metadata. Gilles Caulier will be busy with digiKam documentation and tools for exporting images to web services. 

Dan Vratil will be busy with <a href="https://community.kde.org/KDE_PIM">KDE PIM</a> and Akonadi. He plans to discuss accessibility in Kontact with other KDE PIM developers, and complete the process of porting all PIM code away from KDE4.

<h2>You Can Be Part of Randa 2017, Too</h2>

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/randa-2017-digikam.jpg"><img src="/sites/dot.kde.org/files/randa-2017-digikam.jpg" alt="" width="420" /></a><br />digiKam will have several developers working on it all at the<br />same time.</figure></p>

KDE software is developed every day by people from all around the world. For some of them, Randa Meetings are a unique, rare opportunity to finally meet other KDE developers in person. After many months, or sometimes even years, of communicating exclusively via email and IRC, the developers can sit down and work together on resolving the most pressing issues. Apart from writing code, they also discuss long-term goals and decide on the future of KDE projects. 

Even if you're not a developer, you can also participate in Randa Meetings 2017 by donating to <a href="https://www.kde.org/fundraisers/randameetings2017/">our fundraiser</a>. Donations are used to cover accommodation and travel costs, and to make sure the developers are not hungry and thirsty during the sprint. This is your chance to support Free and open source software, and to directly contribute to the KDE Community. 

<h3><a href="https://www.kde.org/fundraisers/randameetings2017/">Don't miss out!</a></h3>

<!--break-->