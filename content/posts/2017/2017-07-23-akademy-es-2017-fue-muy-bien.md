---
title: "Akademy-es 2017 Fue Muy Bien"
date:    2017-07-23
authors:
  - "jriddell"
slug:    akademy-es-2017-fue-muy-bien
---
<figure style="padding: 1ex; margin: 1ex; border: 1px solid grey; width: 500px"><a href="/sites/dot.kde.org/files/akademy-es-2017-cropped.jpg"><img src="/sites/dot.kde.org/files/akademy-es-2017-cropped-wee.jpg" width="500" height="177" /></a><br /><figcaption>Akademy-ES 2017</figcaption></figure> 

On the 20th and 21st of July, <a href="https://www.kde-espana.org/">KDE España</a> held, with the invaluable help of <a href="http://asociacion-unia.es/osl-unia/">UNIA</a>, <a href="http://hacklabalmeria.net/">HackLab Almería</a> and the <a href="https://www.ual.es/">University of Almería</a>, and  with the sponsorship of <a href="http://www.opentia.com/en/">Opentia</a>, its 12th annual gathering: <a href="https://www.kde-espana.org/akademy-es2017">Akademy-es 2017</a>.

As it always happens when Akademy takes place in Spain, Akademy-es 2017 became a prelude of the international event and many well-known KDE developers attended.

Throughout two days, talks were offered covering many different topics, including Plasma, programming (C++, Qt, mobile), exciting projects like Kirigami, proposals for the future such as KDE on automobile, encouragement to use KDE software and contribute to KDE, and information about KDE España. 
 
People who could not attend should not be worried as videos of the talks will be available online.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/akademy-press.jpeg"><img src="/sites/dot.kde.org/files/akademy-press-wee.jpeg" width="200" height="356" /></a><br /><figcaption>Akademy makes the news</figcaption></figure> 

The local newspaper stopped by for a photo shoot and to write a story on the world gathering of KDE developers that was about to happen.

Attendees also got a chance to play around with Slimbook Ultrabooks such as the well-known KDE flavour or their new Pro edition.

As usual, KDE España members gathered to celebrate their AGM. If you wish to find out what goes on in there, or if you wish to help us out organizing events like Akademy-es and getting the word out in Spain about KDE, please consider joining <a href="https://www.kde-espana.org/">KDE España</a>. It is now easier than ever!

<figure style="padding: 1ex; margin: 1ex; border: 1px solid grey; width: 500px"><a href="/sites/dot.kde.org/files/akademy-espana-board.jpeg"><img src="/sites/dot.kde.org/files/akademy-espana-board-wee.jpeg" width="500" height="375" /></a><br /><figcaption>KDE España board, Baltasar, Adrián, Antonio, José commonly knows as Los Guapos</figcaption></figure> 
<figure style="padding: 1ex; margin: 1ex; border: 1px solid grey; width: 500px"><a href="/sites/dot.kde.org/files/akademy-es-slimbook.jpeg"><img src="/sites/dot.kde.org/files/akademy-es-slimbook-wee.jpeg" width="500" height="375" /></a><br /><figcaption>Slimbook Talk by Alejandro</figcaption></figure> 
<!--break-->
