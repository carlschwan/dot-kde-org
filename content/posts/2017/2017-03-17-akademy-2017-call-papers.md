---
title: "Akademy 2017 Call for Papers"
date:    2017-03-17
authors:
  - "sealne"
slug:    akademy-2017-call-papers
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/akademyLogo4Dot.png"/></div>
Akademy is <b>the</b> KDE Community conference. If you are working on topics relevant to KDE or Qt, this is your chance to <a href="http://akademy.kde.org/2017/cfp">present your work and ideas</a> at the Conference from 22nd-27th July in Almería, Spain. The days for talks are Saturday and Sunday, 22nd and 23rd July. The rest of the week will be BoFs, unconference sessions and workshops.
<!--break-->

<h2>What we are looking for</h2>

The goal of the conference section of Akademy is to learn and teach new skills and share our passion around what we're doing in KDE with each other.

For the sharing of ideas, experiences and state of things, we will have short Fast Track sessions in a single-track section of Akademy. Teaching and sharing technical details is done through longer sessions in the multi-track section of Akademy.

If you think you have something important to present, please tell us about it. If you know of someone else who should present, please encourage them. For more details see the proposal guidelines and the <a href="http://akademy.kde.org/2017/cfp">Call for Papers</a>.

<b>The submission deadline is 10th April, 23:59:59 CEST.</b>

<h2>About Akademy</h2><p>For most of the year, KDE—one of the largest free and open software communities in the world—works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2017">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact The <a href="https://akademy.kde.org/2017/contact">Akademy Team</a>.</p>