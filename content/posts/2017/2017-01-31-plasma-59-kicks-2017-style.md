---
title: "Plasma 5.9 Kicks off 2017 in Style"
date:    2017-01-31
authors:
  - "jriddell"
slug:    plasma-59-kicks-2017-style
comments:
  - subject: "What about HUD"
    date: 2017-01-31
    body: "<p>Since Global Menus are back... what about KDE Plasma's HUD??? Is it back as well?!</p><p>It was a very (VERY) usefull feature to be able to search menu entries thru KRunner!!!</p>"
    author: "john"
  - subject: "Congratulations and thank you!"
    date: 2017-01-31
    body: "<p>Congratulations and thank you for big job!!!</p><p>Unfortunately, despite it is already 9-th Plasma 5 release, the Icon Widgets on Desktop and some other launchers like Trashcan still has no adequate reaction to \"System settings =&gt; Input devices =&gt; Mouse =&gt; Double click to open files and folders\" option, and launch everything with single click despite I set this option to double one. :((( This pity fact makes these launchers in fact to be unusable. Please, provide the possibility to set ALL Desktop-located launchers to use double click for launching applications and single one just for focusing as it was in KDE 4 and all previous KDE versions!!!</p>"
    author: "Menak Vishap"
  - subject: "Fix multiple monitor support!"
    date: 2017-02-01
    body: "<p>Seriously folks, multiple monitor support is beyond bad. I have my desktop set up but when Iock and turn off screens to go home in the afternoon, when I come back to work in the morning, everything's messed up: panels are in different places, windows open \"sometime-in-this-sometime-in-other-display\", sometime background image is gone and only black background is shown. Not to speak of Task Manager preview \"feature\": I have to have it turned off because it crashes the desktop in a few seconds when previewing. Compared to KDE4, the desktop experience is catastrophic. Never mind the better architecture in KDE5. I'm talking about user experience.</p>"
    author: "Anonymous"
  - subject: "Meta+Number shortcuts in task bar"
    date: 2017-02-03
    body: "<p>My favourite feature (which is mentioned above but which I missed when skimming through the changes at first): Meta+1 through 9 automatically select or start the corresponding application from the task bar, same as in Windows 7+ and Unity. This works best with the \"icon-only\" task manager and pinned launchers, as the launched application replaces the launcher icon (and thus the shortcut remains the same whether the application was already started or not).</p>"
    author: "Anonymous"
  - subject: "Global Menus works only with Breeze"
    date: 2017-02-10
    body: "<h2 style=\"box-sizing: border-box; font-family: Oxygen, sans-serif; font-weight: 400; line-height: 1.1; color: #475057; margin-top: 20px; margin-bottom: 10px; font-size: 22px;\">As in subject. I use mixed style with Oxygen and I can't use Global Menus. I hope that in the future Global Menus will work also with different than Breeze styles.</h2>"
    author: "piomiq"
  - subject: "Multiple monitors support"
    date: 2017-02-19
    body: "<p>Have the same problem. Fixing this is more important than new features.</p>"
    author: "Anonymous Coward"
  - subject: "Taskbar pin"
    date: 2017-02-25
    body: "<p>Some programs keep getting pinned to the taskbar. Just because some applications are often used does not mean that I want them pinned! Is there a way to disable pinning entirely?</p>"
    author: "hij"
  - subject: "Experience plasma 5.5.5"
    date: 2017-05-01
    body: "<p>Yep, I agree. Since you are trying to make Plasma powerful DE, fix the damn multi monitor support. My panels move from screen to screen every other reboot. My window not obstructing the bar is also broken. New features are great, but having a solid working environment is more important. Please, think about this when making Plasma 6. We don't need 50 milion new features. We need 0 bugs and basic functionallity. Then you can add features.</p>"
    author: "Kali"
  - subject: "Fix multiple monitor support!"
    date: 2017-09-12
    body: "<p>Please, multi-monitor stability and usability have been incessant issues since 4.x days, that simply no one seems to ever want to fix.&nbsp; It drives me away every time, but I love kde otherwise...&nbsp; Cinnamon isn't as quirky, and doesn't constantly jumble my windows when displays are detached, so someone seems to get how this is done.</p>"
    author: "Mike B."
---
<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/lm0sqqVcotA?rel=0" frameborder="0" allowfullscreen></iframe>
<br clear="all" />
&nbsp;
<br clear="all" />
<a href="http://www.kde.org/announcements/plasma-5.9/plasma-5.9.png">
<img src="http://www.kde.org/announcements/plasma-5.9/plasma-5.9-wee.png" style="border: 0px" width="600" height="338" alt="KDE Plasma 5.9" />
</a>
<figcaption><a href="https://www.kde.org/announcements/plasma-5.9.0.php">KDE Plasma 5.9</a></figcaption>
</figure>


<p>
Tuesday, 31 January 2017. Today KDE releases this year’s first Plasma feature update, <a href="https://www.kde.org/announcements/plasma-5.9.0.php">Plasma 5.9</a>. While this release brings many exciting new features to your desktop, we'll continue to provide bugfixes to Plasma 5.8 LTS.
</p>

<br clear="all" />

<h2>Be even more productive</h2>
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/spectacle-notification.png">
<img src="http://www.kde.org/announcements/plasma-5.9/spectacle-notification-wee.png" style="border: 0px" width="350" height="192" alt="Spectacle screenshot notifications can now be dragged into e-mail composers (including web mail)" />
</a>
<figcaption>Spectacle screenshot notifications can now be dragged into e-mail composers (including web mail)</figcaption>
</figure>

<p>In our ongoing effort to make you more productive with Plasma we added interactive previews to our notifications. This is most noticeable when you take a screenshot using Spectacle's global keyboard shortcuts (Shift+Print Scr): you can drag the resulting file from the notification popup directly into a chat window, an email composer or a web browser form, without ever having to leave the application you're currently working with. Drag and drop was improved throughout the desktop, with new drag and drop functionality to add widgets directly to the system tray. Widgets can also be added directly from the full screen Application Dashboard launcher.</p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/icon-dialog.png">
<img src="http://www.kde.org/announcements/plasma-5.9/icon-dialog-wee.png" style="border: 0px" width="350" height="327" alt="Icon Widget Properties" />
</a>
<figcaption>Icon Widget Properties</figcaption>
</figure>
<p>The icon widget that is created for you when you drag an application or document onto your desktop or a panel sees the return of a settings dialog: you can now change the icon, label text, working directory, and other properties. Its context menu now also sports an 'Open with' section as well as a link to open the folder the file it points to is located in.</p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/mute.png">
<img src="http://www.kde.org/announcements/plasma-5.9/mute.png" style="border: 0px" width="340" height="399" alt="Muting from Panel Task Manager" />
</a>
<figcaption>Muting from Panel Task Manager</figcaption>
</figure>

<p>Due to popular demand we implemented switching between windows in Task Manager using Meta + number shortcuts for heavy multi-tasking. Also new in Task Manager is the ability to pin different applications in each of your activities. And should you rather want to focus on one particular task, applications currently playing audio are marked in Task Manager similar to how it’s done in modern web browsers. Together with a button to mute the offending application, this can help you stay focused.</p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/kickoff-krunner-result.png">
<img src="http://www.kde.org/announcements/plasma-5.9/kickoff-krunner-result-wee.png" style="border: 0px" width="350" height="145" alt="Search Actions" />
</a>
<figcaption>Search Actions</figcaption>
</figure>
<p>The Quick Launch applet now supports jump list actions, bringing it to feature parity with the other launchers in Plasma. KRunner actions, such as “Run in Terminal” and “Open containing folder” are now also shown for the KRunner-powered search results in the application launchers.</p>

<p>A new applet was added restoring an earlier KDE 4 feature of being able to group multiple widgets together in a single widget operated by a tabbed interface. This allows you to quickly access multiple arrangements and setups at your fingertips.</p>

<br clear="all" />
<h2>More streamlined visuals</h2>

<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/breeze-scrollbar.ogv">
 <video width="350" height="240" autoplay="true">
  <source src="http://www.kde.org/announcements/plasma-5.9/breeze-scrollbar.ogv" type="video/ogg" />
 </video> 
</a>
<figcaption>New Breeze Scrollbar Design</figcaption>
</figure>
<p>Improvements have been made to the look and feel of the Plasma Desktop and its applications. Scroll bars in the Breeze style, for instance, have transitioned to a more compact and beautiful design, giving our applications a sleek and modern look.</p>

<br clear="all" />
<h2>Global Menus</h2>
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/global-menus-widget.png">
<img src="http://www.kde.org/announcements/plasma-5.9/global-menus-widget-wee.png" style="border: 0px" width="350" height="207" alt="Global Menus in a Plasma Widget" />
</a>
<figcaption>Global Menus in a Plasma Widget</figcaption>
<a href="http://www.kde.org/announcements/plasma-5.9/global-menus-window-bar.png">
<img src="http://www.kde.org/announcements/plasma-5.9/global-menus-window-bar-wee.png" style="border: 0px" width="350" height="258" alt="Global Menus in the Window Bar" />
</a>
<figcaption>Global Menus in the Window Bar</figcaption>
</figure>
<p>Global Menus have returned.  KDE's pioneering feature to separate the menu bar from the application window allows for new user interface paradigm with either a Plasma Widget showing the menu or neatly tucked away in the window bar.</p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/task-manager-tooltips.png">
<img src="http://www.kde.org/announcements/plasma-5.9/task-manager-tooltips.png" style="border: 0px" width="284" height="225" alt="Neater Task Manager Tooltips" />
</a>
<figcaption>Neat Task Manager Tooltips</figcaption>
</figure>
<p>Task Manager tooltips have been redesigned to provide more information while being significantly more compact. Folder View is now able to display file emblems which are used, for example, to indicate symlinks. Overall user experience when navigating and renaming files has been greatly improved.</p>

<br clear="all" />
<h2>More powerful Look and Feel import & export</h2>

<figure style="float: right; width: 360px;">
 <iframe width="350" height="197" src="https://www.youtube.com/embed/RX5HwumKPP8" frameborder="0" allowfullscreen></iframe>
<figcaption>Look and Feel Themes</figcaption>
</figure>
<p>The global Look and Feel desktop themes now support changing the window decoration as well – the 'lookandfeelexplorer' theme creation utility will export your current window decoration to the theme you create.</p>

<p>If you install, from the KDE store, themes that depend on other artwork packs also present on the KDE store (such as Plasma themes and Icon themes) they will be automatically downloaded, in order to give you the full experience intended by the theme creator.</p>


<h2>New network configuration module</h2>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="http://www.kde.org/announcements/plasma-5.9/network-configuration.png">
<img src="http://www.kde.org/announcements/plasma-5.9/network-configuration-wee.png" style="border: 0px" width="350" height="309" alt="Network Connections Configuration" />
</a>
<figcaption>Network Connections Configuration</figcaption>
</figure>
<p>A new configuration module for network connections has been added to System Settings, using QML and bringing a new fresh look. Design of the module is inspired by our network applet, while the configuration functionality itself is based on the previous Connection Editor. This means that although it features a new design, functionality remains using the proven codebase.</p>

<br clear="all" />
<h2>Wayland</h2>
<figure style="float: right; width: 360px;">
<!--
<a href="http://www.kde.org/announcements/plasma-5.9/kwin-wayland.png">
<img src="http://www.kde.org/announcements/plasma-5.9/kwin-wayland-wee.png" style="border: 0px" width="349" height="107" alt="Plasma with Wayland Can Now Take Screenshots and Pick Colors" />
</a>
<figcaption>Plasma with Wayland Can Now Take Screenshots and Pick Colors</figcaption>
<br />
-->
<iframe width="350" height="197" src="https://www.youtube.com/embed/_kQwFpZoDZY" frameborder="0" allowfullscreen></iframe>
<figcaption>Pointer Gesture Support</figcaption>
<a href="http://www.kde.org/announcements/plasma-5.9/wayland-touchpad.png">
<img src="http://www.kde.org/announcements/plasma-5.9/wayland-touchpad-wee.png" style="border: 0px" width="350" height="352" alt="Touchpad Configuration" />
</a>
<figcaption>Wayland Touchpad Configuration</figcaption>
</figure>

<p>Wayland has been an ongoing transitional task, getting closer to feature completion with every release. This release makes it even more accessible for enthusiastic followers to try Wayland and start reporting any bugs they might find. Notable improvements in this release include:</p>
<p>An ability to take screenshots or use a color picker.  Fullscreen users will be pleased at borderless maximized windows.</p>
<p>Pointers can now be confined by applications, gestures are supported (see video right) and relative motions used by games were added. Input devices were made more configurable and now save between sessions.  There is also a new settings tool for touchpads.</p>
<p>Using the Breeze style you can now drag applications by clicking on an empty area of the UI just like in X.  When running X applications the window icon will show up properly on the panel.  Panels can now auto-hide.  Custom color schemes can be set for windows, useful for accessibility.</p>

<p><a href="http://www.kde.org/announcements/plasma-5.8.5-5.9.0-changelog.php">Full Plasma 5.9.0 changelog</a></p>

<!--break-->

