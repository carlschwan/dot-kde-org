---
title: "First Talks for conf.kde.in, Registration Open"
date:    2014-01-15
authors:
  - "pradeepto"
slug:    first-talks-confkdein-registration-open
comments:
  - subject: "Really Amazing!"
    date: 2014-02-15
    body: "Hi,\r\n\r\nI look forward to see you guys at conf.kde.in\r\nReally excited about this fun meetup and looking forward to a great knowledge sharing platform.\r\n\r\nDeep S"
    author: "Deep Sukhwani"
---
conf.kde.in <a href="http://dot.kde.org/2013/11/20/confkdein-2014">was announced in November</a>, to take place February 21 – 23, 2014 in Gandhinagar, India. This three-day conference, the biggest KDE event in India,  will bring together Qt developers, KDE contributors, open source enthusiasts and users from all across the nation. Visitors will have the opportunity to learn, share, contribute, innovate and create around Qt and KDE technology.

<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEIndialogoKDEBlue177.png" /></div>

conf.kde.in is an excellent platform for you to learn about FOSS and start contributing to it. You will learn about KDE technology and community and how to participate, paving the way for future participation in programs like Google Summer of Code (GSoC) and Season of KDE. Last year, KDE was the largest participating organization in GSoC with 60 students selected. The conference sessions will range from beginner to advanced level, to facilitate all kinds of participants.

<h2>Talks</h2>
There will be a vibrant gamut of talks at the event; with a variety of technical as well as non technical talks covered by members of the KDE community from the extremes of India as well as the world. Be a witness to this cultural and intellectual infusion of ideas and experiences, of knowledge and guidance; by people from different walks of life, all with a common passion - open source and KDE; ready to ignite the same passion in you through their words and their minds.

Here are some of the talks currently scheduled for this event:

<ul><li>How I got involved in KDE and How to attack a problem - Sujith H</li>
<li>Hacking on KStars: Progress and challenges - Rishab Arora</li>
<li>One app to rule all your media - Sinny Kumari</li>
<li>KDE unlike a coconut - Smit Shah</li>
<li>Plasma Workspace 2: Little introduction - Bhushan Shah</li>
<li>Baloo - Metadata and Search - Vishesh Handa</li>
<li>Cute (Qt) C++ idioms - Nikhil Marathe</li>
<li>Qt Project - Frederik Gladhorn</li>
<li>Language learning with KDE - Samikshan Bairagya</li>
<li>Effective Open-Source Speech Recognition in Your Application - Peter Grasch</li>
<li>Where KDE is and where it is going - Jos Poortvliet</li>
<li>C++11: A Language Renaissance - Kévin Ottens</li>
</ul

The above is the incomplete list of talks, we are adding more talks as we get confirmation from the speakers. The updated list of talks can be found at <a href="http://kde.in/conf/accepted-talks">this page</a>.

Every speaker at conf.kde.in is a bona fide contributor to KDE, some of them contributing already for over a decade. Attending the conference will give you the opportunity to meet seasoned open source contributors, discuss with them, exchange ideas with them, learn from them and also teach them something new.

Many of these contributors work for great companies (which themselves are involved in various open source projects) inluding Mozilla, SUSE, Red Hat, Blue Systems, ThoughtWorks, KDAB, Digia. Also you will find that many of these contributors were students until recently.
<div style="float: right; padding: 1ex; margin: 1ex; width: 300px; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" /></div>
<h2>Wondering what KDE/Qt is?</h2>

KDE is one of the largest international free software communities. It has an integrated set of cross-platform applications designed to run on GNU/Linux, FreeBSD, Solaris, Microsoft Windows, and Apple OS X systems. KDE is known for its Plasma Workspace, an environment provided as the default working environment on many Linux distributions, such as Kubuntu, Pardus, and openSUSE.

To find more information about KDE, please visit  <a href="http://www.kde.org/">kde.org</a>.

Qt is a cross-platform application framework that is widely used for developing application software with a graphical user interface (GUI) and also used for developing non-GUI programs  such as command-line tools and consoles for servers. Qt is used in Adobe Photoshop Elements, Skype, VLC media player, VirtualBox, Dassault DraftSight and Mathematica, and by the European Space Agency, DreamWorks, Google, HP, KDE, Lucasfilm, Autodesk Maya, The Foundry's Nuke, Panasonic, Philips, Blackberry applications, Samsung, Siemens, Volvo, Walt Disney, Animation Studios and Research In Motion. Qt runs on multiple platforms which include Linux, Windows, Mac OS X, Embedded Linux, Blackberry 10, Android, Sailfish and Ubuntu Phone OS.

To find more information about Qt, please visit  <a href="http://qt-project.org/">qt-project.org</a>.

<h2>Details about conf.kde.in</h2>

<strong>Venue:</strong>   Dhirubhai Ambani Institute of Information and Communication Technology, Gandhinagar
<strong>Date:</strong>      21st - 23rd Feb, 2014
<strong>Time:</strong>     9:00 AM - 6:30 PM

Registration for the conference is now open. Please register yourself <a href="http://cki2014.doattend.com/">here</a> and grab the “Early Bird” discount until the 15th of January. There are limited seats so hurry up! You will find accommodation options at <a href="http://conf.kde.in/accommodation">this page</a>.

We are working very hard to make conf.kde.in 2014 a huge success. And it will happen with the help of your participation. Looking forward to meet you all. Stay tuned for regular updates about this event on <a href="https://www.facebook.com/kdeindia">our Facebook</a> and <a href="https://twitter.com/kdeindia">Twitter page</a>.

For any queries, feel free to reach us at <a href="mailto:conf@kde.in">conf@kde.in</a>. 