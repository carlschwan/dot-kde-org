---
title: "KDE Commit-Digest for 9th March 2014"
date:    2014-05-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-9th-march-2014
---
In <a href="http://commit-digest.org/issues/2014-03-09/">this week's KDE Commit-Digest</a>:

<ul><li>Plasma Desktop can highlight currently open applets</li>
<li>Plasma framework adds an optional EGL/X11 backend for WindowThumbnail QQuickItem</li>
<li>KDE-PIM can create calendar events from e-mail</li>
<li>Kate substantially improves highlighting of reStructuredText (rest.xml)</li>
<li>Skrooge adds "quarter" and "semester" period in graph and "Incomes & Expenditures" dashboard widget.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-03-09/">Read the rest of the Digest here</a>.
<!--break-->