---
title: "Akademy 2014 Day 2 Talks"
date:    2014-09-08
authors:
  - "jospoortvliet"
slug:    akademy-2014-day-2-talks
---
It was a cloudy morning in Brno.... luckily not as hot as the first day. The traces of fun from last night kept many participants similarly subdued but they were soon woken up by a truly inspiring keynote by Cornelius Schumacher, our fresh <em>former</em> president of KDE e.V.!

<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width:760px; text-align: center;">
<a href="http://byte.kde.org/~duffus/akademy/2014/groupphoto/"><img src="/sites/dot.kde.org/files/akademy2014_group_photo-wee.png" width="750" height="251" /></a><br />
<a href="http://byte.kde.org/~duffus/akademy/2014/groupphoto/">Akademy 2014 Group Photo</a>, how many can you name?
</div>
<!--break-->
<h2>Keynote by Cornelius</h2>
Cornelius opened by telling us he became a better person by participating in the KDE Community and wants to share with us why. He kicked off with a tale about his early days in KDE—one day he removed a folder from his code repository: the admin folder. Little did he know about the consequences of this action. The admin folder was shared among all KDE projects and contained the scripts and tools needed to build all KDE applications. Rest assured, it did not take others long to notice that every build had broken. Ultimately, Cornelius learned a lot about CVS and fixing things with it - it is mistakes which teach best. Later on, he ran for the Board of KDE e.V. and learned some of the world's longest words, courtesy of the necessary bureaucratic skills for running a legal organization in Germany. Many of the skills he learned he applied in his work; his KDE experience helped him grow in his role as a manager.

And he is not the only one who benefited from the learning environment the KDE community offers. An old picture of Till Adam shows the Managing Director for KDAB Germany wasn't always the best dressed person in KDE. Cornelius also found a picture of Eva Brucherseifer at a KDE meeting long before she started her own company, BasysKom, which has been sponsoring Akademy for many years. KHTML, our web browsing engine, has grown to be at the core of most modern browsers like Safari and Chrome. One of its initial developers, George Staikos, is now VP of Web Technologies at Blackberry. The pictures Cornelius treated us with once again didn't make it look like either would make it that far. A few more quips were made about t-shirts. Rohan and Vishesh got applause for getting as far as they did in the time they have been part of the KDE community—from students to being full-time employed to do the awesome work they do.

Not all in KDE have been so successful. Upon seeing the screenshot of the most famous Plasma theme, the IRC channel erupted in protest and many contended all was fluffy but before this protest reached him, Cornelius had already moved on to the next slide. And yes, the KDE 2 wallpaper with the slogan 'I con do it' (to promote the work on new icons) might not have been our best and brightest marketing moment. Finally, Cornelius touched on the downside of working with <strikethrough>know-it-alls</strikethrough>talented and <strikethrough>stubborn</strikethrough>ambitious people: sometimes people bump heads. The KDE community has been dealing with such issues in a comparatively constructive matter, in part through the Community Working Group and other structures.

The question then is: how did all this come to be? Why is KDE such a great environment for growth? Cornelius gives three main reasons for this, starting with Freedom.
<ul><em>"Freedom is central. Freedom is nothing else but a chance to be better"</em> ~ Albert Camus</ul>
The license and the freedoms that were defined by Richard Stallman are at the core of our culture and this results a low barrier of entry, motivation to do interesting and fun things, and it facilitates learning because making mistakes is not punished like they are in other environments.

A second important thing is purpose. We have a common goal: build software for client users, specifically the desktop. Cornelius felt this most strongly at an event in Frankfurt where Linus Torvalds handed out an award for providing the best desktop. There was clearly a higher purpose to the KDE community and its work noticeable there. Another great picture giving this feeling is from an event in India where Pradeepto is talking to a group of students. They were about to leave the event (traveling back for almost a day!) as they did not feel their skills were enough to benefit from the sessions available at the event. Pradeepto convinced them to stay and organized, at that moment, a series of beginner-level sessions which were suitable for them, so the second day they could participate already in the normal schedule.

<div style="border: thin solid grey; padding: 1ex; margin: 1ex;  text-align: center; width: 730px">
<img src="http://apachelog.files.wordpress.com/2010/05/fluffy-new.jpg" width="720" height="450" /><br />
Fluffy Developers Try to Subvert our Reputation as an Enterprise Desktop
</div>

The third ingredient Cornelius sees in KDE is fun. And he can only say this in this room: doing C++ is fun! It is even more enjoyable if you use Qt! It isn't the only fun stuff we do, we are a diverse bunch, but it is a common ground, something we all feel at home with. He now works as a manager, but sometimes going back to writing some C++ is relaxing. And it gets us to accomplish great things, be creative, bring our software to millions of users. This is all fun! This is expressed beautifully in the Randa picture he showed: you can see and feel how it is fun, getting into the zone, being productive, helping each other grow. Not many places allow focus as deep as the Randa meetings.

So, freedom, purpose and fun. We should ask ourselves next: how do we preserve what has made KDE great? On the freedom side, we have a solid, strong base. We're pretty safe with that. There are challenges for freedom, but many people are addressing them. Fun is a safe thing, too, this Akademy is a testament of that: we are having fun.

Purpose is the challenge for the KDE community. Our native platform, C++ is still big, but in a slow decline. And the numbers of our contributors and contributions are in decline as well. Yes, lies, damn lies and statistics, and this Akademy we welcomed a lot of new people, but we had bigger events in the past. We have to change to stay relevant and grow. We should be serious about that and shouldn't just say our move to Git screwed our statistics - maybe it did, but that is no reason not to try and do better. At CeBIT we got an award, a readers-choice for the best desktop - won by a large margin. We have a great base and great software and we know what we are doing. But is the desktop still our purpose? If not, then what is? Cornelius wants to see our purpose a little wider. He sees our goal being "give people access to great technology". We want to do great technology, we're ambitious, but in the end it is about bringing it to people. This is where we should put our emphasis to keep KDE relevant.

The secret is, in the end that it isn't KDE which makes anybody a better person. It is us - all of us, together. Cornelius' final message is: be free, maintain your purpose and have fun! Then we can all grow. He receives a great applause and throughout the rest of the event, his keynote comes up many times: it was inspiring and motivating, but also made us think about where we need to go.

<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 510px;  text-align: center;">
<img src="/sites/dot.kde.org/files/8508855633_281c1e2dac_b.jpg" width="500" height="375" /><br />
Pradeepto Inspires students at kde.conf.in
</div>

<h2>Fast Track Time</h2>
After Cornelius' opening keynote, it is time for the fast track again, starting with <strong>Kevin Ottens</strong> on "Software Craftsmanship". He discussed hacker culture and taking pride in creating a beautiful (finished) product as a teaser for his other talks and workshop. <strong>Alex Fiestas</strong> showed off KDE Connect and its nifty features to make your phone work with your Plasma desktop.  He was interrupted by his phone (named Rusty Trombone) receiving a call during the talk which successfully stopped his showing of Dr Who. <strong>Kai Uwe Broulik</strong> showed how to integrate your KDE application with native features on Android and iOS devices.  <strong>Jos Poortvliet</strong> gave some tips on how to deal with people AFK (away from keyboard, with his real-life examples drawing quite some laughs from the audience.

<strong>Björn Balazs</strong> shared a guide on the impossibility of doing usability. The hardest issue is that we can't do usability in KDE very well as we can't reach our users... In order to save the world, we need to find a way to connect to our users! Starting with our user interface itself. There are plenty of ways to do that and there will be a BoF session to make this happen. <strong>Albert Astals Cid</strong> ended the morning track with a overview of the release management process. He showed the amount of work put in each release. His point was clear—with 8 releases in July alone, the current process has to be improved. Of course, a BoF session will take place on this subject.
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/Akadem2014.jpg"><img src="/sites/dot.kde.org/files/Akadem2014_wee_0.jpg" /></a><br />Group Photo time!</div> 

<h2>Lunch Time and Technical Sessions</h2>
After the release management talk the audience was herded outside to have their picture taken, and then again unleashed on Brno to hunt and gather. Food had to come from rather far as nearby places were closed but the clouds had disappeared and the sunny walk provided some time for discussion and the production of vitamin D. After lunch, there was a technical and a less technical track. We don't have coverage of all sessions there, but you can watch the videos later and we have a small selection below again.

<strong>Andrew Lake</strong> from the Visual Design Group gave an inspiring talk about building up and fostering a community of very different people with a wide variety of skill levels. He explains there were originally fears of design-by-committee (the implicit thought: that produces not-great-design). Andrew claims that while things can go wrong, they don't have to. Oh, and - the same arguments against community design have been leveraged against Free Software itself in the past.

The approach to get a group of people to produce good design is roughly as follows:
<ul>
<li>Organically curate content by everybody (here it matters to teach people proper communication skills: be gentle, respect opinions etc.)</li>
<li>Encourage those who have gained influence chime in on proposed designs</li>
<li>Limited time for review of a design proposal (this is really important)</li>
<li>Build and sustain a community</li>
<li>Explicitly encourage constructive feedback - set an example - draw a line on destructive behavior</li>
<li>A community needs tools.. forum, VDG in the HIG, mockup toolkit</li>
<li>Sometimes focus on short-term success, even if you want to shoot for the moon</li>
<li>effective = correctness * commitment</li>
<li>Interaction with developers (a 3-lobed circle with "resident designer" and "designer community" interacting with the "developer")</li></ul>

The community design cycle was described by him as follows: develop candidate design; announce the cycle and the candidate design and length of cycle; execute.

Last, Andrew shared a multi-year roadmap for building up the design community - there is still plenty to do!

After Andrew, <strong>Jens Reuterberg and Thomas Pfeiffer</strong> continued on this subject with a zombie-themed talk. Thomas Pfeiffer (who later got <a href="https://plus.google.com/117563705675081959469/posts/b3LaKDwryaX">injured in the line of duty</a>) explained how to infiltrate software development teams with tasty brains to eat as ultimate goal and how development teams can get designers and usability experts involved, again, with brains as goal. The talk was conversation-style with Jens talking about communication and the realities of design work and collaboration. It was noted that as a first step:
<ul><li>Developers and usability experts/designers need to go outside of their comfortable boarded-up houses and lure the zombies closer</li>
<li>Shuffle like the zombies, moan like the zombies</li></ul>

Step 2 of the plan (for the zombies!) is to listen, understand, speak the language of the developers, learn to blend in, learn their ambitions and challenges.  Jens explained the position of designers and why it is often hard to get them to work in the open on the same channels as developers: <em>"Designers have to care about their careers - and hearing your code is bad once or twice won't impact your career; but having an employer see that others considered your design that bad will"</em>. That is an important reason for the use of forums, Google Hangouts and so on by the KDE Visual Design Group.

The third step is to conquer by crafting a productive relationship. This is based on trust: the developer should trust the designer to know what he/she is talking about and the designer should trust that the developer takes him/her serious. And the other way around!

In the end, this leads to better software and most importantly: more brains for everybody.

<strong>Jonathan Riddell</strong> inspired the masses with  drama and emotion.  He had a really, <em>really</em> bad accident a few years ago, from which he recovered impressively well although it still hampers him at times. Of course, you don't <em>need</em> to be brain-damaged to care about Free Software and Kubuntu, he cared about them long before the accident happened. He is grateful to Blue Systems as it allowed him to continue working on Kubuntu.

<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 410px;  text-align: center;">
<img src="/sites/dot.kde.org/files/dinner.jpeg#overlay-context=2014/09/08/akademy-2014-day-2-talks" width="400" height="226" /><br />
KDE Community Food BoF
</div>

In a talk about the Next Generation desktop applications, <strong>Vishesh Handa and Alex Fiestas</strong> showed off their app Jungle, a new video player, which aims to bring intelligence to video handling, as an example of bringing web-style ideas to the desktop. Alex asked why we use native applications on tablets and why on desktops we use web applications. He looked at web applications and saw features like suggested content, sharing content and giving feedback, of the desktop apps he still uses, none of them offer these features. So he and Vishesh set out to write a video player which would be smart. Jungle learns from the user and organizes your video library for you. In the discussion, it even was suggested to suggest romantic movies to the user when the phone of the boy/girlfriend of the user is detected. It will have a Home/Dashboard and it will download subtitles in the right language.  Alex and Vishesh want an Android app (written in Qt) as a remote control, and they want to be able to stream to and from other devices for the future.

The parallel technical session was kicked off by <b>Jan Kundrát</b> who spoke about bringing the Gerrit code review tool to KDE with help of the XIFI project.  <a href="https://gerrit.vesnicky.cesnet.cz/">Gerrit is already available</a> in a testing mode and KDE developers are free to <a href="mailto:jkt@flaska.net?Subject=KDE+Gerrit">email Jan to request their projects to be available through Gerrit</a>. The future plans are ambitious - each patch, no matter who sends it for evaluation, will be checked by the Continuous Integration system which will run on resources provided by the XIFI project.

<strong>Aleix Pol</strong> gave a history of his attempts to put KDE Software on mobile platforms, the N900 then the N9, Jolla and now he is working with Android and feels this is the way forward.  He has made KAlgebra and it's in Google Play.  He has worked on CMake to allow it to compile apps on Android. He says we need to work with F-Droid as well as Google Play and other stores but warns <em>"We need to be mindful of our inner RMS and remember user freedom."</em>

<strong>Frederik Gladhorn</strong> started with a demo of accessibility for blind people on his laptop opening his presentation with his monitor blanked out, the voice spoke out the elements of the Plasma 5 UI so that he could open his talk. This works well now in Qt 5.4.  We badly need a virtual keyboard with working word suggestion, touch friendly and spell checking.  Some parts need fixes for the screen reader: KickOff and system tray and KWin need some improvements. Once it's good enough, we will get feedback from the blind community and listen to them.

<strong>Kevin Ottens and craftsmanship "agile to the rescue"</strong>
What are the promises we make / made in FOSS? Collaboration, security, freedom... To Kevin, these feel like broken promises. For instance, it's less collaborative than we pretend it should be; bus-number of 1 is too common. Does this stem from the methodology advocated as Open Source? Or something else? Even if we knew what caused these problems, the problems are still there. So let's look for solutions. Kevin discussed agile methods for that, mentioning the awesome hamster-wheel effect and sharing a lot of ideas for improvements.

After Kevin, we should have had <strong>Tomaz  Canabrava</strong> explain how he coerced Linus Torvalds into coding C++ but unfortunately, he broke a leg and had to stay in Brazil. Instead of him, <strong>Aleix Pol</strong> talked about KDevelop5 and the work that is being done on that.

<h2>Closing</h2>

After the last session, everybody got together in the big room for sponsor presentations.
<ul><li>Blue Systems' Jonathan Riddell created the usual big show. He's been on this for years, making a mess on the stage by inviting people for the goodies he brings - polo shirts this year.
<li>Digia's Tejo had a simple message: we build Qt, you build on top of it - you're awesome, keep it up!</li>
<li>Red Hat was happy to welcome all of the KDE contributors and would love it if some of them would stay even longer: join Red Hat!</li>
<li>KDAB told everybody to enjoy the conference and keep hacking!</li>
<li>The Open Invention Network makes no money directly but builds a patent pool to protect Open Source and Free Software and invites us to join, help publish defensive patent articles and so on.</li>
<li>SUSE was represented by Bruno Friedman, openSUSE board member. He's a long time KDE fan/user/occasional contributor and sponsor and he thanked the KDE community for being welcoming and inspiring and strengthening his faith in freedom and collaboration. He finished noting that if anybody asks him where he'd like to be 10 years from now, the answer would be Akademy 2024!</li>
<li>ownCloud was represented by its founder, Frank Karlitschek. He said that he is happy that ownCloud, having grown from the KDE community into its own strong community with a startup behind it, is now in a position to sponsor Akademy. He brought up the state of ownCloud support in KDE and especially the Emblem support in the upcoming ownCloud client release which won't make it in Dolphin due to unfortunate timing and asked if anybody wanted to sit together and see what could be done about it.</li>
<li>Froglogic, Google and BasysKom had no representative at the conference but big thanks for them and their support as well! Each has been sponsoring Akademy for quite a while and we're glad they continued their contribution.</li></ul>
After the sponsors talks it was time for the Akademy Award Winners, which you can read about in <a href="https://dot.kde.org/2014/09/08/akademy-award-winners-2014">our previous article</a>.

Akademy now continues with five days of BoF sessions and hacking to discuss, design and create the next year's worth of output from KDE.

<em>Thanks to everybody who contributed notes to this article: Jonathan Riddell, Jan Kundrát, Camila Ayres and Adriaan de Groot.  Remember we offer free hugs for anyone at Akademy who read this far.</em>
