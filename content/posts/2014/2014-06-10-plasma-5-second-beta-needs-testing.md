---
title: "Plasma 5 Second Beta Needs Testing"
date:    2014-06-10
authors:
  - "jriddell"
slug:    plasma-5-second-beta-needs-testing
comments:
  - subject: "Install"
    date: 2014-06-11
    body: "With the new Plasma 5 using QT 5.3 etc, is it possible now/future to switch @login to try it out alongside kde4.  If something would crash during work, just log out and login with kde4, like people can do today to try out gnome for example?"
    author: "Bikkne"
  - subject: "Finally ! :)"
    date: 2014-06-13
    body: "I have waited this very long time... Good job, this is beautiful."
    author: "Reclad"
---
<a href="http://kde.org/announcements/plasma5.0-beta2/plasma50b2-main.png"><img src="http://kde.org/announcements/plasma5.0-beta2/plasma50b2-main-wee.png" width="400" height="225" style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex;" /></a>

The next generation desktop from KDE is taking shape and <a href="http://kde.org/announcements/plasma5.0-beta2/">the second beta is out now for testing</a>. The developers have settled on a name - Plasma 5, and there is only one month to go until the first release so please test packages from your distro or download the <a href="http://neon.blue-systems.com/live-iso/">Neon 5 Live ISO</a> to see what is working and what needs fixed.  

The main layout of the desktop remains similar to previous versions, no massive new workflows here, but it has been entirely remade in Qt Quick to give it a smoother, more dynamic feel.  The new Breeze artwork from the <a href="http://vdesign.kde.org/">Visual Design Group</a> is taking shape and various elements made more consistent so the widget explorer, window and activity switcher now share a common feel.  As with any major release some features are yet to be ported and some new problems will have slipped in, do let use know how you find it.

<!--break-->
