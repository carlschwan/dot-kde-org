---
title: "Make the World a Better Place! - KDE End of Year 2014 Fundraising"
date:    2014-10-16
authors:
  - "jriddell"
slug:    make-world-better-place-kde-end-year-2014-fundraising
---
<a href="https://www.kde.org/fundraisers/yearend2014/"><img src="https://www.kde.org/images/teaser/bannerFundraiser2014.png" width="830" height="300"alt="fundraiser banner" /></a>
As we approach the end of the year we begin the season of giving. What would suit the holiday better than giving to the entire world through the <a href="https://www.kde.org/fundraisers/yearend2014/">KDE Year End 2014 fundraiser</a>?

Here is a unique way to give back to KDE allowing us to keep giving free software to humankind.

By participating in this fundraiser, you'll be part of the improvements we'll put into our educational software, so kids can have better tools for school; our office suite, so we have the best tools for the workplace; and our desktop so we can all experience a fun and productive experience when interacting with our computers.

Donating to KDE is not for you, it is for the entire world.

As a way to say thank you, starting with €30 we will send a KDE themed postcard to any given address. You will get an extra card for every additional €10 donation. Get cards for yourself and for your family and friends to show them you care for freedom. It's the perfect way to spread the festive cheer and donate to your favorite project at the same time.
<!--break-->