---
title: "Akademy 2014 Talk Videos Available"
date:    2014-09-25
authors:
  - "jriddell"
slug:    akademy-2014-talk-videos-available
---
Videos of all of the Akademy Talks are now available online to watch in your own time.

You can access them from the <a href="https://akademy.kde.org/2014/conference-schedule">Akademy schedule</a>. Follow the schedule to the talks for the links to the videos and the slides.

A small note, due to technical issues with equipment at the venue, some of the audio isn't great - we can only apologise about this.
