---
title: "Kate and KDevelop sprint in January 2014"
date:    2014-06-05
authors:
  - "agonzalez"
slug:    kate-and-kdevelop-sprint-january-2014
---
From January 18th to 25th, Kate, KDevelop and Skanlite developers met in Barcelona. The sprint was focused on the work of the upcoming few months, and covered a wide range of aspects of these projects.

<div align=center style="padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/kdev_kate_2014_sprint.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdev_kate_2014_sprint700.jpg" /></a><br /><small>left to right, back row: Kevin Funk, Gregor Mi, Dominik Haumann, Christoph Cullmann, Milian Wolff, Joseph Wenninger 
front row: Sven Brauch, Aleix Pol, Heinz Wiesinger, Miquel Sabaté</small></div>
<!--break-->

One of the big initiatives that the developers have been working on recently is the KDE Frameworks 5 migration. During the sprint, Kate's port to Frameworks 5 matured while KDevelop received its first push towards the adoption of the new Frameworks. This is an important step because it lets the team think ahead about adopting the technologies that will be developed on for the next years.

<h2>KDevelop</h2>
KDevelop also improved on the supported languages front. The new KDevelop Clang plugin got a big push, and, while it is not going to be released yet, it is expected to supersede the current C++ plugin in the long term. Clang is expected to improve the support for standard C++, and also offers an opportunity to support C projects properly. Eventually, an Objective-C plugin could be built on top of Clang. Clang integration reduces the maintenance burden compared to the self-written C++ parser. During the sprint, we carved out a roadmap for the Clang plugin and also extended what we already have so far. The main focus was on polishing infrastructure inside KDevelop for providing a solid base for integrating Clang's useful diagnostics and fixits module. 

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/kdevelop-clang-fixits.png" /></a><br />Clang diagnostic</div>

Currently, the kdev-clang plugin consists of only about 4000 lines of code, compared to nearly 55000 in the old plugin. Further good news—there will be a Google Summer of Code 2014 project that will take care of delivering a first releasable version. There's still *a lot to do* to make this as usable as the previous C++ support plugin. Read more about <a href="http://milianw.de/blog/katekdevelop-sprint-2014-let-there-be-clang">kdev-clang</a>.

KDevelop's code assistant popup has gotten a revamp, which will -- after some polishing -- provide a more flexible and better integrated UI for the assistant features. The useful "blame" feature, which shows who touched each line in the current file as provided by the project's VCS, was improved as well. It now shows the commiter's name instead of the commit identifier and also works properly with dark color schemes. KDevelop's interface is now more customizable, toolviews can be detached (for example, source code documentation can be detached from the main window and moved to another screen). KDevelop's codebase was cleaned up and quite a few optimizations were added. This and other improvements will give a noticeable performance boost when operating on large projects consisting of thousands of files.

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/PREUL0V.png" /></a><br />Find out quickly who the writer is</div>

<h3>Python</h3>
Much internal cleanup was done in Python support and some long-standing bugs were fixed, such as the debugger not working properly. <a href="http://scummos.blogspot.de/2014/03/kdevelop-python-for-python-3-first.html">Python 3</a> support is now finished, and future development will focus on that.

<h3>Ruby</h3>
The KDevelop Ruby plugin was also greatly improved during the sprint. Lots of bugs have been fixed and a first stable release is closer now.

<h3>PHP</h3>
The sprint also provided a good opportunity to improve the language support within the PHP plugin. A lot of progress was made on completing syntax support for the new features introduced by PHP 5.4. Most notably there is now full support for PHP's trait syntax. While catching up with newer syntax features is important, so too is improving support for older features. One of the most requested improvements for the plugin is proper support for PHP's namespace syntax. During the sprint we worked on making this a lot more usable. However, there are still some kinks to be worked out.

<h3>Kate</h3>
For Kate, the focus was mostly on the Frameworks 5 port. The <a href="http://kate-editor.org/2014/01/11/ktexteditor-on-frameworks-5-timing-is-everything/">port already started back in December 2013</a>, resulting in the KF5-ready KTextEditor framework and stable KF5/Qt5 versions of the Kate and KWrite applications. During the sprint, the Kate team worked on a lot of details, polishing the KTextEditor framework.

<h3>KTextEditor Interface Cleanup</h3>
The KTextEditor interfaces are responsible for all the interaction between the editor component Kate Part and the host application (eg. KDevelop, Kile, Kate, ...). So it is important that these interfaces allow good integration of the editor into the host applications. During the sprint, these interfaces were cleaned up and optimized for speed. In addition, the default colors were extended to allow for <a href="http://kate-editor.org/2014/03/07/kate-part-kf5-new-default-styles-for-better-color-schemes/">better color schemas</a> in the future.

<h3>New Status Bar</h3>
Previously, Kate Part did not provide a status bar. All host applications (KDevelop, Kile, ...) had to write their own variant of a status bar, displaying the cursor position and similar information. In the KTextEditor framework, Kate Part will ship a default status bar, showing the cursor position, the edit mode, the modification state, the highlighting, encoding and the indentation settings. Further information can be found in <a href="http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/">this blogpost</a>.

<h3>KTextEditor Plugin Architecture</h3>
KTextEditor's plugin architecture was improved substantially. Plugins written for the KTextEditor framework will be available in all applications embedding Kate Part, making it possible to share a lot of features such as collaborative editing, search & replace in multiple files, and similar tools. This is possible because the plugin interfaces are now much more powerful than the former interface for shared plugins.

As a byproduct, the Kate application interfaces were completely dropped in favor of the KTextEditor plugin architecture. Most of the Kate plugins are already turned into KTextEditor plugins, such as the Documents sidebar, the Filesystem Browser, Search & Replace, the Build Plugin, the Backtrace Browser.

<h3>Kate Application Changes</h3>
The Kate application saw several changes; among the most visible is the new built-in tab bar. Previously, Kate provided the Documents sidebar to navigate through files. The Documents sidebar has the advantage that it stays usable when working on a large number of files. However, a lot of users want an integrated tab bar for quick file navigation. Therefore, the Frameworks 5 version of Kate will have both—the Documents sidebar as well as the tab bar. Since the number of visible tabs is often limited, only the tabs that were most recently used will be displayed. Users will be able to navigate quickly through the files being worked on. Besides quick navigation, the tab bar also allows the view to be split the view vertically or horizontally, to show the quick-open view, and to maximize the currently active view by hiding all other view spaces. A preliminary version of this tab bar as well as a KF5 version of Kate is described further in <a href="http://kate-editor.org/2014/01/25/katekdevelop-sprint-finishing-line/">this blogpost</a>.

<h3>vi mode</h3>
Kate's vi input mode also gained several improvements and polishing.

<h3>Wrap-up</h3>
All in all, a lot of work was done under the hood in both Kate (<a href="http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/">detailed sprint wrap up report</a>) and KDevelop. The Kate developers are still improving and extending the KDE 4 version of Kate, KWrite and Kate Part, while the KF 5 port is being finalized. The KDevelop team started porting to KF 5 as well, but continues to improve the KDE 4 version in the meantime. A major effort is being made to rework KDevelop's C++ language support to be more reliable,  powerful and easy to maintain in the future.

<h3>Thank you</h3>
Thanks to Blue Systems for hosting the Kate+KDevelop sprint in Barcelona! Your support is greatly appreciated!</p>