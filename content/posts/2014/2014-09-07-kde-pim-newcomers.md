---
title: "KDE PIM Newcomers"
date:    2014-09-07
authors:
  - "jospoortvliet"
slug:    kde-pim-newcomers
---
<div style="width: 360px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="https://community.kde.org/File:Mascot_20140702_konqui-commu-journalist.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-commu-journalist_wee_0.png" /></a></div> 
With Akademy in full swing, we thought we'd treat you all on a conversation with a handful of newcomers to the KDE PIM team. The conversation took place both online over the last months and offline at Akademy yesterday. Let's start with introductions, in order of their replies.

<strong>Guy Maurel</strong> is French, almost 67 and retired. Having studied electrical engineering, he has seen the first coax cables forming an intranet and later managed DNS, mail and router systems following the growth of the IT industry. He's been using Linux for a while and when a student told him he should be contributing to Open Source, he decided to join KDE PIM.

<strong>Daniel Vrátil</strong> lives in Brno, Czech Republic and works at Red Hat, maintaining KDE on Fedora and working 'upstream'. He hacks on Telepathy, KScreen and recently became maintainer of the Akonadi framework. He's also the team lead of the local Akademy team this year!

<div style="float: right; border: solid thin grey; margin: 1ex; padding: 1ex">
<a href="/sites/dot.kde.org/files/sandro.jpg"><img src="/sites/dot.kde.org/files/sandro-wee.jpg" width="450" height="454" /></a><br />
Sandro Knauß
</div>
<strong>Sandro Knauß</strong> is a 29 year old German who, since finishing studying physics, has been working as an IT freelancer. He's been in touch with KDE since he was 17, having used several distributions since starting with SUSE. When his current distribution (Debian) moved to KMail 2 he decided to join its development, fixing the crypto message part and getting hooked after that. He also hacks on ownCloud and maintains packages for Debian.

<strong>Michael Bohlender</strong>, Mike for the English speaking crowd, studied social anthropology with a minor in computer science. He initially started to contribute to Plasma Active and got into KDE PIM hacking through his GSOC project for developing a touch mail client. He's still mostly hacking on this client as well as QML components around mail data.

<strong>Scarlett Clark</strong>, hailing from Portland, USA, has been an avid Linux user since about 1998. She has a BA in Computer Information Systems and a background in System administration and web design. She's interested in learning more about technical writing and decided to put some effort into the documentation of her beloved OS. KDE PIM, and KMail in particular, is the lucky project she started with.

<h2>What are you up to now?</h2>
Sandro was first to explain more about what he's up to: <em>"I was very interested in KMail 2 because I thought that the architecture with Akonadi is a very good approach. A friend of mine switched to KMail 2 two years ago and I was very disappointed about the crypto support. Then a year ago I had time and started to look at the code and searched for the bugs in the code. When I heard about the PIM sprint in berlin, I was very enthusiastic and hoped to find a person to help me fix the crypto stack. At the sprint Volker and I prepared one bug fix. The sprint motivated me to make the bug fix nice and shiny and I started to close a bunch of bugs in the crypto stack. In the long run I would like to make the crypto support as good and shiny as possible."</em>

He added a call for help: <em>"I'm looking for a UI Person, who likes to make the interface for sending encrypted messages better. Now, sending a crypto message requires clicking through up to five dialogs and I can't imagine that this can not be done better"</em>.

Things are now even more exciting for Sandro: during the course of this interview, he was employed by Kolab to work on the KDE PIM stack, including the libraries, UI, kolab-utils and so on. <em>"For me it makes things easier to get paid regulary to work complety for an Open Source project. I'll hope both side will profit from my work: KDE PIM and Kolab"</em>.

Dan continued: <em>"Most of the time I spend on KDE development is hacking on Akonadi - adding new features, improving speed and of course fixing existing bugs. Occasionally I'm improving the IMAP resource, usually when a bug gets into the way of my workflow  I'm also maintaining Akonadi resources for integration with Google Contacts, Calendars and Tasks services and the library with Google API implementation, LibKGAPI. This is a special project for me, as it was my first code I contributed to KDE few years ago and through it I got involved with the awesome KDE PIM community"</em>.

Scarlett explains that she has moved on to doing packaging for Kubuntu: <em>"I have been an avid Open Source user for 15 years, so I decided to find a place for me to become a contributor and hopefully with time, a career. Documentation seemed like a great entry point, so I jumped in and was received with a lot of kindness and help from the KDE team. While I still do some documentation, I have started packaging for Kubuntu and absolutely love it, so packaging &gt; documentation right now and in the future"</em>.

<div style="float: right; border: solid thin grey; margin: 1ex; padding: 1ex">
<a href="/sites/dot.kde.org/files/scarlett-michael.jpg"><img src="/sites/dot.kde.org/files/scarlett-michael-wee.jpg" width="450" height="376" /></a><br />
Scarlett and Michael at Akademy in Brno
</div>
<h2>KDE folk and Akademy</h2>
With the <a href="http://akademy.kde.org">KDE Akademy Conference</a> going on, it made sense to ask the interviewees about their thoughts around meeting KDE people.

Dan is part of the local team organizing Akademy this year. He <em>"first attended Akademy in Tallin in 2012, to finally put faces to IRC nicknames. Meeting all the nice KDE people there was probably the main reason I decided to get more involved in KDE (PIM). I always enjoy meeting fellow KDE hackers on Akademy and various sprints and conferences, as it means lots of fun, hugs, beer and hacking (not strictly in this order ;-)) and builds these special relationships that make working on KDE pure pleasure for me."</em>

Scarlett went from <em>"I always would have loved to go to something like Akadamy, but being in the US makes it difficult to swing a trip like that"</em> to <em>"Courtesy of Ubuntu donors I am attending Akademy this year!"</em>. Her first day is over and she told us that it <em>"is really awesome! I've talked to so many people that I worked with online..."</em>

Sandro has already visited the Desktop Summit in Berlin in the past, describing it as <em>"very amazing, to get in touch with all the people I read about. On the other side, it was hard to remember so many people, because I didn't know anyone before.  When I wasn't shy to ask, the people were all very nice and chatty. It is much easier to get into touch with people at sprints, because there are less people"</em>.

Michael, also at Akademy, shared <em>"the AGM meeting wasn't very exciting but the first conference day was awesome"</em>. Meanwhile, he is working on a new mail client, build on the KDE libraries and work that was done earlier to make Akonadi and KDE PIM ready for Frameworks. He has <a href="https://forum.kde.org/viewtopic.php?f=285&t=122221">brought this idea up on the forums</a> and has gotten help and mockups from the design and usability teams.

At Akademy, the PIMsters both new and old will be coming together at a <a href="https://community.kde.org/Akademy/2014/AllBoF">KDE PIM BoF</a> on Tuesday the 9th in room 4 and there'll be plenty of work and conversation going on for sure!