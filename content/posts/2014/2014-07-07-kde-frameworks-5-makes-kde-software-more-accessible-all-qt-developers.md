---
title: "KDE Frameworks 5 Makes KDE Software More Accessible for all Qt Developers"
date:    2014-07-07
authors:
  - "jriddell"
slug:    kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers
comments:
  - subject: "KConfig: cascading"
    date: 2014-07-07
    body: "KConfig: cascading directories?, why dont get JSON config possible, so we can use open standard like manifest format."
    author: "Anonymous"
  - subject: "KDE FOR THE WIN, AGAIN!"
    date: 2014-07-08
    body: "You KDE guys and gals are True Winners. KDE 5 is some superb work. Thank you all!\r\n\r\nI think that you should teach the GNOME devs how to do it. They're struggling really badly. They just don't know what it takes to make a good Linux desktop environment.\r\n\r\nI yearn for the GNOME 2.x and the KDE 3.5.x days when there was some real competition going on. This made both GNOME and KDE better platforms.\r\n\r\nBut today, KDE is the clear winner. KDE is The Champion of Champions. While KDE 5 is some great work, I do think that some more real competition would be good for everyone. But there's just no competition now that GNOME is a dead project."
    author: "KDE FOR THE WIN!"
  - subject: "Congrats!"
    date: 2014-07-08
    body: "Congrats on the release!\r\n\r\nAnd a minor thing, the link to the announcement on kde.org (the first link) has got the URL mangled.\r\n\r\nCheers."
    author: "ricardo.barberis"
  - subject: "Hello,"
    date: 2014-07-08
    body: "Hello,\r\n\r\nThe link to the official announcement is broken in that article, it should point to:\r\nhttp://www.kde.org/announcements/kde-frameworks-5.0.php\r\n\r\nRegards."
    author: "ervin"
  - subject: "Fixed, thanks!"
    date: 2014-07-08
    body: "Fixed, thanks!"
    author: "jospoortvliet"
  - subject: "..a dead project?"
    date: 2014-07-08
    body: "Both GNOME AND the KDE community are doing great work, there is a lot going on over at the GNOME Dev's ...not sure where you got: \"GNOME is a dead project\" ?\r\nAnyhoo, I enjoy playing around with both and each has properties unique to itself."
    author: "Luke"
  - subject: "Indeed."
    date: 2014-07-08
    body: "Agreed. GNOME is doing great work, trying new UI paradigms and ways of working with a modern computer. Meanwhile, it is good that KDE developers keep familiar and efficient work flows working in Plasma, as that is very valuable for many users.\r\n\r\nBut this article is about Qt and libraries - where I'm glad to see some projects starting to move from GTK to Qt as that means they move to more platforms, even mobile/android!"
    author: "jospoortvliet"
---
<img src="/sites/dot.kde.org/files/konqui-framework_small.png" width="280" height="240" style="float: right" />
Today, the KDE community has made available the <a href="http://kde.org/announcements/kde-frameworks-5.0.php">first stable release of Frameworks 5</a>. At the <a href="http://dot.kde.org/2011/06/29/platform-frameworks-kde-hackers-meet-switzerland">Randa Meetings back in 2011</a>, we started work on porting KDE Platform 4 to Qt 5. But as part of this effort, we also began modularizing our libraries, <a href="https://dot.kde.org/2013/12/17/qt-52-foundation-kde-frameworks-5">integrating portions into Qt 5 proper</a> and modularizing the rest so applications can just use the functionality they need. Three years later, <a href="http://www.kde.org/fundraisers/randameetings2014/">while a fundraiser for the 2014 Randa Meetings is in progress</a>, Frameworks is out. Today you can save yourself the time and effort of repeating work that others have done, relying on <a href="http://kde.org/announcements/kde-frameworks-5.0.php">over 50 Frameworks with mature, well tested code</a>.  For a full list and technical details coders can read the <a href="http://api.kde.org/frameworks-api/frameworks5-apidocs/">API documentation</a>.

<h2>Highlights</h2>
<p>
<strong>KArchive</strong> offers support for many popular compression codecs in a  self-contained, featureful and easy-to-use file archiving and extracting  library. Just feed it files; there's no need to reinvent an archiving  function in your Qt-based application!
<br clear="all" />

<strong>ThreadWeaver</strong> offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread  execution by specifying dependencies between the threads and executing  them satisfying these dependencies, greatly simplifying the use of multiple threads.
<br clear="all" />

<strong>KConfig</strong> is a Framework to deal with storing and  retrieving configuration settings. It features a group-oriented API. It  works with INI files and XDG-compliant cascading directories. It  generates code based on XML files.
<br clear="all" />

<strong>Solid</strong> offers hardware detection and can inform an  application about storage devices and volumes, CPU, battery status,  power management, network status and interfaces, and Bluetooth. For  encrypted partitions, power and networking, running daemons are  required.
<br clear="all" />

<strong>KI18n</strong> adds Gettext support to applications, making it easier to integrate the translation workflow of Qt applications in the general translation infrastructure of many projects.

This is just a taste of the many Frameworks made available today.
<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />
<h2>Getting started</h2>
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<ul>
<li><a href='http://community.kde.org/Frameworks/Binary_Packages'>Binary package distro install instructions</a>.<br /></li>
</ul>
Building  from source is possible using the basic <em>cmake .; make; make  install</em> commands. For a single Tier 1 framework, this is often  the easiest solution. People interested in contributing to Frameworks  or tracking progress in development of the entire set are encouraged to  <a href='http://kdesrc-build.kde.org/'>use kdesrc-build</a>.

Frameworks 5.0 requires Qt 5.2.  It represents the first in a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
<ul>
<li><a href='http://kde.org/info/kde-frameworks-5.0.0.php'>KDE Frameworks 5.0 source info page with known bugs and security issues</a></li>
</ul>
<h2>Where the code goes</h2>
Those interested in following and contributing to the development of Frameworks can check out the <a href='https://projects.kde.org/projects/frameworks'>git repositories</a>, follow the discussions on the <a href='https://mail.kde.org/mailman/listinfo/kde-frameworks-devel'>KDE Frameworks development mailing list</a> and send in patches through <a href='https://git.reviewboard.kde.org/groups/kdeframeworks/'>review board</a>.

KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='http://www.kde.org/community/donations/'>donations page</a> for further information. And as was mentioned above, KDE is currently running a fundraiser to make the <a href="http://www.kde.org/fundraisers/randameetings2014/">Randa Meetings 2014</a> possible. Your contribution is crucial to make an event like this possible - and with that, projects like KDE Frameworks!</p>