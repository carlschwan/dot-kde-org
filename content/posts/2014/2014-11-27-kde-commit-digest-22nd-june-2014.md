---
title: "KDE Commit-Digest for 22nd June 2014"
date:    2014-11-27
authors:
  - "mrybczyn"
slug:    kde-commit-digest-22nd-june-2014
---
In <a href="http://commit-digest.org/issues/2014-06-22/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://edu.kde.org/marble/">Marble</a> can preview Geo Data files (GPX, OSM, KML, ESRI) in the file manager or file dialogs</li>
<li>Plasma workspace introduce a list of apps that are limited to single notification only to limit the number of notifications the user sees</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> gains an initial implementation of native Gmail resource</li>
<li><a href="https://krita.org/">Krita</a> allows global selection masks to be shown in the Layers docker to be used with other tools; implements drag & drop of multiple layers</li>
<li>First approach to a KF5 port of KDE Connect.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-06-22/">Read the rest of the Digest here</a>.