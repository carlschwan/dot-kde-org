---
title: "Ian Wadham, Venerable KDE Programmer"
date:    2014-06-03
authors:
  - "kallecarl"
slug:    ian-wadham-venerable-kde-programmer
comments:
  - subject: "Thanks for your work!"
    date: 2014-06-04
    body: "Thank you for all the contributions that you've made and just because you are \"retiring\" don't feel like you can't still be a part of KDE!"
    author: "Chuckula"
  - subject: "Thank you"
    date: 2014-06-04
    body: "Games are important. Thanks for your work and for this interesting interview.\r\n\r\nA KPat player, rare KSudoku player, and previously Riven and Alpha Centauri player"
    author: "Filipus Klutiero"
  - subject: "Thank You"
    date: 2014-06-04
    body: "Ian\r\n\r\nThank you for all your work on the KDEGames suite, really enjoy your work on KSudoku."
    author: "George Moody"
  - subject: "Thank you!"
    date: 2014-06-04
    body: "Thank you Ian not only for all your work on KDE, but also for this interview - it was a very interesting read!\r\n\r\nThanks also to everyone who helped to get this interview published here - this was a brilliant idea, Albert!"
    author: "Frank Reininghaus"
  - subject: "Thank you"
    date: 2014-06-05
    body: "Many thanks to Ian for excellent and always well-documented applications."
    author: "Yuri Chornoivan"
  - subject: "Thank you, everyone"
    date: 2014-06-06
    body: "Thank you, everyone for your comments and compliments.\r\n@Chukula: I certainly intend to stay in touch with KDE and may reappear from time to time to rattle some cages... ;-)\r\n@Filipus: Games are certainly important, not just as an intro for new users or new KDE developers, but also because they have driven the development of PC hardware, graphics and software for the last 30 years. KDE Games, in particular, figured prominently in the launch of KDE 4.0 and were an early stimulus for Qt 4 graphics software to become faster and more efficient."
    author: "Ian W"
  - subject: "Interesting Interview!"
    date: 2014-06-09
    body: "Wow, this was really an interesting interview. So much experience for us to learn from! I'm glad you will still be part of KDE.\r\n\r\nThough it's always a bit sad to see a KDE contributor finding his home in a different desktop environment, but it also motivates us to improve Plasma.\r\n\r\nYou should give Plasma Next (and especially the Breeze artwork) a try when it comes out, you'll likely find it easier on the eyes and less distracting than Plasma 4.X and Oxygen."
    author: "Thomas Pfeiffer"
---
The <a href="http://kde.org/announcements/4.13/applications.php">KDE Applications 4.13 announcement</a> highlighted the delightful new capabilities of Palapeli, the KDE jigsaw puzzle application. What the announcement did not mention is that the Palapeli maintainer, Ian Wadham, is celebrating 50 years of software experience. He’s ready to hand off Palapeli and his other KDE software development responsibilities. Albert Astals Cid called attention to Ian’s achievements and suggested a Dot interview.
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/IanWadham.jpg" /><br />A Portrait of the Programmer as a Young Man</div>
<!--break-->
<big><strong>Ian Wadham's bio at a glance</strong></big>
<ul>
<li>1938 - Born in England</li>
<li>1959 - Graduated in Physics and Mathematics at the University of Cambridge</li>
<li>1960 - Migrated to Australia</li>
<li>April 1964 - Became a computer programmer 50 years ago</li>
<li>KDE Games developer for about 12 years</li>
<li>Currently lives in Melbourne, Australia</li>
</ul>

<big><strong>What are your thoughts about cutting back on software development?</strong></big>
Regret, but I have had a long innings. I will still potter around on my own, but not following any schedules. If something turns out well, I might commit it.

This is my second retirement. My first, from the workforce, was in 1998. This time I am withdrawing from writing programs for public use. I will continue to present a Science course for seniors at the local U3A (<a href="http://www.u3a.org.au/links/U3A">University of the Third Age</a>) .

I seem to be getting involved in moves to make KDE's portability work better on the Apple Mac OS X platform. And my grandchildren are always a joy.

<big><strong>How did you get started as a coder?</strong></big>
I prefer to call myself a programmer - and never a hacker. A hacker is someone who carves wood with an axe. I see programming as a craft. Sometimes I hack, to get a quick answer to a question, but when I have the result, I always like to go back and program it "properly". If I do not, I find I cannot understand my own code a few months later.

How I started was one of those accidents of fate. My Ph.D. studies were not working out and I was looking for a new career. My girl-friend at the time was a programmer and she told me no qualifications were required, only an aptitude test, and that the job was interesting and the pay excellent. This was 1964 and I was nearly 26.

So I put in some applications and accepted the offer of my first programming job the night before I was interviewed as a Physics Instructor in the Australian Navy. All next morning I was saying that I had already accepted another job, but the military has its own ways of doing things. I went through the full medical check, the eye test, the IQ test, the psychology interview... Finally I entered a room with a long table and wall-to-wall admirals and captains - gold braid everywhere - and was finally allowed to deliver my news. "Oh, thank you for telling us," they said.

At that time in Australia, very few physicists were using computers. Computer use was more common in the US, UK and Europe, especially in large, well-financed organizations such as NASA, the Atomic Energy Commission and the US Military.

Computers for individual physicists were an exotic and trendy means of avoiding lengthy and tedious calculations, if you could afford the time and money to acquire one and learn how to use it, but were not yet a routine tool as computers became within the following 10 years.

<big><strong>What major technology shifts have you been involved in?</strong></big>
You name it. It was good luck to join in when transistor hardware, assembly language and Fortran II were taking hold. Some people were still using valve machines and machine language. As a programmer I was involved in the beginnings of operating systems, real-time systems, on-line screens, minicomputers, structured programming, supercomputers, LANs, desktop systems (Xerox Star), relational database systems, C and multi-host commercial Unix systems.

<big><strong>Companies? </strong></big>
The best company I ever worked for was Control Data Australia, 1965 to 1969. We have a <a href="http://excda.site44.com/">website of memories</a>, and we still keep in touch. On 17 May, we had a biennial reunion. I do not know of any other company group like it.

Control Data Corporation, our US parent company, made the largest and fastest computers in the world. Our chief designer was <a href="http://en.wikipedia.org/wiki/Seymour_Cray">Seymour Cray</a> and for decades he designed the world's largest and fastest computers: later in Cray Research, his own company. At Control Data he insisted on seclusion and freedom to work in his private laboratory in his home town, Chippewa Falls, Wisconsin. There were many legends about him. One of my favorites is that each morning he would walk down to the river near the Falls and an eagle that nested there would fly down and perch on his shoulder.
<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/CDC160A700.jpg" /><br />CDC-160A Personal Computer</div>
My first computer was one of Seymour's lesser-known designs, <a href="http://archive.computerhistory.org/resources/text/CDC/CDC.160A.1962.102646114.pdf">a Control Data 160-A</a>. The desk in the foreground is the entire machine. Some say it was the first minicomputer, even the first PC. It was significant too because essentially the same design, shaped as a 20cm cube, went into the ten peripheral processors on the CDC 6600 supercomputer. The 160-A was a wonderful first machine because it was possible for one person to learn everything about it - something that has been impossible with most machines since.

<big><strong>This was your "personal" computer? Wow!</strong></big>
Er, no! I was a young, penniless guy and it cost US$60,000. I meant it was the first computer I worked on, not the first one I owned. It belonged to Australia's telephone research labs and I was programmer in charge, bureau manager and general dogsbody. If I stayed late at work, I could have a play... 

<big><strong>FOSS Projects?</strong></big>
KDE is my only official FOSS project, but I feel that, by avoiding the IBM and Microsoft worlds, I have been able to work on FOSS all along. Free because it came with the hardware and you could modify it or fix bugs (i.e. also free in the Stallman sense). In the 80s and 90s you could still obtain access to source code (UNIX) on minis and megaminis for a fee, but I never found the need.

<big><strong>How did you come to KDE? And how long have you been part of KDE?</strong></big>
Microsoft and its business practices had always turned me off. My first IBM compatible PC (1995) was mainly for work purposes. After retiring in 1998, I started programming again in 2000. My son gave me a set of SuSE CDs for Christmas and it included this new desktop system called KDE 1.

Among the source code, in the Alpha section, I found the first version of KGoldRunner, by Marco Krüger. I had always liked Loderunner's unique combination of action, strategy and puzzle solving and had always wanted to do something non-trivial in object-oriented programming, ever since Simula and Smalltalk days. So I set to work to learn C++ and Qt and with Marco's permission produced a new version of KGoldrunner, committed to kdenonbeta (a precursor of playground and review) in March 2002.

<big><strong>Akademy is being held this year in Brno, Czech Republic. Have you been to Akademy?</strong></big>
Yes, Akademy 2008 in Belgium. The talks were great and so were the BOF sessions on games. That Gouden Carolus beer on the river trip and on the dinner night at the brewery in Mechelen was something else again. I now know it to be one of the world's most powerful beers - and they were serving it in half-litre glasses! The high point of Akademy 2008 for me was to meet my fellow workers on KGoldrunner and take them to dinner in the main square at Mechelen.

<big><strong>You maintain several KDE applications, mostly games. What are they?</strong></big>
KGoldrunner, Kubrick, KJumpingCube, KSudoku and Palapeli. They are about 13% of the KDE Games, but 20-25% of the lines of code.

KGoldrunner is based on my all-time favorite game: Loderunner. One intriguing thing about it is the way bugs become features. One day I was sitting with my son (grown up) when he found out that it was possible for the hero to dig holes while falling through the air. Before I could fix the bug, he had made up a level that exploited it. Now that "feature" is an important part of many creative new games that people from around the world have contributed.
 
Kubrick was an effort to branch out into 3-D and OpenGL. It's fine, but I am no good at cubing and I wonder if others enjoy Kubrick.

KJumpingCube and KSudoku I rescued from unmaintained. In KJC I added features and AI to make it more intelligible and also more challenging. In KSudoku, there was a half-finished re-development which left it so that it would generate mainly easy puzzles - no good at all for a serious player like my wife. I found a Python puzzle-generation algorithm on the net and, with the author's permission, adapted it to C++ and KSudoku. I like KSudoku because it supports so many variations on the basic puzzle. I do not know of any other Sudoku game that does that.

My favorites games to play are KPat (solitaire card games), KSudoku (X and Aztec variations) and Palapeli.

<big><strong>You've gotten the applications into good shape, and are ready to hand them off. What type of person would you like to see take over? What will they get out of working on these applications?</strong></big>
I would like to see KDE set up a maintenance group and standards for "maintainability" of code. Programs that reach a reasonably good standard could then be maintained interchangeably by members of the group.

The group could be continually changing. Nobody can stay interested in such work for long. Also the group and its stock of programs would be a good source of Junior Jobs and a place for newbies to start. It would need to have some experienced members, or ready access to such people, because some bugs are too hard for trainees to solve.

This is not a new idea. It is roughly what has been happening everywhere I have worked since about 1967, when the burden of people quitting jobs and leaving behind unmaintainable, half-finished messes became intolerable for most organizations.

<big><strong>What was your experience in the various game transitions from early days to now? Did you play computer games when they were first available?</strong></big>
Games go back a very long way. Some of the earliest computers played music and a few played games. I remember a fellow-student in 1959 giving me a copy of a paper tape as a souvenir. It contained an EDSAC II program to determine if the king was in check.

Even the first supercomputer, the Control Data 6600, in 1966, had a game similar to KSpaceDuel (spaceships orbiting the Sun and shooting missiles). Only the elite and hardware engineers got time late at night to play games on those multi-megadollar machines.

Things became easier with minicomputers. In 1978 I was at a customer's DEC PDP-11 site presenting a new version of their application system and was asked to finish up early. Why? It was the night the users all got on the computer to play Adventure, the original adventure game. They had made a wall-chart mapping out all the caves.

On our first PC, an Apple IIC, my children and I played a lot of Loderunner and Zork ("You are in a maze of twisty little passages, all of them alike."). That was in 1983. We wore out several joysticks on Loderunner. We made up levels and challenged each other to solve them. Many of the current KGoldrunner levels come from that time. My Apple IIC still runs and can still play the Loderunner demo, but I used up the last gasp of the last joystick working out design details to be used in KGoldrunner. So I cannot play Loderunner now.

Later I had a Commodore Amiga 500 and became very hooked on <a href="http://en.wikipedia.org/wiki/Flood_(video_game)">Flood</a> and <a href="http://en.wikipedia.org/wiki/Populous">Populous I and II</a>. The Amiga had much better graphics and OS than the IBM PC and Windows, but sadly the Commodore company lost its way. My Amiga also is still in working order.

Later still, on Windows, I worked my way through Myst, Riven and Alpha Centauri. I never liked first person shooters, though.

<big><strong>You've added a lot to Palapeli, the jigsaw puzzle application. The new capabilities make it possible to do puzzles with a lot of pieces. It's fun and challenging. The manual is quite helpful, especially with a lot of pieces. What was the thought process to create the latest version?</strong></big>
Several of us in the KDE Games group had kicked around ideas in 2010. Johannes Löhnert wrote the puzzle Preview class then, but it did not get implemented.

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/JuanGrisTheBook700.png" /><br />The Book by Juan Gris in Palapeli; 320 pieces; main screen, right edge piece-holder, preview</div>

It seemed necessary to resolve the issues with a practical test, so I bought a 1000 piece puzzle and tried to solve it on a small table, no larger than the completed puzzle, using only the table and the box to hold pieces. As I played, I noted difficulties that arose and how they could be overcome. At the same time, I was mentally trying out analogues of what might be feasible on a computer screen. And of course, I had already tried solving large puzzles with Palapeli 1 and had seen what physical difficulties arose there.

Most of the manual was already written by Johannes Löhnert and Stefan Majewsky. I just added the chapter on large puzzles.

<big><strong>When I was growing up, we often had a jigsaw puzzle on the dining room table. It was a social event. What about adding a capability for working with others on a puzzle with a lot of pieces?<strong></big>
That's a nice idea. I have never understood how networking is done in KDE Games. Maybe it is time I found out.

<big><strong>It's hard to beat the computer at KJumpingCube. Do you have any strategy tips?</strong></big>
Only what is in the Handbook. KJC is about to become harder. Inge Wallin has an AI library he would like to try out on it. You have met the AI players Kepler and Newton. Prepare to meet Einstein!

<big><strong>What would you recommend to young programmers?</strong></big>
Read as many of other people's programs as you can - even bad programs. Try and figure out what makes them good or bad, easy or hard to understand and maintain.

<big><strong>Can people learn to program when they are older?</strong></big>
Yes. My friend Glen, a retired airline pilot, taught himself on an IBM PC when he was fifty something. Within a year or two he became good enough to get work as an independent contractor.

<big><strong>Windows, Mac or Linux? Why?</strong></big>
Apple Macbook Pro, with OS X 10.7, Lion.

One day my Windows/Linux dual-boot system's hardware died suddenly. I bought a new machine and installed the latest OpenSuSE. When I booted up KDE, my carefully constructed four-part Plasma desktop had been long gone, and I found myself in some new, empty and quite alien-seeming version of KDE and Plasma. It took me two days just to find out how to get rid of the blue glow around active windows, which was hard on my tired old eyes.

<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/IanW.jpg" /><br /><div align=center>Ian Wadham - older, wiser, still gaming</div></div>
It was important for me to keep working rather than play with settings, so I turned to the MacBook I had been messing around with, which was also supporting my wife's iPhone and iPad (pre iCloud). I had some KDE and Qt software already installed, with MacPorts, and I was soon able to set up a KDE development environment. OS X really is quite a lot like Unix and Linux.

I like working on the Macbook. The desktop is quiet, unobtrusive and easy on the eyes. I can work for hours without getting tired or being distracted. Also the battery is long-lived, Time Machine does regular backups and the Spotlight indexer collects everything (even my source code) with no perceptible overhead. I feel I am more productive with Mac OS X than I ever was with Windows or KDE because I do not have to think about what the desktop is doing.

I feel as if I have come home.

<big><strong>The traditional KDE interview question—Richard Stallman or Linus Torvalds?</strong></big>
<a href="http://en.wikipedia.org/wiki/Edsger_W._Dijkstra">Edsger Dijkstra</a>.

Thank you for the many years of work you've dedicated to the advancement of FOSS, and the KDE games in particular. Thanks also for sharing some of your experiences on the front lines of computer development over the fifty years of your active career.

Many thanks to Albert for the idea of interviewing Ian, and to Bob Potter for bringing in technical perspectives. 