---
title: "Akademy 2014 Program Schedule: Fast, fun, inspiring"
date:    2014-08-04
authors:
  - "jospoortvliet"
slug:    akademy-2014-program-schedule-fast-fun-inspiring
comments:
  - subject: "recorded video"
    date: 2014-08-04
    body: "Will the keynotes & the sessions be recorded and made available online for those who cannot attend?"
    author: "Jay"
  - subject: "When are lightning talks"
    date: 2014-08-05
    body: "When are lightning talks planned? "
    author: "Anonymous"
  - subject: "How about streaming it live?"
    date: 2014-08-08
    body: "How about streaming it live?"
    author: "Arjun AK"
  - subject: "Yes talks will be recorded"
    date: 2014-08-09
    body: "Yes, the talks on Saturday and Sunday will be recorded"
    author: "sealne"
  - subject: "Streaming is not currently"
    date: 2014-08-09
    body: "Streaming is not currently planned but the videos should be available within a few days"
    author: "sealne"
  - subject: "fast track talks"
    date: 2014-08-09
    body: "The 10 minute fast track talks on the Saturday and Sunday morning are effectively lightning talks."
    author: "sealne"
  - subject: "Great!"
    date: 2014-08-10
    body: "thanks for the effort!"
    author: "Jay"
  - subject: "Thanks"
    date: 2014-08-12
    body: "Thanks a lot. Where to publish the videos, then?"
    author: "Chenxiong Qi"
  - subject: "access videos at the Akademy website"
    date: 2014-08-12
    body: "The videos will be available through the Akademy website. There will be an announcement that provides more details."
    author: "kallecarl"
---
<div align="center" style="float: center; padding: 1ex; margin: 1ex; margin-top: 0ex; "><a href="https://akademy.kde.org/2014"><img src="/sites/dot.kde.org/files/banner01700.jpg"></div>
The Akademy Program Committee is excited to announce the Akademy 2014 Program. It is worth the wait! We waded through many high quality proposals and found that it would take more than a week to include all the ones we like. However we managed to bring together a concise and (still packed) schedule.

<h2>Sharing</h2>
As we wrote in the <a href="https://akademy.kde.org/2014/cfp">Call for Papers</a>, sharing is an important goal of Akademy. So on Saturday and Sunday in the morning, there will be a single track in the main room which will start with a <a href="https://dot.kde.org/2014/07/30/akademy-2014-keynotes-sascha-meinrath-and-cornelius-schumacher">keynote</a>, followed by 9 short talks. These cover a wide range of KDE-related topics including technical, governance, design, social issues and more, providing inspiration and material for further conversation and debate. Some examples are:
<ul>
<li>the <a href="https://conf.kde.org/en/Akademy2014/public/events/103">state of KWin</a> and <a href="https://conf.kde.org/en/Akademy2014/public/events/107">Akonadi</a>,</li>
<li>an argument for <a href="https://conf.kde.org/en/Akademy2014/public/events/161">collecting data on KDE usage</a>,</li>
<li>the work involved in moving <a href="https://conf.kde.org/en/Akademy2014/public/events/130">GCompris to QtQuick</a>,</li>
<li><a href="https://conf.kde.org/en/Akademy2014/public/events/142">release management</a>,</li>
<li>and <a href="https://conf.kde.org/en/Akademy2014/public/events/102">automated testing</a>.</li>
</ul>

In the afternoon, there are two tracks of longer, more traditional talks, with a stronger-than-usual in-depth focus. The goal of these sessions is to share knowledge and experience, to learn from each other. In these sessions, you can explore:
<div style="width: 200px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="https://community.kde.org/File:Mascot_20140728_konqui-app-presentation.png"><img src="/sites/dot.kde.org/files/mascot_20140728_konqui-app-presentation_wee.png" /></a></div> 
<ul>
<li><a href="https://conf.kde.org/en/Akademy2014/public/events/92">design</a>,</li>
<li><a href="https://conf.kde.org/en/Akademy2014/public/events/126">writing faster QtQuick code</a>,</li>
<li><a href="https://conf.kde.org/en/Akademy2014/public/events/86">how to port an application from GTK to Qt</a>,</li>
<li><a href="https://conf.kde.org/en/Akademy2014/public/events/106">KDE on Android</a>,</li>
<li><a href="https://conf.kde.org/en/Akademy2014/public/events/141">accessibility</a>,</li>
<li><a href="https://conf.kde.org/en/Akademy2014/public/events/133">Frameworks 5</a>,</li>
<li>and <a href="https://conf.kde.org/en/Akademy2014/public/events/143">the importance of communication</a>.</li>
</ul>

The <a href="https://conf.kde.org/en/Akademy2014/public/schedule">entire schedule is here</a>.

The Akademy Program is packed with goodies for many interests. And there will be many opportunities to learn, contribute, and work hard throughout Akademy. <a href="https://akademy.kde.org/2014/register">We urge you to register</a> now if you haven't already!

<h2>Akademy 2014 Brno</h2>
For most of the year, KDE—one of the largest FOSS communities in the world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2014">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work to bring those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.