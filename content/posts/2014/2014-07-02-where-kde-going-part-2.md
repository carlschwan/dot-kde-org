---
title: "Where KDE is going - Part 2"
date:    2014-07-02
authors:
  - "jospoortvliet"
slug:    where-kde-going-part-2
comments:
  - subject: "Advertisement"
    date: 2014-07-03
    body: "Thanks for the great article Jos!\r\n\r\nRecently, I get to talk to many people that want to switch from Windows to Linux. Most of them don't know where to start or know Ubuntu. In my optinion the lack of a good advertisment campaign is a major shortcoming in Linux promotion. When I tell those people to start off with Kubuntu because it's got a huge community and using the KDE is a bit alike when one knows Windows. Then they are confused about the difference to Ubuntu and the various desktop environments.\r\n\r\nI suggest, that KDE e.V. sets up an advertisement campaign. The theme of the ad could be something like: \"A computer. Your tool. Your freefom.\" or quite neatly \"Kan Do Everything\" for the letters of KDE. Of course, such a campain should promote KDE software, but the focus should be on the desktop and a pointer or linklist of where to obtain Linux distributions directly shipping with KDE. A good start for a link list would be \"linux.kde.org\" which is not used yet and it's very easy to remember. There should be short non-technical descriptions of the distributions and why an average consumer should favor that distribution over others to help them in their decision.\r\n\r\nI'm not into advertisement business, but such a campain should be run on TV and Posters along road sides."
    author: "Robert Wloch"
  - subject: "But who's gonna pay..."
    date: 2014-07-05
    body: "Thanks for the compliment, AND the suggestion! I'm afraid, however, that we won't be doing this anytime soon... Despite the fact that it would be awesome to see KDE mentioned road-side ;-)\r\n\r\nThe thing is - if we had they, say, 15 million needed for such a campaign - would it be the best investment possible? Hiring somebody to improve KDE's communication and marketing certainly makes sense. I doubt that road-side advertisements are a wise move at this point, however, as we'd first have to capture a larger part of the enthusiast market. And I'd argue that at least part of this money should go into further polish of the user experience of KDE software - the 'finishing touch' is notoriously hard to do in a volunteer community and we certainly suffer from that, still."
    author: "jospoortvliet"
  - subject: "Atheist bus campaigns"
    date: 2014-07-13
    body: "It doesn't need to be so expensive. Especially if you make the campaign smart and/or provoking, just few thousands euro can give a huge visibility in a country. Ads nowadays cost very little compared to 5 years ago so we've had several organisations trying.\r\nhttps://en.wikipedia.org/wiki/Atheist_Bus_Campaign"
    author: "Federico"
---
This is the second half of the 'where KDE is going' write-up. <a href="http://dot.kde.org/2014/06/26/where-kde-going-part-1">Last week</a>, I discussed what is happening with KDE's technologies: Platform is turning modular in Frameworks, Plasma is moving to new technologies and the Applications change their release schedule. In this post, I will discuss the social and organizational aspects: our governance.

<div style="float: right; padding: 1ex; margin: 1ex; width: 300px; "><img src="http://dot.kde.org/sites/dot.kde.org/files/ev.png" /></div>
<h2>KDE e.V.</h2>
You can't talk about KDE and governance without bringing up KDE e.V. (<em>eingetragener Verein</em> or, in English, registered association). This Germany-based non-profit is the legal organization behind the KDE community, and it plays several important roles.

Initially set up to handle funding for KDE's conferences, the e.V. still has the organization of events as a major task. But that is no longer limited to the yearly Akademy conference. There are now events all over the world, from Camp KDE and Lakademy in the Americas to conf.kde.in in India. In addition, many Developer Sprints, usually with about 5-15 people, are supported, as are the <a href="http://dot.kde.org/2014/05/27/randa-moving-kde-forward">annual Randa Meetings</a> which can attract 40-60 developers.

KDE e.V. also provides legal services, pays for infrastructure and takes care of our trademarks. The <i>KDE Free Qt Foundation</i> is equally represented by KDE and Digia. The Foundation was set up between the former Trolltech and KDE to maintain Qt's open status, and has been continued with Nokia and Digia, the successive holders of the Qt trademark. KDE e.V. is not the guardian of technical decisions, however. This is up to the natural community development processes.

<h3>Change</h3>
But KDE e.V. does act as an agent of change. It provides a place where core KDE contributors come together and discuss a wide variety of subjects.

<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://manifesto.kde.org"><img src="http://dot.kde.org/sites/dot.kde.org/files/Manifesto%20tree.jpg" /></a><br />We created the KDE Manifesto</div> 

In the last 8 years or so, KDE e.V. has been the major driver behind increasing the number of developer sprints and has created the Fiduciary Licensing Agreement which allows it to re-license KDE code when needed, while protecting developers’ interests. The Code of Conduct originated with KDE e.V., as did our Community Working Group which helps deal with communication issues in the community.  

A recent example of our ongoing improvement efforts is the <a href="http://manifesto.kde.org">KDE Manifesto</a>. This has been for a very long time coming but Kévin Ottens got it to the finish line.

The Manifesto explicitly defines our community: our values and our commitments to each other. The importance of this can hardly be overstated – knowing who you are and what you want helps you make decisions but also shows others what you are about. The Manifesto made plain what was involved in being part of the KDE Community, including the benefits and the ways we operate.

<h3>Joining the KDE community</h3>
Since the Manifesto was written, several projects have joined KDE or begun the process of doing so:
<ul>
<li>KBibTex (a bibtex editor)</li>
<li>QtCurve (theme/style)</li>
<li>Kdenlive (non-linear video editor)</li>
<li>Tupi (2d animation tool)</li>
<li>GCompris (an educational application, former GNOME project - <a href="http://gcompris.wordpress.com/2014/06/06/preview-gcompris-qt-0-11/">progress</a>)</li>
<li>The wikiFM project, an Italian university-based knowledge database</li>
<li><a href="http://aseigo.blogspot.de/2012/10/kde-manifesto-in-action-bodega-server.html">Bodega</a> (digital content store)</li>
<li>Kxstitch (cross stitch patterns and charts)</li>
<li>Trojita (a Qt IMAP mail client)</li>
</ul>

<div style="width: 604px; float: center; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/project%20logos_0.png" /></div> 

It is clear that these projects contribute to the growing diversity in the KDE community. The many projects joining us has prompted some refinement of the KDE Incubator, an effort to document the process of becoming part of the KDE community (also started by Kévin, who probably felt sorry for the fallout of his manifesto creating so much extra work ;-)).

Other projects have moved on, or emerged from the KDE community and have become independent communities. Examples are Necessitas (provides Qt-on-Android) and Ministro (installer for libraries on Android) but also the well-known <a href="http://owncloud.org">ownCloud project</a> which was announced at Camp KDE 2010 in San Diego and still has many KDE folks involved in it.

<div style="float: right; padding: 1ex; margin: 1ex; width: 300px; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" /></div>

<h3>Growing Qt'er</h3>
KDE has grown – and so has Qt (pronounced 'cute'). The Qt ecosystem today is very big – it is estimated that there are half a million Qt developers world wide! And not only has Qt usage grown, but so has the ecosystem around it, with more and more contributions from more and more companies, communities and individuals. Including KDE.

KDE has always been close to the Qt community, with many KDE developers working for former Trolltech, now Digia, or in one of the companies providing Qt consulting. KDE played a major role in establishing the <i>KDE Free Qt Foundation</i>, and KDE people were critical to the process of creating Open Governance within the Qt Project in 2011. In 2013, the Qt Contributor Summit was co-located with Akademy, and as discussed in the previous article, KDE is contributing a lot of code to Qt and building closer bonds with the Qt ecosystem through our Frameworks 5 efforts.

<h2>Extrapolating change</h2>
Based on the above, one could extrapolate. More projects will join the KDE Community, and KDE will become more diverse. KDE is also working on formalizing the relationships with external entities through a community partnership program. This will allow KDE e.V. to work closer with other communities in legal and financial matters and share KDE’s strengths with them. With these changes, the community shows a desire to expand its scope.

<div style="width: 304px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/jtg%20pieces.jpg" /></a><br />The <a href="http://jointhegame.kde.org">Join the Game program</a> looked for 500 KDE supporters</div> 

Another area where change might take place is in the financial area. KDE e.V. does not have the mandate to define technical directions. To sponsor a developer, the Krita team set up the Dutch Krita Foundation to handle the funds. Currently, Krita, Free Software's most successful and powerful drawing application, is running <a href="http://krita.org/kickstarter.php">a Kickstarter campaign</a> to obtain funding for several developers with the aim of bringing the upcoming Krita 2.9 release to a new level. In other cases, external organizations supported developers working on KDE code, like <a href="https://kolabsys.com/">Kolabsys</a> supporting several developers on the KDE PIM suite (like <a href="http://cmollekopf.wordpress.com/2014/05/21/a-new-folder-subscription-system/">Christian</a>), and of course the various Linux Distributions (<a href="http://ltinkl.blogspot.com/2014/06/last-month-may-in-red-hat-kde.html">like Red Hat</a>) which have made massive improvements to KDE possible over the years.

Paid development is a complicated topic, as most KDE contributors volunteer their time. Money can be damaging to such intrinsic motivation. At the same time, some tasks are just no fun – paid developers can perhaps help. Some people in the community feel that KDE e.V. (or perhaps another organization?) could play a more active role in raising funds for certain projects, for example. The Randa Meetings fundraiser 2 years ago might be a sign of things to come, and again we've started a fundraiser to make Randa 2014 as successful as the earlier meetings: <a href="http://dot.kde.org/2014/05/27/randa-moving-kde-forward">Please support it</a>!

There may be more sustaining membership programs such as 'Join the Game' in the future, and suggestions, ideas and practical help in obtaining and using funds for KDE development are very much welcome.

<h3>Not all change</h3>
But in all this change, it is crucial that the KDE community preserves what makes it work well. KDE has gotten where it is today by the <em>culture and practices</em> of today (see <a href="http://blog.jospoortvliet.com/2014/05/open-governance-update.html">some of my thoughts on this</a>).  Like in any community, these are hidden rules that allow KDE to pool the knowledge of so many brilliant people, and without too much politics, to make the <a href="http://blog.jospoortvliet.com/2013/05/consensus-decision-making.html">best decisions possible</a>. The KDE culture, so to say. This includes well known Free Software soft rules like Who Codes, Decides, RTFM, Talk is Cheap and Just Do It but also very <em>'KDE'</em> rules like Assume Good Intentions and Respect the Elders. And just like in the French Liberté, Egalité, Fraternité, the rules are inseparable and interdependent. They are what makes KDE such an amazing place, full of creativity, innovation and fun.

Of course, in other areas, it can hamper progress or block people from making changes as quickly as they like. All culture has good and bad aspects, and we should stay flexible to adapt to changes and <a href="http://tsdgeos.blogspot.com/2014/05/kde-is-nice-community-but-we-can-do.html">fix shortcomings</a>. This is a slow process, often counted in years, rather than months - putting off some people who wish things went a little faster. But change happens.

<h3>Change to keep things healthy</h3>
The KDE Community Working Group is an institution with the goal of preserving positive aspects of our culture. The CWG consists of a group of trusted community members dealing with conflicts and social issues. It has been around since 2008 and has recently been given the <a href="http://community.kde.org/Akademy/2013/ConflictResolution#Proposal_for_the_very_stormy_day">ability to deal with 'stormy day' scenarios</a> – they will now handle situations where somebody is persistently behaving contrary to the culture. Luckily such situations are extremely rare but it is good to have a process in place to deal with them.

The <a href="https://mail.kde.org/mailman/listinfo/kde-community">kde-community mailing list</a> has moved many discussions into an open forum that used to be limited to KDE e.V. members. Opening up our internal discussions is more inclusive; it preserves the open nature of our project in the face of growth and change.

<h3>Meta change</h3>
Putting these changes together, a pattern starts to emerge:
<em>The Manifesto has defined what we have become and what we want. It has brought in other people and other projects. We open up our community, formalize community management. KDE e.V. wants to collaborate more with other organizations, Frameworks 5 brings KDE technology to a wider audience, and we're bringing our desktop and application technology to more and more devices.</em>

<b>I think KDE is going meta.</b>

KDE is becoming a meta organization. Perhaps you can call it an Eclipse for GUI or end user software, bringing a wide variety of projects together under one umbrella. The challenge for the KDE community is to guide these changes, keep good practices and develop new ones that fit the new world!

<h2>Conclusion</h2>
KDE is becoming a umbrella-community, a community of communities. A place where people with a huge variety of interests and ideas come together, sharing a common vision about the world. Not a technical vision, mind you, but a vision about HOW to do things. Shared values are what brings us together. And with KDE going meta, there is more room for everybody!

<em>These articles are based on <a href="http://kde.in/content/where-kde-and-where-it-going">a talk</a> given at <a href="http://conf.kde.in">conf.kde.in</a> by <a href="http://blog.jospoortvliet.com">Jos Poortvliet</a> with lots of input from KDE contributors. A more extensive version of these articles, quoting members of the KDE community for more background, can be found in the upcoming September issue of <a href="http://www.linuxvoice.com/">Linux Voice magazine</a>, 'the magazine that gives back to the Free Software community'</em>