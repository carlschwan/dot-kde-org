---
title: "Randa: Moving KDE Forward"
date:    2014-05-27
authors:
  - "unormal"
slug:    randa-moving-kde-forward
---
<p>
<img src="/sites/dot.kde.org/files/randa1.jpg" style="float: right; padding: 1ex; margin: 1ex; border: 0; background-image: none; " width="400" height="250" />
It's the time of the year again when the Randa Meetings plan starts to get <a href="http://www.kde.org/fundraisers/randameetings2014/">quite concrete</a>. After a break last year, the 5th edition of the Randa Meetings will happen in the middle of the Swiss Alps in August 2014. So once again more then 50 KDE contributors will meet in Randa to hack, discuss, decide and work for a full week.</p>

<p>This year the following groups and projects will <a href="http://www.randa-meetings.ch">come together in Randa</a>:</p>

<ul>
<li><a href="http://amarok.kde.org">Amarok/Multimedia/Phonon</a>: to work on the next edition of Amarok and Phonon and some exciting new KDE multimedia applications</li>
<li><a href="http://mail.kde.org/pipermail/kde-frameworks-devel/2014-April/014575.html">KDE Books</a>: to renew our great KDE development booklet and focus on KDE Frameworks 5</li>
<li><a href="http://edu.kde.org">KDE Edu</a> and <a href="http://www.gcompris.net">GCompris</a>: to work on KDE language learning and math applications, and finish the port of GCompris to Qt</li>
<li><a href="http://community.kde.org/Frameworks/Porting_Notes">KDE Frameworks 5 porting</a>: to bring application developers together with KF5 specialists to port their applications</li>
<li><a href="http://www.proli.net/2014/03/19/kde-sdk-next-how-will-we-develop-in-a-year/">KDE SDK</a>: to create a simple KDE SDK</li>
<li><a href="http://www.kdenlive.org">Kdenlive</a>: to reinforce one of the best video editing applications on Linux, and  start a port to KF5</li>
<li><a href="http://userbase.kde.org/Gluon">Gluon</a>: to put new energy in this KDE gaming framework</li>
</ul>

<p>Even if you are not participating yourself, you can help make Randa Meetings happen. More about this below. Here is what former <a href="http://sprints.kde.org/sprint/212">Randa Meetings participants</a> think about this event:</p>

<p>Kévin Ottens: <em>"It's important for sprints to be extremely focused for a few days. Randa is perfect for that, it is the ideal place to be away from our hectic lives and only work on what's needed for your sprint. At the same time the atmosphere and the nature around is relaxing a nice bonus which makes such sprints all the more efficient."</em></p>

<p>Aleix Pol: <em>"I've been to Randa 3 times already, with 2 different teams. It's always been a really productive and intense week of working non-stop. Pure KDE hacking for everyone, it's how projects move forward."</em></p>

<p>and</p>

<p>Albert Astals Cid: <em>"The Randa Meetings is the most amazing place to do a KDE Sprint. As a KDE contributor, attending the Randa Meetings gives you an amazing venue: a dedicated local team that lets you focus on the Sprinting, an amazing environment to air up your thoughts and the co-location of various sprints that creates great synergies between the various participants."</em></p>

<p>The Randa Meetings really bring KDE and its software forward. But as most of the participants are young people, students (and we try to bring new people to every <a href="http://sprints.kde.org">KDE sprint</a>), parents or just can't afford the travel costs, we need some help.</p>

<p>Here are some more concrete examples of what you can do with your money:</p>

<ul>
<li>With EUR 20 you can pay the accommodation for a Kdenlive developer for one night.</li>
<li>With EUR 40 you can pay the Swiss train tickets (ticket from the airport to Randa) for a Marble hacker.</li>
<li>With EUR 200 you can bring a KDE Edu hacker from somewhere in Europe to the meetings.</li>
<li>And with EUR 1000 you can bring a great documentation writer from the US to our small village in the Swiss Alps.</li>
</ul>

<p>With some additional costs for the house rental, we need a total amount of <a href="http://www.kde.org/fundraisers/randameetings2014/">EUR 20,000</a> to cover the costs of the Randa Meetings.</p>

<p>More about the plans the developers have for Randa will be shared in additional stories in the coming weeks. So be prepared to learn more about GCompris, Kdenlive, KDE Frameworks 5 and other projects at Randa this August! And if you think that what we're doing is important please <a href="http://www.kde.org/fundraisers/randameetings2014/">support us</a>. Every bit means a lot! Also, help us spread the word and tell others what we're doing and why financial help directly benefits KDE software. The Randa Meetings are a quantum jump for KDE.</p>