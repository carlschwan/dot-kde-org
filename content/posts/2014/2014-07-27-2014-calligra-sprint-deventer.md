---
title: " 2014 Calligra Sprint in Deventer "
date:    2014-07-27
authors:
  - "Boudewijn Rempt"
slug:    2014-calligra-sprint-deventer
comments:
  - subject: "Don't Disable Plan & Karbon "
    date: 2014-07-30
    body: "Disable Plan & Karbon??\r\nIt's sad... They are nice & useful apps. And their interface & workflow is much better than alternatives."
    author: "snguyen"
---
From the fourth to the sixth of July, the Calligra team got together in sunny Deventer (Netherlands) for the yearly developer sprint at the same location as the last Krita sprint. Apart from seeing the sights and having our group photo in front of one of the main attractions of this quaint old Dutch town in the province of Overijssel, namely <a href="http://www.kaashandel-debrink.nl/">the cheese shop</a> (and much cheese was taken home by the Calligra hackers, as well as stroopwafels from the Saturday market) we spent our time planning the future of Calligra and doing some healthy hacking and bug fixing!

<div align="center" style="float: center; padding: 1ex; margin: 1ex; margin-top: 0ex; "><a href="http://www.kaashandel-debrink.nl/"><img width="600" src="http://www.valdyas.org/~boud/calligra-2014-groupphoto.jpg" alt="Calligra 2014 sprint group picture "/></a><br /><div align="center">Jos van den Oever, Thorsten Zachmann, Arjen Hiemstra, Jigar Raisinghani, <br />Friedrich Kossebau, Dmitry Kazakov, Jaroslaw Staniek, Boudewijn Rempt (left to right)</div></div>

<h2>What's coming</h2>
For Calligra, we've been planning the next release: Calligra 2.9, which is planned for December. It will be the last release based on Qt4. We're a bit slow in porting to Qt5 and KDE Frameworks 5, because we still have the scars of the port to Qt4, which took years... This release will be pretty much Calligra as you know it, with all the bits intact.

So then, after 2.9, we're planning to do a proper port to Qt5 and KF5, using Frameworks where appropriate. In early January, we'll lock down the repo, send everyone on vacation while the porting scripts run. When every script has ran, and everything builds again, we'll start properly porting Calligra. We expect to be able to release Calligra 3.0 end of March, so that's a three month release cycle.

<h2>We need help!</h2>
Here's a bit of serious news, though: parts of Calligra are essentially unmaintained. Applications like Karbon or Plan, which have a long pedigree and have been around for ages, have not seen any development for over a year -- or more. These applications are still unique, have lots and lots of promise... But for Calligra 3.0, this means that we'll disable those applications from the build after the initial automated port. And it's up to volunteers to re-enable them, fix all porting issues and <i>take up maintainership</i>!

<h2>Frameworking</h2>
As for the Calligra libraries, we've long felt that some of them could do with a wider exposure. There are libraries for handling property bags and showing them in a gui, there are libraries for loading vector images, for handling OpenDocument. We've got a library for handling color management, too. Right now, the libraries are tangled together, and it would be good to split them up again.

We did that once before, but the split was undone during the KOffice 2 development process. Revisiting it right now isn't an option for lack of manpower. However, especially the vector image library might make a good addition to the KDE Frameworks, with a bit of work on API, documentation and so on. We want to get down to that during the port to Qt5.

<h2>Translation</h2>
Finally, Dmitry has been working with the Russian translation team to iron out some kinks in the translation process. In particular, undo strings are difficult in a language like Russian that uses a different (grammatical) case in different contexts. He has created a version of the undo library that forces developers to provide the proper context. We also discussed more long-term plans to make it easy to see the strings that need to be translated in the context of the gui -- as well as trying to create tools that make it easier to add new tooltips and other helpful strings.

All in all, the get-together was very fruitful. We now have a pretty good plan for 2014 and 2015 and know where we want to go. Planning and setting directions matters: it motivates the current developers and makes clear to potential contributors where things are going and where they can chime in.