---
title: "Introducing Konqi"
date:    2014-07-28
authors:
  - "jospoortvliet"
slug:    introducing-konqi
---
Do not publish. Probably never. Just used this to upload the images to the dot and prepare the html. Use from <a href="https://community.kde.org/Promo/graphics">https://community.kde.org/Promo/graphics</a>.

The dot has always been a place where content came first. But it is 2014 and articles without images don't do well. We don't always have images with articles, unfortunately. Sprints have group photo's, much coding work comes with screenshots but sometimes you're just not able to find something fitting to spice up the article.

This is now solved! We are proud to introduce the DOT stock images created by the incredible <a href="http://tysontan.comr">Tyson Tan</a>. In the brief time before text becomes obsolete altogether and we move to DBI (Direct Brain Interface) we will use his awesome work.https://community.kde.org/File:

<div align="center" style="width: 510px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:kde.png"><img src="/sites/dot.kde.org/files/kde_wee.png" /></a><br />The Konqi family!</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:mascot_20140702_konqui-group.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /></a><br />Our Konqi's as they are in the KDE about dialogs.</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:mascot_20140702_konqui-original.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-original_wee_0.png" /></a><br />Original Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:mascot_20131006_konqui_t_.png"><img src="/sites/dot.kde.org/files/mascot_20131006_konqui_t_wee_0.png" /></a><br />Creative Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:mascot_20140702_konqui-framework.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-framework_wee_0.png" /></a><br />Frameworks Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:mascot_20140702_konqui-commu-journalist.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-commu-journalist_wee_0.png" /></a><br />Press Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:mascot_20140702_konqui-plasma.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-plasma_wee_0.png" /></a><br />Plasma Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:mascot_20140702_konqui-app-science.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-app-science_wee_0.png" /></a><br />Science and Education Konqi</div>


<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:Mascot_20140728_konqui-app-dev.png"><img src="/sites/dot.kde.org/files/mascot_20140728_konqui-app-dev_wee_0.png" /></a><br />Developer Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:Mascot_20140728_konqui-app-games.png"><img src="/sites/dot.kde.org/files/mascot_20140728_konqui-app-games_wee_0.png" /></a><br />Game Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:Mascot_20140728_konqui-app-presentation.png"><img src="/sites/dot.kde.org/files/mascot_20140728_konqui-app-presentation_wee_0.png" /></a><br />Presenting Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:Mascot_20140731_konqui-app-system.png"><img src="/sites/dot.kde.org/files/mascot_20140731_konqui-app-system_wee.png" /></a><br />System configuring Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:Mascot_20140730_konqui-app-multimedia.png"><img src="/sites/dot.kde.org/files/mascot_20140730_konqui-app-multimedia_wee.png" /></a><br />Rocking Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:Mascot_20140731_konqui-dev-qt.png"><img src="/sites/dot.kde.org/files/mascot_20140731_konqui-dev-qt_wee_0.png" /></a><br />Qt love inside konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://community.kde.org/File:Mascot_20140731_konqui-app-utilities.png"><img src="/sites/dot.kde.org/files/mascot_20140731_konqui-app-utilities_wee.png" /></a><br />Utilities Konqi</div>

<div style="width: 360px; float: none; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href=""><img src="" /></a><br />Image caption</div>
