---
title: "Randa Meetings 2014 - Another Great Success"
date:    2014-12-08
authors:
  - "unormal"
slug:    randa-meetings-2014-another-great-success
---
It's been quite some time since the <a href="http://community.kde.org/Sprints/Randa/2014">Randa Meetings 2014</a> and even this year's edition of the KDE Community Summit called <a href="http://akademy.kde.org">Akademy</a> has already happened, but it's still nice to look back and see what was accomplished at this KDE Tech Summit in the <a href="http://www.randa.ch">middle of the Swiss Alps</a>.

And before we tell you about the seven groups that participated in the meetings this year (and because of the different participating groups and thus a collection of several meetings under the same roof, the event is called "Randa Meetings" with a plural s ;-), we send a big <a href="https://www.kde.org/fundraisers/randameetings2014">thank you to all the supporters and people</a> who made this gathering possible. It's because of you that we were able to work hard for a whole week and make the software you love even better. There was a range of participants of various ages, countries and KDE projects. The group picture shows that some KDE contributors brought their families and non-techie partners; they didn't need to decide between either family holidays or hacking for KDE.

<div style="float: center; padding: 1ex; margin: 1ex;"><img src="https://community.kde.org/images.community/4/4f/Randa_group_pic_preview.jpeg" /><br />2014 Group picture, by Martin Klapetek (CC-BY-SA)</div>

<h3>The KDE Edu group brought students and mentors together</h3>

From the KDE education group we had people from <a href="http://edu.kde.org/rocs">Rocs</a>, the graph tool, from <a href="http://edu.kde.org/kig">Kig</a>, KDE's geometry teaching tool, from <a href="http://edu.kde.org/artikulate">Artikulate</a>, the new language acoustic learning tool, from <a href="http://edu.kde.org/kstars">KStars</a>, the KDE astronomy tool and of course from <a href="http://marble.kde.org">Marble</a>, which doesn't need any explanation ;-). Two Google Summer of Code students for Marble saw their mentors for the first time in Randa and thus got a stronger bonding with our awesome community. At least one of them found another playground in KDE: GCompris.

<a hre="http://www.gcompris.net">GCompris</a> is a new and still young (at least for the Qt version) member of the KDE family. Bruno Coudoin and his colleagues met in Randa to make great progress on porting the more than 140 activities in GCompris to Qt and KDE technologies. Their main focus is currently to finish a first version for Android and thus Smartphones and Tablets. Bruno even did some live user-testing (of course with kids!) during lunch time. But besides the normal hacking on KDE educational software one other focus was the <a href="http://api.kde.org/frameworks-api/frameworks5-apidocs/">porting of the applications to KDE Frameworks 5 (KF5)</a>. Some of the them will be released as a KF5-based version in December as part of the <a href="https://techbase.kde.org/Schedules/Applications/14.12_Release_Schedule">KDE Applications 14.12 release</a>.

<h3>Different applications ported to KDE Frameworks 5</h3>

One other application that was ported to KF5 is <a href="http://www.kmymoney.org">KMyMoney</a>, one of our finance tools. The developers also worked on their Windows port. <a href="https://www.kde.org/applications/graphics/gwenview/">Other applications are Gwenview</a>, which has since gained a <a href="http://agateau.com/2014/habemus-maintainer/">new maintainer</a> and Jungle, a fresh video player.

Porting to and working on KDE Frameworks was a general focus at this year's Randa Meetings. We tried to push forward Windows and Mac (unfortunately we weren't able to bring many KDE Mac people to Randa this year) variants and worked on a more coherent developer story and thus a KDE SDK. The port of Kate and KDevelop to KF5 made great progress and their developers even achieved making it start on Windows. Besides the porting work, this group also got some GSoC students together with their mentors and thus there was some integration of their work on CLang support and QML/JS.

<div style="float: center; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/mascot_konqi-commu-randa_1.png" /><br />Picture of the new Randa Konqi: with a white star and an Edelweiss like on the flag of <a href="http://www.randa.ch">Randa</a>. Thanks a lot to the artist Tyson Tan!</div>

<h3>The KDE Multimedia group worked on many different things</h3>

Alongside and integrated with all this work and groups some other teams participated at the meetings. They are almost part of the inventory of the Randa Meetings ;-). Amarok and the KDE Multimedia team worked on bug triage (cleaning up more than 200 bugs!), polishing their handbook and Phonon. During this intense week we saw the <a href="http://www.dvratil.cz/2014/08/hacking-my-way-through-randa/">release of the GStreamer Phonon backend</a>. KMix, the KDE mixer application got another strong push forward.

And another multimedia application found its way to Randa: <a href="http://www.kdenlive.org">Kdenlive</a>, our great non-linear video editor. The project lacked some direction recently and so a face-to-face meeting of some old and some new contributors was a logical step to take. With Till Theato and Simon Eugster some oldsters of the project could help to document and explain the current code and state to the new maintainer Vincent Pinon. Interestingly enough we learned during the meetings that Vincent's wife Lucie is using Kdenlive in her professional work and as she was in Randa as well we took the opportunity to make a short interview with her (Thanks Françoise Wybrecht for doing the interview and Myriam Schweingruber for the translation. You can <a href="/sites/dot.kde.org/files/Interview-Lucie-Robin-French.txt">download the French version</a> of the interview.

<h3>Lucie Robin lives in Voiron near Grenoble - France and works as a professional video maker with Kdenlive.</h3>

<div style="float: right; padding: 1ex; margin: 1ex;"><img width="350px" src="/sites/dot.kde.org/files/Lucie-Robin.jpg" /><br />Lucie with her daughter and Konqi in the background</div>

<p>What brought you to open source?</p>
<p><i>Despite being an alien to the open source world, I discovered it through my Linux-using husband.</i></p>

<p>What pushed you to use free software?</p>
<p><i>As a video maker, I needed professional video software. Since most software in this field are over pricey, my husband suggested to try Kdenlive back in 2011.</i></p>

<p>After having used it for three years, what is your overall impression?</p>
<p><i>I got aware very quickly that Kdenlive is an answer to professional needs. But let's be honest, its use is full of pitfalls and inconsistencies, which could have discouraged me more than once. But luckily I discovered the philosophy behind free software which kept me going.</i></p>

<h3>New energy for Kdenlive and a new KDE Book</h3>

The Kdenlive team spend most of their time discussing and setting the <a href="https://kdenlive.org/node/9182">roadmap</a> for the future. At the end of the meetings we decided to bring Kdenlive even closer to KDE and thus start an <a href="https://community.kde.org/Incubator/Projects/Kdenlive">incubation process</a> for them. And thanks to Françoise, Lucie and Kdenlive you can <a href="https://www.youtube.com/watch?v=yua6M9jqoEk&feature=youtu.be">watch</a> yourself how it looked at the Randa Meetings 2014.

Another group came to Randa to work on a new edition of our <a href="https://www.flossmanuals.net/kde-guide/">KDE Guide</a>. After some problems with internet connectivity (ever heard of this problem at sprints or conferences? ;-) they decided to scratch the planned working process and to quickly develop a new one so they could work offline and integrate the <a href="http://books.kde.org">new book</a> directly with KDE source code and extract live snippets from it. They had a great start in Randa but it's not yet done. So if you're an expert in one of the KDE Frameworks, please help and submit a paragraph, short text or chapter.

In addition to these bigger groups some smaller teams and single developers were in Randa and worked on their projects. One of them is the <a href="https://userbase.kde.org/Gluon">Gluon</a> group. Developing a way of creating and playing games, they worked on a KF5 port, and also had a lot of discussion of their future directions and plans. And <a href="https://projects.kde.org/projects/playground/www/qmlweb">QMLWeb</a>, a JavaScript library, is currently a one man project with Anton Kreuzkamp working hard to improve the code and connect with new people.

<h3>An amazing event that should be repeated</h3>

Organization-wise we tried something new this year to connect the different groups participating at the Randa Meetings even more. During lunch and dinner one person of each group had to tell the others in a few sentences what they are currently working on. So everybody in Randa was more or less aware of what was discussed in the other groups and when they should connect and talk with them. But we learned something else too: doing this talks two times a day was not necessary and so we plan to do it only once a day in 2015.

For even more information about the Randa Meetings 2014 and some personal views, see <a href="https://community.kde.org/Sprints/Randa/2014#Blog_posts_and_other_news_about_the_meeting">a list of blog posts</a> and <a href="https://community.kde.org/Sprints/Randa/2014#Picture_collections_about_the_meeting">picture collections</a>. It was an amazing time and we got great feedback for the organizational work and thus are looking forward to 2015 and more very productive, successful and inspiring meetings and sprints. So please support us!

<p><a href="https://www.kde.org/fundraisers/yearend2014/"><img src="https://www.kde.org/images/teaser/bannerFundraiser2014.png" width="830" height="300" alt="fundraiser banner" /></a></p>

PS: There is another <a href="http://www.kdeblog.com/resumen-del-sprint-randa-2014.html">great summary in Portuguese</a> of what happened in Randa this year.

[<strong>Updated Dec 8 2014:</strong> Clarified the variety of the Randa Meetings]
