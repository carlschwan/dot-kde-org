---
title: "KDE Commit-Digest for 26th January 2014"
date:    2014-03-02
authors:
  - "mrybczyn"
slug:    kde-commit-digest-26th-january-2014
---
In <a href="http://commit-digest.org/issues/2014-01-26/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.kdevelop.org/">KDevelop</a> gets a new widget for the code assistant, which is more flexible in how it looks and behaves; adds a possibility to (de)serialize problems from/to disk; sees various optimizations</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> adds support for interval refresh of Google calendars and contacts</li>
<li>In <a href="http://skrooge.org/">Skrooge</a>, monthly report is now able to display reports on months, quarters, semesters and years</li>
<li><a href="https://community.kde.org/Baloo">Baloo</a> adds a plugin for <a href="http://pim.kde.org/akonadi/">Akonadi</a></li>
<li>libmm-qt implements OMA (Open Mobile Alliance) interface</li>
<li><a href="http://uml.sourceforge.net/">Umbrello</a> adds additional debug support</li>
<li>Ktexteditor adds a status bar.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-01-26/">Read the rest of the Digest here</a>.