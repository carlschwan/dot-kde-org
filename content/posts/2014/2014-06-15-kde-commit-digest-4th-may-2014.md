---
title: "KDE Commit-Digest for 4th May 2014"
date:    2014-06-15
authors:
  - "mrybczyn"
slug:    kde-commit-digest-4th-may-2014
---
In <a href="http://commit-digest.org/issues/2014-05-04/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://pim.kde.org/">KDE-PIM</a> sees huge performance improvement for POP3 users with large maildirs</li>
<li>KAddressbook adds a category filter</li>
<li><a href="http://krita.org/">Krita</a> implements support for more types of palettes</li>
<li>Also in Calligra, Docx export filter has partial support for comments</li>
<li><a href="http://www.digikam.org/">Digikam</a> sees work on better support of multicore CPUs with important performance improvements</li>
<li>Bluedevil has an initial port to KF5.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-05-04/">Read the rest of the Digest here</a>.