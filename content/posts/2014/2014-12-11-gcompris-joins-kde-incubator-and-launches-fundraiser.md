---
title: "GCompris Joins the KDE incubator and Launches a Fundraiser"
date:    2014-12-11
authors:
  - "unormal"
slug:    gcompris-joins-kde-incubator-and-launches-fundraiser
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://gcompris.net/incoming/gcompris-logo-square.png" /></div>

<a href="http://gcompris.net/index-en.html">GCompris</a> has joined the <a href="https://community.kde.org/Incubator">KDE incubator</a>. GCompris is the high quality educational software suite comprising numerous activities for children aged 2 to 10, and well known by parents and teachers all over the world.

GCompris was started in 2000 by Bruno Coudoin as a Free Software project. Originally written in GTK+, the project developers decided in early 2014 to make a radical change and rewrite it in Qt Quick. The main motivation is the ability of the Qt platform to address the desktop and the tablet market from a single code base.

In order to get the great level of support from a strong developer community, <a href="https://community.kde.org/Incubator">GCompris joined the KDE incubator</a>.

The rewrite is going smoothly; 86 educational activities of the 140 have been ported to Qt Quick. A release on Android is planned for December 2014 with the help of the KDE translation teams to make sure it is properly localized. Other platforms will follow.

<h2>Fundraiser for graphics redesign</h2>
Currently the graphics are one of the weakest parts of GCompris, as they were mostly done by the developers, using free graphics assets and sparse graphic artist contributions.

To address this problem, Timothée Giet, a talented graphics artist proposed himself to work on a complete graphics redesign.  He is a long standing Free Software contributor, active member of the <a href="https://krita.org/">Krita</a> team and recognized member of the KDE community. Making new graphics for more than 100 activities is a big task, so a fundraiser has been set up.

The project is not only about creating new background images but consists of a whole graphical rework. At first a graphics charter will be defined as none currently exists in GCompris. This will drive the project towards a unified style that it lacks today. Usability will be a primary emphasis, so that children find their way easily in each activity.

If you want to help, please consider <a href="http://igg.me/at/gcompris-unified-graphics">making a donation</a>.