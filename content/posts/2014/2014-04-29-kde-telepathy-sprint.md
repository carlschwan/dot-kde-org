---
title: "KDE Telepathy Sprint"
date:    2014-04-29
authors:
  - "David Edmundson"
slug:    kde-telepathy-sprint
comments:
  - subject: "Great Work!"
    date: 2014-04-29
    body: "Great work, thank you guys!"
    author: "QUASAR"
  - subject: "Still no OTR support"
    date: 2014-04-29
    body: "First let me say that the visual improvements look really great. I also know that is not something directly related to your work but related to the telepathy library as such. But it is really suprising that even one year after Snowden (KDE) Telepathy is still one of the few messengers which can't deal with secure chats aka OTR. I hope this will change soon because I really like the desktop integration. "
    author: "pinky"
  - subject: "Sounds Okay-ish"
    date: 2014-04-30
    body: " <strong>First of all: </strong> That group chat really shines! A much better UI to follow the conversation. Two thumbs up :)\r\n \r\n<strong>Second:  </strong>\"Our answer to this question was that [...] we realise that for them, we can only create an \"okay-ish\" experience at best[...]. Only with open protocols we can use all of our capabilities to create a truly awesome experience for users.\"\r\n\r\nHere you miss a major point imho. As 95% of the users will use popular clients, Telepathy (read: KDE (Software)) might disappoint many people as the focus is low. I think that even if the API changes many times, it is important to integrate Google Chat, Facebook Chat, Telegram (!!!) the best possible. It will not show all the great things Telepathy is capable of, but I think it is preferable over focus on 'geek' protocols.\r\n\r\nSummary: Rather have 10.000.000 okay-ish users than 5.000 delighted and 9.995.000 disappointed.\r\n\r\n <strong>Third:  </strong>SMS is being pushed out by Whatsapp, Telegram, Facebook Messenger etc etc. Any chance they will get supported too? (this ain't a: go do that asap, just if it is technically possible. I guess Android uses a standard 'notification API' )\r\n\r\nLast: I really like to see those blogposts about Telepathy. However, this is the first time I am tempted to start using it again. I remember being amazed while 4.0 was developed by the idea behind it.  <strong>Too see it mature now (\"written using KDE Telepathy's collaborative editing features\") is totally awesome. Thank you all for all your hard work </strong>\r\n"
    author: "BartOtten"
  - subject: "OTR"
    date: 2014-04-30
    body: "Also I would love to see OTR high on the TODO list. After the last year I think i needs no comments why :-)\r\nA deep Telepathy integration would probably bring this also to mobile clients where the situation is even worth...\r\n\r\nOtherwise...nice work!"
    author: "Toni W"
  - subject: "This is shaping up nicely"
    date: 2014-04-30
    body: "...and I'm very excited about SMS support. It seems silly to have a phone out on my desk beside my keyboard and yet have to pick it up and poke at it to reply to a message. Props to everyone at the sprint. :)"
    author: "Jeff"
  - subject: "OTR"
    date: 2014-04-30
    body: "I also wonder, why OTR is not on the roadmap of telepathy yet. Sure, it's open source and nobody can complain about the work of volunteers, but I don't understand why security and encryption is completely ignored by a framework, which claims to be a platform-independent standard. "
    author: "Max Power"
  - subject: "State of Jabber"
    date: 2014-04-30
    body: "You should look into what folks use and not into what folks should use. \r\n\r\nYou want to provide an awesome experience with open protocols like Jabber. I go to jabber.com. I want to register for Jabber and when I click register.jabber.com it takes me to a page it shows me a message from June 25 2013 that they have disabled account registration at register.jabber.com. I don't know what public servers are and they kinda sound like shady porn websites.\r\n\r\nYou should really rethink on what kind of service you want to provide. Folks won't adopt to KDE unless KDE fits in their shoe and trying to make them wear beach floaters."
    author: "Sudhir Khanger"
  - subject: "Amazing!"
    date: 2014-04-30
    body: "Guys, you're AMAZING! Thank you very much!!! The backend to KDE Connect is really handy!"
    author: "Ruggero"
  - subject: "\"foremost messaging client\"?"
    date: 2014-04-30
    body: "You must be kidding. As of today kopete still beats telepathy hands down.\r\nSome of the protocols that telepathy has plugins for can't even log in to the respective services, without any error messages to help one figure out why..."
    author: "Mathias"
  - subject: "Group chat question"
    date: 2014-04-30
    body: "Which protocol is the group chat you are showing running over?"
    author: "Leachim"
  - subject: "I have always said \"if"
    date: 2014-04-30
    body: "I have always said \"if someone wants to work on it, please do\". Instead all I hear are complaints by a very very vocal usergroup, but seemingly either a very small one or a very lazy one. \r\nI'm not going to tell my team what they should spend their free time on, if our product doesn't work for you, you are perfectly free to use something else.\r\n\r\nThat said, someone has stepped up to the plate with a quite clever design for GSOC, and we should see some results this summer. We'll announce news when it happens. "
    author: "d_ed"
  - subject: ">Here you miss a major point"
    date: 2014-05-01
    body: ">Here you miss a major point imho. As 95% of the users will use popular clients.\r\n\r\nIt was heavily discussed, and it's not a move we took lightly. \r\n\r\nIt's very frustrating for us; I made everyone switch to KDE Talk (a jabber server) at the sprint; and so much stuff that even I thought was broken \"just works\":  group chats, including, ad-hoc and inviting people, file transfers, we shared a document with a room and edited it, we had some video chats... mostly because their XMPP gateways suck.\r\nI honestly think we make a better chat experience with other KDE users than Google can. We can do lots of cool stuff.\r\n\r\nIn the same amount of effort we could probably just about make Facebook stickers work; and as soon as we do that it might just break again.\r\n\r\nTelegram we're hoping to work with, they have an open protocol - we like that. Anything else, if people step up and want to do other things that would be super cool!  Telepathy upstream just gained JS connection manager bindings, this could make adding support for some of these services easier.\r\n\r\nThis motion isn't about preventing us from working with proprietory services, but rather to not ourselves be limited by them. If we do something Facebook doesn't support, tough. We'll move forwards anyway; and promote and encourage people switching to open."
    author: "d_ed"
  - subject: "Which is what we want to fix."
    date: 2014-05-01
    body: "Which is what we want to fix. \r\nTry KDE Talk as a jabber server. There's a nice massive button in the \"create account\" list."
    author: "d_ed"
  - subject: "\"I honestly think we make a"
    date: 2014-05-01
    body: "\"I honestly think we make a better chat experience with other KDE users than Google can. We can do lots of cool stuff.\"\r\n\r\nI am sure. But untill KDE konquers the world, I doubt people will use it a lot.\r\n\r\n\"but rather to not ourselves be limited by them\"\r\n\r\nFor feature: sure, never limit yourself! But I think it would be worthwhile to first embrace the parties we can not konquer, and when done try to pull people to the open platforms. It's hard to do so when the app is not used."
    author: "Bart Otten"
  - subject: "I personally questioned the"
    date: 2014-05-03
    body: "I personally questioned the decision about OTR. In my point of view, security is more important than any UI improvement and especially the NSA surveillance has shown, that we need end to end encryption. That's the reason, why I still have to use kopete. Sad but true. I like to effort all you guys put into this project, but without OTR... I won't use it. "
    author: "Anonymous"
  - subject: "sms"
    date: 2014-06-29
    body: "when will we appreciate the wonderful sms feature ? what is the roadmap for kde telepathy ?\r\n\r\nthanks"
    author: "promeneur"
---
In April 2014, we had a sprint for KDE Telepathy, KDE's foremost instant messaging client. The sprint consisted of both past and new contributors from around the world.

<h2>Group chats</h2>
For the sprint we decided to concentrate our hacking efforts into a few key areas that are currently weak inside KDE Telepathy. We chose group chats as it was one of the most repeated feature requests coming especially from enterprise circles. It was something we supported at a basic level but, since none of us used it on a daily basis, it did not receive the attention it deserved.

Firstly, we forced ourselves to use a conference room for the duration of the sprint for all our chatting purposes and made a list of every potential improvement we could find. Afterwards we picked things off the list one by one and made significant improvements to the group chatting experience.

Our usability expert, Thomas Pfeiffer, was there with us providing valuable input from the usability point of view; thanks to that it was not just a mad hacking on features but also about making the application more usable. The screenshots below speak for themselves!

<img style="max-width:100%" src="http://dot.kde.org/sites/dot.kde.org/files/ktp_before_after.png" />
<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kdeconnect.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdeconnect_wee.jpg" /></a><br />Answer text messages from your desktop</div>

<h2>Easy chat via SMS using KDE Telepathy</h2>
During the sprint Albert Vaca and Alexandr Akulich worked on making a backend for Telepathy to talk to <a href="http://community.kde.org/KDEConnect">KDE Connect</a>. This allows you to receive SMS messages from your phone and reply through a comfortable and familiar chat interface on your desktop.

<h2>Reducing the bug count</h2>
Another area we focused on at the sprint was our bug count. At the beginning we had 62 reported bugs (excluding wishes and tasks), some of them had been open for a longer time. So we allocated some space on the whiteboard for a giant bug counter; whoever then fixed a bug from that list got the honor of erasing the number on the whiteboard and writing a new one.
We are now under 50 bugs, with under 50 further wishlist items.

<h2>Building for the future</h2>
We didn't just hack but also discussed where to go with KDE Telepathy.

<h3>Vision</h3>
According to the <a href="http://techbase.kde.org/Projects/Usability/HIG/Vision">KDE Human Interface Guidelines</a>, "A vision describes the goal of the project. It can be emotive and a source of inspiration, for instance by outlining how the final product makes the world a better place. It is roughly similar to purpose or aim, and guides through the development." In order to guide KDE Telepathy's development, Thomas, our aforementioned usability expert, led a session where we defined a vision for us.

<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/whiteboard.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/whiteboard_wee.jpg" /></a><br />Whiteboard work</div>

The first question that was important for us was "Which users do we want to focus on?". We decided that we want to focus on Plasma users. We do not shut out users of other desktop environments or operating systems, but we clearly focus on integrating well with Plasma and provide the best experience for Plasma users.

The other defining question for our focus was "Do we want to focus on providing the best possible experience for users of popular instant messaging systems such as Facebook Chat or Google Hangouts, or do we want to focus on providing an awesome experience to users of open protocols such as Jabber?" Our answer to this question was that while we do not want to exclude users of popular systems, we realise that for them, we can only create an "okay-ish" experience at best, because those systems' APIs are very restricted and always subject to change. Only with open protocols we can use all of our capabilities to create a truly awesome experience for users.

Our vision draft is still being discussed on the mailing list and will be published once it's been agreed upon by the whole KDE Telepathy community.

<h3>Frameworks</h3>
In KDE Telepathy we provide several plasma widgets, which need to be ported in order to run on Plasma Next. In order to do this we first had to port our libraries to work on top of KDE Frameworks. By the end of the sprint we had the contact list and chat plasma widgets fully running and working on Plasma Next. We  hope to release the widgets so that they are available for Plasma Next users.

<img style="max-width:100%" src="http://dot.kde.org/sites/dot.kde.org/files/ktp5.png" />

<h3>API Breaks in the larger Telepathy stack</h3>
There is an upcoming change in the interfaces to the Telepathy backends that talk to the various protocols such as jabber. We need to be prepared for this change - otherwise when distributions update Telepathy, our application will cease working. We are making sure we have code ported and ready, so we can release at the same time as upstream switches. Most of these updates are inside TelepathyQt and we are working on the elements lower in the stack that will benefit not only us but also Ubuntu and Jolla.

Planning how to handle these two upcoming changes at once is awkward at best  and required some delicate planning.
<h2>Wrapping up</h2>
Overall the sprint was incredibly useful in helping push our project forward both in terms of the extra development and planning moving forwards.

<strong>Thanks to the Blue Systems Barcelona office for hosting us.</strong>

More photos <a href="https://plus.google.com/photos/101026761070865237619/albums/6003955002855280545/6003956266031555314?pid=6003956266031555314&oid=101026761070865237619">here</a>

<img style="max-width:100%" src="http://dot.kde.org/sites/dot.kde.org/files/IMG_3635_v1.JPG" />

<em>This document was written by the KDE Telepathy team; written using KDE Telepathy's collaborative editing features.</em>