---
title: "Akademy 2014 Keynotes: Sascha Meinrath and Cornelius Schumacher"
date:    2014-07-30
authors:
  - "jospoortvliet"
slug:    akademy-2014-keynotes-sascha-meinrath-and-cornelius-schumacher
comments:
  - subject: "Forks"
    date: 2014-08-01
    body: "Perhaps he's suggesting we need more forks?\r\n;)"
    author: "bluelightning"
  - subject: "Forks"
    date: 2014-08-06
    body: ". . . or perhaps forks over knives?  (Not highly likely, knowing his gustatory preferences.)\r\n"
    author: "deborah forest hart"
---
<a href="https://akademy.kde.org/2014">Akademy 2014</a> will kick off on September 6 in Brno, Czech Republic; our keynote speakers will be opening the first two days. Continuing a tradition, the first keynote speaker is from outside the KDE community, while the second is somebody you all know. On Saturday, Sascha Meinrath will speak about the dangerous waters he sees our society sailing into, and what is being done to help us steer clear of the cliffs. Outgoing KDE e.V. Board President, Cornelius Schumacher, will open Sunday's sessions with a talk about what it is to be KDE and why it matters.

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/SaschaDot300.jpg" /><br />Sascha Meinrath - photo by Faith Swords</div>
<h2>Sascha Meinrath on the Internet of Things</h2>
Sascha Meinrath is well-known in the broad FOSS community. <a href="http://en.wikipedia.org/wiki/Sascha_Meinrath">Wikipedia describes him</a> as an "Internet culture leader and community Internet pioneer". He was a leading voice in the successful opposition to the U.S. <a href="http://en.wikipedia.org/wiki/Stop_Online_Piracy_Act">SOPA</a> and <a href="http://en.wikipedia.org/wiki/PROTECT_IP_Act">PIPA</a> legislation, and is the founder of the <a href="http://oti.newamerica.org/">Open Technology Institute</a> (OTI), a public policy think tank advocating policy and regulations that are healthy for open source, open standards and innovation. OTI also works on lowering the barrier to wireless communication (<a href="http://en.wikipedia.org/wiki/Commotion_Wireless">Commotion Wireless</a>) and advancing network research in the Measurement Lab. Recently, Sascha started the X-Lab, which anticipates technology directions and develops public policy for them, rather than reacting afterward with the risk of being caught off guard.

Sascha looks ahead at potential challenges, aware of the ways governments and companies abuse technology or could do so. With the Akademy program committee, he discussed "digital feudalism—the interlocking system of devices and applications that are reducing us to a serf-like state". Having coined this term, he is in a good position to explain the ways in which private and government forces are undermining the democratic, participatory platform of the Internet. And how this subterfuge has further broad impacts that reduce our freedom.

Resolving this dilemma cannot be solely a technical endeavor. Sascha said, "I see the work with the Commotion Wireless Project or fighting against NSA surveillance or on major spectrum licensure reform as different facets of the same problem, but am most worried about what happens with the so-called 'Internet of Things'—which I view with extreme skepticism". The Internet of Things (IoT) has the potential to transform communication networks massively. <a href="http://www.thefiscaltimes.com/Articles/2014/07/27/Why-CIA-Fears-Internet-Things">Enormous security implications</a> aren't even the biggest concern. Sascha notes that "there are tremendous opportunities for building open ecosystems and privacy-protecting equivalents to mainstream products—but that has to be combined with strong pushes in governmental/policy circles as well as in outreach/PR".

In other words, it is Sascha's intention to ensure that this transition to the Internet of Things—whatever shape it will have—is built on open standards, protocols and strong protection of individual freedom. "And if that disrupts the dominant business model of many major corporations today (who all want to commoditize your private data), so be it".

Sascha Meinrath is one of Time Magazine's "Most Influential Minds in Tech" and Newsweek's "Digital Power Index Top 100 Influencers". KDE is in a strong position to provide technical innovation and has consistently demonstrated the power of community, freedom and openness. At <a href="https://akademy.kde.org/2014">Akademy 2014</a> in Brno, there is a strong possibility that this partnership will produce outcomes that will benefit people the world over. Anyone who is committed to having technology make a difference owes it to themselves to be part of Akademy.

<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/CorneliusDotStory.jpg" /><br /><small>Cornelius in the (g)olden days - photo by Helge  Heß</small></div>
<h2>Cornelius Schumacher on How KDE Makes You a Better Person</h2>
A strikingly related subject will be brought to the Akademy audience on Sunday. Cornelius Schumacher, president of KDE e.V., has been a KDE contributor since 1999. He has seen changes in every direction and has been at the heart of several of them. Cornelius will talk about the tremendous opportunity KDE provides to learn and grow, not only technology, but also people. He will show how the community consistently acts as a breeding ground for software and for personal growth as well. "I joined KDE for the technology, but stayed for the community", Cornelius says. "I have never stopped being amazed by the people around me in KDE, the talent, the friendship, the passion to do something for the greater good. I learned so much from these people and owe a big part of my career and personal development to the community."

Over the years Cornelius has seen many people join KDE and grow, and often outgrow the community. Roots for industry-changing technology and for amazing careers can be found in KDE. But what makes this environment so special? What holds it together over the many years where hundreds, even thousands of people contribute and form the KDE community? Cornelius gives a hint: "If the community is the soil, freedom is the fertilizer. The ideals of free software create the foundation that makes KDE possible, and these ideals extend to more than just software. Within KDE, it's a commonly felt responsibility to give everybody access to great technology, retaining individual freedoms and control about not only your computing, but your life."

Cornelius's topic is not just abstract or conceptual; it is something which relates to all of us on a personal level. This is a challenge and a chance. In the end Cornelius will reveal the secret of how KDE makes you a better person.

<h2>Akademy 2014 Brno</h2>
For most of the year, KDE—one of the largest FOSS communities in the  world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2014">Akademy</a> provides all KDE contributors the opportunity to meet in person to  foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and other types of KDE  contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work to bring those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

If you are someone who believes that it's possible for technology to make a difference in the world, <a href="https://akademy.kde.org/2014">Akademy 2014</a> in Brno, Czech Republic is the place to be.