---
title: "Calligra 2.8 Released"
date:    2014-03-06
authors:
  - "ingwa"
slug:    calligra-28-released
comments:
  - subject: "yeah!"
    date: 2014-03-07
    body: "I just switched from LibreOffice to Calligra and i'm 100% Satisfied! Much better than LibreOffice imho\r\n\r\nEspecially Krita and Flow are my absolute Favourite!"
    author: " \u767d\u5ddd\u9593\u702c\u6d41"
---
<div style="width: 200px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/calligra-logo-200.png" /></div>
The Calligra team is proud and pleased to announce the release of version 2.8 of the <a href="http://www.calligra-suite.org/">Calligra Suite</a>, Calligra Active and the Calligra Office Engine. This version is the result of thousands of commits which provide new features, polishing of the user experience and bug fixes.

<h2>New in This Release</h2>
Major new features in this release are comments support in Author and Words, improved Pivot tables in Sheets, improved stability and the ability to open hyperlinks in Kexi. Flow introduces SVG based stencils and as usual there are many new features in Krita including touch screens support and a wraparound painting mode for the creation of textures and tiles.

You can find more details <a href="http://www.calligra.org/news/calligra-2-8-released/">in the official announcement on the calligra site</a>.