---
title: "KDE Commit-Digest for 22nd December 2013"
date:    2014-01-11
authors:
  - "mrybczyn"
slug:    kde-commit-digest-22nd-december-2013
---
In <a href="http://commit-digest.org/issues/2013-12-22/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://edu.kde.org/kstars/">KStars</a> adds support to online plate solving using <a href="http://astrometry.net/">astrometry.net</a> web services API, optimizes memory usage</li>
<li><a href="http://uml.sourceforge.net/">Umbrello</a> adds duplication of diagrams</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> adds an option in oxygenrc to disable window background</li>
<li><a href="http://krita.org/">Krita</a> adds thumbnail support for kra and ora files</li>
<li>Numerous bug fixes in <a href="http://www.kde.org/applications/multimedia/kmix/">KMix</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-12-22/">Read the rest of the Digest here</a>.