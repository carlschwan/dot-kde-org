---
title: "KDE Commit-Digest for 16th February 2014"
date:    2014-03-22
authors:
  - "mrybczyn"
slug:    kde-commit-digest-16th-february-2014
---
In <a href="http://commit-digest.org/issues/2014-02-16/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> merges advanced track statistics importers (a <a href="http://konradzemek.com/category/gsoc/">GSoC project</a>)</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> allows language plugins to provide styles to formatters</li>
<li><a href="http://konsole.kde.org/">Konsole</a> stores terminal size in the profile, each profile can now set desired column and row size; allows users to specify css file for tab bar style (this can be used to set minimum width of the tabs, distinguish active tab, etc)</li>
<li>Kwallet replaces SHA with PBKDF2-SHA512+Salt</li>
<li>Porting to Qt5 and Frameworks 5 continues, we have initial ports of kfind and konq.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-02-16/">Read the rest of the Digest here</a>.