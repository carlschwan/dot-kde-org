---
title: "2014 Krita Sprint in Deventer, Netherlands"
date:    2014-06-04
authors:
  - "Boudewijn Rempt"
slug:    2014-krita-sprint-deventer-netherlands
---
Though the Krita team was one of the first to start the tradition of having sprints, with the <a href="http://www.valdyas.org/fading/index.cgi/hacking/krita/done.html">first Krita Sprint in Deventer, in 2005</a>, Krita sprints are rather infrequent! But, of course, we also meet each other during the more regular Calligra sprints.
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KritaLogo3.png" /></div>

Anyhow... Krita developers and artists met again in Deventer in May 2014. It was the most awful weather you can imagine for a sprint—warm, sunny, bright, lovely to be outside! Long and lazy lunches, discussions out on the roof terrace until after midnight, walks through the park. Is it a wonder nothing much got done?
<!--break-->

Wait, that's wrong! Bravely resisting the lure of the fine spring weather, three artists and six developers got down to some serious work! In the week before the Krita sprint, Boud, Dan, Arjen and Stuart already had a week-long sprint working towards the <a href="http://store.steampowered.com/app/280680/">final release of Krita Gemini on Steam</a>, and on Thursday the others started to arrive. And only by Tuesday the house was empty again...

<div align=center style="padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KritaSprintTeam2014.png" /><br />Krita Sprint Team</div>

<h3>Let's see what got done:</h3>

<strong>Fundraiser</strong>
We've been working on the Krita 2.9 Fund Raiser since April. It took more work than we'd ever imagined, but now we've got a great promo video thanks to Björn Sonnenschein, and together we set up and created the Kickstarter campaign. We've got ambitious goals here: not only do we want Dmitry to go on working full-time on Krita, we want to extend that and have Sven (who's been a Krita hacker for over ten years) work full-time on Krita, too. And if we exceed that goal, well, there's OSX to conquer! Stay tuned, we expect to go live really soon now!

<strong>Releases</strong>
Krita 2.9 will probably be the last release based on Qt4 and KDE4. It's a huge and ambitious release, with the fund raiser being based around 24 ambitious goals and then -- we also want to finish the resource bundle manager that Victor Lafon from Toulouse started and make it possible to have more than one image open in a window. After the 2.9 release, it's time to port to Qt5 and KDE Frameworks 5. We spent some time discussing which tiers from the Frameworks we want to use, and how to best approach the port.

<strong>Translations</strong>
With the interest in Krita growing by leaps and bounds—the Windows installer has seen more than half a million downloads—interest in translations is growing, too. It takes some effort to get the volunteers who want to work on translating Krita to push their efforts through the right channels, but that is working out well. And Dmitry Kazakov is in close contact with the Russian translation team. We discovered that Krita's terminology is a bit of an organically grown mess, and that there are many places where context is missing. So we decided to get Dmitry together with Alexander Potashev and Paul Geraskin in Russia, and they would go through Krita, figure out what needs fixing, and fix it. That effort is underway right now, and might already be merged by the time you're reading this.

<strong>Krita Foundation</strong>
<a href="http://krita.org/support-krita/foundation">The Krita Foundation</a> was created to support the development of Krita by, well, actually paying for development. Time is money, and money gets us time, and accelerated development. Thanks to a big sponsor, Dmitry has been able to work full-time for the entire development period of Krita 2.8. The Foundation is pretty healthy, with money coming in from donations and sales of the DVD, as well as the <a href="http://krita.org/support-krita#general">Krita Development Fund</a>, but we still want to grow and do more and more!

<strong>SIGGRAPH</strong>
During the sprint, we got mail from <a href="http://www.blender.org/">Blender's</a> Ton Roosendaal, proposing to cooperate on having a stand at <a href="http://s2014.siggraph.org/">SIGGRAPH 2014</a>, the foremost conference and trade show for graphics. We've found at least one sponsor, and have already reserved some floor space. Now we need to furnish it and find money to travel to Vancouver. It's an awesome opportunity to be the first KDE project to show off at SIGGRAPH!

<strong>User Sessions</strong>
Last sprint, in Amsterdam at the Blender Institute in 2011, we sat down our attending artists and made them work with Krita for half an hour, videotaping and recording their work. They were allowed to gripe and whine to their hearts' content, and the developers were allowed to listen and make notes and fix Krita! Back then, Krita 2.4 hadn't been released yet, and Krita was, in the words of one of the artists, a very nice amateur application for amateur artists, but not suitable for production work. Well, we've come a long way! Sure, one artist made Krita crash (and we fixed the crash within fifteen minutes...), but there was almost no whining, no griping, people could just draw and paint. Still, <a href="https://docs.google.com/document/d/1f2weyaHAw7xLfUo0geKj9DRys3dLY3QCIRO-NVO_t-Y/edit?usp=sharing
">Dmitry's notes were extensive</a>, and we've got work to do!

<strong>Book Sprint</strong>
One thing that's missing is a book on Krita. Wolthera is busy with the <a href="http://userbase.kde.org/Krita/Manual">manual on userbase</a> and Timothée Giet has proposed to organize a book sprint for Krita. He is currently investigating whether we can get a subsidy for the book sprint!

<strong>Sunshine</strong>
Yes, in the end, we did enjoy the sunshine, too... It turns out that our lunches were just as productive as the meeting and the hacking sessions, and it's true about the late nights on the roof, where we were busy sketching, painting, hacking, discussing and planning.

<strong>Thanks!</strong>
Thanks are due to KDE e.V. who sponsored travel for our hackers and artists! The Krita Foundation paid for the t-shirts. We managed to mostly fit into my place in Deventer, which helped keep the accommodation costs down. It was an awesome sprint, and we're going to make Krita better, better and better!