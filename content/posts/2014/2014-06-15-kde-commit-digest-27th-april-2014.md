---
title: "KDE Commit-Digest for 27th April 2014"
date:    2014-06-15
authors:
  - "mrybczyn"
slug:    kde-commit-digest-27th-april-2014
---
In <a href="http://commit-digest.org/issues/2014-04-27/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://umbrello.kde.org/">Umbrello</a> adds find text in tree view, current diagram and all diagrams feature</li>
<li>KDE Telepathy can share images over common image sharing networks</li>
<li>Sflphone-kde adds security evaluation framework with GUI</li>
<li>Punctuation data is accessible to Jovie</li>
<li>Initial import of Application Menu aka (Homerun) Kicker</li>
<li>In IMAP-Resource, refactoring of retrieveitemstask introduces multiple improvements</li>
<li><a href="http://www.kexi-project.org/">Kexi</a> is on the way to Qt5: Forms ported to Qt4's scroll area.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-04-27/">Read the rest of the Digest here</a>.