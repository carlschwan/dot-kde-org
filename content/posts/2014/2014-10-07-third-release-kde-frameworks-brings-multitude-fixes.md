---
title: "Third Release of KDE Frameworks Brings a Multitude of Fixes"
date:    2014-10-07
authors:
  - "jriddell"
slug:    third-release-kde-frameworks-brings-multitude-fixes
---
Releases of KDE Frameworks are now a monthly feature.  The release of <a href="https://www.kde.org/announcements/kde-frameworks-5.3.0.php">KDE Frameworks 5.3</a> brings many small, but important fixes including:
<ul>
<li>KWindowSystem has added features needed for future versions of KWin,</li>
<li>KTextEditor used in Kate fixes memory leaks,</li>
<li>Frameworkintegration was fixed for Qt 5.4, and</li>
<li>KActivities can load plugins at runtime.</li>
</ul>
A detailed listing of all Frameworks and other third party Qt libraries is at <a href="http://inqlude.org/">inqlude.org</a>, the curated archive of Qt libraries. A complete list with API documentation is on <a href="http://api.kde.org/frameworks-api/frameworks5-apidocs/">api.kde.org</a>.
<!--break-->
