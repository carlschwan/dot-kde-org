---
title: "KDE Frameworks Sprint - How to Release a Platform"
date:    2014-08-13
authors:
  - "jriddell"
slug:    kde-frameworks-sprint-how-release-platform
---
<div style="width: 510px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://www.flickr.com/photos/jriddell/14882348636" title="Konqui at Montjuic"><img src="https://farm6.staticflickr.com/5562/14882348636_f6667e5076.jpg" width="500" height="281" alt="DSC_0538"></a><br />Konqui finds the Spectacular Montjuic next door to the KDE office.</div>

KDE Frameworks 5 is the result of two years of hard work porting, tidying, modularizing and refactoring KDELibs4 into a new addition to the Qt 5 platform.  In January, Alex Fiestas announced <a href="http://www.afiestas.org/we-are-open-for-business-a-kde-hub-in-barcelona/">The KDE Barcelona Hub</a>—an office where anyone is welcome to come and work on KDE projects.  It was just what the Frameworks team needed to finish off the code so it could be released to the world.  Read on for some of what happened.
<!--break-->

<a href="http://agateau.com/2014/back-from-the-kde-frameworks-sprint/">Aurelien Gateau</a> reports:
<blockquote>
I spent most of my time working on translation support, ironing out details to get them to install properly and working with David [Faure] on the release tarballs scripts. I also worked a bit on KApidox, the code generating API documentation for KF5 on api.kde.org. I updated the script to match with the latest Framework changes and switched to the Jinja2 template engine. Using Jinja will make it possible to generate an up-to-date list of Frameworks on the landing page, based on the information from the Framework metainfo.yaml files.
</blockquote>
<a href="http://api.kde.org/">api.kde.org</a> now contains a complete list of Frameworks thanks to Aurelien with Frameworks 5 now the default option.

<a href="http://randomguy3.wordpress.com/2014/05/06/kf5-sprint-in-barcelona/">Alex Merry</a> spent his time on the small but tricky tasks all software needs to be of a high enough quality for release.
<blockquote>
Friday was spent trying to understand the KItemModels unit tests and figure out why one of the tests was failing. I eventually determined that the pattern of signal emission when moving rows around had probably changed between Qt4 and Qt5, and the fix was fairly simple.
</blockquote>
He also reports on other important topics such as install paths, meta data files and writing the <a href="https://community.kde.org/Sysadmin/GitKdeOrgManual#Advanced_Git">Advanced Git tutorial</a>.

One important discussion that took place in Barcelona was on the <a href="http://mail.kde.org/pipermail/kde-frameworks-devel/2014-April/015204.html">KDE Frameworks Release Cycle</a>.  We made the controversial decision to do away with bugfix releases and instead have monthly feature releases.  Although some distribution packagers noted concerns about the lack of stable release updates, this is the pattern Frameworks is now following, which allows for much faster turnaround of new features.

<div style="width: 650px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="http://ervin.ipsquad.net/share/pics/kf5-201404-sprint-board.jpg" width="640" height="360" /><br />The Post-it notes kept things orderly</div> 

Few people have done more to help the Frameworks project than <a href="http://ervin.ipsquad.net/2014/04/27/live-from-barcelona-kde-frameworks-sprint/">Kévin Ottens</a> who kept the weekend ticking over by sorting the post-it notes with tasks and highlighting notes which were taking longer than expected to progress.  He highlights the processes that the team are following to allow for monthly releases:

<ui>
<li>    more dog fooding from framework developers;</li>
<li>    more contributions from application developers;</li>
<li>    more automated tests and peer reviews;</li>
<li>    finer grained feature delivery.</li>
</ui>

Aleix Pol spent his time "<em>Mostly moving things around in the CMake, especially install variables that got changed to make them more compatible with Qt 5</em>".

Alex Fiestas reports "<em>Kai and I worked on Solid, we added QML support and designed the new asynchronous power management api</em>".

One of the people who has been a constant throughout the development of KDE's platform is David Faure.  He spent time working on the scripts that make releases possible without much overhead.

It was a successful week that wrapped up many of the loose ends that were needed to allow for last month's successful release of Frameworks 5.  With Plasma 5 now out and Applications releases on their way, Frameworks can be assured to be a platform for future work for years to come.
