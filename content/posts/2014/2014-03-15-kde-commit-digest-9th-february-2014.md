---
title: "KDE Commit-Digest for 9th February 2014"
date:    2014-03-15
authors:
  - "mrybczyn"
slug:    kde-commit-digest-9th-february-2014
---
In <a href="http://commit-digest.org/issues/2014-02-09/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://pim.kde.org/akonadi/">Akonadi</a> server now supports searching via 3rd party search plugins which means it can retrieve results very quickly; it also supports server-search (searching items not indexed by a local indexing service)</li>
<li>Systemtray allows DBus-activation for Plasmoids</li>
<li><a href="http://dolphin.kde.org/">Dolphin</a> and KMail's messagelist filter and search windows have been ported to Baloo</li>
<li><a href="http://okular.org/">Okular</a> adds tabbed interface</li>
<li>In <a href="http://www.kdevelop.org/">KDevelop</a>, it is now possible to jump to runtime output error messages</li>
<li><a href="http://kate-editor.org/">Kate</a> adds keyword-based completion model</li>
<li>Google Drive API support has been added.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-02-09/">Read the rest of the Digest here</a>.