---
title: "KDE Commit-Digest for 30th March 2014"
date:    2014-05-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-30th-march-2014
comments:
  - subject: "baloo improvements"
    date: 2014-06-02
    body: "The improvements to Baloo sound promising. I understand why DrEeevil was banned from the channel (nagging just costs time), but I think his constructive comments are nicely to the point: http://gentooexperimental.org/~patrick/weblog/archives/2014-05.html#e2014-05-03T03_40_52.txt"
    author: "ArneBab"
---
In <a href="http://commit-digest.org/issues/2014-03-30/">this week's KDE Commit-Digest</a>:

<ul><li>Baloo reduces I/O usage during initial indexing; introduces a new database wrapper which adds features such as combining a writable + read only db and many convenience functions</li>
<li>Plasma Framework makes the desktop scripting activity-aware</li>
<li>KDE-PIM makes some of the IMAP dialogs asynchronous which improves error handling</li>
<li>Umbrello adds synchronous message support to collaboration diagrams</li>
<li>Skrooge filters date better in operation page</li>
</ul>

<a href="http://commit-digest.org/issues/2014-03-30/">Read the rest of the Digest here</a>.
<!--break-->