---
title: "Akademy Tuesday Wrapup"
date:    2014-09-10
authors:
  - "jriddell"
slug:    akademy-tuesday-wrapup
---
Akademy is in full swing here in Brno in the Czech Republic.  The days are now filled with BoF sessions to discuss given topics and make decisions in person much faster than would be possible online.  Here is the <a href="https://www.youtube.com/watch?v=qONKM3dHeDQ">wrapup session from Tuesday</a> which covered the outcomes from sessions on Solid, Plasma Media Centre, Inqlude, UI design, Frameworks and more.
<iframe width="420" height="315" src="//www.youtube.com/embed/qONKM3dHeDQ?rel=0" frameborder="0" allowfullscreen></iframe>
