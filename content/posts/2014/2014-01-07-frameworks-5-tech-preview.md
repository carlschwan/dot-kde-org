---
title: "Frameworks 5 Tech Preview"
date:    2014-01-07
authors:
  - "jospoortvliet"
slug:    frameworks-5-tech-preview
comments:
  - subject: "more ThreadWeaver"
    date: 2014-01-07
    body: "More <a href=\"http://creative-destruction.me/2014/01/07/kde-frameworks-5-tech-preview-released-with-updated-threadweaver/\">details about ThreadWeaver</a>. And a pretty picture too."
    author: "kallecarl"
  - subject: "ThreadWeaver"
    date: 2014-01-08
    body: "Does it mean that kde activities could use isolated applications\u2019 threads, so I would be able to completely stop/pause heavy activitie for a couple of months and use the same applications with other sessions in other activities?"
    author: "Sergey"
  - subject: "I don't think it works that"
    date: 2014-01-08
    body: "I don't think it works that way... It's a way for an application to use multiple threads more easily, but that's neither isolated nor activity related."
    author: "jospoortvliet"
  - subject: "Looking forward"
    date: 2014-01-09
    body: "Looking forward of Tech Preview of KDE Frameworks 5 in Qt environment. hope it lives up-to the buzz."
    author: "Ajinkya"
  - subject: "Some more writing about Frameworks"
    date: 2014-01-09
    body: "Some more writing about Frameworks:\r\n<ul><li><a href=\"http://www.linux.com/news/software/applications/755768-kde-frameworks-5-a-big-deal-for-free-software\">Linux.com: KDE Frameworks 5 a Big Deal for Free Software</a></li>\r\n<li><a href=\"http://blog.jospoortvliet.com/2014/01/kde-frameworks-5-big-deal.html\">big deal (my blog)</a></li>\r\n<li><a href=\"http://blog.jospoortvliet.com/2014/01/building-converging-uis.html\">About Plasma Workspaces and Converging UI's (my blog)</a></li></ul>"
    author: "jospoortvliet"
  - subject: "Qt Addons = libraries"
    date: 2014-01-09
    body: "There has been some confusion about Qt Addons. It is a term we use to describe an independent library with well-defined abilities and dependencies offering functionality to Qt ; they are thus not plugins of any kind. The main difference between KDELibs and the Qt Addons in Frameworks 5 is the fact that one can use them independently from each other (within the boundaries of the tiers)."
    author: "jospoortvliet"
---
<div style="float: right; padding: 1ex; margin: 1ex; width: 300px; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" /></div>
The KDE Community is proud to announce a Tech Preview of KDE Frameworks 5. Frameworks 5 is the result of almost three years of work to modularize, review and port the set of libraries previously known as KDElibs or KDE Platform 4 into a set of Qt Addons, separate libraries with well-defined dependencies and abilities, ready for Qt 5. This gives the Qt ecosystem a powerful set of drop-in libraries providing additional functionality for a wide variety of tasks and platforms, based on over 15 years of KDE experience in building applications. Today, all the Frameworks are available in Tech Preview mode; a final release is planned for the first half of 2014. Some Tech Preview addons (notably KArchive and Threadweaver) are more mature than others at this time.

<h2>What is Frameworks 5?</h2>
The KDE libraries are currently the common code base for (almost) all KDE applications. They provide high-level functionality such as toolbars and menus, spell checking and file access. Currently, 'kdelibs' is distributed as a single set of interconnected libraries. Through KDE Frameworks efforts, these libraries have been methodically reworked into a set of independent, cross platform classes that will be readily available to all Qt developers.

The KDE Frameworks—designed as drop-in Qt Addon libraries—will enrich Qt as a development environment with functions that simplify, accelerate and reduce the cost of Qt development. Frameworks eliminate the need to reinvent key functions.

The transition from Platform to Frameworks has been underway for almost three years and is being implemented by a team of about 20 (paid and volunteer) developers and actively supported by four companies. Frameworks 5 consists of 57 libraries: 19 independent Qt addons not requiring any dependencies; 9 that require libraries which themselves are independent; and 29 with more significant dependency chains. Frameworks are developed following the <a href="http://community.kde.org/Frameworks/Policies">Frameworks Policies</a>, in a vendor neutral, open process.

<a href="http://dot.kde.org/2013/09/25/frameworks-5">This KDE News article</a> has more background on Frameworks 5.

<h2>Available today</h2>
The tech preview made available today contains all 57 libraries that are part of Frameworks 5. Of these, two have a maturity level that shows the direction of Frameworks: ThreadWeaver and KArchive. Developers are invited to take all of the libraries for a spin and provide feedback (and patches) to help bring them to the same level of maturity.

KArchive offers support for many popular compression codecs in a self-contained, featureful and easy-to-use file archiving and extracting library. Just feed it files; there's no need to reinvent an archiving function in your Qt-based application! ThreadWeaver offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread execution by specifying dependencies between the threads and executing them while satisfying these dependencies, greatly simplifying the use of multiple threads. These are available for production use now.

<div style="float: center; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kf5_big_0.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kf5_small2.png" /></a><br />The KDE Frameworks and their dependencies (overview is a work in progress)</div> 

There is a <a href="http://community.kde.org/Frameworks/List">full list of the Frameworks</a>; tarballs with the current code can be <a href="http://download.kde.org/unstable/frameworks/4.95.0/">downloaded</a>. <a href="http://community.kde.org/Frameworks/Binary_Packages">Binaries</a> are available as well.

The team is currently working on providing a detailed listing of all Frameworks and third party libraries at <a href="http://inqlude.org">inqlude.org</a>, the curated archive of Qt libraries. Each entry includes a dependency tree view. Dependency diagrams can also be found <a href="http://agateau.com/tmp/kf5/">here</a>.

<h2>Working towards a final release</h2>
The team will do monthly releases with a beta planned for the first week of April and a final release in the beginning of June.

Plans for this period include tidying up the infrastructure, integration with QMake and pkg-config for non-CMake users, getting CMake contributions upstream, and a final round of API cleanups and reviews. Frameworks 5 will be open for API changes until the beta in April.

Those interested in following progress can check out the <a href="https://projects.kde.org/projects/frameworks">git repositories</a>, follow the discussions on the <a href="https://mail.kde.org/mailman/listinfo/kde-frameworks-devel">KDE Frameworks Development mailing list</a> and contribute patches through <a href="https://git.reviewboard.kde.org/groups/kdeframeworks/">review board</a>. Policies and the current state of the project and plans are available at the <a href="http://community.kde.org/Frameworks">Frameworks wiki</a>. Real-time discussions take place on the <a href="irc://#kde-devel@freenode.net">#kde-devel IRC channel on freenode.net</a>.