---
title: "Randa Meetings Interview Four: Myriam Schweingruber"
date:    2014-08-02
authors:
  - "devaja"
slug:    randa-meetings-interview-four-myriam-schweingruber
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img width="200" height="225"  src="/sites/dot.kde.org/files/picture_3.png" /><br />Myriam Schweingruber</div>

<p>In one week the <a href="https://sprints.kde.org/sprint/212">Randa Meetings 2014</a> will start and this is possible because of you. You <a href="http://www.kde.org/fundraisers/randameetings2014/">supported us</a> (and can still <a href="http://kde.org/community/donations/">support us</a> ;-) and thanks to you we will be able to improve your beloved KDE software even more. So it's time to give you something new. Here is another interview with one of the persons who will be participating in this year's meetings (and participated since the start in 2009). And watch out for some other interviews to come in the next days and weeks.</p>

<p>Here is a glimpse into Myriam Schweingruber’s life and her dedication and love for KDE.</p>

<em>Myriam; could you tell us a bit about yourself and where you live?</em>
<p>I am 55 years old and a trained pharmacist from Switzerland. I currently work as a part-time scientific translator in the fields of pharmacy and medicine. I am also an avid computer user since the very first days of the Commodore 64 and the PC; you could describe me as kind of a nerd! I’ve been living in Germany for the last few years.</p>

<em>How did you first get involved with KDE?</em>
<p>When I first tried my hands on Linux in the late 90’s, KDE was the only sane desktop available. It must have been version 1 something and of course is not comparable to what we see now. Over the years I have tried a lot of other window managers and desktop solutions, but KDE applications and the Plasma desktop remain my solution of choice when I switch on the computer.</p>

<em>Wow. That sounds so interesting.  You’ve been with KDE for a very long time. How would you describe the evolution of KDE over the years? Any specific jumps/breakthroughs/changes that have been strongly imprinted in your mind? I’m sure everyone who’s been fairly new to KDE would love to hear about its history!</em>
<p>Well, I have used KDE software since the first versions of it, but I only got involved as a contributor later. I think what we all fell in love with what was KDE 3: it was really a desktop way ahead and left all the freedom to the user who could configure whatever they liked. This was indeed the true spirit of freedom as an indication of what Free Software means. I don't think there were other desktops around at that time that came up to par with it.</p>
<p>And KDE SC 4 was yet another big step to pave the way onto how a modern computer works; but also a new challenge that was hard to meet and didn't go without glitches. I remember to have been an early adopter of KDE SC 4 since the 4.0 previews, and I continuously had to remind myself that I was actually testing a technical preview, not a finished product. With the amount of applications that had to be moved to Qt4 it took years to get it polished, something maybe people were not aware of. What I usually reminded them of was to compare what is comparable: proprietary desktops get new releases every 5 or 6 years, at most, and there is a lot of money behind it. KDE SC 4 did it in just 3-4 years, and most of the work was done in our free time while still maintaining the Qt3 branches.</p>

<em>Why is KDE so special to you?</em>
<p>First of all; the community is awesome, as there is a spot for everybody who wants to get involved, regardless of your skills or background. It is probably also the only Free Software community out there who never had problems with integrating people. I am a member of KDE Women, of course. But while it is a stepping stone to attract more women into KDE it is certainly not needed as a hide-out; as women like all other groups have always been an integral part of the community.</p>

<em>It is so amazing to find a lady so passionate about technology and coding which as per the old fashioned norms has largely been viewed as a male’s domain. What is your message to all the girls out there who are budding tech geeks and wish to be involved with coding and FOSS? How would you motivate them to make their space in a male driven area of interest? Any words to get them to try their hand at technology and venture into different spaces?</em>
<p>One word: don't be shy! We have the same capabilities as men do, and remember: the first computer programmers were almost exclusively women! I would suggest all women making their way into IT to read up the history of computers: both the Zuse as well as the ENIAC and other early computers were programmed by women, as men thought that it was like kitchen recipes, so it couldn't be that difficult!

<em>Could you talk about how KDE in particular is working for more involvement of women and how gender biases as well as any other forms of discrimination are actually metamorphosed into encouragement and due equality so as to have a community where everyone feels just as welcome and no one is left out?</em>
<p>Well, I don't think we do anything special in the KDE community, we just consider every contributor as equals, and since we do that, we really don't need any specific groups. Everybody is welcome to contribute, what we value is the contributor, regardless of their background. Unlike other women groups, KDE Women doesn't act like a place where women come in and then stay there, it is just another door to the KDE Community. So the activity of KDE Women is rather low, we try to organize something on Ada Lovelace Day to get new contributors, but for the rest of the time it is just an open door and we never lock it!</p>

<em>Which specific area of KDE applications do you contribute to?</em>
<p>I am part of the Amarok team. I also contribute to the Bugsquad and the KDE testing team.</p>

<em>Could you pinpoint any particular role you play in KDE when it comes to contributing?</em>
<p>I don't think I have a specific role, I just work on what I can contribute to, so currently I triage bugs, mostly for Amarok and Phonon, and also work on other non-coding areas in Amarok (promotion, user support, documentation, etc).</p>

<em>Could you give a brief description of your experience in the past few years at Randa Meetings and your involvement?</em>
<p>The very first Randa Meeting was actually a Plasma sprint and took place in the family chalet of Mario Fux. I and Mark were living in Switzerland at that time and went for a visit. During a walk I remembered the summer camps I spent in Randa as a kid and spotted the old house where I’ve spent many summers. We (Mario, Mark and I) had the idea of checking if the house could actually be used to host other sprints and that is how everything started. I have since then attended every sprint in Randa and those have always been the most productive ones I ever attended.</p>

<em>Wow. So I think that I can very aptly label you as one of the founding fathers errr sorry; founding mother of the Randa Meetings! Any particular funny/memorable/scary incident in particular at any Randa Meet that you’d like to share?</em>
<p>Don't give too much credit to me, I was only one of the people who were at that particular sprint and Mario already had a similar idea, maybe I was just another push to move the idea forward.</p>
<p<There were indeed a few moments we are not going to forget: we discovered in Randa that Ivan Cukic was a great climber, Sebastian Kügler can tell everybody how cold a mountain lake really is, Ian Monroe proposed to Sherry in Randa and both David Faure and Sebastian Rahn are great piano players! Also eating potato chips is better done with a spoon or chopsticks, so that you don't get your hands dirty and can continue to hack without interruptions. I guess we could write a book with Randa anecdotes as I could go on for hours with memorable moments!</p>

<em>Have you got anything in particular planned for Randa this year?</em>
<p>Randa has always been the place where we got a tremendous amount of work done. The gorgeous surroundings, the good air and the lack of local distractions make working in Randa very productive. So yes, I plan to concentrate on my usual work, namely triaging bugs and updating the current documentation and user handbook to the latest changes, as well as adapting the website theme so that we can make the transition to a newer Drupal instance. We also plan to prepare for the Amarok 2.9 release that will integrate a lot of the work done over the last year.</p>

<em>What will you be looking forward to the most in Randa? Any particular people or projects you are eager to meet and collaborate with?</em>
<p>First of all: meeting all the old friends who gather in Randa that we rarely see in real life, as all of us are from different places around the world and Randa is the perfect place to finally get together again. Unlike Akademy, it is a more intimate gathering, focused on working on our projects, without the distractions of the talks and sightseeing. For me it is also the perfect occasion to meet the team members again, and finally also meet a GSoC student who is working with us since quite some time. Of course collaboration with the other KDE multimedia people is something I am also looking forward to, and of course the great food!</p>

<em>How important has Randa been for you in your journey with KDE and FOSS contribution through the past few years?</em>
<p>I think it is important not only for me, but for all who’ve been in Randa before, and we can all agree that it really is the best place to get a lot of work done. In the last few gatherings we managed to do a lot of work that needed active collaboration, like documentation, just to name one specific field. One of the Randa sprints was probably the week where we fixed more Amarok bugs than we did in the whole year before the meeting, just because we had everybody together in the same place.</p>

<em>Why do you think Meetings such as Randa are very important for KDE and FOSS communities around the globe?</em>
<p>While a lot of the work in Free Software is done over the internet, nothing replaces the real life meetings, as it provides an extra drive in terms of motivation. Modern software development is mostly agile, something even corporate software development is using more and more. Due to the global distribution of our contributors; Free Software development has always been agile to start with, even if we didn't put a label on it in the early days.</p>
<p>And in agile development; sprints are a very important element to push the project forward. While sprints can be done over the web, they are hindered by time-zones, external distractions, availability of contributors, etc. Having real life sprints, even if those are few, are more productive as all the hindrances of the web meetings are eliminated and the productivity is greatly enhanced.</p>

<em>Why do you think supporting such meetings is of importance and how has the support helped you as a KDE developer?</em>
<p>The support allows us to bring developers together who would not be able to attend the sprints and would then have to be involved with it remotely, with all the downsides of remote work.</p>

<em>Could you give a brief description of what your typical day in Randa was like in the past few years? </em>
<p>Get up early, grab a great breakfast and meet the team members at the breakfast table; plan the day, eventually schedule a meeting for later during the day or attend the meetings scheduled in advance. Then discuss specific problems with the people on site, get some work done, lunch break, short walk to get fresh air, discussions during walks, get work done, grab some fruit, work again, oh, it is already supper time! After supper, continue work, relax with friends and discuss more plans, eventually get aware that time flies and it is already midnight and try to get some sleep.</p>
<p>During the whole day tea and coffee are available, as well as fruits and sweets. Since all teams have specific rooms you always know where to find somebody, and of course we all use IRC to communicate and eventually schedule a meeting within 5 minutes.</p>

<em>Lastly; any particular message to the people of the world?</em>
<p><a href="http://kde.org/community/donations/">Support KDE</a> and the Randa Meetings!</p>

<em>Thanks a lot, Myriam, for your time for the interview and dedication to Amarok and the KDE community.</em>

