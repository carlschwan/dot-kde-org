---
title: "Calligra Gemini Added to Calligra Suite"
date:    2014-11-21
authors:
  - "leinir"
slug:    calligra-gemini-added-calligra-suite
comments:
  - subject: "Awesome! I want buy a 2-in-1"
    date: 2014-11-22
    body: "Awesome! I want buy a 2-in-1 Ultrabook!!!!!!!! I want Calligra Gemini now!!\r\nGreat work!"
    author: "Baltasar Ortega"
  - subject: "You totally should - having"
    date: 2014-11-22
    body: "You totally should - having used them for a while now, i'm finding myself wanting the functionality on my work laptop (a classic thinkpad), and stabbing at the screen with my finger ;) It's really a great thing :)\r\n\r\nYay, really glad you're so excited about it! :D\r\n\r\nThank you! :D"
    author: "leinir"
  - subject: "Great on windows but what about linux!"
    date: 2014-11-23
    body: "Great Job! That is trully the best convertible, convergent, experience I ever saw!\r\nDo you have any experience on linux? Can it detect the \"change of format\" too? \r\nI am really looking forward plasma and other kde apps gets the same functiunality, it will be a real killer!"
    author: "sylvain"
  - subject: "What he said"
    date: 2014-11-28
    body: "I fully agree with Baltasar: This really shows what convergence should look like.\r\nIsn't it ironic that a Microsoft device with a Microsoft OS needs KDE to show off how that hardware can be put to actually good use?"
    author: "Thomas Pfeiffer"
  - subject: "Linux :)"
    date: 2014-11-30
    body: "Thank you very much! Yeah, i must say i'm quite pleased with the whole thing myself, it really does work very pleasantly :)\r\n\r\nSo far i have not worked on it - however, i hope to change this very soon :) From a technical point of view, there are two things going on here. First you have to detect the current state on application startup (which, i have to be honest, Windows makes a total joke - basically there's three different APIs to use, and you have to sniff the windows version to tell which to use... which means we're not doing it, because quite frankly that's too silly). Secondly it's detecting the change, which requires a GPIO driver, which to my knowledge is built into the Linux kernel, but which is a not-always-present driver on Windows... So it /should/ be straight forward to do on Linux, i just have not had the time to actually check (my convertible hardware runs linux, though i expect i might be able to change that in the near future) :)\r\n\r\nA hope i've got on the rest-of-kde side is to perhaps create a Framework which will help you do these things, and add it to more applications... Obviously it won't magically lead to applications suddenly having a touch optimised UI, but it /would/ make it much easier to switch between the two when one such is developed :)"
    author: "leinir"
  - subject: "Sailfish Port"
    date: 2014-12-02
    body: "Great news. As Jolla's Sailfish OS also uses Qt, any possibility of collaboration to see this working on Jolla Tablet or Phone?\r\nhttps://together.jolla.com/question/65413/port-calligra-gemini-to-sailfishos-as-native-office-suite/"
    author: "Anand"
---
About <a href="https://dot.kde.org/2013/09/11/krita-demonstrated-idf-keynote">a year ago</a>, the <a href="https://www.calligra.org/">Calligra community</a> added a new application to the suite by the name of Krita Gemini, which combined the functionality of the Krita digital painting application with the touch optimised user interface of the tablet focused <a href="https://dot.kde.org/2012/12/19/krita-sketch-mobile-artistry">Krita Sketch</a>, into a shell with the ability to switch between the two at runtime. The goal was to create a responsive user interface for Krita, and this is now a part of Calligra. In May of this year, Intel approached <a href="http://www.kogmbh.com/">the team which produced Krita Gemini</a> with the idea of doing the same for other parts of Calligra, by creating an application which would encapsulate the Words and Stage components in the same way as Krita Gemini did for the Krita component.

<image src="http://leinir.dk/calligra/calligra-gemini-dot/mascot_konqi-app-office.png" style="float: right;" />

Now, about half a year later, we have an application which, while rough around the edges, works for day to day use. In fact, the author of this article has been using Calligra Gemini to produce both a novel and <a href="http://leinir.deviantart.com/art/Geiko-Eien-Ni-Eternally-Geisha-489060899">a short story</a>, as well as various other bits of work, and <a href="https://www.qtdeveloperdays.com/europe/lightning-talks#B3">a presentation which was shown off at the Qt Developer Days 2014 in Berlin</a>. Also worth mentioning here is that the pdf, epub and mobi versions of the short story available on the page there were also all created using Calligra Gemini, functionality which is available out of the box with Calligra.

<h2>Create-Edit-View</h2>
The workflow concept which guided the development of the application was conceived as Create-Edit-View, and it is built around the notion of what each of the modes of these devices do best in each mode.
<image src="http://leinir.dk/calligra/calligra-gemini-dot/mascot_konqi-app-presentation.png" style="float: right;" />
With a classic setup which includes a mouse and a keyboard, you use a classically designed application to create your content. This is the application layout you are used to from Words and Stage already - indeed they are exactly the same components used in those applications already, making use of the powerful component structure available in Calligra, made powerful by the KDE libraries.

When you then switch to only having a touch screen, you get a touch optimised experience, which allows you to edit the content of an existing document - create annotations, change layouts, move shapes around and so on. This, and indeed the view mode, is where the majority of the new work was done, and the whole thing is built around a set of Qt Quick components.

Finally, when you want to simply view the content, each component presents you with the ability to switch to a full-screened, distraction free environment designed to let you focus entirely on the content itself. In the Words component this means the application turns itself into an ebook style reader application, showing a full page at a time and with toolbars and other distractions hidden, when you turn the device into portrait mode, rather than landscape. For Stage, this is accessed by tapping on the play icon, which launches the presentation from the start, and shows a screen which allows you to give your presentation with only the information and functionality needed for that.

<h2>And Now, a Demonstration</h2>

The following two videos are demos of the concept described above, going through creation, editing and viewing in both the Words and Stage components. I hope you enjoy these as much as we did creating them!

<div style="text-align: center;"><iframe src="https://player.vimeo.com/video/111737183" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> <p style="text-align: center;"><a href="http://vimeo.com/111737183">Create, Edit and View Documents with Calligra Gemini</a> from <a href="http://vimeo.com/leinir">Dan Leinir Turthra Jensen</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

<div style="text-align: center;"><iframe src="https://player.vimeo.com/video/112050358" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> <p style="text-align: center;"><a href="http://vimeo.com/112050358">Create, Edit and View Presentations with Calligra Gemini</a> from <a href="http://vimeo.com/leinir">Dan Leinir Turthra Jensen</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

One thing to note: Apart from the obvious that we don't speed up and slow down in real life the way we do in those videos, nothing is faked. The videos are shot in a single take, and only edited for speed. The screen images you see are entirely real. The image on the wall in the second video is an Intel WiDi adaptor connected to a projector and the 2-in-1 ultrabook, meaning that yes, while there are no wires, that's the external display and showing the actual presentation. The document is loaded from a real Git repository, and the presentation from a real DropBox account. No simulated screen images here, we like our community too much to do that to them.

<h2>Qt Quick</h2>

<image src="http://leinir.dk/calligra/calligra-gemini-dot/mascot_konqi-dev-qt.png" style="float: right;" />
So, you might have noticed above the mention of Qt Quick. The components mentioned there are a continuation of the Qt Quick components built originally for Calligra Active and used as the basis for the Jolla Documents application. We have extended them with editing functionality and greatly improving both rendering speed and quality.

As you might notice especially in the Words demo video, there is some flickering when moving shapes around on the canvas. Unfortunately this is a side effect of the rendering method employed by Qt Quick 1. However, despair not - while this could have been ironed out now, we decided against doing that, as this is something which will all change during the Qt Quick 2 port which will happen as a part of the Calligra 3.0 process, which is planned to begin in January, which is when porting to KDE Frameworks 5 and Qt 5 will happen.

<h2>Completed, But Never Finished</h2>

Today marks the day when this project has been merged into the master branch of Calligra, which means that it will be a part of the Calligra 2.9 release. We are proud to be able to announce that we reached this milestone, and invite you to take it for a spin. Of course, as with all software projects, especially free software, there is no such thing as a finished project. This is but the beginning of Calligra Gemini, and we look forward to a bright future. So if you like our work and want us to keep going, please support us!

<p><a href="https://www.kde.org/fundraisers/yearend2014/"><img src="https://www.kde.org/images/teaser/bannerFundraiser2014.png" width="830" height="300" alt="fundraiser banner" /></a></p>