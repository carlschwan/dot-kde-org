---
title: "Thank You Akademy 2014 Sponsors"
date:    2014-09-02
authors:
  - "kallecarl"
slug:    thank-you-akademy-2014-sponsors
---
<div align="center" style="float: center; padding: 1ex; margin: 1ex; margin-top: 0ex; "><a href="https://akademy.kde.org/2014"><img src="/sites/dot.kde.org/files/banner700.jpg"></a></div>
<a href="https://akademy.kde.org/2014">Akademy</a> is a non-commercial event, free of charge for all who want to attend. Generous sponsor support helps make Akademy possible. Most of the Akademy budget goes towards travel support for KDE community members from all over the world, contributors who would not be able to attend the conference otherwise. The wide diversity of attendees is essential to the success of the annual in-person Akademy conference. Many thanks to <a href="https://akademy.kde.org/2014/sponsors">Akademy 2014 sponsors</a><!--break-->

Sponsors receive benefits beyond the feel-good rewards of supporting a worthy cause. They have the opportunity to:
<ul>
<li>Work closely with KDE contributors, upstream and downstream maintainers and users of one of the foremost User Interface technology platforms.</li>
<li>Meet other leading Free and Open Software players. In addition to the KDE Community, many other Free Software projects and companies participate in Akademy.</li>
<li>Get inspired. Akademy provides an excellent environment for collecting feedback on new products, or for generating new ideas.</li>
<li>Support Free and Open technologies. Much of the actual work of Akademy is done in new application development, hackfests and intensive coding sessions. These activities generate value that goes far beyond KDE and Akademy.</li>
<li>Be part of the KDE Community. KDE is one of the largest Free and Open Software communities in the world. It is also dynamic, fun-loving, cooperative, committed, creative and hard-working.</li>
</ul>

<big><strong>2014 Silver Sponsors</big></strong>
<strong><a href="http://www.blue-systems.com/">Blue Systems</a></strong> is a company investing in Free/Libre Computer Technologies. It sponsors several KDE projects and distributions such as Kubuntu and Netrunner. Their goal is to offer solutions for people valuing freedom and choice. 	

<strong><a href="http://www.digia.com/">Digia</a></strong> is responsible for all worldwide Qt activities including product development, commercial licensing, and open source licensing working together with the <a href="http://qt-project.org/wiki/The_Qt_Governance_Model">Qt Project under the Open Governance model</a>. Digia has in-depth Qt expertise and experience from demanding mission-critical development projects, as well as hundreds of in-house certified Qt developers. Digia offers licensing, support and services capabilities, and works closely with developers working on Qt projects. 
 
<strong><a href="http://www.redhat.com/en">Red Hat</a></strong> is the world's leading provider of open source solutions, using a community-powered approach to provide reliable and high-performing cloud, virtualization, storage, Linux, and middleware technologies. 	
 
<big><strong>2014 Bronze Sponsors</strong></big>
<strong><a href="http://www.froglogic.com/">froglogic GmbH</a></strong> is a software company based in Hamburg, Germany. Their flagship product is Squish, the market-leading automated cross-platform testing tool for GUI applications based on Qt, QML, KDE, Java AWT/Swing and SWT/RCP, JavaFX, Windows MFC and .NET, Mac OS X Carbon/Cocoa, iOS Cocoa Touch, Android and for HTML/Ajax or Flex-based Web applications running in various Web browsers. 	
 
<strong><a href="https://www.google.com/intl/en/about/">Google</a></strong> is a leading user and supporter of open source software and development methodologies. Google contributes to the Open Source community in many ways, including more than 50 million lines of source code, programs for students including Google Summer of Code and the Google Code-in Contest, and support for a wide variety of projects, Linux User Groups, and other events around the world.
 
<strong><a href="http://www.kdab.com/">KDAB</a></strong>—the world's largest independent source of Qt knowledge—provides services, training and products for Qt development. KDAB engineers deliver peerless software, providing expert help on any implementation detail or problem. Market leaders in Qt training, our trainers are all active developers, ensuring that the knowledge delivered reflects real-world usage.
 
<big><strong>2014 Supporters</big></strong>
<strong><a href="http://www.openinventionnetwork.com/">Open Invention Network</a></strong> (OIN) is a collaborative enterprise that enables Linux through a patent non-aggression community of licensees. OIN also supports Linux Defenders, which helps the open source community defend itself against poor quality patents by crowdsourcing defensive publications and prior art. 	
 
<strong><a href="https://owncloud.com/">ownCloud Inc.</a></strong> was founded in 2011, based on the popular ownCloud open source file sync and share community project launched within the KDE Community. The goal of ownCloud Inc. is to give corporate IT greater control of their data and files — combining flexibility, openness and extensibility with on-premise servers and storage.

A special thank you to <strong><a href="http://ev.kde.org/supporting-members.php">KDE's Patrons</a></strong> who support the KDE Community throughout the year.

<h2>Akademy 2014 Brno</h2>
For most of the year, KDE—one of the largest FOSS communities in the  world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2014">Akademy</a> provides all KDE contributors the opportunity to meet in person to  foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and other types of KDE  contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work to bring those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

If you are someone who wants to make a difference with technology, <a href="https://akademy.kde.org/2014">Akademy 2014</a> in Brno, Czech Republic is the place to be.