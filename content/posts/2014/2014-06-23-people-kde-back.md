---
title: "People of KDE is back"
date:    2014-06-23
authors:
  - "unormal"
slug:    people-kde-back
---
As <a href="http://mbohlender.wordpress.com">Michael Bohlender</a> (known to some e.g. for his GSoC project about Kmail Active last year) needed to do some interviews for his anthropology course at the university he decided to reactive the <a href="http://www.behindkde.org">People behind KDE series</a> or, as they are now named, the <em>People of KDE</em> series.

In the <a href="http://mbohlender.wordpress.com/2014/06/12/people-of-kde-mario-fux/">first episode</a> (as computer scientists call it: Episode 0) he had a video chat with Mario Fux, best known for the organization of the <a href="http://randa-meetings.ch/">Randa Meetings</a>. Watch <a href=" http://www.youtube.com/watch?v=XRSK-1Ka1wc">this 30 minutes episode</a> to get more information about what Mario does in KDE, about the history of the Randa Meetings and why it is well worth to <a href="http://www.kde.org/fundraisers/randameetings2014/index.php">support them</a>.

<div style="float: right; padding: 1ex; margin: 1ex;"><img width="400" height="225"  src="/sites/dot.kde.org/files/gcompris-menu.jpg" /><br />GCompris</div>

After the test run Michael had another <a href="https://www.youtube.com/watch?v=VzDRznPwXFM
">video chat with Bruno Coudoin</a> this week. Bruno is best known for his work on <a href="http://www.gcompris.net">GCompris</a> a great educational application for kids between 2 and 10 years old (and older ones ;-). Some months ago he decided to <a href="http://gcompris.net/wiki/Qt_Quick_Migration_status">rewrite</a> his application and chose Qt Quick as the technology to reach out to other platforms like Android and Co. As Bruno and his team need to rewrite more than 140 activities this will need some dedicated hacking time and they will participate in the Randa Meeting this August. You can take a look of the current state of porting GCompris from GTK to Qt in <a href="https://www.youtube.com/watch?v=J675KRVuFZA">this video</a>.

A big <em>thank you</em> to Michael for his interviews, we are looking forward for more episodes of People of KDE!