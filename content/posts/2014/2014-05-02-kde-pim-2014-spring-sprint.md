---
title: "KDE PIM 2014 Spring Sprint"
date:    2014-05-02
authors:
  - "markg85"
slug:    kde-pim-2014-spring-sprint
comments:
  - subject: "KAddressbook without maintainer ??"
    date: 2014-05-02
    body: "It's interesting because I am the maintainer from 3 releases and I added some features to kaddressbook and improve it.\r\nSo it will ported to qt5 I did it yet in framework branch.\r\nSo yes I removed knode from framework branch but not kaddressbook.\r\nRegards.\r\n"
    author: "Montel"
  - subject: "\"KOrganizer love\" this is"
    date: 2014-05-02
    body: "\"KOrganizer love\" this is truly a great thing to hear."
    author: "Anonymous"
  - subject: "Kontact without KAddressbook?"
    date: 2014-05-05
    body: "That makes no sense to me ..... How should i keep my contacts. In some txt files? How should i synchronize my per contact settings over caldav (or some other service) after this?\r\nSo i am relay glad By Mondel corrected this. I means Kontact without Addresses is like KMail wihtout the ability to send Mails :-)\r\n\r\nAnyhow, thanks for the rest of your work! I like Kontact a lot."
    author: "Till Sch\u00e4fer"
---
We continue the tradition of having the PIM sprint in a place that starts with a "B". The last 3 PIM sprints were in Berlin (twice) and Brno. The Spring edition of this year took place in Barcelona, continuing the tradition. Add to this the name of the company hosting us which conveniently starts with a "B" as well (BlueSystems).

<div style="position: relative; left: 0%; padding: 1ex; margin-left: -5px; width: 100%;"><img src="http://dot.kde.org/sites/dot.kde.org/files/group_picture.JPG" /></a><br />From left to right, top row: Martin Klapetek, Christian Mollekopf, Mark Gaiser, Alex Fiestas<br />Bottom row: Vishesh Handa, Daniel Vrátil, David Edmundson, Sergio Martins, Sandro Knauß</div>
<h2>KOrganizer love</h2>
One of our regular attendees, Christian Mollekopf, had a nice surprise for us at the sprint. He works for Kolab Systems and told us about some of their plans in terms of the upcoming deployment of KDE PIM in the city of Munich. This should bring some usability improvements, including work on KOrganizer which should be able to better address a situation where a user literally has access to thousands of calendars. Kolab Systems is also working on bug fixes, stabilization improvements and optimizations throughout the KOrganizer product and entire KDE PIM application. The best thing is that all changes will be sent back for the KDE community to enjoy. The regular KDE software user will begin to see these changes in the KDE Applications release 4.14.
<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/debugging.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/debugging_wee.jpg" /></a></div>
<h2>Frameworks</h2>
With the upcoming releases of KDE Frameworks and Plasma 2014.06, there is a need to port the heart of PIM applications to Qt 5. Some sprint attendees started work in that area which will make it possible to do things such as KMail running on top of Plasma 2014.06. We also decided that applications without a maintainer won't be ported to Qt 5. Applications that will probably not be ported are KNode and KAddressbook. The latter is currently being rewritten in a GSoC project.

<h2>Bugs and performance</h2>
A habit in sprints is to take a look at those pesky issues that are difficult to figure out alone. Quite a few bugs were resolved and performance has been improved throughout the PIM applications. Many of these changes are already available in the recently released KDE Applications 4.13.
<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://dot.kde.org/sites/dot.kde.org/files/debugging_1.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/debugging_1_wee.jpg" /></a></div>
<h2>Donations</h2>
KDE software is developed mostly by volunteers. Most sprint attendees are volunteers (and some are fortunate to do KDE software development as a daily job). We spend much of our free time improving the experience of our users. Having a sprint is made possible by a company willing to host us and our KDE e.V. organization willing to cover the traveling and hotel costs. We are rely on your <a href="http://www.kde.org/community/donations/">donations</a> to continue this. If you want to support KDE software development and have the financial means, please consider hitting the donations link.