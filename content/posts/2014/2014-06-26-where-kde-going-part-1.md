---
title: "Where KDE is going - Part 1"
date:    2014-06-26
authors:
  - "jospoortvliet"
slug:    where-kde-going-part-1
comments:
  - subject: "Thanks"
    date: 2014-06-26
    body: "This really sounds great. Thank you for those wonderful products !"
    author: "nicofrand"
  - subject: "Optimistic"
    date: 2014-06-27
    body: "Everything I've been reading about the switch to frameworks-5 has me optimistic that things will go dramatically better, this time around, than it did with kde4.  Among other things, the pre-release kde4 PR was all about the whiz-bang new technology, and I guess people kinda lost sight of actually keeping working things supported and working until the replacement properly worked as well as what it was intended to replace.\r\n\r\nThis time around, however, the focus seems to be on modularity and letting devs and distros and users pick and choose kde5/frameworks based solutions as they mature, while continuing to support kde4 -- and interoperation with non-kde technology where it makes sense too -- for areas where the frameworks-5 based solution isn't yet mature.\r\n\r\nThat seems far healthier than the last time around, and I'm looking forward to the chance to try various bits out. =:^)\r\n\r\n(As for testing, I'm actually running live-git kde4 from the gentoo/kde overlay package build scripts, and there's frameworks-5 and plasma-next package builds as well, but they depend on qt5, which isn't there or in the main gentoo tree yet, only (presumably) in the gentoo/qt overlay, which I don't have loaded.  So I've not get gotten a chance to actually try the plasma-next stuff, but am looking forward to it.)"
    author: "Duncan"
  - subject: "mm."
    date: 2014-06-27
    body: "\"and I guess people kinda lost sight of actually keeping working things supported and working until the replacement properly worked as well as what it was intended to replace.\"\r\n\r\nNot really; we supported 3.5 for an additional year post-4.0 with new releases, something we announced prior to the release of 4.0 and repeated in the keynote presentation at the release event. That vast majority of new things in the 4.x codebase were not replacements, either; they were new frameworks that provided access to new functionality.\r\n\r\nWhat has changed this time around is:\r\n\r\n* the workspaces and libraries are fully separated in terms of release, so each can be released when they are individually ready\r\n* the port from Qt4 to Qt5 compared to the work necessary for Qt3 -> Qt4 (exception: QML)\r\n* very little architecture is being revisited (including a few things that need that; next time :)\r\n\r\nThese differences also explain the differences in promotion. This is very similar, in fact, to the 3.0 release relative to the 2.x release.\r\n\r\nLearning the _right_ lessons are important, otherwise one makes the wrong decisions thinking they've learned when they haven't. In the case of 4.0 it wasn't an innovation problem but problems stemming from how KDE made releases (starting from 1.0 and never changing suitably), and taking it as an innovation problem may well lead to the mythology that innovation is not worth doing. The lesson was to change the dev-and-release processes to match the evolving needs of the various projects."
    author: "aseigo"
  - subject: "Please, fix font rendering"
    date: 2014-06-27
    body: "Please, fix font rendering"
    author: "Guilherme"
  - subject: "Featured as news"
    date: 2014-06-27
    body: "Thanks for the KDE article! It includes informative images and goes into details but not too much. \r\n    \r\nIt's featured as news and people talk about it in www.osnews.com/comments/27803"
    author: "Sys"
  - subject: "KDE"
    date: 2014-06-27
    body: "Are we playing black and white here gnome 3 is black and kde is all white."
    author: "archuser"
  - subject: "This is more up to taste than even modern art..."
    date: 2014-06-28
    body: "'fix' it into what? Everybody disagrees on how fonts should look. Half the world hates Mac fonts, the other half loves it. Half the world wants sub-pixel rendering, the other half pukes at it.\r\n\r\nActually, the halfs mentioned above are part of the 0.0001% who care about font rendering, the rest of the world just thinks people moaning about it should get a life ;-)"
    author: "jospoortvliet"
  - subject: "Nice work"
    date: 2014-06-28
    body: "KDE is really amazing, I really hope it will become the most used Linux DE. Thank you for your work."
    author: "vonVlad"
  - subject: "The _Right_ Lessons"
    date: 2014-06-28
    body: "There might be more than one lesson. The switch to KDE 4.x cost me my favorite features and settings. I still miss Kasbar daily. And I personally didn't see any benefit from the innovations but had an awful time with dual screen support while others struggled with the database backends. I'm not saying that for some users and developers the releases were the issues. However, for some the innovations were the issues. For me, feature regressions remain the issue."
    author: "Eric Fitton"
  - subject: "UI Development"
    date: 2014-06-30
    body: "It's nice to see that some real effort is being made to improve the aesthetic of the UI, though by the looks of it there is still much to be done. As a UI designer/developer myself I always found the harsh juxtaposition of KDE4's strong, functional UI and ugly UI components to be quite bemusing, so much effort was put into ensuring that the usability was of a high standard that one could only assume that the aesthetic it's self was given little attention - the window manager, the lackluster polish on navigation and menu components and the icon theme as being prime examples. Unfortunately not even the most rigorous attempts to compensate for these perceived flaws via the available customization was truly sufficient.\r\n\r\nThe clean, modern, minimalist style has certainly got my attention though. I look forward to seeing further improvements across the UI as development progresses."
    author: "bitV"
  - subject: "Thanks for the nice words. I"
    date: 2014-07-05
    body: "Thanks for the nice words. I've updated the screenshots in this article, and there's a nice video on youtube showing the latest version off quite nicely: <a href=\"https://www.youtube.com/watch?v=M5_zJJfsGsA\">here</a>."
    author: "jospoortvliet"
---
<div style="width: 304px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/people%20oriented%20programming.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/people%20oriented%20programming.png" /></a></div> This article explores where the KDE community currently stands and where it is going. Frameworks, Plasma, KDE e.V., Qt5, KDE Free Qt Foundation, QtAddons - you heard some of these terms and want to know what all the fuss is about? A set of articles on the Dot aims to bring some clarity in the changes and constants of the KDE community in 2014 and further. This is the first article, diving into the technical side of things: Plasma, applications and libraries.

<h2>KDE is People</h2>
Today our technology goes much further than the <a href="http://www.kde.org/community/history/">humble beginnings</a> in 1996, when we started out building a <i>'Desktop Environment'</i>. KDE today has many hundreds of active developers. They make not only a 'desktop' (<i>Plasma Desktop</i>) but also a variant for tablets (<i>Plasma Active</i>) and TVs (<i>Plasma Media Center</i>); <i>Plasma Netbook</i> is already 5 years old!

Meanwhile, the KDE applications have gone beyond simple clocks and calculators – we have a full office suite, mail and calendaring, video and image editors and much more. Not only that, KDE applications are being ported to multiple platforms - not just Windows and Mac, but also Android and other mobile operating systems. And our libraries (being renamed to Frameworks 5) are going modular, making them freely available to a far wider audience than just KDE developers. 

Today, KDE is no longer a Unix Desktop Environment. Today, KDE is people: Us. You and me. And our technologies—Plasma, Applications and Frameworks—are doing more today than ever before. Let's explore where they are going, starting with Plasma, central to our desktop interface.
<div style="width: 304px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/clocks.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/clocks.png" /></a></div> 

<h2>Plasma by KDE</h2>
Plasma was conceived as the next generation of KDE's desktop technology. When its architecture was drafted in 2006 and 2007, the goal of the developers was to build a modular base suitable for multiple different user interfaces. It is easy to see this as an obvious goal in a world with high resolution displays, tablets, mobile phones, media centers and so on. But as <a href="http://blog.jospoortvliet.com/2014/01/building-converging-uis.html">argued here</a>, until today, KDE technology is unique in its ability to converge the different form factors at a code level. Others are still either attempting to build one interface for a wide variety of devices, looking for a middle ground or have realized that user interface convergence is a futile exercise and created separate interfaces. 

<div style="width: 304px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/plasma-workspaces.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma-workspaces.png" /></a><br />Multiple Plasma Workspaces in <a href="http://www.notmart.org/index.php/Software/Towards_a_declarative_Plasma">March 2011</a></div> 

<h3>Plasma 5</h3>
Plasma took some time to mature, in part due to its ambitious design, in part because the technologies it built upon were not mature enough for the needs of Plasma. This is still somewhat of a problem today, and the 4.x series has workarounds to deal with the deficiencies in the platforms below it.

This is where the next generation of Plasma technology comes in. Conveniently named Plasma 5, it will bring pixel-perfect design and super smooth performance thanks to the QML and Qt 5 technologies and fully hardware accelerated display rendering. High DPI support and the ability to work with Wayland (Linux's next generation display server) are planned as well, but neither are expected to be fully finished with the first release.

<div style="width: 354px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/scaling-3.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/scaling-3.png" /></a><br />A scalable UI</div> 

With Plasma 5 the team can start working on bringing seamless switching of work-spaces when moving from device to device. For example, plugging a keyboard and mouse into a tablet can trigger Plasma to transform its tablet-and-touch optimized UI into the desktop interface. And the applications, being notified of the change, can follow adapting to the new form factor. The current Plasma technology already can hint to applications which QML/ Javascript/ graphics files fit the current form factor and is already being used in Plasma Active, the tablet-optimized workspace of Plasma. None of this requires logging out-and-in – you can just continue working with the document you were working in or keep reading that web site!

These capabilities put the current Plasma far ahead of any competitor and the gap will only increase with the release of Plasma 5. But these advanced features do not take away from the familiar interface. The Plasma team is fully aware of value of established work flows of computer users and the need of not disrupting them. This means that there will be minimal feature loss or changes in the setup of the desktop. Just butter-smooth performance, polished look and more flexibility.

<h3>The Visual Design Group, Interaction Design and Usability</h3>
<div style="width: 604px; float: center; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/plasma-main_0.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/plasma-main_0.png" /></a><br />A new design for Plasma</div> 

Aside from technical work, there is design and usability work going on. The idea behind the Visual Design Group was to build a team in KDE which would focus on design. This is done in a rather novel way, led by the enthusiasm of Jens Reuterberg, a FOSS enthusiast and designer from Sweden. Since the inception of the design team, there has been work in many areas. There have been new icons and improvements to existing design elements of KDE software but the majority of work has been focused on Plasma 5. A widget theme is in development, a cursor theme as well and icons are being discussed. And Plasma 5.0 will move to the <a href="http://vizzzion.org/blog/2014/05/grumpy-wizards/">Oxygen font</a> by default. But the team also looks at interaction design and work flows in the interface, working together with the KDE usability team.

The usability team keeps developers and designers experimenting with new user interfaces close to the ground, making sure the user impact of their work is evaluated. The team conducts surveys and tests as well as using its own expertise to help the KDE developers design powerful but easy to use applications.

<div style="width: 604px; float: center; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/lockscreen.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/lockscreen.png" /></a><br />The new lock screen</div> 

Usability experts have been giving feedback in various areas of KDE's software, for example working closely with the developers of a new network manager interface for Plasma. Another example is the chat room experience in KDE Telepathy. Currently, work is being put into redesigning Systemsettings and many other things.

<div style="width: 304px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/battery_0.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/battery_0.png" /></a><br />A redesigned battery applet</div> 

 At events like Akademy, the usability team gives developers training in testing user interfaces with real users. Aside of working directly with developers and training them, the usability team has been reworking KDE's Human Interface Guidelines.

<h3>Work in progress</h3>
The first release of this new generation Plasma will not be without its issues. With a substantial change in underlying stack come exciting new crashes and problems that need time to be shaken out. This can also lead to visual artifacts. While QML2 brings better looks due to its seamless integration of openGL and more precise positioning, the immaturity of Qt Quick Controls, the successor to the 15 year old widget technology in Qt, will bring some rough edges in other areas. Moreover, as the latest Beta announcement points out, performance is also heavily dependent on specific hardware and software configuration:
 
“<i>In some scenarios, Plasma 5 will display the buttery smooth performance it is capable off - while at other times, it will be hampered by various shortcomings. These can and will be addressed, however, much is dependent on components like Qt, Mesa and hardware drivers lower in the stack. This will need time, as fixes made in Qt now simply won't be released by the time the first Plasma 5 version becomes available.”</i> Plasma 5.0 is scheduled for release July 2014.

<b>Read more:</b>
<ul>
<li><a href="http://dot.kde.org/2013/12/20/plasma-2-technology-preview">Plasma Next technology preview announcement</a> (currently, <a href="http://dot.kde.org/2014/06/10/plasma-5-second-beta-needs-testing">beta 2 is out</a> and testing is appreciated!</li>
<li><a href="http://vizzzion.org/blog/2014/02/next-means-focus-on-the-core/">"Next means focus on the Core" blog post on Plasma Next changes</a> by Sebastian Kügler</li>
<li>KDE's Converging User Interfaces work <a href="http://blog.jospoortvliet.com/2014/01/building-converging-uis.html">is ahead of the competition</a></li>
<li><a href="http://wheeldesign.blogspot.com/">Blog by KDE's design team</a></li>
<li>Work is going on to redesign Systemsettings, see <a href="http://sessellift.wordpress.com/2014/04/02/choose-your-own-experience-this-time-its-for-real/">this post</a>, <a href="http://wheeldesign.blogspot.se/2014/04/a-monday-report-9-of-sorts.html">this</a> and <a href="http://user-prompt.com/kde-system-settings-about-the-general-navigation/">this</a>. There is also the <a href="http://user-prompt.com/kde-network-manager-understanding-the-mental-model/">usability work on Network Management</a></li>
<li><a href="http://kokeroulis.wordpress.com/2014/06/08/plasma-active-on-qt5kf5-wallpapers-and-activities-configuration/">Work on Plasma Active restarting</a>, <a href="http://vizzzion.org/blog/2014/02/reasonable-dpi-in-plasma-next/">High DPI work on Plasma Next</a> and <a href="http://vizzzion.org/blog/2014/05/locale-changes-in-plasma-next/">work on locale changes</a>, <a href="http://blog.martin-graesslin.com/blog/2014/03/system-tray-in-plasma-next/">systemtray in Plasma Next</a> and <a href="http://vizzzion.org/blog/2014/02/dbus-activated-systemtray-plasmoids/">one more on that subject</a></li>
<li>Harald Sitter announced that there are now <a href="http://apachelog.wordpress.com/2014/06/20/weekly-plasma-5-live-iso/">weekly Plasma 5 live images being released</a> to aid testing and feedback</li>
</ul>

<div style="width: 354px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE%20in%20progress.png" /></div> 

<h2>The KDE Applications</h2>
Compared to the desktop and libraries, the situation with KDE's applications is simpler. Currently at 4.13, the next release will be 4.14, coming in August. After that there will be another release (together with KF5-based applications) but what comes next is still up for discussion. KDE's release team has been experimenting with shortening the release cycle. Shorter release cycles seem to be a trend throughout the ecosystem, facilitated by improved tools and processes.

<h3>A very fast release cycle?</h3>
Experiences in the world of mobile and web applications have shown that users are far more likely to start using features and appreciate small batches instead of large dumps. Short release cycles can bring bug fixes and improvements to our users much faster. On the other hand, most users of KDE software access their software and updates through the downstream distributions which are on slower release cycles even though they have repositories for updated software. Therefor this is a discussion which needs to include the distributions as much as the upstream developers.

And in any case, both our release infrastructure and our promotion will have to be adjusted as well. This has been started on the KDE Community mailing list, with proposals involving a clean up of the KDE Applications and changes in the release cadence.
<!-- and turning KDE Applications 4.15 into a Long Term Support release so application developers can move their focus on Frameworks 5.-->

<h3>Moving to Frameworks 5</h3>
The trend towards shorter release cycles requires many questions answered before it becomes feasible in practice. But a move to Frameworks 5 is certain to happen at some point, the question merely is when. Some applications have already started porting, encouraged by the swift progress being made on Frameworks 5. However most have not; it is not likely that most applications will have been ported to Frameworks 5 by the end of the year. Porting is relatively easy but the teams vary in focus and goals so we will have a Frameworks 5 based Applications release next to a 4.x series for a while.

Here again, KDE developers want the upgrade process to be smooth for users. In short, the 4.x series will be with us for the time being, and a Frameworks 5 series will be available in parallel. Regardless of the series, applications will work fine under any desktop. Developers want to ensure that migration is not an issue.

<b>Read more:</b>
<ul>
<li><a href="http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search">Next generation search by KDE available for the 4.x series</a>. More such major changes will probably not come out for the 4.x series but instead focus on Frameworks.</li>
<li>A recent blog post about <a href="http://www.proli.net/2014/06/21/porting-your-project-to-qt5kf5/">porting applications to Qt5 and Frameworks 5</a> by Aleix Pol</li>
<li><a href="http://dot.kde.org/2014/03/03/kde-pim-november-sprint">KDE PIM team meeting report</a>. The first porting steps to Qt5 and Frameworks 5 are being taken but the team expects this to take 1-2 years. In the mean time, KDE mail, calendaring and contacts as part of Applications 4.14 continues to get features like the <a href="http://www.progdan.cz/2014/06/improved-gmail-integration-in-kde-pim-4-14/">improved integration of GMail coming in KDE Applications 4.14</li>
<li>QML support <a href="http://steckdenis.be/post-2014-06-17-qml-module-versions-and-automatic-imports.html">being improved in KDE's development tools</a></li>
<li><a href="http://mail.kde.org/pipermail/kde-community/2014q2/000722.html">Discussion about where KDE Applications and releases are going</a> on the KDE Community mailing list. <a href="http://mail.kde.org/pipermail/kde-community/2014q2/000723.html">Proposal One</a>. <a href="http://mail.kde.org/pipermail/kde-community/2014q2/000724.html">Proposal Two</a>. <a href="http://mail.kde.org/pipermail/kde-community/2014q2/000725.html">Proposal Three</a>.</li>
<li>Work on Platform 4.x series applications continuing: <a href="http://grulja.wordpress.com/2014/02/27/plasma-nm-is-finally-out/">Plasma NM is out</a>, <a href="http://www.digikam.org/node/710">Digikam 4.0 progress</a>, <a href="http://soliverez.com.ar/home/2014/02/on-the-road-to-kmymoney-4-8/">KMyMoney 4.8 coming</a>, <a href="http://feedproxy.google.com/%7Er/krita/news/%7E3/GvI4HTFWKVA/220-krita-2-8-0-released">Calligra</a> and <a href="http://feedproxy.google.com/%7Er/krita/news/%7E3/GvI4HTFWKVA/220-krita-2-8-0-released">Krita 2.8 released</a> (note the <a href="http://krita.org/kickstarter.php">krita kickstarter</a> action).</li>
</ul>

<div style="width: 304px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE%20QT%205.png" /></div> 

<h2>KDE Libraries</h2>
When KDE began more than 15 years ago, development was application-driven. Libraries were intended to share work, making development easier and faster. New functionality in the libraries was added based on simple rules. For example, if a particular functionality was used in more than one place, it was put into a shared library. Today, the KDE libraries provide high-level functionality like toolbars and menus, spell checking and file access. They are also used occasionally to fix or work around issues in Qt and other libraries that KDE software depends upon. Distributed as a single set of interconnected libraries, they form a common code base for (almost) all KDE applications. 

<h3>Frameworks 5</h3>
Under the KDE Frameworks efforts, these libraries are being methodically reworked into independent, cross platform modules that will be readily available to all Qt developers. Some functions have already been adopted as Qt standards. The KDE Frameworks—designed as drop-in Qt Addon libraries—will enrich Qt as a development environment. The Frameworks can simplify, accelerate and reduce the cost of Qt development by eliminating the need to reinvent key functions. Qt is growing in <a href="http://qtinsights.com/wp-content/uploads/edd/2013/09/Qt-Insights-Report-Sept2013.pdf">popularity</a>. Ubuntu is building on Qt and QML for Ubuntu Phone and <a href="https://lists.ubuntu.com/archives/ubuntu-devel/2013-March/036776.html">planning to move over the desktop in the future</a>. The <a href="http://blog.lxde.org/?p=1046">LXDE desktop</a> and <a href="http://sourceforge.net/mailarchive/message.php?msg_id=31951393">GCompris</a> projects are in the process of porting over to Qt. Subsurface (a divelog project made famous by having Linus Torvalds as core contributor) has had its <a href="http://liveblue.wordpress.com/2013/12/16/subsurface-4-0-has-been-released/">first Qt based release</a>.

With Frameworks, <a href="http://www.datamation.com/open-source/why-do-users-choose-kde.html">KDE</a> is getting closer to Qt, benefiting both, as well as more and more users and developers. The Frameworks team plans to go for monthly releases with 'branch-less development'. This means that everything will be developed in master, so each release will contain a few new features and bugfixes. Of course, this type of release cycle comes with a price of its own. Features in released modules can only be introduced in a very fine grained way so as to not jeopardize stability and our continuous integration and testing tools will be taken very seriously. All modified code has to come with corresponding tests and there is a strong focus on peer review. This model is still under discussion with the distribution teams, considering the impact on their release practices. KDE Frameworks 5.0 is planned to be released in the first week of July 2014.

<b>Read more:</b>
<ul>
<li>Frameworks 5 was started at the Randa meeting in 2011. Consider supporting the <a href="http://dot.kde.org/2014/05/27/randa-moving-kde-forward">fundraiser to make Randa 2014 happen</a></li>
<li><a href="http://dot.kde.org/2013/09/25/frameworks-5">Frameworks 5 explained</a></li>
<li><a href="http://dot.kde.org/2013/12/17/qt-52-foundation-kde-frameworks-5">KDE's contributions to Qt 5</a></li>
<li><a href="https://www.linux.com/news/software/applications/755768-kde-frameworks-5-a-big-deal-for-free-software">article on Linux.com about relevance of Frameworks 5 for Free Software</a></li>
<li><a href="http://dot.kde.org/2014/01/07/frameworks-5-tech-preview">Frameworks 5 Tech Preview released</a> (<a href="http://dot.kde.org/2014/06/05/kde-releases-3rd-beta-frameworks-5">Beta 3 is out already</a>)</li>
<li>Sebastian Kügler shares some <a href="http://vizzzion.org/blog/2014/06/five-musings-on-frameworks-quality/">musings on the quality of Frameworks</a></li>
<li><a href="http://agateau.com/2014/03/01/dependency-diagrams-on-api.kde.org">Dependency diagrams now visible on api.kde.org</a></li>
</ul>

<h2>Conclusion</h2>
Now, we've covered the Frameworks, Applications and Plasma—the full gamut of KDE technologies. By summer of this year we can expect new generation Frameworks and Plasma to be available. The Applications will take a tad longer, but should run on any desktop. All have release cycle changes, no longer releasing as part of the full <i>"KDE Software Compilation"</i>. Compared to the previous major change in platform (KDE 4.0), these will be incremental on a technical level. Plasma 5 and Frameworks 5 are very much about taking advantage of the fact that our infrastructure has caught up with our ambitions. We intend to deliver these benefits in the form of a great experience for our users!

Next week, we'll publish part two of the 'where KDE is going' mini-series, with a look at KDE's governance and how our community has been changing.

<em>These articles are based on <a href="http://kde.in/content/where-kde-and-where-it-going">a talk</a> given at <a href="http://conf.kde.in">conf.kde.in</a> by <a href="http://blog.jospoortvliet.com">Jos Poortvliet</a> with lots of input from KDE contributors. A more extensive version of these articles, quoting members of the KDE community for more background, can be found in the upcoming (August) issue of <a href="http://www.linuxvoice.com/">Linux Voice magazine</a>, 'the magazine that gives back to the Free Software community'</em>