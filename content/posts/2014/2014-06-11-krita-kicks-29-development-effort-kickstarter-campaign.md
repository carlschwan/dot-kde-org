---
title: "Krita Kicks Off 2.9 Development Effort with a Kickstarter Campaign"
date:    2014-06-11
authors:
  - "Boudewijn Rempt"
slug:    krita-kicks-29-development-effort-kickstarter-campaign
comments:
  - subject: "Nice,could there be something like this for Akregator/KMail too?"
    date: 2014-06-12
    body: "Nice, could there be something like this for Akregator/KMail too?"
    author: "mark"
---
<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://krita.org/kickstarter.php"><img src="http://www.valdyas.org/~boud/kickstarter.png" /></a><br />Krita Fundraiser on Kickstarter</div>

Five years ago, the Krita team decided raise funds to raise Krita to the level of a professional applications . That <a href="http://dot.kde.org/2009/12/02/krita-team-seeking-sponsorship-take-krita-next-level">fundraiser was successful</a> beyond all expectations and enabled us to release Krita 2.4, the first version of Krita ready for professional artists! 

Now, it’s time for <a href="http://krita.org/kickstarter.php">another fundraiser</a>, much, much more ambitious in scope! Dmitry Kazakov has worked full-time on Krita 2.8, and now we want him to work full-time on Krita 2.9, too. And it’s not just Dmitry: Sven, who has contributed to Krita for over ten years now, has recently finished university and is available as well.

So, we’ve setup a base goal that would cover Dmitry’s work, a stretch goal that would cover Sven’s work and a super-stretch goal that would cover porting Krita to the last remaining OS we don’t cover: OS X.

Since 2009, the Krita project has had three more sponsored projects, and all of them delivered: the Comics with Krita and Muses training DVD’s and Dmitry’s work on Krita 2.8. With Krita 2.4, Krita could be used by professional artists, with Krita 2.8, artists all over the world started taking notice and with 2.9, well -- we’ll make Krita irresistible!

Help us spread the word and make this <a href="http://krita.org/kickstarter.php">campaign</a> a big success!