---
title: "Akademy Award Winners 2014"
date:    2014-09-08
authors:
  - "jriddell"
slug:    akademy-award-winners-2014
---
The talks weekend at Akademy finished with the traditional announcing of the Akademy Awards, our recognition of the stars of KDE.  The winners are selected by those who received the award the previous year.

Winners for 2014 are:
<table style="border:1px solid #ffffff ;" border="0" cellspacing="10" cellpadding="10" width="700">
   <col width="75">
   <col width="625">
<tr><td align="right">
<img src="/sites/dot.kde.org/files/kdenlive_icon.png" width="128" height="128" />
</td><td>
<b>Application Award: Jean-Baptiste Mardelle</b> for his work on Kdenlive, the leading video editing application which is now part of KDE.
</td></tr><tr><td align="right">
<img src="/sites/dot.kde.org/files/michael-pyne.jpg#overlay-context=2014/09/08/akademy-award-winners-2014" width="200" height="200" />
</td><td>
<b>Non-application Award: Michael Pyne</b> for eleven years of kdesrc-build and its previous incarnations kdecvs-build and kdesvn-build which makes it easy to compile all of KDE Software.
</td></tr><tr><td align="right">
<img src="/sites/dot.kde.org/files/bcooksley-pic.jpg" width="250" height="187" />
</td><td>
<b>Jury Award: Ben Cooksley</b> for planning our systems and keeping them running smoothly. 
</td></tr><tr><td align="right">
<img src="/sites/dot.kde.org/files/claudia-rauch.jpg#overlay-context=2014/09/08/akademy-award-winners-2014" width="140" height="140" />
<img src="/sites/dot.kde.org/files/dan-vradil.jpg" width="160" height="160" />
</td><td>
And two special thank yous went to <b>Claudia Rauch</b> for making sure we were successfully organized, plus <b>Daniel Vratil and the Akademy 2014 team</b>.
</td></tr></table>

<div style="text-align: center">
<a href="https://www.flickr.com/photos/jriddell/14991119087" title="DSC_0754 by Jonathan Riddell, on Flickr"><img src="/sites/dot.kde.org/files/14991119087_00dbb7cd25.jpg" width="500" height="281" alt="DSC_0754"><br />Dan accepts the Akademy 2014 organizers' certificate.</a>
</div>
