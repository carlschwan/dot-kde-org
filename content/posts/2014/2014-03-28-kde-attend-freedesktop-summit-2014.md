---
title: "KDE to Attend Freedesktop Summit 2014"
date:    2014-03-28
authors:
  - "jospoortvliet"
slug:    kde-attend-freedesktop-summit-2014
---
Next week, from Monday the 31st of March to the 4th of April (Friday), developers from the major Linux desktops (GNOME, KDE, Unity and RazorQt) will meet in Nuremberg for the second <a href="http://www.freedesktop.org/">Freedesktop</a> Summit.

The summit is a joint technical meeting of developers working on 'desktop infrastructure' on the major Free Desktop projects. The event aims to support collaboration between projects by discussing specifications and the sharing of platform-level components.  David Faure will be KDE's primary representative at this year's summit.

<a href="http://dot.kde.org/2013/04/17/report-freedesktop-summit">Last year's event</a> led to agreements related to D-Bus and management of trash folders. It also mapped the way forward for joint development and management of specifications that are important to multiple providers of desktop software.

Like last year, the event is supported by SUSE.