---
title: "Meet Cornelius Schumacher - Akademy Keynote Speaker"
date:    2014-08-27
authors:
  - "kallecarl"
slug:    meet-cornelius-schumacher-akademy-keynote-speaker
comments:
  - subject: "PCLinusOS KDE"
    date: 2014-08-29
    body: " Dear Sir.\r\n   I am new to the Linux world and I am not a programer, so there is a lot that I don't know. I have the kaddressbook and was looking at it and I thought why isn't there a way to save on usb drive, or a way of storing information, what if a computer crashes and can not get the information off the hard drive, I think there should be.\r\n\r\nsave    and save as  to meet the needs of the user.\r\n\r\n  I would also like the see a recipe book for those who like to cook, be on the bases of 3 by 5 cards, and a place for instructions and again a place to save   and save as and a choice of using usb drive. \r\n\r\n  Thank You for your time \r\n   Ralph Trout           rtks1947@gmail.com      or     rtks1947@yahoo.com \r\n\r\n\r\n"
    author: "Ralph Trout"
  - subject: "Possible Meaning of Akonadi and Nepomuk"
    date: 2014-09-02
    body: "I may be wrong, but I suspect that akonadi and nepomuk are the sort of programmes that you will need to use to record/export this kind of database content. Can someone back me up on this ?\r\n"
    author: "Darren Drapkin"
  - subject: "KAddressbook has an export"
    date: 2014-09-08
    body: "KAddressbook has an export function, that is all the parent poster needs to save his/her data.\r\n\r\nI'm not aware of a cooking application, but perhaps there is one on <a href=\"http://kde-apps.org\">kde-apps.org</a> ;-)"
    author: "jospoortvliet"
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/CorneliusInterview.png" /><br /><div align="center">Cornelius Schumacher<br /><small><small>photo by Michal Kubeček</small></small></div></div>
At <a href="https://akademy.kde.org/2014">Akademy 2014</a>, outgoing KDE e.V. Board President Cornelius Schumacher will give the <a href="https://dot.kde.org/2014/07/30/akademy-2014-keynotes-sascha-meinrath-and-cornelius-schumacher">community keynote</a>. He has attended every Akademy and has been amazed and inspired at every one of them. If you want more of what KDE can bring to your life, Cornelius's talk is the perfect elixir.
<!--break-->

Here are glimpses of Cornelius that most of us have never seen. They give a sense of what has made him a successful leader of KDE for several years.

<big><strong>Behind the KDE scene</big></strong>
<em>Food?</em>
I like eating, and I like cooking. I'm a recipe type of cook, so I have a large collection of cook books. At the moment <a href="http://www.jamieoliver.com/recipes">Jamie Oliver</a> is one of the favorites in my family. Especially his 30 minute menus fascinate me in terms of well-thought-through procedures. Sometimes I feel the urge to revolutionize the way recipes are presented, though. Especially these kind of sophisticated procedures such as the 30 minutes menus deserve a more accurate and consumable way of being presented, don't they? I see something like flow diagrams in my mind. But that's another project at another time...

<em>Favorite beer?</em>
I'm glad that you asked. I feel lucky to live in a region where the variety and quality of beer is so fabulous that there really is no excuse for drinking bad beer. One of my favorites is <a href="http://www.kuchlbauers-bierwelt.de/Brewery.26.0.html?&lang=en">Kuchlbauer Weißbier</a>. It's one of the tastiest beers I know, and the brewery is a piece of art in itself. They have a <a href="http://www.kuchlbauers-bierwelt.de/THE-KUCHLBAUER-TOWER.28.0.html?&lang=en">tower dedicated to the idea of beer done as a project with the famous artist Friedensreich Hundertwasser</a>. There is an installation of beer dwarves in their cellar, as well as a full-size reproduction of Leonardo da Vinci's "Last Supper", along with some interpretation by the founder of the brewery himself. Kind of crazy, but absolutely worth a visit.

<em>Who are you when you are not at work?</em>
The energy from food and beer I don't use for my job or KDE, I spend on one of my bicycles. My career as bicycle racer started and ended with the one race I did when I was sixteen. But I still enjoy going by bike whenever I can. My rough estimate is that I have done something like 100,000 km in total by bike in my life up to now.

<big><strong>What's your tech setup?</big></strong>
I'm standing on the shoulders of giants. I have a stack of computers and devices which have accumulated over the years; some feel gigantic today. What I actually use these days is my current desktop—which is optimized to be silent, and a small laptop I mostly use for traveling. All my computers run openSUSE, which I discovered when it was delivered on floppy disks, and which is still one of the best systems out there.

Recently an Android tablet has sneaked into my life. It is a great device for some things. I only wish there would be more KDE software on it.

<big><strong>Why should someone attend Akademy?</big></strong>
Akademy is the place to be to see the KDE community in action. It's always so amazing to see the high level of community KDE operates on. This hasn't changed at all over the years. I have attended all Akademies and I haven't experienced a single one which didn't amaze me.

The level of energy is incredibly high, and the common passion for writing free software brings together such a diverse group of people. It is an example of what can be achieved by bringing together the right factors of motivation, people who are driven by a common idea, the environment, which allows these people to get stuff done.

Being at Akademy always feels like there are no limits to what a person can do.

<big><strong>Why should someone attend your talk?</big></strong>
I will tell how to become a better person through KDE. KDE is a tremendously supportive environment for growth, and I think we sometimes don't recognize or value that enough. It is worth having a closer look at what happens there and why KDE is such a supportive environment.

I will also tell parts of my personal story, how KDE altered my life. I have been around for quite a while, and I have seen many things that illustrate how KDE facilitates growth. And I do have some embarrassing photos from the past, which work very well to prove this point.

<big><strong>What do you see as the most important issues for free and open technology over the next few years?</big></strong>
There are so many and such strong interests in computing from so many sides today that it really is a challenge to maintain the sovereignty and freedom of the individual there. The only way to prevent abuse of technology and harmful consequences of thoughtless use is to put a strong foundation of values under it and create examples and implementations of how to do things in the right way to protect people. Free software does both of those, so we have to make sure it continues to deliver on them.

<big><strong>What is distinctive and important about FOSS and about KDE in particular?</big></strong>
FOSS provides an environment which is tailored to stimulate the best that we can do with technology. It uses the right mechanisms to bring to the surface what good people can do. KDE has cultivated that to an amazing degree. It shows that we have done that for many, many years, and that we have learned one or two lessons about what works and what doesn't.

<big><strong>Torvalds or Stallman?</big></strong>
I respect and value both of them. Richard for the clarity of thought and the strong vision he provides, Linus for his technical brilliance. If I had to choose, I would choose Richard, though. While I wouldn't want to live without Linux or Git, I do think that technology in itself is not sufficient. It does have to serve a purpose.

<h2>Akademy 2014 Brno</h2>
For most of the year, KDE—one of the largest FOSS communities in the  world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2014">Akademy</a> provides all KDE contributors the opportunity to meet in person to  foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and other types of KDE  contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work to bring those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

If you are someone who wants to make a difference with technology, <a href="https://akademy.kde.org/2014">Akademy 2014</a> in Brno, Czech Republic is the place to be.