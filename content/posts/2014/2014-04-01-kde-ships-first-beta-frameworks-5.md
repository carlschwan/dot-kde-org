---
title: "KDE Ships First Beta of Frameworks 5"
date:    2014-04-01
authors:
  - "jospoortvliet"
slug:    kde-ships-first-beta-frameworks-5
comments:
  - subject: "Naming"
    date: 2014-05-18
    body: "I'm really excited  about Framework 5, I just have one question fo you: Who had the idea to name the 'KDE-aids' after a really bad disease? 'Helpers' would be a much smarter name.\r\nJ."
    author: "Jean Horten"
---
<div style="float: right; padding: 1ex; margin: 1ex; width: 300px; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" /></div>Today KDE makes available the first beta of Frameworks 5. This release is part of a series of releases leading up to the final version planned for June 2014 following the <a href="http://dot.kde.org/2014/02/14/kde-frameworks-5-alpha-two-out">second alpha</a> last month. This release marks the freeze of source incompatible changes and the introduction of the Frameworks 5 Porting Aids.

<h2>Frameworks 5 Porting Aids</h2>
To ease the porting of KDE Platform 4 based applications, the Frameworks team has brought the 'Porting Aids' group into existence. These Frameworks contain kdelibs4 modules and API's that are being deprecated in KF5 and are provided only to assist applications in porting to KF5. As such these Frameworks will only have a limited support period, currently planned to be three release cycles.  Application developers are strongly encouraged to port away from these Frameworks during this support period to prevent dependency on obsolete and unsupported code.  Once support is ended, some unofficial development may continue on some modules, but they will not be part of the officially supported Frameworks release.

Currently, the following Frameworks belong to this group:
<ul>
<li>khtml</li>
<li>kjs</li>
<li>kjsembed</li>
<li>krunner</li>
<li>kmediaplayer</li>
<li>kdelibs4support*</li>
</ul>

* kdelibs4support contains deprecated API's from modules which no longer exist or deprecated classes from existing modules.

See <a href="http://www.kde.org/announcements/announce-frameworks5-beta1.php">the  announcement on kde.org</a> for more information and links to  downloads. For information about Frameworks 5, see <a href="http://dot.kde.org/2013/09/25/frameworks-5">this earlier article on the dot</a>.
