---
title: "KDE Commit-Digest for 8th June 2014"
date:    2014-11-20
authors:
  - "mrybczyn"
slug:    kde-commit-digest-8th-june-2014
---
In <a href="http://commit-digest.org/issues/2014-06-08/">this week's KDE Commit-Digest</a>:

<ul><li>Calligra adds <a href="http://okular.org/">Okular</a> generator for DOC/DOCX, based on the one for ODT</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> supports polygon moving, node deletion and adding polygon holes</li>
<li><a href="http://solid.kde.org/">Solid</a> offers more detailed battery status information</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a> plugin controller architecture gains important optimizations</li>
<li>Kdelibs sees config file parsing optimization thanks to reduced number of memory allocations</li>
<li><a href="https://krita.org/">Krita</a> adds a Scalable Distance option to Weighted Smoothing</li>
<li>In KDE Frameworks, <a href="http://stepcore.sourceforge.net/">Step</a> has been ported to Qt5/KF5</li>
</ul>

<a href="http://commit-digest.org/issues/2014-06-08/">Read the rest of the Digest here</a>.