---
title: "KDE Applications 14.12 - New Features, Frameworks Ports"
date:    2014-12-17
authors:
  - "unormal"
slug:    kde-applications-1412-new-features-frameworks-ports
---
Today KDE released KDE Applications 14.12, delivering new features and bug fixes to more than a hundred applications. Most of these applications are based on KDE Development Platform 4 but the first applications have been ported to <a href="https://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a>. Frameworks is a set of modularized libraries providing additional functionality for Qt5, the latest version of the popular <a href="http://qt-project.org/">Qt cross-platform application framework</a>.

<div style="width: 360px; float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /><br /><div align="center">KDE app dragons</div></div>

<p>This release marks the beginning of a new style of releases replacing the threesome of KDE Workspaces, Platform and Applications in the 4 series which ended with the latest <a href="https://www.kde.org/announcements/announce-4.14.3.php">KDE Applications update last month</a>.</p>

<ul>
<li> The Platform has now morphed into KDE Frameworks 5, releasing new features and bugfixes monthly.</li>
<li> The Workspaces have become Plasma 5, delivering feature releases every 3 months and monthly bug fixes in between.</li>
<li> The remaining applications and supporting libraries make up KDE Applications with feature releases expected every 4 months and monthly bugfix releases in between. We expect the porting of applications to Qt 5 and Frameworks 5 to happen over the next year or two.</li>
<li> The KDE software on independent release cycles will remain as is, with porting to Frameworks 5 and Qt5 coming at various points in time. Examples include many well known open source applications like <a href="http://www.kdenlive.org">Kdenlive</a>, <a href="https://community.kde.org/Incubator/Projects/Rkward">Calligra</a>, <a href="http://amarok.kde.org">Amarok</a>, <a href="https://www.kde.org/applications/office/kile/">Kile</a>, <a href="http://www.tellico-project.org">Tellico</a> and some new projects like <a href="https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser">GCompris</a> and <a href="https://community.kde.org/Incubator/Projects/Rkward">Rkward</a>.</li>
</ul>

<!--break-->
The release includes the first KDE Frameworks 5-based versions of <a href="http://www.kate-editor.org">Kate</a> and <a href="https://www.kde.org/applications/utilities/kwrite/">KWrite</a>, <a href="https://www.kde.org/applications/system/konsole/">Konsole</a>, <a href="https://www.kde.org/applications/graphics/gwenview/">Gwenview</a>, <a href="http://edu.kde.org/kalgebra">KAlgebra</a>, <a href="http://edu.kde.org/kanagram">Kanagram</a>, <a href="http://edu.kde.org/khangman">KHangman</a>, <a href="http://edu.kde.org/kig">Kig</a>, <a href="http://edu.kde.org/parley">Parley</a>, <a href="https://www.kde.org/applications/development/kapptemplate/">KApptemplate</a> and <a href="https://www.kde.org/applications/utilities/okteta/">Okteta</a>. Some libraries are also ready for KDE Frameworks 5 use: analitza and libkeduvocdocument.

<a href="http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html">Libkface</a> is new in this release; it is a library to enable face detection and face recognition in photographs.
    
The <a href="http://kontact.kde.org">Kontact Suite</a> is now in Long Term Support in the 4.14 version while developers are focusing on a port to KDE Frameworks 5 with renewed energy.

Some of the new features in this release include:
    <ul>
    <li><a href="http://edu.kde.org/kalgebra">KAlgebra</a> has a new <a href="https://play.google.com/store/apps/details?id=org.kde.kalgebramobile&hl=en">Android version</a> thanks to KDE Frameworks 5 and is now able to <a href="http://www.proli.net/2014/09/18/touching-mathematics/">print its graphs in 3D</a></li>
    <li><a href="http://edu.kde.org/kgeography">KGeography</a> has a new map for <a href="http://en.wikipedia.org/wiki/Bihar">Bihar</a>.</li> 
    <li>The document viewer <a href="http://okular.kde.org">Okular</a> now has support for latex-synctex reverse searching in dvi and some small improvements in the ePub support.</li> 
    <li><a href="http://umbrello.kde.org">Umbrello</a>—the KDE UML modeller—has many new features too numerous to list here.</li>
</ul>

See the <a href="https://www.kde.org/announcements/fulllog_applications-14.12.0.php">full list of changes</a> in KDE Applications 14.12.

The April release of KDE Applications 15.04 will include more new features, as well as more applications ported to the modular KDE Frameworks 5.

<h2>Spread the Word</h2>

Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets, KDE depends on people like you talking with other people! Even for those who are not software developers, there are many ways to support our community and our product. Report bugs. Encourage others to join the KDE Community. Or <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5">support the nonprofit organization behind the KDE community</a>.

Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 14.12 KDE Applications release.
Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from Twitter, YouTube, flickr, PicasaWeb, blogs, and other social networking sites.

<h2>Learning more and getting started</h2>

Find more details and download links in <a href="https://www.kde.org/announcements/announce-applications-14.12.0.php">the announcement on the KDE website</a>.