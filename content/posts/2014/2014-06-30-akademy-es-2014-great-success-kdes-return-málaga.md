---
title: "Akademy-es 2014: Great success in KDE's return to M\u00e1laga"
date:    2014-06-30
authors:
  - "tsdgeos"
slug:    akademy-es-2014-great-success-kdes-return-málaga
---
The ninth edition of Akademy-es was held last month in Málaga at the Telecommunications School of University of Málaga. Akademy-es had never been held in the city before but it is where the idea of Akademy-es began, during Akademy 2005, resulting in the first Akademy-es in 2006 in Barcelona. KDE old timer Antonio Larrosa is the link between both editions, being the local organizer of Akademy-es 2014 and Akademy 2005.

This year Akademy-es has continued its upwards trend in people registered, ending up with around 100 people. Talks as always have been varied, including philosophical talks about what KDE is, technical ones about how to use ASAN to debug your apps, practical ones on how to make your computer and your [Android] phone work better together,  some programming with an introductory QtQuick talk (in English!), and much more.

<a href="http://www.kde-espana.org/akademy-es2014/fotogrupo.html"><img src="http://www.kde-espana.org/akademy-es2014/fotogrupo.jpg"></a>

Besides the serious talks there was always time for some socializing, an important part these kind of conferences. 
Specially interesting was Saturday dinner at <a href="http://www.lainvisible.net/">La Casa Invisible</a>  where we met a group of people that like KDE is investing lots of time in helping society for the greater good.

Finally, please join KDE España in thanking <a href="http://www.kde-espana.org/akademy-es2014/patrocinadores.php">our sponsors Digia, Opentia, openSUSE and Wabobo</a> for helping make Akademy-es 2014 possible.