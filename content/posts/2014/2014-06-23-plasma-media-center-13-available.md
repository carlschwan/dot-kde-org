---
title: "Plasma Media Center 1.3 Available"
date:    2014-06-23
authors:
  - "shantanu"
slug:    plasma-media-center-13-available
comments:
  - subject: "Congrats"
    date: 2014-06-25
    body: "Aa always, congrats to the team for this great release and all of the hard work that goes into it! Looking forward to getting this installed in Kubuntu ASAP. "
    author: "James Cain"
  - subject: "Thanks, looking forward to"
    date: 2014-07-09
    body: "Thanks, looking forward to DVB-T support. It seems just a matter of time before Kaffeine disappears... :("
    author: "Anonymous"
---
Plasma Media Center 1.2 <a href="http://dot.kde.org/2013/12/20/plasma-media-center-12-released-time-christmas">was released as a Christmas gift</a>. Now, we bring to you Plasma Media Center 1.3! As always, we have made sure to make it easier to enjoy your favorite videos, music and photos - both from your collection as well as online sources. A big focus has been performance improvements when showing you the media on your computer as well as general polish for the UI.


<div style="width: 704px; float: center; padding: 1ex; margin: 1ex; border: opx solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PMCInAction.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PMCInAction_wee.jpg" /></a><br />Plasma Media Center in action</div> 

<h2>What is new?</h2>
<ul><li>Support for fetching media from <a href="http://vhanda.in/blog/2014/04/desktop-search-configuration/">Baloo</a><br />
Plasma Media Center now supports fetching your media collection from the new KDE Semantic Search - Baloo.
This means your media library loads faster than ever. However, PMC continues to support Nepomuk if you have KDE libraries installed.</li>
<li>MPRIS support<br />
This lets you use any sort of controller that supports MPRIS to control PMC - this ranges from taskbar previews, Now Playing plasmoid and many more!</li>
<li><em>(Experimental)</em> support for a simple filesystem based media scanner<br />
For when you don't have Nepomuk or Baloo installed</li>
<li>More details in All Music mode<br />
We now show more details about media in the All Music mode, like artist, album and duration.</li>
<li>Improved GStreamer compatibility<br />
A conflict between GStreamer0.10 and GStreamer1.0 in some distros will no longer break playback (<a href="https://bugs.launchpad.net/ubuntu/+source/plasma-mediacenter/+bug/1310077">Launchpad Bug</a>)
<li>We now have tests for the PMC core libs<br />
This will lead to less bugs in the future!</li>
<li>Numerous bug fixes and UI polish</li>
</ul>

<div style="width: 354px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/MPRIS%20support.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/MPRIS%20support_wee.png" /></a><br />MPRIS support allows deep desktop integration</div> 
<h2>Installing PMC 1.3</h2>
Here is the <a href="http://download.kde.org/stable/plasma-mediacenter/1.3.0/src/plasma-mediacenter-1.3.0.tar.bz2.mirrorlist">source tarball link</a>.

Follow instructions <a href="http://www.sinny.in/pmcinstall">here</a> to install Plasma Media Center on your machine from source. For binary packages, check with your distro if they have. If you're a packager (or know someone who is), we will be glad to help if you have any questions.

<h2>Learning more and contributing</h2>
To know more about Plasma Media Center, check out <a href="http://community.kde.org/Plasma/Plasma_Media_Center">the wiki</a>. If you want to contribute, <a href="http://community.kde.org/Plasma/Plasma_Media_Center/getting_started_with_PMC">this will get you started</a>.

<h2>Bugs</h2>
Found any bug in PMC or want to have your favourite feature included in future release? <a href="https://bugs.kde.org/enter_bug.cgi?product=plasma-mediacenter&format=guided">File a bug</a>!

<h2>Coming up</h2>
While this release was more focused on switching desktop search and UI polish, we have some exciting features lined up for the next release. These include:
<ul>
<li>Plasma Media Center for Plasma Next - your Plasma desktop will magically transform into a Media Center (<a href="https://bhush9.github.io/2014/06/09/gsoc-report-2-plasma-mediacenter-functional-mode-on/">current status</a>)</li>
<li>DVB support (i.e you can watch TV in PMC if you have DVB at your home) (<a href="http://www.nikhatzi.gr/?p=23">current status</a>)</li>
<li>Voice recognition support - so you say it, we play it! (<a href="https://ashishmadeti.github.io/gsoc/kde/2014/06/13/simon-mpris-plugin.html">current status</a>)</li>
<li>And some more surprises ;).</li>
</ul>

Thanks to all the developers, testers and people for useful feedback on improving Plasma Media Center. And of course a thanks to all our users: we hope you enjoy the new release!