---
title: "KDE at LISA 2014 - Seattle, November 12 and 13"
date:    2014-11-05
authors:
  - "kallecarl"
slug:    kde-lisa-2014-seattle-november-12-and-13
comments:
  - subject: "Diving into Plasma"
    date: 2014-11-05
    body: "For readers who want more detail about Plasma status ...\r\nSebastian K\u00fcgler's blog post <a href=\"http://vizzzion.org/blog/2014/11/diving-into-plasmas-2015/\">Diving into Plasma 2015</a>\r\n\r\nI have been using KDE Frameworks and Plasma 5 for a couple of months. It's amazing to me how graceful and transparent both of these major transitions have been...especially when compared to my first hand experiences of Microsoft's Windows 7 to Windows 8 fiasco. \r\n\r\nPlease stop by our space at the conference to take the new KDE technology for a test drive."
    author: "kallecarl"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/USENIX_KDE.png" /></div>

There will be a <a href="https://www.usenix.org/conference/lisa14/lisa-expo">KDE exhibit</a> at the upcoming <a href="http://www.usenix.org/lisa14/kde">LISA</a> (Large Installation System Administration) Conference. The full conference takes place November 9 ‒ 14 in Seattle; the expo is open on the 12th and 13th. There is no charge to attend the expo. 

Several members of the KDE Community will be in the booth—presenting various aspects of KDE; answering questions; demonstrating applications (thanks especially to <a href="https://krita.org/">Krita</a> and <a href="http://ogbog.net/">ogbog</a>); recruiting contributors, users, companies and sponsors. All members of the KDE Community are welcome to visit, to jump in & represent KDE, or to just make contact with other KDE people. These small regional gatherings are necessary until we are financially self-sustaining enough to justify a national gathering such as <a href="https://akademy.kde.org/">Akademy</a>. The <a href="http://www.meetup.com/KDE-Users-Seattle">Seattle KDE group</a> is off to a great start.

This is also an opportunity for people who are curious, or interested in what the KDE Community is doing. Our governance, separation between development and administration, and strong mentoring programs are the foundation for an effective international community that is resilient and innovative. Just in the past few years, KDE developers have built a new development platform (<a href="https://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5)</a>, a fully redesigned desktop environment (<a href="https://dot.kde.org/2014/07/15/plasma-5.0">Plasma 5</a>) and a modern look-and-feel (<a href="http://arstechnica.com/information-technology/2014/08/kde-plasma-5-for-those-linux-users-undecided-on-the-kernels-future/2/">Breeze</a>)—demonstrating KDE's value to the broad technology industry.

The LISA conference has long served as the annual vendor-neutral meeting place for the wider system administration community. Recognizing the overlap and differences between traditional and modern IT operations and engineering, the highly-curated 6-day program offers training, workshops, invited talks, panels, paper presentations, and networking opportunities around 5 key topics: Systems Engineering, Security, Culture, DevOps, and Monitoring/Metrics. Don't miss the chance to be a part of this unique career-building journey.

Many thanks to <a href="http://www.usenix.org/lisa14/kde">USENIX</a> for the generous support of KDE.