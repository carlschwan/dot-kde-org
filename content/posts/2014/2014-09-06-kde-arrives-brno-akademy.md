---
title: "KDE Arrives in Brno for Akademy"
date:    2014-09-06
authors:
  - "jriddell"
slug:    kde-arrives-brno-akademy
---
<a href="https://akademy.kde.org/2014"><img src="/sites/dot.kde.org/files/banner02.png" width="950" height="114" /></a>

Yesterday KDE contributors from around the world arrived in Brno for <a href="https://akademy.kde.org/2014">Akademy</a>, our annual meeting.  Over the next week, we will share ideas, discover common problems and their solutions, and rekindle offline friendships for another year.  We have traveled from around the world to work on free software in the spirit of community and cooperation.  This year we can celebrate the success of the last 12 months when we released major new versions of our platform—KDE Frameworks—and our desktop—Plasma 5.  This work has been well received by the press and our community of users, but we know there is much more to do to keep KDE Software relevant for the years to come in a world where desktops are only one way of using computer software.  We'll be discussing and planning how to make the best desktop software for Linux and how to expand to new platforms.
<!--break-->

<h2>Annual General Meeting</h2>
Many attendees arrived a day early to attend the Annual General Meeting of KDE e.V., the association that represents the Community in legal and financial matters.  Of the five board members, three positions were open for election. Marta Rybczyńska was elected on a ticket of wanting to be treasurer, Aleix Pol was elected as a new member, and we re-elected Lydia Pintscher who has been chosen by the Board to be President.  Many thanks to our new Board, and to Cornelius, the outgoing president, for 6 years of work on one of the more important but bureaucratic jobs needed to keep our project functional.

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex;">
<a href="https://www.flickr.com/photos/jriddell/15127975756" title="DSC 0694 by Jonathan Riddell, on Flickr"><img src="https://farm6.staticflickr.com/5557/15127975756_04f45f867c.jpg" width="281" height="500" alt="DSC 0694"></a>
</div>

<h2>Evening get together</h2>
In the evening, the opening party was held at the shiny new Red Hat office on the outskirts of Brno.  We enjoyed fine Czech food and sampled the local beer.  

In the words of Adriaan de Groot "<i>RedHat was very welcoming, and a good mix of people showed up. Awkward-introductions-circle pulled in lots of new people and Marta shared at least one of her dark secrets (she's a pilot!). Peter Grasch held forth on linguistic models and we tried to collect some Czech pronunciations</i>".  KDE Korea lead Park Shinjo enjoyed the novel local brew "<i>Kofola, although it sounds like cola, it's not. It has some herbal tastes, so it's unique</i>".

<h2>Join us</h2>
Follow us over the week on ...
<a href="http://planet.kde.org"><img src="http://kde.org/stuff/clipart/klogo-official-oxygen.png" width="32" height="32" /></a> <a href="http://planet.kde.org">blogs on Planet KDE</a>, 
<a href="https://twitter.com/search?q=%23akademy"><img src="http://wiki.ubuntu.com/UtopicUnicorn/Beta1/Kubuntu?action=AttachFile&do=get&target=icon-twitter.png"/></a> <a href="https://twitter.com/search?q=%23akademy">the #akademy tag on Twitter</a> or on the 
<a href="https://www.facebook.com/kde"><img src="http://wiki.ubuntu.com/UtopicUnicorn/Beta1/Kubuntu?action=AttachFile&do=get&target=facebook_icon_30x30.gif"/></a> <a href="https://www.facebook.com/kde">KDE Facebook page</a> or the 
<a href="https://plus.google.com/u/0/b/105126786256705328374/105126786256705328374/posts"><img src="http://wiki.ubuntu.com/UtopicUnicorn/Beta1/Kubuntu?action=AttachFile&do=get&target=googleplus.png"/></a> <a href="https://plus.google.com/u/0/b/105126786256705328374/105126786256705328374/posts">KDE Community page on Google+</a>.

Communicate with people at Akademy by asking questions and commenting below. 

<h2>Akademy 2014 Brno</h2>
For most of the year, KDE—one of the largest FOSS communities in the  world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2014">Akademy</a> provides all KDE contributors the opportunity to meet in person to  foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and other types of KDE  contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work to bring those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

If you are someone who wants to make a difference with technology, <a href="https://akademy.kde.org/2014">Akademy 2014</a> in Brno, Czech Republic is the place to be. It's not too late to join us at Akademy.