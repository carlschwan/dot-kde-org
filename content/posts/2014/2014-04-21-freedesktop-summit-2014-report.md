---
title: "Freedesktop Summit 2014 Report"
date:    2014-04-21
authors:
  - "dfaure"
slug:    freedesktop-summit-2014-report
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/hardatwork.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/hardatwork_wee.jpg" /></a><br />The team in action</div>From March 31 to April 4, Free Software desktop hackers from many of the largest desktop projects (including GNOME, KDE, Unity and LXDE-Qt) met to collaborate on specifications and tools to improve application interoperability between the desktops.  Clarified standards are expected not only to improve the experience of running applications designed for one desktop inside of another, but also to provide a clearer picture of what is required from third party application developers approaching the Free Software desktop for the first time.

This was the second time the annual event occurred.  Both times, it was sponsored and hosted by SUSE at their offices in Nuremberg.

<h2>Results</h2>
The meeting accomplished a standardization of the XDG_CURRENT_DESKTOP environment variable for allowing applications to know in which desktop environment they are running.  The meeting also produced the first formal specification of how applications should be associated with given mime types and URI schemes, including how to select the default application in a way appropriate to the current desktop environment, respecting the choices of the OS vendor and the local system administrator as well as the user.
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/davidinaction.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/davidinaction_wee.jpg" /></a><br />Our very own David in action</div>
Small progress was made on a simple common inhibit specification that applications can use to prevent events such as locking of the screen or powering down of the network interfaces, but with improvements on fine-grained power control capabilities on forthcoming hardware devices, additional research (and perhaps time) is required before a complete specification can be produced.

The meeting also produced an agreement on the future of startup notification in the Wayland world.  A protocol based on broadcast of D-Bus signals will be used instead of the current approach with X client messages.  This approach is expected to integrate nicely with future frameworks for sandboxed applications.  Improvements were also made to the protocol to allow for tab-based applications that make dynamic choices about creating a new tab or a new window depending on the workspace in which a document was opened.
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/latenightconversation.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/latenightconversation_wee.jpg" /></a><br />A late night conversation</div>
The introduction of the long-awaited "Implements=" key in desktop files was also finalized.  This is used to express support of a given interface by an application.  Among other things, this is expected to be used to advertise supporters of the provider side in a future "portals" system for exchanging data between sandboxed applications.

Also discussed was the possibility of defining a "resource base" for desktop files so that application resources such as icons can be accessed without being merged into the 'hicolor' icon theme.  The lack of certainty over how future sandboxing approaches would deal with this situation prevented any progress on this point.

There were significant cleanups of the xdg specifications (and their build system) and to deal with bug backlog on some freedesktop components such as the shared-mime-info database.
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/havingabeer.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/havingabeer_wee.jpg" /></a><br />Team enjoying a beer</div>
The meeting was also used to discuss details of the API of the forthcoming memfd interface in the kernel that will be used to support efficient sending of very large kdbus messages.  kdbus and GVariant were also discussed, as were the desktop file index and systemd support for time-based activation of applications that want to exit when idle (such as alarm clocks).  The possibility of standardizing the new notification interface based on D-Bus activatable applications was also briefly discussed.

After the meeting, work continues on publishing updated specifications and writing implementations.  The meeting is expected to happen again, in 2015.

<em>Written by Ryan Lortie from glib/gnome</em>
<em>Pictures by <a href="https://plus.google.com/+JeromeLeclanche">Jerome Lechlanche</a></em>