---
title: "KDE Ships First Beta of Applications and Platform 4.14"
date:    2014-07-10
authors:
  - "unormal"
slug:    kde-ships-first-beta-applications-and-platform-414
---
KDE has released the first beta of the 4.14 versions of Applications and Development Platform. With API, dependency and feature freezes in place,  the focus is now on fixing bugs and further polishing. Your <a href="http://dot.kde.org/2014/03/12/applications-413-coming-soon-help-us-test">assistance is requested</a>!

A more detailed list of the improvements and changes will be available for the final release in the middle of August.

This first beta release needs a thorough testing in order to improve quality and user experience. A variety of actual users is essential to  maintaining high KDE quality, because developers cannot possibly test  every configuration. User assistance helps find bugs early so they can  be squashed before the final release. Please join the 4.14 team's release effort by installing the beta and <a href="http://bugs.kde.org">reporting any bugs</a>. The <a href="http://www.kde.org/announcements/announce-4.14-beta1.php">official announcement</a> has information about how to install the betas.