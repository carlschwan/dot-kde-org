---
title: "Franklin Weng: The strength behind open source is the strength of contributing"
date:    2014-11-10
authors:
  - "martgnz"
slug:    franklin-weng-strength-behind-open-source
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img width="320" height="480" src="/sites/dot.kde.org/files/franklin.jpg"></img><br /></div>
<p><strong>This is the first part of KDE &amp; Freedom, a series of interviews with people who use and contribute to FOSS in their everyday lives. Please consider donating to the <a href="https://www.kde.org/fundraisers/yearend2014/">KDE End of Year 2014 Fundraiser</a>. We need your help!</strong></p>

<p>Franklin is a 39 year old FOSS activist based in Taipei. He has coordinated KDE&#39;s zh_TW translation team since 2006, and is the core developer of <a href="http://ezgo.westart.tw/">ezgo</a> (Chinese), a compilation of educational software used by schools all over Taiwan. ezgo, which in its Linux installation uses KDE software by default, blends more than 100 free software applications into one localized, easy to use package. [More information in a <a href="https://dot.kde.org/2013/10/02/ezgo-free-and-open-source-software-taiwans-schools">previous Dot article</a>.]</p>

<p>Exchanging emails led to a voice conversation between The Dot and Franklin.</p>

<p><strong>What is your motivation behind computer freedom?</strong></p>
<p>Many people asked me the same question.  My simple answer is because I like to be free.</p>
<p>Before the year 2000, when we wanted to install and run an open source application, we would download the code, then there would be a file called &quot;INSTALL&quot; telling us how to compile and install the application. If we followed all the steps there would be a 95% chance that the compilation will fail. But then we would still have clues to find out what the problem was. We didn&#39;t need to call the vendor and ask why it&#39;s not working and have him ignore us. I like the feeling of finding the answer, no matter if it is by myself or by discussing with others on the Internet.</p>
<p>In Taiwan there were many excellent people working on the i18n [internationalization] framework, which made localization a lot easier. I appreciated their contributions very much, and that&#39;s also what drove me to contribute more into the open source world.</p>

<p><strong>What do you think of the situation of free software in Taiwan?</strong></p>
<p>In my opinion it is far behind where it should be.</p>

<p><strong>Why?</strong></p>
<p>There are people working, contributing and promoting FOSS in Taiwan.  But we still are under the control of many big international software vendors like Microsoft or Adobe.</p>
<p>All these years we tried to let people see the value of free software as a public resource, but people have gotten used to commercial software. One of the reasons, I think, is that there was a lot of software piracy several years ago.  Now it is much better, but I think it could be one of the main reasons why we have difficulties promoting open source software.</p>

<p style="font-size: xx-large; font-style: italic; font-family: Georgia,serif; line-height: 40px; border-left: 5px solid rgb(128, 128, 128); padding-left: 10px; margin-left: 10px; margin-top: 20px;">“Contribute without thinking too much and opportunities will come to knock on your door.”</p>

<p><strong>You are the main developer of an educational platform. How can free software improve education?</strong></p>
<p>Contributing. I told teachers and people that the strength behind the world of open source is the strength of contributing.  So many people contribute without asking for any economic feedback.  That&#39;s the most amazing strength in the world, and that should be what teachers should tell our kids while they&#39;re using FOSS.</p>
<p>It would help a lot if teachers could understand that FOSS is actually not only for computer classes, but for all kinds of subjects.  There are so many educational FOSS and educational public domain resources!</p>

<div style="margin-left: auto; margin-right: auto; width: 70%; margin-top: 20px;"><img src="/sites/dot.kde.org/files/ezgo11.png"></img><p>The latest release of ezgo is based on Kubuntu 13.04 and has Linux 3.8.0 in its core.<p/></div>

<p><strong>And why did you start to collaborate on ezgo?</strong></p>
<p>Before 2007 I just translated KDE software and other applications and I sent them upstream. I started to be the coordinator of the KDE zh_TW l10n team in 2006 and then my partner Eric Sun sent me an email. He told me that he was running a project under the Ministry of Education in Taiwan promoting free and open software to elementary and high schools. ezgo it was one of their products, collecting many good FOSS applications in it.  The target was to make it easier for people to understand and use FOSS.</p>

<p><strong>So everything was started by your translation work?</strong></p>
<p>When translating KDE software I didn&#39;t think too much.  I didn&#39;t expect to get any feedback or credit, I didn&#39;t expect to get any chance to be a part of any other communities.</p>
<p>That&#39;s also what I would tell young people.  Contribute without thinking too much and opportunities will come to knock on your door.</p>
My partner told me his thoughts and the value of ezgo. At that time I had two babies and I started to think about what I can do for education using FOSS. I joined his group, and at first I was just a consultant, helping them to solve problems.  From ezgo X, I became the main developer.</p>

<p style="font-size: 25px; font-style: italic; font-family: Georgia,serif; line-height: 40px; border-left: 5px solid rgb(128, 128, 128); padding-left: 10px; margin-left: 10px; margin-top: 20px;">“Open source and public domain are good for education.”</p>

<p><strong>In the beginning, ezgo had GNOME 2 by default, but since ezgo X you have used Plasma. Why?</strong></p>
<p>The main component of ezgo is the menu. It&#39;s categorized and sorted so people can easily understand and find out the applications. It&#39;s very important for us but when GNOME 3 was released it was gone. Then Ubuntu started to use Unity, which didn&#39;t have a menu either. So we had to decide if we should let people use GNOME 3, Unity, or any other desktop environment. Finally we decided to switch to Plasma.  The most important reason was that it kept the normal menu.</p>

<div style="float: right; padding: 1ex; margin: 1ex;"><img width="300" height="415" src="/sites/dot.kde.org/files/ezgo-apps.png"></img><br /><p>ezgo provides a huge selection of FOSS apps.</p></div>

<p><strong>And what has been the response so far?</strong></p>
<p>We have two different reactions. When we are promoting ezgo, we don&#39;t highlight what desktop we are using because we focus on the applications. So most people aren&#39;t aware that they&#39;re using KDE software. It doesn&#39;t matter what OS, window manager or desktop we are using, we are always using the same applications. We collected more than 100 free software applications in ezgo, no matter if KDE apps or not. So for most people, it was okay to switch to KDE software. However, there was another reaction—some people don't like KDE. They think that interfaces like Gnome 3 or Unity would be the correct way since mobile devices are more and more popular. They think that PCs will vanish one day.</p>

<p><strong>What are the next steps for ezgo?</strong></p>
<p>ezgo hasn&#39;t completed its mission.  It&#39;s just like an auxiliary wheel on a bicycle.  We still hope to let more and more people understand and enjoy the world of open source. From the aspect of education, we hope that more schools and teachers understand that open source and public domain are good for education.  They don&#39;t need to rely only on commercial software. We&#39;re not against such software, but we want people to know that they have choices.</p>
<p>ezgo will complete its mission when people don&#39;t need ezgo anymore.</p>

<p><strong>How do you imagine education in Taiwan in 20 years?</strong></p>
<p>Though there are many problems in Taiwan&#39;s education system, there are many teachers and even students working to improve our education from the bottom to the top. I believe that it will be more open and more creative.</p>

<p><strong>And technical education?</strong></p>
<p>Technical education in Taiwan has never been an issue. There are so many teachers and government officers who like to play with new toys!</p>
<p>We hope that in 20 years kids and students will learn how to find resources and solve their problems, no matter if it is a public resource or commercial software. In Taiwan we&#39;re now hot on things like &#39;flip classroom&#39; or &#39;mobile learning&#39;. Some teachers believe that computer classrooms will vanish in a few years, replaced by tablets.</p>
<p>But I don&#39;t agree with them. Tablets are good for collecting information and for some learning.  But there are many basic skills, concepts and fundamental knowledge that they can&#39;t satisfy. I&#39;m not against these devices but I believe that all kinds of computers have their own role.</p>

<a href="https://www.kde.org/fundraisers/yearend2014/"><img src="https://www.kde.org/images/teaser/bannerFundraiser2014.png" width="830" height="300"alt="fundraiser banner" /></a>