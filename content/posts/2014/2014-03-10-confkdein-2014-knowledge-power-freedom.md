---
title: "Conf.kde.in 2014 - Knowledge. Power. Freedom."
date:    2014-03-10
authors:
  - "devaja"
slug:    confkdein-2014-knowledge-power-freedom
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDEIndialogoKDEBlue177.png" /></div><a href="http://conf.kde.in">conf.kde.in 2014</a> was held at <a href="http://www.daiict.ac.in/daiict/index.html">DA-IICT</a> (Dhirubhai Ambani Institute of Information and Communication Technology) in Ghandinagar, India during the weekend of 22nd to 24th February. It was a big mashup of many different cultures with speakers and delegates from Europe, the USA and different parts of India. A platform for the exchange of ideas, and spontaneous discussions about goals and thoughts regarding open source as well as technological advancements. Also how to make paper planes.

<h2>What came before</h2>
Conf.kde.in was first organized in 2011 in Bangalore; last year a KDE India Meetup took place at DA-IICT. Both of those helped bring forth an expanded conf.kde.in 2014. The growing KDE community in India welcomed new, cheerful friends. And the open source community in India welcomed a new generation of stalwarts.
<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Discussion.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/DiscussionWee.jpg" /></a><br />Group discussion</div> 
<h2>Where it was</h2>
Dhirubhai Ambani Institute of Information and Communication Technology (DA-IICT), Gandhinagar is an institution of higher learning located in one of the most thriving technological hubs of western India. It has been fostering young minds in the fields of computer science and information technology for many years and features an active local community. It was the perfect location for conf.kde.in to reach out to more young minds. With the conference at the institutional level, KDE and top talent made a solid connection.

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -231px; width: 464px; border: 1px solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/GroupPhotoConfKDEIn.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/GroupPhotoConfKDEInWee.jpg" /></a><br />Group photo!</div> 

<h2>What it was all about</h2>
conf.kde.in 2014 was a fertile environment for getting people started with open source contribution, telling them about KDE technology and the community, introducing them to various applications, answering questions, and appealing to them to make the switch to open source. There were about 260 attendees for the event.
<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PeterTalking.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PeterTalkingWee.jpg" /></a><br />Peter in action</div> 
<h2>Starting</h2>
<em>The first day</em> - the 21st of February saw the start of the conference with a talk by Pradeepto Bhattacharya (a member of the <a href="http://ev.kde.org/">KDE e.V. Board</a>) on the essence of the KDE Community. That was followed by a Qt hands-on session, with the students experiencing the power of Qt by fiddling with it, rather than just listening and trying to imagine how to use it. Some people couldn't keep up with the pace, but by the end of the day, almost everyone had a fully functioning Linux system running on their laptop and was beginning to explore the power of Open Source. There was a general level of satisfaction with the learning opportunities, no matter the person's starting skill level. People's willingness to help others made a big difference.

<h2>Going deeper</h2>
<em>The second day</em> - on the 22nd of February there was a huge line up of talks - spread out over different realms of open source. The sessions by Sinny Kumari, Chandan Kumar, Samikshan Bairagya, Smit Shah, Shubham Chaudhary were specific to the projects they are working on—Plasma Media Center, Artikulate (the language trainer application), Localization Team Management tool, KDE Multimedia and others. There was also some informal bug solving. The point of these sessions was to introduce the students to various KDE projects, projects that students have worked on previously as a part of the Google Summer of Code, the Season of KDE and other mentoring programs. This helped them understand real life applications of coding techniques and skills, and the value of direction and guidance from mentors. It also showed them how to get started contributing to open source.
<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/Lunch.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/LunchWee.jpg" /></a><br />Lunch!</div> 
The talks by Nikhil Marathe, Vishesh Handa, Siteshwar Vashisht and Shantanu Tushar Jha went deeper into specifics and covered technical details of various KDE applications. They covered topics such as memory and synchronization management with <a href="http://en.wikipedia.org/wiki/Resource_Acquisition_Is_Initialization">RAII</a>, the <a href="http://merproject.org/">Mer Project</a>, <a href="https://community.kde.org/Baloo">Baloo</a> (dealing with meta data and search indexing). These presentations expanded the attendees' horizons and helped them explore advanced issues and technologies. 

The non-technical talks—on various facets of open source and FOSS communities—were given by Kévin Ottens and Jos Poortvliet. They talked about Free and Open Source Software and how its principles operate within the KDE Community. Their presentations emphasized the practical aspects of FOSS on KDE's work and beliefs. Conference participants got a clear view into KDE as an open source organization, further broadening their horizons. 
<iframe width="640" height="360" src="//www.youtube.com/embed/kGori5uIeZg?rel=0" frameborder="0" allowfullscreen></iframe>
On February 23rd, Bhushan Shah, Sayantan Datta, Rishabh Arora, Punit Mehta and Peter Grasch talked about their KDE projects which are (respectively):
<ul>
<li>Plasma Workspace</li>
<li>Digikam - photo editing</li>
<li>KStars - astronomical sky guide</li>
<li>Khipu - mathematical graph plotting</li>
<li>Simon - speech recognition software</li>
</ul>
Students could choose a project and experiment with code, documentation and testing. Of course, everyone had the opportunity to use open source technology and experience its power. Kévin Ottens and Prashant Udupa spoke briefly about specific technologies such as C++11 and Generic Component Framework.
<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/PaperPlane.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PaperPlaneWee.jpg" /></a><br />Paper planes</div> 
<h2>Closing thoughts</h2>
The primary goal of the conference was to encourage people to get involved with open source and to understand its power and its reach. We also wanted to help them get started by teaching them the basics and by getting them to know more about KDE. When the conference was over, it didn't matter how many lines of code anyone could understand or even actually write. If some people were convinced of the magic of open source and of KDE, and are now willing to be contributors to this noble cause even if only slightly, then the event accomplished its aim. Events, speakers and mentors like these add fuel to the fire inside. Students were inspired to reach out and experience the power of free and open source technology. 

Be free. Live KDE.

<em>Editors' note:</em> Also, on the last day, a competition in building paper airplanes took place. No correlation was found between C++ coding skills and the distance airplanes flew.