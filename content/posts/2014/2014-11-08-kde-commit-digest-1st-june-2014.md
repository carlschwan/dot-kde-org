---
title: "KDE Commit-Digest for 1st June 2014 "
date:    2014-11-08
authors:
  - "mrybczyn"
slug:    kde-commit-digest-1st-june-2014
---
In <a href="http://commit-digest.org/issues/2014-06-01/">this week's KDE Commit-Digest</a>:

<ul><li>In <a href="http://www.kdevelop.org/">KDevelop</a> it is now possible to modify many include directories and defined macros at the same time</li>
<li><a href="http://edu.kde.org/kstars/">KStars</a> adds an "eyepiece view" feature to the observation planner; It renders a view of the eyepiece and DSS / other imagery if available, side-by-side</li>
<li><a href="http://www.digikam.org/">Digikam</a> adds a geotagging status for thumbnails when viewing the album</li>
<li>Calligra adds <a href="http://okular.org/">Okular</a> generator for ODT (and OTT) files</li>
<li><a href="http://solid.kde.org/">Solid</a> now has a proper Inhibition object, with start/stop methods</li>
<li>Trojita implements PasswordWatcher for imap access</li>
<li>Akonadi implements direct streaming of part data</li>
</ul>

<a href="http://commit-digest.org/issues/2014-06-01/">Read the rest of the Digest here</a>.