---
title: "KDE Commit-Digest for 11th May 2014"
date:    2014-06-30
authors:
  - "mrybczyn"
slug:    kde-commit-digest-11th-may-2014
---
In <a href="http://commit-digest.org/issues/2014-05-11/">this week's KDE Commit-Digest</a>:

<ul><li>Clang backend of <a href="http://www.kdevelop.org/">KDevelop</a> gains basic implementation of adjust signature assistant</li>
<li>Kscreen KCM has been rewritten; the interface has two parts: a view with monitors that can be dragged around to reposition screens, and a widget-based part, that provides detailed configuration for each screen, like resolution, rotation, etc</li>
<li>The screen locker gets the KCM back too</li>
<li><a href="http://pim.kde.org/akonadi/">Akonadi</a> gets incremental changes for MERGE and tag support for MERGE and APPEND</li>
<li><a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a> supports SQLCipher database driver</li>
<li>Plasma Media Center prioritizes photos taken by a camera-like device.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-05-11/">Read the rest of the Digest here</a>.