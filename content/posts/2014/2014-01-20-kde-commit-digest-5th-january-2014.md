---
title: "KDE Commit-Digest for 5th January 2014"
date:    2014-01-20
authors:
  - "mrybczyn"
slug:    kde-commit-digest-5th-january-2014
comments:
  - subject: "Choqok with pump.io support!"
    date: 2014-01-20
    body: "That's great! I mean, everything in the list is, but I'm specially grateful for that. Thanks!"
    author: "John"
---
In <a href="http://commit-digest.org/issues/2014-01-05/">this week's KDE Commit-Digest</a>:

<ul><li>Clicking a <a href="http://en.wikipedia.org/wiki/Geo_URI">Geo URI</a> link will now open <a href="http://marble.kde.org/">Marble</a>; Stars plug-in (Moon and Sun) rendering sees improvements</li>
<li><a href="http://okular.kde.org/">Okular</a> sees improvements in Find and Undo/Redo actions</li>
<li><a href="http://kate-editor.org/">Kate</a> makes AltGr enabled keyboards work in vim mode</li>
<li><a href="http://www.kde.org/applications/internet/choqok/">Choqok</a> implements Pump.io APIs</li>
<li><a href="http://kde.org/applications/games/navalbattle/">Naval Battle</a> shows enemies' ships after the game ends</li>
<li><a href="http://dot.kde.org/2014/01/07/frameworks-5-tech-preview">KDE Frameworks 5</a> work includes splitting the katession.h/.cpp files and unit tests.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-01-05/">Read the rest of the Digest here</a>.