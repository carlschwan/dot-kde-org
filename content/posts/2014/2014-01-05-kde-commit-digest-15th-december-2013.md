---
title: "KDE Commit-Digest for 15th December 2013"
date:    2014-01-05
authors:
  - "mrybczyn"
slug:    kde-commit-digest-15th-december-2013
comments:
  - subject: "New format"
    date: 2014-01-07
    body: "I just wanted to say I really like the new format of the digest, and as always you girls and guys do an amazing job!\r\nThank you!\r\n"
    author: "ricardo.barberis"
---
In <a href="http://commit-digest.org/issues/2013-12-15/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://marble.kde.org/">Marble</a> adds support for <a href="http://www.cyclestreets.net/">cyclestreets.net</a> bicycle routing, showing journey duration for <a href="http://wiki.openstreetmap.org/wiki/Open_Source_Routing_Machine">OSRM</a> routing</li>
<li><a href="http://userbase.kde.org/KHelpCenter">KHelpcenter</a> adds alphabetical sorting for modules and category reorganization to make it easier to use</li>
<li><a href="http://pim.kde.org/akonadi/">Akonadi</a> speeds up appending new items</li>
<li><a href="http://www.kde.org/workspaces/">Plasma</a> improves change wallpaper animation</li>
<li><a href="http://dot.kde.org/2013/12/17/qt-52-foundation-kde-frameworks-5">Qt5/KDE Frameworks 5</a> effort includes porting and enabling SMB kioslave to Qt5/KF5.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-12-15/">Read the rest of the Digest here</a>.