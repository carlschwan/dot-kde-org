---
title: "KDE Ships First Beta of Applications and Platform 4.13"
date:    2014-03-07
authors:
  - "unormal"
slug:    kde-ships-first-beta-applications-and-platform-413
---
KDE has released the first beta of the 4.13 versions of Applications and Development Platform. With API, dependency and feature freezes in place, the focus is now on fixing bugs and further polishing. Your assistance with finding and fixing issues is requested!

A partial list of improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan">4.13 Feature Plan</a>. A more complete list of the improvements and changes will be available for the final release in the middle of April.

This first beta release needs a thorough testing in order to improve quality and user experience. A variety of actual users is essential to maintaining high KDE quality, because developers cannot possibly test every configuration. User assistance helps find bugs early so they can be squashed before the final release. Please join the 4.13 team's release effort by installing the beta and <a href="http://bugs.kde.org">reporting any bugs</a>. 

The <a href="http://www.kde.org/announcements/announce-4.13-beta1.php">official announcement</a> has information about how to install the betas.
