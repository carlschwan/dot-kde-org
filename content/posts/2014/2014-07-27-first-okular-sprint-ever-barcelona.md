---
title: " First Okular Sprint ever at Barcelona"
date:    2014-07-27
authors:
  - "tsdgeos"
slug:    first-okular-sprint-ever-barcelona
comments:
  - subject: "One thing one forgot about!"
    date: 2014-07-28
    body: "Forgot to thank Blue Systems Barcelona for letting us use their office and the KDE e.V. for the financial support to have this sprint. If you want more sprints happening please donate at http://kde.org/community/donations/"
    author: "tsdgeos"
  - subject: "Okular rendering of scanned pages"
    date: 2014-07-28
    body: "Always interesting to hear about your progress, thank you :)\r\n\r\nI'm wondering if there's ever been any discussion about the slow speed at which Okular renders .pdf's that are not text but bitmap images for pages. I have often wondered why that was. Like I'll be waiting 5 seconds for Okular to render the next page of a scanned book and it breaks my reading flow."
    author: "Michael"
  - subject: "Digital signatures"
    date: 2014-07-28
    body: "A thing I've been missing for a while now is to display signatures of digitally signed PDF documents. I just leave the sugestion if someone takes another go at okular.\r\n\r\nCheers"
    author: "Miguel Andrade"
  - subject: "Searching"
    date: 2014-07-28
    body: "I would really like to see some improvements to searching.\r\n- searching in a big document is quite slow compared to acrobat reader\r\n- I am not a fan of how Okular starts searching automatically - it will automatically start searching if I pause whily typing my search term.  And it could search for the wrong thing if I make a typo while entering it.\r\n- a search widget that could be a part of the toolbar near the top of the window would be great\r\n\r\nLove the tabbed interface that Okular got recently though!"
    author: "DeKay"
  - subject: "PDF Annotation"
    date: 2014-07-30
    body: "Nice to hear that development of Okular is going on.\r\n\r\nPlease improve the annotations in Okular. No FOSS covers that area in a manner that is really usable in business. Okular is already quite close to that but there are some points:\r\n- missing ability to move pop-up note icons\r\n- configurable icon size of pop-up notes (the default is too huge for many documents)\r\n- option that enables that the text of the annotation is shown at the left/right margin at the corresponding height (now they are shown always at the upper left corner, the position is not stored, and no anchor is used)\r\n- behavior of inline notes (initial size too huge, resizing after edit, manual resizing, font update)\r\n- missing insert text annotation\r\n- missing strike-through and replacement text annotation\r\n- missing arrows.\r\n\r\nApart from that above, I am very happy with Okular. Many thanks! "
    author: "Heinz"
  - subject: "bug tracker"
    date: 2014-08-10
    body: "It is highly recommended to put requests like these in the bug tracker as feature requests so the developers see them and can track progress."
    author: "jospoortvliet"
  - subject: "Yes!!!"
    date: 2014-08-11
    body: "I very much need this feature. It is something that is lacking in Linux. Why doesn't Okular have it?"
    author: "Birbo"
---
In May a group of three Okular developers met for four days at the Blue Systems Barcelona office to hack on the KDE universal document viewer.

<div align="center" style="float: center; padding: 1ex; margin: 1ex; margin-top: 0ex; "><img width="620" src="/sites/dot.kde.org/files/IMG_0323.JPG" alt="Okular 2014 sprint group picture"/><br /><div align="center">Albert Astals Cid, Luigi Toscano and Fabio D'Urso</div></div>

The first day the team triaged a lot of bugs resulting in a <a href="https://bugs.kde.org/reports.cgi?product=okular&datasets=UNCONFIRMED&datasets=CONFIRMED&datasets=ASSIGNED&datasets=REOPENED&datasets=NEW&datasets=NEEDSINFO">nice drop of unconfirmed bugs</a>. There are still a lot of UNCONFIRMED items in that chart, almost all of them are wishes that we didn't know how to omit on the chart.

During the next days there were discussions about ideas and implementations, including:
<ul><li>Mimetype backend priority now that we have a txt backend that can open almost any file</li>
<li>Investigation about a printing regression regarding hardware margins in newer versions of CUPS</li>
<li>Removal of lots of widget code from backends</li>
<li>Idea of creating a command line okular2text application to test libokularcore which can be used in a gui-less environment.</li></ul>
<div align="right" style="float: right; padding: 1ex; margin: 1ex; margin-top: 0ex; "><img width="320" src="/sites/dot.kde.org/files/IMG_0324_modif.jpg" alt="Albert  talking about the workflow for saving documents"/><br /><div align="center">Albert  talking about the workflow for saving documents</div></div>

We decided that when porting to KF5 we will aim to have libokularcore be dependent on QtGui but not on QtWidget.

The most important item we discussed was how Okular saves file data. The proposal is to never autosave, making Okular act more like an editor. This has implications for bringing up old autosaved content if autosave was not used any more. By the end of the last day, we think we developed what seems a good plan. Now we only need time to code it ;)