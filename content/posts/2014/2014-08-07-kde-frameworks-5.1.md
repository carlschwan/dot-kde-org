---
title: "First Update to KDE Frameworks 5"
date:    2014-08-07
authors:
  - "jriddell"
slug:    kde-frameworks-5.1
comments:
  - subject: "Kwallet"
    date: 2014-08-07
    body: "In the qt5 kwallet: is there now native (OOB) support for single signon?"
    author: "Thomas22"
  - subject: "Weather Widget"
    date: 2014-08-07
    body: "Any plans to add this much needed widget?"
    author: "Weather widget"
  - subject: "different product"
    date: 2014-08-07
    body: "This is a release of KDE Frameworks: the libraries under KDE applications.\r\n\r\nThe weather widget is part of Plasma, which is an entirely different product in the same way Konsole, Kontact, Calligra, etc. are."
    author: "aseigo"
  - subject: "Re: Weather Widget"
    date: 2014-08-08
    body: "I've had no time to even look at KDE 5 Frameworks porting, unfortunately. Soon maybe, help is requested too."
    author: "Shawn Starr"
  - subject: "Recommended KDE SC version?"
    date: 2014-08-08
    body: "Hi,\r\n\r\nwhich kde SC version is recommended with plasma 5.1? 4.13 od 4.14 RC1? I have two machines and having ptoblem  with start od kde plasma 5 session - after login is only one od ten trying start kde successfull."
    author: "Petr Mal\u00fd"
  - subject: "both experimental"
    date: 2014-08-10
    body: "Try both and pickwhat works best - it is all work in progress at the moment..."
    author: "jospoortvliet"
---
<div style="width: 360px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="https://community.kde.org/File:Mascot_20140702_konqui-framework.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-framework_wee_0.png" /></a></div>

KDE has today made the <a href="http://kde.org/announcements/kde-frameworks-5.1.php">first update to KDE Frameworks 5</a>.  Frameworks are our addon libraries for Qt applications which provide numerous useful features using peer reviewed APIs and regular monthly updates.  This release has 60 different Frameworks, adding features from Zip file support to Audio file previews. For a full list, see KDE's Qt library archive website <a href="http://inqlude.org/">Inqlude</a>.  In this release KAuth gets a backend so you can again add features which require root access, KWallet gets a migration system from its KDELibs 4 version, and support has been added for AppStream files.

To code with Frameworks, read the <a href="http://api.kde.org/frameworks-api/frameworks5-apidocs/">API documentation</a> and install them with <a href="https://community.kde.org/Frameworks/Binary_Packages">Linux distro binary packages</a> or <a href="http://kde.org/info/kde-frameworks-5.1.0.php">grab the source code directly</a>.
<!--break-->
