---
title: "KDE Frameworks 5 Alpha Is Out"
date:    2014-02-14
authors:
  - "jospoortvliet"
slug:    kde-frameworks-5-alpha-out
comments:
  - subject: "Mac OS X"
    date: 2014-02-15
    body: "In case you want to experiment with KF5 on Mac OS X, here's a tap for homebrew users: https://github.com/haraldF/homebrew-kf5"
    author: "Harald Fernengel"
  - subject: "Article of its own"
    date: 2014-02-15
    body: "I think this comment deserves a complete article in the dot."
    author: "Mario"
  - subject: "ryanbram@gmail.com"
    date: 2014-02-17
    body: "Nice to hear that. Hope it will works on Windows as there are many great KDE application wasn't usable on Windows."
    author: "RyanBram"
  - subject: "Agreed, feel like writing it?"
    date: 2014-02-17
    body: "Agreed, feel like writing it? ;-)"
    author: "jospoortvliet"
  - subject: "Maybe someone that knows is better suited "
    date: 2014-02-17
    body: "I actually didn't knew about KF5 on mac much the less about a brew tap that compiles it.\r\n\r\nI'm not really a developer so I wouldn't know where to start or what to write.\r\n\r\n.... I'll try to follow the instructions on the git page and if I'm able to make it work I'll sure write something"
    author: "Mario"
---
<div style="float: right; padding: 1ex; margin: 1ex; width: 300px; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" /></div>Today KDE released the first alpha of Frameworks 5, part of a series of releases leading up to the final version planned for June 2014. This release includes progress since the <a href="http://www.kde.org/announcements/frameworks5TP/index.php">Frameworks 5 Tech Preview</a> in the beginning of this year.

See <a href="http://www.kde.org/announcements/announce-frameworks5-alpha.php">the announcement on kde.org</a> for more information and links to downloads. For information about Frameworks 5, see <a href="http://dot.kde.org/2013/09/25/frameworks-5">this earlier article on the dot</a>.