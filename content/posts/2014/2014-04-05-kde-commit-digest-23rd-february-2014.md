---
title: "KDE Commit-Digest for 23rd February 2014"
date:    2014-04-05
authors:
  - "mrybczyn"
slug:    kde-commit-digest-23rd-february-2014
---
In <a href="http://commit-digest.org/issues/2014-02-23/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://www.kdevelop.org/">KDevelop</a> Clang support adds refactoring / renaming of variables and functions</li>
<li><a href="http://kate-editor.org/">Kate</a> adds jump to next/previous change function and a plugin that allows you to launch the replicode executable with specified settings</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> implements webdav sharelink, adds optional KAccounts support to the facebook resource</li>
<li><a href="http://krita.org/">Krita</a> has a config option to pick colors with opacity</li>
<li><a href="http://community.kde.org/Plasma/Plasma_Media_Center">Plasma MediaCenter</a> implements a media cache populated by one or more media sources</li>
<li><a href="http://userbase.kde.org/NetworkManagement">NetworkManager</a> supports an airplane mode</li>
<li>Porting to <a href="http://dot.kde.org/2013/09/25/frameworks-5">Frameworks5</a> continues in <a href="http://rekonq.kde.org/">rekonq</a>, ksecrets, <a href="http://yakuake.kde.org/">YaKuake</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-02-23/">Read the rest of the Digest here</a>.