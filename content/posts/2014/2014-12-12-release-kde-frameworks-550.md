---
title: "Release of KDE Frameworks 5.5.0"
date:    2014-12-12
authors:
  - "jriddell"
slug:    release-kde-frameworks-550
---
KDE today announces the release of <a href="https://www.kde.org/announcements/kde-frameworks-5.5.0.php">KDE Frameworks 5.5.0</a>.

KDE Frameworks are 60 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see the Frameworks 5.0 release announcement.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.



<h3>Attica</h3>

<ul>
<li>Use all of QT_PLUGIN_PATH paths rather than just QLibraryInfo path to look for plugins</li>
</ul>

<h3>KActivities</h3>

<ul>
<li>Fix plugin loading with KDE_INSTALL_USE_QT_SYS_PATHS ON</li>
</ul>

<h3>KCMUtils</h3>

<ul>
<li>Restore KStandardGuiItems to get consistent icons and tooltips</li>
</ul>

<h3>KCodecs</h3>

<ul>
<li>Introduce KEmailAddress class for email validation</li>
<li>Use more robust implementation of MIME codecs taken from the KMime library</li>
<li>Add KCodecs::encodeRFC2047String()</li>
</ul>

<h3>KCompletion</h3>

<ul>
<li>Fix PageUp/Down actions in the completion popup box</li>
</ul>

<h3>KCoreAddons</h3>

<ul>
<li>Add KTextToHTML class for plaintext->HTML conversion</li>
<li>Add KPluginMetaData::metaDataFileName()</li>
<li>Allow to read KPluginMetaData from .desktop files</li>
<li>Kdelibs4Migration now gives priority to distro-provided KDE4_DEFAULT_HOME</li>
</ul>

<h3>KDeclarative</h3>

<ul>
<li>Use Qt's method of blocking for component completion rather than our own</li>
<li>Make it possible to delay initialization of object incubated from QmlObject</li>
<li>Add guard when trying to access root object before component is complete</li>
</ul>

<h3>KEmoticons</h3>

<ul>
<li>Add KEmoticonsIntegrationPlugin for KTextToHTML from KCoreAddons</li>
</ul>

<h3>KHTML</h3>

<ul>
<li>A number of forward-ported fixes from kdelibs, no API changes.</li>
</ul>

<h3>KIO</h3>

<ul>
<li>Fix Size columns being empty in the KFileWidget detailed views</li>
<li>Do not drop ASN passed to KRun when executing desktop files</li>
<li>Fix passing of DESKTOP_STARTUP_ID to child process in kioexec</li>
<li>Fix compilation with Qt 5.2, which also fixes a race condition</li>
<li>KFileItem: cleanup overlay icon usage    </li>
<li>Implement back/forward side mouse buttons to navigate in the history</li>
<li>Allow user to cancel out of the certificate accept duration dialog box.</li>
</ul>

<h3>KJobWidgets</h3>

<ul>
<li>Fix compilation with Qt 5.2.0</li>
</ul>

<h3>KNewStuff</h3>

<ul>
<li>Also allow absolute filepaths for configfile parameter.</li>
<li>Fix compilation on Windows</li>
</ul>

<h3>KNotifications</h3>

<ul>
<li>Make KNotificationPlugin a public class</li>
<li>KPassivePopup - Set default hide delay</li>
</ul>

<h3>KRunner</h3>

<ul>
<li>Add a simple cli tool to run a query on all runners</li>
</ul>

<h3>KService</h3>

<ul>
<li>Fix KPluginTrader::query() for old JSON</li>
<li>Deprecate kservice_desktop_to_json for kcoreaddons_desktop_to_json</li>
<li>Implement KPluginTrader::query() using KPluginLoader::findPlugins()</li>
<li>Fix KPluginInfo::entryPath() being empty when not loaded from .desktop</li>
</ul>

<h3>KTextEditor</h3>

<ul>
<li>Fix bug #340212: incorrect soft-tabs alignment after beginning-of-line</li>
<li>Add libgit2 compile-time check for threads support</li>
</ul>

<h3>KWidgetsAddons</h3>

<ul>
<li>Add class KSplitterCollapserButton, a button which appears on the side of</li>
  a splitter handle and allows easy collapsing of the widget on the opposite side
<li>Support monochrome icon themes (such as breeze)</li>
</ul>

<h3>KWindowSystem</h3>

<ul>
<li>Add KStartupInfo::createNewStartupIdForTimestamp</li>
<li>Add support for more multimedia keys</li>
<li>Add support for initial mapping state of WM_HINTS</li>
<li>Drop incorrect warnings when using KXMessages without QX11Info</li>
</ul>

<h3>Plasma Framework</h3>

<ul>
<li>Fix compilation with Qt 5.2.0</li>
<li>Fix the platformstatus kded module</li>
<li>Migrate BusyIndicator, ProgressBar to QtQuick.Controls</li>
<li>Add thumbnailAvailable property to PlasmaCore.WindowThumbnail</li>
</ul>

<h3>Solid</h3>

<ul>
<li>Fix warning: No such signal org::freedesktop::UPower::Device...</li>
</ul>

<h3>Extra cmake modules</h3>

<ul>
<li>Set CMAKE_INSTALL_SYSCONFDIR to /etc when CMAKE_INSTALL_PREFIX is /usr (instead of /usr/etc)</li>
<li>Enable -D_USE_MATH_DEFINES on Windows</li>
</ul>

<h3>Frameworkintegration</h3>

<ul>
<li>Implement standardButtonText().</li>
<li>Fix restoring the view mode and sizes in the file dialog</li>
</ul>
