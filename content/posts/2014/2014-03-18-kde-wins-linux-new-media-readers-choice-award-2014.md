---
title: "KDE wins Linux New Media Readers Choice Award 2014"
date:    2014-03-18
authors:
  - "cschumacher"
slug:    kde-wins-linux-new-media-readers-choice-award-2014
comments:
  - subject: "Award"
    date: 2014-03-18
    body: "Cornelius has posted an image of the award on Twitter:\r\nhttps://twitter.com/cschum/status/444370175631306752/photo/1"
    author: "Stuart Jarvis"
  - subject: "Congratulation"
    date: 2014-03-20
    body: "Congratulation... I also use KDE on my GNU/Linux system.\r\n:)"
    author: "Demsking"
---
Last week at CeBIT, KDE won the <a href="http://www.linux-magazin.de/NEWS/Cebit-2014-KDE-Tor-Bitcoin-und-Git-gewinnen-Preise">Linux New Media Readers Choice Award 2014</a> (link to German language Linux Magazine) for the best Linux Desktop Environment. 46% of the readers of Linux New Media's global publications voted for KDE. Runner-ups were GNOME with 18% and XFCE with 13%. Other awards went to CyanogenMod, Raspberry Pi, Bitcoin, Puppet, Tor and Git.

<div align=center style="padding: 1ex; margin: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/LinuxNewMediaAward10085.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/LinuxNewMediaAward700.jpg" /></a><br />Readers Choice: Best Linux Desktop Environment <small>(click for larger)</small></div>
Cornelius Schumacher, President of <a href="http://ev.kde.org/">KDE e.V.</a> received the award on behalf of the KDE Community from Mathias Huber, Editor at Linux Magazine. The video of the award ceremony will be available on the <a href="http://www.linux-magazin.de/VIDEOS/Konferenzvideos/Special-Conference-Open-Source-2014">Linux Magazine web site</a> later.

KDE is delighted to receive the award. Hundreds of our volunteers dedicate hard work and passion to creating free software for end users and it is great to be recognized in this way. KDE's software runs on tablets and phones, Windows and Mac OS, but the core of what the KDE Community is doing is still focused on the Linux Desktop. Continuous work over many years has made KDE's Plasma the reliable choice of the majority of Linux Desktop users.
