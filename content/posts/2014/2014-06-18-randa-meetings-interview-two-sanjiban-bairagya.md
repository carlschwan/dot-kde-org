---
title: "Randa Meetings Interview Two: Sanjiban Bairagya"
date:    2014-06-18
authors:
  - "devaja"
slug:    randa-meetings-interview-two-sanjiban-bairagya
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img width="200" height="225"  src="/sites/dot.kde.org/files/Sanjiban_Bairagya.jpg" /><br />Sanjiban Bairagya</div>

First and foremost we would like to thank everybody that already supported the <a href="http://www.kde.org/fundraisers/randameetings2014/index.php">Randa Meetings fundraising</a>. We have reached almost 1/3 of the our goal. Please help more and spread the word. If we reach our goal we can have an even more stable <a href="http://www.kdenlive.org">Kdenlive</a>, more applications ported to <a href="http://community.kde.org/Frameworks">KDE Frameworks 5</a>, further progress on Phonon, a look at <a href="http://amarok.kde.org">Amarok 3</a>, even better <a href="http://edu.kde.org">KDE educational applications</a>, a finished port of <a href="http://www.gcompris.net">GCompris</a> to Qt and KDE technologies, an updated KDE Book, more work on Gluon and a new and amazing KDE SDK!

Here we are in conversation with Sanjiban Bairagya, a current Google Summer of Code 2014 intern who is working on <a href="http://marble.kde.org">Marble</a> for KDE and is one of the younger, fresher, newer lots at KDE and has quite a bit to offer in terms of enthusiasm and brilliant ideas as well as zeal! 

<em>Could you describe yourself in a few lines and tell us where you're from?</em>

I'm a B.Tech student of Information Technology studying in the National Institute of Technology, Durgapur, India. I am a FOSS enthusiast and have been contributing to KDE since April of last year, and have been selected for Google Summer of Code this year. I am currently working on implementing interactive tours in Marble, with Dennis Nienhüser and Torsten Rahn as my mentors.

<em>How did you first chance upon KDE? Could you describe your journey in short?</em>

KDE is seen very importantly in the university I study in. Some of my seniors before me have been working on KDE for a long time, and the juniors were told were told by them (I was a junior once) about the friendly and helpful nature of the community. So I also thought of giving it a shot and I started my "research" on KDE. I found it to be a really cool desktop environment to work in. After a few months of playing around, I came across this list of junior jobs in bugs.kde.org, so i started scrolling through them and  wanted somehow to contribute to Marble since that was one software I did use regularly. And so I did. I just started solving bugs, one after the other. Then applied for GSoC this year, and got selected. Ah, just to mention, a few months before GSoC, we (me and a couple of more guys) even held a talk in our college, specifically about KDE. I spoke about Marble. Vedant spoke about Amarok, etc. Anyways, that's it. That's my "journey" (which is still ongoing) in KDE.

<em>Why is KDE so special to you?</em>

KDE is actually the most special thing to me. It gave me something nothing else could: a job related with real world software, with real world actual core developers. Plus, this global acknowledgement is simply amazing. In fact, I think that the single-most significant best thing which has happened to me at university was finding KDE. As I say to myself, "KDE gave me wings". I am just proud to be a member of a community so rich with knowledge, that I find myself kind of privileged to be in it.

<em> When did you first hear about the meeting in Randa and why do you wish to be a part of it?</em>

I was going through a conversation in #marble, where Mario had mentioned the term Randa to Dennis, asking him whether he will be going to it or not, so I asked Dennis whether this Randa thing was related to KDE in some way or not. And he said yes, and gave me the link, so that's how I got to know about it. I want to participate in it, firstly because Dennis is going there and I would love to meet him in person, and secondly, because I will be able to sit down and code away all day with so many more brilliant developers. Plus, I also heard (and I was going through the previous years' pics as well) that the folks have pretty good fun over there. So that is also one of the reasons. Mainly I want to go there for the experiences and the new things I will learn. I also have a few goals/points related to Marble as well, which I want to finish while I am there.

<em>Which specific area of KDE applications do you contribute to? Could you describe it in short?</em>

I contribute to an application under KDE Edu, called Marble. It is a virtual globe, with which you can view the planet Earth (and moon as well) in a humongously different number of ways, with different map themes, routes and directions, tracks, satellite maps, weather maps, temperature maps, precipitation maps and even historical maps. Whatever you need when it comes to maps is there.

<div style="float: center; padding: 1ex; margin: 1ex;"><img width="600" height="225"  src="/sites/dot.kde.org/files/marble-1.png" /><br />Marble is a virtual globe and world atlas — your swiss army knife for maps.</div>

<em>What is your specific role in the particular group of KDE Applications that you are a part of and how long have you been working?</em>

My role is just writing code like every other Marble developer out there. Right now I am working on my GSoC project. And I have been working on Marble since April, 2013.

<em>Have you got anything in particular planned for Randa?</em>

Yes, I will be working on Map theme tours on Marble (taking tours on different themes on Marble), implementing Gpsies services to Marble, and I am also planning on working on the QML or mobile part of Marble as well while in Randa.

<me>What will you be looking forward to the most in the Randa Sprint? Any expectations or hopes of what it will be like?</em>

I am looking forward to having a great time there in Randa, making new friends, meeting new people, and just keep learning more and more.

<em>What does KDE mean to you and what role has it played in shaping you as a contributor/developer?</em>

KDE means everything to me. It is the only thing which I have been this serious about. It sharpened my skills of object oriented programming, from A to Z. All thanks goes to none other than my mentor Dennis Nienhüser who has been patient enough to guide me thoroughly in this journey. All my contributions to KDE that I have been able to make so far, I owe to him. And I am very sure all the others in KDE are also as helpful as him.

<em>Why do you think Meetings such as Randa are very important for KDE and for open source communities around the globe?</em>

Meetings such as these, in my opinion, are very important, because these are the events, in which top developers get to discuss their ideas face to face, and come up with great plans, and then execute them. And all this happens within one week, which is really amazing. These meetings are very important for having a lot of progress, in a short duration of time.

<em>Why do you think supporting them is of importance and how has the support helped you as a KDE developer and an open source contributor?</em>

It is very important, since open source contributions should be increased more and more, so that people with ideas and skills, can get them executed, for free.

<em>Could you briefly describe a rough outline of what you'd imagine your typical day in Randa this time around to be?</em>

My typical day in Randa I would imagine, would start with some good food and then some coding, and then having some fun and hanging around. But, seriously, I don't even have the slightest idea. Which is what I am going to find out there.

<em>Is this your first time to Switzerland? Are you excited about being in another country?</em>

Not even Switzerland, this is actually the first time I am going to <b>any</b> country outside of India. In fact, it is only after knowing about Randa, that I applied for a passport. I am tremendously excited about this trip. I am pretty sure, that it's gonna be a hell of a bumpy ride. So, see you all in Randa then! 

<em>Thanks a lot, Sanjiban, for your time for the interview and dedication to Marble and the KDE community.</em>

Please <a href="http://www.kde.org/fundraisers/randameetings2014/">support us</a> in the organization of the Randa Meetings 2014.
