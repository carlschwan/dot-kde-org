---
title: "KDE PIM November Sprint"
date:    2014-03-03
authors:
  - "kevkrammer"
slug:    kde-pim-november-sprint
comments:
  - subject: "Bug searching"
    date: 2014-03-03
    body: "I really hope someone has added wish to bugtracker that Ctrl+M would hide the menubar in every new child window as well what gets opened from main window (like opening email to read in own window, or writing a new email). \r\n\r\nit is always fun to see photos from these marathons, especially from the progress boards and other fancy blog posts from process and ideas what has been throwed around in those. "
    author: "Fri13"
  - subject: "After reading about Baloo, I"
    date: 2014-03-03
    body: "After reading about Baloo, I cleaned up my KDE installation and built everything without Nepomuk.\r\nAnd I simply don't have to open up gmail to search for my emails anymore.\r\nKMail (Akonadi, technically) works perfectly with Baloo. You will not even notice it. No high CPU usage. Instant search results.\r\nAlso, KMail has got a new 'Quick search' which works amazing.\r\n\r\nThank you guys. KMail is awesome. Keep it going!"
    author: "emilsedgh"
  - subject: "sooner would be better :D"
    date: 2014-03-04
    body: "Dude. I've seen it work, yes, it is awesome. So sad that it isn't out yet ;-)\r\n\r\nI don't want to use beta packages so for the time being, I'm waiting... Too bad the release is still like a month and a half away :D"
    author: "jospoortvliet"
  - subject: "Do you have plans for upgrade"
    date: 2014-03-04
    body: "Do you have plans for upgrade UI. It's just horrible right now. Actually kopete from kde3 was horrible with look and feel.. but not too much has changed from that time."
    author: "Maksim"
  - subject: "Chat UI?"
    date: 2014-03-05
    body: "If you are looking for a different chat interface, have a look at the KDE Telepathy options."
    author: "kevkrammer"
---
<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kdepim2013.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdepim2013_wee.jpg" /></a><br />KDE PIM sprint Nov 2013</div>In early 2013, it was established that "<em>Osnabrück is not a place</em>". Meaning that the KDE PIM spring sprint, which traditionally takes place in Osnabrück, could happen at a different location and <a href="http://dot.kde.org/2013/04/11/kde-pim-sprint-berlin-2013-cuter-pictures">still be a continuation of the tradition</a>.

KDE PIM's autumn sprint has traditionally been in Berlin, but since the team decided that "<em>Berlin is not a place</em>" applies as well, this year's installment of the sprint took place in Brno in the Czech Republic.

Even people without the exceptional skills of Sherlock Holmes have certainly deduced by now that KDE PIM sprints happen in cities that are not places but coincidentally contain the letters B, R and N.

Now, without further procrastination, the story about the KDE PIM autumn sprint, brought to you by <a href="http://ev.kde.org">KDE e.V.</a>, <a href="http://www.redhat.cz">Red Hat</a> and the letters B, R and N.
<h2>Early birds</h2>
Unlike with most other sprints, where all but some local people arrive on the first day of the sprint, there had already been a week of intense KDE hacking been going on.

Bob - <a href="http://dot.kde.org/2012/10/25/kde-pim-october-sprint">you remember Bob?</a> - and his merry henchmen from the KDE Barcelona Squad, had already arrived earlier that week and hacked on various pieces of KDE software and had beer delivered to them on trains. Yes, trains! That's the Czech Republic for you.

<h2>Notes</h2>
We're sorry to have to notify you of the fact that nobody worked on KNotes. There were plenty of old school sticky notes though. Because Kevin Ottens likes to draw rectangles on white boards and sticky notes are a natural choice for filling them.

There were also a lot of notes taken, notably on the outcome of the discussions which were scheduled by moving notes on the whiteboard.

This kind of structured handling of topics is a noteworthy improvement over some of the previous sprints and very necessary given the increased number of people who nowadays attend and take note of them.

<div align=center style="padding: 1ex; "><a href="http://dot.kde.org/sites/dot.kde.org/files/PIMgroup2013Nov.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/PIMgroup2013Nov700_0.jpg"></a><br /><small><b>Back row:</b> Lukas Tinkl, John Layt, Michael Bohlender, David Edmundson, Ingo Klöcker, Daniel Vratil<br /><b>Middle row:</b> Kevin Krammer, Martin Klapetek, Mark Gaiser<br /><b>Front row:</b> Christian Mollekopf, Alex Fiestas, Vishesh Handa, Jan Grulich</small></div>

<h2>Bugs</h2>
As has become tradition, a significant portion of the meeting was dedicated to mercilessly squashing those nasty little buggers. David Faure, a man who certainly needs no further introduction, used the presence of several component maintainers to get issues fixed. "Getting fixed" meaning he did the actual fixing, being aided by the aforementioned component specialists with insight into inner workings and assumptions of the respective code.
<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/food_0.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/food_wee.jpg" /></a><br />Hackers' vitamins</div> 

The previous and current maintainers of Akonadi had fun with things so deep down in the guts of the system that not even the author of this article would be able to fully understand them. Those people are way smarter than him!

In addition to fixes in the sense of correcting erroneous behavior, this also included several improvements in the area of runtime performance. And a faster KDE PIM makes everybody happy.
<h2>Progress</h2>
One of the other fun aspects of a sprint, aside from the obvious awesomeness of hanging out with great people and doing interesting code work, is to ponder and prototype potential progressive programming pieces.
<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/pizza.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/pizza_wee.jpg" /></a><br />Big ass pizza!</div> 
Mark Gaiser, Michael Bohlender and Thomas Pfeiffer had a closer look at how to get beyond quaint, dare I say boring, user interfaces and enable QtQuick-based applications to tap into the power provided by KDE PIM libraries. Some example code was written, plans were drawn - but much is still to be done.
<h2>Secrets</h2>
Naturally the presence of the KDE Barcelona Squad made secrecy a paramount objective. Not only do we need to hide their identities, a job made easy by several Squad members disguising themselves with enormous fake beards, we are bound by oath—under threat of <a href="http://lmgtfy.com/?q=konqi">dra<big>g</big>onian</a> punishment—to not talk about rocket science like advances in PIM data search. Well, "rocket science" doesn't even cut it, more likely on the level of warp science!

<em>Editor's note</em>: recent <a href="http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search">leaks</a> have nothing, absolutely nothing, to do with our ability to keep things, you know, secret. Really!