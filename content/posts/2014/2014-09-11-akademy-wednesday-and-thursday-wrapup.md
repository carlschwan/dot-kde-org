---
title: "Akademy Wednesday and Thursday Wrapup"
date:    2014-09-11
authors:
  - "jriddell"
slug:    akademy-wednesday-and-thursday-wrapup
comments:
  - subject: "Sorry for the video quality"
    date: 2014-09-11
    body: "Sorry for the video quality folks but I thought it better than nothing"
    author: "jriddell"
  - subject: "Transcription"
    date: 2014-09-18
    body: "Can someone who can hear what they are saying produce a transcription.  I can not recognise a single word."
    author: "Darren Drapkin"
---
Akademy continues with hacking and BoF meetings.  This <a href="https://www.youtube.com/watch?v=kyvTRLt7rX8">wrapup meeting video</a> covers sessions from Wednesday and Thursday including accessibility, release team, user information reporting, KDE applications websites, KDevelop and share-like-connect.

<iframe width="560" height="315" src="//www.youtube.com/embed/kyvTRLt7rX8?rel=0" frameborder="0" allowfullscreen></iframe>
