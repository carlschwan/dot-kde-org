---
title: "KDE Commit-Digest for 8th December 2013"
date:    2014-01-04
authors:
  - "mrybczyn"
slug:    kde-commit-digest-8th-december-2013
---
In <a href="http://commit-digest.org/issues/2013-12-08/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://userbase.kde.org/Artikulate">Artikulate</a> enables initial learner profile support</li>
<li><a href="http://edu.kde.org/marble/">Marble</a> gets implementation of movie capturing</li>
<li><a href="http://www.calligra-suite.org/">Calligra</a> gains Apple Keynote document import filter</li>
<li><a href="http://www.kde.org/applications/internet/choqok/">Choqok</a> gets initial <a href="http://pump.io/">Pump.io</a> implementation</li>
<li><a href="http://userbase.kde.org/KWin">KWin</a> improves icon handling: uses a QIcon in client for the icons instead of Pixmaps and now all sizes are available in one QIcon allowing easy access the best fitting one</li>
<li>Plasma improves crash recovery for plasma-shell</li>
<li><a href="http://pim.kde.org/components/knotes.php">KNotes</a> is converted to <a href="http://pim.kde.org/akonadi/">Akonadi</a></li>
<li>Interesting work in progress: server-side search in Akonadi, online banking including sending credit transfers in <a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a>.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-12-08/">Read the rest of the Digest here</a>.