---
title: "KDE Software Down Under"
date:    2014-02-18
authors:
  - "jospoortvliet"
slug:    kde-software-down-under
comments:
  - subject: "Nice"
    date: 2014-02-19
    body: "we're a small company (~12 employees) and are using KDE on the desktop since 10 years now on OpenSuse.\r\nSuse 13.1 is giving us a lot of headaches (cups network communication is defunct and no workaround, nfs client hangs on shutdown... ) lately, but we'll manage (not KDE related).\r\nAlso the transition to the new KDEPIM was quite painful, but that's now working again."
    author: "thomas"
  - subject: "Nice to hear!"
    date: 2014-02-23
    body: "If you need any help with the KDE implementation used by openSUSE, feel free to drop by on #opensuse-kde in Freenode. For general PIM questions (since you mention difficulty), there are instead the friendly people at http://forum.kde.org. "
    author: "einar"
---
Today we proudly feature an interview with Bernard Gray from <a href="http://www.debortoli.com.au/">De Bortoli Wines</a>, an Australian winemaking company.

<div align=center style="padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/DeBortoli700.png" /><br />Hot and Dry in Australia</div>

We spoke with Bernard Gray who has worked for the company for over 10 years in an IT project management and development role. He is, in his own words: <q style="color:dimgray">"a tertiary qualified programmer, and has been involved in either core development or supporting development with a few Open Source distros/projects over the years"</q>.

De Bortoli Wines is one of Australia's largest family-owned wine makers. Bernard: <q style="color:dimgray">"The company started in 1928, and is now under the leadership of the third generation of the De Bortoli Family. Our approach as a company is innovative, forward thinking, and sustainable – this is inherent in all areas of our business, not least with our IT approach."</q>

We started by asking him how long they have been using Linux.

<p style="color:#696969">"As a company, our first production Linux server was deployed in the mid-late 90s. Personally, I first used Linux in 1999 when I started University – I had very little idea of computing at the time, let alone Operating Systems and their differentiating qualities. It wasn't until 2003 that I really began to get my teeth into Linux when I started developing the Linux based Live CD environment that we've internally branded “GTs” (Graphical Terminals, originally designed to replace our thin-client telnet terminals)."</p>

<p style="color:#696969">"The beauty of these devices is that we could purchase commodity PC hardware, outsource hardware support, maintain a single image Live CD based operating system environment with its read-only root filesystem rendering it “unbreakable” so to speak. Combined with the fact that it runs out of a ramdisk and on generously spec'd desktop hardware, we finally managed to nail the trifecta of Cheap, Fast AND Good."</p>

<p style="color:#696969">"We deployed our first "GT" in production in April 2004."</p>

<strong>The Dot</strong>: Could you tell us a bit about the migration, both the reasons for it and the experiences you had with it?

<p style="color:#696969">"Our original GT shipped with an early 2.x Gnome release. This had more to do with my general lack of skills with package management and live image building than by design. Since the distro I was using at the time shipped Gnome by default – I went along with it. Since then, we've migrated to KDE 3.5, back to Gnome 2.8 and finally to KDE 4.9 which we've just completed the rollout for, and which now makes up approximately three quarters of our 250+ desktop fleet."</p>

<p style="color:#696969">"The key to all smooth migrations we've found is Desktop Environment consistency. Keep the major applications cross-platform where we can (browsers, office suites, assorted tools). Keep the icons where people are expecting them (they're in the same spot on our Windows desktops too)."</p>

<p style="color:#696969">"The KDE 4.x series keeps the consistent Desktop Environment paradigms that our users are familiar with (application menu, panels/toolbars, window management), while still bringing incremental features that enhance their productivity (compositing, more advanced window management like improved snapping, widgets etc). It also brings some neat and flexible customization options via Plasma and its javascript interface which makes my life so much easier when it comes to customizing the DE and getting consistent behavior across different setups."</p>

<p style="color:#696969">"Still on the development side, the KDE techbase site has been an invaluable source of information for me, as has the KDE community–both users and developers. Often large technical communities struggle with a high amount of enthusiastic users but without a lot of knowledge, which make the skilled base hard to find/interact with for solving more difficult issues. With KDE I have had nothing but good experiences - it's worth a special mention for a particular Okular bug we were experiencing which was resolved in record time after I contacted the Okular developers mailing list."</p>

<p style="color:#696969">"All these things combine to greatly reduce the development and training load requirement on our small team - which keeps us, and most importantly our users, happy and productive."</p>

That is great to hear! Thank you Bernard for taking the time to answer our questions. We wish you and De Bortoli wines a great time with our software.