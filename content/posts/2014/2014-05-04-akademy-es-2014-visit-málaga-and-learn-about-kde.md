---
title: "Akademy-es 2014: Visit M\u00e1laga and learn about KDE"
date:    2014-05-04
authors:
  - "tsdgeos"
slug:    akademy-es-2014-visit-málaga-and-learn-about-kde
---
From May 16th to 18th, Málaga is hosting Akademy-es 2014 in <a href="http://www.etsit.uma.es/">Escuela Técnica Superior de Ingenieria de Telecomunicación</a> of Universidad de Málaga. This event is organized by <a href="http://www.kde-espana.org/">KDE España</a>, <a href="http://www.linux-malaga.org/">Linux Málaga</a> and <a href="http://bitvalley.cc/">Bitvalley</a>, and represents the return of a KDE event to Málaga 9 years after it hosted Akademy 2005.

This Akademy-es will have a <a href="http://www.kde-espana.org/akademy-es2014/programa.php">wide range of talks</a> from informing users about KDE software to programming with QtQuick and ASAN. There will also be introductory talks for people who want to start contributing to KDE.

As always the event is free to attend, but <a href="http://www.kde-espana.org/akademy-es2014/registro.php">you should register</a> to make it easier for the organization and to get a printed badge instead of a handwritten one ;).

<div align=center style="padding: 1ex; margin: 1ex; "><a href="http://www.kde-espana.org/akademy-es2013/fotogrupo.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/AkademyESfotogrupo700.jpg" /></a><br />Akademy-es 2013 group photo<small> (click for larger)</small></div>

Thank you to Gold <a href="http://www.kde-espana.org/akademy-es2014/patrocinadores.php">Sponsors</a> Digia, Opentia and openSUSE for helping make this happen!