---
title: "KDE Releases Alpha Version of Next-gen Plasma Workspace"
date:    2014-04-02
authors:
  - "sebas"
slug:    kde-releases-alpha-version-next-gen-plasma-workspace
comments:
  - subject: "What about a new look?"
    date: 2014-04-03
    body: "Is there any plan to have a new icon/widget theme?\r\nOxygen has served us well, on both fronts, but isn't it time to refresh the look a bit? To something more modern and polished?\r\n\r\nExcept oxygen, there is currently no polished widget theme available. At all. It's all a qtcurve based mess :("
    author: "Josh"
  - subject: "suggestions"
    date: 2014-04-03
    body: "as a kde user since kde2 please accept an humble suggestion:\r\ndamn, still wobbly windows and blue leds as default?\r\nplease do a more sober default setup, that is the most abused criticism on kde by so many people!"
    author: "Andrea Z."
  - subject: "Week numbers in the calendar"
    date: 2014-04-03
    body: "Looks great.\r\nThere are no week numbers in the calendar!"
    author: "JW"
  - subject: "Looks awesome!"
    date: 2014-04-03
    body: "Looks awesome, keep up the good work!"
    author: "Erik"
  - subject: "Congratulations!"
    date: 2014-04-03
    body: "Congratulations! You guys are doing a terrific job"
    author: "pedrorodriguez"
  - subject: "Wobbly windows the default?"
    date: 2014-04-03
    body: "If so this is news to me - which distro is that on?"
    author: "bluelightning"
  - subject: "KDE is still inconsistent"
    date: 2014-04-03
    body: "Hi to all! :) I'm kde user since years and I really apreciate the work devs and designers did on kde. What I don't understand is... am I the only one who thinks the integration between \"plasmoids\" and other apps(qt apps) is inconsistent ? Why we can't try to have plasmoids' look even for the qt? This will bring kde to be one piece insted of various(not always visually integrated) components.\r\nLet me know what do you think. Thanks :)"
    author: "Aleksandar Petrinic"
  - subject: "icons theme"
    date: 2014-04-03
    body: "Please, change icons to this (as suggested by KDE's visual design group)\r\nhttp://wheeldesign.blogspot.fr/2014/03/monday-report-7-beach-edition.html"
    author: "Walid"
  - subject: "matching flat widget and window theme for  Breeze Plasma theme"
    date: 2014-04-03
    body: "It would be nice if a matching flat themes for widget and window decoration is also made available( a more complete flat themeing)"
    author: "anonymus"
  - subject: "oxygen transparent"
    date: 2014-04-03
    body: "Can there be default support for oxygen transparent. Would look good with the more refined desktop"
    author: "shaurya"
  - subject: "agree"
    date: 2014-04-03
    body: "Yeah,\r\nto be honest I have some friends that says they prefer the kde philosophy, but they run under gnome 3, because the kde theme, is really from an other age, and when you spend 8h a day in front of an interface you don't like, it's not pleasant.\r\nDo it as beautifull as gnome with the same perfectionism, but not their #*@ workflow, and you'll get a lot of new supporters...\r\nMy advises : sober and clean is the way to go (gnome/osx), and window 8/tablet is the way to not go..."
    author: "keopsis"
  - subject: "good"
    date: 2014-04-03
    body: "this looks great."
    author: "joe"
  - subject: "It's coming, it's coming."
    date: 2014-04-03
    body: "The thing is these things take time. Right now we're working on a new widget as well as a new window decoration and icon theme. But we got in fairly late in the game so it hasn't gone through yet. But it will."
    author: "Jens Reuterberg"
  - subject: "Looks very edgy"
    date: 2014-04-04
    body: "Any way to have smooth corners with the start menu?\r\n...\r\nCourse they'll."
    author: "Cheesy_popcorn"
  - subject: "Wobbly Windows is not enabled by default"
    date: 2014-04-04
    body: "Wobbly Windows has never been, is not and will never be enabled by default. It's used in the video as it got recorded by a developer who likes using Wobbly Windows."
    author: "mgraesslin"
  - subject: "What about activities?"
    date: 2014-04-04
    body: "Nice and elegant but... For some time Activities has been one of the flags of the Plasma workspace and the new Activities chooser looks cool. The usage and usefulness of Activities, however, introduces a new concept to the users that has not been well promoted and demonstrated. The video above shows some activities but completely empty: what is the shown \"email activity\" usefull for? in what is it different from, ex organizing applications in different screens? I am affraid that unless there is some investment in promoting the concept and practical use of Activities, this will be just an \"interesting concept\" but underused."
    author: "Nuno"
  - subject: "Great look with some room for optimizations"
    date: 2014-04-04
    body: "I like the new calendar and notifier a lot and i would like to thank you for your awesome work with kde. \r\n\r\nHowever, the inactive tasks do have a very low contrast, so it is hard to read the title, I do also like the old kickoff menus indicators of the current active category more than the new one. The small line above the category (in the screen shot: favorites) need some more attention to get the current state of the menu. So while this is very shiny. keep in mind the \"contrast\" / \"readability\" / \"easy to capture\" aspect of the gui"
    author: "Till Sch\u00e4fer"
  - subject: "*sigh* still same problems as 10 years ago"
    date: 2014-04-04
    body: "At 30 second mark, the user has to make the window horizontally bigger, as the content of the window doesn't fit it properly at it's default size. I think it has now been about 10 years when I was talking about these exact same problems in various KDE mailing-lists, bug-reports and the like, and it seems that they are still not fixed.\r\n\r\nThese are tiny things, easy to fix. And requiring the user to deal with these issues just leaves a bad taste in users mouth."
    author: "Janne from the past"
  - subject: "Where is the blur?"
    date: 2014-04-04
    body: "This is all very nice and I like it very much, ... but weren't we promised with more blur instead of more \"white\" on transparent menus?\r\nThe panels don't have to seem enough blur for a non-white theme and also in the activity manager, you can see there is no blur at all behind the activity name (which will make it unreadable with a relevant wallpaper).\r\n\r\nSo pleeeaase keep the blur in mind."
    author: "rich ard"
  - subject: "Where are your patches?"
    date: 2014-04-05
    body: "If it's easy to fix I'm wondering why you don't submit a patch? For the record I fix thus an issue a week ago and I didn't find it an easy to fix but quite some work."
    author: "mgraesslin"
  - subject: "It's up to the user"
    date: 2014-04-05
    body: "The power of activities is that it will look different for each user. We as the developers cannot know what possible activities will look like for the user. It's a tool to work in a more productive way. So of course it's valid to use an activity for emails as that will hide the mails from other activities and provides a more distraction free work. But if it suits a user better to have the emails always visibile on another screen (if available) or on a virtual desktop that is equally valid and supported."
    author: "mgraesslin"
  - subject: "Not suited for default"
    date: 2014-04-05
    body: "Oxygen transparent is still not suited for default due to technical reasons."
    author: "mgraesslin"
  - subject: "Separation between Desktop Shell and Applications"
    date: 2014-04-05
    body: "There is a visual separation between the elements belonging to the Desktop Shell and the Applications. This is nothing special to our software stack but can also be seen in other environments, e.g. GNOME Shell."
    author: "mgraesslin"
  - subject: "Do you realize that now some of us are dying of anxiety, do you?"
    date: 2014-04-05
    body: "Please, please, please show us something as soon as you can!\r\nAnd of course, thanks a lot for what you do :)"
    author: "msx"
  - subject: "But, not 9 years ago."
    date: 2014-04-06
    body: "Most of those isses you remember, was actually fixed 9 years ago. For the KDE 3.4 release a lot of size tweaks was made by Waldo Bastian, cleaning up the initial size of most applications.\r\n\r\nUnfortunatly it's a reappearing problem, both because of changes made since then and applications becomming replaced. And to make the problem worse, it's common with varius relsolutions. Real high resulution display is becomming more common and there also are users using larege screens with lower resolutions(TVs). This combined with different font sizes tend to make the issue more noticable. Back when almost everyone did 96dpi, it was simpler to handle. Applications also tend to use some dynamic size tricks, unfortunatly not always with luck. In the end, it makes it less easy to fix. \r\n\r\nRegardless, a good start would be to check out all applications from a fresh user and identify the missbehaving applications. It's probably not a thing developers test often, and it's a trivial detail to notice compared to other more complex issues they work on. But first impressions count, and making the developers aware gives a bigger chancce the issue will get resolved.\r\n"
    author: "Morty"
  - subject: "Why didn't I?"
    date: 2014-04-06
    body: "Why didn't I submit a patch? Because I'm not a coder, that's why. But for someone who can code, fixing issues like proper default size of windows is probably very easy thing to fix. Or are you saying that increasing the default size of the app-window is very hard to do? \r\n\r\nThis attitude is one if the reasons I was sot turned off by KDE and free software in general. If you don't code, you are basically dead weight. Useless, unneeded, unwanted. If you point out a shortcoming or something, the answer is always \"fix it yourself\". "
    author: "Janne from the past"
  - subject: "Your comment; Martin's reply"
    date: 2014-04-07
    body: "You wrote that \"These are tiny things, easy to fix.\" Martin replied appropriately. He also wrote that fixing it took quite a bit of work. In other words, you did not know what you were talking about, even though you gave the impression that you did know.\r\n\r\nNow you've turned this into an unjustified complaint, unrelated to what Martin wrote. Check the KDE forums and the distro forums...it's simply not true that \"the answer is always 'fix it yourself'\"."
    author: "kallecarl"
  - subject: "bart.otten85@gmail.com"
    date: 2014-04-07
    body: "Showing the concept does not force anybody to setup the same activities. Yet, it DOES show the idea behind it.\r\n\r\nAs I speak for myself: I once created some and never used them as I did not find a benefit over virtual desktops. Yet, there is a huge difference between them that I (and a lot of other people) just don't know. Like energy saving modus being set per activity (and probably much more...but how to know?)"
    author: "BartOtten"
  - subject: "Idea: Send feedback widget"
    date: 2014-04-07
    body: "As I am not a developer, I have no idea how hard it is to do the following:\r\n\r\n- Create an widget which on click\r\n--  captures the screen\r\n-- saves the clicked app-name\r\n-- gives you a field to add a comment\r\n\r\nThis way it would be easy for novice testers to send in feedback. Not devs using Bugzilla, not nerds using the forum, not geeks using a Wiki. That way I could give my mom a copy and she could easily report what whe thinks should be improved or what is unclear.\r\n\r\nEnable the widget by default on Alpha and Beta and voila....."
    author: "BartOtten"
  - subject: "+1 to matching flat theme"
    date: 2014-04-15
    body: "+1"
    author: "travis"
  - subject: "Agree"
    date: 2014-05-02
    body: "+1 for Qt app looks"
    author: "Jinesh"
  - subject: "Not terribly hard, I think."
    date: 2014-05-14
    body: "Not terribly hard, I think. We even have something similar - likeback. It is used by Amarok. Downside is that it creates a lot of feedback you have to go through... Time you could spend on coding. The issue with these things is always that it is a trade-off :("
    author: "Anonymous"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/calendar.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/calendar_wee.png" /></a><br />Plasma Next: familiar yet polished</div>KDE today releases the <a href="http://www.kde.org/announcements/announce-plasma-2014.6-alpha1.php">first Alpha version of the next-generation Plasma workspace</a>. This kicks off the public testing phase for the next iteration of the popular Free software workspace, code-named "Plasma Next" (referring to the 'next' Plasma release-see below "A note on versioning and naming"). Plasma Next is built using QML and runs on top of a fully hardware-accelerated graphics stack using Qt 5, QtQuick 2 and an OpenGL(-ES) scenegraph. Plasma Next provides a core desktop experience that will be easy and familiar for current users of KDE workspaces or alternative Free Software or proprietary offerings. Plasma Next is <a href="http://techbase.kde.org/Schedules/Plasma/2014.6_Release_Schedule">planned to be released</a> as 2014.6 on the 17th of June.
<!--break-->

<iframe width="640" height="360" src="//www.youtube.com/embed/IfyXip_e1ms?rel=0" frameborder="0" allowfullscreen></iframe>

<h2>The converged workspace</h2>
Modern day computing device abilities are starting to blend with each other. Tablets can be used with a keyboard, phones can stream their screen contents to a television, laptops have gotten flip and touch screens. To deal with this, Plasma Next has been designed as a <em>converged workspace shell</em>. It will be able to switch on demand between workspaces optimized for these different form factors, like a tablet user interface turning into a traditional desktop workspace when paired with a keyboard and a mouse. Plasma will be easily extensible as new form factors emerge. 
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/kickoff2.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kickoff2_wee.png" /></a><br />Smoother Kickoff menu</div>
The mechanism to adapt to different form factors is fully implemented and functional, but, as there is only one workspace available right now, it is not useful at this point. In the months to come, the Plasma team plans to make available additional workspaces, such as the tablet-oriented Plasma Active user experience, and the media-consumption-targeted Plasma Mediacenter.

<em>A note on versioning and naming</em>: The code name "Plasma Next" always points to the upcoming release of Plasma, KDE's end user workspace. The current Alpha will become 2014.6, to be released in June of this year. If the team opts for a 6 month release cycle (still to be determined), Plasma Next will refer to the 2014.12 release once 2014.6 is out.

<h2>Ready for testing, not production</h2>
The workspace demonstrated in this pre-release is Plasma Desktop. It represents an evolution of known desktop and laptop paradigms. Plasma Next keeps existing workflows intact, while providing incremental visual and interactive improvements. Many of those can be observed in this technology preview, others are still being worked on. Workspaces optimized for other devices will be made available in future releases.

As an Alpha release, this pre-release is not suitable for production use. It is meant as a base for testing and gathering feedback, so that the initial stable release of Plasma Next in June will be a smooth ride for everybody involved and lay a stable foundation for future versions. Plasma Next is intended for end users, but will not provide feature parity with the latest 4.x release (although this is expected to be accomplished in a follow-up release). The team is concentrating on the core desktop features first, instead of trying to transplant every single feature into the new workspaces. The feature set presented in Plasma Next will suffice for most users, though some might miss a knob here and there. This is not because the Plasma team wants to remove features, but simply that not everything has been done yet. Of course, everybody is encouraged to help bringing Plasma back to its original feature set and beyond.
<div style="float: none; border: 1px solid grey; padding: 1ex; width: 600px; margin: 1ex"><a href="http://dot.kde.org/sites/dot.kde.org/files/wallpapers.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/wallpapers_wee.png" /></a><a href="http://dot.kde.org/sites/dot.kde.org/files/paneltoolbox.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/paneltoolbox_wee.png" /></a><a href="http://dot.kde.org/sites/dot.kde.org/files/notification.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/notification_wee.png" /></a><a href="http://dot.kde.org/sites/dot.kde.org/files/networkmanager.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/networkmanager_wee.png" /></a><br />left-to-right, top-to-bottom: Wallpaper dialog, panel toolbox, notifications and network manager in the system tray</div>
<h2>For developers</h2>
Plasma Next builds on top of Qt 5. With this transition, all QML-based UIs—which Plasma is built exclusively with—will make use of a new scenegraph and scripting engine, resulting in huge performance wins as well as architectural benefits, such as being able to render using available graphics hardware.

Plasma Next is the first complex codebase to transition to <a href="http://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a>, which is a modular evolution of the KDE development platform into leaner, less interdependent libraries.

<h2>For users</h2>
Users testing this Plasma pre-release are greeted with a more refined visual appearance. The new Breeze Plasma theme debuts in this pre-release with a flatter, cleaner look. Less visual clutter and improved contrast make Plasma Next a noticeable improvement over the current stable Plasma workspaces. There has been some polish to much of Plasma's default functionality, such as the system tray area, the notifications, the settings for the compositor and window manager, and many more. While it will feel familiar, users will notice a more modern workspace.
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/activities.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/activities_wee.png" /></a><br />New Activities chooser</div>
<h2>Installing and providing feedback</h2>
You can install Plasma Next directly from source. KDE's community wiki has <a href="http://community.kde.org/Frameworks/Building">instructions</a>. Some distributions have created packages; for an overview of Alpha 1 packages, see <a href="http://community.kde.org/Plasma/Next/UnstablePackages">this page</a>. You can provide feedback either via the <a href="irc://#plasma@freenode.net">#Plasma IRC channel</a>, <a href="https://mail.kde.org/mailman/listinfo/plasma-devel">Plasma-devel mailing list</a> or report issues via <a href="https://bugs.kde.org/enter_bug.cgi?product=plasma-shell&format=guided">bugzilla</a>. Plasma Next is also <a href="http://forum.kde.org/viewforum.php?f=287">discussed on the KDE Forums</a>. Your feedback is greatly appreciated. If you like what the team is doing, please let them know!