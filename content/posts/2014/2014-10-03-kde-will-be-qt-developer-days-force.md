---
title: "KDE will be at Qt Developer Days in Force"
date:    2014-10-03
authors:
  - "jriddell"
slug:    kde-will-be-qt-developer-days-force
comments:
  - subject: "Qt and cross-platform KDE "
    date: 2014-10-04
    body: "Given Qt's cross-platform mission, here's to hoping that that will become more than just an afterthought for KDE!\r\n\r\n(kept typing 'playform' ... Freudian slip? :))"
    author: "RJVB"
---
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right">
<a href="https://www.qtdeveloperdays.com/europe"><img src="https://www.qtdeveloperdays.com/sites/all/themes/qtdevdays14/images/developerdays_europe_land_pos.png" width="210" height="83" /></a>
</div>

<a href="https://www.qtdeveloperdays.com/europe">Qt Developer Days Europe</a> is next Monday to Wednesday in Berlin.  It features tutorials and talks on making the most of the Qt toolkit most KDE Software is based upon.  Since Qt opened up its development process a large part of KDE Frameworks development has been to ensure close cooperation between the two projects.  This has succeeded spectaularly well and at this Qt Dev Days an incredible over 50% of the speakers are active or past developers with KDE.

Some highlights include <a href="https://www.qtdeveloperdays.com/europe/designers-cookbook">The Designer's Cookbook</a> by VDG master Jens Reuterberg, Dan Leinir Turthra Jensen talking about Calligra Gemini in <a href="https://www.qtdeveloperdays.com/europe/lightning-talks#B3">2-in-1 Office Suite</a>, David Faure <a href="https://www.qtdeveloperdays.com/europe/additional-qt-libraries-outside-qt-project">will talk about inqlude.org and the KDE Frameworks</a> and David Edmundson will talk on <a href="https://www.qtdeveloperdays.com/europe/lightning-talks#A5">Plasma - A flexible workspace shell</a>.   Look at the <a href="https://www.qtdeveloperdays.com/europe/schedule">schedule</a> to see many more.

As usual slides from the talks should be up on the website after the event for those who can not be there.  KDE e.V. is a partner organisation of Qt Dev Days with vice-president Aleix Pol representing our community to anyone who wants to find out more.
<!--break-->
