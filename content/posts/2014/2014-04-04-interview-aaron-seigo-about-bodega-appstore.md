---
title: "Interview with Aaron Seigo About Bodega Appstore "
date:    2014-04-04
authors:
  - "jospoortvliet"
slug:    interview-aaron-seigo-about-bodega-appstore
comments:
  - subject: "Great interview "
    date: 2014-04-05
    body: "This project needs to stay in the spotlight and continue to be discussed. Good to see it posted here, along with relevant links."
    author: "James Cain"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey; text-align: center;"><a href="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_Welcome.png"><img alt="" src="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_Welcome-300x235.png" /></a><br />
<strong><span style="color: #800000;">Welcome to the Bodega store!</span></strong></div>
The openSUSE News site <a href="https://news.opensuse.org/2014/04/03/bodega-app-stores-and-the-open-build-service/">features an interview<a/> with <a href="http://aseigo.blogspot.de/">Aaron Seigo</a> about <a href="http://community.kde.org/Bodega">Bodega</a>, a content store technology developed under the KDE umbrella. We replicate the KDE-relevant parts of the article here.
<h2>What is Bodega?</h2>
<p>First off, let&#8217;s find out what Bodega is all about. Aaron explains:</p>
<blockquote><p>Bodega is a store for digital stuff. In fancy words: it creates a catalog of metadata which represents digital assets.</p></blockquote>
<p>The most important thing is of course the &#8216;digital asset&#8217; term. That can be anything. For example, applications. Applications can be self contained &#8211; think how android does its APK files. Of course, things on Linux are often more complicated. Apache isn&#8217;t exactly a self-contained thing. And look further &#8211; perl, php, ruby, they all have their own addons like gems that need managing. Generalizing further, there are manuals. And books in general. Music, movies, pictures, you can go on.</p>
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey; text-align: center;"><a href="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_Account.png"><img alt="" src="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_Account-300x235.png" /></a><br />
<strong><span style="color: #800000;"> Setting up a Bodega account</span></strong></div>
<p>Of course, the competition has these too &#8211; look at Apple or Google.</p>
<h3>And how about Linux&#8230;</h3>
<p>Linux does not have a store where you can get such a wide variety of things. For a game, you can use Appstream, get it from Apper or GNOME&#8217;s software center. They all give a view on applications. Unfortunately, that is only useful for desktops and can handle things barely above the level of Angry Birds. If you want a python module as developers &#8211; these fancy tools won&#8217;t help you. Nor are they useful on servers. For those you have to rely on command line tools or even do things completely by hand. And it is all different between distributions.</p>
<p>Going further, where do you get documentation? For openSUSE, that&#8217;s <a href="http://activedoc.opensuse.org">activedoc</a> or the forums or <a href="http://en.opensuse.org/SDB">our support database on the wiki</a>. Not from zypper. Music &#8211; you can get that from <a href="http://magnatune.com">Magnatune</a> and so on.</p>
<blockquote><p>What if you can have one place where you can get a book, game, applications, isn&#8217;t that nice? That is what Bodega is.</p></blockquote>
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey; text-align: center;"><a href="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_MainScreen.png"><img alt="" src="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_MainScreen-300x235.png" /></a><br />
<strong><span style="color: #800000;">The main screen of the store</span></strong></div>
<h2>How is Bodega different?</h2>
<p>So, Bodega offers a digital store which can handle a wider variety of things than our current solutions. But what sets it apart from proprietary technologies like the Playstore and of course Canonical&#8217;s store solution? Aaron:</p>
<blockquote><p>Most Linux solutions like Appstream assume their audience are users who play Angry Birds and use spreadsheets. Fair enough. Bodega takes a different approach and is far more ambitious.</p></blockquote>
<p>Bodega has all the meta data in one place and offers &#8216;stores&#8217; which are views on that data. That means you can have a software developer store, for example listing all languages and their addons separate; and a server section etc. And a separate UI for the angry-bird-and-spreadsheet crowd. All from the same bodega system, filtered by tags (not static categories!).</p>
<p>Talking about Appstream, Bodega can of course benefit from the metadata gathered for Appstream. And GNOME&#8217;s Software Center could be reworked to be a front-end to Bodega, adding books, music and lots of other digital data to its store. This is not meant to be a rewrite of what is there, or an isolated effort!</p>
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey; text-align: center;"><a href="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_ownCloud.png"><img alt="" src="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_ownCloud-300x235.png" /></a><br />
<strong><span style="color: #800000;">An application in the store</span></strong></div>
<h3>And why would you build on Bodega?</h3>
<p><strong>Bodega is open</strong>: everybody can quite easily add their own stores; or their own data sources; and add content and even sell it through their channels. It is not a closed system, on the contrary.</p>
<p>Open is a <em>must</em>, especially for Linux:</p>
<blockquote><p>Take the 440.000 users of openSUSE. That would be a minimal amount of sales&#8230; The top-10 of paid apps in ubuntu makes less than a $100 per month of sales. Not really worth the effort. But if we could aggregate the sales between distributions, it would become relevant for third-party developers. Bodega as a cross-distribution is important!</p></blockquote>
<p>And Bodega is useful for people outside of Linux. You can have your store on your own website so it is realistically possible for a independent author to sell their books in a bodega instance on their own website and never even SEE Linux. Yet the openSUSE users can get the books and benefit from the larger ecosystem&#8230;</p>
<blockquote><p>The beauty of it is that it is all Free and Open Source Software, front and back. You can self-host all you want.</p></blockquote>
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey; text-align: center;"><a href="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_Preview.png"><img alt="" src="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_Preview-300x230.png" /></a><br />
<strong><span style="color: #800000;">Preview of a wallpaper</span></strong></div>
<h2>Current state</h2>
<p>You might be eager to find out what is there, today. Well, if you&#8217;ve seen the screenshots to the side, you know there is an app to access the store. It is build for touch screens but works just fine and you can get it in openSUSE <a href="http://software.opensuse.org/package/bodega-client">through software.opensuse.org</a>. Once installed, you can fire it up typing &#8220;active-addons&#8221; in a run command dialog.</p>
<p>Shawn Dunn (of cloverleaf fame) is putting together a more traditional desktop UI, while maintaining these packages as well. You will be able to have a conversation with him as he&#8217;s going to be at the <a href="http://conference.opensuse.org">openSUSE Conference in Dubrovnik</a> this month where he will present a session about Bodega! He is known as SFaulken online and pretty much always hangs in the <a href="irc://#opensuse-kde@freenode.net">#opensuse-kde channel on Freenode</a> where you can ask how to get things running or how to help him break stuff anytime. He&#8217;s also yelling at the world on <a href="https://plus.google.com/+ShawnWDunn/posts">google plus</a>.</p>
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey; text-align: center;"><a href="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_Famous.png"><img alt="" src="https://news.opensuse.org/wp-content/uploads/2014/03/Bodega_Famous-300x230.png" /></a><br />
<strong><span style="color: #800000;">Famous books included!</span></strong></div>
<p>Bodega now contains the entire book set of Project Gutenberg (thousands of awesome, free books) as well as a number of wallpapers and applications. Aaron:</p>
<blockquote><p>There is work to be done to include all openSUSE Software in Bodega. The store can use a little work too, but is based on QML which makes it very easy to improve. If you&#8217;re interested in helping out, let us know!</p></blockquote>
<p>You can contact Aaron on IRC as aseigo in the <a href="irc://#active@freenode.net">#plasma active channel on Freenode</a>, ping him on <a href="https://plus.google.com/+AaronSeigo/posts">Google+</a> or shoot him a mail on aseigo on the KDE.org servers.</p>