---
title: "KDE Commit-Digest for 19th January 2014"
date:    2014-02-23
authors:
  - "mrybczyn"
slug:    kde-commit-digest-19th-january-2014
comments:
  - subject: "gdrive and box support in kde-pim"
    date: 2014-02-24
    body: "I think of kde-pim as being akonadi, kmail, korganizer and so on. What does gdrive and box.com support mean there? Will something from pim handle file sync? "
    author: "Christian"
  - subject: "gdrive and box support in kde-pim"
    date: 2014-02-24
    body: "It allows sharing big attachments in storage service.\r\nSo when you have big attachment and your provider doesn't like it, you can put on these services.\r\nI created an application to manage the services too => you can upload file/download file/ copy/paste/ create folder etc.\r\n\r\nRegards"
    author: "Montel"
  - subject: "How about ownCloud?"
    date: 2014-02-24
    body: "Hi Laurent and thanks for all your work on kdepim!\r\n\r\nI'm not using kmail's latest version yet so I'm curious about ownCloud for big attachments.\r\n\r\nThanks!"
    author: "ricardo.barberis"
  - subject: "How about ownCloud?"
    date: 2014-02-25
    body: "we use webdav for it\r\nand all is implemented.\r\nRegards"
    author: "Montel"
---
In <a href="http://commit-digest.org/issues/2014-01-19/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://kate-editor.org/">Kate</a> gets new features in the vim mode</li>
<li>All built-in effects move into a library KWin links against</li>
<li><a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kdecore/html/classKTranscript.html">KTranscript module</a> for dynamic translation moved from <a href="http://api.kde.org/4.7-api/kdelibs-apidocs/kjs/api/html/index.html">KJS</a> to <a href="http://en.wikipedia.org/wiki/QtScript">QtScript</a></li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> starts supporting gdrive and Box.com</li>
<li>KNavalbattle makes the ship red if it cannot be placed instead of hiding it</li>
<li><a href="http://uml.sourceforge.net/">Umbrello</a> makes Enable/Disable Undo option work</li>
<li>Bug fixes in <a href="http://okular.org/">Okular</a>, <a href="http://konsole.kde.org/">Konsole</a>, KDE-PIM, Calligra.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-01-19/">Read the rest of the Digest here</a>.