---
title: "KDE Commit-Digest for 20th April 2014"
date:    2014-06-03
authors:
  - "mrybczyn"
slug:    kde-commit-digest-20th-april-2014
---
In <a href="http://commit-digest.org/issues/2014-04-20/">this week's KDE Commit-Digest</a> you can find an interview with Aaron Seigo about KF 5 and a look into KDE SC on devices. And of course, the overview of the development effort is there as every week and includes:

              <ul><li>Ktexteditor adds keyword-based completion model</li>
<li>Smb4k changes the implementation of notifications correctly, as a side-effect the notifications can be edited via system settings</li>
<li>libtmdbqt adds basic tv show support.</li>
</ul>

                <a href="http://commit-digest.org/issues/2014-04-20/">Read the rest of the Digest here</a>.
