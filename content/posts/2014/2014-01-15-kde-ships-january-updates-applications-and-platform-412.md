---
title: "KDE Ships January Updates to Applications and Platform 4.12"
date:    2014-01-15
authors:
  - "unormal"
slug:    kde-ships-january-updates-applications-and-platform-412
---
Today KDE released <a href="http://www.kde.org/announcements/announce-4.12.1.php">updates for its Applications and Development Platform</a>, the first in a series of monthly stabilization updates to the 4.12 series. Starting with the next Applications and Development Platform release, 4.12.2, there will also be a maintenance release of Workspaces 4.11.6. This release contains only bugfixes and translation updates; it will be a safe and pleasant update for everyone.

There were more than 45 improvements to the Personal Information Management suite Kontact, the UML tool Umbrello, the document viewer Okular, the web browser Konqueror, the file manager Dolphin and others. Umbrello standardizes the global, diagram and widget settings and adds a clone diagram function. Dolphin received a fix to a bug that slowed down PDF preview under certain circumstances. In Kontact, several bugs and regressions were fixed in the KOrganizer, Akregator and KMail components.

A more complete <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2013-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.12.1&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">list of changes</a> can be found in KDE's issue tracker.