---
title: "Akademy 2014 Call for Papers"
date:    2014-04-30
authors:
  - "sealne"
slug:    akademy-2014-call-papers
---
Akademy is <strong>the</strong> KDE Community conference. It is where we meet, discuss plans for the future, get inspired, learn and get work done. If you are working on topics relevant to KDE, this is your chance to present your work and ideas at the Conference from September 6-12 in Brno, Czech Republic. The main days for talks are Saturday 6th and Sunday 7th of September. The rest of the week will be BoFs, unconference sessions and workshops.

<div style="float: center; padding: 1ex; margin: 1ex;"><a href="http://byte.kde.org/~duffus/akademy/2013/groupphoto/"><img src="http://dot.kde.org/sites/dot.kde.org/files/ak2013-group-photo700.jpg" /></a><br />Akademy 2013 attendees (click for larger) <small><small>by Knut Yrvin</small></small></div>

<h2>What we are looking for</h2>

The goal of the conference section of Akademy is to learn and teach new skills and share our passion around what we're doing in KDE with each other.

For the sharing of ideas, experiences and state of things, we will have short Fast Track sessions in a single-track section of Akademy. Teaching and sharing technical details is done through longer sessions in the multi-track section of Akademy.

If you think you have something important to present, please tell us about it. If you know of someone else who should present, please nominate them. For more details see the proposal guidelines and the <a href="http://akademy.kde.org/2014/cfp">Call for Papers</a>. The <a href="https://conf.kde.org/en/Akademy2014/cfp/session/new">submission</a> deadline is Sunday 18th May, 23:59:59 CEST.

<h2>About Akademy 2014 Brno, Czech Republic</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. <a href="http://akademy.kde.org/2014">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.