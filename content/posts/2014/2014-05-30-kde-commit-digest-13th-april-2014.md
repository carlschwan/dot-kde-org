---
title: "KDE Commit-Digest for 13th April 2014"
date:    2014-05-30
authors:
  - "dannya"
slug:    kde-commit-digest-13th-april-2014
---
In <a href="http://commit-digest.org/issues/2014-04-13/">this week's KDE Commit-Digest</a>:

              <ul><li>Exponential zoom in/out feature in "Solar System Viewer" of <a href="http://edu.kde.org/kstars/">KStars</a></li>
<li>Lessons pane gets expand / collapse all buttons in <a href="http://edu.kde.org/parley/">Parley</a></li>
<li>Fixes to placement of KRunner on multi-screen setups</li>
<li>More grid presets and shortcuts for toggle and snap, and improvements to the EXR file format converter in Calligra</li>
<li>Fixes to opening documents in new tabs in <a href="http://okular.org/">Okular</a></li>
<li>Speedups in item synchronization in <a href="http://pim.kde.org/akonadi/">Akonadi</a></li>
<li>Notification support removed from KGlobalAccel</li>
<li>Further progress in ktp-text-ui and libqgit2</li>
<li><a href="http://dolphin.kde.org/">Dolphin</a> starts to support being compiled with KF5.</li>
</ul>

                <a href="http://commit-digest.org/issues/2014-04-13/">Read the rest of the Digest here</a>.