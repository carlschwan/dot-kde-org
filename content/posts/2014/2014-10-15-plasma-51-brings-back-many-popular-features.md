---
title: "Plasma 5.1 Brings Back Many Popular Features"
date:    2014-10-15
authors:
  - "jriddell"
slug:    plasma-51-brings-back-many-popular-features
comments:
  - subject: "kde 5.0"
    date: 2014-10-16
    body: "Too white for my liking. "
    author: "archuser"
  - subject: "Release Parties for 5.1"
    date: 2014-10-16
    body: "If you're a party fan or just want to learn more, join one of the release parties or organize your own!\r\n\r\nSee <a href=\"https://community.kde.org/Promo/Events/Release_Parties/Plasma5.1\">the 5.1 party page</a> for the planned parties and the <a href=\"https://community.kde.org/Promo/Events/Release_Parties\">general release party page</a> for info on how to organize your own.\r\n\r\nFun guaranteed!"
    author: "jospoortvliet"
  - subject: "there is choice!"
    date: 2014-10-16
    body: "That is OK, style is a matter of taste! There are dark themes as well if that is more your thing. Try it out and share a screenshot here so others can see what it looks like in dark ;-)"
    author: "jospoortvliet"
  - subject: "That's not the worst design idea"
    date: 2014-10-16
    body: "Too white, yes. It seems that mobiles' interface design is influenced KDE's design team a little too much. But the default theme isn't a big annoyance since you can choose a dark theme or set your own color combination. What I find more disagreeable is the text boxes and tabs size, aprox 3 times the font's height. Isn't that exaggerated? Again they seem to be designed for mobile phones, made to be handled with our fingers ergo these elements need to be big enough for a human finger, but for computers, handled whit a mouse? Do you, devs, plan at least make that size configurable? I haven't found any option in Systemsettings.\r\nOther than that and the frequent plasmashell crashes, presumably solved in next updates, this version feels more agile and responsive than ever. Wonderful job, guys. Sincere contratulations. :)"
    author: "Javier Y\u00e1\u00f1ez"
  - subject: "Systemsettings"
    date: 2014-10-17
    body: "So, what happened to the redesign of system settings? Shouldn't it be refreshed for the 5.1 cycle? \r\n\r\nAnd what about all the other things people talked about after the initial 5.0 release?"
    author: "Burke"
  - subject: "I like it. Elegant."
    date: 2014-10-21
    body: "I like it. Elegant. \r\nTitlebar buttons, however, need some work. They need to integrate better with the colorscheme, and a more squared shape, I think, would fit the theme more.\r\n\r\nOverall a step in the right direction, for Linux desktops. \r\n"
    author: "Anonymous"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: solid thin grey; background-image: none; text-align: center">
<a href="https://www.kde.org/announcements/plasma-5.1/plasma-main.png">
<img src="https://www.kde.org/announcements/plasma-5.1/plasma-main_wee.png" style="padding: 1ex; margin: 1ex; border: 0; background-image: none;" width="400" height="225" /></a><br />
Plasma 5</div>

Today, <a href="https://www.kde.org/announcements/plasma-5.1/">KDE releases Plasma 5.1</a>, the first release containing new features since the release of Plasma 5.0 this summer. Plasma 5.1 sports a wide variety of improvements, leading to greater stability, better performance and new and improved features. Thanks to the feedback of the community, KDE developers were able to package a large number of fixes and enhancements into this release, among which more complete and higher quality artwork following the new-in-5.0 Breeze style, re-addition of popular features such as the Icon Tasks taskswitcher and improved stability and performance.

Those travelling regularly will enjoy better support for time zones in the panel's clock, while those staying at home a revamped clipboard manager, allowing you to easily get at your past clipboard's content. The Breeze widget style is now also available for Qt4-based applications, leading to greater consistency across applications. The work to support Wayland as display server for Plasma is still ongoing, with improved, but not complete support in 5.1. <a href="https://community.kde.org/Plasma/5.1_Changes">Changes</a> throughout many default components improve accessibility for visually impaired users by adding support for screenreaders and improved keyboard navigation.

Aside from the visual improvements and the work on features, the focus of this release lies also on stability and performance improvements, with over 180 bugs resolved since 5.0 in the shell alone. Plasma 5.1 requires <a href="https://www.kde.org/announcements/kde-frameworks-5.3.0.php">KDE Frameworks 5.3</a>, which brings in another great amount of fixes and performance improvements on top of the large number of fixes that have gone into Plasma 5.1. If you want to help to make more of this happen, consider <a href='https://www.kde.org/fundraisers/yearend2014'>a donation</a> to KDE, so we can support more developers getting together to make great software.
<!--break-->
<ul>
<li>
<a href='https://community.kde.org/Plasma/5.1_Changes'>5.1 Changes List</a>
</li>
</ul>

<br clear="all" />
<h2>Artwork and Visuals</h2>
<p>
<br clear="all" />
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<a href="https://www.kde.org/announcements/plasma-5.1/qt4_widgets_plus_icontasks.jpg"><img src="https://www.kde.org/announcements/plasma-5.1/qt4_widgets_plus_icontasks-wee.jpg" width="500" height="281"/></a><br />
Breeze Theme for Qt 4
</div>
A new Breeze widget theme for Qt 4 lets applications written with KDE Platform 4 fit in with your Plasma 5 desktop.

The Breeze artwork concept, which has made its first appearance in Plasma 5.0 has seen many improvements. The icon set is now more complete. The icons in the notification area in the panel have been touched up visually. A new native widget style improves rendering of applications used in Plasma. This new native style also works for Qt 4 letting applications written with KDE Platform 4 fit in with your Plasma 5 desktop. There is a <a href="https://www.kde.org/announcements/plasma-5.1/plasma-lookandfeel.png">new System Settings module</a> that lets you switch between desktop themes.

Overall, Plasma 5.1's Look and Feel refines the experience found in 5.0 noticeably. Behind all these changes are improvements to the <a href="https://techbase.kde.org/Projects/Usability/HIG">Human Interface Guidelines</a>, which have led to a more consistent overall user experience.
<br clear="all" />
<h2>New and Old Features</h2>
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<a href="https://www.kde.org/announcements/plasma-5.1/icons_task_manager.jpg">
<img src="https://www.kde.org/announcements/plasma-5.1/icons_task_manager-wee.jpg" width="400" height="58" style="border: 0px" />
</a>
Icons-only Task Manager</div>
Plasma 5.1 brings back many features that users have grown used to from its 4.x predecessor. Popular additional widgets such as the <em>Icons-only Task Manager</em>, the <em>Notes</em> widget and the <em>System Load Viewer</em> make their re-entry. Support for multiple time zones has been added back in the panel's clock. The notifications have been visually improved, along with many bigger and smaller bug fixes.
<br clear="all" />
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<a href="https://www.kde.org/announcements/plasma-5.1/alt_switcher.jpg">
<img src="https://www.kde.org/announcements/plasma-5.1/alt_switcher-wee.jpg" width="320" height="260" style="border: 0px" /></a><br />
Applet Switcher
</div>
A new feature allows you to easily switch between different widgets which share the same purpose. Changing the application launcher for example has become much easier to discover. Plasma panels have new switchers to easily swap between different widgets for the same task. You can select which application menu, clock or task manager you want with ease. The new <em>Clipboard</em> widget offers a redesigned user interface on top of Plasma's venerable clipboard manager, allowing the user to easily use the clipboard's history and preview files currently in the clipboard. Plasma's alternative launcher, <em>Kicker</em> has seen a large number of <a href="https://community.kde.org/Plasma/5.1_Changes#Kicker_Application_Menu">improvements</a>, among which better accessibility and integration with the package manager.

Thanks to two Google Summer of Code projects, the Plasma Media Center and tablet-centric Plasma Active user experiences now have basic ports available from Git, but are not release-quality yet.
<br clear="all" />
<h2>Wayland</h2>
Further progress has been made on Wayland support. A new window manager binary 'kwin_wayland' now complements the existing 'kwin_x11', and is equipped with the ability to start a nested X server for compatibility with X11-based applications. A newly-created KWayland library provides Wayland setup information to KInfoCenter and other consumers. More work is needed and ongoing to run the Plasma workspace on Wayland; we expect this to bear fruit for end-users in 2015.
<h2>Suitability and Updates</h2>
Plasma 5.1 provides a core desktop with a feature set that will suffice for many users. The development team has concentrated on tools that make up the central workflows. While many features known from the Plasma 4.x series are already available in Plasma 5.1, not all of them have been ported and made available for Plasma 5 yet. As with any software release of this size, there may be bugs that make a migration to Plasma 5 hard for some users.  The development team would like to hear about issues you may run into, so they can be addressed and fixed. We have compiled a <a href="https://community.kde.org/Plasma/5.1_Errata">list of problems</a> we are aware of, and working on. Users can expect monthly bugfix updates. A release bringing new features and bringing back even more old features will be made in early 2015.

<ul>
<li>
<a href='https://community.kde.org/Plasma/5.1_Errata'>5.1 Known Bugs</a>
</li>
</ul>

<br clear="all" />

<!-- // Boilerplate again -->

<h2>Live Images</h2>

The easiest way to try it out is  with a live image booted off a USB disk.  Images are available for development versions of <a href='http://cdimage.ubuntu.com/kubuntu-plasma5/'>Kubuntu Plasma 5</a>.

<h2>Package Downloads</h2>

Distributions have created, or are in the process of creating, packages listed on our wiki page.
<ul><li>
<a href='https://community.kde.org/Plasma/Packages'>Package download wiki page</a>
</li></ul>

<h2>Source Downloads</h2>

You can install Plasma 5 directly from source. KDE's
community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.

<ul>
<li>
<a href='https://www.kde.org/info/plasma-5.1.0.php'>Source Info Page</a>
</li>
</ul>

<h2>Feedback</h2>

You can give us feedback and get updates on <a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a> or <a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a> or <a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>.
<p>
Discuss Plasma 5 on the <a href='https://forum.kde.org/viewforum.php?f=289'>KDE Forums Plasma 5 board</a>.</a>
</p>

<p>You can provide feedback direct to the developers via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>Bugzilla</a>.  If you like what the team is doing, please let them know!</p>

<p>Your feedback is greatly appreciated.</p>

<h2>Supporting KDE</h2>

We produce beautiful software for your computer, please we'd love you to join us improving it or helping fellow users.  If you can't find the time to contribute directly do consider <a href='https://www.kde.org/fundraisers/yearend2014'>sending a donation</a>, help to make the world a better place!
