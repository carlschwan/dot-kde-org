---
title: "KDE Commit-Digest for 23rd March 2014"
date:    2014-05-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-23rd-march-2014
---
In <a href="http://commit-digest.org/issues/2014-03-23/">this week's KDE Commit-Digest</a>:

<ul><li>KDE Frameworks changes default font settings to Oxygen font</li>
<li>KDevelop improves Custom Buildsystem Plugin, and it is now possible to define macros and add include directories/files through GUI and the GUI is always accessible by clicking on Project->Open Configuration</li>
<li>Kwin introduces a new Effect Loading mechanism</li>
<li>Trojita distinguishes between IMAP errors and network failures</li>
<li>In KDEPIM, work starts on mailmerge support</li>
<li>Skrooge can reorder the suboperations in the split operation</li>
<li>Plasma MediaCenter adds a Baloo plugin</li>
<li>Baloo reduces memory usage of the Akonadi indexer.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-03-23/">Read the rest of the Digest here</a>.
<!--break-->