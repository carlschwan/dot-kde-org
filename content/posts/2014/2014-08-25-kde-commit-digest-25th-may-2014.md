---
title: "KDE Commit-Digest for 25th May 2014 "
date:    2014-08-25
authors:
  - "mrybczyn"
slug:    kde-commit-digest-25th-may-2014
comments:
  - subject: "commit digest rocks!"
    date: 2014-08-25
    body: "yay, commit digest back!!! Awesome, I wish I had some time to contribute but I don't... :(\r\n\r\nBillions of thanks to the contributors, Marta Rybczynska, Alex Fikl and Danny Allen!!!\r\n\r\nAnybody else who wants to keep this alive and moving forward?!?"
    author: "jospoortvliet"
---
In <a href="http://commit-digest.org/issues/2014-05-25/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://amarok.kde.org/">Amarok</a> implements popular demand to restore scroll location when collection filter is cleared; adds a new option to support icon-view large thumb size (over 256x256 px)</li>
<li>Plasma desktop streamlines comment fields of KCMs by applying common language and type-setting to the systemsettings modules in kde-workspace</li>
<li><a href="http://kopete.kde.org/">Kopete</a> adds support for SOCKS5 proxy in ICQ protocol</li>
<li><a href="http://uml.sourceforge.net/">Umbrello</a> sees work on UML 2.0</li>
<li><a href="https://krita.org/">Krita</a> adds the indexed color filter</li>
<li>Porting to KF5/Qt5 continues, including massif-visualizer and partitionmanager.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-05-25/">Read the rest of the Digest here</a>.