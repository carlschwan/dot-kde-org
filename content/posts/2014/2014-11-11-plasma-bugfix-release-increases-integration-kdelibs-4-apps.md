---
title: "Plasma Bugfix Release Increases Integration with KDELibs 4 Apps"
date:    2014-11-11
authors:
  - "jriddell"
slug:    plasma-bugfix-release-increases-integration-kdelibs-4-apps
---
This month the Plasma 5 team brings you <a href="https://www.kde.org/announcements/plasma-5.1.1.php">5.1.1, a bugfix release</a> to polish up the offering.  It includes all the latest translations and a bunch of bugfixes.  The bugfixes include syncing settings better with kdelibs4 applications so if you select which web browser you prefer to use that will affect all KDE applications.  The VDG team have also continued their impressive work with numberous tweaks to the Breeze and Oxygen styles to get those margins just right as well as improve support for right-to-left languages.