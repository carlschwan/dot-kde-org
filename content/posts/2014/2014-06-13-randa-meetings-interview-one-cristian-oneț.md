---
title: "Randa Meetings Interview One: Cristian One\u021b"
date:    2014-06-13
authors:
  - "devaja"
slug:    randa-meetings-interview-one-cristian-oneț
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img width="200" height="225"  src="http://www.kmymoney.org/images/people/cristian.png" /><br />Cristian Oneț</div>

This is one of our first interviews with the excited attendees of the <a href="http://www.kde.org/fundraisers/randameetings2014/">Randa meetings</a> and today you shall get a glimpse into the mind, workings and makings of Cristian Oneț who has been with KDE since quite some time now and has been a prominent contributor.

<em>Could you describe yourself in a few lines and tell us where you're from?</em>

My name is Cristian Oneț, I'm a software developer. I live in Timișoara, Romania. At my day job I work on developing/maintaining a suite of desktop applications on Windows (using Qt lately). I'm also a member of the <a href="http://www.kmymoney.org">KMyMoney</a> development team.

<em>How did you first chance upon KDE? Could you describe your journey in short?</em>

My first contact with KDE was back in the 3.x days (I think it was 3.2). I was just starting to get familiar with Linux (first years at the Computer Science Faculty) and I was looking for a desktop that looked and felt good. KDE's workspace was my pick then and it stayed that way ever since.

<em>Why is KDE so special to you?</em>

It's the most visible part of my computer. By using it and contributing to its improvement it allowed me to grow as a developer. It feels good to be able contribute to something you find useful and to do it in a fun way.

<em>Will this be your first time in Randa?</em>

Yes it's my first time.

<em>When did you first hear about the meetings in Randa and why do you wish to be a part of it?</em>

I've heard about previous meetings in Randa from reading Planet KDE. I didn't really think that I'll ever participate but this year I was contacted by Mario Fux with the proposal to help port KMyMoney to KF5. After a short exchange of e-mails I decided that it would be nice to be there.

<em>Which specific area of KDE software do you contribute to? Could you give a brief overview?</em>

I'm a part of the team that develops <a href="http://www.kmymoney.org">KMyMoney</a> the KDE personal finances manager application. I also had small contributions (mostly small patches) in other parts of KDE software (kdepim, kdelibs), most of these were fixes for problems that I encountered using KDE software or developing KMyMoney. Last but not least, I also contributed with Romanian translations since I believe that software should be properly internationalized.

As a KMyMoney developer one of the biggest task that I contributed to was porting it to KDE Platform 4. This was a great chance to get familiar with Qt's MVC programming. That period was one of the biggest wave of development on the project lately. After porting the application to KDE Platform 4 the port to Windows followed. That was also fun since I got to know some KDE Windows project members on the way.

<em>How do you manage to balance your job and contribution to KDE?</em>

I try to do both in a way that makes me happy with the work I'm doing. My KDE contribution can keep me happy as a developer which is not always possible at my job. There is also a limit to what I can do when it  comes to contribution and if the time's consumed by my job I can't really contribute much. I'm usually productive as an Open Source contributor after my summer holiday. Contributions are also influenced by the feedback of the community and the development team. I find that it is usually easier to fix problems that effect a lot of people.

<em>You work on the windows platform during your job and have an in-depth understanding of it. But you prefer to use Linux as your primary OS. Could you give us a few reasons why someone should make the switch to Open Source?</em>

Yes, I always preferred Linux but that preference is pretty influenced by the way I relate to computers. I think that anyone who desires freedom of information should use Open Source, but of course, this is a disputable statement. The counter argument would be that one is only free if he has the knowledge and time to fix stuff that's broken. It's nice that the knowledge is out there but that does not really help somebody who just needs things to work.

I came a long way learning about computers by using Linux (Gentoo Linux that is) and I'm thankful for that. Still, I find myself once in a while after an update mumbling about some stuff that just broke because somebody thought it should be re-written from scratch. Not trying to send forth a wrong message, I know that there are problems on other platforms as well but on Linux they tend to be more frequent (probably caused by the faster release cycles). That's when the freedom to change stuff gets handy.

<em>As a person who has been with KDE since his student days; what would your advice be to the students who are currently contributing to KDE to keep them motivated to continue development when they start working on a fully fledged job?</em>

I would advise them to do what they enjoy doing. If they enjoy contributing to Open Source now then that probably won't change and they will keep doing it after they have a job. If they really enjoy Open Source they could be looking for a job on an Open Source project if they have the opportunity. Meeting the people they work with in Open Source could be also creating a kind of connection that would keep them contributing even when they have less free time in the future. Last but not least Open Source can be a kind of "escape" where one can really do the things they like when there is no such freedom at a job.

<div style="float: left; padding: 1ex; margin: 1ex;"><img width="400" height="225"  src="/sites/dot.kde.org/files/KMyMoney.png" /><br />The Randa Meetings organizers use KMyMoney for their finances.</div>

<em>Since you are working on KMyMoney on both Windows and Linux could you describe the particulars of the development process in both and which one you prefer to work on?</em>

I only developed KMyMoney on Linux, on Windows I only work on platform specific issues. But I can compare the two development platforms using the experience I have in C++ development on Windows at my job. My opinion is that except for the debugger; the tools on Linux are much more developer friendly. I use KDevelop, I love it's syntax highlighting, symbol navigation and documentation features but it still crashes once in a while (mainly while switching branches in Git). It's great to edit code but the integration with gdb does not seem so smooth as Microsoft Visual Studio's debugger. Code highlighting and navigation can also be improved on Windows with some add-ons. I have heard a lot about Qt Designer but I really like KDevelop and I can live with the debugger (it works 90 % of the time).

The KDE Platform is still pretty unstable on Windows and this was causing a lot of issues with the deployment once the application was ported. I guess this is caused by the fact that KDE software is mainly developed on Linux. The KDE on Windows team did a great job of trying to patch things to make them work on Windows but it seems it's hard to keep up with the pace KDE software is being developed. That's why, once we had our hands on a good KDE Windows release (that was 4.10.5 but it still needed custom patches), we stuck with it in the standalone installer that we provide. I would like KDE to focus on making the platform more stable than always looking at the next big thing in UI design.

I think that on Windows users only care about applications, if they would like to use the whole desktop they would definitely switch to Linux.

So the answer to your question is: I prefer to develop on Linux but I would also like the framework to be cross platform and so I would like to contribute to improve this situation.

<em>Have you got anything in particular planned for Randa?</em>

As I mentioned earlier hopefully I will be able to finish my task of porting KMyMoney to KF5 as well as meet KDE Windows project members, learn how KF5 will improve packaging on Windows and have fun while doing all that.

<em>What will you be looking forward to the most in the Randa Sprint? Any expectations or hopes of what it will be like? Any particular people or projects you are looking to collaborate on/with in Randa? Any targets set on completing with respect to development?</em>

The most interesting will be meeting the people that attend. I would start with some KDE Windows project members since I've been working with some of them while we ported KMyMoney to Windows. Packaging on Windows is still pretty hard so I would expect this to be improved. I would like to discuss about this and see if I could contribute since I'm at home in C++ development on Windows (it's my job).

<em>What does KDE mean to you and what role has it played in shaping you as a contributor/developer?</em>

It's my desktop of choice which I've been using for more than 10 years now. I really enjoy working with KDE/Qt as a developer since I think both have some of the most well designed API in the world of C++ frameworks/libraries. Since we use Qt at my job it was pretty useful to have previously worked with it.

<em>Why do you think Meetings such as Randa are very important for KDE and for Open Source communities around the globe?</em>

I've participated only once at a KDE related developer meeting. It was the <a href="https://dot.kde.org/2010/07/15/successful-kde-finances-sprint-held">KDE Finances Sprint in 2010</a>. I felt that it was really nice that I could meet the people I was working with face to face. Such a meeting can create different kind of connections than an acquaintance using the usual (e-mail, irc) communication channels.

<em>Why do you think supporting them is of importance and how has the support helped you as a KDE developer and an Open Source contributor?</em>

Building on my previous answer I think that it's important to build well knit teams. People who meet in person work better together, at least that is the experience I've had while working on KMyMoney.  Our meeting gave the team a big boost so if KDE is to move forward at a good pace it needs to encourage and support developer meetings. As for me as a developer it was a real pleasure to get to know my colleagues who came from different parts of the world to see the similarities and the differences between us.

<em>Could you briefly describe a rough outline of what you'd imagine your typical day in Randa this time around to be?</em>

I guess it will be similar to the days we had at the <a href="https://dot.kde.org/2010/07/15/successful-kde-finances-sprint-held">KDE Finances sprint</a>. After breakfast meetings, lunch then meetings again then some socializing over a beer in the evening.

<em>Is this your first time to Switzerland? Are you excited about being in another country?</em>

Yes, I've never been to Switzerland before, being able to visit it was one of the reasons I've decided to attend the meeting. At first I've declined since the period was overlapping with my family holiday but after I found out that it would be OK to spend a few days working at the meeting and the rest I could spend with my wife (we will be there together) I've decided to go.

<em>Thanks a lot, Cristian, for your time for the interview and dedication to KMyMoney and the KDE community.</em>

Please <a href="http://www.kde.org/fundraisers/randameetings2014/">support us</a> in the organization of the Randa Meetings 2014.