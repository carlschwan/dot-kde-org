---
title: "Plasma Team Gathers in Barcelona"
date:    2014-01-24
authors:
  - "sebas"
slug:    plasma-team-gathers-barcelona
---
In the second week of January, KDE's Plasma team gathered in the Blue Systems office in Barcelona, Spain, to discuss and work on the next generation of KDE's popular workspace products. The meeting comes just at the right time, as the Plasma team has just finished a <a href="http://dot.kde.org/2013/12/20/plasma-2-technology-preview">first technology preview</a>, which puts the base technology in place and allows for an evaluation of the current progress. It also gives an opportunity for more refined plans for a first stable release.

In this article, we will give you an impression of some of the topics which have been discussed. Please note that discussions have by now moved to the various online communication channels, such as mailinglists and IRC. Not all results are set in stone, they rather serve as blueprints for ongoing discussions.


<h2>Naming and repository structure</h2>
One of the things that had been discussed online, but has not been concluded, is the naming of Plasma's next release. A proposal has been made to continue calling KDE's workspace products "Plasma", possibly extended as "Plasma by KDE". The term "workspace" will be on the way out, as it bears rather little meaning to most users. Plasma simply refers to all the workspace products, with the technology itself taking care of the distinguishing UI per device.

In line with this thinking, the repository structure will be changed. There is a number of interesting repositories, which are oriented towards likely deployment scenarios. The plasma-frameworks repository contains the library pieces needed to run a Plasma environment and build applications using Plasma technology. The kde-workspace repository will be split into a generic repository, which contains device-independent components. Then there will be repositories for different form-factor UIs. This means, that on a typical desktop system, one would install plasma-framework, plasma-generic and plasma-desktop. To add support for other devices, one can simply add another repository (such as a mediacenter or tablet user experience), and a specialized UI will be offered for this.

<h2>Supporting Plasma</h2>
Plasma, being a central product for KDE, receives many bugreports. In order to provide good support, better prioritization and focus is needed. For this reason, the team plans to structure the different components in Bugzilla in a way that makes it easier to identify higher priority problems (for example in default components), and make a clearer distinction between "officially supported" and "community-supported" components. This should lead to improved stability and shorter reaction times for the core components of the desktop.

<h2>Launcher</h2>
One of the things that kept the Plasma developers busy during the sprint was the question what to do with the main application launcher. This seems to be a question which has a different answer for almost every user. Plasma's strategy has been to offer a well-tested default (Kickoff), with other options to choose from (traditional menu, Lancelot). For the next release, this flexibility will remain intact. The idea is to replace the traditional menu with one that works similarly, but sports an updated look and some interaction improvements. The current version of Kickoff, which has already been reimplemented in QML will get a visual update.

<h2>KRunner Plans</h2>
The future of KRunner, Plasma's mini-commandline was discussed. There is a replacement in the works, though at this early stage is it unclear when it is going to land. In order to allow an alternative to fully mature before it replaces a core component, the team decided to port the current version of KRunner, and adapt it for improvements in the desktop search  area.

<h2>Logging in</h2>
The team also discussed the login procedure. While KDM has reached the end of its lifetime, being a fork of the ancient XDM, there are better alternatives on the horizon. The Plasma developers decided to improve and update the theming of LightDM and SDDM, although neither are currently perfect solutions. LightDM suffers from requirements around copyright assignment, which some developers refuse to sign; on the other hand, SDDM is not yet fully finished in terms of features needed.

<div style="float: right; padding: 1ex; margin: 1ex; width: 330px; border: 1px solid grey; "><a href="/sites/dot.kde.org/files/IMG_1491_v1.JPG"><img src="/sites/dot.kde.org/files/IMG_1491_v1_wee.jpg" /></a><br />Activity Switching (click for larger)<br /><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></div>
<h2>Activity Switching</h2>
Plasma developer Ivan Čukić presented a redesign of Plasma Desktop's activity switcher. The new design has a vertical layout and presents a more visual way to manage activities, move and assign windows, and add and rename them. The design presented was well thought out, and apart from a few visual changes, Ivan's concept was received very positively.

<h2>Notication Area Improvements</h2>
The team discussed the current version of the systemtray, also known as notification area. The new approach to use one popup dialog for all, which reduces visual clutter, was welcomed. The team has identified a few issues with the current implementation, which will be addressed in the coming weeks. One of the things that was brought up is that applications or services should have a way to enable a Plasma widget in the system tray. This could be useful for Bluetooth, for example, where a widget would show up as soon as the Bluetooth hardware is found or enabled. On the other hand, some widgets which do not make sense in a given hardware environment could be entirely hidden (for example, the battery widget on non-laptop systems).

<div style="float: right; padding: 1ex; margin: 1ex; width: 330px; border: 1px solid grey; "><a href="/sites/dot.kde.org/files/IMG_1396_v1.JPG"><img src="/sites/dot.kde.org/files/IMG_1396_v1_wee.jpg" /></a><br />Wayland discussion (click for larger)<br /><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></div>
<h2>Kwin/Wayland</h2>

KWin maintainer Martin Gräßlin presented the status of KWin and Wayland support in the window manager and compositor, and shared his plans for the future. Wayland support is well under way, although not everything is entirely clear from an architectural point of view. There is simply no example or reference implementation for many of the technical problems we face, so his work is, to no small degree, about covering new ground.

<h2>A new focus on design</h2>
One of the hot topics during the sprint was the visual and interaction design in Plasma. These things have been identified as needing a more structured approach. Thanks to the effort of Jens Reuterberg, an illustrator from Sweden who <a href="http://wheeldesign.blogspot.de/2014/01/my-very-first-sprint.html">recently joined</a> the Plasma team, work has commenced on forming a stronger design team. Another topic in this area was the creation of visual guidelines, which has already been started. The team hopes that these efforts will result in greater visual consistency and more elegance throughout the whole user experience.

<div style="padding: 1ex; margin: 1ex; width: 720px; border: 1px solid grey; "><a href="/sites/dot.kde.org/files/IMG_1652_v1_2.JPG"><img src="/sites/dot.kde.org/files/IMG_1652_v1_2_med2.jpg" /></a><br />Group photo: Left to right: (top) Martin Klapetek, Mitch Curtis, Ivan Čukić, Jens Reuterberg, David Edmundson, Martin Gräßlin, Aleix Pol, Giorgos Tsiapaliokas, Sebastian Kügler, Antonis Tsiapaliokas, (front) Marco Martin, Vishesh Handa, Àlex Fiestas (click for larger)<br /><small>by Martin Klapetek (<a href="http://creativecommons.org/licenses/by/3.0/">CC BY</a>)</small></div>

<h2>Much hacking</h2>
Of course there is not sprint without hacking. Many ideas have been put into code already, various pieces have been cleaned up, missing features were implemented. Everyone attending enjoyed the sprint, especially the productive and friendly atmosphere. The whole team is excited and already working on the results of the sprint.

Special thanks go to the <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://blue-systems.de/">Blue Systems</a> for supporting the sprint -- without their help it could not have happened.



