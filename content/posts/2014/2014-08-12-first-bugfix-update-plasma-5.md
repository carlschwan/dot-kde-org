---
title: "First Bugfix Update to Plasma 5"
date:    2014-08-12
authors:
  - "jriddell"
slug:    first-bugfix-update-plasma-5
comments:
  - subject: "Huh? What did you guys fix?"
    date: 2014-08-13
    body: "Huh? What did you guys fix? I'd be curious about that."
    author: "vdboor"
  - subject: "Changelog"
    date: 2014-08-13
    body: "I can't find the changelog anywhere. Could you provide a link to it please ?"
    author: "yves"
  - subject: "+1, KDE SC releases usually"
    date: 2014-08-13
    body: "+1, KDE SC releases usually have a bugzilla link to fixed_in_this_release bugs. Has that link just been forgotten from the announcement, or does bugzilla not contain the info ?\r\n\r\nFrom a software maturity POV it's very interesting to read details of bugfix releases, especially for a \".0\" release.\r\n\r\nThanks in advance."
    author: "moltonel"
---
<div style="width: 360px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="https://community.kde.org/File:Mascot_20140702_konqui-plasma.png"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-plasma_wee_0.png" /></a></div> 
KDE is now getting into the swing of releases numbered 5.  Today we add <a href="http://kde.org/announcements/plasma-5.0.1.php">Plasma 5's first bugfix update</a>.  The release features KDE's flagship desktop project as well as the base software needed to keep your computer running.  Plasma will have feature releases every three months and bugfix releases in the months in between.

This update adds all the translations for the last month as well as a bunch of fixes for issues such as using the right icons, showing toolbars on second screens, using translations and fixes for right to left text.  Grab your <a href="https://community.kde.org/Plasma/Packages">distro packages</a> or you can try the new <a href="http://cdimage.ubuntu.com/kubuntu-plasma5/daily-live/">Kubuntu Plasma 5</a> images.
<!--break-->
