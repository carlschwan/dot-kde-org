---
title: "LaKademy 2014 - KDE Latin America Summit"
date:    2014-08-20
authors:
  - "araceletorres"
slug:    lakademy-2014-kde-latin-america-summit
---
Two years have passed since the reality of the first Latin American meeting of KDE contributors in 2012 in Porto Alegre, capital of Rio Grande do Sul, Brazil. Now we are proud to announce that the <a href="https://br.kde.org/lakademy-2014">second LaKademy</a> will be held August 27th to 30th in São Paulo, Brazil, at one of the most important and prestigious universities in the world—the <a href="http://en.wikipedia.org/wiki/University_of_S%C3%A3o_Paulo">University of São Paulo</a>.

<div style="position: relative; left: 50%; padding: 1ex; margin-left: -350px; width: 700px; border: 0px solid grey"><a href="https://br.kde.org/lakademy-2014"><img src="/sites/dot.kde.org/files/800px-Lakademy2014.png"  height="300" /></a></div> 

In this LaKademy we intend to do something different than what we did in 2012. It's not going to be just an event to bring together KDE contributors in Latin America, who will dedicate time and passion in hacking sessions. This time we be in touch with the KDE user community and attract possible future KDE contributors. Thus, we prepared some activities such as talks and short courses for the public that will be taught by KDE members.

The event will be four days long. Its program reflects the diverse fields of KDE: there will be talks on systems administration, development with Qt (the programming library that forms the foundation under most KDE software), KDE and Qt software on Android, artwork and more. Specific technical sessions will be dedicated to topics such as developing educational software, networks, translation and software internationalization. The event will also include cultural activities, such as the <a href="https://garoa.net.br/wiki/Konvescote_-_Encontro_com_a_comunidade_KDE">Konvescote</a> at the <a href="https://garoa.net.br/wiki/P%C3%A1gina_principal">hackerspace Garoa</a>.

The <a href="https://br.kde.org/lakademy-2014">LaKademy 2014 website</a> has more information about the program, directions to the venue and registration instructions.

 Put LaKademy on your calendar and come join KDE community!
