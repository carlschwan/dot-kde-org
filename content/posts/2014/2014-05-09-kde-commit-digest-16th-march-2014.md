---
title: "KDE Commit-Digest for 16th March 2014"
date:    2014-05-09
authors:
  - "mrybczyn"
slug:    kde-commit-digest-16th-march-2014
---
In <a href="http://commit-digest.org/issues/2014-03-16/">this week's KDE Commit-Digest</a>:

<ul><li>KWallet adds support for pam-kwallet in kwalletd</li>
<li>KWin introduces a X-KWin-Internal in kwineffect services</li>
<li>Krita shows rulers in pixel units by default (user can change this)</li>
<li>Smb4k implements permanent (re)mounting of shares</li>
<li>Choqok adds preview of images from Twitter.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-03-16/">Read the rest of the Digest here</a>.
<!--break-->