---
title: "KDE Commit-Digest for 2nd March 2014"
date:    2014-04-06
authors:
  - "mrybczyn"
slug:    kde-commit-digest-2nd-march-2014
---
In <a href="http://commit-digest.org/issues/2014-03-02/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://edu.kde.org/parley/">Parley</a> adds support for sound in multiple modes, allows the use of image instead of word for flashcard training</li>
<li><a href="http://okular.org/">Okular</a> adds Magnifier and play/pause in the presentation mode</li>
<li><a href="http://community.kde.org/KDE_PIM">KDE-PIM</a> adds <a href="https://projects.kde.org/projects/playground/base/kde-accounts/kaccounts-integration">KAccounts</a> support to Google resources</li>
<li><a href="http://krita.org/">Krita</a> uses the <a href="https://code.google.com/p/google-breakpad/wiki/GettingStartedWithBreakpad">google-breakpad crashhandler</a> for Krita on Windows.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-03-02/">Read the rest of the Digest here</a>.