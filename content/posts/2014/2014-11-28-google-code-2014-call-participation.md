---
title: "Google Code In 2014 : Call for Participation"
date:    2014-11-28
authors:
  - "jriddell"
slug:    google-code-2014-call-participation
---
<img src="http://3.bp.blogspot.com/-ufghVvqzHX8/VG34OiIM5JI/AAAAAAAACm0/iqvv_c8CIrU/s1600/GCI_2014_logo_small.png" width="300" height="145" align="right" />

The <a href="https://developers.google.com/open-source/gci/">Google Code-in</a> is a contest to introduce pre-university students (ages 13-17) to the many kinds of contributions that make open source software development possible. The contest runs from December 1, 2014 to January 19, 2015. For many students the Google Code-in contest is their first introduction to open source development. 

KDE is selected among the 12 organizations, who are selected as a mentoring organization for Google Code In 2014. 

<b>Who can mentor? </b>

Well, we need mentors to help the students with tasks. Mentors should be developers of KDE and should be aware of the tasks they would be creating. 

<b>Why KDE?</b>

KDE focuses on the development and distribution of free and open source software for desktop and portable computing, has been a proud GCI mentoring organization for the last four years. 

<b>Who can participate ? </b>

Pre-university students 13-17 years old from all over the world can choose from a large pool of code, documentation, research, quality assurance and user interface tasks. The pool is created by the mentors of the participating open source organizations who continue to add to it throughout the contest. A task is a set of work in one of the above five categories that can be completed in a short time, taking approximately a few hours to a day to complete. In addition to self-contained tasks, task series are also created where similar work is split into several tasks or related work is split into sequential tasks. This way all sorts of work can be converted into manageable pieces for open source newbies.

<b>Directions for mentors :</b>
Discuss tasks with the related team to let others know what you are up to.  Subscribe to <a href="https://mail.kde.org/mailman/listinfo/kde-soc-mentor">kde-soc-mentor</a>. Create a mentor account at <a href="http://www.google-melange.com/gci/profile/register/org_member/google/gci2014">Google Melange</a>

<b>Directions for students (from 1 December onwards)</b>

Create a student account at <a href="http://www.google-melange.com/">Google Melange</a>.  Go through the tasks provided by KDE.    Explore the issue and discuss any queries with the assigned mentor and #kde-soc as required.   Complete the task and repeat.
