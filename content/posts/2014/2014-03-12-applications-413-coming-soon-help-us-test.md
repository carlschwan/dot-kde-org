---
title: "Applications 4.13 Coming Soon, Help Us Test!"
date:    2014-03-12
authors:
  - "jospoortvliet"
slug:    applications-413-coming-soon-help-us-test
comments:
  - subject: "RAW support in Gwenview"
    date: 2014-03-12
    body: "Photographers shooting raw images can focus on the RAW preview support in our favourite image viewer! :) Thanks"
    author: ""
  - subject: "Live-CD for testing purposes?"
    date: 2014-03-12
    body: "4.13 sounds like a great step forward, especially baloo sounds promising. But why is there no Live-CD to help with testing upcoming releases?\r\n\r\nThis would increase the number of bug hunters. If you feel like you do not want to give one distribution favourable treatment, maybe a rotation could be set up. For one release it is a Live-CD based on Fedora and for the next release it could be based on Kubuntu, etc.\r\n\r\nI am not a programmer just an ordinary user but I feel like this is such an obvious oversight that I wonder why the creation of a Live-CD is not part of every beta testing effort. \r\n\r\nOther than that I am thankful for your great work and wish you happy bug hunting!"
    author: "Erik"
  - subject: "agreed"
    date: 2014-03-14
    body: "Yes, a live CD would make for a great addition to the testing efforts. It is not entirely trivial to do but http://susestudio.com and the openSUSE KDE beta packages could make creating such a image doably by anybody. It just takes time - if you are willing to do it, I'll promote it over the KDE social media. Just ping me...\r\n\r\nOf course this offer is valid for anybody who is willing to help."
    author: "jospoortvliet"
  - subject: "Live testing image"
    date: 2014-03-15
    body: "There is now a live testing image: https://susestudio.com/a/pRvzFf/kde-applications-4-13b2\r\n\r\nIf packages are missing, tell me. Artikulate is missing due to a missing dependency, sorry..."
    author: "jospoortvliet"
  - subject: "Image is updated with"
    date: 2014-03-21
    body: "Image is updated with Artikulate and to Beta 3.\r\n\r\nhttps://susestudio.com/a/pRvzFf/kde-applications-4-13b2"
    author: "jospoortvliet"
  - subject: "I would really like to test"
    date: 2014-03-23
    body: "I would really like to test the new KDE stuff. However, if a Facebook or Google account is required, then I have no more interest. Why does everyone constantly needs access to my personal details?! So I'd rather wait ... until the final is available"
    author: "Anonymous"
  - subject: "Kubuntu 14.04 LTS Beta 2"
    date: 2014-03-29
    body: "<a href=\"http://www.kubuntu.org/news/kubuntu-1404-lts-beta-2\">Kubuntu 14.04 LTS Beta 2</a> is also availabe to test with KDE SC 4.13 beta"
    author: "sealne"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/searching%20dragon_0.png" /></div>

Last week, <a href="http://dot.kde.org/2014/03/06/kde-ships-first-beta-applications-and-platform-413">the first beta of Applications and Platform 4.13 was released</a>. This week, beta 2 is coming. The openSUSE team has already <a href="http://www.dennogumi.org/2014/03/4-13-beta-1-workspaces-platform-and-applications-for-opensuse-start-the-testing-engines/">asked its users to <em>start the testing engines</em></a> and that request extends to the entire community of KDE users!

<h2>What's to be tested?</h2>
Let's go over a list of major and minor changes in this release, and areas where developers have explicitly asked us for help.

<h3>Search</h3>
A major new improvement is the introduction of <a href="http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search">KDE’s next generation Semantic Search</a>. This makes search faster, saves memory, improves stability, and generates more reliable search results. And it could use a good testing.

Various applications use the search abilities, most notably Dolphin and KDE PIM (see the next section). Also tagging (Gwenview!) and KRunner (Alt-F2 run command dialog) can use some attention.

Some of your existing data will need to be migrated from the current Nepomuk backend to the new 'Baloo' backend. Running the <em>nepomukbaloomigrator</em> should take care of that. The old Nepomuk support is considered “legacy” (but it is still provided). The programs that have not yet been ported to the new architecture have Nepomuk integration disabled. One significant regression is file-activity linking, which will not work until KDE Applications and Platform 4.14. If you rely on this feature, we recommend not upgrading at this time. For the final release, distributions might choose to optionally have the old search (Nepomuk) available.

<h3>Kontact</h3>
The Kontact Suite (email, calendaring, contacts and more) benefits from the improvements in search; there is also a new quick filter bar and search. IMAP will be more reliable, and performance should be massively improved. There is also a <a href="http://www.aegiap.eu/kdeblog/2014/01/new-in-kdepim-4-13-sieveeditor/">brand new sieve editor</a> and integration with cloud storage functions, where Kontact can automatically put big attachments on Box/DropBox/Hubic/Kolab/ownCloud/UbuntuOne/WebDav/YousendIt and link to them. Last but not least, there's an update to KNotes that needs testing.

<h3>Okular, Kate and Umbrello</h3>
Document viewer Okular has a lot of new features like tabs, media handling and a magnifier, improved Find and Undo/Redo.

Text editor <a href="http://kate-editor.org/">Kate</a> has gotten a lot of attention, so there are many new features in the areas of further VIM style support, bracket matching, highlighting and more. You can read <a href="http://kate-editor.org/">the blogs on the Kate site</a> and test some of that awesome.

The UML modeling application Umbrello received some improvements and bugfixes. If you use it, now is a good time to help out a little and see if it works better! There is new duplication of diagrams and improvements to the context menus (which only shows relevant actions).

<h3>Education and Games</h3>
We received a special request from developer Ian Wadham:
<blockquote>Please give the new version of Palapeli jigsaw puzzling a whirl. This contribution to KDE is my celebration of 50 years as a programmer. I started in April 1964.

If you ever enjoyed jigsaw puzzles, especially those 500 and 1,000 piece boxed puzzles, please take a look at the new version of Palapeli. The main thing is its attempt to make solving large puzzles (300 to 10,000 pieces) possible, realistic and enjoyable on a small screen.  You can make your own large puzzle from any photo or picture file you fancy.

So I am very interested in *usability* feedback (look and feel). As well as bugs, of course.  I am currently "testing" on a 10,000 piece puzzle ... The Handbook changes should be finished in a few days, but there is already a long help message that appears when you start a large puzzle (> 300 pieces).

The new features are described, but in a technical way, <a href="http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames">in the usual place<a/>.

Have fun, everyone.</blockquote>
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/bugs_green.png" /></div> 
<a href="https://projects.kde.org/projects/kde/kdeedu/artikulate/">Artikulate</a> (<a href="http://techbase.kde.org/Projects/Edu/Artikulate">technical information</a>) is a brand new application in KDE Edu and will have its first official release with KDE Applications 4.13. Find some information about it on <a href="https://community.kde.org/KDEEdu/Artikulate">community.kde.org</a>.

<h2>And how does that work?</h2>
Testing follows these steps:
<ol>
<li>set up your testing environment</li>
<li>pick something to test</li>
<li>test it</li>
<li>back to 2 until something unexpected/bad happens</li>
<li>check if what you found is really a bug</li>
<li>file the bug</li>
</ol>

<h3>You're not alone!</h3>
In KDE, testing is not only an individual action by our users but it's also coordinated through the <a href="https://community.kde.org/Getinvolved/Quality">KDE Quality team</a>. That does not mean you <em>must</em> work or coordinate with them, but it sure helps! You can reach them on <a href="irc://freenode/#kde-quality">IRC</a>, as well as on <a href="https://mail.kde.org/mailman/listinfo/kde-testing">their mailing list</a>.

The testing of this beta is also coordinated on <a href="http://forum.kde.org/viewforum.php?f=286">this forum page</a> for those more comfortable on forums.

The <a href="https://community.kde.org/Getinvolved/Quality">KDE Quality Team wiki page</a> is worth a read if you're unexperienced. There is even a real <a href="http://community.kde.org/Getinvolved/Quality/Tutorial">tutorial on becoming a KDE tester</a>!
<h3>Get the beta and prepare</h3>
To get testing, you can either build the source of the Beta or RC, or <a href="http://community.kde.org/KDE_SC/Binary_Packages">grab packages for your distribution</a>. If your distro is not on that list but you know there are packages, you can add them there!

The second step is to create a testing user account. We recommend this to prevent destroying data on your current account. Many users also use a separate installation of KDE software on a separate partition.

On most flavors of Linux, creating a new user is easy. On the command line, it goes a bit like this (as root):
<ul><li><em>useradd -m kde-test</em></li>
<li><em>passwd kde-test</em></li></ul>
And now you've created a user kde-test and given the account a password. Just switch user accounts (<em>menu - leave - switch user</em> or <em>Alt-F2 - switch</em>) and have fun testing!

<h3>The real testing</h3>
Testing is a matter of trying out some scenarios you decide to test, for example, pairing your Android phone to your computer with KDE Connect. If it works – awesome, move on. If it doesn't, find out as much as you can about why it doesn't and use that for a bug report.

This is the stage where you should see if your issue is already reported by checking on the <a href="http://forum.kde.org/viewforum.php?f=286">forum</a>, <a href="irc://freenode/#kde-quality">IRC channel</a> or <a href="https://mail.kde.org/mailman/listinfo/kde-testing">mailing list</a>. It might even be fixed, sometimes! It can also be fruitful to contact the developers <a href="https://mail.kde.org/mailman/listinfo">on the relevant mailing list</a>.

Finally, if the issue you bump into is a clear bug and the developers are not aware of it, file it on <a href="https://bugs.kde.org/enter_bug.cgi?format=guided">bugs.kde.org</a>.

<h3>How else can I help?</h3>
Another useful contribution is triaging bugs:
<ul><li>determine if it's really a bug (it can be reproduced)</li>
<li>find out which component has the bug and</li>
<li>assign or cc the maintainer of that component</li></ul>

If you can’t reproduce a bug, the bug might have to be marked as “WORKSFORME” or “NEEDINFO” if you can’t reproduce it due to a lack of information. And in some cases, the bug report is plain wrong (“Plasma doesn’t make coffee“) and must be closed as “INVALID”. You can find more information in <a href="http://techbase.kde.org/Contribute/Bugsquad/Guide_To_BugTriaging">the Ultimate Bug Triaging Guide</a>. As long as you can't close bugs on bugzilla, you can just add your information as comments and they will be picked up by a maintainer – it is just as useful!

<h2>It is a big help!</h2>
We're very grateful for your help in this. Not all areas of our many applications receive the same amount of care and attention, and there may not always be an immediate reply to bug reports. However, developers greatly appreciate the attention given to their applications by users and testers.

<em>KDEntomologists rule!</em>