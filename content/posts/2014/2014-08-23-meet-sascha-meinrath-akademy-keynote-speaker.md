---
title: "Meet Sascha Meinrath - Akademy Keynote Speaker"
date:    2014-08-23
authors:
  - "kallecarl"
slug:    meet-sascha-meinrath-akademy-keynote-speaker
---
<div align="center" style="float: center; padding: 1ex; margin: 1ex; margin-top: 0ex; "><img src="/sites/dot.kde.org/files/SaschaInterview.png"><br /><div align="center">Sascha Meinrath<br /><small><small>photo from fisl quinze (<a href="http://en.wikipedia.org/wiki/F%C3%B3rum_Internacional_Software_Livre">Fórum Internacional do Software Livre</a>) <a href="https://creativecommons.org/licenses/by-nc-sa/2.0/">CC-BY-SA</a></small></small></div></div>
A few weeks ago, the <a href="https://dot.kde.org/2014/07/30/akademy-2014-keynotes-sascha-meinrath-and-cornelius-schumacher">keynote speakers</a> for Akademy were announced. KDE is fortunate to have Sascha Meinrath at Akademy in Brno, Czech Republic to open our eyes about hot topics and important issues. Sascha's work doesn't fit into limited categories; he's an activist, think tank founder, policy pundit, hacker, futurist, political strategist and more...as the following interview shows.  

<big><strong>For people want to know more about you</strong></big>
Tech policy and political strategy work can often be both high-impact and high-stress. To relax, I like to cook -- not the sort of "oh hey, I can cook a few dishes o.k." cooking -- more, "you should open a gourmet restaurant". For the past 8 years, I've been hosting an epicurean feast called "Basque" (long story), which usually brings together 1-5 dozen people for 2-4 dozen courses. We've done everything from cooking with dangerous chemicals to building a KitchenAid-powered lamb rotisserie.

I'm also an avid gardener -- which, I suppose, is an unusual skill for a technologist, having done a lot of permaculture over the years. And I bike around town as much as possible, play guitar, and enjoy working with my hands whenever I get the chance. 

Prior to my work in Washington DC, I did a lot with a movement called Indymedia -- which pioneered digital media documentation tactics that are now standard during protests and unrest. I've been chased, lied to, beaten, teargassed, and otherwise had my civil and human rights trampled upon by police while doing nothing more than documenting their behavior -- which was an initial catalyzer for developing the technologies my teams have pioneered over the past 14 years.

<big><strong>What's your setup?<</strong></big>
I'm running a (heavily modded) version of Ubuntu on a Lenovo X1 Carbon. Thus, my monitor size is small for day-to-day activities. I also have a multi-media server hooked into a projector that faces a 10+ foot screen (hand built by me to have the perfect dimensions for my media room) -- so either small or mind-blowingly ginormous, depending on whether I'm seeking to kill bad legislation or zombies (sometimes it's difficult to know which is which). I also have a 4-year old daughter, which means I am thoroughly adept at mixing "kiddie cocktails", building towers, sneaking around the house like "cat-princess-ninjas" and being subjected to tickle sneak attacks again and again and again... and again.

<big><strong>Why should someone attend your talk? How will people's realities be affected?</strong></big>
Today, throughout the geekosphere, almost everyone is thinking about how to secure their communications over inherently insecure networks. No one's paying attention to major leaps forward in circumvention technology -- not to just keep personal information private, but also to create entire alternative infrastructures that are far more difficult to surveille, control, censor, and shut down. I'll provide a world-wide survey of the state-of-the-art in circumvention *infrastructure* -- and point to the resources participants need to build their own systems, whether within their neighborhood or community, city, or region. I'll explain tools that are available -- both fully legal and ones whose deployment is the equivalent of electromagnetic jaywalking but may prove vital in many of the worlds' hot spots as well as in people's own back yards.

<big><strong>What are some important issues for different kinds of free and open technology over the next few years?</strong></big>
We need an entire alternative ecosystem -- I worry that we're winning the battle (to create functional equivalents of proprietary software) while losing a war over the basic control of the hardware we use. We're heading into a CryptoWar II epoch -- where surveillance is moving out of the networks and into our edge devices -- which means that we need to think differently about everything from how to maintain our privacy to how we fundamentally communicate. The core fault line is over the locus of control over new technologies -- either it resides with us (the end users) or we're simply serfs in a 21st Century Digital Feudalism. It is a very stark, and very real, battle.

<big><strong>What is distinctive and important about FOSS? And about KDE?</strong></big>
FOSS, as exemplified by KDE, is about placing control in the hands of its users. We are heading into an era that will be exemplified by an "Internet of Things" that surveille, intrude, and control our private lives in ways we currently think unimaginable. Within that near-future, FOSS and KDE are liberatory opportunities -- the potential to develop a different societal trajectory for the future of a computer-mediated world.

<big><strong>Torvalds or Stallman?</strong></big>
I'll definitely take a cranky old bastard who's continuing to push the envelope over a game-changing developer (no matter how talented). To me, Stallman exemplifies the never-ending quest to liberate society writ large -- it's not enough to rest on our laurels or declare things "good enough" -- until everyone is fully liberated from Digital Feudalism, visionaries like Richard Stallman provide leadership and guidance on where we should focus our next efforts.

<h2>Akademy 2014 Brno</h2>
For most of the year, KDE—one of the largest FOSS communities in the  world—works online by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2014">Akademy</a> provides all KDE contributors the opportunity to meet in person to  foster social bonds, work on concrete technology issues, propose and consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and other types of KDE  contributors to celebrate the achievements of the past year and help determine the direction for the following year. Hands-on sessions offer the opportunity for intense work to bring those plans to reality. The KDE Community welcomes companies building on KDE technology, or looking to begin using it.

If you are someone who wants to make a difference with technology, <a href="https://akademy.kde.org/2014">Akademy 2014</a> in Brno, Czech Republic is the place to be.