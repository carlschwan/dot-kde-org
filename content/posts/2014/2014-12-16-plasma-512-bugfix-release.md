---
title: "Plasma 5.1.2 Bugfix Release"
date:    2014-12-16
authors:
  - "jriddell"
slug:    plasma-512-bugfix-release
---
<a href="https://www.kde.org/announcements/plasma-5.1.2.php">Plasma 5.1.2</a> is the December output from our desktop team. It's a bugfix release which adds several dozen fixes and the latest translations.
     
Some highlights include:
<ul>
<li>The Breeze icons licence has been clarified as LGPL 3+.
 <li>The remaining battery time in PowerDevil now updates correctly.
 <li>VirtualBox shell resizing fixed.
 <li>The free space notifier icon hides correctly when space becomes available again.
 <li>Dr Konqi updated for future proofing.
</ul>

This is the final update to Plasma 5 for this year, we'll see you again next year with a new feature release at the end of January.
<!--break-->
