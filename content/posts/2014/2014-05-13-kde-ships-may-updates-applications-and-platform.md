---
title: "KDE Ships May Updates to Applications and Platform"
date:    2014-05-13
authors:
  - "unormal"
slug:    kde-ships-may-updates-applications-and-platform
---
Today KDE released <a href="http://www.kde.org/announcements/announce-4.13.1.php">updates for its Applications and Development Platform</a>, the first in a series of monthly stabilization updates to the 4.13 series. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 50 recorded bugfixes include improvements to Personal  Information Management suite Kontact, Umbrello UML Modeller, the Desktop search functionality, web browser Konqueror and the file manager Dolphin. A more complete <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2013-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.13.1&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">list of changes</a> can be found in KDE's issue tracker.

To find out more about the 4.13 versions of KDE Applications and Development Platform, please refer to the <a href="http://www.kde.org/announcements/4.13/">4.13 release notes</a>.