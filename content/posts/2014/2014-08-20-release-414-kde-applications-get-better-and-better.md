---
title: "Release 4.14 - KDE Applications get better and better"
date:    2014-08-20
authors:
  - "kallecarl"
slug:    release-414-kde-applications-get-better-and-better
---
The KDE Community <a href="http://kde.org/announcements/4.14/">has announced</a> the latest major updates to KDE Applications delivering primarily improvements and bugfixes. Plasma Workspaces and the KDE Development Platform are frozen and receiving only long term support; those teams are focused on the transition to Plasma 5 and Frameworks 5. This 4.14 release is dedicated to long time KDE contributor Volker Lanz who passed away last April. The <a href="http://kde.org/announcements/4.14/">full announcement</a> has more information and details.

<div style="width: 360px; float: right; padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /><br /><div align="center">KDE Applications</div></div>
In the past, KDE has jointly released the three major divisions of KDE software—Plasma Workspaces, KDE Development Platform and KDE Applications. The KDE Development Platform is being reworked into KDE Frameworks. The monolithic libraries that comprise the Development Platform are becoming independent, cross platform modules (KDE Frameworks 5) that will be readily available to all Qt developers. Plasma Workspaces is being moving to a new technology foundation based on Qt5 and KDE Frameworks 5. With the 3 major KDE software components moving at different paces, their release schedules are now separated. For the most part, KDE's 4.14 release involves KDE Applications.

<h2>Development Platform/KDE Frameworks 5</h2>
The <a href="https://dot.kde.org/2013/09/25/frameworks-5">modular Frameworks structure</a> will have widespread benefits for KDE software. In addition, Frameworks is a substantial <a href="https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers">contribution to the Qt ecosystem</a> by making KDE technology available to all Qt developers.

<h2>Plasma Workspaces</h2>
<a href="https://dot.kde.org/2014/07/15/plasma-5.0">Plasma 5</a> was recently released after 3 years of work; it is on its own release schedule with feature releases every three months and bugfix releases in the intervening months. The Plasma team has built a solid foundation that will support Plasma Workspaces for many years.

<h2>KDE Applications</h2>
Release 4.14 is not about lots of "new and improved stuff". Many KDE developers are focused on the Next Experience (Plasma 5) or porting to KDE Frameworks (based on <a href="http://qt-project.org/qt5">Qt5</a>). Mostly, the 4.14 release is needed by aspects of our workflow (such as translations). This release offers more software stability, with little emphasis on new and less-proven stuff. People who want the latest and greatest KDE software may want to experiment with the Plasma 5 Workspace.

There are over 200 actively maintained <a href="http://kde.org/applications/">KDE applications</a>. Many of them are listed in the <a href="https://userbase.kde.org/Applications">KDE userbase</a>. Wikipedia also has another <a href="http://en.wikipedia.org/wiki/List_of_KDE_applications">list of KDE applications</a>.

Most previous releases had highlights of new features and prominent applications. This gave some people the impression that KDE developers favored new-and-shiny over quality, which is not true. So, for this announcement of the 4.14 release, developers were asked for details—small, incremental improvements and bugfixes that might not even be noticeable to most users. These are the kinds of tasks that most developers work on, the kinds of tasks that allow beginners to make meaningful, mostly invisible contributions. The <a href="http://kde.org/announcements/4.14/">announcement</a> has examples of the kinds of improvements that KDE developers have made in this release.

Thank you to all KDE developers working on behalf of people all over the world.

<h2>Spread the Word</h2>
Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the 4.13 releases. Report bugs. Encourage others to join the KDE Community. Or <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5">support the nonprofit organization behind the KDE community</a>.

Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.14 release.

Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from Twitter, YouTube, flickr, PicasaWeb, blogs, and other social networking sites.

<h2>Learning more and getting started</h2>
Find more details and download links in <a href="http://kde.org/announcements/4.14/">the announcement on the KDE website</a>.
