---
title: "KDE Ships January Updates to Plasma Workspaces, Applications and Platform"
date:    2014-01-08
authors:
  - "tnyblom"
slug:    kde-ships-january-updates-plasma-workspaces-applications-and-platform
comments:
  - subject: "Catalyst"
    date: 2014-01-12
    body: "Catalyst has supported it for 6 months."
    author: "d2kx"
  - subject: "Buffer Age Implementation in KWin"
    date: 2014-01-08
    body: "With 4.11.5 KWin got a major performance improvement with the support of buffer age extension on drivers which support it."
    author: "mgraesslin"
  - subject: "NVIDIA?"
    date: 2014-01-08
    body: "NVIDIA proprietary driver only, or some other drivers too?  Anyway, thanks for all your work on improving kwin."
    author: "Vajsvarana"
  - subject: "Updates versioning"
    date: 2014-01-08
    body: "So this latest update of 4.11 comes after the release of 4.12?"
    author: "alvise"
  - subject: "Yup"
    date: 2014-01-09
    body: "Correct. And: <cite><em>\"current generation Plasma Workspaces will continue to receive updates until August 2015.\"</em></cite> so there will be more."
    author: "jospoortvliet"
  - subject: "at the moment I think NVIDIA"
    date: 2014-01-09
    body: "at the moment I think NVIDIA is the only driver providing the extension for GLX. But it's generic and should just work if the drivers add support for it."
    author: "Martin Gr\u00e4\u00dflin"
---
Today KDE <a href="http://www.kde.org/announcements/announce-4.11.5.php">released updates for its Workspaces, Applications and Development Platform</a>. These updates are the fifth in a series of monthly stabilization updates to the 4.11 series. Plasma Workspaces and the KDE Platform are frozen and receiving only long term support. Workspace development efforts are focused on <a href="http://dot.kde.org/2013/12/20/plasma-2-technology-preview">Plasma 2</a>; current generation Plasma Workspaces will continue to receive updates until August 2015. KDE Platform is in transition to <a href="http://dot.kde.org/2014/01/07/frameworks-5-tech-preview">Frameworks 5</a>. So this release is focused on KDE Applications. The release contains only bugfixes and translation updates, and will be a safe and pleasant update for everyone.

Several recorded bugfixes include improvements to the personal information management suite Kontact, the UML tool Umbrello, the document viewer Okular, the web browser Konqueror, the file manager Dolphin, and others.The Plasma calculator can handle Greek letters now, and Okular can print pages with long titles. A bugfix brought Konqueror better web fonts support.

A more complete list of changes can be found in <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.11.5&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">KDE's issue tracker</a>. Browse the Git logs for a detailed list of changes.

To find out more about the 4.11 versions of KDE Workspaces, Applications and Development Platform, please refer to the <a href="http://www.kde.org/announcements/4.11/">4.11 release notes</a>.