---
title: "KDE Commit-Digest for 12th January 2014"
date:    2014-02-10
authors:
  - "mrybczyn"
slug:    kde-commit-digest-12th-january-2014
comments:
  - subject: "KDM"
    date: 2014-02-10
    body: "It doesn't seem to be in the commit message - what's the chosen replacement for KDM? I know there has been a lot of talk about LightDM, perhaps that is it...?"
    author: "bluelightning"
  - subject: "Interesting question. Just"
    date: 2014-02-11
    body: "Interesting question. Just putting \"Removed KDM\" into a commit digest without any further explanation seems weird to me. It's not like KDM is a minor, unimportant part of the system."
    author: "me"
  - subject: "... or SDDM."
    date: 2014-02-11
    body: "... or SDDM.\r\nAFAIK there is no decision yet."
    author: "tom100"
  - subject: "digest of Commit Digest"
    date: 2014-02-11
    body: "This article is a news flash about the <a href=\"http://commit-digest.org\"> full Commit Digest</a>, which is \"a weekly overview of the development activity in KDE.\" A little poking around reveals <a href=\"http://fedoraproject.org/wiki/Changes/SDDMinsteadOfKDM\">more about KDM and SDDM</a>. Someone may be by later to explain further."
    author: "kallecarl"
  - subject: "There is not yet a chosen"
    date: 2014-02-11
    body: "There is not yet a chosen replacement, but any login manager can be used. The currently most promising options are LightDM and SDDM. We've investigated the situation during a recent sprint, and briefly discussed the result on the Plasma mailinglist, you can find the  thread <a href=\"http://osdir.com/ml/plasma-devel/2014-01/msg00205.html\">here</a>.\r\n\r\nNot having kdm on top of Frameworks 5 is not an immediate problem (of course one can still use KDM from KDE SC 4.x).\r\n\r\n"
    author: "sebas"
  - subject: "Re: There is not yet a chosen"
    date: 2014-02-13
    body: "OK, thanks for the clarification."
    author: "bluelightning"
---
In <a href="http://commit-digest.org/issues/2014-01-12/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://en.wikipedia.org/wiki/KDE_Display_Manager">KDM</a> has been removed</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> improves the attachment editor; better ability to switch between inline and linked attachments, allows undo/redo archive mail</li>
<li><a href="http://skrooge.org/">Skrooge</a> adds options in "Income & Expenditure" dashboard widget</li>
<li><a href="http://nepomuk.kde.org/">Nepomuk</a> adds Baloo migrator</li>
<li><a href="http://edu.kde.org/kgeography/">KGeography</a> porting to KF5 in progress</li>
<li>Bug fixes in <a href="http://www.kdevelop.org/">KDevelop</a> and more.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-01-12/">Read the rest of the Digest here</a>.