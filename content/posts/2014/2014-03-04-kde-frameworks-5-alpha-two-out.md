---
title: "KDE Frameworks 5 Alpha Two Is Out"
date:    2014-03-04
authors:
  - "jospoortvliet"
slug:    kde-frameworks-5-alpha-two-out
comments:
  - subject: "Alpha2 imported to homebrew"
    date: 2014-03-06
    body: "For those of you on Mac OS X, you can now install most Alpha 2 packages from my homebrew tap at https://github.com/haraldF/homebrew-kf5"
    author: "Harald Fernengel"
---
<div style="width: 304px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://blog.martin-graesslin.com/blog/2014/02/running-frameworks-powered-applications-on-wayland/"><img src="http://dot.kde.org/sites/dot.kde.org/files/tabbox-icon-sizes1.png" /></a><br />Frameworks 5 based apps on Wayland</div>Today KDE released the second alpha of Frameworks 5, part of a series of releases leading up to the final version planned for June 2014. This release includes progress since the <a href="http://dot.kde.org/2014/02/14/kde-frameworks-5-alpha-out">previous alpha</a> last month.

See <a href="http://www.kde.org/announcements/announce-frameworks5-alpha2.php">the announcement on kde.org</a> for more information and links to downloads. For information about Frameworks 5, see <a href="http://dot.kde.org/2013/09/25/frameworks-5">this earlier article on the dot</a>.