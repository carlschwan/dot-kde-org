---
title: "KDE Releases Applications and Development Platform 4.13"
date:    2014-04-16
authors:
  - "jospoortvliet"
slug:    kde-releases-applications-and-development-platform-413
comments:
  - subject: "Akonadi is still broken!!!"
    date: 2014-04-16
    body: "Sweet Lord, the upgrade from the 4.12 to 4.13 breaks Akonadi again!\r\n\r\nBug filed, but watch out.  https://bugs.kde.org/show_bug.cgi?id=331867\r\n\r\n"
    author: "joe"
  - subject: "Kubuntu 14.04 LTS"
    date: 2014-04-18
    body: "<a href=\"http://www.kubuntu.org/news/kubuntu-14.04\">Kubuntu 14.04 LTS</a> has just been released fresh with KDE SC 4.13.  It even has a working Akonadi :)\r\n"
    author: "Jonathan Riddell"
  - subject: "Akondi is broken on upgrade..."
    date: 2014-04-18
    body: "Please see above. Akonadi is NOT working on upgrade."
    author: "joe"
  - subject: "After every boot I get baloo"
    date: 2014-04-19
    body: "After every boot I get baloo crash, nice and stable release, good job!"
    author: "linux-void"
  - subject: "search@KMail"
    date: 2014-04-22
    body: "Yup, needs manual cleanup after upgrade (ubuntu 13.10->14.04)\r\nAt least kmail search works as (i) expected right now; thumbs up! \r\n1) speed - all mailbox of few thousands emails indexed ~1 min, search results appear without delay\r\n2) function wise - nice shortcuts below searchbox . waiting now for google style keywords from:, to: etc ...\r\n\r\nWonder how the rest of the desktop works in 4.13 but too scared to move away from ldxe\r\n\r\n"
    author: "Karol"
  - subject: "It's been fixed"
    date: 2014-04-22
    body: "Since today this issue has been fixed: you may want to ask your distribution to provide the fix in their packages.\r\n\r\nhttp://commits.kde.org/akonadi/4ca8b846baaad48ebbd723f6411f9571a3b0f5ad"
    author: "einar"
  - subject: "openSUSE packages"
    date: 2014-04-24
    body: "What's going at openSUSE? Usually they are the first updating their KDE packages. Now my Kubuntu machine has KDE 4.13 from its default repositories, but the suse one has still not get any update from the \"backports\" repository here:\r\nhttp://download.opensuse.org/repositories/KDE:/Current/openSUSE_13.1/\r\nOnly the factory repo contains 4.13 packages: http://download.opensuse.org/repositories/KDE:/Distro:/Factory/openSUSE_13.1/\r\nAre there any packaging issues?"
    author: "fabma"
  - subject: "kde repositories"
    date: 2014-04-25
    body: "In case you are still wondering - details all on this page:\r\nhttps://www.dennogumi.org/2014/04/kdecurrent-and-4-13-packages-for-opensuse/ "
    author: "Gerry G"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="http://dot.kde.org/sites/dot.kde.org/files/release-4.13.gif" /></div>
April 16 2014 - The KDE Community <a href="http://kde.org/announcements/4.13/">proudly announces</a> the latest major updates to KDE Applications delivering new features and fixes. Major improvements are made to KDE's Semantic Search technology, benefiting many applications. With Plasma Workspaces and the KDE Development Platform frozen and receiving only long term support, those teams are focusing on the transition to Frameworks 5. This release is translated into 53 languages; more languages are expected to be added in subsequent monthly minor bugfix releases.

<h2>KDE Applications 4.13 Benefit From The New Semantic Search, Introduce New Features</h2>
The KDE Community is proud to announce the latest major updates to the KDE Applications delivering new features and fixes. Kontact (the personal information manager) has been the subject of intense activity, benefiting from the improvements to KDE's Semantic Search technology and bringing new features. Document viewer Okular and advanced text editor Kate have gotten interface-related and feature improvements. In the education and game areas, we introduce the new foreign speech trainer Artikulate; Marble (the desktop globe) gets support for Sun, Moon, planets, bicycle routing and nautical miles. Palapeli (the jigsaw puzzle application) has leaped to unprecedented new dimensions and capabilities. <a href="http://kde.org/announcements/4.13/applications.php">read the announcement</a>.

<div style="width: 300px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/okular_big.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/okular_small.png" /></a></div>

<h2>KDE Development Platform 4.13 Introduces Improved Semantic Search</h2>
The KDE Development Platform libraries are frozen and receive only bugfixes and minor improvements. The upgrade in the version number for the Development Platform is only for packaging convenience. All bug fixes and minor features developed since the release of Applications and Development Platform 4.11 have been included. The only major change in this release is the introduction of an improved Semantic Search, which brings better performance and reliability to searching on the Linux Desktop.

Development of the next generation KDE Development Platform—called KDE Frameworks 5—is in beta stage. Read <a href="http://dot.kde.org/2013/09/25/frameworks-5">this article</a> to find out what is coming and <a href="http://kde.org/announcements/">see here</a> for the latest announcements.

<h3>Improved Semantic Search</h3>
The major new addition to the KDE Development Platform is the <a href="http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search">next generation Semantic Search</a>. To maintain compatibility, this is included as a new component rather than a replacement for the previous Semantic Search. Applications need to be ported to the new search component; most KDE Applications have already been ported. Downstream distributions can decide whether or not to ship the deprecated Semantic Search alongside the new version.

The improvements to search bring significant benefits in terms of faster, more relevant results, greater stability, lower resource usage and less data storage. The upgrade requires a one-time database migration that will take a few minutes of increased processing power.

<div style="width: 128px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="http://dot.kde.org/sites/dot.kde.org/files/kblogger.png" /></div>

<h2>Spread the Word</h2>
Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the 4.13 releases. Report bugs. Encourage others to join the KDE Community. Or <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5">support the nonprofit organization behind the KDE community</a>.

Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.13 releases.

Follow what is happening on the social web at the KDE live feed, <a href="http://buzz.kde.org">buzz.kde.org</a>. This site aggregates real-time activity from Twitter, YouTube, flickr, PicasaWeb, blogs, and other social networking sites.

<h2>Learning more and getting started</h2>
Find more details and download links in <a href="http://kde.org/announcements/4.13/">the announcement on the KDE website</a>.