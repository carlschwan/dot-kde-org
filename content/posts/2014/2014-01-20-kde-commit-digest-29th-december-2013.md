---
title: "KDE Commit-Digest for 29th December 2013"
date:    2014-01-20
authors:
  - "mrybczyn"
slug:    kde-commit-digest-29th-december-2013
---
In <a href="http://commit-digest.org/issues/2013-12-29/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://umbrello.kde.org/">Umbrello</a> offers more intelligent context menus: when multiple widgets are selected, a context menu is shown containing only the actions that work on the whole selection; better undo support and visual properties improvements</li>
<li><a href="http://marble.kde.org/">Marble</a> supports nautical miles as a measurement system</li>
<li><a href="http://www.digikam.org/">Digikam</a> improves detection of new images on the camera and sees a bug in face detection fixed</li>
<li><a href="http://krita.org/">Krita</a> improves usability of Shade Selector</li>
<li>Multiple bug fixes in Krita, digikam, <a href="http://kmymoney2.sourceforge.net/index-home.html">KMyMoney</a> and more.</li>
</ul>

<a href="http://commit-digest.org/issues/2013-12-29/">Read the rest of the Digest here</a>.