---
title: "Randa Meetings Interview Three: Vedant Agrawal"
date:    2014-06-27
authors:
  - "devaja"
slug:    randa-meetings-interview-three-vedant-agrawal
---
<div style="float: right; padding: 1ex; margin: 1ex;"><img width="200" height="225"  src="/sites/dot.kde.org/files/vedu_1.jpg" /><br />Vedant Agarwala</div>

Thanks again for your further support of the <a href="http://www.kde.org/fundraisers/randameetings2014/index.php">Randa Meetings fundraising</a>. We have now reached almost 40% of the our goal and there is still time to go. Please help even more and spread the word. If we reach our goal we can have an even more stable <a href="http://www.kdenlive.org">Kdenlive</a>, more applications ported to <a href="http://community.kde.org/Frameworks">KDE Frameworks 5</a>, further progress on Phonon, a look at <a href="http://amarok.kde.org">Amarok 3</a>, even better <a href="http://edu.kde.org">KDE educational applications</a>, a finished port of <a href="http://www.gcompris.net">GCompris</a> to Qt and KDE technologies, an updated KDE Book, more work on Gluon and a new and amazing KDE SDK!

Here is another little snippet of the musings of a young student, Vedant Agarwala, from India who is doing his Google Summer of Code project with KDE this year.

<em>Could you describe yourself in a few lines and tell us where you're from?</em>

I am a Computer Science Engineer, currently in my final year of graduation from National Institute of Technology, Durgapur.

<em>How did you first chance upon KDE? Could you describe your journey in short?</em>

Towards the end of my first year in college I wanted to do some real world coding - code that actually had some meaning and that would be really useful to someone - as opposed to college assignments/evaluations - and so I had a talk with one of my college seniors who was a two-time Google Summer of Code student. He was a KDE developer and suggested I do the same. That was the beginning of my journey into Linux, Open Source and KDE. It has been uphill since and two years later; here I am; eagerly awaiting arrival at a KDE Sprint.

<em>Could you describe your Google Summer of Code experience and GSoC project in short?</em>

So far my GSoC experience is really nice. Some really busy times researching, coding, testing, debugging. I am improving the way lyrics are fetched and displayed in <a href="http://amarok.kde.org">Amarok</a>. Personally; I like to follow the lyrics of the song that is playing; so I added this idea to the <a href="http://community.kde.org/GSoC/2014/Ideas">Ideas Page</a> for GSoC 2014. And now here I am, working on it. The goal of my project is to highlight the particular line from the entire lyrics text that is being played.
My ultimate goal is to add the features that I have promised. I hope I achieve it. Not only would it improve the Amarok experience, it would pave the way to developments in Amarok; like karaoke. I love karaoke (even though I am very bad at it). Though it is out of the scope of this project to implement a karaoke feature, implementing LRC support is halfway to karaoke.

<div style="float: right; padding: 1ex; margin: 1ex;"><img width="600" height="225"  src="/sites/dot.kde.org/files/amarok%20scolling%20lyrics.png" /><br />Amarok - a powerful music player for Linux, Unix and Windows</div>

<em>Could you tell us how GSoC helps many students like you both in increasing their knowledge as well as in experience?</em>

GSoC is a great program for us students who get paid for working on Free Software. A lot is learnt while writing code over a period of three months. It is different from bug fixes that take typically a week. Also, the mentorship and two evaluations by Google (on which the stipend depends) are great motivators.
Rather than spending time writing closed source code for companies (in internships), students can spend their summer vacation contributing to the world of Free Software. 

<em>Why is KDE so special to you?</em>

KDE to me is freedom. I used windows before KDE software and I felt like a bird let out of a cage.  

<em>Is this your first time at Randa? </em>

Yes it is my first time. To any KDE conference actually. 

<em>When did you first hear about the meetings in Randa and why do you wish to be a part of it?</em>

I heard about this meeting in Randa from Myriam on IRC. I'd heard that she, Mark and other developers of Amarok and KDE were going to be there and so I thought that it would be a nice opportunity to meet them in person. 

<me>Which specific area of KDE Applications do you contribute to?</em>

I work on Amarok - KDE's music player.

<em>What is your specific role in the particular group of KDE Applications that you are a part of and how long have you been working?</em>

I contribute code, do some code reviews, and fix bugs. I have been working on it for one and half years now.

<em>Have you got anything in particular planned for Randa?</em>

I have planned to talk about porting Amarok to Qt5 and talk with Amarok developers to decide what Amarok 3.0 will be like.

<em> Why do you think meetings such as Randa are very important for KDE and for Free Software communities around the globe?</em>

The contributors of Free Software do not receive any monetary perks for the work they do. Sponsored meetings like this is a nice incentive, especially for students. 

<em>Is this your first time to Switzerland? Are you excited about being in another country?</em>

I went to Switzerland when I was 5 years old. I haven't been abroad after that. Those memories are a haze but I would love to re-live them this time.

<em>Thanks a lot, Vedant, for your time for the interview and dedication to Amarok and the KDE community.</em>

Please <a href="http://www.kde.org/fundraisers/randameetings2014/">support us</a> in the organization of the Randa Meetings 2014.