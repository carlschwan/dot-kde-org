---
title: "KDE releases 3rd beta of Frameworks 5"
date:    2014-06-05
authors:
  - "sethkenlon"
slug:    kde-releases-3rd-beta-frameworks-5
---
<div style="float: right; padding: 1ex; margin: 1ex; width: 300px; "><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" /></div>Today KDE <a href="http://kde.org/announcements/announce-frameworks5-beta3.php">makes available the third beta of Frameworks 5</a>. This beta release is part of a series of pre-releases leading up to the final version planned for July 2014.

This beta features multiple bug fixes, and the finishing touches required to ease the transition for developers to the newest iteration of the KDE Frameworks. This process has included contributions back to Qt5, the modularisation of the kdelibs, and general improvements to the components that developers can use to improve their applications and user experience. This pre-release improves co-installability with kdelibs4 and with future versions of KDE Frameworks (i.e. 6). This is also the first release with translations for Frameworks using the KDE's i18n translation system.

For more information about Frameworks 5, see <a href="http://dot.kde.org/2013/09/25/frameworks-5">this earlier article on the KDE News site</a>.