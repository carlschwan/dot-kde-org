---
title: "KDE Ships Third Beta of Applications and Platform 4.13"
date:    2014-03-20
authors:
  - "unormal"
slug:    kde-ships-third-beta-applications-and-platform-413
comments:
  - subject: "Live image for testing"
    date: 2014-03-21
    body: "Live testing image available here: https://susestudio.com/a/pRvzFf/kde-applications-4-13b2"
    author: "jospoortvliet"
---
The KDE community today released the third beta of Applications and  Development Platform 4.13. With API, dependency and feature freezes in place,  the focus is now on fixing bugs and further polishing. We kindly <a href="http://dot.kde.org/2014/03/12/applications-413-coming-soon-help-us-test">request your assistance</a> with finding and fixing issues.

A partial list of improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan">4.13  Feature Plan</a>. A more complete list of the improvements and  changes will be available for the final release in the middle of April.

This third beta release needs a thorough testing in order to improve quality  and user experience. A variety of actual users is essential to  maintaining high KDE quality, because developers cannot possibly test  every configuration. User assistance helps find bugs early so they can  be squashed before the final release. Please join the 4.13 team's  release effort by installing the beta and <a href="http://dot.kde.org/2014/03/12/applications-413-coming-soon-help-us-test">reporting any bugs</a>.

The <a href="http://www.kde.org/announcements/announce-4.13-beta3.php">official announcement</a> has information about how to install the betas.
