---
title: "KDE Commit-Digest for 2nd February 2014"
date:    2014-03-06
authors:
  - "mrybczyn"
slug:    kde-commit-digest-2nd-february-2014
---
In <a href="http://commit-digest.org/issues/2014-02-02/">this week's KDE Commit-Digest</a>:

<ul><li><a href="http://userbase.kde.org/Artikulate">Artikulate</a> improves filtering, can show phrases containing native speaker recordings</li>
<li><a href="http://pim.kde.org/">KDE-PIM</a> adds email filtering and contact auto-completion via Baloo</li>
<li><a href="http://skrooge.org/">Skrooge</a> offers new "responsive" template for monthly reports</li>
<li>Bug fixes in Calligra, KGet.</li>
</ul>

<a href="http://commit-digest.org/issues/2014-02-02/">Read the rest of the Digest here</a>.