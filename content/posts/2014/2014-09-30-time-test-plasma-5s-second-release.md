---
title: "Time to Test Plasma 5's Second Release"
date:    2014-09-30
authors:
  - "jriddell"
slug:    time-test-plasma-5s-second-release
comments:
  - subject: "System settings"
    date: 2014-10-01
    body: "So.. what happened to the redesign of system settings which was planed three months ago? And what happened to the new breeze windeco, on which Martin Graesslin was working on if I am not mistaken?\r\n\r\nAnd what about all the yet to be ported plasmoids, on which work was started? \r\nAnd what about the calendar integration in the clock?\r\n\r\n"
    author: "Burke"
  - subject: "Love the new kde!"
    date: 2014-10-05
    body: "Congratulations are in order. I always disliked KDE and preferred other DEs, but the new KDE Framework/Plasma 5 is amazing and a lot snappier. It's going to be my default DE from now on."
    author: "rvx"
  - subject: "I asked for help"
    date: 2014-10-06
    body: "<cite>And what happened to the new breeze windeco, on which Martin Graesslin was working on if I am not mistaken?</cite>\r\n\r\nI asked for help several times. Unfortunately there wasn't enough help to get KDecoration2 into a shape that we could use it in Plasma 5.1. So as you asks what happened to it, I ask: why didn't you help?"
    author: "mgraesslin"
  - subject: "Weather Widget"
    date: 2014-10-15
    body: "Please, we need a weather widget. I can't make the change without one.\r\nCheers!"
    author: "Weather"
---
Put your testing hats on, <a href="https://www.kde.org/announcements/plasma-5.0.95.php">Plasma 5 has a beta release</a>.  The second version of Plasma 5 is due out in under two weeks and now is your chance to test it for bugs which have crept in.  It features a bunch of missing features which have been added back such as the much requested icon only task bar.  The <a href="https://vdesign.kde.org/">Visual Design Group</a> have been hard at work over the last three months adding a Qt 4 Breeze theme to make all KDE applications fit into the desktop and many new icons as part of the Breeze icon theme.  Check for <a href="https://community.kde.org/Plasma/Packages">packages for your distro</a> and try it out</a>.
