---
title: "KDE and Google Summer of Code 2014"
date:    2014-02-25
authors:
  - "sethkenlon"
slug:    kde-and-google-summer-code-2014
---
KDE is happy to announce that it has been accepted as a mentoring organization for <a href="http://www.google-melange.com/gsoc/homepage/google/gsoc2014">Google Summer of Code 2014</a>. This will allow students from around the world to work with mentors on KDE software projects. Successful students will receive stipends from Google.

<div align=center style="padding: 1ex; margin: 1ex;"><img src="http://dot.kde.org/sites/dot.kde.org/files/GoogleSummer_2014logo700.jpg" /></div>

The Google Summer of Code program offers development funding as well as mentors to students who want to work on open source software projects. The program provides students the opportunity to learn more about coding, work within structured software development environments, and push bug fixes and new features into real-world, production software. For Google and the rest of the world, the program provides improvements to the software that millions of people use on a daily basis.

The ideas on what a student entering the GSoC program might work on are offered from developers and users alike. The KDE project has a list of potential projects for 2014 on the <a href="http://community.kde.org/GSoC/2014/Ideas">Community Wiki</a>. Depending on what students choose to develop, this could mean it will be a big year for popular KDE software such as Amarok, digiKam, Marble, Krita and the PIM suite as well as more niche applications and libraries.