---
title: "KDE at LISA 2014 conference"
date:    2014-11-30
authors:
  - "kallecarl"
slug:    kde-lisa-2014-conference
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/KDE_USENIX141118.png" /></div>

KDE was one of about 50 <a href="https://www.usenix.org/conference/lisa14/lisa-expo">exhibitors</a> at the <a href="http://www.usenix.org/lisa14/kde">LISA</a> (Large Installation System Administration) Conference November 12th and 13th in Seattle. The expo was part of the week-long conference for system administrators that has been held annually since 1986. Expo participants included big name tech companies and smaller niche organizations offering products and services to this audience of professional technical people. As we discovered, KDE is well known among this audience.
<!--break-->

Visitors included people who have been "using KDE since version 1.0" and other long time users and supporters. Several people said that they have been contributing code and money for many years. We encouraged visitors to check out <a href="https://www.kde.org/fundraisers/yearend2014/">the year-end fundraising campaign</a>. 

The big KDE logo attracted people to the exhibit space where we covered KDE mentoring programs, answered questions and shared information about current KDE activities. The <a href="https://krita.org/">Krita</a> demonstrations by <a href="http://ogbog.net/">Oscar Baechler</a> generated a lot of fascinated attention too. It is obvious that Krita offers much more to real artists than GIMP or Inkscape. The large artwork of an Emperor Penguin done in <a href="http://en.wikipedia.org/wiki/Coast_Salish_art">Coast Salish style</a> also drew admiring comments; people liked the tie-in between the Pacific Northwest aboriginal art and free/open technology. 

We had intended to have a slide show featuring various aspects of KDE. It didn't work out as planned...fortunately. Instead, people had the opportunity to experiment with the latest KDE goodies—<a href="https://dot.kde.org/2013/09/25/frameworks-5">KDE Frameworks 5</a> and <a href="https://dot.kde.org/2014/07/15/plasma-5.0">Plasma 5</a> running on a high performance machine. The system performed well. Several attendees were amazed at the story behind the story: a near complete re-do of the KDE Development Environment and the up-to-date Plasma Workspace. This was an audience that appreciated the effort, the professionalism and the results of KDE's innovations.

Andrew Lake of the KDE Visual Design Group put together a slide show for his big screen, featuring design principles and examples, along with a visual explanation of the technical structure of Frameworks and Plasma.

<div align="center" style="padding: 1ex; margin: 1ex; "><a href="/sites/dot.kde.org/files/20141112KDELISA.jpg"><img src="/sites/dot.kde.org/files/20141112KDELISA700.jpg" /></a><br />KDE excitement<br /><small><small>photo by <a href="http://ogbog.net/">ogbog </a></small></small></div>

Some members of the <a href="http://www.meetup.com/KDE-Users-Seattle">Seattle KDE group</a> discussed possibilities of more regional KDE outreach. Valorie Zimmerman and Andrew Lake are part of this group; the Meetup organizer, Aaron Peterson, was also in the booth space.

The LISA conference has long served as the annual vendor-neutral meeting place for the wider system administration community. Recognizing the overlap and differences between traditional and modern IT operations and engineering, the highly-curated 6-day program offers training, workshops, invited talks, panels, paper presentations, and networking opportunities around 5 key topics: Systems Engineering, Security, Culture, DevOps, and Monitoring/Metrics. 

LISA was an excellent opportunity to connect with people who know KDE well, as well as people who appreciate KDE software and what the Community stands for. 

Many thanks to <a href="http://www.usenix.org/lisa14/kde">USENIX</a> for the generous support of KDE.