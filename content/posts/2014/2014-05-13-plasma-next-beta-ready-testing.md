---
title: "Plasma Next Beta Ready for Testing"
date:    2014-05-13
authors:
  - "jriddell"
slug:    plasma-next-beta-ready-testing
comments:
  - subject: "Look"
    date: 2014-05-14
    body: "Looks great. Neat and professional."
    author: "alexoi"
  - subject: "Please, hire a visual designer"
    date: 2014-05-14
    body: "I really praise the technical effort, but why KDE can't, once and for all, fix its main issue: it looks like it's designed by software developers. \r\nIcons are ugly (settings, KDE itself etc...), too much chrome, shadows in UI looks feels late 90's, all sort of visual inconsistencies even in this limited screenshot (why there is a star for 'Download' ??   OK should be always to the right etc)\r\n\r\ni genuinely hope they will realise it, and fix it. "
    author: "andy"
  - subject: "OK should be always to the"
    date: 2014-05-14
    body: "<cite>OK should be always to the right<cite>\r\n\r\nSorry, but I don't follow you here. I want my ok button on the left, thank you."
    author: "Solerman"
  - subject: "They have :)"
    date: 2014-05-14
    body: "*With a comforting voice:* Have you really read the article in detail?  There is a KDE Visual Design Group which you could report these infos to and it's mentioned in the article. I mostly agree with that, but just complaining will not help. There are numerous ways to reach the Designers and Developers. They've done a great work here. I honestly like the design. You're basically saying that you want a \"FLAT UI\" like you are used to with \"Windows 8.1 and iOS 7.1\". Maybe worth a try, removing gradients wouldn't harm, I guess. \r\n\r\nPS: You can also just fix things yourself if you feel motivated and commited to the community =) \r\nBut before you do that you should communicate with the responsible persons. Just so that your efforts don't get put into something that's already being worked on."
    author: "Anonymous"
  - subject: "Hi..."
    date: 2014-05-14
    body: "We're around ten visual designers working on Plasma Next - the issue is that we have half a year to do something AND we can't just change things automagically. There are a ton of things coming along but like we've said all along it has to happen bit-by-bit. You may notice that the window decorations and the widget theme are just plain old oxygen - and the reason for that is that Qtcurve and the window decorations aren't stable - BUT if you're a C++ programmer please say so that issue is something we DO need help with.\r\n\r\nI know its easy to assume that visual design is something you just go swish-swash and its fixed. It's not if you intend to do it well and considering the complexity of Plasma it will take longer - we've said this since the beginning. Considering that to most casual users what you see is the widget, window decoration and icons. There is soooo much more that you need to take care of.\r\n\r\nIn the forums you can find information on the work and where it's heading. You can see the work done so far and whats needed and you can join in. Feel free to do so.\r\n"
    author: "Jens Reuterberg"
  - subject: "the ui looks quite close to"
    date: 2014-05-14
    body: "the ui looks quite close to the kde4 ui\r\nwhich hey, is probably a good thing ;)"
    author: "Anonymous"
  - subject: "This is actually happening"
    date: 2014-05-14
    body: "This is <a href=\"http://wheeldesign.blogspot.se/\">actually happening now</a>, but it looks like a lot of the new visuals didn't make it into the first beta. Still, if you look at the panel in the screen shot, that's a lot cleaner than before."
    author: "Logi"
  - subject: "This is being taken care of"
    date: 2014-05-14
    body: "The KDE Visual Design Group, with the help of the community, is tackling these issues at the KDE forums. The new themes aren't shipping with this release since they aren't 'fully baked'. If you'd like to see the progress and give your feedback, please check out the blog and drop by the Visual Design Group subsection of the forums. http://vdesign.kde.org/\r\n\r\nThe work is certainly not limited to themes- there are also proposed improvements to layout across the desktop, and much work has already been done to improve things on a fundamental level (switching to QML, as mentioned in this post, being one of them). So fret not, there are many people striving to improve these areas which have admittedly been a sore spot for a while."
    author: "Jack"
  - subject: "Actually, it is just a very"
    date: 2014-05-14
    body: "Actually, it is just a very rough, unfinished work in terms of user interface ;-)"
    author: "jospoortvliet"
  - subject: "Linux desktop always seems to"
    date: 2014-05-14
    body: "Linux desktop always seems to be 5-10 years behind when it comes to visual design..."
    author: "ferdg"
  - subject: "Please, volunteer"
    date: 2014-05-14
    body: "KDE is an open source project, and is a do-ocracy. Step up and offer assistance."
    author: "greggit"
  - subject: "Keep it up!"
    date: 2014-05-15
    body: "Awesome! I took a look at the blog/concepts and I think you guys are heading in a great direction! KDE4 was around quite a while before Gnome3, Unity, Windows 8, Android, iOS7, etc. People have adapted to seeing minimal/flat UI everywhere and don't seem to realize that despite the interface being simpler, you can't just immediately change an entire OS to be like that. It's going to take time, but I have a feeling you guys have a good concept of where you want to take KDE, and I'm stoked to see it and give KDE another shot after all these years!"
    author: "Rowan"
  - subject: "I like it"
    date: 2014-05-15
    body: "Looks great.  It doesn't follow the new \"flat\" design trend, so yes, it does look a little dated.  Very clean, though, and something I can live with.  One thing I would like to see more \"modern\" though is the icons - something with more sillohettes, less shadows, and more simplified color palettes."
    author: "matt c"
  - subject: "Looks great"
    date: 2014-05-15
    body: "I especially like the blue top border in the taskbar that identifies the active/focused window. I think the active window effect in KDE4 is too subtle. The new Status & Notifications widget looks a lot better too. I'm happy I downloaded the ISO, keep up the great work."
    author: "pwesselius"
  - subject: "A screenshot of 'work in"
    date: 2014-05-15
    body: "A screenshot of 'work in progress' shows a lot more 'flat' ;-)\r\n\r\nhttp://wstaw.org/m/2014/05/11/loving_it.png\r\n"
    author: "jospoortvliet"
  - subject: "This is not the final design"
    date: 2014-05-15
    body: "Please note that this is not the final look at all - as the announcement states:\r\n<blockquote><strong>Design</strong> is not yet finalized. Much of the work on theming has not made it in yet and the state of design in this beta is not representative for the first Plasma Next release.</blockquote>\r\n\r\nPerhaps this image gives a little more of an idea of what is coming:\r\nhttp://wstaw.org/m/2014/05/11/loving_it.png\r\n\r\n(of course that, too, is work in progress and will probably not be what it looks like in the end)"
    author: "jospoortvliet"
  - subject: "Panel widgets"
    date: 2014-05-15
    body: "Three things:\r\n1) I hope we can select to use monochrome white icons as well.\r\n2) The former battery icon is much more interesting in my opinion.\r\n3) Please let the weather forecast widget the way it is now, i. e. don't make it monochrome.\r\n\r\nCheers."
    author: "George"
  - subject: "Concur..."
    date: 2014-05-15
    body: "I've used KDE since KFM 0.0.1 when it was released in January of 1997.  KDE has always felt a bit cheap when it comes to design.  I love the developers, I love the community and honestly I think KDE has a technical superiority over GTK/Gnome.  But when it comes to themes and look and feel.... eww...  Course, the same can be said for Ubuntu's Unity.  *sigh*\r\n\r\nWe need to clone Garrett LeSage, Tigert, etc... send them to work for all the major *nix distro's and then we'll have awesomeness. :)"
    author: "Trae McCombs"
  - subject: "Great thanks for kde team.I"
    date: 2014-05-19
    body: "Great thanks for kde team.I want to download Neon5 ISO,but can't I update kde through Terminal?"
    author: "linchengshen"
  - subject: "Cool"
    date: 2014-05-20
    body: "Now .... that's beautiful"
    author: "Mario"
  - subject: "thank you"
    date: 2014-05-23
    body: "This screen shot makes KDE look amazing.  I have been adverse to using KDE for so long, and have tried it a few times.  But this.... this is where things are going.  It is clean, crisp beautiful!\r\nIf KDE slims down on memory consumption and gets a handle on the dependencies for all the apps I think more and more people will be using it."
    author: "Israel"
  - subject: "Very Nice"
    date: 2014-05-25
    body: "It's look very nice and professional. I hopefully you can develop further. It has corporate and a serious air. I like it!\r\nNice work!"
    author: "Mira\u00e7"
---
<a href="http://kde.org/announcements/plasma-next-beta/ss-wallies.png"><img src="http://kde.org/announcements/plasma-next-beta/ss-wallies-wee.png" style="float: right; padding: 1ex; margin: 1ex; border: 0; background-image: none; " width="400" height="250" /></a>KDE is pleased to announce that <a href="http://kde.org/announcements/announce-plasma-next-beta1.php">Plasma Next Beta 1 has been released</a>.  Plasma Next is the codename for the new version of our beautiful desktop workspace built on KDE Frameworks 5.  It features the same familiar layout you will be used to but with a simplified and more slick look from the new <a href="http://vdesign.kde.org/">KDE Visual Design Group</a>.  For the first time our desktop ships with its own font, the <a href="https://www.google.com/fonts/specimen/Oxygen">Oxygen Font</a>.  Internally much has been rewritten in QML to make it smoother to render and easier to develop.  The source has been split into over 20 sources making it easier for distributions to package.  We need as many testers as possible to iron out the many bugs we know still exist.  Many distributions have started making packages so you can easily test it, listed on the <a href="http://community.kde.org/Plasma/Next/UnstablePackages">unstable packages wiki page</a>.  The easiest way to try it out is to download the <a href="http://files.kde.org/snapshots/">Neon5 ISO</a> and boot from it on a USB drive, Neon5 has the latest daily builds of Plasma code.  Please test it out and <a href="http://forum.kde.org/viewforum.php?f=287">let the Plasma team know</a> what to work on.<!--break-->

