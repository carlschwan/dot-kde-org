---
title: "KDE e.V. Quarterly Report for Q4 2013"
date:    2014-06-19
authors:
  - "devaja"
slug:    kde-ev-quarterly-report-q4-2013
---
The KDE e.V. Quarterly Report for the fourth quarter of 2013 features a brief note of all the activities and events carried out, supported and funded by KDE e.V in this span of time, as well as a short overview of the major events, conferences and mentoring programs. !ll of this in one document that you should not miss out on to know about almost everything that has been KDE in those four months!

<h2>Contents</h2>
The featured article covers all the students of GSoC 2013 with personal quotes and a first-hand note of their experiences and time and an overview of each student's project and contributions followed by another article covering the young participants of Google Code-in and the massive feats that school children achieve in such a short span of time. KDE has been involved with such mentoring programs since quite a while now but the enthusiasm, productivity and talent in the youth is something which still doesn't cease to surprise and awe the community members; same goes for all the participants who make KDE an integral part of their lives through such associations.

Another must read is the synopsis of the member activities around the globe - the Qt Developer Days in both Europe and US organized by John Layt and Carl Symons with Dario Freddi; respectively; the KDE EDU Sprint in October - which brought to light, discussion and loads of work - important issues such as KDE Frameworks 5/Qt Migration, math and language learning applications and many more such areas of focus. The KDEPIM Sprint in November has been covered in a fun and witty manner by Kevin Krammer and it is an article surely not to be missed in the report. A brief of the community events in France, primarily Akademy-fr in Toulouse and the occurrences at the event have been covered in another article.

For all those with a stronger affinity for numbers than letters; the finances for KDE e.V. for 2013 have been mentioned at the end.

The entire report can be found <a href="http://ev.kde.org/reports/ev-quarterly-2013_Q4.pdf">here</a>.

Now since everything that was exciting for 2013 has been noted by you, a glimpse of what's in store for 2014 can be obtained <a href="http://sprints.kde.org/sprint/212">here</a>. The Randa Meetings 2014 is scheduled soon, 9th to 15th of August to be precise. We could strongly use your <a href="http://www.kde.org/fundraisers/randameetings2014/index.php">support</a> in its organization.